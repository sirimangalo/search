1
00:00:00,000 --> 00:00:07,840
 Okay, good evening everyone. Welcome to our daily Dhamma.

2
00:00:07,840 --> 00:00:22,160
 Today's topic is, what is it like to be enlightened?

3
00:00:25,040 --> 00:00:32,750
 It's a good question, isn't it? It is what we're striving

4
00:00:32,750 --> 00:00:34,240
 for, ultimately, right?

5
00:00:34,240 --> 00:00:43,730
 So why is it good? Why is it a good question? Why is it

6
00:00:43,730 --> 00:00:46,520
 good to know?

7
00:00:50,840 --> 00:00:54,980
 I think obviously it's something we all are curious about

8
00:00:54,980 --> 00:00:56,200
 and want to know.

9
00:00:56,200 --> 00:01:01,330
 First of all, so that we know whether it's something that

10
00:01:01,330 --> 00:01:04,680
 might be worth attaining ourselves.

11
00:01:04,680 --> 00:01:09,700
 Also to know how far we are from it.

12
00:01:09,700 --> 00:01:15,880
 But another, I think, interesting point is

13
00:01:17,840 --> 00:01:22,520
 it allows us to direct ourselves in the right direction.

14
00:01:22,520 --> 00:01:33,200
 Even though we're not enlightened ourselves,

15
00:01:33,200 --> 00:01:37,680
 we can

16
00:01:37,680 --> 00:01:42,840
 mimic an enlightened being.

17
00:01:42,840 --> 00:01:45,960
 Mimic is not the right word. We can emulate.

18
00:01:46,800 --> 00:01:48,800
 It's the word.

19
00:01:48,800 --> 00:01:56,370
 Emulate is like when we keep the five precepts. When we don

20
00:01:56,370 --> 00:01:59,840
't kill, well, enlightened beings don't kill naturally.

21
00:01:59,840 --> 00:02:04,400
 For the unenlightened, it's something you have to work at.

22
00:02:04,400 --> 00:02:07,080
 So you emulate enlightened behavior.

23
00:02:10,520 --> 00:02:14,490
 Some cynic might say, "Well, you fake." But you know the

24
00:02:14,490 --> 00:02:17,630
 saying, "Fake it till you make it." There's something

25
00:02:17,630 --> 00:02:18,680
 interesting there because

26
00:02:18,680 --> 00:02:23,950
 the Tibetans believe that by pretending to be a Buddha or

27
00:02:23,950 --> 00:02:25,720
 not pretending by

28
00:02:25,720 --> 00:02:29,820
 really believing that you are a Buddha, you actually become

29
00:02:29,820 --> 00:02:31,520
 closer to being a Buddha.

30
00:02:31,520 --> 00:02:37,180
 And well, I don't think that that's, I don't agree that

31
00:02:37,180 --> 00:02:39,960
 that's adequate practice.

32
00:02:41,720 --> 00:02:43,460
 I don't know that the Tibetans really do either, though I

33
00:02:43,460 --> 00:02:46,400
 don't know too much about their tradition.

34
00:02:46,400 --> 00:02:49,200
 But

35
00:02:49,200 --> 00:02:55,320
 there's something to it. I mean, it gives you a roadmap.

36
00:02:55,320 --> 00:03:00,200
 It gives you some

37
00:03:00,200 --> 00:03:02,520
 markers to live by.

38
00:03:02,520 --> 00:03:05,420
 To know when you're acting like an enlightened being and to

39
00:03:05,420 --> 00:03:08,400
 know when you're not, so to know what you have to change.

40
00:03:08,400 --> 00:03:21,960
 So this is the question, "What is it like to be enlightened

41
00:03:21,960 --> 00:03:22,920
?"

42
00:03:22,920 --> 00:03:26,400
 There's a story that I think is,

43
00:03:26,400 --> 00:03:31,230
 I think is interesting. I've got two lists that I can think

44
00:03:31,230 --> 00:03:33,200
 of where the Buddha gave a list of

45
00:03:33,200 --> 00:03:36,520
 characteristics of what it might be like to be enlightened.

46
00:03:37,440 --> 00:03:39,440
 The first one is I think my favorite.

47
00:03:39,440 --> 00:03:45,750
 Sariputta, the Buddha asks Sariputta. Sariputta says

48
00:03:45,750 --> 00:03:46,560
 something

49
00:03:46,560 --> 00:03:54,200
 about the Buddha and the Buddha says, "Oh, so you have

50
00:03:54,200 --> 00:03:56,760
 faith in me that I'm enlightened?"

51
00:03:56,760 --> 00:03:59,860
 And Sariputta said, "No, I don't have faith in you that you

52
00:03:59,860 --> 00:04:01,160
're enlightened."

53
00:04:06,120 --> 00:04:08,120
 And the Buddha went away.

54
00:04:08,120 --> 00:04:12,310
 Sariputta was the Buddha's chief disciple, right? I mean

55
00:04:12,310 --> 00:04:14,800
 chief disciple saying, "Hey, I don't have faith in you."

56
00:04:14,800 --> 00:04:17,840
 And so the monks were all

57
00:04:17,840 --> 00:04:23,880
 disturbed by this, the unenlightened monks were disturbed

58
00:04:23,880 --> 00:04:26,480
 by it because they misunderstood what he meant. Of course,

59
00:04:26,480 --> 00:04:27,000
 he didn't mean

60
00:04:27,000 --> 00:04:31,070
 that Sariputta didn't mean he didn't think the Buddha was

61
00:04:31,070 --> 00:04:33,120
 enlightened, but he knew that the Buddha was enlightened.

62
00:04:34,240 --> 00:04:36,240
 It wasn't out of faith, right?

63
00:04:36,240 --> 00:04:40,060
 So they started talking about this, "Oh, Sariputta has no

64
00:04:40,060 --> 00:04:41,720
 faith in the Buddha."

65
00:04:41,720 --> 00:04:46,020
 And the Buddha hears them talking and he says, "What are

66
00:04:46,020 --> 00:04:48,240
 you guys talking about?"

67
00:04:48,240 --> 00:04:51,720
 They say,

68
00:04:51,720 --> 00:04:55,230
 they tell him and the Buddha said, "Yes, that's right. He's

69
00:04:55,230 --> 00:04:56,040
 faithless."

70
00:04:56,040 --> 00:05:01,840
 And he said, and he gave this verse, he said,

71
00:05:01,840 --> 00:05:06,800
 "Asadho akattanyu jasandhityedho jayonarou."

72
00:05:06,800 --> 00:05:11,140
 And these words you really have to know Pali to really get

73
00:05:11,140 --> 00:05:11,840
 the joke.

74
00:05:11,840 --> 00:05:14,380
 I mean, it's probably the closest, one of the closest

75
00:05:14,380 --> 00:05:16,240
 examples the Buddha comes to humor.

76
00:05:16,240 --> 00:05:22,360
 Asadho means faithless.

77
00:05:22,360 --> 00:05:27,480
 Akattanyu means, it's a word that means ungrateful.

78
00:05:27,480 --> 00:05:30,640
 A means not.

79
00:05:31,360 --> 00:05:35,410
 And then kattaa means what is done and anyu means one who

80
00:05:35,410 --> 00:05:36,320
 knows.

81
00:05:36,320 --> 00:05:39,920
 So it means one who doesn't know, means in the sense of

82
00:05:39,920 --> 00:05:43,880
 doesn't keep in mind the things that were done for them.

83
00:05:43,880 --> 00:05:46,080
 Someone does something for you, you just

84
00:05:46,080 --> 00:05:48,640
 forget about it, not interested.

85
00:05:48,640 --> 00:05:51,320
 Not grateful is what it means. Akattanyu.

86
00:05:51,320 --> 00:06:00,320
 Sanditjedo means one who breaks chains, breaks locks.

87
00:06:00,640 --> 00:06:02,640
 A lockpick.

88
00:06:02,640 --> 00:06:06,550
 So when you have, you know, they would lock up, lock their

89
00:06:06,550 --> 00:06:07,840
 gates in India.

90
00:06:07,840 --> 00:06:12,250
 I had to keep the robbers out and so a sanditjedo was a

91
00:06:12,250 --> 00:06:13,280
 robber.

92
00:06:13,280 --> 00:06:18,440
 Someone who picks locks, a housebreaker kind of thing.

93
00:06:18,440 --> 00:06:23,520
 Sandi means a chain and

94
00:06:24,320 --> 00:06:28,450
 sanditjedo means one who breaks and who cuts with whatever

95
00:06:28,450 --> 00:06:30,240
 they had to cut the chain.

96
00:06:30,240 --> 00:06:40,560
 Hattawakaso, someone who has destroyed all opportunity.

97
00:06:40,560 --> 00:06:52,000
 Someone who has destroyed all opportunity. It's for someone

98
00:06:52,000 --> 00:06:52,160
 who is

99
00:06:53,680 --> 00:06:58,370
 without any, you know, without any future. Like this is

100
00:06:58,370 --> 00:07:00,960
 what you'd say about a bum, someone who had dropped out of

101
00:07:00,960 --> 00:07:01,520
 school and

102
00:07:01,520 --> 00:07:04,520
 had no interest and maybe just did drugs all day or

103
00:07:04,520 --> 00:07:06,960
 something, drank alcohol all day.

104
00:07:06,960 --> 00:07:09,600
 So someone with no opportunity.

105
00:07:09,600 --> 00:07:12,440
 Hattawakaso.

106
00:07:12,440 --> 00:07:17,360
 Hantaso means someone who is hopeless.

107
00:07:17,360 --> 00:07:22,000
 Means someone who you have no hope in. You look at them and

108
00:07:22,000 --> 00:07:22,320
 you say

109
00:07:23,520 --> 00:07:26,500
 and you cannot hope good for that person. There is no hope

110
00:07:26,500 --> 00:07:28,720
 for them. All hope is lost.

111
00:07:28,720 --> 00:07:34,240
 Then the Buddha says,

112
00:07:34,240 --> 00:07:36,240
 Hattawakaso, one to someone then he says,

113
00:07:36,240 --> 00:07:38,240
 Sathway, utama puriso.

114
00:07:38,240 --> 00:07:40,240
 Utama puriso.

115
00:07:40,240 --> 00:07:42,560
 This is the height of humanity, he says.

116
00:07:42,560 --> 00:07:46,080
 He's talking about sareeputa.

117
00:07:47,120 --> 00:07:49,120
 Yeah.

118
00:07:49,120 --> 00:07:53,770
 And you have to know the Pali to know how this all makes

119
00:07:53,770 --> 00:07:59,050
 sense. The first one's easy to understand that's from the

120
00:07:59,050 --> 00:08:00,000
 story.

121
00:08:00,000 --> 00:08:02,640
 Asadho means

122
00:08:02,640 --> 00:08:06,260
 someone who doesn't have to believe anyone, not in regards

123
00:08:06,260 --> 00:08:07,680
 to what's important.

124
00:08:07,680 --> 00:08:11,360
 They have no faith in the Buddha because they know

125
00:08:11,360 --> 00:08:15,360
 it's knowledge, it's no longer belief.

126
00:08:15,360 --> 00:08:17,360
 Asadho.

127
00:08:17,360 --> 00:08:23,700
 All these, I got an argument with a Buddhist, when Sri Lank

128
00:08:23,700 --> 00:08:26,420
an Buddhist went, he said, oh, faith is so important. I said

129
00:08:26,420 --> 00:08:26,880
, oh, yeah.

130
00:08:26,880 --> 00:08:30,560
 Listen to the Buddha, he said, faithless is the best.

131
00:08:30,560 --> 00:08:37,200
 This kind of joking, faith is important, faith is useful.

132
00:08:37,200 --> 00:08:41,680
 And as a quality of mind, it's the same thing.

133
00:08:43,040 --> 00:08:45,670
 In fact, the faith is much stronger in the mind when you

134
00:08:45,670 --> 00:08:48,240
 actually know it. So it still is faith, but

135
00:08:48,240 --> 00:08:51,840
 in a conventional sense, we wouldn't call it faith because

136
00:08:51,840 --> 00:08:52,880
 you actually know,

137
00:08:52,880 --> 00:08:56,850
 if you know something to be true, you don't have to believe

138
00:08:56,850 --> 00:08:57,120
 it.

139
00:08:57,120 --> 00:09:00,160
 But technically, you still do believe it.

140
00:09:00,160 --> 00:09:05,920
 Akatanyu is an interesting compound because it actually can

141
00:09:05,920 --> 00:09:07,440
 mean two different things.

142
00:09:08,640 --> 00:09:13,300
 So, Akatanyu, katanyu means one who knows what is done, Ak

143
00:09:13,300 --> 00:09:15,360
atanyu, one who doesn't know.

144
00:09:15,360 --> 00:09:19,440
 But you can also split it, Akatat and Anyu.

145
00:09:19,440 --> 00:09:24,160
 One who knows what is Akata.

146
00:09:24,160 --> 00:09:27,540
 And Akata is that which is not made, that which is not

147
00:09:27,540 --> 00:09:28,240
 produced.

148
00:09:28,240 --> 00:09:31,830
 And there are only two things that are not, there's only

149
00:09:31,830 --> 00:09:33,440
 one thing, I guess, that's not made,

150
00:09:33,440 --> 00:09:36,240
 not produced, and that is Nibbana.

151
00:09:37,760 --> 00:09:41,440
 Nibbana is called the Akatadhamma. It's not made, it's not

152
00:09:41,440 --> 00:09:44,880
 produced, it's not caused.

153
00:09:44,880 --> 00:09:51,200
 So Sariputta is someone who knows Nibbana is what it means.

154
00:09:51,200 --> 00:09:55,920
 Sandhicheda is fairly easy because Sandhi is the chain,

155
00:09:55,920 --> 00:10:00,260
 and it's the chain of samsara, the chain of causation, you

156
00:10:00,260 --> 00:10:00,400
 know,

157
00:10:06,560 --> 00:10:09,520
 because of ignorance there is karma, because of karma there

158
00:10:09,520 --> 00:10:10,800
 is vinyana, birth.

159
00:10:10,800 --> 00:10:15,280
 If you cut that chain, if you cut out the ignorance,

160
00:10:15,280 --> 00:10:20,110
 then there is no karma, because there is no karma, there is

161
00:10:20,110 --> 00:10:20,640
 no rebirth.

162
00:10:20,640 --> 00:10:24,990
 Because there's no rebirth, then there is no suffering and

163
00:10:24,990 --> 00:10:25,680
 so on.

164
00:10:25,680 --> 00:10:28,800
 There's no craving, because there's no craving, there's no

165
00:10:28,800 --> 00:10:29,200
 clinging,

166
00:10:29,200 --> 00:10:33,280
 no clinging means no becoming and so on. You cut this chain

167
00:10:33,280 --> 00:10:33,280
.

168
00:10:33,280 --> 00:10:38,960
 [Hantavakaso]

169
00:10:38,960 --> 00:10:45,120
 Hantavakaso means someone who has no opportunity.

170
00:10:45,120 --> 00:10:47,760
 Opportunity here means opportunity for more arising,

171
00:10:47,760 --> 00:10:49,600
 especially of defilements.

172
00:10:49,600 --> 00:10:55,320
 In this person there's no opportunity for more karma, for

173
00:10:55,320 --> 00:10:56,560
 more becoming.

174
00:10:56,560 --> 00:10:59,870
 Everything they do is just functional, it's just, it's what

175
00:10:59,870 --> 00:11:00,560
 we think we are.

176
00:11:01,120 --> 00:11:04,160
 I mean it sounds like a zombie, but it's not, it's what we

177
00:11:04,160 --> 00:11:05,040
 think we are.

178
00:11:05,040 --> 00:11:07,600
 We think when I eat, I'm just eating, right?

179
00:11:07,600 --> 00:11:11,820
 And then when you come to meditate, you realize it's not

180
00:11:11,820 --> 00:11:12,640
 actually the case.

181
00:11:12,640 --> 00:11:19,110
 When we eat, we're lost, we're often not even anywhere near

182
00:11:19,110 --> 00:11:19,760
 the food,

183
00:11:19,760 --> 00:11:22,990
 our minds are off doing something else, or if they're near

184
00:11:22,990 --> 00:11:25,280
 the food, they're obsessing over it.

185
00:11:25,280 --> 00:11:30,480
 It's good, it's bad, it's too sweet, too salty, too plain,

186
00:11:30,480 --> 00:11:32,640
 too hot, too cold,

187
00:11:32,640 --> 00:11:36,240
 or it's just right, it's perfect, it's delicious.

188
00:11:36,240 --> 00:11:44,130
 An enlightened being is, there's no opportunity for any of

189
00:11:44,130 --> 00:11:44,960
 that.

190
00:11:44,960 --> 00:11:51,170
 There's no opportunity for defilements to get in, no

191
00:11:51,170 --> 00:11:54,000
 opportunity for Mara, for evil.

192
00:11:55,040 --> 00:11:59,580
 You can't hurt such a person, you can't trigger them, you

193
00:11:59,580 --> 00:12:02,560
 can't instigate them.

194
00:12:02,560 --> 00:12:11,840
 One hantavaka-so.

195
00:12:11,840 --> 00:12:15,200
 One taso, someone who is hopeless.

196
00:12:15,200 --> 00:12:17,360
 I mean this is a great word.

197
00:12:17,360 --> 00:12:19,440
 Someone who's hopeless means someone who doesn't hope.

198
00:12:19,440 --> 00:12:23,440
 Someone who hopes means they still want things.

199
00:12:25,440 --> 00:12:29,660
 Meditators hope that tomorrow will be, the pain will go

200
00:12:29,660 --> 00:12:31,760
 away tomorrow.

201
00:12:31,760 --> 00:12:35,040
 I had a bad day today, I hope that tomorrow's a better day.

202
00:12:35,040 --> 00:12:38,390
 Or I had a really good day, I hope tomorrow's just like

203
00:12:38,390 --> 00:12:40,000
 today or even better.

204
00:12:40,000 --> 00:12:46,720
 So here's a reminder for you, abandon all hope.

205
00:12:46,720 --> 00:12:49,920
 Abandon all hope you enter here.

206
00:12:52,400 --> 00:12:54,320
 Without hope, it's like without wanting.

207
00:12:54,320 --> 00:12:59,120
 If you have no hope, then you're already perfect.

208
00:12:59,120 --> 00:13:03,680
 It's really that simple.

209
00:13:03,680 --> 00:13:07,460
 If you want to be happy, the only way to be happy is to

210
00:13:07,460 --> 00:13:08,240
 stop wanting,

211
00:13:08,240 --> 00:13:10,480
 and so therefore to stop hoping.

212
00:13:10,480 --> 00:13:17,600
 Hope will always be a vulnerability, never be an asset.

213
00:13:21,360 --> 00:13:23,120
 It's not an asset to become enlightened.

214
00:13:23,120 --> 00:13:24,800
 You can't hope you're going to become enlightened.

215
00:13:24,800 --> 00:13:25,840
 That doesn't work that way.

216
00:13:25,840 --> 00:13:30,800
 Hope is a detriment because it means you're discontent.

217
00:13:30,800 --> 00:13:35,100
 It means you're not fully present here, objective with

218
00:13:35,100 --> 00:13:35,920
 reality.

219
00:13:35,920 --> 00:13:40,320
 You're biased, partial,

220
00:13:40,320 --> 00:13:47,280
 and therefore disappointed much of the time.

221
00:13:47,280 --> 00:14:17,200
 [

222
00:14:17,200 --> 00:14:18,480
 So that's familiar to me.

223
00:14:18,480 --> 00:14:22,950
 You can only find it in the Paddisambhida manga, which is a

224
00:14:22,950 --> 00:14:26,000
 sort of a lesser or...

225
00:14:26,000 --> 00:14:34,130
 I mean, it's one of the more technical part, books of the

226
00:14:34,130 --> 00:14:34,560
 Tipitaka,

227
00:14:34,560 --> 00:14:37,360
 probably not actually the words of the Buddha.

228
00:14:37,360 --> 00:14:42,320
 It's supposed to be the words of Sariputta, but maybe.

229
00:14:45,680 --> 00:14:47,680
 So anyway, it says,

230
00:14:47,680 --> 00:15:01,920
 [

231
00:15:12,960 --> 00:15:15,530
 This appears to have come from an earlier text, but I don't

232
00:15:15,530 --> 00:15:16,400
 know where that is.

233
00:15:16,400 --> 00:15:21,840
 So it gives a list of qualities of an aria.

234
00:15:21,840 --> 00:15:24,770
 I mean, this is the list of what it means to be an

235
00:15:24,770 --> 00:15:25,920
 enlightened being.

236
00:15:25,920 --> 00:15:27,760
 So it's another really good list to think of.

237
00:15:27,760 --> 00:15:30,480
 So it's simpler than the other one.

238
00:15:30,480 --> 00:15:34,400
 These two words often go together.

239
00:15:34,400 --> 00:15:38,000
 They're on that translation of...

240
00:15:40,800 --> 00:15:43,000
 They're not a translation. They relate to the verse that we

241
00:15:43,000 --> 00:15:43,920
 have hanging on the wall.

242
00:15:43,920 --> 00:15:46,880
 Akodno means one who doesn't get angry.

243
00:15:46,880 --> 00:15:50,480
 Sariputta was famous for this.

244
00:15:50,480 --> 00:15:53,840
 There was a monk who accused him of all sorts of things.

245
00:15:53,840 --> 00:15:55,280
 There was another monk.

246
00:15:55,280 --> 00:15:59,760
 There's a good story of a monk who heard that Sariputta

247
00:15:59,760 --> 00:16:01,920
 didn't get angry.

248
00:16:01,920 --> 00:16:05,680
 Everyone's praising Sariputta, and so he thought,

249
00:16:05,680 --> 00:16:07,360
 "I'm going to test this."

250
00:16:07,360 --> 00:16:08,320
 So he took a stick,

251
00:16:08,880 --> 00:16:11,280
 and when Sariputta was walking, he actually came up behind

252
00:16:11,280 --> 00:16:11,360
 him

253
00:16:11,360 --> 00:16:14,720
 and just whacked him across the back with a stick.

254
00:16:14,720 --> 00:16:19,200
 Yeah, it's never happened to me,

255
00:16:19,200 --> 00:16:23,200
 but I've certainly been tested by my share of people.

256
00:16:23,200 --> 00:16:28,790
 I don't have to compare myself to Sariputta, but as a monk,

257
00:16:28,790 --> 00:16:29,200
 you know,

258
00:16:29,200 --> 00:16:36,080
 it sounds crazy to think, but yeah, there are people who

259
00:16:36,080 --> 00:16:36,320
 just...

260
00:16:37,840 --> 00:16:42,030
 The first order of business for them is to test the people

261
00:16:42,030 --> 00:16:45,520
 who they're going to look up to.

262
00:16:45,520 --> 00:16:51,070
 So he slapped him across the back, and Sariputta just turns

263
00:16:51,070 --> 00:16:51,840
 around

264
00:16:51,840 --> 00:16:55,840
 and looks at him and then keeps walking.

265
00:16:55,840 --> 00:17:02,720
 Akodno, he didn't get angry.

266
00:17:04,160 --> 00:17:09,120
 Anupanahi is what relates to that verse on the wall.

267
00:17:09,120 --> 00:17:11,040
 Anupanahi means to get angry back,

268
00:17:11,040 --> 00:17:15,440
 or it means to hold on to the anger.

269
00:17:15,440 --> 00:17:27,680
 Holding on to the anger is the worst evil, right?

270
00:17:27,680 --> 00:17:32,170
 I mean, getting angry is not something we all work with,

271
00:17:32,170 --> 00:17:32,720
 work at,

272
00:17:33,440 --> 00:17:37,120
 work to overcome as meditators.

273
00:17:37,120 --> 00:17:43,840
 In big meditation centers, meditators will often get angry,

274
00:17:43,840 --> 00:17:46,350
 and people who work in the meditation center certainly will

275
00:17:46,350 --> 00:17:47,360
 get angry at each other.

276
00:17:47,360 --> 00:17:52,440
 And that's something that, to some extent, we have to allow

277
00:17:52,440 --> 00:17:52,640
.

278
00:17:52,640 --> 00:17:54,800
 We have to make allowance for each other.

279
00:17:54,800 --> 00:17:57,280
 Yes, this person is angry at me.

280
00:17:59,120 --> 00:18:12,400
 I mean, I think often it's dangerous we fall into this word

281
00:18:12,400 --> 00:18:16,960
 where we don't allow it.

282
00:18:16,960 --> 00:18:22,960
 Lack of English words.

283
00:18:27,600 --> 00:18:34,160
 We aren't able to accept someone else's angry, intolerance,

284
00:18:34,160 --> 00:18:35,200
 that sort.

285
00:18:35,200 --> 00:18:38,080
 We're intolerant of other people's anger, and we think,

286
00:18:38,080 --> 00:18:40,030
 "Hey, these are meditators. What are they doing getting

287
00:18:40,030 --> 00:18:40,400
 angry?"

288
00:18:40,400 --> 00:18:42,320
 I had one monk.

289
00:18:42,320 --> 00:18:47,330
 He was threatening to smash me against a wall, and a really

290
00:18:47,330 --> 00:18:49,600
 big British guy once.

291
00:18:49,600 --> 00:18:54,400
 Another monk chased me through the forest once.

292
00:18:54,400 --> 00:18:57,040
 I had a monk take a broomstick to my head once.

293
00:18:57,040 --> 00:19:00,720
 He didn't actually hit me, but he would have.

294
00:19:00,720 --> 00:19:01,520
 He really would have.

295
00:19:01,520 --> 00:19:02,400
 He was ready to.

296
00:19:02,400 --> 00:19:07,360
 I was testing him because, long story.

297
00:19:07,360 --> 00:19:14,800
 But you have to allow for, to some extent.

298
00:19:14,800 --> 00:19:16,640
 I mean, I think the broomstick was a bit much.

299
00:19:16,640 --> 00:19:22,400
 The problem is when we hold on to it,

300
00:19:23,760 --> 00:19:26,240
 you know, if you understand these things, you understand

301
00:19:26,240 --> 00:19:27,600
 that these conditions come up,

302
00:19:27,600 --> 00:19:30,800
 and you deal with them.

303
00:19:30,800 --> 00:19:35,600
 I remember angry people coming to my teacher and leaving

304
00:19:35,600 --> 00:19:36,400
 without the anger

305
00:19:36,400 --> 00:19:38,640
 because he didn't partake in it, you know?

306
00:19:38,640 --> 00:19:42,640
 I'd sit and watch, and people come up with all sorts,

307
00:19:42,640 --> 00:19:46,590
 and it's like a sponge just taking it in, cleaning it out,

308
00:19:46,590 --> 00:19:49,440
 in with a bad, out with a good.

309
00:19:51,920 --> 00:19:54,880
 You know, that's kind of how we should be, is like filters.

310
00:19:54,880 --> 00:19:58,160
 People bring us all their garbage.

311
00:19:58,160 --> 00:19:58,880
 We take it in.

312
00:19:58,880 --> 00:20:05,840
 And really, it means we don't take it in.

313
00:20:05,840 --> 00:20:08,320
 But it's kind of like a filter.

314
00:20:08,320 --> 00:20:11,770
 You act like a filter because normally not taking in means

315
00:20:11,770 --> 00:20:12,560
 to reject it.

316
00:20:12,560 --> 00:20:13,520
 You know, you're angry at me.

317
00:20:13,520 --> 00:20:14,480
 I reject that.

318
00:20:14,480 --> 00:20:16,240
 No, you're not allowed to be angry at me.

319
00:20:16,240 --> 00:20:16,720
 Go away.

320
00:20:16,720 --> 00:20:18,560
 Get out of my face.

321
00:20:18,560 --> 00:20:19,280
 I won't.

322
00:20:19,280 --> 00:20:20,240
 I can't handle this.

323
00:20:20,240 --> 00:20:20,880
 Go in your room.

324
00:20:20,880 --> 00:20:21,520
 Lock the door.

325
00:20:22,640 --> 00:20:25,360
 That's how we normally reject people's anger.

326
00:20:25,360 --> 00:20:27,520
 But that's this.

327
00:20:27,520 --> 00:20:28,640
 That's holding on to it.

328
00:20:28,640 --> 00:20:29,840
 That's reacting to it.

329
00:20:29,840 --> 00:20:33,200
 It's really, the Buddha said it's the worst evil.

330
00:20:33,200 --> 00:20:37,600
 The sevate na papi o yokudang patikuchiti.

331
00:20:37,600 --> 00:20:40,560
 One who is angry back at someone who gets angry.

332
00:20:40,560 --> 00:20:41,520
 It's the worst evil.

333
00:20:41,520 --> 00:20:45,840
 Because that's what creates the conflict.

334
00:20:49,200 --> 00:20:54,080
 To be a real filter and to have true purity.

335
00:20:54,080 --> 00:20:58,640
 You take it in.

336
00:20:58,640 --> 00:20:59,360
 You accept it.

337
00:20:59,360 --> 00:21:00,400
 Someone's angry at you.

338
00:21:00,400 --> 00:21:05,600
 I mean, the anger doesn't come to you.

339
00:21:05,600 --> 00:21:09,670
 We think of it like the anger is some kind of vibe that

340
00:21:09,670 --> 00:21:10,320
 makes us angry.

341
00:21:10,320 --> 00:21:10,960
 But it's not.

342
00:21:10,960 --> 00:21:15,200
 All that comes to us is seeing, hearing, smelling, tasting,

343
00:21:15,200 --> 00:21:15,840
 feeling, thinking.

344
00:21:15,840 --> 00:21:18,800
 Our anger is totally unrelated to their anger.

345
00:21:19,760 --> 00:21:20,960
 That's what we don't realize.

346
00:21:20,960 --> 00:21:24,240
 Your anger is not because they are angry.

347
00:21:24,240 --> 00:21:27,520
 It's because you get tricked into reacting the way they

348
00:21:27,520 --> 00:21:28,160
 react.

349
00:21:28,160 --> 00:21:32,720
 Where we mimic someone's angry.

350
00:21:32,720 --> 00:21:33,600
 We get angry.

351
00:21:33,600 --> 00:21:35,760
 Someone's stressed.

352
00:21:35,760 --> 00:21:37,200
 We get stressed.

353
00:21:37,200 --> 00:21:41,360
 And we think, oh, these, they rubbed off on me.

354
00:21:41,360 --> 00:21:42,240
 It didn't rub off.

355
00:21:42,240 --> 00:21:44,080
 You're, you're mimicking them.

356
00:21:44,080 --> 00:21:46,480
 You're, you're letting it trigger you.

357
00:21:47,360 --> 00:21:49,840
 So,

358
00:21:49,840 --> 00:21:51,600
 (speaking in foreign language)

359
00:21:51,600 --> 00:21:54,000
 Not to get angry, not to hold on to anger.

360
00:21:54,000 --> 00:21:57,120
 (speaking in foreign language)

361
00:21:57,120 --> 00:22:00,320
 Means to not be crooked.

362
00:22:00,320 --> 00:22:02,400
 What is (speaking in foreign language)

363
00:22:02,400 --> 00:22:03,200
 I know (speaking in foreign language)

364
00:22:03,200 --> 00:22:03,760
 (speaking in foreign language)

365
00:22:03,760 --> 00:22:07,200
 Is to not look down on people.

366
00:22:07,200 --> 00:22:10,640
 To not be condescending or arrogant, conceited.

367
00:22:14,320 --> 00:22:19,360
 The, this one monk accused Sariputta of, of, of hitting him

368
00:22:19,360 --> 00:22:19,440
.

369
00:22:19,440 --> 00:22:22,560
 And what had happened is Sariputta had walked by and

370
00:22:22,560 --> 00:22:23,520
 brushed his robe.

371
00:22:23,520 --> 00:22:25,040
 Right?

372
00:22:25,040 --> 00:22:27,280
 There was part of his robe saying Sariputta brushed him

373
00:22:27,280 --> 00:22:27,600
 like that.

374
00:22:27,600 --> 00:22:30,150
 And so he goes around saying that he really had a bone to

375
00:22:30,150 --> 00:22:30,960
 pick.

376
00:22:30,960 --> 00:22:32,400
 It was something about something else.

377
00:22:32,400 --> 00:22:35,680
 He felt like Sariputta was partial against him.

378
00:22:35,680 --> 00:22:40,080
 And so he, he wanted to create trouble.

379
00:22:40,080 --> 00:22:42,240
 And so he went around saying that Sariputta had hit him.

380
00:22:43,280 --> 00:22:45,520
 And Sariputta came before the Buddha and the Buddha said,

381
00:22:45,520 --> 00:22:47,360
 "Hey, so they're saying that you hit this monk.

382
00:22:47,360 --> 00:22:49,360
 Is it true?"

383
00:22:49,360 --> 00:22:51,340
 Or he didn't even ask whether it's true because he knows it

384
00:22:51,340 --> 00:22:51,840
's not true.

385
00:22:51,840 --> 00:22:53,680
 But he said, "This is what they're saying."

386
00:22:53,680 --> 00:22:58,500
 And Sariputta said, "You know, for someone who valued this

387
00:22:58,500 --> 00:22:58,800
 body,

388
00:22:58,800 --> 00:23:03,600
 someone who valued the physical body, then I might,

389
00:23:03,600 --> 00:23:06,720
 such a person might hit another person."

390
00:23:06,720 --> 00:23:11,880
 He said, "But this body is, is like a corpse to me that I

391
00:23:11,880 --> 00:23:12,960
 have to carry around."

392
00:23:13,920 --> 00:23:17,440
 "I have no attachment to the physical realm whatsoever."

393
00:23:17,440 --> 00:23:21,200
 "I would have no reason to hit anyone."

394
00:23:21,200 --> 00:23:30,880
 This, the teaching is that an enlightened being

395
00:23:30,880 --> 00:23:37,840
 doesn't have any, you know, attachment to themselves.

396
00:23:37,840 --> 00:23:42,080
 No reason to harm others because

397
00:23:43,040 --> 00:23:44,320
 they don't cling to themselves.

398
00:23:44,320 --> 00:23:49,440
 So there's no holding yourself as hired as someone else.

399
00:23:49,440 --> 00:23:53,850
 The idea that you might hit someone else is just ridiculous

400
00:23:53,850 --> 00:23:54,160
.

401
00:23:54,160 --> 00:24:02,880
 And then we have "Wisudho, Sudatangatoh."

402
00:24:02,880 --> 00:24:04,800
 "Wisudho" means someone who is pure.

403
00:24:04,800 --> 00:24:08,000
 So I mean, this is really the key.

404
00:24:08,000 --> 00:24:10,010
 And this is something that we're all, I think, fairly well

405
00:24:10,010 --> 00:24:10,480
 aware of.

406
00:24:10,480 --> 00:24:14,480
 An enlightened being doesn't have anger, greed, delusion.

407
00:24:14,480 --> 00:24:16,240
 Their minds are pure.

408
00:24:16,240 --> 00:24:22,300
 Sudatangatoh means going to that which is pure, which is

409
00:24:22,300 --> 00:24:23,200
 nibbana again.

410
00:24:23,200 --> 00:24:29,910
 They enter into states of purity where they become free

411
00:24:29,910 --> 00:24:31,280
 from suffering.

412
00:24:34,880 --> 00:24:41,550
 "Sampanaditi made dhavi." They have right view and their

413
00:24:41,550 --> 00:24:41,760
 wives.

414
00:24:41,760 --> 00:24:47,920
 "Dangjanya aryo." Such a person you know as an Arya.

415
00:24:47,920 --> 00:24:52,640
 So "Sampanaditi" means right view.

416
00:24:52,640 --> 00:24:55,440
 And there's much, there's many aspects to this.

417
00:24:55,440 --> 00:24:58,300
 I mean, right view to some extent is just knowing that what

418
00:24:58,300 --> 00:24:59,360
 the Buddha taught was right.

419
00:24:59,360 --> 00:25:04,480
 But most importantly, it relates to the Four Noble Truths.

420
00:25:05,520 --> 00:25:09,840
 To know that nothing is worth clinging to, basically.

421
00:25:09,840 --> 00:25:15,090
 What you're learning now may not realize it, but if you

422
00:25:15,090 --> 00:25:19,440
 think about it, and if you reflect upon what

423
00:25:19,440 --> 00:25:22,600
 you are learning, not even maybe what you expected to learn

424
00:25:22,600 --> 00:25:24,960
, but what you're learning is that nothing

425
00:25:24,960 --> 00:25:29,460
 is worth clinging to. You're looking at, you're realizing

426
00:25:29,460 --> 00:25:32,240
 that you're clinging to a lot of stuff

427
00:25:32,240 --> 00:25:36,330
 that really isn't worth clinging to. Our suffering comes

428
00:25:36,330 --> 00:25:38,640
 from clinging to things.

429
00:25:38,640 --> 00:25:45,200
 That's the Four Noble Truths. When you really realize that,

430
00:25:45,200 --> 00:25:49,760
 that's called right view, noble right view.

431
00:25:49,760 --> 00:25:55,360
 That's what leads to nibbana. That's what leads to freedom.

432
00:25:56,880 --> 00:26:01,200
 "May thou be as wisdom, or one who is wise." And so that

433
00:26:01,200 --> 00:26:03,600
 relates a lot to right view.

434
00:26:03,600 --> 00:26:07,600
 But there's more, I think. What right view does for you

435
00:26:07,600 --> 00:26:11,700
 is allows you wisdom of so many, you know, of many

436
00:26:11,700 --> 00:26:13,360
 different kinds.

437
00:26:13,360 --> 00:26:17,440
 You find that amazingly you're able to solve all your

438
00:26:17,440 --> 00:26:18,880
 worldly problems much better.

439
00:26:18,880 --> 00:26:23,740
 And still challenges, but the challenges are reduced by an

440
00:26:23,740 --> 00:26:26,320
 order of magnitude, at least.

441
00:26:28,080 --> 00:26:31,580
 They're not difficult in the same way because you realize

442
00:26:31,580 --> 00:26:34,800
 the true problem is never with your

443
00:26:34,800 --> 00:26:40,620
 experiences or your situation, it's with your reactions.

444
00:26:40,620 --> 00:26:43,840
 You're able to look at a situation

445
00:26:43,840 --> 00:26:48,050
 objectively because you're not hating it or loving it. You

446
00:26:48,050 --> 00:26:50,720
're observing it. And so therefore,

447
00:26:50,720 --> 00:26:54,380
 you can see what most other people overlook. And they tie

448
00:26:54,380 --> 00:26:56,400
 themselves in knots to get what they

449
00:26:56,400 --> 00:27:00,600
 want and to get away from what they don't want. So that

450
00:27:00,600 --> 00:27:07,440
 sort of wisdom, it's the wisdom of an

451
00:27:07,440 --> 00:27:12,320
 enlightened being to be able to solve and to fix and to

452
00:27:12,320 --> 00:27:16,880
 organize and to build great things.

453
00:27:16,880 --> 00:27:24,980
 And that's the Dhamma for tonight. A little bit of insight

454
00:27:24,980 --> 00:27:27,440
 into what it's like to be enlightened.

455
00:27:27,440 --> 00:27:31,600
 I mean, just some idea of some of the qualities of mind

456
00:27:31,600 --> 00:27:33,920
 that an enlightened being possesses.

457
00:27:33,920 --> 00:27:39,010
 Useful for us as this is what we strive for and remind us

458
00:27:39,010 --> 00:27:43,200
 of some of these things and hopefully

459
00:27:43,200 --> 00:27:45,820
 give us something to aim for, that's something that we'd

460
00:27:45,820 --> 00:27:47,920
 like to be. Hopefully some of this seems

461
00:27:47,920 --> 00:27:56,100
 desirable, like something that you would like to strive

462
00:27:56,100 --> 00:28:00,160
 towards. Because just feeling that way,

463
00:28:00,160 --> 00:28:06,660
 just aligning yourself with these qualities, just that, I

464
00:28:06,660 --> 00:28:10,640
 mean, is a great step towards becoming

465
00:28:10,640 --> 00:28:14,480
 that. Because of your intention, your intention and your

466
00:28:14,480 --> 00:28:16,960
 inclination will drive you. It's what

467
00:28:16,960 --> 00:28:21,730
 will lead you. Having that framework puts you in the right

468
00:28:21,730 --> 00:28:25,120
 direction. It focuses your practice.

469
00:28:25,120 --> 00:28:31,190
 So there you go. That's the Dhamma for tonight. Thank you

470
00:28:31,190 --> 00:28:32,400
 all for tuning in.

471
00:28:35,920 --> 00:28:39,460
 You all can go practice. I'm going to answer a few

472
00:28:39,460 --> 00:28:40,320
 questions.

473
00:28:40,320 --> 00:28:45,890
 Let's see how many questions. Okay, we've got four

474
00:28:45,890 --> 00:28:50,560
 questions. Since killing is against the precepts,

475
00:28:50,560 --> 00:28:53,010
 is spraying a house against bugs cockroaches killing

476
00:28:53,010 --> 00:28:56,000
 considered killing? I mean, unless it's

477
00:28:56,000 --> 00:28:59,270
 repellent, those things usually kill the insect. So yes,

478
00:28:59,270 --> 00:29:03,120
 that would be killing. As far as bacteria,

479
00:29:03,120 --> 00:29:05,450
 I don't know. I don't really have an answer for that. I'm

480
00:29:05,450 --> 00:29:07,360
 not sure whether bacteria has a mind or not.

481
00:29:07,360 --> 00:29:12,520
 Someone was saying no, a monk, I asked a monk this once and

482
00:29:12,520 --> 00:29:14,000
 he said no, because

483
00:29:14,000 --> 00:29:17,010
 you don't even, you don't even know that the bacteria are

484
00:29:17,010 --> 00:29:17,600
 there.

485
00:29:17,600 --> 00:29:22,270
 That's kind of a weird answer. I'm not so convinced, but it

486
00:29:22,270 --> 00:29:24,000
 was more complicated than that. But

487
00:29:24,000 --> 00:29:27,840
 it's not something I would worry about personally.

488
00:29:27,840 --> 00:29:36,670
 I mean, I don't sympathize, I don't empathize so much with

489
00:29:36,670 --> 00:29:40,080
 bacteria. I empathize with insects.

490
00:29:40,080 --> 00:29:44,460
 So therefore I feel it important not to kill insects. Don't

491
00:29:44,460 --> 00:29:47,120
 empathize with bacteria.

492
00:29:52,880 --> 00:29:56,670
 Okay, I'm feel less propelled to engage in social

493
00:29:56,670 --> 00:29:58,080
 interactions.

494
00:29:58,080 --> 00:30:03,840
 Okay, I'm also doing Samatha and Ripasa. Well, I mean, this

495
00:30:03,840 --> 00:30:05,040
 is a long question, but

496
00:30:05,040 --> 00:30:08,450
 my only answer to you is do one or the other. I mean, I don

497
00:30:08,450 --> 00:30:10,160
't teach Samatha, I think you know.

498
00:30:10,160 --> 00:30:15,650
 So if you want my advice, you really have to, your best bet

499
00:30:15,650 --> 00:30:18,960
 is to just practice as I teach. Now,

500
00:30:18,960 --> 00:30:21,270
 if you're practicing something else, really, you should

501
00:30:21,270 --> 00:30:23,040
 find a teacher who will help you with that.

502
00:30:23,040 --> 00:30:27,840
 What are Nagas in Buddhism?

503
00:30:27,840 --> 00:30:35,370
 I don't offer anything to the Nagas. Nagas are dragons

504
00:30:35,370 --> 00:30:40,000
 really, or it's a serpent race that

505
00:30:40,000 --> 00:30:43,480
 apparently lives under the earth, under the water, I don't

506
00:30:43,480 --> 00:30:45,680
 know, in their own realm, actually. It's

507
00:30:45,680 --> 00:30:51,650
 like a another dimension or something. How important is

508
00:30:51,650 --> 00:30:54,880
 kindness and or giving to the

509
00:30:54,880 --> 00:30:57,290
 practice? How should one interact with people not directly

510
00:30:57,290 --> 00:30:58,480
 involved with one's life?

511
00:30:58,480 --> 00:31:03,780
 I think it's important. I mean, I think it's a quality of

512
00:31:03,780 --> 00:31:07,680
 an enlightened being. So doing it is

513
00:31:07,680 --> 00:31:10,200
 a way of emulating an enlightened being. Someone who,

514
00:31:10,200 --> 00:31:12,480
 because an enlightened being has no attachment

515
00:31:12,480 --> 00:31:15,220
 to things. So if someone asks you for something, it doesn't

516
00:31:15,220 --> 00:31:16,560
 mean you should always give,

517
00:31:16,560 --> 00:31:19,910
 but it is an interesting practice. It's a useful practice

518
00:31:19,910 --> 00:31:22,480
 because it teaches you to let go.

519
00:31:22,480 --> 00:31:25,760
 When you don't give, when someone asks you, when you don't

520
00:31:25,760 --> 00:31:27,280
 help, when someone needs help,

521
00:31:27,280 --> 00:31:31,600
 you're stingy, you're greedy, you're holding on.

522
00:31:31,600 --> 00:31:35,680
 Maybe you're even arrogant.

523
00:31:40,640 --> 00:31:45,100
 Okay, and that's the questions. So thank you all for coming

524
00:31:45,100 --> 00:31:47,920
 out. Have a good night.

