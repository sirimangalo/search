1
00:00:00,000 --> 00:00:01,000
 Right?

2
00:00:01,000 --> 00:00:06,300
 Anybody know what makes the Buddha special?

3
00:00:06,300 --> 00:00:10,500
 What's special about the Buddha?

4
00:00:10,500 --> 00:00:13,000
 There are three things that are special about the Buddha.

5
00:00:13,000 --> 00:00:16,000
 Three incense represents the Buddha.

6
00:00:16,000 --> 00:00:17,500
 Always remember that.

7
00:00:17,500 --> 00:00:20,500
 It doesn't represent the Buddha, not the Sun.

8
00:00:20,500 --> 00:00:25,590
 It depends on you, as this is the tradition, maybe there

9
00:00:25,590 --> 00:00:26,500
 are other traditions.

10
00:00:26,500 --> 00:00:29,000
 But I think this is the best tradition that I have.

11
00:00:30,000 --> 00:00:31,500
 The Buddha has three special qualities.

12
00:00:31,500 --> 00:00:33,500
 The first one was he was pure.

13
00:00:33,500 --> 00:00:38,500
 The Buddha had no environment.

14
00:00:38,500 --> 00:00:40,500
 Actually the wrong one.

15
00:00:40,500 --> 00:00:42,500
 The Buddha had no environment.

16
00:00:42,500 --> 00:00:44,500
 So what's special about enlightenment?

17
00:00:44,500 --> 00:00:48,160
 The Buddha has that when you become enlightened you have no

18
00:00:48,160 --> 00:00:49,500
 greed, no anger.

19
00:00:49,500 --> 00:00:53,500
 You have no evil thoughts in your mind.

20
00:00:53,500 --> 00:04:37,420
 You don't want to hurt other people, you don't want to, you

21
00:04:37,420 --> 00:00:58,500
 don't want anything.

22
00:00:58,500 --> 00:01:01,000
 You become like the bird.

23
00:01:01,000 --> 00:01:04,000
 This is the first special thing about the Buddha.

24
00:01:04,000 --> 00:01:08,000
 The second thing is the wisdom of the Buddha.

25
00:01:08,000 --> 00:01:13,700
 The Buddha's wisdom is something that you can't find any

26
00:01:13,700 --> 00:01:15,000
 limit to.

27
00:01:15,000 --> 00:01:19,000
 The Buddha had so much wisdom he could understand anything

28
00:01:19,000 --> 00:01:20,000
 that he could explain.

29
00:01:20,000 --> 00:01:24,350
 He could remember all of his past lives, any past life that

30
00:01:24,350 --> 00:01:25,000
 he wanted.

31
00:01:25,500 --> 00:01:28,000
 And he understood everything.

32
00:01:28,000 --> 00:01:32,000
 He knew exactly how to teach because he had special powers.

33
00:01:32,000 --> 00:01:35,000
 He knew what practices lead to the Buddha.

34
00:01:35,000 --> 00:01:38,000
 When he practiced this, this is what had come from it.

35
00:01:38,000 --> 00:01:41,000
 He knew the ways to teach different people.

36
00:01:41,000 --> 00:01:45,000
 He knew what could be and what could not be.

37
00:01:45,000 --> 00:01:48,160
 He could always tell you this is possible, this is not

38
00:01:48,160 --> 00:01:49,000
 possible.

39
00:01:49,000 --> 00:01:54,520
 So I had as many special... this is the wisdom of the

40
00:01:54,520 --> 00:01:55,000
 Buddha.

41
00:01:55,000 --> 00:01:58,500
 And so we have a whole bookshelf full of his teaching.

42
00:01:58,500 --> 00:02:05,500
 It's 45 years worth of the greatest wisdom in life.

43
00:02:05,500 --> 00:02:08,500
 Which is number three.

44
00:02:08,500 --> 00:02:12,500
 Anakaruna.

45
00:02:12,500 --> 00:02:15,500
 Anakaruna, I think.

46
00:02:15,500 --> 00:02:17,500
 Anakaruna, I think.

47
00:02:17,500 --> 00:02:19,500
 Which means...

48
00:02:19,500 --> 00:02:20,500
 Anakaruna.

49
00:02:20,500 --> 00:02:23,500
 The third one... I was just testing.

50
00:02:24,000 --> 00:02:26,500
 I couldn't forget this.

51
00:02:26,500 --> 00:02:31,500
 The third one is the compassion of the Buddha.

52
00:02:31,500 --> 00:02:34,850
 Because for all of us we can practice and we don't have to

53
00:02:34,850 --> 00:02:36,500
 teach other people.

54
00:02:36,500 --> 00:02:40,500
 You can come and practice with me and then you go home.

55
00:02:40,500 --> 00:02:44,500
 I can go home, I can run off into the forest if I want to.

56
00:02:44,500 --> 00:02:48,660
 But the Buddha had his special qualities of me, he's really

57
00:02:48,660 --> 00:02:49,500
 good.

58
00:02:49,500 --> 00:02:52,500
 The Buddha could have done the same thing.

59
00:02:53,000 --> 00:02:55,500
 He became enlightened in Bhagavad-Gita.

60
00:02:55,500 --> 00:02:58,500
 He didn't have to walk 120 miles.

61
00:02:58,500 --> 00:03:02,500
 He could have just stayed there and made a happen.

62
00:03:02,500 --> 00:03:05,930
 But he walked all that way just to find someone who would

63
00:03:05,930 --> 00:03:06,500
 understand what he thought.

64
00:03:06,500 --> 00:03:09,500
 He could see something that was used for him.

65
00:03:09,500 --> 00:03:13,220
 And he went on for 45 years and never thought of doing this

66
00:03:13,220 --> 00:03:13,500
.

67
00:03:13,500 --> 00:03:17,570
 He always just teaching other people that he could serve as

68
00:03:17,570 --> 00:03:18,500
 a Buddha.

69
00:03:19,000 --> 00:03:23,500
 So this is the great passion of the Buddha.

70
00:03:23,500 --> 00:03:26,490
 Not only that, but we talked about how long he had to spend

71
00:03:26,490 --> 00:03:28,500
 just to become a Buddha.

72
00:03:28,500 --> 00:03:34,500
 So this is something very special.

73
00:03:34,500 --> 00:03:36,500
 Do you think this is true?

74
00:03:36,500 --> 00:03:39,500
 The second is the true candles.

75
00:03:39,500 --> 00:03:42,500
 Does anyone know what the true candles represent?

76
00:03:42,500 --> 00:03:45,500
 They already told something.

77
00:03:45,500 --> 00:03:48,500
 I already told you about this.

78
00:03:48,500 --> 00:03:51,000
 What are the two candles?

79
00:03:51,000 --> 00:03:58,000
 The two candles represent the Dhamma and the Vinayana.

80
00:03:58,000 --> 00:04:02,000
 So it represents the Dhamma, the teaching of the Buddha.

81
00:04:02,000 --> 00:04:05,000
 The teaching of the Buddha is separated into two parts.

82
00:04:05,000 --> 00:04:08,000
 One is the things that the Buddha said you should do,

83
00:04:08,000 --> 00:04:10,750
 and one is the things that the Buddha said you shouldn't do

84
00:04:10,750 --> 00:04:11,000
.

85
00:04:11,000 --> 00:04:14,120
 The things that you shouldn't do are to kill, to steal, to

86
00:04:14,120 --> 00:04:18,000
 lie, to cheat, to take drugs and alcohol.

87
00:04:18,000 --> 00:04:23,220
 The things that you should do are to develop morality,

88
00:04:23,220 --> 00:04:25,500
 concentration, and wisdom.

89
00:04:25,500 --> 00:04:29,500
 Follow the path of the three.

90
00:04:29,500 --> 00:04:32,500
 So these are the two parts.

91
00:04:32,500 --> 00:04:36,500
 So what is left then?

