1
00:00:00,000 --> 00:00:04,220
 Hello, welcome back to Ask a Monk. This is a, I think a

2
00:00:04,220 --> 00:00:06,260
 short, hopefully a short video

3
00:00:06,260 --> 00:00:10,360
 talking about the, it's an old question that I've just been

4
00:00:10,360 --> 00:00:13,820
 putting off, about the working

5
00:00:13,820 --> 00:00:19,220
 of karma in regards to a holocaust. How does karma work

6
00:00:19,220 --> 00:00:21,280
 when you're talking about mass

7
00:00:21,280 --> 00:00:27,830
 suffering, mass realizations of a certain state? I think

8
00:00:27,830 --> 00:00:30,680
 actually a holocaust is a bad

9
00:00:30,680 --> 00:00:34,670
 example but it does highlight some part of the answer so I

10
00:00:34,670 --> 00:00:36,960
'll take it as a part of it.

11
00:00:36,960 --> 00:00:43,470
 But let's consider several examples. The holocaust, a

12
00:00:43,470 --> 00:00:48,840
 tsunami and the planes crashing

13
00:00:48,840 --> 00:00:52,310
 into the World Trade Center. Let's examine all three of

14
00:00:52,310 --> 00:00:53,600
 these. The holocaust is actually

15
00:00:53,600 --> 00:00:57,070
 the easiest one to see because the answer in all three

16
00:00:57,070 --> 00:00:58,840
 cases is that no one dies in

17
00:00:58,840 --> 00:01:02,310
 the same way, no one experiences things in the same way or

18
00:01:02,310 --> 00:01:04,200
 has the same sort of suffering.

19
00:01:04,200 --> 00:01:06,950
 This is really easy to see in a holocaust because even

20
00:01:06,950 --> 00:01:08,560
 though so many people were killed

21
00:01:08,560 --> 00:01:11,440
 in similar ways, none of them died in the same way and none

22
00:01:11,440 --> 00:01:12,880
 of them experienced it in

23
00:01:12,880 --> 00:01:15,390
 the same way. Some of them might have been calm, some of

24
00:01:15,390 --> 00:01:17,240
 them would have been very frightened,

25
00:01:17,240 --> 00:01:23,490
 some of them would have been enraged and so on. The death

26
00:01:23,490 --> 00:01:25,960
 of each person is totally dependent

27
00:01:25,960 --> 00:01:29,050
 on their mind state. So to say that all of these people

28
00:01:29,050 --> 00:01:31,120
 died in the same way and therefore

29
00:01:31,120 --> 00:01:35,980
 it's hard to believe that it was all based on their karma

30
00:01:35,980 --> 00:01:39,680
 is really a superficial examination.

31
00:01:39,680 --> 00:01:42,990
 Tsunami is another good example. As you think all of these

32
00:01:42,990 --> 00:01:44,720
 people drown, well that's not

33
00:01:44,720 --> 00:01:50,250
 karma, that's an act of nature, but it's really hard to say

34
00:01:50,250 --> 00:01:53,000
 one way or the other because the

35
00:01:53,000 --> 00:01:57,500
 people died based on their individual situations and though

36
00:01:57,500 --> 00:02:01,040
 it's true that many people drowned,

37
00:02:01,040 --> 00:02:08,090
 each individual's drowning was quite different. So their

38
00:02:08,090 --> 00:02:11,080
 experience of the event would be

39
00:02:11,080 --> 00:02:15,250
 quite different as well. Also their experience and their

40
00:02:15,250 --> 00:02:17,760
 response to it would have been quite

41
00:02:17,760 --> 00:02:21,190
 different as I said. Some people will be quite disturbed by

42
00:02:21,190 --> 00:02:22,960
 it, some people will be quite

43
00:02:22,960 --> 00:02:27,030
 calm or reasonably calm, some people will die instantly,

44
00:02:27,030 --> 00:02:28,920
 some people will take a long

45
00:02:28,920 --> 00:02:32,430
 time to die and so on. It totally depends, I think it

46
00:02:32,430 --> 00:02:34,360
 mainly depends on the person's

47
00:02:34,360 --> 00:02:38,910
 karma. Now you may say there are other, you know in fact

48
00:02:38,910 --> 00:02:41,280
 the word karma, first of all

49
00:02:41,280 --> 00:02:44,350
 as I've said the Buddha didn't talk about karma, he talked

50
00:02:44,350 --> 00:02:46,240
 about the mind and the intentions

51
00:02:46,240 --> 00:02:49,200
 that we have in the mind and then he said only a Buddha

52
00:02:49,200 --> 00:02:51,280
 could possibly understand karma.

53
00:02:51,280 --> 00:02:53,300
 It's one of those things that you should never think about,

54
00:02:53,300 --> 00:02:54,680
 the workings of karma, what caused

55
00:02:54,680 --> 00:02:57,660
 this, what caused that because all it means is that things

56
00:02:57,660 --> 00:02:58,960
 go by cause and effect and

57
00:02:58,960 --> 00:03:02,570
 the mind is causally effective, that's all it means, the

58
00:03:02,570 --> 00:03:04,520
 mind is not an epiphenomenon,

59
00:03:04,520 --> 00:03:07,900
 it's not something that is created by the brain, it's an

60
00:03:07,900 --> 00:03:12,160
 active participant in reality.

61
00:03:12,160 --> 00:03:15,900
 But what causes lead to what results is so intricately

62
00:03:15,900 --> 00:03:18,320
 intertwined that you could never

63
00:03:18,320 --> 00:03:21,130
 really break it apart. So you could say what was the karma

64
00:03:21,130 --> 00:03:22,680
 that caused all those people

65
00:03:22,680 --> 00:03:27,140
 to be brought to the tsunami? Well you're talking about a

66
00:03:27,140 --> 00:03:29,240
 whole confluence I think is

67
00:03:29,240 --> 00:03:34,170
 the word of different causes that bring everyone together

68
00:03:34,170 --> 00:03:36,880
 just in that one instant and it's

69
00:03:36,880 --> 00:03:40,110
 just a fluctuation of the universe, it's scientific, you

70
00:03:40,110 --> 00:03:42,280
 know science would explain the tsunami

71
00:03:42,280 --> 00:03:45,610
 and all the people dying in terms of atoms coming together

72
00:03:45,610 --> 00:03:47,100
 and just happened to come

73
00:03:47,100 --> 00:03:52,840
 together in that way. Karma simply explains the part that

74
00:03:52,840 --> 00:03:54,480
 the mind played and all that

75
00:03:54,480 --> 00:03:58,230
 to bring people together into this one place and then to

76
00:03:58,230 --> 00:03:59,800
 separate. And again as I say it's

77
00:03:59,800 --> 00:04:02,710
 not really that they were all in one place or one

78
00:04:02,710 --> 00:04:05,040
 experience because each person's experience

79
00:04:05,040 --> 00:04:09,500
 would be indeed quite different. The third example is in a

80
00:04:09,500 --> 00:04:11,760
 plane crash which is the hardest

81
00:04:11,760 --> 00:04:15,600
 to see I think, but even there you still have differences

82
00:04:15,600 --> 00:04:18,040
 of experience and more importantly

83
00:04:18,040 --> 00:04:23,000
 differences of reaction to the experience and therefore

84
00:04:23,000 --> 00:04:25,840
 different results and you could

85
00:04:25,840 --> 00:04:35,010
 say different karma that led to it. But there is an element

86
00:04:35,010 --> 00:04:38,120
 of group and mutual karma and

87
00:04:38,120 --> 00:04:41,580
 it has to do with how we are intertwined with each other,

88
00:04:41,580 --> 00:04:43,440
 why we are born to the parents

89
00:04:43,440 --> 00:04:46,140
 that we are, why we live in the same house with certain

90
00:04:46,140 --> 00:04:47,840
 people, why we work with certain

91
00:04:47,840 --> 00:04:51,740
 people are coming together and you know why wouldn't we

92
00:04:51,740 --> 00:04:53,840
 equally ask isn't it strange

93
00:04:53,840 --> 00:04:56,420
 that we should all be working in the same job or isn't it

94
00:04:56,420 --> 00:04:57,720
 strange that we should all

95
00:04:57,720 --> 00:05:01,820
 be born into the same family. It's all the same, it all has

96
00:05:01,820 --> 00:05:03,560
 to do with our path and we

97
00:05:03,560 --> 00:05:07,480
 meet up and we experience certain events, death being one

98
00:05:07,480 --> 00:05:12,240
 of them, sometimes tragic and simultaneous

99
00:05:12,240 --> 00:05:17,270
 death. But it all depends on so many different factors that

100
00:05:17,270 --> 00:05:18,880
 bring you together and of course

101
00:05:18,880 --> 00:05:24,260
 it has to do with the nature of the physical body which isn

102
00:05:24,260 --> 00:05:27,000
't able to bring or prevents

103
00:05:27,000 --> 00:05:30,620
 the mind from bringing about results instantaneously. If it

104
00:05:30,620 --> 00:05:32,880
 were all mental you'd receive karmic

105
00:05:32,880 --> 00:05:37,840
 results much quicker for instance angels and ghosts and so

106
00:05:37,840 --> 00:05:40,640
 on, they receive karmic results

107
00:05:40,640 --> 00:05:45,060
 much quicker when an angel gets angry I think they fall, if

108
00:05:45,060 --> 00:05:47,440
 they get angry enough they die

109
00:05:47,440 --> 00:05:50,130
 simply by being angry. Now it takes a lot of anger for a

110
00:05:50,130 --> 00:05:52,120
 human being to die but for

111
00:05:52,120 --> 00:05:55,080
 an angel it's not that much because it changes their mind

112
00:05:55,080 --> 00:05:57,040
 and because they are mostly mental

113
00:05:57,040 --> 00:06:02,490
 with a very limited physical, simply a little amount of

114
00:06:02,490 --> 00:06:06,080
 anger can cause them to change their

115
00:06:06,080 --> 00:06:09,650
 state because the mind is very quick to change but the body

116
00:06:09,650 --> 00:06:11,440
 is not because the body is so

117
00:06:11,440 --> 00:06:16,670
 coarse and as I said before has such inertia, it can take a

118
00:06:16,670 --> 00:06:19,120
 long time for it to catch up

119
00:06:19,120 --> 00:06:22,380
 with us and as a result it can all build up, build up and

120
00:06:22,380 --> 00:06:24,160
 build up until the body has a

121
00:06:24,160 --> 00:06:26,920
 chance in the case of a plane crash and all this build up

122
00:06:26,920 --> 00:06:28,480
 of karma and so on that brings

123
00:06:28,480 --> 00:06:31,930
 people together and pushes them all into the same position

124
00:06:31,930 --> 00:06:33,640
 and then suddenly snaps. This

125
00:06:33,640 --> 00:06:37,020
 is the way the physical works because of the build up of

126
00:06:37,020 --> 00:06:38,920
 power and the capacity it has

127
00:06:38,920 --> 00:06:44,420
 to collect static energy, that's how the physical works,

128
00:06:44,420 --> 00:06:47,440
 the physical has so much static energy

129
00:06:47,440 --> 00:06:50,610
 in it that this is how nuclear atomic bombs are created

130
00:06:50,610 --> 00:06:52,520
 because there's so much energy

131
00:06:52,520 --> 00:06:57,820
 in matter and this is all just a part of it, how matter

132
00:06:57,820 --> 00:07:01,080
 works in ways that somehow mask

133
00:07:01,080 --> 00:07:05,020
 the workings of the mind simply because of inertia and

134
00:07:05,020 --> 00:07:06,840
 static energy. So that's really

135
00:07:06,840 --> 00:07:10,450
 all that, it shouldn't be a great mystery as to why people

136
00:07:10,450 --> 00:07:12,480
 all die together and it shouldn't

137
00:07:12,480 --> 00:07:17,780
 be certainly a disprove of the workings of karma because

138
00:07:17,780 --> 00:07:20,920
 karma is such a Buddhist concept

139
00:07:20,920 --> 00:07:23,670
 and karma is such an intricate thing, all you can say is

140
00:07:23,670 --> 00:07:25,080
 that when you do good things

141
00:07:25,080 --> 00:07:29,010
 your life changes and your path changes in a good way, when

142
00:07:29,010 --> 00:07:30,960
 you do bad things it changes

143
00:07:30,960 --> 00:07:37,470
 in a bad way. What exactly that will be and the scale and

144
00:07:37,470 --> 00:07:41,920
 the nature is very very very

145
00:07:41,920 --> 00:07:44,950
 difficult to understand because it involves so many

146
00:07:44,950 --> 00:07:47,400
 different things working. Obviously

147
00:07:47,400 --> 00:07:51,640
 it could never be so simple as A leads to B and C leads to

148
00:07:51,640 --> 00:07:53,800
 D because you have so many

149
00:07:53,800 --> 00:07:59,890
 different causes every moment creating new effects and so

150
00:07:59,890 --> 00:08:03,040
 on. So I hope that helps to

151
00:08:03,040 --> 00:08:05,940
 give an explanation of how karma can possibly work in the

152
00:08:05,940 --> 00:08:07,860
 case of a holocaust. It doesn't

153
00:08:07,860 --> 00:08:12,140
 seem to be at all out of line with the idea of cause and

154
00:08:12,140 --> 00:08:14,680
 effect, it obviously is cause

155
00:08:14,680 --> 00:08:18,820
 and effect, there is obviously something that is a cause

156
00:08:18,820 --> 00:08:20,440
 for this. It's something that we

157
00:08:20,440 --> 00:08:23,590
 don't see, we don't think about this whole idea of karma,

158
00:08:23,590 --> 00:08:25,240
 how all these people who died

159
00:08:25,240 --> 00:08:29,850
 in the holocaust could possibly be guilty of something. But

160
00:08:29,850 --> 00:08:31,640
 it really, if you look at

161
00:08:31,640 --> 00:08:35,330
 how many people are guilty of such horrible atrocities even

162
00:08:35,330 --> 00:08:36,840
 in this life and where it

163
00:08:36,840 --> 00:08:42,450
 can possibly lead them in the future and how, where these

164
00:08:42,450 --> 00:08:44,640
 people will be going in the future

165
00:08:44,640 --> 00:08:50,690
 you can have people who are developing on wholesome mind

166
00:08:50,690 --> 00:08:54,120
 states and setting themselves

167
00:08:54,120 --> 00:08:57,310
 up for great suffering in the future. On a massive scale

168
00:08:57,310 --> 00:08:58,760
 this sort of thing occurs, people

169
00:08:58,760 --> 00:09:03,580
 who engage in racism, people who engage in bigotry, who

170
00:09:03,580 --> 00:09:06,160
 have great hatred in their mind.

171
00:09:06,160 --> 00:09:09,040
 I mean even those places where people don't actively hurt

172
00:09:09,040 --> 00:09:10,600
 others but simply build up and

173
00:09:10,600 --> 00:09:13,890
 cultivate prejudice and hate, I think there are a lot of

174
00:09:13,890 --> 00:09:15,760
 people like this in the world,

175
00:09:15,760 --> 00:09:18,970
 there are cultures like this in the world, and I'm not

176
00:09:18,970 --> 00:09:21,080
 going to name names but many cultures

177
00:09:21,080 --> 00:09:24,250
 in many different places, in many different countries of

178
00:09:24,250 --> 00:09:25,860
 the world have a great amount

179
00:09:25,860 --> 00:09:30,180
 of hate and prejudice for things that are foreign or for a

180
00:09:30,180 --> 00:09:32,040
 certain group of people and

181
00:09:32,040 --> 00:09:36,680
 so on, certain type of people. And that's what, it's that

182
00:09:36,680 --> 00:09:37,920
 sort of thing that will lead

183
00:09:37,920 --> 00:09:42,120
 you to become a victim of your own anger and your own

184
00:09:42,120 --> 00:09:45,000
 hatred and of course other people's

185
00:09:45,000 --> 00:09:50,800
 taking advantage of that, taking advantage of your, or

186
00:09:50,800 --> 00:09:53,920
 becoming a part of that stream

187
00:09:53,920 --> 00:10:01,080
 of karmic energy. So that's my take on the Holocaust, so

188
00:10:01,080 --> 00:10:02,800
 thanks for tuning in and all

