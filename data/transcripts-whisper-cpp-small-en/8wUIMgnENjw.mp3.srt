1
00:00:00,000 --> 00:00:05,000
 Hi, welcome to this edition of Ask a Monk.

2
00:00:05,000 --> 00:00:08,140
 The latest question is, "I have a fear of connecting to

3
00:00:08,140 --> 00:00:09,000
 people.

4
00:00:09,000 --> 00:00:12,000
 Harsh criticisms or dirty looks seem to cripple me.

5
00:00:12,000 --> 00:00:15,810
 When a person insults me or if I feel someone is angry with

6
00:00:15,810 --> 00:00:16,000
 me,

7
00:00:16,000 --> 00:00:19,450
 I lose all energy and really do feel deeply cut. Do you

8
00:00:19,450 --> 00:00:21,000
 have any advice?"

9
00:00:21,000 --> 00:00:27,000
 Um, yeah, well, meditation really helps with this.

10
00:00:27,000 --> 00:00:30,000
 I think it's a direct answer to your question,

11
00:00:30,000 --> 00:00:35,000
 is to acknowledge the feelings that come up.

12
00:00:35,000 --> 00:00:38,490
 I mean, the point is that the other person's reality and

13
00:00:38,490 --> 00:00:40,000
 our reality are two different things.

14
00:00:40,000 --> 00:00:42,000
 No one can hurt you.

15
00:00:42,000 --> 00:00:45,790
 There's no one in this earth, in the universe, that can

16
00:00:45,790 --> 00:00:48,000
 cause suffering for you.

17
00:00:48,000 --> 00:00:51,000
 All they can do is impinge upon your six senses.

18
00:00:51,000 --> 00:00:54,820
 They can make you see certain things. They can make you

19
00:00:54,820 --> 00:00:56,000
 hear certain things.

20
00:00:56,000 --> 00:00:58,780
 They can make you smell, taste, feel, or even possibly

21
00:00:58,780 --> 00:01:00,000
 think certain things.

22
00:01:00,000 --> 00:01:03,490
 But they can't make you react to those things in a certain

23
00:01:03,490 --> 00:01:04,000
 way.

24
00:01:04,000 --> 00:01:08,000
 They can't say, "When I show you this, may you get angry.

25
00:01:08,000 --> 00:01:12,660
 When I let you listen to that, may you hear this, may you

26
00:01:12,660 --> 00:01:15,000
 get attached," or so on.

27
00:01:15,000 --> 00:01:18,000
 May this or that arise in your mind. Only you can do that.

28
00:01:18,000 --> 00:01:20,000
 And that's where your choice arises.

29
00:01:20,000 --> 00:01:24,460
 So when someone says something to you, when someone critic

30
00:01:24,460 --> 00:01:25,000
izes you

31
00:01:25,000 --> 00:01:29,660
 or gives you a dirty look, the problem is not the dirty

32
00:01:29,660 --> 00:01:30,000
 look or the criticism.

33
00:01:30,000 --> 00:01:33,000
 The problem is your reaction to it.

34
00:01:33,000 --> 00:01:35,250
 Your mind is generally, or the mind is generally just

35
00:01:35,250 --> 00:01:38,000
 waiting there for something to cling to.

36
00:01:38,000 --> 00:01:41,730
 And so they say the bad things in our minds are like a

37
00:01:41,730 --> 00:01:43,000
 snake in the grass.

38
00:01:43,000 --> 00:01:45,430
 When you look at a field of grass and it looks very

39
00:01:45,430 --> 00:01:48,000
 peaceful and very wonderful,

40
00:01:48,000 --> 00:01:51,540
 it's easy to be deceived and you don't know what's in the

41
00:01:51,540 --> 00:01:52,000
 field.

42
00:01:52,000 --> 00:01:55,000
 When you go walking through it, feeling happy and peaceful.

43
00:01:55,000 --> 00:01:58,210
 And then when the snake, when you get near the snake, it

44
00:01:58,210 --> 00:02:00,000
 jumps up and bites you.

45
00:02:00,000 --> 00:02:03,000
 But until that time, it was quite a peaceful scene.

46
00:02:03,000 --> 00:02:07,770
 And the mind is like that. The mind is just waiting for its

47
00:02:07,770 --> 00:02:08,000
 prey.

48
00:02:08,000 --> 00:02:12,510
 So we'll just be sitting there and as soon as someone looks

49
00:02:12,510 --> 00:02:14,000
 at us, we jump on it.

50
00:02:14,000 --> 00:02:16,000
 As soon as someone says something to us, we jump on it.

51
00:02:16,000 --> 00:02:19,000
 And we can change that through the meditation.

52
00:02:19,000 --> 00:02:22,000
 When you see something, say seeing, seeing.

53
00:02:22,000 --> 00:02:24,000
 When you hear someone, when someone's saying nasty things,

54
00:02:24,000 --> 00:02:26,000
 do you say hearing, hearing?

55
00:02:26,000 --> 00:02:29,000
 That's quite difficult. That takes a lot of training.

56
00:02:29,000 --> 00:02:31,000
 But we are training to get to that level.

57
00:02:31,000 --> 00:02:35,710
 And it does work if it's continuous and you can eventually

58
00:02:35,710 --> 00:02:37,000
 remind yourself.

59
00:02:37,000 --> 00:02:40,240
 Even in daily life when it's very difficult to do so, if

60
00:02:40,240 --> 00:02:43,000
 you're careful and if you are,

61
00:02:43,000 --> 00:02:45,000
 especially if you're prepared for it.

62
00:02:45,000 --> 00:02:48,010
 A lot of people have told me that they knew there was a

63
00:02:48,010 --> 00:02:50,000
 confrontation coming up

64
00:02:50,000 --> 00:02:53,000
 and so they reminded themselves and they readied themselves

65
00:02:53,000 --> 00:02:53,000
.

66
00:02:53,000 --> 00:02:55,000
 They said, "If they yell at me, I'm going to say hearing,

67
00:02:55,000 --> 00:02:56,000
 hearing.

68
00:02:56,000 --> 00:02:59,000
 If they looked at me, seeing, seeing and so on."

69
00:02:59,000 --> 00:03:03,000
 Just to be mindful. And if you prepare yourself in advance,

70
00:03:03,000 --> 00:03:05,730
 then when the situation comes up, you find that you're

71
00:03:05,730 --> 00:03:09,000
 really able to deal with it in a much more profitable way.

72
00:03:09,000 --> 00:03:11,620
 The easier thing to do is once these emotions have already

73
00:03:11,620 --> 00:03:12,000
 arisen,

74
00:03:12,000 --> 00:03:15,000
 by then it's too late, then you're already suffering.

75
00:03:15,000 --> 00:03:17,390
 But you can minimize the impact and you can stop it from

76
00:03:17,390 --> 00:03:18,000
 snowballing

77
00:03:18,000 --> 00:03:21,470
 because anger leads to thinking about it again and thinking

78
00:03:21,470 --> 00:03:24,000
 about it again leads to get angry again and so on.

79
00:03:24,000 --> 00:03:27,000
 And it's a cycle. And you could break that at any time when

80
00:03:27,000 --> 00:03:27,000
 you say to yourself,

81
00:03:27,000 --> 00:03:31,610
 for instance, thinking, thinking or even angry, angry when

82
00:03:31,610 --> 00:03:33,000
 you're upset at someone.

83
00:03:33,000 --> 00:03:40,000
 Just to pick that as your meditation object and focus on it

84
00:03:40,000 --> 00:03:41,000
 and see it clearly as it is.

85
00:03:41,000 --> 00:03:45,270
 And don't make the link, "Oh, I'm angry. I have to do this

86
00:03:45,270 --> 00:03:47,000
 and this and this to them."

87
00:03:47,000 --> 00:03:50,700
 And then thinking about what they did to me, getting angry

88
00:03:50,700 --> 00:03:53,000
 again and so on, going in this cycle.

89
00:03:53,000 --> 00:03:56,140
 Cut it off wherever you can. When you feel angry, say angry

90
00:03:56,140 --> 00:03:57,000
, angry.

91
00:03:57,000 --> 00:03:59,990
 When you feel sad, sad, sad. When you feel depressed,

92
00:03:59,990 --> 00:04:03,000
 depressed, depressed. Whatever.

93
00:04:03,000 --> 00:04:05,700
 One thing you mentioned about losing all energy. I wouldn't

94
00:04:05,700 --> 00:04:07,000
 worry about that.

95
00:04:07,000 --> 00:04:12,240
 That's common associating with people who are critical and

96
00:04:12,240 --> 00:04:14,000
 mean and nasty and so on.

97
00:04:14,000 --> 00:04:16,320
 I think even if you're enlightened, it can be very tiresome

98
00:04:16,320 --> 00:04:17,000
 to be around.

99
00:04:17,000 --> 00:04:20,470
 They say even the Buddha was very tired when he was around

100
00:04:20,470 --> 00:04:21,000
 such people

101
00:04:21,000 --> 00:04:25,000
 and he would often dismiss them and say, "Go and meditate."

102
00:04:25,000 --> 00:04:28,420
 So one thing that you have to acknowledge is that if you're

103
00:04:28,420 --> 00:04:31,000
 going to live your life in a peaceful and happy way,

104
00:04:31,000 --> 00:04:33,520
 you have to surround yourself with peaceful and happy

105
00:04:33,520 --> 00:04:35,000
 people, with nice people.

106
00:04:35,000 --> 00:04:38,000
 If you can't do that, just try to stay to yourself.

107
00:04:38,000 --> 00:04:42,000
 The greatest treasure that we have is our solitude.

108
00:04:42,000 --> 00:04:45,000
 If we can manage to stay to ourselves as much as possible,

109
00:04:45,000 --> 00:04:48,120
 we'll find that we're a lot more peaceful and a lot happier

110
00:04:48,120 --> 00:04:50,000
 and a lot less lonely.

111
00:04:50,000 --> 00:04:52,640
 We find that when we learn to live alone, when we learn to

112
00:04:52,640 --> 00:04:53,000
 stay alone,

113
00:04:53,000 --> 00:04:56,980
 we can then be around anybody and we don't feel so much

114
00:04:56,980 --> 00:04:59,000
 stress and suffering.

115
00:04:59,000 --> 00:05:03,490
 We learn to understand ourselves and come to grips with who

116
00:05:03,490 --> 00:05:05,000
 we are.

117
00:05:05,000 --> 00:05:08,480
 We live like an island unto ourselves. We don't rely on

118
00:05:08,480 --> 00:05:10,000
 anyone else.

119
00:05:10,000 --> 00:05:13,500
 The part of the problem is that our happiness generally

120
00:05:13,500 --> 00:05:16,000
 relies on other people, depends on other people.

121
00:05:16,000 --> 00:05:19,810
 We want other people to accept us and to like us and that's

122
00:05:19,810 --> 00:05:22,000
 the only way we can feel happy.

123
00:05:22,000 --> 00:05:24,410
 Until we learn to overcome that and just be happy in

124
00:05:24,410 --> 00:05:25,000
 ourselves

125
00:05:25,000 --> 00:05:29,030
 and need other people's praise and appreciation and

126
00:05:29,030 --> 00:05:30,000
 acceptance,

127
00:05:30,000 --> 00:05:33,000
 then we'll really be able to stand on our own two feet

128
00:05:33,000 --> 00:05:37,000
 and be an island unto ourselves as the Buddha recommended.

129
00:05:37,000 --> 00:05:39,000
 Okay, so I hope that helps and if there's more questions,

130
00:05:39,000 --> 00:05:40,000
 please keep them coming.

