 Good evening everyone.
 Welcome to our daily live broadcast.
 Today's quote is about contentment.
 It's a more famous quote for the Buddha regarding content
ment.
 Don't just be told, not many of the Buddha's quotes are all
 that famous,
 and even the most famous quotes are not that famous.
 The most famous Buddha quotes, of course, are the quotes
 that the Buddha never actually said
 that have been propagated around the internet falsely.
 It's an interesting phenomenon when the Buddha is, I think,
 the most misquoted person on the internet, maybe besides
 Albert Einstein.
 But probably up there, rifling Albert Einstein.
 There are very few quotes on the internet, relatively, that
 are actual quotes.
 It's not very quotable.
 It's not very quotable because his teaching is somewhat
 deep,
 and it challenges, doesn't really ring true, not in a
 conventional sense, it challenges.
 There are quotable quotes like, "No dhamma is worth
 clinging to."
 "Nothing is worth clinging to."
 It doesn't have the same ring to it as the fake Buddha
 quotes.
 This is a good one. This one is one that you could quote.
 The most quotable of the Buddha that I can think of is,
 "Don't let the moment pass you by."
 It's actually words of the Buddha, but it's such a generic
 saying that it probably wasn't the first,
 and he certainly wasn't the last to say it.
 But here, this quote is basically like the wings of a bird.
 Like the wings of a bird.
 Like a bird, just as a bird carries its wings as its only
 burden.
 So too, one is satisfied with a robe to sustain the body
 and food to satisfy, to sustain the stomach.
 See, it's still not all that quotable.
 This is how one is content.
 This is a motto to live by for a Buddhist monk.
 See, because we can't get what we want.
 We can't live our lives in poverty, in renunciation.
 If we begin to covet, which does happen, covet possession,
 the belongings, things,
 we're inevitably disappointed unless we have a large
 following and are a great teacher.
 By the time a monk becomes a great teacher, not great, but
 like a well-known teacher,
 then they get lots of support and are able to get whatever
 they want within reason.
 During training and living when traveling, one has to be
 content.
 No matter what, one has to be content, you know.
 Even a great teacher who has great relative wealth and fame
 and support.
 To follow the Buddhist teaching, one need be content.
 To what extent should one be content?
 One should be content to the extent that one only clings to
 food and robes.
 There are only two things one actually needs.
 So clings to means seeks out as a requirement.
 It doesn't go without.
 Everything else one is able to go without.
 But food, one seeks out food and robes, one seeks out
 clothes, one seeks out clothing.
 Besides these two wings, one seeks out nothing.
 This is how one is content.
 I mean, for all of us monks are known.
 This is a good measure, measuring post.
 It allows us to measure up and see what sort of things are
 we clinging to.
 The smartphone is not included in there.
 A nice house is not even included.
 Notice he doesn't even include the other requisites,
 dwelling and medicines, or the other two requisites,
 but he doesn't include those because you don't actually
 need them.
 If you want to be truly content, it doesn't matter where
 you sleep.
 Sleep on the floor, sleep in the forest, sleep under a park
 bench.
 I tell the story of how I once slept under a park bench.
 Actually, I've done it more than once.
 When I was in Sri Lanka, I got into Colombo.
 I guess I was going to give a talk.
 But I got in the night before, and the monastery that I was
 going to was already closed.
 So rather than bother them, rather than wake them up, I
 just walked to the nearby park.
 I actually kind of snuck in.
 I think I snuck into the park because it was closed as well
.
 I went and slept on the park bench.
 I think you haven't really lived until you've gone to,
 until you've completely let go and lived in this kind of
 poverty.
 In a sense, those who are homeless are enviable in terms of
 their renunciation.
 They've gotten to the bottom.
 Oscar Wilde, I think, he was Oscar Wilde.
 No, not Oscar Wilde.
 George Orwell.
 George Orwell wrote this book about his time in Paris and
 London when he had no money.
 He said, "When you get to the bottom, there's a curious
 feeling that you can't fall any further.
 Suddenly, it's this feeling that we get as renunciants.
 You can't do anything more to me.
 I've fallen as far as I can fall."
 I remember small things like I realized one day that I just
 couldn't get laundry soap.
 In the early days, I was using money.
 Monks in Thailand often used money, and I didn't have any.
 The thing about using money is there was never enough.
 I just realized I didn't have any money to buy laundry soap
.
 I really panicked because I wouldn't be able to get soap to
 wash my clothes.
 No, it was soap to wash my body as well.
 What am I going to do?
 Then it just hit me.
 I'm like, "Well, I'll wash without soap."
 It was just liberating to realize that you can just go
 without.
 Of course, the funny thing is once I started keeping the
 rules and not using money,
 it was a lot more well-supported.
 There were people appreciating.
 There was an appreciation for what I was doing.
 There was an appreciation for supporting my practice.
 Monks get support.
 We're supported and looked after.
 It's not like we're extravagant or hard to look after.
 It's not like providing food and clothing, basic requisites
.
 It's not like it's a great burden.
 It's out of appreciation.
 It's an appreciation for someone who is cultivating
 spiritual truth
 and cultivating peace and goodness, making themselves a
 better person.
 If you want to put it like that,
 when you see someone trying to become a better person,
 wouldn't you want to help them?
 Being a monk is all about that and about becoming a better
 person.
 That's the kind of thing I support.
 That's why we support how many people who come here to do
 meditation courses?
 Free food, free shelter, free teaching.
 Then you talk about the teaching that the Buddhist monk can
 provide
 because of the time that they have to practice and study
 and teach.
 But we should all be content.
 This is the greatest contentment.
 To be free like a bird with only two wings, two things to
 cling to,
 to hold on to, everything else, let it go and fly away.
 So we have some questions.
 I believe we're all caught up.
 We have from today,
 "Should you note even the smallest of feelings in
 meditation,
 or only note it if it's a big distraction?"
 This type of meditation, let's get this clear,
 there's no such thing as a distraction.
 That which you experience is the proper object of
 meditation,
 that which is present.
 So if a feeling is present, it's not a distraction from
 something.
 It's an experience of something.
 So it's that something that you want to be clear about.
 So if a feeling arises small or big, that feeling is now
 there.
 So that's what should be your object of meditation.
 So you should note it.
 "How to practice gratitude without being fake gratitude?
 A truly true gratitude coming from the bottom of the heart
?"
 Well, it's with anything.
 Sincerity is a product of purity.
 You can't just...
 I mean, it's kind of a funny question because you're asking
,
 "How do you manufacture sincerity?"
 You can't. You have to be sincere.
 You can't manufacture anything.
 "What can I do that will make me be sincere?"
 You can't. It doesn't work that way.
 The other thing is to learn to be sincere, to stop clinging
.
 What is it that's making the gratitude fake? Delusion?
 Crookedness? Pretense?
 So if you give up pretense, then you only have sincerity.
 It's like love, you know. You don't have to cultivate it.
 If you cultivate it, it's sort of just an artificial.
 To get true and pure and real love or gratitude or any of
 these things.
 Just be sincere.
 Learn to let go. Don't cling.
 There will be a natural sense of appreciation and gratitude
.
 The sensitivity, if you will, you become sensitive to
 important things,
 important events, important actions.
 Whatever happened to that second book you were working on,
 do you think you might finish it?
 There's only one more chapter to write.
 I've been thinking of writing the last chapter.
 It's not the most important.
 It is the most important topic, but in that sense,
 it's not really the kind of chapter that you have to write
 about.
 Or it's a little bit sensitive to write about.
 Because you can't talk about nirvana, you can't describe it
.
 Because I'm trying to make these books fairly open to non-B
uddhists,
 and I don't just want to talk about nirvana.
 I have to explain how it comes out of the practice
 and still not go into too much detail.
 Because it's so much to be experienced on your limits.
 If anything is esoteric in Theravada Buddhism, nirvana is,
 I think, fairly esoteric.
 It's the kind of thing you really have to experience to
 appreciate.
 So, it's not like the other chapters in that sense.
 But I plan on finishing it, so it's just a matter of
 getting around to it.
 The rest of it is all you really need.
 A very beautiful quote.
 What scripture is it from? This is from the Dighanikaya,
 the Samanyapala, Palasuddha.
 The fruits of recluseship, the fruits of renunciation.
 This was taught to Ajatasattu, the son of King Bimbisara.
 Ajatasattu killed his father at the behest of Devadatta.
 Devadatta tried to kill the Buddha. Ajatasattu tried to
 kill his father,
 and actually succeeded in killing his father,
 which was just about the worst thing up there, among the
 worst things a person can do.
 And so as a result, even through teaching the suttas, one
 of the greatest suttas,
 Ajatasattu wasn't able to become enlightened because he was
 destined for hell.
 But it's an important sutta. Ajatasattu wanted to go to get
 a teaching
 from one of the wise men around, but he actually wanted to
 go and hear the Buddha talk.
 But he knew that if he were to say, "Let's go and talk to
 the Buddha,"
 he wouldn't really...it would be kind of hypocritical
 because he killed...
 he plotted with Devadatta to kill the Buddha,
 and he killed one of the Buddha's greatest disciples, his
 father.
 So he wanted somebody else to suggest that they go and see
 the Buddha.
 He felt awkward, felt embarrassed like he wasn't worthy.
 That's what karma does to you. If you do very bad things,
 it tears you away from good people,
 makes you feel ashamed and unable to associate with good
 people.
 So he was ashamed for good reason.
 And so he asked all of his advisors who he should go see,
 and they all suggested various teachers that were
 contemporaries of the Buddha.
 Probably these are the teachers that you find in the Up
anishads.
 And then he turns to Jivaka. Jivaka is this doctor who
 looked after the Buddha
 and was a Buddhist, suttapan, I think.
 He said, "What about you? Why are you quiet?"
 He said, "Well, you should go see the Buddha."
 And so he decides to go see the Buddha.
 And he gets there because...
 He's asked all these other teachers this question,
 and they couldn't really give him an answer.
 So he comes and asks the Buddha, "What is the fruit of re
clusorship, of pronunciation?
 What good is it to go off in the forest and live with just
 a robe and bowl?"
 Because he says, "I have all these workers working for me,
 and I see the benefit of what they do. They do their work,
 they get paid.
 Whatever they do, they have the satisfaction and the
 gratification and the reward
 that comes with their occupation.
 What do monks get? What do hermits and recluses get?
 What do people get from the religion?
 The whole sutta is about all the benefits. It's one of the
 longest suttas.
 So the "diga" means long.
 So it's one of the few long suttas that the Buddha has,
 very long.
 There are many, many teachings in it.
 This is one of them.
 It's contentment.
 This is later on in the sutta when he starts to talk about
 the real benefits,
 how one attains nimbana.
 This is one of the things that supports one,
 to find the greatest benefit, which is freedom, freedom
 from suffering.
 Worth reading, Samanjapala Sutta.
 I believe there's a translation on the internet, probably
 by Tanisoro.
 I don't recommend his translations as the best,
 unfortunately.
 He's a great monk. He's done a great service for Buddhism
 with his Vinaya teachings.
 But his translations are not... I don't know.
 I think you have to be one of his followers in his
 tradition to really appreciate him.
 He's not a very good translator, I don't think.
 Sorry.
 21-day meditation course question.
 He was a traditional Zen cushion for sitting meditation,
 for all the sitting bones for proper posture.
 We have small cushions for you to sit on, put your butt
 under your bottom.
 You can raise your pelvis, but I'm not concerned about
 proper posture.
 There's no real proper posture, not for vipassana practice.
 For some time, sure, it's useful.
 I don't know. I mean, in Thailand, we sit on these flat
 cushions. Everybody does.
 It's very rare to find someone who tries to sit in any sort
 of proper posture.
 Even if they do, they still sit flat. They don't put
 anything under their bottom.
 That's a fairly Western concept, or maybe Hindu, I don't
 know, yoga.
 But in Buddhism, Theravada Buddhism anyway, traditional
 Buddhism, not just Theravada, traditional Buddhism,
 there were none of these cushions.
 There was one teacher, he was always complaining, he was
 complaining to me, he said,
 "Oh, I went to America, these Westerners are so soft.
 I like this monk, like Turand. Likes Turand, I'm sure he's
 still alive."
 He was the teacher at Wat Mahathat in Bangkok, and he said,
 "I've been all over America, and they're so soft. They sit
 on these big cushions."
 That's funny, because yeah, we sit on the floor.
 Just a little cloth.
 Put your cloth down on the carpet or even the wooden floor.
 That's why you sit, a good reason to sit in full lotus is
 so that the balls of your feet,
 the balls of your ankles don't touch the floor.
 If you've ever sat flat on the wooden floor, your ankles
 touch the floor,
 and that's quite uncomfortable.
 But if you sit full lotus, they don't.
 The link to my book is at the top.
 Maybe the second version isn't there, is it?
 Let me see, teachings.
 It's there. It's on our site.
 There's a link to it in the menus.
 It's somewhat buried, I admit.
 But you're welcome to bring your own cushion if you want.
 You can put out cushions if you need one.
 Tina made some nice little cushions about this big that you
 can put under.
 "Can we say that in Maita meditation we are sincere about
 wanting to subdue anger
 instead of letting it cause harm?
 Otherwise being sincere would mean letting anger take
 control of your words and actions."
 I mean sincerity is just natural rather than forced or
 artificial.
 Sincerity means coming from the heart.
 So in anything you do.
 Yes, in Maita, if you are sincere,
 there will be a sincere aversion to anger.
 Not aversion, but you know what I mean?
 Here we hold towards anger and its results.
 But it would be sincere, it would be artificial or fake.
 Because it comes from insight.
 The reading in the Buddha's words by Bhikkhu Bodhi,
 and I still find it fascinating.
 I'm sure it's a very fascinating compilation.
 One of the verses said that after enlightenment one uses
 its insight to direct it
 to the destruction of other taint.
 No.
 No.
 And after enlightenment, an Arahant does not have greed,
 anger or delusion.
 So you'll have to give me that quote and I'll explain it to
 you
 how you're misreading it or it's being mistranslated.
 I'm pretty sure he didn't mistranslate it.
 He's a pretty good translator.
 So probably there's a misinterpretation going on there.
 So such thing as after enlightenment.
 It's probably not after enlightenment,
 it's maybe after the attainment of sotapan.
 Sotapan still has it's balance.
 What is the first sutta I should read to begin a formal
 study of Buddhism?
 Mostly studied Buddhism through secondary sources in the
 past.
 But there's no one sutta,
 but there are compilations that you can read.
 Read the Majima Nikaya.
 I think that's pretty standard for people to suggest.
 The middle length discourses, bhikubodhi's translations,
 you can get them, you have to buy them.
 Or you have to get one of these pirated versions,
 which are actually available.
 There's an app for Android called Teravada Buddhist Texts.
 And if you read through the description it talks about such
 texts.
 If you can find an archive of such texts,
 you could read them on Android.
 But of course that would be probably in violation of
 copyright law,
 although bhikubodhi was fine with it.
 So read that as you will.
 But yeah, reading the Majima Nikaya is good.
 Majima Nikaya, Digha Nikaya, these two, if you read these,
 you should get a pretty good feeling for what the Buddha
 taught.
 It's not a perfect one, you still need lots more study
 and even practice for sure,
 but also guidance from the commentary, I would argue,
 to sort of help you get a coherent sense of all the
 different teachings.
 [no speech detected]
 I'm scheduled for the 21 day meditation course.
 Will I be sleeping on a pad on the floor?
 We have mats that are about an inch or two thick.
 And you can put a couple of them together if you need.
 We probably need to get more come to think of it.
 That's what we should do right away,
 because we don't have enough I don't think for four medit
ators, I don't think.
 You may have to look into that and get more in the next
 couple of days.
 We have bedding, we have sheets, pillows, blankets.
 Bring your own towel, we probably have them here,
 but that's something you should bring on your own washcloth
,
 on your own toiletries.
 You can wear socks during the day, bare feet.
 Bare feet I like that, there's no rule,
 but I appreciate bare feet more.
 That's just me.
 I won't look down on you if you wear socks.
 If you run out of something, we can help you.
 Yeah, no we can't.
 If you run out of something, don't run out of something.
 Bring everything you need or else just live without it.
 Yeah, we don't have anybody.
 You're going to be looking after yourselves in the
 monastery.
 We probably can help you.
 In May we've got a Thai woman who can,
 a local supporter who brings food to the monastery.
 She can probably help get anything you need.
 But, remember scouts, boy scouts, be prepared.
 Be prepared to do without in some cases.
 (
 )
 (
 )
 Okay, look, if you're going to ask questions,
 you have to put the little question mark before them.
 For the new people, it might not be clear.
 Yeah, it's not easily understood.
 We don't even explain that.
 But it's harder for us to find which are questions
 and which are comments unless you put the question right
 before it.
 There's a little button there that you use to add the
 question button.
 Chanting has different benefits.
 What are your thoughts on the benefits of chanting?
 Yeah.
 Yeah, but it doesn't have anything to do with concentrating
 on mindfulness.
 I don't know how chanting would relate to mindfulness.
 Certainly concentration and intention and reiteration of
 basic teachings.
 I don't have too many thoughts on it.
 I'm dissociated since starting meditation
 and having a rough time getting past this.
 I'm personalised and de-realised.
 Boy, for someone who's de-personalised, you use the word 'I
' a lot.
 How can I get past this disconnect between myself and
 reality?
 Obsessing over yourself.
 Stop worrying about yourself. That will help.
 If you're worried, say 'worried'. If you're upset, say 'ups
et'.
 Spoiler, there is no self.
 There's no self there. There's only experiences.
 Those experiences arise when it's safe. None of them are
 self.
 You don't have to let go of them. You'll feel better.
 What languages would be best to learn for a lot of studies
 and travel?
 English would be a good one.
 You've got a head start in there.
 Would it not be beneficial to add a request to use the
 question button?
 Yes, it certainly would.
 We're changing the website. We've just finally got the new
 website online.
 Should I tell you all the address? No, I don't have it here
.
 We've got a test version of it up.
 It's live. It's just a matter of tweaking it, fine-tuning
 it, making sure it works.
 I'm sure it works better than this website already.
 Probably fairly soon we'll just switch over to it.
 It may not be as full-featured yet. There may be a few
 things about it that I am not.
 I just looked at it briefly today for the first time.
 We'll be moving over to that soon.
 Once it gets there, we can add all of them. We can tweak it
 as necessary.
 I don't want to tweak this one anymore. We won't be using
 it soon.
 But you're right. I can't scold you for not using the
 question mark when you haven't been told.
 How often do you do these live shows? Sorry, I'm new here.
 Right now I'm doing them every day, but that isn't always
 the case.
 They're scheduled to happen every day, but I don't do them
 every day.
 I do try to keep everyone on our toes, not being too compl
acent.
 Sometimes we don't broadcast, but otherwise mostly every
 day.
 All right. Sounds like we're kind of winding down now.
 We'll end it there. Thank you all for tuning in.
 I'll pick it up again tomorrow night.
 We're moving this weekend, so may not broadcast every day
 this weekend.
 But we'll see. I'll try to. It should be okay.
 And then there's going to be a switch over.
 The internet at the new place isn't up until Wednesday,
 but that just probably means I'll stay here until Wednesday
,
 and then Wednesday morning I'll do the final move over.
 Robin's here. Robin got here this morning.
 She's not here right now. She went to Toronto,
 but she's already started packing stuff up,
 and we got more people coming to help.
 If you're in the area and want to come help us move, great.
 Come on out.
 We start moving. We start actually moving Saturday.
 That's when we finally get to go to the new place.
 Anyway, good night, everyone.
 [
