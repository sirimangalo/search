1
00:00:00,000 --> 00:00:05,930
 There's a question whether in this life or a future life to

2
00:00:05,930 --> 00:00:10,000
 be free from suffering one must rid oneself of desire.

3
00:00:10,000 --> 00:00:14,000
 Is one required to be a monk to be enlightened?

4
00:00:14,000 --> 00:00:26,000
 No, not explicitly required.

5
00:00:29,000 --> 00:00:34,220
 Somebody even said that even though one is decked out in

6
00:00:34,220 --> 00:00:40,000
 jewels and wearing a royal attire,

7
00:00:40,000 --> 00:00:46,260
 if they become free from desire and free from defilement,

8
00:00:46,260 --> 00:00:48,000
 they should be considered a monk.

9
00:00:48,000 --> 00:00:52,270
 I believe if I'm not wrong it was Santati, this minister of

10
00:00:52,270 --> 00:00:53,000
 the king,

11
00:00:53,000 --> 00:01:01,870
 who the Buddha told this verse about because Santati was a

12
00:01:01,870 --> 00:01:06,000
 minister to some king, Pasenadi, maybe,

13
00:01:06,000 --> 00:01:15,230
 and he was very clever and was able to defeat an enemy or

14
00:01:15,230 --> 00:01:19,000
 quell an uprising in a border village.

15
00:01:19,000 --> 00:01:24,940
 The king, as a reward to him, allowed him to live as a king

16
00:01:24,940 --> 00:01:26,000
 for seven days,

17
00:01:26,000 --> 00:01:29,900
 not to be the king but to live the luxury and to have as

18
00:01:29,900 --> 00:01:32,000
 much luxury as the king had for seven days.

19
00:01:32,000 --> 00:01:39,000
 For seven days he lived in complete luxury and sensuality.

20
00:01:39,000 --> 00:01:44,390
 He was even drinking alcohol and sporting himself with

21
00:01:44,390 --> 00:01:47,000
 women and prostitutes.

22
00:01:47,000 --> 00:01:52,000
 He had this one dancing girl who was in his employment,

23
00:01:52,000 --> 00:01:56,000
 and he was so in love with her or in lust with her,

24
00:01:56,000 --> 00:01:59,810
 that he would just love to sit there and drink, get really

25
00:01:59,810 --> 00:02:02,000
 drunk and watch her dance.

26
00:02:02,000 --> 00:02:07,160
 I guess there was another thing at the time that, similar

27
00:02:07,160 --> 00:02:10,000
 to now, as I understand nowadays,

28
00:02:10,000 --> 00:02:13,110
 is women had to be really thin in order to be attractive or

29
00:02:13,110 --> 00:02:14,000
 something.

30
00:02:14,000 --> 00:02:16,600
 I don't know if that's still the case, but I did hear this

31
00:02:16,600 --> 00:02:18,000
 whole thing about anorexia,

32
00:02:18,000 --> 00:02:21,000
 women think they have to be thinner and thinner.

33
00:02:21,000 --> 00:02:22,620
 If you come to our monastery, you don't really have a

34
00:02:22,620 --> 00:02:25,000
 choice, you just get thinner and thinner.

35
00:02:25,000 --> 00:02:33,060
 So she was abstaining from food because she wanted to be "

36
00:02:33,060 --> 00:02:35,000
beautiful."

37
00:02:35,000 --> 00:02:39,550
 She got so bad that when she got up on stage to dance on

38
00:02:39,550 --> 00:02:42,000
 the seventh day,

39
00:02:42,000 --> 00:02:47,000
 she had some nerve broke or something broke inside of her.

40
00:02:47,000 --> 00:02:48,810
 There's this story, the same thing happened to a monk

41
00:02:48,810 --> 00:02:51,000
 apparently in the Buddhist time.

42
00:02:51,000 --> 00:02:55,450
 She died, just suddenly, boom, died right there on the

43
00:02:55,450 --> 00:02:56,000
 stage.

44
00:02:56,000 --> 00:02:59,000
 He's looking at her and he's drunk.

45
00:02:59,000 --> 00:03:05,000
 He got so upset about this that he went to find the Buddha,

46
00:03:05,000 --> 00:03:10,000
 all decked out in his jewels and his robes and so on.

47
00:03:10,000 --> 00:03:14,220
 He was so sobered up by the affair that in the time it took

48
00:03:14,220 --> 00:03:16,000
 him to walk to the Buddha,

49
00:03:16,000 --> 00:03:19,770
 he no longer was drunk or something like that because he

50
00:03:19,770 --> 00:03:21,000
 got to the Buddha

51
00:03:21,000 --> 00:03:24,370
 and the Buddha taught him and he became an arahant, if I

52
00:03:24,370 --> 00:03:26,000
 remember correctly.

53
00:03:26,000 --> 00:03:30,930
 The story is so... I'm not so good with my stories, but

54
00:03:30,930 --> 00:03:35,000
 either he became an arahant...

55
00:03:35,000 --> 00:03:37,010
 I think he became an arahant and then he passed away or

56
00:03:37,010 --> 00:03:38,000
 something like that.

57
00:03:38,000 --> 00:03:42,000
 There was one man, if it wasn't Santati, who passed away.

58
00:03:42,000 --> 00:03:45,300
 And then they were all asking him, "Well, what is he? Is he

59
00:03:45,300 --> 00:03:49,000
 a Brahmin or is he a shaman or is he a monk?"

60
00:03:49,000 --> 00:03:54,580
 What is he? And the Buddha said, "Even whether they are in

61
00:03:54,580 --> 00:03:59,000
 these royal clothes and bedecked in jewels,

62
00:03:59,000 --> 00:04:03,060
 if they practice correctly, they're considered a Brahmin,

63
00:04:03,060 --> 00:04:05,000
 they're considered a Samhain, something like that."

64
00:04:05,000 --> 00:04:11,430
 But for sure the theory is sound, that one can become

65
00:04:11,430 --> 00:04:14,000
 enlightened without becoming a monk.

66
00:04:14,000 --> 00:04:19,640
 Now, the orthodox theory is that... or doctrine, is that if

67
00:04:19,640 --> 00:04:23,000
 they don't ordain, they will pass away.

68
00:04:23,000 --> 00:04:27,260
 That day or within seven days, I think within seven days is

69
00:04:27,260 --> 00:04:29,000
 the orthodox doctrine.

70
00:04:29,000 --> 00:04:33,860
 And it seems to be the case that either they became monks

71
00:04:33,860 --> 00:04:36,000
 or else they passed away.

72
00:04:36,000 --> 00:04:39,260
 And the question, of course, is whether that means a full

73
00:04:39,260 --> 00:04:43,000
 ordination or just means living the home life.

74
00:04:43,000 --> 00:04:46,000
 Anything to add? No.

75
00:04:46,000 --> 00:04:49,000
 Nothing to add to that.

