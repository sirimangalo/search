 Okay, one rather simple question about eating and
 meditation. For example, I am doing something
 which will take me half an hour or hour to finish, and
 after that I am thinking to do
 some meditation. But during this half hour I become hungry.
 Should I eat and when I finish
 doing what I'm doing meditate or not eat and keep working,
 then do the meditation on hungry
 stomach or maybe just have a snack. It's not exactly a
 simple question. There's a lot in
 there that's a bit, I'm not quite clear on the whole of the
 question. But one thing I
 would say which is kind of smarmy is that your work should
 be meditation and your eating
 should be meditation. So that's really where your answer,
 that's the most important part
 of the answer. It's not really smarmy at all. It's probably
 not the answer that most people
 would expect. The meditation starts now. It doesn't start
 30 minutes from now when you're
 going to meditate. Because then you're wasting 30 minutes
 thinking it's okay. In 30 minutes
 I'll meditate or in an hour I'll meditate or after this I
'll meditate. And during all that
 30 minutes or an hour you're wasting good meditation time.
 So when you work you should
 try to be mindful. When you eat you should try to be
 mindful. You should work on that.
 Why it's not such a smarmy answer after all is that that
 will really help your formal
 meditation. If a person just does formal meditation and
 doesn't think at all about meditation
 during their daily life, the formal meditation becomes
 quite difficult and it's a constant
 uphill struggle because it's out of tune with your ordinary
 reality. So you should try to
 constantly be bringing your mind back to attention. I mean
 even right now here we are sitting
 together we should try to make it a mindful experience.
 Being aware at least of our emotions,
 likes and dislikes, the distractions in the mind, the
 feelings that we have. If you're
 just listening here, because I'm talking I have to do more
 action, but for some of you
 just listening you can just revert back to your meditation
 unless you have questions.
 But as far as me making decisions as to when you should do
 eating and when you should do
 meditating, meditating on an empty stomach is interesting,
 can be useful to help you
 to learn about hunger. But I'd say generally you need the
 food for the energy for the meditation,
 so better to have a little bit of food before you meditate.
 I don't know, meditate anytime.
 Whenever you meditate, meditate, get some time for it and
 do it. Just don't procrastinate.
 It kind of sounds like you might be talking about procrast
inating, which is dangerous.
 So it goes, "Work for a half an hour and then I'll have a
 snack." And then when I'm having
 a snack I think about, "Oh then I have to go check Facebook
 or email and check email."
 And then something comes up in email and I have to call
 this person or that person or
 do this or I have to go to the store. And then you never
 meditate after all. That kind of
 thing is dangerous. So better to be clear with yourself
 that if you really need food
 and you're really hungry, then eat or eat a little bit. But
 if you don't, sometimes
 it's just procrastinating. Because meditation can be
 difficult and the mind will actually
 develop aversion towards it if you're not careful. And if
 you're not aware of the aversion,
 if you're not looking at it and contemplating it as well,
 it can become a great hindrance.
 And more than a hindrance, it can actually trick you into
 every time you think of meditation
 your mind says, "Oh, he's thinking of meditation again. Let
's divert his attention. Hey, look
 at that. That's nice. Hey, aren't you hungry? Hey, want to
 go for burgers or something?"
 Right? The mind will do this to you. Because it doesn't
 want to meditate. It's like, "Oh
 no, don't put me back there. That's torture." So be careful
 of that one.
