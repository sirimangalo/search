1
00:00:00,000 --> 00:00:05,280
 Hello and the next question comes from CC path blesses

2
00:00:05,280 --> 00:00:09,790
 Would you maybe talk a bit about having children in

3
00:00:09,790 --> 00:00:11,280
 relation to Buddhism?

4
00:00:11,280 --> 00:00:14,140
 I know it will bring feelings, attachment and so on. What

5
00:00:14,140 --> 00:00:15,280
 can you tell about this?

6
00:00:15,280 --> 00:00:18,400
 Um, yeah.

7
00:00:18,400 --> 00:00:22,700
 The first thing obviously to say is from a monk's point of

8
00:00:22,700 --> 00:00:23,040
 view

9
00:00:23,040 --> 00:00:26,560
 um, you know, don't have children

10
00:00:27,200 --> 00:00:29,840
 if you can at all help it.

11
00:00:29,840 --> 00:00:33,600
 And of course the argument right away is

12
00:00:33,600 --> 00:00:37,200
 well, if nobody had children there would be no more humans

13
00:00:37,200 --> 00:00:39,120
 and then we get into this whole debate about whether

14
00:00:39,120 --> 00:00:42,610
 it's useful somehow for this world to be populated with

15
00:00:42,610 --> 00:00:44,800
 humans whether the human existence is

16
00:00:44,800 --> 00:00:49,120
 is somehow beneficial. But it's really a silly argument in

17
00:00:49,120 --> 00:00:50,480
 terms of

18
00:00:50,480 --> 00:00:53,200
 today's world because

19
00:00:53,200 --> 00:00:56,180
 you know, there's enough people already if if we were for

20
00:00:56,180 --> 00:00:56,480
 some

21
00:00:57,120 --> 00:01:00,320
 some reason lacking of lacking of people

22
00:01:00,320 --> 00:01:02,960
 humans in this world and then maybe you could say yeah, we

23
00:01:02,960 --> 00:01:04,960
 have to bring some more into the world.

24
00:01:04,960 --> 00:01:07,950
 But considering there's so much work that has to be done

25
00:01:07,950 --> 00:01:09,840
 just to support each other just to

26
00:01:09,840 --> 00:01:12,560
 um, you know,

27
00:01:12,560 --> 00:01:14,560
 develop

28
00:01:14,560 --> 00:01:16,560
 humanity as it is.

29
00:01:16,560 --> 00:01:19,120
 Um, they don't really have that

30
00:01:19,120 --> 00:01:21,920
 um, that argument.

31
00:01:21,920 --> 00:01:25,250
 I guess the argument is more personal that you want someone

32
00:01:25,250 --> 00:01:27,760
 to take care of or or so on. Um,

33
00:01:27,760 --> 00:01:30,720
 you know, somehow you you

34
00:01:30,720 --> 00:01:36,040
 You you feel the need to have to have children for some

35
00:01:36,040 --> 00:01:38,560
 reason or other. Um,

36
00:01:38,560 --> 00:01:42,530
 once you do have children how to take care of them because

37
00:01:42,530 --> 00:01:45,040
 you can't send them back obviously.

38
00:01:45,040 --> 00:01:46,960
 Um,

39
00:01:46,960 --> 00:01:49,970
 and so it becomes imperative for you to take care of them.

40
00:01:49,970 --> 00:01:51,680
 It's it's the duty of parents to

41
00:01:51,680 --> 00:01:54,960
 teach their children to stay away from evil

42
00:01:54,960 --> 00:01:57,600
 um to tell them what is good.

43
00:01:57,600 --> 00:02:01,000
 One of the most important lessons that I've found in

44
00:02:01,000 --> 00:02:03,360
 regards to to parents and children

45
00:02:03,360 --> 00:02:06,680
 is that as parents you should never think that your

46
00:02:06,680 --> 00:02:08,960
 children aren't listening to you. You should never

47
00:02:08,960 --> 00:02:11,840
 become frustrated when you say something again and again

48
00:02:11,840 --> 00:02:15,280
 and again and your children still don't seem to respond.

49
00:02:16,000 --> 00:02:19,340
 Um, you have to understand that they're not you and they're

50
00:02:19,340 --> 00:02:19,840
 not yours.

51
00:02:19,840 --> 00:02:22,000
 Uh, the the

52
00:02:22,000 --> 00:02:24,990
 children are people in and of themselves. They're human

53
00:02:24,990 --> 00:02:26,640
 beings and they're on their own path.

54
00:02:26,640 --> 00:02:29,990
 They came on their of their own volition and they'll go of

55
00:02:29,990 --> 00:02:34,160
 their own volition. We we aren't in control of their lives.

56
00:02:34,160 --> 00:02:36,880
 Um, that being said

57
00:02:36,880 --> 00:02:39,040
 from what i've seen

58
00:02:39,040 --> 00:02:42,840
 most children do keep a very good record of what their

59
00:02:42,840 --> 00:02:45,520
 parents have told them but because of their

60
00:02:46,080 --> 00:02:48,080
 defilements because of their their own

61
00:02:48,080 --> 00:02:51,760
 mental problems, you know the things that are

62
00:02:51,760 --> 00:02:54,400
 going on in their minds,

63
00:02:54,400 --> 00:02:57,600
 they're not able to process and they're not able to respond

64
00:02:57,600 --> 00:02:58,000
 and to

65
00:02:58,000 --> 00:03:01,360
 you know obviously be subservient all the time.

66
00:03:01,360 --> 00:03:04,370
 Um, but they do keep in mind and once they're able once

67
00:03:04,370 --> 00:03:06,560
 they've given up and they've they've

68
00:03:06,560 --> 00:03:09,990
 gained some sense of responsibility and they're able to

69
00:03:09,990 --> 00:03:12,800
 tell the difference between right and wrong

70
00:03:13,680 --> 00:03:15,890
 the the teachings that parents have given their children

71
00:03:15,890 --> 00:03:16,900
 will become invaluable.

72
00:03:16,900 --> 00:03:18,480
 So

73
00:03:18,480 --> 00:03:20,920
 the buddha said parents are the first teachers and and I

74
00:03:20,920 --> 00:03:22,800
 think that is the best role one can

75
00:03:22,800 --> 00:03:26,960
 um one can have in regard in relation with one's children.

76
00:03:26,960 --> 00:03:28,480
 One shouldn't be attached to them

77
00:03:28,480 --> 00:03:30,870
 obviously from a buddha's point of view one should

78
00:03:30,870 --> 00:03:33,650
 understand they are human beings. They're going to have

79
00:03:33,650 --> 00:03:34,240
 their own lives.

80
00:03:34,240 --> 00:03:36,400
 um and

81
00:03:36,400 --> 00:03:37,920
 my duty

82
00:03:37,920 --> 00:03:40,560
 which if I fail to do I fail to do my duty

83
00:03:40,960 --> 00:03:44,000
 is to teach them and to give them information

84
00:03:44,000 --> 00:03:47,730
 with which they can do whatever they like. Um, but give

85
00:03:47,730 --> 00:03:50,990
 them information that's useful. That's true. That's a

86
00:03:50,990 --> 00:03:51,840
 wholesome

87
00:03:51,840 --> 00:03:55,600
 and that that teaches them the difference between what is

88
00:03:55,600 --> 00:03:56,640
 right and what is wrong

89
00:03:56,640 --> 00:03:59,280
 and um

90
00:03:59,280 --> 00:04:03,380
 helps them to to you know to make decisions and to have the

91
00:04:03,380 --> 00:04:04,560
 information

92
00:04:04,560 --> 00:04:07,520
 available to them.

93
00:04:07,520 --> 00:04:10,880
 Uh, other than that

94
00:04:10,880 --> 00:04:12,880
 I

95
00:04:12,880 --> 00:04:16,250
 I would say um, you know try to let them them live their

96
00:04:16,250 --> 00:04:17,840
 lives try not to

97
00:04:17,840 --> 00:04:22,640
 um think of them as our possessions or or as as as ours

98
00:04:22,640 --> 00:04:25,120
 um and

99
00:04:25,120 --> 00:04:29,000
 try to use it as a topic as an object of meditation. Our

100
00:04:29,000 --> 00:04:30,640
 children can be um,

101
00:04:30,640 --> 00:04:33,200
 you know very frustrating at times

102
00:04:33,200 --> 00:04:35,920
 can be um

103
00:04:35,920 --> 00:04:37,120
 very

104
00:04:37,120 --> 00:04:40,550
 addictive at times where we have feelings of great

105
00:04:40,550 --> 00:04:43,360
 attachment towards them and

106
00:04:43,360 --> 00:04:48,880
 from from a human from a secular point of view this is

107
00:04:48,880 --> 00:04:50,640
 genetic and and

108
00:04:50,640 --> 00:04:53,440
 it's something natural and it's something that we should

109
00:04:53,440 --> 00:04:56,640
 cultivate but from a buddha's point of view it's a it's a

110
00:04:56,640 --> 00:05:00,430
 constructed phenomenon. It's something that we've developed

111
00:05:00,430 --> 00:05:02,880
 from lifetime after lifetime and something that we've

112
00:05:02,880 --> 00:05:05,520
 ingrained into ourselves artificially

113
00:05:06,160 --> 00:05:07,120
 and

114
00:05:07,120 --> 00:05:10,100
 so the the addiction there's nothing special about it. It's

115
00:05:10,100 --> 00:05:12,480
 something that is not helpful to us not helpful to

116
00:05:12,480 --> 00:05:16,900
 the children. It's not something that um, you know allows

117
00:05:16,900 --> 00:05:19,840
 them spiritual development and freedom

118
00:05:19,840 --> 00:05:22,080
 so

119
00:05:22,080 --> 00:05:25,420
 um, that's an object of a meditation for us something that

120
00:05:25,420 --> 00:05:27,040
 we should consider very seriously

121
00:05:27,040 --> 00:05:31,060
 and try to see objectively and not say no i'm a mother i'm

122
00:05:31,060 --> 00:05:32,480
 a father and therefore

123
00:05:33,120 --> 00:05:36,220
 um, it's right for me to feel this way. Right is just a

124
00:05:36,220 --> 00:05:38,560
 judgment call. You say it's right

125
00:05:38,560 --> 00:05:42,680
 um, therefore it's right and it's really meaningless. The

126
00:05:42,680 --> 00:05:45,440
 point is whether it's useful or not and it's really not

127
00:05:45,440 --> 00:05:49,040
 um, you know if you love someone set them free

128
00:05:49,040 --> 00:05:51,360
 it's uh

129
00:05:51,360 --> 00:05:53,680
 that's that's the best thing to do best thing you can do

130
00:05:53,680 --> 00:05:56,720
 learning how to meditate on these feelings can be very very

131
00:05:56,720 --> 00:05:57,840
 very important

132
00:05:57,840 --> 00:06:00,880
 for parents. Okay, so I hope that helps.

133
00:06:01,040 --> 00:06:04,190
 Um, you know, I guess I don't have a lot to say about about

134
00:06:04,190 --> 00:06:05,040
 being a parent

135
00:06:05,040 --> 00:06:09,990
 um, not being with myself, but um, you know from from the

136
00:06:09,990 --> 00:06:12,320
 buddhist teaching this is what I have to offer. So

137
00:06:12,320 --> 00:06:14,960
 there you go

