1
00:00:00,000 --> 00:00:03,380
 How do we know that meditation is working for us? I know it

2
00:00:03,380 --> 00:00:04,760
's not a magic pill

3
00:00:04,760 --> 00:00:07,650
 But is there a point when we should see a difference in

4
00:00:07,650 --> 00:00:08,640
 ourselves?

5
00:00:08,640 --> 00:00:14,080
 It's a bit difficult

6
00:00:14,080 --> 00:00:18,640
 it's difficult for two reasons one meditation works very

7
00:00:18,640 --> 00:00:19,640
 slowly and

8
00:00:19,640 --> 00:00:22,760
 two

9
00:00:22,760 --> 00:00:27,480
 It's very difficult to see a change in yourself, right

10
00:00:28,920 --> 00:00:31,280
 It's very difficult to see what's wrong with you

11
00:00:31,280 --> 00:00:33,640
 To see your problems

12
00:00:33,640 --> 00:00:35,890
 It's very easy to see the problems that other people have

13
00:00:35,890 --> 00:00:38,280
 and their problems tend to be very easy to solve, right?

14
00:00:38,280 --> 00:00:40,680
 And to solve someone else's problems is very easy

15
00:00:40,680 --> 00:00:43,690
 Solving your own problems is quite difficult because you

16
00:00:43,690 --> 00:00:45,960
 don't really see them correctly you think you do

17
00:00:45,960 --> 00:00:49,370
 You think you see I have this problem. I have that problem,

18
00:00:49,370 --> 00:00:50,240
 but it's quite superficial

19
00:00:50,240 --> 00:00:54,100
 You're not able to see that actually thinking like that is

20
00:00:54,100 --> 00:00:55,760
 is a part of the problem

21
00:00:56,720 --> 00:00:58,720
 What

22
00:00:58,720 --> 00:01:02,200
 You should see is is a change in understanding

23
00:01:02,200 --> 00:01:05,760
 It's the first most important thing to gain

24
00:01:05,760 --> 00:01:08,400
 It's really the final thing to gain as well

25
00:01:08,400 --> 00:01:11,130
 The the key is that your understanding of things is

26
00:01:11,130 --> 00:01:14,120
 changing not that you feel more happy or that you feel

27
00:01:14,120 --> 00:01:18,430
 more at peace or so on but that you have more understanding

28
00:01:18,430 --> 00:01:18,720
 and

29
00:01:18,720 --> 00:01:22,560
 as a result

30
00:01:22,920 --> 00:01:25,510
 You have an you and you have a knowledge that as a result

31
00:01:25,510 --> 00:01:27,440
 you're more at peace because

32
00:01:27,440 --> 00:01:31,440
 It's very hard to convince yourself that you feel more

33
00:01:31,440 --> 00:01:33,840
 peaceful because it's impermanent

34
00:01:33,840 --> 00:01:37,240
 Sometimes you feel pain and a lot of pain even as a medit

35
00:01:37,240 --> 00:01:38,440
ator. Sometimes you feel

36
00:01:38,440 --> 00:01:41,640
 Quite stressed even as a meditator

37
00:01:41,640 --> 00:01:45,640
 But

38
00:01:45,640 --> 00:01:49,390
 When you when you think about the understanding that you've

39
00:01:49,390 --> 00:01:49,920
 gained

40
00:01:50,640 --> 00:01:55,400
 Then you you can convince yourself that actually it's true

41
00:01:55,400 --> 00:01:56,360
 that overall

42
00:01:56,360 --> 00:02:01,150
 You you must be and you definitely are more peaceful even

43
00:02:01,150 --> 00:02:04,050
 though it's hard to tell for yourself because you you can

44
00:02:04,050 --> 00:02:04,600
 think

45
00:02:04,600 --> 00:02:07,850
 Gee, you know yesterday. I was more peaceful today. I'm not

46
00:02:07,850 --> 00:02:08,720
 peaceful or last week

47
00:02:08,720 --> 00:02:11,730
 I was one or a month ago. I was not peaceful now. I am

48
00:02:11,730 --> 00:02:15,240
 peaceful, but you don't really have this kind of comparison

49
00:02:15,240 --> 00:02:18,480
 It's not a real comparison. So you can't really

50
00:02:19,320 --> 00:02:21,880
 Confirm that you're more peaceful than you were before you

51
00:02:21,880 --> 00:02:23,480
're more happy than you were before

52
00:02:23,480 --> 00:02:26,500
 but you can convince yourself that it must be the case

53
00:02:26,500 --> 00:02:28,200
 because you know the nature of

54
00:02:28,200 --> 00:02:31,820
 Wisdom the nature of the understanding the understandings

55
00:02:31,820 --> 00:02:34,040
 that you've gained have untied knots

56
00:02:34,040 --> 00:02:37,440
 They've they've changed the way you look at things. They've

57
00:02:37,440 --> 00:02:40,510
 straightened and they've simplified your understanding of

58
00:02:40,510 --> 00:02:42,240
 reality and because of that simplification

59
00:02:42,240 --> 00:02:45,600
 There's less stress. There's less complication less busy

60
00:02:45,600 --> 00:02:45,920
ness

61
00:02:46,960 --> 00:02:49,960
 So look for that look for the understanding

62
00:02:49,960 --> 00:02:53,180
 This simplification of how you look at things rather than

63
00:02:53,180 --> 00:02:54,920
 making a big deal out of things

64
00:02:54,920 --> 00:02:58,680
 just the ability to just see things as they are naturally

65
00:02:58,680 --> 00:03:00,360
 and the the

66
00:03:00,360 --> 00:03:08,040
 The ability to let go and to not stress over things as much

67
00:03:08,040 --> 00:03:10,040
 you

