1
00:00:00,000 --> 00:00:06,200
 Good evening everyone.

2
00:00:06,200 --> 00:00:15,720
 We're broadcasting live April 18th.

3
00:00:15,720 --> 00:00:35,480
 Tonight's quote is about pots.

4
00:00:35,480 --> 00:00:37,960
 Pots are useful things.

5
00:00:37,960 --> 00:00:49,800
 Lots of things you can fill with whatever you like,

6
00:00:49,800 --> 00:00:56,120
 provided they're not broken.

7
00:00:56,120 --> 00:01:04,450
 And so this quote compares people who quarrel and who don't

8
00:01:04,450 --> 00:01:08,040
 reconcile themselves with each

9
00:01:08,040 --> 00:01:12,610
 other, who don't reconcile with each other, with their

10
00:01:12,610 --> 00:01:14,840
 friends and associates, like broken

11
00:01:14,840 --> 00:01:18,160
 pots.

12
00:01:18,160 --> 00:01:25,360
 Because the relationship is broken.

13
00:01:25,360 --> 00:01:31,120
 If a society is quarreling, then the society is broken.

14
00:01:31,120 --> 00:01:36,960
 If the family is quarreling, the family is broken.

15
00:01:36,960 --> 00:01:42,060
 If the country is quarreling, if the world is quarreling,

16
00:01:42,060 --> 00:01:44,560
 then the world is broken.

17
00:01:44,560 --> 00:01:50,000
 Relationships are broken.

18
00:01:50,000 --> 00:01:59,000
 So all that's interesting, and it's a useful quote from the

19
00:01:59,000 --> 00:02:00,680
 Jataka.

20
00:02:00,680 --> 00:02:04,700
 But I think it gets more interesting when you apply it to a

21
00:02:04,700 --> 00:02:06,760
 single individual, which

22
00:02:06,760 --> 00:02:10,080
 is what I learned years ago in peace studies.

23
00:02:10,080 --> 00:02:15,760
 It's that all the models that they use to talk about

24
00:02:15,760 --> 00:02:25,480
 interpersonal and inter-organizational,

25
00:02:25,480 --> 00:02:29,920
 talk about peace and conflict between multiple individuals,

26
00:02:29,920 --> 00:02:31,920
 all those can be applied to the

27
00:02:31,920 --> 00:02:33,240
 single individual.

28
00:02:33,240 --> 00:02:44,100
 Of course, we know this from Buddhism, but we don't

29
00:02:44,100 --> 00:02:50,640
 actually talk so much about the

30
00:02:50,640 --> 00:02:54,400
 connection.

31
00:02:54,400 --> 00:03:00,550
 Conflict that we have between people is the same as the

32
00:03:00,550 --> 00:03:01,240
 conflict.

33
00:03:01,240 --> 00:03:04,680
 Many of the same things apply to a single individual.

34
00:03:04,680 --> 00:03:10,460
 Of course, that's more interesting to us as meditators, I

35
00:03:10,460 --> 00:03:11,480
 think.

36
00:03:11,480 --> 00:03:17,450
 Because our own mind, our own person is made up of all

37
00:03:17,450 --> 00:03:24,800
 sorts of conflicting mind states.

38
00:03:24,800 --> 00:03:32,360
 And in fact, single issues, single habits create conflict.

39
00:03:32,360 --> 00:03:36,760
 When we want something, it's like breaking a pot.

40
00:03:36,760 --> 00:03:40,840
 You get two parts.

41
00:03:40,840 --> 00:03:45,110
 You get the desire for the thing and the aversion for not

42
00:03:45,110 --> 00:03:46,720
 having the thing.

43
00:03:46,720 --> 00:03:47,720
 You get them both.

44
00:03:47,720 --> 00:03:50,640
 It's not just one or the other.

45
00:03:50,640 --> 00:03:56,760
 When an attraction, they go together.

46
00:03:56,760 --> 00:04:02,670
 When you want something or you like something, there's an

47
00:04:02,670 --> 00:04:06,040
 associated disliking of states

48
00:04:06,040 --> 00:04:13,600
 that are devoid of that thing.

49
00:04:13,600 --> 00:04:20,960
 You get to keep both parts and it's broken.

50
00:04:20,960 --> 00:04:27,420
 You get the desire and you get the aversion and your mind

51
00:04:27,420 --> 00:04:30,320
 is broken as a result.

52
00:04:30,320 --> 00:04:33,240
 Those mind states are useless.

53
00:04:33,240 --> 00:04:35,240
 Mind states are dangerous.

54
00:04:35,240 --> 00:04:40,140
 Like a broken pot, you can cut yourself on the sharp, on

55
00:04:40,140 --> 00:04:42,000
 the jagged edges.

56
00:04:42,000 --> 00:04:50,040
 And anything you could do with the mind and your work or

57
00:04:50,040 --> 00:04:58,280
 your accomplishments, you could

58
00:04:58,280 --> 00:05:03,640
 achieve with the mind, with the whole mind, the wholesome

59
00:05:03,640 --> 00:05:06,200
 mind, with the mind that is

60
00:05:06,200 --> 00:05:16,880
 collected, that is coherent.

61
00:05:16,880 --> 00:05:27,440
 None of that is possible with a broken mind.

62
00:05:27,440 --> 00:05:32,720
 The desire makes you obsess over the thing that you want.

63
00:05:32,720 --> 00:05:37,690
 The aversion inflames the mind, makes it impossible to

64
00:05:37,690 --> 00:05:41,080
 accomplish anything but attaining what

65
00:05:41,080 --> 00:05:48,640
 you want and see ears above and angry, debilitates.

66
00:05:48,640 --> 00:05:56,880
 It's impossible to succeed.

67
00:05:56,880 --> 00:06:04,190
 I think that's more interesting than interpersonal because

68
00:06:04,190 --> 00:06:08,800
 one thing, teaching here in New York,

69
00:06:08,800 --> 00:06:15,900
 I've noticed is a lot of people are, a lot of Buddhists are

70
00:06:15,900 --> 00:06:21,520
 focused much on convention,

71
00:06:21,520 --> 00:06:23,600
 conventional teaching.

72
00:06:23,600 --> 00:06:30,510
 I think actually more clearly we have a hard time getting

73
00:06:30,510 --> 00:06:35,600
 past the conventional, conceptual.

74
00:06:35,600 --> 00:06:40,650
 And so when we deal with our problems, we deal with

75
00:06:40,650 --> 00:06:42,560
 conventional.

76
00:06:42,560 --> 00:06:48,100
 We think in terms of people, we think in terms of

77
00:06:48,100 --> 00:06:49,960
 situations.

78
00:06:49,960 --> 00:06:53,990
 We don't think so much in terms of mind states because

79
00:06:53,990 --> 00:06:56,240
 until you get to, when you get to

80
00:06:56,240 --> 00:07:04,090
 get to Banganyana and start to see the ephemeral nature of

81
00:07:04,090 --> 00:07:09,080
 reality, the experiential nature

82
00:07:09,080 --> 00:07:15,500
 of reality, and the concepts begin to fade and you no

83
00:07:15,500 --> 00:07:19,240
 longer see reality in terms of

84
00:07:19,240 --> 00:07:20,680
 people, places and things.

85
00:07:20,680 --> 00:07:26,320
 Until you reach that and break through the illusion.

86
00:07:26,320 --> 00:07:27,720
 Everything is content.

87
00:07:27,720 --> 00:07:30,920
 A problem is made up of people.

88
00:07:30,920 --> 00:07:34,450
 So if you talk about a relationship, you're thinking in

89
00:07:34,450 --> 00:07:36,360
 terms of people, how do I deal

90
00:07:36,360 --> 00:07:37,360
 with this person?

91
00:07:37,360 --> 00:07:42,000
 What do I, and everyone wants to know how to use the Dhamma

92
00:07:42,000 --> 00:07:44,000
 in their daily life.

93
00:07:44,000 --> 00:07:47,750
 And so maybe this is also relating to all these questions

94
00:07:47,750 --> 00:07:49,920
 people have about their life.

95
00:07:49,920 --> 00:07:50,920
 What can I do?

96
00:07:50,920 --> 00:07:53,960
 Do you have any advice for me?

97
00:07:53,960 --> 00:07:57,310
 And why the advice always comes back to meditation is

98
00:07:57,310 --> 00:07:59,920
 because you're not really seeing what is

99
00:07:59,920 --> 00:08:01,680
 the true problem.

100
00:08:01,680 --> 00:08:05,640
 The problem is not in people, places and things.

101
00:08:05,640 --> 00:08:08,090
 The problem is really that you're, the biggest problem is

102
00:08:08,090 --> 00:08:09,560
 that you're looking at it in terms

103
00:08:09,560 --> 00:08:12,000
 of people, places and things.

104
00:08:12,000 --> 00:08:16,250
 None of those things are the problem because none of them

105
00:08:16,250 --> 00:08:17,040
 exist.

106
00:08:17,040 --> 00:08:20,410
 Even in a relationship between two people that's broken

107
00:08:20,410 --> 00:08:22,240
 like this quote says, in the

108
00:08:22,240 --> 00:08:24,230
 end it has nothing to do with the people or their

109
00:08:24,230 --> 00:08:25,080
 relationship.

110
00:08:25,080 --> 00:08:28,920
 It has to do with the mind.

111
00:08:28,920 --> 00:08:35,240
 When the mind is pure, the relationship mends itself.

112
00:08:35,240 --> 00:08:43,760
 And the minds of the people involved are pure.

113
00:08:43,760 --> 00:08:47,640
 And then goodness comes from it of course, good things.

114
00:08:47,640 --> 00:08:53,770
 Good people, good places, good things come from good mind

115
00:08:53,770 --> 00:08:54,800
 state.

116
00:08:54,800 --> 00:08:55,800
 Good states of mind.

117
00:08:55,800 --> 00:09:15,160
 So I'm here teaching in New York.

118
00:09:15,160 --> 00:09:20,800
 Saturday I taught, I'm teaching with another, with Sudha S

119
00:09:20,800 --> 00:09:21,560
ohi.

120
00:09:21,560 --> 00:09:30,020
 He's the teacher here so I'm not doing single teachings on

121
00:09:30,020 --> 00:09:31,320
 my own.

122
00:09:31,320 --> 00:09:34,680
 I lead people through guided meditation and then we do

123
00:09:34,680 --> 00:09:36,760
 discussion afterwards but he's

124
00:09:36,760 --> 00:09:40,520
 also involved.

125
00:09:40,520 --> 00:09:46,800
 So I taught meditation on Saturday to a group in Rockaway.

126
00:09:46,800 --> 00:09:51,320
 Rockaway, Rockaway.

127
00:09:51,320 --> 00:09:59,000
 Last night I taught in Brooklyn at a ballet studio.

128
00:09:59,000 --> 00:10:03,630
 Not to ballet students but they used the ballet studio as a

129
00:10:03,630 --> 00:10:05,160
 meditation hall.

130
00:10:05,160 --> 00:10:10,960
 They rented for $25 an hour which is pretty good.

131
00:10:10,960 --> 00:10:13,800
 So they had a small group of people there.

132
00:10:13,800 --> 00:10:18,260
 Today nothing, today was a nothing day so I've been here

133
00:10:18,260 --> 00:10:20,520
 all day but two of the meditators

134
00:10:20,520 --> 00:10:25,250
 from last night or two of the people from last night came

135
00:10:25,250 --> 00:10:26,360
 to see me.

136
00:10:26,360 --> 00:10:29,590
 And again, these two people were very focused on becoming a

137
00:10:29,590 --> 00:10:31,240
 monk and I didn't have too much

138
00:10:31,240 --> 00:10:37,100
 to tell them because well they're mostly interested in the

139
00:10:37,100 --> 00:10:39,000
 forest tradition.

140
00:10:39,000 --> 00:10:40,000
 Anjan Shah.

141
00:10:40,000 --> 00:10:46,940
 And I don't have too much advice because of course for us,

142
00:10:46,940 --> 00:10:49,760
 ordination isn't the most

143
00:10:49,760 --> 00:10:51,720
 important thing.

144
00:10:51,720 --> 00:10:57,200
 But I've been giving out several, I've given out quite a

145
00:10:57,200 --> 00:10:58,920
 few booklets.

146
00:10:58,920 --> 00:11:01,740
 And you know it's nice being in New York it feels like

147
00:11:01,740 --> 00:11:03,760
 there's a lot of interest and a

148
00:11:03,760 --> 00:11:05,160
 lot of potential here.

149
00:11:05,160 --> 00:11:10,640
 So I'm happy to have been invited to be part of this.

150
00:11:10,640 --> 00:11:17,760
 It's new, they're a new organization so it's not large.

151
00:11:17,760 --> 00:11:20,480
 But Thursday I'm going to Dharma Punks which is a big

152
00:11:20,480 --> 00:11:21,360
 organization.

153
00:11:21,360 --> 00:11:23,760
 I'm assuming there will be a large group there.

154
00:11:23,760 --> 00:11:25,760
 I don't know.

155
00:11:25,760 --> 00:11:29,760
 Maybe we're just using their center.

156
00:11:29,760 --> 00:11:32,760
 But they're a big organization.

157
00:11:32,760 --> 00:11:38,760
 Anyway, big or small it's nice to be useful.

158
00:11:38,760 --> 00:11:45,980
 I'll be here till Sunday, then Sunday afternoon I head back

159
00:11:45,980 --> 00:11:47,760
 to Ontario.

160
00:11:47,760 --> 00:11:54,760
 I have a meditator coming a couple of days after that.

161
00:11:54,760 --> 00:11:59,760
 And then I have to make time to go see my father.

162
00:11:59,760 --> 00:12:02,760
 And then June, the end of May we have ways up.

163
00:12:02,760 --> 00:12:07,760
 And then June I'll be in Thailand and Sri Lanka.

164
00:12:07,760 --> 00:12:12,760
 And then it's already getting on into the summer.

165
00:12:12,760 --> 00:12:16,760
 We have more people coming, July, August, September.

166
00:12:16,760 --> 00:12:21,230
 Maybe we're going to have to try to find a new place, a

167
00:12:21,230 --> 00:12:22,760
 bigger place.

168
00:12:22,760 --> 00:12:27,760
 Anyway, does anybody have any questions for me?

169
00:12:27,760 --> 00:12:30,760
 I can see you all on chat.

170
00:12:30,760 --> 00:12:38,760
 I'm assuming some people are listening.

171
00:12:57,760 --> 00:13:01,760
 Last night Robin came all the way from Connecticut.

172
00:13:01,760 --> 00:13:06,760
 She says it's about two hours, so two hour train ride.

173
00:13:06,760 --> 00:13:10,690
 Although for us it was an hour and a half bus ride, so it's

174
00:13:10,690 --> 00:13:12,760
 not that bad.

175
00:13:12,760 --> 00:13:22,760
 Okay, well then have a good night everyone.

176
00:13:22,760 --> 00:13:25,620
 Actually this is probably the only night I'm going to be

177
00:13:25,620 --> 00:13:26,760
 able to broadcast

178
00:13:26,760 --> 00:13:29,760
 because it seems like everything's at seven.

179
00:13:29,760 --> 00:13:32,760
 I think Saturday is the only day, something's at three.

180
00:13:32,760 --> 00:13:36,760
 So Saturday will probably be my next broadcast.

181
00:13:36,760 --> 00:13:38,760
 We'll see, I'll try my best.

182
00:13:38,760 --> 00:13:42,350
 See if I can find places to get internet around nine o'

183
00:13:42,350 --> 00:13:42,760
clock.

184
00:13:42,760 --> 00:13:45,760
 Actually probably could, some of the places.

185
00:13:45,760 --> 00:13:51,760
 See if I can broadcast at nine, but no, I promise you.

186
00:13:51,760 --> 00:13:55,760
 Anyway, good night everyone.

