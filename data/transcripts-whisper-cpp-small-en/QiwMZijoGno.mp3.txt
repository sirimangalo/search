 Hello and welcome back to our study of the Dhammapada.
 Today we continue on with verse number 127, which reads as
 follows.
 Nāntadike na samudamājhe na pabhata nāngvivarāng pavī
sa nāvijati sa jagati pade sa yatatthi to mujjaya pāpakam
ā
 which means not up in the sky or in the middle of the ocean
.
 Nāntadike na samudamājhe na pabhata nāngvivarāng pavī
sa, not having entered into a cave,
 a cavern in a mountain, entered into the middle of a
 mountain, the center of a mountain.
 Nāvijati sa jagati pade sa it cannot be found such a place
 on this earth.
 One cannot find a place on this earth, on the earth.
 Yatatthi to where standing, mujjaya pāpakamā. One might
 be free from evil karma, evil deeds.
 There's nowhere.
 Not up in the sky, not under the ocean, not in the deepest,
 darkest cave.
 There's nowhere on earth.
 That's the quote.
 So an interesting story, actually three stories, that goes
 with this verse.
 It seems there were three groups of monks, and so we have
 three stories.
 The Buddha was living in Jaitawana, but a first group of
 monks set out to meet the Buddha,
 and on their way they entered a village, a certain village
 for alms, and while they were
 sitting or after they had eaten, or while they were waiting
 for food, someone who was
 cooking food in the morning made their fire too hot, and
 suddenly the flame burst up and
 lit the satch, roof of their hut, and the satch went flying
 up in a ball of flame or
 floating through the air being carried away by the wind.
 At that moment a crow was flying, and so this tuft of satch
 caught the crow and immediately
 burst into flames.
 It was on fire, so the crow burnt to a crisp, fell to the
 ground, dead, right in front of
 the monks.
 And they thought it was kind of remarkable because just one
 tuft of satch flew up and
 caught the crow squarely as it was flying by.
 And so they wondered to themselves, sort of monk talk, "I
 wonder what the karma of this
 bird was that this would happen?"
 And they said, "Who would know besides the Buddha?
 So when we get there we should ask him."
 And so they continued on their way.
 That's the first group.
 A second group of monks also on their way to see the Buddha
.
 I mean, it seems like probably this was a thing where monks
 would come to the Buddha
 with these sorts of questions.
 So I think we shouldn't be surprised that there were three
 groups of them.
 The second group, on their way to see the Buddha, they took
 a boat across the ocean
 somehow.
 Not sure where they were.
 Maybe they were in Burma or Burma's attached.
 Maybe Thailand.
 Maybe they were in Sri Lanka.
 Who knows?
 Probably not.
 It's interesting to think of them being on a ship because
 there was not much record of
 monks outside of India.
 But I'm really not clear about that sort of thing.
 But it happened that in the middle of the ocean their boat
 stopped.
 The wind stopped.
 And they couldn't sail anymore.
 And this was a thing for sailors that if the wind stopped
 for a long period of time they
 would get superstitious.
 And they would think somebody on board is, their karma is
 not allowing us to continue.
 And so they'd throw that person overboard.
 If they could figure out who it was, and the way they did
 it was they found the scientific
 method.
 They'd draw lots and whoever drew the shortest straw would
 get thrown overboard.
 It seems reasonable, no?
 So they did this.
 And we thought whoever gets the shortest straw gets thrown
 over.
 That's just the way karma works.
 I guess.
 Except lo and behold, the captain's wife drew the lot.
 Now the captain's wife was loved by everyone.
 She was young, she was pretty, she was kind, she was great.
 Just all around, not the sort of person you want to throw
 overboard, to say the least.
 Especially since she was the captain's wife.
 And so everyone agreed they couldn't throw her overboard.
 So they said, "Well, we'll draw lots again."
 Then they drew lots and again for a second time the captain
's wife drew the shortest
 straw.
 And they still couldn't, they said there was no way we can
't do this.
 And so a third time they drew straws and a third time the
 captain's wife drew the shortest
 straw.
 So they went to the captain and they said, "This is what
 happened three times.
 It's got to be her.
 She's the one with the bad luck.
 We have to throw her overboard."
 And so they grabbed her.
 Then the captain said, "Well, then yes, I guess that's how
 it has to go.
 It's scientifically shown that she's to blame for the wind
."
 And so he said, "Throw overboard."
 And as they started to throw overboard, she started
 screaming reasonably.
 I mean, she doesn't seem to be that confident in the
 scientific method that they used, but
 she certainly doesn't seem to have wanted to be thrown
 overboard.
 So interestingly, the captain has them take her jewels away
.
 He says, "Well, there's no need to."
 When he heard this, he saw and he said, "Oh, there's no
 need for her jewels to go to waste."
 So he took her jewels and had them wrap her up in a cloth
 and tie a rope around her neck
 so that she couldn't, or wrap her up in a cloth so that she
 wouldn't scream.
 And then tie a rope around her neck and tie it to a big
 heavy pot of sand so that they
 wouldn't see her, so that he wouldn't have to see her
 because he was fond of her.
 And so they threw overboard.
 And as soon as she hit the ocean, sharks and turtles and
 fish and so on ate her, tore her
 to bits and she died.
 The monks on board were watching this and of course didn't
 really have a say in it
 all, but they were shocked as well.
 They couldn't believe that this sort of thing could happen,
 that she could really be at
 the mercy of these people because it was a bit of a
 coincidence that she drew the short
 straw three times.
 It's quite, unless there were only like a few people on
 board, that was quite a coincidence.
 And so they said, "I wonder what karma she did to deserve
 such a horrible fate?"
 And likewise they said, "Well, let's ask the Buddha and
 find out."
 It's important to point out as we go along, because I'm
 sure the question coming up in
 people's minds is, "Well, what about the people who did
 that to her?"
 I mean, it's not karma, it's those people.
 And it has to be mentioned that karma isn't like one thing
 in the past, you blame things
 in your past life.
 That's not true at all.
 Those people who threw that woman into the ocean did a very
, very bad thing.
 There's no question about it.
 That was an evil deed.
 And then Buddhism doesn't condone that.
 But how she got herself in that situation, you see, where
 the likelihood of her being
 subject to that, because these people were not doing it out
 of hate for her.
 They were doing it out of ignorance and superstition.
 But they didn't just randomly pick someone.
 So how did she get herself in that situation?
 The theory is that there's more behind it.
 Our life comes to these points.
 Anyway, that was the second one.
 The third story, there were seven monks who likewise set
 out to see the Buddha.
 And on their way, they came to a certain monastery and they
 asked to stay the night.
 And the seven of them were invited to stay in a special
 cave in the side of the mountain
 that was designated for visiting monks.
 So they went there and they settled down and they fell
 asleep for the night.
 During the night, a huge boulder, it says the size of a pag
oda, which would be very,
 very large, fell down the mountain and covered the entrance
 to the cave where they were staying,
 just out of the blue, blocking their exit and making it
 impossible for them to get out.
 When the resident monks found out what happened, they said,
 "We've got to move that rock.
 There's monks trapped in there."
 And so they gathered men, strong people from all around the
 countryside and they worked
 tirelessly for seven days to remove this rock, but the rock
 wouldn't budge.
 Until finally, on the seventh day, after seven days, the
 rock moved as though it had never
 been stuck there.
 The rock moved very easily.
 I think it even says that it moved by itself away from the
 entrance.
 It suddenly became dislodged.
 And so these seven monks had spent seven days without food,
 without water, were almost dead.
 And yet when they came out, they were able to get water and
 food and survive.
 But they said to themselves, "I wonder what we did.
 It seems a very strange sort of thing to happen.
 I wonder if this is a cause of past karma."
 And so likewise, they decided to ask the Buddha.
 So these three groups of monks met up and this is the story
.
 And then the Buddha tells three stories about their pasts.
 The first, the past of the crow, they go to see the Buddha
 and the Buddha says, "The crow
 is suffering for past deeds."
 And it seems like the story is kind of suggesting that it's
 not just one past deed, but it's
 sort of a habit of bad deeds.
 But he gives examples.
 So he says, "For a long time ago, the crow was a farmer and
 he had an ox and he was trying
 to get this ox to do work for him.
 But try as he might, he couldn't tame the ox.
 He'd get it to work and then it would work for a little bit
 and then it would go lie
 down.
 And then he'd get it to move and it wouldn't move.
 This ox was just terribly, terribly stubborn.
 And so he got increasingly more and more angry until he
 finally got angry enough that the
 ox just lay down, that he just covered it up in straw and
 lit it on fire."
 And the Buddha said, "Because of that evil deed, cruel evil
 deed, he was born in hell
 actually.
 And after being born in hell, he was born back in the human
 realm and he was born back
 in the animal realm as a crow and still suffering from it
 to this day."
 In fact, it says he was seven times in succession, reborn
 as a crow.
 And then we have the story of the woman on the boat.
 This woman in the past, she was a woman who lived in Ben
ares, Varanasi as it's known
 now.
 And she had a dog.
 She was a housewife and so she did all these chores, but
 there was a dog in the house that
 would follow her around everywhere.
 And for some reason, people would tease her because this
 dog was following her like a
 shadow and very, very much, very, very affectionate,
 actually like normal dogs are, but people
 were joking about it because I guess it wasn't a big thing
 for women to have dogs following
 them around.
 In fact, it was a common thing for hunters to have dogs, as
 we learned in our previous
 story.
 So these young men joked about it and said, "Oh, here comes
 the hunter with their dog.
 We're going to have meat to eat tonight."
 There will be meat coming, just joking about her looking
 like a hunter, having this big
 dog go along with it.
 And this woman was, I guess, of a cruel bent.
 And so getting angry and feeling embarrassed and ashamed,
 she picked up a stick and beat
 the dog almost to death.
 But dogs, being the way they are, have a funny resiliency
 to these sorts of things.
 And so the dog was actually unmoved and was still very much
 in love with this woman.
 It turns out, actually, the commentary says that this dog
 used to be her husband.
 And so that was recently her husband in one of her recent
 births.
 And so even though it's impossible to find, they say, you
 can't find someone who hasn't
 been your husband, your wife, your son, your daughter, your
 mother, your father.
 But recent births, there tends to still be some sort of
 affinity or enmity in cases when
 there was enmity before.
 And so she beat this dog and it still came back.
 And so she was increasingly angry, irrationally angry at
 this dog.
 And so lo and behold, when it came close, she picked up a
 rope and she made a loop and waited
 for the dog.
 And when the dog came close, she wrapped the loop around
 the dog and tied it to a pot full
 of sand and threw the pot of sand into this big pool.
 And it rolled down into the pool and the dog was pulled and
 was dragged after it into the
 pool and it drowned.
 And that was the karma that caused her to be thrown
 overboard as well as be spent many
 years in hell.
 That's story number two.
 Story number three, there he tells these, you monks are
 also have done bad things in
 the past.
 But one time you were cowherds and you came upon this huge
 lizard and I guess it was something
 that they would like to eat, people would like to eat.
 And so they ran after it trying to catch this big lizard.
 But it ran into an ant hill that had seven holes.
 Some reason seven is a big number.
 I think it probably just means there were a bunch of holes.
 And so they plugged up all these holes and then they, you
 know, thinking that they could
 catch it at one of the holes.
 But then they said, you know, we just don't have time for
 this.
 We'll plug up all the holes and we'll come back tomorrow
 and we'll catch this lizard
 because there's now no way out of this big, I guess, a term
ite mound or something.
 Something were lizards.
 I don't know.
 Something that lizards like to stay in.
 Big lizard though.
 So we'll come back tomorrow and we'll catch it.
 And so they went home but then they forgot all about it.
 And so for seven days they went and bought their business
 tending cows elsewhere.
 But then on the seventh day they came back and they were
 tending cows and they saw the
 ant hill again and they realized, oh, I wonder what
 happened to that lizard.
 And so they opened up the holes and the lizard at this
 point starved and dehydrated, not
 afraid of its, for its life at all.
 And it's basically at the end of its tether.
 I had to come out.
 And so came out and they said to themselves, they felt,
 they took pity on it.
 They said, oh, that's not killing it.
 This is this poor thing.
 We tortured it terribly.
 So they nursed it and they actually brought it back to life
.
 And he said, the Buddha said, see, because of that you were
 able to escape because you
 came back for this lizard.
 If not, that would have been it for you.
 I think these stories are interesting, whether you believe
 them or not, but they give some
 idea of the nature of karma according to Buddhism and some
 of the ways.
 They're just examples.
 It doesn't mean they're not law, like this has to be like
 this.
 But apparently the way things sometimes turn out, like our
 past deeds influence both in
 this life and the next, they influence the things that
 happen to us.
 And then they said, but is it really that, is it really
 that way that you can't escape
 your karma?
 Isn't there somewhere you can go to escape it?
 Couldn't you run away?
 And the Buddha said, no, you couldn't run away.
 There's no place on earth that you can go to run away from
 your karma.
 There's another jataka that talks about this.
 There's a goat that this Brahman, the goat talks to him and
 the goat says, you know,
 it's this crying, laughing jataka where he cries and then
 he laughs, or he laughs and
 then he cries.
 He's about to be killed and the goat starts laughing and
 then he says, why are you laughing?
 He said, because this is my last life.
 I was a bra, I'm now, this is the last life that I have to
 be born as a goat.
 For 500 years I've been a sacrificial goat.
 This is it.
 And then he starts crying and the guys, the brahmanas, why
 are you crying?
 And he said, because I'm thinking of you.
 Why I was a goat for 500 years, being sacrificed, having my
 head cut off is because before that
 I was a brahman just like you who killed goats.
 So I know this is where you are going to go.
 And the brahman said, oh, then I'll protect you.
 I won't let them kill you.
 And he said, there's nothing you can do.
 There's no way you can stop it.
 And sure enough, the brahman tried to protect him and made
 sure nobody came near him.
 But a rock fell actually on this goat.
 There's some really strange coincidence.
 He ended up dying.
 And the brahmanas like that.
 You see potentially these sorts of things in the world.
 Very strange things happened.
 There was a woman once, the wife of a top Monsanto exec.
 Not that that means anything, but it's interesting.
 Walking down the road, I read this in the paper some years
 ago, walking down the road
 and was suddenly hit by a car and pulled under the car and
 dragged screaming for several
 blocks before she died.
 Dragged under the car.
 Turns out the woman who was driving the car was an old lady
 who could barely see above
 the dash and had no idea what she'd done and probably to
 this day has never been told what
 she did.
 The story said they hadn't told her.
 So it wasn't a bad karma.
 She didn't have the intention to kill.
 Probably some bad karma involved with driving when you
 shouldn't be driving.
 But that's a bit different.
 But it's the kind of sort of, I mean we have no idea why
 that happened.
 People, modern people would say it's just a coincidence,
 but it's interesting to look
 and see.
 I mean if you think in terms of sort of cause and effect,
 the problem I think is that people
 focus too much on physical cause and effect and they call
 the mind, there's this term
 epiphenomenon that the mind is at best just a byproduct
 that is ineffectual, that has no
 consequence that the mind can't affect the body, can't
 affect reality.
 So mind is just this thing that happens sort of like just a
 byproduct, a side product that
 is meaningless.
 But if you think of the mind as being powerful, as being
 potent, then it makes sense to think
 that such a powerful experience should have some cause.
 Like physical things don't just happen coincidentally.
 An explosion takes gunpowder.
 A supernova takes a lot of energy.
 And so the idea that these experiences of being dragged
 under a car should take some,
 should not just happen randomly.
 I think there's something to that.
 I think there's something that we're missing often.
 When we say it's just random, it's just coincidence.
 I think there's an argument, even not from meditation or so
 on, that it would take some
 kind of structure, some kind of cause and effect.
 But for meditative purposes, I mean this is of great
 importance to us, the idea that our
 intentions, our minds have consequences.
 This is something that moves people to meditate and moves
 meditators not to do unwholesome
 deeds.
 Meditation for this reason will change your life because
 you start to see how powerful
 and how poisonous the mind can be, how harmful the mind can
 be when misdirected, how dangerous
 it is.
 You can see these things building.
 You can see how poisonous.
 You can imagine what it's like to do these things and you
 can remember the things that
 you've done.
 And without any prompting, like anyone telling you it's
 wrong or it's bad, you just start
 to feel really bad about the things that you've done, bad
 things you've done.
 It doesn't take someone to tell you that's bad karma or
 something.
 That happens as well.
 You feel guilty because they're told that things are bad.
 But you just can't abide by it because it's so powerful.
 This is where you start to feel the power of these deeds.
 That's karmic and it's built up inside of you.
 That's why our life flashes before our eyes.
 Our life doesn't really flash before our eyes when we die.
 The things that have had an impact on our mind, that's
 karma.
 Those things flash before our eyes.
 So this has importance in our practice and to remind us and
 to reinforce the things that
 we see during our practice.
 But also to encourage people to meditate, to learn about
 these things.
 If you want to become a better person, look at your mind,
 learn about your mind.
 These things occur throughout our lives.
 People do evil things, evil things happen to people who don
't seem to deserve them,
 who don't seem to ever have done anything to deserve them.
 But when through meditation we see that there actually is
 this cause and effect relationship.
 And it's quite reasonable to suggest that it continues into
 the future.
 It's quite reasonable to think whether you have evidence or
 proof of it or not.
 It's quite reasonable to think that it's going to have an
 effect on your next life.
 It's going to have an effect on your choices of rebirth.
 It's going to have an effect on how people react to you in
 the future.
 That things don't go away.
 The key to this verse is that they don't just go away.
 The only way to outrun it would be to become an arahant, an
 enlightened being, before it's
 going to catch up with you.
 And if you die as an arahant, then the future karma doesn't
 have an opportunity to bear
 fruit.
 Anyway, so some stories about karma, some ideas of karma,
 that's the Dhamma Pandapur
 tonight.
 Thank you all for tuning in.
 I wish you all good practice.
 1
