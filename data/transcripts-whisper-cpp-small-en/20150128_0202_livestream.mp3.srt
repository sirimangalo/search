1
00:00:00,000 --> 00:00:07,000
 Okay, I believe we are connected here.

2
00:00:07,000 --> 00:00:23,080
 Good morning everyone.

3
00:00:23,080 --> 00:00:40,880
 I'm live here from Mehong-san, province in northern

4
00:00:40,880 --> 00:00:44,880
 Thailand.

5
00:00:44,880 --> 00:00:54,850
 We were talking yesterday about death and the importance of

6
00:00:54,850 --> 00:00:57,600
 death and the significance

7
00:00:57,600 --> 00:01:08,480
 of this fact of our existence.

8
00:01:08,480 --> 00:01:13,280
 Death is a hot topic in Buddhism.

9
00:01:13,280 --> 00:01:18,440
 It's a cause for reflection.

10
00:01:18,440 --> 00:01:31,220
 It's a subject of an entire meditation practice because it

11
00:01:31,220 --> 00:01:39,960
's the most conclusive argument

12
00:01:39,960 --> 00:01:54,040
 against clinging to the pleasure and the attachment of our

13
00:01:54,040 --> 00:02:01,120
 lives.

14
00:02:01,120 --> 00:02:13,400
 Listening to anything in this life is, in the end, futile.

15
00:02:13,400 --> 00:02:22,600
 It's not a goal and it can't be a goal in and of itself

16
00:02:22,600 --> 00:02:26,800
 because in the end it's all

17
00:02:26,800 --> 00:02:27,800
 left behind.

18
00:02:27,800 --> 00:02:37,040
 There has to be something further, something greater.

19
00:02:37,040 --> 00:02:43,940
 It's also a reminder for us that we have a limited amount

20
00:02:43,940 --> 00:02:47,200
 of time to continue our current

21
00:02:47,200 --> 00:02:51,280
 state of affairs.

22
00:02:51,280 --> 00:03:02,320
 So our practice of meditation is circumscribed by our lives

23
00:03:02,320 --> 00:03:03,040
.

24
00:03:03,040 --> 00:03:04,720
 We don't know what happens when we die.

25
00:03:04,720 --> 00:03:23,340
 We do know that it's a significant disruption.

26
00:03:23,340 --> 00:03:33,170
 And if we're not ready for it, it can be a great source of

27
00:03:33,170 --> 00:03:37,360
 suffering and it can be a

28
00:03:37,360 --> 00:03:48,340
 cause for falling into states of loss as we're not prepared

29
00:03:48,340 --> 00:03:52,620
 for the choices that you have

30
00:03:52,620 --> 00:04:03,670
 to make when death comes, the reactions that will occur in

31
00:04:03,670 --> 00:04:06,000
 the mind.

32
00:04:06,000 --> 00:04:09,040
 So it's something that spurs us on.

33
00:04:09,040 --> 00:04:12,410
 The thought of death is something that leads to

34
00:04:12,410 --> 00:04:15,520
 strengthening of resolve and meditation

35
00:04:15,520 --> 00:04:26,520
 practice as we realize that we really aren't satisfied or

36
00:04:26,520 --> 00:04:33,920
 assured in our hearts of our

37
00:04:33,920 --> 00:04:35,720
 attainments in this life.

38
00:04:35,720 --> 00:04:38,800
 We still have further to go.

39
00:04:38,800 --> 00:04:44,800
 And it reminds us not to be lazy, not to be lax, not to

40
00:04:44,800 --> 00:04:48,400
 rest on our laurels, not to be

41
00:04:48,400 --> 00:04:54,760
 content with what we've already attained.

42
00:04:54,760 --> 00:05:02,030
 But a curious part of our conversation was when one of the

43
00:05:02,030 --> 00:05:06,440
 monks asked, just as an example,

44
00:05:06,440 --> 00:05:14,490
 he asked me, "So in your country, are there people who don

45
00:05:14,490 --> 00:05:16,040
't die?"

46
00:05:16,040 --> 00:05:17,880
 And of course I gave the expected response.

47
00:05:17,880 --> 00:05:19,380
 I said, "No, everyone dies."

48
00:05:19,380 --> 00:05:22,570
 But at the same time, one of the other monks laughed at him

49
00:05:22,570 --> 00:05:24,320
 and said, "What a ridiculous

50
00:05:24,320 --> 00:05:25,320
 question.

51
00:05:25,320 --> 00:05:32,040
 How can you ask such a dumb question?

52
00:05:32,040 --> 00:05:36,500
 And so it opened up another branch of thought, of

53
00:05:36,500 --> 00:05:40,320
 discussion when I said, "But it's true,

54
00:05:40,320 --> 00:05:41,320
 you know.

55
00:05:41,320 --> 00:05:44,240
 I mean, I'm not sure.

56
00:05:44,240 --> 00:05:48,400
 Maybe some of the people alive now won't die."

57
00:05:48,400 --> 00:05:51,320
 They were laughing, so I just made it as a joke.

58
00:05:51,320 --> 00:05:56,440
 I said, "Maybe there are some people who won't die.

59
00:05:56,440 --> 00:06:01,200
 I haven't seen everyone die."

60
00:06:01,200 --> 00:06:03,430
 And it was kind of the wrong answer to give because that's

61
00:06:03,430 --> 00:06:05,560
 obviously not a Buddhist answer.

62
00:06:05,560 --> 00:06:10,480
 And it's not a Buddhist belief that anyone can...

63
00:06:10,480 --> 00:06:16,520
 One of the monks said, "Anything that is born has to die.

64
00:06:16,520 --> 00:06:26,240
 It's not possible for someone to be born and not die."

65
00:06:26,240 --> 00:06:33,390
 But there's a deeper issue here, is the fact that you can't

66
00:06:33,390 --> 00:06:36,000
 really know that.

67
00:06:36,000 --> 00:06:40,140
 This is something that science teaches us, the modern

68
00:06:40,140 --> 00:06:42,880
 scientific method, that you can't

69
00:06:42,880 --> 00:06:44,600
 prove that.

70
00:06:44,600 --> 00:06:47,520
 All you can do is wait to disprove it.

71
00:06:47,520 --> 00:06:53,090
 And for as long as you haven't disproved it with a person

72
00:06:53,090 --> 00:06:56,000
 living forever, then you say

73
00:06:56,000 --> 00:06:58,360
 it's true.

74
00:06:58,360 --> 00:07:04,250
 But you don't have absolute proof because the next moment

75
00:07:04,250 --> 00:07:06,880
 someone could be born who

76
00:07:06,880 --> 00:07:16,760
 doesn't die.

77
00:07:16,760 --> 00:07:21,140
 And so I said, "Well, you know, death is really just a

78
00:07:21,140 --> 00:07:22,200
 concept.

79
00:07:22,200 --> 00:07:29,920
 We don't actually even know whether someone has died.

80
00:07:29,920 --> 00:07:32,320
 When you see a person, this person has died, that person

81
00:07:32,320 --> 00:07:32,880
 has died.

82
00:07:32,880 --> 00:07:33,880
 What does that mean?

83
00:07:33,880 --> 00:07:35,040
 What have you actually seen?

84
00:07:35,040 --> 00:07:40,800
 What have you actually experienced?"

85
00:07:40,800 --> 00:07:42,930
 You don't know that the person was ever alive to begin with

86
00:07:42,930 --> 00:07:43,120
.

87
00:07:43,120 --> 00:07:45,080
 They could have been a zombie.

88
00:07:45,080 --> 00:07:47,080
 They could have been a robot.

89
00:07:47,080 --> 00:07:52,680
 They could have been a mindless automaton.

90
00:07:52,680 --> 00:08:01,360
 They never had a contact with their mind, unless you have

91
00:08:01,360 --> 00:08:05,800
 special magical powers that

92
00:08:05,800 --> 00:08:14,760
 allow you to read other people's minds, to know the minds

93
00:08:14,760 --> 00:08:16,560
 of others.

94
00:08:16,560 --> 00:08:22,670
 All you know is that that person stopped moving, stopped

95
00:08:22,670 --> 00:08:27,400
 breathing, the body went cold, that

96
00:08:27,400 --> 00:08:30,080
 body is now useless.

97
00:08:30,080 --> 00:08:37,040
 That's all you know.

98
00:08:37,040 --> 00:08:44,700
 And this is a crucial delimiter in Buddhism between what

99
00:08:44,700 --> 00:08:47,960
 you can know and what you can't

100
00:08:47,960 --> 00:08:51,720
 know.

101
00:08:51,720 --> 00:08:55,880
 Because Buddhism makes the claim that that all being said,

102
00:08:55,880 --> 00:08:58,120
 there still are certain things

103
00:08:58,120 --> 00:09:05,680
 that you can know for certain.

104
00:09:05,680 --> 00:09:08,980
 And they're very simple things, so simple that mostly they

105
00:09:08,980 --> 00:09:10,560
're overlooked and ignored

106
00:09:10,560 --> 00:09:18,840
 as meaningless, useless.

107
00:09:18,840 --> 00:09:22,560
 But they're very simple things you can know that seeing,

108
00:09:22,560 --> 00:09:23,680
 for example.

109
00:09:23,680 --> 00:09:29,960
 You can know that seeing is seeing.

110
00:09:29,960 --> 00:09:31,800
 When you see something, you don't know the details, you don

111
00:09:31,800 --> 00:09:32,980
't know whether you're actually

112
00:09:32,980 --> 00:09:39,430
 seeing the thing that the object that you think you're

113
00:09:39,430 --> 00:09:40,800
 seeing.

114
00:09:40,800 --> 00:09:45,030
 You don't know who it is that's doing the seeing or all the

115
00:09:45,030 --> 00:09:47,000
 details, but you do know

116
00:09:47,000 --> 00:09:51,360
 that it's the experience of seeing.

117
00:09:51,360 --> 00:09:57,440
 You can know that.

118
00:09:57,440 --> 00:10:02,940
 Seeing is hearing, smelling, smelling, tasting, feeling,

119
00:10:02,940 --> 00:10:04,120
 thinking.

120
00:10:04,120 --> 00:10:09,240
 You can be quite clear that there is an experience.

121
00:10:09,240 --> 00:10:13,490
 It's not reasonable to suggest that it is something besides

122
00:10:13,490 --> 00:10:14,840
 an experience.

123
00:10:14,840 --> 00:10:17,400
 So there's a knowledge there.

124
00:10:17,400 --> 00:10:21,010
 And that's such a simple statement that it seems quite

125
00:10:21,010 --> 00:10:23,940
 useless, meaningless, quite obvious,

126
00:10:23,940 --> 00:10:28,560
 so obvious it need not be mentioned.

127
00:10:28,560 --> 00:10:36,540
 The truth is that's a doorway to a whole realm of profound

128
00:10:36,540 --> 00:10:38,640
 knowledge.

129
00:10:38,640 --> 00:10:42,740
 Because experience works very much the way we perceive the

130
00:10:42,740 --> 00:10:44,320
 physical world to work.

131
00:10:44,320 --> 00:10:48,960
 It works based on cause and effect.

132
00:10:48,960 --> 00:10:54,570
 So you can come to understand what we call karma just by

133
00:10:54,570 --> 00:10:57,240
 watching experience.

134
00:10:57,240 --> 00:11:04,670
 When you watch your experience literally being born and die

135
00:11:04,670 --> 00:11:09,080
, you start to see that it actually

136
00:11:09,080 --> 00:11:14,960
 has a certain order to it, orderliness.

137
00:11:14,960 --> 00:11:20,280
 That when you see certain things, liking arises.

138
00:11:20,280 --> 00:11:25,400
 When you see other things, disliking arises.

139
00:11:25,400 --> 00:11:29,200
 Based on liking or disliking, there are arises further mind

140
00:11:29,200 --> 00:11:36,840
 states and further physical states.

141
00:11:36,840 --> 00:11:45,700
 And so you can actually learn something about the order

142
00:11:45,700 --> 00:11:49,820
liness of experience.

143
00:11:49,820 --> 00:11:57,410
 You can also learn about the difference between the quality

144
00:11:57,410 --> 00:12:01,600
, different quality of certain

145
00:12:01,600 --> 00:12:08,920
 states, seeing that actually anger and greed, liking and

146
00:12:08,920 --> 00:12:12,960
 disliking are a cause for stress

147
00:12:12,960 --> 00:12:18,240
 and suffering.

148
00:12:18,240 --> 00:12:21,960
 Disliking is painful, liking is dissatisfying.

149
00:12:21,960 --> 00:12:28,400
 In the end it's addictive and leads to disappointment,

150
00:12:28,400 --> 00:12:34,160
 leads to stress, leads to addiction and seeking

151
00:12:34,160 --> 00:12:44,560
 and dissatisfaction, constant wanting and needing.

152
00:12:44,560 --> 00:12:47,920
 On the opposite side you can see how clarity of mind, that

153
00:12:47,920 --> 00:12:49,720
 simply being aware of things,

154
00:12:49,720 --> 00:12:52,720
 how it changes the mind.

155
00:12:52,720 --> 00:12:57,960
 You can see the effect of observation, of objectivity when

156
00:12:57,960 --> 00:13:00,160
 the mind is simply aware

157
00:13:00,160 --> 00:13:04,440
 of the experiences of seeing and hearing.

158
00:13:04,440 --> 00:13:14,810
 When the mind stops reacting, just knowing seeing is seeing

159
00:13:14,810 --> 00:13:19,160
, hearing is hearing.

160
00:13:19,160 --> 00:13:21,440
 It's as it is.

161
00:13:21,440 --> 00:13:23,160
 You can see how that affects the mind.

162
00:13:23,160 --> 00:13:48,600
 The mind becomes clear and bright, calm, content.

163
00:13:48,600 --> 00:13:52,040
 And you start to know so many more things from this.

164
00:13:52,040 --> 00:13:55,540
 This is actually the path that leads to what we call

165
00:13:55,540 --> 00:13:56,960
 enlightenment.

166
00:13:56,960 --> 00:14:03,010
 You begin to see the nature of reality from this point of

167
00:14:03,010 --> 00:14:06,280
 view, from the point of view

168
00:14:06,280 --> 00:14:08,480
 of experience.

169
00:14:08,480 --> 00:14:15,700
 You see true birth and death is momentary and everything is

170
00:14:15,700 --> 00:14:17,840
 born and dies.

171
00:14:17,840 --> 00:14:26,100
 You see that it follows this steady course of cause and

172
00:14:26,100 --> 00:14:30,480
 effect, regardless of our own

173
00:14:30,480 --> 00:14:34,280
 likes and dislikes, wants and perversions.

174
00:14:34,280 --> 00:14:54,160
 And so slowly, slowly the mind becomes objective.

175
00:14:54,160 --> 00:14:56,960
 It loses all of its attachment as it sees that the

176
00:14:56,960 --> 00:14:58,920
 attachments are only a cause for

177
00:14:58,920 --> 00:15:00,920
 greater suffering.

178
00:15:00,920 --> 00:15:07,260
 And our attachments are actually futile because things don

179
00:15:07,260 --> 00:15:09,760
't go the way we want.

180
00:15:09,760 --> 00:15:20,680
 They don't follow any kind of servitude.

181
00:15:20,680 --> 00:15:29,400
 They don't serve us in any way.

182
00:15:29,400 --> 00:15:31,360
 In fact, they're habitual.

183
00:15:31,360 --> 00:15:35,120
 Our states are accumulative.

184
00:15:35,120 --> 00:15:40,320
 So greed leads to more greed, anger leads to more anger.

185
00:15:40,320 --> 00:15:44,760
 The anxiety leads one to become anxious.

186
00:15:44,760 --> 00:15:48,400
 Fear leads one to become paranoid and so on.

187
00:15:48,400 --> 00:15:52,600
 They don't ever have any kind of conclusion.

188
00:15:52,600 --> 00:16:01,750
 They become more and more pronounced until they become true

189
00:16:01,750 --> 00:16:06,600
 problems, obstacles, debilitating.

190
00:16:06,600 --> 00:16:08,040
 And so we begin to let go of this.

191
00:16:08,040 --> 00:16:15,420
 We see the futility of our reaction, futility of our

192
00:16:15,420 --> 00:16:20,760
 partialities and our aspersions and

193
00:16:20,760 --> 00:16:27,240
 our aspirations, futility of our ambition.

194
00:16:27,240 --> 00:16:36,060
 And to see that it's all circumscribed by this natural law,

195
00:16:36,060 --> 00:16:39,320
 natural orderliness.

196
00:16:39,320 --> 00:16:46,040
 And in the end, the eventuality or the eternity of

197
00:16:46,040 --> 00:16:51,560
 existence means eventually it all falls

198
00:16:51,560 --> 00:16:54,960
 apart and all breaks down.

199
00:16:54,960 --> 00:16:57,240
 It can never be sustained indefinitely.

200
00:16:57,240 --> 00:17:03,580
 It doesn't lead to any plateau where you can finally rest

201
00:17:03,580 --> 00:17:05,680
 and be content.

202
00:17:05,680 --> 00:17:11,900
 And the only contentment lies in letting go and giving up

203
00:17:11,900 --> 00:17:16,560
 and becoming free from the vicissitudes

204
00:17:16,560 --> 00:17:23,040
 of life.

205
00:17:23,040 --> 00:17:24,200
 This is actually the true death.

206
00:17:24,200 --> 00:17:28,960
 When we talk about death, this is true death, is the death

207
00:17:28,960 --> 00:17:31,400
 of our, the bond that ties us

208
00:17:31,400 --> 00:17:39,910
 to circumstance, the death of our attachment, the death of

209
00:17:39,910 --> 00:17:42,320
 defilements.

210
00:17:42,320 --> 00:17:54,930
 True death is what dies as our desire for becoming

211
00:17:54,930 --> 00:17:59,200
 something, for some state to arise,

212
00:17:59,200 --> 00:18:03,600
 a desire for anything.

213
00:18:03,600 --> 00:18:11,470
 When that's gone, what we call true death is, and that is

214
00:18:11,470 --> 00:18:13,440
 gone forever.

215
00:18:13,440 --> 00:18:18,070
 This is when one attains nirvana or nirvana and there's no

216
00:18:18,070 --> 00:18:19,280
 clinging.

217
00:18:19,280 --> 00:18:26,120
 This is called the death of, that is cut off, so it's

218
00:18:26,120 --> 00:18:32,680
 complete and utter and final cessation,

219
00:18:32,680 --> 00:18:36,680
 cessation of craving, cessation of suffering.

220
00:18:36,680 --> 00:18:41,560
 So, a little bit of food for thought done this morning.

221
00:18:41,560 --> 00:18:47,000
 I try to give a little bit every day.

222
00:18:47,000 --> 00:18:48,520
 Thanks everyone for tuning in.

223
00:18:48,520 --> 00:18:50,280
 I hope that went through.

224
00:18:50,280 --> 00:18:53,990
 I actually have a live audience today, so I can give them a

225
00:18:53,990 --> 00:18:55,600
 little bit of done every

226
00:18:55,600 --> 00:18:56,600
 day as well.

227
00:18:56,600 --> 00:18:59,600
 I have meditators here in the modern monastery.

228
00:18:59,600 --> 00:19:04,600
 So, thank you all.

229
00:19:04,600 --> 00:19:05,880
 Again tomorrow at the same time.

230
00:19:05,880 --> 00:19:06,880
 Thank you.

231
00:19:06,880 --> 00:19:07,880
 Thank you.

232
00:19:07,880 --> 00:19:07,880
 Thank you.

233
00:19:07,880 --> 00:19:08,880
 Thank you.

234
00:19:08,880 --> 00:19:09,880
 Thank you.

