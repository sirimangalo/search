1
00:00:00,000 --> 00:00:08,520
 Hello, sir. I have just started meditation, though I find

2
00:00:08,520 --> 00:00:10,680
 it very positive and calm, but

3
00:00:10,680 --> 00:00:15,950
 still I don't manage to maintain that calmness throughout

4
00:00:15,950 --> 00:00:19,860
 the day. Sir, in the sleep I am

5
00:00:19,860 --> 00:00:24,190
 having dreams related to my past life, and when I woke up

6
00:00:24,190 --> 00:00:26,240
 the next morning I used to

7
00:00:26,240 --> 00:00:31,750
 think a lot about my past life, maybe because I just had

8
00:00:31,750 --> 00:00:35,120
 dreams related to my past experiences.

9
00:00:35,120 --> 00:00:39,270
 What should I do to maintain the calmness throughout the

10
00:00:39,270 --> 00:00:47,480
 day? Well, as the kids in the

11
00:00:47,480 --> 00:00:53,350
 hall said, the only way to be happy is to know you can't be

12
00:00:53,350 --> 00:00:56,560
 happy all the time. So I'll say the same

13
00:00:56,560 --> 00:00:59,650
 for calm. The only way to be calm is to know that you can't

14
00:00:59,650 --> 00:01:07,160
 be calm all the time. It's impermanent.

15
00:01:07,160 --> 00:01:12,830
 What you're seeing is one of the important realities of

16
00:01:12,830 --> 00:01:16,480
 life that we as human beings fail

17
00:01:16,480 --> 00:01:22,110
 to appreciate, fail to accept. We're unable to accept imper

18
00:01:22,110 --> 00:01:26,720
manence on so many levels. We strive

19
00:01:26,720 --> 00:01:33,840
 always for stability, for continuity. How much of our lives

20
00:01:33,840 --> 00:01:37,800
 is spent trying to ward off change,

21
00:01:37,800 --> 00:01:47,870
 trying to ward off loss? What you're talking about is just

22
00:01:47,870 --> 00:01:58,680
 an extension of this idea of loss,

23
00:01:58,680 --> 00:02:06,850
 when a person clings to their possessions. You cling to

24
00:02:06,850 --> 00:02:10,280
 your house or your car and then your

25
00:02:10,280 --> 00:02:15,750
 car gets stolen or your house burns down. We guard so much

26
00:02:15,750 --> 00:02:20,400
 against loss. So really what you're

27
00:02:20,400 --> 00:02:23,380
 saying is not categorically different, and this is

28
00:02:23,380 --> 00:02:27,440
 something important to realize. This fear or

29
00:02:27,440 --> 00:02:33,150
 this aversion to losing even a state of calm, this is an

30
00:02:33,150 --> 00:02:36,400
 attachment. This is an inability to

31
00:02:36,400 --> 00:02:42,600
 accept the reality of the situation. That's partiality. In

32
00:02:42,600 --> 00:02:45,480
 that sense, that's what's stopping

33
00:02:45,480 --> 00:02:50,380
 you from being calm. If you want to be truly at peace, let

34
00:02:50,380 --> 00:02:53,880
's put aside the word calm because it

35
00:02:53,880 --> 00:02:57,440
 brings up connotations of a feeling. As I said, that's only

36
00:02:57,440 --> 00:03:02,880
 one type of equanimity. This actually

37
00:03:02,880 --> 00:03:05,820
 goes to the charge that people level on us that we're just

38
00:03:05,820 --> 00:03:07,880
 going to become equanimous zombies.

39
00:03:07,880 --> 00:03:12,460
 We'll just be calm all the time and like stoners or

40
00:03:12,460 --> 00:03:16,600
 something. But that's really not the case,

41
00:03:16,600 --> 00:03:22,000
 that you actually are able to experience a whole spectrum

42
00:03:22,000 --> 00:03:25,400
 of mind states and of experiences. You

43
00:03:25,400 --> 00:03:28,800
 can't experience, an enlightened person can't experience

44
00:03:28,800 --> 00:03:30,920
 anger or greed. So if you really like

45
00:03:30,920 --> 00:03:35,940
 those ones, you're in for trouble. But don't worry, as long

46
00:03:35,940 --> 00:03:39,440
 as you have craving, people are always

47
00:03:39,440 --> 00:03:43,300
 afraid of this thing I mentioned, people are afraid of

48
00:03:43,300 --> 00:03:46,080
 losing their craving. Well, if you're

49
00:03:46,080 --> 00:03:48,780
 if you want it, if you think it's a good thing, you'll

50
00:03:48,780 --> 00:03:50,960
 never let how could you possibly let go of

51
00:03:50,960 --> 00:03:54,990
 it? What is there to be afraid of? You can't just oops,

52
00:03:54,990 --> 00:03:57,760
 suddenly I lost my I didn't mean to let that

53
00:03:57,760 --> 00:04:05,860
 go. I still wanted that one. It's not like that. You give

54
00:04:05,860 --> 00:04:10,240
 up what you're willing to give up. And

55
00:04:10,240 --> 00:04:14,950
 this is the equanimity that comes comes to us. We can still

56
00:04:14,950 --> 00:04:18,000
 experience pain, we can still experience,

57
00:04:18,000 --> 00:04:21,880
 even if we become an Arahant, we'll still be able to

58
00:04:21,880 --> 00:04:26,600
 experience the whole range of experiences. So

59
00:04:26,600 --> 00:04:31,500
 we won't feel calm all the time, even an Arahant will not

60
00:04:31,500 --> 00:04:34,840
 feel a feeling of calm at all times.

61
00:04:34,840 --> 00:04:40,970
 Sometimes they'll be happy. But they'll either be happy or

62
00:04:40,970 --> 00:04:44,560
 they'll become. And even that calm may

63
00:04:44,560 --> 00:04:47,000
 not feel calm at times, because the mind can still be

64
00:04:47,000 --> 00:04:49,120
 active and there can still be thought in the

65
00:04:49,120 --> 00:04:52,340
 mind. So it may not be perceived as a state of calm, but it

66
00:04:52,340 --> 00:04:54,600
 will be a state of equanimity and the

67
00:04:54,600 --> 00:05:00,420
 feeling will be one of equanimity. But what is important is

68
00:05:00,420 --> 00:05:03,040
 not the state of calm that you

69
00:05:03,040 --> 00:05:07,390
 experience. What's important is you're staying calm, even

70
00:05:07,390 --> 00:05:10,240
 when the calm disappears. Can you do that?

71
00:05:10,240 --> 00:05:15,470
 When the calm is gone, are you still calm? Because it's on

72
00:05:15,470 --> 00:05:19,360
 a different level. Are you judging the

73
00:05:19,360 --> 00:05:23,530
 experience that you have now? When you're not calm? Because

74
00:05:23,530 --> 00:05:25,960
 when you're judging that, that's what's

75
00:05:25,960 --> 00:05:28,870
 causing your suffering. It's craving that causes suffering.

76
00:05:28,870 --> 00:05:30,560
 If you look at the Four Noble Truths,

77
00:05:30,560 --> 00:05:35,450
 the Buddha was quite clear. "Tanha" "Yayang tanha" "Ono bh

78
00:05:35,450 --> 00:05:39,560
avika" "Nandi raga sahagata" "Tatar tatar

79
00:05:39,560 --> 00:05:49,460
 abi nandini" Whatever craving that leads to further

80
00:05:49,460 --> 00:05:56,160
 becoming and is associated with desire,

81
00:05:56,160 --> 00:05:59,660
 this is the cause of suffering, which finds delight in this

82
00:05:59,660 --> 00:06:02,360
 or that or anything that is

83
00:06:02,360 --> 00:06:08,020
 the cause of suffering. Even attachment to wholesome state

84
00:06:08,020 --> 00:06:10,240
 is a cause, as you can see,

85
00:06:10,240 --> 00:06:21,670
 for cause of suffering. I would like to say something about

86
00:06:21,670 --> 00:06:26,800
 the dreams. Yes, good. Thank you.

87
00:06:26,800 --> 00:06:32,810
 You said you're having dreams related to your past life and

88
00:06:32,810 --> 00:06:38,040
 so on. Don't take that so serious.

89
00:06:38,040 --> 00:06:45,450
 It's nothing important. It may be a dream about a past life

90
00:06:45,450 --> 00:06:50,640
. It may be a dream about experiences

91
00:06:50,640 --> 00:06:57,950
 of the day. That doesn't really matter. We had that earlier

92
00:06:57,950 --> 00:07:04,400
. It's past and gone. Of course,

93
00:07:04,400 --> 00:07:10,710
 in the sleep, we work out things that we dealt with in the

94
00:07:10,710 --> 00:07:14,360
 past. We can't control that really.

95
00:07:14,360 --> 00:07:20,150
 So this is just going on and maybe it's important for you

96
00:07:20,150 --> 00:07:23,720
 to work that out. So just let it happen

97
00:07:23,720 --> 00:07:29,330
 and be mindful that it happened. You don't need to always

98
00:07:29,330 --> 00:07:33,120
 be calm about it. Sometimes these things

99
00:07:33,120 --> 00:07:40,200
 are scary. Then look at it or sometimes they are important.

100
00:07:40,200 --> 00:07:46,080
 Then look at it. So just push things

101
00:07:46,080 --> 00:07:53,950
 away by wanting to be calm. That's great danger. To mental

102
00:07:53,950 --> 00:07:59,040
 or to cover everything with calmness is

103
00:07:59,040 --> 00:08:05,190
 not wanting to see reality as it is because reality is not

104
00:08:05,190 --> 00:08:08,720
 calm. The only thing that helps

105
00:08:08,720 --> 00:08:15,910
 is to develop equanimity. As Banta said in the in the

106
00:08:15,910 --> 00:08:20,120
 answer earlier, then you can be calm naturally.

107
00:08:20,120 --> 00:08:24,920
 But if you want to be calm, Banta mentioned it already,

108
00:08:24,920 --> 00:08:28,120
 then you won't be calm because there's

109
00:08:28,120 --> 00:08:36,840
 always that that greed for wanting to be calm. So when you

110
00:08:36,840 --> 00:08:41,400
 take this all to serious and then

111
00:08:41,400 --> 00:08:48,640
 want to cover it up, it one day might explode and you will

112
00:08:48,640 --> 00:08:51,480
 not be calm at all.

113
00:08:51,480 --> 00:09:02,670
 Yeah, just to reiterate what I think is the most important

114
00:09:02,670 --> 00:09:08,720
 is that past is past. If you've had past

115
00:09:08,720 --> 00:09:13,320
 life memories or so on, it's still past. Buddha said, "ad

116
00:09:13,320 --> 00:09:15,600
itya nannvakamaya" don't go back to the past.

117
00:09:15,600 --> 00:09:20,580
 So sitting around thinking about the past is not going to

118
00:09:20,580 --> 00:09:23,080
 do much except cultivate the habit of

119
00:09:23,080 --> 00:09:23,480
 thinking.

120
00:09:23,480 --> 00:09:33,480
 [BLANK_AUDIO]

