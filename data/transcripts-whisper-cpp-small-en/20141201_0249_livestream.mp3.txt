 Okay, good morning everyone.
 Broadcasting again.
 Again broadcasting from Jontong, Thailand.
 I'm staying in the shade of a short but broad tree in a
 newly planted forest here in Jontong.
 The place has really grown up.
 The teacher has always been interested in the forest.
 He was born in Haut, which is a small village just south of
 here.
 It quickly was established as one of the big monks in Ch
iang Mai.
 When he was a novice, he was a student of a very famous Ch
iang Mai monk, Kubasivichai.
 So maybe that had something to do with it.
 But he made friends with one monk who went on to become the
 head monk in Chiang Mai.
 He was established as one of the big meditation teachers in
 Chiang Mai.
 That's from what I've been told by the head monk.
 The head monk in Chiang Mai told me, he's retired now, but
 at the time he told me,
 I said I'll take the bureaucratic side and Rumpotong and
 take the Achanthong and take the meditation side.
 So he split it up like that.
 Whether it actually happened like that or not, I'm not sure
.
 But that's from the head of Chiang Mai province.
 Anyway, even then, he was set up as the head of a monastery
 in Chiang Mai.
 Even then, he quickly moved to the forest.
 At some point he moved to the forest, found this ruined
 monastery that had been destroyed
 by Japanese bombs during World War II.
 It was out of the city, kind of in the forests surrounding
 Chiang Mai.
 He thought, well this would be a nice place to set up the
 meditation center, I guess.
 And undertook to rebuild this forest monastery.
 That monastery now is one of the, probably the biggest,
 well I think,
 indisputably the biggest meditation center in Chiang Mai.
 I can't think of any that are near that size.
 Wat Lam Phu.
 Eventually he got quite famous and the bureaucracy in
 Thailand
 decided to move him to a royal monastery, which is this
 monastery here in Chantong,
 which is just a dinky little monastery right next to the
 highway.
 So I think that kind of disappointed some of his followers.
 But not at Chantong.
 We're headed into what we see now, which is this sprawling
 megalithic meditation center
 that just gets bigger and bigger and bigger.
 But here I am in a forest that I assume he had planted.
 I'm not sure actually who gave the call on this, but I'm
 assuming it had something to do with him.
 Here in a, well one day it would be a forest. Right now the
 trees are fairly small,
 but you can still find shade under many of them.
 So I've found a refuge here in the craziness of this
 monastery.
 This is interesting because this morning I want to talk
 about refuges, taking refuge.
 My father sent me a link to a book called, I don't know if
 I should be calling out names of books,
 but it's about this climate change catastrophe that we're
 apparently on the cusp of.
 It's coming, it's just a matter of how severe it's going to
 be.
 Of course we knew this many years ago when I was in high
 school, we were talking about it.
 Nobody ever really did anything and now it's apparently too
 late.
 Or it's only a matter of, the only thing we can do now is
 mitigate.
 We can't stop the coming floods and craziness of weather.
 So the future in some ways looks kind of scary.
 And there's danger on the horizon.
 And when there's danger, of course everyone looks for a
 refuge.
 The Buddha himself talked quite often about refuges.
 The word that we usually translate as refuges is sattarna,
 but I'm not a Pali expert.
 But sarna has two general meanings. One is refuge and one
 is object of reflection.
 It actually comes from the same root as sati.
 So this word that we translate as mindfulness.
 Sarana, sarnati is the verb. Sati and sarna are two
 different forms of the noun.
 And the grammars call them synonymous. Saranangwa sati.
 One meaning of sati is sarna.
 It's really a different form of the same word, so it
 probably had different connotations.
 But the more literal word that would be translated as refug
es is natta.
 So we take the Buddha as our natta as well.
 The common theme my teacher once, he actually, he was
 giving a talk once
 and he was explaining how when we go traveling we should
 have this mantra in our minds.
 Buddho meinato, damo meinato, sangho meinato.
 The Buddha is my refuge, the dhamma is my refuge, the san
gha is my refuge.
 And then he turned to me, and this was back when I was just
 learning Thai,
 and I was smiling because I just smile whenever he talks.
 And he looked at me and he said, "You understand?"
 He said, "Yes, sir."
 And he said, "Then go, tell all the foreigners, okay? Tell
 all the foreigners as well."
 So I was telling you, when you go traveling, say to
 yourself,
 "Buddho meinato, the Buddha is my refuge."
 Natta is this word.
 And I think that would have been the talk where he went on
 to explain what are the refuges of Buddhism.
 The Buddha said, "Mabhikawe natta bhikawe satnata bhikawe v
iharata maa natta."
 Dwell with a refuge, O monks, not without a refuge.
 Dukho bhikawe an natto viharati.
 One without a refuge dwells in suffering.
 One without a refuge is subject to the vicissitudes of life
.
 So finding a refuge that is impervious to the vicissitudes
 of life, this is important.
 So the obvious refuge is, as I've mentioned before, is to
 become invincible.
 I mean, impervious or mindfulness is the most obvious
 refuge, but there's more.
 So actually, in order to give a comprehensive, practical
 instruction,
 and to flesh out what it means to live a mindful life,
 he gave ten dhammas that lead to the creation of a refuge,
 that make a refuge for oneself.
 They're called the natta karana dhammas.
 The dhammas that provide refuge.
 So they're directed, especially towards monks, but useful
 for everyone to know.
 These are things that we can keep in mind in our lives,
 words of the Buddha, on how to live your life,
 so that you don't get mulled over, especially when there
 are catastrophes looming on the horizon.
 The first one is sī laua, sī lānga.
 One is sī laua, one who has sī lā, has morality.
 So the first part of our refuge is to be moral, is to live
 according to ethical rules of conduct,
 so that one is not blamed or blameworthy, and so that one's
 mind is clear,
 so that one doesn't waver in the face of adversity.
 One stands strong and confident in the knowledge that one
 is a moral and ethical individual,
 to strive not to do or to say or to think,
 at the very least not to do or to say things that are
 unethical, things that hurt others,
 will hurt ourselves.
 The second nātaka-rṇadāma is bahūsūta, to be learned.
 Learning doesn't mean broadly learned in worldly things,
 like reading books on climate change.
 I don't think that was what the Buddha had in mind.
 The meaning here is to be well-learned in wise things.
 Of course, we're biased when we say Buddhist things.
 We believe that all the wisdom you need is encompassed in
 the Buddha's teaching,
 but by all means, if you find wisdom elsewhere, it's still
 wisdom, no question.
 Even the Buddha didn't deny that.
 But when studying the Buddha's teaching, the advantage is
 you can be reasonably sure
 that it's going to be free from misleading ideas, concepts,
 more so than reading, say, Plato or the European
 philosophers.
 Studying the Buddha's teaching is a refuge because it helps
 you deal quickly
 with a broad range of situations.
 It gives you a shortcut.
 Rather than having to learn all of these things by yourself
,
 having studied the Buddha's teaching, though it certainly
 isn't a replacement for good practice,
 it gives you a shortcut to practice.
 So rather than having to make mistakes and figure out all
 the right ways by yourself,
 it's like an added artificial framework.
 It's like a draw, an outline of your structure before you
 build.
 When you have that structure, then you can build your
 structure much easier
 than having to build it without the guide.
 Having a guide to life, having a guide to practice.
 As long as you don't use it as a supplement, as a
 replacement to actual practice,
 it can be quite a good support.
 The problem is if people learn too much, then they think
 they know everything
 and then they don't actually practice.
 They mistake the outline, the guideline for the actual
 structure.
 That's the problem, not learning.
 Learning is useful.
 As long as you can then replace the learning with actual
 knowledge,
 with actual wisdom and insight, that's how you have to look
 at it.
 But it prevents you from having to make the mistakes that
 others have already made before you.
 When you listen, suta actually means listening.
 It wouldn't have been reading.
 The word they used was in regards to listening.
 Just listening to teachings is also a good way of
 practicing.
 This is another good reason for having these talks.
 It's a means of, even as you're listening, cultivating
 wholesomeness.
 As you listen to this talk, you can settle your mind in
 good thoughts,
 settle your mind on the right path,
 remind yourself of good things.
 Actually, it's a good supplement for you.
 It's a great supplement for practice.
 Bhagusutta makes a refuge.
 It reminds you to stay on track when you're around people
 who speak the Dhamma all the time.
 It's a constant reminder to do and say and think good
 things.
 Which brings us to number three, which is kalyanamitata.
 Kalyanamitohoti kalyanasahayo.
 One has good friends.
 Kalyanam means beautiful.
 We usually translate as beautiful or just good.
 And mitta.
 Mitta is where the word metta comes from.
 Mitta comes from the word mitta.
 Mitta means friend.
 Mitta means friendship.
 We usually translate it as love or loving-kindness,
 but it actually means friendliness.
 Yeah, friendliness is the word metta.
 Interesting.
 Kalyanamitta means having good friends or being a good
 friend.
 It's not clear whether this means one should be a good
 friend
 or one should have good friends, but both are equally
 important.
 You need to be a good friend to have good friends.
 Being a good friend is important because it surrounds you
 with good people.
 Being someone who good people like to associate with is a
 refuge.
 So when things get bad,
 one of the greatest refuges are our friends.
 And if the world is full of friendliness,
 then really we should be able to...
 Well, we do much better than if we're full of animosity and
 divisiveness, hatred.
 We can't stop certain things from happening,
 and many people will suffer.
 But that suffering is going to be...
 The level of suffering will always be affected by our
 relationships with each other.
 Our relationships as human beings, interactions as a human
 race.
 So are our friends beautiful or are our friends ugly?
 We're surrounding ourselves with people who are going to
 betray us
 and going to abandon us when the going gets rough
 or can we expect?
 Do our friends really love and appreciate us?
 Are we surrounded by people who we can trust?
 Being a good friend, helping others, and so on.
 Having good friends is a very important refuge.
 Next one, Suvajasata.
 Suv means in this case, easy.
 Suv usually means good, but in this case it means easy.
 Vajja just means speech.
 Vajja and then vajjo means one in regards to whom speech is
 easy.
 One who is easy to talk to.
 It means one who is easy to criticize, to admonish.
 One who is easy to talk to.
 I think in general that's how it should be understood.
 Easy to talk to can mean many different things.
 So when you have problems, someone who is easy to talk to.
 But specifically here I think the implication is in regards
 to
 one who is malleable, one who isn't stubborn and hard to
 instruct.
 Because the great thing about good friends is that they
 will help you progress
 if you let them.
 They will call you out on your faults, on your mistakes, on
 your weaknesses,
 and encourage you to improve and become a better person.
 A good friend won't just flatter you and ignore your
 shortcomings.
 And so an important quality that is a refuge in life,
 is the ability to take criticism.
 The Buddha said we should see someone who points criticism
 out to us
 as someone who has pointed out hidden treasure.
 Now this is as if the problem you often find is people
 always turn this one around
 and say, "Well in that case I should be able to admonish
 anyone I please."
 But the key here is good friends.
 You shouldn't just go up to anyone on the street and start
 criticizing them
 or people you don't know or people who you've just met,
 people who you have no reason to expect that level of
 intimacy.
 You have to know whether people can take it.
 But we ourselves should take it.
 Take it from anyone. The Buddha said sometimes people will
 come to you
 at the wrong time with harsh speech, with useless speech,
 with mean speech.
 And you should take it all.
 This is something we should keep in mind at all times,
 that we can't expect people to only come to us with nice
 things to say,
 that people will often come to us with mean things to say.
 So even if we feel that the criticism is unwelcome,
 well first of all we should reflect in ourselves.
 The first thing we should do when someone criticizes us,
 the obvious thing is to really ask ourselves whether that
 criticism is found in us.
 Because no matter how awful the person criticizing us is,
 they usually will point out something that they think is
 wrong with us.
 Even if they're trying to find fault, they'll find the ch
ink in our armor,
 they'll try to find our weakness.
 So we should reflect not in such a way that
 lessens our own sense of confidence,
 that we should feel happy that this person has pointed out
 this thing to us.
 It actually is an advantage that someone has pointed this
 out to us,
 as it is to our advantage.
 We actually become a better person, potentially a special
 person,
 because we have seen our shortcomings.
 This is how you deal with shortcomings in Buddhism, is to
 see them.
 So if someone points it out to you, you should feel blessed
.
 You'll be glad.
 It's the first thing.
 First thing to do when someone talks to you,
 this is how you become suvajja.
 And this is a refuge, because if you don't have that,
 you go through life, ignoring your faults, hiding them,
 showing one personality when your inner personality is
 totally different,
 hiding away your faults.
 And then when the catastrophe comes, when problems arise,
 then we see that everyone shows their true colors in
 adversity,
 and we'll be totally unprepared because we haven't admitted
 to ourselves,
 we've been lying to ourselves and lying to everyone else,
 pretending that we weren't in trouble.
 So for this reason, suvajja, also because it leads to good
 friendship.
 Number five, I believe,
 one, two, three.
 Number five is
 김가는에 수, 다다, 다고호리.
 김가는에, 다다, 다고호리.
 안나서.
 Whatever 가는에, whatever work,
 사부마자리날, whatever work our fellow meditators
 or fellows in the holy life, this one actually is most
 applicable to monks,
 but you can of course extend it to make people.
 김가는에, whatever work they have,
 whatever burdens they may carry,
 to be energetic, 다가.
 다가 means to help them with their work,
 help them with their activities.
 To see someone sweeping in the monastery,
 it's a good reason to go and help them.
 In the Buddha's time they would not speak to each other for
 days on end,
 but then when they saw someone was struggling with
 something,
 like when one of them is struggling to move a water pot,
 rather than talk he would just wave his hand
 and call the other one over with just his hand and they
 would move it together.
 It doesn't mean getting involved, getting caught up with
 other people,
 but it means helping.
 Helping the Buddhist religion, helping meditation centers,
 going to meditation centers and cleaning toilets.
 In Thailand that's a big thing, people would just come to
 the monasteries
 and clean the toilets.
 It's a great merit, a great merit to clean toilets.
 This is something no one wants to do.
 It's a great support and because it leads to cleanliness,
 it leads you to feel the benefits of having brought clean
liness to others.
 It brought comfort and convenience to others.
 Going to the monastery, some people act as chauffeurs for
 the monks
 or do whatever things they can for the monastery or a
 meditation center.
 These sorts of things of course are, they make or break an
 organization,
 the help and the teamwork.
 I have an organization and I have a few good people who
 help organize things.
 We have a treasurer and a couple of other people who are
 helping to run errands.
 Then in the monastery where I live in Canada, we help each
 other
 when we need to.
 When the head monk has work, I help him and I need a ride
 somewhere.
 He drives, so he drives me places.
 I write letters for him and he signs them and that kind of
 thing.
 This is how you work together.
 This is what makes or breaks an organization.
 You can see that, the power in teamwork.
 When you put one and one, it's always more than two.
 One plus one is always more than two.
 Two heads are not only better than one, they're better than
 two.
 Two heads working together are better than two heads
 working apart.
 This is a great refuge, working together.
 You get more than just numbers.
 Same goes with meditation.
 We have a lot of us together here on this site.
 I think we all agree that this has been widely successful.
 Not just making more people meditate, but encouraging those
 of us who do meditate, to meditate more.
 And increasing our sense of confidence and encouragement in
 the meditation by coming together in this way.
 Teamwork.
 Number five. Number six is dhammakamu.
 Dhammakamu.
 Kama is a funny word because kama normally means lust or
 desire.
 This is one of the instances where the Buddha used the word
 desire in a positive way.
 Dhammakamu.
 Dhammakamu Bhavanghoti, the Buddha says also.
 One who has kama in regards to the dhammam is bhavang.
 Bhavang means developed.
 It's high, it's exalted, it's sophisticated.
 [
 The Buddha is a very important person in the dhammam.]
 The one who loves the dhamma, who is keen to practice the d
hamma,
 and one who feels a certain appreciation really for the d
hamma, this magnetic attraction to the dhamma,
 the one who is attracted to it.
 Meaning when they hear the dhamma, their mind leaps up and
 inclines towards the dhamma,
 inclines towards practicing the dhamma, bringing it to
 fulfillment.
 This is a huge refuge for us.
 And it's a practice in itself because dhamma means truth,
 and if you're not inclined towards the truth, then that's a
 practice to see what's wrong,
 what's going on in your mind that disinclines you to the
 practice.
 What is it that makes you disincline to meditate?
 What is it that makes meditation seem like a chore?
 What is it that makes the teachings of the Buddha seem
 boring or dull or interesting?
 A meditator can see in their mind, because they know the d
hamma is good,
 but they can see in their mind these stubborn, persistent,
 pernicious inclinations
 to become dissatisfied with goodness, dissatisfied with
 peace, dissatisfied with right,
 with what is true.
 So it's a refuge that has to be cultivated.
 It doesn't mean you have to pretend, and it would be very
 wrong to just force yourself to like it
 and say like it, or feel guilty thinking that there's
 something wrong with you.
 What is it that can be taken as a practice? What is it
 about me that makes me want to incline towards unwholesomen
ess?
 It's a scientific study. You shouldn't feel guilty that you
're inclined towards wholesomeness.
 That doesn't help. It actually makes it worse.
 You should study yourself. Why am I...
 It's not really I, but what is this function, what is this
 process, this habit
 that is inclined towards unwholesomeness?
 Because really that's a danger.
 That's a danger for the future, not just this life, but
 when we pass away,
 it will be a danger in the next life as well.
 Number seven, aradha, vihirati.
 Aku salanam damanam bahana yah, ku salanam damanam bahasabh
ata yah.
 Aradha means established, I think, or strong, firm.
 It comes from arabati, which means started or established.
 Virya means energy or effort. So one with strong effort of
 steadfast exertion.
 One who works hard. This is a refuge.
 This is again something you have to cultivate, because many
 of us are very lazy.
 We've become quite lazy in this modern times.
 It doesn't mean you have to push yourself, but you have to
 cultivate.
 And it's interesting how actually wisdom leads to energy in
 some ways.
 Mindfulness leads to energy, leads to effort.
 It should come naturally by itself.
 Observing laziness actually leads to effort.
 We think of laziness as a deficiency when in fact it's an
 addition.
 If you take away the laziness, you enter into a natural
 state of effort and energy.
 That is firm, that is aradha, that is unshakable.
 Because if you just push and push and push, like we do in
 physical sports,
 then you're just creating a new formation.
 Like people who work out their bodies, eventually they find
 it useless
 because they still get old, sick and die.
 The energy doesn't last.
 It only lasts so long as you push.
 So the Buddha said the energy that we need is in regards to
 giving up wholesome,
 giving up unwholesome dhammas and cultivating wholesome d
hammas.
 [Hindi]
 So we need effort.
 Number eight is santutthoti.
 Santutthi means contentment.
 Tutti means happiness or delight.
 San means with what you've got.
 San tutti means being happy with what you've got,
 not needing being happy no matter what you've got.
 So it's a conventional word.
 In conventional terms it means contentment, contentment
 with your lot in life.
 It doesn't mean contentment in regards to your attainments.
 It doesn't mean you should be content to wander through
 life
 than average ordinary human being without any spiritual
 attainments whatsoever.
 It doesn't mean you should stop striving for enlightenment.
 But it means you should be content with experience, content
 with material wealth.
 Contentment is a great refuge because it prepares you for
 the future.
 In a conventional sense, if things go bad in the future and
 society crumbles or whatever,
 if the economy gets worse, if many of us lose our
 livelihood,
 contentment is what's going to divide us.
 Those who are unable to be content with little will suffer
 the most.
 Already it's happened frequently in the world
 where people have to go hungry and put up with hardship,
 sickness, death.
 And if you can't be content with whatever comes,
 then you'll be the hardest hit, the harder hit.
 It will make the experience even worse.
 That's what will lead to suffering.
 Number nine is sati mā hoti, parameena sati nikke pne pak
ke nā.
 Sati mā means one should have mindfulness or sati,
 remembrance.
 One should remember oneself, not forget oneself.
 This is of course the greatest refuge.
 This is the most important one.
 It leads to all other dhammas.
 And when this has sati, it's the beginning.
 It's like the key that starts the engine.
 So one's ability to remember, not just remember the past,
 although remembering the past can be useful.
 It's useful as a refuge.
 The conventional sati means to remember things that have
 happened in the past.
 Which is of course a refuge because it means you don't
 repeat your mistakes.
 Having a good memory is a refuge.
 And it's the same sort of quality of mind that we use in
 meditation.
 The ability to remember where we are and what we're doing
 is similar to this ability to remember things that happened
 long ago.
 But much more immediately useful in meditation.
 And number ten is wisdom.
 The final nāta karana dhamma.
 A dhamma that leads to a refuge is that we should be wired.
 Because it's wisdom that allows us to deal with any
 situation.
 The difference between acting appropriately and acting
 inappropriately
 is always going to come down to wisdom.
 So whether our wisdom comes from learning or whether it
 comes from experience,
 comes from meditation practice,
 it is our wisdom that is going to save us in all situations
.
 So sūta maya panya, wisdom that comes from learning, is
 useful.
 But as I was saying yesterday,
 you can't possibly learn how to act in every situation.
 There's no book that's going to tell you what to do in
 every situation.
 And even if there were, it would be a pale imitation
 of the true wisdom that's born of experience,
 especially from meditation practice,
 which allows you to understand the very building blocks of
 the situation
 from an experiential point of view,
 allowing you to experience things and interact with them,
 rather than react to them and blow them out of proportion
 and project your own ideas about them and so on.
 Wisdom, wisdom is like the moon
 and all other virtues are like the stars.
 The moon always shines brighter.
 Alright, so that was the Dhamma for this morning.
 Just something to inspire and hopefully inspire and
 encourage.
 Thanks for tuning in.
 And thanks for joining in our meditation.
 Be well.
