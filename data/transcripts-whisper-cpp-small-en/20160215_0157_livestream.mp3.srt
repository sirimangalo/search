1
00:00:00,000 --> 00:00:22,000
 [silence]

2
00:00:22,000 --> 00:00:30,150
 Hi, good evening everyone. I'm broadcasting live February

3
00:00:30,150 --> 00:00:44,000
 14th, which is an important day.

4
00:00:44,000 --> 00:00:52,000
 I'll tell you about today. Today is a very important day.

5
00:00:52,000 --> 00:00:54,000
 No, it's not a very important day.

6
00:00:54,000 --> 00:01:02,000
 It is a day that has meaning for people.

7
00:01:02,000 --> 00:01:15,190
 Something to do with violence and torture and martyrdom.

8
00:01:15,190 --> 00:01:20,000
 Historical facts.

9
00:01:20,000 --> 00:01:30,830
 Valentine of Rome was a priest who was martyred and added

10
00:01:30,830 --> 00:01:36,000
 to the St. Condor saints.

11
00:01:36,000 --> 00:01:48,000
 Martyrs. So they were persecuted.

12
00:01:48,000 --> 00:01:53,720
 Today is where we celebrate a Christian martyr or a bunch

13
00:01:53,720 --> 00:01:56,000
 of Christian martyrs.

14
00:01:56,000 --> 00:01:59,620
 There's a priest who was martyred. The flower crown skull

15
00:01:59,620 --> 00:02:06,000
 of St. Valentine is exhibited in the basilica.

16
00:02:06,000 --> 00:02:11,410
 There's a reason why I'm telling this. There's St.

17
00:02:11,410 --> 00:02:14,000
 Valentine.

18
00:02:14,000 --> 00:02:24,000
 All that is reliably known is he was martyred and buried.

19
00:02:24,000 --> 00:02:36,000
 So little is known of him.

20
00:02:36,000 --> 00:02:43,680
 I was hoping to get some juicy details because it's a story

21
00:02:43,680 --> 00:02:49,460
 of hate and prejudice and bigotry and arrogance and

22
00:02:49,460 --> 00:02:56,000
 clinging to wrong views and so on.

23
00:02:56,000 --> 00:03:07,110
 That's what today is about. How humans, it's a testament to

24
00:03:07,110 --> 00:03:09,100
 how awful and terrible human beings can be. That's what

25
00:03:09,100 --> 00:03:10,000
 today is all about.

26
00:03:10,000 --> 00:03:16,000
 And lo and behold, today's quote, our Valentine's Day quote

27
00:03:16,000 --> 00:03:16,000
,

28
00:03:16,000 --> 00:03:19,630
 has the Buddha saying that if animals can be courteous,

29
00:03:19,630 --> 00:03:23,430
 differential and polite to each other, then so should you

30
00:03:23,430 --> 00:03:30,420
 be. Basically, if animals can behave themselves, why the

31
00:03:30,420 --> 00:03:43,000
 heck can't you be?

32
00:03:43,000 --> 00:03:49,000
 They can dwell with respect, Sagarova, respectful.

33
00:03:49,000 --> 00:04:09,000
 What the heck is Sagpattisa?

34
00:04:09,000 --> 00:04:16,000
 Humble, maybe. Differential, yeah.

35
00:04:16,000 --> 00:04:21,000
 Sambhagavatika.

36
00:04:21,000 --> 00:04:28,000
 Having the practice of getting along.

37
00:04:28,000 --> 00:04:35,000
 Living in mutual courtesy. Animals can get along.

38
00:04:35,000 --> 00:04:38,000
 Today's telling the monks, why can't you guys get along?

39
00:04:38,000 --> 00:04:41,000
 Why can't you monks get along?

40
00:04:41,000 --> 00:04:47,580
 Apparently in Thailand these days, I just heard of bizarre

41
00:04:47,580 --> 00:04:52,000
 news this morning. All very apropos.

42
00:04:52,000 --> 00:04:57,010
 Apparently the monk who was in line to be the Sangaraja,

43
00:04:57,010 --> 00:04:59,890
 the king of the monks, this is what I was saying in English

44
00:04:59,890 --> 00:05:07,000
, the supreme patriarch, committed suicide.

45
00:05:07,000 --> 00:05:18,760
 Now I think I'm not the only person to wonder how it

46
00:05:18,760 --> 00:05:26,000
 happens that someone who is high up in the monastic order

47
00:05:26,000 --> 00:05:30,810
 and in line to become the most powerful monk in Thailand

48
00:05:30,810 --> 00:05:35,000
 suddenly commit suicide.

49
00:05:35,000 --> 00:05:39,340
 And I think a lot of people are, I mean I don't actually

50
00:05:39,340 --> 00:05:49,000
 know this, I'm looking it up, but...

51
00:05:49,000 --> 00:06:01,000
 Bizarre.

52
00:06:01,000 --> 00:06:09,100
 What I mean to say is a lot of people think he didn't kill

53
00:06:09,100 --> 00:06:11,000
 himself.

54
00:06:11,000 --> 00:06:16,440
 I don't see any quotes of it, maybe it's not true. But that

55
00:06:16,440 --> 00:06:19,000
's what I was told this morning by a Thai woman.

56
00:06:19,000 --> 00:06:23,310
 Visited to offer us food this morning. I asked her, how's

57
00:06:23,310 --> 00:06:27,910
 it going in Thailand? Our students started talking about

58
00:06:27,910 --> 00:06:29,000
 Thailand.

59
00:06:29,000 --> 00:06:34,880
 Yeah, there's a big uproar. Monks can't get along, to say

60
00:06:34,880 --> 00:06:36,000
 the least.

61
00:06:36,000 --> 00:06:40,700
 There's an old story of the monk who was responsible, I've

62
00:06:40,700 --> 00:06:46,340
 talked about this before, who was responsible for bringing

63
00:06:46,340 --> 00:06:49,350
 insight meditation, the meditation that we practice into

64
00:06:49,350 --> 00:06:50,000
 Thailand,

65
00:06:50,000 --> 00:06:55,530
 which is now practiced by, it's now taught in the monastic

66
00:06:55,530 --> 00:07:01,720
 universities, the big insight practice in Thailand, sort of

67
00:07:01,720 --> 00:07:05,000
 the establishment practice.

68
00:07:05,000 --> 00:07:11,060
 He's responsible for bringing insight, sorry, Abhidhamma

69
00:07:11,060 --> 00:07:13,000
 studies as well.

70
00:07:13,000 --> 00:07:18,100
 He was very big on working with other countries, bringing

71
00:07:18,100 --> 00:07:22,410
 together monks from all the different countries, and

72
00:07:22,410 --> 00:07:26,000
 learning from other countries.

73
00:07:26,000 --> 00:07:33,840
 And monks were critical of him saying, why are we bringing

74
00:07:33,840 --> 00:07:41,520
 Buddhism from other countries, isn't our Buddhism good

75
00:07:41,520 --> 00:07:43,000
 enough?

76
00:07:43,000 --> 00:07:47,810
 Criticizing him like as though he was putting down Thai

77
00:07:47,810 --> 00:07:54,680
 Buddhism. And he boldly replied and said, forget about the

78
00:07:54,680 --> 00:08:01,000
 Abhidhamma, we don't even teach sutta in Thailand.

79
00:08:01,000 --> 00:08:04,000
 Anyone who knows anything about Thai Buddhism can tell you.

80
00:08:04,000 --> 00:08:09,120
 You can get to the highest level of Pali and Dhamma studies

81
00:08:09,120 --> 00:08:12,000
 that there is in Thailand today.

82
00:08:12,000 --> 00:08:17,590
 And never have read the Dhabitika. You can get to the level

83
00:08:17,590 --> 00:08:21,740
 9 Pali studies without ever having translated anything

84
00:08:21,740 --> 00:08:27,000
 besides the Dhammapada verses and commentary and other

85
00:08:27,000 --> 00:08:28,000
 commentaries.

86
00:08:28,000 --> 00:08:32,480
 So you'll never have translated directly an actual Dhabit

87
00:08:32,480 --> 00:08:35,000
ika, which isn't really a problem when you're learning Pali,

88
00:08:35,000 --> 00:08:38,490
 but these people are considered to be the top Buddhist

89
00:08:38,490 --> 00:08:39,000
 scholars,

90
00:08:39,000 --> 00:08:43,020
 and they've never had any real instruction in the Buddhist

91
00:08:43,020 --> 00:08:44,000
 teaching.

92
00:08:44,000 --> 00:08:47,160
 There are three levels of Dhamma study, and as far as I

93
00:08:47,160 --> 00:08:49,000
 know, even the third level,

94
00:08:49,000 --> 00:08:53,490
 well, it's three years, it's really, really basic. But that

95
00:08:53,490 --> 00:08:56,380
's it. You get to the third level and you're supposed to be

96
00:08:56,380 --> 00:08:58,000
 an accomplished teacher.

97
00:08:58,000 --> 00:09:03,040
 As far as I know, you don't ever have to have read anything

98
00:09:03,040 --> 00:09:08,000
 from the actual Dhabitika, the Buddhist teachings.

99
00:09:08,000 --> 00:09:13,820
 I don't remember where I was going with this. Totally lost

100
00:09:13,820 --> 00:09:17,000
 my train of thought.

101
00:09:17,000 --> 00:09:21,430
 Right. The monk. Anyway, long story short, he was

102
00:09:21,430 --> 00:09:25,490
 eventually put in. He was eventually accused. It was really

103
00:09:25,490 --> 00:09:26,000
 terrible.

104
00:09:26,000 --> 00:09:32,560
 He was the next in line to become the Sangharaja. And

105
00:09:32,560 --> 00:09:39,000
 suddenly he's being accused by some anonymous woman who was

106
00:09:39,000 --> 00:09:42,000
 interviewed by the police and by the Sangha council,

107
00:09:42,000 --> 00:09:47,430
 the Master Council, who says he raped her. And you believe

108
00:09:47,430 --> 00:09:53,530
 it. Suddenly, to have these detractors, this woman giving a

109
00:09:53,530 --> 00:09:55,000
 signed statement,

110
00:09:55,000 --> 00:09:58,680
 that he in fact, actually I don't think she was, it was

111
00:09:58,680 --> 00:10:01,000
 probably signed, but yeah.

112
00:10:01,000 --> 00:10:07,770
 At any rate, she was interviewed and they accused him of

113
00:10:07,770 --> 00:10:13,000
 raping this woman and said he should disrobe.

114
00:10:13,000 --> 00:10:15,820
 And he replied, you know, and the Sangharaja, the king of

115
00:10:15,820 --> 00:10:19,180
 the monks at the time, said he should disrobe, told him he

116
00:10:19,180 --> 00:10:21,000
 should disrobe.

117
00:10:21,000 --> 00:10:25,690
 And in Thailand it's not disrobe, but it's stop being a

118
00:10:25,690 --> 00:10:30,000
 monk, like quit, he should quit being a monk.

119
00:10:30,000 --> 00:10:32,450
 And he replied by saying, you know, if I had actually done

120
00:10:32,450 --> 00:10:35,000
 that, I would no longer be a monk. It was really absurd.

121
00:10:35,000 --> 00:10:43,000
 They were really just trying to get rid of him. And they,

122
00:10:43,000 --> 00:10:48,520
 huge smear campaign, everyone denouncing this monk, really

123
00:10:48,520 --> 00:10:50,000
 done awesome things.

124
00:10:50,000 --> 00:10:56,690
 For Buddhism. And finally, somehow, they found this woman,

125
00:10:56,690 --> 00:11:04,150
 talked to her, and she came forward and retracted the

126
00:11:04,150 --> 00:11:05,000
 entire thing

127
00:11:05,000 --> 00:11:08,280
 and wrote this letter that just drove me to tears when I

128
00:11:08,280 --> 00:11:14,000
 read it. I was shivering, just imagining her state of mind.

129
00:11:14,000 --> 00:11:18,640
 She said, I just want to sleep at night. I can't live with

130
00:11:18,640 --> 00:11:22,000
 what I did. I don't want to go to hell.

131
00:11:22,000 --> 00:11:25,290
 She was really afraid. I mean, that was just, can you

132
00:11:25,290 --> 00:11:28,000
 imagine? She said it was not true.

133
00:11:28,000 --> 00:11:32,010
 I don't, I've never, you know, been in close contact with

134
00:11:32,010 --> 00:11:35,340
 that monk. I just want to be able to sleep at night. I don

135
00:11:35,340 --> 00:11:37,000
't want to go to hell.

136
00:11:37,000 --> 00:11:40,570
 So that died down. And then later on, they tried to pin him

137
00:11:40,570 --> 00:11:44,000
 as a communist when it was a communist scare in Thailand.

138
00:11:44,000 --> 00:11:48,510
 And eventually they put him in jail for being a communist

139
00:11:48,510 --> 00:11:50,000
 without trial.

140
00:11:50,000 --> 00:11:53,760
 And he spent three years or something translating the Wisud

141
00:11:53,760 --> 00:11:57,740
imanga into Thai, which is now the standard Thai translation

142
00:11:57,740 --> 00:11:58,000
.

143
00:11:58,000 --> 00:12:01,090
 People asked him about what it was like being in jail. He

144
00:12:01,090 --> 00:12:04,000
 said, it was great. People brought me food every day.

145
00:12:04,000 --> 00:12:08,130
 I had guards guarding me 24/7. Didn't have to worry about

146
00:12:08,130 --> 00:12:10,380
 anything. Didn't have to act as a head monk of the

147
00:12:10,380 --> 00:12:11,000
 monastery.

148
00:12:11,000 --> 00:12:15,800
 He was the head of Wat Mahatat at the time, one of the big,

149
00:12:15,800 --> 00:12:19,000
 the big monastery back in the day.

150
00:12:19,000 --> 00:12:22,380
 That's where insight meditation in Thailand became a big

151
00:12:22,380 --> 00:12:23,000
 thing.

152
00:12:23,000 --> 00:12:26,210
 And that'd be done with studies in the first monastic

153
00:12:26,210 --> 00:12:30,000
 university in Thailand. All under him.

154
00:12:30,000 --> 00:12:34,240
 Then he got out and they actually put him on trial and he

155
00:12:34,240 --> 00:12:37,000
 was exonerated of all wrongdoing.

156
00:12:37,000 --> 00:12:39,900
 And they had some really hard words. The judges had some

157
00:12:39,900 --> 00:12:44,000
 really strong wording for the people who had accused him.

158
00:12:44,000 --> 00:12:48,910
 And then eventually he was reinstated. So during this time

159
00:12:48,910 --> 00:12:51,730
 when he was put in jail, of course, he was removed of his

160
00:12:51,730 --> 00:12:52,000
 rank.

161
00:12:52,000 --> 00:12:55,090
 And they actually forcibly disrobed him and made him wear

162
00:12:55,090 --> 00:12:56,000
 white robes.

163
00:12:56,000 --> 00:12:58,890
 And he came back and started wearing robes again and they

164
00:12:58,890 --> 00:13:01,000
 said, well look, you're no longer a monk.

165
00:13:01,000 --> 00:13:04,480
 And he said, that's not true. You can't disrobe someone

166
00:13:04,480 --> 00:13:07,830
 else. The only way to disrobe is in front of one monk in

167
00:13:07,830 --> 00:13:09,000
 front of another.

168
00:13:09,000 --> 00:13:12,640
 It was really interesting, this whole, there's a book about

169
00:13:12,640 --> 00:13:16,000
 it. It's a really fascinating read.

170
00:13:16,000 --> 00:13:20,000
 But it's a good example of how people just can't get along.

171
00:13:20,000 --> 00:13:25,810
 Human beings can be worse than animals. It's not that

172
00:13:25,810 --> 00:13:28,000
 animals are better than humans.

173
00:13:28,000 --> 00:13:31,860
 It's just that animals are stupider than humans. They don't

174
00:13:31,860 --> 00:13:34,000
 have the intelligence.

175
00:13:34,000 --> 00:13:37,560
 They aren't capable of the great good and the great evil

176
00:13:37,560 --> 00:13:40,000
 that human beings are capable of.

177
00:13:40,000 --> 00:13:45,510
 Which makes you wonder whether that's why Mara is actually

178
00:13:45,510 --> 00:13:51,000
 an angel, a deva, considered to be in the deva realms.

179
00:13:51,000 --> 00:13:56,000
 Wonder whether angels are capable of even more evil.

180
00:13:56,000 --> 00:14:00,000
 But Mara is actually an interesting case.

181
00:14:00,000 --> 00:14:03,090
 Because it doesn't sound like Mara actually does, actually

182
00:14:03,090 --> 00:14:04,000
 hurts people.

183
00:14:04,000 --> 00:14:09,370
 He just convinces people to stay in samsara. He intoxicates

184
00:14:09,370 --> 00:14:12,540
 them with sensual pleasure, tricks them into being

185
00:14:12,540 --> 00:14:14,000
 intoxicated.

186
00:14:14,000 --> 00:14:18,230
 I don't know, there's lots of stories about this type of

187
00:14:18,230 --> 00:14:19,000
 angel.

188
00:14:19,000 --> 00:14:24,000
 But about human beings, human beings can be terrible.

189
00:14:24,000 --> 00:14:31,160
 We are too intelligent. We let our intelligence guide us

190
00:14:31,160 --> 00:14:34,000
 rather than wisdom.

191
00:14:34,000 --> 00:14:44,000
 Intelligence is a weapon. But wisdom is not a tool.

192
00:14:44,000 --> 00:14:49,500
 Wisdom is only useful for good. Wisdom cannot make you do

193
00:14:49,500 --> 00:14:50,000
 evil.

194
00:14:50,000 --> 00:14:55,710
 Intelligence can very much allow you and support you in

195
00:14:55,710 --> 00:14:58,000
 cultivating evil.

196
00:14:58,000 --> 00:15:04,000
 The intellect would be a great tool for evil people.

197
00:15:04,000 --> 00:15:09,000
 It can be very clever.

198
00:15:09,000 --> 00:15:17,200
 And information, logic, reason, all these things will help

199
00:15:17,200 --> 00:15:19,000
 you, can help you.

200
00:15:19,000 --> 00:15:21,270
 This is why they say the road to hell is paved with good

201
00:15:21,270 --> 00:15:22,000
 intentions.

202
00:15:22,000 --> 00:15:26,360
 Part of that is that you can convince yourself that you're

203
00:15:26,360 --> 00:15:29,000
 doing the right thing and be doing the wrong thing.

204
00:15:29,000 --> 00:15:32,400
 Think of the Spanish Inquisition or these people who killed

205
00:15:32,400 --> 00:15:34,000
 the Christian martyrs.

206
00:15:34,000 --> 00:15:37,000
 I don't know the history, I mean.

207
00:15:37,000 --> 00:15:43,000
 But I understand they were persecuted and killed.

208
00:15:43,000 --> 00:15:57,000
 And this was all certainly justified.

209
00:15:57,000 --> 00:16:01,000
 People had logical arguments as to why the earth was flat.

210
00:16:01,000 --> 00:16:07,750
 That argument says to the earth being the center of the

211
00:16:07,750 --> 00:16:14,000
 universe, the sun orbiting around the earth.

212
00:16:14,000 --> 00:16:18,000
 So there's something else that's needed and that's wisdom.

213
00:16:18,000 --> 00:16:25,300
 Wisdom looks beyond intelligence. It doesn't describe what

214
00:16:25,300 --> 00:16:29,000
 is like science tries to.

215
00:16:29,000 --> 00:16:32,000
 It looks also at what should be.

216
00:16:32,000 --> 00:16:36,000
 Or it looks deeper than intelligence.

217
00:16:36,000 --> 00:16:38,380
 I mean, I think from a Buddhist point of view you could

218
00:16:38,380 --> 00:16:40,000
 argue that it's still the same.

219
00:16:40,000 --> 00:16:45,000
 It's just a different type of intelligence.

220
00:16:45,000 --> 00:16:48,810
 Because in Buddhism we don't exactly look at wisdom as a

221
00:16:48,810 --> 00:16:50,000
 value judgment.

222
00:16:50,000 --> 00:16:54,000
 But it's a different type of intelligence or knowledge.

223
00:16:54,000 --> 00:16:57,000
 It's knowledge from experience.

224
00:16:57,000 --> 00:17:01,000
 Knowledge not based on thought or supposition or logic.

225
00:17:01,000 --> 00:17:04,900
 It doesn't come from the mind. It comes directly and solely

226
00:17:04,900 --> 00:17:07,000
 from the experience itself.

227
00:17:07,000 --> 00:17:10,000
 There's no interpretation of the experience.

228
00:17:10,000 --> 00:17:13,000
 There's no extrapolation.

229
00:17:13,000 --> 00:17:17,000
 There's no...

230
00:17:17,000 --> 00:17:20,000
 There's no...

231
00:17:20,000 --> 00:17:23,000
 There's no part for the mind.

232
00:17:23,000 --> 00:17:28,580
 It's solely what we experience, what is observed, and what

233
00:17:28,580 --> 00:17:32,000
 is undeniably true.

234
00:17:32,000 --> 00:17:45,000
 That's what leads to enlightenment.

235
00:17:45,000 --> 00:17:47,000
 So I mean this quote isn't that...

236
00:17:47,000 --> 00:17:52,840
 It's a nice quote, but it's not like a deep doctrinal

237
00:17:52,840 --> 00:17:54,000
 teaching.

238
00:17:54,000 --> 00:17:57,000
 Because of course human animals are...

239
00:17:57,000 --> 00:18:01,000
 He's just trying to shame them by saying even animals can

240
00:18:01,000 --> 00:18:02,000
 get along.

241
00:18:02,000 --> 00:18:16,000
 Why can't you guys?

242
00:18:16,000 --> 00:18:20,000
 But the reason why animals are better able to get along is

243
00:18:20,000 --> 00:18:20,000
...

244
00:18:20,000 --> 00:18:24,990
 Part of the reason is they're just too dumb to plot against

245
00:18:24,990 --> 00:18:26,000
 each other.

246
00:18:26,000 --> 00:18:35,000
 They might attack each other, but they forget very quickly.

247
00:18:35,000 --> 00:18:38,000
 They're not very good at putting things together.

248
00:18:38,000 --> 00:18:45,000
 They may have evil thoughts, but they quickly go away.

249
00:18:45,000 --> 00:18:47,440
 I suppose even more you could say that animals are very

250
00:18:47,440 --> 00:18:50,000
 much stuck in their routines.

251
00:18:50,000 --> 00:18:53,420
 As a result of low intelligence, they're stuck in their

252
00:18:53,420 --> 00:18:54,000
 ways.

253
00:18:54,000 --> 00:19:00,000
 And that has the result of creating sort of harmony, right?

254
00:19:00,000 --> 00:19:03,000
 Where the predator and prey are always balanced.

255
00:19:03,000 --> 00:19:06,430
 And this is why when people look at nature they think, "Oh,

256
00:19:06,430 --> 00:19:08,000
 it's so harmonious."

257
00:19:08,000 --> 00:19:10,720
 Because if you actually live in nature for any period of

258
00:19:10,720 --> 00:19:14,000
 time, it's a war zone.

259
00:19:14,000 --> 00:19:19,770
 Killing, killer, be killed. Always killing, always fighting

260
00:19:19,770 --> 00:19:22,000
, always...

261
00:19:22,000 --> 00:19:26,000
 Not really as peaceful as it seems.

262
00:19:26,000 --> 00:19:30,000
 But animals can get along.

263
00:19:30,000 --> 00:19:33,000
 It's a shaming because we have more intelligence.

264
00:19:33,000 --> 00:19:38,450
 It's a shaming because we don't often use the intelligence

265
00:19:38,450 --> 00:19:40,000
 for any good.

266
00:19:40,000 --> 00:19:44,130
 So I should all feel quite proud that we are using our

267
00:19:44,130 --> 00:19:46,000
 intelligence for good.

268
00:19:46,000 --> 00:19:49,430
 Using our intelligence to cultivate wisdom through the

269
00:19:49,430 --> 00:19:51,000
 meditation practice.

270
00:19:51,000 --> 00:19:57,000
 We're using our intelligence to refine our intelligence.

271
00:19:57,000 --> 00:20:00,440
 Refine our knowledge to create true knowledge and

272
00:20:00,440 --> 00:20:05,000
 understanding of reality as it is.

273
00:20:05,000 --> 00:20:13,000
 So, good job everyone.

274
00:20:13,000 --> 00:20:20,000
 Alright, so that's all I got to say about that short quote.

275
00:20:20,000 --> 00:20:30,320
 I'm posting the hangout if anybody has a question they want

276
00:20:30,320 --> 00:20:34,000
 to come on and ask.

277
00:20:34,000 --> 00:20:48,000
 [Silence]

278
00:20:48,000 --> 00:20:57,170
 We need, potentially need more people to get involved with

279
00:20:57,170 --> 00:20:59,000
 our organization.

280
00:20:59,000 --> 00:21:05,000
 So, having a bit of an organizational...

281
00:21:05,000 --> 00:21:09,000
 Am I allowed to say this online? Where's Robin?

282
00:21:09,000 --> 00:21:12,000
 I don't know if this is private information or not.

283
00:21:12,000 --> 00:21:15,000
 Is this top-secret information?

284
00:21:15,000 --> 00:21:18,320
 Well, I wanted to say that if anyone would like to get

285
00:21:18,320 --> 00:21:26,000
 involved with the organization, please come on out.

286
00:21:26,000 --> 00:21:28,500
 Because now that we've got an organization and we're really

287
00:21:28,500 --> 00:21:29,000
...

288
00:21:29,000 --> 00:21:35,320
 We've got resources and support. We need people to manage

289
00:21:35,320 --> 00:21:40,000
 and make use of this support.

290
00:21:40,000 --> 00:21:49,000
 [Silence]

291
00:21:49,000 --> 00:21:55,590
 We also could use a bit of help on the IT side. It'd be

292
00:21:55,590 --> 00:21:59,000
 nice if this website could be updated.

293
00:21:59,000 --> 00:22:02,000
 It'd be nice if we could move to a more...

294
00:22:02,000 --> 00:22:07,270
 You know, there was at one point this guy from Germany who

295
00:22:07,270 --> 00:22:14,000
 made a better organized version of this site.

296
00:22:14,000 --> 00:22:20,000
 [Silence]

297
00:22:20,000 --> 00:22:28,290
 Like, I don't know, somehow more standard and modern

298
00:22:28,290 --> 00:22:34,000
 programming-based version of this site.

299
00:22:34,000 --> 00:22:37,000
 So, if anybody wants to help out with that kind of thing.

300
00:22:37,000 --> 00:22:44,580
 Also, the iOS version of our app. I don't think it ever

301
00:22:44,580 --> 00:22:46,000
 happened, so maybe that could get finished.

302
00:22:46,000 --> 00:22:50,430
 If somebody wants to work on the Android app, there's

303
00:22:50,430 --> 00:22:53,000
 people with IT skills, programming skills.

304
00:22:53,000 --> 00:22:56,000
 Please let us know.

305
00:22:56,000 --> 00:22:59,000
 [Silence]

306
00:22:59,000 --> 00:23:03,000
 But yeah, big part is we need organizational people.

307
00:23:03,000 --> 00:23:04,000
 Especially people in Ontario.

308
00:23:04,000 --> 00:23:08,480
 If there's anybody in Ontario who wants to help out, please

309
00:23:08,480 --> 00:23:10,000
 don't hesitate.

310
00:23:10,000 --> 00:23:17,000
 We're having a volunteer meeting next Sunday at 12.

311
00:23:17,000 --> 00:23:21,000
 I think there's even a Facebook group, right?

312
00:23:21,000 --> 00:23:38,000
 [Silence]

313
00:23:38,000 --> 00:23:42,160
 Anyway, no questions, then I'm off. Have a good night,

314
00:23:42,160 --> 00:23:43,000
 everyone.

315
00:23:43,000 --> 00:23:52,000
 [Silence]

