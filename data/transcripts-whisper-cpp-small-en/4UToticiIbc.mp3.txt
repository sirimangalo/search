 [BLANK_AUDIO]
 [BLANK_AUDIO]
 Okay, and good evening, everyone.
 [BLANK_AUDIO]
 Welcome to our evening broadcast, Saturday Dhamma Talk.
 [BLANK_AUDIO]
 We have meditators staying with us and we have visitors.
 [BLANK_AUDIO]
 And we have an online community tuning in.
 [BLANK_AUDIO]
 Tuning in to hear the Dhamma.
 The Dhamma is the,
 [BLANK_AUDIO]
 The Dhamma is what the Buddha taught, Dhamma.
 Dharma is the Sanskrit.
 [BLANK_AUDIO]
 So how do you learn the Dhamma?
 [BLANK_AUDIO]
 This might seem like an odd question.
 Teach us.
 It's a simple answer.
 We learn the Dhamma by reading books and listening to talks
.
 [BLANK_AUDIO]
 Ah, but what do we mean by learned?
 [BLANK_AUDIO]
 By learn, the word learn.
 [BLANK_AUDIO]
 Does it mean that you remember what I said?
 [BLANK_AUDIO]
 If I tell you that the Buddha taught the four noble truths,
 did you just learn anything?
 What did you just learn?
 [BLANK_AUDIO]
 Suppose someone now asks you what did the Buddha teach?
 Well, the Buddha taught the four noble truths.
 Wow, you learned the Dhamma, right?
 There appears to be something missing there.
 [BLANK_AUDIO]
 I could tell you what the four noble truths are.
 You could repeat that back to someone.
 Wow, you know the four noble truths.
 Are you enlightened?
 Maybe, but not because of that.
 [BLANK_AUDIO]
 Well, what if you think about the four noble truths and you
 read books that people have written,
 maybe even write your own books?
 [BLANK_AUDIO]
 But you understand them.
 You say, yes, that relates.
 I've had that experience.
 I know suffering.
 Yes, I've suffered.
 I agree.
 I understand.
 [BLANK_AUDIO]
 Craving, that's the cause of suffering.
 Yes, I know when I crave, suffering comes from it.
 Or maybe you disagree.
 Maybe you say, no, just not true.
 I can like things and want things, doesn't make me suffer.
 But I understand the Buddha's teaching.
 I understand that it's wrong, you might say.
 [BLANK_AUDIO]
 Have you learned Buddhism when you think about it and you
 get it, right?
 It's logical, it's reasonable, it makes sense.
 What I teach, people who listen ostensibly agree or
 disagree with me,
 think about what I say and take it in.
 Maybe it gives them encouragement to practice.
 Why does it give them encouragement?
 Because they understand it.
 Because they agree with it.
 They think that's right, that's true.
 What's missing, something's missing, right?
 The glaring obvious omission.
 It's interesting, you know, if you ask this question,
 people would say, well, read a book on Buddhism.
 If you want to learn Buddhism, what happens when you read a
 book?
 What is going on?
 What is the actual reality of having read a book on
 Buddhism?
 There's courses on Buddhism, I've taken courses on Buddhism
.
 I've even taught classes on Buddhism.
 What happened? What's the result?
 Not very much from a Buddhist perspective.
 Some good stuff, some bad stuff, but not all that
 impressive in any way.
 What I mean is that Buddhism doesn't pay much attention to
 this learning stuff
 that we're talking about.
 Just like Buddhism doesn't pay much attention to things
 like Buddhism
 or things in general. People, places, things, not really
 important.
 Buddhism places emphasis on reality, on the phenomen
ological reality.
 When you've studied a book, read a book about Buddhism,
 the most interesting thing from a Buddhist perspective is
 what happened to you?
 What was the change?
 Like a science experiment. Let's give the lab rat,
 let's give the meditator something to read and see what
 happens after they've read it.
 That's what's interesting to us. We don't let you read
 because we've done these experiments
 and we say, "Oh, not positive results, extra thinking,
 perhaps some doubting, confusion, not much concentration
 and focus on reality."
 Meaning they don't even know what's going on in their mind.
 Someone who reads a book, after they've read the book,
 they're not more focused through the act of reading,
 through the physical act of reading on reality.
 Doesn't mean you shouldn't read,
 you have to acknowledge the fact that the act of reading
 distracts you during that time from reality.
 You're engaged in concepts, ideas.
 Hey, it's good, you do have to read.
 I wrote a little booklet, right? Make people read it.
 You want to do a course, you've got to read this booklet.
 Or listen to a talk.
 Listening to talks is, of course, less violent than reading
.
 It's more passive.
 But what I thought I'd do tonight, after all that,
 is try and do some experiential learning, what we call
 guided meditation.
 I've been asked before to do a guided meditation.
 I haven't done much guided meditation.
 I did one once in California.
 I was specifically asked to do it.
 Now I do do brief guided meditations, like when teaching
 mindfulness I'll have.
 That's what I'm going to do tonight.
 But it's going to be a little more extended.
 So the title of my talk is guided meditation on the Four
 Noble Truths.
 That's what we're going to do.
 We've said that the Buddha taught the Four Noble Truths.
 And we are going to learn about the Four Noble Truths.
 Not by talking about them. I'll be talking.
 But I'll be guiding. That's the hope. That's the idea.
 So for this one you're going to have to close your eyes.
 YouTube's kind of a funny platform to give a Buddhist talk
 because you're not supposed to be looking at me.
 I'm not the object of entertainment.
 You are. You are your object.
 So the Buddha taught the Four Noble Truths.
 We're going to learn about the Four Noble Truths.
 We're going to learn, not from watching me, but from
 watching you.
 I don't know if I dare, but I'm going to dare because I'm
 daring.
 There. Now you can barely see me.
 Now you've got to close your eyes.
 So I'm going to go through the Four Noble Truths.
 I'm going to go through things sequentially.
 But meditation isn't sequential, not like that.
 But we're on a tour. This is a guided tour.
 This isn't quite meditation.
 It's just a guided tour of the meditation space, your mind.
 So that you can come back and meditate.
 Now you know the meditation hall, meditation space, your
 own mind.
 Now you can come back and visit your mind later.
 We'll even have free gifts on the tour in the form of
 skills that allow you to,
 tools that you can bring back when you come again, souven
irs.
 So we start with the First Noble Truths.
 The First Noble Truth is suffering.
 Yes, the one thing nobody wants a tour of.
 We could skip this part of the tour, right?
 Let's skip right to the good stuff.
 Unfortunately not.
 It's not unfortunate. It's undesirable.
 It's disagreeable to have to take a tour through suffering.
 We can do it anyway because it's an important question.
 Why are we suffering?
 What is this?
 You want to learn about a problem.
 You don't go looking somewhere else.
 If you have a thorn in your foot, you don't go looking in
 your hand.
 We have to look where the problem is. You want to
 understand suffering.
 You want to be free from suffering. You have to understand
 suffering.
 You want to understand the problem.
 It's quite simple. You have to study the problem.
 And it's a wonderful thing that when you study the problem,
 you can, you do.
 Solve the problem.
 How are we doing? Eyes closed? Is this fun? Are we having
 fun?
 There's many ways you can go in meditation.
 You can try to calm your mind. It's what most people think
 of, right?
 If you've never done meditation before, that might be your
 only idea of it.
 Meditation is to calm the mind, empty the mind, etc.
 Maybe you think we're going to have special experiences
 like see bright lights or leave our bodies,
 be able to read each other's minds, that sort of thing.
 Many things you can do with the mind. Many things we do do
 with our minds in our daily life.
 But nothing we do, nothing we do with our minds or with our
 lives or with our bodies, with our self.
 None of it lasts forever. None of it satisfies us. None of
 it is really sustainable.
 What we're going to do is just look at our experiences,
 look at our minds, look at our bodies.
 Sometimes we can live our lives, some people live their
 lives with great happiness, great pleasure.
 I wonder what all this talk is about suffering.
 Just by sitting here, something that people who are very
 happy and very pleased and enjoy life never generally do,
 people who are engaged in pleasure seeking, they're doing
 that. They're not doing this.
 They would think this is boring, uninteresting. But it's
 quite unsettling that when you sit here,
 when you just look at reality, look at yourself, not some
 mystical idea of reality, just you.
 It's quite disturbing that it's not quite the way we think
 it is. Pleasure isn't quite the way we think it is.
 Pain isn't even quite the way we think it is.
 Our minds are jumping here, jumping there, our experiences
 are coming and going.
 It's like looking behind the curtain and the Wizard of Oz,
 you see this magical world full of marvellous, wonderful,
 scary, dangerous things.
 Then you look behind the curtain and you say, "Oh, it's
 just a machine. Oh, it's just that."
 I don't even know what to do with this. It's just like you
 look under the hood of a car.
 You come here new if you're new on this tour. You look
 under the hood and you say, "Oh, just an--
 I know that's a car engine, but I have no idea how to fix
 it.
 I can see something's broken, but I'm not a mechanic."
 Overwhelming. The mind can be quite overwhelming.
 Sitting here is not fun. Some of you have probably already
 switched over to Facebook.
 Much more fun.
 Go back to his other videos. They were more fun than this.
 Meditation. Mindfulness meditation.
 A word that many of you have heard.
 This is the tool. This is the knowledge. This is the
 mechanics training that allows us to look at suffering.
 Make sense of it. Why am I suffering? Am I suffering?
 Am I happy? What is happiness? What's going on in my mind?
 There are people who thought they were happy and worry
 about meditation.
 Meditation is going to stop me from daydreaming and
 enjoying thinking and planning and imagining.
 Creativity. Is it going to stifle my creativity?
 Meditation is going to help you see what all these things
 really are.
 It's not going to help you let go of something that's
 useful and beneficial to you.
 But it's quite interesting how when you look under the hood
, things aren't quite as they seem.
 "My car's running fine," you say, but the mechanic says, "
Oh, no. Your car is about to break down.
 That noise is not a good noise, but I like that noise.
 Like it or not like it, there's something going on under
 the hood.
 And you can like it all the way until your car breaks down,
 but it's going to break down."
 For example, not that I can claim that it is.
 You have to look under the hood to see.
 And it's a remarkable thing about meditation, about the
 mind, about mindfulness, that,
 again, that's all it takes is when you look, you do see.
 You see what's the problem.
 Another remarkable thing is that when you see the problem,
 well, we won't get ahead of ourselves.
 We'll just talk about the problem. Suffering.
 Yes, for most of us, this is unpleasant. We don't have to
 talk about this idea that life is all pleasurable and happy
,
 because for most of us, it's quite easy to see.
 If you're just an average ordinary person, this is kind of
 unpleasant, having to close your eyes
 and not knowing what to do. Kind of lost. It's not really
 familiar.
 So mindfulness is this tool that allows us to make this
 unfamiliar landscape more familiar.
 We start with something simple, like you're in kindergarten
.
 I'll give you a crayon and you can draw with it.
 Write your name in bright pink crayon.
 So we give you the stomach. Watch the stomach rising and
 falling.
 That's your pink crayon, or orange, or whatever color you
 want. Maybe no colors.
 This is the basics, the beginning, something simple.
 When the stomach rises, say to yourself, "Rising." Not out
 loud, but in the mind,
 it should be with the stomach. And when the stomach falls,
 say, "Falling." Rising, falling.
 It's not because the stomach is special in any way. It's
 just an easy beginner's tool, beginner's object.
 Introduction to the art of mindfulness, if I could coin
 that phrase.
 Because it's an art, it's a skill, it's a training. You
 have to get good at it.
 You have to really feel it. That's not the right word. You
 have to become it, maybe, to be zen about it.
 It's not about intellectual. People ask questions, "Should
 I be like this when I'm mindful?
 Should my mindfulness be like this? Should it be like that?
 Should I train myself in this way?" All that intellectual
ization doesn't work.
 You have to really become mindful through a lot of trial
 and error, adjustments, observations,
 seeing how you're doing it wrong. And by wrong, this means
 you're doing it in a way that is creating stress.
 You're creating stress, creating suffering.
 The Buddha taught the Four Noble Truths, it's all about
 suffering.
 If you're creating suffering, you're doing something wrong.
 But it's quite complicated. It's something that you have to
 practice, but our minds are quite complicated.
 If I ask you, "In what ways are you creating suffering?" Oh
, it's not a simple answer.
 Suffering, really, for our purposes, just means our minds
 are mixed up.
 There's stress. It's not pleasant sitting here. Why is it
 not pleasant sitting here?
 I'm not beating you over the head with the teachings. I'm
 not yelling at you.
 I'm not calling you nasty names. Hopefully there's not
 insects biting at you.
 It's not too hot or not too cold. Apart from all that, it's
 still unpleasant, still uncomfortable.
 Why can we not be at peace? This is suffering.
 The second Noble Truth is the cause of suffering.
 This is what we see when we start to observe suffering,
 when you look at the stomach rising and falling.
 When you look at your feelings and you say, "Pain, pain,
 pain."
 When you're thinking and you say, "Thinking, thinking."
 When you observe these things, you observe the suffering.
 When you're stressed and you say, "Stressed, stressed."
 When you like something or you dislike something, you say,
 "Liking, liking, disliking, disliking."
 You start to see what's really causing you suffering. You
 start to realize, "Hey, wait a minute."
 Here I thought, "Boy, that bad memory that I have of that
 thing that bothers me, that's suffering."
 This person yelling at me, "Oh, what's suffering?" The heat
, it's so hot this summer, suffering terribly from the heat.
 Too cold, I'm hungry, I'm thirsty, constipated, diuretic.
 I have a stomach ache, I have a back ache, I have a
 headache, I have a tooth ache.
 All these thoughts that keep coming back, all these
 memories.
 The sadness, "I've lost this, I've lost that."
 The burdens I have of debt, maybe I'm unhealthy, maybe I'm
 overweight, maybe I'm getting old, maybe I have cancer,
 diabetes, maybe I'm disabled, maybe I'm this or that.
 We start to see things a little bit differently, eventually
 a lot differently.
 And in fact, it's not these experiences that are the
 problem.
 Suffering is in fact not the problem.
 The problem is those things that are causing suffering,
 which aren't actually the sufferings themselves, but are
 reactions to those things.
 It's not that you feel pain, it's that you don't like
 feeling pain, it's not that you're thinking about something
 in the past, no matter how many times it comes up.
 It's that you're afraid or upset or sad or even attracted
 to the thought that leads to a cycle of positive or
 negative emotions, attachments and obsessions, stress,
 chaos, evil if you want to even go so far.
 Evil, you hurt other people, you hurt yourselves, to get
 what we want, to get away from what we don't want and do
 anything, which drives us further and further into this
 habit of reaction.
 If you react negatively to something, it doesn't free you
 of the problem, it makes you more reactionary.
 Ah, this is the problem.
 The Buddha said craving is the cause of suffering.
 I like to be a little more broad than that. He does talk
 about three types of craving.
 But if you want to break it down, it's really just any
 reaction or any addition we make to reality.
 The wonderful thing about mindfulness and the reason why it
's so potent, so powerful.
 If you're sitting here with me, being mindful, you can note
 the sound of my voice even as hearing, hearing.
 If you're noting the pain as pain, pain, pain.
 You start to see that remarkably none of this is unpleasant
, none of this is problematic.
 It's only when you stop being mindful and you like or
 dislike, bored, excited, amused, and so on, that you lose
 track of you, you lose track of this, of now.
 You lose touch with reality, with the natural reality of
 experience.
 That's where all the stress comes from.
 The Buddha singled out craving because it's that which
 drives us.
 Talking about it as craving means any kind of, or thirst is
 maybe a better literal translation,
 any kind of drive you have to get rid of something, to any
 kind of reaction really we have to our experiences.
 That's bad, how can I get rid of it?
 Thirst, you're thirst for annihilation of things or
 destruction or escape, create stress.
 You don't like this, that's your problem.
 This is not your problem, they're not liking it as your
 problem.
 So sitting here, can you see it?
 Can you see likes and dislikes?
 Fears, aversions, boredom, sadness.
 Wants, desires, all these things that take us away.
 Why can't we just sit?
 Why can't I just be here and now?
 Oh, there's so much that I want, so much that I like and
 dislike.
 There's ego getting in the way, so many different things.
 This monk doesn't know what he's talking about, boring.
 Why doesn't he turn the light back on?
 All this stuff, that's not here, that's not now.
 Give you a tool, a really remarkable tool.
 Use the mantra to remind yourself, this is this, it is what
 it is.
 Pain is pain, thoughts are thoughts, emotions are emotions,
 the body is the body.
 Watch the stomach, it's an easy one, just say rising,
 falling, put your hand on your stomach even.
 Not only will you see these things that we call suffering,
 but you'll see what's making them suffering,
 it's only our likes and dislikes of them, our partialities
 and our reaction.
 Take a look at them and you'll see, oh yes, that thing I
 thought was so pleasant,
 the state of finding it pleasant, right?
 The state of finding it pleasant is actually stressful.
 It's coarse, it's less peaceful than just being content.
 How do you make it so that you never ever don't get what
 you want?
 How do you make it so that you always get what you want?
 The only way is to never want anything. It seems kind of
 like a trick,
 but in reality it is actually the solution.
 You want to be happy, don't ever want anything.
 Don't take my word for it.
 You can see it now for yourselves.
 Take a look, it's right here.
 If you're practicing along, move on to the third noble
 truth.
 This is the cessation of suffering and really it's talking
 about something called nirvana,
 which technically or dogmatically refers to this moment
 where you let go of everything.
 I say dogmatically because it's not in a bad way, but we're
 going to broaden the definition a little bit
 and point out that even here and now you can get an idea of
 what this is like by letting go of little things.
 Just here and now and you say to yourself, pain, pain, and
 you find yourself letting go of the pain.
 Maybe you're bored of what you're hearing and you're kind
 of annoyed that I keep talking and don't let you meditate.
 Focus on the sound of my voice and say hearing, hearing.
 And you let go of the judgment, the likes, the dislikes.
 Focus on the likes and dislikes, say liking, liking, and
 you say, oh, look at that, it's just disappeared.
 If you're bored, what's the best way to overcome boredom?
 Sounds awful, but you just actually just say to yourself,
 bored, bored.
 If you can do it without judgment instead of like reaff
irming it, this is terrible, bored, I'm bored, I'm bored.
 That's not mindfulness.
 But if you say it objectively, bored, bored, trying to see
 that, yes, indeed, this is boredom instead of judging it.
 Poof.
 It's magic.
 People have anxiety and then try this and say, oh, I'm not
 anxious.
 If you've ever had butterflies in your stomach, you have to
 do something that you normally be so anxious about.
 It's kind of funny to feel and your body is still shaking
 and butterflies still in your stomach, but your mind is
 like totally at peace.
 And you realize that wasn't it.
 All of that wasn't it. It wasn't the problem.
 The problem was totally in my mind, my reaction to all
 these things.
 That's the cessation of suffering.
 Nibbana. Nibbana is... it's remarkable.
 It's not that it doesn't exist or that it's even different.
 It's quite different.
 But all of these things are hooks in us.
 Nibbana is just the natural result of freeing yourself from
 all the hooks.
 You get free.
 That's the cessation of suffering.
 Do this enough.
 You'll get there.
 Simple, right? Sounds simple.
 It should seem a lot simpler now that you're actually,
 hopefully, doing it.
 It's not some complex theory that you have to study in
 books.
 Just start to look at things as they are. Start to see.
 Make sense of your own mind.
 So finally we come to the fourth noble truth.
 Again, these aren't sequential. This is just a tour.
 The fourth is really what we've been doing, hopefully.
 When you say to yourself, "Pain, pain," or so on, you're
 cultivating the fourth truth,
 which is the path.
 So you've already been doing it.
 But we'll break it down because the Buddha actually said
 the path is composed of eight parts.
 They're important.
 It's important to cover all eight bases.
 Again, not in sequential order, but they are factors.
 So as a tour, we'll go through them all.
 Not quite as you would do in meditation.
 The first one is right view.
 As a meditator, you develop right view.
 I mean, the most remarkable part for a beginner is just
 seeing what's there.
 Realizing it's not what we thought.
 It's not how we thought, and we didn't really even think.
 A lot of wrong view is just no view.
 So no view in the sense of being blind.
 If you ask people, "What do you think about your mind? What
 is your theory of your mind?"
 They might give you a half-baked answer, but many people
 don't even think about such things.
 Or they accept the views of others.
 They accept what scientists will tell you or religious
 leaders will tell you.
 Right view means you actually start to see the Four Noble
 Truths.
 The Fourth Noble Truth starts with seeing the Four Noble
 Truths.
 We could say it starts with seeing the First Noble Truth,
 and as a result letting go of the Second Noble Truth.
 Seeing that the things that we hold on to are not worth
 holding on to.
 The things that we get upset about are not worth getting
 upset about.
 Suffering only comes from clinging.
 It only comes from craving.
 From reaction.
 So when you look, when you gain right view, meaning just
 seeing things, seeing what's going on,
 while accompanying this, there's also right thought.
 You can put these in order. There is a way to do that.
 Because right view does lead to right thought.
 But it's more complicated. It's not something you should
 stick to.
 But for the tourist purposes, it's useful.
 When you have right view, there's right thought.
 Right thought just means, or the right intention, but you
're right mental activity.
 Your mind starts to get right.
 You no longer want to kill people, or steal, or lie, or
 cheat.
 You no longer get angry.
 Anger is reduced. Greed is reduced.
 Delusion is reduced.
 Bad thoughts are reduced.
 Eos.
 Mind states that stress you are reduced.
 Purified.
 Just saying to yourself, pain, pain, or seeing, seeing,
 thinking, thinking.
 Just watching the stomach rising and falling.
 This is the thoughts here.
 The thoughts that arise or that you cultivate from this
 are thoughts of clarity, thoughts of purity.
 You start to think objectively.
 Live objectively. Observe and interact with the world
 objectively.
 It's not devoid of...it's not missing something.
 It's much more real, vibrant, and alive,
 than living in your mind with your worries, and your fears,
 and your doubts, and your addictions, your desires, your
 ambitions, and so on.
 Even your wants and desires, the stuff that we say is good.
 The act of wanting and desiring, again, it's not real.
 It's not in touch with reality. It's not here. It's not now
.
 It's all up in our heads, in our minds.
 So you should be seeing your thoughts now.
 Oh, these thoughts are stressful. These thoughts are...
 These thoughts are peaceful.
 Work that all out. You'll start to gain right thought,
 or right intention, right mental activity.
 Along with this comes three more things.
 All the next three, speech, action, livelihood.
 We can lump these all together and talk about them in terms
 of
 how pure is this state.
 People don't realize about meditation is that
 it can't be a hobby, something you do on the side.
 If you want meditation to be valuable, beneficial,
 it's a change in who you are.
 And so it requires something we call morality.
 It's quite opposite or in opposition to things like killing
 and stealing
 and lying and cheating.
 It's even in opposition to talking too much.
 Another part of this that we don't realize is that
 throughout our lives,
 our daily lives, we're constantly doing and saying things
 that mess us up.
 We say things we regret.
 Maybe we even say good things, but then we feel doubt and
 self-conscious
 and "Did I really say the right thing?"
 It's even possible to feel bad about doing the right thing,
 about something that was objectively good because of our
 minds.
 When your minds are not screwed up, when your minds are
 more ordered
 and more clear, your actions and your speech become more
 clear.
 But what I mean to say is that when we're talking a lot,
 acting a lot,
 especially with minds that are not yet pure,
 we're quite likely to have wrong speech, wrong action,
 wrong livelihood,
 wrong activities.
 So the question might come up, "Well, right speech, but I'm
 not even talking.
 How can it be right speech?"
 It's the best speech.
 What it means is that there's no bad speech.
 It's right by omission, meaning it's completely pure.
 That aspect of who you are is beautiful.
 There can be no badness coming from it.
 It means you're not being bombarded by these guilty
 feelings
 of having said something wrong,
 the stressful feelings of talking too much
 and getting worked up over your speech,
 even the mental fatigue of having to think of things to say
 or a distraction from, again, talking a lot.
 Actions, doing no actions, just sitting still.
 This isn't theoretical. This is psychological.
 The very real results of not saying or doing anything, or
 very little,
 are profound.
 Why is meditation always done sitting still?
 Try sitting still.
 Very difficult.
 It's not only difficult, it's profound.
 It's so pure.
 It's so now and here and real.
 Very difficult, but quite profound.
 Right livelihood, well, that just goes along with the other
 two.
 That being said, I'm going to reiterate that this is life.
 I'm not trying to say that you're totally absolved
 from all the bad things you do in life
 just because you come and purify here and now in this
 session.
 They have to go hand in hand.
 It has to be a life. It has to be a part of who you are.
 You want to become free from suffering.
 Most important is that you can see.
 You can see how important things like speech, how effective
,
 how efficacious they are. They bring results.
 You say things, do things.
 There's a power to them. There's a result there.
 Psychological changes your mind, affects your mind.
 At this moment when we're not saying or doing anything,
 there's a very important part of our meditation.
 It's a great power that comes from that,
 keeping us focused, keeping us present.
 In the last three, we've gone through five.
 The next one is effort.
 Effort.
 We sit here quietly. We're not running around.
 We're not striving for worldly gains.
 It's not much like business or commerce or technology or
 whatever.
 Employment in the world.
 Where's the effort?
 Try watching your stomach say to yourself, "Rising, falling
."
 Try saying, "Pain, pain," or "Thinking, thinking,"
 "hearing, hearing when you hear my voice."
 The challenge there and the intricacy involved,
 that you have to kind of be on a knife's edge,
 the perfect balance of just being here, which takes skill.
 That's effort.
 The attempt, the interest, the inclination
 to know that your stomach is rising
 and just know that your stomach is rising takes effort.
 It doesn't mean you have to push, push really hard.
 It's a finely tuned instrument, and tuning it takes effort.
 Right mindfulness, well, that's what we're doing.
 That's the saying to yourself, "hearing, hearing, seeing,
 seeing."
 The grasping of the object just as it is.
 You use effort, but you use effort to gain mindfulness.
 When you're mindful, you just see things as they are.
 Pain is pain, thinking is thinking,
 liking is liking, disliking is disliking.
 That's it, that's all.
 Mindful, it's not a great translation.
 It's not bad, but mindfulness means reminding yourself
 or remembering, reminding yourself so that you remember,
 "Oh yeah, this is this, nothing more, nothing less."
 Try that, right? That's what we're trying.
 Right now, you feel pain, say to yourself,
 "Pain, you have thoughts, say thinking."
 You feel happy as well, say, "Happy, happy."
 Learn to see things as they are.
 That's mindfulness and effort and all these things.
 Right view, right thought, and so on.
 And the final one is concentration, really the important
 outcome.
 Not that you're just so focused that you don't get
 distracted,
 but that you're focused on reality,
 that your focus actually shifts from all sorts of mental
 activity
 and thoughts and stuff, concepts, to experience,
 to being here and now.
 I'm sitting, sitting is sitting, it's here, it's real.
 Pain is pain, thoughts are thoughts, likes are likes, disl
ikes are dislikes, and so on.
 It's this concentration, or the focus may be a better
 translation,
 a balanced mind, a mind that is in perfect tune, in tune
 with reality.
 It's that mind that is going to slip through, slip out,
 free itself from suffering,
 from all the barbs, all the hooks that pull us here and
 there.
 The hooks are those things that allow other people to hurt
 us,
 things to hurt us, makes us vulnerable.
 Take out those hooks, you're invincible, the world cannot
 harm you.
 And that's the fourth noble truth, that's all for noble
 truths.
 That concludes our tour.
 This isn't meditation quite, so hopefully you are doing
 some meditation there,
 as a tour.
 You take this as sort of a guide of the, again the outline,
 the space,
 the realm of meditation and the direction your meditation
 is encouraged to go.
 So it's a little guidance, a guided tour.
 Thank you all for tuning in.
 I guess I won't turn the light on, I'll just transition
 back to this nice photo of a big brick thing.
 Alright, thank you all for tuning in, have a good night.
 [ Silence ]
 [ Silence ]
