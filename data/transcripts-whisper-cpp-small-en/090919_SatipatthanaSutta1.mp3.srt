1
00:00:00,000 --> 00:00:04,440
 Okay, so we'll just get started.

2
00:00:04,440 --> 00:00:12,600
 Today we will be discussing the Satipatthana sūta, the

3
00:00:12,600 --> 00:00:16,600
 discourse on the establishment

4
00:00:16,600 --> 00:00:27,240
 or the setting up or the foundation of mindfulness.

5
00:00:27,240 --> 00:00:33,570
 This is one of the most loved and best known discourses of

6
00:00:33,570 --> 00:00:34,960
 the Lord Buddha because it's

7
00:00:34,960 --> 00:00:42,110
 considered to be a fairly complete and lucid explanation of

8
00:00:42,110 --> 00:00:46,240
 the practice of Buddhist meditation.

9
00:00:46,240 --> 00:00:51,170
 And this elusive word that we always translate as

10
00:00:51,170 --> 00:00:52,920
 mindfulness.

11
00:00:52,920 --> 00:00:57,410
 Slightly so because when we look at the Lord Buddha's

12
00:00:57,410 --> 00:00:59,760
 teaching as a whole, at least in

13
00:00:59,760 --> 00:01:05,600
 the early forms, in the early traditions, we see that

14
00:01:05,600 --> 00:01:14,400
 mindfulness or this sort of mind

15
00:01:14,400 --> 00:01:17,900
 state or this quality of mind which we're translating as

16
00:01:17,900 --> 00:01:19,800
 mindfulness, we can see that

17
00:01:19,800 --> 00:01:24,960
 it's very much at the core of the Buddha's teaching.

18
00:01:24,960 --> 00:01:27,860
 We can think of when the Lord Buddha passed away right

19
00:01:27,860 --> 00:01:29,720
 before he passed away, his last

20
00:01:29,720 --> 00:01:47,590
 words were "apamātena sampadita," which means "look, O you

21
00:01:47,590 --> 00:01:48,600
 who see the danger in samsara,

22
00:01:48,600 --> 00:01:54,470
 all things that arise have to in the end cease or pass away

23
00:01:54,470 --> 00:01:54,920
.

24
00:01:54,920 --> 00:01:57,760
 Everything that comes in the end has to go."

25
00:01:57,760 --> 00:02:04,070
 And "apamātena sampadita" means to work hard to become

26
00:02:04,070 --> 00:02:10,360
 perfect in heedfulness or religiosity,

27
00:02:10,360 --> 00:02:16,040
 religions which is the opposite of negligence.

28
00:02:16,040 --> 00:02:18,790
 And so we see that the core of the Buddha's teaching,

29
00:02:18,790 --> 00:02:20,640
 wherever we look, we'll see that

30
00:02:20,640 --> 00:02:24,820
 this idea of not being negligent or this word that I've

31
00:02:24,820 --> 00:02:27,160
 coined "religence," which basically

32
00:02:27,160 --> 00:02:30,440
 means having a religious attitude or being of a religious

33
00:02:30,440 --> 00:02:32,180
 nature, which simply means

34
00:02:32,180 --> 00:02:38,790
 the opposite of negligence, paying attention and giving

35
00:02:38,790 --> 00:02:42,520
 life some measure of importance

36
00:02:42,520 --> 00:02:45,570
 instead of taking it to be some kind of a game where we can

37
00:02:45,570 --> 00:02:47,280
 just do and say and think whatever

38
00:02:47,280 --> 00:02:51,830
 we want, that we have to have some sort of what has come to

39
00:02:51,830 --> 00:02:54,240
 be known as mindfulness.

40
00:02:54,240 --> 00:02:58,420
 And that's exactly what is meant by "apamāta," not being

41
00:02:58,420 --> 00:02:59,480
 negligent.

42
00:02:59,480 --> 00:03:07,340
 The correct explanation of the word "apamāta" is to be

43
00:03:07,340 --> 00:03:10,880
 mindful at all times.

44
00:03:10,880 --> 00:03:12,980
 So this is a very important discourse and a very important

45
00:03:12,980 --> 00:03:14,160
 teaching that the Lord Buddha

46
00:03:14,160 --> 00:03:15,160
 gave.

47
00:03:15,160 --> 00:03:19,020
 So if you have time to read through it, you can sort of get

48
00:03:19,020 --> 00:03:20,960
 a feeling for how it is that

49
00:03:20,960 --> 00:03:24,840
 one establishes what we call mindfulness.

50
00:03:24,840 --> 00:03:27,730
 First of all, the word "mindfulness," as I've said before,

51
00:03:27,730 --> 00:03:30,000
 is a fairly poor translation,

52
00:03:30,000 --> 00:03:34,680
 fairly misleading and vague rendering of the word "sati."

53
00:03:34,680 --> 00:03:38,440
 "Sati" means to remember, and it's used often to talk about

54
00:03:38,440 --> 00:03:40,440
 remembering things in the past

55
00:03:40,440 --> 00:03:46,470
 in a sense of recollecting or thinking over, bringing to

56
00:03:46,470 --> 00:03:47,440
 mind.

57
00:03:47,440 --> 00:03:50,700
 It can also be used to talk about the future, thinking

58
00:03:50,700 --> 00:03:52,760
 about or remembering things that

59
00:03:52,760 --> 00:03:57,160
 we have to do in the future.

60
00:03:57,160 --> 00:04:01,820
 Here it simply means to remember reality or remember

61
00:04:01,820 --> 00:04:05,080
 oneself or remember the experience

62
00:04:05,080 --> 00:04:12,350
 which one is faced with. The word "sati," as we'll see,

63
00:04:12,350 --> 00:04:14,520
 means the clear remembrance or

64
00:04:14,520 --> 00:04:17,760
 recognition of something for what it is.

65
00:04:17,760 --> 00:04:24,060
 And it's brought about by a recalling, calling to mind,

66
00:04:24,060 --> 00:04:28,500
 just like memory when you see something

67
00:04:28,500 --> 00:04:34,020
 and you remember it to be a person, you remember the color,

68
00:04:34,020 --> 00:04:37,040
 you recognize some factor about

69
00:04:37,040 --> 00:04:40,150
 it. Maybe when you recognize that it's something that has

70
00:04:40,150 --> 00:04:41,520
 caused you suffering in the past

71
00:04:41,520 --> 00:04:44,340
 or something that has caused you pleasure and then it leads

72
00:04:44,340 --> 00:04:45,680
 to liking or disliking of

73
00:04:45,680 --> 00:04:48,600
 it.

74
00:04:48,600 --> 00:04:51,990
 But here, what we're talking about is recognizing something

75
00:04:51,990 --> 00:04:53,760
 for what it is, and as I've said

76
00:04:53,760 --> 00:04:56,800
 before, not making more of it than it actually is.

77
00:04:56,800 --> 00:05:00,880
 So when we see remembering that that is an experience of

78
00:05:00,880 --> 00:05:03,800
 seeing, when an emotion arises,

79
00:05:03,800 --> 00:05:06,760
 remembering that that's an emotion, remembering it for what

80
00:05:06,760 --> 00:05:08,320
 it is, reminding ourselves of

81
00:05:08,320 --> 00:05:13,770
 the reality of it. This is what is meant by the word "sati

82
00:05:13,770 --> 00:05:14,160
."

83
00:05:14,160 --> 00:05:16,300
 So we're going to use the word "mindfulness," but we have

84
00:05:16,300 --> 00:05:17,760
 to understand that it really means

85
00:05:17,760 --> 00:05:21,740
 to remember or to have a sort of a clear awareness and

86
00:05:21,740 --> 00:05:24,720
 clear recognition of something for what

87
00:05:24,720 --> 00:05:27,160
 it is.

88
00:05:27,160 --> 00:05:32,390
 So the sutra starts, "Ey wamey sutang," which means, "Thus

89
00:05:32,390 --> 00:05:34,920
 have I heard," or "This is what

90
00:05:34,920 --> 00:05:38,600
 I have heard." And so this is something that has been

91
00:05:38,600 --> 00:05:40,600
 passed on from generation to generation

92
00:05:40,600 --> 00:05:45,130
 that at one time, the blessed one was living in the land of

93
00:05:45,130 --> 00:05:47,560
 the Kurus at Kamasadhama, which

94
00:05:47,560 --> 00:05:55,550
 they have identified as most likely a place in a suburb of

95
00:05:55,550 --> 00:05:58,680
 New Delhi in India.

96
00:05:58,680 --> 00:06:02,480
 And so the talk that Lord Buddha gave starts, "This is the

97
00:06:02,480 --> 00:06:04,520
 only way of bhikkhus, for the

98
00:06:04,520 --> 00:06:07,290
 purification of beings, for the overcoming of sorrow and

99
00:06:07,290 --> 00:06:09,040
 lamentation, for the destruction

100
00:06:09,040 --> 00:06:11,920
 of suffering and grief, for the reaching the right path,

101
00:06:11,920 --> 00:06:13,880
 for the attainment of nirvana,

102
00:06:13,880 --> 00:06:19,440
 namely the four arousings of mindfulness."

103
00:06:19,440 --> 00:06:21,720
 And they say that the reason the Lord Buddha gave this talk

104
00:06:21,720 --> 00:06:22,840
 in the land of the Kurus is

105
00:06:22,840 --> 00:06:29,500
 because the land of the Kurus, or the people of this land,

106
00:06:29,500 --> 00:06:33,120
 were a fairly exceptional group

107
00:06:33,120 --> 00:06:36,160
 of people, and the land was a fairly exceptional place.

108
00:06:36,160 --> 00:06:39,180
 The weather was always nice, never too hot or too cold,

109
00:06:39,180 --> 00:06:40,960
 there was no famine, there was

110
00:06:40,960 --> 00:06:46,120
 no war, there was no political turmoil. It was all in all a

111
00:06:46,120 --> 00:06:47,720
 fairly nice place to live.

112
00:06:47,720 --> 00:06:55,880
 And so the people were fairly well-cultured individuals.

113
00:06:55,880 --> 00:06:58,760
 There was not a lot of time spent

114
00:06:58,760 --> 00:07:04,260
 bickering and worrying about the lower aspects of life, so

115
00:07:04,260 --> 00:07:07,000
 there was a lot of more higher

116
00:07:07,000 --> 00:07:11,600
 intellectual and spiritual discussion going on.

117
00:07:11,600 --> 00:07:14,970
 And once Buddhism reached this place, as the Lord Buddha

118
00:07:14,970 --> 00:07:16,880
 went again and again to the land

119
00:07:16,880 --> 00:07:20,720
 of the Kurus to teach, they were able to, as a society,

120
00:07:20,720 --> 00:07:22,820
 become very mindful and very

121
00:07:22,820 --> 00:07:26,950
 interested in the Buddha's teaching, very able to apply it

122
00:07:26,950 --> 00:07:28,880
 on a practical basis, such

123
00:07:28,880 --> 00:07:33,210
 that not only the monks and the nuns, but also the ordinary

124
00:07:33,210 --> 00:07:35,520
 people living their everyday

125
00:07:35,520 --> 00:07:41,240
 lives were able to practice the four foundations of

126
00:07:41,240 --> 00:07:43,160
 mindfulness.

127
00:07:43,160 --> 00:07:46,270
 So they would ask themselves, they would ask each other, "

128
00:07:46,270 --> 00:07:47,760
So what part of the Buddha's

129
00:07:47,760 --> 00:07:50,280
 teaching are you practicing today? Are you being mindful of

130
00:07:50,280 --> 00:07:51,680
 your body, the things that

131
00:07:51,680 --> 00:07:57,560
 you do? Are you being mindful of your speech, mindful of

132
00:07:57,560 --> 00:07:59,520
 your emotions?"

133
00:07:59,520 --> 00:08:03,890
 And the story goes that whenever somebody said, "Oh no, I

134
00:08:03,890 --> 00:08:05,400
 haven't been mindful, I

135
00:08:05,400 --> 00:08:07,150
 haven't had any time today," they would yell at each other,

136
00:08:07,150 --> 00:08:08,080
 they would scold each other

137
00:08:08,080 --> 00:08:11,410
 and say, "Oh, you're like a person who is dead, having

138
00:08:11,410 --> 00:08:13,340
 lived your life without being

139
00:08:13,340 --> 00:08:18,740
 mindful, without taking the time, even though they're

140
00:08:18,740 --> 00:08:22,000
 working, to become aware and to sort

141
00:08:22,000 --> 00:08:28,630
 of keep the mind present in the here and now." It's like

142
00:08:28,630 --> 00:08:30,760
 being dead, they would scold each

143
00:08:30,760 --> 00:08:32,440
 other as being dead.

144
00:08:32,440 --> 00:08:38,440
 So it was a fairly cultured and high-minded group of people

145
00:08:38,440 --> 00:08:39,340
. So this is the reason the

146
00:08:39,340 --> 00:08:44,060
 Lord Buddha preached many discourses in that area,

147
00:08:44,060 --> 00:08:48,400
 especially this one, which is of course,

148
00:08:48,400 --> 00:08:52,800
 as I've said, a very important teaching.

149
00:08:52,800 --> 00:08:56,460
 So first of all, the word bhikkhu here is generally used to

150
00:08:56,460 --> 00:08:58,080
 refer to the monks, and it

151
00:08:58,080 --> 00:09:01,100
 may very well be that the Lord Buddha was addressing

152
00:09:01,100 --> 00:09:04,960
 directly the monks. But it's also

153
00:09:04,960 --> 00:09:08,960
 quite possible, and likely even in this case, that he was

154
00:09:08,960 --> 00:09:11,480
 talking to the group of practitioners

155
00:09:11,480 --> 00:09:17,560
 as a whole. Because of course, just becoming a monk doesn't

156
00:09:17,560 --> 00:09:19,920
 mean that one will really and

157
00:09:19,920 --> 00:09:23,430
 truly take to heart the Lord Buddha's teaching and really

158
00:09:23,430 --> 00:09:25,200
 and truly practice it. And just

159
00:09:25,200 --> 00:09:27,720
 because one was not a monk doesn't mean one doesn't have

160
00:09:27,720 --> 00:09:29,400
 the opportunity to practice and

161
00:09:29,400 --> 00:09:36,240
 to see clearly the truth of the Lord Buddha time.

162
00:09:36,240 --> 00:09:41,040
 So here it means one who has seen ikati, which means to see

163
00:09:41,040 --> 00:09:43,680
 ika, one who has seen the fear

164
00:09:43,680 --> 00:09:49,590
 or the danger, as I said, the danger in samsara, paayang ik

165
00:09:49,590 --> 00:09:53,820
ati, pikkhu, one who sees the danger.

166
00:09:53,820 --> 00:09:57,380
 So one who has come in their life to see the suffering

167
00:09:57,380 --> 00:09:59,880
 inherent in existence, to come to

168
00:09:59,880 --> 00:10:04,710
 some realization that life is not just a game where we can

169
00:10:04,710 --> 00:10:07,200
 do and say and think whatever

170
00:10:07,200 --> 00:10:10,130
 we want, because it has repercussions. And these

171
00:10:10,130 --> 00:10:11,760
 repercussions can be devastating. People

172
00:10:11,760 --> 00:10:15,550
 kill themselves. People have to take medication. People

173
00:10:15,550 --> 00:10:17,360
 have to go see psychotherapists, because

174
00:10:17,360 --> 00:10:22,130
 they just aren't able to deal with the suffering that which

175
00:10:22,130 --> 00:10:24,920
 they encounter in their lives.

176
00:10:24,920 --> 00:10:27,800
 They've been negligent. And so these are people who see the

177
00:10:27,800 --> 00:10:29,160
 danger of being negligent, the

178
00:10:29,160 --> 00:10:35,160
 danger of being unmindful. And so this is what that word

179
00:10:35,160 --> 00:10:36,200
 means.

180
00:10:36,200 --> 00:10:39,390
 And so here, the Lord Buddha is making a fairly bold claim.

181
00:10:39,390 --> 00:10:41,920
 He's saying this is the only way

182
00:10:41,920 --> 00:10:44,740
 the four foundations of mindfulness or the four arousings

183
00:10:44,740 --> 00:10:46,160
 of mindfulness are the only

184
00:10:46,160 --> 00:10:52,290
 way to realize these five goals which the Lord Buddha has

185
00:10:52,290 --> 00:10:53,800
 outlined.

186
00:10:53,800 --> 00:10:56,020
 And what does it mean to say that something is the only way

187
00:10:56,020 --> 00:10:59,080
? Well, in this instance, it's

188
00:10:59,080 --> 00:11:03,520
 actually explained in some detail in the commentaries. It's

189
00:11:03,520 --> 00:11:06,280
 something that they were sort of maybe

190
00:11:06,280 --> 00:11:10,690
 not having difficulty agreeing on, but didn't pinpoint down

191
00:11:10,690 --> 00:11:12,640
 to having one meaning. So they

192
00:11:12,640 --> 00:11:16,060
 give it all together four or five reasons, four or five

193
00:11:16,060 --> 00:11:16,920
 meanings.

194
00:11:16,920 --> 00:11:22,530
 First of all, the only way means that it is only one way.

195
00:11:22,530 --> 00:11:25,640
 And many people object to this

196
00:11:25,640 --> 00:11:30,270
 thinking that there are many ways that you can practice and

197
00:11:30,270 --> 00:11:32,320
 all these paths that lead

198
00:11:32,320 --> 00:11:36,110
 to the same goal. And everyone wants to think that there

199
00:11:36,110 --> 00:11:38,160
 are many ways to reach the same

200
00:11:38,160 --> 00:11:40,230
 goal because of course we see in the world that there are

201
00:11:40,230 --> 00:11:42,000
 many different practices. But

202
00:11:42,000 --> 00:11:45,670
 here we're not talking about any particular tradition or

203
00:11:45,670 --> 00:11:48,080
 school of Buddhism or any particular

204
00:11:48,080 --> 00:11:52,000
 religion even. What we're talking about again is focusing

205
00:11:52,000 --> 00:11:53,640
 on reality, putting our attention

206
00:11:53,640 --> 00:11:56,730
 on reality and seeing it for what it is. And so what the

207
00:11:56,730 --> 00:11:57,760
 Buddha is saying here is not saying

208
00:11:57,760 --> 00:12:00,460
 that Buddhism is the only way or my Buddhism is the only

209
00:12:00,460 --> 00:12:03,320
 way or this Buddhism or that Buddhism.

210
00:12:03,320 --> 00:12:06,140
 What he's saying is the only way to become free from

211
00:12:06,140 --> 00:12:08,240
 suffering and to attain these five

212
00:12:08,240 --> 00:12:13,990
 great benefits, these five great goals, is to look and to

213
00:12:13,990 --> 00:12:16,840
 see reality clearly for what

214
00:12:16,840 --> 00:12:23,740
 it is, to see things as they are and not become confused

215
00:12:23,740 --> 00:12:29,320
 and distracted by concepts and delusion.

216
00:12:29,320 --> 00:12:33,450
 The second meaning is that it's the only way, it's the one

217
00:12:33,450 --> 00:12:35,460
 way meaning that one has to go

218
00:12:35,460 --> 00:12:39,110
 by oneself, that the practice of meditation is not

219
00:12:39,110 --> 00:12:42,240
 something that you can learn from somebody

220
00:12:42,240 --> 00:12:47,440
 else. No one else can enlighten you simply by listening, by

221
00:12:47,440 --> 00:12:49,520
 studying or by joining a

222
00:12:49,520 --> 00:12:52,680
 group, becoming a monk or a nun or so on. Without

223
00:12:52,680 --> 00:12:55,600
 practicing it's not of any use at all. It's

224
00:12:55,600 --> 00:13:03,310
 not something which can bring one to any state of release

225
00:13:03,310 --> 00:13:06,280
 or freedom. It's the one way meaning

226
00:13:06,280 --> 00:13:10,840
 it's the way only of the Buddha or it's the way of only

227
00:13:10,840 --> 00:13:14,320
 someone who is a Buddha. So someone

228
00:13:14,320 --> 00:13:18,040
 who is not a Buddha could not teach this, could not explain

229
00:13:18,040 --> 00:13:19,800
 it, could not come up with

230
00:13:19,800 --> 00:13:23,970
 this because they wouldn't have seen clearly the nature of

231
00:13:23,970 --> 00:13:26,840
 reality. Because of their confusion

232
00:13:26,840 --> 00:13:30,130
 and their misunderstandings or still exist in their mind,

233
00:13:30,130 --> 00:13:31,720
 they wouldn't be able to have

234
00:13:31,720 --> 00:13:34,130
 the right to be called a Buddha and because they were not a

235
00:13:34,130 --> 00:13:35,400
 Buddha they wouldn't be able

236
00:13:35,400 --> 00:13:42,400
 to teach this. So only a Buddha could teach this, that's

237
00:13:42,400 --> 00:13:44,920
 another meaning.

238
00:13:44,920 --> 00:13:50,300
 And finally, it leads to only one goal. It leads to nirvana

239
00:13:50,300 --> 00:13:52,700
 or freedom from suffering.

240
00:13:52,700 --> 00:13:56,180
 So it's not by practicing this path you don't have to say

241
00:13:56,180 --> 00:13:58,640
 maybe it'll lead you in this direction,

242
00:13:58,640 --> 00:14:01,400
 maybe it'll lead you in that direction. As people who

243
00:14:01,400 --> 00:14:02,840
 practice meditation can see, the

244
00:14:02,840 --> 00:14:06,360
 more they practice, the closer they become to freedom and

245
00:14:06,360 --> 00:14:08,040
 the more freedom and peace

246
00:14:08,040 --> 00:14:15,820
 of mind they're able to obtain from the practice. So some

247
00:14:15,820 --> 00:14:17,960
 people object to the idea that this

248
00:14:17,960 --> 00:14:22,150
 is the only way, that the practice of mindfulness could be

249
00:14:22,150 --> 00:14:24,200
 the only way and any other way is

250
00:14:24,200 --> 00:14:29,020
 not perfect, is not right. Well, there are many other ways

251
00:14:29,020 --> 00:14:31,640
 you can constrict this to,

252
00:14:31,640 --> 00:14:34,970
 you can translate this word, ekayano, you could translate

253
00:14:34,970 --> 00:14:36,440
 it simply as not leading to

254
00:14:36,440 --> 00:14:40,140
 any other destination. If you practice this it's the direct

255
00:14:40,140 --> 00:14:41,400
 way or so on. But really I

256
00:14:41,400 --> 00:14:46,500
 think it's clear that if you're not being mindful, if you

257
00:14:46,500 --> 00:14:49,400
're not being present and alert

258
00:14:49,400 --> 00:14:53,800
 and a tent on reality, then it's not possible by any

259
00:14:53,800 --> 00:14:56,720
 stretch of the imagination for you

260
00:14:56,720 --> 00:15:00,720
 to think that you could become, you could come to see

261
00:15:00,720 --> 00:15:03,440
 clearly about reality, you could

262
00:15:03,440 --> 00:15:06,670
 become free from suffering. Because our sufferings of

263
00:15:06,670 --> 00:15:09,480
 course come directly from a misunderstanding,

264
00:15:09,480 --> 00:15:17,720
 from a misapprehension of things other than what they are.

265
00:15:17,720 --> 00:15:22,870
 Okay, so there are five great goals in Buddhism and this is

266
00:15:22,870 --> 00:15:25,680
 what the Lord Buddha starts off

267
00:15:25,680 --> 00:15:28,500
 by saying, he says, "Through the practice of the Four Found

268
00:15:28,500 --> 00:15:29,720
ations of Mindfulness, we

269
00:15:29,720 --> 00:15:37,850
 can come to the purification of our minds, we come to

270
00:15:37,850 --> 00:15:40,280
 overcome sorrow and lamentation,

271
00:15:40,280 --> 00:15:43,490
 we're able to destroy suffering and grief, we're able to

272
00:15:43,490 --> 00:15:45,000
 reach the right path when we

273
00:15:45,000 --> 00:15:49,470
 are able to attain nirvana." Now anyone who's watched some

274
00:15:49,470 --> 00:15:51,720
 of the videos that I made on

275
00:15:51,720 --> 00:15:56,420
 YouTube may recognize these as being the basis for my top

276
00:15:56,420 --> 00:15:59,240
 five reasons why everyone should

277
00:15:59,240 --> 00:16:01,880
 practice meditation. And I don't mention in those videos

278
00:16:01,880 --> 00:16:03,260
 where I got them from, but this

279
00:16:03,260 --> 00:16:07,080
 is directly where they came from. These are not some

280
00:16:07,080 --> 00:16:09,800
 arbitrary list that I came up with,

281
00:16:09,800 --> 00:16:12,310
 it's the Buddhist teaching. And these are the top five

282
00:16:12,310 --> 00:16:14,200
 reasons why everyone should practice

283
00:16:14,200 --> 00:16:17,490
 meditation. I think these are the greatest reason why

284
00:16:17,490 --> 00:16:19,880
 anyone should ever have for practicing

285
00:16:19,880 --> 00:16:24,380
 meditation. First of all, that it purifies the mind that

286
00:16:24,380 --> 00:16:26,020
 when we are able to see things

287
00:16:26,020 --> 00:16:29,860
 and to acknowledge things for what they are and not make

288
00:16:29,860 --> 00:16:31,960
 more of them than they really

289
00:16:31,960 --> 00:16:36,490
 are, then our minds become pure, there's no greed, there's

290
00:16:36,490 --> 00:16:38,680
 no anger, there's no wanting

291
00:16:38,680 --> 00:16:42,080
 things to be other than they are and so on. And this is the

292
00:16:42,080 --> 00:16:43,600
 first great reason why we

293
00:16:43,600 --> 00:16:47,720
 practice meditation. We're not overwhelmed by our emotions.

294
00:16:47,720 --> 00:16:48,520
 As a result of that, our

295
00:16:48,520 --> 00:16:53,140
 sorrow and lamentation is done away with any kind of upset,

296
00:16:53,140 --> 00:16:55,560
 any kind of mental depression

297
00:16:55,560 --> 00:17:00,650
 or anxiety, worries, fears are all done away with as a

298
00:17:00,650 --> 00:17:03,880
 result of having no impurities in

299
00:17:03,880 --> 00:17:10,230
 the mind. As a result of not getting upset at things, then

300
00:17:10,230 --> 00:17:13,440
 there's no suffering. So even

301
00:17:13,440 --> 00:17:17,880
 bodily suffering can be overcome. As we practice meditation

302
00:17:17,880 --> 00:17:20,520
, we're able to see through the

303
00:17:20,520 --> 00:17:25,760
 pain that arises in meditation. So when there's pain, we

304
00:17:25,760 --> 00:17:28,520
 can see it simply as a sensation

305
00:17:28,520 --> 00:17:35,410
 and not become upset or distressed by it. This is

306
00:17:35,410 --> 00:17:36,680
 considered to be the right path, so

307
00:17:36,680 --> 00:17:39,170
 it means attaining the right path. Once we're living our

308
00:17:39,170 --> 00:17:40,480
 lives in this way where we see

309
00:17:40,480 --> 00:17:45,770
 and understand everything the way it is, then this is

310
00:17:45,770 --> 00:17:49,080
 considered to be the right way to

311
00:17:49,080 --> 00:17:52,030
 live one's life no matter what you're doing. So you can be

312
00:17:52,030 --> 00:17:53,440
 a monk or not a monk, a nun

313
00:17:53,440 --> 00:17:57,760
 or not a nun. You can be a Buddhist or not a Buddhist, but

314
00:17:57,760 --> 00:17:59,800
 if you're living your life

315
00:17:59,800 --> 00:18:03,380
 alert and aware, this is considered to be the right path.

316
00:18:03,380 --> 00:18:04,320
 And finally, the attainment

317
00:18:04,320 --> 00:18:11,110
 of nirvana, which simply means freedom from suffering,

318
00:18:11,110 --> 00:18:14,440
 which is being totally free from

319
00:18:14,440 --> 00:18:19,590
 any sort of stress or suffering at all. And that is kind of

320
00:18:19,590 --> 00:18:21,920
 like a supreme state. So it's

321
00:18:21,920 --> 00:18:25,360
 not just saying, "Oh, now I don't have any pain in my back,

322
00:18:25,360 --> 00:18:26,960
 so now that's nirvana." It's

323
00:18:26,960 --> 00:18:30,780
 a supreme state where there is absolutely no more suffering

324
00:18:30,780 --> 00:18:32,560
 ever again. It's kind of

325
00:18:32,560 --> 00:18:36,200
 like the end goal is. It's fine if that's not easy to

326
00:18:36,200 --> 00:18:38,600
 understand because it shouldn't

327
00:18:38,600 --> 00:18:42,850
 be. It's something that takes time and effort through the

328
00:18:42,850 --> 00:18:45,000
 practices to be realized.

329
00:18:45,000 --> 00:18:48,790
 So what are the four arousings of mindfulness? The four are

330
00:18:48,790 --> 00:18:51,040
 the body, the feelings, and here

331
00:18:51,040 --> 00:18:55,710
 by feelings we simply mean physical sensations, and the

332
00:18:55,710 --> 00:18:59,040
 mind, and then dhammas. Here dhamma

333
00:18:59,040 --> 00:19:02,300
 is translated as mental objects. Actually, mental object is

334
00:19:02,300 --> 00:19:03,720
 a very poor translation in

335
00:19:03,720 --> 00:19:08,230
 this case. What it really means is teachings. Dhamma here

336
00:19:08,230 --> 00:19:10,680
 is the teachings of the Lord Buddha,

337
00:19:10,680 --> 00:19:13,910
 various teachings, various groups of things that the Lord

338
00:19:13,910 --> 00:19:16,120
 Buddha has taught, various important

339
00:19:16,120 --> 00:19:21,400
 groups of realities or mind states or even physical

340
00:19:21,400 --> 00:19:24,920
 phenomenon that we have to keep in

341
00:19:24,920 --> 00:19:28,640
 mind. And we'll get to that next week. This week I'm going

342
00:19:28,640 --> 00:19:30,040
 to try to just skim through

343
00:19:30,040 --> 00:19:34,690
 the body. So these four foundations are considered to be a

344
00:19:34,690 --> 00:19:37,600
 good basis of reality. If we want

345
00:19:37,600 --> 00:19:42,290
 to understand what is reality, these four foundations sort

346
00:19:42,290 --> 00:19:44,240
 of put it all together and

347
00:19:44,240 --> 00:19:46,690
 there's nothing that we will encounter in meditation that

348
00:19:46,690 --> 00:19:47,760
 we can't put under one of

349
00:19:47,760 --> 00:19:51,200
 these categories. The first three are pretty obvious. We

350
00:19:51,200 --> 00:19:53,440
 have the body and that's the physical.

351
00:19:53,440 --> 00:19:57,660
 We have sensations of pain and happiness and equanimity.

352
00:19:57,660 --> 00:19:59,720
 And then there's the mind, which

353
00:19:59,720 --> 00:20:03,150
 is thinking. Dhammas just means everything else. It's got

354
00:20:03,150 --> 00:20:04,880
 emotions in there. There's

355
00:20:04,880 --> 00:20:07,810
 the senses, seeing, hearing, smelling, tasting, feeling,

356
00:20:07,810 --> 00:20:09,720
 thinking. There's even the eightfold

357
00:20:09,720 --> 00:20:14,800
 noble path is in there. So these are things which are, the

358
00:20:14,800 --> 00:20:16,520
 dhammas are just sort of a

359
00:20:16,520 --> 00:20:18,970
 miscellany of things which are going to be useful and going

360
00:20:18,970 --> 00:20:20,000
 to come into play during

361
00:20:20,000 --> 00:20:23,300
 the meditation that is sort of going to lead us up and

362
00:20:23,300 --> 00:20:25,680
 onward in the practice. And there's

363
00:20:25,680 --> 00:20:29,900
 something that often teachers will bring up slowly as the

364
00:20:29,900 --> 00:20:32,280
 meditator progresses. And in

365
00:20:32,280 --> 00:20:34,470
 the beginning we can focus on the fourth one as being

366
00:20:34,470 --> 00:20:36,240
 emotions because in the very beginning

367
00:20:36,240 --> 00:20:39,650
 that's all that becomes readily apparent to the meditator.

368
00:20:39,650 --> 00:20:41,440
 So we understand it to be negative

369
00:20:41,440 --> 00:20:47,820
 emotions like liking, anger, frustration, boredom, sadness,

370
00:20:47,820 --> 00:20:51,760
 wanting, drowsiness, distraction,

371
00:20:51,760 --> 00:20:57,350
 doubt. So these are states of mind. So we can look at this

372
00:20:57,350 --> 00:20:58,440
 as mental objects, at least

373
00:20:58,440 --> 00:21:03,270
 in the beginning, or mind states. We can understand it to

374
00:21:03,270 --> 00:21:05,200
 be in the beginning.

375
00:21:05,200 --> 00:21:08,670
 And what do we do? We contemplate the body in the body.

376
00:21:08,670 --> 00:21:11,080
 This is number one. Contemplating

377
00:21:11,080 --> 00:21:14,520
 the body in the body. What does this mean? It's simply a

378
00:21:14,520 --> 00:21:16,540
 Pali colloquialism. It's a way

379
00:21:16,540 --> 00:21:21,070
 of saying that in regards to the body we focus on the body.

380
00:21:21,070 --> 00:21:23,280
 Or when focusing on the body

381
00:21:23,280 --> 00:21:28,680
 we focus specifically on the body. If we're going to look

382
00:21:28,680 --> 00:21:31,640
 at the body then we don't mix

383
00:21:31,640 --> 00:21:34,680
 it with the feelings. We separate the four out. So in

384
00:21:34,680 --> 00:21:36,160
 regards to the body we look at

385
00:21:36,160 --> 00:21:39,120
 the body. When we're using the body as our meditation

386
00:21:39,120 --> 00:21:41,360
 object we're focusing on the body.

387
00:21:41,360 --> 00:21:43,620
 That's all this means. And it can be quite confusing and

388
00:21:43,620 --> 00:21:44,880
 people don't understand. In

389
00:21:44,880 --> 00:21:47,160
 fact there are some schools of meditation that have

390
00:21:47,160 --> 00:21:48,680
 actually taken this to mean that

391
00:21:48,680 --> 00:21:51,590
 inside of this body there is another body. And inside of

392
00:21:51,590 --> 00:21:53,440
 that body there is another body.

393
00:21:53,440 --> 00:21:55,520
 And they go deeper and deeper and deeper trying to find

394
00:21:55,520 --> 00:21:57,040
 these bodies inside of bodies. And

395
00:21:57,040 --> 00:22:00,470
 they go, you can find this on the internet. It's kind of a

396
00:22:00,470 --> 00:22:02,240
 humorous understanding of what

397
00:22:02,240 --> 00:22:06,760
 it means by body in the body when it's just a Pali gram

398
00:22:06,760 --> 00:22:09,920
matical structure which of course

399
00:22:09,920 --> 00:22:12,620
 is the way it would have to be. Pali is a much different

400
00:22:12,620 --> 00:22:13,840
 language than English in the

401
00:22:13,840 --> 00:22:16,920
 way it constructs itself is different and that's all. It

402
00:22:16,920 --> 00:22:18,800
 doesn't mean anything spiritual

403
00:22:18,800 --> 00:22:22,900
 at all. It just means we separate the four out. For

404
00:22:22,900 --> 00:22:25,120
 instance when we're walking, have

405
00:22:25,120 --> 00:22:27,690
 you ever seen people do walking meditation? What they do is

406
00:22:27,690 --> 00:22:28,880
 they focus on their feet.

407
00:22:28,880 --> 00:22:31,520
 As the foot is moving you're focusing only on the foot. You

408
00:22:31,520 --> 00:22:32,640
're not focusing on your

409
00:22:32,640 --> 00:22:36,220
 thoughts. You're not focusing on feelings. You're focusing

410
00:22:36,220 --> 00:22:37,600
 on a part of the body. So

411
00:22:37,600 --> 00:22:40,410
 in regards to the whole body you pick a piece of the body

412
00:22:40,410 --> 00:22:42,160
 and you focus on that. And when

413
00:22:42,160 --> 00:22:45,840
 the foot moves they say stepping right and when the left

414
00:22:45,840 --> 00:22:47,920
 will move, step being left or

415
00:22:47,920 --> 00:22:50,910
 so on. When lifting the foot they say lifting, when placing

416
00:22:50,910 --> 00:22:52,320
 the foot they say placing and

417
00:22:52,320 --> 00:22:58,320
 so on. Using the body as a meditation object. Normally when

418
00:22:58,320 --> 00:22:59,360
 we're sitting meditation we'll

419
00:22:59,360 --> 00:23:02,570
 focus on the abdomen. When the stomach rises we say to

420
00:23:02,570 --> 00:23:04,800
 ourselves rising. When the stomach

421
00:23:04,800 --> 00:23:07,820
 falls we say falling. Just focusing on a piece of the body

422
00:23:07,820 --> 00:23:09,440
 because when you're sitting there's

423
00:23:09,440 --> 00:23:16,500
 not much movement going on. And the same with the feelings,

424
00:23:16,500 --> 00:23:17,600
 the same with the mind, the

425
00:23:17,600 --> 00:23:26,160
 same with the dhammas or the mental mind states and so on.

426
00:23:26,160 --> 00:23:27,680
 And then there's this phrase having

427
00:23:27,680 --> 00:23:31,880
 overcome in this world covetousness and grief. And there's

428
00:23:31,880 --> 00:23:36,080
 a lot of argument over the translation

429
00:23:36,080 --> 00:23:40,360
 of this one. It most likely means for the purpose of

430
00:23:40,360 --> 00:23:44,000
 overcoming covetousness and grief. And

431
00:23:44,000 --> 00:23:47,120
 the argument goes thus that if it were to mean having

432
00:23:47,120 --> 00:23:49,600
 overcome then the point of meditating

433
00:23:49,600 --> 00:23:51,530
 would have already been accomplished. Why are you sitting

434
00:23:51,530 --> 00:23:52,480
 and meditating when you've

435
00:23:52,480 --> 00:23:56,190
 already overcome covetousness and grief or liking and disl

436
00:23:56,190 --> 00:23:58,000
iking, all kinds of mental

437
00:23:58,000 --> 00:24:02,310
 defilements, all kinds of anger and depression, all kinds

438
00:24:02,310 --> 00:24:04,800
 of suffering states. If they've

439
00:24:04,800 --> 00:24:09,210
 already been overcome then why are you practicing

440
00:24:09,210 --> 00:24:12,160
 mindfulness? And that is the goal as we talked

441
00:24:12,160 --> 00:24:15,840
 about above. And the word vinaya could mean either way. It

442
00:24:15,840 --> 00:24:17,920
 could either mean having overcome

443
00:24:17,920 --> 00:24:20,620
 or it could mean for the purpose of overcoming. And the

444
00:24:20,620 --> 00:24:22,480
 general agreement is that no, this is

445
00:24:22,480 --> 00:24:24,860
 a poor translation. It should mean for the purpose of

446
00:24:24,860 --> 00:24:26,800
 overcoming. But you could look at it as

447
00:24:26,800 --> 00:24:31,470
 having given up one's attachments to the world. It means

448
00:24:31,470 --> 00:24:34,640
 gone off and just said enough. I no longer

449
00:24:34,640 --> 00:24:37,700
 want to get involved in the world. I'm going to try to give

450
00:24:37,700 --> 00:24:39,680
 this up. And so in a sense you have

451
00:24:39,680 --> 00:24:45,700
 given up or gone beyond your worldly cares, at least for a

452
00:24:45,700 --> 00:24:48,800
 time. So you can look at it either

453
00:24:48,800 --> 00:24:51,570
 way. But I think it is quite proper to say that no, you

454
00:24:51,570 --> 00:24:53,600
 haven't given these things up. But this is

455
00:24:53,600 --> 00:24:57,060
 what our purpose is. And this is a very important purpose

456
00:24:57,060 --> 00:24:59,280
 in the practicing of meditation.

457
00:24:59,920 --> 00:25:03,490
 And when we practice we should try to give up our likes and

458
00:25:03,490 --> 00:25:06,800
 dislikes, our attachments and aversions.

