 Okay.
 Welcome everyone to a weekly Sunday talk.
 I apologize for any of you who showed up last week.
 I had to go out of town.
 And I did let them know in advance, but I don't think any
 notices went out, so...
 Anyway, I'm here today, so welcome. Good to see people
 coming.
 So today I'd like to talk about how to make progress and
 how to find success in the meditation practice.
 I think that the how-to idea has great importance in
 Buddhism.
 It's a very good way to approach the Buddhist teaching,
 having how-to guides, how-to this, how-to that.
 And so, of course, the first thing is how to meditate, how
 to practice, how to follow the Buddhist teaching.
 But another important how-to is how to meet with success in
 your practice, how to practice meditation in such a way
 that you actually come closer to the goal,
 when you actually progress and reach the goal.
 Because there's often the complaint that these teachings
 are all nice and good, but impossible to practice in daily
 life, or very difficult to practice.
 Or hard to understand, get this a lot.
 It sounds all fine and good, but I can't believe that such
 a thing is possible.
 So we need to explain what it is that makes your meditation
 practice fail and how you can work to make sure that your
 practice succeeds.
 I suppose the biggest thing that causes meditation to fail
 is inadequate instruction.
 So you pick up a book, or you go and listen to a talk, or
 you even go to a meditation center and are given basic
 instruction on how to sit, how to practice meditation.
 But because you don't have one-on-one interaction with a
 teacher, or oftentimes teachers aren't adequately trained
 to be able to answer the types of questions and to fix the
 sorts of problems that come up.
 So you get people teaching very basic meditation and not
 able to go further, but more often than that even is just
 lack of a teacher entirely.
 It's not that the teachers that exist are incompetent, it's
 that we're not able to find a teacher.
 And this can be quite discouraging, it makes you think that
 Buddhism is just a theoretical impossibility, this idea of
 attaining nibbana or nirvana, the idea of not having any
 greed or anger or delusion, the idea of enlightenment.
 And it's generally because for many people we live in a
 location where there is no teacher or else we don't feel
 ready or so interested in making the effort to go to a
 meditation center.
 So we pick up a book or we listen to a talk or we find some
 guide on meditation and when we get stuck we're not able to
 get the answers to our questions.
 So we don't have the proper guidance and it makes us afraid
 or uncertain and so we quit.
 And I've heard this a lot of people quitting because they
 got to a point where something came up and they didn't know
 how to deal with it.
 Another reason why meditation often fails, and this is
 something that occurs quite often, is that the meditation
 practice is out of whack with our daily life.
 Our way of life is not in line with the meditation practice
.
 People who take drugs or alcohol can often be interested in
 the practice of meditation and so will come to practice.
 There have even been stories of people going to meditation
 centers and bringing marijuana or other drugs and sneaking
 off and getting high during the meditation course,
 thinking that somehow it's the same thing, getting stoned
 and becoming enlightened, have some connection somehow.
 Other things like people who have wrong livelihood or so on
, or who are simply unprepared for the nature of the
 meditation.
 So people who think that meditation is kind of a relaxation
 and who have no interest whatsoever in giving up their add
ictions and their attachments,
 people who are sort of trying meditation for fun.
 And we'll go to do a meditation course, thinking of it kind
 of as a vacation, and when they go back to their lives they
 have no intention of changing their lives.
 They have no intention of making their lives more med
itative.
 Or though they might have that sort of intention, they don
't realize what it entails, that it entails giving up much
 of our ambition and much of our pursuit and much of our
 addiction, much of our busyness.
 So we go back unprepared to change. And that's really a
 problem because of course enlightenment is a great change.
 We're not trying to come here to add something on to who we
 are. We're coming here to change things about who we are.
 If you're not interested in changing yourself, then there's
 not much that the meditation practice can do for you.
 Except gives you high states of concentration, which are in
 addition to your ordinary state of existence.
 And so for many people that's all meditation is. It's a
 break, it's a relief. You come meditate, you get highly
 focused, maybe see some strange things, maybe experience
 some funny things,
 come back to your daily life, it wears off and you go back
 to who you were.
 This is of course not what we mean by meditation and it's
 certainly not the way to become enlightened.
 So how do we succeed? How do we actually bring the
 meditation into our lives and say you're a person who's new
 to Buddhism and new to meditation and you want to start med
itating.
 What can you do to actually succeed in incorporating it
 into your life?
 Well, there are steps that we can follow. And the first
 thing to note is that we really have to be in it for the
 long haul.
 We have to think of meditation as a part of our lives. When
 we approach the practice of meditation, it's not a break,
 it's not a vacation, it's not a temporary thing.
 It's a skill that we're hoping to acquire and bring into
 our ordinary lives, bring into our lives and become a part
 of who we are.
 This is the attitude we should approach it with. It doesn't
 mean we have to be at the point where we're ready to change
 everything.
 In the beginning we just have to understand that it's going
 to be a long-term thing. You can't expect to go to a
 meditation center for 10 days and become enlightened.
 You shouldn't expect to gain much from 10 days. It's a
 wonderful thing to do and it's a very important part as I
'll explain.
 But the biggest problem that comes from doing meditation
 courses is the high expectations that come along with it.
 We expect that after a meditation course we should be
 enlightened. We should have attained some high state of
 realization.
 And I would say that when you do a meditation course there
 are states of realization that are gained.
 They're generally not on the level that we assume them to
 be, that we would expect them to be.
 So you get people leaving from a meditation course all full
 of themselves thinking they're enlightened already.
 They go home and they start to teach other people and they
 start to act as though they're fully enlightened and
 everything they say is right.
 When people don't agree with them they get angry and
 realize that they're not enlightened, feel all depressed
 and think they gain nothing out of the practice.
 It was a fake and a sham and so on. And often give up
 meditation as a result.
 Not often but can give up meditation. At the very least
 they feel discouraged and confused and go through quite a
 bit of unnecessary suffering because of their high
 expectations.
 So it's going to be a long haul. It's a skill that we're
 going to gain. It's a training.
 What we're learning in a meditation course is a path of
 training.
 The 10 days or the 15 days or the one month or so on is not
 the end of our lives.
 It's not like we go in the meditation center, come out and
 we're perfect.
 What we're gaining is a means to then continue our life in
 a way so that we're always developing ourselves towards the
 ultimate goal.
 So the first thing you need to succeed in meditation is a
 demonstration. Someone has to show you how to meditate.
 I think in many cases this can supplant any sort of
 theoretical knowledge and understanding.
 With a qualified demonstrator or demonstration you don't
 really need to understand all of these high and mighty
 Buddhist concepts.
 All you need is someone to show you what we're doing and
 explain why we're doing it. Explain what everything means.
 So for instance, I know many places that teach the same
 sort of practice that I teach in Thailand for example, many
, many monasteries.
 I'd say probably a majority of monasteries in Thailand are
 capable of teaching basic meditation practice.
 But they're generally not capable of explaining why we're
 meditating and why we're doing these odd things that we do
 in meditation.
 For instance, you sit and we tell you how to watch the
 stomach.
 When the breath comes into the body the stomach will rise.
 When the breath goes out of the body the stomach will fall.
 And what we have you do is when the stomach rises we have
 you say to yourself, "Rising."
 When it falls we have you say, "Falling, rising, falling."
 And that's a basic meditation practice.
 The problem is even today a woman came to see me and I had
 a bit of difficulty explaining to her because she wasn't
 really interested in hearing what I had to say.
 She's a Thai woman and she practices watching her breath
 and she wasn't interested.
 She probably tried, she even said she had tried this
 practice and she'd gone even for a week to do this practice
 but didn't really understand why.
 What's the point and what's the benefit?
 It seems kind of mundane that we're watching something that
 is quite unrelated to our state of mind and our state of
 our way of life.
 Watching our stomach seems like a very pointless exercise.
 So the proper instruction, the demonstration of how to
 practice should include an explanation of why we practice.
 And this is one of the difficulties that we find that there
's generally not this level of explanation.
 Why we do the rising and falling, or any meditation
 technique for that matter, is as a means of training our
 mind in a certain task or developing a certain ability in
 our mind.
 We're not interested in the stomach per se. There's nothing
 special about this rising and falling movement except that
 it's the only movement that occurs in a relaxed body.
 When you first start meditating it in fact might not be
 clear at all.
 You'll find yourself breathing from the chest or you'll
 find that when you breathe in your stomach actually falls
 or goes inward.
 When you breathe out your stomach expands.
 And this was the case of the woman today and I had to have
 her lie down on her back before she was able to see that
 indeed in a natural state the rising of the stomach is
 accompanied with the in-breath.
 When we breathe in our stomach should expand and you can
 test this by lying on your back.
 So the reason we use it is because it should be the only
 movement of the body. The point is we're trying to look at
 reality.
 We're trying to teach our mind that when something arises
 it should be seen as it is.
 When the stomach rises we should see it as rising.
 When anything arises we should see it just for what it is
 because this is going to be the fundamental talent that we
're trying to gain.
 The fundamental ability that we're trying to train in
 ourselves.
 This ability to see things as they are. When someone yells
 at you to see it for what it is.
 It's a sound touching your ear. There's things going on in
 their mind and to not turn it into an issue where you've
 got this person and I am being attacked.
 This attachment to self and so on. To be able to break
 reality up into its component parts and see it for what it
 is.
 When you feel pain in the body to be able to see it just
 for pain and not get upset or attached to it.
 We're using the rising and the falling in this case as a
 means of training this talent, this ability to see things
 for what they are.
 The other thing is that it doesn't really matter what you
 use to meditate.
 In the beginning you're going to see a big difference
 between watching the rising and the falling and watching
 your emotions.
 When you watch your emotions you can see an immediate
 result that you find yourself letting go of your anger,
 letting go of your greed.
 When you watch the pain in the body you'll be able to see
 yourself letting go.
 The rising and falling seems like a rather useless object.
 But the other interesting thing is this idea that
 everything is the same, that all objects are equal.
 You can gain wisdom by watching absolutely anything as long
 as that object is real.
 This is very clearly observable in the rising and falling.
 This is to the extent that it's amazing the benefits that
 you can gain from such a mundane activity.
 You can become enlightened watching your stomach.
 I guarantee that anyone who tries this will be able to
 verify it.
 The problem is when you start to verify it, it seems like
 you're doing something wrong.
 I will explain.
 What we're trying to realize in Buddhism on the path is we
're trying to see the three characteristics of existence.
 We're trying to see that nothing in the world is worth
 clinging to.
 We're trying to get this through our heads that there's
 nothing worth clinging to.
 Clinging to any specific thing in the world is not of any
 benefit to us.
 We're trying to see the true nature of everything that
 arises, that there's nothing good or bad about anything.
 And so to be able to experience all of reality for what it
 is, to live a natural, a balanced and a free life.
 The three characteristics that we're going to see are imper
manence, suffering and non-self.
 And these exist in absolutely everything inside of us and
 the world around us.
 And this is what makes it like it or not, makes it the
 truth that nothing in the world is worth clinging to.
 First of all, impermanence.
 When you watch, say, the rising and the falling, you'll see
 it changing.
 You'll see in the beginning it can be quite easy.
 Rising, falling, rising, falling.
 And then suddenly it changes.
 Suddenly the rising gets stuck or the belly gets stiff or
 uncomfortable.
 Maybe suddenly it disappears.
 Suddenly it's no longer rising and falling.
 You can't find it.
 And then it comes back and then it goes.
 And this can be quite frustrating because we expect things
 to be stable.
 We expect things to be constant.
 If it's not constant, we don't want to have anything to do
 with it.
 So we're always on the search for finding something that is
 stable, finding stability,
 finding something, a way of life that is going to let us be
 that way forever.
 This is why these theistic religions are so appealing, the
 idea that you can have eternal life.
 In fact, the funny story is I had the Mormons come and
 visit me yesterday
 and they were trying to make a pitch for the kingdom, the
 celestial kingdom of God.
 And he says, "Well, what's the difference between these
 different kingdoms?"
 And they said, "Well, in this third kingdom you get to be
 with your family for the rest of eternity."
 And I thought, "Oh dear, that's not really a very good
 selling point, not for many of us anyway."
 But this is the kind of thing that they've been indoctr
inated and it really appeals if you think about it.
 And if you're indoctrinated from an early age, you would
 tend to develop love and harmony with your family.
 And as a result, it would really appeal to people to be
 able to have this stability.
 The problem is it's not the truth. There is no stability.
 There's no permanence to be found in the universe.
 The second one is suffering. And the word suffering here,
 sort of the cornerstone of Buddhism,
 and this one that everyone's always afraid of. Teachers are
 afraid to teach it.
 Newcomers to Buddhism are afraid to hear it. And it leads
 people away from Buddhism.
 It leads opponents to Buddhism to make all sorts of crazy
 accusations.
 The meaning of suffering is that everything in the world is
 unsatisfying, is suffering.
 The suffering comes from clinging to it.
 Because when you cling to something that's unstable, you
 suffer when it's gone.
 This is why we don't like things like watching our stomach.
 This is why when you start to do it, it seems like you're
 doing something wrong.
 Or it seems like this meditation is not a good meditation
 because you practice it and it makes you suffer.
 It seems like watching the rising, the falling, is
 unpleasant.
 Why is it unpleasant? Because you're trying to keep it
 permanent. You're trying to force your breath.
 It's the case with everybody when you start doing it.
 The truth is we try to force everything in our lives. We
 can't just experience things.
 We react immediately. "Oh, no, this is no good. It has to
 be like this."
 Or, "Oh, yes, this is good. Stay like that."
 We're always trying to force things to be the way we expect
 them.
 Because of our limited intelligence and wisdom, that's
 generally quite far out of touch with the way things really
 are.
 The way we expect things to be is not in tune with reality.
 So as a result, when the rising, falling, and then it
 changes,
 or it's not deep enough, or it's not coming at the time
 that we want it to.
 We're watching it. It hasn't come yet. We make it rise.
 Or we expect it to be smooth.
 We immediately try to find this rhythm where it would be
 totally comfortable all the time.
 Just like music, you'd have this rhythm of your breath,
 which would be totally comfortable and peaceful.
 We want that. The problem is reality is not like that.
 If you want that, you have to turn on the stereo and turn
 on something fake, something created, because reality doesn
't work that way.
 So as a result, we suffer. Just a simple thing like
 watching the rising and falling makes people suffer,
 because they're not able to keep up with the rhythm of life
.
 They're not in tune with the rhythm of nature, the reality
 in front of them, which is a totally inconstant rhythm.
 It's changing all the time. It's a lot more like jazz than
 bebop.
 So for this reason, it's very easy for people to fail even
 when they've started to practice correctly.
 They think something's wrong. And I get this question a lot
. This is the role of a teacher.
 So the second thing that you need besides a good
 explanation is a good teacher, someone who's able to
 explain these things to you.
 You have to, once you've been explained how to practice,
 shown how to practice and explained why we're practicing,
 so that when you do it, you understand why you're doing it.
 And you know in which direction to extend your mind. You're
 not trying to force things. You're just trying to watch it.
 What you should be gaining is just a sense that this is the
 rising, this is the falling.
 And you should look at it as a training, not expecting
 anything else to appear,
 where it's a rising, falling, and suddenly you start to see
 angels or start to see heaven or so on.
 You should be thinking that you're doing this to train your
 mind, so that then when anything else arises, you're able
 to see it clearly as well.
 Then the next thing you need is an instructor, someone to
 lead you through the practice and explain what you are
 seeing.
 And to point out to you that what you're seeing, there's
 nothing wrong with what you're seeing.
 There can't be anything wrong with what you're seeing. What
 you're seeing is the nature of that object.
 We mistakenly assume that we're doing something wrong or
 there's something wrong with this type of practice.
 Where if we analyze it logically and rationally, we're
 actually not doing anything except watching it and
 reminding ourselves it is what it is.
 Saying to ourselves, "This is rising. This is falling."
 Recognizing it for what it is.
 Like when you see a friend and you're like, "Oh, that's
 this person. That's that person."
 The problem is our minds go so much further and our minds
 expect things to be in a different way.
 So the job of the teacher is to remind you of these things,
 remind you that actually what you're seeing is reality.
 The problem is not the objects. The problem is your
 expectations. You think it should be other than that and so
 you suffer.
 Even pain that arises, sometimes when you say to yourself,
 "Pain, pain, pain."
 Just reminding yourself, recognizing it as pain.
 Sometimes the pain gets worse and this is where people
 really start to freak out or become sure in their mind that
 there's something wrong with this practice.
 Because the idea though it's generally pretty clearly
 stated that this isn't the intention.
 The idea that people have in their minds is the
 acknowledgement somehow gets rid of all your suffering,
 somehow removes the pain.
 So you say to yourself, "Pain, pain, pain." And poof, the
 pain is gone.
 And that's not at all the point. The point of saying to
 yourself, "Pain, pain, pain."
 Is to teach your mind the correct way of responding to the
 object.
 Instead of, "Bad, bad, bad." Or, "What am I going to do
 about this?"
 And going on and on in your mind about what's wrong with
 this terrible pain.
 You're just saying to yourself, "Pain, pain." You're just
 knowing it for what it is.
 When you see the pain you say, "Oh, this is pain." And you
 remind yourself again and again.
 To stop yourself from turning something, one thing into
 many things that it's not.
 What it is, is pain. And that's really all it is.
 The idea that it's I, me, mine, good, bad, is totally
 external to the reality.
 The next thing you need to do is to set up a schedule for
 your practice.
 And scheduled practice comes in many forms and it's worth
 discussing these different forms.
 The best type of scheduled practice, I would say, is the
 retreat.
 It is the intensive meditation course.
 And the problem, as I said, is that we often are people who
 are keen on doing retreats,
 tend to segregate their existence into two modes.
 The retreat mode and the daily life mode.
 And are not interested or willing to bring these two
 together.
 There's a difficulty here because the retreat is a very
 intensive way of life.
 And the problem generally is if you're coming from a life
 where you're not meditating,
 where your mind is not ready to accept this intensity of
 practice,
 you wind up suppressing most of your defilements.
 So the results that you get from a short course can be
 profound on a certain level,
 but are never going to likely get at the deep defilements
 because most of them are simply suppressed.
 In Buddhism there are many levels of defilement.
 There are the course defilements, the refined ones, and the
 very subtle and hard to understand
 or hard to realize, hard to see defilements.
 And in order to get rid of the course ones, you have to
 cover up so many more of them.
 In the practice we aren't able to deal with or come to
 terms with all of the problems that exist in our minds.
 So we wind up crushing simply through our tendency or our
 habit to do so, most of the defilements.
 We aren't able to see the intricacies of our mind.
 And that's the only way we could survive in such an intense
 meditation practice.
 So short retreats tend to bring about a strong amount of
 concentration
 and are only able to cut off a certain level of the defile
ments.
 But I still think it's the best way.
 It's just that you shouldn't see it as 10 days and then
 back to life as normal.
 You should think, "Okay, this is a way of helping me to
 change my life.
 This is going to give me some tools, some understanding. It
's going to lift me up for sure."
 But it's also going to be an example, the purest example of
 how I could possibly live my life.
 For most of us it's not possible to get into that state of
 constantly meditating,
 constantly doing a formal meditation practice.
 Most people have jobs or commitments of various sorts.
 So the best they can do is to take a vacation and go off
 and do a retreat.
 So that's great because you're certainly going to get some
 level of attainment.
 But the important lesson that we have to bring from the
 meditation
 is that we have to transform our lives.
 This indeed is the highest state, the state of pure
 contemplation of reality for what it is.
 These contrived states of being a professional, having a
 career, being a mother or a father,
 all of the contrived state of even being human,
 these are all limited and superficial, ephemeral,
 and not connected with true and lasting peace and happiness
.
 So what you tend to see for those people who are successful
 in meditation,
 that they tend to drastically alter their way of life.
 Even if they're living in the world, they might give up a
 good job in favor of a part time
 or a job that frees them from the burden of responsibility
 that they had before.
 And this is what you'll see. I'm not making this up.
 If you've talked to meditators, you'll see them changing
 their lives
 and you can ask them how they've changed their lives.
 You'll find them living much for the moment, not worrying
 about things like pension or retirement or so on.
 But rather trying to take as much time as they can to med
itate
 because they get this understanding that in the end we all
 have to die
 and really all that's real is our experience of sensual
 reality, no matter what position we're in.
 So they're not really concerned about stability or this
 false sort of stability and security
 that ordinary people are keen on.
 And eventually they find themselves gravitating more and
 more towards the monastic life or the meditative life.
 Sometimes they'll go and work at meditation centers,
 volunteer at monasteries,
 even to the point where they'll become male and female mon
astics.
 And so this is the one side of things, this monastic type
 of meditation retreat,
 which is really the most pure and refined sort of practice.
 But how do we begin this process of bringing the meditation
 into our life?
 I'd like to encourage everyone to take the time, to find
 the time,
 to take a vacation, find a meditation center.
 There are lots of them and generally we can find a way to
 sacrifice certain things
 in order to take the time to do a meditation course
 somewhere.
 If not in your country, then maybe in the nearby country or
 even going to Asia for instance.
 Whether you're able to do that or not, there are ways that
 you should be incorporating the meditation into your daily
 life.
 The way of bringing the two together, your daily life and
 the retreat experience,
 is to begin to practice in daily life.
 And the easiest way to do this and the way that most
 successful meditators do
 is to do a daily meditation practice, to set time aside in
 their schedule.
 And I would submit that it's easier than it sounds.
 And it's generally easier than people make it out to be.
 We think that we're so busy that we couldn't possibly have
 time for meditation.
 We think that our minds are so stressed out we couldn't
 possibly do a proper meditation.
 And the first one, the fact that we don't have enough time
 is a myth.
 I would say for most people, it's an excuse and it's the
 result of not looking close enough at our lives.
 If you've got enough time to spend a half hour on Facebook,
 then you've got enough time to meditate.
 I would say many people spend far more time than that on
 Facebook.
 And all you need to do is find five minutes a day and you
 can start meditating.
 If you can find five minutes out of what, 1440? Is that how
 many minutes there are in a day?
 24 times 60. Is that right? Yeah, 1440.
 That's one thousand four hundred and forty minutes in a day
. All you need are five.
 That's like one in, I don't know how many, one in twenty,
 one in thirty.
 Almost one in thirty. One thirtieth of your life you spend
 in meditation.
 That's how you start.
 People always ask me, how many minutes a day should you
 practice?
 And the answer is more than zero.
 If you're practicing zero minutes a day, then it's not
 enough.
 If you're practicing more than that, then you're on the
 path. You're starting.
 And it starts in this way. I guarantee that everyone,
 almost everyone in the world can find thirty minutes a day
 to meditate.
 And just by starting with five minutes, ten minutes a day,
 you can really, truly change your life.
 You can begin on a path of bringing these two together,
 bringing together the life of a meditator and the life of
 an ordinary person.
 To the point where you're living your life meditatively.
 You're living your life mindfully.
 So the next step after you've done a course, or even if you
 haven't had the chance to do a course, is to find a way to
 start practicing in your life.
 Begin to do a daily meditation practice.
 The best thing to do is do twice a day.
 Once when you wake up in the morning before you start your
 day, and once before you go to sleep or sometime in the
 evening.
 Once you start to do that, if you can do twice a day, then
 you start to incorporate it into your daily life.
 You take a break during your work or lunch hour or so on,
 and do five minutes, ten minutes of meditation.
 And you slowly increase this. If you can do some days
 twenty minutes and then eventually thirty minutes when you
 start to get good at it,
 then you'll find that you really are making a dent in
 things.
 You really are working out a lot of the problems that you
 have in your life.
 That you're able to sort out the confusion and the mess
 that is our mind, and file away a lot of the errant
 thoughts and make sense of your life in a way that wasn't
 possible before.
 The other important part of bringing meditation into your
 daily life is, as I've explained before, actually med
itating in your daily life.
 Meditation isn't something that you have to do just on the
 sitting mat.
 You don't have to expect that the only time you can med
itate is when you're sitting down on the mat with your eyes
 closed.
 The word meditation means to consider, right? Or to examine
, to somehow be aware of what's going on or of a certain
 issue, to ponder.
 So here we're pondering reality, and reality is all around
 us at all times. It's even here in second life.
 They call this virtual reality in the sense that it's not
 real. But there is reality here.
 There's a reality in the chair that you're sitting on.
 There's a reality in the light that's coming off of your
 screen, the sound that's coming out of the speakers.
 There's a reality that's going on in your mind that's
 creating all of these people.
 You look at these pixels and you see a person. You look at
 those pixels and you see a frog.
 You look at those pixels and you see somebody with black
 wings, and so on and so on.
 You create all of this in your mind. There's a reality
 going on in your mind.
 You like this. This looks attractive. This looks repulsive.
 This makes me think of that. That makes me think of this
 and so on.
 And this is the reality that goes on through our minds and
 our bodies and our world throughout our daily lives.
 You talk to someone. They make you angry. You talk to
 someone. They make you happy.
 All of these things arise and appear to us in our daily
 lives.
 The...
 Our work is simply to see things as they are, is to remind
 ourselves, it's only that. It's only this.
 It is what it is. To prevent ourselves from over-analyzing
 and judging and making more of things than they actually
 are.
 So we go in this sort of progression. The best way to start
 is to find a teacher, find a center.
 Whatever you can do, bug people. Find someone who's going
 to help you.
 And don't be shy. Don't be afraid. If you don't like it,
 you can always quit. You can always find a new teacher.
 Buddhism is not... We're not trying to convert people or
 take people as me, as my students and so on.
 We're just trying to help others. So find someone who is
 willing to help you.
 Lead you through the basics and then start to develop in
 this way to the point where you're practicing once, twice a
 day.
 And actually incorporate it into your daily life. When you
 get angry, saying to yourself, "Angry, angry."
 When you feel pain and you feel sick or so on, saying to
 yourself, "Pain, pain, aching, aching."
 When you feel depressed, depressed, depressed. When you
 feel stressed, stressed, stressed.
 Reminding you of things as they are. Reminding yourself of
 things as they are, for what they are.
 And slowly changing the way you look at things. Until your
 mind starts to get it.
 When your mind starts to get the point that it is what it
 is, reality, everything that arises.
 Simply is. Clinging is not a part of peace, happiness and
 freedom from suffering.
 Then you are on the path to enlightenment.
 And this act of getting it is what releases the mind.
 Which frees... Is that which frees the mind from clinging,
 from our attachment to samsara.
 And leads us to become enlightened.
 So, that I think is anyway my take on what it takes to
 succeed in meditation.
 I'd like to thank everyone for coming today. If you have
 any questions, I'm happy to answer them.
 Otherwise, I wish you all the best. And hope that you are
 all able to find peace, happiness and freedom from
 suffering.
