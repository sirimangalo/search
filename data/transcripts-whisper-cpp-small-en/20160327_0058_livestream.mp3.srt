1
00:00:00,000 --> 00:00:21,400
 Good evening everyone.

2
00:00:21,400 --> 00:00:31,600
 I'm practicing live March 26th.

3
00:00:31,600 --> 00:00:43,680
 The day's quote is a generic quote because there's nothing

4
00:00:43,680 --> 00:00:45,320
 really generic about it except

5
00:00:45,320 --> 00:00:58,220
 it is something that appears in the Dibhittika in lots of

6
00:00:58,220 --> 00:01:05,320
 different places.

7
00:01:05,320 --> 00:01:11,930
 In fact, I'm not sure that this is where he's taking it

8
00:01:11,930 --> 00:01:15,920
 from, but the reference is to the

9
00:01:15,920 --> 00:01:35,480
 Salaika Sutta, which is to Chandu.

10
00:01:35,480 --> 00:01:39,810
 It's actually quite dense, but Mahasya Sayadawd wrote a

11
00:01:39,810 --> 00:01:42,480
 book on it, gave talks and then turned

12
00:01:42,480 --> 00:01:44,920
 it into a book which is very much worth reading.

13
00:01:44,920 --> 00:01:48,910
 Salaika Sutta, a commentary by Mahasya Sayadawd, very much

14
00:01:48,910 --> 00:01:50,240
 worth reading.

15
00:01:50,240 --> 00:01:51,640
 That's not what this quote is about though.

16
00:01:51,640 --> 00:01:55,280
 The quote is tacked on at the end.

17
00:01:55,280 --> 00:02:04,680
 "Yangko sattara karuni yang savakanam."

18
00:02:04,680 --> 00:02:11,600
 Whatever should be done by a teacher for his students.

19
00:02:11,600 --> 00:02:23,960
 "Hite sinanukampakena anukampangupada."

20
00:02:23,960 --> 00:02:35,440
 Out of compassion, with reference to compassion, the

21
00:02:35,440 --> 00:02:40,080
 teacher who is compassionate, who is thinking

22
00:02:40,080 --> 00:02:47,440
 of or desiring the welfare of those students.

23
00:02:47,440 --> 00:02:52,880
 "Katang wo tangmaya."

24
00:02:52,880 --> 00:02:56,160
 Done for you, that is by me.

25
00:02:56,160 --> 00:02:59,960
 Done for you, that is by me.

26
00:02:59,960 --> 00:03:00,960
 It's very Yoda.

27
00:03:00,960 --> 00:03:01,960
 Pali.

28
00:03:01,960 --> 00:03:07,960
 "Katang dan wo for you, tang vatmaya by me."

29
00:03:07,960 --> 00:03:10,120
 "Katang wo tangmaya."

30
00:03:10,120 --> 00:03:13,380
 I have done that for you or that has been done for you by

31
00:03:13,380 --> 00:03:13,840
 me.

32
00:03:13,840 --> 00:03:18,390
 They're very passive, they have a lot of passive in Pali

33
00:03:18,390 --> 00:03:19,880
 and Sanskrit.

34
00:03:19,880 --> 00:03:20,880
 Twist things around.

35
00:03:20,880 --> 00:03:27,370
 Instead of saying, "I've done that for you," they say, "

36
00:03:27,370 --> 00:03:37,040
That was done for you by me."

37
00:03:37,040 --> 00:03:40,640
 I have done for you what I need to do as your teacher.

38
00:03:40,640 --> 00:03:45,000
 Means I've taught you what you need to know basically.

39
00:03:45,000 --> 00:03:56,130
 "Etani," over there, "chunda," "rukamulani," those tree

40
00:03:56,130 --> 00:03:57,840
 roots.

41
00:03:57,840 --> 00:04:08,640
 "Etani sunyagarani," over there are empty huts.

42
00:04:08,640 --> 00:04:20,640
 "Jayata," "Jayata," "Jayata," "junda," meditate junda.

43
00:04:20,640 --> 00:04:29,440
 "Mapa madata," don't be negligent.

44
00:04:29,440 --> 00:04:35,800
 "Mapa chahvi pati saarino ahu vata," ahu vata is

45
00:04:35,800 --> 00:04:38,160
 interesting.

46
00:04:38,160 --> 00:04:43,440
 I think that's like a future perfect or something.

47
00:04:43,440 --> 00:04:45,440
 Don't be one who has...

48
00:04:45,440 --> 00:04:49,640
 I don't know, ahu vata is...

49
00:04:49,640 --> 00:04:50,640
 What is that?

50
00:04:50,640 --> 00:04:53,640
 I think that's a panchami or something.

51
00:04:53,640 --> 00:04:55,640
 I forget.

52
00:04:55,640 --> 00:05:07,640
 Don't be one who is...

53
00:05:07,640 --> 00:05:16,300
 "Vipati saarino," who feels guilty afterwards or is vexed

54
00:05:16,300 --> 00:05:21,640
 by their negligence afterwards.

55
00:05:21,640 --> 00:05:32,370
 "Mapa chahvi pati saarino ahu vata," ayankho amha kanganu s

56
00:05:32,370 --> 00:05:33,640
asanmi.

57
00:05:33,640 --> 00:05:44,640
 This is my exhortation to all of you.

58
00:05:44,640 --> 00:05:48,640
 This is the Buddha's standard exhortation.

59
00:05:48,640 --> 00:06:01,400
 It's interesting for many reasons, but for the one reason

60
00:06:01,400 --> 00:06:11,940
 specifically that it hints at the idea of simplicity or

61
00:06:11,940 --> 00:06:15,640
 contentment in learning.

62
00:06:15,640 --> 00:06:26,640
 He's saying, "Look, I've taught you now enough of that."

63
00:06:26,640 --> 00:06:35,460
 He's basically saying, "Enough. That's enough. Now go med

64
00:06:35,460 --> 00:06:38,640
itate."

65
00:06:38,640 --> 00:06:45,220
 Directing the conversation, directing the attention of the

66
00:06:45,220 --> 00:06:46,640
 students.

67
00:06:46,640 --> 00:06:49,480
 When I teach meditation at McMaster, this five minute

68
00:06:49,480 --> 00:06:54,640
 meditation lesson, it's interesting the range of responses.

69
00:06:54,640 --> 00:06:59,170
 I don't have enough feedback yet to gauge what the fruit of

70
00:06:59,170 --> 00:07:03,640
 the teaching is, but I get the feeling that for some people

71
00:07:03,640 --> 00:07:07,640
 it's...

72
00:07:07,640 --> 00:07:15,640
 They're just looking for information.

73
00:07:15,640 --> 00:07:20,240
 Maybe the idea of meditation sounds good or something, but

74
00:07:20,240 --> 00:07:22,640
 absolutely not all of them.

75
00:07:22,640 --> 00:07:28,640
 Many people are actually into it.

76
00:07:28,640 --> 00:07:37,080
 Sometimes it's easy to say, "I want to meditate." It's easy

77
00:07:37,080 --> 00:07:38,640
 to say, "I should meditate."

78
00:07:38,640 --> 00:07:42,980
 It's easy to pick up a book and learn how to meditate. It's

79
00:07:42,980 --> 00:07:45,640
 easy to take a lesson on meditation.

80
00:07:45,640 --> 00:07:49,410
 But it's a whole other thing to sit down and say, "Oh yeah,

81
00:07:49,410 --> 00:07:52,640
 what that means is actually meditating.

82
00:07:52,640 --> 00:08:04,640
 Actually paying attention. Actually doing something."

83
00:08:04,640 --> 00:08:14,640
 Jayata. Bhikkhu-ve. Jayata. Jayata. Meditate.

84
00:08:14,640 --> 00:08:19,260
 Why that's interesting to me is because I get lots of

85
00:08:19,260 --> 00:08:22,640
 questions and lots of inquiries.

86
00:08:22,640 --> 00:08:29,380
 People... I've talked about many times asking questions

87
00:08:29,380 --> 00:08:30,640
 about their life.

88
00:08:30,640 --> 00:08:37,640
 "How do I deal with this? How do I deal with that?"

89
00:08:37,640 --> 00:08:46,170
 This quote speaks to me in that way. It reflects some of

90
00:08:46,170 --> 00:08:55,640
 the dilemma there of wanting to help people.

91
00:08:55,640 --> 00:09:07,640
 But trying to explain to people what is really helpful. It

92
00:09:07,640 --> 00:09:07,640
's not talking or...

93
00:09:07,640 --> 00:09:11,350
 There's no solution. It's not like there's anything I'm

94
00:09:11,350 --> 00:09:13,640
 going to say that's going to fix your problem.

95
00:09:13,640 --> 00:09:17,720
 And then, "Aha! That information will allow me to solve my

96
00:09:17,720 --> 00:09:21,640
 problems. That's not it."

97
00:09:21,640 --> 00:09:29,640
 We want to fix things. We still want to fix everything.

98
00:09:29,640 --> 00:09:32,970
 And in fact, that's really the wrong way of looking at

99
00:09:32,970 --> 00:09:37,640
 things. There is no fix.

100
00:09:37,640 --> 00:09:41,640
 You have to begin to accept that things are broken.

101
00:09:41,640 --> 00:09:48,730
 That there's an intrinsic... Like Leonard Cohen said, there

102
00:09:48,730 --> 00:09:51,640
's a crack in everything.

103
00:09:51,640 --> 00:09:59,640
 Everything is broken.

104
00:09:59,640 --> 00:10:06,140
 And to stop trying to find solutions and to start

105
00:10:06,140 --> 00:10:12,520
 understanding problems, understanding the nature of the

106
00:10:12,520 --> 00:10:15,640
 experiences behind the problems.

107
00:10:15,640 --> 00:10:18,640
 And so on.

108
00:10:18,640 --> 00:10:22,050
 So, in that vein, I'm not going to talk too much. Probably

109
00:10:22,050 --> 00:10:23,640
 already talked more than I should.

110
00:10:23,640 --> 00:10:27,050
 I should have just said, "Ah, you see? The Buddha says, 'Go

111
00:10:27,050 --> 00:10:29,640
 meditate. That's enough for tonight.'"

112
00:10:29,640 --> 00:10:33,640
 But no, I have to.

113
00:10:33,640 --> 00:10:41,840
 It's the duty of a teacher to instruct and to advise and to

114
00:10:41,840 --> 00:10:43,640
 explain.

115
00:10:43,640 --> 00:10:49,640
 And so here I've hopefully done a little bit of that.

116
00:10:49,640 --> 00:10:58,240
 If anyone has any questions about their meditation, I will

117
00:10:58,240 --> 00:11:04,640
 post the hangout.

118
00:11:04,640 --> 00:11:07,840
 I missed Second Life today, though it sounds like maybe I

119
00:11:07,840 --> 00:11:10,640
 had already discussed that I would be missing.

120
00:11:10,640 --> 00:11:13,390
 And it just totally skipped my mind. So probably at some

121
00:11:13,390 --> 00:11:14,640
 point I did say,

122
00:11:14,640 --> 00:11:20,630
 "I'm going to probably be too busy this weekend. I can't

123
00:11:20,630 --> 00:11:22,640
 remember."

124
00:11:22,640 --> 00:11:28,640
 I'm writing my essay. Slow going.

125
00:11:28,640 --> 00:11:32,640
 And so we've kind of worked out.

126
00:11:32,640 --> 00:11:39,640
 Sounds like month of June I'll be away in Asia.

127
00:11:39,640 --> 00:11:46,150
 So if you want to meet me in Thailand, I'd probably be

128
00:11:46,150 --> 00:11:48,640
 there about two weeks.

129
00:11:48,640 --> 00:11:52,640
 It's probably not worth going for two weeks to Thailand.

130
00:11:52,640 --> 00:11:59,640
 If you're in Thailand or in Asia, welcome to come with me.

131
00:11:59,640 --> 00:12:07,960
 I can come meet my teacher, but it would probably just be a

132
00:12:07,960 --> 00:12:10,640
 brief meeting.

133
00:12:10,640 --> 00:12:22,640
 He's very old and probably still very tired. Overworked.

134
00:12:22,640 --> 00:12:43,640
 So yeah, that's something. That's everything. Nothing else.

135
00:12:43,640 --> 00:12:57,520
 That's all I can think of. No questions? And I'm going to

136
00:12:57,520 --> 00:13:01,640
 say goodnight. Go meditate.

137
00:13:02,640 --> 00:13:08,640
 Thank you.

