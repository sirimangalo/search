1
00:00:00,000 --> 00:00:02,770
 doesn't mean we should do away with all laws or rules. It

2
00:00:02,770 --> 00:00:04,160
 means we have to have a better way of

3
00:00:04,160 --> 00:00:09,150
 of actually ensuring that the rules are kept, you know,

4
00:00:09,150 --> 00:00:12,400
 that we're able to build a society where we

5
00:00:12,400 --> 00:00:18,930
 have an order whereby people don't fall into behavior that

6
00:00:18,930 --> 00:00:21,440
 is conducive to conflict,

7
00:00:21,440 --> 00:00:24,720
 either inner conflict or conflict in society.

8
00:00:28,400 --> 00:00:32,790
 And eventually, ideally, a society that encourages people

9
00:00:32,790 --> 00:00:35,360
 in to become wiser, to become better, to

10
00:00:35,360 --> 00:00:40,960
 become more pure, more free, more happy.

11
00:00:40,960 --> 00:00:55,920
 And the final one is having common purity of view. So this

12
00:00:55,920 --> 00:00:58,000
 relates to what I've been talking about

13
00:00:58,000 --> 00:01:02,450
 not only do we need rules, but to have a pure society, to

14
00:01:02,450 --> 00:01:05,280
 have a harmonious society,

15
00:01:05,280 --> 00:01:09,420
 you really do need to some extent a community of a common

16
00:01:09,420 --> 00:01:13,760
 purpose, right? So most of Canada is

17
00:01:13,760 --> 00:01:18,080
 in agreement that providing health care to all people is a

18
00:01:18,080 --> 00:01:22,560
 good thing. And having that right view

19
00:01:25,120 --> 00:01:31,420
 of kindness and support and caring for our society. You

20
00:01:31,420 --> 00:01:33,840
 could say that's an important

21
00:01:33,840 --> 00:01:36,210
 part of creating the harmony. You know, if you look in

22
00:01:36,210 --> 00:01:39,600
 America, there's incredible disharmony.

23
00:01:39,600 --> 00:01:44,400
 America is very much divided down party lines. And probably

24
00:01:44,400 --> 00:01:48,560
 there's some intent behind that,

25
00:01:49,360 --> 00:01:54,610
 the intent to create a divide and conquer divided society.

26
00:01:54,610 --> 00:01:56,560
 I don't know. I don't want to get in too

27
00:01:56,560 --> 00:02:01,600
 much into it. But when you've got such ideology,

28
00:02:01,600 --> 00:02:06,000
 ideological diversity, that's where conflict

29
00:02:06,000 --> 00:02:09,890
 arises. So this recent in Canada, it's come up recently,

30
00:02:09,890 --> 00:02:14,720
 there's been a move to, I mean, I'd say

31
00:02:14,720 --> 00:02:21,090
 it's fairly radical, but this move to try and criminalize

32
00:02:21,090 --> 00:02:26,400
 or criminalize hate. And not just

33
00:02:26,400 --> 00:02:32,720
 hate, but physical and verbal acts of hate. And to apply

34
00:02:32,720 --> 00:02:36,400
 some fairly broad criteria for what we

35
00:02:36,400 --> 00:02:40,400
 understand as hate. So if you deny a person's identity,

36
00:02:40,400 --> 00:02:44,080
 gender identity is the big hot topic now,

37
00:02:44,560 --> 00:02:49,690
 that can be considered hate speech, or an act of a hate

38
00:02:49,690 --> 00:02:54,640
 crime. I mean, if it was done with malicious

39
00:02:54,640 --> 00:03:00,510
 intent, that's not like policing speech, not what everyone

40
00:03:00,510 --> 00:03:04,080
 thinks it is. If you are doing so in a

41
00:03:04,080 --> 00:03:08,650
 dismissive fashion, where you really and truly, anyway, I

42
00:03:08,650 --> 00:03:11,680
 don't know, it's a bit complicated and

43
00:03:11,680 --> 00:03:14,320
 perhaps radical in a sense, but it's caused a great

44
00:03:14,320 --> 00:03:18,160
 division in our society, I think. I mean,

45
00:03:18,160 --> 00:03:22,030
 on the one hand, there's one side, there's people who are

46
00:03:22,030 --> 00:03:25,360
 quite militaristic in their, in their

47
00:03:25,360 --> 00:03:33,280
 push for their own rights, you know, a push for their own,

48
00:03:33,280 --> 00:03:35,520
 what the other side calls agenda,

49
00:03:36,240 --> 00:03:42,430
 the liberal extremist agenda. I mean, in the end, there's,

50
00:03:42,430 --> 00:03:45,280
 there's some real good there,

51
00:03:45,280 --> 00:03:52,980
 trying to, trying to make sure that people feel and have

52
00:03:52,980 --> 00:03:58,000
 this sense of self worth and sense of,

53
00:03:58,000 --> 00:04:01,130
 of grounding in who they are, and that they're not left out

54
00:04:01,130 --> 00:04:03,360
 of the conversation, that they're not

55
00:04:03,360 --> 00:04:09,900
 dismissed as, or, or, or forced to be something they're not

56
00:04:09,900 --> 00:04:14,320
, right? That they can have this space

57
00:04:14,320 --> 00:04:21,280
 to breathe. But it does get quite vegment and angry, which

58
00:04:21,280 --> 00:04:26,800
 is not, it's not conducive to progress and

59
00:04:26,800 --> 00:04:32,370
 harmony. Not when it gets to that level, I think. It can be

60
00:04:32,370 --> 00:04:35,760
 so much better done if it was tempered and

61
00:04:35,760 --> 00:04:43,760
 associated with kindness. On the other side, there's an

62
00:04:43,760 --> 00:04:47,200
 incredibly vile and corrupt form of

63
00:04:47,200 --> 00:04:52,950
 hatred and bigotry that is cloaked in the guise of free

64
00:04:52,950 --> 00:04:55,520
 speech, but really, it's just about

65
00:04:56,320 --> 00:04:58,880
 hating people who are different and people who have

66
00:04:58,880 --> 00:05:05,740
 a different way of looking at the world. So we see this in

67
00:05:05,740 --> 00:05:07,680
 society. I mean, you see this in

68
00:05:07,680 --> 00:05:12,990
 monasteries, the story of, we'll have many stories of monks

69
00:05:12,990 --> 00:05:16,000
 beating on monks and monks attacking

70
00:05:16,000 --> 00:05:19,060
 monks and monks fighting with monks. We have stories going

71
00:05:19,060 --> 00:05:21,280
 back to the Buddha's time, this sort of thing.

72
00:05:21,280 --> 00:05:32,160
 Right now we're talking about view. We have, we have

73
00:05:32,160 --> 00:05:37,360
 stories of monks arguing with monks. We have

74
00:05:37,360 --> 00:05:39,560
 divisions from the time of the Buddha. Right after the

75
00:05:39,560 --> 00:05:41,200
 Buddha passed away, there was right away a

76
00:05:41,200 --> 00:05:45,060
 split and it split and it split and it split. And now we've

77
00:05:45,060 --> 00:05:47,360
 got forms of Buddhism that I don't even

78
00:05:47,360 --> 00:05:52,320
 recognize as anything to do with Buddhism. Very different

79
00:05:52,320 --> 00:05:55,760
 forms. I've got splits among communities.

80
00:05:55,760 --> 00:05:58,950
 I've been in, I was in a monastery once where there were

81
00:05:58,950 --> 00:06:01,600
 three groups of monks. They were had a split

82
00:06:01,600 --> 00:06:06,310
 and in the monastery there were three, three communities

83
00:06:06,310 --> 00:06:08,720
 and they didn't interact with each

84
00:06:08,720 --> 00:06:16,130
 other. They meant they schemed against each other. It was

85
00:06:16,130 --> 00:06:17,920
 quite, quite, well,

86
00:06:17,920 --> 00:06:22,480
 impressive in a way, I guess.

87
00:06:22,480 --> 00:06:29,520
 Having the same view. Now for us as meditators, this means

88
00:06:29,520 --> 00:06:33,200
 really having a view of the four noble

89
00:06:33,200 --> 00:06:36,440
 truths. That gives you the greatest harmony. When you see

90
00:06:36,440 --> 00:06:38,880
 that, then there's no, there's none of this.

91
00:06:38,880 --> 00:06:43,230
 There would never be a question about identity. There would

92
00:06:43,230 --> 00:06:44,960
 never be a question about

93
00:06:44,960 --> 00:06:50,080
 a clique or a group or a division of the Sangha.

94
00:06:50,080 --> 00:06:54,800
 I just wouldn't have any sense or meaning or purpose.

95
00:06:54,800 --> 00:07:00,600
 Not to mention all the anger and greed involved would just

96
00:07:00,600 --> 00:07:01,760
 have disappeared.

97
00:07:02,720 --> 00:07:06,170
 It's important that we're clear and that we, I mean, we

98
00:07:06,170 --> 00:07:09,280
 have these debates in society. It's

99
00:07:09,280 --> 00:07:13,440
 important that we debate. It's important that we discuss.

100
00:07:13,440 --> 00:07:15,200
 It's important that we,

101
00:07:15,200 --> 00:07:20,480
 we listen to each other. It's one thing that being in the

102
00:07:20,480 --> 00:07:24,560
 university has taught me is how to listen,

103
00:07:25,760 --> 00:07:30,310
 how to listen to people who I might disagree with. It

104
00:07:30,310 --> 00:07:33,280
 really gives you interesting perspective on why

105
00:07:33,280 --> 00:07:36,770
 people think things they do and it helps you understand how

106
00:07:36,770 --> 00:07:38,320
 people get to the views that

107
00:07:38,320 --> 00:07:40,560
 they get to, even though you might completely disagree with

108
00:07:40,560 --> 00:07:43,760
 their views. You at least can

109
00:07:43,760 --> 00:07:47,120
 understand what that makes them and where they come from.

110
00:07:47,120 --> 00:07:54,320
 By listening and by talking, we can,

111
00:07:55,120 --> 00:07:59,540
 we can break through and come to an understanding with each

112
00:07:59,540 --> 00:08:00,560
 other. And,

113
00:08:00,560 --> 00:08:04,900
 you know, I mean, there's many, many aspects, many factors

114
00:08:04,900 --> 00:08:06,720
 involved. You need rules, you need

115
00:08:06,720 --> 00:08:11,780
 kindness, you need a desire for truth and the desire for

116
00:08:11,780 --> 00:08:14,960
 harmony. The worst thing,

117
00:08:14,960 --> 00:08:18,440
 the worst enemy to harmony, of course, is the desire for

118
00:08:18,440 --> 00:08:19,120
 conflict.

119
00:08:19,760 --> 00:08:27,680
 If society is keen to hurt and to cause harm and to disrupt

120
00:08:27,680 --> 00:08:27,680
,

121
00:08:27,680 --> 00:08:32,030
 and you never, it doesn't matter how well-intentioned you

122
00:08:32,030 --> 00:08:32,960
 might be or

123
00:08:32,960 --> 00:08:39,520
 how clear you might be in your mind, you're never going to

124
00:08:39,520 --> 00:08:43,520
 succeed in bringing harmony to society.

125
00:08:46,640 --> 00:08:50,500
 And that's a sort of view in itself, right? The view, the

126
00:08:50,500 --> 00:08:52,560
 view that harmony and peace are

127
00:08:52,560 --> 00:08:55,920
 worth working towards, that the view that the true goal

128
00:08:55,920 --> 00:08:59,520
 should be happiness and should be peace,

129
00:08:59,520 --> 00:09:03,590
 that there is no happiness without peace. Nati santi parang

130
00:09:03,590 --> 00:09:04,240
 sukhang.

131
00:09:04,240 --> 00:09:09,520
 Once you have peace, you have happiness.

132
00:09:13,680 --> 00:09:16,670
 So there you go. Those are the Saraniya dhammas, they're d

133
00:09:16,670 --> 00:09:17,760
hammas that we should all

134
00:09:17,760 --> 00:09:22,080
 recollect and they help us think, fondly of each other.

135
00:09:22,080 --> 00:09:24,560
 They help make the community

136
00:09:24,560 --> 00:09:30,160
 a harmonious community. So thank you all for tuning in.

137
00:09:30,160 --> 00:09:34,560
 Have a good night.

138
00:09:34,560 --> 00:09:36,560
 (

139
00:09:36,560 --> 00:09:44,560
 sound effect )

140
00:09:46,560 --> 00:09:46,560
 (

141
00:09:46,560 --> 00:09:56,560
 sound effect )

142
00:09:58,560 --> 00:10:06,560
 ( sound effect )

143
00:10:06,560 --> 00:10:16,560
 ( sound effect )

144
00:10:16,560 --> 00:10:26,560
 ( sound effect )

145
00:10:26,560 --> 00:10:34,560
 ( sound effect )

146
00:10:34,560 --> 00:10:37,280
 My understanding is there's never been a communist

147
00:10:37,280 --> 00:10:38,560
 government.

148
00:10:38,560 --> 00:10:42,560
 Oops, that was the wrong button.

149
00:10:42,560 --> 00:10:52,560
 What does that do?

150
00:10:52,560 --> 00:10:57,030
 A communist government is, I mean, there is a sort of a

151
00:10:57,030 --> 00:10:58,560
 tension between the self and the community.

152
00:10:58,560 --> 00:11:06,560
 I don't think it's proper to, um, yeah, I don't know.

153
00:11:06,560 --> 00:11:09,480
 I think to some extent you have to remember the importance

154
00:11:09,480 --> 00:11:10,560
 of the individual and the importance

155
00:11:10,560 --> 00:11:15,030
 for individual freedom to pursue one's own, one's own

156
00:11:15,030 --> 00:11:19,360
 enlightenment, one's own goals. I mean,

157
00:11:19,360 --> 00:11:23,440
 as a pluralistic society is always going to be a challenge

158
00:11:23,440 --> 00:11:25,200
 because you have different ideas of

159
00:11:25,200 --> 00:11:28,990
 what's good and what's right. So until you get everyone

160
00:11:28,990 --> 00:11:33,760
 with the same right view, very difficult,

161
00:11:33,760 --> 00:11:37,440
 very difficult to find harmony, which was that last point.

162
00:11:37,440 --> 00:11:47,440
 ( sound effect )

163
00:11:47,440 --> 00:11:57,440
 ( sound effect )

164
00:11:57,440 --> 00:12:19,440
 ( sound effect )

165
00:12:19,440 --> 00:12:21,500
 Students criticize faculty. That must be interesting. I

166
00:12:21,500 --> 00:12:23,440
 just had to do my evaluations.

167
00:12:23,440 --> 00:12:31,440
 I wasn't sure if there'd be a lot of criticism or not.

168
00:12:31,440 --> 00:12:36,820
 Um, out of defensiveness. Well, I mean, ego plays a big

169
00:12:36,820 --> 00:12:37,440
 part of that then.

170
00:12:37,440 --> 00:12:41,660
 The whole idea is to, I mean, that's the big problem with

171
00:12:41,660 --> 00:12:43,440
 the university is there's a lot of ego.

172
00:12:43,440 --> 00:12:49,440
 That can be a problem.

173
00:12:51,440 --> 00:12:57,440
 ( sound effect )

174
00:12:57,440 --> 00:13:01,410
 So, I mean, a bit the best advice I think is learning to

175
00:13:01,410 --> 00:13:03,440
 have a thick skin and not,

176
00:13:03,440 --> 00:13:07,440
 I'd say thick skin, but with faculty it's scary.

