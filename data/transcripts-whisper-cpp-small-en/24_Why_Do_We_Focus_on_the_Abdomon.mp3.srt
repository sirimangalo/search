1
00:00:00,000 --> 00:00:02,900
 Why do we focus on the abdomen?

2
00:00:02,900 --> 00:00:05,660
 The abdomen is a physical phenomenon.

3
00:00:05,660 --> 00:00:08,020
 It is the element of motion,

4
00:00:08,020 --> 00:00:11,400
 wayo dhatu as described in Buddhist texts.

5
00:00:11,400 --> 00:00:13,400
 Mindfulness of breathing,

6
00:00:13,400 --> 00:00:17,100
 anapanasati is technically considered

7
00:00:17,100 --> 00:00:20,580
 tranquility, samatha, meditation.

8
00:00:20,580 --> 00:00:23,220
 While analysis of the elements,

9
00:00:23,220 --> 00:00:28,610
 shatudhatu wawatana is considered the basis of insight

10
00:00:28,610 --> 00:00:29,740
 meditation.

11
00:00:29,740 --> 00:00:33,260
 While the elements are experienced directly,

12
00:00:33,260 --> 00:00:36,220
 the breath itself is a concept.

13
00:00:36,220 --> 00:00:39,570
 Although watching the abdomen can be thought of as

14
00:00:39,570 --> 00:00:41,500
 mindfulness of breathing,

15
00:00:41,500 --> 00:00:45,870
 it differs from watching in and out breathing in one

16
00:00:45,870 --> 00:00:47,260
 important way.

17
00:00:47,260 --> 00:00:50,860
 The latter often leads to tranquility

18
00:00:50,860 --> 00:00:54,860
 rather than directly to seeing the nature of reality.

19
00:00:54,860 --> 00:00:59,100
 While the former is such family in ultimate reality

20
00:00:59,100 --> 00:01:03,740
 and thus conducive to seeing it clearly for what it is.

