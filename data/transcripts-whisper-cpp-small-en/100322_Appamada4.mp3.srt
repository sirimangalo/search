1
00:00:00,000 --> 00:00:13,830
 In Buddhism, we follow what is called the tipitaka, the

2
00:00:13,830 --> 00:00:17,280
 three baskets.

3
00:00:17,280 --> 00:00:30,610
 It's a large body of texts that is said to contain the

4
00:00:30,610 --> 00:00:35,920
 teaching of the Buddha and his

5
00:00:35,920 --> 00:00:48,180
 close disciples in what you could call the purest or the

6
00:00:48,180 --> 00:00:53,880
 closest to original form.

7
00:00:53,880 --> 00:01:02,170
 And so we can understand how important it is that our

8
00:01:02,170 --> 00:01:09,200
 practice be in line with, if not

9
00:01:09,200 --> 00:01:14,940
 encompass all of the tipitaka, then at least be in line

10
00:01:14,940 --> 00:01:17,280
 with the tipitaka.

11
00:01:17,280 --> 00:01:22,480
 And yet we see that often this isn't the case.

12
00:01:22,480 --> 00:01:29,490
 Why this isn't the case is because most Buddhists don't

13
00:01:29,490 --> 00:01:33,560
 bother to read the tipitaka.

14
00:01:33,560 --> 00:01:38,470
 For the most part, most Buddhists will take word of mouth

15
00:01:38,470 --> 00:01:41,360
 accounts of what is the Buddhist

16
00:01:41,360 --> 00:01:45,690
 teaching, often relying on their teachers or even just

17
00:01:45,690 --> 00:01:48,080
 relying on parents or friends

18
00:01:48,080 --> 00:01:55,760
 or school teachers.

19
00:01:55,760 --> 00:01:58,440
 But even among those people who have read the tipitaka,

20
00:01:58,440 --> 00:01:59,960
 there are then those people

21
00:01:59,960 --> 00:02:05,670
 who the majority of people who have studied the tipitaka

22
00:02:05,670 --> 00:02:07,520
 haven't practiced the Buddhist

23
00:02:07,520 --> 00:02:12,480
 teaching, have never put it into practice.

24
00:02:12,480 --> 00:02:14,980
 The majority of people who practice the Buddhist teaching

25
00:02:14,980 --> 00:02:16,800
 who actually come to meditate haven't

26
00:02:16,800 --> 00:02:26,930
 read the scriptures, the tipitaka, or even large pieces of

27
00:02:26,930 --> 00:02:28,000
 it.

28
00:02:28,000 --> 00:02:34,500
 It's hard to find someone who has both studied the tipitaka

29
00:02:34,500 --> 00:02:38,880
 and practiced meditation, Buddhist

30
00:02:38,880 --> 00:02:50,900
 meditation or made some attempt at practicing according to

31
00:02:50,900 --> 00:02:54,320
 the tipitaka.

32
00:02:54,320 --> 00:02:58,120
 And so we have kind of this quandary that we're dealing

33
00:02:58,120 --> 00:03:00,240
 with many different ways of

34
00:03:00,240 --> 00:03:05,760
 approaching Buddhism, often simply because of not reading

35
00:03:05,760 --> 00:03:08,760
 the text or not following text,

36
00:03:08,760 --> 00:03:13,720
 following someone who isn't following the text.

37
00:03:13,720 --> 00:03:19,230
 And I tell you, if you haven't practiced meditation, the

38
00:03:19,230 --> 00:03:21,960
 texts of the tipitaka, they don't have

39
00:03:21,960 --> 00:03:23,460
 much meaning.

40
00:03:23,460 --> 00:03:28,760
 They won't have much significance in one's mind.

41
00:03:28,760 --> 00:03:33,880
 They won't appear to be of any value or any use.

42
00:03:33,880 --> 00:03:38,960
 They will appear dense and confusing.

43
00:03:38,960 --> 00:03:41,020
 But the amazing thing about the Buddhist teaching is that

44
00:03:41,020 --> 00:03:43,640
 the more you practice, the more alive

45
00:03:43,640 --> 00:03:49,620
 and appealing and indeed wonderful and amazing are the

46
00:03:49,620 --> 00:03:53,560
 Buddhist teaching and the tipitaka,

47
00:03:53,560 --> 00:03:58,060
 which is a sign both that the Buddhist teaching is the

48
00:03:58,060 --> 00:04:01,520
 meditation practice is something very

49
00:04:01,520 --> 00:04:08,660
 profound and that the teachings in the tipitaka are

50
00:04:08,660 --> 00:04:12,720
 something very profound.

51
00:04:12,720 --> 00:04:17,190
 And so it doesn't serve our purpose to rely on someone who

52
00:04:17,190 --> 00:04:20,240
 has simply read the texts because

53
00:04:20,240 --> 00:04:23,320
 for the most part they don't understand them.

54
00:04:23,320 --> 00:04:26,630
 They think they understand them and they understand the

55
00:04:26,630 --> 00:04:28,440
 letter but not the meaning.

56
00:04:28,440 --> 00:04:37,160
 And so they wind up applying it to very ordinary things,

57
00:04:37,160 --> 00:04:42,040
 saying that the Buddha was a socialist

58
00:04:42,040 --> 00:04:48,890
 or someone who was trying to revolutionize the social order

59
00:04:48,890 --> 00:04:52,000
 or they say that the Buddha

60
00:04:52,000 --> 00:04:58,130
 was teaching us how to live our lives in an ordinary

61
00:04:58,130 --> 00:05:02,160
 everyday sense and so on and so on.

62
00:05:02,160 --> 00:05:06,910
 And they won't ever get around to teaching profound

63
00:05:06,910 --> 00:05:10,520
 subjects like insight meditation.

64
00:05:10,520 --> 00:05:16,060
 On the other hand, if someone simply practices meditation,

65
00:05:16,060 --> 00:05:19,240
 often without a teacher or perhaps

66
00:05:19,240 --> 00:05:28,330
 with a teacher but in an informal setting, then it's quite

67
00:05:28,330 --> 00:05:33,000
 likely that one will 90% of

68
00:05:33,000 --> 00:05:35,160
 the time get off track.

69
00:05:35,160 --> 00:05:39,750
 You could say even 100% of the time without real formal

70
00:05:39,750 --> 00:05:42,200
 instruction if one hasn't read

71
00:05:42,200 --> 00:05:48,650
 great amounts of the text, it would be very likely that

72
00:05:48,650 --> 00:05:51,800
 they will get off track.

73
00:05:51,800 --> 00:05:55,650
 But the curious thing here is that reading the texts

74
00:05:55,650 --> 00:05:58,280
 themselves without practicing is

75
00:05:58,280 --> 00:06:00,320
 a difficult task.

76
00:06:00,320 --> 00:06:07,030
 And so then if practicing without reading is a dangerous

77
00:06:07,030 --> 00:06:10,600
 task or a dangerous practice,

78
00:06:10,600 --> 00:06:15,320
 then how do we go about approaching this dilemma?

79
00:06:15,320 --> 00:06:18,040
 How do we go about approaching the Buddha's teaching?

80
00:06:18,040 --> 00:06:21,150
 And of course the answer is to have a teacher, to have

81
00:06:21,150 --> 00:06:22,920
 someone who has done both.

82
00:06:22,920 --> 00:06:27,440
 And so what we're relying on really is not just the text

83
00:06:27,440 --> 00:06:29,720
 but it's the people who have

84
00:06:29,720 --> 00:06:33,710
 carried the texts from the time of the Buddha and have

85
00:06:33,710 --> 00:06:36,280
 practiced according to them.

86
00:06:36,280 --> 00:06:39,420
 It's also possible to read the texts for oneself and

87
00:06:39,420 --> 00:06:42,080
 practice according to them but this takes

88
00:06:42,080 --> 00:06:43,600
 a very special individual.

89
00:06:43,600 --> 00:06:48,160
 There have been people who have done this in the history.

90
00:06:48,160 --> 00:06:50,380
 When they didn't have a teacher or in a time when there

91
00:06:50,380 --> 00:06:51,720
 were very few or maybe even no

92
00:06:51,720 --> 00:06:57,800
 teachers, they were able to apply the texts according to

93
00:06:57,800 --> 00:07:00,280
 their core message.

94
00:07:00,280 --> 00:07:03,270
 But for the most part, people who do this simply get off

95
00:07:03,270 --> 00:07:04,800
 track, get lost and don't know

96
00:07:04,800 --> 00:07:09,800
 what to do next, don't know where to go.

97
00:07:09,800 --> 00:07:14,920
 And so the most recommended path is to have a teacher who

98
00:07:14,920 --> 00:07:17,240
 can explain the text.

99
00:07:17,240 --> 00:07:21,650
 And at the same time, it's important to say that there's no

100
00:07:21,650 --> 00:07:23,560
 need in that case to actually

101
00:07:23,560 --> 00:07:28,470
 read the texts themselves because what you'll find is that

102
00:07:28,470 --> 00:07:30,880
 most of the texts were given

103
00:07:30,880 --> 00:07:40,040
 at some point of the Buddha's 45 years as a Buddha to a

104
00:07:40,040 --> 00:07:46,200
 large, a broad variety of audiences

105
00:07:46,200 --> 00:07:51,660
 with different needs, different interests, different incl

106
00:07:51,660 --> 00:07:52,880
inations.

107
00:07:52,880 --> 00:07:56,280
 And so he taught different things for different people.

108
00:07:56,280 --> 00:07:59,070
 Now in this sense, it's useful for say a teacher or it's

109
00:07:59,070 --> 00:08:00,860
 even useful for a meditator to see

110
00:08:00,860 --> 00:08:02,320
 all the different approaches.

111
00:08:02,320 --> 00:08:05,870
 But on the other hand, not all of the teachings will

112
00:08:05,870 --> 00:08:08,920
 necessarily be suitable for an individual

113
00:08:08,920 --> 00:08:11,940
 meditator at any given time.

114
00:08:11,940 --> 00:08:14,950
 And so to read all of them simply for the purposes of med

115
00:08:14,950 --> 00:08:16,680
itating is both overkill and

116
00:08:16,680 --> 00:08:20,320
 perhaps even misleading because some of the texts might

117
00:08:20,320 --> 00:08:22,640
 even seem to contradict each other

118
00:08:22,640 --> 00:08:30,600
 having been given for different people at different times.

119
00:08:30,600 --> 00:08:33,460
 But on the other hand, it's very important that our

120
00:08:33,460 --> 00:08:35,480
 practice and the practice that our

121
00:08:35,480 --> 00:08:39,280
 teacher gives to us is according to the Dvitika.

122
00:08:39,280 --> 00:08:43,240
 And so often there should be citations given where does

123
00:08:43,240 --> 00:08:45,280
 this teaching come from?

124
00:08:45,280 --> 00:08:47,920
 For instance, the Four Foundations of Mindfulness.

125
00:08:47,920 --> 00:08:51,610
 It's important to make clear that this isn't something we

126
00:08:51,610 --> 00:08:53,520
 just made up or that was made

127
00:08:53,520 --> 00:08:58,560
 up by a commentator or a disciple of the Buddha.

128
00:08:58,560 --> 00:09:03,610
 It's actually the key practice that the Buddha recommended

129
00:09:03,610 --> 00:09:06,440
 for his students, mindfulness

130
00:09:06,440 --> 00:09:14,800
 of the body, the feelings, the mind and the dhammas.

131
00:09:14,800 --> 00:09:17,600
 Of course there are other practices that are in the Dvitika

132
00:09:17,600 --> 00:09:19,400
 and these are given for individual

133
00:09:19,400 --> 00:09:31,200
 meditators but by and large the key, you could say, or the

134
00:09:31,200 --> 00:09:33,440
 core practice of the Buddha is

135
00:09:33,440 --> 00:09:34,600
 the Four Foundations of Mindfulness.

136
00:09:34,600 --> 00:09:37,620
 And he made this clear in the Great Discourse on the Four

137
00:09:37,620 --> 00:09:39,440
 Foundations of Mindfulness as

138
00:09:39,440 --> 00:09:43,150
 well as the Discourse on the Four Foundations of Mind

139
00:09:43,150 --> 00:09:44,080
fulness.

140
00:09:44,080 --> 00:09:49,590
 Almost the same Discourse is found in two places as well as

141
00:09:49,590 --> 00:09:51,920
 a huge section of the, a

142
00:09:51,920 --> 00:09:55,060
 large section of the Sanyutanikaya devoted directly to the

143
00:09:55,060 --> 00:09:57,120
 Four Foundations of Mindfulness.

144
00:09:57,120 --> 00:10:01,080
 As far as meditation practice goes, the Four Foundations of

145
00:10:01,080 --> 00:10:03,080
 Mindfulness is the most, as

146
00:10:03,080 --> 00:10:08,430
 far as I can think, it's the most often referred to, most

147
00:10:08,430 --> 00:10:10,200
 widely taught.

148
00:10:10,200 --> 00:10:14,850
 Of course there are other teachings that are maybe more

149
00:10:14,850 --> 00:10:18,240
 often referred to such as the teachings

150
00:10:18,240 --> 00:10:21,850
 on the Six Senses or the Five Aggregates or the Eightfold

151
00:10:21,850 --> 00:10:23,720
 Noble Path or the Four Noble

152
00:10:23,720 --> 00:10:27,580
 Truths but none of those are specifically meditation

153
00:10:27,580 --> 00:10:28,680
 practices.

154
00:10:28,680 --> 00:10:32,170
 So when we do our meditation we can be, rest assured, that

155
00:10:32,170 --> 00:10:35,000
 the Four Foundations of Mindfulness,

156
00:10:35,000 --> 00:10:38,880
 of the body for instance when we say rising, falling of the

157
00:10:38,880 --> 00:10:41,000
 abdomen or walking, stepping

158
00:10:41,000 --> 00:10:44,370
 right, stepping left, that we're practicing according to

159
00:10:44,370 --> 00:10:45,920
 the Buddha's teaching.

160
00:10:45,920 --> 00:10:50,500
 When we pay attention to the feelings, pain or aching or

161
00:10:50,500 --> 00:10:53,320
 soreness, happy feelings, neutral

162
00:10:53,320 --> 00:10:57,850
 feelings, saying to ourselves pain, pain or happy, happy or

163
00:10:57,850 --> 00:11:00,160
 calm, calm, that we're practicing

164
00:11:00,160 --> 00:11:01,920
 according to the Buddha's teaching.

165
00:11:01,920 --> 00:11:07,060
 When we watch the mind, thoughts, good thoughts, bad

166
00:11:07,060 --> 00:11:11,280
 thoughts, past thoughts, future thoughts,

167
00:11:11,280 --> 00:11:15,420
 then we're paying attention to the mind, we are practicing

168
00:11:15,420 --> 00:11:19,000
 according to the Buddha's teaching.

169
00:11:19,000 --> 00:11:23,270
 Or when we focus on the many dhammas, the many groups of

170
00:11:23,270 --> 00:11:25,520
 phenomena or of mind states

171
00:11:25,520 --> 00:11:31,450
 or of qualities of mind or specific teachings that the

172
00:11:31,450 --> 00:11:35,400
 Buddha gave, when we're mindful of

173
00:11:35,400 --> 00:11:40,140
 these realities, for instance the Five Hindrances when we

174
00:11:40,140 --> 00:11:43,440
 say liking, liking or disliking, disliking,

175
00:11:43,440 --> 00:11:48,160
 drowsy, drowsy, distracted, doubting.

176
00:11:48,160 --> 00:11:50,990
 When we acknowledge these we can see that we're practicing

177
00:11:50,990 --> 00:11:52,400
 according to the Buddha's

178
00:11:52,400 --> 00:11:55,280
 teaching as the Buddha said.

179
00:11:55,280 --> 00:11:58,780
 When one of these things is present, we know this is

180
00:11:58,780 --> 00:12:01,240
 present or when we're walking, we

181
00:12:01,240 --> 00:12:05,240
 know walking, when we're sitting, we know we're sitting.

182
00:12:05,240 --> 00:12:08,840
 We know to ourselves, I am sitting, I am walking, or simply

183
00:12:08,840 --> 00:12:11,040
 put in English walking, walking,

184
00:12:11,040 --> 00:12:15,120
 sitting, sitting.

185
00:12:15,120 --> 00:12:17,680
 When there's a painful feeling present, one knows there is

186
00:12:17,680 --> 00:12:19,000
 a painful feeling present or

187
00:12:19,000 --> 00:12:25,240
 simply pain, pain, and so on.

188
00:12:25,240 --> 00:12:29,260
 So this is one thing we can rest assured but the thing I

189
00:12:29,260 --> 00:12:32,040
 wanted to talk about tonight besides

190
00:12:32,040 --> 00:12:38,480
 simply giving this sort of idea is to give a core, sort of

191
00:12:38,480 --> 00:12:42,600
 a core overview of the tipitaka.

192
00:12:42,600 --> 00:12:46,960
 Because we have this huge body of text, three baskets of 84

193
00:12:46,960 --> 00:12:49,800
,000 teachings or somewhere in

194
00:12:49,800 --> 00:12:57,400
 that number, somewhere near that number.

195
00:12:57,400 --> 00:12:59,460
 How do we make sense of this all and how do we understand

196
00:12:59,460 --> 00:13:00,760
 what is it that the Buddha taught?

197
00:13:00,760 --> 00:13:04,420
 Okay, so Four Foundations of Mindfulness is somewhere near

198
00:13:04,420 --> 00:13:06,160
 the core but what exactly is

199
00:13:06,160 --> 00:13:07,920
 the core?

200
00:13:07,920 --> 00:13:10,560
 How do you sum up the Buddha's teaching?

201
00:13:10,560 --> 00:13:12,760
 And there are many ways to do this.

202
00:13:12,760 --> 00:13:15,870
 I'm only going to talk about one and I'm going to give what

203
00:13:15,870 --> 00:13:17,520
 is the accepted summary.

204
00:13:17,520 --> 00:13:19,700
 You could summarize the Buddha's teaching and the Four

205
00:13:19,700 --> 00:13:21,360
 Noble Truths because really everything

206
00:13:21,360 --> 00:13:25,720
 the Buddha taught was based on the Four Noble Truths.

207
00:13:25,720 --> 00:13:28,860
 But there's another way of summing up the Buddha's teaching

208
00:13:28,860 --> 00:13:30,520
 that is much more practical.

209
00:13:30,520 --> 00:13:34,310
 Not to say that Four Noble Truths are not a very important

210
00:13:34,310 --> 00:13:36,280
 teaching but on a practical

211
00:13:36,280 --> 00:13:37,960
 level.

212
00:13:37,960 --> 00:13:40,040
 How do we put the Four Noble Truths into practice?

213
00:13:40,040 --> 00:13:42,600
 How do we put the Buddha's teaching into practice?

214
00:13:42,600 --> 00:13:45,870
 And for this we look to the Buddha's last words because

215
00:13:45,870 --> 00:13:47,800
 before the Buddha passed away

216
00:13:47,800 --> 00:13:53,510
 he said, "apamādhin sampādheda" which is variously

217
00:13:53,510 --> 00:13:59,480
 translated as something like "strive

218
00:13:59,480 --> 00:14:08,000
 on with diligence" or "strive on with heedfulness".

219
00:14:08,000 --> 00:14:09,520
 And this was all he said.

220
00:14:09,520 --> 00:14:12,810
 Before this he said that all formations are subject to

221
00:14:12,810 --> 00:14:13,480
 cease.

222
00:14:13,480 --> 00:14:16,100
 Everything is subject to cease which was basically using

223
00:14:16,100 --> 00:14:17,320
 himself as an example.

224
00:14:17,320 --> 00:14:20,600
 That even he passes away, even the Buddha's pass away.

225
00:14:20,600 --> 00:14:22,840
 That everything is subject to cease.

226
00:14:22,840 --> 00:14:25,880
 Everything that arises is subject to cease.

227
00:14:25,880 --> 00:14:31,270
 Then he said, "apamādhin sampādheda" two words which is "

228
00:14:31,270 --> 00:14:34,560
strive on" or actually a literal

229
00:14:34,560 --> 00:14:39,940
 translation would be "fill yourself up with or come to full

230
00:14:39,940 --> 00:14:42,520
 attainment of heedfulness".

231
00:14:42,520 --> 00:14:47,370
 "apamādha" which is the opposite of "pamādha" which means

232
00:14:47,370 --> 00:14:50,160
 intoxication or heedlessness or

233
00:14:50,160 --> 00:14:52,360
 negligent.

234
00:14:52,360 --> 00:14:55,960
 So this idea of being heedful in one sense being mindful

235
00:14:55,960 --> 00:14:58,040
 and I'll get to that in a second

236
00:14:58,040 --> 00:15:01,120
 because actually the two are very much related.

237
00:15:01,120 --> 00:15:06,160
 But why we know this is the core of the Buddha's teaching?

238
00:15:06,160 --> 00:15:09,520
 Well besides it being his last words and therefore having

239
00:15:09,520 --> 00:15:12,280
 great significance we have the commentator's

240
00:15:12,280 --> 00:15:19,290
 appraisal of the tāpādhika in regards to this teaching of

241
00:15:19,290 --> 00:15:21,320
 "apamādha".

242
00:15:21,320 --> 00:15:24,740
 And so the commentator says that when you boil down all of

243
00:15:24,740 --> 00:15:26,640
 the Buddha's teachings, all of

244
00:15:26,640 --> 00:15:31,190
 the three tāpādhikas in their entirety, "sakalampihitepit

245
00:15:31,190 --> 00:15:33,280
akan", the whole entirety

246
00:15:33,280 --> 00:15:36,280
 of the three tāpādhikas, the three baskets which are the

247
00:15:36,280 --> 00:15:37,920
 Buddha vāchana, the Buddha's

248
00:15:37,920 --> 00:15:40,840
 teaching.

249
00:15:40,840 --> 00:15:45,390
 When you boil it down, when you compact it down, it is all

250
00:15:45,390 --> 00:15:47,660
 for the purpose of pointing

251
00:15:47,660 --> 00:15:55,080
 out the what is called "apamādha patan", "apamādha patan

252
00:15:55,080 --> 00:15:57,240
 evaotarati".

253
00:15:57,240 --> 00:16:04,280
 It boils down to the path of heedfulness, the path of

254
00:16:04,280 --> 00:16:09,360
 diligence, the path of mindfulness

255
00:16:09,360 --> 00:16:11,400
 if you will.

256
00:16:11,400 --> 00:16:15,450
 Now why we can say the path of mindfulness is because of

257
00:16:15,450 --> 00:16:17,480
 what I'd like to talk about

258
00:16:17,480 --> 00:16:23,200
 exactly what the Buddha said is "apamādha".

259
00:16:23,200 --> 00:16:24,920
 And there are four things that are "apamādha".

260
00:16:24,920 --> 00:16:27,740
 This is something that I think is important to talk about,

261
00:16:27,740 --> 00:16:29,360
 important to let everyone know

262
00:16:29,360 --> 00:16:34,000
 what is the state of mind that we're trying to attain.

263
00:16:34,000 --> 00:16:36,650
 How should we approach our meditation when we meditate,

264
00:16:36,650 --> 00:16:38,000
 what is important to keep in

265
00:16:38,000 --> 00:16:39,000
 mind.

266
00:16:39,000 --> 00:16:42,630
 And if we keep in mind these four principles, then we have

267
00:16:42,630 --> 00:16:44,720
 an understanding of what state

268
00:16:44,720 --> 00:16:45,880
 of mind we're looking for.

269
00:16:45,880 --> 00:16:48,830
 Of course we're still practicing the four foundations of

270
00:16:48,830 --> 00:16:50,520
 mindfulness, but here we have

271
00:16:50,520 --> 00:16:59,180
 four things that are sort of a way of keeping ourselves in

272
00:16:59,180 --> 00:17:00,720
 check.

273
00:17:00,720 --> 00:17:06,580
 The first one is "akodhanō", no, "apayapano", sorry, I'm

274
00:17:06,580 --> 00:17:10,560
 thinking in a different sense, "apayapano",

275
00:17:10,560 --> 00:17:11,560
 which basically means "akodhanō".

276
00:17:11,560 --> 00:17:16,470
 It's the same thing, it means not having freedom from ill

277
00:17:16,470 --> 00:17:21,520
 will, not wishing ill of others.

278
00:17:21,520 --> 00:17:25,240
 Basically means not being angry, not getting angry.

279
00:17:25,240 --> 00:17:30,950
 "Apayapano satāsato", "satāsato" means always being

280
00:17:30,950 --> 00:17:35,880
 mindful, so mindfulness is in there.

281
00:17:35,880 --> 00:17:43,330
 "Achatangsu-sama-hito", number three, which means being

282
00:17:43,330 --> 00:17:46,280
 internally composed, being sort

283
00:17:46,280 --> 00:17:50,540
 of, you could say, level-headed or composed, having an

284
00:17:50,540 --> 00:17:53,280
 internal composition or being well

285
00:17:53,280 --> 00:17:57,320
 composed internally.

286
00:17:57,320 --> 00:18:04,230
 And then number four, "abhicha-vinayi-sikam", training

287
00:18:04,230 --> 00:18:08,600
 oneself to overcome or to leave behind

288
00:18:08,600 --> 00:18:13,560
 greed, to leave behind one's clinging or craving.

289
00:18:13,560 --> 00:18:21,550
 So Buddha said, "abhaymano-ti", "abhicha-vinayi-sikam", "ab

290
00:18:21,550 --> 00:18:25,840
haymano-ti uchati", this is called "apamata",

291
00:18:25,840 --> 00:18:33,640
 this is called "negligence", "diligence", "non-negligence".

292
00:18:33,640 --> 00:18:37,010
 So these four things are sort of a very important teaching

293
00:18:37,010 --> 00:18:38,840
 that we should keep in mind.

294
00:18:38,840 --> 00:18:44,320
 "Apayapano" is, we should be free from any kind of anger

295
00:18:44,320 --> 00:18:46,940
 towards other beings.

296
00:18:46,940 --> 00:18:49,040
 So when we're practicing, this is something that we're

297
00:18:49,040 --> 00:18:50,400
 working on, and anger towards not

298
00:18:50,400 --> 00:18:53,140
 just beings, but all things in general.

299
00:18:53,140 --> 00:18:56,450
 This is something that we should work out in the very

300
00:18:56,450 --> 00:18:58,540
 beginning and try to give up our

301
00:18:58,540 --> 00:19:05,950
 resentment and our frustrations and our intolerance of

302
00:19:05,950 --> 00:19:12,800
 unpleasant situations or unpleasant individuals

303
00:19:12,800 --> 00:19:19,060
 or unpleasant interactions, that we should try to free our

304
00:19:19,060 --> 00:19:22,160
 minds from this anger, this

305
00:19:22,160 --> 00:19:28,560
 hatred or frustration, even things like boredom or sadness.

306
00:19:28,560 --> 00:19:31,440
 The Buddha said, "this is one thing you can kill and sleep

307
00:19:31,440 --> 00:19:32,200
 soundly."

308
00:19:32,200 --> 00:19:35,990
 There was one time a brahmin came to the Buddha, he was

309
00:19:35,990 --> 00:19:39,400
 very angry because his wife was Buddhist,

310
00:19:39,400 --> 00:19:42,520
 and she was making a nuisance of herself at home.

311
00:19:42,520 --> 00:19:49,020
 When he had brahmin guests over, she would say the Buddha's

312
00:19:49,020 --> 00:19:50,880
 name and so on.

313
00:19:50,880 --> 00:19:54,800
 When she tripped and fell, she would say, "Oh Buddha, oh

314
00:19:54,800 --> 00:19:56,120
 Buddha help me."

315
00:19:56,120 --> 00:19:57,120
 Or something like that.

316
00:19:57,120 --> 00:19:58,120
 "Oh my God."

317
00:19:58,120 --> 00:20:01,200
 Instead of "Oh my God, oh my Buddha."

318
00:20:01,200 --> 00:20:04,360
 And so her guests would get upset and leave.

319
00:20:04,360 --> 00:20:07,250
 And so he went to see the Buddha and got angry and he said

320
00:20:07,250 --> 00:20:08,840
 to the Buddha, "What can you

321
00:20:08,840 --> 00:20:14,200
 kill and rest at peace?

322
00:20:14,200 --> 00:20:17,000
 What would you kill if you wanted to rest at peace?"

323
00:20:17,000 --> 00:20:27,680
 And so he said, "King kattwa sukang seiti."

324
00:20:27,680 --> 00:20:30,640
 Having killed what, can you sleep at ease?

325
00:20:30,640 --> 00:20:33,320
 The Buddha said, "Kotang kattwa sukang seiti."

326
00:20:33,320 --> 00:20:35,840
 He threw it back in his face.

327
00:20:35,840 --> 00:20:44,200
 He said, "By killing anger is what you can kill.

328
00:20:44,200 --> 00:20:49,960
 Anger is what you can kill and rest in peace."

329
00:20:49,960 --> 00:20:54,880
 And then he asked, "Kotang kattwa sukang seiti."

330
00:20:54,880 --> 00:21:04,120
 Having killed what, will you not weep or not be remorseful?

331
00:21:04,120 --> 00:21:07,330
 And the Buddha said the same thing, "Kotang kattwa sukang

332
00:21:07,330 --> 00:21:08,000
 seiti."

333
00:21:08,000 --> 00:21:11,880
 When you destroy anger, you don't weep.

334
00:21:11,880 --> 00:21:17,160
 You don't regret.

335
00:21:17,160 --> 00:21:18,360
 It's the one thing that you can kill.

336
00:21:18,360 --> 00:21:20,720
 It's the one thing that you can destroy.

337
00:21:20,720 --> 00:21:24,220
 Normally, we think of destroying those things that make us

338
00:21:24,220 --> 00:21:24,840
 angry.

339
00:21:24,840 --> 00:21:27,120
 The Buddha said, "Really, it's not possible.

340
00:21:27,120 --> 00:21:30,760
 It's not the way of peace and happiness."

341
00:21:30,760 --> 00:21:34,550
 You have to kill this that's inside of yourself, which is

342
00:21:34,550 --> 00:21:37,720
 the anger, the upset about your situation.

343
00:21:37,720 --> 00:21:39,760
 So this is the first one.

344
00:21:39,760 --> 00:21:41,990
 The second one is satāsato, which is one we already

345
00:21:41,990 --> 00:21:42,760
 understand.

346
00:21:42,760 --> 00:21:43,760
 Satā means always.

347
00:21:43,760 --> 00:21:47,680
 Satō means one with mindfulness, always being one who is

348
00:21:47,680 --> 00:21:48,520
 mindful.

349
00:21:48,520 --> 00:21:52,450
 And you could say that this is, as I said, mindfulness and

350
00:21:52,450 --> 00:21:55,520
 apamāda are very closely related.

351
00:21:55,520 --> 00:22:01,060
 So there's another point where the Buddha said, "Sati avipa

352
00:22:01,060 --> 00:22:07,000
 wasa apamāda uti uchitti."

353
00:22:07,000 --> 00:22:12,450
 Being always never without, never being without mindfulness

354
00:22:12,450 --> 00:22:12,720
.

355
00:22:12,720 --> 00:22:16,360
 This is called apamāda.

356
00:22:16,360 --> 00:22:19,640
 So constant mindfulness, where we're constantly aware when

357
00:22:19,640 --> 00:22:21,800
 we're walking, walking, when we're

358
00:22:21,800 --> 00:22:25,370
 sitting, sitting, when we're seeing, we say seeing, seeing,

359
00:22:25,370 --> 00:22:26,680
 hearing, hearing.

360
00:22:26,680 --> 00:22:29,950
 We're clearly aware of everything, piece by piece as it

361
00:22:29,950 --> 00:22:32,080
 comes one by one, of everything.

362
00:22:32,080 --> 00:22:35,010
 So here we are sitting here and there's many things going

363
00:22:35,010 --> 00:22:35,400
 on.

364
00:22:35,400 --> 00:22:39,810
 And you can see that things are rising and ceasing, coming

365
00:22:39,810 --> 00:22:42,000
 and going, pain and aching

366
00:22:42,000 --> 00:22:46,080
 and boredom and so on, thoughts coming.

367
00:22:46,080 --> 00:22:48,300
 And so grabbing all of these things and catching them one

368
00:22:48,300 --> 00:22:49,720
 by one in their attention and just

369
00:22:49,720 --> 00:22:55,110
 seeing them clearly, coming to see everything as it comes

370
00:22:55,110 --> 00:22:57,360
 and goes clearly, knowing it for

371
00:22:57,360 --> 00:23:00,480
 what it is, this is mindfulness.

372
00:23:00,480 --> 00:23:04,350
 This is the state of constant mindfulness where we're aware

373
00:23:04,350 --> 00:23:06,280
 again and again and again.

374
00:23:06,280 --> 00:23:09,020
 Where we send our mind out again and again to know every

375
00:23:09,020 --> 00:23:10,640
 moment that's going on in that

376
00:23:10,640 --> 00:23:15,040
 moment and to see it clearly for what it is.

377
00:23:15,040 --> 00:23:21,000
 This the Buddha said is called apamāda.

378
00:23:21,000 --> 00:23:22,000
 This is number two.

379
00:23:22,000 --> 00:23:25,970
 Number three, achittang su-sama-hi-to means to keep

380
00:23:25,970 --> 00:23:28,960
 ourselves, keep our cool, you could

381
00:23:28,960 --> 00:23:29,960
 say.

382
00:23:29,960 --> 00:23:32,160
 It's to be internally composed.

383
00:23:32,160 --> 00:23:34,290
 This is the feeling that you get when you really are med

384
00:23:34,290 --> 00:23:35,960
itating, when you've been practicing

385
00:23:35,960 --> 00:23:40,440
 for some days, even a week, two weeks and suddenly you

386
00:23:40,440 --> 00:23:43,000
 start to settle in and you find

387
00:23:43,000 --> 00:23:44,000
 your balance.

388
00:23:44,000 --> 00:23:51,600
 In the beginning it's very tumultuous.

389
00:23:51,600 --> 00:23:56,320
 It's very turbulent.

390
00:23:56,320 --> 00:23:58,480
 There's good things coming and so we like them.

391
00:23:58,480 --> 00:24:01,900
 There's bad things coming and so we run away from them and

392
00:24:01,900 --> 00:24:04,040
 we find ourselves back and forth.

393
00:24:04,040 --> 00:24:10,470
 Our meditation is not quiet, it's not stable, it's not

394
00:24:10,470 --> 00:24:14,040
 happy, it's not peaceful.

395
00:24:14,040 --> 00:24:17,310
 And yet as we continue we start to work these first the

396
00:24:17,310 --> 00:24:19,600
 coarse defilements and then slowly

397
00:24:19,600 --> 00:24:22,900
 the more subtle defilements until we find ourselves in this

398
00:24:22,900 --> 00:24:25,520
 sort of balanced state of mind where

399
00:24:25,520 --> 00:24:28,360
 good things don't affect us and bad things don't affect us.

400
00:24:28,360 --> 00:24:31,230
 In fact we fail to see things as good or bad, we simply see

401
00:24:31,230 --> 00:24:32,960
 them for what they are, coming

402
00:24:32,960 --> 00:24:33,960
 and going.

403
00:24:33,960 --> 00:24:40,140
 We're able to live as it were on the waves as they go up

404
00:24:40,140 --> 00:24:41,640
 and down.

405
00:24:41,640 --> 00:24:45,560
 We feel like we're going, we're smooth riding.

406
00:24:45,560 --> 00:24:50,690
 This is the state of meditation, sort of what we're aiming

407
00:24:50,690 --> 00:24:53,200
 for, to be impartial, to have

408
00:24:53,200 --> 00:24:55,680
 a balanced state of mind.

409
00:24:55,680 --> 00:24:59,750
 This is number three and number four is the training to

410
00:24:59,750 --> 00:25:02,560
 overcome greed, which is of course

411
00:25:02,560 --> 00:25:05,720
 the opposite of anger so they actually go together.

412
00:25:05,720 --> 00:25:09,470
 But the Buddha placed emphasis here that the idea is not to

413
00:25:09,470 --> 00:25:11,420
 not be greedy, it's to train

414
00:25:11,420 --> 00:25:14,240
 ourselves out of it because really that's the core of the

415
00:25:14,240 --> 00:25:14,920
 training.

416
00:25:14,920 --> 00:25:17,790
 Anger is something, it's actually quite easy to see that it

417
00:25:17,790 --> 00:25:19,280
's a bad thing, much easier

418
00:25:19,280 --> 00:25:21,320
 to see that anger is a bad thing.

419
00:25:21,320 --> 00:25:23,960
 But it's our greed that we really have to work on and go

420
00:25:23,960 --> 00:25:25,420
 deeper on and really come to

421
00:25:25,420 --> 00:25:29,420
 see and to give up our clinging to everything, to give up

422
00:25:29,420 --> 00:25:31,480
 all of our wants and all of our

423
00:25:31,480 --> 00:25:38,770
 needs to not be in discontent at all times, to actually be

424
00:25:38,770 --> 00:25:42,200
 content with reality as it

425
00:25:42,200 --> 00:25:47,110
 is, however it is, and to not become attached to it so that

426
00:25:47,110 --> 00:25:49,400
 when it changes we can also

427
00:25:49,400 --> 00:25:54,120
 accept the changes, to simply be aware of things as they

428
00:25:54,120 --> 00:25:54,760
 are.

429
00:25:54,760 --> 00:25:57,800
 And this takes a training, sikhang is where the Buddha is,

430
00:25:57,800 --> 00:25:59,160
 and here we're training to

431
00:25:59,160 --> 00:26:02,810
 look at things, we're examining the things of the objects

432
00:26:02,810 --> 00:26:03,920
 of our desire.

433
00:26:03,920 --> 00:26:07,190
 So as opposed to when we know we're addicted to something,

434
00:26:07,190 --> 00:26:09,200
 running away from it, we examine

435
00:26:09,200 --> 00:26:11,000
 the process of addiction.

436
00:26:11,000 --> 00:26:14,650
 Now in some cases this does require us to leave behind the

437
00:26:14,650 --> 00:26:16,360
 object of addiction, for

438
00:26:16,360 --> 00:26:20,320
 instance with drugs or alcohol.

439
00:26:20,320 --> 00:26:23,600
 But in most cases we can actually at the moment of part

440
00:26:23,600 --> 00:26:25,880
aking in some simple addiction like

441
00:26:25,880 --> 00:26:33,120
 games or television or people or sensuality, food for

442
00:26:33,120 --> 00:26:39,360
 instance, junk food is a good example.

443
00:26:39,360 --> 00:26:42,730
 We can just watch ourselves as we're partaking and slowly,

444
00:26:42,730 --> 00:26:44,760
 slowly wean ourselves off of it,

445
00:26:44,760 --> 00:26:47,880
 slowly, slowly we realize that this is a process of

446
00:26:47,880 --> 00:26:50,680
 suffering, this addiction, that there's

447
00:26:50,680 --> 00:26:53,070
 no need to be addicted to these things and that it's

448
00:26:53,070 --> 00:26:54,840
 actually bringing us great stress

449
00:26:54,840 --> 00:26:55,840
 and suffering.

450
00:26:55,840 --> 00:26:59,550
 The little bit of happiness that we get is actually totally

451
00:26:59,550 --> 00:27:01,360
 removed from the addiction

452
00:27:01,360 --> 00:27:02,360
 itself.

453
00:27:02,360 --> 00:27:07,380
 And so even though there may be some amount of happiness in

454
00:27:07,380 --> 00:27:09,720
 partaking in these things,

455
00:27:09,720 --> 00:27:13,750
 it comes about not because we're addicted but simply

456
00:27:13,750 --> 00:27:16,040
 because we're partaking.

457
00:27:16,040 --> 00:27:19,800
 And because of how small an amount of happiness it is, we

458
00:27:19,800 --> 00:27:22,280
 can see that being addicted to these

459
00:27:22,280 --> 00:27:24,840
 things is actually a great amount of suffering.

460
00:27:24,840 --> 00:27:29,440
 When we don't get what we want, we suffer.

461
00:27:29,440 --> 00:27:31,870
 So we learn to just live with things as they are and we

462
00:27:31,870 --> 00:27:33,560
 learn to find happiness and peace

463
00:27:33,560 --> 00:27:39,840
 in all things of all types and from all sides of the

464
00:27:39,840 --> 00:27:42,960
 spectrum of reality.

465
00:27:42,960 --> 00:27:46,610
 So in a basic sense, we're talking about a state of

466
00:27:46,610 --> 00:27:49,340
 mindfulness and observation of good

467
00:27:49,340 --> 00:27:52,640
 and bad phenomena as they arise.

468
00:27:52,640 --> 00:27:56,000
 When bad things arise, learning to not get angry at them

469
00:27:56,000 --> 00:27:57,680
 and not see them as bad, to

470
00:27:57,680 --> 00:28:00,920
 give up this idea that certain phenomena are bad or

471
00:28:00,920 --> 00:28:03,520
 unpleasant and to broaden our horizons

472
00:28:03,520 --> 00:28:07,480
 and get out of this idea of being a human being stuck to

473
00:28:07,480 --> 00:28:10,040
 this one birth and so on, to

474
00:28:10,040 --> 00:28:14,500
 start to see reality as an experience or a series of

475
00:28:14,500 --> 00:28:17,760
 experiences of seeing, hearing,

476
00:28:17,760 --> 00:28:23,060
 smelling, tasting, feeling, thinking, a whole flux of

477
00:28:23,060 --> 00:28:26,240
 experience and to see that it comes

478
00:28:26,240 --> 00:28:29,770
 and it goes and to not become attached to it, to be like a

479
00:28:29,770 --> 00:28:31,800
 bird flying as we fly through

480
00:28:31,800 --> 00:28:38,120
 this stream of experience and to really live.

481
00:28:38,120 --> 00:28:42,820
 This is something where when you understand and you come to

482
00:28:42,820 --> 00:28:45,300
 attain this state or progress

483
00:28:45,300 --> 00:28:51,050
 along the path, your mind starts to become freed up and you

484
00:28:51,050 --> 00:28:53,600
 feel like a bird flying,

485
00:28:53,600 --> 00:28:56,840
 not clinging to anything.

486
00:28:56,840 --> 00:28:59,140
 And so this is sort of what we're aiming for and this is

487
00:28:59,140 --> 00:29:00,920
 what I hope to impress upon everyone

488
00:29:00,920 --> 00:29:05,720
 and actually when you look at it, it's a very simple thing

489
00:29:05,720 --> 00:29:06,480
 to do.

490
00:29:06,480 --> 00:29:11,130
 All of the Buddhist teaching are in the end for the purpose

491
00:29:11,130 --> 00:29:13,560
 of following this path, for

492
00:29:13,560 --> 00:29:18,580
 the purpose of bringing people to follow this path, this

493
00:29:18,580 --> 00:29:21,760
 way of constant alert and attention

494
00:29:21,760 --> 00:29:27,070
 and awareness of reality as it comes and goes, as it arises

495
00:29:27,070 --> 00:29:29,880
 and ceases through the tool or

496
00:29:29,880 --> 00:29:33,940
 the process or the practice of mindfulness so that in the

497
00:29:33,940 --> 00:29:36,000
 end we can come to see things

498
00:29:36,000 --> 00:29:41,320
 clearly and not be deluded or confused by them.

499
00:29:41,320 --> 00:29:44,230
 So that's the Dhamma that I thought I would give today and

500
00:29:44,230 --> 00:29:45,760
 now of course comes the time

501
00:29:45,760 --> 00:29:50,160
 where we put it into practice and we start to watch the

502
00:29:50,160 --> 00:29:52,320
 reality as it comes in.

503
00:29:52,320 --> 00:29:55,660
 So we should be mindful from moment to moment to moment to

504
00:29:55,660 --> 00:29:57,280
 the best of our ability.

505
00:29:57,280 --> 00:30:00,590
 So first we'll do the mindful prostration and then walking

506
00:30:00,590 --> 00:30:01,880
 and then sitting.

507
00:30:01,880 --> 00:30:11,880
 [BLANK_AUDIO]

