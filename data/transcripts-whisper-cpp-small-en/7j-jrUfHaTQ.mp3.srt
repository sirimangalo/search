1
00:00:00,000 --> 00:00:02,000
 I think a very important question.

2
00:00:02,000 --> 00:00:05,600
 Do you have any experience or advice that you can share

3
00:00:05,600 --> 00:00:09,000
 with people who suffer from depression?

4
00:00:09,000 --> 00:00:14,340
 Breath, meditation, and mindfulness helped me to be more

5
00:00:14,340 --> 00:00:16,000
 balanced.

6
00:00:16,000 --> 00:00:20,000
 How do I progress further from here?

7
00:00:20,000 --> 00:00:25,000
 Start looking at the depression.

8
00:00:25,000 --> 00:00:31,140
 You only suffer from depression because you cling to it,

9
00:00:31,140 --> 00:00:33,000
 because you see it as a problem.

10
00:00:33,000 --> 00:00:38,550
 If you see it as a condition, as an experience, and

11
00:00:38,550 --> 00:00:40,770
 moreover, if you see it as something that you can learn

12
00:00:40,770 --> 00:00:41,000
 from,

13
00:00:41,000 --> 00:00:44,000
 then you find that it's not really that scary.

14
00:00:44,000 --> 00:00:49,000
 Don't try to balance yourself. Learn about your imbalance.

15
00:00:49,000 --> 00:00:51,370
 But that's great. I mean, breath, meditation, and

16
00:00:51,370 --> 00:00:53,000
 mindfulness will help to balance you.

17
00:00:53,000 --> 00:00:56,000
 Learning about it will help to balance you.

18
00:00:56,000 --> 00:00:58,000
 Just don't be afraid of it.

19
00:00:58,000 --> 00:01:03,520
 Yeah. The fact that you're asking the question, it's hard

20
00:01:03,520 --> 00:01:07,000
 because you're not here with me meditating.

21
00:01:07,000 --> 00:01:11,240
 So all these specific questions, it's hard to know if you

22
00:01:11,240 --> 00:01:14,000
're giving the exact advice that the person needs.

23
00:01:14,000 --> 00:01:19,210
 But in general, when you seem to feel like you've hit a

24
00:01:19,210 --> 00:01:22,000
 block, you say, "Okay, it's helped me."

25
00:01:22,000 --> 00:01:24,000
 But what's next?

26
00:01:24,000 --> 00:01:27,830
 And that's kind of... The idea is, what I get from that, is

27
00:01:27,830 --> 00:01:29,000
 that you're still depressed.

28
00:01:29,000 --> 00:01:31,370
 You're better able to deal with it, but you're still

29
00:01:31,370 --> 00:01:32,000
 depressed.

30
00:01:32,000 --> 00:01:34,670
 So what's the next step that's really going to help you get

31
00:01:34,670 --> 00:01:36,000
 rid of your depression?

32
00:01:36,000 --> 00:01:39,560
 And that shows that you're kind of missing... You may be

33
00:01:39,560 --> 00:01:41,000
 kind of missing the point,

34
00:01:41,000 --> 00:01:45,460
 and the point is not to strive for balance, not to strive

35
00:01:45,460 --> 00:01:49,000
 for escape from the depression,

36
00:01:49,000 --> 00:01:52,630
 but to learn about it. Because you wouldn't ask the

37
00:01:52,630 --> 00:01:54,000
 question, "What's next?"

38
00:01:54,000 --> 00:01:57,890
 or "How do I progress further?" if all of your problems

39
00:01:57,890 --> 00:01:59,000
 were gone.

40
00:01:59,000 --> 00:02:02,800
 So if you still have problems, then the only thing I can

41
00:02:02,800 --> 00:02:05,000
 suggest is to look at them.

42
00:02:05,000 --> 00:02:07,500
 Stop being afraid of them. Stop trying to get away from

43
00:02:07,500 --> 00:02:08,000
 them.

44
00:02:08,000 --> 00:02:13,000
 Start learning about them. Start examining them.

45
00:02:13,000 --> 00:02:19,930
 Think of it as a laboratory, as your school. "Playground"

46
00:02:19,930 --> 00:02:21,000
 is a bad word.

47
00:02:21,000 --> 00:02:27,000
 You touched on this question before.

