1
00:00:00,000 --> 00:00:04,320
 Okay, good evening everyone.

2
00:00:04,320 --> 00:00:10,280
 Welcome to our evening dhamma.

3
00:00:10,280 --> 00:00:22,430
 Tonight we're looking at samavitaka, which is usually

4
00:00:22,430 --> 00:00:28,480
 translated as right thought.

5
00:00:28,480 --> 00:00:31,750
 Usually I heard someone say, "Oh no, right thought's

6
00:00:31,750 --> 00:00:34,040
 probably not a good translation."

7
00:00:34,040 --> 00:00:35,440
 And I almost changed it.

8
00:00:35,440 --> 00:00:43,440
 I almost put something different, but I changed it back.

9
00:00:43,440 --> 00:00:44,480
 Because it's simple.

10
00:00:44,480 --> 00:00:46,520
 Right thought's easy to remember.

11
00:00:46,520 --> 00:00:49,560
 It's not a mouthful like anything else.

12
00:00:49,560 --> 00:00:52,060
 And thought is an interesting word.

13
00:00:52,060 --> 00:00:59,560
 It's a good word because of how many uses we have for it.

14
00:00:59,560 --> 00:01:00,560
 We use thought.

15
00:01:00,560 --> 00:01:04,480
 When we say right thought, immediately we think of thought.

16
00:01:04,480 --> 00:01:05,480
 Right?

17
00:01:05,480 --> 00:01:06,480
 "I like turtles."

18
00:01:06,480 --> 00:01:14,240
 That's a thought example.

19
00:01:14,240 --> 00:01:19,000
 We have a thought arise and we think, "Well, that's what we

20
00:01:19,000 --> 00:01:20,720
 mean by thought."

21
00:01:20,720 --> 00:01:23,980
 But when you think of something, when you think about

22
00:01:23,980 --> 00:01:25,800
 something, we also use that for

23
00:01:25,800 --> 00:01:30,080
 thought.

24
00:01:30,080 --> 00:01:35,960
 I think Buddhism is good.

25
00:01:35,960 --> 00:01:43,920
 I think meditation is great.

26
00:01:43,920 --> 00:01:48,680
 That's a judgment, we would say.

27
00:01:48,680 --> 00:01:54,800
 Or I'm thinking about suicide.

28
00:01:54,800 --> 00:01:58,340
 I'm thinking of killing myself.

29
00:01:58,340 --> 00:01:59,760
 That's not just a thought.

30
00:01:59,760 --> 00:02:03,760
 It's an inclination.

31
00:02:03,760 --> 00:02:09,250
 And it's the inclinations, I think, that we're most

32
00:02:09,250 --> 00:02:11,800
 concerned with here.

33
00:02:11,800 --> 00:02:12,800
 This monk was right.

34
00:02:12,800 --> 00:02:17,080
 Right thought is a dangerous translation, it's a

35
00:02:17,080 --> 00:02:20,620
 problematic translation because we're

36
00:02:20,620 --> 00:02:22,440
 certainly not talking about thoughts.

37
00:02:22,440 --> 00:02:25,960
 And it gives the idea that a thought itself can be wrong.

38
00:02:25,960 --> 00:02:31,850
 You know, if a thought arises in your mind, I wonder what

39
00:02:31,850 --> 00:02:37,240
 it would be like to kill myself.

40
00:02:37,240 --> 00:02:40,280
 Maybe the answer to life's problems is to kill myself.

41
00:02:40,280 --> 00:02:42,860
 A thought like that can arise, or you imagine doing

42
00:02:42,860 --> 00:02:44,860
 something terrible and you think about

43
00:02:44,860 --> 00:02:48,240
 killing someone and just the thought arises.

44
00:02:48,240 --> 00:02:51,240
 It can arise without any inclination on your part to do it.

45
00:02:51,240 --> 00:02:54,290
 You can be horrified by the thought or just find it

46
00:02:54,290 --> 00:02:56,680
 ridiculous that you would think such

47
00:02:56,680 --> 00:03:00,840
 a thing.

48
00:03:00,840 --> 00:03:03,340
 And that's important because the thoughts themselves are

49
00:03:03,340 --> 00:03:04,120
 not a problem.

50
00:03:04,120 --> 00:03:06,400
 It's important to get across.

51
00:03:06,400 --> 00:03:10,760
 Others often feel very guilty about their thoughts.

52
00:03:10,760 --> 00:03:14,760
 You think of something and you think, "Oh, that's so wrong

53
00:03:14,760 --> 00:03:15,920
 to think that.

54
00:03:15,920 --> 00:03:18,960
 I shouldn't think that as a meditator."

55
00:03:18,960 --> 00:03:22,080
 That's not the problem.

56
00:03:22,080 --> 00:03:24,280
 What we're really concerned with here is your inclinations.

57
00:03:24,280 --> 00:03:25,960
 Are you inclined to kill yourself?

58
00:03:25,960 --> 00:03:29,680
 Are you inclined to kill someone else?

59
00:03:29,680 --> 00:03:33,440
 If so, there's a problem.

60
00:03:33,440 --> 00:03:35,840
 Are you inclined to think of these things as problems?

61
00:03:35,840 --> 00:03:40,640
 That's a problem as well.

62
00:03:40,640 --> 00:03:45,040
 It's our judgments and our reactions.

63
00:03:45,040 --> 00:03:46,400
 This is what we talk about.

64
00:03:46,400 --> 00:03:52,130
 Samvitaka is being free from me, having the opposite kinds,

65
00:03:52,130 --> 00:03:55,840
 having good kinds of inclinations,

66
00:03:55,840 --> 00:04:03,920
 inclining towards meditation, inclining towards being kind

67
00:04:03,920 --> 00:04:05,640
 to others.

68
00:04:05,640 --> 00:04:08,920
 Good inclinations.

69
00:04:08,920 --> 00:04:16,420
 Good thought is the other member of the wisdom faculty, the

70
00:04:16,420 --> 00:04:20,200
 wisdom group of the Eight Full

71
00:04:20,200 --> 00:04:21,200
 Noble Path.

72
00:04:21,200 --> 00:04:27,480
 So we have right view and then right thought.

73
00:04:27,480 --> 00:04:29,520
 That's important to understand the distinction.

74
00:04:29,520 --> 00:04:33,160
 Right view is what you believe.

75
00:04:33,160 --> 00:04:35,800
 "I believe this is right.

76
00:04:35,800 --> 00:04:38,800
 I believe this is wrong.

77
00:04:38,800 --> 00:04:42,120
 I should kill myself.

78
00:04:42,120 --> 00:04:44,280
 I should kill that person.

79
00:04:44,280 --> 00:04:45,640
 It's right to do.

80
00:04:45,640 --> 00:04:47,520
 Killing is good.

81
00:04:47,520 --> 00:04:49,520
 Stealing is good.

82
00:04:49,520 --> 00:04:52,160
 Getting angry, that's a good thing.

83
00:04:52,160 --> 00:04:55,120
 This is all wrong view.

84
00:04:55,120 --> 00:04:59,470
 Right view is when you say, when you know anger, that's not

85
00:04:59,470 --> 00:05:00,360
 useful.

86
00:05:00,360 --> 00:05:03,640
 It's a cause for suffering.

87
00:05:03,640 --> 00:05:08,880
 Desire, that's also not useful.

88
00:05:08,880 --> 00:05:16,480
 That's your view when you know these things to be true.

89
00:05:16,480 --> 00:05:17,480
 It's actually a lot easier.

90
00:05:17,480 --> 00:05:22,280
 I mean, it's very difficult even to come to that view.

91
00:05:22,280 --> 00:05:25,570
 That's something we should all be quite happy about and

92
00:05:25,570 --> 00:05:28,120
 confident about, the fact that we're

93
00:05:28,120 --> 00:05:32,080
 in a state where we think meditation is good.

94
00:05:32,080 --> 00:05:37,460
 We have this view that meditation is beneficial and that

95
00:05:37,460 --> 00:05:40,600
 getting caught up in the world is

96
00:05:40,600 --> 00:05:41,600
 problematic.

97
00:05:41,600 --> 00:05:46,980
 But it's much more difficult and it's a whole other thing

98
00:05:46,980 --> 00:05:49,800
 to give up the inclination for

99
00:05:49,800 --> 00:05:52,340
 those things.

100
00:05:52,340 --> 00:05:56,720
 You can know that a drug addict knows that the drugs are

101
00:05:56,720 --> 00:05:59,320
 really, really a bad idea.

102
00:05:59,320 --> 00:06:06,800
 But it doesn't mean they don't do drugs.

103
00:06:06,800 --> 00:06:08,620
 They're all the time doing them, all the time that they're

104
00:06:08,620 --> 00:06:09,720
 doing them, they know that this

105
00:06:09,720 --> 00:06:12,240
 is wrong, this is a problem.

106
00:06:12,240 --> 00:06:13,840
 They still do the drugs.

107
00:06:13,840 --> 00:06:17,080
 It's because they don't have right thought yet.

108
00:06:17,080 --> 00:06:18,080
 They have right view.

109
00:06:18,080 --> 00:06:21,600
 But they don't have the right inclination.

110
00:06:21,600 --> 00:06:23,760
 That's what we work on in meditation, really.

111
00:06:23,760 --> 00:06:30,520
 That's where our meditation practice takes place.

112
00:06:30,520 --> 00:06:34,740
 Along the way, right view comes up and right view cuts off

113
00:06:34,740 --> 00:06:36,360
 the inclination.

114
00:06:36,360 --> 00:06:39,120
 When you're first meditating, you don't have either, but

115
00:06:39,120 --> 00:06:40,920
 you work on the right inclination.

116
00:06:40,920 --> 00:06:46,320
 You incline yourself to understand things.

117
00:06:46,320 --> 00:06:47,920
 You incline yourself towards meditation.

118
00:06:47,920 --> 00:06:49,880
 You say, "Okay, well, I'll try this.

119
00:06:49,880 --> 00:06:51,640
 Maybe it's a good thing."

120
00:06:51,640 --> 00:06:52,640
 Right inclination.

121
00:06:52,640 --> 00:06:54,720
 So you work at it.

122
00:06:54,720 --> 00:06:59,140
 You work at purifying your mind, stopping your reactions.

123
00:06:59,140 --> 00:07:01,160
 You work at being objective towards things.

124
00:07:01,160 --> 00:07:04,840
 After you do that, eventually right view arises.

125
00:07:04,840 --> 00:07:07,240
 With the realization of nibbana, right view arises.

126
00:07:07,240 --> 00:07:11,940
 You say, "Yes, yes, these things are good and yes, those

127
00:07:11,940 --> 00:07:13,560
 things are bad."

128
00:07:13,560 --> 00:07:19,240
 That right view supports your right intention.

129
00:07:19,240 --> 00:07:24,800
 But then you have to work on the right intention again.

130
00:07:24,800 --> 00:07:28,040
 Only this time now you know that it's wrong.

131
00:07:28,040 --> 00:07:31,540
 But even someone who knows, someone who has seen nibbana,

132
00:07:31,540 --> 00:07:33,440
 still gets greedy, still gets

133
00:07:33,440 --> 00:07:41,890
 angry, still has wrong inclination, potentially, until they

134
00:07:41,890 --> 00:07:45,360
 work it out completely.

135
00:07:45,360 --> 00:07:52,430
 The Buddha likened it to someone who is wearing clean white

136
00:07:52,430 --> 00:07:56,400
 clothes and perfumed and really

137
00:07:56,400 --> 00:07:59,720
 concerned about cleanliness.

138
00:07:59,720 --> 00:08:04,260
 But then they're walking down this path and suddenly they

139
00:08:04,260 --> 00:08:06,760
 say, "Bull elephant charging

140
00:08:06,760 --> 00:08:09,880
 at them."

141
00:08:09,880 --> 00:08:14,380
 Then they look off to the side of the path and there's a

142
00:08:14,380 --> 00:08:15,600
 cesspool.

143
00:08:15,600 --> 00:08:21,400
 Cesspools are, there's feces and urine and lots and lots of

144
00:08:21,400 --> 00:08:23,120
 gross things.

145
00:08:23,120 --> 00:08:28,270
 And they jump into the cesspool to avoid the charging

146
00:08:28,270 --> 00:08:29,600
 elephant.

147
00:08:29,600 --> 00:08:32,000
 This Buddha likened this sort of thing.

148
00:08:32,000 --> 00:08:36,180
 A sotapan knows that it's wrong, knows that these things

149
00:08:36,180 --> 00:08:37,240
 are wrong.

150
00:08:37,240 --> 00:08:40,660
 But in order to get away from the chelesa, the anger is

151
00:08:40,660 --> 00:08:43,280
 overwhelming, the fear is overwhelming,

152
00:08:43,280 --> 00:08:49,770
 the greed is overwhelming, the desire, the lust, they do it

153
00:08:49,770 --> 00:08:49,840
.

154
00:08:49,840 --> 00:08:51,960
 But once they do it, they feel awful.

155
00:08:51,960 --> 00:08:55,320
 They feel dirty, they feel disgusted.

156
00:08:55,320 --> 00:08:58,920
 They'll never say to themselves, "That was a good idea."

157
00:08:58,920 --> 00:09:03,800
 Well, it breaks down there because the person who jumped

158
00:09:03,800 --> 00:09:06,440
 into the cesspool probably it was

159
00:09:06,440 --> 00:09:11,520
 a good idea to avoid the elephant.

160
00:09:11,520 --> 00:09:14,950
 But I suppose you could go further and say it's like they

161
00:09:14,950 --> 00:09:16,720
 knew that the right thing to

162
00:09:16,720 --> 00:09:21,520
 do would be to somehow tame the elephant.

163
00:09:21,520 --> 00:09:25,160
 But they weren't strong enough to tame that elephant.

164
00:09:25,160 --> 00:09:28,520
 So they jumped into the cesspool and they know it wasn't a

165
00:09:28,520 --> 00:09:29,440
 good thing.

166
00:09:29,440 --> 00:09:30,440
 It wasn't pleasant.

167
00:09:30,440 --> 00:09:39,400
 They're not happy about having jumped in the cesspool.

168
00:09:39,400 --> 00:09:42,240
 That's how it becomes as you practice.

169
00:09:42,240 --> 00:09:45,760
 It's important to understand the orderliness of this.

170
00:09:45,760 --> 00:09:48,560
 The first thing to go away is wrong view.

171
00:09:48,560 --> 00:09:49,880
 In the beginning it's wrong view.

172
00:09:49,880 --> 00:09:53,680
 It goes away because you have faith.

173
00:09:53,680 --> 00:09:56,760
 I believe in what the Buddha taught.

174
00:09:56,760 --> 00:10:01,910
 And that gives you enough of confidence to cultivate right

175
00:10:01,910 --> 00:10:04,720
 thought, right intention.

176
00:10:04,720 --> 00:10:09,570
 But once you hit the Arya manga, the noble path, wrong view

177
00:10:09,570 --> 00:10:10,720
 is cut off.

178
00:10:10,720 --> 00:10:15,780
 So never again will you think, you never doubt, is it right

179
00:10:15,780 --> 00:10:18,280
 to get attached to things?

180
00:10:18,280 --> 00:10:22,600
 Is it right to desire after anything in the world?

181
00:10:22,600 --> 00:10:26,960
 You won't have that thought, you won't have that view.

182
00:10:26,960 --> 00:10:29,600
 But you'll still desire those things.

183
00:10:29,600 --> 00:10:31,600
 You'll still get angry.

184
00:10:31,600 --> 00:10:33,080
 These things will still come up.

185
00:10:33,080 --> 00:10:35,880
 But you'll feel awful afterwards.

186
00:10:35,880 --> 00:10:38,640
 You'll see.

187
00:10:38,640 --> 00:10:43,120
 The Buddha mentions three types of samavitaka.

188
00:10:43,120 --> 00:10:47,520
 I'm sorry, samavitaka.

189
00:10:47,520 --> 00:10:48,520
 I'm using the wrong term.

190
00:10:48,520 --> 00:10:52,920
 I'm using samasankappa.

191
00:10:52,920 --> 00:10:54,920
 I'm mixing up my translations here.

192
00:10:54,920 --> 00:10:57,240
 The Pali word is not vitaka.

193
00:10:57,240 --> 00:11:00,640
 It's the same thing, but samkappa.

194
00:11:00,640 --> 00:11:05,640
 Samkappa is the samaditi samasankappa.

195
00:11:05,640 --> 00:11:09,160
 See what a teacher I am.

196
00:11:09,160 --> 00:11:12,760
 Samasankappa means the same thing, right thought.

197
00:11:12,760 --> 00:11:19,480
 So the three kinds of right thought are nikama samkappa.

198
00:11:19,480 --> 00:11:22,520
 They're also called vitaka somewhere else.

199
00:11:22,520 --> 00:11:30,120
 Nikama samkappa, abhayapada samkappa, and avihingsa samk

200
00:11:30,120 --> 00:11:31,040
appa.

201
00:11:31,040 --> 00:11:39,770
 Vitama is the inclination to renounce, to give up, the

202
00:11:39,770 --> 00:11:43,760
 inclination to let go.

203
00:11:43,760 --> 00:11:54,480
 Abhayapada samkappa is the inclination to not get angry,

204
00:11:54,480 --> 00:11:56,600
 inclination not to hate people

205
00:11:56,600 --> 00:12:00,720
 or hate things.

206
00:12:00,720 --> 00:12:11,920
 And avihingsa samkappa is the inclination not to be cruel.

207
00:12:11,920 --> 00:12:17,860
 And I'm still not clear exactly the difference between abh

208
00:12:17,860 --> 00:12:20,840
ayapada and avihingsa, but it's

209
00:12:20,840 --> 00:12:28,380
 clear with the, or vayapada and mihingsa, but abhayapada

210
00:12:28,380 --> 00:12:30,440
 means meta.

211
00:12:30,440 --> 00:12:34,590
 And the commentary says that not hating people, not being

212
00:12:34,590 --> 00:12:37,840
 angry towards people is having love,

213
00:12:37,840 --> 00:12:42,040
 the opposite of hate.

214
00:12:42,040 --> 00:12:47,440
 And non-cruelty is karuna, is compassion.

215
00:12:47,440 --> 00:12:51,300
 So I was trying to think how that differs, how it would

216
00:12:51,300 --> 00:12:53,440
 differ to be angry at someone

217
00:12:53,440 --> 00:12:54,840
 and to be cruel towards someone.

218
00:12:54,840 --> 00:12:58,540
 I think there are cases where sometimes you just have

219
00:12:58,540 --> 00:13:01,000
 disdain or lack of compassion for

220
00:13:01,000 --> 00:13:02,000
 someone.

221
00:13:02,000 --> 00:13:04,500
 You don't hate them, but you don't care that they're

222
00:13:04,500 --> 00:13:05,760
 suffering, right?

223
00:13:05,760 --> 00:13:09,260
 It doesn't bother you, it doesn't concern you, it doesn't

224
00:13:09,260 --> 00:13:10,920
 move you to stop what you're

225
00:13:10,920 --> 00:13:13,440
 doing.

226
00:13:13,440 --> 00:13:21,360
 So that's vihingsa, you do something that is cruel, which

227
00:13:21,360 --> 00:13:24,720
 is distorted, right?

228
00:13:24,720 --> 00:13:29,240
 Because we have this inclination not to suffer.

229
00:13:29,240 --> 00:13:32,310
 We wouldn't ever want someone to, it sounds cliche, but we

230
00:13:32,310 --> 00:13:33,840
 wouldn't ever want someone

231
00:13:33,840 --> 00:13:36,840
 to do that to us.

232
00:13:36,840 --> 00:13:39,200
 And it sounds like, well, that's just a nice philosophy,

233
00:13:39,200 --> 00:13:41,640
 but it actually, it makes sense.

234
00:13:41,640 --> 00:13:45,560
 You're not inclined towards these states for yourself.

235
00:13:45,560 --> 00:13:48,430
 It's not something that you think of, "Hey, that's a good

236
00:13:48,430 --> 00:13:48,920
 idea.

237
00:13:48,920 --> 00:13:51,000
 Let's hurt her, let's be hurt.

238
00:13:51,000 --> 00:13:52,960
 Let's suffer."

239
00:13:52,960 --> 00:13:56,320
 The idea of it is repulsive towards you.

240
00:13:56,320 --> 00:13:59,770
 So to have that on the one hand and on the other hand, to

241
00:13:59,770 --> 00:14:02,040
 be perfectly fine with it happening

242
00:14:02,040 --> 00:14:05,280
 to someone else, there's a disconnect there.

243
00:14:05,280 --> 00:14:07,520
 It may not seem like it, it seems like a natural thing.

244
00:14:07,520 --> 00:14:10,880
 Well, of course, dog eat dog, everyone for themselves.

245
00:14:10,880 --> 00:14:13,620
 But the interesting thing is, is when you meditate, and

246
00:14:13,620 --> 00:14:15,520
 meditation isn't on these things.

247
00:14:15,520 --> 00:14:19,560
 Meditate means when you're mindful, when you learn how to

248
00:14:19,560 --> 00:14:21,720
 see just ordinary experience

249
00:14:21,720 --> 00:14:25,520
 as it is, a remarkable thing happens.

250
00:14:25,520 --> 00:14:31,400
 You are inclined, it balances out, it comes into focus.

251
00:14:31,400 --> 00:14:37,130
 And you lose this selfishness, which is, it's not real, it

252
00:14:37,130 --> 00:14:39,640
's not a natural state.

253
00:14:39,640 --> 00:14:44,020
 It's not the ultimate balanced state to be only concerned

254
00:14:44,020 --> 00:14:45,560
 about yourself.

255
00:14:45,560 --> 00:14:49,440
 A person who is objective has no cruelty.

256
00:14:49,440 --> 00:14:53,150
 They see their own suffering and someone else's suffering

257
00:14:53,150 --> 00:14:55,040
 really has the same thing.

258
00:14:55,040 --> 00:15:01,520
 They're not inclined towards either, and they're inclined

259
00:15:01,520 --> 00:15:06,320
 to be compassionate.

260
00:15:06,320 --> 00:15:09,390
 And the same with loving, they're inclined to be loving,

261
00:15:09,390 --> 00:15:11,320
 they're not inclined to be angry

262
00:15:11,320 --> 00:15:14,200
 towards others.

263
00:15:14,200 --> 00:15:20,160
 They don't hate others, they don't hate themselves.

264
00:15:20,160 --> 00:15:22,150
 So there's not a lot to say, this is, I mean, it's an

265
00:15:22,150 --> 00:15:24,800
 important teaching, it's quite simple.

266
00:15:24,800 --> 00:15:28,090
 It's important to understand this distinction, right

267
00:15:28,090 --> 00:15:29,680
 thought and right view.

268
00:15:29,680 --> 00:15:34,680
 Samadhi, samasankappa.

269
00:15:34,680 --> 00:15:39,230
 Right view is our beliefs, right thought is actually our in

270
00:15:39,230 --> 00:15:41,200
 practice, what we, what we

271
00:15:41,200 --> 00:15:44,720
 are inclined towards.

272
00:15:44,720 --> 00:15:45,720
 But they're related, of course.

273
00:15:45,720 --> 00:15:49,300
 As I said, our right inclination leads to right view, right

274
00:15:49,300 --> 00:15:50,720
 view leads of course to

275
00:15:50,720 --> 00:15:53,800
 right inclination, they work together.

276
00:15:53,800 --> 00:15:56,780
 So this together is the wisdom, this is a person who has

277
00:15:56,780 --> 00:15:58,440
 these two, is considered to

278
00:15:58,440 --> 00:15:59,440
 be wise.

279
00:15:59,440 --> 00:16:02,840
 This is the wisdom, quite simple, no?

280
00:16:02,840 --> 00:16:10,960
 The wisdom, the portion of the Eightfold Noble Path.

281
00:16:10,960 --> 00:16:16,120
 So questions.

282
00:16:16,120 --> 00:16:18,340
 Practicing formal meditation is one of the first things you

283
00:16:18,340 --> 00:16:19,460
 do in the morning, be better

284
00:16:19,460 --> 00:16:24,190
 than waiting until after breakfast or other waking

285
00:16:24,190 --> 00:16:25,640
 activities.

286
00:16:25,640 --> 00:16:31,560
 It's the kind of thing you should be doing during your

287
00:16:31,560 --> 00:16:35,400
 waking activities, ideally.

288
00:16:35,400 --> 00:16:36,880
 I can see where you're going with that.

289
00:16:36,880 --> 00:16:40,400
 I don't like to answer such questions because life is not

290
00:16:40,400 --> 00:16:42,440
 something you can organize.

291
00:16:42,440 --> 00:16:48,390
 It shouldn't be, it should be something that is, well you

292
00:16:48,390 --> 00:16:51,680
 have to learn about all aspects

293
00:16:51,680 --> 00:16:53,400
 of life.

294
00:16:53,400 --> 00:16:57,350
 And so if I said, this is good, you wouldn't want to

295
00:16:57,350 --> 00:16:58,760
 neglect that.

296
00:16:58,760 --> 00:17:02,720
 So meditate when you can.

297
00:17:02,720 --> 00:17:08,750
 If it seems natural to meditate before you eat, or before

298
00:17:08,750 --> 00:17:11,840
 you do things anything in the

299
00:17:11,840 --> 00:17:14,240
 morning, then great.

300
00:17:14,240 --> 00:17:16,240
 It feels natural to do those things.

301
00:17:16,240 --> 00:17:19,690
 One thing I could say is the commentary does make mention

302
00:17:19,690 --> 00:17:21,760
 of the fact that for monks, most

303
00:17:21,760 --> 00:17:27,500
 especially, or probably mainly for monks, it's better to

304
00:17:27,500 --> 00:17:30,200
 eat before you meditate.

305
00:17:30,200 --> 00:17:34,670
 So they'd have some rice grill in the morning because it

306
00:17:34,670 --> 00:17:37,120
 calms your body down and it gives

307
00:17:37,120 --> 00:17:43,940
 you some strength and it actually makes it easier to med

308
00:17:43,940 --> 00:17:45,200
itate.

309
00:17:45,200 --> 00:17:47,000
 I don't think that's something to rely upon.

310
00:17:47,000 --> 00:17:52,080
 The commentary often says these things.

311
00:17:52,080 --> 00:17:55,280
 It's easy to understand, but it's a good example.

312
00:17:55,280 --> 00:17:59,240
 It can be easier after you eat and that's a good thing.

313
00:17:59,240 --> 00:18:01,200
 But what about before you eat and when it's difficult?

314
00:18:01,200 --> 00:18:03,960
 Are you going to ignore the difficult state?

315
00:18:03,960 --> 00:18:07,840
 Are you going to only be inclined towards the good one?

316
00:18:07,840 --> 00:18:11,710
 So that's what I mean by if I answer one way, then you

317
00:18:11,710 --> 00:18:13,640
 neglect the other way.

318
00:18:13,640 --> 00:18:18,210
 So it's best to be natural about it and to be in tune with

319
00:18:18,210 --> 00:18:20,680
 your rhythm and to see where

320
00:18:20,680 --> 00:18:24,430
 you have problems that need to be worked out and slowly

321
00:18:24,430 --> 00:18:25,600
 adjust them.

322
00:18:25,600 --> 00:18:32,240
 And just work towards that which is easy or convenient.

323
00:18:32,240 --> 00:18:36,960
 So the point of better and worse, not really that valid.

324
00:18:36,960 --> 00:18:41,200
 You want to eventually be mindful all the time.

325
00:18:41,200 --> 00:18:45,510
 No name is asking lots of questions, but it appears that

326
00:18:45,510 --> 00:18:48,400
 this person is practicing a different

327
00:18:48,400 --> 00:18:49,720
 school of meditation.

328
00:18:49,720 --> 00:18:54,430
 So I'd stop you there and suggest that you read my book on

329
00:18:54,430 --> 00:18:56,200
 how to meditate.

330
00:18:56,200 --> 00:19:03,460
 And I think I'm only inclined to answer meditation

331
00:19:03,460 --> 00:19:08,560
 questions about our tradition.

332
00:19:08,560 --> 00:19:10,770
 And you're asking questions whether I've experienced such

333
00:19:10,770 --> 00:19:12,280
 things and I'm not really interested in

334
00:19:12,280 --> 00:19:16,480
 answering questions about my own practice.

335
00:19:16,480 --> 00:19:21,080
 But if you ask how should one deal with something or what

336
00:19:21,080 --> 00:19:24,160
 is the Buddha's advice about fear,

337
00:19:24,160 --> 00:19:26,690
 I'd invite you to read my booklet first because if I'm

338
00:19:26,690 --> 00:19:28,760
 going to answer that, I'm going to

339
00:19:28,760 --> 00:19:33,440
 base it on what's in the booklet, base it on our tradition.

340
00:19:33,440 --> 00:19:38,440
 The answers are in there, but I can elaborate on it if you

341
00:19:38,440 --> 00:19:39,200
 like.

342
00:19:39,200 --> 00:19:42,240
 So yeah, the whole problem with fear, I mean I totally

343
00:19:42,240 --> 00:19:43,200
 sympathize.

344
00:19:43,200 --> 00:19:49,230
 But my advice to you, because you're coming to me for

345
00:19:49,230 --> 00:19:53,120
 advice, is to read my booklet.

346
00:19:53,120 --> 00:19:57,510
 It sounds self-serving and self-promoting, but I wrote it

347
00:19:57,510 --> 00:19:58,600
 for a reason.

348
00:19:58,600 --> 00:20:00,680
 I didn't take it from my own teachings.

349
00:20:00,680 --> 00:20:03,640
 I copied everything from what I'd learned.

350
00:20:03,640 --> 00:20:05,960
 So it's not really my book.

351
00:20:05,960 --> 00:20:15,960
 It's what I believe is the answer to things like fear.

352
00:20:15,960 --> 00:20:18,740
 Being mindful of the five aggregates, I find experience

353
00:20:18,740 --> 00:20:20,560
 leads to volition and thought, feeling

354
00:20:20,560 --> 00:20:21,560
 and body.

355
00:20:21,560 --> 00:20:23,730
 Would it be better to start with the mind-fulness of the

356
00:20:23,730 --> 00:20:25,320
 body instead of experience in order

357
00:20:25,320 --> 00:20:26,640
 to cut craving attachment?

358
00:20:26,640 --> 00:20:29,420
 Well, it's better to start with the body probably for a

359
00:20:29,420 --> 00:20:30,600
 different reason.

360
00:20:30,600 --> 00:20:35,360
 I think you may be complicating things a little too much.

361
00:20:35,360 --> 00:20:37,160
 The body is just gross.

362
00:20:37,160 --> 00:20:38,560
 Gross in a sense, it's coarse.

363
00:20:38,560 --> 00:20:43,760
 It's easy to find.

364
00:20:43,760 --> 00:20:46,000
 Your mind is not easy to find.

365
00:20:46,000 --> 00:20:50,410
 It's like a dog chasing its tail sometimes, but the body is

366
00:20:50,410 --> 00:20:52,200
 fairly easy to find.

367
00:20:52,200 --> 00:20:55,430
 The old texts say, "To what extent the body becomes clear

368
00:20:55,430 --> 00:20:57,200
 and you become clearly aware

369
00:20:57,200 --> 00:21:02,400
 of the body, to that extent the mind also becomes clear."

370
00:21:02,400 --> 00:21:09,200
 So simply focusing on the body, you already start to, you

371
00:21:09,200 --> 00:21:13,040
 will as a result begin to observe

372
00:21:13,040 --> 00:21:14,600
 the mind.

373
00:21:14,600 --> 00:21:18,520
 And then you jump off from there, focus on the mind.

374
00:21:18,520 --> 00:21:21,140
 If you start to think about something, if you start to

375
00:21:21,140 --> 00:21:22,720
 judge something, if you start

376
00:21:22,720 --> 00:21:29,360
 to feel something.

377
00:21:29,360 --> 00:21:32,120
 What kind of meditation is best to do in the morning?

378
00:21:32,120 --> 00:21:33,120
 Best meditation.

379
00:21:33,120 --> 00:21:37,400
 It's the best meditation to do anytime.

380
00:21:37,400 --> 00:21:40,520
 But you can also look up the talk I did on the four

381
00:21:40,520 --> 00:21:43,080
 protective meditations, I think it's

382
00:21:43,080 --> 00:21:45,000
 called.

383
00:21:45,000 --> 00:21:47,560
 Other types of meditation or something.

384
00:21:47,560 --> 00:21:49,960
 I've given some talks on that.

385
00:21:49,960 --> 00:21:53,950
 There are four types of meditation that are supportive of

386
00:21:53,950 --> 00:21:55,720
 insight meditation.

387
00:21:55,720 --> 00:21:58,520
 Maybe someday I'll do a talk on each one of those.

388
00:21:58,520 --> 00:22:04,600
 I think people would be very interested in those.

389
00:22:04,600 --> 00:22:07,320
 Okay so that's all the questions for tonight.

390
00:22:07,320 --> 00:22:09,280
 Thank you all for tuning in.

391
00:22:09,280 --> 00:22:10,280
 Bye.

