1
00:00:00,000 --> 00:00:09,280
 So the day continuing on.

2
00:00:09,280 --> 00:00:15,600
 More full frittat before we practice together.

3
00:00:15,600 --> 00:00:24,410
 A lot of the Buddha's teachings just saying the same thing

4
00:00:24,410 --> 00:00:28,320
 in different words or using

5
00:00:28,320 --> 00:00:32,200
 different terms.

6
00:00:32,200 --> 00:00:37,240
 So he talks about that which is of most help.

7
00:00:37,240 --> 00:00:41,920
 But then we have many things that are of most help.

8
00:00:41,920 --> 00:00:47,640
 So actually when you look at it, it's talking about the

9
00:00:47,640 --> 00:00:50,880
 same thing but from a different

10
00:00:50,880 --> 00:00:55,600
 point of view.

11
00:00:55,600 --> 00:01:03,470
 So he says at first, he says, "I can't think of anything or

12
00:01:03,470 --> 00:01:06,080
 I don't know of anything."

13
00:01:06,080 --> 00:01:12,260
 Buddha said, "I know of no dhamma that causes unwholesomen

14
00:01:12,260 --> 00:01:20,080
ess to arise more than negligent."

15
00:01:20,080 --> 00:01:31,440
 And that which is wholesome to fade away.

16
00:01:31,440 --> 00:01:41,630
 And he says, "I know of nothing that is as potent in giving

17
00:01:41,630 --> 00:01:45,760
 rise to wholesomeness and

18
00:01:45,760 --> 00:01:57,920
 driving away unwholesomeness as vigilance."

19
00:01:57,920 --> 00:02:02,720
 And then he goes on to talk about other dhammas that are of

20
00:02:02,720 --> 00:02:04,160
 equal value.

21
00:02:04,160 --> 00:02:17,870
 So in terms of one being useful and one being helpful and

22
00:02:17,870 --> 00:02:24,240
 one being a hindrance.

23
00:02:24,240 --> 00:02:31,640
 And then he says, "Kosa jang" which means indolence, laz

24
00:02:31,640 --> 00:02:35,760
iness or lounging around, finding

25
00:02:35,760 --> 00:02:41,440
 a look, wasting time.

26
00:02:41,440 --> 00:02:46,240
 And the opposite is putting out effort.

27
00:02:46,240 --> 00:03:01,200
 We are mba, we are mba, we are mba, putting out effort and

28
00:03:01,200 --> 00:03:02,640
 so on.

29
00:03:02,640 --> 00:03:08,880
 So saying the same thing using different words.

30
00:03:08,880 --> 00:03:13,790
 So the point being that in regards to wholesomeness there

31
00:03:13,790 --> 00:03:17,840
 are certain dhammas that are going to

32
00:03:17,840 --> 00:03:23,340
 cultivate wholesomeness and do away with unwholesomeness in

33
00:03:23,340 --> 00:03:24,480
 the mind.

34
00:03:24,480 --> 00:03:30,160
 We have vigilance versus negligence.

35
00:03:30,160 --> 00:03:33,120
 Or negligence versus vigilance.

36
00:03:33,120 --> 00:03:39,600
 We have idleness, laziness versus effort.

37
00:03:39,600 --> 00:03:46,950
 And then we have maha ichatta, having many desires, many

38
00:03:46,950 --> 00:03:51,200
 wants, wantingness and wanting

39
00:03:51,200 --> 00:03:59,120
 that versus having few desires, having few ambitions.

40
00:03:59,120 --> 00:04:15,120
 And then we have discontent and contentment.

41
00:04:15,120 --> 00:04:35,120
 And then we have wise attention and unwise attention.

42
00:04:35,120 --> 00:04:46,730
 And then we have unwise contemplation or thought and wise

43
00:04:46,730 --> 00:04:49,120
 thought, wise contemplation.

44
00:04:49,120 --> 00:04:55,020
 We have clear awareness, full awareness and lack of full

45
00:04:55,020 --> 00:04:57,520
 awareness and full awareness.

46
00:04:57,520 --> 00:05:06,000
 So there are quite a few things here.

47
00:05:06,000 --> 00:05:07,000
 The Buddhists are useful.

48
00:05:07,000 --> 00:05:09,890
 And the final one is having evil friends and having good

49
00:05:09,890 --> 00:05:10,560
 friends.

50
00:05:10,560 --> 00:05:15,620
 So we can look at each of these in turn and see that

51
00:05:15,620 --> 00:05:19,440
 actually it's just many ways of explaining

52
00:05:19,440 --> 00:05:20,440
 our practice.

53
00:05:20,440 --> 00:05:22,610
 And these are all things that are very useful for us to

54
00:05:22,610 --> 00:05:23,640
 know and understand.

55
00:05:23,640 --> 00:05:26,270
 So the first one we've talked about a lot in the Dhammapada

56
00:05:26,270 --> 00:05:27,480
 videos we've been talking

57
00:05:27,480 --> 00:05:32,160
 about, vigilance.

58
00:05:32,160 --> 00:05:35,670
 I don't think I've mentioned to you what is the Buddhist

59
00:05:35,670 --> 00:05:37,440
 teaching on vigilance.

60
00:05:37,440 --> 00:05:40,270
 Basically I mentioned already that it means, in one place

61
00:05:40,270 --> 00:05:41,880
 the Buddha says it means to have

62
00:05:41,880 --> 00:05:46,200
 mindfulness all the time, to never be without mindfulness.

63
00:05:46,200 --> 00:05:51,810
 So there's this story I don't know if I told you when I was

64
00:05:51,810 --> 00:05:54,760
 teaching in Thailand once.

65
00:05:54,760 --> 00:05:58,380
 And all of the foreign meditators had been instructed to

66
00:05:58,380 --> 00:06:00,400
 practice a certain number of

67
00:06:00,400 --> 00:06:01,400
 hours a day.

68
00:06:01,400 --> 00:06:08,200
 So today is six hours, tomorrow eight hours and so on.

69
00:06:08,200 --> 00:06:10,280
 And then they came to me and asked me how many hours a day

70
00:06:10,280 --> 00:06:11,200
 they had to practice.

71
00:06:11,200 --> 00:06:16,200
 And I said, today you have to practice 18 hours.

72
00:06:16,200 --> 00:06:18,710
 And his eyes looked like they were going to pop out of his

73
00:06:18,710 --> 00:06:19,200
 head.

74
00:06:19,200 --> 00:06:23,010
 Because he was very quite stressed out about having to even

75
00:06:23,010 --> 00:06:24,960
 take eight hours, practice

76
00:06:24,960 --> 00:06:27,720
 eight hours a day.

77
00:06:27,720 --> 00:06:33,110
 So now he was trying to do the math and think, how can I

78
00:06:33,110 --> 00:06:35,440
 get out of this one?

79
00:06:35,440 --> 00:06:37,720
 And then I explained to him what I meant.

80
00:06:37,720 --> 00:06:40,090
 When you wake up first thing in the morning you have to be

81
00:06:40,090 --> 00:06:41,320
 mindful of lying there.

82
00:06:41,320 --> 00:06:44,720
 So you have to be yourself lying, lying, sit up, sitting,

83
00:06:44,720 --> 00:06:45,480
 sitting.

84
00:06:45,480 --> 00:06:48,250
 You can watch the rising and falling of the stomach and

85
00:06:48,250 --> 00:06:49,720
 then standing, standing.

86
00:06:49,720 --> 00:06:53,740
 When you go to the washroom, when you brush your teeth,

87
00:06:53,740 --> 00:06:55,760
 when you eat your food, when you

88
00:06:55,760 --> 00:06:59,160
 shower, when you use the toilet and so on, be mindful of

89
00:06:59,160 --> 00:07:00,640
 all of these things.

90
00:07:00,640 --> 00:07:04,890
 It doesn't matter how I don't, I'm not concerned about how

91
00:07:04,890 --> 00:07:07,080
 many hours of formal practice you

92
00:07:07,080 --> 00:07:12,680
 do but you're not to talk with the other meditators, you're

93
00:07:12,680 --> 00:07:19,440
 not to get involved in reading or idle

94
00:07:19,440 --> 00:07:21,040
 pursuits or so on.

95
00:07:21,040 --> 00:07:23,840
 You have to practice for 18 hours a day.

96
00:07:23,840 --> 00:07:25,840
 When you sleep, that's your break.

97
00:07:25,840 --> 00:07:28,880
 You don't have to be mindful.

98
00:07:28,880 --> 00:07:29,880
 But the point is you can do anything.

99
00:07:29,880 --> 00:07:33,770
 You can have tea, you can, I was explaining to him but it

100
00:07:33,770 --> 00:07:36,680
 really actually calmed him down.

101
00:07:36,680 --> 00:07:41,340
 And made him less stressed than before I had given him this

102
00:07:41,340 --> 00:07:42,760
 arduous task.

103
00:07:42,760 --> 00:07:44,920
 Because the point is simply to be mindful.

104
00:07:44,920 --> 00:07:48,350
 It's not to do this or do that, to have to walk for so many

105
00:07:48,350 --> 00:07:49,840
 hours or sit for so many

106
00:07:49,840 --> 00:07:50,840
 hours.

107
00:07:50,840 --> 00:07:55,480
 You can live your natural life but do it mindful.

108
00:07:55,480 --> 00:07:59,320
 This is the meaning of appamanda, to live your life mind

109
00:07:59,320 --> 00:08:00,040
fully.

110
00:08:00,040 --> 00:08:02,680
 Whenever you do something, to do it mindful.

111
00:08:02,680 --> 00:08:04,680
 When you're walking, to walk mind.

112
00:08:04,680 --> 00:08:06,800
 You're eating to eat mind.

113
00:08:06,800 --> 00:08:08,680
 To be aware of it also.

114
00:08:08,680 --> 00:08:09,680
 It's actually quite different.

115
00:08:09,680 --> 00:08:11,800
 It's nothing like how you'd normally live your life.

116
00:08:11,800 --> 00:08:14,840
 But to the outside observer it looks quite normal.

117
00:08:14,840 --> 00:08:17,880
 When you're eating your food, scooping, raising.

118
00:08:17,880 --> 00:08:18,880
 You're aware of all of these.

119
00:08:18,880 --> 00:08:22,400
 When your teeth are chewing, you're aware of the tension in

120
00:08:22,400 --> 00:08:23,600
 the jaw and so on.

121
00:08:23,600 --> 00:08:28,260
 When you swallow, you're aware of the feeling in the throat

122
00:08:28,260 --> 00:08:29,560
 swallowing.

123
00:08:29,560 --> 00:08:31,680
 You make a note of everything as it occurs.

124
00:08:31,680 --> 00:08:34,160
 Seeing it, this is this is this.

125
00:08:34,160 --> 00:08:36,760
 Being aware of it just as it is.

126
00:08:36,760 --> 00:08:39,480
 And reminding yourself of it.

127
00:08:39,480 --> 00:08:40,480
 The opposite is appamada.

128
00:08:40,480 --> 00:08:43,480
 The opposite is pamada.

129
00:08:43,480 --> 00:08:49,240
 When a person spends all their time in idle pursuits.

130
00:08:49,240 --> 00:08:53,570
 Getting caught up in things that are unrelated to the goal

131
00:08:53,570 --> 00:08:55,360
 and have no purpose.

132
00:08:55,360 --> 00:08:57,360
 Watching television or sitting around chatting.

133
00:08:57,360 --> 00:09:00,280
 In Wat Lam Pong they would sit around and spend.

134
00:09:00,280 --> 00:09:02,840
 So they had the eight hours they had to do and then there's

135
00:09:02,840 --> 00:09:04,200
 ten hours that they're awake.

136
00:09:04,200 --> 00:09:06,560
 But they don't have to do anything.

137
00:09:06,560 --> 00:09:10,130
 And so they thought they could just sit around and talk

138
00:09:10,130 --> 00:09:11,440
 with each other.

139
00:09:11,440 --> 00:09:13,440
 Go to the store and so on.

140
00:09:13,440 --> 00:09:16,040
 And all these things that they would do.

141
00:09:16,040 --> 00:09:18,280
 Wat Lam Pong's in the city so they would get quite

142
00:09:18,280 --> 00:09:19,040
 distracted.

143
00:09:19,040 --> 00:09:21,680
 And cause a lot of problems for the other monks.

144
00:09:21,680 --> 00:09:24,810
 So when we put them on this routine, it changed the whole

145
00:09:24,810 --> 00:09:25,760
 atmosphere.

146
00:09:25,760 --> 00:09:32,310
 Then they were quiet and separate and contemplating their

147
00:09:32,310 --> 00:09:36,120
 own minds and their own behavior.

148
00:09:36,120 --> 00:09:39,480
 Their own actions at every moment.

149
00:09:39,480 --> 00:09:41,240
 During a meditation course it's quite necessary.

150
00:09:41,240 --> 00:09:44,060
 Actually for all of us this is something we have to strive

151
00:09:44,060 --> 00:09:44,640
 to do.

152
00:09:44,640 --> 00:09:46,640
 Even living in our daily lives.

153
00:09:46,640 --> 00:09:51,210
 When we go back home until I'm trying to be as mindful as

154
00:09:51,210 --> 00:09:52,440
 possible.

155
00:09:52,440 --> 00:09:55,160
 That's basically what that one means.

156
00:09:55,160 --> 00:09:56,760
 That's the core of the Buddha's teaching.

157
00:09:56,760 --> 00:09:59,880
 To train in this way.

158
00:09:59,880 --> 00:10:02,760
 Because it's what leads us to enlightenment.

159
00:10:02,760 --> 00:10:09,960
 It leads us to understand ourselves.

160
00:10:09,960 --> 00:10:12,040
 And then we have laziness and effort.

161
00:10:12,040 --> 00:10:15,790
 And we have to be careful to understand what we mean by

162
00:10:15,790 --> 00:10:16,960
 effort here.

163
00:10:16,960 --> 00:10:19,310
 When the Buddha talks about effort he doesn't mean just

164
00:10:19,310 --> 00:10:20,920
 pushing yourself and whatever you're

165
00:10:20,920 --> 00:10:21,920
 doing.

166
00:10:21,920 --> 00:10:24,760
 Whatever work you have to do to work hard at it.

167
00:10:24,760 --> 00:10:26,560
 That's not really what he means.

168
00:10:26,560 --> 00:10:30,000
 He means putting out effort to be mindful.

169
00:10:30,000 --> 00:10:33,720
 Because when we look at the Buddha's meaning of effort.

170
00:10:33,720 --> 00:10:36,440
 The meaning of effort is four things.

171
00:10:36,440 --> 00:10:40,520
 The unwholesomeness in your mind you work to get rid of it.

172
00:10:40,520 --> 00:10:41,800
 When it comes up you work.

173
00:10:41,800 --> 00:10:45,170
 You do the work that's necessary to remove it from the mind

174
00:10:45,170 --> 00:10:45,440
.

175
00:10:45,440 --> 00:10:48,720
 To see it clearly and to see the situation clearly.

176
00:10:48,720 --> 00:10:51,600
 So you don't have aversion or attachment in regards to the

177
00:10:51,600 --> 00:10:52,440
 experience.

178
00:10:52,440 --> 00:10:55,400
 When you don't have any delusion.

179
00:10:55,400 --> 00:10:59,200
 To work hard to keep the bad states out.

180
00:10:59,200 --> 00:11:01,950
 So when there is no greed or anger but you know that

181
00:11:01,950 --> 00:11:03,920
 something is coming up that will

182
00:11:03,920 --> 00:11:04,920
 lead to it.

183
00:11:04,920 --> 00:11:07,510
 You're careful to be mindful of it again and again and

184
00:11:07,510 --> 00:11:08,040
 again.

185
00:11:08,040 --> 00:11:09,360
 A happy feeling comes up.

186
00:11:09,360 --> 00:11:12,160
 You're careful to be mindful of it as happy.

187
00:11:12,160 --> 00:11:15,800
 You work hard at staying with the object.

188
00:11:15,800 --> 00:11:19,550
 Knowing that at any moment unwholesomeness could arise if

189
00:11:19,550 --> 00:11:21,080
 you're not mindful.

190
00:11:21,080 --> 00:11:22,880
 This takes quite a bit of effort.

191
00:11:22,880 --> 00:11:27,460
 This takes an exceptional amount of effort to continue

192
00:11:27,460 --> 00:11:29,800
 throughout the day to keep it

193
00:11:29,800 --> 00:11:31,080
 away.

194
00:11:31,080 --> 00:11:34,650
 And then the development, the effort is the other two are

195
00:11:34,650 --> 00:11:36,920
 the development of wholesomeness

196
00:11:36,920 --> 00:11:37,920
 that has endurance.

197
00:11:37,920 --> 00:11:41,610
 So this practice when you're not good at it yet is hard to

198
00:11:41,610 --> 00:11:43,480
 keep the defilements away

199
00:11:43,480 --> 00:11:44,680
 and the mind keeps going back.

200
00:11:44,680 --> 00:11:48,320
 So the work to keep it going.

201
00:11:48,320 --> 00:11:50,800
 This is just the opposite.

202
00:11:50,800 --> 00:11:51,800
 Once you have attained it.

203
00:11:51,800 --> 00:11:54,520
 So once you get good at the practice to stay on track.

204
00:11:54,520 --> 00:11:57,240
 It's very easy once you get good at the practice to become

205
00:11:57,240 --> 00:11:57,960
 negative.

206
00:11:57,960 --> 00:12:02,080
 "I'm good at this already and it's lack off."

207
00:12:02,080 --> 00:12:06,590
 It's also easy to become so intensely focused on the

208
00:12:06,590 --> 00:12:09,680
 practice or eager to reach the goal

209
00:12:09,680 --> 00:12:11,320
 that you over exert yourself.

210
00:12:11,320 --> 00:12:14,380
 Both of these are two extremes that we have to guard

211
00:12:14,380 --> 00:12:15,200
 against.

212
00:12:15,200 --> 00:12:17,660
 When your practice is good you also have to make your

213
00:12:17,660 --> 00:12:18,600
 practice good.

214
00:12:18,600 --> 00:12:23,680
 And you have to have the effort to keep yourself on track.

215
00:12:23,680 --> 00:12:26,040
 So what it's really meant here is the effort to stay

216
00:12:26,040 --> 00:12:26,680
 mindful.

217
00:12:26,680 --> 00:12:31,490
 The effort to keep your mind straight and to straighten the

218
00:12:31,490 --> 00:12:32,320
 mind.

219
00:12:32,320 --> 00:12:34,500
 Just like something that is bent you have to work hard to

220
00:12:34,500 --> 00:12:35,360
 straighten it.

221
00:12:35,360 --> 00:12:42,800
 And then you have to work hard to keep it straight.

222
00:12:42,800 --> 00:12:44,920
 We have many wishes and few wishes.

223
00:12:44,920 --> 00:12:49,260
 So if you have many wishes, here I think there's not such

224
00:12:49,260 --> 00:12:51,760
 an opportunity to have many wishes

225
00:12:51,760 --> 00:12:54,680
 but you can still be thinking a lot about what you want to

226
00:12:54,680 --> 00:12:57,000
 do when you go home or even

227
00:12:57,000 --> 00:13:02,770
 what you want to do here wanting to be a teacher or so on

228
00:13:02,770 --> 00:13:06,440
 or wanting to build a meditation

229
00:13:06,440 --> 00:13:07,440
 center.

230
00:13:07,440 --> 00:13:09,430
 All of these things can get in the way when you have many

231
00:13:09,430 --> 00:13:09,880
 wishes.

232
00:13:09,880 --> 00:13:13,040
 This is something very important to keep in mind.

233
00:13:13,040 --> 00:13:16,150
 That we shouldn't be wanting to have this or wanting to

234
00:13:16,150 --> 00:13:16,960
 have that.

235
00:13:16,960 --> 00:13:19,540
 And when you're thinking about when you go home you're

236
00:13:19,540 --> 00:13:21,120
 going to do this and do that.

237
00:13:21,120 --> 00:13:23,400
 They very much get in the way of your practice.

238
00:13:23,400 --> 00:13:28,260
 They get in the way of your contentment which is the next

239
00:13:28,260 --> 00:13:28,960
 one.

240
00:13:28,960 --> 00:13:32,090
 When a person has many wishes, as the Buddha said, it leads

241
00:13:32,090 --> 00:13:33,920
 to unwholesomeness and it leads

242
00:13:33,920 --> 00:13:36,520
 to wholesomeness to disappear.

243
00:13:36,520 --> 00:13:39,240
 Because you want this and you can never be satisfied.

244
00:13:39,240 --> 00:13:42,360
 You get what you want and you want more in anything.

245
00:13:42,360 --> 00:13:46,820
 Not just things like food and material but it can be in

246
00:13:46,820 --> 00:13:49,680
 becomingness and becoming that

247
00:13:49,680 --> 00:13:52,930
 or in removingness and removing that you'll never be

248
00:13:52,930 --> 00:13:53,880
 satisfied.

249
00:13:53,880 --> 00:13:58,650
 As long as your happiness depends on the situation that you

250
00:13:58,650 --> 00:13:59,520
're in.

251
00:13:59,520 --> 00:14:03,270
 And even here we can have many opportunities for the

252
00:14:03,270 --> 00:14:05,000
 arising of desires.

253
00:14:05,000 --> 00:14:07,400
 Maybe the food is good or maybe the food is not good.

254
00:14:07,400 --> 00:14:10,790
 And so you're thinking, "Oh, I wish I had this kind of food

255
00:14:10,790 --> 00:14:12,240
 or that kind of food."

256
00:14:12,240 --> 00:14:14,640
 Maybe you're thinking about home where you have a nice bed

257
00:14:14,640 --> 00:14:15,920
 or you have an air conditioner

258
00:14:15,920 --> 00:14:18,760
 or you have this or you have that.

259
00:14:18,760 --> 00:14:21,650
 All of these things are our desires that we have to be

260
00:14:21,650 --> 00:14:23,480
 careful about and work hard to

261
00:14:23,480 --> 00:14:27,760
 give up.

262
00:14:27,760 --> 00:14:31,240
 Contentment and discontent goes very much along with that.

263
00:14:31,240 --> 00:14:36,440
 It's really another way of saying having few desires.

264
00:14:36,440 --> 00:14:43,680
 But contentment is a particularly special mind state.

265
00:14:43,680 --> 00:14:47,750
 It means being content with whatever comes when there is

266
00:14:47,750 --> 00:14:50,000
 unpleasantness to be content

267
00:14:50,000 --> 00:14:51,000
 with that.

268
00:14:51,000 --> 00:14:54,430
 When there is pleasantness to be content with as much comes

269
00:14:54,430 --> 00:14:54,680
.

270
00:14:54,680 --> 00:15:00,190
 It means to be level headed or balanced in the mind unpert

271
00:15:00,190 --> 00:15:02,920
urbed by good things or bad

272
00:15:02,920 --> 00:15:03,920
 things.

273
00:15:03,920 --> 00:15:09,310
 It's really the same as another way of talking about wants

274
00:15:09,310 --> 00:15:10,640
 and needs.

275
00:15:10,640 --> 00:15:14,350
 But the Buddha said this is the greatest gain because when

276
00:15:14,350 --> 00:15:16,320
 you have contentment you don't

277
00:15:16,320 --> 00:15:19,320
 want for anything.

278
00:15:19,320 --> 00:15:24,160
 So I think we have to be content there because I've lost

279
00:15:24,160 --> 00:15:25,440
 the light.

280
00:15:25,440 --> 00:15:29,330
 I just want to talk about, I think there was a couple more

281
00:15:29,330 --> 00:15:31,440
 in there, but the last one was

282
00:15:31,440 --> 00:15:36,040
 having good friends and having evil friends.

283
00:15:36,040 --> 00:15:39,840
 So we can consider that the community here is a good friend

284
00:15:39,840 --> 00:15:41,880
 for us, that the other people

285
00:15:41,880 --> 00:15:43,960
 here are a good support for us.

286
00:15:43,960 --> 00:15:47,690
 I consider that all of you are a good support for my own

287
00:15:47,690 --> 00:15:50,400
 life in the sense that you provide

288
00:15:50,400 --> 00:15:52,280
 a community for me.

289
00:15:52,280 --> 00:15:54,970
 All of us, I think for sure, we can appreciate having the

290
00:15:54,970 --> 00:15:55,720
 community.

291
00:15:55,720 --> 00:15:59,330
 If you try to do this at home, try to practice evenness

292
00:15:59,330 --> 00:16:01,400
 that we do at night once in the morning,

293
00:16:01,400 --> 00:16:04,850
 at night to try to get up at 4.30 in the morning or 4 in

294
00:16:04,850 --> 00:16:06,880
 the morning to be able to practice

295
00:16:06,880 --> 00:16:09,400
 together at 4.30.

296
00:16:09,400 --> 00:16:12,520
 If you try to do it alone, you find that it's quite

297
00:16:12,520 --> 00:16:13,520
 difficult.

298
00:16:13,520 --> 00:16:18,210
 You have this incredible push that comes from having other

299
00:16:18,210 --> 00:16:20,800
 people around who are likewise

300
00:16:20,800 --> 00:16:25,590
 dedicated to the development of mindfulness and using

301
00:16:25,590 --> 00:16:28,440
 something around and getting to

302
00:16:28,440 --> 00:16:29,440
 know each other.

303
00:16:29,440 --> 00:16:32,830
 I always used to tell people in Thailand that the best med

304
00:16:32,830 --> 00:16:34,720
itator is someone who doesn't

305
00:16:34,720 --> 00:16:38,880
 even know the names of the other meditators.

306
00:16:38,880 --> 00:16:42,820
 If you can arrive and leave the center without knowing the

307
00:16:42,820 --> 00:16:44,760
 names of the other meditators,

308
00:16:44,760 --> 00:16:49,200
 I consider you to be an exceptional meditator.

309
00:16:49,200 --> 00:16:53,400
 I think it's hard to find such a meditator.

310
00:16:53,400 --> 00:16:58,500
 So if you haven't fulfilled that one, you can see where it

311
00:16:58,500 --> 00:16:59,640
's going.

312
00:16:59,640 --> 00:17:02,290
 The point about having good friends is not that we spend

313
00:17:02,290 --> 00:17:03,240
 time together.

314
00:17:03,240 --> 00:17:05,080
 The Buddha was very critical of this.

315
00:17:05,080 --> 00:17:09,280
 So at the same time he would say, "Have good friends."

316
00:17:09,280 --> 00:17:11,800
 It's very important to have good friends.

317
00:17:11,800 --> 00:17:15,770
 Then he would say, "Don't spend time in other people's

318
00:17:15,770 --> 00:17:16,720
 company.

319
00:17:16,720 --> 00:17:21,280
 Don't spend time in society or socializing."

320
00:17:21,280 --> 00:17:24,880
 So you might wonder, "What's the contradiction here?

321
00:17:24,880 --> 00:17:26,880
 Why say one thing and the other?

322
00:17:26,880 --> 00:17:31,890
 Why tell us to have friends and then not even know their

323
00:17:31,890 --> 00:17:32,960
 names?"

324
00:17:32,960 --> 00:17:35,900
 I think you can get the idea where I'm going is that the

325
00:17:35,900 --> 00:17:37,920
 best friend is an example to us.

326
00:17:37,920 --> 00:17:41,190
 The best friend is someone who gives us the opportunity,

327
00:17:41,190 --> 00:17:43,440
 gives us the time and the opportunity

328
00:17:43,440 --> 00:17:45,440
 and the place.

329
00:17:45,440 --> 00:17:51,290
 All of the work that has gone into this place and making it

330
00:17:51,290 --> 00:17:54,240
 a fairly suitable or minimally

331
00:17:54,240 --> 00:17:58,950
 suitable place for us to practice meditation, I consider

332
00:17:58,950 --> 00:18:00,960
 that this is because of the friendship

333
00:18:00,960 --> 00:18:04,760
 and the kindness of the people who have supported us and

334
00:18:04,760 --> 00:18:07,440
 that we give as a sign of our friendship

335
00:18:07,440 --> 00:18:08,760
 to you.

336
00:18:08,760 --> 00:18:10,160
 You don't even have to know my name.

337
00:18:10,160 --> 00:18:12,770
 You come here and you see there's the meditation practice

338
00:18:12,770 --> 00:18:15,360
 and meet with you once over the

339
00:18:15,360 --> 00:18:17,920
 day and I'll give you the new exercise apart from that.

340
00:18:17,920 --> 00:18:21,750
 You consider that I've been a friend to you by giving you

341
00:18:21,750 --> 00:18:23,680
 the teaching and giving you

342
00:18:23,680 --> 00:18:26,800
 the place and so on.

343
00:18:26,800 --> 00:18:29,560
 The other people here, we consider them to be friends and

344
00:18:29,560 --> 00:18:30,960
 that they don't bother us.

345
00:18:30,960 --> 00:18:37,390
 If they were popamita, evil friends, they would waste all

346
00:18:37,390 --> 00:18:40,320
 our time in idle gossip and

347
00:18:40,320 --> 00:18:45,160
 dragging us away to do this or do that and coming up and

348
00:18:45,160 --> 00:18:48,000
 disturbing us all the time.

349
00:18:48,000 --> 00:18:51,730
 So we consider that we have very good friends here that

350
00:18:51,730 --> 00:18:54,080
 they give us the opportunity and

351
00:18:54,080 --> 00:18:59,920
 the space and the time.

352
00:18:59,920 --> 00:19:02,150
 I'm not a good friend to you because I come and knock on

353
00:19:02,150 --> 00:19:03,520
 your door every day and say,

354
00:19:03,520 --> 00:19:04,520
 "Hey, how are you going?

355
00:19:04,520 --> 00:19:07,600
 Want to talk or want to go for coffee or something?"

356
00:19:07,600 --> 00:19:10,500
 You can consider me a good friend and all the people here

357
00:19:10,500 --> 00:19:11,920
 are good friends and that

358
00:19:11,920 --> 00:19:13,920
 we've given you your space.

359
00:19:13,920 --> 00:19:18,010
 We've done something that most people won't do and that is

360
00:19:18,010 --> 00:19:19,960
 leave you alone and give you

361
00:19:19,960 --> 00:19:25,150
 the time and the space and the teaching to learn more about

362
00:19:25,150 --> 00:19:28,080
 yourself and to come to understand

363
00:19:28,080 --> 00:19:33,150
 how your mind works and to become free from akusula dhamma

364
00:19:33,150 --> 00:19:35,800
 and wholesome states of mind

365
00:19:35,800 --> 00:19:41,300
 developing kusula dhamma, the wholesome states of mind,

366
00:19:41,300 --> 00:19:44,120
 which is the Buddha who makes it

367
00:19:44,120 --> 00:19:46,200
 clear that this is the important thing.

368
00:19:46,200 --> 00:19:49,910
 He says these are for the purpose of wholesomeness and for

369
00:19:49,910 --> 00:19:52,080
 giving up on wholesomeness.

370
00:19:52,080 --> 00:19:54,270
 So what he's saying there is that this is what we should be

371
00:19:54,270 --> 00:19:54,920
 aiming for.

372
00:19:54,920 --> 00:19:58,920
 We need these things because they lead us to develop wholes

373
00:19:58,920 --> 00:19:59,800
omeness.

374
00:19:59,800 --> 00:20:02,680
 Wholesomeness is according to the Buddha the goal, the

375
00:20:02,680 --> 00:20:04,440
 development of wholesome mind states

376
00:20:04,440 --> 00:20:06,480
 or is the path.

377
00:20:06,480 --> 00:20:10,060
 Our development hinges on developing wholesome mind states

378
00:20:10,060 --> 00:20:12,120
 and discarding unwholesome mind

379
00:20:12,120 --> 00:20:16,600
 states coming to see through the delusion that leads to the

380
00:20:16,600 --> 00:20:18,680
 wholesome mind states.

381
00:20:18,680 --> 00:20:26,570
 So that's the pep talk for tonight and we'll continue to

382
00:20:26,570 --> 00:20:29,640
 try to do mindful frustration

