1
00:00:00,000 --> 00:00:05,140
 Hi everyone, I'm Adder, a volunteer for Siri Mangello

2
00:00:05,140 --> 00:00:07,360
 International, the organization that

3
00:00:07,360 --> 00:00:11,400
 supports Yutadamo Biku and his teachings.

4
00:00:11,400 --> 00:00:14,180
 I'd like to take a few minutes to tell you all about some

5
00:00:14,180 --> 00:00:15,840
 of the things our organization

6
00:00:15,840 --> 00:00:17,360
 does.

7
00:00:17,360 --> 00:00:21,720
 If you're watching this video, you're probably aware of the

8
00:00:21,720 --> 00:00:23,520
 plethora of videos that Bontay

9
00:00:23,520 --> 00:00:28,140
 Yutadamo posts on YouTube to share his teachings.

10
00:00:28,140 --> 00:00:31,560
 You might not know about some of the other things we do.

11
00:00:31,560 --> 00:00:36,720
 Feel free to visit siri-mangello.org for more information.

12
00:00:36,720 --> 00:00:41,190
 One thing that we offer is a live meditation center located

13
00:00:41,190 --> 00:00:43,080
 in Ontario, Canada.

14
00:00:43,080 --> 00:00:46,400
 If you'd like to schedule an intensive retreat under the

15
00:00:46,400 --> 00:00:48,420
 guidance of Bontay Yutadamo, please

16
00:00:48,420 --> 00:00:52,120
 visit our website to do so.

17
00:00:52,120 --> 00:00:56,730
 Furthermore, Bontay offers online instruction in our

18
00:00:56,730 --> 00:00:59,380
 Meditation Plus community.

19
00:00:59,380 --> 00:01:04,240
 If you visit meditation.siri-mangello.org, you can schedule

20
00:01:04,240 --> 00:01:06,100
 a time to meet weekly with

21
00:01:06,100 --> 00:01:11,240
 Bontay Yutadamo and do an online meditation course.

22
00:01:11,240 --> 00:01:16,630
 Additionally, the community hosts a chat room, our Ask

23
00:01:16,630 --> 00:01:20,080
 Questions board, where you can ask

24
00:01:20,080 --> 00:01:24,400
 the questions that Yutadamo responds to online.

25
00:01:24,400 --> 00:01:29,270
 And there's also a meditation timer and a way to track your

26
00:01:29,270 --> 00:01:31,520
 meditation progress.

27
00:01:31,520 --> 00:01:37,840
 We also host a weekly Dhamma study group.

28
00:01:37,840 --> 00:01:42,880
 And this happens in the online voice chat platform called

29
00:01:42,880 --> 00:01:44,000
 Discord.

30
00:01:44,000 --> 00:01:48,400
 And I have a link below for you to go there.

31
00:01:48,400 --> 00:01:52,370
 Additionally, our Discord hosts a general room for

32
00:01:52,370 --> 00:01:54,960
 discussion and is a great place for

33
00:01:54,960 --> 00:01:57,120
 our volunteers to connect.

34
00:01:57,120 --> 00:02:00,770
 If you're interested in volunteering for the organization,

35
00:02:00,770 --> 00:02:02,520
 feel free to get on Discord and

36
00:02:02,520 --> 00:02:04,400
 get in touch with us.

37
00:02:04,400 --> 00:02:09,260
 Finally, I want you all to know that our organization

38
00:02:09,260 --> 00:02:13,000
 depends on the generous donations from our

39
00:02:13,000 --> 00:02:14,320
 supporters.

40
00:02:14,320 --> 00:02:19,210
 If you're interested in supporting Siri Mangello, please

41
00:02:19,210 --> 00:02:22,480
 visit siri-mangello.org/support.

42
00:02:22,480 --> 00:02:23,480
 Thank you all.

43
00:02:23,480 --> 00:02:24,080
 Have a nice day.

