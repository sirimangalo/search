1
00:00:00,000 --> 00:00:04,640
 Good evening everyone.

2
00:00:04,640 --> 00:00:10,400
 I'm broadcasting live October, November 9th, 2015.

3
00:00:10,400 --> 00:00:14,520
 5th, November 5th.

4
00:00:14,520 --> 00:00:17,400
 That's correct.

5
00:00:17,400 --> 00:00:22,880
 And I think we've got

6
00:00:22,880 --> 00:00:27,480
 proper sound going on.

7
00:00:27,480 --> 00:00:31,800
 You want to say hello Robin so I can test?

8
00:00:31,800 --> 00:00:40,280
 No, no. Why aren't you coming through?

9
00:00:40,280 --> 00:00:46,200
 Maybe you're not loud enough.

10
00:00:46,200 --> 00:00:49,400
 Oh wait, I see. Look back.

11
00:00:49,400 --> 00:00:53,560
 This one has to come from...

12
00:00:53,560 --> 00:00:56,280
 There. Okay, now say hello.

13
00:00:56,280 --> 00:00:59,160
 Hello? Yes.

14
00:00:59,160 --> 00:01:00,360
 That's okay.

15
00:01:00,360 --> 00:01:03,880
 Now we both should be coming through the audio broadcast.

16
00:01:03,880 --> 00:01:06,360
 Okay.

17
00:01:06,360 --> 00:01:08,600
 Okay, questions?

18
00:01:08,600 --> 00:01:10,120
 We have questions.

19
00:01:10,120 --> 00:01:12,920
 First of all, let's do the announcements first

20
00:01:12,920 --> 00:01:15,560
 because tonight we don't have a Dhammapada video.

21
00:01:15,560 --> 00:01:18,360
 So what's going on here?

22
00:01:18,360 --> 00:01:20,520
 We have two things.

23
00:01:20,520 --> 00:01:25,640
 Well actually only one thing but I have two posters.

24
00:01:25,640 --> 00:01:27,880
 I don't think it's worth showing you posters, is it?

25
00:01:27,880 --> 00:01:30,040
 You guys want to see our new posters?

26
00:01:30,040 --> 00:01:32,280
 Yes.

27
00:01:32,280 --> 00:01:36,120
 You can comment on them.

28
00:01:36,120 --> 00:01:43,000
 Screen share.

29
00:01:43,000 --> 00:01:45,640
 There.

30
00:01:45,640 --> 00:01:51,240
 Introduction to meditation.

31
00:01:51,240 --> 00:01:53,560
 I want to get something and I'll hit them.

32
00:01:53,560 --> 00:01:56,480
 Hit people and make them wake up and say, "Hey, why aren't

33
00:01:56,480 --> 00:01:57,640
 you all meditating?"

34
00:01:57,640 --> 00:01:58,440
 Yeah.

35
00:01:58,440 --> 00:02:03,320
 "Why do you go out drinking every night?"

36
00:02:03,320 --> 00:02:05,880
 That's very nice.

37
00:02:05,880 --> 00:02:09,160
 Okay, so that's the first one.

38
00:02:09,160 --> 00:02:12,200
 And...

39
00:02:12,200 --> 00:02:14,120
 Did it change?

40
00:02:14,120 --> 00:02:17,320
 No, not yet.

41
00:02:17,320 --> 00:02:29,560
 Well then we'll have to stop that.

42
00:02:29,560 --> 00:02:30,600
 There.

43
00:02:30,600 --> 00:02:31,480
 Oh, very nice.

44
00:02:31,480 --> 00:02:32,680
 This one isn't me.

45
00:02:32,680 --> 00:02:37,560
 This is Carolyn, one of our exec members, but I did the

46
00:02:37,560 --> 00:02:38,280
 poster for her.

47
00:02:38,280 --> 00:02:41,880
 She did a poster but then it...

48
00:02:41,880 --> 00:02:43,720
 Well, I redid it.

49
00:02:43,720 --> 00:02:45,480
 It came out nice.

50
00:02:45,480 --> 00:02:46,840
 Yeah.

51
00:02:46,840 --> 00:02:50,840
 The McMaster Buddhism logo is a little bit like the Sri M

52
00:02:50,840 --> 00:02:51,720
ungo logo.

53
00:02:51,720 --> 00:02:53,240
 It's not the McMaster Buddhism logo.

54
00:02:53,240 --> 00:02:56,600
 That's the McMaster Students' Union Clubs.

55
00:02:56,600 --> 00:02:57,720
 Oh, okay.

56
00:02:57,720 --> 00:03:00,840
 They force us to put their logo on our posters.

57
00:03:00,840 --> 00:03:05,720
 So the clubs is very hard to read.

58
00:03:05,720 --> 00:03:11,570
 But it's not important to me because their logo isn't all

59
00:03:11,570 --> 00:03:14,120
 that important.

60
00:03:14,120 --> 00:03:15,720
 But yeah, so we're having a campfire.

61
00:03:15,720 --> 00:03:17,320
 This is our one big announcement.

62
00:03:17,320 --> 00:03:18,760
 This was someone's ideas.

63
00:03:18,760 --> 00:03:21,320
 I think it was Carolyn's idea.

64
00:03:21,320 --> 00:03:24,450
 To go and have a campfire in the evening and talk about

65
00:03:24,450 --> 00:03:28,760
 Buddhism and maybe do some meditation together.

66
00:03:28,760 --> 00:03:30,760
 Out in nature.

67
00:03:30,760 --> 00:03:33,370
 Monks aren't actually allowed to sit around campfires

68
00:03:33,370 --> 00:03:34,040
 together.

69
00:03:34,040 --> 00:03:35,640
 It's an interesting thing.

70
00:03:35,640 --> 00:03:39,000
 Not to look up whether I can take part in the campfire.

71
00:03:39,000 --> 00:03:41,320
 But it's a curious rule.

72
00:03:41,320 --> 00:03:43,720
 Oh, we're not presenting.

73
00:03:43,720 --> 00:03:45,080
 Okay.

74
00:03:45,080 --> 00:03:46,840
 It's a curious rule.

75
00:03:46,840 --> 00:03:50,510
 And the speculation is that it's somehow, sitting around a

76
00:03:50,510 --> 00:03:53,560
 campfire somehow leads to negligence.

77
00:03:53,560 --> 00:03:57,320
 It's just not an appropriate behavior for a Buddhist monk.

78
00:03:57,320 --> 00:04:00,760
 But whatever.

79
00:04:00,760 --> 00:04:03,880
 I think I'll be okay with attending because I'm the only

80
00:04:03,880 --> 00:04:07,110
 monk and I'm going to talk to lay people, not to talk to

81
00:04:07,110 --> 00:04:07,880
 monks.

82
00:04:07,880 --> 00:04:11,480
 And if there happens to be a campfire there.

83
00:04:11,480 --> 00:04:13,560
 Yeah.

84
00:04:13,560 --> 00:04:16,360
 I think the idea is they're going to have s'mores or

85
00:04:16,360 --> 00:04:17,240
 something.

86
00:04:17,240 --> 00:04:22,920
 So, I mean it's sort of in the vein of lay Buddhism.

87
00:04:22,920 --> 00:04:30,050
 You know, if you're in the Ajahn Chah group has like

88
00:04:30,050 --> 00:04:33,160
 Buddhist songs that they sing and Buddhists sing along.

89
00:04:33,160 --> 00:04:39,200
 And Buddhist plays that are kind of humorous about Buddhism

90
00:04:39,200 --> 00:04:39,640
.

91
00:04:39,640 --> 00:04:42,680
 You know, you wonder where the line is going to be drawn.

92
00:04:42,680 --> 00:04:50,620
 But, you know, sometimes you just accept the kusulupaya,

93
00:04:50,620 --> 00:04:55,240
 the whole skillful means to get people interested.

94
00:04:55,240 --> 00:05:00,140
 So, the idea is to create a community of like-minded

95
00:05:00,140 --> 00:05:01,800
 individuals.

96
00:05:01,800 --> 00:05:05,480
 Again, this isn't my idea, but it's interesting.

97
00:05:05,480 --> 00:05:09,320
 So, that's part of what we'll be doing.

98
00:05:09,320 --> 00:05:12,520
 That's next Friday.

99
00:05:14,360 --> 00:05:18,440
 So, that's a couple of things that have been going on here.

100
00:05:18,440 --> 00:05:19,480
 What else?

101
00:05:19,480 --> 00:05:21,000
 Tomorrow I'm going to London.

102
00:05:21,000 --> 00:05:24,200
 No, Saturday I'm going to London.

103
00:05:24,200 --> 00:05:28,360
 Ontario to someone's house.

104
00:05:28,360 --> 00:05:35,390
 Oh, and we've got 27 of 28 slots filled on the appointments

105
00:05:35,390 --> 00:05:36,280
 page.

106
00:05:36,280 --> 00:05:42,040
 That means I'm meeting with 27 different people a week.

107
00:05:42,040 --> 00:05:45,880
 Which is pretty good.

108
00:05:45,880 --> 00:05:49,320
 That's great.

109
00:05:49,320 --> 00:05:54,440
 Yeah.

110
00:05:54,440 --> 00:05:56,520
 So, that's all.

111
00:05:56,520 --> 00:06:02,920
 Any announcements from the volunteer group?

112
00:06:02,920 --> 00:06:06,240
 If anyone has been following along on Facebook and hasn't

113
00:06:06,240 --> 00:06:07,640
 really noticed much activity,

114
00:06:07,640 --> 00:06:11,510
 it's because most of our activity now is on a website

115
00:06:11,510 --> 00:06:13,080
 called slack.com.

116
00:06:13,080 --> 00:06:17,020
 And it's kind of taken the place of those email blasts that

117
00:06:17,020 --> 00:06:17,640
 we're going out

118
00:06:17,640 --> 00:06:22,040
 and taking a place with a lot of the talk on Facebook.

119
00:06:22,040 --> 00:06:26,040
 So, if anyone is interested, just send me a message.

120
00:06:26,040 --> 00:06:29,750
 I'll post the information into the Meditator chat panel as

121
00:06:29,750 --> 00:06:30,120
 well.

122
00:06:30,120 --> 00:06:32,280
 Because it's still a very active group.

123
00:06:32,280 --> 00:06:36,340
 We just have kind of changed our communication means a

124
00:06:36,340 --> 00:06:37,480
 little bit.

125
00:06:39,480 --> 00:06:41,480
 Ready for questions, Bante?

126
00:06:41,480 --> 00:06:43,480
 Yep.

127
00:06:43,480 --> 00:06:45,480
 Okay.

128
00:06:45,480 --> 00:06:50,090
 Venerable Sir, what would be your translation of Lord

129
00:06:50,090 --> 00:06:52,280
 Buddha's final words?

130
00:06:52,280 --> 00:06:54,280
 Thank you.

131
00:06:54,280 --> 00:07:00,280
 Vaya Dhammasankara Appamadhi Nisampadhi.

132
00:07:00,280 --> 00:07:06,600
 Vaya Dhammasankara, all formations are of a nature to cease

133
00:07:06,600 --> 00:07:07,080
.

134
00:07:07,080 --> 00:07:12,680
 Vaya is to fade away.

135
00:07:12,680 --> 00:07:19,420
 Appamadhi Nisampadhi come to fulfillment or fulfill your

136
00:07:19,420 --> 00:07:25,880
 goals or come to become fulfilled.

137
00:07:25,880 --> 00:07:33,480
 Appamadhi Nisampadhi with or in regards to, you could say,

138
00:07:33,480 --> 00:07:36,280
 Appamada.

139
00:07:36,280 --> 00:07:42,830
 So, come to fulfillment in regards to vigilance or

140
00:07:42,830 --> 00:07:45,080
 mindfulness.

141
00:07:45,080 --> 00:07:46,440
 So, become sober really.

142
00:07:46,440 --> 00:07:55,250
 Sober up if you wanted a sort of a crass, crass, pair of

143
00:07:55,250 --> 00:07:58,280
 phrase.

144
00:07:58,280 --> 00:08:05,480
 All, all this stuff, all this stuff is going to disappear.

145
00:08:05,480 --> 00:08:08,680
 Sober up.

146
00:08:08,680 --> 00:08:10,680
 All stuff disappears.

147
00:08:10,680 --> 00:08:14,280
 All stuff.

148
00:08:14,280 --> 00:08:17,480
 All stuff breaks.

149
00:08:17,480 --> 00:08:19,880
 All stuff breaks.

150
00:08:19,880 --> 00:08:20,680
 Stuff breaks.

151
00:08:20,680 --> 00:08:22,280
 Sober up.

152
00:08:22,280 --> 00:08:23,480
 There you go.

153
00:08:23,480 --> 00:08:27,880
 Let's put as crass as I can get.

154
00:08:27,880 --> 00:08:29,880
 There's Buddhism in a nutshell.

155
00:08:29,880 --> 00:08:30,680
 Stuff breaks.

156
00:08:30,680 --> 00:08:33,880
 Sober up.

157
00:08:33,880 --> 00:08:35,480
 There's a meme for you.

158
00:08:35,480 --> 00:08:37,880
 And it's actually a fairly literal translation.

159
00:08:37,880 --> 00:08:41,880
 Interestingly, stuff, Sankara is stuff.

160
00:08:41,880 --> 00:08:45,730
 It depends what you mean by stuff, but it's one way of, you

161
00:08:45,730 --> 00:08:49,480
 know, stuff is a good translation of Sankara.

162
00:08:49,480 --> 00:08:56,830
 Breaks, Vaya means it, you know, it, well, it's more like

163
00:08:56,830 --> 00:09:00,280
 dissipates or ceases, but fades away.

164
00:09:00,280 --> 00:09:03,480
 Breaks is, I think, good.

165
00:09:03,480 --> 00:09:09,080
 Sober because bhammada is as to mud.

166
00:09:09,080 --> 00:09:11,880
 Mud is the root of being intoxicated.

167
00:09:11,880 --> 00:09:14,280
 So bhammada is a kind of mental intoxication.

168
00:09:14,280 --> 00:09:19,010
 Up bhammada is sobering up or non-intoxication, un-intox

169
00:09:19,010 --> 00:09:19,880
icated.

170
00:09:19,880 --> 00:09:30,680
 And sampadita means it's in a sense of sam and sam-pa-pa.

171
00:09:30,680 --> 00:09:36,280
 Pa is like stronger, sam is fully.

172
00:09:36,280 --> 00:09:40,280
 Sam-pa is not strong, sorry.

173
00:09:40,280 --> 00:09:44,280
 Pad is to become, sam-pad become full.

174
00:09:44,280 --> 00:09:53,470
 So sam-badjati is to become full or to fulfill or to

175
00:09:53,470 --> 00:09:55,080
 succeed.

176
00:09:55,080 --> 00:09:58,680
 So it does have a sense of up, I think.

177
00:09:58,680 --> 00:10:05,080
 Not exactly, but there you go, there's my translation.

178
00:10:05,080 --> 00:10:10,280
 Stuff breaks sober up.

179
00:10:10,280 --> 00:10:12,680
 You can make a poster for that too.

180
00:10:12,680 --> 00:10:14,280
 Put it on Facebook.

181
00:10:14,280 --> 00:10:16,680
 Stuff breaks sober up, the Buddha.

182
00:10:16,680 --> 00:10:20,000
 And then if anyone calls you out on it, then we'll get it

183
00:10:20,000 --> 00:10:24,680
 sent to fake Buddha quotes and have him go at it.

184
00:10:24,680 --> 00:10:30,280
 And then we can all laugh at him when he says it's fake.

185
00:10:30,280 --> 00:10:32,280
 No, he's a good friend. Don't do that.

186
00:10:32,280 --> 00:10:39,880
 But it'd be funny to have that conversation.

187
00:10:39,880 --> 00:10:41,080
 Hello, Bhante.

188
00:10:41,080 --> 00:10:44,100
 I recently attained third path by following instructions

189
00:10:44,100 --> 00:10:47,880
 from Mahasya Sayadaw's practical insight meditation.

190
00:10:47,880 --> 00:10:50,820
 But I still feel pleasant sensations when I perceive

191
00:10:50,820 --> 00:10:51,880
 pleasant objects

192
00:10:51,880 --> 00:10:55,460
 and feel sensations of wanting to incline towards pleasant

193
00:10:55,460 --> 00:10:59,080
 objects, although not attached in the same way as before.

194
00:10:59,080 --> 00:11:02,320
 Could you please share your own experience post third path

195
00:11:02,320 --> 00:11:03,880
 with respect to sensual desire

196
00:11:03,880 --> 00:11:08,770
 and whether fourth path makes a difference in perceiving

197
00:11:08,770 --> 00:11:10,680
 pleasant objects?

198
00:11:10,680 --> 00:11:14,490
 Although I still experience theorizing of a thought that

199
00:11:14,490 --> 00:11:18,680
 that object is pleasant when contracting pleasant objects,

200
00:11:18,680 --> 00:11:21,880
 contacting pleasant objects.

201
00:11:21,880 --> 00:11:25,290
 I mean, it doesn't sound like you've reached the third path

202
00:11:25,290 --> 00:11:25,480
.

203
00:11:25,480 --> 00:11:29,480
 The third fruit actually path is just one moment.

204
00:11:29,480 --> 00:11:33,490
 So if you've attained the third path, you've also attained

205
00:11:33,490 --> 00:11:34,680
 the third fruit.

206
00:11:34,680 --> 00:11:37,880
 This path is only one moment.

207
00:11:37,880 --> 00:11:48,280
 It's followed immediately and necessarily by third fruit.

208
00:11:48,280 --> 00:11:51,360
 Any sensations of wanting to incline towards pleasant

209
00:11:51,360 --> 00:11:57,480
 objects are a sign of Loba, I mean of Kamaraga.

210
00:11:57,480 --> 00:12:03,900
 So this is someone, my feeling is that it's someone who

211
00:12:03,900 --> 00:12:07,080
 hasn't attained Anagami.

212
00:12:07,080 --> 00:12:11,520
 As to my own attainments, how do you know I've attained

213
00:12:11,520 --> 00:12:12,280
 third path?

214
00:12:12,280 --> 00:12:16,280
 Whoever told you that?

215
00:12:16,280 --> 00:12:23,080
 It's not something ordinary to become an Anagami.

216
00:12:23,080 --> 00:12:27,480
 It's actually probably pretty rare in this day and age.

217
00:12:27,480 --> 00:12:31,730
 But I have a policy not to talk about myself, so I can't

218
00:12:31,730 --> 00:12:32,680
 answer.

219
00:12:32,680 --> 00:12:35,080
 Next question.

220
00:12:35,080 --> 00:12:39,480
 Dear Bhante, my other leg is on the path.

221
00:12:39,480 --> 00:12:43,480
 My other leg is on the path, the other one in sensual world

222
00:12:43,480 --> 00:12:43,480
.

223
00:12:43,480 --> 00:12:46,990
 When trying to progress on the path, for example,

224
00:12:46,990 --> 00:12:50,280
 practicing a lot during a few days and being mostly alone,

225
00:12:50,280 --> 00:12:53,380
 and afterwards handling lay life issues, going to the bank

226
00:12:53,380 --> 00:12:56,280
 and meeting people, etc., I feel miserable.

227
00:12:56,280 --> 00:12:58,280
 I see how worthless everything is.

228
00:12:58,280 --> 00:13:01,070
 Then I feel I'm making people I meet feel miserable just by

229
00:13:01,070 --> 00:13:02,280
 interacting with them

230
00:13:02,280 --> 00:13:05,260
 in the way of not being capable to show my interest

231
00:13:05,260 --> 00:13:06,280
 whatsoever.

232
00:13:06,280 --> 00:13:10,280
 Then making them miserable makes me want to adjust myself

233
00:13:10,280 --> 00:13:11,880
 by what I feel

234
00:13:11,880 --> 00:13:15,610
 is creating delusional ideals about reality in order to

235
00:13:15,610 --> 00:13:17,880
 make those people find me less intimidating

236
00:13:17,880 --> 00:13:19,880
 so they will feel better.

237
00:13:19,880 --> 00:13:23,080
 And this seems to take me backwards on the path.

238
00:13:23,080 --> 00:13:25,480
 So this is a kind of a seesawing motion.

239
00:13:25,480 --> 00:13:27,080
 What can I do?

240
00:13:27,080 --> 00:13:29,970
 Eventually I must totally dive into the path and let go

241
00:13:29,970 --> 00:13:30,680
 completely.

242
00:13:30,680 --> 00:13:32,280
 But I can't do that yet.

243
00:13:32,280 --> 00:13:37,880
 How to look at this situation?

244
00:13:37,880 --> 00:13:40,680
 Well, you go from where you are, you know.

245
00:13:40,680 --> 00:13:45,480
 You can't start somewhere else.

246
00:13:45,480 --> 00:13:47,480
 So the path starts from where you are.

247
00:13:47,480 --> 00:13:53,480
 That's really the important thing to get across.

248
00:13:53,480 --> 00:14:00,490
 So we moan and complain because we're not able to start

249
00:14:00,490 --> 00:14:02,280
 where we want.

250
00:14:02,280 --> 00:14:04,280
 But the path is right in front of you.

251
00:14:04,280 --> 00:14:06,680
 It starts right where you are.

252
00:14:06,680 --> 00:14:10,870
 It doesn't mean you have to become a monk. It's not all or

253
00:14:10,870 --> 00:14:11,080
 nothing.

254
00:14:11,080 --> 00:14:15,480
 That would be jumping from where you are to somewhere else.

255
00:14:15,480 --> 00:14:18,990
 In fact, becoming a monk often doesn't solve the problem at

256
00:14:18,990 --> 00:14:19,480
 all.

257
00:14:19,480 --> 00:14:23,480
 The only way out is through the practice of mindfulness.

258
00:14:23,480 --> 00:14:29,480
 So the Buddha's advice still applies.

259
00:14:29,480 --> 00:14:34,680
 Stuff breaks, sober up.

260
00:14:34,680 --> 00:14:40,280
 Which means all of the things that make you feel miserable,

261
00:14:40,280 --> 00:14:42,280
 that's what you have to get sober about.

262
00:14:42,280 --> 00:14:44,280
 You have to realize that it's impermanent.

263
00:14:44,280 --> 00:14:45,480
 It breaks, it happens.

264
00:14:45,480 --> 00:14:47,480
 That's part of nature.

265
00:14:47,480 --> 00:14:49,080
 Teach yourself that.

266
00:14:49,080 --> 00:14:55,480
 Teach yourself to let go.

267
00:14:55,480 --> 00:14:59,460
 The part about I feel I'm making the people I meet feel

268
00:14:59,460 --> 00:15:01,480
 miserable just by interacting with them

269
00:15:01,480 --> 00:15:05,250
 and not being able to show my interest whatsoever in what

270
00:15:05,250 --> 00:15:06,280
 they're doing.

271
00:15:06,280 --> 00:15:10,280
 That happens.

272
00:15:10,280 --> 00:15:18,280
 Yeah, I mean sometimes you have to give a cursory approval

273
00:15:18,280 --> 00:15:20,280
 of people's interests.

274
00:15:20,280 --> 00:15:23,280
 When something interests people, you have to smile and nod

275
00:15:23,280 --> 00:15:24,280
 and say,

276
00:15:24,280 --> 00:15:26,280
 "Oh, good for you."

277
00:15:26,280 --> 00:15:28,280
 People say, "I just got married."

278
00:15:28,280 --> 00:15:30,280
 And you can say, "Congratulations."

279
00:15:30,280 --> 00:15:33,080
 And then you think, "Oh, no, marriage."

280
00:15:33,080 --> 00:15:34,280
 Just kidding.

281
00:15:34,280 --> 00:15:36,280
 Actually, marriage is a good thing.

282
00:15:36,280 --> 00:15:39,420
 Marriage is better than the alternative of promiscuity and

283
00:15:39,420 --> 00:15:39,880
 so on.

284
00:15:39,880 --> 00:15:44,120
 Because marriage is a teamwork kind of thing and it's a

285
00:15:44,120 --> 00:15:47,080
 morality kind of thing.

286
00:15:47,080 --> 00:15:49,080
 Of course, it's not better than the alternative,

287
00:15:49,080 --> 00:15:56,680
 which is not to become asexual, celibates.

288
00:15:56,680 --> 00:16:00,680
 For a layperson, marriage is reasonable, I think.

289
00:16:00,680 --> 00:16:05,080
 But yeah, no.

290
00:16:05,080 --> 00:16:09,500
 Ajahn Tong is very interesting because the people around

291
00:16:09,500 --> 00:16:11,080
 him have very little patience.

292
00:16:11,080 --> 00:16:13,080
 It's sad for some of the people.

293
00:16:13,080 --> 00:16:15,280
 I mean, some good people are sometimes around him,

294
00:16:15,280 --> 00:16:20,130
 but sometimes he manages to get surrounded by not so

295
00:16:20,130 --> 00:16:21,680
 mindful people.

296
00:16:21,680 --> 00:16:24,720
 And they'll be always very angry at the people who try to

297
00:16:24,720 --> 00:16:26,280
 come and waste his time.

298
00:16:26,280 --> 00:16:30,280
 But Ajahn Tong, I would have to sit there for hours.

299
00:16:30,280 --> 00:16:33,980
 And sometimes one of the old nuns would come up and just

300
00:16:33,980 --> 00:16:36,280
 talk his ear off about nothing,

301
00:16:36,280 --> 00:16:40,270
 about meaningless things like maybe their aches and pains

302
00:16:40,270 --> 00:16:42,280
 and medicines that they're taking

303
00:16:42,280 --> 00:16:45,280
 and talking about how they saw someone that they both know.

304
00:16:45,280 --> 00:16:47,720
 Because these are people who have known Ajahn Tong for 30,

305
00:16:47,720 --> 00:16:49,080
 40 years.

306
00:16:49,080 --> 00:16:51,630
 And so they're talking to him about, "Oh, I met this person

307
00:16:51,630 --> 00:16:51,880
.

308
00:16:51,880 --> 00:16:52,880
 Remember this person?

309
00:16:52,880 --> 00:16:55,680
 And now they're doing this right now."

310
00:16:59,680 --> 00:17:05,500
 So you learn this very important word for a Buddhist monk

311
00:17:05,500 --> 00:17:11,080
 and it's "mmm, mmm, mmm, mmm, mmm."

312
00:17:11,080 --> 00:17:13,080
 It's very non-committal.

313
00:17:13,080 --> 00:17:14,680
 Someone asked you, "Is that good?

314
00:17:14,680 --> 00:17:16,080
 Do you think that's good?"

315
00:17:16,080 --> 00:17:21,080
 Isn't that awesome?

316
00:17:21,080 --> 00:17:25,080
 So now when you make that sound, we know.

317
00:17:25,080 --> 00:17:27,080
 We know.

318
00:17:27,080 --> 00:17:31,400
 Yeah, it's funny because I was told that it actually sounds

319
00:17:31,400 --> 00:17:32,680
 kind of weird.

320
00:17:32,680 --> 00:17:35,680
 Why are you like, "Mmm, what does it mean to say 'Mmm'?"

321
00:17:35,680 --> 00:17:39,680
 But it's a very common thing in Thailand actually, I think.

322
00:17:39,680 --> 00:17:42,680
 It's what Ajahn Tong does.

323
00:17:42,680 --> 00:17:44,680
 What is kind of perfect?

324
00:17:44,680 --> 00:17:48,160
 You're acknowledging someone said something, but you're not

325
00:17:48,160 --> 00:17:51,680
 really approving or disapproving.

326
00:17:51,680 --> 00:17:57,480
 So what time is daily dharma really?

327
00:17:57,480 --> 00:18:00,480
 It didn't happen at the time the app indicated.

328
00:18:00,480 --> 00:18:01,480
 That's right.

329
00:18:01,480 --> 00:18:02,480
 The app is still on...

330
00:18:02,480 --> 00:18:09,480
 Don't use the app until February.

331
00:18:09,480 --> 00:18:12,480
 That's the answer.

332
00:18:12,480 --> 00:18:17,480
 Actually, that's funny because that means what?

333
00:18:17,480 --> 00:18:19,480
 No, that means it's...

334
00:18:19,480 --> 00:18:22,480
 I got lazy and just changed it in the JavaScript.

335
00:18:22,480 --> 00:18:27,480
 But if I change it in the database, that would work.

336
00:18:27,480 --> 00:18:30,480
 I think...

337
00:18:30,480 --> 00:18:34,480
 Why did it work like that?

338
00:18:34,480 --> 00:18:41,480
 Yeah, should have changed it in the PHP.

339
00:18:41,480 --> 00:18:42,480
 I can do that.

340
00:18:42,480 --> 00:18:44,480
 I can fix that, I think.

341
00:18:44,480 --> 00:18:53,480
 It's not like I have a million other things to be doing.

342
00:18:53,480 --> 00:18:55,480
 Is that all for questions?

343
00:18:55,480 --> 00:18:57,480
 Um...

344
00:18:57,480 --> 00:18:59,480
 I think so.

345
00:18:59,480 --> 00:19:00,480
 Oh no.

346
00:19:00,480 --> 00:19:03,690
 Since changing from an Asian diet to a Western one, have

347
00:19:03,690 --> 00:19:07,060
 you observed any changes in the way your body and mind is

348
00:19:07,060 --> 00:19:08,480
 functioning?

349
00:19:08,480 --> 00:19:13,400
 Yeah, I mean a lot of the Asian diet that I would have from

350
00:19:13,400 --> 00:19:16,480
 time to time, it wasn't about being an Asian diet.

351
00:19:16,480 --> 00:19:18,480
 There's...

352
00:19:18,480 --> 00:19:23,040
 Some Asian societies are less health-conscious than the

353
00:19:23,040 --> 00:19:23,480
 West.

354
00:19:23,480 --> 00:19:25,480
 And that's actually maybe not fair to say.

355
00:19:25,480 --> 00:19:29,480
 I just find more health alternatives here.

356
00:19:29,480 --> 00:19:30,480
 I don't know.

357
00:19:30,480 --> 00:19:33,070
 That's not even fair to say because Western food can be

358
00:19:33,070 --> 00:19:34,480
 really terrible as well.

359
00:19:34,480 --> 00:19:35,480
 It's not like that.

360
00:19:35,480 --> 00:19:38,870
 It's more like I've been in places where the food that I

361
00:19:38,870 --> 00:19:40,480
 ate was not very healthy.

362
00:19:40,480 --> 00:19:41,480
 And you can feel that.

363
00:19:41,480 --> 00:19:43,480
 That changes your body.

364
00:19:43,480 --> 00:19:44,480
 It's an interesting state.

365
00:19:44,480 --> 00:19:45,480
 It's actually...

366
00:19:45,480 --> 00:19:49,080
 It can be quite useful for your practice to be in a state

367
00:19:49,080 --> 00:19:50,480
 of under-nutrition.

368
00:19:50,480 --> 00:19:56,330
 And I've been in other places where I get, you know, really

369
00:19:56,330 --> 00:19:59,480
 rich food and sumptuous food.

370
00:19:59,480 --> 00:20:00,480
 Los Angeles comes to mind.

371
00:20:00,480 --> 00:20:04,480
 When I was in LA, I got to talk creme de la creme of food.

372
00:20:04,480 --> 00:20:08,480
 All super healthy, super nutritious.

373
00:20:08,480 --> 00:20:11,480
 But it's not to say that that's better for your practice.

374
00:20:11,480 --> 00:20:13,480
 That can actually lead to negligence.

375
00:20:13,480 --> 00:20:15,480
 So...

376
00:20:15,480 --> 00:20:16,480
 Whatever.

377
00:20:16,480 --> 00:20:22,480
 It's just food.

378
00:20:22,480 --> 00:20:25,480
 And that is the last question.

379
00:20:25,480 --> 00:20:26,480
 Or...

380
00:20:26,480 --> 00:20:27,480
 Okay.

381
00:20:27,480 --> 00:20:29,480
 Last question.

382
00:20:29,480 --> 00:20:30,480
 One more?

383
00:20:30,480 --> 00:20:31,480
 No.

384
00:20:31,480 --> 00:20:43,480
 No, that was the last question.

385
00:20:43,480 --> 00:20:44,480
 Oh, so is it wrong?

386
00:20:44,480 --> 00:20:53,480
 Is the...

387
00:20:53,480 --> 00:20:54,480
 Does that mean the website got it wrong as well?

388
00:20:54,480 --> 00:20:58,480
 What time are we going to have this thing?

389
00:20:58,480 --> 00:21:01,480
 Because it looks like I didn't actually change it.

390
00:21:01,480 --> 00:21:27,480
 Looks like I was preparing to change it and then I didn't.

391
00:21:27,480 --> 00:21:28,480
 Yeah, I can fix that.

392
00:21:28,480 --> 00:21:29,480
 Should be able to fix it.

393
00:21:29,480 --> 00:21:30,480
 I'll take a look.

394
00:21:30,480 --> 00:21:31,480
 Anyway.

395
00:21:31,480 --> 00:21:32,480
 So that's all for tonight then?

396
00:21:32,480 --> 00:21:33,480
 Yes.

397
00:21:33,480 --> 00:21:34,480
 Well, just...

398
00:21:34,480 --> 00:21:42,870
 There was a lot of question earlier about time changes and

399
00:21:42,870 --> 00:21:44,480
 things.

400
00:21:44,480 --> 00:21:47,420
 The individual meetings that you're doing, did they change

401
00:21:47,420 --> 00:21:48,480
 with the time change?

402
00:21:48,480 --> 00:21:52,210
 Well, there's been some confusion there, but I think we've

403
00:21:52,210 --> 00:21:53,480
 got it worked out.

404
00:21:53,480 --> 00:21:54,480
 Okay.

405
00:21:54,480 --> 00:21:56,480
 Anyway, that's not perfect.

406
00:21:56,480 --> 00:21:59,480
 No, I'm just wondering from my...

407
00:21:59,480 --> 00:22:00,480
 Stuff breaks.

408
00:22:00,480 --> 00:22:01,480
 Yes.

409
00:22:01,480 --> 00:22:04,420
 So it does change with the daylight savings time or the

410
00:22:04,420 --> 00:22:05,480
 lack of...

411
00:22:05,480 --> 00:22:07,480
 Well, it won't change here.

412
00:22:07,480 --> 00:22:12,480
 I mean, for me, 7 a.m. and 6 p.m.

413
00:22:12,480 --> 00:22:14,480
 Those are the time.

414
00:22:14,480 --> 00:22:16,480
 7 a.m., 7.30.

415
00:22:16,480 --> 00:22:18,480
 6 p.m., 6.30.

416
00:22:18,480 --> 00:22:20,480
 So figure that out.

417
00:22:20,480 --> 00:22:21,480
 Those are the time.

418
00:22:21,480 --> 00:22:23,940
 I don't think there was an issue and a different issue

419
00:22:23,940 --> 00:22:24,480
 today.

420
00:22:24,480 --> 00:22:26,480
 It wasn't to do with that.

421
00:22:26,480 --> 00:22:27,480
 Oh, okay.

422
00:22:27,480 --> 00:22:28,480
 It's just confusion.

423
00:22:28,480 --> 00:22:31,480
 Anyway, good night, everyone.

424
00:22:31,480 --> 00:22:32,480
 See you all tomorrow.

425
00:22:32,480 --> 00:22:33,480
 Thank you, Pandey.

426
00:22:33,480 --> 00:22:36,480
 We should have a Dhammapada again tomorrow.

427
00:22:36,480 --> 00:22:37,480
 Okay, good night.

428
00:22:37,480 --> 00:22:38,480
 Thank you.

429
00:22:38,480 --> 00:22:39,480
 Good night.

430
00:22:39,480 --> 00:22:40,480
 Thank you.

