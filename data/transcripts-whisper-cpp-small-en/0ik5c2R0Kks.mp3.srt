1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:15,000
 Broadcasting Live, May 6th.

3
00:00:15,000 --> 00:00:29,000
 Today's quote is somewhat curious.

4
00:00:29,000 --> 00:00:35,010
 And not quite how, well, Bhikkhobodi's translation of

5
00:00:35,010 --> 00:00:36,000
 course is preferred.

6
00:00:36,000 --> 00:00:41,000
 But the word isn't quite false, it's 'wipati'.

7
00:00:41,000 --> 00:00:49,000
 'Wipati' is failure, failings as Bhikkhobodi translates it.

8
00:00:49,000 --> 00:01:01,000
 So 'wipati' and 'sampati'. 'Wipati' and 'sampati'.

9
00:01:01,000 --> 00:01:23,200
 'Wipati' means lacking. 'Sampati' means attaining our

10
00:01:23,200 --> 00:01:27,000
 failings and our attainment.

11
00:01:27,000 --> 00:01:34,420
 And so the Buddha says, and it's kind of, I wouldn't put

12
00:01:34,420 --> 00:01:36,000
 too much on this,

13
00:01:36,000 --> 00:01:40,550
 because the context of the quote is that Devadatta just

14
00:01:40,550 --> 00:01:41,000
 left.

15
00:01:41,000 --> 00:01:47,110
 Devadatta of course was the Buddha's cousin who, well, he

16
00:01:47,110 --> 00:01:52,000
 came to the Buddha and he said,

17
00:01:52,000 --> 00:01:57,550
 'Oh, Venerable Surya, you're getting old, so why don't you

18
00:01:57,550 --> 00:02:00,000
 pass the Sangha on to me,

19
00:02:00,000 --> 00:02:03,000
 'Aali the Sangha?'

20
00:02:03,000 --> 00:02:06,000
 And the Buddha said to him, 'I wouldn't even hand it over

21
00:02:06,000 --> 00:02:07,000
 to Sariputta and Moggallana,

22
00:02:07,000 --> 00:02:11,000
 let alone some insignificant little person like you.'

23
00:02:11,000 --> 00:02:17,000
 He said something really mean to him actually, kind of.

24
00:02:17,000 --> 00:02:20,000
 Because Devadatta is out of control.

25
00:02:20,000 --> 00:02:22,000
 And so there's only one way it's going to end.

26
00:02:22,000 --> 00:02:24,000
 In the end, the Buddha has to be firm.

27
00:02:24,000 --> 00:02:28,530
 It's rare because he's not usually like this, but with Dev

28
00:02:28,530 --> 00:02:31,000
adatta he had to be firm at all times.

29
00:02:31,000 --> 00:02:38,330
 And so Devadatta got very upset and he tried to scheme up a

30
00:02:38,330 --> 00:02:43,000
 way to take control of the Sangha.

31
00:02:43,000 --> 00:02:48,030
 And so he came to the Buddha later and asked him to instate

32
00:02:48,030 --> 00:02:50,000
 five rules for monks,

33
00:02:50,000 --> 00:02:53,140
 that they should always live in the forest, that they

34
00:02:53,140 --> 00:02:57,000
 should only eat vegetarian food,

35
00:02:57,000 --> 00:03:00,230
 and things that actually were contentious and seemed

36
00:03:00,230 --> 00:03:01,000
 reasonable.

37
00:03:01,000 --> 00:03:05,840
 But the Buddha couldn't enforce and he stopped short of

38
00:03:05,840 --> 00:03:07,000
 forcing monks,

39
00:03:07,000 --> 00:03:09,530
 even though he praised living in the forest and even

40
00:03:09,530 --> 00:03:12,000
 obviously eating meat is problematic

41
00:03:12,000 --> 00:03:15,360
 and there are types of meat that absolutely shouldn't be

42
00:03:15,360 --> 00:03:16,000
 eaten.

43
00:03:16,000 --> 00:03:23,890
 So he refused and Devadatta used that as grounds to start

44
00:03:23,890 --> 00:03:26,000
 his schism.

45
00:03:26,000 --> 00:03:30,440
 He got monks on his side saying, 'Hey, the Buddha clearly

46
00:03:30,440 --> 00:03:33,000
 doesn't know what he's doing.'

47
00:03:33,000 --> 00:03:37,000
 But the monks he got on his side were actually new monks.

48
00:03:37,000 --> 00:03:39,000
 They're monks who didn't know better.

49
00:03:39,000 --> 00:03:42,330
 They hadn't studied their mind. They hadn't really learned

50
00:03:42,330 --> 00:03:44,000
 the intricacies of monastic life.

51
00:03:44,000 --> 00:03:51,900
 And so they thought Devadatta was more serious than the

52
00:03:51,900 --> 00:03:53,000
 Buddha.

53
00:03:53,000 --> 00:04:00,000
 The Buddha seemed to be actually kind of lax and indulgent.

54
00:04:00,000 --> 00:04:02,000
 And so they went with Devadatta.

55
00:04:02,000 --> 00:04:06,710
 But later the Buddha had Sariputta and Mughalana go to see

56
00:04:06,710 --> 00:04:07,000
 them.

57
00:04:07,000 --> 00:04:09,000
 And it's kind of funny how it turned out.

58
00:04:09,000 --> 00:04:11,000
 This is after this sutta is actually before that.

59
00:04:11,000 --> 00:04:16,000
 But later on Sariputta and Mughalana went to see Devadatta.

60
00:04:16,000 --> 00:04:19,960
 And Devadatta said, 'Oh, look, here come the Buddha's two

61
00:04:19,960 --> 00:04:21,000
 chief disciples.

62
00:04:21,000 --> 00:04:23,000
 They're joining us as well.'

63
00:04:23,000 --> 00:04:26,000
 And Sariputta and Mughalana didn't say anything.

64
00:04:26,000 --> 00:04:30,410
 And so Devadatta said, 'Sariputta and Mughalana, you teach

65
00:04:30,410 --> 00:04:33,000
 the monks. Now I'm tired.'

66
00:04:33,000 --> 00:04:36,290
 And so he pretended to be like the Buddha where the Buddha

67
00:04:36,290 --> 00:04:38,000
 would say, you know, 'You teach.'

68
00:04:38,000 --> 00:04:41,000
 And the Buddha would lie down and listen mindfully.

69
00:04:41,000 --> 00:04:45,000
 But instead Devadatta lay down and fell asleep.

70
00:04:45,000 --> 00:04:50,000
 And so Sariputta and Mughalana taught the monks the truth

71
00:04:50,000 --> 00:04:52,000
 and converted them all.

72
00:04:52,000 --> 00:04:55,000
 And they all went back to see the Buddha.

73
00:04:55,000 --> 00:04:59,000
 They all went back to be with the Buddha once they realized

74
00:04:59,000 --> 00:05:01,000
 what was right and what was wrong.

75
00:05:01,000 --> 00:05:05,920
 And Devadatta's sort of second in command kicked Devadatta

76
00:05:05,920 --> 00:05:08,000
 in the chest to wake him up.

77
00:05:08,000 --> 00:05:12,790
 And was very angry and said, 'Look, look at what's happened

78
00:05:12,790 --> 00:05:16,000
. I told you to be careful of these two.

79
00:05:16,000 --> 00:05:18,000
 And you didn't listen.'

80
00:05:18,000 --> 00:05:20,000
 But he kicked him in the chest and it wounded him.

81
00:05:20,000 --> 00:05:23,050
 And that ended up, I think, being fatal. That was what

82
00:05:23,050 --> 00:05:25,000
 killed him in the end.

83
00:05:25,000 --> 00:05:30,000
 He got sick after that, coughed up in blood.

84
00:05:30,000 --> 00:05:34,960
 But so this quote is somewhere during this whole fiasco

85
00:05:34,960 --> 00:05:38,000
 when Devadatta has just left.

86
00:05:38,000 --> 00:05:43,680
 And so the Buddha kind of to introduce the topic, he says

87
00:05:43,680 --> 00:05:47,000
 it's appropriate from time to time,

88
00:05:47,000 --> 00:05:53,000
 'gaade na kalang,' to talk about one's own failings.

89
00:05:53,000 --> 00:06:00,000
 Not talk about but to keep that to consider.

90
00:06:00,000 --> 00:06:06,000
 It is good that they be considered, reflected upon.

91
00:06:06,000 --> 00:06:12,000
 And it's good to reflect upon the failings of others.

92
00:06:12,000 --> 00:06:17,000
 And you have to take this with a grain of salt.

93
00:06:17,000 --> 00:06:20,480
 It's important not to get obsessed with the faults of

94
00:06:20,480 --> 00:06:21,000
 others.

95
00:06:21,000 --> 00:06:24,440
 Because the Buddha has said, of course, 'na praisang wilom

96
00:06:24,440 --> 00:06:27,000
ani na praisang katagatango.'

97
00:06:27,000 --> 00:06:29,000
 Don't worry about.

98
00:06:29,000 --> 00:06:33,000
 One should not become obsessed with other people's faults.

99
00:06:33,000 --> 00:06:36,230
 So here he's cautious. He says, 'gaade na kalang,' from

100
00:06:36,230 --> 00:06:37,000
 time to time.

101
00:06:37,000 --> 00:06:39,000
 It's useful, right?

102
00:06:39,000 --> 00:06:41,400
 Because if you look at Devadatta as an example and if you

103
00:06:41,400 --> 00:06:43,000
 talk about him and think about him,

104
00:06:43,000 --> 00:06:46,000
 you can say, 'That's not how I want to be.

105
00:06:46,000 --> 00:06:52,000
 That's something that leads to stress and suffering.'

106
00:06:52,000 --> 00:06:54,940
 And he says it's good from time to time to think of your

107
00:06:54,940 --> 00:06:58,000
 own attainments and attainments of others.

108
00:06:58,000 --> 00:07:01,630
 But then he gets on to the meat of the sutta, which is

109
00:07:01,630 --> 00:07:04,000
 actually more interesting.

110
00:07:04,000 --> 00:07:10,000
 He says, well, he says, 'Devadatta's going to hell,' right?

111
00:07:10,000 --> 00:07:20,000
 That's what he says. Wait, where is it?

112
00:07:20,000 --> 00:07:25,070
 There are these eight by eight, let me read the English

113
00:07:25,070 --> 00:07:26,000
 because I'm...

114
00:07:26,000 --> 00:07:30,620
 'Because he was overcome and obsessed by eight bad

115
00:07:30,620 --> 00:07:32,000
 conditions,

116
00:07:32,000 --> 00:07:36,050
 Devadatta is bound for the plane of misery, bound for hell,

117
00:07:36,050 --> 00:07:37,000
 and he will remain there for a neon.'

118
00:07:37,000 --> 00:07:40,800
 So the point is to introduce... he's now going to talk

119
00:07:40,800 --> 00:07:45,000
 about Devadatta's failings, what Devadatta did wrong.

120
00:07:45,000 --> 00:07:49,420
 And it's interesting because these eight things are a list

121
00:07:49,420 --> 00:07:53,000
 of things that we have to be not obsessed with.

122
00:07:53,000 --> 00:07:56,000
 We cannot let overcome us.

123
00:07:56,000 --> 00:08:00,000
 The first is gain.

124
00:08:00,000 --> 00:08:06,220
 Some people are obsessed with gain, wanting money, wanting

125
00:08:06,220 --> 00:08:09,000
 possessions, wanting to get things.

126
00:08:09,000 --> 00:08:14,840
 And when you get obsessed with that, it overpowers you, it

127
00:08:14,840 --> 00:08:17,000
 inflames the mind.

128
00:08:17,000 --> 00:08:21,680
 Overcome by loss when you don't get what you want, when you

129
00:08:21,680 --> 00:08:25,000
 lose what you like.

130
00:08:25,000 --> 00:08:30,000
 Fame, number three, is fame.

131
00:08:30,000 --> 00:08:32,000
 And people are obsessed with being famous.

132
00:08:32,000 --> 00:08:39,000
 People who post YouTube videos, they can be obsessed with

133
00:08:39,000 --> 00:08:41,000
 people's...

134
00:08:41,000 --> 00:08:44,000
 how many subscribers they have or so.

135
00:08:44,000 --> 00:08:49,000
 We go on Facebook, we're obsessed by how many likes we get.

136
00:08:49,000 --> 00:08:54,000
 That kind of thing. By disrepute the opposite of fame.

137
00:08:54,000 --> 00:08:59,290
 We're obsessed when people have a bad image of you or when

138
00:08:59,290 --> 00:09:03,000
 you become infamous,

139
00:09:03,000 --> 00:09:09,000
 or when no one knows who you are.

140
00:09:09,000 --> 00:09:14,000
 By honor, I'm not sure what the context here of honor is.

141
00:09:14,000 --> 00:09:18,000
 Look at this.

142
00:09:18,000 --> 00:09:21,000
 Sakara, right?

143
00:09:21,000 --> 00:09:23,000
 Yaśa, ayaśa, sakara.

144
00:09:23,000 --> 00:09:33,000
 Sakara is similar to fame, but it's when people present you

145
00:09:33,000 --> 00:09:33,000
 with things,

146
00:09:33,000 --> 00:09:43,000
 like respect and gifts and praise and honor.

147
00:09:43,000 --> 00:09:46,000
 Yaśa, that's where he gets the word honor.

148
00:09:46,000 --> 00:09:50,000
 Sakara means doing rightly.

149
00:09:50,000 --> 00:09:53,000
 And people are obsessed with other people,

150
00:09:53,000 --> 00:10:01,000
 praising them and honoring them and...

151
00:10:01,000 --> 00:10:09,000
 rewarding them, that kind of thing.

152
00:10:09,000 --> 00:10:11,000
 Number seven, by evil desires.

153
00:10:11,000 --> 00:10:13,000
 He was obsessed by evil desires.

154
00:10:13,000 --> 00:10:16,000
 David Datta had the most evil desires.

155
00:10:16,000 --> 00:10:19,000
 He wanted to kill the Buddha, he wanted to become...

156
00:10:19,000 --> 00:10:21,000
 He ended up trying to kill the Buddha.

157
00:10:21,000 --> 00:10:23,940
 He so wanted to be head of the monks, head of the Sangha,

158
00:10:23,940 --> 00:10:25,000
 so badly,

159
00:10:25,000 --> 00:10:27,000
 that he did evil things.

160
00:10:27,000 --> 00:10:30,000
 He was jealous of the Buddha.

161
00:10:30,000 --> 00:10:34,200
 He was covetous of the Buddha's position, that kind of

162
00:10:34,200 --> 00:10:35,000
 thing.

163
00:10:35,000 --> 00:10:38,000
 And number eight, bad friendship.

164
00:10:38,000 --> 00:10:42,000
 So David Datta...

165
00:10:42,000 --> 00:10:45,000
 David Datta was actually the bad friendship himself.

166
00:10:45,000 --> 00:10:50,000
 He ended up making friends with King Bimbisara's son,

167
00:10:50,000 --> 00:10:55,000
 who ended up killing his father.

168
00:10:55,000 --> 00:11:08,000
 And Jatasattu was actually able to kill his father.

169
00:11:08,000 --> 00:11:12,570
 But association with bad friends means, if you're on a bad

170
00:11:12,570 --> 00:11:13,000
 path,

171
00:11:13,000 --> 00:11:17,880
 it's not a good idea to find people who have your own

172
00:11:17,880 --> 00:11:19,000
 faults, right?

173
00:11:19,000 --> 00:11:22,490
 We say birds of a feather flock together, well that's not

174
00:11:22,490 --> 00:11:24,000
 always wise.

175
00:11:24,000 --> 00:11:27,710
 If you have faults, then maybe it's better to associate

176
00:11:27,710 --> 00:11:28,000
 with people

177
00:11:28,000 --> 00:11:30,000
 who don't have those faults.

178
00:11:30,000 --> 00:11:33,000
 They can teach you something, they can remind you,

179
00:11:33,000 --> 00:11:36,000
 they can challenge you, right?

180
00:11:36,000 --> 00:11:39,000
 But we tend the other way, right?

181
00:11:39,000 --> 00:11:45,000
 We tend to flock towards birds with the same faults.

182
00:11:45,000 --> 00:11:47,000
 We tend to flock towards...

183
00:11:47,000 --> 00:11:51,000
 If we are an angry sort of person,

184
00:11:51,000 --> 00:11:56,000
 we tend to find solace in other people who are angry.

185
00:11:56,000 --> 00:11:58,000
 We don't seem so out of place.

186
00:11:58,000 --> 00:12:00,000
 If we're around people who are not angry, we feel guilty

187
00:12:00,000 --> 00:12:00,000
 all the time,

188
00:12:00,000 --> 00:12:01,000
 we feel bad.

189
00:12:01,000 --> 00:12:04,000
 So it's actually easier to be around angry people,

190
00:12:04,000 --> 00:12:06,000
 which is of course the most dangerous thing,

191
00:12:06,000 --> 00:12:08,750
 because you're just going to become more of an angry person

192
00:12:08,750 --> 00:12:09,000
.

193
00:12:09,000 --> 00:12:12,000
 Greed, if you're a greedy person,

194
00:12:12,000 --> 00:12:16,000
 shouldn't be around other addicts or other greedy people.

195
00:12:16,000 --> 00:12:21,000
 If you're deluded, if you're full of arrogance and conceit

196
00:12:21,000 --> 00:12:23,000
 and so on,

197
00:12:23,000 --> 00:12:25,000
 you should be surrounded by humble people,

198
00:12:25,000 --> 00:12:29,330
 you should go to seek out humble people to teach you

199
00:12:29,330 --> 00:12:32,000
 humility.

200
00:12:32,000 --> 00:12:35,000
 And then he says, "It is good for a bhikkhu to overcome

201
00:12:35,000 --> 00:12:39,000
 these eight things

202
00:12:39,000 --> 00:12:42,000
 whenever they arise."

203
00:12:42,000 --> 00:12:46,000
 Overcome, see what the word he uses for overcome.

204
00:12:46,000 --> 00:12:52,000
 "Ambi buya," and the commentary talks about.

205
00:12:52,000 --> 00:12:54,000
 "Ambi buya."

206
00:12:54,000 --> 00:12:57,000
 This is an interesting word.

207
00:12:57,000 --> 00:12:59,000
 To...

208
00:12:59,000 --> 00:13:06,000
 I guess it just means overcome, to be above.

209
00:13:06,000 --> 00:13:11,000
 The commentary says, "Ambi bhavitva, maditva."

210
00:13:11,000 --> 00:13:18,000
 To subjugate, to conquer.

211
00:13:18,000 --> 00:13:20,000
 One should conquer these.

212
00:13:20,000 --> 00:13:23,000
 Conquer these things. These are our enemies.

213
00:13:23,000 --> 00:13:27,000
 Devadatta wasn't the enemy, the Buddha wasn't his enemy.

214
00:13:27,000 --> 00:13:34,000
 His enemy was these eight things.

215
00:13:34,000 --> 00:13:36,610
 It was his obsession for these eight things, I shouldn't

216
00:13:36,610 --> 00:13:37,000
 say.

217
00:13:37,000 --> 00:13:39,000
 Because of course, gain is not a problem,

218
00:13:39,000 --> 00:13:41,000
 loss is not a problem.

219
00:13:41,000 --> 00:13:44,000
 It's not these things that are the problem,

220
00:13:44,000 --> 00:13:46,000
 except for evil wishes.

221
00:13:46,000 --> 00:13:48,000
 But the real problem is our obsession.

222
00:13:48,000 --> 00:13:51,000
 And this goes actually with even our emotions.

223
00:13:51,000 --> 00:13:54,000
 If you're an angry person, that's not the biggest problem.

224
00:13:54,000 --> 00:13:56,000
 The biggest problem is your obsession with it,

225
00:13:56,000 --> 00:14:02,000
 when you let it consume you, when you have desire.

226
00:14:02,000 --> 00:14:04,000
 That's not the biggest problem.

227
00:14:04,000 --> 00:14:06,000
 The biggest problem is you don't...

228
00:14:06,000 --> 00:14:09,930
 You fail to address it, you fail to rise above it, to

229
00:14:09,930 --> 00:14:16,000
 overcome it.

230
00:14:16,000 --> 00:14:19,000
 When you have drowsiness, when you have distraction,

231
00:14:19,000 --> 00:14:23,560
 when you have doubt, you have doubt about the Buddha's

232
00:14:23,560 --> 00:14:24,000
 teaching

233
00:14:24,000 --> 00:14:25,000
 or the practice.

234
00:14:25,000 --> 00:14:30,000
 Doubt isn't the biggest problem that you let it get to you.

235
00:14:30,000 --> 00:14:33,000
 Even these things, they're bad.

236
00:14:33,000 --> 00:14:36,000
 They really become bad when you follow them,

237
00:14:36,000 --> 00:14:41,000
 when you chase them, when you make much of them.

238
00:14:41,000 --> 00:14:45,000
 And so with these eight things, it's really the obsession

239
00:14:45,000 --> 00:14:46,000
 with them.

240
00:14:46,000 --> 00:14:53,000
 But Devadatta wasn't able to see them clearly.

241
00:14:53,000 --> 00:14:54,000
 Dasmatiham...

242
00:14:54,000 --> 00:14:57,660
 No, and then he says, "Why should you not let them consume

243
00:14:57,660 --> 00:14:58,000
 you?"

244
00:14:58,000 --> 00:15:06,000
 And he says, "Because there will be a 'Wigata Parilaha'

245
00:15:06,000 --> 00:15:14,000
 which is a fever burning.

246
00:15:14,000 --> 00:15:18,000
 Those taints, distressful and feverish that might arise

247
00:15:18,000 --> 00:15:21,000
 in one who has not overcome gain and so on,

248
00:15:21,000 --> 00:15:24,000
 do not occur in one who has overcome it."

249
00:15:24,000 --> 00:15:29,000
 So your mind doesn't become inflamed, distressed,

250
00:15:29,000 --> 00:15:35,000
 stressed, fatigued, overwhelmed.

251
00:15:35,000 --> 00:15:46,000
 The Asa is outpouring the emotions and the defilement,

252
00:15:46,000 --> 00:15:53,000
 the stresses that arise when you've overcome them.

253
00:15:53,000 --> 00:15:56,000
 And you've come to see them for what they are

254
00:15:56,000 --> 00:16:04,000
 and discard them as unproductive.

255
00:16:04,000 --> 00:16:07,000
 For that reason you should train yourself.

256
00:16:07,000 --> 00:16:10,000
 Dasmatiham tikure...

257
00:16:10,000 --> 00:16:14,000
 Aivang sikidabang, it should be trained as

258
00:16:14,000 --> 00:16:16,000
 we will dwell having overcome.

259
00:16:16,000 --> 00:16:21,000
 Abhibu ya abhibu ya wihari saam.

260
00:16:21,000 --> 00:16:25,000
 We will dwell having overcome gain and loss

261
00:16:25,000 --> 00:16:33,000
 and pain and infamy and honor and dishonor

262
00:16:33,000 --> 00:16:38,000
 and evil desires

263
00:16:38,000 --> 00:16:39,000
 and evil friends.

264
00:16:39,000 --> 00:16:50,000
 Pa peet chitang pa pam mitatang, evil friendship.

265
00:16:50,000 --> 00:16:52,000
 So a good list, good list.

266
00:16:52,000 --> 00:16:54,000
 It's almost the eight lokiyadamma

267
00:16:54,000 --> 00:16:59,000
 that the last two would be happiness and suffering.

268
00:16:59,000 --> 00:17:04,000
 Dukkha and sukha, instead of pa peet chitang pa pam,

269
00:17:04,000 --> 00:17:12,000
 pa pam mitatang.

270
00:17:12,000 --> 00:17:17,000
 Anyway, there's our dhamma for tonight.

271
00:17:17,000 --> 00:17:20,000
 Don't be like Devadatta.

272
00:17:20,000 --> 00:17:24,000
 Don't let these things overwhelm you.

273
00:17:24,000 --> 00:17:34,000
 Any questions?

274
00:17:34,000 --> 00:17:53,000
 Can you stay for a second? You can go.

275
00:17:53,000 --> 00:17:55,000
 Okay, so we got a couple of questions first.

276
00:17:55,000 --> 00:17:57,000
 I want to say here's...

277
00:17:57,000 --> 00:17:59,000
 Have you met these people?

278
00:17:59,000 --> 00:18:01,000
 Have I showed you?

279
00:18:01,000 --> 00:18:08,000
 This is Michael.

280
00:18:08,000 --> 00:18:11,000
 Michael's living here now.

281
00:18:11,000 --> 00:18:16,000
 He's the steward here for a while.

282
00:18:16,000 --> 00:18:19,000
 Just finished his advanced course,

283
00:18:19,000 --> 00:18:21,000
 so he's done both courses.

284
00:18:21,000 --> 00:18:23,000
 So we'll have to do an interview with Michael

285
00:18:23,000 --> 00:18:25,000
 to see what he thinks of the practice.

286
00:18:25,000 --> 00:18:30,000
 See how it helped him, if it helped him.

287
00:18:30,000 --> 00:18:35,000
 And Sunday we're going to Mississauga together.

288
00:18:35,000 --> 00:18:40,000
 And then...

289
00:18:40,000 --> 00:18:43,000
 I have to go to New York.

290
00:18:43,000 --> 00:18:46,000
 We're going to drive to New York.

291
00:18:46,000 --> 00:18:49,000
 You're going to drive to New York?

292
00:18:49,000 --> 00:18:52,000
 Would you be able to drive to New York?

293
00:18:52,000 --> 00:18:55,000
 How is it about crossing the border back and forth?

294
00:18:55,000 --> 00:18:57,000
 There's no problem, is there?

295
00:18:57,000 --> 00:19:00,000
 What did they say when you came in?

296
00:19:00,000 --> 00:19:02,000
 Did they ask you?

297
00:19:02,000 --> 00:19:04,000
 They did make trouble for you?

298
00:19:04,000 --> 00:19:06,000
 I wouldn't say trouble,

299
00:19:06,000 --> 00:19:08,000
 but they had to wait for about an hour.

300
00:19:08,000 --> 00:19:09,000
 An hour?

301
00:19:09,000 --> 00:19:12,000
 I mean, you know, I mentioned your name,

302
00:19:12,000 --> 00:19:15,000
 and then I was thinking about a story.

303
00:19:15,000 --> 00:19:17,000
 Yeah, they have many questions about that.

304
00:19:17,000 --> 00:19:20,000
 Like suspicious or just curious?

305
00:19:20,000 --> 00:19:21,000
 A little about...

306
00:19:21,000 --> 00:19:23,000
 Oh, we should talk about it.

307
00:19:23,000 --> 00:19:27,000
 You probably should give people letters in the future.

308
00:19:27,000 --> 00:19:31,000
 Letters of recommendation, like letters of invitation.

309
00:19:31,000 --> 00:19:32,000
 How weird.

310
00:19:32,000 --> 00:19:34,000
 Yeah, they did ask for that.

311
00:19:34,000 --> 00:19:35,000
 Yeah.

312
00:19:35,000 --> 00:19:37,000
 Yeah, I think we've had this issue before.

313
00:19:37,000 --> 00:19:40,320
 I just sent a letter of invitation to someone in the

314
00:19:40,320 --> 00:19:41,000
 Ukraine

315
00:19:41,000 --> 00:19:45,360
 who wants to come and meditate, so hopefully that works for

316
00:19:45,360 --> 00:19:46,000
 him.

317
00:19:46,000 --> 00:19:48,000
 So there's this monk I've been talking about.

318
00:19:48,000 --> 00:19:50,860
 He wants to go visit Bikubodi, so he wants me to go with

319
00:19:50,860 --> 00:19:52,000
 him.

320
00:19:52,000 --> 00:19:54,560
 And he was going to drive her, and I said, "Well, I've got

321
00:19:54,560 --> 00:19:55,000
 a driver.

322
00:19:55,000 --> 00:19:58,000
 We've got a Prius, right?

323
00:19:58,000 --> 00:20:05,000
 It's a good mileage, so probably he'll pay for gas."

324
00:20:05,000 --> 00:20:15,000
 But, yeah, so that's a week and a half away, the 17th.

325
00:20:15,000 --> 00:20:22,000
 Sunday we're going to Mississauga for Wesaka.

326
00:20:22,000 --> 00:20:25,000
 Okay.

327
00:20:25,000 --> 00:20:26,000
 So questions.

328
00:20:26,000 --> 00:20:30,000
 Larry says, "During sitting meditation,

329
00:20:30,000 --> 00:20:38,000
 can more than one hindrance be recognized concurrently?"

330
00:20:38,000 --> 00:20:40,000
 Yeah, you won't have them both at once.

331
00:20:40,000 --> 00:20:44,000
 You can't have a virgin end craving concurrently.

332
00:20:44,000 --> 00:20:46,000
 I mean, it feels like that because over time you say,

333
00:20:46,000 --> 00:20:48,000
 "Wow, I was both angry and greedy,"

334
00:20:48,000 --> 00:20:52,000
 but you can't have them both at the exact same moment.

335
00:20:52,000 --> 00:20:55,340
 So concurrently in the broad sense, sure, but in the

336
00:20:55,340 --> 00:20:56,000
 absolute sense, no.

337
00:20:56,000 --> 00:20:58,000
 In the absolute sense, there's one at a time,

338
00:20:58,000 --> 00:21:10,000
 so whichever one is clear in that moment, just focus on it.

339
00:21:10,000 --> 00:21:11,000
 It doesn't really matter.

340
00:21:11,000 --> 00:21:14,760
 I wouldn't worry about which is which, just whichever one

341
00:21:14,760 --> 00:21:15,000
 is clear

342
00:21:15,000 --> 00:21:17,000
 in whatever moment, focus on it.

343
00:21:17,000 --> 00:21:20,000
 You don't have to catch them all.

344
00:21:20,000 --> 00:21:22,000
 But again, you can't have all of them at once,

345
00:21:22,000 --> 00:21:26,370
 except for the fact that aversion to something is kind of

346
00:21:26,370 --> 00:21:27,000
 like desire

347
00:21:27,000 --> 00:21:29,000
 for it not to be, right?

348
00:21:29,000 --> 00:21:32,000
 So you could argue that it's actually in that same moment,

349
00:21:32,000 --> 00:21:33,000
 it's the same thing.

350
00:21:33,000 --> 00:21:35,000
 You're saying the same thing.

351
00:21:35,000 --> 00:21:39,000
 You dislike something means you want it to be gone.

352
00:21:39,000 --> 00:21:42,000
 It's not quite craving, or it is, some people call that

353
00:21:42,000 --> 00:21:50,000
 "vibhavatan," desire for non-existence.

354
00:21:50,000 --> 00:21:54,530
 "Sabhay sankhara dukkha," does that hold true only for unen

355
00:21:54,530 --> 00:21:55,000
lightened beings?

356
00:21:55,000 --> 00:21:57,000
 No.

357
00:21:57,000 --> 00:21:59,000
 No, it does not.

358
00:21:59,000 --> 00:22:01,000
 They are dukkha.

359
00:22:01,000 --> 00:22:07,000
 Dukkha is their characteristic, means they are unsatisfying

360
00:22:07,000 --> 00:22:08,000
.

361
00:22:08,000 --> 00:22:21,000
 It means they are not good.

362
00:22:21,000 --> 00:22:23,000
 It's the intrinsic characteristic.

363
00:22:23,000 --> 00:22:26,000
 It doesn't mean that they are painful,

364
00:22:26,000 --> 00:22:31,000
 or when they arise cause one mental suffering,

365
00:22:31,000 --> 00:22:33,000
 or even physical suffering.

366
00:22:33,000 --> 00:22:35,000
 That's not what dukkha means here.

367
00:22:35,000 --> 00:22:44,000
 Dukkha means unsatisfying or unable to satisfy,

368
00:22:44,000 --> 00:22:47,000
 unable to bring happiness, not happiness.

369
00:22:47,000 --> 00:22:50,000
 Remember these three characteristics are in opposition

370
00:22:50,000 --> 00:22:52,000
 of what we think things are.

371
00:22:52,000 --> 00:22:57,000
 We think things are stable, we think things are satisfying

372
00:22:57,000 --> 00:23:01,000
 or able to satisfy us or productive of true happiness,

373
00:23:01,000 --> 00:23:06,000
 and we think things are ours or me or mine.

374
00:23:06,000 --> 00:23:13,000
 We can control them.

375
00:23:13,000 --> 00:23:16,000
 So this is just realizing that they are not that,

376
00:23:16,000 --> 00:23:19,450
 that they are not nitya, that they are not sukha, that they

377
00:23:19,450 --> 00:23:20,000
 are not ata.

378
00:23:20,000 --> 00:23:23,000
 That's what these three things mean.

379
00:23:23,000 --> 00:23:34,000
 They are actually anichas dukkha anata.

380
00:23:34,000 --> 00:23:36,000
 Can you go back to using the red room?

381
00:23:36,000 --> 00:23:39,000
 Wow, there's someone who actually likes that red.

382
00:23:39,000 --> 00:23:43,000
 No, I can't go back to using the red room.

383
00:23:43,000 --> 00:23:49,000
 There's some disliking, disliking.

384
00:23:49,000 --> 00:23:54,000
 Sadhu means good.

385
00:23:54,000 --> 00:24:04,000
 Sadhu just means good.

386
00:24:04,000 --> 00:24:10,000
 Well, sadhu in Hinduism, sadhu is a, or in India,

387
00:24:10,000 --> 00:24:16,000
 sadhu means a, it's a word also for a holy person,

388
00:24:16,000 --> 00:24:20,000
 like a monk or a recluse, so they call us sadhus.

389
00:24:20,000 --> 00:24:22,000
 They would call me a sadhu.

390
00:24:22,000 --> 00:24:26,000
 But it literally means, at least in Pali, I don't know in

391
00:24:26,000 --> 00:24:26,000
 Sanskrit,

392
00:24:26,000 --> 00:24:29,000
 but in Pali it's just used to mean good.

393
00:24:29,000 --> 00:24:37,000
 Sadhu, it's an exclamation, it means it is good.

394
00:24:37,000 --> 00:24:42,000
 When something is good you say sadhu, that thing is sadhu.

395
00:24:42,000 --> 00:24:48,000
 In Pali you would say, dang sadhu, it is good.

396
00:24:48,000 --> 00:24:51,000
 You wouldn't say it like that, but then like sadhu,

397
00:24:51,000 --> 00:24:53,000
 it was used in the sutta, right?

398
00:24:53,000 --> 00:24:58,400
 Sadhu bhikkhu, bhikkhu, upanang, labang, abhibuya, abhibuya

399
00:24:58,400 --> 00:24:59,000
, bhihareya.

400
00:24:59,000 --> 00:25:11,000
 It is good, or it would be good if one were to dwell again.

401
00:25:11,000 --> 00:25:15,000
 But isn't suffering only mental?

402
00:25:15,000 --> 00:25:17,610
 No, there's two kinds of suffering, there's physical

403
00:25:17,610 --> 00:25:19,000
 suffering and mental suffering,

404
00:25:19,000 --> 00:25:21,000
 but that's again not what this means.

405
00:25:21,000 --> 00:25:24,290
 I don't know, I think there's a lag, so I'm not sure if

406
00:25:24,290 --> 00:25:28,000
 that was after my explanation or before it.

407
00:25:28,000 --> 00:25:30,000
 But again, that's not what dukkha means here.

408
00:25:30,000 --> 00:25:35,000
 Dukkha means not happiness, or not conducive to happiness,

409
00:25:35,000 --> 00:25:40,000
 not productive of happiness.

410
00:25:40,000 --> 00:25:49,000
 Where is Robin?

411
00:25:49,000 --> 00:25:51,000
 Robin's here.

412
00:25:51,000 --> 00:25:55,370
 I was doing video questions, I was forcing people, trying

413
00:25:55,370 --> 00:25:57,000
 to get people to come on the hangout,

414
00:25:57,000 --> 00:26:09,000
 so didn't have any need for someone to read.

415
00:26:09,000 --> 00:26:12,000
 I think there's a way at some time, and then we just stop.

416
00:26:12,000 --> 00:26:16,000
 Everything changes, this is what you have to realize.

417
00:26:16,000 --> 00:26:19,000
 Don't get attached to things as they are.

418
00:26:19,000 --> 00:26:28,000
 Try to change the format around, shake things up.

419
00:26:28,000 --> 00:26:33,000
 Anyway, that's all for tonight.

420
00:26:33,000 --> 00:26:36,000
 Thank you all for tuning in.

421
00:26:36,000 --> 00:26:39,300
 Tomorrow, another thing is tomorrow at the university, it's

422
00:26:39,300 --> 00:26:42,000
 called May at Mac.

423
00:26:42,000 --> 00:26:45,000
 May at Mac, it's the yearly McMaster Open House,

424
00:26:45,000 --> 00:26:50,000
 and the Peace Studies Department has a booth,

425
00:26:50,000 --> 00:26:57,710
 and they've asked some of us to go and talk to new students

426
00:26:57,710 --> 00:27:02,000
 about the Peace Studies Department.

427
00:27:02,000 --> 00:27:05,370
 They take it as an opportunity to get people to sign up for

428
00:27:05,370 --> 00:27:08,000
 the Peace Club as well.

429
00:27:08,000 --> 00:27:11,140
 But it would be interesting, I'll go as a Buddhist monk to

430
00:27:11,140 --> 00:27:13,000
 go and promote the Peace Studies Department.

431
00:27:13,000 --> 00:27:16,000
 I think that'll be good.

432
00:27:16,000 --> 00:27:21,000
 I do believe in it, I'm not sure how, obviously I don't,

433
00:27:21,000 --> 00:27:24,990
 studies is not my, these kind of studies is not my priority

434
00:27:24,990 --> 00:27:25,000
,

435
00:27:25,000 --> 00:27:28,000
 but I want to encourage them and support them.

436
00:27:28,000 --> 00:27:31,500
 Anyone who's talking about peace, studying peace is just

437
00:27:31,500 --> 00:27:34,000
 all good in my book.

438
00:27:34,000 --> 00:27:39,000
 So I'm happy to promote it.

439
00:27:39,000 --> 00:27:44,000
 Anyway, have a good night everyone.

440
00:27:45,000 --> 00:27:54,000
 [BLANK_AUDIO]

