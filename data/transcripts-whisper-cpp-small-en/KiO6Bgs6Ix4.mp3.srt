1
00:00:00,000 --> 00:00:04,800
 Hi, good evening. Tonight I thought I would continue my

2
00:00:04,800 --> 00:00:07,440
 talks on the nature of

3
00:00:07,440 --> 00:00:14,800
 reality, continuing where I last left off, explaining the

4
00:00:14,800 --> 00:00:18,000
 existence of the mind as

5
00:00:18,000 --> 00:00:24,630
 opposed to the existence of material only. And so just to

6
00:00:24,630 --> 00:00:25,480
 recap and sort of

7
00:00:25,480 --> 00:00:28,120
 summarize what I'd said last time, maybe making it a little

8
00:00:28,120 --> 00:00:29,840
 bit more clear,

9
00:00:29,840 --> 00:00:37,850
 basically the idea that there is only the material is about

10
00:00:37,850 --> 00:00:39,520
 as reasonable as

11
00:00:39,520 --> 00:00:45,280
 the idea that there's only mental. And so whereas we can

12
00:00:45,280 --> 00:00:47,760
 make arguments to

13
00:00:47,760 --> 00:00:52,120
 explain the mind away as simply a function of the body,

14
00:00:52,120 --> 00:00:52,760
 well we could also

15
00:00:52,760 --> 00:00:56,730
 explain the body away as simply a delusion or a function of

16
00:00:56,730 --> 00:00:58,240
 the mind. And

17
00:00:58,240 --> 00:01:03,300
 how that works is, well from the body's point of view we

18
00:01:03,300 --> 00:01:05,520
 say that we can look at

19
00:01:05,520 --> 00:01:10,670
 the body and we can look at the brain specifically and take

20
00:01:10,670 --> 00:01:12,280
 parts of this and

21
00:01:12,280 --> 00:01:15,610
 come to say that this part represents this mental function

22
00:01:15,610 --> 00:01:16,440
 and so on and then

23
00:01:16,440 --> 00:01:20,980
 we can say well that's where the mind comes from. On the

24
00:01:20,980 --> 00:01:21,480
 other side we

25
00:01:21,480 --> 00:01:25,400
 can look at the mind and we can say okay when I wish for

26
00:01:25,400 --> 00:01:26,680
 seeing to arise

27
00:01:26,680 --> 00:01:29,200
 then there is seeing and so on when I wish for moving to

28
00:01:29,200 --> 00:01:30,040
 arise then there is

29
00:01:30,040 --> 00:01:33,900
 moving and so we can say well these are just functions of

30
00:01:33,900 --> 00:01:35,840
 the mind, the

31
00:01:35,840 --> 00:01:38,840
 experience of hot, the experience of cold and so on. This

32
00:01:38,840 --> 00:01:41,240
 is simply a result of the

33
00:01:41,240 --> 00:01:48,150
 mind. Now both of these sort of miss the very obvious point

34
00:01:48,150 --> 00:01:49,400
 that both body and

35
00:01:49,400 --> 00:01:55,810
 mind very clearly do exist and so it's taking a leap of

36
00:01:55,810 --> 00:01:57,720
 faith to say that one

37
00:01:57,720 --> 00:02:00,670
 or the other does not exist. It's making an unreasonable

38
00:02:00,670 --> 00:02:02,480
 claim. Now I think the

39
00:02:02,480 --> 00:02:04,720
 problem that most materialists would have with the idea

40
00:02:04,720 --> 00:02:05,600
 that the mind exists

41
00:02:05,600 --> 00:02:09,760
 is this idea that well once you allow the mind to exist

42
00:02:09,760 --> 00:02:11,760
 then your or once you

43
00:02:11,760 --> 00:02:15,770
 create a belief that the mind exists it's associated with

44
00:02:15,770 --> 00:02:19,160
 the belief that

45
00:02:19,160 --> 00:02:22,480
 there's a self or there's a controlling, there is a

46
00:02:22,480 --> 00:02:25,200
 autonomous mind, something

47
00:02:25,200 --> 00:02:30,610
 which is able to experience, is able to have emotions

48
00:02:30,610 --> 00:02:32,520
 without relying or without

49
00:02:32,520 --> 00:02:35,360
 being influenced by the body and of course this is not

50
00:02:35,360 --> 00:02:36,800
 necessary. It's very

51
00:02:36,800 --> 00:02:40,350
 possible to say the mind exists but it's very much inter

52
00:02:40,350 --> 00:02:41,600
dependent with the body

53
00:02:41,600 --> 00:02:45,320
 and it goes both ways because obviously the mind if you

54
00:02:45,320 --> 00:02:47,040
 give it an existence or

55
00:02:47,040 --> 00:02:51,580
 if you allow for its existence then you can very clearly

56
00:02:51,580 --> 00:02:52,320
 see how the mind

57
00:02:52,320 --> 00:02:57,580
 affects the body. Our experience can very much have an

58
00:02:57,580 --> 00:02:58,240
 effect on the body and

59
00:02:58,240 --> 00:03:02,840
 this has been carried out in neuroscience and so on. Now

60
00:03:02,840 --> 00:03:04,360
 you can have

61
00:03:04,360 --> 00:03:07,400
 belief either way and say that it's all just the physical

62
00:03:07,400 --> 00:03:09,080
 reality or

63
00:03:09,080 --> 00:03:11,250
 you can say it's all just the mental reality but the truth

64
00:03:11,250 --> 00:03:12,680
 of it is both of

65
00:03:12,680 --> 00:03:15,580
 these realities very much appear to exist. There's no

66
00:03:15,580 --> 00:03:16,720
 reason to say that one

67
00:03:16,720 --> 00:03:19,180
 or the other doesn't exist and they're very much different

68
00:03:19,180 --> 00:03:23,360
 in their nature. So

69
00:03:23,360 --> 00:03:28,720
 I'd like to then extrapolate on this and say well once you

70
00:03:28,720 --> 00:03:30,400
 admit the existence

71
00:03:30,400 --> 00:03:34,650
 of the mind without admitting that it is some autonomous

72
00:03:34,650 --> 00:03:36,160
 self that is able to

73
00:03:36,160 --> 00:03:42,880
 control or is able to determine one's future without the

74
00:03:42,880 --> 00:03:43,720
 interaction or

75
00:03:43,720 --> 00:03:46,870
 the interdependence on the body. Once you admit the

76
00:03:46,870 --> 00:03:48,400
 existence of the mind then you

77
00:03:48,400 --> 00:03:51,180
 have certain interesting corallaries and so this is what I

78
00:03:51,180 --> 00:03:52,320
 like to talk about in

79
00:03:52,320 --> 00:03:57,760
 my next videos. So in order this video just be sort of an

80
00:03:57,760 --> 00:03:58,280
 introduction

81
00:03:58,280 --> 00:04:03,600
 and I'm going to talk about first of all the existence of

82
00:04:03,600 --> 00:04:05,640
 real ethics which we

83
00:04:05,640 --> 00:04:11,530
 could call absolute ethics as opposed to relative morality.

84
00:04:11,530 --> 00:04:12,080
 The

85
00:04:12,080 --> 00:04:19,910
 non-existence of self and the non-existence of God. I think

86
00:04:19,910 --> 00:04:20,520
 these three

87
00:04:20,520 --> 00:04:22,920
 things there are many other things that you know that come

88
00:04:22,920 --> 00:04:23,960
 from the realization

89
00:04:23,960 --> 00:04:27,120
 of the existence of the mind and of course meditation is a

90
00:04:27,120 --> 00:04:27,960
 very important

91
00:04:27,960 --> 00:04:33,440
 one of those but in these ones I'm going to talk about very

92
00:04:33,440 --> 00:04:34,680
 specifically ideas

93
00:04:34,680 --> 00:04:37,660
 which I think people generally have problems with this is

94
00:04:37,660 --> 00:04:38,400
 the absolute

95
00:04:38,400 --> 00:04:43,330
 morality, the non-existence of self, the non-existence of

96
00:04:43,330 --> 00:04:44,000
 self, the non-existence

97
00:04:44,000 --> 00:04:48,550
 of God. So please tune in and I'll try to address these in

98
00:04:48,550 --> 00:04:52,080
 the next few videos.

