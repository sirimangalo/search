1
00:00:00,000 --> 00:00:04,160
 Hello, welcome back to Ask a Monk. This is a, I think, a

2
00:00:04,160 --> 00:00:05,480
 short, hopefully a short video

3
00:00:05,480 --> 00:00:10,040
 talking about the, it's an old question that I've just been

4
00:00:10,040 --> 00:00:11,680
 putting off, about the

5
00:00:11,680 --> 00:00:16,320
 the working of karma in regards to a Holocaust.

6
00:00:16,320 --> 00:00:21,120
 How does karma work when you're talking about mass

7
00:00:21,120 --> 00:00:24,920
 suffering,

8
00:00:25,160 --> 00:00:29,320
 mass realizations of a certain state? I think actually a

9
00:00:29,320 --> 00:00:31,280
 Holocaust is a bad example,

10
00:00:31,280 --> 00:00:35,040
 but it does highlight some of, part of the answer, so I'll

11
00:00:35,040 --> 00:00:36,360
 take it in as a part of it.

12
00:00:36,360 --> 00:00:40,520
 But let's consider several examples. The Holocaust,

13
00:00:40,520 --> 00:00:45,560
 a tsunami, and

14
00:00:45,560 --> 00:00:50,350
 the planes crashing into the World Trade Center. Let's

15
00:00:50,350 --> 00:00:51,560
 examine all three of these.

16
00:00:51,800 --> 00:00:55,160
 The Holocaust is actually the easiest one to see because

17
00:00:55,160 --> 00:00:57,080
 the answer in all three cases

18
00:00:57,080 --> 00:01:00,000
 is that not, no one dies in the same way. No one

19
00:01:00,000 --> 00:01:02,720
 experiences things in the same way or has the same sort of

20
00:01:02,720 --> 00:01:03,360
 suffering.

21
00:01:03,360 --> 00:01:06,090
 This is really easy to see in a Holocaust because even

22
00:01:06,090 --> 00:01:08,240
 though so many people were killed in

23
00:01:08,240 --> 00:01:11,690
 similar ways, none of them died in the same way and none of

24
00:01:11,690 --> 00:01:13,320
 them experienced it in the same way.

25
00:01:13,320 --> 00:01:15,360
 Some of them might have been calm, some of them would have

26
00:01:15,360 --> 00:01:18,640
 been very frightened, some of them would have been enraged

27
00:01:18,640 --> 00:01:20,280
 and, and so on.

28
00:01:22,360 --> 00:01:25,900
 The death of each person is totally dependent on their mind

29
00:01:25,900 --> 00:01:26,880
 state.

30
00:01:26,880 --> 00:01:30,100
 So to say that all of these people died in the same way and

31
00:01:30,100 --> 00:01:33,300
 therefore it's hard to believe that it was all based on

32
00:01:33,300 --> 00:01:34,240
 their karma is

33
00:01:34,240 --> 00:01:38,240
 really a superficial examination.

34
00:01:38,240 --> 00:01:41,240
 Tsunami is another good example.

35
00:01:41,240 --> 00:01:44,800
 As you think all of these people drowned, well, that's not

36
00:01:44,800 --> 00:01:47,080
 karma, that's an act of nature. But

37
00:01:47,840 --> 00:01:51,200
 it really is, it's hard to say one way or the other because

38
00:01:51,200 --> 00:01:54,320
 the people died based on

39
00:01:54,320 --> 00:01:58,470
 their individual situations and though everyone, it's true

40
00:01:58,470 --> 00:01:59,480
 that many people drowned,

41
00:01:59,480 --> 00:02:03,500
 their, each individual's drowning was quite different. So

42
00:02:03,500 --> 00:02:06,360
 their experience of the, of the, of the

43
00:02:06,360 --> 00:02:13,400
 the event was, would be quite different as well. Also their

44
00:02:13,400 --> 00:02:13,400
,

45
00:02:14,400 --> 00:02:17,190
 their experience and their response to it would have been

46
00:02:17,190 --> 00:02:19,480
 quite different. As I said, some people will be

47
00:02:19,480 --> 00:02:23,520
 quite disturbed by it, some people will be quite calm or

48
00:02:23,520 --> 00:02:27,110
 reasonably calm. Some people will die instantly, some

49
00:02:27,110 --> 00:02:30,240
 people would take a long time to die and so on.

50
00:02:30,240 --> 00:02:34,160
 It totally depends, I think it mainly depends on the person

51
00:02:34,160 --> 00:02:35,800
's karma. Now you may say there are other,

52
00:02:35,800 --> 00:02:38,480
 you know, in fact,

53
00:02:38,480 --> 00:02:40,480
 the word karma, it's,

54
00:02:40,480 --> 00:02:42,910
 first of all, as I've said, the Buddha, the Buddha didn't,

55
00:02:42,910 --> 00:02:45,680
 didn't talk about karma. He talked about the mind and the

56
00:02:45,680 --> 00:02:46,800
 intentions that we have in

57
00:02:46,800 --> 00:02:49,570
 the mind and then he said only a Buddha could possibly

58
00:02:49,570 --> 00:02:50,760
 understand karma.

59
00:02:50,760 --> 00:02:53,200
 It's one of those things that you should never think about,

60
00:02:53,200 --> 00:02:55,480
 the workings of karma, what caused this, what caused that?

61
00:02:55,480 --> 00:02:58,800
 Because all it means is that things go by cause and effect

62
00:02:58,800 --> 00:03:01,520
 and the mind is, is casually effective.

63
00:03:01,520 --> 00:03:04,650
 That's all it means, the mind is not an epiphenomenon, it's

64
00:03:04,650 --> 00:03:06,840
 not something that is created by the brain.

65
00:03:06,840 --> 00:03:09,960
 It's an active participant in reality.

66
00:03:11,600 --> 00:03:14,350
 But what causes lead to what results is so intricately

67
00:03:14,350 --> 00:03:19,280
 intertwined that you could never really break it apart.

68
00:03:19,280 --> 00:03:22,640
 So you could say, what was the karma that caused all those

69
00:03:22,640 --> 00:03:26,200
 people to be brought to the tsunami? Well, you're talking

70
00:03:26,200 --> 00:03:27,080
 about a whole

71
00:03:27,080 --> 00:03:30,880
 confluence, I think is the word, of different

72
00:03:30,880 --> 00:03:34,660
 causes that bring everyone together just in that one

73
00:03:34,660 --> 00:03:38,720
 instant and it's just a fluctuation of the universe.

74
00:03:38,720 --> 00:03:41,870
 It's scientific, you know, science would explain the

75
00:03:41,870 --> 00:03:44,280
 tsunami and all the people dying in terms of atoms coming

76
00:03:44,280 --> 00:03:44,840
 together and

77
00:03:44,840 --> 00:03:47,880
 just came to get, happen to come together in that way.

78
00:03:47,880 --> 00:03:50,520
 Karma simply, you know,

79
00:03:50,520 --> 00:03:54,330
 explains the part that the mind played and all that to

80
00:03:54,330 --> 00:03:56,870
 bring people together into this one place and then to

81
00:03:56,870 --> 00:03:57,760
 separate.

82
00:03:57,760 --> 00:04:01,370
 And again, as I say, it's not really that they were all in

83
00:04:01,370 --> 00:04:02,160
 one place or one

84
00:04:02,160 --> 00:04:05,470
 experience because each person's experience would be indeed

85
00:04:05,470 --> 00:04:06,640
 quite different.

86
00:04:07,040 --> 00:04:09,280
 The third example is in a plane crash,

87
00:04:09,280 --> 00:04:13,900
 which is the hardest to see, I think. But even there, you

88
00:04:13,900 --> 00:04:16,000
 still have differences of

89
00:04:16,000 --> 00:04:19,400
 experience and more importantly differences of reaction to

90
00:04:19,400 --> 00:04:21,560
 the experience and therefore

91
00:04:21,560 --> 00:04:26,480
 different results and you could say different

92
00:04:26,480 --> 00:04:30,520
 karma that led to it.

93
00:04:30,520 --> 00:04:33,920
 But there is an element of

94
00:04:35,320 --> 00:04:37,320
 group and mutual

95
00:04:37,320 --> 00:04:39,800
 karma and it has to do with how we are

96
00:04:39,800 --> 00:04:43,470
 intertwined with each other, why we are born to the parents

97
00:04:43,470 --> 00:04:45,760
 that we are, why we live in the same house with certain

98
00:04:45,760 --> 00:04:45,960
 people,

99
00:04:45,960 --> 00:04:49,840
 why we work with certain people, are coming together in,

100
00:04:49,840 --> 00:04:50,400
 you know,

101
00:04:50,400 --> 00:04:54,000
 why wouldn't we equally ask, isn't it strange that we

102
00:04:54,000 --> 00:04:56,480
 should all be working in the same job?

103
00:04:56,480 --> 00:04:58,600
 Or isn't it strange that we should all be born into the

104
00:04:58,600 --> 00:04:59,280
 same family?

105
00:04:59,560 --> 00:05:03,040
 It's all the same. It all has to do with our path and we

106
00:05:03,040 --> 00:05:06,570
 meet up and we experience certain events, death being one

107
00:05:06,570 --> 00:05:07,560
 of them, sometimes

108
00:05:07,560 --> 00:05:10,800
 tragic and

109
00:05:10,800 --> 00:05:13,080
 simultaneous death.

110
00:05:13,080 --> 00:05:17,010
 But it all depends on so many different factors that bring

111
00:05:17,010 --> 00:05:17,600
 you together.

112
00:05:17,600 --> 00:05:20,540
 And of course, it has to do with the nature of the physical

113
00:05:20,540 --> 00:05:21,320
 body, which

114
00:05:21,320 --> 00:05:24,760
 isn't able to bring or

115
00:05:26,280 --> 00:05:28,980
 prevents the mind from bringing about results instant

116
00:05:28,980 --> 00:05:30,440
aneously. If it were all mental,

117
00:05:30,440 --> 00:05:34,700
 you'd receive karmic results much quicker. For instance,

118
00:05:34,700 --> 00:05:38,360
 angels and ghosts and so on, they receive karmic

119
00:05:38,360 --> 00:05:44,320
 results much quicker. When an angel gets angry, I think

120
00:05:44,320 --> 00:05:46,440
 they fall. If they get angry enough, they die

121
00:05:46,440 --> 00:05:50,070
 simply by being angry. Now, it takes a lot of anger for a

122
00:05:50,070 --> 00:05:52,520
 human being to die, but for an angel

123
00:05:52,520 --> 00:05:55,310
 it's not that much because it changes their mind and

124
00:05:55,310 --> 00:05:58,640
 because they're mostly mental with a very limited physical.

125
00:05:58,640 --> 00:06:04,000
 Simply a little amount of anger can cause them to

126
00:06:04,000 --> 00:06:08,880
 change their state because the mind is very quick to change

127
00:06:08,880 --> 00:06:08,880
.

128
00:06:08,880 --> 00:06:11,770
 But the body is not because the body is so coarse and as I

129
00:06:11,770 --> 00:06:13,680
 said before has such inertia.

130
00:06:13,680 --> 00:06:20,680
 It can take a long time for it to catch up with us and as a

131
00:06:20,680 --> 00:06:21,320
 result,

132
00:06:21,320 --> 00:06:23,770
 it can all build up, build up, and build up until the body

133
00:06:23,770 --> 00:06:26,440
 has a chance in the case of a plane crash and all this

134
00:06:26,440 --> 00:06:29,340
 build-up of karma and so on that brings people together and

135
00:06:29,340 --> 00:06:32,320
 pushes them all into the same position and then suddenly

136
00:06:32,320 --> 00:06:32,800
 snaps.

137
00:06:32,800 --> 00:06:36,030
 This is the way the physical works because of the build-up

138
00:06:36,030 --> 00:06:40,560
 of power and the capacity it has to collect

139
00:06:40,560 --> 00:06:45,400
 static energy. That's how the physical works. The physical

140
00:06:45,400 --> 00:06:47,860
 has so much static energy in it that

141
00:06:48,160 --> 00:06:51,360
 this is how nuclear atomic bombs are created because there

142
00:06:51,360 --> 00:06:52,920
's so much energy in matter.

143
00:06:52,920 --> 00:06:58,560
 This is all just a part of it how matter works in ways that

144
00:06:58,560 --> 00:07:00,400
 somehow mask the

145
00:07:00,400 --> 00:07:04,240
 workings of the mind simply because of inertia and static

146
00:07:04,240 --> 00:07:05,040
 energy.

147
00:07:05,040 --> 00:07:09,000
 So that's really all that... it shouldn't be a great

148
00:07:09,000 --> 00:07:12,430
 mystery as to why people all die together and it shouldn't

149
00:07:12,430 --> 00:07:13,320
 be certainly a

150
00:07:14,640 --> 00:07:18,160
 disproof of the act, you know, the workings of karma

151
00:07:18,160 --> 00:07:19,160
 because karma is such a

152
00:07:19,160 --> 00:07:22,580
 Buddhist concept of karma, such an intricate thing. All you

153
00:07:22,580 --> 00:07:26,280
 can say is that when you do good things your life

154
00:07:26,280 --> 00:07:29,370
 changes and your path changes in a good way. When you do

155
00:07:29,370 --> 00:07:31,560
 bad things it changes in a bad way.

156
00:07:31,560 --> 00:07:35,560
 You know what exactly that will be and the

157
00:07:35,560 --> 00:07:38,400
 scale and the nature

158
00:07:40,800 --> 00:07:43,350
 is very very very difficult to understand because it

159
00:07:43,350 --> 00:07:44,800
 involves so many different

160
00:07:44,800 --> 00:07:49,330
 things working. Obviously it could never be so simple as A

161
00:07:49,330 --> 00:07:51,840
 leads to B and C leads to D because

162
00:07:51,840 --> 00:07:54,920
 you have so many different causes every moment

163
00:07:54,920 --> 00:07:57,840
 creating new causes, creating new effects

164
00:07:57,840 --> 00:08:01,560
 and so on. So I hope that helps to

165
00:08:01,560 --> 00:08:05,380
 give an explanation of how karma can possibly work in the

166
00:08:05,380 --> 00:08:08,840
 case of a Holocaust. Doesn't seem to be at all

167
00:08:09,280 --> 00:08:11,280
 out of line with the idea of

168
00:08:11,280 --> 00:08:15,360
 cause and effect. I mean it obviously is cause and effect.

169
00:08:15,360 --> 00:08:19,360
 There is obviously something that is a cause for this.

170
00:08:19,360 --> 00:08:21,950
 It's something that we don't see, we don't think about this

171
00:08:21,950 --> 00:08:22,880
 whole idea of karma.

172
00:08:22,880 --> 00:08:26,070
 How all these people who died in the Holocaust could

173
00:08:26,070 --> 00:08:26,880
 possibly be

174
00:08:26,880 --> 00:08:31,330
 guilty of something. But it really... if you look at how

175
00:08:31,330 --> 00:08:34,160
 many people are guilty of such horrible atrocities,

176
00:08:34,160 --> 00:08:38,010
 even in this life and where it can possibly lead them in

177
00:08:38,010 --> 00:08:38,640
 the future,

178
00:08:39,160 --> 00:08:41,160
 and how

179
00:08:41,160 --> 00:08:44,880
 where these people will be going in the future.

180
00:08:44,880 --> 00:08:48,520
 You know you can have people who are

181
00:08:48,520 --> 00:08:51,320
 developing on wholesome mind states and

182
00:08:51,320 --> 00:08:55,120
 setting themselves up for great suffering in the future. On

183
00:08:55,120 --> 00:08:58,510
 a massive scale this sort of thing occurs. People who

184
00:08:58,510 --> 00:09:01,040
 engage in racism, people who engage in

185
00:09:01,040 --> 00:09:03,520
 bigotry and

186
00:09:04,560 --> 00:09:06,940
 who have great hatred in their mind. I mean even those

187
00:09:06,940 --> 00:09:09,080
 places where people don't actively hurt others,

188
00:09:09,080 --> 00:09:12,240
 but simply build up and cultivate prejudice and hate. I

189
00:09:12,240 --> 00:09:15,520
 think there are a lot of people like this in the world.

190
00:09:15,520 --> 00:09:18,940
 There's cultures like this in the world, and I'm not going

191
00:09:18,940 --> 00:09:22,040
 to name names, but many cultures in many different places,

192
00:09:22,040 --> 00:09:25,480
 many different countries of the world have a great amount

193
00:09:25,480 --> 00:09:29,400
 of hate and prejudice for things that are foreign or for

194
00:09:29,400 --> 00:09:34,160
 a certain group of people and so on, certain type of people

195
00:09:34,160 --> 00:09:34,160
.

196
00:09:34,760 --> 00:09:37,920
 And that's what it's that sort of thing that will lead you

197
00:09:37,920 --> 00:09:41,200
 to become a victim of your own anger and your own

198
00:09:41,200 --> 00:09:43,600
 hatred and of course other people's

199
00:09:43,600 --> 00:09:47,920
 taking advantage of that, taking advantage of your

200
00:09:47,920 --> 00:09:51,160
 or becoming a part of

201
00:09:51,160 --> 00:09:53,320
 that

202
00:09:53,320 --> 00:09:59,820
 stream of karmic energy. So that's my take on the Holocaust

203
00:09:59,820 --> 00:10:00,200
.

204
00:10:00,200 --> 00:10:03,280
 So thanks for tuning in, all the best.

