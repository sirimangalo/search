1
00:00:00,000 --> 00:00:14,000
 Good evening everyone.

2
00:00:14,000 --> 00:00:34,240
 Broadcasting live July 5th. Here to review, discuss, to

3
00:00:34,240 --> 00:00:41,000
 unravel the Dhamma.

4
00:00:41,000 --> 00:00:46,970
 Today we have a rather odd sort of quote. It doesn't seem

5
00:00:46,970 --> 00:00:54,000
 to me to be all that of an essential quote.

6
00:00:54,000 --> 00:01:00,000
 It talks about associating with good people.

7
00:01:00,000 --> 00:01:03,000
 And I don't think it's quite a good translation.

8
00:01:03,000 --> 00:01:11,050
 The mikubodhi's translation seems more reasonable to me. It

9
00:01:11,050 --> 00:01:15,990
 says a person who should be associated with, to be followed

10
00:01:15,990 --> 00:01:18,000
, to be served.

11
00:01:18,000 --> 00:01:25,920
 What kind of a person? What kind of a person should you

12
00:01:25,920 --> 00:01:28,000
 follow?

13
00:01:28,000 --> 00:01:33,250
 It starts off, the sutta starts off by talking about those

14
00:01:33,250 --> 00:01:37,980
 who should be looked upon with, discussed, and not

15
00:01:37,980 --> 00:01:40,000
 associated with.

16
00:01:40,000 --> 00:01:47,930
 So if someone is immoral or a bad character, impure of

17
00:01:47,930 --> 00:01:54,000
 suspect behavior, such a person one should not follow.

18
00:01:54,000 --> 00:01:59,990
 Because even if one doesn't take their example, it says hab

19
00:01:59,990 --> 00:02:04,000
ikubodhi, it says it doesn't take their example.

20
00:02:04,000 --> 00:02:12,790
 Still people will assume the worst. You will be branded

21
00:02:12,790 --> 00:02:20,000
 tired with the same brush.

22
00:02:20,000 --> 00:02:26,240
 People will think, "Oh, you must be like that person that

23
00:02:26,240 --> 00:02:28,000
 you follow."

24
00:02:28,000 --> 00:02:34,980
 Among other things, I mean, there's lots of trouble that

25
00:02:34,980 --> 00:02:39,000
 surrounds people who do bad things.

26
00:02:39,000 --> 00:02:43,940
 So sometimes we remain friends and we, out of loyalty,

27
00:02:43,940 --> 00:02:49,000
 think that we should stick with people who are in a bad way

28
00:02:49,000 --> 00:02:53,000
 for reasons other than their behavior.

29
00:02:53,000 --> 00:02:57,340
 And regardless of whether you agree with that, there's a

30
00:02:57,340 --> 00:03:01,690
 certain amount of trouble that follows from associating

31
00:03:01,690 --> 00:03:11,000
 with corrupt individuals, people who are immoral, impure,

32
00:03:11,000 --> 00:03:13,000
 who do bad things.

33
00:03:13,000 --> 00:03:19,500
 Such a person, in the end, such people should be avoided if

34
00:03:19,500 --> 00:03:21,000
 possible.

35
00:03:21,000 --> 00:03:25,310
 There's lots of reasons. The Buddha gives one of the, I

36
00:03:25,310 --> 00:03:29,000
 guess the most obvious is the bad reputation.

37
00:03:29,000 --> 00:03:33,330
 And it makes it hard to live your life because no one wants

38
00:03:33,330 --> 00:03:37,680
 to be around you all of a sudden because they know who your

39
00:03:37,680 --> 00:03:39,000
 friends are.

40
00:03:39,000 --> 00:03:45,500
 And there's another type of person who should be looked

41
00:03:45,500 --> 00:03:52,000
 upon with equanimity but also not associated with.

42
00:03:52,000 --> 00:03:57,980
 So the first type of person should be looked upon with

43
00:03:57,980 --> 00:04:00,000
 disgust.

44
00:04:00,000 --> 00:04:10,000
 "Chikuchitobol" to be shunned, should be shunned.

45
00:04:10,000 --> 00:04:15,640
 The second type of person should be seen with equanimity

46
00:04:15,640 --> 00:04:19,000
 but also not associated with.

47
00:04:19,000 --> 00:04:25,770
 This is a person who though they may not do bad things, say

48
00:04:25,770 --> 00:04:32,520
 bad things, they still, they don't do evil deeds but they

49
00:04:32,520 --> 00:04:36,000
 still have evil mind states.

50
00:04:36,000 --> 00:04:40,090
 So they're prone to anger. If they're slightly criticized,

51
00:04:40,090 --> 00:04:44,220
 they become, they lose their temper and become irritable,

52
00:04:44,220 --> 00:04:46,000
 hostile and stubborn.

53
00:04:46,000 --> 00:04:50,000
 They display irritation, hatred and bitterness.

54
00:04:50,000 --> 00:04:55,540
 Just as a festering sore, if struck by a stick or a shard,

55
00:04:55,540 --> 00:05:00,000
 will discharge even more matter, so too.

56
00:05:00,000 --> 00:05:07,000
 Or just as a firebrand, will sizzle and crack.

57
00:05:07,000 --> 00:05:12,960
 Just as a pit of feces, if struck by a stick, will become

58
00:05:12,960 --> 00:05:16,000
 even more foul smelling.

59
00:05:16,000 --> 00:05:23,000
 These sort of people, you best just leave them alone.

60
00:05:23,000 --> 00:05:30,140
 They're not identifiably evil in terms of giving you a bad

61
00:05:30,140 --> 00:05:37,150
 reputation or getting you caught up in evil deeds or even

62
00:05:37,150 --> 00:05:38,000
 worse,

63
00:05:38,000 --> 00:05:43,630
 encouraging you yourself to engage in evil deeds but there

64
00:05:43,630 --> 00:05:47,500
's a certain nastiness for people who get angry and for

65
00:05:47,500 --> 00:05:49,000
 people who are greedy.

66
00:05:49,000 --> 00:05:55,760
 These sort of people, you should still try to keep your

67
00:05:55,760 --> 00:06:04,140
 distance, not because of the explicit evil but because of

68
00:06:04,140 --> 00:06:06,000
 the more,

69
00:06:06,000 --> 00:06:13,800
 not still explicit but the more inner corruption that's

70
00:06:13,800 --> 00:06:18,000
 going to poke it with a stick, it smells even worse.

71
00:06:18,000 --> 00:06:21,950
 If you get involved with such people, there's always

72
00:06:21,950 --> 00:06:25,000
 conflict, there's always trouble.

73
00:06:25,000 --> 00:06:32,000
 These sort of people are best treated like a pile of crap.

74
00:06:32,000 --> 00:06:37,110
 It's okay as long as you don't poke it with a stick or step

75
00:06:37,110 --> 00:06:38,000
 in it.

76
00:06:38,000 --> 00:06:43,040
 But any such person should be associated with followed and

77
00:06:43,040 --> 00:06:46,580
 served. This is what the quote says, "If someone is

78
00:06:46,580 --> 00:06:48,750
 virtuous and of good character, you shouldn't hang out with

79
00:06:48,750 --> 00:06:49,000
 him."

80
00:06:49,000 --> 00:06:53,000
 This is the sort of person you should stick around with.

81
00:06:53,000 --> 00:06:56,550
 He gives this curious, it's just a reflection of the other

82
00:06:56,550 --> 00:07:01,060
 one but it's still kind of curious that it's not the only

83
00:07:01,060 --> 00:07:02,000
 reason.

84
00:07:02,000 --> 00:07:06,950
 But he says, "Even if you don't follow the example and

85
00:07:06,950 --> 00:07:12,280
 become a good person yourself, people will give a good

86
00:07:12,280 --> 00:07:14,000
 reputation."

87
00:07:14,000 --> 00:07:19,500
 Oh yes, this person has good friends, this person has good

88
00:07:19,500 --> 00:07:21,000
 companions.

89
00:07:21,000 --> 00:07:26,070
 I think it's a little bit superficial to say but there's

90
00:07:26,070 --> 00:07:30,000
 something a little more to it than that.

91
00:07:30,000 --> 00:07:37,530
 It's a sign of good judgment and I think more importantly

92
00:07:37,530 --> 00:07:40,000
 it's a doorway.

93
00:07:40,000 --> 00:07:43,130
 Even if you don't become a good person by associating with

94
00:07:43,130 --> 00:07:47,000
 good people, there's always the option, the opportunity.

95
00:07:47,000 --> 00:07:50,070
 The sort of superficial benefits of having people say good

96
00:07:50,070 --> 00:07:54,250
 things about you and even being free from any of the

97
00:07:54,250 --> 00:07:57,230
 dangers or the troubles that come from associating with

98
00:07:57,230 --> 00:07:58,000
 corrupt people.

99
00:07:58,000 --> 00:08:01,000
 But all that's fairly superficial.

100
00:08:01,000 --> 00:08:05,410
 Obviously more important is even if you don't pick up their

101
00:08:05,410 --> 00:08:10,010
 habits immediately, the opportunity to pick up good habits

102
00:08:10,010 --> 00:08:15,020
 and the potential to become a better person is of course

103
00:08:15,020 --> 00:08:18,000
 greatly improved when you hang out with good people.

104
00:08:18,000 --> 00:08:22,870
 So you may not be, you don't need to feel inferior or

105
00:08:22,870 --> 00:08:28,230
 unworthy and so on. You should seek out good people with

106
00:08:28,230 --> 00:08:31,000
 the thought that I will become a better person myself.

107
00:08:31,000 --> 00:08:37,000
 Anyway, it's a fairly simple quote.

108
00:08:37,000 --> 00:08:42,910
 Not much to say about that. But there's so much to say

109
00:08:42,910 --> 00:08:47,410
 about friendship and hanging out with good people, being

110
00:08:47,410 --> 00:08:49,000
 around good people.

111
00:08:49,000 --> 00:08:53,630
 Especially when you're doing something so personal as

112
00:08:53,630 --> 00:08:57,000
 meditation. So much to do with the individual.

113
00:08:57,000 --> 00:09:00,030
 It's important you associate with good individuals who are

114
00:09:00,030 --> 00:09:04,960
 a good example. People who we'd like to become more and

115
00:09:04,960 --> 00:09:07,000
 more similar to.

116
00:09:07,000 --> 00:09:13,120
 People who are engaged in activities that we're attempting

117
00:09:13,120 --> 00:09:16,000
 to cultivate ourselves.

118
00:09:16,000 --> 00:09:20,550
 So when you see other people meditating, it's more likely

119
00:09:20,550 --> 00:09:24,000
 that you're going to meditate yourself.

120
00:09:24,000 --> 00:09:29,780
 More importantly, the good qualities of such a person are

121
00:09:29,780 --> 00:09:35,000
 more likely to rub off and provide a good example.

122
00:09:35,000 --> 00:09:41,730
 Sort of a road sign. Something to look for when you're on

123
00:09:41,730 --> 00:09:43,000
 the path.

124
00:09:43,000 --> 00:09:47,320
 To start becoming more like these people as you see the

125
00:09:47,320 --> 00:09:49,000
 goodness in them.

126
00:09:49,000 --> 00:09:55,000
 There's a great benefit for being around good people.

127
00:09:55,000 --> 00:09:58,630
 It's great to come to a meditation center. We've got lots

128
00:09:58,630 --> 00:10:01,000
 of people coming this year. It's great.

129
00:10:01,000 --> 00:10:06,270
 We've got people signing up for courses all the way through

130
00:10:06,270 --> 00:10:08,000
 October so far.

131
00:10:08,000 --> 00:10:16,000
 It's really great to see such interest.

132
00:10:16,000 --> 00:10:18,620
 We're still looking for a steward if there's anybody out

133
00:10:18,620 --> 00:10:23,000
 there who would like to come and stay for a longer period.

134
00:10:23,000 --> 00:10:28,020
 There's one person from Israel who mentions staying long

135
00:10:28,020 --> 00:10:31,000
 term so maybe that'll work out. I don't know.

136
00:10:31,000 --> 00:10:35,000
 Coming in September I think.

137
00:10:35,000 --> 00:10:39,120
 But if anyone would like to talk about that and would be

138
00:10:39,120 --> 00:10:43,000
 interested in helping out around here for months.

139
00:10:43,000 --> 00:10:47,710
 For some longer period of time that'd be great. Lots of

140
00:10:47,710 --> 00:10:52,000
 stuff going on.

141
00:10:52,000 --> 00:10:56,000
 So any questions?

142
00:10:56,000 --> 00:10:59,950
 Does meditating during an activity count as a formal

143
00:10:59,950 --> 00:11:01,000
 practice?

144
00:11:01,000 --> 00:11:12,000
 Would it be alright to log it as time spent in meditation?

145
00:11:12,000 --> 00:11:15,700
 Yeah, I mean it's not like we're going to check on you as

146
00:11:15,700 --> 00:11:18,000
 to how you meditate it or so on.

147
00:11:18,000 --> 00:11:26,000
 But I don't quite understand what you're saying.

148
00:11:26,000 --> 00:11:33,030
 It's really an activity. I don't understand what you mean

149
00:11:33,030 --> 00:11:35,000
 by activity.

150
00:11:35,000 --> 00:11:39,710
 Is there a shared responsibility impact for one another's

151
00:11:39,710 --> 00:11:44,000
 lives rather than just one's own responsibility?

152
00:11:44,000 --> 00:11:51,000
 Collective responsibility.

153
00:11:51,000 --> 00:11:56,000
 Not really but there's a sense of rightness.

154
00:11:56,000 --> 00:12:00,220
 If you don't help other people when given the opportunity

155
00:12:00,220 --> 00:12:03,000
 there's a sense of tension involved.

156
00:12:03,000 --> 00:12:13,560
 And to some extent you have to do some things to benefit

157
00:12:13,560 --> 00:12:15,000
 others.

158
00:12:15,000 --> 00:12:22,790
 If you are cold and ignore the benefit to others it can be

159
00:12:22,790 --> 00:12:29,000
 a hindrance to your practice.

160
00:12:29,000 --> 00:12:35,250
 But no we're not responsible for other people's well-being,

161
00:12:35,250 --> 00:12:37,000
 not directly.

162
00:12:37,000 --> 00:12:43,740
 We just have certain duties that as individuals it's proper

163
00:12:43,740 --> 00:12:45,000
 to perform.

164
00:12:45,000 --> 00:12:50,000
 Or you know it's a part of right, what's right.

165
00:12:50,000 --> 00:12:59,300
 The right action for the individual often involves helping

166
00:12:59,300 --> 00:13:01,000
 others.

167
00:13:01,000 --> 00:13:03,400
 If you're looking for disciples for your centre, other

168
00:13:03,400 --> 00:13:06,000
 monks or people seeking to become monks,

169
00:13:06,000 --> 00:13:10,260
 I would be interested in being a steward. What would be the

170
00:13:10,260 --> 00:13:11,000
 setup?

171
00:13:11,000 --> 00:13:15,520
 Well the setup, you'd have to do about a month of

172
00:13:15,520 --> 00:13:18,000
 meditation with us first.

173
00:13:18,000 --> 00:13:23,010
 And then you'd just stay on after that but it would involve

174
00:13:23,010 --> 00:13:26,000
 doing some of the things that intensive meditators,

175
00:13:26,000 --> 00:13:30,000
 beginner meditators anyway aren't able to do.

176
00:13:30,000 --> 00:13:38,990
 Like some amount of cleaning and organizing and caring for

177
00:13:38,990 --> 00:13:43,000
 the monastery and helping me out with whatever I need.

178
00:13:43,000 --> 00:13:46,600
 I mean we had someone recently who had a car which was

179
00:13:46,600 --> 00:13:49,000
 great so he could drive me places.

180
00:13:49,000 --> 00:13:58,000
 But that's not entirely necessary but it would be a plus.

181
00:13:58,000 --> 00:14:01,000
 Yeah some cooking maybe but not really.

182
00:14:01,000 --> 00:14:05,690
 Some things to do with, you know we need a local person to

183
00:14:05,690 --> 00:14:08,000
 organize things that I can't organize.

184
00:14:08,000 --> 00:14:12,640
 Things with money and buying basic supplies for the

185
00:14:12,640 --> 00:14:14,000
 monastery.

186
00:14:14,000 --> 00:14:18,000
 There's not a lot to do but there are things to be done.

187
00:14:18,000 --> 00:14:23,000
 It would help to have a steward.

188
00:14:23,000 --> 00:14:26,790
 I think part of it was the organization one, someone who

189
00:14:26,790 --> 00:14:28,000
 would cook for me.

190
00:14:28,000 --> 00:14:34,510
 Because right now I'm going to the restaurant and taking

191
00:14:34,510 --> 00:14:37,000
 advantage of people's generosity.

192
00:14:37,000 --> 00:14:42,350
 In offering gift cards for certain restaurants but it's

193
00:14:42,350 --> 00:14:45,000
 quite expensive.

194
00:14:45,000 --> 00:14:52,820
 It's not exactly what you call, it's not efficient or

195
00:14:52,820 --> 00:14:55,000
 economical in doing that.

196
00:14:55,000 --> 00:15:02,760
 It would make more sense to somehow do cooking at the

197
00:15:02,760 --> 00:15:05,000
 monastery.

198
00:15:05,000 --> 00:15:09,000
 But as far as becoming a monk I'm shying away from that.

199
00:15:09,000 --> 00:15:13,000
 But long term meditators, the options certainly open.

200
00:15:13,000 --> 00:15:17,870
 But we're not really advertising it because it's too easy

201
00:15:17,870 --> 00:15:19,000
 to miss the point.

202
00:15:19,000 --> 00:15:22,000
 Ordaining should be a part of your meditation practice.

203
00:15:22,000 --> 00:15:25,000
 It shouldn't be anything separate.

204
00:15:25,000 --> 00:15:28,530
 If you want to meditate long term well becoming a monk

205
00:15:28,530 --> 00:15:31,000
 means allowing you to do that.

206
00:15:31,000 --> 00:15:35,000
 That's all.

207
00:15:35,000 --> 00:15:38,150
 By calling someone's behavior disgusting is that not

208
00:15:38,150 --> 00:15:39,000
 judging it?

209
00:15:39,000 --> 00:15:41,000
 Judging is not always bad.

210
00:15:41,000 --> 00:15:45,000
 Discerning.

211
00:15:45,000 --> 00:15:52,000
 Disgusting is probably the wrong word.

212
00:15:52,000 --> 00:15:56,760
 It's just a word but if you're actually disgusted by

213
00:15:56,760 --> 00:16:02,350
 something obviously that is a disliking of it or a version

214
00:16:02,350 --> 00:16:04,000
 towards it.

215
00:16:04,000 --> 00:16:13,570
 We use disgust in a sort of a word in regards to this sort

216
00:16:13,570 --> 00:16:18,400
 of a different kind of repulsion in the sense of the

217
00:16:18,400 --> 00:16:20,000
 inability to do it through wisdom.

218
00:16:20,000 --> 00:16:24,420
 The inability to get involved with something out of a

219
00:16:24,420 --> 00:16:27,740
 knowledge that it's bad, that it's wrong, that it leads to

220
00:16:27,740 --> 00:16:29,000
 suffering basically.

221
00:16:29,000 --> 00:16:33,660
 So if you know something leads to suffering the more you

222
00:16:33,660 --> 00:16:36,000
 know the harder it is for you to engage in it.

223
00:16:36,000 --> 00:16:41,990
 And when the potential to engage with people for example

224
00:16:41,990 --> 00:16:46,000
 who are doing bad things arises when the opportunity arises

225
00:16:46,000 --> 00:16:46,000
.

226
00:16:46,000 --> 00:16:49,920
 There's a certain recoiling that doesn't have anything to

227
00:16:49,920 --> 00:16:54,000
 do necessarily with aversion but it's based on a knowledge

228
00:16:54,000 --> 00:16:58,000
 that that's not the way to do things.

229
00:16:58,000 --> 00:17:10,000
 Getting involved with that person is not beneficial.

230
00:17:10,000 --> 00:17:16,000
 I clean a bathroom I do it as mindfully as possible.

231
00:17:16,000 --> 00:17:18,000
 Right.

232
00:17:18,000 --> 00:17:20,000
 No that's not quite what we're looking for.

233
00:17:20,000 --> 00:17:23,760
 I mean that's great. That's awesome if you're able to do it

234
00:17:23,760 --> 00:17:24,000
.

235
00:17:24,000 --> 00:17:30,000
 But we're really looking for formal meditation practice.

236
00:17:30,000 --> 00:17:32,000
 That's what's meant here.

237
00:17:32,000 --> 00:17:42,000
 I actually do formal walking and sitting.

238
00:17:42,000 --> 00:17:45,000
 Amazingly you do not cook your own food.

239
00:17:45,000 --> 00:17:49,000
 I meditate with preparing food as part of my practice.

240
00:17:49,000 --> 00:17:55,000
 Monks aren't allowed to keep food.

241
00:17:55,000 --> 00:18:02,000
 It's part of a practice of renunciation as a lifestyle.

242
00:18:02,000 --> 00:18:06,000
 We don't keep food, cook food.

243
00:18:06,000 --> 00:18:10,000
 We can't eat anything unless it's given to us.

244
00:18:10,000 --> 00:18:15,280
 Preparing your own food has its own problems because of the

245
00:18:15,280 --> 00:18:24,340
 ability to choose the sort of mental activity involved with

246
00:18:24,340 --> 00:18:28,220
 choosing your own food and thinking about what food you're

247
00:18:28,220 --> 00:18:29,000
 going to eat.

248
00:18:29,000 --> 00:18:34,000
 To some extent obsessing over the health and so on.

249
00:18:34,000 --> 00:18:38,410
 There's something great about just having to eat whatever

250
00:18:38,410 --> 00:18:40,000
 is being offered.

251
00:18:40,000 --> 00:18:45,430
 And moreover, sort of the scraps that you get by going

252
00:18:45,430 --> 00:18:47,000
 through the village.

253
00:18:47,000 --> 00:18:49,000
 That's the greatest.

254
00:18:49,000 --> 00:18:52,840
 So that's sort of the model is for monks to go walking

255
00:18:52,840 --> 00:18:56,000
 through the village for scraps of alms.

256
00:18:56,000 --> 00:19:01,000
 Whenever people are willing to give us charity.

257
00:19:01,000 --> 00:19:05,840
 Because in India it was a big thing to give food to reck

258
00:19:05,840 --> 00:19:10,000
lessness, to religious people.

259
00:19:10,000 --> 00:19:14,000
 So they would get enough to survive and that was it.

260
00:19:14,000 --> 00:19:17,370
 You avoid a lot of the trouble of having to cook food and

261
00:19:17,370 --> 00:19:23,580
 the potential distraction involved in storing and cooking

262
00:19:23,580 --> 00:19:28,000
 and having a kitchen in the first place.

263
00:19:28,000 --> 00:19:31,000
 So monks aren't allowed to.

264
00:19:31,000 --> 00:19:35,000
 Being a monk is on a different level.

265
00:19:35,000 --> 00:19:38,000
 Of course for meditators cooking your own food is fine.

266
00:19:38,000 --> 00:19:41,000
 There's just a sense that it's an unwarranted distraction.

267
00:19:41,000 --> 00:19:44,400
 If you're able to be free from that you can focus more on

268
00:19:44,400 --> 00:19:46,000
 the deep meditation.

269
00:19:46,000 --> 00:19:48,000
 It's too easy to get distracted.

270
00:19:48,000 --> 00:19:50,000
 Sure you can be mindful cooking.

271
00:19:50,000 --> 00:19:52,000
 It's great.

272
00:19:52,000 --> 00:19:54,570
 But during a meditation course much better not to have to

273
00:19:54,570 --> 00:19:55,000
 cook.

274
00:19:55,000 --> 00:19:58,000
 It's too much of a distraction.

275
00:19:58,000 --> 00:20:01,000
 Especially for a beginner meditator.

276
00:20:01,000 --> 00:20:12,000
 Lots of things the Buddha said don't get involved in.

277
00:20:12,000 --> 00:20:14,000
 Don't get caught up in work.

278
00:20:14,000 --> 00:20:17,000
 Don't get caught up in sleep.

279
00:20:17,000 --> 00:20:22,000
 Don't get caught up in talk.

280
00:20:22,000 --> 00:20:30,000
 Don't get caught up in socializing.

281
00:20:30,000 --> 00:20:35,000
 Don't get caught up in eating.

282
00:20:35,000 --> 00:20:46,000
 These things get in the way of your practice.

283
00:20:46,000 --> 00:20:48,000
 Alright.

284
00:20:48,000 --> 00:20:53,190
 So unfortunately I don't think our quote was all that pithy

285
00:20:53,190 --> 00:20:54,000
 tonight.

286
00:20:54,000 --> 00:20:58,000
 And a few questions.

287
00:20:58,000 --> 00:21:00,000
 It doesn't look like there's any more.

288
00:21:00,000 --> 00:21:04,000
 If you have any more questions I'll be here again tomorrow.

289
00:21:04,000 --> 00:21:09,000
 I'm broadcasting a little bit every day.

290
00:21:09,000 --> 00:21:12,000
 So thank you all for tuning in.

291
00:21:12,000 --> 00:21:15,000
 Keep up the good work.

