1
00:00:00,000 --> 00:00:05,680
 Hi, and welcome back to Ask a Monk.

2
00:00:05,680 --> 00:00:09,320
 Today's question comes from Johnny from Chicago.

3
00:00:09,320 --> 00:00:11,920
 My parents aren't open to me becoming a Buddhist.

4
00:00:11,920 --> 00:00:14,580
 I haven't talked to them because I'm nervous they will not

5
00:00:14,580 --> 00:00:15,920
 accept my new religion.

6
00:00:15,920 --> 00:00:19,000
 What are some tips to have them see eye to eye with me?

7
00:00:19,000 --> 00:00:22,150
 Well, as I mentioned in a recent video, the first thing to

8
00:00:22,150 --> 00:00:23,720
 say is that being a Buddhist

9
00:00:23,720 --> 00:00:30,440
 is not really important.

10
00:00:30,440 --> 00:00:35,770
 In one way, being a good Buddhist is not about being

11
00:00:35,770 --> 00:00:38,320
 Buddhist, meaning the label that you

12
00:00:38,320 --> 00:00:40,100
 give to it isn't so important.

13
00:00:40,100 --> 00:00:43,110
 It's not necessary that you tell your parents that you're

14
00:00:43,110 --> 00:00:44,680
 Buddhist, at least not in the

15
00:00:44,680 --> 00:00:47,200
 beginning.

16
00:00:47,200 --> 00:00:49,550
 It's not even necessary that you identify yourself as being

17
00:00:49,550 --> 00:00:50,120
 Buddhist.

18
00:00:50,120 --> 00:00:53,520
 You can think of yourself as a follower of the Buddha, just

19
00:00:53,520 --> 00:00:54,820
 like someone who follows

20
00:00:54,820 --> 00:00:58,540
 the teachings of Jung or of Einstein or so on is not

21
00:00:58,540 --> 00:01:01,200
 considered to be following their

22
00:01:01,200 --> 00:01:03,740
 religion but simply their teachings.

23
00:01:03,740 --> 00:01:08,150
 The best way to help people come to see eye to eye with you

24
00:01:08,150 --> 00:01:10,260
 in regards to things like

25
00:01:10,260 --> 00:01:15,200
 Buddhism is to live the teachings and have them see your

26
00:01:15,200 --> 00:01:17,560
 example, to really show them

27
00:01:17,560 --> 00:01:21,720
 the benefits.

28
00:01:21,720 --> 00:01:23,600
 People like your parents are not seeing eye to eye with you

29
00:01:23,600 --> 00:01:23,720
.

30
00:01:23,720 --> 00:01:28,410
 It generally means that you're not presenting a positive

31
00:01:28,410 --> 00:01:30,600
 side to the Buddhist teaching.

32
00:01:30,600 --> 00:01:33,470
 You're not yet at the stage where you can give them

33
00:01:33,470 --> 00:01:35,640
 confidence in what you're doing.

34
00:01:35,640 --> 00:01:39,520
 I know for the longest time my parents really thought I had

35
00:01:39,520 --> 00:01:41,400
 joined a cult or I was being

36
00:01:41,400 --> 00:01:46,230
 brainwashed or was just going crazy because it was really

37
00:01:46,230 --> 00:01:47,360
 difficult.

38
00:01:47,360 --> 00:01:50,550
 It was a struggle in the beginning to assimilate this

39
00:01:50,550 --> 00:01:52,760
 incredibly different teaching.

40
00:01:52,760 --> 00:01:55,010
 As I became more comfortable in it and as I was able to

41
00:01:55,010 --> 00:01:56,240
 show them that I was really

42
00:01:56,240 --> 00:02:00,310
 doing good things for myself and for the people around me

43
00:02:00,310 --> 00:02:02,440
 and that I was really able to do

44
00:02:02,440 --> 00:02:06,500
 something positive and what I was doing was actually a

45
00:02:06,500 --> 00:02:07,640
 really good thing.

46
00:02:07,640 --> 00:02:10,200
 The whole thing about being Buddhist wasn't really so

47
00:02:10,200 --> 00:02:10,880
 important.

48
00:02:10,880 --> 00:02:13,570
 On the other hand, there are certain people in my family

49
00:02:13,570 --> 00:02:15,040
 who I think will never accept

50
00:02:15,040 --> 00:02:18,710
 what I'm doing and simply because of the label and because

51
00:02:18,710 --> 00:02:21,040
 I'm obviously an ordained Buddhist

52
00:02:21,040 --> 00:02:24,040
 monk.

53
00:02:24,040 --> 00:02:30,370
 I guess that being said, you can get away with just living

54
00:02:30,370 --> 00:02:32,820
 the teachings especially

55
00:02:32,820 --> 00:02:35,850
 so that people like your parents will be able to accept you

56
00:02:35,850 --> 00:02:37,320
 without you having to tell them

57
00:02:37,320 --> 00:02:38,800
 that you're Buddhist.

58
00:02:38,800 --> 00:02:41,020
 In the end, you do have to go on your own.

59
00:02:41,020 --> 00:02:43,380
 You do have to go your own way and have to accept that

60
00:02:43,380 --> 00:02:45,040
 there are certain people who just

61
00:02:45,040 --> 00:02:49,320
 aren't going to see eye to eye with you.

62
00:02:49,320 --> 00:02:51,360
 I guess in the end, that's the most important.

63
00:02:51,360 --> 00:02:54,060
 The most important is that you follow the right path,

64
00:02:54,060 --> 00:02:55,240
 follow what you understand to

65
00:02:55,240 --> 00:02:58,900
 be the path to peace, happiness, and freedom from suffering

66
00:02:58,900 --> 00:03:00,440
 and the path to becoming a

67
00:03:00,440 --> 00:03:01,520
 better person.

68
00:03:01,520 --> 00:03:03,870
 If other people can't see it, well, maybe they're not

69
00:03:03,870 --> 00:03:05,620
 interested in bettering themselves

70
00:03:05,620 --> 00:03:07,880
 or becoming a better person.

71
00:03:07,880 --> 00:03:11,030
 Many people in this world have a lot of things that are

72
00:03:11,030 --> 00:03:13,540
 stopping them from developing, things

73
00:03:13,540 --> 00:03:17,800
 like views and attachments and their ideas and opinions of

74
00:03:17,800 --> 00:03:19,880
 the way things should be that

75
00:03:19,880 --> 00:03:25,130
 really lead them down a sort of a dead end and make it very

76
00:03:25,130 --> 00:03:27,640
 difficult for them to accept

77
00:03:27,640 --> 00:03:31,200
 a path which might go in a different direction.

78
00:03:31,200 --> 00:03:35,030
 Okay, so first of all, don't worry so much about being

79
00:03:35,030 --> 00:03:36,040
 Buddhist.

80
00:03:36,040 --> 00:03:38,920
 Just practice it and try to talk to them about the things

81
00:03:38,920 --> 00:03:41,040
 which you've gained from the practice

82
00:03:41,040 --> 00:03:44,340
 and show to them and make it clear to them that what you're

83
00:03:44,340 --> 00:03:46,160
 doing is a benefit of benefit

84
00:03:46,160 --> 00:03:47,160
 to you and to other people.

85
00:03:47,160 --> 00:03:50,180
 And not worry so much about what they think of it, just

86
00:03:50,180 --> 00:03:52,040
 show them that it's a good thing.

87
00:03:52,040 --> 00:03:54,920
 And second, kind of let go and don't worry so much about

88
00:03:54,920 --> 00:03:56,800
 other people seeing eye to eye

89
00:03:56,800 --> 00:03:58,040
 with you.

90
00:03:58,040 --> 00:04:00,130
 Once you can at least show them that it's what you want to

91
00:04:00,130 --> 00:04:01,400
 do and it's what is right for

92
00:04:01,400 --> 00:04:05,750
 you then that you've done the best you can do and it's up

93
00:04:05,750 --> 00:04:07,960
 to them to go the next, to

94
00:04:07,960 --> 00:04:11,720
 take the step towards accepting the thing which you're

95
00:04:11,720 --> 00:04:12,440
 doing.

96
00:04:12,440 --> 00:04:14,000
 Okay, so I hope that helped.

97
00:04:14,000 --> 00:04:15,000
 Keep the questions coming.

98
00:04:15,000 --> 00:04:18,240
 If you want to post, you can now post on my channel page.

99
00:04:18,240 --> 00:04:21,280
 So that's uttadamo, youtube.com/uttadamo.

100
00:04:21,280 --> 00:04:26,180
 Okay, just post directly there and I'll respond to them as

101
00:04:26,180 --> 00:04:27,320
 I get time.

102
00:04:27,320 --> 00:04:28,320
 Okay, thanks.

