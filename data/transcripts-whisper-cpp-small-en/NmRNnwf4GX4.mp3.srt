1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:12,290
 Today we continue on with verses 131 and 132, which read as

3
00:00:12,290 --> 00:00:13,000
 follows.

4
00:00:15,000 --> 00:00:20,000
 "Sukka kamani bhudani yodandena vihingsati"

5
00:00:20,000 --> 00:00:27,000
 "Ata no sukame sa no pejaso nalabhate yisukkam"

6
00:00:27,000 --> 00:00:33,000
 "Sukka kamani bhudani yodandena nahingsati"

7
00:00:33,000 --> 00:00:40,000
 "Ata no sukame sa no pejaso labhate yisukkam"

8
00:00:40,000 --> 00:00:45,990
 It's a twin pair of verses, they're saying the same thing

9
00:00:45,990 --> 00:00:49,000
 but in opposition.

10
00:00:49,000 --> 00:00:55,000
 So 131 means "Sukka kamani bhudani"

11
00:00:55,000 --> 00:00:59,000
 "Those beings that desire happiness"

12
00:00:59,000 --> 00:01:02,000
 "Yodandena vihingsati"

13
00:01:02,000 --> 00:01:08,000
 "Who harms them with a stick or with a weapon"

14
00:01:08,000 --> 00:01:14,000
 "Ata no sukame sa no pejaso nalabhate yisukkam"

15
00:01:14,000 --> 00:01:27,000
 "By one who is seeking for happiness"

16
00:01:27,000 --> 00:01:29,000
 Meaning the person doing the harming.

17
00:01:29,000 --> 00:01:33,000
 So a person harming someone who wishes for happiness,

18
00:01:33,000 --> 00:01:37,000
 harming beings that wish for happiness.

19
00:01:37,000 --> 00:01:40,000
 They themselves seeking happiness.

20
00:01:40,000 --> 00:01:44,000
 "Pejaso nalabhate yisukkam"

21
00:01:44,000 --> 00:01:50,000
 In the future they do not attain happiness.

22
00:01:50,000 --> 00:01:53,000
 And 132 is the opposite too.

23
00:01:53,000 --> 00:01:55,000
 Someone does not harm others with a stick,

24
00:01:55,000 --> 00:01:58,000
 the other beings that seek happiness,

25
00:01:58,000 --> 00:02:02,000
 that wish for happiness, want happiness.

26
00:02:02,000 --> 00:02:10,900
 When self seeking happiness, one does find happiness in the

27
00:02:10,900 --> 00:02:13,000
 future.

28
00:02:13,000 --> 00:02:18,000
 So this is the Dhamma for today.

29
00:02:18,000 --> 00:02:22,000
 The story behind these verses,

30
00:02:22,000 --> 00:02:29,120
 it's regarding a group of boys who the Buddha saw on his

31
00:02:29,120 --> 00:02:30,000
 way to Savatthi.

32
00:02:30,000 --> 00:02:35,000
 Dhamma lived in Jethavana for many years of his life.

33
00:02:35,000 --> 00:02:40,000
 You can still go and see the ruins of the ancient monastery

34
00:02:40,000 --> 00:02:42,000
 in India.

35
00:02:42,000 --> 00:02:46,000
 And you can walk from, it's actually quite a ways,

36
00:02:46,000 --> 00:02:50,000
 but you can walk from Jethavana to Savatthi.

37
00:02:50,000 --> 00:02:58,000
 And in Savatthi there are two kind of pyramid type things,

38
00:02:58,000 --> 00:03:00,000
 maybe they were round at one point,

39
00:03:00,000 --> 00:03:05,000
 but they're kind of square, kind of pyramid type things,

40
00:03:05,000 --> 00:03:11,660
 that are said to be the monuments to Anguly Mala and Atap

41
00:03:11,660 --> 00:03:13,000
indika.

42
00:03:13,000 --> 00:03:18,740
 And if you go up on top of the stupa, you can see the walls

43
00:03:18,740 --> 00:03:20,000
 of Savatthi.

44
00:03:20,000 --> 00:03:25,000
 You can see the ruined city.

45
00:03:25,000 --> 00:03:28,000
 And so when the Buddha was on his way into Savatthi,

46
00:03:28,000 --> 00:03:35,000
 he saw a bunch of boys beating a snake with a stick.

47
00:03:35,000 --> 00:03:40,000
 It's an interesting, sort of a humanizing story

48
00:03:40,000 --> 00:03:43,000
 where the Buddha meets with a bunch of children,

49
00:03:43,000 --> 00:03:46,000
 and how he teaches children.

50
00:03:46,000 --> 00:03:49,000
 Of course this is the commentary again.

51
00:03:49,000 --> 00:03:52,000
 We don't have any evidence that this actually happened,

52
00:03:52,000 --> 00:03:56,000
 but it's an interesting look into how the Buddha behaved,

53
00:03:56,000 --> 00:04:01,630
 or how people remember him, the things people remember him

54
00:04:01,630 --> 00:04:03,000
 to have done.

55
00:04:03,000 --> 00:04:08,000
 So he asked them, "What are you doing?"

56
00:04:08,000 --> 00:04:14,000
 Kumarakā, young men, king Garodha, what are you doing?

57
00:04:14,000 --> 00:04:22,000
 Ahing bhante dandekena bharama, it's a snake Venerable Sir,

58
00:04:22,000 --> 00:04:27,000
 we are beating it with a stick.

59
00:04:27,000 --> 00:04:35,000
 And the Buddha said, "King Garuna, for what reason?"

60
00:04:35,000 --> 00:04:40,000
 And they said, "Dang sana bhayena bhante,

61
00:04:40,000 --> 00:04:46,000
 we are afraid of its bite, Venerable Sir."

62
00:04:46,000 --> 00:04:48,730
 And the Buddha shook his head, and I don't know if he shook

63
00:04:48,730 --> 00:04:50,000
 his head, he said,

64
00:04:50,000 --> 00:04:54,000
 "Dum mei, you think to yourself,

65
00:04:54,000 --> 00:04:58,000
 dum mei, you think to yourself, ata no sukang karizam,

66
00:04:58,000 --> 00:05:01,660
 we will make happiness for ourselves, we will bring

67
00:05:01,660 --> 00:05:03,000
 ourselves happiness."

68
00:05:03,000 --> 00:05:09,000
 And for that reason, imam pāranta, you beat this stick,

69
00:05:09,000 --> 00:05:14,000
 you beat this snake with a stick.

70
00:05:14,000 --> 00:05:18,000
 But he said, "You will not be nibata, nibata tāne,

71
00:05:18,000 --> 00:05:24,000
 in whatever places you are born, sukalābhinon nabhavizati,

72
00:05:24,000 --> 00:05:28,000
 you will not be one suhateena happiness."

73
00:05:28,000 --> 00:05:40,000
 Ata no sukampatteena pārāṁ pārītum nabhavatteena.

74
00:05:40,000 --> 00:05:48,000
 One is not able to...

75
00:05:48,000 --> 00:05:53,000
 The beating with the stick does not go for...

76
00:05:53,000 --> 00:06:00,000
 I don't quite get that, but basically,

77
00:06:00,000 --> 00:06:05,000
 beating others doesn't lead to happiness.

78
00:06:05,000 --> 00:06:10,000
 And so he taught this verse.

79
00:06:10,000 --> 00:06:17,930
 So it's simple, I mean, it ties in a lot with the past two

80
00:06:17,930 --> 00:06:20,000
 stories,

81
00:06:20,000 --> 00:06:25,000
 in the past two verses about one should not hurt others,

82
00:06:25,000 --> 00:06:29,000
 oneself not wishing harm.

83
00:06:29,000 --> 00:06:33,000
 But the curious thing here is the logic of the boy is,

84
00:06:33,000 --> 00:06:35,000
 because it seems like sound logic, right?

85
00:06:35,000 --> 00:06:39,970
 We are afraid he'll bite us, we're afraid of the bite of

86
00:06:39,970 --> 00:06:42,000
 the snake.

87
00:06:42,000 --> 00:06:44,000
 But of course, if you think for a moment about it,

88
00:06:44,000 --> 00:06:49,160
 the snake wasn't actually... most likely wasn't actually a

89
00:06:49,160 --> 00:06:52,000
 danger to the boy.

90
00:06:52,000 --> 00:06:54,000
 They just found the snake and they thought,

91
00:06:54,000 --> 00:06:58,000
 "Oh, he might bite us, so we better kill it."

92
00:06:58,000 --> 00:07:00,000
 But the snake wasn't looking to bite a human,

93
00:07:00,000 --> 00:07:04,650
 the snake was probably looking for a smaller animal to kill

94
00:07:04,650 --> 00:07:05,000
.

95
00:07:05,000 --> 00:07:12,000
 Not to say snakes aren't vicious, evil animals, but...

96
00:07:12,000 --> 00:07:18,700
 I mean, we do recognize that as at least a small evil, fair

97
00:07:18,700 --> 00:07:19,000
 evil,

98
00:07:19,000 --> 00:07:24,000
 snakes killing other animals and biting them.

99
00:07:24,000 --> 00:07:28,000
 But he wasn't doing harm to the boys.

100
00:07:28,000 --> 00:07:36,080
 And so it's a good example of our tendency to focus more on

101
00:07:36,080 --> 00:07:37,000
 a cure,

102
00:07:37,000 --> 00:07:43,450
 focus more on the cure to a problem than the cause of a

103
00:07:43,450 --> 00:07:45,000
 problem.

104
00:07:45,000 --> 00:07:51,000
 So it's a good example of that, but it's an important

105
00:07:51,000 --> 00:07:52,000
 delusion

106
00:07:52,000 --> 00:07:56,000
 or error that we make.

107
00:07:56,000 --> 00:08:00,720
 It was a problem that we have that leads to errors and

108
00:08:00,720 --> 00:08:02,000
 judgment.

109
00:08:02,000 --> 00:08:04,000
 So take these boys, for example.

110
00:08:04,000 --> 00:08:08,000
 They think the problem is this...

111
00:08:08,000 --> 00:08:11,000
 they think the cause of their problem is the snake, right?

112
00:08:11,000 --> 00:08:13,000
 The snake is causing problems.

113
00:08:13,000 --> 00:08:17,000
 And so immediately they look for a solution.

114
00:08:17,000 --> 00:08:23,540
 They don't consider and they don't look closely at the

115
00:08:23,540 --> 00:08:24,000
 cause,

116
00:08:24,000 --> 00:08:26,000
 at what the real problem is.

117
00:08:26,000 --> 00:08:29,420
 So they focus on the solution and immediately beat the

118
00:08:29,420 --> 00:08:30,000
 snake.

119
00:08:30,000 --> 00:08:33,000
 "If I get rid of the snake, then my problem will be over."

120
00:08:33,000 --> 00:08:36,000
 And understood the problem.

121
00:08:36,000 --> 00:08:38,480
 And in fact, the real problem is not the snake, it's the

122
00:08:38,480 --> 00:08:39,000
 fear.

123
00:08:39,000 --> 00:08:41,000
 That's what they say, "We're afraid he will bite us."

124
00:08:41,000 --> 00:08:43,650
 And so without investigating and seeing what is the real

125
00:08:43,650 --> 00:08:44,000
 problem,

126
00:08:44,000 --> 00:08:46,000
 whereas an adult would look at it and would say,

127
00:08:46,000 --> 00:08:49,420
 "Well, don't be afraid of the snake. It's not going to harm

128
00:08:49,420 --> 00:08:50,000
 you."

129
00:08:50,000 --> 00:08:54,000
 There's no reason to be afraid of it.

130
00:08:54,000 --> 00:08:57,000
 But we do this with so many different things.

131
00:08:57,000 --> 00:09:05,000
 You know, take the status of mental illness in the West.

132
00:09:05,000 --> 00:09:09,000
 As soon as we have a problem like depression or anxiety,

133
00:09:09,000 --> 00:09:13,270
 we're very quick to find a pill that will give us a

134
00:09:13,270 --> 00:09:14,000
 solution

135
00:09:14,000 --> 00:09:17,000
 without looking at the cause,

136
00:09:17,000 --> 00:09:22,000
 without really looking and seeing what's the problem.

137
00:09:22,000 --> 00:09:27,000
 "I think I can't cope. I can't handle."

138
00:09:27,000 --> 00:09:29,790
 And when in fact the experiences that we have aren't the

139
00:09:29,790 --> 00:09:30,000
 problem,

140
00:09:30,000 --> 00:09:34,000
 it's our reactions to them.

141
00:09:34,000 --> 00:09:37,000
 And the doctor will tell you, of course, that if you want

142
00:09:37,000 --> 00:09:38,000
 to cure a sickness,

143
00:09:38,000 --> 00:09:40,000
 you have to find the cause.

144
00:09:40,000 --> 00:09:41,000
 You have to look at the cause.

145
00:09:41,000 --> 00:09:43,000
 Well, the same should go with the mind.

146
00:09:43,000 --> 00:09:50,000
 The same should go with our addictions and our aversions,

147
00:09:50,000 --> 00:09:54,000
 and our mental problems.

148
00:09:54,000 --> 00:09:56,000
 Because when you look at what's causing you problems,

149
00:09:56,000 --> 00:10:01,000
 when you examine it, you find that really the answer is not

150
00:10:01,000 --> 00:10:02,000
 in finding a cure.

151
00:10:02,000 --> 00:10:06,000
 The cure is not in fixing things.

152
00:10:06,000 --> 00:10:09,000
 The cure is learning to see things differently,

153
00:10:09,000 --> 00:10:15,000
 learning to let go and release yourself

154
00:10:15,000 --> 00:10:28,500
 from the very idea that there's a problem in the first

155
00:10:28,500 --> 00:10:31,000
 place.

156
00:10:31,000 --> 00:10:39,000
 And so, I mean, the real deal is our...

157
00:10:39,000 --> 00:10:45,000
 I mean, the real problem is not that we seek for a cure.

158
00:10:45,000 --> 00:10:46,000
 It's that we don't understand.

159
00:10:46,000 --> 00:10:48,000
 So we don't understand the problem,

160
00:10:48,000 --> 00:10:53,000
 and we're inconsistent in our solution.

161
00:10:53,000 --> 00:10:59,090
 So some of our ideas might be good, some of our ideas might

162
00:10:59,090 --> 00:11:00,000
 be bad.

163
00:11:00,000 --> 00:11:06,790
 We're unable to be consistently successful in solving our

164
00:11:06,790 --> 00:11:08,000
 problems

165
00:11:08,000 --> 00:11:10,000
 because we don't look closely at the cause.

166
00:11:10,000 --> 00:11:14,910
 So it's just this idea that in Buddhism, we do focus much

167
00:11:14,910 --> 00:11:16,000
 more on the cause.

168
00:11:16,000 --> 00:11:19,000
 We're not immediately looking to solve our problems.

169
00:11:19,000 --> 00:11:22,160
 And that's sort of the difference between samatha

170
00:11:22,160 --> 00:11:24,000
 meditation and vipassana meditation.

171
00:11:24,000 --> 00:11:27,550
 Samatha meditation, you're looking for a quick fix, doesn't

172
00:11:27,550 --> 00:11:29,000
 solve the problem.

173
00:11:29,000 --> 00:11:34,500
 With vipassana meditation, you put aside the cure and the

174
00:11:34,500 --> 00:11:35,000
 fix

175
00:11:35,000 --> 00:11:38,000
 in terms of being happy and being peaceful.

176
00:11:38,000 --> 00:11:41,650
 And in fact, insight meditation can be quite unpleasant and

177
00:11:41,650 --> 00:11:43,000
 chaotic,

178
00:11:43,000 --> 00:11:46,000
 stressful even.

179
00:11:46,000 --> 00:11:50,250
 Yes, you're putting aside in favor of looking at the

180
00:11:50,250 --> 00:11:51,000
 problem.

181
00:11:51,000 --> 00:11:53,000
 When you have pain, you look at the pain.

182
00:11:53,000 --> 00:11:58,000
 Well, that's not pleasant.

183
00:11:58,000 --> 00:12:03,000
 But the focus on the cause instead of the cure

184
00:12:03,000 --> 00:12:06,580
 means that you're able to solve problems, to come up with

185
00:12:06,580 --> 00:12:07,000
 solutions

186
00:12:07,000 --> 00:12:12,000
 that are actually beneficial.

187
00:12:12,000 --> 00:12:14,000
 I mean, these boys were looking to be happy.

188
00:12:14,000 --> 00:12:17,000
 The Buddha points this out quite clearly.

189
00:12:17,000 --> 00:12:21,000
 Ata no sukham mei san no, they were seeking happiness.

190
00:12:21,000 --> 00:12:22,000
 That's why they were doing it.

191
00:12:22,000 --> 00:12:24,000
 They were doing it to bring happiness.

192
00:12:24,000 --> 00:12:27,000
 They had the right intention.

193
00:12:27,000 --> 00:12:28,000
 Their minds weren't clear.

194
00:12:28,000 --> 00:12:32,000
 They hadn't studied the problem and the situation

195
00:12:32,000 --> 00:12:35,040
 to the extent that they would really understand what the

196
00:12:35,040 --> 00:12:37,000
 problem was.

197
00:12:37,000 --> 00:12:40,000
 And the Buddha goes into, you know, to Buddha sees things

198
00:12:40,000 --> 00:12:40,000
 beyond

199
00:12:40,000 --> 00:12:42,000
 what most of us can see and says,

200
00:12:42,000 --> 00:12:45,000
 "Not only is it going to be a problem for your minds now,

201
00:12:45,000 --> 00:12:49,000
 when you're reborn, you're going to get beaten by a stick

202
00:12:49,000 --> 00:12:51,000
 or you're going to be in trouble.

203
00:12:51,000 --> 00:12:52,000
 You might go to hell."

204
00:12:52,000 --> 00:12:54,000
 That kind of thing.

205
00:12:54,000 --> 00:12:59,000
 Lots of bad things can happen.

206
00:12:59,000 --> 00:13:05,360
 So, for our meditation practice, we spend our time studying

207
00:13:05,360 --> 00:13:06,000
 causes.

208
00:13:06,000 --> 00:13:09,000
 We don't concern ourselves so much with the peace and the

209
00:13:09,000 --> 00:13:09,000
 happiness.

210
00:13:09,000 --> 00:13:14,000
 And that's really kind of the beauty of the practice.

211
00:13:14,000 --> 00:13:18,000
 We're not dependent on results.

212
00:13:18,000 --> 00:13:21,000
 It's much more about the process.

213
00:13:21,000 --> 00:13:24,400
 And when happiness does come, because it does come from

214
00:13:24,400 --> 00:13:25,000
 inside,

215
00:13:25,000 --> 00:13:28,000
 make no mistake, inside meditation is designed to bring

216
00:13:28,000 --> 00:13:28,000
 peace out.

217
00:13:28,000 --> 00:13:31,000
 And peace, happiness, freedom from suffering.

218
00:13:31,000 --> 00:13:34,000
 But when it does come, we're not attached to it.

219
00:13:34,000 --> 00:13:35,000
 We're not dependent on it.

220
00:13:35,000 --> 00:13:39,000
 We kind of just chalk it up to good results

221
00:13:39,000 --> 00:13:42,000
 and keep going back into the bad stuff.

222
00:13:42,000 --> 00:13:44,000
 It's like when you're cleaning house.

223
00:13:44,000 --> 00:13:47,000
 When the room's clean, you leave and go find a dirty room.

224
00:13:47,000 --> 00:13:49,000
 You do that until you're clean.

225
00:13:49,000 --> 00:13:51,000
 That's how you clean your house.

226
00:13:51,000 --> 00:13:53,000
 That's how you clean your mind.

227
00:13:53,000 --> 00:13:55,000
 Don't sit there and marvel at the clean room

228
00:13:55,000 --> 00:13:57,000
 and forget about all the rest.

229
00:13:57,000 --> 00:13:58,000
 Oh, well, at least this room's clean.

230
00:13:58,000 --> 00:14:00,000
 Maybe I'll just live in this room

231
00:14:00,000 --> 00:14:03,000
 and not live in the rest of the rooms in my house.

232
00:14:03,000 --> 00:14:06,000
 So in meditation, we focus on the bad stuff mostly.

233
00:14:06,000 --> 00:14:08,000
 It's important that we do.

234
00:14:08,000 --> 00:14:10,000
 I think that's proper to say.

235
00:14:10,000 --> 00:14:13,000
 Don't focus on the good stuff. Don't worry about it.

236
00:14:13,000 --> 00:14:17,210
 Focus on the bad stuff, including how we react to good

237
00:14:17,210 --> 00:14:18,000
 things.

238
00:14:18,000 --> 00:14:22,220
 It's not all pain and suffering, but there's greed and

239
00:14:22,220 --> 00:14:25,000
 addiction as well.

240
00:14:25,000 --> 00:14:28,000
 But on either side, focusing on what's wrong.

241
00:14:28,000 --> 00:14:31,520
 Just like a doctor would, just like someone cleaning house

242
00:14:31,520 --> 00:14:32,000
 would.

243
00:14:32,000 --> 00:14:36,000
 A doctor focuses on the sickness, tries to find the cause.

244
00:14:36,000 --> 00:14:45,000
 It can give you medication to suppress the symptoms

245
00:14:45,000 --> 00:14:47,000
 and even cure the sickness, you know?

246
00:14:47,000 --> 00:14:55,190
 Like if a person has abused their body, the doctor can fix

247
00:14:55,190 --> 00:14:56,000
 the symptoms,

248
00:14:56,000 --> 00:14:59,000
 but if you go back and abuse your body again,

249
00:14:59,000 --> 00:15:02,000
 what good is it to cure the symptoms?

250
00:15:02,000 --> 00:15:04,000
 Even if it's a cure?

251
00:15:04,000 --> 00:15:08,000
 You just do the same thing over again.

252
00:15:08,000 --> 00:15:11,000
 So that's more important, is it to find the cause?

253
00:15:11,000 --> 00:15:13,680
 And as I say, an ounce of prevention is worth a pound of

254
00:15:13,680 --> 00:15:14,000
 cure, right?

255
00:15:14,000 --> 00:15:20,000
 That's what I'm saying, for this reason.

256
00:15:20,000 --> 00:15:22,000
 So we're not so much concerned with results.

257
00:15:22,000 --> 00:15:24,000
 This is a problem meditators have.

258
00:15:24,000 --> 00:15:29,000
 They're worried that their meditation is not comfortable.

259
00:15:29,000 --> 00:15:32,000
 Is this meditation actually any good?

260
00:15:32,000 --> 00:15:34,360
 And where you should find results is in the fixing and the

261
00:15:34,360 --> 00:15:35,000
 solving.

262
00:15:35,000 --> 00:15:38,000
 Sorry, not in the fixing and the solving,

263
00:15:38,000 --> 00:15:44,740
 but in the moment of addressing, not the fixing, but the

264
00:15:44,740 --> 00:15:47,000
 addressing.

265
00:15:47,000 --> 00:15:54,000
 When you address the experience and purify the moment

266
00:15:54,000 --> 00:15:57,000
 where you look at the experience in a new way,

267
00:15:57,000 --> 00:16:00,000
 when you have pain, you look at it as just pain.

268
00:16:00,000 --> 00:16:04,000
 When you have thoughts, you look at them as just thoughts.

269
00:16:04,000 --> 00:16:06,000
 Even your emotions, learning to look at them

270
00:16:06,000 --> 00:16:11,000
 and see them just as emotions and not reacting to them.

271
00:16:11,000 --> 00:16:15,000
 So that's the Dhampada verses for today.

272
00:16:15,000 --> 00:16:18,000
 We're now at up to 132.

273
00:16:18,000 --> 00:16:21,000
 Wishing you all the best. Have a good night.

