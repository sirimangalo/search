 Okay, good evening.
 Tonight we're continuing our study of the Dhammapada.
 Today we look at verse 201, which goes as follows.
 Jaya yang wei rang passavati, duhkang seiti parajito, upas
anto sukang seiti, hitwa jaya
 parajayam.
 Which means,
 victory breeds enmity or vengeance, weirak.
 Those who are defeated,
 those who are not victorious dwell in suffering.
 Those who are tranquil or who have found peace,
 dwell in happiness,
 having abandoned victory and defeat.
 So this was told in regards, what we're told, it was in
 relation to Ajatasattu.
 Ajatasattu was a prince in the time of the Buddha and he
 fell in with the wrong crowd.
 He became friends with Devadatta who was out to destroy or
 take over the Buddhist congregation.
 So he teamed up with Ajatasattu and Ajatasattu.
 Devadatta suggested that he kill his father, who is the
 king, and he would be able to take over the kingdom.
 Devadatta would kill the Buddha and he would take over the,
 which is what you call it, the religion, I guess, religious
 organization.
 And they tried and Devadatta was unsuccessful, of course.
 Ajatasattu was eventually successful.
 He tried to kill his father.
 He couldn't bear to actually kill his father, so he threw
 him in the dungeon,
 had him tied to the wall or chained to the wall and
 had him starve to death, not bringing any food to him.
 The story goes that Bimbisara was his father,
 was so happy because his jail cell, he was able to see the
 monastery where the Buddha lived.
 And so he was able to survive for quite some time.
 That's a long story.
 In jail he would do walking meditation.
 And I can't remember, he was so happy that he wasn't even
 concerned about being in prison.
 Eventually he chained him to the wall.
 And the queen brought food in secretly and eventually he
 refused to allow the queen to see the king.
 Eventually King Bimbisara died.
 I'll make a long story short.
 But that's not this story.
 This story is about Ajatasattu after he killed the king.
 He became quite an imperialist or a colonialist trying to
 take over the rest of the world.
 And he fought with the king of Kosala, who I guess was Pas
enadi.
 And it says he was his uncle, so I'm not really sure where
 the connection is.
 But he fought with his uncle and he kept beating his uncle.
 And Pasenadi, the king of Kosala was so upset by being
 beaten by Ajatasattu that he refused to eat and he became
 very ill and very weak.
 And he was just tormented lying on his bed.
 It's actually quite a short story.
 But there's much background to it, the background of this
 prince killing his father, of Devadatta trying to kill the
 Buddha,
 of Bimbisara and his greatness at the moment of death.
 Ajatasattu tried to actually stop his father from starving
 to death.
 At the last moment he went to see his father but he was too
 late.
 His father had died.
 But the essence of this is about victory and defeat.
 What are the results of our perspective on the world?
 A person who strives constantly to defeat and to beat
 others.
 So on the surface it appears to just be good religious
 advice telling people not to fight with each other.
 When you fight, that's what we tell kids, when you fight
 you end up hurting others, it's not very nice.
 And you end up breeding enmity and vengeance and so on.
 But there's a deeper lesson here, and a lesson for us as
 meditators is that this perspective is one that leads to
 suffering.
 Dukkangsi di parajito
 Buddhism is very much about our perspective.
 We use the word "diti" a lot when we talk about our views.
 It really refers to the way we look at the world,
 the way we approach experience, the way we approach our
 experience.
 And it's similar to people who have ambition like Ajatasatt
u had ambition, Devadatta had ambition.
 Even simply approaching reality as problems that we have to
 fix.
 People who fall into this game of vengeance, of victory and
 defeat have a sense of what they call a zero-sum game.
 It means that in order for me to benefit, someone else has
 to suffer.
 And so in the end it equals out to zero. It's like a law of
 thermodynamics or something.
 In order to find happiness, it has to be at the expense of
 someone else.
 And so on the face of it, it seems practically or
 conventionally sort of a terrible outlook.
 And yet it is sort of a conventional outlook that people
 have, thieves and warlords.
 But also ordinary people have a sense of trying to find
 happiness through manipulating others, through harming
 others,
 through defeating others. It's something we fall into quite
 easily.
 We're making enemies. We identify people who are in our way
.
 But it's a deeper psychological issue. It's our outlook.
 And these actions affect our outlook and they spring from
 our outlook.
 Our outlook is a much more basic sense of trying to fix
 things, trying to force,
 trying to bring about resolution by sheer force of our disl
iking of the situation.
 It doesn't have to be a person, though people, other human
 beings become galvanizing icons of hatred.
 If you stub your toe, you might get angry at the table, but
 if someone hits you, you can develop a grudge for a
 lifetime.
 If someone says bad things, it becomes a galvanizing, very
 strengthening focal point for hatred.
 But ultimately it's the same habit. We don't like something
.
 And so we fall into this habit of reacting, trying to get
 rid of what it is that we don't like,
 and what it is that we don't like, and what it is that we
 don't like.
 It forces the bad conditions to become good.
 And so it leads directly to our understanding of the
 practice of mindfulness as an antidote for this sort of
 behavior.
 And sort of as an alternative, it shows quite clearly how
 mindfulness is different.
 Mindfulness is not like fixing problems. It's certainly not
 like achieving goals.
 And it's absolutely not like forcing or controlling reality
 or other people, especially, to try and be the way we want.
 Mindfulness is about experiencing things as they are.
 We talk about this and we explain mindfulness as just being
 aware of things as they are.
 But this sort of verse, this sort of teaching, this sort of
 example of the story of a jatasattu,
 this kind of story of people holding a grudge and fighting
 and even killing each other over their desire for some
 beneficial outcome.
 This kind of story shows how different it is to be mindful
 and it shows the contrast with what we're doing.
 Contrast would lead to when someone tries to harm you,
 to change your perception of the situation from trying to
 defend yourself and trying to keep yourself from
 experiencing suffering,
 being able to see the experience as just an experience and
 let it happen and let it go.
 Which is a very radical sort of approach. It shows how
 different this approach is from ordinary way of living.
 But the difference is quite apparent, quite profound as
 well, the difference in result.
 A person who engages frequently, regularly in hatred, of
 course, is going to find suffering.
 They're going to cultivate enemies and enmity.
 And they're going to suffer defeat and they're going to
 lead to situations where people want to hurt them,
 want to manipulate and take advantage of them.
 And so you see at a very deep level this sort of approach,
 this perspective of trying to force, trying to conquer.
 It has very real results. This should be quite obvious that
 this is the way things are.
 When we try to hurt others, it's unsustainable in terms of
 providing peace and happiness.
 Not only does it create enmity but it changes who you are.
 And so really it's this...
 The teaching is that there are two levels to reality.
 There's the conventional level and then there's I guess you
 could call the psychological level.
 And they work off of each other.
 So when we act and speak trying to harm others, it affects
 us psychologically.
 Our psychological makeup, how we relate to experience, how
 we perceive reality, is going to inform our decisions.
 It's going to affect the way we relate to other people, the
 way we relate to problems and situations in our lives.
 So when you take on a philosophy of non-harming that this
 verse sort of suggests,
 of not trying to conquer others, of not seeing situations
 as something to be conquered as a zero-sum game.
 Because of course the reality is not anything like where
 you defeat others and suddenly you're on top,
 you've won something. The reality is that it's a constant
 changing sort of flux where by sometimes you're winning and
 sometimes you're losing.
 And enmity breeds enmity. When you hurt others, you make
 them want to hurt you.
 So it's not a zero-sum game, it's an everybody loses sort
 of game.
 When you hurt others, you make others want to hurt you.
 And all of this affects our psychology, it affects our
 makeup.
 When you cease to do that, when you stop wanting to hurt
 others, when you allow other people their anger against you
 and you experience the results of bad blood without
 reacting,
 this affects your psychology, it affects your mental makeup
.
 It's a very sort of good practical teaching in terms of the
 direction we want to be heading,
 of trying to be more peaceful, happier, free from suffering
.
 But from the other end as well, when we hear about this,
 when we look at this teaching,
 abandoning victory and defeat, "Hitwa Jaya Parajiyam,"
 we see the deeper teaching of being mindful.
 And when you yourself approach reality in terms of
 experiences to be understood rather than problems to be
 fixed
 or challenges to be overcome or whatever, enemies to be
 conquered, absolutely.
 Pain being an enemy, even our defilements as being enemies
 is really a problematic way of looking at them.
 If you look at anger as an enemy or greed as an enemy, it
 takes on an adversarial sort of state of being
 and there's stress involved and you cultivate this habit of
 trying to control and force and change reality.
 But when you take up mindfulness, it's no longer just about
 our actions and our speech,
 but our whole approach to reality, that pain is not an
 enemy.
 Speech, harsh speech from other people is not something to
 be conquered, not something to be fought against.
 I don't deserve to hear that. I don't deserve to be spoken
 to that way.
 Instead, experiencing it as sound, experiencing it as sight
.
 Seeing reality is made up of experiences. This is what it
 truly means, I think, to give up victory and defeat
 because your whole way of looking at reality is no longer
 adversarial.
 You rise above, it might say, or you change the perspective
.
 There's an old saying, "If you're a hammer, everything
 looks like a nail."
 And it points out something, what it means is when your
 outlook is adversarial, you're constantly going to be
 trying to,
 you're going to react in a certain way.
 And it points out a very important reality that the world
 around us, our environment, our experience of the world,
 is very much dependent on our outlook.
 And so, I mean, an argument against this sort of passivity
 whereby you let other people harm you, for example,
 as it may happen, where you are not only nonviolent but you
're passive, you're not only peaceful but you're passive.
 The criticism might be that you're going to allow people to
 hurt you, you're going to be taken advantage of, for
 example.
 And so they would say, "Well, this is, if you look at it
 this way, it's not a very good way to live."
 But the point is that there's a deeper reality going on
 here. You're changing the way you look at things.
 A person who is harmed when someone attacks someone else or
 speaks violently, speaks angrily to someone else.
 The reality is still just experience.
 Another way of looking at this is rather than an advers
arial situation, looking at it as experiences of seeing,
 hearing, smelling, tasting, feeling, thinking. If you can
 see it like that,
 it doesn't change the situation where suddenly people are
 nice to you.
 It changes the perspective and reality is suddenly
 objective, meaning it doesn't matter whether you're being
 hugged or you're being attacked.
 It's a very radical, again, sort of outlook.
 And I think this exemplifies or this shows us clearly how
 radical it is to be mindful
 and the extent to which mindfulness changes our lives.
 It doesn't necessarily change our situation, so it can be
 quite uncomfortable at first.
 We don't have to even talk about other people who might
 want to hurt us, but our own body wants to hurt us.
 Because of our reactions and our emotions, we make the body
 quite sick and we can be quite tense.
 When you first come to practice meditation, it can be quite
 painful. Your own body can cause you great suffering.
 And if you look at that as an adversarial sort of thing,
 something to be fixed, something to be fought against,
 something to be controlled or tamed, you're just going to
 create more stress and suffering.
 You have a never-ending torment and it can be quite painful
 and quite unpleasant.
 So our way of looking at things when we're mindful is quite
 simple.
 It's not at all easy, but it's quite simple. It's about
 seeing things just as they are.
 This is why we repeat to ourselves, for example, pain, pain
.
 If you're happy, you would say happy, happy. Just trying to
 see things as they are without reacting.
 Trying to cultivate a new perspective, a new way of looking
 at things.
 That is not adversarial, not about conquering reality or
 lives or winning or anything, achieving any goal.
 But it's about cultivating a perspective of objectivity, of
 peace, that rises above experiences of victory or defeat.
 So someone might look at you and say, "Wow, that person is
 a real pushover. They're being defeated."
 And you won't even see it that way. In the end, you see it
 only as experience.
 It's quite powerful because the practical reality as
 someone who is mindful is often quite revered, respected.
 You'll find that people respect your opinion. They lose
 their desire to harm you.
 A person who is very mindful often finds that people who
 they thought were their enemies
 are suddenly able to come to terms and work towards a more
 peaceful resolution because the person has changed the
 parameters.
 A lot of our adversarial conflict, a lot of our conflict as
 humans depends on both sides continuing and perpetuating it
.
 Our anger perpetuates conflict. Our reactivity perpetuates
 it.
 So it's not something that you should be scared of or
 perhaps not see it as so radical because it's not as though
 you have to suddenly let people harm you.
 It's about changing the way we look at reality, changing
 our perspective, which also of course means changing our
 lives.
 Gradually, gradually, you come to live a more peaceful life
 where you work out all your problems.
 You start to look at your enemies as just people who have
 problems and your interactions with them rather than as
 fights to be won, their experiences to be understood.
 You rise above, you abandon victory and defeat. That's the
 verse here.
 So that's the Dhammapada, a very simple verse, but I think
 it ties in well with the understanding of what is
 mindfulness.
 So that's I think the lesson we take from it.
 Thank you all for listening. Have a good night.
 Thank you.
