1
00:00:00,000 --> 00:00:10,000
 [BLANK_AUDIO]

2
00:00:10,000 --> 00:00:12,000
 Welcome to our live podcast.

3
00:00:12,000 --> 00:00:20,000
 [BLANK_AUDIO]

4
00:00:20,000 --> 00:00:30,000
 [BLANK_AUDIO]

5
00:00:30,000 --> 00:00:40,000
 [BLANK_AUDIO]

6
00:00:58,000 --> 00:01:00,000
 >> Hey, Bunsen.

7
00:01:00,000 --> 00:01:02,000
 [BLANK_AUDIO]

8
00:01:02,000 --> 00:01:04,000
 >> You're not here yet.

9
00:01:04,000 --> 00:01:06,000
 [BLANK_AUDIO]

10
00:01:06,000 --> 00:01:08,000
 >> I got caught in a storm.

11
00:01:08,000 --> 00:01:10,000
 >> Hmm. >> Lost the trees down on the road.

12
00:01:10,000 --> 00:01:14,000
 >> Are you coming alone or is your husband anything?

13
00:01:14,000 --> 00:01:16,000
 [BLANK_AUDIO]

14
00:01:16,000 --> 00:01:19,000
 >> No, I'm alone this time. I'm working this week.

15
00:01:19,000 --> 00:01:29,000
 [BLANK_AUDIO]

16
00:01:29,000 --> 00:01:39,000
 [BLANK_AUDIO]

17
00:01:39,000 --> 00:01:55,000
 >> Using the wrong microphone.

18
00:01:55,000 --> 00:02:00,000
 >> Okay. So.

19
00:02:00,000 --> 00:02:03,000
 [BLANK_AUDIO]

20
00:02:03,000 --> 00:02:09,000
 Tonight's quote is about eating.

21
00:02:09,000 --> 00:02:13,000
 [BLANK_AUDIO]

22
00:02:13,000 --> 00:02:15,000
 No, it's about livelihood.

23
00:02:15,000 --> 00:02:17,000
 [BLANK_AUDIO]

24
00:02:17,000 --> 00:02:25,000
 We read at the end it's about how one makes one's living.

25
00:02:25,000 --> 00:02:28,410
 Specifically how religious people make their living, what

26
00:02:28,410 --> 00:02:33,000
 religious people focus on.

27
00:02:33,000 --> 00:02:40,000
 There are religious practices that are low.

28
00:02:40,000 --> 00:02:45,000
 Religious practices that are not worthy of one who is.

29
00:02:45,000 --> 00:02:51,000
 [BLANK_AUDIO]

30
00:02:51,000 --> 00:02:55,000
 One who is seeking to become free from suffering.

31
00:02:55,000 --> 00:02:58,000
 Not worthy of a Buddhist monk at any rate.

32
00:02:58,000 --> 00:03:06,000
 [BLANK_AUDIO]

33
00:03:06,000 --> 00:03:08,000
 So there's looking down and there's looking up.

34
00:03:08,000 --> 00:03:15,000
 Looking down is looking at the signs, that mundane signs.

35
00:03:15,000 --> 00:03:19,000
 Looking up is looking at divine signs.

36
00:03:19,000 --> 00:03:20,000
 Also not good.

37
00:03:20,000 --> 00:03:26,000
 [BLANK_AUDIO]

38
00:03:26,000 --> 00:03:34,000
 There are those who act as messengers and errand boys.

39
00:03:34,000 --> 00:03:38,000
 This is one thing that monks aren't allowed to do.

40
00:03:38,000 --> 00:03:40,000
 We're not allowed to run errands for people.

41
00:03:40,000 --> 00:03:45,000
 [BLANK_AUDIO]

42
00:03:45,000 --> 00:03:49,000
 Because there's, we are living up the charity of others.

43
00:03:49,000 --> 00:03:52,000
 [BLANK_AUDIO]

44
00:03:52,000 --> 00:04:00,000
 We have given up financial security and

45
00:04:00,000 --> 00:04:07,000
 solubility, that's the word.

46
00:04:07,000 --> 00:04:12,000
 [BLANK_AUDIO]

47
00:04:12,000 --> 00:04:15,000
 In favor of the homeless life.

48
00:04:15,000 --> 00:04:19,000
 And so we rely upon charity.

49
00:04:19,000 --> 00:04:21,000
 [BLANK_AUDIO]

50
00:04:21,000 --> 00:04:27,000
 Now if we start working for people.

51
00:04:27,000 --> 00:04:35,000
 Well first of all it's going against our, our determination

52
00:04:35,000 --> 00:04:37,000
 to be free from all that.

53
00:04:37,000 --> 00:04:44,000
 But it's also setting one up for sort of the obligation.

54
00:04:44,000 --> 00:04:47,000
 [BLANK_AUDIO]

55
00:04:47,000 --> 00:04:49,740
 So you ingratiate yourself with people by doing them err

56
00:04:49,740 --> 00:04:51,000
ands and

57
00:04:51,000 --> 00:04:57,680
 then they give you food and then they come to to think of

58
00:04:57,680 --> 00:04:59,000
 it in a different way.

59
00:04:59,000 --> 00:05:05,000
 Rather than being charity it's, it's wages.

60
00:05:05,000 --> 00:05:08,000
 The dynamic breaks down.

61
00:05:08,000 --> 00:05:12,000
 It's no longer a religious thing, it's now employment.

62
00:05:12,000 --> 00:05:20,000
 [BLANK_AUDIO]

63
00:05:20,000 --> 00:05:24,490
 Well as far as palmistry, I don't know how that works as

64
00:05:24,490 --> 00:05:26,000
 another category.

65
00:05:26,000 --> 00:05:28,000
 But that's looking at all.

66
00:05:28,000 --> 00:05:31,180
 So the, the going on errands is looking at the four

67
00:05:31,180 --> 00:05:32,000
 directions.

68
00:05:32,000 --> 00:05:34,000
 Even looking at the four directions.

69
00:05:34,000 --> 00:05:38,000
 It says a symbol, it says symbolism.

70
00:05:38,000 --> 00:05:43,000
 [BLANK_AUDIO]

71
00:05:43,000 --> 00:05:45,990
 Eating and eating looking in four directions is a metaphor

72
00:05:45,990 --> 00:05:47,000
 for

73
00:05:47,000 --> 00:05:54,560
 living religious life as a, as a go between or an errand

74
00:05:54,560 --> 00:05:55,000
 boy.

75
00:05:55,000 --> 00:05:58,000
 [BLANK_AUDIO]

76
00:05:58,000 --> 00:06:02,360
 And looking at the points between the metaphor for palm

77
00:06:02,360 --> 00:06:03,000
istry, etc.

78
00:06:03,000 --> 00:06:07,000
 [BLANK_AUDIO]

79
00:06:07,000 --> 00:06:11,000
 I guess that has to do with the, the body maybe.

80
00:06:11,000 --> 00:06:21,000
 [BLANK_AUDIO]

81
00:06:37,000 --> 00:06:40,910
 I seek my food rightly and rightly do I eat it after I've

82
00:06:40,910 --> 00:06:42,000
 sought it.

83
00:06:42,000 --> 00:06:46,000
 [BLANK_AUDIO]

84
00:06:46,000 --> 00:06:50,000
 Rightfully means without expectation.

85
00:06:50,000 --> 00:06:55,000
 [BLANK_AUDIO]

86
00:06:55,000 --> 00:07:00,000
 Without insinuation or ingratiation.

87
00:07:00,000 --> 00:07:04,000
 It's simply because people want to give.

88
00:07:04,000 --> 00:07:07,000
 [BLANK_AUDIO]

89
00:07:07,000 --> 00:07:14,220
 And in return leading life that is pure and setting an

90
00:07:14,220 --> 00:07:15,000
 example and

91
00:07:15,000 --> 00:07:19,000
 offering teaching, religious teaching.

92
00:07:19,000 --> 00:07:23,000
 Such as the one thing that monks and religious people

93
00:07:23,000 --> 00:07:24,000
 should do for

94
00:07:24,000 --> 00:07:26,000
 lay people.

95
00:07:26,000 --> 00:07:30,000
 Let's offer them teachings that allow them to become more

96
00:07:30,000 --> 00:07:31,000
 religious and

97
00:07:31,000 --> 00:07:34,000
 more spiritual.

98
00:07:34,000 --> 00:07:37,000
 Offering them apart in the spiritual life.

99
00:07:37,000 --> 00:07:49,000
 [BLANK_AUDIO]

100
00:07:49,000 --> 00:07:50,000
 Hmm.

101
00:07:50,000 --> 00:07:54,000
 [BLANK_AUDIO]

102
00:07:54,000 --> 00:07:57,000
 Sort of an interesting quote from an Aztecs but not much

103
00:07:57,000 --> 00:07:57,000
 else.

104
00:07:57,000 --> 00:08:04,000
 [BLANK_AUDIO]

105
00:08:04,000 --> 00:08:06,000
 We have questions today.

106
00:08:06,000 --> 00:08:11,000
 [BLANK_AUDIO]

107
00:08:11,000 --> 00:08:13,000
 Let's look at some questions.

108
00:08:13,000 --> 00:08:15,000
 [BLANK_AUDIO]

109
00:08:15,000 --> 00:08:17,000
 Robin disappeared because I'm on my way.

110
00:08:17,000 --> 00:08:25,000
 [BLANK_AUDIO]

111
00:08:25,000 --> 00:08:29,000
 It is, let's see how far back do I have to go?

112
00:08:29,000 --> 00:08:30,000
 [BLANK_AUDIO]

113
00:08:30,000 --> 00:08:32,000
 Some way back here.

114
00:08:32,000 --> 00:08:34,000
 [BLANK_AUDIO]

115
00:08:34,000 --> 00:08:37,000
 Too much chatting, we've lost all the questions.

116
00:08:37,000 --> 00:08:40,000
 Too much chatting, what do you think this is a chat box?

117
00:08:40,000 --> 00:08:43,000
 [BLANK_AUDIO]

118
00:08:43,000 --> 00:08:45,000
 Meditators, shouldn't be chatting.

119
00:08:45,000 --> 00:08:47,000
 [BLANK_AUDIO]

120
00:08:47,000 --> 00:08:50,930
 Some monks sleep in the floor on a bed made or a bed made

121
00:08:50,930 --> 00:08:52,000
 of rock.

122
00:08:52,000 --> 00:08:55,000
 Do you do the same?

123
00:08:55,000 --> 00:08:57,000
 [BLANK_AUDIO]

124
00:08:57,000 --> 00:09:00,000
 I sleep on the carpet, actually.

125
00:09:00,000 --> 00:09:01,000
 [BLANK_AUDIO]

126
00:09:01,000 --> 00:09:04,000
 Sleeping on a bed made of rock, hmm.

127
00:09:04,000 --> 00:09:07,000
 I don't know how healthy that would be.

128
00:09:07,000 --> 00:09:09,000
 [BLANK_AUDIO]

129
00:09:09,000 --> 00:09:11,000
 Actually, it would be quite unhealthy, I'm assuming.

130
00:09:11,000 --> 00:09:14,600
 Monks were supposed to sleep on simple mattresses, but matt

131
00:09:14,600 --> 00:09:16,000
resses were allowed.

132
00:09:16,000 --> 00:09:25,000
 [BLANK_AUDIO]

133
00:09:25,000 --> 00:09:31,000
 Sleeping on the floor is good, on a carpet or so on.

134
00:09:31,000 --> 00:09:33,000
 We don't sleep too much.

135
00:09:33,000 --> 00:09:37,000
 [BLANK_AUDIO]

136
00:09:37,000 --> 00:09:39,000
 And it said we should guard our mind.

137
00:09:39,000 --> 00:09:42,000
 What instance of us is used to guard the mind?

138
00:09:42,000 --> 00:09:45,000
 Is it the mind itself, the consciousness?

139
00:09:45,000 --> 00:09:48,000
 Yes, the mind is what guards the mind.

140
00:09:48,000 --> 00:09:56,000
 [BLANK_AUDIO]

141
00:09:56,000 --> 00:10:00,000
 So better to meditate after waking up or after breakfast.

142
00:10:00,000 --> 00:10:04,740
 Well, the Visuddhi manga does mention, I think it's the Vis

143
00:10:04,740 --> 00:10:06,000
uddhi manga,

144
00:10:06,000 --> 00:10:15,450
 mentions that eating is useful for meditation, especially

145
00:10:15,450 --> 00:10:16,000
 in the morning,

146
00:10:16,000 --> 00:10:23,000
 because it's hard to focus and the stomach is not settled

147
00:10:23,000 --> 00:10:28,000
 until you've had some orange or oatmeal for breakfast,

148
00:10:28,000 --> 00:10:32,000
 you know, rice, orange or oatmeal, that kind of thing.

149
00:10:32,000 --> 00:10:39,000
 Yagu, it's called, it would be sort of a simple plain

150
00:10:39,000 --> 00:10:41,000
 breakfast

151
00:10:41,000 --> 00:10:44,000
 to calm the stomach.

152
00:10:44,000 --> 00:10:48,000
 So what part is that?

153
00:10:48,000 --> 00:10:51,000
 [BLANK_AUDIO]

154
00:10:51,000 --> 00:10:54,000
 I think it might be in the dutangas, I'm not sure.

155
00:10:54,000 --> 00:11:01,000
 [BLANK_AUDIO]

156
00:11:01,000 --> 00:11:04,000
 So eating first is useful.

157
00:11:04,000 --> 00:11:07,000
 [BLANK_AUDIO]

158
00:11:07,000 --> 00:11:11,240
 Are the hindrances viewed as hindering equanimity or hind

159
00:11:11,240 --> 00:11:12,000
ering concentration

160
00:11:12,000 --> 00:11:16,000
 or hindering any peaceful positive qualities of mind state?

161
00:11:16,000 --> 00:11:20,000
 I think specifically it's about hindering concentration.

162
00:11:20,000 --> 00:11:22,000
 It's a good question.

163
00:11:22,000 --> 00:11:24,000
 I mean, in general, they are hindrances.

164
00:11:24,000 --> 00:11:26,000
 They hinder everything.

165
00:11:26,000 --> 00:11:29,000
 They're in general bad for you.

166
00:11:29,000 --> 00:11:35,000
 But I think probably the orthodox answer would be

167
00:11:35,000 --> 00:11:39,400
 the word hindrance is used to indicate the hindering of

168
00:11:39,400 --> 00:11:42,000
 concentration.

169
00:11:42,000 --> 00:11:44,350
 That's the hindering of wisdom as well because

170
00:11:44,350 --> 00:11:47,000
 concentration leads to wisdom

171
00:11:47,000 --> 00:11:54,000
 or focus, you might say, rather than concentration.

172
00:11:54,000 --> 00:11:58,270
 But they hinder positive qualities of mind states in

173
00:11:58,270 --> 00:11:59,000
 general.

174
00:11:59,000 --> 00:12:03,000
 They'll even hinder success in life, in a mundane sense.

175
00:12:03,000 --> 00:12:06,840
 You can't succeed if you have these five or it's harder to

176
00:12:06,840 --> 00:12:08,000
 succeed with them.

177
00:12:08,000 --> 00:12:13,000
 [BLANK_AUDIO]

178
00:12:13,000 --> 00:12:19,000
 Nii does not mean nibana, sankha you've got it.

179
00:12:19,000 --> 00:12:20,000
 No, no, no, no.

180
00:12:20,000 --> 00:12:24,000
 Maybe that's a commentary answer.

181
00:12:24,000 --> 00:12:27,000
 Nii just means out or away.

182
00:12:27,000 --> 00:12:32,000
 Watarana means toward.

183
00:12:32,000 --> 00:12:34,000
 That's not the real etymology.

184
00:12:34,000 --> 00:12:37,000
 Maybe the commentary talks about that.

185
00:12:37,000 --> 00:12:40,000
 Give you the real etymology.

186
00:12:40,000 --> 00:12:49,000
 [BLANK_AUDIO]

187
00:12:49,000 --> 00:12:59,000
 [BLANK_AUDIO]

188
00:12:59,000 --> 00:13:04,000
 [BLANK_AUDIO]

189
00:13:04,000 --> 00:13:06,000
 Nii Watarana.

190
00:13:06,000 --> 00:13:08,000
 It comes from the verb nii wareti.

191
00:13:08,000 --> 00:13:09,000
 It's a verb.

192
00:13:09,000 --> 00:13:13,000
 It's nothing to do with nibana.

193
00:13:13,000 --> 00:13:19,000
 Let's look at the commentary.

194
00:13:19,000 --> 00:13:26,000
 [BLANK_AUDIO]

195
00:13:26,000 --> 00:13:28,000
 Nii Watarana.

196
00:13:28,000 --> 00:13:34,000
 [BLANK_AUDIO]

197
00:13:34,000 --> 00:13:37,000
 It's a simple word.

198
00:13:37,000 --> 00:13:39,000
 Nothing to do with nibana.

199
00:13:39,000 --> 00:13:42,000
 [BLANK_AUDIO]

200
00:13:42,000 --> 00:13:45,000
 Because nibana isn't about the nii, it's about the wana.

201
00:13:45,000 --> 00:13:47,000
 And it's a good example.

202
00:13:47,000 --> 00:13:57,010
 The nii at the beginning is a prefix meaning out or away or

203
00:13:57,010 --> 00:14:00,000
 out from, from kind of thing.

204
00:14:00,000 --> 00:14:11,000
 Near, near means without, not without or out of or away.

205
00:14:11,000 --> 00:14:14,000
 Ware means like toward.

206
00:14:14,000 --> 00:14:17,940
 It's probably the same as the English word, toward away

207
00:14:17,940 --> 00:14:19,000
 something.

208
00:14:19,000 --> 00:14:22,000
 So nivarana means toward away.

209
00:14:22,000 --> 00:14:27,600
 And a hindrance is something that keeps you away, keeps you

210
00:14:27,600 --> 00:14:29,000
 from.

211
00:14:29,000 --> 00:14:33,000
 Nii Watarana.

212
00:14:33,000 --> 00:14:41,190
 Nii Wana, nibana is away from or out of bondage, free from

213
00:14:41,190 --> 00:14:44,000
 bondage kind of thing.

214
00:14:44,000 --> 00:14:49,000
 [BLANK_AUDIO]

215
00:14:49,000 --> 00:14:52,000
 The move isn't happening yet.

216
00:14:52,000 --> 00:14:57,000
 It's happening, it's happening Saturday.

217
00:14:57,000 --> 00:15:01,000
 I guess we're doing some packing before then.

218
00:15:01,000 --> 00:15:03,360
 Robin was going to be here today, but she got caught up in

219
00:15:03,360 --> 00:15:04,000
 the storm.

220
00:15:04,000 --> 00:15:14,000
 [BLANK_AUDIO]

221
00:15:14,000 --> 00:15:17,830
 Can you accept boxes of food and tea from different

222
00:15:17,830 --> 00:15:19,000
 countries?

223
00:15:19,000 --> 00:15:22,000
 I can't accept food.

224
00:15:22,000 --> 00:15:25,000
 I also can't accept money.

225
00:15:25,000 --> 00:15:29,000
 [BLANK_AUDIO]

226
00:15:29,000 --> 00:15:32,000
 So the best way is through our organization.

227
00:15:32,000 --> 00:15:35,330
 You can go to our support page and look at ways to support

228
00:15:35,330 --> 00:15:36,000
 there.

229
00:15:36,000 --> 00:15:42,250
 As far as food, for me, I've been accepting gift cards to

230
00:15:42,250 --> 00:15:49,000
 Subway or Pita Pit or Starbucks or Tim Hortons.

231
00:15:49,000 --> 00:15:54,000
 And these gift cards can all be purchased online.

232
00:15:54,000 --> 00:15:59,000
 If you purchase them online, the company sends me an e-gift

233
00:15:59,000 --> 00:16:02,000
 or sends a card in the mail or something.

234
00:16:02,000 --> 00:16:04,000
 There's different ways they do that.

235
00:16:04,000 --> 00:16:06,000
 But I'm not sure how much I'll be using that in the future

236
00:16:06,000 --> 00:16:09,760
 because where we're living, there are not any convenient

237
00:16:09,760 --> 00:16:11,000
 stores like that.

238
00:16:11,000 --> 00:16:15,890
 So we'll either hopefully try and get a steward, someone to

239
00:16:15,890 --> 00:16:22,000
 come and stay and prepare food, or I'll go to the local.

240
00:16:22,000 --> 00:16:27,180
 We'll try and set something up at the local restaurant so

241
00:16:27,180 --> 00:16:29,000
 that I have food there.

242
00:16:29,000 --> 00:16:32,000
 Hopefully, I'll be able to stay alive a little longer.

243
00:16:32,000 --> 00:16:43,000
 [BLANK_AUDIO]

244
00:16:43,000 --> 00:16:48,570
 Okay, please use the green or orange button to preface your

245
00:16:48,570 --> 00:16:50,000
 questions.

246
00:16:50,000 --> 00:16:53,000
 Otherwise, I won't be able to find them.

247
00:16:53,000 --> 00:17:03,000
 [BLANK_AUDIO]

248
00:17:03,000 --> 00:17:06,080
 Could you please speak to how the layperson can make a

249
00:17:06,080 --> 00:17:09,370
 living with guidelines to follow for those that do touch

250
00:17:09,370 --> 00:17:10,000
 money?

251
00:17:10,000 --> 00:17:12,000
 [BLANK_AUDIO]

252
00:17:12,000 --> 00:17:18,770
 Yeah, I mean, on the one hand, you have to understand that

253
00:17:18,770 --> 00:17:25,000
 there aren't going to be too many concrete guidelines.

254
00:17:25,000 --> 00:17:28,530
 Because again, it's not so much about what you do, but

255
00:17:28,530 --> 00:17:30,000
 about how you do it.

256
00:17:30,000 --> 00:17:34,020
 That being said, there are some basic concrete guidelines,

257
00:17:34,020 --> 00:17:38,420
 but they're very much common sense from a Buddhist point of

258
00:17:38,420 --> 00:17:39,000
 view.

259
00:17:39,000 --> 00:17:45,000
 So any livelihood that breaks the five precepts is wrong.

260
00:17:45,000 --> 00:17:50,630
 Any livelihood that directly involves substances that harm

261
00:17:50,630 --> 00:17:53,000
 individuals is wrong.

262
00:17:53,000 --> 00:18:02,420
 So selling poison, selling weapons, selling animals,

263
00:18:02,420 --> 00:18:11,810
 selling poisons, weapons, living beings, humans, I think is

264
00:18:11,810 --> 00:18:13,000
 the fourth.

265
00:18:13,000 --> 00:18:18,000
 [BLANK_AUDIO]

266
00:18:18,000 --> 00:18:21,000
 I can never remember these things.

267
00:18:21,000 --> 00:18:23,000
 I usually can actually.

268
00:18:23,000 --> 00:18:34,900
 Poisons, weapons, living beings, intoxicants, I guess is

269
00:18:34,900 --> 00:18:39,000
 the fifth, right?

270
00:18:39,000 --> 00:18:40,000
 This is all wrong.

271
00:18:40,000 --> 00:18:42,000
 Anything that's involved.

272
00:18:42,000 --> 00:18:47,000
 So, but it comes down to some extent common sense.

273
00:18:47,000 --> 00:18:51,000
 What you're doing is involved in harming.

274
00:18:51,000 --> 00:18:56,010
 Or if you have to do harmful things to make a living, then

275
00:18:56,010 --> 00:18:58,000
 it's problematic.

276
00:18:58,000 --> 00:19:03,140
 I guess I would say many types of livelihood are indirectly

277
00:19:03,140 --> 00:19:06,000
 involved in unwholesome things.

278
00:19:06,000 --> 00:19:11,000
 Or involved in unwholesome things that are mild.

279
00:19:11,000 --> 00:19:14,750
 Even entertainment isn't considered the greatest livelihood

280
00:19:14,750 --> 00:19:15,000
.

281
00:19:15,000 --> 00:19:18,000
 It's involved in what we would call unwholesome desire.

282
00:19:18,000 --> 00:19:20,000
 There's desire involved.

283
00:19:20,000 --> 00:19:22,000
 Playing with people's emotions.

284
00:19:22,000 --> 00:19:29,000
 [BLANK_AUDIO]

285
00:19:29,000 --> 00:19:31,000
 You use quantum physics to support Buddhism.

286
00:19:31,000 --> 00:19:33,000
 Something I find compelling.

287
00:19:33,000 --> 00:19:37,930
 Does Buddhism believe in a single universe or a multiverse

288
00:19:37,930 --> 00:19:39,000
 or other?

289
00:19:39,000 --> 00:19:43,080
 Buddhism doesn't have too many beliefs and it isn't focused

290
00:19:43,080 --> 00:19:44,000
 on beliefs.

291
00:19:44,000 --> 00:19:48,460
 Buddhism is much more about the truth of reality, which is

292
00:19:48,460 --> 00:19:51,000
 very much in the present moment here and now.

293
00:19:51,000 --> 00:19:55,000
 Something to do with far away things.

294
00:19:55,000 --> 00:19:58,370
 It has to do with one's own experience within this six foot

295
00:19:58,370 --> 00:19:59,000
 frame.

296
00:19:59,000 --> 00:20:02,000
 Our universe has no size.

297
00:20:02,000 --> 00:20:06,000
 Our universe is not single or multiple.

298
00:20:06,000 --> 00:20:10,000
 Our universe is an experience.

299
00:20:10,000 --> 00:20:17,000
 It's a series of experiences.

300
00:20:17,000 --> 00:20:21,880
 That's what Buddhism, that's what our tradition of Buddhism

301
00:20:21,880 --> 00:20:23,000
 focuses on.

302
00:20:23,000 --> 00:20:27,130
 What do you think of Thich Nhat Hanh and his monasteries in

303
00:20:27,130 --> 00:20:31,000
 the Plum Village monasteries and Blue Kiff monasteries?

304
00:20:31,000 --> 00:20:34,000
 Sorry, I don't think too much about them.

305
00:20:34,000 --> 00:20:39,000
 I mean, I don't think about them too much.

306
00:20:39,000 --> 00:20:42,000
 Is it okay to directly try to notice the elements?

307
00:20:42,000 --> 00:20:45,720
 When I know it has a rising and falling, I get a mental

308
00:20:45,720 --> 00:20:48,000
 picture of the belly rising and falling.

309
00:20:48,000 --> 00:20:50,000
 It does not happen when I focus on the tension.

310
00:20:50,000 --> 00:20:53,000
 You can just say tense if you want.

311
00:20:53,000 --> 00:20:57,950
 But if you get a mental picture, you just say seeing,

312
00:20:57,950 --> 00:20:59,000
 seeing.

313
00:20:59,000 --> 00:21:05,720
 You find that for it not to be, for it not to be amenable

314
00:21:05,720 --> 00:21:10,000
 to your will, when you see the picture, you say seeing.

315
00:21:10,000 --> 00:21:15,990
 The Buddha didn't require you to say things like Earth or

316
00:21:15,990 --> 00:21:17,000
 so on.

317
00:21:17,000 --> 00:21:20,000
 He said, "Gachantu vah gachamiti bachanati"

318
00:21:20,000 --> 00:21:22,000
 When going, one knows I go.

319
00:21:22,000 --> 00:21:25,000
 I'm walking.

320
00:21:25,000 --> 00:21:28,000
 So you say walking, you get a picture of your feet?

321
00:21:28,000 --> 00:21:34,860
 Walking is meant to be a description of the feeling, of the

322
00:21:34,860 --> 00:21:36,000
 elements.

323
00:21:36,000 --> 00:21:43,000
 But you still say walking, or you say sitting, or so on.

324
00:21:43,000 --> 00:21:49,000
 I wouldn't worry too much about that.

325
00:21:49,000 --> 00:21:56,000
 But if you do see the image, you would say seeing, seeing.

326
00:21:56,000 --> 00:21:59,430
 The mind stops inclining, forming words, and starts to

327
00:21:59,430 --> 00:22:02,000
 black out rhythmically.

328
00:22:02,000 --> 00:22:06,000
 So we force it to form, noting word, we don't force it.

329
00:22:06,000 --> 00:22:09,760
 If the mind starts to black out, whatever that means, you

330
00:22:09,760 --> 00:22:15,250
 would say knowing, knowing, or feeling, feeling, or calm,

331
00:22:15,250 --> 00:22:20,000
 calm, quiet, quiet.

332
00:22:20,000 --> 00:22:24,000
 You should use a word based on what you're experiencing.

333
00:22:24,000 --> 00:22:29,100
 The word is a reminder, sati, it's a cultivation of

334
00:22:29,100 --> 00:22:30,000
 mindfulness.

335
00:22:30,000 --> 00:22:36,420
 You're reminding yourself it's just this, so that you don't

336
00:22:36,420 --> 00:22:38,000
 react to it.

337
00:22:38,000 --> 00:22:41,080
 When a kamma is being committed, can there be situations

338
00:22:41,080 --> 00:22:44,000
 when the role of free will becomes insignificant?

339
00:22:44,000 --> 00:22:47,610
 For example, a person has a view that killing animals is

340
00:22:47,610 --> 00:22:51,000
 okay, and if a mosquito bites him, causing anger to arise,

341
00:22:51,000 --> 00:22:55,230
 would he still be able to refrain from killing it, given

342
00:22:55,230 --> 00:22:58,000
 that no other factors intervene?

343
00:22:58,000 --> 00:23:08,300
 And that's very... I don't think you're making things

344
00:23:08,300 --> 00:23:12,000
 overly complicated.

345
00:23:12,000 --> 00:23:17,620
 If the mosquito bite causes anger to arise, one has failed

346
00:23:17,620 --> 00:23:21,000
 in being mindful of the feeling.

347
00:23:21,000 --> 00:23:24,350
 If the feeling comes and one knows it has a feeling, there

348
00:23:24,350 --> 00:23:26,000
 will be no anger arising.

349
00:23:26,000 --> 00:23:29,000
 If anger arises, it's already too late.

350
00:23:29,000 --> 00:23:36,860
 Depending on one state of mind, that anger might result in

351
00:23:36,860 --> 00:23:40,000
 killing the animal.

352
00:23:40,000 --> 00:23:44,210
 But there's required more than anger, there's required

353
00:23:44,210 --> 00:23:48,000
 wrong view, I think, I should think, because a sotapana

354
00:23:48,000 --> 00:23:57,910
 couldn't give rise to the thoughts of that killing is the

355
00:23:57,910 --> 00:24:01,000
 right answer.

356
00:24:01,000 --> 00:24:07,860
 Here, the ottaba would have to intervene without a wholes

357
00:24:07,860 --> 00:24:10,000
ome mind state.

358
00:24:10,000 --> 00:24:13,540
 It didn't intervene in time for the anger, but the anger is

359
00:24:13,540 --> 00:24:15,000
 only one mind state.

360
00:24:15,000 --> 00:24:19,080
 After the anger, there has to be the thought, "I should

361
00:24:19,080 --> 00:24:22,000
 kill this," the thought, "kill."

362
00:24:22,000 --> 00:24:28,400
 Once that thought arises, there has to be a response, an

363
00:24:28,400 --> 00:24:34,000
 anger and a delusion response to that thought.

364
00:24:34,000 --> 00:24:41,000
 It's about whether a hiriyotaba intervene at that point.

365
00:24:41,000 --> 00:24:45,910
 They've already missed the chance to stop the anger from

366
00:24:45,910 --> 00:24:47,000
 arising.

367
00:24:47,000 --> 00:24:54,800
 But anger isn't enough, it requires the thought to kill and

368
00:24:54,800 --> 00:24:58,000
 the reaction to that.

369
00:24:58,000 --> 00:25:22,000
 [silence]

370
00:25:22,000 --> 00:25:26,540
 Alright, I'm going to give it a rest there for tonight, and

371
00:25:26,540 --> 00:25:32,000
 I'm going to do my meditation, skip my evening meditation,

372
00:25:32,000 --> 00:25:35,000
 and I'm going to go do it now.

373
00:25:35,000 --> 00:25:37,000
 One more question.

374
00:25:37,000 --> 00:25:40,010
 I remember hearing that you said we practice satipatana,

375
00:25:40,010 --> 00:25:43,000
 but I also heard you say that we practice anapanasatim.

376
00:25:43,000 --> 00:25:51,000
 What do you mean? Is it a combination?

377
00:25:51,000 --> 00:25:56,000
 Anapanasati means mindfulness, sati is satipatana.

378
00:25:56,000 --> 00:26:00,500
 Anapana means the breath, so if you're watching the breath,

379
00:26:00,500 --> 00:26:02,000
 that's anapanasati.

380
00:26:02,000 --> 00:26:09,040
 But sati, if you're using the elements, then it's satipat

381
00:26:09,040 --> 00:26:10,000
ana.

382
00:26:10,000 --> 00:26:13,000
 You're focusing on the elements.

383
00:26:13,000 --> 00:26:15,330
 So when you watch the stomach, you say to yourself, "Rising

384
00:26:15,330 --> 00:26:16,000
, falling."

385
00:26:16,000 --> 00:26:20,000
 That's both watching the breath and watching the elements.

386
00:26:20,000 --> 00:26:26,000
 So it's both anapanasati and satipatana.

387
00:26:26,000 --> 00:26:29,000
 In the future, do you plan on going back to Thailand?

388
00:26:29,000 --> 00:26:31,000
 Oh, I don't think too much about the future.

389
00:26:31,000 --> 00:26:39,070
 In the future, I plan on moving to Hamilton about ten

390
00:26:39,070 --> 00:26:43,000
 minutes down the road.

391
00:26:43,000 --> 00:26:47,820
 Is it fair to say that an arahant has the perfect free will

392
00:26:47,820 --> 00:26:52,000
 since they are not swayed by the defilement?

393
00:26:52,000 --> 00:26:54,000
 I don't know.

394
00:26:54,000 --> 00:27:05,000
 I don't like using terms like free will or determinism.

395
00:27:05,000 --> 00:27:09,000
 I wouldn't say such a thing.

396
00:27:09,000 --> 00:27:13,000
 An arahant has perfect freedom.

397
00:27:13,000 --> 00:27:16,120
 Besides the fact that they still have to live, still have

398
00:27:16,120 --> 00:27:20,000
 to eat, still have to suffer physically.

399
00:27:20,000 --> 00:27:32,000
 Besides that, they're pretty much free.

400
00:27:32,000 --> 00:27:36,700
 Is exercising another presumed health habit just another

401
00:27:36,700 --> 00:27:38,000
 attachment?

402
00:27:38,000 --> 00:27:44,000
 Well, to some extent, you could argue that it's functional.

403
00:27:44,000 --> 00:27:47,000
 Exercise to some limited extent is functional.

404
00:27:47,000 --> 00:27:51,000
 But for the most part, exercise is because we eat too much.

405
00:27:51,000 --> 00:27:52,000
 We didn't eat too much.

406
00:27:52,000 --> 00:27:55,600
 We wouldn't need to exercise nearly as much as most people

407
00:27:55,600 --> 00:27:56,000
 do.

408
00:27:56,000 --> 00:27:59,000
 Play sports and that kind of thing.

409
00:27:59,000 --> 00:28:02,000
 It's all because of too much food.

410
00:28:02,000 --> 00:28:05,140
 If you ate just enough to survive, you wouldn't need to do

411
00:28:05,140 --> 00:28:07,000
 much exercising to keep yourself healthy.

412
00:28:07,000 --> 00:28:14,000
 That being said, for some exercise is useful.

413
00:28:14,000 --> 00:28:20,000
 And some amount of healthiness is reasonable.

414
00:28:20,000 --> 00:28:23,000
 It's just good practice.

415
00:28:23,000 --> 00:28:29,000
 But often we become very much obsessed with health.

416
00:28:29,000 --> 00:28:31,000
 And that's a problem.

417
00:28:31,000 --> 00:28:35,700
 If you try to be too healthy, too obsessed with always

418
00:28:35,700 --> 00:28:37,000
 being healthy,

419
00:28:37,000 --> 00:28:41,000
 then you won't be able to deal with sickness.

420
00:28:41,000 --> 00:28:44,160
 You'll be very much averse and concerned and afraid of

421
00:28:44,160 --> 00:28:45,000
 sickness.

422
00:28:45,000 --> 00:28:49,000
 You shouldn't be afraid of getting sick.

423
00:28:49,000 --> 00:28:54,910
 You should be ready and able to deal with sickness when it

424
00:28:54,910 --> 00:28:56,000
 comes.

425
00:28:56,000 --> 00:29:02,000
 And so obsessing about health is somewhat counter to that.

426
00:29:02,000 --> 00:29:06,000
 Okay, I'm really going this time. Good night everyone.

427
00:29:06,000 --> 00:29:08,000
 Have a good night. See you next time.

428
00:29:08,000 --> 00:29:18,000
 [BLANK_AUDIO]

