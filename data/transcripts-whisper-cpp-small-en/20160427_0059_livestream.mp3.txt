 Good evening everyone.
 Broadcasting live April 26th.
 Today is a quote again about anger. This time from Nwisudhi
 Maga.
 It's under the teaching about metta. So in order to
 cultivate metta you have to be
 skillful and overcoming anger. And yet there's this set of
 verses that
 form tonight's quote. So the first stanza is, "If someone
 hurts you,
 hurts you insofar as they can, they've done whatever they
 can to hurt you.
 That makes you hurt once. You hurt because of the things
 that they've done."
 And then the question is, the question you should ask
 yourself.
 So this quote is actually supposed to be directed to
 yourself.
 When you get angry about something someone has done to you,
 you should ask yourself, "Why do I want to hurt myself
 again?
 Why am I sitting here hurting myself because someone has
 hurt me?
 Why am I doing it again? Why am I making it twice as bad?
 Why am I helping my enemies? For people who want to bring
 me suffering,
 why do I suffer?" You think that anger is a valid response,
 a proper response to anything really. Buddha said if you're
 in pain and then
 you get upset about it, it's like you have a thorn or a spl
inter in your skin
 and you take another thorn and you try to gouge it out.
 You make it twice as bad.
 The second stanza is, "In tears you left your family.
 They had been kind and helpful too. So why not leave your
 enemy the anger
 that brings harm to you?"
 So we left behind all that was dear to us to come to med
itate,
 to cultivate spirituality. We give up all the good things
 and all the good things or pleasurable things in the world.
 And yet we bring our enemies with us.
 Now when we get angry, why are we holding on to bad things?
 This is an admonishment, a self-admonishment for someone
 who is angry.
 Actually it's generally easier to give up anger than it is
 to give up greed or craving.
 And to this kind of admonishment it's actually quite, you
 know, it's fairly simple.
 Just remind yourself that this is hurting you. This is
 harmful.
 The anger, the reaction, the judging.
 This anger that you entertain, the third stanza, this anger
 that you entertain,
 and again I'm reading actually Nyanamoli's translation
 so it's a bit different from the one on our website.
 This anger that you entertain is gnawing at the very roots
 of all the virtue that you guard.
 Who is there such a fool as you?
 Anger, greed, they gnaw away at our goodness.
 No, they chew at it. They eat away at all the good that we
've done.
 It gets spoiled. You can be helpful to people and
 charitable and kind.
 But you can be, then you're angry one time and you lose
 your reputation as a good person.
 And it inflames your mind and it prevents you from enjoying
 the peace and happiness that come from goodness.
 And remind ourselves of how it's eating away at our virtue.
 It's making us more coarse. It helps us to give up the
 inclination to be angry, to get angry.
 And the next stanza, another does ignoble deeds.
 So you are angry. How is this? Do you then want to copy to
 the sort of acts that he commits?
 So this person has done something evil and it is evil.
 And so why in the world are you now, you who believe that
 it was wrong,
 why are you now doing the same thing?
 That's a curious point because when you get angry at
 someone who's evil, you're joining them in their evil.
 In fact, the Buddha said, and it mentions in this up above
 that it's worse to get angry back at someone.
 It's a worse evil. Someone's angry at you and you return
 the anger. That's worse.
 Because we all get angry. Anger comes and if we love each
 other then we let each other vent.
 When someone gets angry at us, we appreciate what they're
 saying.
 We listen and we try to calm them down and appease them in
 some way.
 But when you get angry back, then you start the fight. You
 start conflict.
 That's when the real problem is. So, and this is a good
 lesson, you know.
 We judge each other very much and if someone gets angry, we
 get angry back.
 If someone is greedy, we get upset at them.
 If someone is arrogant and conceited, we get angry and
 upset about the bad things in others.
 And then that makes us just as bad.
 Moreover, we're too critical. Critical of ourselves and
 critical of each other.
 In monasteries, it's quite common for new monks to come and
 just criticize roundly.
 Criticize every other monk, all the old monks, all the
 monks who have been there for a long time because they're
 not perfect.
 With this kind of conception that if you're not perfect,
 then you deserve to be criticized.
 But really, if you're not perfect, you deserve to be helped
, to be supported and directed and guided and pushed and pro
dded in the right direction, sure.
 But to denounce someone because they're imperfect, that's
 not the Buddhist way.
 For more reasons than one. Because it's useless, it's un
helpful, and because it's harmful to yourself as well.
 The next stanza just says that when you get angry at
 something someone has done, you do what they would have you
 do.
 They want you to suffer, and bingo, you're suffering.
 And that's key because really other people can't make us
 suffer.
 They can hit you, they can denounce you, they can scold you
, they can insult you.
 But they can't make you suffer. They can't make your mind
 suffer.
 They can't make you get angry.
 I mean, it depends how you look at it, but it's possible to
 be free from anger no matter what someone else does.
 But once you get angry, that's when the suffering comes.
 There's no question you suffer when you're angry. You
 suffer when you dislike something. That's where suffering
 comes from.
 Next stanza. If you get angry, then maybe you make him
 suffer, maybe not.
 Though with the hurt that anger brings, you certainly are
 punished now.
 So when you get angry back, usually it's because you are
 thinking of ways to retaliate, to seek revenge.
 And so there's like, well, maybe you can find revenge.
 Maybe you'll actually end up harming the other person and
 getting revenge.
 We don't know. It's possible.
 But what's certain is that you're punishing yourself with
 your anger, and you punish yourself more if you seek
 revenge.
 If anger blinded enemies set out to tread the path of woe,
 do you, by getting angry too, intend to follow heel to toe?
 You intend to follow these anger blinded enemies.
 If hurt is done, you buy a foe because of anger on your
 part.
 Then put your anger down for why should you be harassed
 groundlessly?
 Yes. Why? You weren't the one that did the evil. Why are
 you suffering?
 Someone hurts you or attacks you or criticizes you or so on
. That's their problem.
 Why are you suffering?
 Since states last but a moment's time, those aggregates by
 which was done, the odious act have ceased.
 So now, what is it you are angry with?
 This is very much in meditation. The person who we're angry
 at doesn't exist.
 The states that arose, that gave rise to the event that we
're angry at, those have ceased.
 When we're angry at something, worried about something, or
 when we're attached to something, we want something.
 That thing is only a concept. The experiences arise and
 cease.
 If it's a good thing, well, that good thing is not really
 good in any way.
 It just arises and ceases, comes and goes, and the same
 with bad things.
 There's no person who is our enemy. What are we angry at?
 What really are we angry at?
 What we're angry at is a memory, a thought, a thought that
 arises in our mind that is an echo of some past deed.
 And we get angry at that thought.
 Whom shall he hurt, who seeks to hurt another in the other
's absence?
 Your presence is the cause of hurt. Why are you angry then
 with him?
 Yes, we're angry at ourselves, actually. It's our own mind
 states, the thoughts that arise in our own mind.
 The person who did this deed, if someone sits and is very
 angry at another person,
 it's nothing to do with the other person. It's you yourself
 who are hurting yourself.
 You're angry at thoughts that arise in your own mind.
 Nothing to do with the other person.
 So this has to do with anger. But more generally, it's the
 idea that only we can hurt ourselves.
 And happiness as well has to come from within. Happiness
 can't be based on a thing.
 And suffering doesn't come from things. Happiness and
 suffering don't come from the outside. They come from
 within.
 It's quite simple. Our state of mind determines our state
 of happiness, not the world around us.
 This is crucial. It's a specific Buddhist doctrine that
 happiness and suffering come from the mind.
 This is clear. So a big part of our practice is learning to
 be objective and to see the experiences that come from
 external,
 or that come from the body and the mind, to see them with
 wisdom, to cultivate a relationship with experience that is
 free from likes and dislikes,
 to be objective and to see things as they are. Because when
 you do, there's not intrinsic likeability or dislikeability
,
 attraction, attractiveness, or undetracktiveness. It's not
 a quality of things.
 And so when we look at things as they are, we find that
 they're not attractive. We don't give rise to the
 attraction.
 Attraction and aversion, they come from not seeing things
 as they are. That's key as well. Because it could be
 otherwise.
 Mostly we think it's otherwise. We think that some things
 are intrinsically beautiful or desirable.
 Some other things are intrinsically dislikeable, a direct
 cause for disliking. That's not true.
 When you see things as they are, it's only because of
 delusion that we, because of misconception that we have
 desire and aversion.
 So that's our dhamma for tonight.
 If you're interested in the Vysuddhi manga, we're actually
 studying it on Sundays, but we're most of the way through
 it already.
 I appreciate the Vysuddhi manga. It's more like a manual or
 a reference guide. It's good for teachers, also good for
 meditators, experienced meditators, I think.
 There's just so much in it, so many different, like this
 set of verses, it's quite useful. That's from the Vysuddhi
 manga.
 You too can go. You don't have to stay. I'm just going to
 answer questions if people have.
 I was in New York. The monk I was staying with in New York
 doesn't like the Vysuddhi manga, or the commentaries, or
 the abhidhamma, which is very common with Western monks,
 especially in the Ajahn Chah group.
 Although he's not exactly with the Ajahn Chah group. They
're so keen on the suttas, which is great. The suttas are
 just the most pure.
 But the suttas are not the only source of Buddhism. The s
uttas are talks that were given, mostly that were given to
 the monks, or given to certain people at a certain time.
 And so they have to be interpreted, especially because we
're so far removed from that time and that place. So no
 matter what, you have to explain the suttas.
 And so either you follow the commentary explanation, the V
ysuddhi manga explanation, or you follow some modern
 commentator's explanation.
 And it turns out to be no better. Turns out to be actually
 usually worse, I would say, I would argue. Worse in the
 sense that whether you agree with it or not, far less
 rigorous, far less objective, far less comprehensive, far
 less in-depth.
 The Vysuddhi manga just gets incredible work if you
 disagree with it or disagree with it or disagree with it.
 It's amazing that someone actually wrote it because it's
 huge and it's so in-depth and it's got so much in it.
 I remember I was giving a talk when I was in Sri Lanka last
 time. And the man who was translating for me refused to
 translate because I was quoting the Vysuddhi manga and he
 said, "Oh, we don't follow the Vysuddhi manga here."
 That was a first for me. I was giving a talk and the
 translator wouldn't translate. I think I was saying
 something mean or evil.
 So it's a contentious, the Vysuddhi manga tradition is
 fairly contentious to some people. Most of the, most Therav
ada societies, for the most part, they follow the Vysuddhi
 manga.
 In Thailand, it forms the latter part of one's Pali studies
. So any monk whose studies higher level Pali will have to
 translate the Vysuddhi manga into Thai and then translate
 Thai passages, Thai translations of the Vysuddhi manga back
 into Pali.
 So they have to really memorize. Many of them basically, I
 think, will memorize the Vysuddhi manga in Pali in English,
 which is impressive in itself. Maybe not memorize, but come
 close.
 Any requirements for taking a meditation course at our
 center? Yeah, probably. I mean, if you're coming alone and
 you're under 16, I think we'd have to have a talk about
 that.
 I don't know. I would imagine having people under 16 coming
 and doing courses is problematic unless we talk to their
 parents. I think 16 is probably.
 And as for upper age limit, it's more to do with your
 health, you know. If you're able to do walking and sitting
 meditation for hours a day, it's fine. You know, you can
 sit on a chair if you have to.
 I don't know. Robin, I haven't really thought about the
 future. I mean, there are some other things I'm looking at,
 actually.
 But like, actually, the Anguttranikai is a good volume and
 I've started maybe collecting some good suttas from it.
 A new set of robes for my trip to Asia. Now, the funny
 thing about these robes, they're fairly new, right? But
 they came with holes in them. Like, I think they were mouse
 eaten or they were ripped or something.
 Because in my back, there's actually little holes that I
 probably shouldn't sew up. Not that that's here or there.
 But I know these are fine in Asia.
 It'll be a little hot, but you know, this upper robe I don
't wear a lot of the time. When I'm in my room, I rarely
 wear this.
 Sometimes I just go bare chest or sometimes I have a basic
 what's called an angsa. It's just a very thin shoulder robe
, shoulder cloth.
 But these aren't that hot. It's just cotton. So it's
 actually not that hot. But it's going to be hot in June. So
 I'll try to go up and live on the mountain.
 I'm going to try to go up on the mountain in Sri Lanka.
 I'm arguing with Sangha about that. Sangha wants me to stay
 in Colombo for two weeks. I'm trying to push to take a week
 out. He says a week isn't enough.
 I know it isn't. I mean, a month isn't enough. I could
 spend months in Sri Lanka giving talks every day, but not
 really keen on it.
 And then in Thailand, maybe go back up on the mountain as
 well.
 Probably not staying Jumdong very long because it's
 probably pretty hot in June. Unless it started raining, it
 should be okay.
 Do we only have a few months left? Have we really gone
 through most of the year? When do we start? August or
 something?
 July maybe? Time flies.
 Tempest fuget. Time flies.
 Okay. Good night everyone. See you all next time.
