1
00:00:00,000 --> 00:00:29,980
 [ Silence ]

2
00:00:29,980 --> 00:00:39,980
 [ Silence ]

3
00:00:39,980 --> 00:01:03,980
 [ Silence ]

4
00:01:03,980 --> 00:01:16,980
 I am back just testing it out. See how it goes. I don't see

5
00:01:16,980 --> 00:01:17,980
 it.

6
00:01:17,980 --> 00:01:35,980
 [ Silence ]

7
00:01:35,980 --> 00:01:51,980
 [ Silence ]

8
00:01:51,980 --> 00:02:07,980
 Hmm.

9
00:02:07,980 --> 00:02:19,630
 How's the video? I can't see video. I don't know why it's

10
00:02:19,630 --> 00:02:23,980
 not showing for me.

11
00:02:23,980 --> 00:02:34,760
 Hmm. Yeah, it's much quicker, much higher bandwidth. How's

12
00:02:34,760 --> 00:02:45,980
 the video? Video is better, I guess.

13
00:02:45,980 --> 00:02:52,860
 Okay, I figured it out, I think. We'll see. I mean, it may

14
00:02:52,860 --> 00:02:58,980
 start to buffer again, but I just looked at what YouTube

15
00:02:58,980 --> 00:03:01,980
 advises and what --

16
00:03:01,980 --> 00:03:09,440
 -- FFMPig advises. Okay. All right. We'll stick with this

17
00:03:09,440 --> 00:03:15,360
 then. Oh, no. No. YouTube is not receiving enough video to

18
00:03:15,360 --> 00:03:17,980
 maintain smooth streaming.

19
00:03:17,980 --> 00:03:31,980
 Back to the drawing board. Why would that be?

20
00:03:31,980 --> 00:03:49,980
 [ Silence ]

21
00:03:49,980 --> 00:04:11,980
 [ Silence ]

22
00:04:11,980 --> 00:04:21,980
 Oh, stream is healthy again.

23
00:04:21,980 --> 00:04:37,980
 I still can't see it.

24
00:04:37,980 --> 00:05:05,980
 Oh, yeah. Quality is much better, huh?

25
00:05:05,980 --> 00:05:09,230
 You're welcome. It's good to hear someone actually

26
00:05:09,230 --> 00:05:11,980
 appreciated that video. That's one of the more contentious.

27
00:05:11,980 --> 00:05:19,250
 I've received a lot of attention lately from people who

28
00:05:19,250 --> 00:05:23,980
 wouldn't normally give me that kind of attention, I guess.

29
00:05:23,980 --> 00:05:39,980
 [ Silence ]

30
00:05:39,980 --> 00:05:44,710
 So it says this stream health is good for now. I was hoping

31
00:05:44,710 --> 00:05:45,980
 it stays that way.

32
00:05:45,980 --> 00:05:59,980
 [ Silence ]

33
00:05:59,980 --> 00:06:03,980
 Okay.

34
00:06:03,980 --> 00:06:10,180
 Well, I'm glad you appreciated it. It's good to hear. Good

35
00:06:10,180 --> 00:06:16,160
 to have feedback. I'm always interested to know how people

36
00:06:16,160 --> 00:06:21,350
 affected by such issues, what they think of the things that

37
00:06:21,350 --> 00:06:21,980
 I say.

38
00:06:21,980 --> 00:06:36,980
 [ Silence ]

39
00:06:36,980 --> 00:06:46,800
 Okay. Well, the stream health is still excellent, assuming

40
00:06:46,800 --> 00:06:49,980
 that may not last.

41
00:06:49,980 --> 00:06:59,980
 But I did tweak something, so I don't know if that helped.

42
00:06:59,980 --> 00:07:04,980
 Might not have been an internet thing.

43
00:07:04,980 --> 00:07:16,980
 I might have just fixed it.

44
00:07:16,980 --> 00:07:23,980
 Oh, there's another one, ultra-fast.

45
00:07:23,980 --> 00:07:34,980
 Right. That's what it says. Okay.

46
00:07:34,980 --> 00:07:38,980
 Maybe we're good.

47
00:07:38,980 --> 00:07:56,960
 The subject of the talk today is how to livestream on

48
00:07:56,960 --> 00:08:02,980
 YouTube. This is just a test.

49
00:08:02,980 --> 00:08:08,200
 But there's a video that I just did, and it should be in my

50
00:08:08,200 --> 00:08:09,980
 video section.

51
00:08:09,980 --> 00:08:19,980
 Let me see. Yes. If you look, I'll delete this short one.

52
00:08:19,980 --> 00:08:23,780
 So if you look, there is one called "Livestream" in my

53
00:08:23,780 --> 00:08:24,980
 videos list.

54
00:08:24,980 --> 00:08:29,940
 And the topic of that video was preparation for a

55
00:08:29,940 --> 00:08:39,110
 meditation course, along with a lot of technical

56
00:08:39,110 --> 00:08:42,980
 difficulties.

57
00:08:42,980 --> 00:08:54,980
 All right. If you've got questions, I'm here to answer.

58
00:08:54,980 --> 00:08:56,980
 What are your questions? Go for it.

59
00:08:56,980 --> 00:09:02,230
 Special test question session. Do we really die? We die

60
00:09:02,230 --> 00:09:11,980
 every moment. We're born and die every moment.

61
00:09:11,980 --> 00:09:17,800
 Do you have your favorite chant? If so, why? Mine is "Etip

62
00:09:17,800 --> 00:09:18,980
iso."

63
00:09:18,980 --> 00:09:24,900
 I mean, favorites imply attachment, right? So we have to be

64
00:09:24,900 --> 00:09:27,980
 careful about that.

65
00:09:27,980 --> 00:09:31,980
 But I don't really have a favorite chant. No.

66
00:09:31,980 --> 00:09:36,440
 Oh, there's one chant. I've told this story before. I was

67
00:09:36,440 --> 00:09:37,980
 up on Doi Suthep,

68
00:09:37,980 --> 00:09:44,170
 long story short, these monks, they were going around the

69
00:09:44,170 --> 00:09:44,980
 pagoda.

70
00:09:44,980 --> 00:09:47,380
 And I heard them doing this chanting, and immediately it

71
00:09:47,380 --> 00:09:47,980
 struck me.

72
00:09:47,980 --> 00:09:51,360
 It was just like something resonated with me, with that

73
00:09:51,360 --> 00:09:51,980
 chant.

74
00:09:51,980 --> 00:09:55,070
 I think I was probably Burmese. These were Burmese monks. I

75
00:09:55,070 --> 00:09:57,980
 was probably Burmese in a past life.

76
00:09:57,980 --> 00:10:05,590
 And I had to find out what that chant was. I got stuck in

77
00:10:05,590 --> 00:10:11,980
 my mind, and I kept it in my mind for quite a while.

78
00:10:11,980 --> 00:10:14,720
 And then when I was in Bangkok one time, I was sitting

79
00:10:14,720 --> 00:10:16,980
 beside a Burmese monk, and I asked him about it.

80
00:10:16,980 --> 00:10:20,480
 I kind of mumbled it to him, and he knew right away what it

81
00:10:20,480 --> 00:10:26,980
 was. It was the Mahapatana.

82
00:10:26,980 --> 00:10:31,080
 The Mahapatana, I would say, is my favorite chant, as far

83
00:10:31,080 --> 00:10:33,980
 as that goes.

84
00:10:33,980 --> 00:10:43,980
 Let's see if I can get you a...here. I'll show you a link.

85
00:10:43,980 --> 00:10:47,980
 Maybe I can send a link.

86
00:10:47,980 --> 00:10:54,980
 Does that work? There. There's a link to the chant.

87
00:10:54,980 --> 00:10:57,980
 I think it's the best version I have. It's not very good

88
00:10:57,980 --> 00:11:05,980
 quality audio, but that's the chant.

89
00:11:05,980 --> 00:11:09,670
 How frequently do you stream live on YouTube? Well, this is

90
00:11:09,670 --> 00:11:14,180
 the first time in a while, but I should be doing it most

91
00:11:14,180 --> 00:11:14,980
 days.

92
00:11:14,980 --> 00:11:17,920
 I might reduce the frequency in the future, maybe three

93
00:11:17,920 --> 00:11:23,980
 times a week, something like that. We'll see.

94
00:11:23,980 --> 00:11:28,290
 What are your thoughts on negative and positive energy med

95
00:11:28,290 --> 00:11:29,980
itating with cats?

96
00:11:29,980 --> 00:11:34,520
 I don't believe in such things. I mean, negative and

97
00:11:34,520 --> 00:11:40,200
 positive is how we react to them, how we react to

98
00:11:40,200 --> 00:11:43,980
 experiences.

99
00:11:43,980 --> 00:11:48,980
 So I'm much more about our reactions to things.

100
00:11:48,980 --> 00:11:53,300
 It becomes negative because we have a negative conception

101
00:11:53,300 --> 00:11:53,980
 of it.

102
00:11:53,980 --> 00:11:58,310
 Something is positive because we have a positive reaction

103
00:11:58,310 --> 00:11:58,980
 to it.

104
00:11:58,980 --> 00:12:02,050
 So we try to free ourselves from that, and then energy is

105
00:12:02,050 --> 00:12:06,980
 just energy. It's not positive or negative.

106
00:12:06,980 --> 00:12:09,550
 What can help with insomnia or getting very hungry in the

107
00:12:09,550 --> 00:12:10,980
 evening when it is time to sleep?

108
00:12:10,980 --> 00:12:14,980
 Those are fairly different, no?

109
00:12:14,980 --> 00:12:17,910
 Well, if you haven't, I'd suggest you read my booklet on

110
00:12:17,910 --> 00:12:18,980
 how to meditate.

111
00:12:18,980 --> 00:12:23,250
 That should help with things like insomnia or hunger and

112
00:12:23,250 --> 00:12:24,980
 that kind of thing.

113
00:12:24,980 --> 00:12:30,980
 Do you want a link to that for those of you who don't know?

114
00:12:30,980 --> 00:12:35,980
 I'll send you a link to my booklet on how to meditate.

115
00:12:35,980 --> 00:12:42,980
 There, there's another link.

116
00:12:42,980 --> 00:12:47,180
 What's your opinion on monks? On monk with sakyant. I have

117
00:12:47,180 --> 00:12:48,980
 no idea. Sakyant. Are you Thai?

118
00:12:48,980 --> 00:12:50,990
 You must be a Thai person because you're using Thai words.

119
00:12:50,990 --> 00:12:52,980
 Sakyant is tattoos, right?

120
00:12:52,980 --> 00:13:00,320
 Special, special religious tattoos. I mean superstition,

121
00:13:00,320 --> 00:13:00,980
 really.

122
00:13:00,980 --> 00:13:03,750
 Or, I mean, you could argue that there's some, it affects

123
00:13:03,750 --> 00:13:08,980
 you psychologically, so there's some good to it.

124
00:13:08,980 --> 00:13:12,980
 I don't have much opinion on it.

125
00:13:12,980 --> 00:13:16,700
 I was doing qigong and abilities when meditating and my cat

126
00:13:16,700 --> 00:13:18,980
 freaked out and ran.

127
00:13:18,980 --> 00:13:24,060
 Well, it sounds like your cat is reactionary. Your thoughts

128
00:13:24,060 --> 00:13:26,980
 on dogma, dogmatic beliefs, etc.

129
00:13:26,980 --> 00:13:34,210
 Yeah, I mean, I wouldn't hold on to anything dogmatically.

130
00:13:34,210 --> 00:13:38,980
 Beliefs should come from experience.

131
00:13:38,980 --> 00:13:41,350
 It should come from experience, but it can also come from

132
00:13:41,350 --> 00:13:44,980
 an experience that gives you confidence in authority.

133
00:13:44,980 --> 00:13:47,390
 So I have confidence in many things the Buddha said, even

134
00:13:47,390 --> 00:13:48,980
 though I've never experienced them,

135
00:13:48,980 --> 00:13:51,890
 because the things I have practiced of the Buddhas give me

136
00:13:51,890 --> 00:13:54,980
 confidence in him. That kind of thing.

137
00:13:54,980 --> 00:14:00,970
 So sometimes it's not dogmatic, it's based on an educated

138
00:14:00,970 --> 00:14:08,980
 or an informed confidence in authority.

139
00:14:08,980 --> 00:14:12,980
 Malaysian, but you're using Thai words, sakmyant.

140
00:14:12,980 --> 00:14:19,980
 Why do I care so much? I want to stop, it hurts me.

141
00:14:19,980 --> 00:14:24,980
 Well, it's habits, we build up habits of caring.

142
00:14:24,980 --> 00:14:28,980
 But the great thing is that there's no me being hurt.

143
00:14:28,980 --> 00:14:33,200
 Hurt is just a state of mind, and if you come to see that,

144
00:14:33,200 --> 00:14:34,980
 you free yourself.

145
00:14:34,980 --> 00:14:38,660
 You no longer have this, free yourself. It's a funny thing

146
00:14:38,660 --> 00:14:40,980
 to say, there's freedom.

147
00:14:40,980 --> 00:14:44,970
 You free yourself by not having self, by not clinging to

148
00:14:44,970 --> 00:14:48,980
 the experiences, me, I, I, me.

149
00:14:48,980 --> 00:14:52,310
 But read my booklet. If you practice the meditation that's

150
00:14:52,310 --> 00:14:54,980
 in that booklet, I guarantee it will help.

151
00:14:54,980 --> 00:14:59,980
 It will be less hurting, less suffering.

152
00:14:59,980 --> 00:15:04,980
 Have you seen a ghost? No, I've never seen a ghost.

153
00:15:04,980 --> 00:15:08,800
 I mean, I don't know. I've never seen anything that I could

154
00:15:08,800 --> 00:15:09,980
 identify as a ghost.

155
00:15:09,980 --> 00:15:13,980
 Well, it's the best way to become motivated.

156
00:15:13,980 --> 00:15:17,690
 I mean, read the Buddha's teaching, be surrounded by people

157
00:15:17,690 --> 00:15:23,980
 who are energetic, who are motivated.

158
00:15:23,980 --> 00:15:27,980
 There's different ways.

159
00:15:27,980 --> 00:15:31,720
 Does meditating with eyes closed differ than with eyes open

160
00:15:31,720 --> 00:15:31,980
?

161
00:15:31,980 --> 00:15:36,980
 Can the two methods cause different states of mindfulness?

162
00:15:36,980 --> 00:15:39,980
 I don't know, it's not that important.

163
00:15:39,980 --> 00:15:43,670
 It's easier with your eyes closed, it's easier to get me

164
00:15:43,670 --> 00:15:43,980
 focused.

165
00:15:43,980 --> 00:15:47,980
 Better for beginners, anyway. Better in general, I mean,

166
00:15:47,980 --> 00:15:48,980
 you're just more focused.

167
00:15:48,980 --> 00:15:52,980
 One less thing to concern yourself with.

168
00:15:52,980 --> 00:15:56,170
 How can one begin to see their own false judgments when

169
00:15:56,170 --> 00:15:57,980
 they hinder ability even to know their existence?

170
00:15:57,980 --> 00:16:01,990
 Well, having a teacher helps. Having a technique that you

171
00:16:01,990 --> 00:16:03,980
 force yourself to do helps,

172
00:16:03,980 --> 00:16:07,980
 because it forces you out of your comfort zone.

173
00:16:07,980 --> 00:16:11,680
 But we have this unique ability, or this special, or this

174
00:16:11,680 --> 00:16:14,980
 remarkable ability to change.

175
00:16:14,980 --> 00:16:18,980
 We aren't automatons that are stuck in loops.

176
00:16:18,980 --> 00:16:23,980
 We have the ability to reflect and to change.

177
00:16:23,980 --> 00:16:29,210
 Some of us more than others, of course. Some people don't

178
00:16:29,210 --> 00:16:32,980
 have a very strong ability.

179
00:16:32,980 --> 00:16:36,980
 Do you communicate with beings of other realms? No.

180
00:16:36,980 --> 00:16:40,980
 How to go beyond mind?

181
00:16:40,980 --> 00:16:45,980
 Well, let go of it. Stop clinging to mind.

182
00:16:45,980 --> 00:16:48,730
 Audio is a little ahead of the video by about half a second

183
00:16:48,730 --> 00:16:48,980
.

184
00:16:48,980 --> 00:16:57,980
 Well, that's interesting. Don't know why that would be.

185
00:16:57,980 --> 00:17:03,980
 It's a shame, really.

186
00:17:03,980 --> 00:17:10,980
 Are we living in some kind of hologram? Not that I know of.

187
00:17:10,980 --> 00:17:14,980
 Okay, enough questions. That's all for tonight.

188
00:17:14,980 --> 00:17:17,250
 Thank you all for tuning in. Thanks for being a part of our

189
00:17:17,250 --> 00:17:20,980
 little test. Hopefully it continues to work

190
00:17:20,980 --> 00:17:23,980
 and can be tweaked and improved in the future.

