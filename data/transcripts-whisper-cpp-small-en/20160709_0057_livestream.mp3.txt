 Good evening everyone.
 I'm broadcasting live July 8th.
 Today's quote is about sickness.
 Two kinds of sickness.
 It's a nice little quote.
 Two kinds of sickness.
 Physical and mental.
 Kaikou jarouko jeda sikou jarouko.
 And it says there are differences with physical sickness.
 Well, you see people without physical sickness for days,
 weeks, months, years on end.
 But nobody escapes mental sickness even for a moment.
 Except for those who are in light.
 Mental illness is something that most people are plagued by
 moment after moment.
 Day in and day out.
 A type of sickness that everyone carries with them.
 So in Buddhism, mental illness isn't simply the extreme
 cases of clinical depression or anxiety or OCD or phobias.
 Psychoses, all of these things are just extreme.
 Extreme to the extent that an ordinary, un-instructed
 person can recognize them.
 And latch onto them and identify them as a problem.
 But most of the time we don't recognize and identify how
 our mental illness is for what they are.
 Just illness and discord, distress, until we come to med
itate.
 There are many people when they begin to meditate for the
 first time.
 It's like a revelation, a realization that our minds aren't
 quite as healthy as we thought they were.
 That our minds are plagued by...
 ...the illness of greed, the illness of anger, the illness
 of delusion, and the illnesses.
 That really is the illness and the problem that we should
 focus on and should understand Buddhism to focus on more so
 than simply suffering.
 More important is the cause of suffering.
 If we are suffering, well, we can live with it.
 It is possible to be at peace with what we would normally
 term suffering.
 There is no peace for one who has mental illness.
 The suffering comes because we have mental illness.
 And it's really suffering.
 It becomes suffering because of our mental illness, because
 of our greed, our anger, our delusion.
 I will put in another place, let's go on the subject of
 sickness.
 There are about four types of sickness.
 Sickness from utu, which means the environment.
 Sickness from food, hahara.
 Sickness from the mind, jitta, or jeda-sikamim.
 And sickness from karma, kamma.
 Four types of sickness.
 So in a little more detail, physical sickness is either
 from the environment or from what is around you or from
 what you take inside of you.
 So things like viruses and radiation and environmental
 distress and the environment, discomfort, these would be ut
u.
 Ahara from food, what we eat and drink causes of sickness.
 These are the causes of physical sickness.
 Mental sickness is caused by the mind or caused by karma,
 which is basically the same thing, but karma means in the
 past.
 So you've done nasty things in the past.
 As a result, you have physical and mental impairment.
 Why people are born with certain disabilities, why we
 suddenly get cancer,
 any debilitating disease, multiple sclerosis, etc.
 Not to mention the diseases that come from our greed and
 those are more than the food type.
 But the fourth one from the mind means simply the sickness
 that comes from being mentally sick, from being angry.
 Anytime we're angry, this is considered a mental illness.
 The fact that we have anger is considered to be a mental
 illness.
 When we are addicted to things, when we want things, just
 wanting something is considered to be a mental illness.
 It's a wrong in the mind. It's a cause of suffering for the
 mind.
 It's a cause of stress and distress.
 Anytime we have delusion, arrogance, conceit, or the fact
 that we have these in our mind, this is a cause for illness
.
 It's a type of illness.
 This is the Dhamma of the Buddha. This is how we look at
 things.
 It sounds kind of depressing, I suppose. I've actually had
 people say to me that it's not really fair to call these
 things illness when they're all manageable.
 As though there's a categorical difference between someone
 who is clinically depressed and someone who is just
 depressed.
 There isn't. There's not a categorical difference until you
 create a category.
 Because it's just a matter of degree and eventually the
 perpetuation, the feedback loop that becomes so strong that
 it becomes unmanageable.
 So you dislike something or you're depressed about
 something and that makes you depressed and more depressed
 and depressed.
 You enter this feedback loop where you're actually feeding
 the depression with depression.
 You get angry and then you're angry at the fact that
 someone's made you angry.
 You stew in your anger and it becomes unmanageable.
 Anxiety, you become anxious that you're anxious, afraid of
 your fear and so on.
 You just feed them and by feeding them you end up clinical.
 It's just a matter of degree and so certainly something
 that we should be concerned about and we should be with
 religion towards.
 So it doesn't become unmanageable.
 [Silence]
 No questions today.
 Do we have questions from Australia?
 Why is entertainment dangerous? Understanding the nature of
 it being impermanent and suffering a non-self, what makes
 it dangerous?
 Attachment.
 Got some good talk I suppose.
 Not going to read through it all. Really think you should
 put the cue before the question otherwise it's hard to go
 through here.
 I'm going to have to insist. I'm not going to go back
 through all that talk to find questions.
 So to make a question click on the yellow or green question
 mark.
 That puts the right tag before your question and I can
 easily identify.
 Could my unsatisfying lack of progress in meditation, my
 lack of confidence in seeing the hindrances be due to kamma
?
 Do you have doubt about your practice? Doubt about your
 progress? Because that's a hindrance.
 You can say doubting, doubting.
 It may be not easy to understand what is the progress but
 if you're practicing correctly it's hard to imagine that
 you're not progressing.
 You're not learning to see yourself clearly and to refine
 your character.
 When faced with such continued adversary, violence, racism
 in the world today, how do we teach others to come to
 understand the same benefits of meditation?
 Without seeing unconcerned with our non-reaction.
 If you find that it's considered unjust or cruel not to
 have a fighting opinion when existing in a world that
 demands outrage over every negative situation that arises.
 Well yeah society is messed up, nothing new there.
 We esteem all the wrong things.
 We esteem addiction for what it's worth.
 We certainly esteem opinions, views.
 We esteem outrage.
 We have esteem for things that in the end don't turn out to
 be all that useful.
 We get caught up in things that end up being inconsequ
ential.
 Asa re sara dasinho sare jasara matinoh.
 Asa re sara matinoh sare jasara dasinho.
 We see what is fundamental or what is essential as being un
essential and what is unessential as being essential.
 Dei saarang nadi kath chei nadi kath chit nadi kath chei ya
.
 Mi ca go ca ra sankap.
 Tati kath chantin.
 Such people don't come to it as essential because they're
 always feeding in the wrong pasture.
 They let their minds wander in the wrong pastures.
 So to answer your question, it's not easy to teach yourself
, let alone others.
 This is why we focus on our own well-being.
 You become an example to others.
 People see a better way, and see a way to free themselves
 and to help the world.
 But only if you become an example.
 So work on yourself.
 If you hold fast, if you hold fast to the fact that you're
 not outraged at things,
 there will be people who appreciate that and who esteem
 that and who are affected positively by that.
 Those people who get angry at you, well, it says more about
 them than about you.
 It's sometimes better to let people who are raging let them
 rage.
 Yes, but to be patient with them, unmoved.
 It's really kind of silly to get outraged about everything
 when this sort of thing,
 outrageous things have been sort of the norm of humanity.
 To be surprised by them is naive.
 And to fight for that to change, it's much better to accept
 it as part of the human condition
 and to work with it in the sense of actually trying to
 change,
 not getting angry at the fact that that's part of humanity,
 that there's evil in humanity.
 But to work on good, to better yourself, to better those
 around you,
 to help people to come to let go of their evil inside,
 to help people to see that getting angry at bad things is
 making things worse.
 It's just cultivating a habit of anger.
 You just end up burnt out. You don't believe this theory.
 Look at those people who get burnt out from activism, who
 get burnt out from social justice.
 They don't end up helping people, not in any meaningful way
.
 Well, it's maybe not true. They do often end up,
 but not because of the anger, they often end up, because of
 their determination,
 they'll end up changing the system.
 You don't even be angry to change the system.
 Some of the civil rights movement in America, for example,
 that we hear about,
 a lot of it wasn't very angry.
 It was a matter of just having sit-ins and sitting in the
 wrong place on the bus.
 Just to be adamant.
 I would argue from a Buddhist point of view, but that sort
 of thing is much more,
 much more, a much more, much more, much more acceptable to
 a Buddhist.
 The Buddha himself gave silent protests.
 There's this one case where this king was going, it's a
 long story,
 but actually this guy was a bastard son of one of the
 Buddhist relatives,
 and so they wouldn't recognize him.
 They didn't want to anger him, so they pretended to
 recognize him.
 In the end, they wouldn't even let him eat at the same
 table with them.
 They were so proud.
 He got so angry that he vowed to kill all the Buddhist
 relatives.
 So he went with his army one time, and the Buddha
 intercepted him,
 and the Buddha was sitting there under a tree, but the tree
 had no leaves,
 or very few leaves.
 It was an almost dead tree.
 The king was marching by, and he was told that the Buddha
 was sitting there,
 and so he got down and went to see the Buddha.
 He actually had some faith in the Buddha, even though he
 was a warmongering king.
 He said to the Buddha, "What are you doing?
 Venerable stars sitting under this tree with no shade.
 Why don't you find a better tree?"
 The Buddha said, "Oh, that's okay.
 I live under the cool shade of my relatives."
 So he wouldn't even go to the king and tell him, "Look, you
're wrong.
 Stop it."
 Instead, he sought to remind the king that his relatives
 had good in them.
 They may have been pompous jerks, but they were still
 people.
 They were still a support to humanity in some way, support
 to the Buddha in some way.
 So the king realized, "Oh, the Buddha benefits from his
 relatives."
 So he turned around, called off the war.
 Except he couldn't keep down his anger, so he ended up
 heading off to war again.
 Again, the Buddha did the same thing.
 Then a third time, it's a really interesting story, the
 third time the king says,
 "Look, I'm just going to do it."
 The Buddha sort of reflected on the situation and didn't go
.
 He decided it wasn't going to come to a good end.
 There was nothing he could do to stop it.
 In the end, many of the Buddha's relatives were wiped out,
 the majority of them,
 because the Buddha just let it happen.
 In the end, he felt he couldn't...
 He would have had to get angry, he would have had to get
 upset.
 In a sense, that's...
 You can't stop people from dying.
 You can't stop bad things from happening.
 What we have to stop is things like anger.
 So getting angry doesn't help.
 It sends the wrong message.
 Much more important is to send a message.
 Stop killing each other.
 To actually try and stop one killing or one catastrophe is
 not all that important.
 Those beings are in cycle, they're born and die and born
 and die.
 It's part of a circle of life.
 But the mind, the mind doesn't die.
 It dies every moment, but the mind of a person keeps going.
 Anyway, sickness.
 All right, we're into questions.
 Only one or two questions.
 Must have answered all the questions last night.
 Well, in that case, have a good evening, everyone.
 Tomorrow I might be busy, I might not...
 I should be here.
 But if I'm not here some days, my apologies.
 I should be here.
 Anyway, have a good night.
