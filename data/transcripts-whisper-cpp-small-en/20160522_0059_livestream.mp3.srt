1
00:00:00,000 --> 00:00:16,600
 Good evening everyone from Broadcasting Live, May 21st 2016

2
00:00:16,600 --> 00:00:16,600
.

3
00:00:16,600 --> 00:00:20,800
 So I think there was a bit of confusion or maybe even

4
00:00:20,800 --> 00:00:26,840
 disagreement about when is Wesakapuja,

5
00:00:26,840 --> 00:00:33,160
 Wesak, the Buddha's birthday.

6
00:00:33,160 --> 00:00:38,710
 We counted according to the full moon, so the celebration

7
00:00:38,710 --> 00:00:42,160
 is on the full moon in Wesaka.

8
00:00:42,160 --> 00:00:44,160
 Wesaka is the month.

9
00:00:44,160 --> 00:00:52,760
 Wesaka, I guess, Wesaka, Wesaka in Sanskrit.

10
00:00:52,760 --> 00:00:54,080
 That's the name of the month.

11
00:00:54,080 --> 00:01:06,680
 So on the full moon, which is today, was about three hours

12
00:01:06,680 --> 00:01:11,920
 ago, four hours ago.

13
00:01:11,920 --> 00:01:14,920
 So this is Wesakapuja.

14
00:01:14,920 --> 00:01:20,800
 Puja means the celebration or the holiday.

15
00:01:20,800 --> 00:01:28,820
 This is the holiday or the holy day of Wesaka.

16
00:01:28,820 --> 00:01:33,170
 And it said that that was the day that the bodhisattva was

17
00:01:33,170 --> 00:01:33,880
 born.

18
00:01:33,880 --> 00:01:38,110
 It's the day the bodhisattva became a Buddha and it's the

19
00:01:38,110 --> 00:01:41,520
 day the Buddha passed into Parinibbana.

20
00:01:41,520 --> 00:01:47,880
 That's what they say.

21
00:01:47,880 --> 00:01:50,840
 So it's a good opportunity to talk today.

22
00:01:50,840 --> 00:01:56,800
 I think we'll ignore the quote on our website, whatever

23
00:01:56,800 --> 00:01:58,040
 that is.

24
00:01:58,040 --> 00:02:04,610
 Actually, it's a really good quote, but maybe we can delve

25
00:02:04,610 --> 00:02:06,560
 into it later.

26
00:02:06,560 --> 00:02:10,040
 We'll talk a little bit about the Buddha.

27
00:02:10,040 --> 00:02:11,760
 Why a Buddha?

28
00:02:11,760 --> 00:02:14,760
 What's important about a Buddha?

29
00:02:14,760 --> 00:02:18,320
 Why Buddhism?

30
00:02:18,320 --> 00:02:22,800
 Why do we have to be so concerned about our minds?

31
00:02:22,800 --> 00:02:29,400
 Why do we have to work so hard to train our minds?

32
00:02:29,400 --> 00:02:34,910
 There's so much good in the world, so much happiness in the

33
00:02:34,910 --> 00:02:37,680
 world, so much pleasure in

34
00:02:37,680 --> 00:02:38,680
 the world.

35
00:02:38,680 --> 00:02:41,920
 Why can't we just be content the way things are?

36
00:02:41,920 --> 00:02:42,920
 Isn't it?

37
00:02:42,920 --> 00:02:44,920
 Isn't that enough?

38
00:02:44,920 --> 00:02:53,320
 Isn't that good enough?

39
00:02:53,320 --> 00:03:04,420
 The answer is it's not all happiness and pleasure in this

40
00:03:04,420 --> 00:03:06,360
 world.

41
00:03:06,360 --> 00:03:08,600
 We're not perfect.

42
00:03:08,600 --> 00:03:13,040
 We have problems.

43
00:03:13,040 --> 00:03:16,160
 We get angry.

44
00:03:16,160 --> 00:03:19,360
 We get obsessed.

45
00:03:19,360 --> 00:03:23,900
 We get anxious and worried and afraid and arrogant and

46
00:03:23,900 --> 00:03:26,920
 conceited and we make mistakes.

47
00:03:26,920 --> 00:03:28,040
 We hurt ourselves.

48
00:03:28,040 --> 00:03:29,040
 We hurt others.

49
00:03:29,040 --> 00:03:33,120
 We are confused and afraid.

50
00:03:33,120 --> 00:03:35,920
 We get lost.

51
00:03:35,920 --> 00:03:36,920
 We make mistakes.

52
00:03:36,920 --> 00:03:40,760
 We make bad decisions.

53
00:03:40,760 --> 00:03:41,760
 We suffer.

54
00:03:41,760 --> 00:03:42,760
 We feel pain.

55
00:03:42,760 --> 00:03:43,760
 We get sick.

56
00:03:43,760 --> 00:03:44,760
 We get old.

57
00:03:44,760 --> 00:03:45,760
 We die.

58
00:03:45,760 --> 00:03:50,200
 These are all parts of life, a part of life.

59
00:03:50,200 --> 00:03:52,680
 They're there.

60
00:03:52,680 --> 00:03:53,680
 They exist.

61
00:03:53,680 --> 00:03:59,960
 They are.

62
00:03:59,960 --> 00:04:02,280
 And anyone who thinks that these are just an inevitable

63
00:04:02,280 --> 00:04:03,400
 part of life that we should

64
00:04:03,400 --> 00:04:12,680
 just accept and take the good with the bad.

65
00:04:12,680 --> 00:04:18,920
 As though there were no need to delve deeper or to respond.

66
00:04:18,920 --> 00:04:22,280
 There was no need to train.

67
00:04:22,280 --> 00:04:26,760
 No need to better oneself.

68
00:04:26,760 --> 00:04:33,300
 There's a person who's misleading themselves because really

69
00:04:33,300 --> 00:04:36,960
 that's our whole life.

70
00:04:36,960 --> 00:04:42,590
 Our lives from the time that we're born are spent learning

71
00:04:42,590 --> 00:04:44,000
 just this.

72
00:04:44,000 --> 00:04:48,240
 How to better ourselves.

73
00:04:48,240 --> 00:04:51,770
 Not just means become better people but to be happier

74
00:04:51,770 --> 00:04:52,560
 people.

75
00:04:52,560 --> 00:04:53,960
 How can we find happiness?

76
00:04:53,960 --> 00:04:56,200
 This is what our life is about.

77
00:04:56,200 --> 00:05:01,820
 It's about learning and learning how to cope and deal with

78
00:05:01,820 --> 00:05:03,280
 suffering.

79
00:05:03,280 --> 00:05:08,200
 We're bombarded by so many kinds of challenge and

80
00:05:08,200 --> 00:05:12,040
 difficulty and stress and suffering from

81
00:05:12,040 --> 00:05:13,040
 childhood.

82
00:05:13,040 --> 00:05:15,600
 Our whole lives have been this way.

83
00:05:15,600 --> 00:05:20,160
 This doesn't start when you come to practice meditation.

84
00:05:20,160 --> 00:05:22,880
 We've been asking these questions and finding answers to

85
00:05:22,880 --> 00:05:24,080
 them our whole lives.

86
00:05:24,080 --> 00:05:26,720
 That's what life is.

87
00:05:26,720 --> 00:05:32,600
 Life isn't ever just about a vacation or enjoyment.

88
00:05:32,600 --> 00:05:36,840
 You want to enjoy you have to figure out how to enjoy how

89
00:05:36,840 --> 00:05:38,040
 to be happy.

90
00:05:38,040 --> 00:05:41,560
 How to acquire the things that you want.

91
00:05:41,560 --> 00:05:47,280
 How to escape the things that you don't want.

92
00:05:47,280 --> 00:05:53,240
 Spend our lives bettering ourselves.

93
00:05:53,240 --> 00:05:56,320
 Some of us mostly we don't find the answers.

94
00:05:56,320 --> 00:05:58,320
 Mostly we stumble.

95
00:05:58,320 --> 00:06:01,560
 We're blind.

96
00:06:01,560 --> 00:06:03,360
 We're not equipped.

97
00:06:03,360 --> 00:06:10,920
 We're not capable of finding the answers ourselves.

98
00:06:10,920 --> 00:06:16,400
 So every so often a being arises in the world who is

99
00:06:16,400 --> 00:06:17,800
 capable.

100
00:06:17,800 --> 00:06:24,320
 We are all of lesser or greater faculties.

101
00:06:24,320 --> 00:06:28,960
 There's nothing to be ashamed of.

102
00:06:28,960 --> 00:06:33,800
 It's not to despair but to be realistic.

103
00:06:33,800 --> 00:06:40,320
 We're none of us fully cognizant of our situation, fully

104
00:06:40,320 --> 00:06:43,800
 wise to the ways of our minds and the

105
00:06:43,800 --> 00:06:46,880
 ways of the world.

106
00:06:46,880 --> 00:06:47,880
 We stumble.

107
00:06:47,880 --> 00:06:48,880
 We make mistakes.

108
00:06:48,880 --> 00:06:49,880
 We're confused.

109
00:06:49,880 --> 00:06:55,080
 We're ignorant to so many things.

110
00:06:55,080 --> 00:06:58,480
 But a person arises who is not ignorant, who is able

111
00:06:58,480 --> 00:07:01,080
 through their own striving, through

112
00:07:01,080 --> 00:07:06,160
 their own effort to find the answers.

113
00:07:06,160 --> 00:07:10,600
 And then they share it.

114
00:07:10,600 --> 00:07:16,720
 They spread this teaching.

115
00:07:16,720 --> 00:07:18,200
 Buddhism is essential.

116
00:07:18,200 --> 00:07:20,480
 Buddhism by its very definition.

117
00:07:20,480 --> 00:07:24,750
 I think the word Buddhism it sounds like, but it's just one

118
00:07:24,750 --> 00:07:26,720
 of the many religions.

119
00:07:26,720 --> 00:07:28,920
 Buddhism is not the word Buddha.

120
00:07:28,920 --> 00:07:33,440
 It isn't a name like Charlie or Frank.

121
00:07:33,440 --> 00:07:38,120
 Buddha means one who is awake or one who knows.

122
00:07:38,120 --> 00:07:39,640
 One who understands reality.

123
00:07:39,640 --> 00:07:42,960
 That's what Buddha means.

124
00:07:42,960 --> 00:07:49,360
 So it shouldn't be a specialized religion where some people

125
00:07:49,360 --> 00:07:52,160
 will become Buddhist and

126
00:07:52,160 --> 00:07:53,160
 some people won't.

127
00:07:53,160 --> 00:07:56,100
 Some people become Christians and some people become Buddh

128
00:07:56,100 --> 00:07:57,840
ists and it's up to you to choose.

129
00:07:57,840 --> 00:07:58,840
 It's not really like that.

130
00:07:58,840 --> 00:08:04,330
 Buddhism is for everyone of any religion, of any persuasion

131
00:08:04,330 --> 00:08:06,680
, anyone living any kind

132
00:08:06,680 --> 00:08:10,320
 of life.

133
00:08:10,320 --> 00:08:12,950
 Their success and their failure, their happiness and their

134
00:08:12,950 --> 00:08:13,640
 suffering.

135
00:08:13,640 --> 00:08:18,640
 It's going to be directly related to how close they are to

136
00:08:18,640 --> 00:08:20,520
 Buddha, how close they are to

137
00:08:20,520 --> 00:08:33,330
 awakening, how close they are to science, to knowledge, to

138
00:08:33,330 --> 00:08:39,360
 understanding, to the answers,

139
00:08:39,360 --> 00:08:47,920
 to living their life in a way that makes them happy.

140
00:08:47,920 --> 00:08:50,040
 We think we're happy.

141
00:08:50,040 --> 00:08:53,600
 We have happiness.

142
00:08:53,600 --> 00:08:55,760
 We think so much about happiness.

143
00:08:55,760 --> 00:08:59,320
 You might say we're obsessed.

144
00:08:59,320 --> 00:09:01,240
 You don't see it until you come to meditate, but when you

145
00:09:01,240 --> 00:09:02,320
 come to meditate you see how

146
00:09:02,320 --> 00:09:04,720
 obsessed we are.

147
00:09:04,720 --> 00:09:08,440
 And it seems like, well, you could just get what you want.

148
00:09:08,440 --> 00:09:10,320
 Why do I have to come to meditate?

149
00:09:10,320 --> 00:09:13,320
 Just get what I want.

150
00:09:13,320 --> 00:09:16,320
 But it's not that simple.

151
00:09:16,320 --> 00:09:20,760
 You can't always get what you want.

152
00:09:20,760 --> 00:09:25,200
 It's not just a trite saying.

153
00:09:25,200 --> 00:09:26,320
 It's beyond that.

154
00:09:26,320 --> 00:09:36,160
 You often get what you don't want to such a degree that you

155
00:09:36,160 --> 00:09:38,080
 moan and you wail and you

156
00:09:38,080 --> 00:09:43,210
 lament and you cry and you scream and you shake your fists

157
00:09:43,210 --> 00:09:45,560
 and you beat your breasts

158
00:09:45,560 --> 00:09:52,960
 and all these things.

159
00:09:52,960 --> 00:09:56,910
 People go crazy, kill themselves because they can't take

160
00:09:56,910 --> 00:09:58,200
 the suffering.

161
00:09:58,200 --> 00:09:59,200
 This is part of life.

162
00:09:59,200 --> 00:10:00,920
 It's part of our daily life.

163
00:10:00,920 --> 00:10:09,720
 We make mistakes because suffering for ourselves and others

164
00:10:09,720 --> 00:10:10,080
.

165
00:10:10,080 --> 00:10:12,520
 Buddhism is not optional.

166
00:10:12,520 --> 00:10:16,120
 Buddhism is the right way to live our lives.

167
00:10:16,120 --> 00:10:20,170
 We're trying, you know, we're trying always to live our

168
00:10:20,170 --> 00:10:21,680
 lives in a better way.

169
00:10:21,680 --> 00:10:24,720
 Happiness for us, happiness for others.

170
00:10:24,720 --> 00:10:25,960
 We can't succeed.

171
00:10:25,960 --> 00:10:27,600
 We can't do it.

172
00:10:27,600 --> 00:10:28,600
 We fail.

173
00:10:28,600 --> 00:10:29,600
 Mostly.

174
00:10:29,600 --> 00:10:34,760
 Maybe we get by so we can be happy.

175
00:10:34,760 --> 00:10:37,560
 But that happiness is qualified.

176
00:10:37,560 --> 00:10:45,940
 It's balanced with a lot of stress and suffering and angst

177
00:10:45,940 --> 00:10:50,040
 and anxiety and anguish.

178
00:10:50,040 --> 00:10:53,760
 And that's not something you should settle for.

179
00:10:53,760 --> 00:10:55,280
 It's not something we ever settle for.

180
00:10:55,280 --> 00:10:56,280
 We don't.

181
00:10:56,280 --> 00:10:57,280
 You can say, "Oh, well, you take the..."

182
00:10:57,280 --> 00:11:00,880
 It's not how the world works.

183
00:11:00,880 --> 00:11:02,360
 We're always learning.

184
00:11:02,360 --> 00:11:03,520
 We're always striving.

185
00:11:03,520 --> 00:11:08,720
 We're always aiming for the top.

186
00:11:08,720 --> 00:11:10,280
 We all want to be happy all the time.

187
00:11:10,280 --> 00:11:11,280
 There's no question.

188
00:11:11,280 --> 00:11:14,600
 I think it's impossible, but that's how we live our lives.

189
00:11:14,600 --> 00:11:20,800
 We're always trying to be happy.

190
00:11:20,800 --> 00:11:25,490
 The claim we make is that it's not quite as easy as it

191
00:11:25,490 --> 00:11:28,280
 sounds or as it seems, but that

192
00:11:28,280 --> 00:11:30,160
 there is a way.

193
00:11:30,160 --> 00:11:35,120
 If you work, if you work to straighten your mind of all of

194
00:11:35,120 --> 00:11:37,520
 its crookedness and all of

195
00:11:37,520 --> 00:11:47,780
 its clinginess, to straighten and purify your mind, train

196
00:11:47,780 --> 00:11:52,920
 yourself to see things as they

197
00:11:52,920 --> 00:11:56,960
 are, not how you wish they were.

198
00:11:56,960 --> 00:12:03,870
 Train yourself to accept, not accept, but tolerate, to

199
00:12:03,870 --> 00:12:08,480
 stand strong in the face of anything,

200
00:12:08,480 --> 00:12:13,920
 to be undisturbed by the vicissitudes of life.

201
00:12:13,920 --> 00:12:16,160
 You can train yourself in that.

202
00:12:16,160 --> 00:12:17,160
 You're invincible.

203
00:12:17,160 --> 00:12:20,960
 You can be happy all the time.

204
00:12:20,960 --> 00:12:24,760
 You can be at peace no matter what comes.

205
00:12:24,760 --> 00:12:28,840
 You can conquer all of life's problems and challenges and

206
00:12:28,840 --> 00:12:30,200
 difficulties.

207
00:12:30,200 --> 00:12:36,000
 But it was a very special person.

208
00:12:36,000 --> 00:12:43,670
 Today we honor his memory as someone who taught something

209
00:12:43,670 --> 00:12:48,200
 that for those who have practiced

210
00:12:48,200 --> 00:12:54,430
 it clearly does lead to peace, happiness, and freedom from

211
00:12:54,430 --> 00:12:55,960
 suffering.

212
00:12:55,960 --> 00:12:58,360
 It leads, it's an honest path.

213
00:12:58,360 --> 00:13:00,360
 It's a straight path.

214
00:13:00,360 --> 00:13:03,240
 When you're no longer crooked, you're no longer fooling

215
00:13:03,240 --> 00:13:04,720
 yourself, you're no longer

216
00:13:04,720 --> 00:13:09,040
 deluding yourself, you're no longer making mistakes, you're

217
00:13:09,040 --> 00:13:13,440
 no longer choosing the wrong

218
00:13:13,440 --> 00:13:20,830
 response to problems, to challenges, where you respond

219
00:13:20,830 --> 00:13:31,040
 properly, mindfully, purely, peacefully.

220
00:13:31,040 --> 00:13:34,290
 So the three qualities of the Buddha that we remember on

221
00:13:34,290 --> 00:13:39,000
 this day are, first of all,

222
00:13:39,000 --> 00:13:40,000
 his wisdom.

223
00:13:40,000 --> 00:13:43,600
 This is what we were talking about.

224
00:13:43,600 --> 00:13:48,880
 The Buddha understood the truth and he understood the way

225
00:13:48,880 --> 00:13:50,800
 to find the truth.

226
00:13:50,800 --> 00:13:52,600
 He was able to teach the truth.

227
00:13:52,600 --> 00:13:58,270
 He had great wisdom to understand and to see, at its very

228
00:13:58,270 --> 00:14:01,280
 essence, what does it mean to

229
00:14:01,280 --> 00:14:02,280
 be wise?

230
00:14:02,280 --> 00:14:07,080
 It's a very specific thing in Buddhism.

231
00:14:07,080 --> 00:14:10,740
 It means to see everything that arises and ceases,

232
00:14:10,740 --> 00:14:13,720
 everything that exists in this world,

233
00:14:13,720 --> 00:14:22,760
 everything that arises is not permanent, it's not stable,

234
00:14:22,760 --> 00:14:27,320
 it's not predictable, that everything

235
00:14:27,320 --> 00:14:30,480
 that arises is unsatisfying.

236
00:14:30,480 --> 00:14:33,680
 It's not a source of happiness.

237
00:14:33,680 --> 00:14:36,400
 Happiness can't come from a thing.

238
00:14:36,400 --> 00:14:39,600
 No thing can make you happy because it's impermanent.

239
00:14:39,600 --> 00:14:41,440
 It won't satisfy you.

240
00:14:41,440 --> 00:14:48,340
 If you try to find happiness in it, you will suffer

241
00:14:48,340 --> 00:14:53,200
 disappointment when it's gone.

242
00:14:53,200 --> 00:14:57,200
 And that everything in the world inside of ourselves and

243
00:14:57,200 --> 00:14:59,600
 the world around us, it doesn't

244
00:14:59,600 --> 00:15:08,270
 belong to us, doesn't have its own entity or existence, isn

245
00:15:08,270 --> 00:15:12,320
't a thing that you can possess,

246
00:15:12,320 --> 00:15:16,400
 doesn't belong to a thing, doesn't belong to anyone, which

247
00:15:16,400 --> 00:15:18,280
 means you can't control.

248
00:15:18,280 --> 00:15:21,120
 There's no control over things.

249
00:15:21,120 --> 00:15:30,410
 Nothing you can control and change so that you keep it away

250
00:15:30,410 --> 00:15:31,040
.

251
00:15:31,040 --> 00:15:33,320
 Everything is in a constant flux.

252
00:15:33,320 --> 00:15:37,450
 Seeing, hearing, smelling, tasting, feeling, thinking that

253
00:15:37,450 --> 00:15:39,040
's really all there is.

254
00:15:39,040 --> 00:15:43,280
 When you see a person, the person isn't real.

255
00:15:43,280 --> 00:15:44,280
 The seeing is real.

256
00:15:44,280 --> 00:15:47,280
 When you hear the person, the hearing is real.

257
00:15:47,280 --> 00:15:53,110
 When you taste food, the food isn't real, it's the tasting

258
00:15:53,110 --> 00:15:55,040
 and the feeling.

259
00:15:55,040 --> 00:15:56,040
 It's quite simple.

260
00:15:56,040 --> 00:15:58,960
 The reality is actually quite innocuous.

261
00:15:58,960 --> 00:16:02,120
 There's nothing dangerous or scary.

262
00:16:02,120 --> 00:16:09,310
 If you're being confronted by a scary situation, in the end

263
00:16:09,310 --> 00:16:13,160
 it only comes down to the senses.

264
00:16:13,160 --> 00:16:16,450
 If you're in an argument with someone and you get

265
00:16:16,450 --> 00:16:19,000
 frightened because of how loud they

266
00:16:19,000 --> 00:16:23,500
 are and you get angry because of how vicious they are, you

267
00:16:23,500 --> 00:16:26,320
 remember it's only seeing, hearing,

268
00:16:26,320 --> 00:16:30,200
 smelling, tasting, feeling, thinking.

269
00:16:30,200 --> 00:16:32,520
 Nothing good comes from clinging to it.

270
00:16:32,520 --> 00:16:38,200
 It's impermanent, unsatisfying, uncontrollable.

271
00:16:38,200 --> 00:16:44,480
 You're getting caught up and it doesn't do anything good.

272
00:16:44,480 --> 00:16:47,080
 The first is the wisdom that the Buddha taught.

273
00:16:47,080 --> 00:16:52,210
 All of the teachings on the Four Foundations of Mindfulness

274
00:16:52,210 --> 00:16:55,080
 and how to see things clearly.

275
00:16:55,080 --> 00:17:01,200
 The second is the purity of the Buddha.

276
00:17:01,200 --> 00:17:05,100
 Through his wisdom he found purity and that's what we all

277
00:17:05,100 --> 00:17:06,200
 strive for.

278
00:17:06,200 --> 00:17:10,500
 We strive through our wisdom and understanding to have the

279
00:17:10,500 --> 00:17:12,880
 same pure mind of the Buddha.

280
00:17:12,880 --> 00:17:16,580
 We don't get greedy or we don't get angry or we don't get

281
00:17:16,580 --> 00:17:18,880
 afraid or anxious or depressed

282
00:17:18,880 --> 00:17:29,070
 or worried or confused, arrogant, conceited, sad,

283
00:17:29,070 --> 00:17:31,960
 frustrated.

284
00:17:31,960 --> 00:17:36,800
 None of these things come to us.

285
00:17:36,800 --> 00:17:42,800
 We have a mind that is well trained and it's well tamed.

286
00:17:42,800 --> 00:17:45,800
 We have peace.

287
00:17:45,800 --> 00:17:49,830
 And the third quality of the Buddha that's not universal

288
00:17:49,830 --> 00:17:52,080
 and it's not something we all

289
00:17:52,080 --> 00:17:55,240
 strive for is his compassion.

290
00:17:55,240 --> 00:17:58,140
 We don't all strive for the same level of compassion of the

291
00:17:58,140 --> 00:17:58,760
 Buddha.

292
00:17:58,760 --> 00:18:01,520
 Some do.

293
00:18:01,520 --> 00:18:05,200
 But we praise the Buddha for it and we thank him for it and

294
00:18:05,200 --> 00:18:06,360
 we revere him.

295
00:18:06,360 --> 00:18:10,520
 We feel gratitude because he didn't have to teach.

296
00:18:10,520 --> 00:18:16,540
 He didn't even have to spend all the time becoming capable

297
00:18:16,540 --> 00:18:18,160
 of teaching.

298
00:18:18,160 --> 00:18:22,280
 He had opportunities to follow the teachings of another

299
00:18:22,280 --> 00:18:24,120
 Buddha but he didn't.

300
00:18:24,120 --> 00:18:28,130
 Instead he went on to cultivate his own perfection so he

301
00:18:28,130 --> 00:18:30,840
 could find it by himself in a time when

302
00:18:30,840 --> 00:18:31,840
 there was no Buddha.

303
00:18:31,840 --> 00:18:35,480
 And so that's what we're left with.

304
00:18:35,480 --> 00:18:42,030
 That's what allows us now, 2,500 years later, to still

305
00:18:42,030 --> 00:18:45,480
 practice because he took the time

306
00:18:45,480 --> 00:18:50,670
 to cultivate his own perfection so that he could teach on

307
00:18:50,670 --> 00:18:51,720
 his own.

308
00:18:51,720 --> 00:18:55,350
 Many people who try to do that, who try to emulate his

309
00:18:55,350 --> 00:18:57,760
 perfection, and many people who

310
00:18:57,760 --> 00:19:02,200
 don't, who are content with following his teachings.

311
00:19:02,200 --> 00:19:07,590
 But there are those who are discontent and who actually

312
00:19:07,590 --> 00:19:10,360
 take the time to stay around

313
00:19:10,360 --> 00:19:13,840
 in samsara for lifetime after lifetime, cultivating their

314
00:19:13,840 --> 00:19:16,160
 own perfections and working to better

315
00:19:16,160 --> 00:19:21,920
 themselves to the point where they too can be a Buddha.

316
00:19:21,920 --> 00:19:23,920
 That's what our Buddha did.

317
00:19:23,920 --> 00:19:28,360
 So we respect him for that.

318
00:19:28,360 --> 00:19:32,460
 Today is a day to think about the Buddha and to practice

319
00:19:32,460 --> 00:19:34,560
 the Buddha's teaching.

320
00:19:34,560 --> 00:19:37,500
 The Buddha said if we care for him we will practice his

321
00:19:37,500 --> 00:19:38,360
 teachings.

322
00:19:38,360 --> 00:19:42,320
 So let's all take this as an opportunity to better

323
00:19:42,320 --> 00:19:45,360
 ourselves and our respect for the Buddha

324
00:19:45,360 --> 00:19:46,360
 to practice his teachings.

325
00:19:46,360 --> 00:20:03,680
 So that's the Dhamma for tonight, when we got here.

326
00:20:03,680 --> 00:20:04,920
 Some questions.

327
00:20:04,920 --> 00:20:11,400
 We're going to restructure this site.

328
00:20:11,400 --> 00:20:15,310
 Hopefully in the next week we're going to recreate this

329
00:20:15,310 --> 00:20:17,000
 site in a new format.

330
00:20:17,000 --> 00:20:23,960
 It'll look a little nicer and it'll be a little more

331
00:20:23,960 --> 00:20:27,880
 professional, I guess.

332
00:20:27,880 --> 00:20:31,170
 Is taking refuge in the triple gem required for stream

333
00:20:31,170 --> 00:20:31,840
 entry?

334
00:20:31,840 --> 00:20:36,130
 If not, then what is the minimum requirement for stream

335
00:20:36,130 --> 00:20:36,840
 entry?

336
00:20:36,840 --> 00:20:39,560
 Someone who is a Sotapana has perfect faith in the Buddha,

337
00:20:39,560 --> 00:20:41,160
 the Dhamma, and the Sangha.

338
00:20:41,160 --> 00:20:46,460
 So it's not about actually a ceremony like taking refuge,

339
00:20:46,460 --> 00:20:48,840
 but they do naturally.

340
00:20:48,840 --> 00:20:52,600
 But all that's required for stream entry is seeing Nibbana.

341
00:20:52,600 --> 00:20:55,420
 That's what happens.

342
00:20:55,420 --> 00:20:59,400
 Seeing the three characteristics so clearly that the mind

343
00:20:59,400 --> 00:21:01,520
 lets go, enters into Nibbana

344
00:21:01,520 --> 00:21:02,880
 for the first time.

345
00:21:02,880 --> 00:21:23,760
 That's what makes someone a stream entry, a Sotapana.

346
00:21:23,760 --> 00:21:27,140
 How do you stay in a peaceful state without being

347
00:21:27,140 --> 00:21:30,120
 distracted, especially in society?

348
00:21:30,120 --> 00:21:33,480
 Well, come and do a meditation course.

349
00:21:33,480 --> 00:21:34,880
 We'll teach you how.

350
00:21:34,880 --> 00:21:37,560
 It's not something I can just say to you.

351
00:21:37,560 --> 00:21:39,560
 You should read my booklet if you haven't.

352
00:21:39,560 --> 00:21:42,280
 You should come and do a meditation course with us.

353
00:21:42,280 --> 00:21:43,280
 Stay with us.

354
00:21:43,280 --> 00:21:44,280
 We'll feed you.

355
00:21:44,280 --> 00:21:46,440
 How does you teach you all for free?

356
00:21:46,440 --> 00:21:53,640
 And then you'll have your answer.

357
00:21:53,640 --> 00:21:56,120
 Does Bhanga mean vanishing without remainder?

358
00:21:56,120 --> 00:22:00,770
 If so, does it go against the view that energy can either

359
00:22:00,770 --> 00:22:03,120
 be created or destroyed?

360
00:22:03,120 --> 00:22:06,840
 Sangha, you and your questions.

361
00:22:06,840 --> 00:22:09,560
 Why do you torture me so?

362
00:22:09,560 --> 00:22:11,480
 Bhanga, yes.

363
00:22:11,480 --> 00:22:15,200
 Bhanga means vanishing without remainder.

364
00:22:15,200 --> 00:22:18,960
 And yes, I guess it goes against.

365
00:22:18,960 --> 00:22:25,410
 I mean, I think it does go against the view that energy can

366
00:22:25,410 --> 00:22:29,000
 either be created or destroyed.

367
00:22:29,000 --> 00:22:30,640
 But it's a whole, it's a different worldview.

368
00:22:30,640 --> 00:22:32,920
 We don't talk in terms of energy.

369
00:22:32,920 --> 00:22:35,080
 Energy isn't a thing according to Buddhism.

370
00:22:35,080 --> 00:22:36,800
 It doesn't exist.

371
00:22:36,800 --> 00:22:44,980
 All that exists are experiences, and experiences arise and

372
00:22:44,980 --> 00:22:46,320
 cease.

373
00:22:46,320 --> 00:22:48,870
 What's the official name of the tradition of Mahasya Sayad

374
00:22:48,870 --> 00:22:49,200
aw?

375
00:22:49,200 --> 00:22:54,440
 We call it the Mahasya Sayadaw tradition.

376
00:22:54,440 --> 00:22:58,760
 Some people call it Satipatthana Vipassana.

377
00:22:58,760 --> 00:23:00,760
 Some people call it Theravada.

378
00:23:00,760 --> 00:23:13,600
 Burmese Vipassana, Burmese Satipatthana.

379
00:23:13,600 --> 00:23:20,480
 I think Guadalupe has an autocorrect problem.

380
00:23:20,480 --> 00:23:26,400
 Do the bushes have all the 32 marks of heat, man?

381
00:23:26,400 --> 00:23:30,240
 You have to be careful before you submit.

382
00:23:30,240 --> 00:23:36,590
 I think you mean, do the Buddhas have all the 32 marks of

383
00:23:36,590 --> 00:23:38,480
 the great man?

384
00:23:38,480 --> 00:23:58,640
 Buddha apparently has 32, 32 marks, 32 characteristics.

385
00:23:58,640 --> 00:24:00,240
 You guys don't have to stick around for this.

386
00:24:00,240 --> 00:24:03,360
 You can if you want, but I'm just answering questions.

387
00:24:03,360 --> 00:24:04,360
 Okay.

388
00:24:04,360 --> 00:24:09,360
 Just so you know.

389
00:24:09,360 --> 00:24:10,360
 I've got two meditators here.

390
00:24:10,360 --> 00:24:26,040
 One Michael and one meditator.

391
00:24:26,040 --> 00:24:30,240
 So this week we'll try to work on the website.

392
00:24:30,240 --> 00:24:35,240
 Saturday, next Saturday is the Way Suck celebration.

393
00:24:35,240 --> 00:24:37,120
 This is Saga.

394
00:24:37,120 --> 00:24:45,720
 And then a few days after that I'm off to Thailand.

395
00:24:45,720 --> 00:24:50,090
 We've got someone coming to stay in the house while I'm

396
00:24:50,090 --> 00:24:50,880
 away.

397
00:24:50,880 --> 00:24:55,860
 Jason who's here in Hamilton, he's on our Buddhism

398
00:24:55,860 --> 00:24:57,520
 Association.

399
00:24:57,520 --> 00:25:01,050
 He said he'll come and stay in the house while I'm away,

400
00:25:01,050 --> 00:25:02,240
 which is good.

401
00:25:02,240 --> 00:25:03,240
 Good to hear.

402
00:25:03,240 --> 00:25:05,960
 But we still need someone who can come long term to stay

403
00:25:05,960 --> 00:25:06,600
 with us.

404
00:25:06,600 --> 00:25:10,270
 Someone who can help look after the meditators, who wants

405
00:25:10,270 --> 00:25:13,440
 to spend some time, months, in a

406
00:25:13,440 --> 00:25:14,440
 monastery.

407
00:25:14,440 --> 00:25:19,520
 Someone who doesn't have a dog that they have to go and

408
00:25:19,520 --> 00:25:21,000
 look after.

409
00:25:21,000 --> 00:25:23,620
 Does the point of view mean it can't be changed or

410
00:25:23,620 --> 00:25:24,480
 discarded?

411
00:25:24,480 --> 00:25:32,640
 Or does there always have to be a point of view in all

412
00:25:32,640 --> 00:25:35,080
 situations?

413
00:25:35,080 --> 00:25:37,080
 You're making my brain hurt.

414
00:25:37,080 --> 00:25:41,800
 I didn't know about that question.

415
00:25:41,800 --> 00:25:44,840
 A view is a belief.

416
00:25:44,840 --> 00:25:47,840
 So you can have a view that is in line with reality.

417
00:25:47,840 --> 00:25:51,560
 You can have a view that is out of line with reality.

418
00:25:51,560 --> 00:25:54,080
 You don't always have a view.

419
00:25:54,080 --> 00:25:57,570
 Sometimes you do something out of habit, without a view,

420
00:25:57,570 --> 00:26:01,440
 that it's right or wrong or good or

421
00:26:01,440 --> 00:26:02,440
 bad, etc.

422
00:26:02,440 --> 00:26:04,960
 But views do arise and they're occasional.

423
00:26:04,960 --> 00:26:06,840
 Not every mind has a view in it.

424
00:26:06,840 --> 00:26:18,560
 So not every experience has a view.

425
00:26:18,560 --> 00:26:20,480
 We have an uptick today.

426
00:26:20,480 --> 00:26:22,480
 40 people watching today.

427
00:26:22,480 --> 00:26:25,000
 That's a high.

428
00:26:25,000 --> 00:26:30,680
 When I was in Sri Lanka I used to get 40, 50 I think.

429
00:26:30,680 --> 00:26:34,450
 But since I've been back in Canada, it's rare to get 40

430
00:26:34,450 --> 00:26:35,280
 people.

431
00:26:35,280 --> 00:26:38,280
 So hello everyone.

432
00:26:38,280 --> 00:26:42,040
 Welcome.

433
00:26:42,040 --> 00:26:44,060
 If you're wondering where the questions are, if you're

434
00:26:44,060 --> 00:26:45,360
 watching this on YouTube, maybe

435
00:26:45,360 --> 00:26:49,040
 watching it later even, most of what we do is centered

436
00:26:49,040 --> 00:26:52,360
 around our own website at meditation.serimongal.com.

437
00:26:52,360 --> 00:26:57,470
 That's where people are chatting and asking questions and

438
00:26:57,470 --> 00:26:59,720
 meditating together.

439
00:26:59,720 --> 00:27:05,370
 We also have online meditation courses, so people sign up

440
00:27:05,370 --> 00:27:08,560
 for those and meditate together

441
00:27:08,560 --> 00:27:12,560
 online as well.

442
00:27:12,560 --> 00:27:15,560
 Okay.

443
00:27:15,560 --> 00:27:23,160
 So I'm going to say goodnight to everyone.

444
00:27:23,160 --> 00:27:27,160
 I'm sure you're all happy we're Sakapulcha.

445
00:27:27,160 --> 00:27:28,160
 Happy full moon.

446
00:27:28,160 --> 00:27:28,160
 And see you all later.

447
00:27:28,160 --> 00:27:38,160
 [BLANK_AUDIO]

448
00:27:38,160 --> 00:27:48,160
 [BLANK_AUDIO]

