1
00:00:00,000 --> 00:00:03,560
 Samatajjana, Vipassanajjana

2
00:00:03,560 --> 00:00:05,560
 Question.

3
00:00:05,560 --> 00:00:10,200
 What is the difference between Vipassanajjanas and Samataj

4
00:00:10,200 --> 00:00:11,040
janas?

5
00:00:11,040 --> 00:00:16,370
 Is there an advantage of doing straight insight versus Sam

6
00:00:16,370 --> 00:00:17,120
ata?

7
00:00:17,120 --> 00:00:19,720
 Answer.

8
00:00:19,720 --> 00:00:24,850
 Ultimately, the goal of Buddhism is to see the Four Noble

9
00:00:24,850 --> 00:00:25,600
 Truths,

10
00:00:25,840 --> 00:00:30,210
 which can only be seen through the practice of observing

11
00:00:30,210 --> 00:00:32,520
 ultimate reality as it is.

12
00:00:32,520 --> 00:00:38,820
 The ancient commentaries record different ways to go about

13
00:00:38,820 --> 00:00:39,160
 it,

14
00:00:39,160 --> 00:00:44,440
 but you must eventually focus on reality to see the truth.

15
00:00:44,440 --> 00:00:48,760
 If you want to become a fully enlightened Buddha,

16
00:00:48,760 --> 00:00:51,640
 then you have your work cut out for you.

17
00:00:52,000 --> 00:00:56,080
 If you want to become a private Buddha, then you still have

18
00:00:56,080 --> 00:00:58,840
 an incredible amount of work to do.

19
00:00:58,840 --> 00:01:04,040
 If you want to become one of the Buddha's chief disciples,

20
00:01:04,040 --> 00:01:09,520
 you can look to our Buddha's chief disciples as examples.

21
00:01:09,520 --> 00:01:14,800
 There was Mughalana, who took seven days to become an Arah

22
00:01:14,800 --> 00:01:15,160
ant,

23
00:01:15,160 --> 00:01:20,640
 and Sāyaputta, who took fourteen days to become an Arahant

24
00:01:20,640 --> 00:01:21,000
.

25
00:01:22,640 --> 00:01:26,540
 Why did Sāyaputta take longer to become enlightened than M

26
00:01:26,540 --> 00:01:27,720
ughalana,

27
00:01:27,720 --> 00:01:32,070
 when he was the most excellent in wisdom apart from the

28
00:01:32,070 --> 00:01:33,000
 Buddha?

29
00:01:33,000 --> 00:01:37,990
 And why did both of them take longer than those who became

30
00:01:37,990 --> 00:01:39,200
 enlightened

31
00:01:39,200 --> 00:01:42,360
 from simply listening to the Buddha teach?

32
00:01:42,360 --> 00:01:47,550
 It took Sāyaputta and Mughalana longer because they had

33
00:01:47,550 --> 00:01:49,840
 higher aspirations.

34
00:01:50,400 --> 00:01:54,520
 Sāyaputta had exceptional aspirations,

35
00:01:54,520 --> 00:01:59,370
 and it is amazing that in fourteen days, he could

36
00:01:59,370 --> 00:02:01,320
 accomplish what he did.

37
00:02:01,320 --> 00:02:06,680
 He came to understand reality in a very profound way.

38
00:02:06,680 --> 00:02:12,090
 The suttas states that he was able to observe realities

39
00:02:12,090 --> 00:02:15,800
 appearing in sequence one by one.

40
00:02:16,520 --> 00:02:20,360
 You do not need to follow these examples.

41
00:02:20,360 --> 00:02:25,160
 It is totally up to the individual's aspirations.

42
00:02:25,160 --> 00:02:30,200
 Kondanya, who was the Buddha's first enlightened disciple,

43
00:02:30,200 --> 00:02:34,130
 the first person to realize the truth of the Buddha's

44
00:02:34,130 --> 00:02:34,880
 teaching,

45
00:02:34,880 --> 00:02:39,890
 had made a wish in a past life that he should be the first

46
00:02:39,890 --> 00:02:40,200
 person

47
00:02:40,200 --> 00:02:44,480
 to become enlightened in a Buddha's dispensation.

48
00:02:45,600 --> 00:02:50,820
 To cement his aspiration, he would give the first of

49
00:02:50,820 --> 00:02:53,200
 everything as charity.

50
00:02:53,200 --> 00:02:57,760
 He gave away the first rice when it was on ripe.

51
00:02:57,760 --> 00:03:02,680
 He gave away the first rice when it was ripe on the stalk.

52
00:03:02,680 --> 00:03:07,280
 Then he gave away the first harvest of the rice.

53
00:03:07,280 --> 00:03:12,160
 Then he gave away the first threshing of the rice,

54
00:03:12,160 --> 00:03:16,880
 and then the first polishing of the rice and so on.

55
00:03:16,880 --> 00:03:21,180
 He always gave away the first, and then he made the

56
00:03:21,180 --> 00:03:22,160
 determination

57
00:03:22,160 --> 00:03:26,520
 that he should therefore come to see the truth first.

58
00:03:26,520 --> 00:03:32,160
 His brother, at the same time, decided to give away the

59
00:03:32,160 --> 00:03:33,680
 last of the rice,

60
00:03:33,680 --> 00:03:36,680
 the last threshing, etc.

61
00:03:36,680 --> 00:03:40,220
 And then he made the determination to become the last

62
00:03:40,220 --> 00:03:40,800
 person

63
00:03:40,800 --> 00:03:43,280
 to realize the Buddha's teaching.

64
00:03:43,280 --> 00:03:48,880
 The second brother became Subhadha in the time of our

65
00:03:48,880 --> 00:03:49,840
 Buddha,

66
00:03:49,840 --> 00:03:53,430
 the last person to learn the Buddha's teaching from the

67
00:03:53,430 --> 00:03:55,160
 Buddha's own mouth.

68
00:03:55,160 --> 00:04:00,690
 You have to recognize that there are different ways of

69
00:04:00,690 --> 00:04:03,040
 accomplishing the same goal.

70
00:04:03,040 --> 00:04:09,120
 There is even a sutta that is often quoted in which some

71
00:04:09,120 --> 00:04:10,000
 monk says,

72
00:04:10,480 --> 00:04:15,210
 "We do not have magical powers and we have not attained the

73
00:04:15,210 --> 00:04:16,800
 Arupa jhanas.

74
00:04:16,800 --> 00:04:21,840
 All we have are the jhanas required for attaining wisdom.

75
00:04:21,840 --> 00:04:25,200
 We are paññā-vimutti."

76
00:04:25,200 --> 00:04:27,840
 Vimutti means liberated.

77
00:04:27,840 --> 00:04:33,540
 The Buddha and the later commentaries say there are two

78
00:04:33,540 --> 00:04:35,360
 kinds of vimutti,

79
00:04:35,360 --> 00:04:40,000
 paññā-vimutti and citta-vimutti.

80
00:04:40,480 --> 00:04:46,050
 Those who are citta-vimutti practice samatha first and then

81
00:04:46,050 --> 00:04:48,240
 continue with vipassana.

82
00:04:48,240 --> 00:04:54,030
 Paññā-vimutti practice only vipassana and the jhana that

83
00:04:54,030 --> 00:04:55,200
 they attain

84
00:04:55,200 --> 00:04:57,920
 is the bare minimum to see clearly.

85
00:04:57,920 --> 00:05:03,750
 The commentaries are very clear on this and there is really

86
00:05:03,750 --> 00:05:06,800
 no controversy in the text.

87
00:05:07,760 --> 00:05:12,960
 But in modern times some people do not accept this and they

88
00:05:12,960 --> 00:05:16,400
 reinterpret what the Buddha said in new ways.

89
00:05:16,400 --> 00:05:21,510
 Here I will give my understanding about the samatha and vip

90
00:05:21,510 --> 00:05:24,400
assana jhanas according to the commentaries.

91
00:05:24,400 --> 00:05:29,120
 There are in total three kinds of jhana.

92
00:05:29,120 --> 00:05:32,880
 The first kind of jhana is samatha jhana.

93
00:05:32,880 --> 00:05:36,640
 The second kind is vipassana jhana.

94
00:05:37,440 --> 00:05:40,400
 And the third kind is lokuttara jhana.

95
00:05:40,400 --> 00:05:47,550
 To attain samatha jhana, called aramana upani jhana in the

96
00:05:47,550 --> 00:05:48,560
 commentaries,

97
00:05:48,560 --> 00:05:52,080
 one meditates on an aramana.

98
00:05:52,080 --> 00:05:54,400
 Aramana means object.

99
00:05:54,400 --> 00:05:59,840
 But here specifically it refers to a conceptual object

100
00:05:59,840 --> 00:06:04,750
 because only a conceptual object can be made the single

101
00:06:04,750 --> 00:06:08,560
 object of one's focus over a period of time.

102
00:06:08,560 --> 00:06:14,640
 There are 40 standard objects of samatha meditation

103
00:06:14,640 --> 00:06:16,080
 outlined in the text.

104
00:06:16,080 --> 00:06:25,710
 The ten kasinas, which are earth, water, fire, air, blue,

105
00:06:25,710 --> 00:06:27,600
 yellow, red, white,

106
00:06:28,880 --> 00:06:32,480
 enclosed space and consciousness.

107
00:06:32,480 --> 00:06:40,380
 The ten contemplations on loathsome-ness, which are a

108
00:06:40,380 --> 00:06:43,840
 swollen corpse, a discolored corpse,

109
00:06:43,840 --> 00:06:51,110
 a festering corpse, a fistured corpse, a mangled corpse, a

110
00:06:51,110 --> 00:06:53,760
 dismembered corpse,

111
00:06:54,400 --> 00:07:00,850
 a cot and dismembered corpse, a bleeding corpse, a warm inf

112
00:07:00,850 --> 00:07:04,480
ested corpse, a skeleton.

113
00:07:04,480 --> 00:07:13,680
 The ten anusati, the buddha, the dhamma, the sangha,

114
00:07:14,800 --> 00:07:23,320
 morality, generosity, heavenly beings, death, the body, the

115
00:07:23,320 --> 00:07:25,440
 breath, peace.

116
00:07:25,440 --> 00:07:34,820
 The four brahma viharas, kindness, compassion, joy, and equ

117
00:07:34,820 --> 00:07:35,520
animity,

118
00:07:36,640 --> 00:07:42,490
 the four arupa kamatana, limitless space, limitless

119
00:07:42,490 --> 00:07:45,840
 consciousness, nothingness,

120
00:07:45,840 --> 00:07:53,490
 neither perception nor non-perception, ahare patikula sany

121
00:07:53,490 --> 00:07:53,920
am,

122
00:07:55,440 --> 00:08:04,410
 perception of the repulsiveness of food and cha tu datu v

123
00:08:04,410 --> 00:08:08,000
ava tana, differentiating the four

124
00:08:08,000 --> 00:08:12,720
 primary elements of earth, fire, water, and air.

125
00:08:12,720 --> 00:08:21,340
 Focusing on any one of these forty or something similar

126
00:08:21,340 --> 00:08:22,960
 leads to samatha jhana.

127
00:08:23,920 --> 00:08:29,030
 They are all based on concepts, so meditating on these

128
00:08:29,030 --> 00:08:32,880
 objects will not lead to insight directly.

129
00:08:32,880 --> 00:08:38,570
 For example, you cannot expect to gain insight by thinking

130
00:08:38,570 --> 00:08:40,160
 of the color white,

131
00:08:40,160 --> 00:08:45,060
 because white is a concept apart from the experience of

132
00:08:45,060 --> 00:08:46,080
 seeing.

133
00:08:47,120 --> 00:08:52,040
 The only way you can enter into a samatha jhana is if you

134
00:08:52,040 --> 00:08:54,880
 focus on something that is stable,

135
00:08:54,880 --> 00:08:59,540
 and if you focus on something that is stable, you will

136
00:08:59,540 --> 00:09:02,560
 never see impermanent suffering and non-self.

137
00:09:02,560 --> 00:09:05,360
 This is the distinction here.

138
00:09:07,040 --> 00:09:12,370
 Vipassana jhana is called by the commentaries lakhanupani j

139
00:09:12,370 --> 00:09:17,440
hana, which means meditation on the characteristics.

140
00:09:17,440 --> 00:09:23,260
 Lakhana means characteristic, referring to the three

141
00:09:23,260 --> 00:09:26,080
 characteristics of all real,

142
00:09:26,080 --> 00:09:31,520
 a reason phenomena, impermanence, suffering, and non-self.

143
00:09:32,560 --> 00:09:38,600
 Vipassana jhana is based on reality, focusing on a moment

144
00:09:38,600 --> 00:09:41,520
ary experience that arises and ceases.

145
00:09:41,520 --> 00:09:48,330
 Vipassana means to see clearly the nature of reality as

146
00:09:48,330 --> 00:09:51,840
 being unsatisfying, stressful,

147
00:09:51,840 --> 00:09:56,800
 and not worth clinging to. When you see this, you let go.

148
00:09:56,800 --> 00:10:00,000
 When you let go, you are free,

149
00:10:00,800 --> 00:10:04,240
 and when you are free, you can say, "I am free."

150
00:10:04,240 --> 00:10:10,350
 This jhana is necessary to become enlightened because it

151
00:10:10,350 --> 00:10:13,360
 involves the wisdom that leads to letting go.

152
00:10:13,360 --> 00:10:20,840
 It is the only way one can enter into nibhana. Samatha

153
00:10:20,840 --> 00:10:24,000
 meditation was thought and practiced

154
00:10:24,000 --> 00:10:28,690
 before the time of the Buddha, and yet there are Buddhist

155
00:10:28,690 --> 00:10:31,680
 teachers who claim that it was discovered

156
00:10:31,680 --> 00:10:36,300
 by the Buddha. It seems incredible to think that the

157
00:10:36,300 --> 00:10:40,000
 sublime peace and tranquility described by

158
00:10:40,000 --> 00:10:45,010
 ancient ascetics was something other than jhana. Hindu g

159
00:10:45,010 --> 00:10:47,600
urus are experts in jhanas.

160
00:10:48,560 --> 00:10:54,000
 The Buddha truly did discover Vipassana, as can be seen by

161
00:10:54,000 --> 00:10:57,440
 the absence of the three characteristics

162
00:10:57,440 --> 00:11:02,330
 in the teachings of other religions, or even contradiction,

163
00:11:02,330 --> 00:11:05,040
 especially regarding non-self.

164
00:11:05,040 --> 00:11:12,320
 Vipassana leads to the third type of jhana, lokutara jhana.

165
00:11:12,320 --> 00:11:15,360
 The monks who call themselves

166
00:11:16,000 --> 00:11:21,350
 Panya-vimuti specified that they had never developed form

167
00:11:21,350 --> 00:11:24,240
less jhana or rupa jhana,

168
00:11:24,240 --> 00:11:29,660
 which implies that they did attain jhana with form, rupa jh

169
00:11:29,660 --> 00:11:34,880
ana. Rupa here refers to the physical nature

170
00:11:34,880 --> 00:11:40,050
 of the object of meditation. Some say that means even Panya

171
00:11:40,050 --> 00:11:43,280
-vimuti must practice samatha jhana,

172
00:11:43,920 --> 00:11:48,720
 but the real reason for specifying not attaining a rupa jh

173
00:11:48,720 --> 00:11:51,600
ana is because enlightenment itself

174
00:11:51,600 --> 00:11:57,440
 involves a specific sort of jhana called lokutara jhana.

175
00:11:57,440 --> 00:12:03,730
 Lokutara jhana are based on the level of jhana used to

176
00:12:03,730 --> 00:12:09,040
 attain them. If one enters into nibbana

177
00:12:09,040 --> 00:12:14,920
 from the first jhana, this is the first lokutara jhana. If

178
00:12:14,920 --> 00:12:17,840
 one enters from the second jhana,

179
00:12:17,840 --> 00:12:22,560
 it is the second lokutara jhana, and so on.

