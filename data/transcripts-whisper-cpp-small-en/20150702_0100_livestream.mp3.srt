1
00:00:00,000 --> 00:00:14,480
 So good evening.

2
00:00:14,480 --> 00:00:36,600
 I'll take a quick break again for more recording of the D

3
00:00:36,600 --> 00:00:39,080
hamma.

4
00:00:39,080 --> 00:00:45,640
 There's a famous aspect of the Buddhist teaching that he

5
00:00:45,640 --> 00:00:48,440
 redefined the meaning of the word

6
00:00:48,440 --> 00:00:57,990
 "arya" and at the same time the opposite which was a "was

7
00:00:57,990 --> 00:00:58,920
ala".

8
00:00:58,920 --> 00:01:13,320
 Wasala means a person of low birth or an outcast.

9
00:01:13,320 --> 00:01:20,070
 And so in India there were certain people who by their very

10
00:01:20,070 --> 00:01:23,480
 birth were doomed to forever

11
00:01:23,480 --> 00:01:35,280
 live in the lowest rung of society.

12
00:01:35,280 --> 00:01:40,920
 And then there were those people who by virtue of their

13
00:01:40,920 --> 00:01:44,080
 birth were destined to live in the

14
00:01:44,080 --> 00:01:54,240
 higher stratum of society simply because of their birth.

15
00:01:54,240 --> 00:02:00,070
 So the famous quote of the Buddha is that it's not by one's

16
00:02:00,070 --> 00:02:02,400
 birth that one is noble

17
00:02:02,400 --> 00:02:11,240
 and it's not by birth that one is an outcast.

18
00:02:11,240 --> 00:02:16,310
 And I think it's not often heard the Buddhist teaching on

19
00:02:16,310 --> 00:02:19,240
 exactly what it means to be noble

20
00:02:19,240 --> 00:02:22,400
 and what it means to be an outcast.

21
00:02:22,400 --> 00:02:31,720
 So there's a list of things we have.

22
00:02:31,720 --> 00:02:36,800
 Six things that make one a noble and six things that make

23
00:02:36,800 --> 00:02:40,720
 one a wasala, an ariya and a wasala.

24
00:02:40,720 --> 00:02:56,030
 So the text goes, "Godhano upana hi ca papa maki ca yona ro

25
00:02:56,030 --> 00:02:59,280
" "Vipanna di pi maya vi dangjanya

26
00:02:59,280 --> 00:03:06,640
 vasaloi di" That's for being a wasala when it takes to

27
00:03:06,640 --> 00:03:07,640
 be an outcast.

28
00:03:07,640 --> 00:03:14,760
 An outcast is six things, one, "Godhano" someone who gets

29
00:03:14,760 --> 00:03:16,440
 angry.

30
00:03:16,440 --> 00:03:29,640
 "Upana hi" someone who responds with anger or is vengeful.

31
00:03:29,640 --> 00:03:50,180
 "Bapa maki" someone who finds fault in others and depreci

32
00:03:50,180 --> 00:03:53,640
ates their worth.

33
00:03:53,640 --> 00:04:02,800
 This evil tendency of attacking others and belittling

34
00:04:02,800 --> 00:04:04,640
 others.

35
00:04:04,640 --> 00:04:09,640
 "Vipanna di pi" one with wrong view.

36
00:04:09,640 --> 00:04:19,640
 "Maya vi" one who is treacherous, deceitful.

37
00:04:19,640 --> 00:04:23,640
 "Godhano" one who is courageous, deceitful.

38
00:04:23,640 --> 00:04:28,640
 So should one know a person as being a wasala.

39
00:04:28,640 --> 00:04:35,540
 So should one be known as a wasala, an outcast, a low born,

40
00:04:35,540 --> 00:04:44,640
 a low person, a base individual.

41
00:04:44,640 --> 00:04:48,660
 So "Godhano" is obvious when you're an angry person, anger

42
00:04:48,660 --> 00:04:54,640
 is an ignoble mind state.

43
00:04:54,640 --> 00:04:58,640
 It affects both your mind and your body.

44
00:04:58,640 --> 00:05:00,640
 It affects your physical appearance.

45
00:05:00,640 --> 00:05:02,640
 It makes one look ugly.

46
00:05:02,640 --> 00:05:09,640
 And it makes one's mind ugly, one's personality ugly.

47
00:05:09,640 --> 00:05:16,410
 "Upana hi" the same, responding with anger, bearing a gr

48
00:05:16,410 --> 00:05:20,640
udge against someone.

49
00:05:20,640 --> 00:05:24,230
 But I actually said when you're angry in response to

50
00:05:24,230 --> 00:05:28,640
 someone who's angry, it's worse, it's the worst evil.

51
00:05:28,640 --> 00:05:31,640
 To say what they in a pop you.

52
00:05:31,640 --> 00:05:37,640
 Someone who gets angry back is a worse evil, more evil.

53
00:05:37,640 --> 00:05:43,520
 Because that's what creates the argument, that's what

54
00:05:43,520 --> 00:05:46,640
 creates the dispute.

55
00:05:46,640 --> 00:05:51,100
 If someone gets angry and we don't respond then it stops

56
00:05:51,100 --> 00:05:51,640
 there.

57
00:05:51,640 --> 00:05:58,270
 But when you respond with anger, it's when the problem

58
00:05:58,270 --> 00:05:59,640
 arises.

59
00:05:59,640 --> 00:06:06,640
 "Papamaki" is looking down upon others.

60
00:06:06,640 --> 00:06:11,780
 As so called nobles are often wont to do, it actually makes

61
00:06:11,780 --> 00:06:12,640
 them ignoble.

62
00:06:12,640 --> 00:06:17,090
 Some people who are high born in society or have money or

63
00:06:17,090 --> 00:06:17,640
 power,

64
00:06:17,640 --> 00:06:22,640
 and then look down upon others or disparage others.

65
00:06:22,640 --> 00:06:31,640
 This is clearly a sign of the poor quality of mind.

66
00:06:31,640 --> 00:06:39,050
 So we have this clear statement of the Buddha where he

67
00:06:39,050 --> 00:06:40,640
 stands.

68
00:06:40,640 --> 00:06:46,910
 He stands with goodness on the side of the mind that is

69
00:06:46,910 --> 00:06:50,640
 pure and rejoices in the fortune of others,

70
00:06:50,640 --> 00:06:57,640
 rejoices in the goodness of others.

71
00:06:57,640 --> 00:07:03,640
 "Ripanaditi" is someone with wrong views who believes that.

72
00:07:03,640 --> 00:07:06,850
 It's good to get angry, it's good to believe that greed is

73
00:07:06,850 --> 00:07:07,640
 a good thing,

74
00:07:07,640 --> 00:07:11,640
 it's delusion is a good thing.

75
00:07:11,640 --> 00:07:14,700
 And when he believes there is no good in giving gifts,

76
00:07:14,700 --> 00:07:15,640
 there is no good in keeping morality,

77
00:07:15,640 --> 00:07:18,640
 there is no good in meditation.

78
00:07:18,640 --> 00:07:23,640
 Someone who believes there is no past lives or future lives

79
00:07:23,640 --> 00:07:23,640
.

80
00:07:23,640 --> 00:07:28,200
 He is trying to believe it's all wrong views, many

81
00:07:28,200 --> 00:07:31,640
 different kinds of wrong views.

82
00:07:31,640 --> 00:07:42,640
 Belief in the self, belief in you.

83
00:07:42,640 --> 00:07:52,850
 An eternal afterlife, an eternal heaven, these kinds of

84
00:07:52,850 --> 00:07:55,640
 wrong views.

85
00:07:55,640 --> 00:08:03,020
 These are more subtle, it's harder to see it as a sign of

86
00:08:03,020 --> 00:08:07,640
 low, booty baseness.

87
00:08:07,640 --> 00:08:12,060
 But it's actually one of the worst things because wrong

88
00:08:12,060 --> 00:08:20,640
 view is what sets up your direction in life.

89
00:08:20,640 --> 00:08:25,440
 So it informs all of your decisions, it colors all of your

90
00:08:25,440 --> 00:08:26,640
 judgments,

91
00:08:26,640 --> 00:08:36,440
 it determines whether you are inclined to follow after your

92
00:08:36,440 --> 00:08:45,650
 defilements or whether to be wary of your defilements, your

93
00:08:45,650 --> 00:08:47,640
 emotions.

94
00:08:47,640 --> 00:08:53,520
 For those who have wrong views, people with wrong views are

95
00:08:53,520 --> 00:08:56,640
 clearly inferior, not in a judgment sense,

96
00:08:56,640 --> 00:09:00,320
 but they can clearly see that this person has something

97
00:09:00,320 --> 00:09:01,640
 wrong with them.

98
00:09:01,640 --> 00:09:06,140
 Anyone who has right view, is clear and set and established

99
00:09:06,140 --> 00:09:08,640
 in their right view,

100
00:09:08,640 --> 00:09:15,630
 they feel sorry for anyone and they see the disadvantage,

101
00:09:15,630 --> 00:09:23,640
 the fault in the person who has wrong view.

102
00:09:23,640 --> 00:09:34,500
 Ma yawi, ma yawi means someone who is deceitful, someone

103
00:09:34,500 --> 00:09:38,640
 who will trick setters,

104
00:09:38,640 --> 00:09:49,640
 who will act like holy individuals, who are not holy at all

105
00:09:49,640 --> 00:09:49,640
,

106
00:09:49,640 --> 00:09:54,370
 and they lie to others and cheat them out of their wealth

107
00:09:54,370 --> 00:09:56,640
 and that kind of thing.

108
00:09:56,640 --> 00:10:00,790
 Actually you can become quite powerful doing that in a

109
00:10:00,790 --> 00:10:01,640
 worldly sense,

110
00:10:01,640 --> 00:10:11,640
 but the spiritual level is a sign of baseness of mind,

111
00:10:11,640 --> 00:10:16,640
 clearly not a positive quality.

112
00:10:16,640 --> 00:10:22,300
 Eternability is mental, it's refreshing of course to think

113
00:10:22,300 --> 00:10:28,640
 about how clear the Buddha was on this,

114
00:10:28,640 --> 00:10:42,130
 not buying into any kind of exterior, external idea of high

115
00:10:42,130 --> 00:10:43,640
 and low,

116
00:10:43,640 --> 00:10:48,600
 and high and low have simply to do with one's goodness, the

117
00:10:48,600 --> 00:10:50,640
 quality of one's heart.

118
00:10:50,640 --> 00:10:53,640
 That's what you find throughout the Buddha's teaching.

119
00:10:53,640 --> 00:10:58,850
 So on the other side we have akodana, not getting angry, an

120
00:10:58,850 --> 00:11:02,640
upana, he not being vengeful or spiteful,

121
00:11:02,640 --> 00:11:09,780
 or getting angry, when others have been angry, you won't

122
00:11:09,780 --> 00:11:12,640
 respond with anger.

123
00:11:12,640 --> 00:11:19,640
 Amaki do not be, do not be disparaging about us.

124
00:11:19,640 --> 00:11:24,640
 Sudatang khatod who have gone to purity,

125
00:11:24,640 --> 00:11:32,640
 Sampanaditi to have right view, medha we to be wise,

126
00:11:32,640 --> 00:11:37,190
 instead of maya we it's medha we, medha means wisdom, medha

127
00:11:37,190 --> 00:11:39,640
 we, one who has wisdom.

128
00:11:39,640 --> 00:11:45,970
 Wisdom of course is the highest, it's the mark of a high

129
00:11:45,970 --> 00:11:50,640
 individual, it's the height,

130
00:11:50,640 --> 00:11:56,640
 the pinnacle, the highest,

131
00:11:56,640 --> 00:12:02,640
 and wisdom is the highest.

132
00:12:02,640 --> 00:12:07,790
 So just something interesting, this the Buddha said makes

133
00:12:07,790 --> 00:12:08,640
 one an aria,

134
00:12:08,640 --> 00:12:17,250
 and so now you know the difference between the low-born

135
00:12:17,250 --> 00:12:17,640
 person,

136
00:12:17,640 --> 00:12:27,590
 and the high person, it's all in the mind, nature of one's

137
00:12:27,590 --> 00:12:32,640
 heart, quality of one's heart.

138
00:12:32,640 --> 00:12:36,640
 So that's the demo for today, thank you for tuning in.

