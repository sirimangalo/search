1
00:00:00,000 --> 00:00:06,630
 Okay, welcome back to Ask a Monk. Next question on good

2
00:00:06,630 --> 00:00:06,960
 books to study on

3
00:00:06,960 --> 00:00:12,880
 Buddhism and what parts of the tibitaka I would recommend.

4
00:00:12,880 --> 00:00:15,240
 I'm often cautious to

5
00:00:15,240 --> 00:00:19,830
 recommend anything because there's such a wide audience out

6
00:00:19,830 --> 00:00:21,640
 there and I think my

7
00:00:21,640 --> 00:00:34,200
 tastes are rather specific or specific. But if you agree

8
00:00:34,200 --> 00:00:34,840
 with the sorts of

9
00:00:34,840 --> 00:00:41,680
 things that I teach and say and believe and propound, if

10
00:00:41,680 --> 00:00:41,880
 you

11
00:00:41,880 --> 00:00:44,830
 appreciate the meditation teachings that I've posted and

12
00:00:44,830 --> 00:00:45,760
 you think that's a good

13
00:00:45,760 --> 00:00:51,290
 method, then I would recommend to look into similar

14
00:00:51,290 --> 00:00:54,400
 teachings. The best by far

15
00:00:54,400 --> 00:01:00,610
 in my mind that anyone could ever read after they've

16
00:01:00,610 --> 00:01:01,920
 started this sort of

17
00:01:01,920 --> 00:01:08,340
 meditation is the teachings of Mahasi Sayadaw. M-A-H-A-S-I,

18
00:01:08,340 --> 00:01:09,520
 one word and then the

19
00:01:09,520 --> 00:01:16,200
 next word Sayadaw, S-A-D-A-Y-A-W. Sayadaw means teacher,

20
00:01:16,200 --> 00:01:17,440
 Mahasi actually means big

21
00:01:17,440 --> 00:01:20,940
 drum. It was the name of the place where he became a

22
00:01:20,940 --> 00:01:22,000
 teacher but that's

23
00:01:22,000 --> 00:01:24,540
 the name he's known by and he's passed away but he was an

24
00:01:24,540 --> 00:01:25,960
 excellent teacher and

25
00:01:25,960 --> 00:01:32,650
 well revered. There's a website with his books in a PDF

26
00:01:32,650 --> 00:01:34,200
 form. The

27
00:01:34,200 --> 00:01:37,410
 thing about him is he was both scholarly and meditative. So

28
00:01:37,410 --> 00:01:38,760
 in Burma there are

29
00:01:38,760 --> 00:01:42,400
 scholars that are mind-boggling. They can memorize the

30
00:01:42,400 --> 00:01:44,080
 whole tupitika to an

31
00:01:44,080 --> 00:01:49,320
 extent. Mahasi Sayadaw wasn't such a person but he was very

32
00:01:49,320 --> 00:01:49,600
 well

33
00:01:49,600 --> 00:01:54,960
 learned even by Burmese standards but his focus was

34
00:01:54,960 --> 00:01:56,560
 meditation. So even though

35
00:01:56,560 --> 00:01:59,640
 as a novice, as a young monkey, he went through all the

36
00:01:59,640 --> 00:02:01,640
 studies. Later on he

37
00:02:01,640 --> 00:02:04,380
 realized that the most important thing was to practice them

38
00:02:04,380 --> 00:02:05,520
 which is not common

39
00:02:05,520 --> 00:02:09,450
 actually. It might surprise people but many monks failed to

40
00:02:09,450 --> 00:02:10,520
 realize that the

41
00:02:10,520 --> 00:02:15,030
 important, the most important thing about the Buddhist

42
00:02:15,030 --> 00:02:16,160
 teaching is the practice of

43
00:02:16,160 --> 00:02:19,120
 it and go on to practice. So he went off to practice with

44
00:02:19,120 --> 00:02:20,840
 this monk in a cave and

45
00:02:20,840 --> 00:02:27,600
 that's when he really started to teach, to give something

46
00:02:27,600 --> 00:02:29,440
 and so these

47
00:02:29,440 --> 00:02:31,740
 books that he's written are all after that point when he

48
00:02:31,740 --> 00:02:32,680
 started practicing

49
00:02:32,680 --> 00:02:37,630
 and teaching and realized this sort of way of practice, the

50
00:02:37,630 --> 00:02:38,920
 benefit of it and

51
00:02:38,920 --> 00:02:42,550
 he's helped millions, millions of people directly and

52
00:02:42,550 --> 00:02:44,560
 indirectly with his books,

53
00:02:44,560 --> 00:02:50,040
 with his teachings, and they're exceptional. I've seen many

54
00:02:50,040 --> 00:02:52,640
 teachers, I'm not going to talk about teachers in other

55
00:02:52,640 --> 00:02:53,400
 traditions because I

56
00:02:53,400 --> 00:02:55,320
 don't read them and you're asking me so I wouldn't

57
00:02:55,320 --> 00:02:57,160
 recommend them, not that

58
00:02:57,160 --> 00:03:01,390
 there's necessarily anything wrong with them. But even in

59
00:03:01,390 --> 00:03:02,800
 this tradition I've

60
00:03:02,800 --> 00:03:06,990
 never read or come across any teacher who's able to put the

61
00:03:06,990 --> 00:03:08,120
 Buddha's teaching

62
00:03:08,120 --> 00:03:13,190
 together in such a masterful way. So that's what I would

63
00:03:13,190 --> 00:03:14,880
 recommend as far as

64
00:03:14,880 --> 00:03:20,260
 what is not direct teachings of the Buddha. As far as the t

65
00:03:20,260 --> 00:03:21,240
epidaka and what

66
00:03:21,240 --> 00:03:26,540
 are the direct teachings of the Buddha, I would recommend

67
00:03:26,540 --> 00:03:27,680
 the Majima Nikaya

68
00:03:27,680 --> 00:03:33,790
 I think because it's not too deep and by that I simply mean

69
00:03:33,790 --> 00:03:35,720
 there's not too much

70
00:03:35,720 --> 00:03:39,030
 because if you look at say the Sangyuta Nikaya you've got

71
00:03:39,030 --> 00:03:40,600
 millions of teachings

72
00:03:40,600 --> 00:03:43,540
 or not millions but thousands of teachings and it's really

73
00:03:43,540 --> 00:03:44,240
 difficult to

74
00:03:44,240 --> 00:03:50,870
 digest but on the other hand it's broad enough with 152

75
00:03:50,870 --> 00:03:53,120
 discourses, the Majima

76
00:03:53,120 --> 00:03:55,940
 Nikaya, the middle length discourses of the Buddha

77
00:03:55,940 --> 00:03:57,480
 translation by Bhikkhu Bodhi,

78
00:03:57,480 --> 00:04:01,930
 easy to find on the internet. I would recommend reading

79
00:04:01,930 --> 00:04:03,600
 that, also the Digha

80
00:04:03,600 --> 00:04:09,490
 Nikaya but I think maybe the translation available is not

81
00:04:09,490 --> 00:04:11,920
 as good. I would

82
00:04:11,920 --> 00:04:16,880
 recommend actually all of the tepidaka I suppose, but

83
00:04:16,880 --> 00:04:19,990
 you know start with the Majima Nikaya, that's all I would

84
00:04:19,990 --> 00:04:21,520
 say, but keep

85
00:04:21,520 --> 00:04:24,710
 reading. The problem of course is that translations are

86
00:04:24,710 --> 00:04:26,080
 always makeshift, even

87
00:04:26,080 --> 00:04:29,890
 good translations. So in the end you're better off to learn

88
00:04:29,890 --> 00:04:32,640
 Pali and to start to

89
00:04:32,640 --> 00:04:36,460
 actually read with the Buddha taught and then even just

90
00:04:36,460 --> 00:04:37,840
 reading a few suttas

91
00:04:37,840 --> 00:04:41,790
 is quite profound because it's in the original language. If

92
00:04:41,790 --> 00:04:42,640
 you can't get to

93
00:04:42,640 --> 00:04:46,090
 that point then go with the translations, start with the

94
00:04:46,090 --> 00:04:48,080
 Majima Nikaya.

95
00:04:48,080 --> 00:04:51,890
 Another thing that the Buddha didn't, that isn't Buddha-v

96
00:04:51,890 --> 00:04:53,160
achana in terms of the

97
00:04:53,160 --> 00:04:58,410
 actual words of the Buddha but is based on the commentaries

98
00:04:58,410 --> 00:04:59,440
 is the Visuddhimagga

99
00:04:59,440 --> 00:05:02,570
 or the path of purification which if you follow the sort of

100
00:05:02,570 --> 00:05:04,240
 teaching that I teach

101
00:05:04,240 --> 00:05:12,520
 and propound is a great guide. It's a completely practical

102
00:05:12,520 --> 00:05:13,840
 guide to the

103
00:05:13,840 --> 00:05:16,900
 development of not only the meditation that I follow but

104
00:05:16,900 --> 00:05:17,800
 many different kinds

105
00:05:17,800 --> 00:05:20,210
 of meditation and you'll get a good overview of the

106
00:05:20,210 --> 00:05:21,240
 different kinds of

107
00:05:21,240 --> 00:05:25,440
 meditation. It talks about just about everything. There's

108
00:05:25,440 --> 00:05:26,080
 detailed

109
00:05:26,080 --> 00:05:29,410
 descriptions of how to gain magical powers and fly through

110
00:05:29,410 --> 00:05:30,360
 the air and read

111
00:05:30,360 --> 00:05:34,580
 people's minds and remember past lives and all these things

112
00:05:34,580 --> 00:05:37,160
 that are good but

113
00:05:37,160 --> 00:05:42,880
 but superfluous I suppose, not necessary but are nice

114
00:05:42,880 --> 00:05:44,360
 interesting things to

115
00:05:44,360 --> 00:05:49,530
 study about. So a good book, Visuddhimagga Path of Pur

116
00:05:49,530 --> 00:05:51,240
ification. Okay, hope that

117
00:05:51,240 --> 00:05:54,440
 helps. Thanks for the question.

