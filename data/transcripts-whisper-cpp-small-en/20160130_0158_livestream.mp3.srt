1
00:00:00,000 --> 00:00:17,020
 [ Silence ]

2
00:00:17,020 --> 00:00:18,500
 >> Good evening everyone.

3
00:00:18,500 --> 00:00:28,080
 We're broadcasting live January 29, 2016.

4
00:00:28,080 --> 00:00:35,760
 [ Silence ]

5
00:00:35,760 --> 00:00:46,180
 Today's quote, today's quote is about, well,

6
00:00:46,180 --> 00:00:48,480
 it's about fairly mundane things.

7
00:00:48,480 --> 00:00:50,720
 If you're noticing a pattern, a lot

8
00:00:50,720 --> 00:00:54,140
 of these quotes are Buddhism light.

9
00:00:57,320 --> 00:01:02,800
 It's a bit worrisome because it's easy to get complacent

10
00:01:02,800 --> 00:01:06,840
 if you think of Buddhism as a social philosophy

11
00:01:06,840 --> 00:01:10,040
 to help people live well in the world.

12
00:01:10,040 --> 00:01:15,840
 It's easy to become complacent thinking you can just be

13
00:01:15,840 --> 00:01:21,250
 in generous and do good deeds in the world and that's

14
00:01:21,250 --> 00:01:21,720
 enough.

15
00:01:26,680 --> 00:01:28,360
 Now that this is not a Buddhist teaching,

16
00:01:28,360 --> 00:01:32,480
 it's just an emphasis on worldly aspects of the teaching.

17
00:01:32,480 --> 00:01:38,720
 It's, well, for our purposes it's problematic

18
00:01:38,720 --> 00:01:42,840
 because we're all here interested

19
00:01:42,840 --> 00:01:44,400
 in the core teachings of the Buddha.

20
00:01:44,400 --> 00:01:47,800
 It's hard to get to the core teachings if you keep talking

21
00:01:47,800 --> 00:01:54,560
 about good teachings but good basic teachings like charity

22
00:01:54,560 --> 00:01:59,720
 and living a good household life.

23
00:01:59,720 --> 00:02:04,680
 It doesn't talk about how to address the issues.

24
00:02:04,680 --> 00:02:10,480
 But one thing this quote does mention is to be modest

25
00:02:10,480 --> 00:02:15,240
 about your wealth and to not get upset when it goes away.

26
00:02:15,240 --> 00:02:22,840
 And so that at least hints at a mental development.

27
00:02:24,720 --> 00:02:27,840
 Because it's easy to say don't get upset

28
00:02:27,840 --> 00:02:30,800
 when you lose the things you love

29
00:02:30,800 --> 00:02:32,800
 but it's really impossible in practice.

30
00:02:32,800 --> 00:02:36,080
 If you love something you will be upset when it goes away.

31
00:02:36,080 --> 00:02:39,160
 Can't stop that.

32
00:02:39,160 --> 00:02:43,480
 When you don't get what you want there's a yearning for it.

33
00:02:43,480 --> 00:02:49,960
 When the yearning is unsatisfied or suffering.

34
00:02:49,960 --> 00:03:00,960
 (silence)

35
00:03:00,960 --> 00:03:07,960
 And so this begins to hint at the need for practice.

36
00:03:07,960 --> 00:03:13,960
 (speaks in foreign language)

37
00:03:13,960 --> 00:03:17,040
 To make your mind such that when you're touched

38
00:03:17,040 --> 00:03:21,400
 by the vicissitudes of life, the changes in life,

39
00:03:21,400 --> 00:03:26,600
 impermanence of life, touched by good and evil,

40
00:03:26,600 --> 00:03:30,600
 touched by pleasant and unpleasant.

41
00:03:30,600 --> 00:03:38,080
 When the mind doesn't waver,

42
00:03:38,080 --> 00:03:43,080
 when the mind doesn't cling, doesn't push or pull.

43
00:03:43,080 --> 00:03:49,080
 (silence)

44
00:03:49,080 --> 00:03:52,080
 We have to find the practice that leads us to this.

45
00:03:52,080 --> 00:03:58,080
 (silence)

46
00:03:58,080 --> 00:04:01,080
 Because the theory is quite simple.

47
00:04:01,080 --> 00:04:04,210
 That experience itself isn't capable of causing us

48
00:04:04,210 --> 00:04:05,080
 suffering,

49
00:04:05,080 --> 00:04:08,540
 it's our reactions to our experiences that cause us

50
00:04:08,540 --> 00:04:09,080
 suffering.

51
00:04:09,080 --> 00:04:12,080
 If we didn't react, if we saw things objectively,

52
00:04:12,080 --> 00:04:14,080
 we wouldn't suffer from them.

53
00:04:14,080 --> 00:04:16,080
 (silence)

54
00:04:16,080 --> 00:04:19,260
 If someone's yelling at you, it's only sound until you let

55
00:04:19,260 --> 00:04:20,080
 it get to you.

56
00:04:20,080 --> 00:04:22,080
 (silence)

57
00:04:22,080 --> 00:04:26,080
 If you have great pain, it's only pain until you let it get

58
00:04:26,080 --> 00:04:26,080
 to you,

59
00:04:26,080 --> 00:04:32,080
 hot and cold and hard and soft.

60
00:04:32,080 --> 00:04:35,080
 (silence)

61
00:04:35,080 --> 00:04:38,080
 It's amazing what as a meditator you can bear

62
00:04:38,080 --> 00:04:43,080
 and you can endure without suffering.

63
00:04:43,080 --> 00:04:45,770
 When I first went to meditate, we had to sleep on the hard

64
00:04:45,770 --> 00:04:46,080
wood floor,

65
00:04:46,080 --> 00:04:52,080
 which is the very thing that...

66
00:04:52,080 --> 00:04:55,080
 I mean, it's not like I was in great hardship,

67
00:04:55,080 --> 00:04:58,080
 but this was one thing that I remember.

68
00:04:58,080 --> 00:05:01,080
 If you don't have much fat on you,

69
00:05:01,080 --> 00:05:05,080
 sleeping like that is very important.

70
00:05:05,080 --> 00:05:10,080
 Sleeping like that is very painful, but you know,

71
00:05:10,080 --> 00:05:13,080
 one night I slept under a park bench

72
00:05:13,080 --> 00:05:16,080
 and it was really comfortable on the cement,

73
00:05:16,080 --> 00:05:19,080
 in bitter cold as well.

74
00:05:19,080 --> 00:05:26,080
 (silence)

75
00:05:26,080 --> 00:05:28,080
 There was a story behind that,

76
00:05:28,080 --> 00:05:32,080
 but I always tell this story about this park bench because

77
00:05:32,080 --> 00:05:37,080
 there was so much...

78
00:05:37,080 --> 00:05:39,080
 It was a really complicated situation

79
00:05:39,080 --> 00:05:44,080
 and people were pushing and pulling and

80
00:05:44,080 --> 00:05:48,290
 finally I just walked out and went and stayed under a park

81
00:05:48,290 --> 00:05:49,080
 bench.

82
00:05:49,080 --> 00:05:53,080
 And it felt so great because there was no...

83
00:05:53,080 --> 00:05:59,080
 nobody, no, I didn't have to...

84
00:05:59,080 --> 00:06:08,080
 concern myself with politics or opinions and so on, people.

85
00:06:08,080 --> 00:06:12,080
 But the point here is...

86
00:06:12,080 --> 00:06:15,080
 it's all in the mind.

87
00:06:15,080 --> 00:06:22,080
 Some people...

88
00:06:22,080 --> 00:06:24,080
 The Buddha said even in a comfortable bed,

89
00:06:24,080 --> 00:06:29,080
 a comfortable chair and great misery, great suffering,

90
00:06:29,080 --> 00:06:34,080
 great stress, their minds are filled with lust,

91
00:06:34,080 --> 00:06:38,080
 they're filled with anger, they're filled with delusion.

92
00:06:38,080 --> 00:06:42,080
 Those are the three bad things, the three things that

93
00:06:42,080 --> 00:06:44,080
 poison our mind.

94
00:06:44,080 --> 00:06:48,080
 Our minds are full of them.

95
00:06:48,080 --> 00:06:50,080
 It doesn't matter what comforts we feel,

96
00:06:50,080 --> 00:07:02,080
 comforts we get, we still suffer.

97
00:07:02,080 --> 00:07:04,080
 So our practice is quite simple.

98
00:07:04,080 --> 00:07:09,640
 We just remind ourselves of what things are, keeps us

99
00:07:09,640 --> 00:07:13,080
 objective.

100
00:07:13,080 --> 00:07:15,080
 When you do it, you can see...

101
00:07:15,080 --> 00:07:18,600
 Some people doubt this technique and that doubt gets in the

102
00:07:18,600 --> 00:07:19,080
 way.

103
00:07:19,080 --> 00:07:22,080
 They don't give it an honest try.

104
00:07:22,080 --> 00:07:24,080
 But as soon as you give it an honest try,

105
00:07:24,080 --> 00:07:28,080
 you can see in that moment the clarity,

106
00:07:28,080 --> 00:07:34,080
 the mind is clear, the mind is pure.

107
00:07:34,080 --> 00:07:37,080
 It's amazing teaching people five minute meditation.

108
00:07:37,080 --> 00:07:39,080
 I bet a lot of them will forget about it,

109
00:07:39,080 --> 00:07:44,080
 but you give them one moment, you give them that minute

110
00:07:44,080 --> 00:07:49,080
 where they come out of like a one minute meditation

111
00:07:49,080 --> 00:07:54,080
 where we do at the end just about one minute

112
00:07:54,080 --> 00:07:58,080
 and they rave about it because they've seen it,

113
00:07:58,080 --> 00:08:00,080
 they've seen that moment.

114
00:08:00,080 --> 00:08:04,080
 It takes us one moment.

115
00:08:04,080 --> 00:08:06,080
 So when you doubt about the practice,

116
00:08:06,080 --> 00:08:10,080
 when you're not sure, you should scold yourself

117
00:08:10,080 --> 00:08:12,720
 because if you've been practicing at all, you've seen the

118
00:08:12,720 --> 00:08:13,080
 moment,

119
00:08:13,080 --> 00:08:17,080
 you've seen that moment, you've had that one moment.

120
00:08:17,080 --> 00:08:20,080
 Later when you doubt, "Is this really helping?"

121
00:08:20,080 --> 00:08:23,080
 you should call yourself foolish for thinking that.

122
00:08:23,080 --> 00:08:27,080
 You should call yourself out on it, absolutely.

123
00:08:27,080 --> 00:08:33,080
 We trick ourselves, "Is this really good?"

124
00:08:33,080 --> 00:08:36,080
 It's like a person... This happens, you know,

125
00:08:36,080 --> 00:08:43,080
 a person gets...

126
00:08:43,080 --> 00:08:49,910
 We get free from an illness and then we become reckless

127
00:08:49,910 --> 00:08:52,080
 again.

128
00:08:52,080 --> 00:08:55,080
 We get a terrible illness and we suffer terribly

129
00:08:55,080 --> 00:09:00,080
 and then when we get free from it,

130
00:09:00,080 --> 00:09:04,080
 we act as though it never happened.

131
00:09:04,080 --> 00:09:09,080
 We don't admit our vulnerability to dealness.

132
00:09:09,080 --> 00:09:16,080
 We don't work to try to change or to train our mind.

133
00:09:16,080 --> 00:09:29,080
 We're very good at forgetting.

134
00:09:29,080 --> 00:09:34,080
 We should try to practice remembering.

135
00:09:34,080 --> 00:09:36,080
 And so if you want to know real progress in the practice,

136
00:09:36,080 --> 00:09:38,080
 think about the moments.

137
00:09:38,080 --> 00:09:46,080
 In this moment when I'm mindful, see the result.

138
00:09:46,080 --> 00:09:50,080
 All it takes is a moment.

139
00:09:50,080 --> 00:09:53,080
 So that's a little bit for tonight.

140
00:09:53,080 --> 00:10:00,080
 Tomorrow at 3 pm Eastern Time, mid... 12 pm,

141
00:10:00,080 --> 00:10:06,080
 midday, second lifetime, I'll be giving a talk

142
00:10:06,080 --> 00:10:08,080
 in Second Life at the Buddha Center.

143
00:10:08,080 --> 00:10:13,080
 So if you know what that is, see you there.

144
00:10:13,080 --> 00:10:14,080
 Have a good night.

