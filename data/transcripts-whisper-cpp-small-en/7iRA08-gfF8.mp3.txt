 Hello, welcome back to Ask a Monk. This next question is a
 pretty good example, I think,
 of how not to ask questions on this forum. And that's
 because it's specifically, well,
 it seems quite clearly to be a presentation of one's own
 philosophy in the form of a question,
 because here we have a premise and a question. I'll read it
 to you. "I want to ask about
 nonduality. When one doesn't distinguish the two realms of
 samsara and nirvana, then there
 is no goal. So my question is, is wanting to transcend
 rebirth selfish? But what you
 have here is a complete philosophy that exists in Buddhism.
 In Buddhism there are many philosophies
 and they're not all compatible. This one I happen to not
 agree with. And this is a complete
 presentation of this philosophy. And it's clearly so
 because the premise and the conclusion
 that is presented in the premise, which the implicit
 conclusion being that striving for
 the end of rebirth is selfish, it has nothing to do, they
 have nothing to do with each other,
 unless except in the greater framework of this philosophy,
 this specific philosophy.
 So it seems clear that the purpose of this post is not to
 ask a question, it's to present
 a philosophy, which is not really appropriate. Now, I'm not
 sure this is the case. It may
 be that the person asking is really interested and really
 thinks that I believe that these
 two things are indistinguishable and somehow thinks that
 that has some effect on the question
 as to whether sending rebirth is selfish. I don't see it.
 So briefly I'll go over this,
 but this isn't the kind of question that I'm interested in
 asking. I would like to be answering
 questions about meditation and about, you know, that really
 help you understand the Buddha's
 teaching and understand the meditation practice and improve
 your meditation practice and help
 you along the path. So, but anyway, the premise, why I
 disagree with this philosophy, the idea
 that samsara and nirvana are indistinguishable is just a
 sophism. It's a theory, it's a view.
 It has something to do with reality. If you experience sams
ara, samsara is one thing. This
 is samsara, seeing, the hearing, the smelling, the tasting,
 the feeling and thinking. It's
 the arising of suffering and the pursuit of suffering.
 Nirvana is the opposite. It's the
 non arising of suffering, the non pursuit of suffering. And
 the experience of it is completely
 different. The reason why people are unable to distinguish
 it is because they haven't
 experienced it. They, everything they've experienced is the
 same. They practice meditation and
 their experiences come and go and so they relate that back
 to samsara and it also comes
 and goes and everything comes and goes and they say nothing
 is different. So they're
 unable to distinguish one from the other, which is correct
 really because everything is indistinguishable
 except nirvana. Everything else arises and ceases and this
 is why the Buddha said that
 nothing's worth clinging to because it all arises and
 ceases. So it's basically equating
 two opposites or confusing two opposites. It's like saying
 that night and day are indistinguishable.
 Light and darkness are indistinguishable, which
 theoretically is, you can come up with a theory
 that says that is the case, but doesn't affect the reality
 that light is quite different
 from darkness and that's objectively so. And nirvana and s
amsara are objectively so as well,
 no matter what theory or philosophy you come up with
 because you can come up with any philosophy
 you like and it doesn't affect, it doesn't have any effect
 on reality. Now as to the
 question, I'm assuming that the implication here is that
 somehow that premise effect,
 implies that or leads to the conclusion, supports the
 conclusion that freedom from or the desire
 wanting to transcend rebirth is selfish. I'm assuming that
's what's implied here. So I
 can talk about that. Selfishness only really comes into
 play in Buddhism as I understand
 it in terms of clinging. So if you cling to something and
 you don't want someone else
 to have it, if you ask me for something and I don't want to
 give it to you, it's only
 because I'm clinging to the object. There's no true selfish
ness that arises. It's a clinging,
 it's a function of the mind that wants an object. You don't
 think about the other person.
 What we call selfishness is this absorption in one's
 attachment and only by overcoming
 that can you possibly give up something that is a benefit
 to you and we're able to do that
 as we grow up. It's not because we become, well it could be
 because we become what we
 call more altruistic but that is not the way of the Buddha,
 that's not the answer because
 this idea of altruism is just another theory, it's another
 mind game and there are people
 who even refute this theory that say how can you possibly
 work for the benefit of other
 people but the Buddha's teaching doesn't go either way.
 There's no idea that working
 for one's benefit, another person's benefit is better than
 working for one's own benefit.
 You act in such a way that brings harmony. You act in such
 a way that leads to the least
 conflict. You act in such a way that is going to be most
 appropriate at any given time and
 it's not an intellectual exercise. You don't have to study
 the books and learn what is
 most appropriate though that can help for people who don't
 have the experience. You
 understand the nature of reality through your practice and
 therefore are able to clearly
 see what is of the greatest benefit, what is the most
 appropriate at any given time
 and it might be acting for your own benefit, it might be
 acting for someone else's benefit
 but the point is not either or, the point is the appropriat
eness of the act. So if someone
 comes and asks you for something many things might come
 into play. It depends on the situation.
 If you ask me for something I might give it to you even
 though it might cause me suffering.
 There may be many reasons for that. It could be because you
've helped me before, it could
 be because I am able to deal with suffering and I know that
 you're not. I have practiced
 meditation and I am able to be patient and I can see that
 you're not, I can see that
 I'm giving to you will lead to less suffering overall
 because for me it's only physical
 suffering for you, it's mental and so on. I might not give
 you any of it because I might
 see that you are not going to use it or I might see that if
 you are without it you're
 going to learn something, you're going to learn patience
 and so on. There are many responses.
 This is talking about an enlightened person. If a person is
 enlightened they will act in
 this way. They will never rise to the my benefit, your
 benefit because there's no clinging and
 that's the only time that selfishness arises. So the idea
 that wanting to transcend rebirth
 is selfish really doesn't, the idea of selfishness doesn't
 come into play. It has nothing to
 do with anyone else and the only way you could call it
 selfish is if you start playing these
 mind games and logic and it's an intellectual exercise
 where you say well if you did stay
 around you could help so many people and therefore it is
 selfish to, but it's just a theory right?
 It has nothing to do with the state of mind. The person
 could be totally unselfish and
 if anyone asked them for something they would help them,
 but they don't ever think hmm why
 I should stick around otherwise it's going to be selfish
 because there's no clinging,
 there's no attachment to this state or that state. The
 person simply acts as is appropriate
 in that instance. So the realization of freedom from
 rebirth or the attainment of freedom
 from rebirth is of the greatest benefit to a person. If a
 person wants to strive for
 that then that's a good thing, it creates benefit. If a
 person decides to extend their,
 or not work for the, towards the freedom from rebirth and
 they want to be reborn again and
 again then that's their prerogative. The Buddha never laid
 down any you know laws of nature
 in this regard because there are none. There is simply the
 cause and effect. If you, if
 a person decides that they want to extend their existence
 and come back again and again
 and be reborn again and again thinking that they're going
 to help people then that's fine.
 Or that's their choice, that's their path. There's no need
 for any sort of judgment at
 all. If a person decides that they want to become free from
 suffering in this life and
 not come back and be free from rebirth then that's their
 path as well. There's only the
 cause and effect so a person who is reborn again and again
 and again will have to come
 back again and again and suffer again and again. And you
 know it's questionable as to
 how much help they can possibly be given that they are not
 yet free from clinging because
 a person who is free from clinging would not be reborn. And
 the person, actually the person
 who does become free from rebirth, who does transcend
 rebirth so to speak, is able to
 help people because their mind is free from clinging and
 therefore they are able to provide
 great support and help and great insight to people. The
 Buddha is a good example. Now
 there is the question of whether the Buddha and enlightened
 beings do come back and because
 there are even philosophies that believe a person who is
 free from clinging still is
 reborn but that's not really the question here. Or it's
 getting a little bit too much
 into other people's philosophies and I don't want to create
 more confusion than is necessary.
 So the point here is that we work for benefit, we work for
 welfare, we do what's appropriate
 or we try to. This is our practice and an enlightened being
 is someone who is perfect
 at it, who doesn't have any thought of self or other and is
 not subject to suffering.
 When they pass away from this life they are free from all
 suffering and there is no more
 coming back, no more arising. So basically I just wanted to
 say that this is not the
 sort of question that I'm hoping to entertain and I think
 probably in the future I'm just
 going to skip over because I got a lot of questions and I'm
 going to try to be selective.
 I've been quite random actually in choosing which questions
 to answer because it's difficult
 to sort them. I've got a lot of them and Google doesn't
 make it easy by any means. So I'm
 going to try to skip and go directly to those ones that I
 think are going to be useful for
 people. So anyway thanks for tuning in, this has been
 another episode of Ask a Monk and
 wishing you all peace, happiness and freedom from suffering
.
