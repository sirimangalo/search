1
00:00:00,000 --> 00:00:06,160
 Hi everyone, I'm a little bit sick today so I wasn't

2
00:00:06,160 --> 00:00:07,600
 thinking originally to make a video but

3
00:00:07,600 --> 00:00:15,280
 no, sitting around the house. So I thought I'd go into one

4
00:00:15,280 --> 00:00:16,880
 question that has come up a lot and

5
00:00:16,880 --> 00:00:21,390
 I've tried to stay away from it and that is how I got into

6
00:00:21,390 --> 00:00:24,800
 Buddhism and ultimately became a monk.

7
00:00:24,800 --> 00:00:29,710
 But I think yeah it's probably worth explaining or getting

8
00:00:29,710 --> 00:00:32,080
 over because there's a lot of people

9
00:00:32,080 --> 00:00:41,040
 wondering who I am, whether they can trust the things I say

10
00:00:41,040 --> 00:00:42,880
 and so on, wondering where I'm coming

11
00:00:42,880 --> 00:00:48,040
 from, what my background is. So how I got interested in

12
00:00:48,040 --> 00:00:51,280
 Buddhism I think actually relates to

13
00:00:51,280 --> 00:00:56,530
 when I was young. I was one of those people who I think

14
00:00:56,530 --> 00:00:59,680
 among many people who had a keen

15
00:00:59,680 --> 00:01:09,310
 interest in these big questions like why do we all have to

16
00:01:09,310 --> 00:01:12,160
 die and what is the meaning of life

17
00:01:12,160 --> 00:01:16,660
 and trying to find a way of living that has more meaning

18
00:01:16,660 --> 00:01:19,680
 than just living for the sake of living or

19
00:01:20,800 --> 00:01:26,400
 just to get by. I remember thinking about my family and

20
00:01:26,400 --> 00:01:28,880
 just realizing one day when I was about

21
00:01:28,880 --> 00:01:32,000
 eight that everyone in my family is going to die, that all

22
00:01:32,000 --> 00:01:33,920
 the people I knew are going to die,

23
00:01:33,920 --> 00:01:37,140
 that this is a fact of life and it hit me quite profoundly

24
00:01:37,140 --> 00:01:38,560
 and it was something that

25
00:01:38,560 --> 00:01:44,520
 posed questions in my mind why or what do we do about this.

26
00:01:44,520 --> 00:01:46,720
 There's nothing we can do,

27
00:01:46,720 --> 00:01:51,980
 it was a sense of hopelessness and I started getting this

28
00:01:51,980 --> 00:01:52,880
 idea that

29
00:01:52,880 --> 00:02:00,660
 my understanding of life was that we go to school to get a

30
00:02:00,660 --> 00:02:01,600
 job, to get

31
00:02:01,600 --> 00:02:05,770
 to collect money, to get old, to retire and by the time we

32
00:02:05,770 --> 00:02:09,440
 retire we're old and we've lived most

33
00:02:09,440 --> 00:02:13,220
 of our life out and haven't gained anything from it. So

34
00:02:13,220 --> 00:02:15,840
 even when I was young, when I was I think

35
00:02:15,840 --> 00:02:20,340
 maybe 11 or 12, I was really set in this idea that the

36
00:02:20,340 --> 00:02:22,720
 ordinary way of living was meaningless

37
00:02:22,720 --> 00:02:30,010
 and I had to do something to find a way out. So I started

38
00:02:30,010 --> 00:02:33,200
 looking into things like meditation when

39
00:02:33,200 --> 00:02:37,130
 I was practicing martial arts and I remember incorporating

40
00:02:37,130 --> 00:02:39,600
 meditation into it. When I started

41
00:02:39,600 --> 00:02:42,220
 doing a little bit of teaching martial arts I would teach

42
00:02:42,220 --> 00:02:44,000
 people meditation as well and people

43
00:02:44,000 --> 00:02:47,580
 really liked it and I thought it was great and I would

44
00:02:47,580 --> 00:02:50,400
 practice meditation at home even lying

45
00:02:50,400 --> 00:02:56,840
 down in bed I would enter into these states of intense

46
00:02:56,840 --> 00:03:01,680
 concentration and it was something that

47
00:03:01,680 --> 00:03:07,300
 was always with me even from that time. And then someone

48
00:03:07,300 --> 00:03:10,480
 handed me in school the Dao De Ching which

49
00:03:10,480 --> 00:03:15,650
 was a very important book for me for most of my teenage

50
00:03:15,650 --> 00:03:18,480
 years. It was sort of what set me on this

51
00:03:18,480 --> 00:03:22,410
 path. I instantly reading this book I was instantly

52
00:03:22,410 --> 00:03:25,600
 converted and thought I'm a Daoist and from then

53
00:03:25,600 --> 00:03:29,380
 on when people asked I said I was a Daoist and this is what

54
00:03:29,380 --> 00:03:32,560
 I believed in and what resonated

55
00:03:32,560 --> 00:03:37,730
 with me was this teaching in the Dao De Ching. So I tried

56
00:03:37,730 --> 00:03:41,040
 to get through high school and did really

57
00:03:41,040 --> 00:03:44,860
 well but hated it and asked repeatedly to leave school to

58
00:03:44,860 --> 00:03:47,360
 be taken out of school. Through public

59
00:03:47,360 --> 00:03:49,410
 school I had been home schooled and it was only in high

60
00:03:49,410 --> 00:03:51,840
 school that we went back and it was just

61
00:03:51,840 --> 00:03:54,500
 wasn't doing it for me. I said this isn't the way to live

62
00:03:54,500 --> 00:03:56,320
 our lives this isn't for any purpose

63
00:03:56,320 --> 00:04:01,790
 except getting into this wheel of samsara. And so my

64
00:04:01,790 --> 00:04:05,600
 parents moved me to an alternative school and

65
00:04:05,600 --> 00:04:09,580
 then another alternative school and finally I finished high

66
00:04:09,580 --> 00:04:11,280
 school and went on to university.

67
00:04:11,280 --> 00:04:16,320
 And then in university I decided that I'd had enough and I

68
00:04:16,320 --> 00:04:17,840
 dropped out of my first year of

69
00:04:17,840 --> 00:04:20,600
 university and you know there was a lot of stuff going on

70
00:04:20,600 --> 00:04:22,800
 here. I wasn't at all spiritual. I was

71
00:04:22,800 --> 00:04:27,900
 getting caught up in the teenage years and hedonism, drugs,

72
00:04:27,900 --> 00:04:30,080
 sex, rock and roll, all of the

73
00:04:30,080 --> 00:04:32,780
 these things that are supposed to bring happiness and they

74
00:04:32,780 --> 00:04:34,480
 were making me miserable and I remember

75
00:04:34,480 --> 00:04:37,040
 this was a terrible period of my life and it just built up

76
00:04:37,040 --> 00:04:38,880
 and built up to the point where

77
00:04:38,880 --> 00:04:43,570
 in university I said that's it and I withdrew and tried to

78
00:04:43,570 --> 00:04:45,440
 go find the place to go.

79
00:04:47,280 --> 00:04:52,060
 If I had find a way to some sort of spiritual path to

80
00:04:52,060 --> 00:04:54,960
 follow I was thinking of going to Central

81
00:04:54,960 --> 00:04:59,780
 America to do all these crazy hallucinogenic drugs with the

82
00:04:59,780 --> 00:05:02,880
 shamans and so on. I was still

83
00:05:02,880 --> 00:05:07,490
 sort of in that mode and a little bit lost. I didn't know

84
00:05:07,490 --> 00:05:09,440
 where to go. But people kept pushing

85
00:05:09,440 --> 00:05:12,230
 me to go to Thailand. This person would talk about Thailand

86
00:05:12,230 --> 00:05:13,520
, that person would talk about

87
00:05:13,520 --> 00:05:18,450
 Thailand until finally one of my rock climbing friends

88
00:05:18,450 --> 00:05:20,720
 suggested that we go there to do some

89
00:05:20,720 --> 00:05:24,420
 rock climbing. So I took him up on that thinking this will

90
00:05:24,420 --> 00:05:26,800
 give me a chance to practice to follow

91
00:05:26,800 --> 00:05:30,330
 Taoism, to follow these Taoist teachings. From Thailand I

92
00:05:30,330 --> 00:05:32,320
'll make a trip to China or Japan,

93
00:05:32,320 --> 00:05:36,150
 maybe get into some Zen or something. Try to find some

94
00:05:36,150 --> 00:05:39,600
 place and I was joking with people that I

95
00:05:39,600 --> 00:05:42,120
 would, my intention was to become a monk but I never really

96
00:05:42,120 --> 00:05:45,840
 took it seriously. I was looking

97
00:05:45,840 --> 00:05:50,100
 for something and I thought I'll find the truth, I'll find

98
00:05:50,100 --> 00:05:52,000
 some spiritual path.

99
00:05:52,000 --> 00:05:57,490
 When I got to Thailand it wasn't really what I had hoped

100
00:05:57,490 --> 00:05:59,920
 for and I wasn't really even looking

101
00:05:59,920 --> 00:06:03,310
 in Thailand. I wasn't interested in Buddhism actually at

102
00:06:03,310 --> 00:06:05,200
 all except maybe Zen Buddhism. I

103
00:06:05,200 --> 00:06:07,560
 was more interested in Taoism and I was thinking about

104
00:06:07,560 --> 00:06:09,520
 going to China. So I spent some time looking

105
00:06:09,520 --> 00:06:12,540
 around and reading up on what's going on in China and where

106
00:06:12,540 --> 00:06:15,840
 I could find some kind of Taoist

107
00:06:15,840 --> 00:06:19,970
 teachings. I didn't have much luck but I bought a ticket to

108
00:06:19,970 --> 00:06:22,800
 go to China, got my visa already and I

109
00:06:22,800 --> 00:06:25,780
 was ready to go and then finally it just kind of hit me

110
00:06:25,780 --> 00:06:28,720
 that China's probably not going to be the

111
00:06:28,720 --> 00:06:32,770
 most spiritual places. It's a communist country at the time

112
00:06:32,770 --> 00:06:35,440
, it wasn't yet even openly Buddhist.

113
00:06:36,880 --> 00:06:43,680
 It was a pretty scary thing to just go into China not

114
00:06:43,680 --> 00:06:44,320
 knowing anyone.

115
00:06:44,320 --> 00:06:48,470
 So I changed my mind and went to Meditate in Thailand which

116
00:06:48,470 --> 00:06:50,880
 turned out to be I think the

117
00:06:50,880 --> 00:06:55,620
 right choice. It helped me to understand what is Buddhism

118
00:06:55,620 --> 00:06:57,760
 because when you read about Buddhism

119
00:06:57,760 --> 00:07:01,460
 often it's people talking about suffering and that seems

120
00:07:01,460 --> 00:07:03,200
 quite dry and dull and boring

121
00:07:04,080 --> 00:07:07,870
 especially sometimes the way it's presented. But when I

122
00:07:07,870 --> 00:07:12,080
 went to Meditate and started actually

123
00:07:12,080 --> 00:07:16,240
 practicing instead of just reading and theorizing and

124
00:07:16,240 --> 00:07:17,600
 thinking about it,

125
00:07:17,600 --> 00:07:21,330
 it really helped me to understand what was this thing I was

126
00:07:21,330 --> 00:07:25,040
 looking for, talking about wisdom.

127
00:07:25,040 --> 00:07:27,740
 It kind of seemed in the beginning that wisdom would be

128
00:07:27,740 --> 00:07:29,760
 something that you talk about and you

129
00:07:29,760 --> 00:07:33,330
 just get it. But now once you started meditating, wisdom

130
00:07:33,330 --> 00:07:35,680
 became something that you understand,

131
00:07:35,680 --> 00:07:40,070
 you realize for yourself through experience, through seeing

132
00:07:40,070 --> 00:07:43,600
 for yourself. So I spent a month

133
00:07:43,600 --> 00:07:47,750
 there meditating and that changed my life. I would say that

134
00:07:47,750 --> 00:07:50,960
's probably the most important

135
00:07:50,960 --> 00:07:58,670
 thing I've ever done. Then I went back, it was kind of 50/

136
00:07:58,670 --> 00:08:00,480
50, should I stay, should I go?

137
00:08:00,480 --> 00:08:03,390
 Someone suggested I should go back and get my education if

138
00:08:03,390 --> 00:08:05,440
 my parents wanted me to so I

139
00:08:05,440 --> 00:08:09,350
 followed that advice and went back and spent the next two

140
00:08:09,350 --> 00:08:11,440
 years. This is where you get into where

141
00:08:11,440 --> 00:08:14,210
 you might be becoming a monk. So I'm just talking about my

142
00:08:14,210 --> 00:08:17,920
 background. But I want you to understand

143
00:08:17,920 --> 00:08:21,080
 it's a little bit convoluted. You may not become a monk in

144
00:08:21,080 --> 00:08:23,200
 exactly this way because it took me two

145
00:08:23,200 --> 00:08:30,700
 years to get my parents almost consent. In Buddhism we have

146
00:08:30,700 --> 00:08:33,280
 this rule that if you're going to become

147
00:08:33,280 --> 00:08:35,880
 a monk, your parents have to agree on it. It's a useful

148
00:08:35,880 --> 00:08:37,520
 rule because it's very difficult to

149
00:08:37,520 --> 00:08:39,970
 meditate if you have problems with the people that are

150
00:08:39,970 --> 00:08:44,560
 close to you. So I spent two years trying to

151
00:08:46,240 --> 00:08:49,260
 get to the point where I'd be able to ordain. I think in

152
00:08:49,260 --> 00:08:51,120
 the beginning it wasn't really the

153
00:08:51,120 --> 00:08:59,600
 major goal. But as I studied and as I meditated more and

154
00:08:59,600 --> 00:09:01,200
 learned more about what the Buddha taught,

155
00:09:01,200 --> 00:09:05,150
 it just seemed like the right way to go. It seemed like the

156
00:09:05,150 --> 00:09:09,280
 answer, the next step.

157
00:09:10,480 --> 00:09:13,710
 So I spent time learning about how to become, you know,

158
00:09:13,710 --> 00:09:15,840
 what was the life of a monk and what are the

159
00:09:15,840 --> 00:09:21,690
 roles of the monk. And I spent most of those two years

160
00:09:21,690 --> 00:09:26,080
 actually in a Buddhist monastery in Canada.

161
00:09:26,080 --> 00:09:31,340
 I was quite fortunate to be able to stay at a Buddhist

162
00:09:31,340 --> 00:09:34,240
 monastery while I was going to school.

163
00:09:34,240 --> 00:09:37,420
 So instead of renting an apartment, getting a job, whatever

164
00:09:37,420 --> 00:09:39,920
, I had no money. I went to a

165
00:09:39,920 --> 00:09:43,080
 Cambodian Buddhist monastery, this little church that they

166
00:09:43,080 --> 00:09:45,360
 had bought and started a Buddhist

167
00:09:45,360 --> 00:09:48,600
 monastery and asked them if I could stay there. And they

168
00:09:48,600 --> 00:09:51,600
 were, you know, kind of like the abbot

169
00:09:51,600 --> 00:09:54,710
 said to me, it said it's okay. And I learned after staying

170
00:09:54,710 --> 00:09:56,560
 with him from him that it's okay,

171
00:09:56,560 --> 00:09:59,780
 really means, no, we'd rather that you don't. But it was

172
00:09:59,780 --> 00:10:01,280
 his way of politely saying,

173
00:10:01,280 --> 00:10:06,880
 probably not a good idea. But I was stubborn and he let me.

174
00:10:06,880 --> 00:10:08,240
 And we ended up getting along really

175
00:10:08,240 --> 00:10:11,120
 well. And he's now one of my very good friends, even though

176
00:10:11,120 --> 00:10:12,960
 we've kind of lost touch. But I stayed

177
00:10:12,960 --> 00:10:15,960
 with him there, went to school and kept eight, eight, the

178
00:10:15,960 --> 00:10:18,160
 eight Buddhist precepts, I wouldn't eat

179
00:10:18,160 --> 00:10:20,360
 in the evening. So I was at university all day, and I would

180
00:10:20,360 --> 00:10:23,120
 only eat in the morning. I didn't have

181
00:10:23,120 --> 00:10:26,550
 a job. So I would, you know, just get by where there was a

182
00:10:26,550 --> 00:10:28,720
 few days where I didn't have any food

183
00:10:28,720 --> 00:10:32,280
 to eat. It was a really a hard life, but I felt like it was

184
00:10:32,280 --> 00:10:36,160
 a training for me. So I think in that

185
00:10:36,160 --> 00:10:41,170
 sense, it's important to point out that, you know,

186
00:10:41,170 --> 00:10:46,160
 sometimes it, it helps to prepare yourself. This is

187
00:10:46,160 --> 00:10:49,320
 why they often suggest people to go and stay at a monastery

188
00:10:49,320 --> 00:10:51,280
 for a year or so. I kind of did this

189
00:10:51,280 --> 00:10:55,830
 before I went to ordain. I was not really in a real mon

190
00:10:55,830 --> 00:11:00,480
astic situation. But, you know, I was kind

191
00:11:00,480 --> 00:11:04,020
 of living the life keeping some of the monks roles. In fact

192
00:11:04,020 --> 00:11:06,080
, I tried to keep as many of the monks

193
00:11:06,080 --> 00:11:10,290
 roles as I could even not as a monk. But finally, it became

194
00:11:10,290 --> 00:11:13,040
 too much. And I decided that school

195
00:11:13,040 --> 00:11:18,200
 wasn't going to work for me. And so I went back to Thailand

196
00:11:18,200 --> 00:11:23,600
, ordained as a monk. And the only, the

197
00:11:23,600 --> 00:11:26,100
 only way I could manage to ordain as a monk is if I

198
00:11:26,100 --> 00:11:28,560
 promised my parents that I would come back and

199
00:11:28,560 --> 00:11:31,400
 continue to learn. I said, don't worry, I'll become a monk,

200
00:11:31,400 --> 00:11:34,080
 but I'll still come back and study. So

201
00:11:36,080 --> 00:11:38,980
 so I did, I became a monk. And then in the middle of the

202
00:11:38,980 --> 00:11:43,120
 winter, I came went back to Canada, and went

203
00:11:43,120 --> 00:11:49,380
 back to school as a monk. And unsurprisingly, that didn't

204
00:11:49,380 --> 00:11:54,080
 last long. I wasn't into it. I had kind of

205
00:11:54,080 --> 00:11:57,340
 had enough of school, I'd had enough of school many years

206
00:11:57,340 --> 00:11:59,600
 ago, actually, but it was sort of an

207
00:11:59,600 --> 00:12:03,220
 agreement that I'd had with my father that I go back to

208
00:12:03,220 --> 00:12:05,840
 school. And so in the end, I broke the

209
00:12:05,840 --> 00:12:10,690
 agreement, I said, I can't do it, it's not going to work. I

210
00:12:10,690 --> 00:12:14,960
 have to have to follow my, my way. And

211
00:12:14,960 --> 00:12:19,440
 so I left, did about a half year of school, hung out for a

212
00:12:19,440 --> 00:12:22,720
 while, did some stuff at the monastery

213
00:12:22,720 --> 00:12:29,750
 help these novices. And then went back to Thailand. That's

214
00:12:29,750 --> 00:12:35,040
 my story on how I became a monk. The actual

215
00:12:35,040 --> 00:12:40,170
 ordination procedure was, there's not much to say. I think,

216
00:12:40,170 --> 00:12:42,720
 you know, what's important here is the

217
00:12:42,720 --> 00:12:46,110
 path that leads up to it getting your life in order if you

218
00:12:46,110 --> 00:12:48,080
 if you're this kind of person who

219
00:12:48,080 --> 00:12:52,850
 believes in, in, in these sorts of things, you know, that

220
00:12:52,850 --> 00:12:58,480
 this is the the way out, the this is the

221
00:12:58,480 --> 00:13:05,650
 path that has real meaning and benefit and purpose. Then,

222
00:13:05,650 --> 00:13:10,160
 you know, do some meditation, start start on

223
00:13:10,160 --> 00:13:12,430
 the course, don't just say, okay, I'm going to go ordain as

224
00:13:12,430 --> 00:13:14,080
 a monk, I would say these people don't

225
00:13:14,080 --> 00:13:17,300
 tend to make it very far. If that's how you're starting out

226
00:13:17,300 --> 00:13:19,200
 your spiritual life, you're, you're

227
00:13:19,200 --> 00:13:23,150
 probably just going to to give up and get frustrated and go

228
00:13:23,150 --> 00:13:25,440
 home, because you're not really ready for

229
00:13:25,440 --> 00:13:31,000
 it. It's a big step. For me, it was two years of meditation

230
00:13:31,000 --> 00:13:34,480
 study and just generally living the

231
00:13:34,480 --> 00:13:39,730
 monastic life. And so if you can do that first, I think you

232
00:13:39,730 --> 00:13:44,000
'll be greatly benefited. A lot of

233
00:13:44,000 --> 00:13:47,800
 when I when I decided when I the day came to be ordained,

234
00:13:47,800 --> 00:13:51,600
 there were 18 of us who ordained,

235
00:13:51,600 --> 00:13:57,140
 and I'm the only one left. They all most of them disrobe

236
00:13:57,140 --> 00:14:00,080
 within a week, that standard,

237
00:14:00,080 --> 00:14:02,670
 but even the ones who had decided to stay on never made it.

238
00:14:02,670 --> 00:14:04,720
 There were three other two other

239
00:14:04,720 --> 00:14:11,010
 Westerners and 15 Thai people, and they've all disrobed.

240
00:14:11,010 --> 00:14:14,160
 And you know, I felt ready for it. For

241
00:14:14,160 --> 00:14:18,540
 me, it wasn't a big deal. It was, well, it felt great. It

242
00:14:18,540 --> 00:14:21,280
 was like a final release. Yes, I've made

243
00:14:21,280 --> 00:14:24,880
 it. But as for a change in lifestyle, it didn't change much

244
00:14:24,880 --> 00:14:27,040
. There were a bunch more rules that

245
00:14:27,040 --> 00:14:30,730
 I had to keep. But other than that, you know, I had already

246
00:14:30,730 --> 00:14:32,640
 really prepared myself for it.

247
00:14:33,600 --> 00:14:37,820
 That's my story. That's the first half of my story. The

248
00:14:37,820 --> 00:14:40,560
 second half gets even crazier

249
00:14:40,560 --> 00:14:46,060
 after I became a monk, but nobody's asked that question yet

250
00:14:46,060 --> 00:14:48,880
. So that's an answer to your

251
00:14:48,880 --> 00:14:52,940
 question. How did I become a monk? I hope that covers the

252
00:14:52,940 --> 00:14:55,200
 doubts that were in your mind or

253
00:14:56,080 --> 00:15:00,760
 answers your question as you would have it answered. Okay,

254
00:15:00,760 --> 00:15:02,800
 thanks. Talk to you soon.

