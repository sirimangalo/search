1
00:00:00,000 --> 00:00:05,000
 Okay, and we're live.

2
00:00:05,000 --> 00:00:07,200
 So welcome everyone.

3
00:00:07,200 --> 00:00:12,440
 We're going to try to study the new opposite today.

4
00:00:12,440 --> 00:00:15,360
 As we tried yesterday and failed.

5
00:00:15,360 --> 00:00:19,720
 That was a, not even a technical failure, that was a human

6
00:00:19,720 --> 00:00:20,400
 error.

7
00:00:20,400 --> 00:00:23,400
 In our part, the internet works get a shutdown.

8
00:00:27,240 --> 00:00:32,480
 So yeah, tonight we'll be studying Majimini Kaya, Supta 25,

9
00:00:32,480 --> 00:00:35,320
 Nuapa, Supta the Bait.

10
00:00:35,320 --> 00:00:44,440
 And format as always is we will start by reciting the Pali.

11
00:00:44,440 --> 00:00:49,880
 On behalf of all of the Supta, we'll see how much time we

12
00:00:49,880 --> 00:00:49,880
 have.

13
00:00:49,880 --> 00:00:55,280
 And then we will study the English, reading it and

14
00:00:55,280 --> 00:00:58,880
 explaining aspects of it that need to be explained.

15
00:00:58,880 --> 00:01:09,170
 Maybe in the future we could set up a Q&A, turn on this Q&A

16
00:01:09,170 --> 00:01:09,680
 thing,

17
00:01:09,680 --> 00:01:10,400
 see how that works.

18
00:01:10,400 --> 00:01:14,280
 We have Q&A for the Supta study, that would be interesting.

19
00:01:14,280 --> 00:01:19,200
 Anyway, not tonight, it's too late.

20
00:01:19,200 --> 00:01:21,560
 You can't turn it on after you started the broadcast.

21
00:01:21,560 --> 00:01:25,960
 [COUGH]

22
00:01:25,960 --> 00:01:28,920
 >> Okay, there it is.

23
00:01:28,920 --> 00:01:31,240
 >> I'm going to turn this feature on and off.

24
00:01:31,240 --> 00:01:34,480
 Yeah, so we can't do it yet.

25
00:01:34,480 --> 00:01:35,240
 Tomorrow we'll try it.

26
00:01:35,240 --> 00:01:39,120
 Tonight we will start with,

27
00:01:39,120 --> 00:01:42,120
 [BLANK_AUDIO]

28
00:01:42,120 --> 00:01:49,080
 Right in with the, well, stick to a standard set up.

29
00:01:49,080 --> 00:01:51,000
 Tomorrow we'll try to have question and answer.

30
00:01:51,000 --> 00:01:57,200
 So if you show up 7 PM Winnipeg time, if you're on YouTube

31
00:01:57,200 --> 00:01:57,560
 or

32
00:01:57,560 --> 00:02:02,190
 Google+, you should be able to see us on my YouTube channel

33
00:02:02,190 --> 00:02:04,400
 tomorrow, I think.

34
00:02:04,400 --> 00:02:06,840
 No, tomorrow's Friday, we won't be having it, I don't think

35
00:02:06,840 --> 00:02:06,960
.

36
00:02:06,960 --> 00:02:13,440
 But besides Friday, anyway, if not, you'll be seeing this

37
00:02:13,440 --> 00:02:13,840
 on YouTube.

38
00:02:13,840 --> 00:02:17,560
 Okay, so it's 7 o'clock, we'll set the screen share and

39
00:02:17,560 --> 00:02:21,840
 here we go with our study session.

40
00:02:21,840 --> 00:02:29,840
 [BLANK_AUDIO]

41
00:02:29,840 --> 00:02:39,840
 [MUSIC]

42
00:02:39,840 --> 00:02:49,840
 [MUSIC]

43
00:02:49,840 --> 00:02:59,840
 [MUSIC]

44
00:02:59,840 --> 00:03:09,840
 [MUSIC]

45
00:03:09,840 --> 00:03:19,840
 [MUSIC]

46
00:03:19,840 --> 00:03:29,840
 [MUSIC]

47
00:03:29,840 --> 00:03:39,840
 [MUSIC]

48
00:03:39,840 --> 00:03:49,840
 [MUSIC]

49
00:03:49,840 --> 00:03:59,840
 [MUSIC]

50
00:03:59,840 --> 00:04:09,840
 [MUSIC]

51
00:04:09,840 --> 00:04:19,840
 [MUSIC]

52
00:04:19,840 --> 00:04:29,840
 [MUSIC]

53
00:04:29,840 --> 00:04:39,840
 [MUSIC]

54
00:04:39,840 --> 00:04:49,840
 [MUSIC]

55
00:04:49,840 --> 00:04:59,840
 [MUSIC]

56
00:04:59,840 --> 00:05:09,840
 [MUSIC]

57
00:05:09,840 --> 00:05:19,840
 [MUSIC]

58
00:05:21,840 --> 00:05:31,840
 [MUSIC]

59
00:05:31,840 --> 00:05:41,840
 [MUSIC]

60
00:05:41,840 --> 00:05:51,840
 [MUSIC]

61
00:05:51,840 --> 00:06:01,840
 [MUSIC]

62
00:06:01,840 --> 00:06:11,840
 [MUSIC]

63
00:06:11,840 --> 00:06:21,840
 [MUSIC]

64
00:06:21,840 --> 00:06:31,840
 [MUSIC]

65
00:06:33,840 --> 00:06:43,840
 [MUSIC]

66
00:06:43,840 --> 00:06:53,840
 [MUSIC]

67
00:06:53,840 --> 00:07:03,840
 [MUSIC]

68
00:07:03,840 --> 00:07:13,840
 [MUSIC]

69
00:07:13,840 --> 00:07:23,840
 [MUSIC]

70
00:07:23,840 --> 00:07:33,840
 [MUSIC]

71
00:07:33,840 --> 00:07:43,840
 [MUSIC]

72
00:07:43,840 --> 00:07:53,840
 [MUSIC]

73
00:07:53,840 --> 00:08:03,840
 [MUSIC]

74
00:08:03,840 --> 00:08:13,840
 [MUSIC]

75
00:08:15,840 --> 00:08:25,840
 [MUSIC]

76
00:08:27,840 --> 00:08:37,840
 [MUSIC]

77
00:08:39,840 --> 00:08:49,840
 [MUSIC]

78
00:08:49,840 --> 00:08:59,840
 [MUSIC]

79
00:08:59,840 --> 00:09:09,840
 [MUSIC]

80
00:09:09,840 --> 00:09:19,840
 [MUSIC]

81
00:09:19,840 --> 00:09:29,840
 [MUSIC]

82
00:09:29,840 --> 00:09:39,840
 [MUSIC]

83
00:09:39,840 --> 00:09:49,840
 [MUSIC]

84
00:09:49,840 --> 00:09:59,840
 [MUSIC]

85
00:09:59,840 --> 00:10:09,840
 [MUSIC]

86
00:10:09,840 --> 00:10:19,840
 [MUSIC]

87
00:10:19,840 --> 00:10:29,840
 [MUSIC]

88
00:10:29,840 --> 00:10:39,840
 [MUSIC]

89
00:10:39,840 --> 00:10:49,840
 [MUSIC]

90
00:10:49,840 --> 00:10:59,840
 [MUSIC]

91
00:10:59,840 --> 00:11:09,840
 [MUSIC]

92
00:11:09,840 --> 00:11:19,840
 [MUSIC]

93
00:11:19,840 --> 00:11:29,840
 [MUSIC]

94
00:11:29,840 --> 00:11:39,840
 [MUSIC]

95
00:11:39,840 --> 00:11:49,840
 [MUSIC]

96
00:11:49,840 --> 00:11:59,840
 [MUSIC]

97
00:11:59,840 --> 00:12:09,840
 [MUSIC]

98
00:12:09,840 --> 00:12:19,840
 [MUSIC]

99
00:12:19,840 --> 00:12:29,840
 [MUSIC]

100
00:12:29,840 --> 00:12:39,840
 [MUSIC]

101
00:12:39,840 --> 00:12:49,840
 [MUSIC]

102
00:12:49,840 --> 00:12:59,840
 [MUSIC]

103
00:12:59,840 --> 00:13:09,840
 [MUSIC]

104
00:13:09,840 --> 00:13:19,840
 [MUSIC]

105
00:13:19,840 --> 00:13:29,840
 [MUSIC]

106
00:13:29,840 --> 00:13:39,840
 [MUSIC]

107
00:13:39,840 --> 00:13:49,840
 [MUSIC]

108
00:13:49,840 --> 00:13:59,840
 [MUSIC]

109
00:13:59,840 --> 00:14:09,840
 [MUSIC]

110
00:14:09,840 --> 00:14:19,840
 [MUSIC]

111
00:14:19,840 --> 00:14:29,840
 [MUSIC]

112
00:14:29,840 --> 00:14:39,840
 [MUSIC]

113
00:14:39,840 --> 00:14:49,840
 [MUSIC]

114
00:14:49,840 --> 00:14:59,840
 [MUSIC]

115
00:14:59,840 --> 00:15:09,840
 [MUSIC]

116
00:15:09,840 --> 00:15:19,840
 [MUSIC]

117
00:15:19,840 --> 00:15:29,840
 [MUSIC]

118
00:15:29,840 --> 00:15:39,840
 [MUSIC]

119
00:15:39,840 --> 00:15:49,840
 [MUSIC]

120
00:15:49,840 --> 00:15:59,840
 [MUSIC]

121
00:15:59,840 --> 00:16:09,840
 [MUSIC]

122
00:16:09,840 --> 00:16:19,840
 [MUSIC]

123
00:16:19,840 --> 00:16:29,840
 [MUSIC]

124
00:16:29,840 --> 00:16:39,840
 [MUSIC]

125
00:16:41,840 --> 00:16:49,840
 [MUSIC]

126
00:16:49,840 --> 00:16:59,840
 [MUSIC]

127
00:16:59,840 --> 00:17:09,840
 [MUSIC]

128
00:17:09,840 --> 00:17:19,840
 [MUSIC]

129
00:17:19,840 --> 00:17:29,840
 [MUSIC]

130
00:17:29,840 --> 00:17:39,840
 [MUSIC]

131
00:17:39,840 --> 00:17:49,840
 [MUSIC]

132
00:17:49,840 --> 00:17:59,840
 [MUSIC]

133
00:17:59,840 --> 00:18:09,840
 [MUSIC]

134
00:18:09,840 --> 00:18:19,840
 [MUSIC]

135
00:18:19,840 --> 00:18:29,840
 [MUSIC]

136
00:18:29,840 --> 00:18:39,840
 [MUSIC]

137
00:18:39,840 --> 00:18:49,840
 [MUSIC]

138
00:18:49,840 --> 00:18:59,840
 [MUSIC]

139
00:18:59,840 --> 00:19:09,840
 [MUSIC]

140
00:19:09,840 --> 00:19:19,840
 [MUSIC]

141
00:19:19,840 --> 00:19:29,840
 [MUSIC]

142
00:19:29,840 --> 00:19:39,840
 [MUSIC]

143
00:19:39,840 --> 00:19:49,840
 [MUSIC]

144
00:19:49,840 --> 00:19:59,840
 [MUSIC]

145
00:19:59,840 --> 00:20:09,840
 [MUSIC]

146
00:20:09,840 --> 00:20:19,840
 [MUSIC]

147
00:20:19,840 --> 00:20:29,840
 [MUSIC]

148
00:20:29,840 --> 00:20:39,840
 [MUSIC]

149
00:20:39,840 --> 00:20:49,840
 [MUSIC]

150
00:20:49,840 --> 00:20:59,840
 [MUSIC]

151
00:20:59,840 --> 00:21:09,840
 [MUSIC]

152
00:21:09,840 --> 00:21:19,840
 [MUSIC]

153
00:21:19,840 --> 00:21:29,840
 [MUSIC]

154
00:21:29,840 --> 00:21:39,840
 [MUSIC]

155
00:21:39,840 --> 00:21:49,840
 [MUSIC]

156
00:21:49,840 --> 00:21:59,840
 [MUSIC]

157
00:21:59,840 --> 00:22:09,840
 [MUSIC]

158
00:22:09,840 --> 00:22:19,840
 [MUSIC]

159
00:22:19,840 --> 00:22:29,840
 [MUSIC]

160
00:22:29,840 --> 00:22:39,840
 [MUSIC]

161
00:22:39,840 --> 00:22:47,840
 [MUSIC]

162
00:22:47,840 --> 00:22:57,840
 [MUSIC]

163
00:22:57,840 --> 00:23:07,840
 [MUSIC]

164
00:23:07,840 --> 00:23:17,840
 [MUSIC]

165
00:23:17,840 --> 00:23:27,840
 [MUSIC]

166
00:23:27,840 --> 00:23:37,840
 [MUSIC]

167
00:23:37,840 --> 00:23:47,840
 [MUSIC]

168
00:23:47,840 --> 00:23:57,840
 [MUSIC]

169
00:23:57,840 --> 00:24:07,840
 [MUSIC]

170
00:24:07,840 --> 00:24:17,840
 [MUSIC]

171
00:24:17,840 --> 00:24:27,840
 [MUSIC]

172
00:24:27,840 --> 00:24:37,840
 [MUSIC]

173
00:24:37,840 --> 00:24:47,840
 [MUSIC]

174
00:24:47,840 --> 00:24:57,840
 [MUSIC]

175
00:24:57,840 --> 00:25:07,840
 [MUSIC]

176
00:25:07,840 --> 00:25:17,840
 [MUSIC]

177
00:25:17,840 --> 00:25:25,840
 [MUSIC]

178
00:25:25,840 --> 00:25:35,840
 [MUSIC]

179
00:25:35,840 --> 00:25:45,840
 [MUSIC]

180
00:25:45,840 --> 00:25:55,840
 [MUSIC]

181
00:25:55,840 --> 00:26:05,840
 [MUSIC]

182
00:26:05,840 --> 00:26:15,840
 [MUSIC]

183
00:26:15,840 --> 00:26:25,840
 [MUSIC]

184
00:26:25,840 --> 00:26:35,840
 [MUSIC]

185
00:26:35,840 --> 00:26:45,840
 [MUSIC]

186
00:26:47,840 --> 00:26:55,840
 [MUSIC]

187
00:26:55,840 --> 00:27:05,840
 [MUSIC]

188
00:27:05,840 --> 00:27:15,840
 [MUSIC]

189
00:27:15,840 --> 00:27:25,840
 [MUSIC]

190
00:27:25,840 --> 00:27:27,840
 [COUGH]

191
00:27:27,840 --> 00:27:31,840
 >> Trying to compare our state of affairs,

192
00:27:31,840 --> 00:27:34,840
 [COUGH]

193
00:27:34,840 --> 00:27:40,840
 problem that we are trying to address as Buddhists,

194
00:27:40,840 --> 00:27:50,840
 the problem of the debate and the desire for the debate,

195
00:27:50,840 --> 00:27:56,840
 the desire ability of the debate and this,

196
00:27:56,840 --> 00:27:59,840
 [BLANK_AUDIO]

197
00:27:59,840 --> 00:28:00,840
 deer trapper.

198
00:28:00,840 --> 00:28:02,840
 Now the word isn't actually deer trapper.

199
00:28:02,840 --> 00:28:03,840
 [COUGH]

200
00:28:03,840 --> 00:28:12,840
 Niwapa means bait and the word is niwapika.

201
00:28:12,840 --> 00:28:15,840
 So you take niwapa and you put an A there,

202
00:28:15,840 --> 00:28:19,840
 which is strengthened from E to A and then you put

203
00:28:19,840 --> 00:28:20,840
 [INAUDIBLE]

204
00:28:20,840 --> 00:28:24,840
 niwapika, I think it changes to ika as well.

205
00:28:24,840 --> 00:28:28,840
 It means one who has bait or a baiter.

206
00:28:28,840 --> 00:28:30,840
 You might say a trapper.

207
00:28:30,840 --> 00:28:33,840
 The only English word that we have is a trapper,

208
00:28:33,840 --> 00:28:37,840
 so it's probably the meaning here is trapper.

209
00:28:37,840 --> 00:28:43,840
 So niwapa could be the trap and niwapika is the trapper

210
00:28:43,840 --> 00:28:51,840
 or the bait and the baiter is probably how it should be.

211
00:28:51,840 --> 00:28:53,840
 But he's gonna explain the simile himself,

212
00:28:53,840 --> 00:28:56,840
 so I guess I'm not gonna do it for us.

213
00:28:56,840 --> 00:28:58,840
 Just keep reading.

214
00:28:58,840 --> 00:29:01,840
 Now the deer, the first heard ate food unwarily

215
00:29:01,840 --> 00:29:03,840
 by going right in amongst the bait that the deer

216
00:29:03,840 --> 00:29:04,840
 trapper laid down.

217
00:29:04,840 --> 00:29:06,840
 By doing so, they become intoxicated.

218
00:29:06,840 --> 00:29:09,840
 When they were intoxicated, they fell into negligence.

219
00:29:09,840 --> 00:29:11,840
 When they were negligent, the deer trapper

220
00:29:11,840 --> 00:29:14,840
 did what they liked on account of that bait.

221
00:29:14,840 --> 00:29:16,840
 That is how the deer, the first heard,

222
00:29:16,840 --> 00:29:20,980
 failed to get free from the deer trapper's power and

223
00:29:20,980 --> 00:29:22,840
 control.

224
00:29:22,840 --> 00:29:27,840
 Maybe there's something we can say here.

225
00:29:27,840 --> 00:29:30,840
 He's making this point, which is going to become clear

226
00:29:30,840 --> 00:29:32,840
 when he explains it.

227
00:29:32,840 --> 00:29:35,650
 I think it's fairly transparent what he's talking about

228
00:29:35,650 --> 00:29:35,840
 here,

229
00:29:35,840 --> 00:29:40,840
 but the point that should be emphasized is that

230
00:29:40,840 --> 00:29:46,840
 the universe doesn't conspire for us to be happy.

231
00:29:46,840 --> 00:29:54,840
 Anyway, there is this situation where you become baited,

232
00:29:54,840 --> 00:29:58,840
 and this is actually a very common,

233
00:29:58,840 --> 00:30:03,840
 much more ever ubiquitous than we would think.

234
00:30:03,840 --> 00:30:05,840
 It's in everything we do, really.

235
00:30:05,840 --> 00:30:13,670
 Many of our interactions in the world have some strings

236
00:30:13,670 --> 00:30:14,840
 attached.

237
00:30:14,840 --> 00:30:17,840
 Anything that has strings attached,

238
00:30:17,840 --> 00:30:23,770
 that's sort of the image, the simile of having strings

239
00:30:23,770 --> 00:30:24,840
 attached,

240
00:30:24,840 --> 00:30:29,840
 is really, there are pulls on you.

241
00:30:29,840 --> 00:30:31,840
 As soon as you accept the conditions,

242
00:30:31,840 --> 00:30:33,840
 then there are pulls on you.

243
00:30:33,840 --> 00:30:36,840
 This is why they say there's no such thing as a free lunch,

244
00:30:36,840 --> 00:30:38,840
 which of course I can test.

245
00:30:38,840 --> 00:30:42,840
 But in most cases, there is no free lunch.

246
00:30:42,840 --> 00:30:45,570
 In most cases, a free lunch only puts you in someone's

247
00:30:45,570 --> 00:30:45,840
 power

248
00:30:45,840 --> 00:30:53,840
 because of expectations, because of desire, actually.

249
00:30:53,840 --> 00:31:00,060
 What he's saying in this simile is the addiction to the

250
00:31:00,060 --> 00:31:00,840
 bait, actually.

251
00:31:00,840 --> 00:31:03,840
 He's not talking about a trap in the ordinary sense,

252
00:31:03,840 --> 00:31:07,840
 and it's actually the way it goes with deer.

253
00:31:07,840 --> 00:31:16,840
 That's a big reason for having...

254
00:31:16,840 --> 00:31:21,090
 When people hunt deer, they will go to the fruit trees, for

255
00:31:21,090 --> 00:31:21,840
 example,

256
00:31:21,840 --> 00:31:26,840
 and they'll grow fruit trees not just for the fruit,

257
00:31:26,840 --> 00:31:29,840
 but they'll leave some of the fruit for the deer.

258
00:31:29,840 --> 00:31:35,530
 The deer are sure every year to come back to the same fruit

259
00:31:35,530 --> 00:31:35,840
 tree,

260
00:31:35,840 --> 00:31:40,840
 and so it's very easy to find the deer.

261
00:31:40,840 --> 00:31:44,840
 It's loosely skirting the law because you can't bait deer.

262
00:31:44,840 --> 00:31:48,710
 This is the easiest way to catch an animal, of course, with

263
00:31:48,710 --> 00:31:49,840
 some kind of bait.

264
00:31:49,840 --> 00:31:52,840
 You get them addicted to this, and then they can't help it.

265
00:31:52,840 --> 00:31:54,840
 They'll never be scared enough to run away from it

266
00:31:54,840 --> 00:32:00,840
 because their brains are too small to understand,

267
00:32:00,840 --> 00:32:04,840
 or this is the hope of the hunter anyway.

268
00:32:04,840 --> 00:32:09,840
 His hope is that he can entice the deer

269
00:32:09,840 --> 00:32:12,840
 and then, as a result, do what they want.

270
00:32:12,840 --> 00:32:14,840
 This is the warning that the Buddha's giving,

271
00:32:14,840 --> 00:32:23,840
 that people don't give out free lunches most of the time

272
00:32:23,840 --> 00:32:26,840
 thinking, "Hey, I want to give this person a free lunch."

273
00:32:26,840 --> 00:32:29,840
 Most of the time, the free lunch is thinking,

274
00:32:29,840 --> 00:32:31,840
 "I'm going to make lots of money off this."

275
00:32:31,840 --> 00:32:33,840
 This is what you see on the Internet a lot now,

276
00:32:33,840 --> 00:32:35,840
 the Google+, of course.

277
00:32:35,840 --> 00:32:39,840
 Although I like Google, I think they're actually thinking

278
00:32:39,840 --> 00:32:43,840
 to help people to the universe in their own way.

279
00:32:43,840 --> 00:32:47,840
 Facebook is not...

280
00:32:47,840 --> 00:32:53,840
 Its main goal is not to connect people.

281
00:32:53,840 --> 00:32:56,840
 Its main goal is to make money.

282
00:32:56,840 --> 00:32:59,840
 For example, I mean, I shouldn't pick out examples,

283
00:32:59,840 --> 00:33:03,840
 but on the Internet, so much free stuff, right?

284
00:33:03,840 --> 00:33:05,840
 People have learned that giving away free stuff

285
00:33:05,840 --> 00:33:07,840
 is a great way to make money

286
00:33:07,840 --> 00:33:12,840
 because it traps you in what they call these ecosystems,

287
00:33:12,840 --> 00:33:13,840
 right?

288
00:33:13,840 --> 00:33:14,840
 You can buy an Apple product.

289
00:33:14,840 --> 00:33:16,840
 I shouldn't make examples today.

290
00:33:16,840 --> 00:33:18,840
 You buy a certain product,

291
00:33:18,840 --> 00:33:22,840
 and then you're stuck into their ecosystem,

292
00:33:22,840 --> 00:33:25,840
 their walled garden or whatever.

293
00:33:25,840 --> 00:33:26,840
 The Capeen is not a prophet.

294
00:33:26,840 --> 00:33:28,840
 That's pretty cool.

295
00:33:28,840 --> 00:33:34,840
 Capeen is an example of not what we're talking about.

296
00:33:34,840 --> 00:33:39,840
 Maybe an example of a free lunch, free lunches.

297
00:33:39,840 --> 00:33:42,840
 It's an example, and it's a sign of the goodness

298
00:33:42,840 --> 00:33:44,840
 that exists in the world, I think.

299
00:33:44,840 --> 00:33:46,840
 It's inherent.

300
00:33:46,840 --> 00:33:47,840
 But there's attachment there.

301
00:33:47,840 --> 00:33:50,840
 You see, there's the attachment to knowledge,

302
00:33:50,840 --> 00:33:52,840
 attachment to...

303
00:33:52,840 --> 00:33:54,840
 There can be ego and fights break out,

304
00:33:54,840 --> 00:33:58,840
 so the general principle is somehow good,

305
00:33:58,840 --> 00:34:00,840
 at least in a worldly sense.

306
00:34:00,840 --> 00:34:08,840
 So some deer take the bait right away.

307
00:34:08,840 --> 00:34:09,840
 The bait is there, and they take it,

308
00:34:09,840 --> 00:34:12,840
 and when they do it, they get addicted to it,

309
00:34:12,840 --> 00:34:14,840
 and when they're addicted, they become negligent,

310
00:34:14,840 --> 00:34:18,840
 because they get fat and they get...

311
00:34:18,840 --> 00:34:23,840
 well, negligent, overconfident.

312
00:34:23,840 --> 00:34:24,840
 And so it's very easy to catch that.

313
00:34:24,840 --> 00:34:29,840
 They become domesticated almost.

314
00:34:29,840 --> 00:34:33,840
 Now the deer of a second herd reckon thus.

315
00:34:33,840 --> 00:34:34,840
 The deer of that first herd,

316
00:34:34,840 --> 00:34:36,840
 by acting as they do without precaution,

317
00:34:36,840 --> 00:34:40,200
 failed to get free from the deer trapper's power and

318
00:34:40,200 --> 00:34:40,840
 control.

319
00:34:40,840 --> 00:34:43,840
 Suppose we altogether shunned that bait food,

320
00:34:43,840 --> 00:34:45,840
 shutting that fearful enjoyment.

321
00:34:45,840 --> 00:34:48,840
 Let's go out into the forest wilds and live there.

322
00:34:48,840 --> 00:34:50,840
 And they did so.

323
00:34:50,840 --> 00:34:52,840
 But in the last month of the hot season,

324
00:34:52,840 --> 00:34:54,840
 when the grass and the water were used up,

325
00:34:54,840 --> 00:34:57,840
 their bodies were reduced to extreme emaciation,

326
00:34:57,840 --> 00:35:00,840
 and with that, they lost their strength and energy.

327
00:35:00,840 --> 00:35:02,840
 When they had lost their strength and energy,

328
00:35:02,840 --> 00:35:06,330
 they returned that same bait that the deer trapper had laid

329
00:35:06,330 --> 00:35:06,840
 down.

330
00:35:06,840 --> 00:35:10,840
 They ate food unwearily by going right in amongst it.

331
00:35:10,840 --> 00:35:12,840
 By doing so, they became intoxicated.

332
00:35:12,840 --> 00:35:15,840
 When they were intoxicated, they fell into negligence.

333
00:35:15,840 --> 00:35:18,450
 When they were negligent, the deer trapper did with him as

334
00:35:18,450 --> 00:35:18,840
 he liked,

335
00:35:18,840 --> 00:35:19,840
 on account of that bait.

336
00:35:19,840 --> 00:35:22,840
 And that as hell, a deer of the second herd also failed to

337
00:35:22,840 --> 00:35:22,840
 get free

338
00:35:22,840 --> 00:35:24,840
 from the deer trapper's power and control.

339
00:35:24,840 --> 00:35:29,840
 So some deer are wise to the hunter,

340
00:35:29,840 --> 00:35:33,840
 and they see other deer...

341
00:35:33,840 --> 00:35:36,840
 Well, this is how it goes with hunters.

342
00:35:36,840 --> 00:35:38,840
 When you kill a deer in the spot,

343
00:35:38,840 --> 00:35:40,840
 it gets harder to kill more deer in that spot

344
00:35:40,840 --> 00:35:45,840
 because they see that the other deer were done for,

345
00:35:45,840 --> 00:35:51,840
 or were destroyed or lost because of taking the bait.

346
00:35:51,840 --> 00:35:53,840
 So they avoid the fruit trees.

347
00:35:53,840 --> 00:35:54,840
 They avoid the fruit.

348
00:35:54,840 --> 00:35:59,840
 They avoid the bait, whatever bait is put out.

349
00:35:59,840 --> 00:36:05,460
 But eventually, it gets real bad, and so they have to go

350
00:36:05,460 --> 00:36:05,840
 back.

351
00:36:05,840 --> 00:36:08,840
 This goes for people as well.

352
00:36:08,840 --> 00:36:13,340
 We know that something is wrong, and we try and stay away

353
00:36:13,340 --> 00:36:13,840
 from it.

354
00:36:13,840 --> 00:36:17,840
 Without meditation practice, without insight,

355
00:36:17,840 --> 00:36:20,840
 without actually seeing it and getting bored by it,

356
00:36:20,840 --> 00:36:23,840
 and becoming disinterested in it,

357
00:36:23,840 --> 00:36:32,160
 without clear and absolute understanding of it or what it

358
00:36:32,160 --> 00:36:32,840
 is,

359
00:36:32,840 --> 00:36:38,150
 you can say, "No, no, no, no, no," and abstain, abstain,

360
00:36:38,150 --> 00:36:38,840
 abstain.

361
00:36:38,840 --> 00:36:41,500
 All you're doing is abstaining, and you're not actually

362
00:36:41,500 --> 00:36:41,840
 learning.

363
00:36:41,840 --> 00:36:44,840
 You're not actually changing.

364
00:36:44,840 --> 00:36:47,840
 Changing your priorities, changing your desires,

365
00:36:47,840 --> 00:36:51,840
 changing your intentions, then there's no point.

366
00:36:51,840 --> 00:36:53,840
 Well, there's no point.

367
00:36:53,840 --> 00:36:55,840
 Eventually, it comes back to you.

368
00:36:55,840 --> 00:36:57,840
 Eventually, it catches up to you,

369
00:36:57,840 --> 00:36:59,840
 and this is what's happening here.

370
00:36:59,840 --> 00:37:01,840
 We got the third.

