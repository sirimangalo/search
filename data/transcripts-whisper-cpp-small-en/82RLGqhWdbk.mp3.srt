1
00:00:00,000 --> 00:00:05,460
 I have made a 24-hour retreat and making nightmares since

2
00:00:05,460 --> 00:00:08,000
 then. Why is that happening? Is it meaning something?

3
00:00:08,000 --> 00:00:13,000
 My teacher always said, he said, "Good to have nightmares."

4
00:00:13,000 --> 00:00:16,200
 He said, "When you have bad dreams, it's a sign that good

5
00:00:16,200 --> 00:00:18,000
 things are going to come."

6
00:00:18,000 --> 00:00:22,170
 I think he was just talking cultural stuff, Thai stuff. I'm

7
00:00:22,170 --> 00:00:24,000
 not sure actually.

8
00:00:24,000 --> 00:00:29,110
 But it's an interesting conceptual teaching. I mean, the

9
00:00:29,110 --> 00:00:33,000
 reality of it, you'd have to get into psychology and so on.

10
00:00:33,000 --> 00:00:39,270
 And I don't want to make too much of an assumption. But in

11
00:00:39,270 --> 00:00:41,810
 general, when you're meditating a lot of stuff that we've

12
00:00:41,810 --> 00:00:44,000
 kept repressed comes out.

13
00:00:44,000 --> 00:00:46,620
 I don't want it to sound like we're trying to pull stuff

14
00:00:46,620 --> 00:00:48,000
 out. We're certainly not.

15
00:00:48,000 --> 00:00:51,960
 And I don't want you to expect that you're going to pull up

16
00:00:51,960 --> 00:00:54,000
 skeletons in the closet.

17
00:00:54,000 --> 00:00:56,830
 When I first started, the first teachers I had were always

18
00:00:56,830 --> 00:01:00,000
 on about this, talking about, "Are you repressing something

19
00:01:00,000 --> 00:01:00,000
?"

20
00:01:00,000 --> 00:01:02,720
 "Or I have the feeling that you've got something deep down

21
00:01:02,720 --> 00:01:04,000
?" And it drove me crazy, really.

22
00:01:04,000 --> 00:01:06,880
 Because I don't have anything hidden deep down, and yet I

23
00:01:06,880 --> 00:01:09,000
 was looking for it. And I can tell you that I don't.

24
00:01:09,000 --> 00:01:15,050
 If I do, man, it's buried. It was really, you know, I was

25
00:01:15,050 --> 00:01:17,000
 just messed up in general.

26
00:01:17,000 --> 00:01:20,820
 There was nothing hidden about it. I had been watching God

27
00:01:20,820 --> 00:01:25,000
father movies for the past.

28
00:01:25,000 --> 00:01:29,740
 I don't know, Scarface and all that. This is how I've been

29
00:01:29,740 --> 00:01:33,000
 living my spiritual life.

30
00:01:33,000 --> 00:01:41,490
 But it does happen that the things that you repress, just

31
00:01:41,490 --> 00:01:49,280
 to keep facade of normality, are brought up because you

32
00:01:49,280 --> 00:01:50,000
 stop that.

33
00:01:50,000 --> 00:01:57,280
 You quit the fighting. You stop fighting against them. You

34
00:01:57,280 --> 00:02:01,000
 stop pushing them away. You stop pretending.

35
00:02:01,000 --> 00:02:07,790
 Most people are, the difference between people who are able

36
00:02:07,790 --> 00:02:11,750
 to get by in society and those that aren't is generally

37
00:02:11,750 --> 00:02:13,000
 just a pretension.

38
00:02:13,000 --> 00:02:16,110
 Some people are able to fake it better than others. For

39
00:02:16,110 --> 00:02:19,310
 most of us, it's just faking it. I can't get along well in

40
00:02:19,310 --> 00:02:20,000
 society.

41
00:02:20,000 --> 00:02:28,990
 I feel somehow it has to do with my interest in reality as

42
00:02:28,990 --> 00:02:34,200
 opposed to faking it, because it's tiresome to have to keep

43
00:02:34,200 --> 00:02:35,000
 up appearances.

44
00:02:35,000 --> 00:02:38,880
 When you meditate, you start to realize that. And so you

45
00:02:38,880 --> 00:02:44,560
 let your guard down. And as a result, a lot of crazy stuff

46
00:02:44,560 --> 00:02:46,000
 comes up.

47
00:02:46,000 --> 00:02:51,460
 So in the beginning or at certain times, this can lead to

48
00:02:51,460 --> 00:02:53,000
 nightmares.

49
00:02:53,000 --> 00:02:56,380
 I would say that that's probably a good guess as to what's

50
00:02:56,380 --> 00:02:59,440
 happening in your case. Sometimes you have very vivid

51
00:02:59,440 --> 00:03:00,000
 dreams.

52
00:03:00,000 --> 00:03:05,720
 But I think it's clear that apparently, what did my teacher

53
00:03:05,720 --> 00:03:09,000
 say? He said, "An enlightened being doesn't dream."

54
00:03:09,000 --> 00:03:12,230
 And that makes sense because an enlightened being's mind is

55
00:03:12,230 --> 00:03:15,330
 very clear. And when they sleep, they sleep. When they're

56
00:03:15,330 --> 00:03:16,000
 awake, they're awake.

57
00:03:16,000 --> 00:03:19,960
 They don't have any of this mental fomentation. It's a sign

58
00:03:19,960 --> 00:03:23,530
 that stuff is coming up. And you should never, ever, ever

59
00:03:23,530 --> 00:03:25,000
 take your dreams seriously.

60
00:03:25,000 --> 00:03:28,210
 I can't remember. I think I did a video about dreams. I did

61
00:03:28,210 --> 00:03:30,750
 take a video, do a video about dreams when I was in America

62
00:03:30,750 --> 00:03:31,000
.

63
00:03:31,000 --> 00:03:33,820
 Look that video up, because I think I said everything that

64
00:03:33,820 --> 00:03:36,000
 I have to say about dreams in that video.

65
00:03:36,000 --> 00:03:40,810
 Don't take them seriously. And whatever else I said on that

66
00:03:40,810 --> 00:03:44,000
 video, endorsing it 100%. Hopefully it was all correct.

