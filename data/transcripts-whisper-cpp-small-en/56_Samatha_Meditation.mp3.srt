1
00:00:00,000 --> 00:00:02,000
 Samatha Meditation

2
00:00:02,000 --> 00:00:05,520
 Question

3
00:00:05,520 --> 00:00:09,550
 "Pante, based on many of your videos, I think you do not

4
00:00:09,550 --> 00:00:11,480
 like samatha meditation at all.

5
00:00:11,480 --> 00:00:16,770
 From what I know there are two clear ways for enlightenment

6
00:00:16,770 --> 00:00:17,180
.

7
00:00:17,180 --> 00:00:20,440
 Samatha first and then vipassana or

8
00:00:20,440 --> 00:00:23,600
 vipassana first and then samatha.

9
00:00:23,600 --> 00:00:26,640
 What do you think about this?"

10
00:00:29,680 --> 00:00:31,400
 Answer

11
00:00:31,400 --> 00:00:34,920
 Actually according to the Buddha there are four ways to

12
00:00:34,920 --> 00:00:36,140
 enlightenment.

13
00:00:36,140 --> 00:00:40,140
 Samatha first and then vipassana.

14
00:00:40,140 --> 00:00:43,520
 Vipassana first and then samatha.

15
00:00:43,520 --> 00:00:46,880
 Samatha and vipassana together.

16
00:00:46,880 --> 00:00:52,500
 Or the settling of the mind in regards to the dhammas.

17
00:00:52,500 --> 00:00:57,610
 The Buddha said those are the four ways people can become

18
00:00:57,610 --> 00:00:58,640
 an arahant.

19
00:01:00,640 --> 00:01:04,190
 Regarding your comment of you thinking I do not like sam

20
00:01:04,190 --> 00:01:06,800
atha meditation at all,

21
00:01:06,800 --> 00:01:12,630
 I will say that if that is true, then it is wrong of me of

22
00:01:12,630 --> 00:01:13,720
 course,

23
00:01:13,720 --> 00:01:17,800
 because disliking itself is a bad thing.

24
00:01:17,800 --> 00:01:22,390
 I admit that I tend to discourage people from focusing

25
00:01:22,390 --> 00:01:24,080
 their attention on

26
00:01:24,080 --> 00:01:27,080
 cultivating samatha meditation.

27
00:01:27,960 --> 00:01:31,600
 I do not ever say to not practice it.

28
00:01:31,600 --> 00:01:36,860
 Rather I try to focus on the benefits of vipassana

29
00:01:36,860 --> 00:01:37,800
 meditation.

30
00:01:37,800 --> 00:01:41,280
 There are two reasons for that.

31
00:01:41,280 --> 00:01:46,360
 First, vipassana meditation is what I know,

32
00:01:46,360 --> 00:01:51,680
 so it is what I am best suited to help them with.

33
00:01:52,760 --> 00:01:57,170
 If instead I start talking about the benefits of samatha

34
00:01:57,170 --> 00:01:58,360
 meditation,

35
00:01:58,360 --> 00:02:03,190
 I am not going to be able to help anyone realize those

36
00:02:03,190 --> 00:02:04,160
 benefits.

37
00:02:04,160 --> 00:02:09,310
 I will end up saying to them, I am sorry I do not teach sam

38
00:02:09,310 --> 00:02:10,680
atha meditation,

39
00:02:10,680 --> 00:02:13,280
 you will have to go somewhere else.

40
00:02:13,280 --> 00:02:18,480
 So it is better for me to promote vipassana meditation

41
00:02:18,480 --> 00:02:21,880
 because I can help with that directly.

42
00:02:22,560 --> 00:02:26,700
 The other reason I do not recommend samatha first is

43
00:02:26,700 --> 00:02:32,320
 because that path seems to me to have three disadvantages.

44
00:02:32,320 --> 00:02:38,720
 It seems to take longer, it requires more effort,

45
00:02:38,720 --> 00:02:43,500
 and it has a greater potential for falling into wrong

46
00:02:43,500 --> 00:02:44,520
 practice.

47
00:02:46,000 --> 00:02:50,800
 The benefits of practicing samatha meditation first are

48
00:02:50,800 --> 00:02:54,400
 that it is more complete and more powerful.

49
00:02:54,400 --> 00:02:59,840
 More complete means it provides the potential to enter into

50
00:02:59,840 --> 00:03:03,080
 profound states of calm and tranquility

51
00:03:03,080 --> 00:03:09,840
 where one can sit stiff as a board in deep bliss or calm.

52
00:03:10,640 --> 00:03:15,250
 It is also more complete in the sense that it allows the

53
00:03:15,250 --> 00:03:17,840
 cultivation of magical powers,

54
00:03:17,840 --> 00:03:23,380
 reading people's minds, remembering past lives, all sorts

55
00:03:23,380 --> 00:03:24,760
 of fun stuff.

56
00:03:24,760 --> 00:03:31,060
 It is more powerful in that one's ability to enter and

57
00:03:31,060 --> 00:03:34,960
 remain in cessation becomes more powerful.

58
00:03:35,600 --> 00:03:41,010
 Having cultivated samatha meditation, you can more easily

59
00:03:41,010 --> 00:03:45,800
 enter into cessation for hours or days on end.

60
00:03:45,800 --> 00:03:48,680
 Those are the two benefits.

61
00:03:48,680 --> 00:03:55,970
 Regarding the disadvantages, practicing samatha first takes

62
00:03:55,970 --> 00:04:01,060
 longer because you need to first cultivate meditation based

63
00:04:01,060 --> 00:04:05,120
 on a concept that has nothing to do with reality.

64
00:04:06,040 --> 00:04:10,170
 True freedom from suffering comes only from understanding

65
00:04:10,170 --> 00:04:14,900
 reality, so you need to eventually change the object of

66
00:04:14,900 --> 00:04:16,720
 your meditation.

67
00:04:16,720 --> 00:04:22,400
 Focusing on concepts only suppresses mental defilement.

68
00:04:22,400 --> 00:04:28,010
 Practicing samatha first requires more because in order to

69
00:04:28,010 --> 00:04:33,040
 practice samatha, you need to seclude yourself.

70
00:04:33,520 --> 00:04:38,670
 You need to find a quiet place away from distraction and

71
00:04:38,670 --> 00:04:42,320
 this is not always easy or even possible.

72
00:04:42,320 --> 00:04:48,390
 Samatha meditation is ideally practiced in the wilderness,

73
00:04:48,390 --> 00:04:53,520
 away from any kind of human contact or worldly disruption.

74
00:04:53,520 --> 00:05:00,420
 The third disadvantage of practicing samatha first is the

75
00:05:00,420 --> 00:05:03,040
 potential for getting lost.

76
00:05:04,000 --> 00:05:08,410
 As samatha deals with the realm of concepts, it is possible

77
00:05:08,410 --> 00:05:13,850
 to become fully accomplished in profound states of peace

78
00:05:13,850 --> 00:05:18,720
 and bliss without every attaining enlightenment.

79
00:05:18,720 --> 00:05:23,050
 It is clear from the text that such achievements are not

80
00:05:23,050 --> 00:05:27,120
 sufficient for the attainment of enlightenment.

81
00:05:27,920 --> 00:05:32,840
 They lead at most to rebirth in the Brahma words as in the

82
00:05:32,840 --> 00:05:36,880
 example of the bodhisattva's two teachers.

83
00:05:36,880 --> 00:05:43,120
 Now, samatha is great. It is a wonderful thing. It quiets

84
00:05:43,120 --> 00:05:49,520
 and calms your mind. It is not a matter of disliking it.

85
00:05:49,520 --> 00:05:54,430
 We do not call to it samatha meditation because we do not

86
00:05:54,430 --> 00:05:56,880
 have the time or resources.

87
00:05:58,080 --> 00:06:02,980
 We streamline the path because our time and resources are

88
00:06:02,980 --> 00:06:04,960
 of the essence.

89
00:06:04,960 --> 00:06:10,700
 So we teach the most direct path to help the most people. I

90
00:06:10,700 --> 00:06:14,480
 do not think that I do not like samatha.

91
00:06:14,480 --> 00:06:19,410
 When I was young, I did practice it, sort of not knowing

92
00:06:19,410 --> 00:06:23,700
 what it was, but it was very different from what I

93
00:06:23,700 --> 00:06:25,440
 practiced now.

94
00:06:26,160 --> 00:06:31,750
 I do take issue with those who do not see the difference,

95
00:06:31,750 --> 00:06:36,180
 those who think they are practicing vipassana but are

96
00:06:36,180 --> 00:06:38,160
 actually practicing samatha.

97
00:06:38,160 --> 00:06:42,840
 It is tough because you cannot just tell someone that their

98
00:06:42,840 --> 00:06:46,560
 practice is inferior to your own practice.

99
00:06:47,440 --> 00:06:53,570
 Everyone is going to say that their practice is better, but

100
00:06:53,570 --> 00:06:59,160
 it is an unavoidable reality that some meditation cannot

101
00:06:59,160 --> 00:07:04,560
 lead to enlightenment because it is not focused on reality.

102
00:07:06,000 --> 00:07:10,770
 It is like the story of the person who lost their contact

103
00:07:10,770 --> 00:07:15,460
 lens in a dark room and went outside to look for it because

104
00:07:15,460 --> 00:07:17,280
 the light was better.

105
00:07:17,280 --> 00:07:21,870
 Even though samatha meditation might be easier and more

106
00:07:21,870 --> 00:07:28,090
 pleasant, that is primarily because it is conceptually

107
00:07:28,090 --> 00:07:28,960
 based.

