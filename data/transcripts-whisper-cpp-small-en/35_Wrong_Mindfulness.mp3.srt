1
00:00:00,000 --> 00:00:04,240
 difficulties in meditation. Wrong Mindfulness

2
00:00:04,240 --> 00:00:08,650
 The Buddha taught that mindfulness is always good. In the

3
00:00:08,650 --> 00:00:10,000
 Abhidharma it is called

4
00:00:10,000 --> 00:00:15,710
 Soba-nacetasika, which means a beautiful mind state. The

5
00:00:15,710 --> 00:00:18,400
 Buddha himself did however use the term

6
00:00:18,400 --> 00:00:22,240
 wrong mindfulness at times. To answer the question of what

7
00:00:22,240 --> 00:00:23,920
 wrong mindfulness might mean,

8
00:00:23,920 --> 00:00:26,960
 we can begin with a technical definition of mindfulness.

9
00:00:28,160 --> 00:00:31,510
 In the traditional Theravada texts, all dharmas have four

10
00:00:31,510 --> 00:00:34,480
 qualities. Each dharma will have a

11
00:00:34,480 --> 00:00:39,250
 characteristic, a function, a manifestation, an approximate

12
00:00:39,250 --> 00:00:42,960
 or nearby cause. The characteristic

13
00:00:42,960 --> 00:00:47,690
 of mindfulness is being unwavering. The ordinary mind wob

14
00:00:47,690 --> 00:00:50,720
bles and wavers, does not stable nor fixed

15
00:00:50,720 --> 00:00:54,360
 on an object. Mindfulness enables grasping the object

16
00:00:54,360 --> 00:00:58,000
 firmly without wavering. The function

17
00:00:58,000 --> 00:01:01,900
 of mindfulness is to not forget. Ordinary mindfulness

18
00:01:01,900 --> 00:01:03,920
 refers to the ability to recollect

19
00:01:03,920 --> 00:01:07,480
 things that happened long ago. Then there is Satipatthana,

20
00:01:07,480 --> 00:01:09,760
 the mindfulness meditation practice.

21
00:01:09,760 --> 00:01:13,530
 The meaning of Satipatthana is to not forget the object in

22
00:01:13,530 --> 00:01:15,120
 the present moment.

23
00:01:15,120 --> 00:01:19,730
 Ordinarily we experience something momentarily and then get

24
00:01:19,730 --> 00:01:21,920
 caught up in judgment and reaction,

25
00:01:21,920 --> 00:01:25,320
 forgetting about the actual object. We see something but we

26
00:01:25,320 --> 00:01:27,440
 are not interested in the seeing.

27
00:01:27,920 --> 00:01:31,560
 Rather we are interested in what it means, wondering, "Is

28
00:01:31,560 --> 00:01:34,000
 it good? Is it bad? Is it me?

29
00:01:34,000 --> 00:01:38,050
 Is it mine?" Mindfulness does not do that. Mindfulness

30
00:01:38,050 --> 00:01:40,240
 sticks with the pure experience of the

31
00:01:40,240 --> 00:01:44,940
 object. Mindfulness manifests in two ways. Firstly as

32
00:01:44,940 --> 00:01:47,920
 guarding and secondly as confronting the

33
00:01:47,920 --> 00:01:52,510
 objective field. An ordinary mind is unguarded and defile

34
00:01:52,510 --> 00:01:55,520
ments may enter easily. Mindfulness

35
00:01:55,520 --> 00:01:58,640
 guards against these defilements. Mindfulness also

36
00:01:58,640 --> 00:02:02,080
 confronts the objective field. An ordinary

37
00:02:02,080 --> 00:02:05,950
 mind is not always able to confront objects of experience.

38
00:02:05,950 --> 00:02:08,240
 When an unpleasant experience comes,

39
00:02:08,240 --> 00:02:11,970
 the ordinary mind shies away from it and when a pleasant

40
00:02:11,970 --> 00:02:14,240
 experience arises, the mind immediately

41
00:02:14,240 --> 00:02:18,150
 chases after it. Guarding the mind and confronting the

42
00:02:18,150 --> 00:02:21,120
 object are signs of mindfulness which enables

43
00:02:21,120 --> 00:02:24,520
 one to have positive and negative experiences without

44
00:02:24,520 --> 00:02:27,200
 reacting. The proximate cause that gives

45
00:02:27,200 --> 00:02:30,810
 rise to mindfulness is strong perception. When you perceive

46
00:02:30,810 --> 00:02:33,440
 that you are seeing, that perception

47
00:02:33,440 --> 00:02:36,930
 is called sahana. When you perceive hearing that cat me

48
00:02:36,930 --> 00:02:39,680
owing, that perception is called sahana.

49
00:02:39,680 --> 00:02:44,710
 Thirasana is when you reaffirm the perception and it

50
00:02:44,710 --> 00:02:48,080
 becomes strengthened. This is accomplished by

51
00:02:48,080 --> 00:02:52,680
 reminding yourself of the experience as seeing, seeing or

52
00:02:52,680 --> 00:02:56,160
 hearing, hearing. Reminding yourself of

53
00:02:56,160 --> 00:02:59,150
 what you are experiencing strengthens the pure perception

54
00:02:59,150 --> 00:03:00,800
 of the experience giving rise to

55
00:03:00,800 --> 00:03:04,330
 mindfulness. The text saying that mindfulness is like a

56
00:03:04,330 --> 00:03:06,800
 pillar because it is firmly founded.

57
00:03:06,800 --> 00:03:10,410
 The ordinary mind is like a ball floating on water that fl

58
00:03:10,410 --> 00:03:12,240
its about here and there.

59
00:03:13,200 --> 00:03:16,400
 Mindfulness is like a pillar sunk in the bottom of a lake.

60
00:03:16,400 --> 00:03:19,280
 No matter how the wind or water buffets

61
00:03:19,280 --> 00:03:23,480
 the pillar, it does not shake. Mindfulness is also said to

62
00:03:23,480 --> 00:03:26,080
 be like a gatekeeper. It guards the eye,

63
00:03:26,080 --> 00:03:29,180
 the ear and the other sense doors where all of our

64
00:03:29,180 --> 00:03:33,200
 experience comes. Mindfulness lets experiences

65
00:03:33,200 --> 00:03:36,710
 enter without letting in the defilements. What then is

66
00:03:36,710 --> 00:03:39,920
 wrong mindfulness? There are four ways

67
00:03:39,920 --> 00:03:44,060
 we might think of wrong mindfulness. Unmindfulness, mis

68
00:03:44,060 --> 00:03:47,760
directed mindfulness, lapsed mindfulness and

69
00:03:47,760 --> 00:03:52,320
 impotent mindfulness. Unmindfulness is just the opposite of

70
00:03:52,320 --> 00:03:55,520
 mindfulness. The characteristic of

71
00:03:55,520 --> 00:04:00,050
 unmindfulness is being wavering. Its function is forget

72
00:04:00,050 --> 00:04:03,520
fulness, its manifestation is not confronting

73
00:04:04,240 --> 00:04:08,620
 and its proximate cause is weak perception. If you never go

74
00:04:08,620 --> 00:04:11,120
 to a meditation center or you never take

75
00:04:11,120 --> 00:04:14,500
 up the practice of meditation, you are generally unmindful

76
00:04:14,500 --> 00:04:16,560
 and therefore your mind wobbles.

77
00:04:16,560 --> 00:04:20,010
 Your mind is also forgetful so you cannot remember things

78
00:04:20,010 --> 00:04:21,520
 that happened yesterday

79
00:04:21,520 --> 00:04:24,480
 and you can never remember the present moment. You

80
00:04:24,480 --> 00:04:26,960
 experience something and immediately you react.

81
00:04:26,960 --> 00:04:30,180
 You do not face objects of experience because of weak

82
00:04:30,180 --> 00:04:33,920
 perception. You perceive something but your

83
00:04:33,920 --> 00:04:36,760
 mind is not trained to stick with the simple perception so

84
00:04:36,760 --> 00:04:38,480
 you get lost in your reactions.

85
00:04:38,480 --> 00:04:42,440
 Misdirected mindfulness means mindfulness that is focused

86
00:04:42,440 --> 00:04:45,360
 on the wrong objects. This type of

87
00:04:45,360 --> 00:04:48,360
 mindfulness is not intrinsically wrong, just wrong for a

88
00:04:48,360 --> 00:04:51,440
 specific practice. If you want to go

89
00:04:51,440 --> 00:04:54,050
 somewhere you need to go on the right road. There is

90
00:04:54,050 --> 00:04:55,360
 nothing wrong with the other roads.

91
00:04:55,360 --> 00:04:58,720
 They just do not take you where you are trying to go. By

92
00:04:58,720 --> 00:05:01,440
 the same token if you practice mindfulness

93
00:05:01,440 --> 00:05:04,800
 of the past, remembering past life for example, it is never

94
00:05:04,800 --> 00:05:07,040
 going to allow you to attain enlightenment

95
00:05:07,040 --> 00:05:11,240
 because it is focused on the wrong objects. There is

96
00:05:11,240 --> 00:05:13,200
 nothing technically wrong with the past,

97
00:05:13,200 --> 00:05:17,320
 it is just a mundane conceptual object. Likewise,

98
00:05:17,320 --> 00:05:20,560
 mindfulness of the future, like when you plan

99
00:05:20,560 --> 00:05:23,890
 ahead or have an experience of precognition where you see

100
00:05:23,890 --> 00:05:25,600
 something before it happens,

101
00:05:25,600 --> 00:05:28,690
 could also be considered a form of mindfulness. But it does

102
00:05:28,690 --> 00:05:30,880
 not help you either because it is

103
00:05:30,880 --> 00:05:35,320
 also not focused on actual reality as you experience it. If

104
00:05:35,320 --> 00:05:36,880
 you focus on a concept,

105
00:05:36,880 --> 00:05:40,320
 for example a candle flame, eventually you are able to see

106
00:05:40,320 --> 00:05:42,560
 this object in your mind conceptually.

107
00:05:42,560 --> 00:05:46,480
 When this happens the object is stable, satisfying and even

108
00:05:46,480 --> 00:05:49,920
 controllable. You can expand and contract

109
00:05:49,920 --> 00:05:53,620
 it in your mind. You can enter into very high states of

110
00:05:53,620 --> 00:05:56,000
 calm taking a concept as an object,

111
00:05:56,000 --> 00:05:59,890
 but it is not going to lead you to enlightenment. It is not

112
00:05:59,890 --> 00:06:04,000
 an icha, impermanent, dukkha, suffering,

113
00:06:04,000 --> 00:06:07,540
 or anatta, non-self. You will never see these three

114
00:06:07,540 --> 00:06:09,760
 characteristics through mindfulness of

115
00:06:09,760 --> 00:06:13,730
 concepts. So, for the purposes of enlightenment, it is mis

116
00:06:13,730 --> 00:06:18,000
directed. Lapsed mindfulness is like

117
00:06:18,000 --> 00:06:22,270
 unmindfulness, except that it arises in someone who is

118
00:06:22,270 --> 00:06:26,000
 practicing correctly. We, as meditators,

119
00:06:26,000 --> 00:06:29,690
 are not robots or machines. This is not an assembly line

120
00:06:29,690 --> 00:06:31,600
 where we pass people through

121
00:06:31,600 --> 00:06:34,720
 and they all come out enlightened. Everyone has conditions

122
00:06:34,720 --> 00:06:36,400
 in which they come to meditation,

123
00:06:36,400 --> 00:06:40,770
 and every meditator has a different experience. Often this

124
00:06:40,770 --> 00:06:43,600
 shows itself in positive or negative

125
00:06:43,600 --> 00:06:48,000
 states, or in deep states that may have been hidden. The

126
00:06:48,000 --> 00:06:50,400
 ten imperfections of insight describe

127
00:06:50,400 --> 00:06:53,090
 some of the positive states that result from proper

128
00:06:53,090 --> 00:06:55,440
 practice. These positive states will not

129
00:06:55,440 --> 00:06:58,560
 lead to enlightenment, although sometimes meditators start

130
00:06:58,560 --> 00:06:59,760
 to think that they will,

131
00:06:59,760 --> 00:07:04,600
 and are led astray. Impotent mindfulness relates to an

132
00:07:04,600 --> 00:07:06,800
 absence of the other factors of the noble

133
00:07:06,800 --> 00:07:10,400
 eightfold path. If you have wrong view, you will not see

134
00:07:10,400 --> 00:07:12,560
 clearly no matter how mindful you are.

135
00:07:13,760 --> 00:07:16,970
 If you believe any of the five candas are the self, or if

136
00:07:16,970 --> 00:07:18,960
 you do not believe in karma,

137
00:07:18,960 --> 00:07:22,190
 or if you think that God created us, all of these views

138
00:07:22,190 --> 00:07:24,400
 will prevent you from seeing ordinary

139
00:07:24,400 --> 00:07:28,450
 experiences clearly. The same goes for wrong thought, bad

140
00:07:28,450 --> 00:07:30,400
 intentions or ambitions, and

141
00:07:30,400 --> 00:07:34,030
 cruelty in the mind. These will all get in the way of

142
00:07:34,030 --> 00:07:37,440
 mindfulness practice. If you lie, gossip,

143
00:07:37,440 --> 00:07:39,850
 or just talk a lot, it is going to get in the way of

144
00:07:39,850 --> 00:07:43,520
 mindfulness. The same goes if you are a murderer,

145
00:07:43,520 --> 00:07:47,150
 or a thief, or if you take drugs or alcohol. You can try

146
00:07:47,150 --> 00:07:49,520
 your best at being mindful, but if you are

147
00:07:49,520 --> 00:07:52,920
 not keeping the five precepts, you will not see clearly. I

148
00:07:52,920 --> 00:07:54,800
 would never guide someone through a

149
00:07:54,800 --> 00:07:57,570
 meditation course if they were not able to keep at least

150
00:07:57,570 --> 00:08:00,400
 the five precepts. It is just futile.

151
00:08:00,400 --> 00:08:04,470
 If you practice wrong livelihood, making a living from bad

152
00:08:04,470 --> 00:08:06,880
 things, or if you practice wrong effort,

153
00:08:06,880 --> 00:08:09,770
 being lazy, or directing effort towards the cultivation of

154
00:08:09,770 --> 00:08:10,800
 unwholesomeness,

155
00:08:10,800 --> 00:08:14,400
 all of these will prevent mindfulness from being effective.

156
00:08:14,400 --> 00:08:17,360
 If you are unfocused through wrong

157
00:08:17,360 --> 00:08:20,480
 concentration, or if you are focused on the wrong things,

158
00:08:20,480 --> 00:08:22,800
 it will make whatever mindfulness you have

159
00:08:22,800 --> 00:08:26,820
 impotent. The eightfold path must work together with

160
00:08:26,820 --> 00:08:29,360
 mindfulness, as mindfulness on its own is

161
00:08:29,360 --> 00:08:32,630
 not enough. Mindfulness is like the key that starts the

162
00:08:32,630 --> 00:08:34,880
 engine. It gets everything going,

163
00:08:34,880 --> 00:08:38,450
 but if your engine is broken, the key does not do much. So

164
00:08:38,450 --> 00:08:40,640
 these are what would be considered

165
00:08:40,640 --> 00:08:44,120
 wrong mindfulness. The consequences of wrong mindfulness

166
00:08:44,120 --> 00:08:46,000
 are regression, stagnation, and

167
00:08:46,000 --> 00:08:50,120
 complication. Regression happens when you get discouraged,

168
00:08:50,120 --> 00:08:52,480
 and then, instead of gaining wholesome

169
00:08:52,480 --> 00:08:56,290
 states, you regress and become unmindful, afraid of, or

170
00:08:56,290 --> 00:08:59,440
 upset with your practice. You may think

171
00:08:59,440 --> 00:09:02,260
 the practice is useless because you are not practicing

172
00:09:02,260 --> 00:09:04,240
 properly. This may cause one to leave

173
00:09:04,240 --> 00:09:07,010
 the practice and Buddhism, and it may even lead one to

174
00:09:07,010 --> 00:09:10,960
 disparage Buddhism as useless. Stagnation is

175
00:09:10,960 --> 00:09:13,840
 another danger, and meditators may practice for years

176
00:09:13,840 --> 00:09:16,160
 without progressing if their mindfulness

177
00:09:16,160 --> 00:09:19,900
 is not well-directed. Some meditators may experience great

178
00:09:19,900 --> 00:09:22,240
 calm and peace in their practice,

179
00:09:22,240 --> 00:09:25,160
 but never gain insight because they fail to cultivate the

180
00:09:25,160 --> 00:09:26,400
 four satipatthana.

181
00:09:26,400 --> 00:09:31,250
 Complication is the third consequence of wrong mindfulness,

182
00:09:31,250 --> 00:09:32,480
 and this is the most dangerous.

183
00:09:33,600 --> 00:09:37,020
 "Mindfulness is designed to simplify things," the Buddha

184
00:09:37,020 --> 00:09:40,080
 said. "When you see, let it just be seen.

185
00:09:40,080 --> 00:09:44,400
 This is how you should train yourself. When you are hearing

186
00:09:44,400 --> 00:09:46,720
, let it just be hearing. When you

187
00:09:46,720 --> 00:09:50,800
 experience something, let it just be the experience. When

188
00:09:50,800 --> 00:09:53,360
 you are unmindful, or if you have a distorted

189
00:09:53,360 --> 00:09:56,820
 state of mindfulness in which one of the path factors is

190
00:09:56,820 --> 00:09:59,600
 missing, your perception is complicated

191
00:09:59,600 --> 00:10:03,110
 rather than simplified. This can actually lead to mind

192
00:10:03,110 --> 00:10:05,920
 states that are more tense, stressed,

193
00:10:05,920 --> 00:10:09,590
 or poisonous, and through repeated and intensive practice

194
00:10:09,590 --> 00:10:12,000
 you may lose your mindfulness entirely

195
00:10:12,000 --> 00:10:15,520
 and become temporarily insane, unable to control yourself.

196
00:10:15,520 --> 00:10:18,560
 I've seen this happen to meditators,

197
00:10:18,560 --> 00:10:21,060
 though I've never had it happen to someone practicing under

198
00:10:21,060 --> 00:10:21,600
 my guidance.

199
00:10:21,600 --> 00:10:25,760
 Without close and proper guidance, some meditators may lose

200
00:10:25,760 --> 00:10:27,120
 their mindfulness,

201
00:10:27,120 --> 00:10:30,460
 and that can be dangerous. Wrong mindfulness is something

202
00:10:30,460 --> 00:10:32,400
 we all have to be concerned with.

