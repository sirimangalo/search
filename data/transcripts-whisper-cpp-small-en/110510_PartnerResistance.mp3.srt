1
00:00:00,000 --> 00:00:04,200
 Oh, welcome back to Ask a Monk.

2
00:00:04,200 --> 00:00:07,040
 Next question.

3
00:00:07,040 --> 00:00:10,330
 I've been studying the teachings in meditation for a few

4
00:00:10,330 --> 00:00:12,040
 months now, but my partner doesn't

5
00:00:12,040 --> 00:00:15,810
 like it and makes me feel bad, like I'm doing something

6
00:00:15,810 --> 00:00:18,240
 wrong as a person in general, even

7
00:00:18,240 --> 00:00:21,000
 if I'm more compassionate towards others.

8
00:00:21,000 --> 00:00:26,000
 Do you have any advice?

9
00:00:26,000 --> 00:00:34,080
 Well, it's actually not that difficult of a problem.

10
00:00:34,080 --> 00:00:38,160
 A worse problem would be if you actually believed your

11
00:00:38,160 --> 00:00:40,880
 partner, which it doesn't sound like

12
00:00:40,880 --> 00:00:41,960
 you do.

13
00:00:41,960 --> 00:00:46,060
 And that, you know, if so, if you're in doubt, then I would

14
00:00:46,060 --> 00:00:48,160
 say that's much more important

15
00:00:48,160 --> 00:00:54,470
 to address, whether you really think that the meditation is

16
00:00:54,470 --> 00:00:56,160
 beneficial.

17
00:00:56,160 --> 00:01:00,960
 It sounds like you do, though I wouldn't say that the most

18
00:01:00,960 --> 00:01:03,960
 important benefit is being compassionate

19
00:01:03,960 --> 00:01:07,360
 towards others, because you have to be very compassionate

20
00:01:07,360 --> 00:01:09,120
 towards yourself as well.

21
00:01:09,120 --> 00:01:13,490
 The most important is to gain understanding about reality,

22
00:01:13,490 --> 00:01:16,400
 and that really leads to compassion.

23
00:01:16,400 --> 00:01:20,030
 And I suppose that is probably implicit in what you're

24
00:01:20,030 --> 00:01:22,420
 saying, that you've learned more

25
00:01:22,420 --> 00:01:26,370
 and come to understand yourself better and understand, you

26
00:01:26,370 --> 00:01:28,240
 know, what is of benefit,

27
00:01:28,240 --> 00:01:30,860
 what is right and what is proper and what is wrong and what

28
00:01:30,860 --> 00:01:31,680
 is improper.

29
00:01:31,680 --> 00:01:35,220
 And therefore you're acting in a much more proper way or

30
00:01:35,220 --> 00:01:36,720
 somewhat more proper.

31
00:01:36,720 --> 00:01:39,920
 And therefore you're more interested in helping yourself

32
00:01:39,920 --> 00:01:42,160
 and helping other people, which makes

33
00:01:42,160 --> 00:01:44,840
 you more compassionate.

34
00:01:44,840 --> 00:01:48,750
 So being able to see that benefit is really an important

35
00:01:48,750 --> 00:01:49,480
 thing.

36
00:01:49,480 --> 00:01:54,000
 So the question of whether to listen to your partner

37
00:01:54,000 --> 00:01:56,600
 therefore doesn't arise.

38
00:01:56,600 --> 00:02:02,470
 The question you're asking is what to do about their

39
00:02:02,470 --> 00:02:03,680
 feelings.

40
00:02:03,680 --> 00:02:06,120
 And you should get that straight because it will make you

41
00:02:06,120 --> 00:02:06,960
 feel a lot better.

42
00:02:06,960 --> 00:02:10,660
 You won't have to be concerned about what they say.

43
00:02:10,660 --> 00:02:14,120
 If they're making you feel bad, then it means they're

44
00:02:14,120 --> 00:02:15,440
 getting to you.

45
00:02:15,440 --> 00:02:18,690
 It means you're still clinging somehow to their words and

46
00:02:18,690 --> 00:02:22,000
 to their views and their ideas.

47
00:02:22,000 --> 00:02:25,040
 If it's clear in your mind what is right and what is wrong,

48
00:02:25,040 --> 00:02:26,520
 no one's able to make you feel

49
00:02:26,520 --> 00:02:28,720
 good or bad.

50
00:02:28,720 --> 00:02:34,060
 You are at peace with yourself and your happiness doesn't

51
00:02:34,060 --> 00:02:38,240
 depend on other people or externalities.

52
00:02:38,240 --> 00:02:45,150
 So what to do when someone else doesn't like what you know

53
00:02:45,150 --> 00:02:49,220
 to be right and proper and useful

54
00:02:49,220 --> 00:02:53,120
 and beneficial.

55
00:02:53,120 --> 00:02:55,450
 The easiest question, the easiest answer, and it's probably

56
00:02:55,450 --> 00:02:56,480
 not the one you prefer,

57
00:02:56,480 --> 00:03:01,080
 is to leave your partner.

58
00:03:01,080 --> 00:03:04,150
 And I think that's just so obvious and it's probably not

59
00:03:04,150 --> 00:03:07,520
 the answer you're looking for.

60
00:03:07,520 --> 00:03:11,660
 That would imply that somehow you have some other

61
00:03:11,660 --> 00:03:14,200
 attachment to this person.

62
00:03:14,200 --> 00:03:16,960
 Because the meditation has great benefit to you in your

63
00:03:16,960 --> 00:03:19,880
 life and it's bringing you some,

64
00:03:19,880 --> 00:03:23,640
 you know, it will bring you peace and happiness.

65
00:03:23,640 --> 00:03:26,320
 Whether you can see this or not, I'm not sure.

66
00:03:26,320 --> 00:03:30,050
 But once you practice more you can see how much benefit the

67
00:03:30,050 --> 00:03:32,040
 meditation brings to you.

68
00:03:32,040 --> 00:03:38,860
 So to stay with someone who doesn't agree with that or who

69
00:03:38,860 --> 00:03:42,080
 believes the opposite, that

70
00:03:42,080 --> 00:03:46,890
 it is actually hurting you or that it is making you a bad

71
00:03:46,890 --> 00:03:50,640
 person or so on, doesn't make sense.

72
00:03:50,640 --> 00:03:56,340
 Because if you stay with them it's going to conflict with

73
00:03:56,340 --> 00:03:58,360
 your own benefit.

74
00:03:58,360 --> 00:04:00,370
 So obviously there must be something else, that there must

75
00:04:00,370 --> 00:04:01,360
 be a clinging to this person

76
00:04:01,360 --> 00:04:05,590
 or could be perhaps that there is some structural reason

77
00:04:05,590 --> 00:04:08,160
 for being in a relationship in terms

78
00:04:08,160 --> 00:04:10,320
 of stability and so on.

79
00:04:10,320 --> 00:04:15,200
 Or it could be to help that person because that person

80
00:04:15,200 --> 00:04:17,280
 needs you and so on.

81
00:04:17,280 --> 00:04:21,890
 So there could be many reasons, none of which I am aware of

82
00:04:21,890 --> 00:04:21,940
.

83
00:04:21,940 --> 00:04:28,610
 So the more important question then is how to live with

84
00:04:28,610 --> 00:04:32,360
 this person, how to live with

85
00:04:32,360 --> 00:04:38,000
 a person who doesn't agree with what you know to be proper

86
00:04:38,000 --> 00:04:39,960
 and beneficial.

87
00:04:39,960 --> 00:04:41,600
 And there are many ways.

88
00:04:41,600 --> 00:04:45,560
 I think the first one is sort of a compromise.

89
00:04:45,560 --> 00:04:47,560
 I say leave this person.

90
00:04:47,560 --> 00:04:50,880
 Well, it doesn't have to be complete.

91
00:04:50,880 --> 00:04:55,220
 Then you can take time apart and you can distance yourself

92
00:04:55,220 --> 00:04:57,760
 to some extent from the person, not

93
00:04:57,760 --> 00:05:01,330
 dropping them, but you can take time out for yourself and

94
00:05:01,330 --> 00:05:03,080
 say you want to be alone for

95
00:05:03,080 --> 00:05:04,440
 some time.

96
00:05:04,440 --> 00:05:06,570
 When you have time alone then they don't see you, they are

97
00:05:06,570 --> 00:05:07,720
 not aware of what you are doing

98
00:05:07,720 --> 00:05:09,920
 and it doesn't upset them, what you do.

99
00:05:09,920 --> 00:05:12,620
 If you have the ability to take time alone where you are

100
00:05:12,620 --> 00:05:14,440
 not with this person, even though

101
00:05:14,440 --> 00:05:18,480
 you are still in a relationship, then it can be a real half

102
00:05:18,480 --> 00:05:19,040
 way.

103
00:05:19,040 --> 00:05:22,680
 It might lead to you breaking up, but it might also lead to

104
00:05:22,680 --> 00:05:25,320
 you coming to some better understanding

105
00:05:25,320 --> 00:05:28,610
 of each other's position and could even make the

106
00:05:28,610 --> 00:05:31,420
 relationship stronger and more in line

107
00:05:31,420 --> 00:05:36,120
 with what is truly right and beneficial.

108
00:05:36,120 --> 00:05:39,690
 Now I am guessing that it has something to do with religion

109
00:05:39,690 --> 00:05:41,480
 because it usually does.

110
00:05:41,480 --> 00:05:44,780
 If people believe that meditation makes you a bad person

111
00:05:44,780 --> 00:05:46,680
 then it usually, well there are

112
00:05:46,680 --> 00:05:47,680
 two reasons.

113
00:05:47,680 --> 00:05:51,600
 One is it conflicts with their beliefs, their religion.

114
00:05:51,600 --> 00:05:56,630
 Or two, and this is probably not your case given that you

115
00:05:56,630 --> 00:05:59,360
 are benefiting from it, the

116
00:05:59,360 --> 00:06:03,710
 person who is practicing meditation is practicing it

117
00:06:03,710 --> 00:06:07,080
 incorrectly and is giving rise to states

118
00:06:07,080 --> 00:06:09,240
 that are disturbing.

119
00:06:09,240 --> 00:06:12,450
 So some people when they begin to practice meditation they

120
00:06:12,450 --> 00:06:14,020
 have the best of intentions

121
00:06:14,020 --> 00:06:16,270
 and eventually they will get good at it, but when they are

122
00:06:16,270 --> 00:06:17,440
 not good at it it can lead to

123
00:06:17,440 --> 00:06:21,140
 great stress and conflict inside as you start to learn how

124
00:06:21,140 --> 00:06:23,080
 to deal with the brain, with

125
00:06:23,080 --> 00:06:24,080
 the mind.

126
00:06:24,080 --> 00:06:25,400
 It is like learning to drive a car.

127
00:06:25,400 --> 00:06:29,210
 When you are learning to drive a manual transmission car

128
00:06:29,210 --> 00:06:31,720
 the people in the car are going to have

129
00:06:31,720 --> 00:06:35,520
 to put up with a lot of jerking in the beginning.

130
00:06:35,520 --> 00:06:36,880
 So that can cause conflict.

131
00:06:36,880 --> 00:06:39,510
 But I would imagine that in your case it has something to

132
00:06:39,510 --> 00:06:41,000
 do with religion or belief or

133
00:06:41,000 --> 00:06:42,000
 I don't know.

134
00:06:42,000 --> 00:06:49,500
 I know that the person is a scientist, an atheist, is a

135
00:06:49,500 --> 00:06:52,760
 person who is a secularist I

136
00:06:52,760 --> 00:06:55,920
 suppose, someone who doesn't believe in the existence of

137
00:06:55,920 --> 00:06:58,080
 the mind or the benefits of meditation

138
00:06:58,080 --> 00:07:09,080
 and thinks you are being brainwashed and so on and so on.

139
00:07:09,080 --> 00:07:12,070
 In this case you really have to, it is something that is

140
00:07:12,070 --> 00:07:14,040
 really going to take time and it may

141
00:07:14,040 --> 00:07:15,040
 never be sorted out.

142
00:07:15,040 --> 00:07:18,760
 It may eventually mean that you have to part ways.

143
00:07:18,760 --> 00:07:24,380
 But the best way to deal with this, to approach this is to

144
00:07:24,380 --> 00:07:28,120
 walk around the person, to practice

145
00:07:28,120 --> 00:07:29,880
 around the person.

146
00:07:29,880 --> 00:07:35,190
 You don't have to be sitting on a cushion in a silent room

147
00:07:35,190 --> 00:07:37,240
 to be meditating.

148
00:07:37,240 --> 00:07:39,740
 You can meditate in a chair, you can meditate whether there

149
00:07:39,740 --> 00:07:41,080
 are other people in the room,

150
00:07:41,080 --> 00:07:44,900
 you can meditate while there is noise, you can meditate

151
00:07:44,900 --> 00:07:46,600
 anywhere at any time.

152
00:07:46,600 --> 00:07:50,060
 It's ideal to have solitude, it's ideal to have quiet, it's

153
00:07:50,060 --> 00:07:51,840
 ideal to be sitting cross-legged

154
00:07:51,840 --> 00:07:53,260
 on a cushion.

155
00:07:53,260 --> 00:07:58,240
 But none of these are absolutely necessary.

156
00:07:58,240 --> 00:08:01,430
 Rather than bemoaning the fact that you are unable to

157
00:08:01,430 --> 00:08:03,720
 pursue the ideal or trying to pursue

158
00:08:03,720 --> 00:08:07,860
 the ideal and as a result bring in conflict with the people

159
00:08:07,860 --> 00:08:10,280
 around you, you can incorporate

160
00:08:10,280 --> 00:08:12,680
 it into your relationship with them.

161
00:08:12,680 --> 00:08:16,900
 While they are watching television, you can be sitting

162
00:08:16,900 --> 00:08:19,200
 quietly meditating with your eyes

163
00:08:19,200 --> 00:08:20,960
 open, with your eyes closed.

164
00:08:20,960 --> 00:08:25,620
 You can be in another room, you can be doing anything or be

165
00:08:25,620 --> 00:08:27,940
 in any sort of position.

166
00:08:27,940 --> 00:08:30,160
 You don't have to make it obvious that you are meditating

167
00:08:30,160 --> 00:08:31,360
 or you don't have to say to

168
00:08:31,360 --> 00:08:34,070
 them "Look, I need my half an hour now, could you please

169
00:08:34,070 --> 00:08:35,600
 leave the room, could you please

170
00:08:35,600 --> 00:08:40,200
 turn off the television, etc. etc."

171
00:08:40,200 --> 00:08:42,530
 Because eventually you are going to realize that all of

172
00:08:42,530 --> 00:08:44,080
 that is a part of your meditation,

173
00:08:44,080 --> 00:08:47,840
 it's a part of the practice that we are following.

174
00:08:47,840 --> 00:08:51,060
 And eventually you will be able to, if you are successful

175
00:08:51,060 --> 00:08:52,800
 in the practice, you will be

176
00:08:52,800 --> 00:08:56,900
 able to deal with it all, you will be able to overcome your

177
00:08:56,900 --> 00:08:59,000
 aversions and attachments

178
00:08:59,000 --> 00:09:01,320
 to these things.

179
00:09:01,320 --> 00:09:04,260
 So you can live your life with this person, you can go,

180
00:09:04,260 --> 00:09:06,000
 suppose it's a religious thing

181
00:09:06,000 --> 00:09:07,930
 and they want you to go to church and you don't go to

182
00:09:07,930 --> 00:09:09,340
 church, you can go to church and

183
00:09:09,340 --> 00:09:12,680
 while they are doing their thing you can sit and meditate

184
00:09:12,680 --> 00:09:13,520
 and whatever.

185
00:09:13,520 --> 00:09:17,160
 If they are singing their praises to God, you can sing your

186
00:09:17,160 --> 00:09:18,720
 praises to God and watch

187
00:09:18,720 --> 00:09:23,310
 your lips moving as you sing "Watch the lips moving, feel

188
00:09:23,310 --> 00:09:25,320
 the lips moving" and just be

189
00:09:25,320 --> 00:09:28,480
 aware of what's happening and if standing and when you hear

190
00:09:28,480 --> 00:09:30,080
 the sound hearing, hearing

191
00:09:30,080 --> 00:09:33,350
 and you can even just mouth something and take it as a

192
00:09:33,350 --> 00:09:36,080
 mouth meditation or whatever.

193
00:09:36,080 --> 00:09:40,680
 Examples, we do walking and sitting meditation and this

194
00:09:40,680 --> 00:09:43,360
 prostration but all of these are

195
00:09:43,360 --> 00:09:47,440
 just examples, you can do meditation in any way, in any

196
00:09:47,440 --> 00:09:50,420
 form, you can do driving meditation,

197
00:09:50,420 --> 00:09:53,500
 whatever you do in life, if you are the cook in the family

198
00:09:53,500 --> 00:09:56,000
 you can do cooking meditation,

199
00:09:56,000 --> 00:10:00,490
 if you are working in an office job you can do office

200
00:10:00,490 --> 00:10:03,240
 meditation or you can take time

201
00:10:03,240 --> 00:10:08,370
 out of your work to do 5 minutes, 10 minutes, try to work

202
00:10:08,370 --> 00:10:10,920
 around the person so that they

203
00:10:10,920 --> 00:10:13,360
 are not even aware that you are meditating.

204
00:10:13,360 --> 00:10:17,980
 That's much better because what's really going to change

205
00:10:17,980 --> 00:10:20,600
 them is the strength in your mind.

206
00:10:20,600 --> 00:10:24,180
 Once your mind becomes strong, once you become sure, there

207
00:10:24,180 --> 00:10:26,120
 is no way, especially if there

208
00:10:26,120 --> 00:10:29,430
 are a person who can't see the benefit of meditation, who

209
00:10:29,430 --> 00:10:30,880
 is so blind that they are

210
00:10:30,880 --> 00:10:34,730
 unable to see the benefits of it, there is no way that they

211
00:10:34,730 --> 00:10:36,680
 can fight, again, there is

212
00:10:36,680 --> 00:10:39,320
 no way that they can compete against your strength.

213
00:10:39,320 --> 00:10:43,450
 You have the strength of mind because you are practicing

214
00:10:43,450 --> 00:10:45,680
 every day to strengthen your

215
00:10:45,680 --> 00:10:52,280
 mind and to clarify your mind and then what are they doing?

216
00:10:52,280 --> 00:10:56,390
 Their mind will constantly be wavering and it may not be

217
00:10:56,390 --> 00:10:58,360
 that case right now, it may

218
00:10:58,360 --> 00:11:00,900
 be that they have the strength and you don't and therefore

219
00:11:00,900 --> 00:11:02,200
 you are wavering and you are

220
00:11:02,200 --> 00:11:07,280
 not sure what to do but that's the goal.

221
00:11:07,280 --> 00:11:09,950
 If you can get to the point where your mind doesn't waver

222
00:11:09,950 --> 00:11:11,760
 then they will have to capitulate,

223
00:11:11,760 --> 00:11:14,390
 eventually they will realize, they will come up in their

224
00:11:14,390 --> 00:11:15,800
 mind and they will realize the

225
00:11:15,800 --> 00:11:19,130
 wrongness of their beliefs and their ideas and eventually

226
00:11:19,130 --> 00:11:21,160
 they will even become interested

227
00:11:21,160 --> 00:11:23,940
 in the meditation because they will see how much strength,

228
00:11:23,940 --> 00:11:25,440
 confidence and peace it brings

229
00:11:25,440 --> 00:11:29,720
 to you, no matter what their religious views are.

230
00:11:29,720 --> 00:11:34,560
 That's the deal, views and opinions and beliefs are a

231
00:11:34,560 --> 00:11:37,840
 source of strength in a sense and so

232
00:11:37,840 --> 00:11:41,700
 you have to get quite powerful to be able to overcome those

233
00:11:41,700 --> 00:11:44,600
 views which will eventually,

234
00:11:44,600 --> 00:11:47,530
 they don't jive, when they don't jive with reality that

235
00:11:47,530 --> 00:11:49,160
 person will have to let go but

236
00:11:49,160 --> 00:11:52,150
 it can take time, people can hold on to, it's amazing the

237
00:11:52,150 --> 00:11:53,800
 beliefs people can hold on to

238
00:11:53,800 --> 00:11:59,310
 even in the face, even when those beliefs fly directly in

239
00:11:59,310 --> 00:12:01,560
 the face of reality.

240
00:12:01,560 --> 00:12:07,570
 So good luck and the most obvious answer is to always try

241
00:12:07,570 --> 00:12:11,360
 to surround yourself with people

242
00:12:11,360 --> 00:12:14,490
 who are meditating, this is a very important part of the

243
00:12:14,490 --> 00:12:16,440
 Buddhist teaching, to surround

244
00:12:16,440 --> 00:12:19,130
 yourself with people who are interested in meditation, who

245
00:12:19,130 --> 00:12:20,520
 are meditating, who believe

246
00:12:20,520 --> 00:12:26,180
 in the benefits and see the benefits of meditation practice

247
00:12:26,180 --> 00:12:29,400
 and always strive to avoid people

248
00:12:29,400 --> 00:12:32,360
 who don't see the benefits.

249
00:12:32,360 --> 00:12:36,760
 So it's basically choosing people whose beliefs and

250
00:12:36,760 --> 00:12:41,160
 opinions don't fly in the face of reality,

251
00:12:41,160 --> 00:12:47,180
 don't conflict with the truth, try to stick only to people

252
00:12:47,180 --> 00:12:50,720
 whose understanding and beliefs

253
00:12:50,720 --> 00:12:54,200
 and views are in line with reality because meditation is a

254
00:12:54,200 --> 00:12:55,880
 great thing and anyone who

255
00:12:55,880 --> 00:13:00,360
 believes otherwise is missing something.

256
00:13:00,360 --> 00:13:04,230
 So this is an answer to your question, this has been

257
00:13:04,230 --> 00:13:06,720
 another episode of Ask a Monk, wishing

258
00:13:06,720 --> 00:13:09,080
 you all peace, happiness and freedom from suffering.

