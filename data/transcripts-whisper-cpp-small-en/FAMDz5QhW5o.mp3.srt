1
00:00:00,000 --> 00:00:05,310
 Would anything cause you to disrobe? Do you see any benefit

2
00:00:05,310 --> 00:00:07,000
 to the lay life?

3
00:00:07,000 --> 00:00:11,000
 They're kind of two different questions.

4
00:00:11,000 --> 00:00:14,000
 Would anything cause me to disrobe?

5
00:00:14,000 --> 00:00:18,520
 I have seen circumstances where people were forced to dis

6
00:00:18,520 --> 00:00:19,000
robe.

7
00:00:19,000 --> 00:00:23,620
 People who had no interest in the lay life were forced to

8
00:00:23,620 --> 00:00:24,000
 disrobe.

9
00:00:24,000 --> 00:00:28,000
 Generally dealing with their parents.

10
00:00:28,000 --> 00:00:33,640
 There's one really impressive man in Thailand who was a

11
00:00:33,640 --> 00:00:38,000
 monk for 17 years and a very strong meditator.

12
00:00:38,000 --> 00:00:44,300
 And by all appearances seems to be perhaps one of the four

13
00:00:44,300 --> 00:00:46,000
 enlightened types of beings.

14
00:00:46,000 --> 00:00:50,300
 Probably not an arahant because they say that requires ord

15
00:00:50,300 --> 00:00:51,000
ination.

16
00:00:51,000 --> 00:00:55,020
 But certainly a very strong meditator and a very wise

17
00:00:55,020 --> 00:00:57,000
 individual.

18
00:00:57,000 --> 00:01:02,280
 And he collects garbage and sells garbage in Thailand,

19
00:01:02,280 --> 00:01:04,000
 sells recyclables.

20
00:01:04,000 --> 00:01:07,000
 Which is actually a fairly lucrative trade.

21
00:01:07,000 --> 00:01:12,480
 But he just gets on his motorcycle with a sidecar, or a

22
00:01:12,480 --> 00:01:15,000
 trailer or something.

23
00:01:15,000 --> 00:01:22,490
 And he rides around looking for garbage to pick up and sell

24
00:01:22,490 --> 00:01:26,000
 or to trade or whatever.

25
00:01:26,000 --> 00:01:30,000
 But he had to disrobe because of his mother.

26
00:01:30,000 --> 00:01:32,450
 I can't remember the exact story but his mother was sick

27
00:01:32,450 --> 00:01:34,000
 and couldn't take care of herself.

28
00:01:34,000 --> 00:01:39,000
 And he was her only son and so he felt he had to disrobe.

29
00:01:39,000 --> 00:01:43,840
 And he lives his life, sell a bit, and alone in a very

30
00:01:43,840 --> 00:01:45,000
 simple life.

31
00:01:45,000 --> 00:01:49,120
 He cooks food for his mother and then goes off to work and

32
00:01:49,120 --> 00:01:55,000
 comes back in the evening and they say it's called rice.

33
00:01:55,000 --> 00:01:58,000
 And so he lives quite a noble life.

34
00:01:58,000 --> 00:02:03,660
 So what I mean to say is that even in his case where he was

35
00:02:03,660 --> 00:02:06,000
 obviously someone who was quite advanced,

36
00:02:06,000 --> 00:02:08,000
 he still felt the need to disrobe.

37
00:02:08,000 --> 00:02:13,000
 So I can't say that nothing would cause me to disrobe.

38
00:02:13,000 --> 00:02:15,370
 But I think they're two pertinent questions because it

39
00:02:15,370 --> 00:02:20,000
 points to the fact that you would have to see something,

40
00:02:20,000 --> 00:02:23,000
 some benefit in the lay life to disrobe.

41
00:02:23,000 --> 00:02:26,490
 And I don't tend to make a mistake about disrobing that it

42
00:02:26,490 --> 00:02:31,000
's giving up something, that you're giving up the path.

43
00:02:31,000 --> 00:02:36,580
 I suppose it's not a mistake but it's really only one way

44
00:02:36,580 --> 00:02:39,000
 of looking at the issue.

45
00:02:39,000 --> 00:02:43,000
 Because disrobing doesn't just mean taking off the robes.

46
00:02:43,000 --> 00:02:45,570
 I mean if it just means taking off the robes you become a

47
00:02:45,570 --> 00:02:49,000
 naked ascetic or something or become an animal again.

48
00:02:49,000 --> 00:02:54,950
 It means taking off the robes and generally means putting

49
00:02:54,950 --> 00:03:01,000
 on another set of culturally and role-specific clothing.

50
00:03:01,000 --> 00:03:05,860
 Because when you put on pants it really does have quite a

51
00:03:05,860 --> 00:03:07,000
 meaning.

52
00:03:07,000 --> 00:03:10,850
 Pants are not the default and the t-shirt or whatever it is

53
00:03:10,850 --> 00:03:14,000
 that you end up wearing are not the default.

54
00:03:14,000 --> 00:03:18,230
 In fact you might think that robes are much more a default

55
00:03:18,230 --> 00:03:21,000
 clothing and that's why the Buddha chose them.

56
00:03:21,000 --> 00:03:29,210
 So to become a lay person again you would have to have, I

57
00:03:29,210 --> 00:03:33,310
 would think you would have to see some benefit in the lay

58
00:03:33,310 --> 00:03:34,000
 life.

59
00:03:34,000 --> 00:03:37,240
 You would have to think that some other was a benefit to

60
00:03:37,240 --> 00:03:38,000
 lay life.

61
00:03:38,000 --> 00:03:40,000
 Do I see any benefit in the lay life?

62
00:03:40,000 --> 00:03:44,030
 Besides the benefit that I just mentioned, taking care of

63
00:03:44,030 --> 00:03:49,540
 your parents, which I'm still skeptical of because in many

64
00:03:49,540 --> 00:03:53,000
 cases you can as a monk take care of your parents.

65
00:03:53,000 --> 00:03:57,420
 You can't do everything for them but for myself I wouldn't

66
00:03:57,420 --> 00:04:00,000
 probably have such a concern.

67
00:04:00,000 --> 00:04:03,600
 I can't say that. You never know what the future is going

68
00:04:03,600 --> 00:04:04,000
 to bring.

69
00:04:04,000 --> 00:04:09,410
 But there are always ways to, even as a monk, to support

70
00:04:09,410 --> 00:04:11,000
 your parents.

71
00:04:11,000 --> 00:04:16,060
 For example, given that I have a number of students who

72
00:04:16,060 --> 00:04:21,610
 appreciate what I do and wish for me to continue doing what

73
00:04:21,610 --> 00:04:22,000
 I do,

74
00:04:22,000 --> 00:04:28,240
 I can explain to them if there is a desire for me to remain

75
00:04:28,240 --> 00:04:32,300
 a monk then maybe I can have some people help me to help my

76
00:04:32,300 --> 00:04:33,000
 parents.

77
00:04:33,000 --> 00:04:37,450
 This is an example. So in my case it's not that pressing of

78
00:04:37,450 --> 00:04:41,000
 a concern or not that big of a worry.

79
00:04:41,000 --> 00:04:45,300
 I might someday want to go back to Canada to be closer to

80
00:04:45,300 --> 00:04:48,000
 them. I'm always thinking of that.

81
00:04:48,000 --> 00:04:52,420
 But that's I think another story. Obviously it wouldn't be

82
00:04:52,420 --> 00:04:55,650
 to this robe and I don't have any thought in that direction

83
00:04:55,650 --> 00:04:56,000
.

84
00:04:56,000 --> 00:05:02,000
 But seeing a benefit, besides taking care of your parents,

85
00:05:02,000 --> 00:05:06,000
 seeing a benefit in the lay life, no.

86
00:05:06,000 --> 00:05:10,000
 No, I don't see any benefit in the lay life.

87
00:05:10,000 --> 00:05:16,000
 It really speaks of where your mind is.

88
00:05:16,000 --> 00:05:21,490
 The point is that personally I don't see a great benefit in

89
00:05:21,490 --> 00:05:27,000
, for example, supporting or contributing to society.

90
00:05:27,000 --> 00:05:29,410
 Because I think maybe this is something that keeps people

91
00:05:29,410 --> 00:05:32,780
 as lay people or makes them just robe because they want to

92
00:05:32,780 --> 00:05:34,000
 go and do good things in the world.

93
00:05:34,000 --> 00:05:37,210
 And they feel like as a monk or a nun that they're not able

94
00:05:37,210 --> 00:05:38,000
 to do that.

95
00:05:38,000 --> 00:05:44,000
 Which I think is really, or I don't see things in that way.

96
00:05:44,000 --> 00:05:49,000
 So as a result I probably wouldn't follow such a thought.

97
00:05:49,000 --> 00:05:52,560
 Because from my point of view the best, the thing that

98
00:05:52,560 --> 00:05:55,970
 helps people the most is meditation and the practice of the

99
00:05:55,970 --> 00:05:57,000
 Buddhist teaching.

100
00:05:57,000 --> 00:06:00,840
 I mean even today just the wonderful things that went on in

101
00:06:00,840 --> 00:06:06,000
 this ceremony, again and again, it's such a wonderful thing

102
00:06:06,000 --> 00:06:07,000
 to give.

103
00:06:07,000 --> 00:06:11,370
 And to be a part of giving. And even not to give, but to

104
00:06:11,370 --> 00:06:13,000
 rejoice in the giving of others.

105
00:06:13,000 --> 00:06:17,710
 To watch other people give and be generous and be kind and

106
00:06:17,710 --> 00:06:21,000
 be helpful. That's just a wonderful thing.

107
00:06:21,000 --> 00:06:25,000
 And it's a wonderful thing that becomes so very powerful in

108
00:06:25,000 --> 00:06:29,000
 a Buddhist context because it's our spiritual practice.

109
00:06:29,000 --> 00:06:33,330
 Of course it would be the same in a Christian context. Or a

110
00:06:33,330 --> 00:06:37,000
 Jewish context, whatever context.

111
00:06:37,000 --> 00:06:42,530
 A Muslim context, a Hindu context. This giving, of course

112
00:06:42,530 --> 00:06:49,450
 given our belief or our view, our opinion that the Buddha's

113
00:06:49,450 --> 00:06:53,000
 teaching is the purest.

114
00:06:53,000 --> 00:06:56,060
 Of course because it's very much the core of the Buddhist

115
00:06:56,060 --> 00:06:59,000
 teaching is to do good deeds and to avoid bad deeds.

116
00:06:59,000 --> 00:07:09,300
 There's no overhead. There's no extraneous views or

117
00:07:09,300 --> 00:07:14,000
 opinions or beliefs or practices.

118
00:07:14,000 --> 00:07:18,640
 The practice is really to make yourself a better person, to

119
00:07:18,640 --> 00:07:22,810
 make your mind more pure by giving gifts and by keeping

120
00:07:22,810 --> 00:07:24,000
 morality.

121
00:07:24,000 --> 00:07:28,000
 So this is really our spiritual practice.

122
00:07:28,000 --> 00:07:31,610
 And this is the wonderful thing is that this is people

123
00:07:31,610 --> 00:07:35,000
 getting together to do this in a spiritual sense.

124
00:07:35,000 --> 00:07:38,810
 And you can only really get that in a community like this

125
00:07:38,810 --> 00:07:43,000
 or it becomes much more powerful in a community like this,

126
00:07:43,000 --> 00:07:45,000
 in a monastic community.

127
00:07:45,000 --> 00:07:49,560
 And there's no question in my mind that the benefit of

128
00:07:49,560 --> 00:07:54,230
 being a monk is the greater benefit to oneself and to

129
00:07:54,230 --> 00:07:57,000
 others, to the whole world.

130
00:07:57,000 --> 00:08:01,480
 Even just as an example, even just as someone, it's a very

131
00:08:01,480 --> 00:08:04,000
 powerful thing to be an example.

132
00:08:04,000 --> 00:08:10,520
 To see someone doing good things, to see someone meditating

133
00:08:10,520 --> 00:08:11,000
.

134
00:08:11,000 --> 00:08:14,130
 When we're sitting around and suddenly we see one of us is

135
00:08:14,130 --> 00:08:17,000
 meditating, we all remember, "Oh yes."

136
00:08:17,000 --> 00:08:19,000
 And it brings us back to our meditation.

137
00:08:19,000 --> 00:08:21,610
 But it can really change your life and of course it's

138
00:08:21,610 --> 00:08:25,150
 changed many people's lives just to see other people

139
00:08:25,150 --> 00:08:26,000
 ordained.

140
00:08:26,000 --> 00:08:28,650
 When we go on alms round or when people come to the

141
00:08:28,650 --> 00:08:33,430
 monastery and see us, they're so happy and it really

142
00:08:33,430 --> 00:08:37,000
 changes people.

143
00:08:37,000 --> 00:08:41,740
 So I don't see benefit because I'm not really concerned

144
00:08:41,740 --> 00:08:45,500
 about society or the direction that it's taking or the

145
00:08:45,500 --> 00:08:48,000
 benefits towards society.

146
00:08:48,000 --> 00:08:54,660
 I'm concerned I suppose, or concerned I'm interested or I

147
00:08:54,660 --> 00:08:59,000
 don't know how to put it correctly.

148
00:08:59,000 --> 00:09:06,120
 I enjoy I suppose and appreciate and do as a matter of

149
00:09:06,120 --> 00:09:12,000
 course try to help people, help humanity.

150
00:09:12,000 --> 00:09:14,760
 But I don't think that has anything to do with the lay life

151
00:09:14,760 --> 00:09:15,000
.

152
00:09:15,000 --> 00:09:18,000
 I don't think the lay life is anything that benefits people

153
00:09:18,000 --> 00:09:20,540
 because of course when you put on the pants, when you put

154
00:09:20,540 --> 00:09:21,000
 on the shirt,

155
00:09:21,000 --> 00:09:24,340
 when you go to get a job you're taking part in many of the

156
00:09:24,340 --> 00:09:28,540
 things that we see as being the problem, the consumerism

157
00:09:28,540 --> 00:09:30,000
 and materialism

158
00:09:30,000 --> 00:09:35,000
 and just so much uselessness that goes on out there.

159
00:09:35,000 --> 00:09:39,200
 Even just our interaction with people who come here, the

160
00:09:39,200 --> 00:09:44,000
 comparison, what they get from us and what we pick up from

161
00:09:44,000 --> 00:09:44,000
 them

162
00:09:44,000 --> 00:09:48,000
 is really two different, two totally different things.

163
00:09:48,000 --> 00:09:52,020
 So what we get from them is their greed and their stress

164
00:09:52,020 --> 00:09:58,320
 and their frustrations and sadness and suffering that they

165
00:09:58,320 --> 00:10:01,000
 have in their lives and living in the world.

166
00:10:01,000 --> 00:10:06,820
 And when you compare it with the peace and the happiness

167
00:10:06,820 --> 00:10:12,370
 and the simplicity of the life that we lead, it's really

168
00:10:12,370 --> 00:10:15,000
 not difficult to make a choice.

169
00:10:15,000 --> 00:10:19,220
 I would say the biggest reason, the only really powerful

170
00:10:19,220 --> 00:10:24,540
 reason that leads people to disrobe is sensuality or

171
00:10:24,540 --> 00:10:28,000
 sexuality to put it bluntly.

172
00:10:28,000 --> 00:10:32,580
 I suppose you could expand that to sensuality. Some people

173
00:10:32,580 --> 00:10:39,000
 are just attached to their food for example and it's funny

174
00:10:39,000 --> 00:10:40,000
 how silly people can be

175
00:10:40,000 --> 00:10:43,610
 as wanting this food or wanting that food and knowing that

176
00:10:43,610 --> 00:10:48,430
 as a monk it's not convenient to get it, entertainment and

177
00:10:48,430 --> 00:10:49,000
 so on.

178
00:10:49,000 --> 00:10:51,760
 But at the core I would say sexuality and it's something

179
00:10:51,760 --> 00:10:54,000
 that leads people to not be able to ordain.

180
00:10:54,000 --> 00:11:00,130
 Now I've been down there, that road, and I'm happy to

181
00:11:00,130 --> 00:11:05,000
 discuss that, the question of sexuality.

182
00:11:05,000 --> 00:11:13,240
 It's really a, it's one of the more wonderful benefits of

183
00:11:13,240 --> 00:11:21,470
 being a Buddhist monk is to look objectively at the sexual

184
00:11:21,470 --> 00:11:24,000
 desire, the sexual impulse

185
00:11:24,000 --> 00:11:28,430
 and to come to break it up as I've said in other videos and

186
00:11:28,430 --> 00:11:34,000
 to see the pieces of it, to see the pleasure and to finally

187
00:11:34,000 --> 00:11:39,000
, because even as lay people we repress and we feel guilty

188
00:11:39,000 --> 00:11:41,000
 about our sexual desires

189
00:11:41,000 --> 00:11:43,430
 and it's something that you have to do in private and

190
00:11:43,430 --> 00:11:53,600
 something that you have to hide. But we still, even though

191
00:11:53,600 --> 00:11:56,580
 we might say a Buddhist monk is someone who is repressing

192
00:11:56,580 --> 00:11:57,000
 these desires

193
00:11:57,000 --> 00:12:02,610
 it's actually the opposite. You finally have a forum in

194
00:12:02,610 --> 00:12:07,640
 which you can approach these issues. You can sit in your k

195
00:12:07,640 --> 00:12:11,000
utti and you can not have to feel guilty.

196
00:12:11,000 --> 00:12:13,650
 You say, "Well I know I'm not going to follow it, I'm just

197
00:12:13,650 --> 00:12:16,000
 sitting here and I have this desire and so on."

198
00:12:16,000 --> 00:12:20,190
 And so actually it winds up being the opposite. But finally

199
00:12:20,190 --> 00:12:24,190
 you have a chance to say, "Yes I have this sexual desire, I

200
00:12:24,190 --> 00:12:28,450
 have this pleasure arising in the body, all these chemical

201
00:12:28,450 --> 00:12:30,000
 reactions and so on."

202
00:12:30,000 --> 00:12:37,460
 Anyway that's not really what the question was about but

203
00:12:37,460 --> 00:12:45,000
 for myself I think I've been lucky to have good teachers

204
00:12:45,000 --> 00:12:56,000
 and I'm able to

205
00:12:56,000 --> 00:13:01,170
 look honestly at the issues that are inside myself. So

206
00:13:01,170 --> 00:13:05,590
 these issues where you might say, "I know it's wrong but I

207
00:13:05,590 --> 00:13:07,000
 have to disrobe."

208
00:13:07,000 --> 00:13:11,060
 It seems to me that when you really look at them and when

209
00:13:11,060 --> 00:13:15,250
 you are honest about them that they cease to become a

210
00:13:15,250 --> 00:13:20,000
 problem and they simply become another object of meditation

211
00:13:20,000 --> 00:13:20,000
.

212
00:13:20,000 --> 00:13:24,030
 So good question and these questions are always interesting

213
00:13:24,030 --> 00:13:27,490
 I think for people because there's a lot of people out

214
00:13:27,490 --> 00:13:32,000
 there looking to ordain or considering ordination.

215
00:13:32,000 --> 00:13:37,960
 So interesting to talk about. And these are also the videos

216
00:13:37,960 --> 00:13:40,120
 that become very contentious because half the people say, "

217
00:13:40,120 --> 00:13:42,000
Wonderful, wonderful, I agree completely."

218
00:13:42,000 --> 00:13:45,200
 And other people who say, "How can you denounce society and

219
00:13:45,200 --> 00:13:48,800
 lay life?" And the Buddha said that lay people could become

220
00:13:48,800 --> 00:13:50,000
 enlightened as well and so on and so on.

221
00:13:50,000 --> 00:13:53,510
 So there's nothing wrong with being a lay person and you

222
00:13:53,510 --> 00:13:58,770
 can practice meditation but there's no question that from

223
00:13:58,770 --> 00:14:02,960
 Buddhist point of view the monastic life or the simple med

224
00:14:02,960 --> 00:14:11,000
itative life is to be...

225
00:14:11,000 --> 00:14:14,550
 You could say, really could say the monastic life is of

226
00:14:14,550 --> 00:14:18,360
 greater benefit. So you're giving up something that is of

227
00:14:18,360 --> 00:14:23,180
 lesser benefit or is of lesser use or is less conducive for

228
00:14:23,180 --> 00:14:26,110
 the practice of meditation because of course the monastic

229
00:14:26,110 --> 00:14:28,000
 community has its benefits as well.

230
00:14:28,000 --> 00:14:32,900
 Even though lay people can live meditative simple lives, it

231
00:14:32,900 --> 00:14:36,920
's very easy for them to get off track and to get lost

232
00:14:36,920 --> 00:14:41,680
 because of the lack of discipline and community which is

233
00:14:41,680 --> 00:14:45,000
 gained from being a Buddhist monastic.

234
00:14:45,000 --> 00:14:48,000
 So those are my thoughts. Just some thoughts.

