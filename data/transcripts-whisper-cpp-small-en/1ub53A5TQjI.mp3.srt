1
00:00:00,000 --> 00:00:03,530
 What sort of benefit is there when one writes up the suttas

2
00:00:03,530 --> 00:00:06,000
 copying them down by hand?

3
00:00:06,000 --> 00:00:12,000
 Well, it's a good way to remember them.

4
00:00:12,000 --> 00:00:22,420
 I suppose maybe you're asking because you're searching for

5
00:00:22,420 --> 00:00:27,000
 a way to get closer to the suttas and remember them and so

6
00:00:27,000 --> 00:00:27,000
 on.

7
00:00:27,000 --> 00:00:31,460
 The best way to remember the suttas is, or the best way to

8
00:00:31,460 --> 00:00:36,000
 approach the suttas is to read them.

9
00:00:36,000 --> 00:00:40,010
 No, to practice meditation, then read them, then go back

10
00:00:40,010 --> 00:00:42,000
 and practice meditation.

11
00:00:42,000 --> 00:00:45,980
 You should practice meditation first because it will help

12
00:00:45,980 --> 00:00:48,000
 you to understand the suttas.

13
00:00:48,000 --> 00:00:52,200
 So you should at least start to practice before you go into

14
00:00:52,200 --> 00:00:54,000
 in-depth study of them.

15
00:00:55,000 --> 00:00:59,380
 And once you've undergone in-depth or taken some study of

16
00:00:59,380 --> 00:01:02,000
 them, then you should go back to meditate.

17
00:01:02,000 --> 00:01:06,820
 Because meditation improves your memory and improves your

18
00:01:06,820 --> 00:01:10,520
 interest in the suttas, which also improves your memory of

19
00:01:10,520 --> 00:01:11,000
 the suttas.

20
00:01:11,000 --> 00:01:15,210
 Because it's interest that leads us to be able to keep

21
00:01:15,210 --> 00:01:17,000
 things in the mind.

22
00:01:17,000 --> 00:01:21,110
 We aren't easily able to remember just facts and stories,

23
00:01:21,110 --> 00:01:23,000
 but if something is interesting to us,

24
00:01:23,000 --> 00:01:26,000
 we're much more able to remember it.

25
00:01:26,000 --> 00:01:31,020
 So simply writing the suttas down is not a very good way to

26
00:01:31,020 --> 00:01:35,660
 get closer to the suttas or to gain benefit from the suttas

27
00:01:35,660 --> 00:01:36,000
.

28
00:01:36,000 --> 00:01:42,760
 There's a story of a monk who went through the 152 suttas

29
00:01:42,760 --> 00:01:45,000
 of the Majima Nikaya.

30
00:01:45,000 --> 00:01:49,920
 And he was trying to memorize them and it was very, very

31
00:01:49,920 --> 00:01:51,000
 difficult.

32
00:01:51,000 --> 00:01:53,520
 And so eventually he gave up and went and practiced

33
00:01:53,520 --> 00:01:57,000
 meditation for 20 years and became an Arahant.

34
00:01:57,000 --> 00:02:02,000
 And then someone came to ask him about the Majima Nikaya

35
00:02:02,000 --> 00:02:05,000
 and he was able to explain it without fail.

36
00:02:05,000 --> 00:02:09,370
 Even though he said, "Well, in 20 years I haven't looked at

37
00:02:09,370 --> 00:02:11,000
 the Majima Nikaya."

38
00:02:11,000 --> 00:02:15,830
 And he said, "But try, go ahead and start reciting and I'll

39
00:02:15,830 --> 00:02:18,000
 see if I can pick it up."

40
00:02:18,000 --> 00:02:22,060
 And he was able to pick it up without failing a single word

41
00:02:22,060 --> 00:02:23,000
 or so on.

42
00:02:23,000 --> 00:02:25,000
 This is what it says in the V.C.D. Maga.

43
00:02:25,000 --> 00:02:30,350
 So this is how we should approach, this is a good example

44
00:02:30,350 --> 00:02:33,000
 of how we should approach the suttas.

45
00:02:33,000 --> 00:02:38,870
 That it's much more important and beneficial to meditate

46
00:02:38,870 --> 00:02:43,710
 based on them than it is to really do any kind of in-depth

47
00:02:43,710 --> 00:02:45,000
 study of them.

48
00:02:45,000 --> 00:02:55,000
 If your impetus for asking is in regards to the sort of

49
00:02:55,000 --> 00:03:00,000
 general merit that one gains from it,

50
00:03:00,000 --> 00:03:04,140
 well, one feels happy in the mind and that peace in mind

51
00:03:04,140 --> 00:03:06,000
 for doing a good deed.

52
00:03:06,000 --> 00:03:13,850
 The commentary says, writing one letter of Dhamma is

53
00:03:13,850 --> 00:03:18,000
 equivalent to, if it's one letter,

54
00:03:18,000 --> 00:03:24,530
 each letter that makes up a Dhamma sutta that you write out

55
00:03:24,530 --> 00:03:28,340
 or print out is the equivalent of building one Buddha

56
00:03:28,340 --> 00:03:29,000
 statue.

57
00:03:30,000 --> 00:03:34,620
 It's a monument of the Dhamma, a Buddhist monument that you

58
00:03:34,620 --> 00:03:36,000
're creating.

59
00:03:36,000 --> 00:03:43,240
 It's helping to spread Buddhism. It's helping to spread the

60
00:03:43,240 --> 00:03:45,000
 meditation practice,

61
00:03:45,000 --> 00:03:58,000
 to keep Buddhism alive and to give guidance to other people

62
00:03:58,000 --> 00:03:58,000
.

63
00:03:58,000 --> 00:04:02,920
 So, printing up Dhamma books, sharing Dhamma books is

64
00:04:02,920 --> 00:04:05,000
 always a good thing.

65
00:04:05,000 --> 00:04:08,430
 It's something which benefits your own mind, makes you feel

66
00:04:08,430 --> 00:04:10,000
 peaceful and happy inside.

67
00:04:10,000 --> 00:04:13,400
 And so there is great benefit. If you make the

68
00:04:13,400 --> 00:04:17,000
 determination at the time to become,

69
00:04:17,000 --> 00:04:21,740
 you can do it as a practice to encourage your mind to get

70
00:04:21,740 --> 00:04:23,000
 closer to the suttas

71
00:04:23,000 --> 00:04:26,870
 and to gain wisdom and understanding and to be able to meet

72
00:04:26,870 --> 00:04:28,000
 with the Buddha

73
00:04:28,000 --> 00:04:32,120
 or to be able to become well versed in the Buddha's

74
00:04:32,120 --> 00:04:36,000
 teaching in the future, maybe in a future life,

75
00:04:36,000 --> 00:04:41,900
 then it can be a great symbol or object for you to

76
00:04:41,900 --> 00:04:45,000
 cultivate this determination for.

77
00:04:45,000 --> 00:04:52,680
 And as a result, it can support your determination, your Ad

78
00:04:52,680 --> 00:04:53,000
hitaana,

79
00:04:53,000 --> 00:04:55,120
 when you make a determination that in a future existence,

80
00:04:55,120 --> 00:04:57,000
 may I become this or become that,

81
00:04:57,000 --> 00:05:00,860
 it can have that benefit as well. If you use it just to

82
00:05:00,860 --> 00:05:03,000
 make a determination,

83
00:05:03,000 --> 00:05:06,040
 may this support my meditation practice, then you can use

84
00:05:06,040 --> 00:05:07,000
 that as well.

85
00:05:07,000 --> 00:05:12,570
 It's like all good deeds, it has the benefits of fortifying

86
00:05:12,570 --> 00:05:15,000
 this one's mind state.

