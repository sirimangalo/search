1
00:00:00,000 --> 00:00:24,000
 [Silence]

2
00:00:24,000 --> 00:00:31,620
 Okay, good evening everyone. We're broadcasting live.

3
00:00:31,620 --> 00:00:38,000
 January 23rd, 2016.

4
00:00:45,000 --> 00:00:53,000
 Today's quote is from the Anguttara Nikaya, Book of Four's.

5
00:00:53,000 --> 00:00:58,160
 "With four qualities, the wise, intelligent, worthy person

6
00:00:58,160 --> 00:01:02,500
 proceeds through life with firm foundations, smoothly, irre

7
00:01:02,500 --> 00:01:07,000
proachably, not censured by the wise.

8
00:01:07,000 --> 00:01:18,970
 What for? With good conduct of body, speech, and mind, and

9
00:01:18,970 --> 00:01:27,000
 with gratitude, with gratefulness."

10
00:01:27,000 --> 00:01:33,130
 Tonight we're broadcasting from the main hall, otherwise

11
00:01:33,130 --> 00:01:39,000
 known as the living room of our house, the monastery.

12
00:01:39,000 --> 00:01:48,000
 We have two meditators, one from Finland, one from Ukraine.

13
00:01:48,000 --> 00:01:55,990
 Listening. We have a live audience tonight. It's pretty

14
00:01:55,990 --> 00:01:57,000
 awesome.

15
00:01:57,000 --> 00:02:03,220
 We have people staying here with us. So all of those of you

16
00:02:03,220 --> 00:02:09,430
 who have supported this place can know that it's being put

17
00:02:09,430 --> 00:02:11,000
 to good use.

18
00:02:11,000 --> 00:02:19,450
 We have people coming next month and already in March we

19
00:02:19,450 --> 00:02:23,000
 have people coming.

20
00:02:23,000 --> 00:02:26,890
 We also have a web camera, but didn't set it up in time so

21
00:02:26,890 --> 00:02:35,070
 maybe by tomorrow we'll have a webcam if you want to take a

22
00:02:35,070 --> 00:02:43,000
 peek at us sitting here in the hall.

23
00:02:43,000 --> 00:02:47,200
 So the quote is four things that lead one to proceed

24
00:02:47,200 --> 00:02:52,310
 through life with firm foundations, smoothly, irreproach

25
00:02:52,310 --> 00:02:56,000
ably, not censured by the wise.

26
00:02:56,000 --> 00:03:00,000
 So four things that make life go smoothly.

27
00:03:00,000 --> 00:03:11,000
 The first three are good deeds, body, speech, and mind.

28
00:03:11,000 --> 00:03:18,000
 Kaya kamma, kuadhi kamma, mano kamma.

29
00:03:18,000 --> 00:03:23,080
 These are the three doors. Every karma that we perform,

30
00:03:23,080 --> 00:03:27,550
 every act that we perform has to be through one of the

31
00:03:27,550 --> 00:03:29,000
 three doors.

32
00:03:29,000 --> 00:03:34,000
 It's just a convention.

33
00:03:34,000 --> 00:03:40,650
 It is conventionally the three doors that it's useful to

34
00:03:40,650 --> 00:03:46,000
 allow us to identify quickly our actions.

35
00:03:46,000 --> 00:03:52,760
 Am I committing an act, a wholesome act or an unwholesome

36
00:03:52,760 --> 00:04:00,000
 act with my body, with my speech, or even in the mind?

37
00:04:00,000 --> 00:04:06,370
 I think significant here is that the mind is given its own

38
00:04:06,370 --> 00:04:10,690
 door, which is significant in that it shows that karma can

39
00:04:10,690 --> 00:04:20,310
 be performed without even acting or speaking, just by

40
00:04:20,310 --> 00:04:29,000
 thinking.

41
00:04:29,000 --> 00:04:34,000
 And in fact, all three doors require the mind.

42
00:04:34,000 --> 00:04:38,840
 Something is only wholesome if it has a wholesome mind

43
00:04:38,840 --> 00:04:41,000
 associated with it.

44
00:04:41,000 --> 00:04:47,000
 Simply acting, going through the motions is not wholesome

45
00:04:47,000 --> 00:04:49,000
 or unwholesome.

46
00:04:49,000 --> 00:04:52,410
 It's only the intention in the mind that gives it the

47
00:04:52,410 --> 00:04:55,000
 wholesome or unwholesome quality.

48
00:04:55,000 --> 00:04:59,000
 And that's how meditation affects our lives.

49
00:04:59,000 --> 00:05:06,120
 Think, how does it, what good does it do to walk and sit

50
00:05:06,120 --> 00:05:09,000
 alone in your room?

51
00:05:09,000 --> 00:05:13,000
 Especially when this kind of meditation isn't so calming as

52
00:05:13,000 --> 00:05:15,000
 you might wish for people.

53
00:05:15,000 --> 00:05:18,490
 It's very easy to be turned off in this meditation. It's

54
00:05:18,490 --> 00:05:23,000
 not something that everyone would want to practice.

55
00:05:23,000 --> 00:05:28,450
 It's not something everyone will continue to practice once

56
00:05:28,450 --> 00:05:32,000
 they get a feel for the nature of it.

57
00:05:32,000 --> 00:05:36,000
 It's too scary to look at the negative aspects of the mind.

58
00:05:36,000 --> 00:05:43,000
 It's too difficult, too painful.

59
00:05:43,000 --> 00:05:53,000
 People wonder why we do it. This is it, really, the mind.

60
00:05:53,000 --> 00:06:01,000
 It's the prominence of the mind. How important the mind is.

61
00:06:01,000 --> 00:06:08,560
 How our bad habits are actually habits that we've

62
00:06:08,560 --> 00:06:11,000
 cultivated.

63
00:06:11,000 --> 00:06:15,000
 We act based on habit. We get angry based on habit.

64
00:06:15,000 --> 00:06:19,000
 We are greedy and attached to things based on habit.

65
00:06:19,000 --> 00:06:23,000
 We're arrogant and conceited based on habit.

66
00:06:23,000 --> 00:06:28,170
 And so we can change these habits. If we learn to be

67
00:06:28,170 --> 00:06:29,000
 objective,

68
00:06:29,000 --> 00:06:35,000
 we can start to incline our minds in a different way.

69
00:06:35,000 --> 00:06:41,000
 Incline our minds towards patience.

70
00:06:41,000 --> 00:06:45,000
 Incline our minds towards renunciation.

71
00:06:45,000 --> 00:06:51,000
 Incline our minds towards humility and wisdom.

72
00:06:51,000 --> 00:06:58,000
 Incline our minds towards freedom.

73
00:06:58,000 --> 00:07:01,000
 So when you wonder what you're doing in the meditation,

74
00:07:01,000 --> 00:07:05,000
 you just remember every moment when your mind is clear,

75
00:07:05,000 --> 00:07:07,000
 when your mind is pure.

76
00:07:07,000 --> 00:07:13,000
 That's a karma, mano kamma. It's a wholesome karma.

77
00:07:13,000 --> 00:07:17,000
 And what that means is it's cultivating a habit.

78
00:07:17,000 --> 00:07:21,580
 So you may not see the results today, maybe not even

79
00:07:21,580 --> 00:07:24,000
 tomorrow.

80
00:07:24,000 --> 00:07:29,530
 You have no reason to doubt, no room for doubt that it's

81
00:07:29,530 --> 00:07:31,000
 changing.

82
00:07:31,000 --> 00:07:40,360
 It's changing your habit. It's cultivating a new, better

83
00:07:40,360 --> 00:07:42,000
 habit.

84
00:07:42,000 --> 00:07:45,000
 And the fourth one in this list is actually interesting.

85
00:07:45,000 --> 00:07:50,000
 It looks like it's kind of just thrown in there.

86
00:07:50,000 --> 00:07:54,000
 And the Anguttranika is kind of like this. There's the book

87
00:07:54,000 --> 00:07:54,000
 of twos,

88
00:07:54,000 --> 00:08:00,000
 the book of ones, the book of twos, all the way up to the

89
00:08:00,000 --> 00:08:04,000
 book of elevens.

90
00:08:04,000 --> 00:08:09,000
 And some of these lists are repeated.

91
00:08:09,000 --> 00:08:12,080
 It will be in the book of fours and then the book of fives

92
00:08:12,080 --> 00:08:13,000
 will be the same list,

93
00:08:13,000 --> 00:08:15,000
 but one more thing is added on.

94
00:08:15,000 --> 00:08:19,250
 And in the book of six, they add a sixth thing on. It kind

95
00:08:19,250 --> 00:08:20,000
 of goes like that.

96
00:08:20,000 --> 00:08:24,200
 Sometimes the same list is in two or three different books,

97
00:08:24,200 --> 00:08:26,000
 just with one more thing.

98
00:08:26,000 --> 00:08:37,000
 It seems that these lists were catered to individuals.

99
00:08:37,000 --> 00:08:40,520
 And so this one may have just been catered to a group of

100
00:08:40,520 --> 00:08:41,000
 monks,

101
00:08:41,000 --> 00:08:45,000
 a group of laypeople talking about gratitude.

102
00:08:45,000 --> 00:08:51,000
 But gratitude doesn't get so much airtime in Buddhism.

103
00:08:51,000 --> 00:08:59,000
 But in this context, it is actually added to this list

104
00:08:59,000 --> 00:09:03,730
 because it really is a very powerful mind state to be

105
00:09:03,730 --> 00:09:05,000
 grateful.

106
00:09:05,000 --> 00:09:09,600
 In Buddhism, we often, in all different schools and

107
00:09:09,600 --> 00:09:11,000
 traditionally,

108
00:09:11,000 --> 00:09:15,000
 if you go to a traditional Buddhist culture,

109
00:09:15,000 --> 00:09:22,000
 there often is a sense of thanking people who do you harm.

110
00:09:22,000 --> 00:09:24,330
 You thank them for doing you harm because it's a chance for

111
00:09:24,330 --> 00:09:27,000
 you to practice patience.

112
00:09:27,000 --> 00:09:33,000
 There were Sri Lankan monk ones who said, "He doesn't like

113
00:09:33,000 --> 00:09:34,000
 going to Thailand."

114
00:09:34,000 --> 00:09:39,050
 "He doesn't like going to Thailand or any of the Southeast

115
00:09:39,050 --> 00:09:41,000
 Asian countries."

116
00:09:41,000 --> 00:09:43,000
 But his reasoning was interesting.

117
00:09:43,000 --> 00:09:45,810
 He said, "He doesn't like going there because he can't

118
00:09:45,810 --> 00:09:47,000
 practice metta."

119
00:09:47,000 --> 00:09:54,000
 When he goes to Thailand, the people are too nice to him.

120
00:09:54,000 --> 00:09:58,130
 Too nice to him and so he can't cultivate love because of

121
00:09:58,130 --> 00:09:59,000
 course in Sri Lanka,

122
00:09:59,000 --> 00:10:02,300
 or not of course, but in Sri Lanka, oh, they treat the

123
00:10:02,300 --> 00:10:03,000
 monks.

124
00:10:03,000 --> 00:10:08,000
 Not badly, but they really put them through their paces.

125
00:10:08,000 --> 00:10:14,000
 If a monk is misbehaving, they'll call them out on it.

126
00:10:14,000 --> 00:10:17,000
 They'll argue with the monks.

127
00:10:17,000 --> 00:10:21,000
 This is a fairly famous Buddhist monk.

128
00:10:21,000 --> 00:10:25,000
 It's common in Zen Buddhism, for example.

129
00:10:25,000 --> 00:10:28,780
 They go around hitting the meditators to make them straight

130
00:10:28,780 --> 00:10:30,000
en their backs,

131
00:10:30,000 --> 00:10:34,000
 make sure they sit up straight and they get them a whack.

132
00:10:34,000 --> 00:10:38,810
 Then they have to put their hands together and say, "Thank

133
00:10:38,810 --> 00:10:41,000
 you."

134
00:10:41,000 --> 00:10:45,220
 But why this is interesting is I think it points to

135
00:10:45,220 --> 00:10:46,000
 something,

136
00:10:46,000 --> 00:10:49,000
 the power of gratitude.

137
00:10:49,000 --> 00:10:52,520
 The power of gratitude specifically, not in a practice

138
00:10:52,520 --> 00:10:53,000
 sense

139
00:10:53,000 --> 00:10:56,000
 in terms of cultivating humility and so on.

140
00:10:56,000 --> 00:11:02,000
 It can lead to fake humility even where you say thank you.

141
00:11:02,000 --> 00:11:08,000
 It pretends, like you're trying to be a good person,

142
00:11:08,000 --> 00:11:13,000
 but you're really angry at the person.

143
00:11:13,000 --> 00:11:17,000
 But it's important because it smooths things over.

144
00:11:17,000 --> 00:11:20,000
 If you try, if you're grateful to people,

145
00:11:20,000 --> 00:11:23,170
 it's a really good way to make friends, to keep friends, to

146
00:11:23,170 --> 00:11:25,000
 solidify friendship.

147
00:11:25,000 --> 00:11:31,000
 It says, "I see what you did. I recognize what you did."

148
00:11:31,000 --> 00:11:36,000
 I know as a Buddhist monk we have a lot to be grateful for.

149
00:11:36,000 --> 00:11:40,000
 People give us food, they give us shelter.

150
00:11:40,000 --> 00:11:48,000
 And I know I've been guilty of ingratitude to supporters

151
00:11:48,000 --> 00:11:51,000
 and mostly to supporters.

152
00:11:51,000 --> 00:11:57,770
 I've been supported when I was in Thailand, when I was in

153
00:11:57,770 --> 00:11:59,000
 Sri Lanka,

154
00:11:59,000 --> 00:12:04,160
 when I was in California, I was supported really well in

155
00:12:04,160 --> 00:12:05,000
 California,

156
00:12:05,000 --> 00:12:08,000
 supported by so many different people.

157
00:12:08,000 --> 00:12:13,610
 I know at times it's easy to forget, and it's a lesson to

158
00:12:13,610 --> 00:12:15,000
 be learned

159
00:12:15,000 --> 00:12:21,000
 because it's unfair to the people who are supporting you.

160
00:12:21,000 --> 00:12:26,100
 So now I try, I'm not perfect, but I try to keep track if

161
00:12:26,100 --> 00:12:28,000
 someone sends me a gift.

162
00:12:28,000 --> 00:12:32,000
 I have a whole stack full of cards that people have sent me

163
00:12:32,000 --> 00:12:32,000
,

164
00:12:32,000 --> 00:12:38,300
 and I try not to throw them out because it's a good

165
00:12:38,300 --> 00:12:40,000
 reminder

166
00:12:40,000 --> 00:12:42,000
 to say thank you to people.

167
00:12:42,000 --> 00:12:45,000
 It's a good way to live your life.

168
00:12:45,000 --> 00:12:49,000
 And if they do the smallest things for you to be grateful,

169
00:12:49,000 --> 00:12:54,000
 it's part of being mindful. It's convention.

170
00:12:54,000 --> 00:12:59,000
 It's certainly not deep teaching for meditators.

171
00:12:59,000 --> 00:13:02,950
 I think you could argue that a meditator becomes more

172
00:13:02,950 --> 00:13:04,000
 grateful.

173
00:13:04,000 --> 00:13:06,730
 Remember when I did my first meditation course, I started

174
00:13:06,730 --> 00:13:07,000
 crying,

175
00:13:07,000 --> 00:13:09,000
 and they came and said, "What's wrong?"

176
00:13:09,000 --> 00:13:14,000
 I miss my parents, and I really felt grateful to them.

177
00:13:14,000 --> 00:13:18,510
 And I felt like a real jerk that I hadn't been grateful to

178
00:13:18,510 --> 00:13:19,000
 them

179
00:13:19,000 --> 00:13:20,000
 when I was younger.

180
00:13:20,000 --> 00:13:23,000
 We take our parents, parents are a good example.

181
00:13:23,000 --> 00:13:26,000
 I mean, some parents are really horrible to their kids,

182
00:13:26,000 --> 00:13:28,000
 but many parents are good to their children,

183
00:13:28,000 --> 00:13:33,000
 and the children just don't appreciate it.

184
00:13:33,000 --> 00:13:36,000
 I was one of those kids, I think.

185
00:13:36,000 --> 00:13:39,000
 I don't think I ever really appreciated them.

186
00:13:39,000 --> 00:13:42,000
 Not fully, anyway.

187
00:13:42,000 --> 00:13:47,000
 The sacrifices my parents went through.

188
00:13:47,000 --> 00:13:51,000
 And it causes friction, I mean, because they feel,

189
00:13:51,000 --> 00:13:56,000
 they feel depressed that they're not appreciated.

190
00:13:56,000 --> 00:13:58,390
 It's not that they expect anything in return, but it's

191
00:13:58,390 --> 00:13:59,000
 depressing.

192
00:13:59,000 --> 00:14:09,000
 It's sad to have your children be selfish and ungrateful.

193
00:14:09,000 --> 00:14:11,010
 I think it's something that comes from meditation,

194
00:14:11,010 --> 00:14:12,000
 certainly.

195
00:14:12,000 --> 00:14:14,000
 It's something that comes from meditation.

196
00:14:14,000 --> 00:14:20,000
 Your mind becomes pure, you're able to see things clear.

197
00:14:20,000 --> 00:14:25,000
 The word for gratitude in Pali is kattanyu and kattaweidi.

198
00:14:25,000 --> 00:14:27,140
 The reason there's two words here is because he's

199
00:14:27,140 --> 00:14:28,000
 translating both of them.

200
00:14:28,000 --> 00:14:31,000
 Kattanyu and kattaweidi.

201
00:14:31,000 --> 00:14:36,000
 So kattanyu anya means knowledge, anyu means one who knows,

202
00:14:36,000 --> 00:14:38,000
 one who has knowledge.

203
00:14:38,000 --> 00:14:40,000
 Katta means what is done.

204
00:14:40,000 --> 00:14:43,000
 So it's very much, the way it's worded is kind of in terms

205
00:14:43,000 --> 00:14:43,000
 of wisdom

206
00:14:43,000 --> 00:14:46,000
 or even mindfulness.

207
00:14:46,000 --> 00:14:50,000
 Kattanyu, one who knows what has been done.

208
00:14:50,000 --> 00:14:53,000
 So the implication is that if you're ungrateful,

209
00:14:53,000 --> 00:14:57,000
 it's because you just don't keep an awareness of it.

210
00:14:57,000 --> 00:15:14,000
 You don't pay attention to it.

211
00:15:14,000 --> 00:15:18,360
 And kattaweidi, kattaweidi means etymologically it means

212
00:15:18,360 --> 00:15:19,000
 the same thing

213
00:15:19,000 --> 00:15:23,000
 or grammatically it means the same thing.

214
00:15:23,000 --> 00:15:28,000
 Kattaweidi, veda means knowledge.

215
00:15:28,000 --> 00:15:31,580
 It's a different kind of knowledge but it's still

216
00:15:31,580 --> 00:15:33,000
 translated similarly.

217
00:15:33,000 --> 00:15:38,000
 The vedi, one who has knowledge or one who has awareness,

218
00:15:38,000 --> 00:15:42,000
 is conscious of it,

219
00:15:42,000 --> 00:15:46,000
 one who is conscious of what has been done.

220
00:15:46,000 --> 00:15:49,000
 So it could be synonymous but the commentary is kattaweidi,

221
00:15:49,000 --> 00:15:53,000
 kattanyu means knowledge, it means gratitude,

222
00:15:53,000 --> 00:15:56,000
 thankfulness, being thankful.

223
00:15:56,000 --> 00:16:00,000
 And kattaweidi means the desire to pay them back,

224
00:16:00,000 --> 00:16:08,000
 to do something, to express your gratitude.

225
00:16:08,000 --> 00:16:18,000
 So that's fourth one.

226
00:16:18,000 --> 00:16:21,000
 And I think we'll stop there.

227
00:16:21,000 --> 00:16:24,000
 Thank you all for tuning in.

228
00:16:24,000 --> 00:16:27,000
 Have a good night.

229
00:16:27,000 --> 00:16:28,000
 [

