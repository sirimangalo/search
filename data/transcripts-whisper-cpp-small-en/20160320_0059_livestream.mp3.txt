 [silence]
 [silence]
 [silence]
 Good evening everyone.
 I'm broadcasting live.
 Broadcasting live, March 19th.
 I think everyone's doing everything just okay.
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 Sorry, I'm just reading through the comments.
 Tonight's quote is actually a series of quotes.
 Four verses from the Dhammapada.
 One of which we've actually studied in our Dhammapada
 verses.
 [silence]
 Not the defaults of others, not the faults of others.
 Not what they have done, not what others have done and not
 done.
 Ata nova avikhe ha. One should look upon oneself.
 Look upon those of oneself. Katani akata nita.
 Done, things done and not done.
 This is a theme that runs through paravada Buddhism.
 It's our understanding that the Buddha actually taught us
 to look after ourselves.
 Actually on face value this one says something a little bit
 different.
 She says, "Don't go around blaming other people."
 Looking at the faults of others, criticizing others.
 But you could expand that to think about concern and
 worrying about others.
 Trying to fix everyone else's problems.
 There's another Dhammapada verse. Actually it might be one
 of these four.
 I think it's missing here.
 There's another one that's...
 Ata namimabhata mampatirupayuniwesay.
 One should set oneself in what is right first.
 And only then should one teach others.
 That's a wise one wouldn't defile themselves.
 Which is really the best explanation of what's going on.
 And some of these quotes is that in order to help others
 you really have to help yourself.
 And so there's this perception that it's selfish.
 Self-centered, selfish.
 To go off in your room and practice meditation.
 To come to a meditation center and spend your time med
itating apropos.
 Just this morning something happened that I didn't really
 expect to happen in Canada.
 Though I suppose thinking about it, it's happened many
 years ago.
 But it hasn't happened in many years.
 Someone in a van just down the street here was walking down
 the street in a white van.
 As it was driving by someone shouted out, "Get a job!"
 I couldn't really believe it. It was a surprise.
 Someone told me I should get a job.
 Which I think is apropos because there's a sense that
 spiritual practice is selfish, is lazy.
 That shout, that experience this morning, actually there's
 much more I could say on that.
 But not that relevant. Let's not go too far afield.
 But there is this sense, which is a shame.
 It shows a misunderstanding of what is truly a benefit and
 where one can be of most benefit.
 How one can best benefit the world.
 If everyone were to better themselves, then that's the
 first thing you can say.
 If everyone were to work on themselves, everyone in the
 world, then who would need to help anyone else?
 Second thing you could say is that someone who has worked
 on themselves is in a far better position to help others.
 If I just think about all the horrible things I did to
 other people before I knew anything about meditation.
 Mean things I've done, and unpleasantness I've created,
 suffering I've caused.
 And people who try to help do this as well.
 You try to help and you just end up confusing the situation
 because you yourself are full of biases and delusions and
 opinions and views that are not really based on reality.
 Meditation is actually really the highest duty that we have
.
 It's not an indulgence or laziness. Meditation is doing
 your duty.
 Just like you wouldn't walk into a crowded place if you
 haven't showered, if you haven't cleaned your body, if you
 haven't cleaned your clothes, washed your clothes.
 You wouldn't go walking into a public place because there's
 a sense that you have a duty to be clean and not smell.
 It's unpleasant in this world.
 And the same goes on a far more important scale with our
 mind.
 Though we don't do our duty, we have a duty to clean our
 minds.
 Because we don't do this, we fight and we argue and we
 manipulate each other and we cling to each other.
 We're not ready to face the world. We're not equipped to
 live in peace and harmony.
 We're defiled. If you want to put it in very harsh and sort
 of negative ways.
 In a very real sense we are defiled and until we clean
 ourselves we shouldn't be allowed to go in public.
 We shouldn't try to interact with other people.
 This is why people become monks, go off in the forest
 because they have a job to do.
 Because they don't want to get a job. They have a job.
 They're doing the most important job. This is why we
 appreciate and welcome people who want to come and meditate
.
 Because we feel they're doing the world a service.
 They're bettering the world just by learning about
 themselves.
 It's like how we say someone who keeps five precepts
 actually is giving a great gift to the world.
 They're not really doing something for themselves. They're
 bestowing freedom.
 Freedom from danger. That no being in the universe has
 reason to fear this person.
 When they stop killing and stealing and cheating and lying
 they free so many beings from fear.
 All beings actually. No being in the universe has reason to
 fear that person.
 So helping oneself is very important.
 When one looks at another's faults, and as always let's try
 and find this quote as well.
 (Tibetan)
 When a person looks at the faults of others, "Nithjang, Uj
janasan" always perceiving fault. Always full of envy.
 "Nithjang, Ujjanasan" is funny that envy is there. It's not
 envy. "Ujjanasan" is picking on others really.
 Seeing faults in others.
 "Paravanjanupasisa"
 For such a person, "Asavatasa vadhanti" such a person, "As
avas" the taints grow, increase.
 "Araso asavakaya" they're far away from the ending of the
 defilements.
 And then we skip to 159.
 "Atanam jeta takahira yatanum manu sasati"
 If only you would do, right? If you would do, if oneself
 would do, what one teaches others.
 "Sudantovattadam meta hatahikiradudamu"
 What does that mean?
 "Sudantovattadam meta"
 Well trained oneself, one could then train, one should then
 train others.
 For it is difficult to train oneself. "Atanikiradudamu"
 The self is difficult to tame.
 It's easy to give advice to others, right? Actually
 teaching is a lot easier than actually practicing.
 So if people say thank you, to me I say no thank you, you
're the one who's doing all the work.
 I'm just teaching you, I'm just leading you on the path,
 you're the one who has to do the work for yourself.
 Even the Buddha just pointed the way.
 And the last one which is I think probably the most.
 359, 379, what's the second one? 379.
 This is the most inspiring.
 "Atanak jodaya tanang patimang seta atana"
 "So atagutto satima sukhandikku ya isi"
 "Jodaya tanang" That's not it.
 "The self should guard oneself"
 "And should examine oneself"
 When one is self-guarded and mindful, "So atagutto satima
 sukhandikku ya isi"
 It's a good name for a monk.
 "Atagutto"
 One who guards oneself. If you want a good Pali name, there
's a good one.
 "Self-guarded" means to guard your senses. Guard the eye,
 the ear, the nose, the tongue, the body, and the mind.
 So the six doors, it's a good description of the meditation
 practice because all of our experiences come through these
 six doors.
 And if you get caught up in any one of them, it leads to
 bad habits.
 To put it simply, because it becomes habitual and it gets
 you caught up in it, gets you off track,
 gets you caught up in some kind of cycle of addiction or
 aversion or conceit or views or whatever.
 And leads to suffering, leads to stress.
 Because you don't see things clearly biased.
 And your biases create embarrassment and create stress and
 create conflict, create disappointment and expectations.
 So the next verses are all about yourself because that's
 where we focus.
 A river is only as pure as its source.
 All of our life and our interactions with the world around
 us, it's all dependent on our minds.
 Your state of mind is not pure, you can't expect to help
 others or have a good influence on others or have a good
 relationship with others.
 Work on yourself, it's the best thing you can do and in
 fact it's the only thing you need to do.
 Everything else comes naturally.
 Once your mind is pure, the more your mind is pure, the
 better your relationship with others, your influence on
 others, your impact on the world.
 And that's why we meditate, that's why we work on ourselves
, that's why we study ourselves.
 That's why we try to free ourselves from suffering.
 You can't help others if you're in pain and stressed about
 your own problems.
 But a person who's truly happy, they are a benefit to the
 world and to everyone they come in contact with.
 Alright, so that's the quote for tonight.
 I'm going to post the hangout if anyone has questions, you
're welcome to click on the link and come ask.
 I'll hang around for a few minutes to see if anyone has
 questions.
 This is the second broadcast today, today was second life
 day so I broadcasted in virtual reality.
 Only 20 people can listen to a second life talk, it's
 actually a limit placed by the company.
 It's quite expensive, second life is quite expensive for
 people who have land like the Buddha Center.
 Steven you're back but you're still... hello?
 Yep, I hear you.
 Do you have a question for me?
 No, I don't have a question.
 I just want to say a comment if you think it's proper.
 Just because you say something about how people feel about
 meditation, that they think sometimes that we are kind of
 wasting our time.
 I used to have a mental illness, I had depression and
 anxiety, severe depression and anxiety.
 And naturally I went through a paranoia state for one and a
 half months.
 At that time all my people, all my family took out and
 supported me.
 But after the crisis passed, they think that it wasn't
 necessary to do more things about it.
 Even though I didn't want to leave taking pills.
 So I decided not to take pills anymore and start meditating
 more seriously.
 And I think that sometimes we don't should pay attention on
 what other people said about leaving our jobs and stop
 taking meditamins.
 And if we feel that meditating is going to help us, we
 should just do that.
 That's how I make a comment.
 Well, thank you.
 Hey, you're smoking? Who is this guy?
 I'm not convinced.
 Stephen, can you tell us a little bit about yourself? Why
 are you here?
 I don't know who this guy is, but I think smoking on our
 broadcast is not allowed.
 Apologies.
 Should we have rules like that? Is that appropriate? Well,
 yes. I mean, that's not very respectful.
 You have to stop smoking if you want to join our broadcast.
 No smoking, no eating, that kind of thing.
 Do you have a question, sir?
 The reason I stick around is in case people have questions.
 If nobody has questions, I'm going to end the broadcast.
 Thank you. Thank you all. Have a good night.
 In there.
