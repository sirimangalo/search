1
00:00:00,000 --> 00:00:10,200
 The Buddha called the mind radiant

2
00:00:10,200 --> 00:00:20,200
 "Pabasarang"

3
00:00:20,200 --> 00:00:25,200
 and to be clear he wasn't

4
00:00:25,200 --> 00:00:35,200
 implying that the mind had substance or luminosity.

5
00:00:35,200 --> 00:00:40,200
 Like if you went in a dark room with the mind,

6
00:00:40,200 --> 00:00:43,200
 it would light up the room.

7
00:00:43,200 --> 00:00:51,200
 It was a figure of speech that has been misinterpreted.

8
00:00:51,200 --> 00:00:57,200
 But he simply meant that the mind is pure.

9
00:00:57,200 --> 00:01:01,200
 Pure like a radiant gem.

10
00:01:01,200 --> 00:01:06,200
 Crystal clear like a crystal clear pool of water.

11
00:01:06,200 --> 00:01:10,200
 There are no defilements inherent in the mind.

12
00:01:10,200 --> 00:01:17,200
 It was a literary tool, oratory tool.

13
00:01:17,200 --> 00:01:21,200
 When he said the mind is brilliant,

14
00:01:21,200 --> 00:01:25,200
 he was trying to make a point that

15
00:01:25,200 --> 00:01:31,200
 our ordinary experience of things

16
00:01:31,200 --> 00:01:37,200
 is not defiled. It's not unwholesome.

17
00:01:37,200 --> 00:01:42,200
 It's not bad ever. It's not good ever either.

18
00:01:42,200 --> 00:01:46,200
 It has no ethical quality to it whatsoever.

19
00:01:46,200 --> 00:01:52,200
 It's innocent.

20
00:01:52,200 --> 00:01:58,050
 No matter what we see or hear, smell or taste or feel or

21
00:01:58,050 --> 00:02:00,200
 think.

22
00:02:00,200 --> 00:02:04,200
 Even our thoughts are innocent.

23
00:02:04,200 --> 00:02:09,200
 Which is an important point because

24
00:02:09,200 --> 00:02:15,200
 quite often we feel guilty for having certain thoughts.

25
00:02:15,200 --> 00:02:19,200
 For imagining certain situations.

26
00:02:19,200 --> 00:02:26,200
 But our thoughts and our imaginations are not entirely

27
00:02:26,200 --> 00:02:32,200
 caused by our inclinations.

28
00:02:32,200 --> 00:02:38,200
 Thoughts can come based on past habits.

29
00:02:38,200 --> 00:02:48,200
 At any rate, the thoughts themselves are ethically inert.

30
00:02:48,200 --> 00:02:54,200
 So really, there's nothing wrong with the world.

31
00:02:54,200 --> 00:03:03,200
 There's no problem in the world, in our world, in our lives

32
00:03:03,200 --> 00:03:03,200
.

33
00:03:03,200 --> 00:03:14,200
 Except for three very small, tiny details.

34
00:03:14,200 --> 00:03:17,200
 Only three things that are wrong with the world.

35
00:03:17,200 --> 00:03:21,200
 Wrong with our world. Wrong with our minds.

36
00:03:21,200 --> 00:03:25,200
 These are called the Akuṣa the Mula.

37
00:03:25,200 --> 00:03:30,200
 The roots of unwholesomeness.

38
00:03:30,200 --> 00:03:33,200
 Mula means root.

39
00:03:33,200 --> 00:03:41,200
 By root is meant

40
00:03:41,200 --> 00:03:45,200
 base of a mind.

41
00:03:45,200 --> 00:03:50,200
 So an important quality of the mind

42
00:03:50,200 --> 00:03:56,200
 that roots it on either side of the fence.

43
00:03:56,200 --> 00:04:03,200
 If a mind is rootless, then you can't say it's good or bad.

44
00:04:03,200 --> 00:04:07,200
 But if it's rooted in one of these three things,

45
00:04:07,200 --> 00:04:12,200
 these three make it bad.

46
00:04:12,200 --> 00:04:15,200
 One of the three there.

47
00:04:15,200 --> 00:04:17,200
 Loba.

48
00:04:17,200 --> 00:04:20,200
 Desire.

49
00:04:20,200 --> 00:04:22,200
 Dosa.

50
00:04:22,200 --> 00:04:26,200
 Aversion.

51
00:04:26,200 --> 00:04:28,200
 And moha.

52
00:04:28,200 --> 00:04:33,200
 Delusion.

53
00:04:33,200 --> 00:04:38,240
 Our colloquial speech would say greed, anger, and delusion,

54
00:04:38,240 --> 00:04:42,200
 these three.

55
00:04:42,200 --> 00:04:46,200
 These are what's wrong with the world.

56
00:04:46,200 --> 00:04:53,860
 All problems can be boiled down to these three from a

57
00:04:53,860 --> 00:05:01,200
 Buddhist perspective.

58
00:05:01,200 --> 00:05:06,840
 It's not easy to see this. It's quite likely to be

59
00:05:06,840 --> 00:05:09,200
 contested.

60
00:05:09,200 --> 00:05:12,800
 Not a very appealing teaching when we think of all that's

61
00:05:12,800 --> 00:05:14,200
 wrong with the world.

62
00:05:14,200 --> 00:05:17,560
 We want to say there's so many other things wrong with this

63
00:05:17,560 --> 00:05:18,200
 world.

64
00:05:18,200 --> 00:05:23,200
 How can you trivialize the evil of the world?

65
00:05:23,200 --> 00:05:31,200
 Ignore all that's wrong with this world.

66
00:05:31,200 --> 00:05:36,740
 This sort of opposition is likely caused by our investment

67
00:05:36,740 --> 00:05:38,200
 in solutions.

68
00:05:38,200 --> 00:05:42,070
 We know what's wrong with the world. Anyone will tell you

69
00:05:42,070 --> 00:05:44,200
 what's wrong with the world.

70
00:05:44,200 --> 00:05:47,780
 And they'll offer up all sorts of solutions, maybe

71
00:05:47,780 --> 00:05:59,200
 economical, societal, political.

72
00:05:59,200 --> 00:06:08,080
 None of these are actually getting at the root of the

73
00:06:08,080 --> 00:06:10,200
 problem.

74
00:06:10,200 --> 00:06:16,010
 Through the practice of mindfulness, it's a crucial shift

75
00:06:16,010 --> 00:06:21,430
 in perspective to realize that there's nothing wrong with

76
00:06:21,430 --> 00:06:22,200
 experience.

77
00:06:22,200 --> 00:06:31,200
 Experience isn't the problem. It is what it is.

78
00:06:31,200 --> 00:06:35,540
 It's not worth clinging to. There's nothing about

79
00:06:35,540 --> 00:06:40,590
 experience that is worth getting upset about and worth

80
00:06:40,590 --> 00:06:47,200
 getting excited about.

81
00:06:47,200 --> 00:06:52,700
 The cause of suffering is not our experiences. It's our

82
00:06:52,700 --> 00:06:55,200
 relationship to them.

83
00:06:55,200 --> 00:06:58,200
 We're greed, anger, and delusion.

84
00:06:58,200 --> 00:07:09,200
 Greed is our desire, liking, and wanting.

85
00:07:09,200 --> 00:07:18,880
 And there should be no argument as to whether greed can be

86
00:07:18,880 --> 00:07:21,200
 a problem.

87
00:07:21,200 --> 00:07:28,500
 Obviously greed leads to addiction. It leads to rapacious

88
00:07:28,500 --> 00:07:30,200
ness.

89
00:07:30,200 --> 00:07:36,200
 It leads to manipulation.

90
00:07:36,200 --> 00:07:52,460
 It leads to ruin for those who become gamblers, for those

91
00:07:52,460 --> 00:08:02,200
 who are addicted to the point of obsessive consumption.

92
00:08:02,200 --> 00:08:07,200
 But it has more subtle ramifications.

93
00:08:07,200 --> 00:08:14,740
 I don't think anyone with a basic appreciation of the state

94
00:08:14,740 --> 00:08:19,200
 of reality has an argument against rapacious greed.

95
00:08:19,200 --> 00:08:28,200
 That somehow it can be okay or benevolent or innocuous.

96
00:08:28,200 --> 00:08:39,580
 But what about romance? What about a love of food, music,

97
00:08:39,580 --> 00:08:43,200
 poetry, art?

98
00:08:43,200 --> 00:08:50,320
 What about a love of friendship, a love of family? What

99
00:08:50,320 --> 00:08:56,200
 about these likeings?

100
00:08:56,200 --> 00:09:03,760
 Well, we can see some of the ways that some of our subtle

101
00:09:03,760 --> 00:09:14,280
 desires have led to ruin, have created great suffering in

102
00:09:14,280 --> 00:09:17,200
 the world.

103
00:09:17,200 --> 00:09:22,610
 For example, the climate, the environment, we're seeing the

104
00:09:22,610 --> 00:09:26,200
 results of seemingly innocuous greed.

105
00:09:26,200 --> 00:09:29,200
 Everybody wants stuff.

106
00:09:29,200 --> 00:09:35,200
 But all this stuff, getting what we want when we want it,

107
00:09:35,200 --> 00:09:39,200
 has actually made our world less comfortable.

108
00:09:39,200 --> 00:09:44,420
 It has made the planet a less comfortable place to live. We

109
00:09:44,420 --> 00:09:49,150
 polluted our own air, water, we've destroyed our natural

110
00:09:49,150 --> 00:09:53,200
 environment, made it ugly.

111
00:09:53,200 --> 00:10:02,190
 There's no question that the world is uglier because of our

112
00:10:02,190 --> 00:10:05,200
 rapaciousness.

113
00:10:05,200 --> 00:10:10,200
 There's no question that greed has an effect on health.

114
00:10:10,200 --> 00:10:17,670
 Our love of good food makes us fat, makes us unhealthy,

115
00:10:17,670 --> 00:10:24,200
 leads to diabetes, heart disease, cancer even.

116
00:10:24,200 --> 00:10:30,140
 But it goes even deeper than that. We're not as happy as we

117
00:10:30,140 --> 00:10:32,200
 think we are.

118
00:10:32,200 --> 00:10:39,270
 Take love of family, for example. We'll just skip romance

119
00:10:39,270 --> 00:10:46,790
 because it's a little easier to see how disturbing that can

120
00:10:46,790 --> 00:10:47,200
 be.

121
00:10:47,200 --> 00:10:51,270
 If you've ever been at the end of the receiving end of a

122
00:10:51,270 --> 00:10:56,300
 breakup, you know how much stress and suffering is involved

123
00:10:56,300 --> 00:10:58,200
 in this intense anguish.

124
00:10:58,200 --> 00:11:06,200
 Not being able to get what you want, to put it very simply.

125
00:11:06,200 --> 00:11:12,760
 But family, take family. It seems like a good thing, you

126
00:11:12,760 --> 00:11:18,200
 know. You should love your family.

127
00:11:18,200 --> 00:11:24,870
 What's wrong with that? It's one of the least threatening

128
00:11:24,870 --> 00:11:31,200
 forms of desire, of liking, of attachment, let's say.

129
00:11:31,200 --> 00:11:36,080
 It seems pretty innocent and even wholesome. Family is

130
00:11:36,080 --> 00:11:39,200
 important, people would say.

131
00:11:39,200 --> 00:11:46,230
 But consider, what makes your family special? What makes

132
00:11:46,230 --> 00:11:52,080
 your family worth your attachment more than somebody else's

133
00:11:52,080 --> 00:11:53,200
 family?

134
00:11:53,200 --> 00:12:01,270
 What makes family? For some people, family is people

135
00:12:01,270 --> 00:12:04,200
 unrelated by blood.

136
00:12:04,200 --> 00:12:08,670
 They call those people family. Friends become so close, you

137
00:12:08,670 --> 00:12:10,200
 call them family.

138
00:12:10,200 --> 00:12:16,480
 Close family becomes someone persona non grata. You no

139
00:12:16,480 --> 00:12:20,060
 longer consider them family based on their character, based

140
00:12:20,060 --> 00:12:24,200
 on their actions and so on.

141
00:12:24,200 --> 00:12:31,880
 But my point is, what benefit comes from seeing certain

142
00:12:31,880 --> 00:12:37,200
 individuals as special? Why is that a good thing? What's so

143
00:12:37,200 --> 00:12:39,200
 good about that?

144
00:12:39,200 --> 00:12:46,390
 How is that better than, say, having a friendly attitude

145
00:12:46,390 --> 00:12:53,590
 towards all beings? That's the greatness that we stress and

146
00:12:53,590 --> 00:12:56,200
 we focus on in Buddhism.

147
00:12:56,200 --> 00:13:04,240
 That's the greatness of universal friendliness, universal

148
00:13:04,240 --> 00:13:12,200
 appreciation and cordiality and kindness, compassion.

149
00:13:12,200 --> 00:13:19,490
 I don't want to say caring, but it looks a lot like caring.

150
00:13:19,490 --> 00:13:27,200
 Kindness is a little more innocuous, a little more proper.

151
00:13:27,200 --> 00:13:32,610
 Imagine if the whole world was family, if you just saw

152
00:13:32,610 --> 00:13:36,200
 everyone as you see your family.

153
00:13:36,200 --> 00:13:45,690
 People with flaws, individuals with flaws, but worthy of

154
00:13:45,690 --> 00:13:53,560
 appreciation and respect, respecting that they too have the

155
00:13:53,560 --> 00:13:59,200
 potential to better themselves if given the opportunity.

156
00:13:59,200 --> 00:14:06,760
 The world is not very equitable at the moment. If you look

157
00:14:06,760 --> 00:14:14,770
 in rich societies like Canada, America, China, there's a

158
00:14:14,770 --> 00:14:17,200
 great inequity in those rich countries.

159
00:14:17,200 --> 00:14:22,370
 But there are also very, very poor countries where there

160
00:14:22,370 --> 00:14:27,740
 may be a few rich, corrupt politicians, but the majority of

161
00:14:27,740 --> 00:14:32,520
 people live in what we would consider to be abject poverty,

162
00:14:32,520 --> 00:14:41,410
 without running water, without medicine, often without food

163
00:14:41,410 --> 00:14:42,200
.

164
00:14:42,200 --> 00:14:49,980
 I guess this occurs in rich countries, but we see in the

165
00:14:49,980 --> 00:14:57,920
 world there are places where the norm is to live in abject

166
00:14:57,920 --> 00:15:00,200
 poverty.

167
00:15:00,200 --> 00:15:11,210
 My point being that we do nothing. We look after our own.

168
00:15:11,210 --> 00:15:16,200
 Canada is my family. What a great nation.

169
00:15:16,200 --> 00:15:25,390
 I was thinking today about someone who had been accused of

170
00:15:25,390 --> 00:15:35,060
 murder. I was a Nazi in Germany who just ran away from a

171
00:15:35,060 --> 00:15:45,200
 trial and they've been charged with 8,000 counts of murder.

172
00:15:45,200 --> 00:15:58,980
 Complicity. Being complicit. Complicency? I don't know the

173
00:15:58,980 --> 00:16:04,200
 word. Complicity. To murder.

174
00:16:04,200 --> 00:16:08,200
 Certainly it sounds like a terrible thing that they were

175
00:16:08,200 --> 00:16:12,270
 involved in. It was a terrible thing that it sounds like

176
00:16:12,270 --> 00:16:14,200
 they were involved in.

177
00:16:14,200 --> 00:16:21,100
 But it's interesting how we call that murder, which it was.

178
00:16:21,100 --> 00:16:28,200
 And yet, when we go to war, war is never considered murder.

179
00:16:28,200 --> 00:16:32,200
 This isn't at all to trivialize Nazi Germany.

180
00:16:32,200 --> 00:16:37,700
 It's the Holocaust. A terrible thing like the killing

181
00:16:37,700 --> 00:16:43,530
 fields in Cambodia. We call it murder, but we kill people

182
00:16:43,530 --> 00:16:45,200
 all the time.

183
00:16:45,200 --> 00:16:50,950
 Why do soldiers kill people? Why isn't a soldier convicted

184
00:16:50,950 --> 00:16:56,780
 of murder? It's a ridiculous question. It does bring up an

185
00:16:56,780 --> 00:17:02,070
 interesting point that we don't see the rest of the world

186
00:17:02,070 --> 00:17:04,200
 as actual beings.

187
00:17:04,200 --> 00:17:10,210
 It's on the same level as our country. It's implicit in a

188
00:17:10,210 --> 00:17:16,700
 lot of things. We are kind often to take in refugees. Some

189
00:17:16,700 --> 00:17:20,200
 of the monks here are refugees.

190
00:17:20,200 --> 00:17:26,380
 When it comes right down to it, if you're not a Canadian

191
00:17:26,380 --> 00:17:35,030
 citizen, you're not one of us. Just an example of how the

192
00:17:35,030 --> 00:17:40,960
 world does do itself a disservice to think in terms of us

193
00:17:40,960 --> 00:17:42,200
 and them.

194
00:17:42,200 --> 00:17:48,680
 Just an example of greed not being all that useful. I got

195
00:17:48,680 --> 00:17:54,310
 quite a far field, but I was making a point that this is

196
00:17:54,310 --> 00:17:59,400
 based on greed. It's based on partiality, which requires

197
00:17:59,400 --> 00:18:00,200
 greed.

198
00:18:00,200 --> 00:18:05,800
 The only way for us, and this goes for so many things, but

199
00:18:05,800 --> 00:18:11,290
 the only way for us to overcome this us and them mentality

200
00:18:11,290 --> 00:18:13,200
 is to give up greed.

201
00:18:13,200 --> 00:18:19,690
 Because you can't talk yourself into loving all beings. It

202
00:18:19,690 --> 00:18:25,000
's a mistake to try to force yourself to be kind, to

203
00:18:25,000 --> 00:18:31,200
 practice Metta meditation as Buddhists often want to do

204
00:18:31,200 --> 00:18:34,200
 without purifying yourself from greed because it's fake.

205
00:18:34,200 --> 00:18:39,940
 It's artificial. It's temporary. It's not a sustainable

206
00:18:39,940 --> 00:18:43,780
 solution. Someone can practice Metta and be very loving and

207
00:18:43,780 --> 00:18:49,820
 kind, but until they can give up this partiality, they're

208
00:18:49,820 --> 00:18:55,000
 always going to prefer and be kinder to certain people than

209
00:18:55,000 --> 00:18:56,200
 others.

210
00:18:56,200 --> 00:19:05,200
 We haven't even got into talking about anger yet.

211
00:19:05,200 --> 00:19:11,390
 But giving up greed is incredibly powerful. It's just one

212
00:19:11,390 --> 00:19:13,200
 example of it.

213
00:19:13,200 --> 00:19:16,520
 If we gave up greed, the environment would benefit. If we

214
00:19:16,520 --> 00:19:20,440
 gave up greed, our political system, what would it look

215
00:19:20,440 --> 00:19:24,200
 like if politicians were not so corrupt and greedy?

216
00:19:24,200 --> 00:19:29,120
 What would it look like if we weren't so greedy when we

217
00:19:29,120 --> 00:19:34,200
 voted and were more thoughtful and conscientious?

218
00:19:34,200 --> 00:19:37,490
 What would our economic system look like if we were not so

219
00:19:37,490 --> 00:19:38,200
 greedy?

220
00:19:38,200 --> 00:19:42,210
 But most importantly, what would our mind be like? Imagine.

221
00:19:42,210 --> 00:19:44,200
 No preference.

222
00:19:44,200 --> 00:19:52,130
 Imagine being able to experience your life, your reality

223
00:19:52,130 --> 00:19:58,200
 without judgment, without partiality.

224
00:19:58,200 --> 00:20:04,080
 The question, why is greed a bad thing, can only come from

225
00:20:04,080 --> 00:20:06,200
 a place of ignorance.

226
00:20:06,200 --> 00:20:12,890
 If you are wondering what is wrong with greed and you just

227
00:20:12,890 --> 00:20:20,200
 can't see it, I tell you it's because the mind is unclear.

228
00:20:20,200 --> 00:20:28,830
 There's no explanation I can give you to explain to you why

229
00:20:28,830 --> 00:20:37,200
 greed is not on the same level of asking you to spend time

230
00:20:37,200 --> 00:20:39,200
 present with your experiences,

231
00:20:39,200 --> 00:20:45,090
 seeing them as they are, to cleanse your mind, to clear up

232
00:20:45,090 --> 00:20:50,480
 this fog of delusion and anger and greed to the point where

233
00:20:50,480 --> 00:20:55,630
 you see how much stress it's causing, how it really limits

234
00:20:55,630 --> 00:20:58,200
 you as a being.

235
00:20:58,200 --> 00:21:01,710
 The more you cling, the more limited you become. Human

236
00:21:01,710 --> 00:21:04,200
 beings are a good example.

237
00:21:04,200 --> 00:21:08,160
 Apparently the story of why there are human beings, why

238
00:21:08,160 --> 00:21:13,410
 there are earthlings at all, is because we were these lumin

239
00:21:13,410 --> 00:21:16,200
ous beings in the outer space.

240
00:21:16,200 --> 00:21:24,060
 And then we saw this luminescent ball of whatever and got

241
00:21:24,060 --> 00:21:28,200
 attached to it and clung to it.

242
00:21:28,200 --> 00:21:32,140
 Clung and clung and evolved through our clinging and become

243
00:21:32,140 --> 00:21:39,930
 corrupted through our clinging and through our consumption

244
00:21:39,930 --> 00:21:45,540
 actually, through consuming the energy from the earth, the

245
00:21:45,540 --> 00:21:49,200
 thing that came to be called earth.

246
00:21:49,200 --> 00:21:55,840
 Our own beings became coarser, the earth became coarser and

247
00:21:55,840 --> 00:21:57,200
 coarser.

248
00:21:57,200 --> 00:22:01,210
 And here we are, here we are, and that's why we're stuck to

249
00:22:01,210 --> 00:22:02,200
 this rock.

250
00:22:02,200 --> 00:22:06,900
 So the short version of the story of why we're stuck to

251
00:22:06,900 --> 00:22:08,200
 this rock.

252
00:22:08,200 --> 00:22:11,360
 Anyway, that's not probably very convincing to people who

253
00:22:11,360 --> 00:22:14,450
 wouldn't believe such things, but it does present an

254
00:22:14,450 --> 00:22:16,200
 interesting perspective.

255
00:22:16,200 --> 00:22:20,160
 It certainly does fit with reality, the reality that when

256
00:22:20,160 --> 00:22:22,200
 we cling, we become less.

257
00:22:22,200 --> 00:22:27,200
 The more we desire, the less happiness we end up with.

258
00:22:27,200 --> 00:22:32,680
 Cling and cling and cling. And our realm of what is

259
00:22:32,680 --> 00:22:39,200
 acceptable to us shrinks the realm of what we strive for.

260
00:22:39,200 --> 00:22:51,200
 Our horizons become smaller.

261
00:22:51,200 --> 00:23:02,200
 Mirror. So that's greed.

262
00:23:02,200 --> 00:23:10,200
 Anger, anger we can see quite clearly.

263
00:23:10,200 --> 00:23:22,050
 They say that greed is hard to see the, hard to see the

264
00:23:22,050 --> 00:23:27,520
 problem with greed, the fault in greed, but it's slow to

265
00:23:27,520 --> 00:23:29,200
 change.

266
00:23:29,200 --> 00:23:42,070
 So it's bad, most, I mean, it's hard to get rid of, because

267
00:23:42,070 --> 00:23:47,200
 it's slow to change.

268
00:23:47,200 --> 00:23:55,420
 Anger is easier to see. It's more commonly faulted. When

269
00:23:55,420 --> 00:23:58,200
 someone is angry, you don't hear people cheering them on.

270
00:23:58,200 --> 00:24:01,200
 Not so much.

271
00:24:01,200 --> 00:24:06,690
 Anger makes you ugly. Makes you sick. Greed can make you

272
00:24:06,690 --> 00:24:10,390
 sick, as I said, but anger is a state of sickness and

273
00:24:10,390 --> 00:24:12,200
 illness really.

274
00:24:12,200 --> 00:24:18,880
 Your blood boils, they say. Not literally, but it does heat

275
00:24:18,880 --> 00:24:28,200
 up your body. Creates tension, stress, headaches.

276
00:24:28,200 --> 00:24:37,200
 Anger is related to cruelty, required for cruelty.

277
00:24:37,200 --> 00:24:43,380
 It's what allows us to go to war. Think of all that hatred.

278
00:24:43,380 --> 00:24:46,620
 Of course greed is a cause of war as well, but when it

279
00:24:46,620 --> 00:24:52,200
 comes down to the killing, you have to be cruel.

280
00:24:52,200 --> 00:24:57,890
 You have to ignore, repress your natural state of

281
00:24:57,890 --> 00:25:04,200
 appreciation of life, because we don't want to die.

282
00:25:04,200 --> 00:25:09,170
 We think of life as a positive, a happy thing, and we have

283
00:25:09,170 --> 00:25:14,510
 to intentionally, knowingly take that happiness away from

284
00:25:14,510 --> 00:25:15,200
 others.

285
00:25:15,200 --> 00:25:22,200
 When we kill, when we hurt, when we harm others.

286
00:25:22,200 --> 00:25:29,080
 There are the more subtle versions of anger as well. Why is

287
00:25:29,080 --> 00:25:37,160
 anger bad is not such a hard question to answer, but on a

288
00:25:37,160 --> 00:25:40,200
 subtle level.

289
00:25:40,200 --> 00:25:55,200
 Our anger can be directed even at ourselves.

290
00:25:55,200 --> 00:26:06,050
 We have self-hatred, but ultimately it's simply the hatred

291
00:26:06,050 --> 00:26:10,200
 of our experiences.

292
00:26:10,200 --> 00:26:18,010
 We can have anger towards pain, anger towards the past,

293
00:26:18,010 --> 00:26:21,200
 towards the future.

294
00:26:21,200 --> 00:26:26,910
 Anger is easier to see the problem in, but it still gets

295
00:26:26,910 --> 00:26:37,320
 hidden with things like pain, even boredom, frustration,

296
00:26:37,320 --> 00:26:39,200
 and any time it becomes self-righteous.

297
00:26:39,200 --> 00:26:51,200
 You make me so angry. What's the problem? You. Right? What

298
00:26:51,200 --> 00:26:51,200
's the problem here? You make me so angry. What's the

299
00:26:51,200 --> 00:26:51,200
 problem? You are the problem.

300
00:26:51,200 --> 00:26:54,780
 Totally overlooked the fact that you're angry, that oh yeah

301
00:26:54,780 --> 00:26:58,200
, wait, if I weren't angry there would be no problem.

302
00:26:58,200 --> 00:27:07,200
 But you deserve it. I don't deserve that. How could you?

303
00:27:07,200 --> 00:27:12,200
 How dare you?

304
00:27:12,200 --> 00:27:19,560
 Our inability to accept reality. It's not necessary to get

305
00:27:19,560 --> 00:27:26,620
 angry, to strive for justice, for example, to fight for

306
00:27:26,620 --> 00:27:28,200
 justice.

307
00:27:28,200 --> 00:27:32,610
 There's no need to be angry with the Nazis in Germany. We

308
00:27:32,610 --> 00:27:35,200
 can be horrified.

309
00:27:35,200 --> 00:27:40,960
 I think that it's possible in some sense to be horrified,

310
00:27:40,960 --> 00:27:44,200
 shocked. Those aren't a big deal.

311
00:27:44,200 --> 00:27:49,260
 We should never be angry. We don't do any good by being

312
00:27:49,260 --> 00:27:50,200
 angry.

313
00:27:50,200 --> 00:27:55,200
 It's not better to be angry than to be, say, wise.

314
00:27:55,200 --> 00:28:00,880
 I mean, wisdom's not going to say, "Oh, it's okay. You

315
00:28:00,880 --> 00:28:05,510
 killed all those people. No problem." Wisdom is going to

316
00:28:05,510 --> 00:28:06,450
 say, "That was a very bad thing you did, just the same as

317
00:28:06,450 --> 00:28:07,200
 anger, with or without anger."

318
00:28:07,200 --> 00:28:10,250
 You don't need the anger to understand that it was a

319
00:28:10,250 --> 00:28:14,260
 horrible thing that the Nazis did in Germany, or the Khmer

320
00:28:14,260 --> 00:28:16,200
 Rouge did in Cambodia,

321
00:28:16,200 --> 00:28:21,600
 or any other of the many atrocities that what was done to

322
00:28:21,600 --> 00:28:27,720
 the First Nations people in Canada, the residential schools

323
00:28:27,720 --> 00:28:28,200
.

324
00:28:28,200 --> 00:28:32,970
 Today, I think, or if it's tomorrow, I'm not sure, it's the

325
00:28:32,970 --> 00:28:37,200
 day of truth and reconciliation in Canada.

326
00:28:37,200 --> 00:28:49,380
 Horrible things were done, not just out of anger, but anger

327
00:28:49,380 --> 00:28:55,200
's required to be so cruel.

328
00:28:55,200 --> 00:29:00,770
 And the third is delusion. Delusion is the worst of the

329
00:29:00,770 --> 00:29:06,010
 three, because it's both highly blamed and it's slow to

330
00:29:06,010 --> 00:29:07,200
 change.

331
00:29:07,200 --> 00:29:11,000
 Anger's quick to change, I didn't mention it. Highly blamed

332
00:29:11,000 --> 00:29:16,090
, quick to change. Delusion, highly blamed, slow to change,

333
00:29:16,090 --> 00:29:17,200
 it's the worst.

334
00:29:17,200 --> 00:29:24,880
 And you can't have greed or anger without delusion, because

335
00:29:24,880 --> 00:29:27,200
 they're wrong.

336
00:29:27,200 --> 00:29:29,610
 The only way they can arise is if you don't know that they

337
00:29:29,610 --> 00:29:31,940
're wrong, if you aren't clear in your mind that they're

338
00:29:31,940 --> 00:29:32,200
 wrong.

339
00:29:32,200 --> 00:29:37,770
 When they have an opportunity to arise, there has to be, at

340
00:29:37,770 --> 00:29:43,370
 that moment, some sort of delusion with a clear mind that

341
00:29:43,370 --> 00:29:44,200
 they would never arise.

342
00:29:44,200 --> 00:29:52,680
 And that's, of course, the great power of mindfulness, of "

343
00:29:52,680 --> 00:29:57,200
wibasana," seeing clearly.

344
00:29:57,200 --> 00:30:00,910
 And it's something that we have to remind meditators over

345
00:30:00,910 --> 00:30:07,200
 and over again. We're not trying to fix our problems.

346
00:30:07,200 --> 00:30:11,200
 We're trying to see them clearly.

347
00:30:11,200 --> 00:30:18,710
 It seems a bit disingenuous, because, of course, we're

348
00:30:18,710 --> 00:30:23,200
 trying to fix our problems.

349
00:30:23,200 --> 00:30:28,200
 In some sense. We're trying to fix something, anyway.

350
00:30:28,200 --> 00:30:32,640
 We're trying to fix something, but the way of fixing turns

351
00:30:32,640 --> 00:30:36,200
 out to be very different from what we think.

352
00:30:36,200 --> 00:30:41,200
 Our idea of fixing is doing something to fix.

353
00:30:41,200 --> 00:30:47,650
 When the real fix is the change in perspective, the clarity

354
00:30:47,650 --> 00:30:54,200
, that the true fix is freedom from delusion.

355
00:30:54,200 --> 00:31:00,720
 Let's go through it. What are some of the big delusions out

356
00:31:00,720 --> 00:31:03,200
 there in the world?

357
00:31:03,200 --> 00:31:16,200
 Racism? Bigotry? Xenophobia?

358
00:31:16,200 --> 00:31:19,200
 Religion. Religion is a huge delusion.

359
00:31:19,200 --> 00:31:26,200
 Think of Israel, the situation in the Middle East.

360
00:31:26,200 --> 00:31:32,720
 There's this place in Jerusalem that they've been fighting

361
00:31:32,720 --> 00:31:36,200
 over for thousands of years.

362
00:31:36,200 --> 00:31:39,920
 I mean, they've at least been fighting over for decades now

363
00:31:39,920 --> 00:31:40,200
.

364
00:31:40,200 --> 00:31:46,210
 But the whole fight is over some ridiculous notion that it

365
00:31:46,210 --> 00:31:52,200
's unfathomable how much delusion is involved there.

366
00:31:52,200 --> 00:31:55,200
 There's, of course, greed and anger, of course.

367
00:31:55,200 --> 00:32:01,930
 Anger, bitterness, us and them, the enemy, the dehuman

368
00:32:01,930 --> 00:32:08,200
ization of other humans, other beings.

369
00:32:08,200 --> 00:32:10,200
 We don't get so much delusion involved.

370
00:32:10,200 --> 00:32:12,200
 Those are the big ones, we can see those.

371
00:32:12,200 --> 00:32:14,930
 Arrogance, on a personal level, arrogance. Who likes an

372
00:32:14,930 --> 00:32:16,200
 arrogant person?

373
00:32:16,200 --> 00:32:18,200
 Conceit.

374
00:32:18,200 --> 00:32:22,200
 Self-righteousness.

375
00:32:22,200 --> 00:32:24,200
 People who are preachy.

376
00:32:24,200 --> 00:32:29,200
 Buddhists can be very preachy sometimes.

377
00:32:29,200 --> 00:32:33,420
 Just because you say you're Buddhist or become Buddhist

378
00:32:33,420 --> 00:32:37,430
 doesn't mean you're going to be free from these kind of

379
00:32:37,430 --> 00:32:44,200
 arrogance and conceit and self-righteousness.

380
00:32:44,200 --> 00:32:50,300
 A good example of a good opportunity for a reminder that we

381
00:32:50,300 --> 00:32:56,810
 should be thoughtful and respectful to our fellow Buddhists

382
00:32:56,810 --> 00:33:00,200
, that they won't always be perfect.

383
00:33:00,200 --> 00:33:02,200
 And we shouldn't overlook the good side.

384
00:33:02,200 --> 00:33:09,560
 The Buddha said, "Put aside the bad side. Try and focus on

385
00:33:09,560 --> 00:33:15,200
 the good in people and help nourish that."

386
00:33:15,200 --> 00:33:18,200
 But on a personal level, we can see this in other people.

387
00:33:18,200 --> 00:33:20,870
 We can see it in ourselves, especially when we start to

388
00:33:20,870 --> 00:33:22,200
 practice meditation.

389
00:33:22,200 --> 00:33:24,200
 It's ugly.

390
00:33:24,200 --> 00:33:27,200
 Conceit, arrogance.

391
00:33:27,200 --> 00:33:31,720
 But the worst or the most ultimate form of delusion is

392
00:33:31,720 --> 00:33:36,200
 quite simple. It's just ignorance.

393
00:33:36,200 --> 00:33:40,200
 Awijya, Pachaya, Sankara.

394
00:33:40,200 --> 00:33:47,200
 All of our inclinations, good or bad, come from ignorance.

395
00:33:47,200 --> 00:33:52,200
 There's nothing we need to do besides clearing this up.

396
00:33:52,200 --> 00:33:55,570
 We spend all our time in meditation doing something very

397
00:33:55,570 --> 00:33:58,200
 simple, and it's so easy to misunderstand, to overlook it.

398
00:33:58,200 --> 00:34:03,230
 It's so hard to really get what we're doing, not because it

399
00:34:03,230 --> 00:34:06,690
's something deep or abstruse or complicated, because it's

400
00:34:06,690 --> 00:34:07,200
 so simple.

401
00:34:07,200 --> 00:34:09,200
 You miss it.

402
00:34:09,200 --> 00:34:12,200
 When you're walking, what are you trying to do?

403
00:34:12,200 --> 00:34:14,200
 You're trying to know that your foot is raising.

404
00:34:14,200 --> 00:34:19,640
 But what else? What's behind that? What's the point of that

405
00:34:19,640 --> 00:34:20,200
?

406
00:34:20,200 --> 00:34:26,200
 Honestly, the point is to give up asking what is the point.

407
00:34:26,200 --> 00:34:29,330
 Because as long as you try to find a point in things, you

408
00:34:29,330 --> 00:34:31,200
're making more of them than they actually are.

409
00:34:31,200 --> 00:34:34,200
 That's the whole point. That's it.

410
00:34:34,200 --> 00:34:37,560
 Once you can see, seeing is just seeing, hearing is just

411
00:34:37,560 --> 00:34:40,200
 hearing, then you will see clearly.

412
00:34:40,200 --> 00:34:45,490
 Then you will be present. Then you will free yourself from

413
00:34:45,490 --> 00:34:48,940
 all of this greed, anger and delusion stuff it's caught up

414
00:34:48,940 --> 00:34:49,200
 in.

415
00:34:49,200 --> 00:34:53,200
 In ignorance.

416
00:34:53,200 --> 00:34:57,640
 It comes because you don't see, because rather than having

417
00:34:57,640 --> 00:35:00,690
 sati, meaning remembering what you're experiencing, being

418
00:35:00,690 --> 00:35:05,200
 with what you experience, you make it into something else.

419
00:35:05,200 --> 00:35:11,200
 You get lost in conceptualization.

420
00:35:11,200 --> 00:35:18,200
 So, as I've said before, reality is quite simple.

421
00:35:18,200 --> 00:35:23,530
 Understanding it is not an easy thing, but it is a fairly

422
00:35:23,530 --> 00:35:25,200
 simple thing.

423
00:35:25,200 --> 00:35:29,200
 There's not much in the world that is a problem.

424
00:35:29,200 --> 00:35:32,470
 If we can focus on these three things and appreciate them,

425
00:35:32,470 --> 00:35:35,200
 and most importantly,

426
00:35:35,200 --> 00:35:39,910
 try to see our experiences clearly as they are, freeing

427
00:35:39,910 --> 00:35:42,200
 ourselves from ignorance.

428
00:35:42,200 --> 00:35:48,470
 The whole chain of causation leading us to sorrow, sadness,

429
00:35:48,470 --> 00:35:54,200
 lamentation, despair, suffering.

430
00:35:54,200 --> 00:35:58,200
 That whole chain of causality is broken.

431
00:35:58,200 --> 00:36:03,200
 And experience is just experience.

432
00:36:03,200 --> 00:36:06,200
 So that's the demo for tonight.

