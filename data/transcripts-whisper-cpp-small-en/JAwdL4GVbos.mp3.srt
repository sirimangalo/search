1
00:00:00,000 --> 00:00:04,240
 Hey everyone, this is Adder, a volunteer with Siri Mangalo

2
00:00:04,240 --> 00:00:06,000
 International.

3
00:00:06,000 --> 00:00:10,320
 I'm here today to give a brief update on our fundraising

4
00:00:10,320 --> 00:00:11,000
 project.

5
00:00:11,000 --> 00:00:15,080
 A week ago we started our fundraiser for our meditation

6
00:00:15,080 --> 00:00:16,000
 center project

7
00:00:16,000 --> 00:00:20,070
 in hopes that we can, with enough support from our

8
00:00:20,070 --> 00:00:21,000
 community,

9
00:00:21,000 --> 00:00:26,320
 establish a permanent home of our own for our meditation

10
00:00:26,320 --> 00:00:27,000
 practice

11
00:00:27,000 --> 00:00:31,330
 and serve more meditators rather than operating out of a

12
00:00:31,330 --> 00:00:32,000
 rented space

13
00:00:32,000 --> 00:00:35,000
 with a relatively low capacity.

14
00:00:35,000 --> 00:00:40,500
 And in this first week it's been amazing to see all of the

15
00:00:40,500 --> 00:00:44,000
 support and excitement that our community has.

16
00:00:44,000 --> 00:00:48,000
 We've raised $14,000 in those first seven days.

17
00:00:48,000 --> 00:00:54,000
 And I want to take a moment to share my thanks to everyone

18
00:00:54,000 --> 00:00:55,000
 in our community

19
00:00:55,000 --> 00:00:58,620
 and all those who have offered their support monetarily or

20
00:00:58,620 --> 00:01:01,000
 through their volunteer efforts

21
00:01:01,000 --> 00:01:06,000
 or through sharing our fundraiser with others.

22
00:01:06,000 --> 00:01:11,390
 We don't see this project as just something that a few of

23
00:01:11,390 --> 00:01:12,000
 us are working on

24
00:01:12,000 --> 00:01:16,000
 and sort of putting out there to collect stuff back.

25
00:01:16,000 --> 00:01:20,110
 It's really, we want this to be a community event, a

26
00:01:20,110 --> 00:01:22,000
 community project.

27
00:01:22,000 --> 00:01:25,370
 So if there's any way that you're excited to contribute and

28
00:01:25,370 --> 00:01:28,000
 be a part of this, let us know.

29
00:01:28,000 --> 00:01:32,000
 We have some people running their own mini fundraisers

30
00:01:32,000 --> 00:01:37,000
 or sharing this with their communities in their own way.

31
00:01:37,000 --> 00:01:40,340
 If there's any way that you want to contribute or if you

32
00:01:40,340 --> 00:01:43,000
 have any ideas of things that we could be doing,

33
00:01:43,000 --> 00:01:46,000
 reach out to us. Let us know.

34
00:01:46,000 --> 00:01:52,000
 We want to support you and we'd gladly accept any support

35
00:01:52,000 --> 00:01:56,180
 that you want to give to the rest of the Sierra Mongolo

36
00:01:56,180 --> 00:01:57,000
 community.

37
00:01:57,000 --> 00:02:02,000
 Our GoFundMe page has a comments feature.

38
00:02:02,000 --> 00:02:06,640
 And I'd like to ask people if you feel so inclined to post

39
00:02:06,640 --> 00:02:08,000
 a brief comment

40
00:02:08,000 --> 00:02:12,870
 sharing something about your practice or about what this

41
00:02:12,870 --> 00:02:15,000
 project means to you.

42
00:02:15,000 --> 00:02:19,000
 Perhaps you have a dedication you want to make or a message

43
00:02:19,000 --> 00:02:22,000
 you want to send to the Sierra Mongolo community.

44
00:02:22,000 --> 00:02:27,660
 And we also will see more support the further reach that

45
00:02:27,660 --> 00:02:30,000
 this fundraiser has.

46
00:02:30,000 --> 00:02:34,700
 So if you want to share this with the communities that you

47
00:02:34,700 --> 00:02:37,000
're involved in outside of Sierra Mongolo,

48
00:02:37,000 --> 00:02:42,000
 that would be a great blessing.

49
00:02:42,000 --> 00:02:46,940
 Maybe that's your real life community, maybe that's your

50
00:02:46,940 --> 00:02:48,000
 friends and family

51
00:02:48,000 --> 00:02:51,000
 or a local sangha that you're involved in.

52
00:02:51,000 --> 00:02:55,000
 But for many of us that's an online community.

53
00:02:55,000 --> 00:02:58,090
 If there's a subreddit that you're involved in or a

54
00:02:58,090 --> 00:02:59,000
 Facebook group,

55
00:02:59,000 --> 00:03:05,000
 these are great opportunities to share what we're doing.

56
00:03:05,000 --> 00:03:11,580
 We'll also be doing a testimonial series, a series of

57
00:03:11,580 --> 00:03:14,000
 videos in which people share their own story

58
00:03:14,000 --> 00:03:18,800
 about their meditation practice and their experience with

59
00:03:18,800 --> 00:03:20,000
 the Dhamma.

60
00:03:20,000 --> 00:03:22,880
 So we'll come up with a list of questions that people can

61
00:03:22,880 --> 00:03:24,000
 use as guidelines.

62
00:03:24,000 --> 00:03:28,460
 But really we're just looking for a wide collection of

63
00:03:28,460 --> 00:03:31,000
 people's individual stories.

64
00:03:31,000 --> 00:03:34,710
 And so if you'd like to share yours, the rest of us would

65
00:03:34,710 --> 00:03:36,000
 love to hear it.

66
00:03:36,000 --> 00:03:39,000
 So you can feel free to record a video on your own.

67
00:03:39,000 --> 00:03:43,990
 We could even do a virtual interview or you could arrange a

68
00:03:43,990 --> 00:03:47,000
 live interview with someone.

69
00:03:47,000 --> 00:03:51,530
 You could briefly share a few words or tell us in more

70
00:03:51,530 --> 00:03:54,000
 detail about your journey.

71
00:03:54,000 --> 00:03:58,000
 So if you feel like sharing, we would love to hear that.

72
00:03:58,000 --> 00:04:03,830
 And we will show those as part of our fundraising series of

73
00:04:03,830 --> 00:04:05,000
 videos.

74
00:04:05,000 --> 00:04:09,000
 So please stay in touch.

75
00:04:09,000 --> 00:04:12,250
 The rest of us will try to make sure to stay in touch with

76
00:04:12,250 --> 00:04:14,000
 the community at large.

77
00:04:14,000 --> 00:04:17,880
 We hope to keep you updated as we continue this fundraiser

78
00:04:17,880 --> 00:04:20,000
 over the next three months.

79
00:04:20,000 --> 00:04:24,000
 So I look forward to staying in touch with you all.

80
00:04:24,000 --> 00:04:29,000
 And I can't wait to see what happens next.

81
00:04:29,000 --> 00:04:33,000
 Thank you all. May you be well.

