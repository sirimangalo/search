1
00:00:00,000 --> 00:00:07,750
 These four are said to be not born of anything, not caused

2
00:00:07,750 --> 00:00:11,000
 by anything. Why? Because there is no arising of arising.

3
00:00:11,000 --> 00:00:15,670
 And the other two are the mere maturing and breaking up of

4
00:00:15,670 --> 00:00:17,000
 what has arisen.

5
00:00:17,000 --> 00:00:22,050
 That means they are just the different phases of the

6
00:00:22,050 --> 00:00:26,000
 material properties which have arisen.

7
00:00:26,000 --> 00:00:30,620
 They are not, actually they are not separate material

8
00:00:30,620 --> 00:00:36,000
 properties. So they are not said to be caused by anything.

9
00:00:36,000 --> 00:00:43,240
 Anything means any karma or consciousness or temperature or

10
00:00:43,240 --> 00:00:45,000
 nutriment.

11
00:00:45,000 --> 00:00:49,880
 But there is a passage which says the visible database, the

12
00:00:49,880 --> 00:00:54,850
 sound base, the older base, the flavor base, the tangible

13
00:00:54,850 --> 00:00:57,000
 database, the space element, the water element,

14
00:00:57,000 --> 00:01:00,220
 lightness of matter, malleability of matter, wilderness of

15
00:01:00,220 --> 00:01:03,990
 matter, growth of matter, continuity of matter, these two,

16
00:01:03,990 --> 00:01:06,000
 and physical food.

17
00:01:06,000 --> 00:01:10,180
 These states are consciousness originated. So this is from

18
00:01:10,180 --> 00:01:12,000
 the Abhidhamma text.

19
00:01:12,000 --> 00:01:14,850
 So in Abhidhamma, it says that growth of matter and

20
00:01:14,850 --> 00:01:18,390
 continuity of matter are caused by, or originated by

21
00:01:18,390 --> 00:01:20,000
 consciousness.

22
00:01:20,000 --> 00:01:25,770
 And likewise, there are passages where they are said to be

23
00:01:25,770 --> 00:01:30,000
 caused by other causes like karma and so on.

24
00:01:30,000 --> 00:01:37,570
 So how about that? And the answer is, actually they are not

25
00:01:37,570 --> 00:01:39,000
 caused by anything.

26
00:01:39,000 --> 00:02:03,000
 But they are evident when the functions of karma and so on

27
00:02:03,000 --> 00:02:08,000
 are still said to be existing.

28
00:02:08,000 --> 00:02:15,500
 Now, suppose there is a relinking or rebirth. At the first

29
00:02:15,500 --> 00:02:22,000
 sub-movement of rebirth, there is a karma-borne matter.

30
00:02:22,000 --> 00:02:27,990
 Now, karma is always concerned about producing something.

31
00:02:27,990 --> 00:02:35,030
 Only after it has produced something will it give up this

32
00:02:35,030 --> 00:02:36,000
 concern.

33
00:02:36,000 --> 00:02:41,980
 So it is still concerned about producing results. So at the

34
00:02:41,980 --> 00:02:50,980
 very moment of arising, its concern has not yet disappeared

35
00:02:50,980 --> 00:02:51,000
.

36
00:02:51,000 --> 00:02:57,750
 But for these two, growth of matter and continuity of

37
00:02:57,750 --> 00:03:02,000
 matter are seen at that moment.

38
00:03:02,000 --> 00:03:06,890
 So that is why they are said to be caused by karma, jeta,

39
00:03:06,890 --> 00:03:12,490
 and so on. Although, in fact, they are not caused by

40
00:03:12,490 --> 00:03:14,000
 anything.

41
00:03:14,000 --> 00:03:22,120
 They are certainly caused by karma because the exercising

42
00:03:22,120 --> 00:03:28,000
 of the function of karma is still there.

43
00:03:28,000 --> 00:03:34,110
 Karma's concern for producing material property is still

44
00:03:34,110 --> 00:03:38,770
 exercising at the moment of genesis or at the moment of

45
00:03:38,770 --> 00:03:40,000
 arising.

46
00:03:40,000 --> 00:03:49,030
 So that is why they are said to be caused by karma and jeta

47
00:03:49,030 --> 00:03:56,000
, with regard to jeta, the same thing.

48
00:03:56,000 --> 00:04:05,000
 Then we come to consciousness aggregate, or vinya-nakanda.

49
00:04:05,000 --> 00:04:08,700
 So among the remaining aggregates of whatever has the

50
00:04:08,700 --> 00:04:13,000
 characteristic of being felt, no, it is not correct.

51
00:04:13,000 --> 00:04:16,910
 Whatever has the characteristic of feeling should be

52
00:04:16,910 --> 00:04:20,000
 understood, all taken together as the feeling aggregate.

53
00:04:20,000 --> 00:04:29,520
 Not being felt, but feeling. He misunderstood the word. The

54
00:04:29,520 --> 00:04:33,000
 Pali word is vaidhihita.

55
00:04:33,000 --> 00:04:41,320
 And it looks like a passive past participle, the word. But

56
00:04:41,320 --> 00:04:44,000
 it is not passive past participle here.

57
00:04:44,000 --> 00:04:51,410
 It is something like a verbal noun. Not that which is felt,

58
00:04:51,410 --> 00:04:57,000
 but that which feels, or just feeling.

59
00:04:57,000 --> 00:05:04,000
 So here the word vaidhihita means feeling, not being felt.

60
00:05:04,000 --> 00:05:09,670
 If you know a little Pali, you will understand that. The

61
00:05:09,670 --> 00:05:16,870
 suffix -ta, T-A, the suffix -ta is added to the root, to

62
00:05:16,870 --> 00:05:23,000
 mean passive.

63
00:05:23,000 --> 00:05:33,000
 So here vaidhihita, so it could mean which is felt.

64
00:05:33,000 --> 00:05:39,030
 But here it is used in the sense of just feeling, not being

65
00:05:39,030 --> 00:05:40,000
 felt.

66
00:05:40,000 --> 00:05:46,000
 So the characteristic of feeling should be understood, all

67
00:05:46,000 --> 00:05:50,000
 taken together as the feeling aggregate.

68
00:05:50,000 --> 00:05:54,430
 Because feeling aggregate is that which has a feeling as

69
00:05:54,430 --> 00:06:00,000
 characteristic, and that which is not felt.

70
00:06:00,000 --> 00:06:06,000
 If it is felt, then there must be another feeling.

71
00:06:06,000 --> 00:06:10,850
 And feeling here is explained in the sub-commentary as

72
00:06:10,850 --> 00:06:14,000
 experiencing the taste of object.

73
00:06:14,000 --> 00:06:20,000
 So that is what we call feeling.

74
00:06:20,000 --> 00:06:28,000
 We can even say enjoying the taste of object.

75
00:06:28,000 --> 00:06:32,000
 And now the aggregate of consciousness.

76
00:06:32,000 --> 00:06:37,730
 So aggregate of consciousness is described in this book

77
00:06:37,730 --> 00:06:44,000
 following the order given in the original Abhidhamma text.

78
00:06:44,000 --> 00:06:49,490
 And this order is different from the order we are familiar

79
00:06:49,490 --> 00:06:50,000
 with.

80
00:06:50,000 --> 00:06:56,350
 Because we are familiar with the order given in the manual

81
00:06:56,350 --> 00:06:58,000
 of Abhidhamma.

82
00:06:58,000 --> 00:07:01,960
 And the order given in manual of Abhidhamma is different

83
00:07:01,960 --> 00:07:06,000
 from the order given in the original Abhidhamma text.

84
00:07:06,000 --> 00:07:27,060
 And the Risudu mother follows the order given in the

85
00:07:27,060 --> 00:07:32,000
 original Abhidhamma text.

86
00:07:32,000 --> 00:08:00,000
 So the number given in the book is different from the

87
00:08:00,000 --> 00:08:00,000
 number given in the manual of Abhidhamma.

88
00:08:00,000 --> 00:08:12,000
 Let us say if you read the paragraph 83.

89
00:08:12,000 --> 00:08:16,960
 Herein that of the sense fear is hateful, being classified

90
00:08:16,960 --> 00:08:20,470
 according to joy, equanimity, knowledge, and prompting that

91
00:08:20,470 --> 00:08:22,000
 is to say one.

92
00:08:22,000 --> 00:08:24,000
 When accompanied by joy, I told you.

93
00:08:24,000 --> 00:08:34,210
 This number one given in this book is number 31 in this

94
00:08:34,210 --> 00:08:36,000
 chart.

95
00:08:36,000 --> 00:08:40,000
 Now you can identify it.

96
00:08:40,000 --> 00:08:47,480
 So now number two given in the path of purification is what

97
00:08:47,480 --> 00:08:56,000
? Number 32 in this.

98
00:08:56,000 --> 00:09:05,000
 So it can help you to identify.

99
00:09:05,000 --> 00:09:13,650
 So the plan of the types of consciousness given in the path

100
00:09:13,650 --> 00:09:21,220
 of purification is, as I said, according to the text, the

101
00:09:21,220 --> 00:09:23,000
 original text of Abhidhamma.

102
00:09:23,000 --> 00:09:29,000
 And so they are classified in this way, the last sheet.

103
00:09:29,000 --> 00:09:35,000
 So first, kusala. Second, akusala.

104
00:09:35,000 --> 00:09:39,500
 And then third, abhyakada. So first, these three major

105
00:09:39,500 --> 00:09:45,500
 divisions, kusala, wholesome, akusala, unwholesome, and ab

106
00:09:45,500 --> 00:09:48,000
hyakada, indeterminate.

107
00:09:48,000 --> 00:09:52,400
 And then kusala is divided into kama vajra, belonging to

108
00:09:52,400 --> 00:09:57,250
 sense fear, ruba vajra, belonging to material fear, aruba v

109
00:09:57,250 --> 00:10:01,710
ajra, belonging to in material fear, and lokutra, supermande

110
00:10:01,710 --> 00:10:02,000
.

111
00:10:02,000 --> 00:10:07,240
 Then akusala is divided into three, lobamula, that is

112
00:10:07,240 --> 00:10:13,280
 accompanied by loba attachment, dosaamula, accompanied by d

113
00:10:13,280 --> 00:10:18,000
osa, or having dosa as a root, and mohamula.

114
00:10:18,000 --> 00:10:23,440
 You can find the English translation in the path of pur

115
00:10:23,440 --> 00:10:25,000
ification.

116
00:10:25,000 --> 00:10:29,980
 And I give the paragraph numbers so you can find them

117
00:10:29,980 --> 00:10:32,000
 easily in the book.

118
00:10:32,000 --> 00:10:38,000
 And then abhyakada, abhyakada is the most comprehensive.

119
00:10:38,000 --> 00:10:43,320
 And abhyakada is first divided into vipaka and then down k

120
00:10:43,320 --> 00:10:44,000
irya.

121
00:10:44,000 --> 00:10:51,700
 So vipaka are the resultant and kirya functioning. Then vip

122
00:10:51,700 --> 00:10:56,540
aka is subdivided into kama vajra vipaka, and then going

123
00:10:56,540 --> 00:11:02,000
 down, ruba vajra vipaka, aruba vajra vipaka, lokutra vipaka

124
00:11:02,000 --> 00:11:02,000
.

125
00:11:02,000 --> 00:11:07,360
 And then kama vajra vipaka is divided into kusala vipaka, a

126
00:11:07,360 --> 00:11:09,000
kusala vipaka.

127
00:11:09,000 --> 00:11:13,340
 And then kusala vipaka is divided into ahi tuka, without

128
00:11:13,340 --> 00:11:16,000
 root, and sahi tuka with root.

129
00:11:16,000 --> 00:11:18,000
 So this is the plan.

130
00:11:18,000 --> 00:11:24,320
 And then kirya is divided into kama vajra, ruba vajra, ar

131
00:11:24,320 --> 00:11:26,000
uba vajra.

132
00:11:26,000 --> 00:11:31,080
 And then kama vajra kirya is divided into ahi tuka and sahi

133
00:11:31,080 --> 00:11:32,000
 tuka.

134
00:11:32,000 --> 00:11:41,000
 So this is the order given in the Path of Purification.

135
00:11:41,000 --> 00:11:45,600
 So there are altogether 89 types of consciousness explained

136
00:11:45,600 --> 00:11:47,000
 in this chapter.

137
00:11:47,000 --> 00:11:52,690
 Not 121, but 89 and 121 are to say it because the eight

138
00:11:52,690 --> 00:11:56,000
 superman, the eight types of consciousness,

139
00:11:56,000 --> 00:11:58,330
 when we multiply the eight superman, the eight types of

140
00:11:58,330 --> 00:12:04,000
 consciousness with five jhanas, we get 40 superman-day.

141
00:12:04,000 --> 00:12:09,000
 So the number of jhata's become 121.

142
00:12:09,000 --> 00:12:15,410
 If we take just, if we take superman-day consciousness as

143
00:12:15,410 --> 00:12:19,180
 just eight, just eight, then we get 89 types of

144
00:12:19,180 --> 00:12:20,000
 consciousness.

145
00:12:20,000 --> 00:12:26,980
 So these 89 types of consciousness are described in this

146
00:12:26,980 --> 00:12:28,000
 book.

147
00:12:28,000 --> 00:12:36,180
 Now, in the footnote on page 507, there's a discussion

148
00:12:36,180 --> 00:12:43,000
 about the words nama, vinyana, mano, cheeta, and cheeto.

149
00:12:43,000 --> 00:12:45,000
 These are synonyms.

150
00:12:45,000 --> 00:12:49,000
 Nama, vinyana, mano, cheeta, and cheeto.

151
00:12:49,000 --> 00:12:53,790
 But, say while their etymology can be looked up in the

152
00:12:53,790 --> 00:12:57,000
 dictionary, one or two points needs noting here.

153
00:12:57,000 --> 00:13:01,550
 Nama, rendered by mentality when not used to refer to a

154
00:13:01,550 --> 00:13:06,130
 name, is almost confined in the sense it is considered to

155
00:13:06,130 --> 00:13:08,000
 the expression nama,

156
00:13:08,000 --> 00:13:11,870
 ruba, mentality, materiality as a food member of the

157
00:13:11,870 --> 00:13:15,570
 dependent origination where it comprises the three mental

158
00:13:15,570 --> 00:13:17,000
 aggregates of feeling,

159
00:13:17,000 --> 00:13:23,000
 perceptions, and formations, but not that of consciousness.

160
00:13:23,000 --> 00:13:28,000
 I cannot agree with the word almost here.

161
00:13:28,000 --> 00:13:30,000
 It's almost confined.

162
00:13:30,000 --> 00:13:32,000
 Now, let's see.

163
00:13:32,000 --> 00:13:39,870
 Nama means, as it is said here, nama means three mental

164
00:13:39,870 --> 00:13:45,620
 aggregates only when it is used in the dependent orig

165
00:13:45,620 --> 00:13:47,000
ination.

166
00:13:47,000 --> 00:13:53,110
 So if it is used as a link in dependent origination, yes, n

167
00:13:53,110 --> 00:13:59,000
ama means feeling, perception, and formations.

168
00:13:59,000 --> 00:14:01,000
 Other places.

169
00:14:01,000 --> 00:14:06,870
 Nama means cheeta and cheetah seekers, both, not cheetah

170
00:14:06,870 --> 00:14:08,000
 seekers only.

171
00:14:08,000 --> 00:14:13,000
 So I do not like the word almost here.

172
00:14:13,000 --> 00:14:17,000
 Maybe it is sometimes confined, something like that.

173
00:14:17,000 --> 00:14:23,660
 Not almost, but sometimes, only when given as a link in the

174
00:14:23,660 --> 00:14:28,000
 dependent origination.

175
00:14:28,000 --> 00:14:51,000
 If you go to chapter 18 and paragraph 8,

176
00:14:51,000 --> 00:14:56,270
 now, the immaterial states, immaterial states just mean n

177
00:14:56,270 --> 00:14:57,000
ama.

178
00:14:57,000 --> 00:15:02,160
 And there, it is described as, that is to say, the 81 kinds

179
00:15:02,160 --> 00:15:07,620
 of mundane consciousness consisting of the two sets of five

180
00:15:07,620 --> 00:15:10,000
 consciousness and so on.

181
00:15:10,000 --> 00:15:20,000
 So, nama means both consciousness and mental factors.

182
00:15:20,000 --> 00:15:29,000
 But in another Abhidharma text, nama also includes nibbana.

183
00:15:29,000 --> 00:15:32,000
 So nibbana is also called nama.

184
00:15:32,000 --> 00:15:36,310
 Although it is not mental, nibbana is something different

185
00:15:36,310 --> 00:15:39,000
 from mentality and materiality.

186
00:15:39,000 --> 00:15:44,000
 But in Pali, it is called nama.

187
00:15:44,000 --> 00:15:52,000
 If you want to get confused, then just leave it alone.

188
00:15:52,000 --> 00:15:58,970
 So let us take that nama means, mostly means, consciousness

189
00:15:58,970 --> 00:16:01,000
 and mental factors together.

190
00:16:01,000 --> 00:16:06,350
 But in the dependent origination, as a full member of the

191
00:16:06,350 --> 00:16:09,000
 dependent origination,

192
00:16:09,000 --> 00:16:15,320
 then there is the word nama rupa and there nama means only

193
00:16:15,320 --> 00:16:18,000
 the mental factors.

194
00:16:18,000 --> 00:16:22,000
 You know why?

195
00:16:22,000 --> 00:16:26,000
 Because there is consciousness above it.

196
00:16:26,000 --> 00:16:31,000
 Depending on ignorance, there are mental formations.

197
00:16:31,000 --> 00:16:34,000
 Depending on mental formations, there is consciousness.

198
00:16:34,000 --> 00:16:38,000
 And depending on consciousness, there is nama and rupa.

199
00:16:38,000 --> 00:16:44,630
 So, when we say nama depends on consciousness, then nama

200
00:16:44,630 --> 00:16:47,000
 cannot include consciousness.

201
00:16:47,000 --> 00:16:53,000
 That is why nama is made to mean only feeling, perception

202
00:16:53,000 --> 00:16:57,000
 and mental formations in the dependent origination.

203
00:16:57,000 --> 00:17:03,150
 But outside the dependent origination, nama always means,

204
00:17:03,150 --> 00:17:07,000
 mind, I mean, jita and jita sequence together.

205
00:17:07,000 --> 00:17:15,000
 So, nama, vinyana, mano, jita and jita are all synonyms.

206
00:17:15,000 --> 00:17:17,000
 And they just mean jita.

207
00:17:17,000 --> 00:17:21,660
 So vinyana means jita, mano means jita, jita means jita and

208
00:17:21,660 --> 00:17:25,000
 jita, and sometimes also jita.

209
00:17:25,000 --> 00:17:36,000
 And then, footnote 36, the explanation of karma wajira.

210
00:17:36,000 --> 00:17:46,000
 Now, sense fear, here there are two kinds of sense desire.

211
00:17:46,000 --> 00:17:50,000
 Note that there are two kinds of sense desires.

212
00:17:50,000 --> 00:17:53,000
 Let us see. There are two kinds of karma.

213
00:17:53,000 --> 00:17:58,000
 Now, please note the word karma, the Pali waj.

214
00:17:58,000 --> 00:18:01,000
 There are two kinds of karma.

215
00:18:01,000 --> 00:18:07,410
 One is, karma is vatu-kama. You see the word vatu-kama

216
00:18:07,410 --> 00:18:08,000
 there?

217
00:18:08,000 --> 00:18:14,000
 So, one is vatu-kama and the other is kilesa-kama.

218
00:18:14,000 --> 00:18:16,000
 So, there are two kinds of karma.

219
00:18:16,000 --> 00:18:22,000
 Vatu-kama means objects of desire.

220
00:18:22,000 --> 00:18:30,480
 Say, sight, sound, smell, taste and touch. They are called

221
00:18:30,480 --> 00:18:31,000
 vatu-kama.

222
00:18:31,000 --> 00:18:36,400
 So vatu-kama means just the thing, the thing which is

223
00:18:36,400 --> 00:18:38,000
 desired.

224
00:18:38,000 --> 00:18:47,230
 So, when it means these five things, then the word karma is

225
00:18:47,230 --> 00:18:50,000
 to be known in a passive sense.

226
00:18:50,000 --> 00:18:58,000
 Desired, so something which is desired is called karma.

227
00:18:58,000 --> 00:19:06,190
 When we mean karma to mean kilesa-kama, kilesa means mental

228
00:19:06,190 --> 00:19:07,000
 defilements,

229
00:19:07,000 --> 00:19:16,000
 then it is active sense, something that desires.

230
00:19:16,000 --> 00:19:19,000
 So, sense desire is defilement, kilesa-kama.

231
00:19:19,000 --> 00:19:21,000
 So, there are two kinds of karma.

232
00:19:21,000 --> 00:19:28,260
 Whenever we use the word karma, we must be sure that we

233
00:19:28,260 --> 00:19:32,000
 mean vatu-kama or kilesa-kama.

234
00:19:32,000 --> 00:19:36,460
 So, whenever we find the word karma, we cannot translate it

235
00:19:36,460 --> 00:19:38,000
 as sense desire.

236
00:19:38,000 --> 00:19:42,190
 Sometimes, I prefer the word sense objects, the objects of

237
00:19:42,190 --> 00:19:47,000
 senses, objects of desire actually.

238
00:19:47,000 --> 00:19:49,000
 So, there are two kinds of karma.

239
00:19:49,000 --> 00:19:52,000
 vatu-kama and kilesa-kama.

240
00:19:52,000 --> 00:19:56,000
 Of these, sense desire is objective basis.

241
00:19:56,000 --> 00:20:00,510
 That means just the objects, particularized as the five

242
00:20:00,510 --> 00:20:02,000
 courts of sense desire.

243
00:20:02,000 --> 00:20:07,000
 They are called five courts of sense objects.

244
00:20:07,000 --> 00:20:16,000
 And they are simply sight, sound, smell, taste and touch.

245
00:20:16,000 --> 00:20:23,000
 And sense desire is defilement, which is craving, desires.

246
00:20:23,000 --> 00:20:27,000
 So, here it is the active meaning.

247
00:20:27,000 --> 00:20:32,000
 The sense fear is where these two operate together.

248
00:20:32,000 --> 00:20:39,000
 Sense fear means the world of human beings,

249
00:20:39,000 --> 00:20:44,280
 the world of the woeful stage and the world of lower

250
00:20:44,280 --> 00:20:46,000
 celestial beings.

251
00:20:46,000 --> 00:20:49,000
 So, that fear is called sense fear

252
00:20:49,000 --> 00:20:57,000
 because both vatu-kama and kilesa-kama operate here.

253
00:20:57,000 --> 00:20:58,000
 But what is that?

254
00:20:58,000 --> 00:21:01,000
 It is the elevenfold sense desire becoming.

255
00:21:01,000 --> 00:21:04,400
 Hell, asura, demons, ghosts, animals, human beings and six

256
00:21:04,400 --> 00:21:06,000
 sensual spheres of heaven.

257
00:21:06,000 --> 00:21:10,000
 So too, the fine material sphere and the immaterial sphere.

258
00:21:10,000 --> 00:21:13,260
 Taking fine material as craving for the fine material too

259
00:21:13,260 --> 00:21:14,000
 and so on.

260
00:21:14,000 --> 00:21:18,570
 So, these are the explanation of the words rupa-vajara and

261
00:21:18,570 --> 00:21:22,000
 arupa-vajara.

262
00:21:22,000 --> 00:21:26,000
 And lokutra is explained as something which crosses over

263
00:21:26,000 --> 00:21:29,000
 from the world.

264
00:21:29,000 --> 00:21:32,000
 So, it is called lokutra, super-mande.

265
00:21:32,000 --> 00:21:36,160
 So, whenever we find the word kama, we must be careful not

266
00:21:36,160 --> 00:21:40,000
 to translate it as sense desire everywhere.

267
00:21:40,000 --> 00:21:44,480
 Sometimes it means sense desire, sometimes it means sense

268
00:21:44,480 --> 00:21:53,000
 objects or objects of desire.

269
00:21:53,000 --> 00:22:07,000
 And on page 508, paragraph 80, before the paragraph 85.

270
00:22:07,000 --> 00:22:11,000
 It's not a paragraph, but the number is given there as 85.

271
00:22:11,000 --> 00:22:15,000
 There's an explanation of the word sankara.

272
00:22:15,000 --> 00:22:21,320
 For in this sense, prompting is the term for the prior

273
00:22:21,320 --> 00:22:25,000
 effort exerted by himself or others.

274
00:22:25,000 --> 00:22:30,290
 Now, when we describe consciousness, we use the word sank

275
00:22:30,290 --> 00:22:31,000
ara.

276
00:22:31,000 --> 00:22:35,000
 It is with sankara or without sankara.

277
00:22:35,000 --> 00:22:42,000
 And the word sankara here means effort.

278
00:22:42,000 --> 00:22:46,000
 A prior effort before doing something.

279
00:22:46,000 --> 00:22:49,000
 So, it has a special sense.

280
00:22:49,000 --> 00:22:52,000
 I think you're familiar with the word sankara.

281
00:22:52,000 --> 00:22:59,410
 Sankara means those that are conditioned and also those

282
00:22:59,410 --> 00:23:02,000
 that condition.

283
00:23:02,000 --> 00:23:08,200
 But here, in this particular context, sankara means just

284
00:23:08,200 --> 00:23:10,000
 prior effort.

285
00:23:10,000 --> 00:23:13,000
 So, with effort and without effort.

286
00:23:13,000 --> 00:23:18,000
 That means with prompting and without prompting.

287
00:23:18,000 --> 00:23:24,000
 So, this is the definition of the word sankara.

288
00:23:24,000 --> 00:23:29,080
 And we should note this for explanation when you teach abh

289
00:23:29,080 --> 00:23:30,000
irama.

290
00:23:30,000 --> 00:23:34,270
 In this sense, prompting is a term for a prior effort

291
00:23:34,270 --> 00:23:37,000
 exerted by himself or others.

292
00:23:37,000 --> 00:23:41,470
 And then the examples of the types of consciousness are

293
00:23:41,470 --> 00:23:42,000
 given.

294
00:23:42,000 --> 00:23:45,000
 They are not difficult to understand.

295
00:23:45,000 --> 00:23:51,300
 And then in paragraph 86, at the end of that paragraph, the

296
00:23:51,300 --> 00:23:54,000
 fifth is associated with equanimity.

297
00:23:54,000 --> 00:24:03,000
 And equanimity here means feeling, neutral feeling.

298
00:24:03,000 --> 00:24:05,990
 You know, there are pleasant feeling, unpleasant feeling,

299
00:24:05,990 --> 00:24:08,000
 and neither pleasant nor unpleasant.

300
00:24:08,000 --> 00:24:10,000
 So, the third one is called neutral feeling.

301
00:24:10,000 --> 00:24:13,000
 And the third one is called upika.

302
00:24:13,000 --> 00:24:18,320
 Because the word upika can mean feeling, neutral feeling,

303
00:24:18,320 --> 00:24:22,000
 all the real equanimity.

304
00:24:22,000 --> 00:24:28,000
 So, here equanimity means just neutral feeling.

305
00:24:28,000 --> 00:24:37,000
 Not soma nasa, not dho manasa.

306
00:24:37,000 --> 00:24:41,000
 And then the numbers are given here.

307
00:24:41,000 --> 00:24:46,060
 And so, when you see the numbers, you can check with the

308
00:24:46,060 --> 00:24:50,000
 notes given today, this sheet.

309
00:24:50,000 --> 00:24:58,200
 And then go to that sheet if you are already familiar with

310
00:24:58,200 --> 00:25:00,000
 the dots.

311
00:25:00,000 --> 00:25:13,200
 So, on page 509, paragraph 991, when a man is happy and

312
00:25:13,200 --> 00:25:14,000
 content,

313
00:25:14,000 --> 00:25:19,190
 in placing wrong view foremost of the thought beginning,

314
00:25:19,190 --> 00:25:22,000
 there is no danger in sense desire and so on.

315
00:25:22,000 --> 00:25:26,000
 I want to strike out the word in.

316
00:25:26,000 --> 00:25:30,000
 He is not happy in placing wrong view foremost.

317
00:25:30,000 --> 00:25:34,600
 He is happy in his content, and he places wrong view

318
00:25:34,600 --> 00:25:36,000
 foremost in his mind.

319
00:25:36,000 --> 00:25:41,000
 That means with wrong view, he does something.

320
00:25:41,000 --> 00:25:45,320
 So, when a man is heavy and content, placing wrong view

321
00:25:45,320 --> 00:25:48,000
 foremost of the thought beginning,

322
00:25:48,000 --> 00:25:51,000
 there is no danger in sense desire and either and so on.

323
00:25:51,000 --> 00:25:55,000
 So, the word in is not needed here.

324
00:25:55,000 --> 00:26:02,740
 And then down the paragraph, about the bottom of the

325
00:26:02,740 --> 00:26:05,000
 paragraph,

326
00:26:05,000 --> 00:26:08,860
 "But when the consciousnesses are devoid of joy in these

327
00:26:08,860 --> 00:26:12,600
 four instances, through encountering no excellence in these

328
00:26:12,600 --> 00:26:14,000
 sense desires."

329
00:26:14,000 --> 00:26:18,000
 Here, sense objects, not sense desires.

330
00:26:18,000 --> 00:26:23,000
 Sense objects which are desired, or which are objects of

331
00:26:23,000 --> 00:26:24,000
 desire.

332
00:26:24,000 --> 00:26:28,000
 And then...

333
00:26:28,000 --> 00:26:47,000
 And then page 510, paragraph 96, "I Consciousness."

334
00:26:47,000 --> 00:26:50,000
 "I Consciousness" means "seeing consciousness."

335
00:26:50,000 --> 00:26:54,040
 That's the characteristic of being supported by the "I" and

336
00:26:54,040 --> 00:26:56,000
 cognizing visible data.

337
00:26:56,000 --> 00:27:00,000
 Its function is to have only visible data as its object.

338
00:27:00,000 --> 00:27:08,240
 That means the "I Consciousness" sees just the visible

339
00:27:08,240 --> 00:27:09,000
 object.

340
00:27:09,000 --> 00:27:15,790
 And when it sees, it just sees it and it does not see that

341
00:27:15,790 --> 00:27:20,000
 it is blue or that it is red or whatever.

342
00:27:20,000 --> 00:27:25,310
 Just the "I Consciousness" sees the object just as a

343
00:27:25,310 --> 00:27:27,000
 visible object.

344
00:27:27,000 --> 00:27:33,000
 And then we know that it is blue, it is green by Manodwara,

345
00:27:33,000 --> 00:27:40,710
 by Mind or Thought Process, not by the "I" or Thought

346
00:27:40,710 --> 00:27:42,000
 Process.

347
00:27:42,000 --> 00:27:47,940
 So that is why it is an only visible data and not color,

348
00:27:47,940 --> 00:27:50,000
 shape and so on.

349
00:27:50,000 --> 00:27:54,000
 And it is manifested as occupation with visible data.

350
00:27:54,000 --> 00:28:02,000
 I think it is not occupied...

351
00:28:02,000 --> 00:28:11,000
 Let me see. Let us go to paragraph 107, about four lines.

352
00:28:11,000 --> 00:28:17,000
 It is manifested as confrontation of visible data, the same

353
00:28:17,000 --> 00:28:18,000
 words.

354
00:28:18,000 --> 00:28:22,000
 So here also, not occupation.

355
00:28:22,000 --> 00:28:27,000
 It is being faced with visible data.

356
00:28:27,000 --> 00:28:32,250
 Confrontation means being faced with visible data, not

357
00:28:32,250 --> 00:28:34,000
 occupation with visible data.

358
00:28:34,000 --> 00:28:38,280
 And its proximate cause is departure of, that means

359
00:28:38,280 --> 00:28:41,640
 disappearance of, the functional mind element that has

360
00:28:41,640 --> 00:28:44,000
 visible data as its object.

361
00:28:44,000 --> 00:28:51,850
 Now, in order to understand these passages, you have to

362
00:28:51,850 --> 00:28:59,000
 understand at least the Thought Process.

363
00:28:59,000 --> 00:29:03,670
 During the Abhidhamma classes, I talk about Thought Process

364
00:29:03,670 --> 00:29:04,000
es.

365
00:29:04,000 --> 00:29:08,850
 So if you have notes, please take those notes and look at

366
00:29:08,850 --> 00:29:14,000
 the first Thought Process and then read these passages.

367
00:29:14,000 --> 00:29:17,480
 Because here, the "I Consciousness", its proximate cause is

368
00:29:17,480 --> 00:29:21,080
 the departure of the functional mind element that has

369
00:29:21,080 --> 00:29:23,000
 visible data as its object.

370
00:29:23,000 --> 00:29:29,850
 That means, immediately before "I Consciousness", what is

371
00:29:29,850 --> 00:29:31,000
 there?

372
00:29:31,000 --> 00:29:35,680
 So, disappearance of that functional mind element or

373
00:29:35,680 --> 00:29:40,950
 disappearance of five-door adverting is proximate cause for

374
00:29:40,950 --> 00:29:43,000
 "I Consciousness".

375
00:29:43,000 --> 00:29:48,990
 That is, if the functional mind element does not disappear,

376
00:29:48,990 --> 00:29:53,000
 then "I Consciousness" cannot arise.

377
00:29:53,000 --> 00:29:57,530
 So, functional mind element disappears so that "I Conscious

378
00:29:57,530 --> 00:29:59,000
ness" can arise.

379
00:29:59,000 --> 00:30:03,570
 Therefore, disappearance of functioning and consciousness

380
00:30:03,570 --> 00:30:07,000
 is a proximate cause for "I Consciousness".

381
00:30:07,000 --> 00:30:13,470
 Something like when you are in a line, the disappearance of

382
00:30:13,470 --> 00:30:20,000
 the man before is the proximate cause for you being there.

383
00:30:20,000 --> 00:30:43,570
 And then, the mind element is receiving or accepting and so

384
00:30:43,570 --> 00:30:46,000
 on.

385
00:30:46,000 --> 00:30:55,230
 And then, on page 500, beginning with page 513, paragraph

386
00:30:55,230 --> 00:31:02,410
 110, the 14 kinds of modes or 14 kinds of functions are

387
00:31:02,410 --> 00:31:04,000
 explained.

388
00:31:04,000 --> 00:31:10,560
 So, these 89 types of consciousness have different

389
00:31:10,560 --> 00:31:16,490
 functions. Some types of consciousness have one function

390
00:31:16,490 --> 00:31:21,120
 only, but others have maybe two functions, five functions

391
00:31:21,120 --> 00:31:22,000
 and so on.

392
00:31:22,000 --> 00:31:27,440
 So, these 14 functions performed by different types of

393
00:31:27,440 --> 00:31:33,000
 consciousness are explained beginning with paragraph 110.

394
00:31:33,000 --> 00:31:38,450
 And these also, to understand these functions also, you

395
00:31:38,450 --> 00:31:43,000
 need to have a diagram of the thought process.

396
00:31:43,000 --> 00:31:47,690
 So, looking at the thought process diagram, you read these

397
00:31:47,690 --> 00:31:51,000
 passages and you will understand them.

398
00:31:51,000 --> 00:32:03,990
 And on paragraph 111, about seven lines, eight lines down,

399
00:32:03,990 --> 00:32:07,000
 "entering upon the state of your nuts".

400
00:32:07,000 --> 00:32:15,210
 That means being reborn as your nuts. So, entering upon the

401
00:32:15,210 --> 00:32:22,000
 state of your nuts simply means being reborn as.

402
00:32:22,000 --> 00:32:30,720
 And then, in the footnote 40, 42, third line, "sign of

403
00:32:30,720 --> 00:32:37,890
 karma is the gift to be given that was a condition for the

404
00:32:37,890 --> 00:32:41,000
 volition and so on".

405
00:32:41,000 --> 00:32:47,490
 So, after "to be given" please add "etc". "The gift to be

406
00:32:47,490 --> 00:32:54,000
 given" etc. that was a condition for the volition.

407
00:32:54,000 --> 00:33:00,570
 And footnote 44, with that same object, "if karma is the

408
00:33:00,570 --> 00:33:06,000
 life-continuance", it's a mistake, "relinking's object",

409
00:33:06,000 --> 00:33:10,000
 not life-continuance, relinking or rebirth.

410
00:33:10,000 --> 00:33:22,790
 So, if karma is the relinking's object, then it is that

411
00:33:22,790 --> 00:33:27,000
 karma and so on.

412
00:33:27,000 --> 00:33:42,420
 So, these 14 modes of voting functions, explained one by

413
00:33:42,420 --> 00:33:44,000
 one.

414
00:33:44,000 --> 00:33:53,820
 And at the end, where do we end here? Now, paragraph 123,

415
00:33:53,820 --> 00:33:58,110
 there is a saying, "For the last life-continuum

416
00:33:58,110 --> 00:34:04,550
 consciousness of all in one becoming is called death duty,

417
00:34:04,550 --> 00:34:08,000
 because of falling from that becoming".

418
00:34:08,000 --> 00:34:16,930
 Now, in one given life, relinking life-continuum and death

419
00:34:16,930 --> 00:34:23,000
 are of the same type of consciousness.

420
00:34:23,000 --> 00:34:29,290
 Actually, relinking consciousness is a resultant

421
00:34:29,290 --> 00:34:34,690
 consciousness. And this consciousness repeats itself all

422
00:34:34,690 --> 00:34:36,000
 through the life.

423
00:34:36,000 --> 00:34:43,400
 But after the first moment of relinking, it arises, it is

424
00:34:43,400 --> 00:34:47,000
 called life-continuum.

425
00:34:47,000 --> 00:34:52,780
 And at the end of life also, dead-diver consciousness

426
00:34:52,780 --> 00:34:58,000
 arises. And that we call death consciousness.

427
00:34:58,000 --> 00:35:03,230
 So, in one given life, the relinking life-continuum and

428
00:35:03,230 --> 00:35:09,000
 death consciousness are of the same type of consciousness.

429
00:35:09,000 --> 00:35:16,030
 So, that is why it is said here, "For the last life-contin

430
00:35:16,030 --> 00:35:23,000
uum consciousness of all in one becoming is called death".

431
00:35:23,000 --> 00:35:31,000
 So, the last life-continuum is what we call death.

432
00:35:31,000 --> 00:35:43,480
 So, these three kinds of consciousness are just one and the

433
00:35:43,480 --> 00:35:50,000
 same type of consciousness.

434
00:35:50,000 --> 00:36:08,000
 Okay.

435
00:36:08,000 --> 00:36:14,130
 When I taught Abhidhamma, the chapter on the chedra takes

436
00:36:14,130 --> 00:36:20,000
 me about how many talks? Six or seven talks?

437
00:36:20,000 --> 00:36:30,000
 Today we are doing in one hour.

438
00:36:30,000 --> 00:36:37,000
 So, next week, what? At the end of the chapter...

439
00:36:37,000 --> 00:36:45,000
 Yeah, let me see.

440
00:36:45,000 --> 00:36:53,990
 Feeling aggregate, perception aggregate, and then what? The

441
00:36:53,990 --> 00:37:14,000
... Follition aggregate.

442
00:37:14,000 --> 00:37:29,000
 Oh, not yet to the end.

443
00:37:29,000 --> 00:37:43,000
 According to period, maybe about page 535.

444
00:37:43,000 --> 00:37:58,050
 Classification of the five aggregates under 11 hands. Up to

445
00:37:58,050 --> 00:38:00,000
 that.

446
00:38:00,000 --> 00:38:10,080
 And do we have a note for... I handed Abhidhamma class?

447
00:38:10,080 --> 00:38:16,200
 Those would be handy because next week we will do the

448
00:38:16,200 --> 00:38:18,000
 description of the mental state.

449
00:38:18,000 --> 00:38:47,000
 So, if you have those notes with you, it's better.

450
00:38:47,000 --> 00:39:16,000
 Okay.

451
00:39:16,000 --> 00:39:45,000
 Okay.

