1
00:00:00,000 --> 00:00:05,680
 studying jhanas and mindfulness before vipassana.

2
00:00:05,680 --> 00:00:07,840
 Question.

3
00:00:07,840 --> 00:00:12,160
 Should the four foundations of mindfulness and the five jh

4
00:00:12,160 --> 00:00:17,440
ana factors of concentration and absorption be learned

5
00:00:17,440 --> 00:00:23,720
 before vipassana meditation can be correctly used?

6
00:00:23,720 --> 00:00:29,200
 Answer. A person might cultivate the five jhana factors

7
00:00:29,200 --> 00:00:35,080
 based on a conceptual object before cultivating vipassana.

8
00:00:35,080 --> 00:00:40,550
 This way of practice can be quite powerful, but it seems to

9
00:00:40,550 --> 00:00:44,790
 generally take longer as the time span focused on a

10
00:00:44,790 --> 00:00:49,800
 conceptual object could have been used in cultivating vip

11
00:00:49,800 --> 00:00:51,440
assana as well.

12
00:00:51,440 --> 00:00:57,170
 Those who practice vipassana from the beginning seem to

13
00:00:57,170 --> 00:01:02,890
 attain results in a few weeks or at most a month, whereas

14
00:01:02,890 --> 00:01:08,250
 those who practice tranquility first seem generally to take

15
00:01:08,250 --> 00:01:12,520
 months or even years to get the same results.

16
00:01:12,520 --> 00:01:18,430
 Assuming they ever get around to vipassana meditation at

17
00:01:18,430 --> 00:01:23,710
 all, some people seem to get stuck on the calm and bliss of

18
00:01:23,710 --> 00:01:30,040
 tranquility, never realizing that there is something higher

19
00:01:30,040 --> 00:01:32,200
 than those states.

20
00:01:32,200 --> 00:01:36,850
 As for the four foundations of mindfulness, if the question

21
00:01:36,850 --> 00:01:41,320
 is, do you have to practice them before practicing vipass

22
00:01:41,320 --> 00:01:46,790
ana meditation, then this is a misunderstanding about vipass

23
00:01:46,790 --> 00:01:51,750
ana meditation, because vipassana meditation is the practice

24
00:01:51,750 --> 00:01:55,120
 of the four foundations of mindfulness.

25
00:01:55,120 --> 00:02:00,360
 You practice the four foundations of mindfulness to see

26
00:02:00,360 --> 00:02:02,080
 clearly vipassana.

27
00:02:02,080 --> 00:02:06,220
 There are ways to use the four foundations of mindfulness

28
00:02:06,220 --> 00:02:11,880
 to cultivate tranquility, but mostly they are used to focus

29
00:02:11,880 --> 00:02:16,360
 on and see clearly ultimate reality.

30
00:02:16,360 --> 00:02:20,200
 If the question is whether one has to study the four

31
00:02:20,200 --> 00:02:24,600
 foundations before practicing, you do not have to study

32
00:02:24,600 --> 00:02:29,350
 them in detail, but obviously you have to know about them

33
00:02:29,350 --> 00:02:32,760
 because they are the practice itself.

34
00:02:32,760 --> 00:02:37,910
 Knowing and understanding how to practice in regard to each

35
00:02:37,910 --> 00:02:42,410
 of the four foundations, at least on a basic level, is

36
00:02:42,410 --> 00:02:47,690
 essential. Without understanding what it is that you intend

37
00:02:47,690 --> 00:02:52,760
 to practice, you cannot be expected to practice properly.

