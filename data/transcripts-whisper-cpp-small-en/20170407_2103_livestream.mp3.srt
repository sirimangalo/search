1
00:00:00,000 --> 00:00:07,320
 Then you're never without what you want.

2
00:00:07,320 --> 00:00:10,560
 So it's the greatest wealth.

3
00:00:10,560 --> 00:00:13,960
 Santu di paramangdhanam.

4
00:00:13,960 --> 00:00:18,140
 Contentment is the greatest wealth.

5
00:00:18,140 --> 00:00:22,180
 And so it's a classic example of how he got these teachings

6
00:00:22,180 --> 00:00:22,240
.

7
00:00:22,240 --> 00:00:23,400
 And what does he do?

8
00:00:23,400 --> 00:00:28,400
 He goes and preaches them to others.

9
00:00:28,400 --> 00:00:31,690
 This happens more often than you might think, where people

10
00:00:31,690 --> 00:00:33,000
 get good teachings and then they

11
00:00:33,000 --> 00:00:36,230
 go around trying to what they do with it, meaning they don

12
00:00:36,230 --> 00:00:37,880
't actually practice it.

13
00:00:37,880 --> 00:00:40,590
 Instead of taking it in and practicing it, they say, "Yeah,

14
00:00:40,590 --> 00:00:42,040
 I heard this great teaching.

15
00:00:42,040 --> 00:00:44,160
 It really makes a lot of sense."

16
00:00:44,160 --> 00:00:46,880
 And then they pass it on to others.

17
00:00:46,880 --> 00:00:49,440
 So he actually became acting.

18
00:00:49,440 --> 00:00:56,880
 He collected all sorts of things, a lot of robes, I think.

19
00:00:56,880 --> 00:01:06,580
 And so when it came time for the rains, there's a thing

20
00:01:06,580 --> 00:01:07,960
 where you have to stay in one, you

21
00:01:07,960 --> 00:01:10,750
 have to say that you're going to stay in a specific

22
00:01:10,750 --> 00:01:11,660
 monastery.

23
00:01:11,660 --> 00:01:14,290
 So what he did is he went to four different monasteries and

24
00:01:14,290 --> 00:01:15,560
 he left his shoes here and

25
00:01:15,560 --> 00:01:19,900
 he left an umbrella there and he left some of his

26
00:01:19,900 --> 00:01:23,440
 belongings to make people think that

27
00:01:23,440 --> 00:01:24,440
 he was staying there.

28
00:01:24,440 --> 00:01:27,820
 And then at the end of the rains, he went around and

29
00:01:27,820 --> 00:01:30,040
 collected because at the end of

30
00:01:30,040 --> 00:01:34,040
 the rains, people would generally offer robes for the monks

31
00:01:34,040 --> 00:01:36,040
 who were then going to travel

32
00:01:36,040 --> 00:01:38,960
 during the cold season.

33
00:01:38,960 --> 00:01:42,550
 And he went and collected his share of the offerings at the

34
00:01:42,550 --> 00:01:43,840
 end of the rains.

35
00:01:43,840 --> 00:01:46,760
 And he carried them around in a cart with him.

36
00:01:46,760 --> 00:02:00,240
 He had like robes and all sorts of things.

37
00:02:00,240 --> 00:02:03,920
 He put them in a cart and continued on his journey.

38
00:02:03,920 --> 00:02:10,790
 So he's wandering around as a monk with a cart full of

39
00:02:10,790 --> 00:02:13,880
 robes and garments.

40
00:02:13,880 --> 00:02:17,940
 And as he was going along, he came upon two monks who were

41
00:02:17,940 --> 00:02:20,820
 unable to divide their belongings.

42
00:02:20,820 --> 00:02:26,200
 They had two robes.

43
00:02:26,200 --> 00:02:29,720
 I guess it was probably two upper robes and an outer robe.

44
00:02:29,720 --> 00:02:36,080
 I'm not quite sure what the Pali was.

45
00:02:36,080 --> 00:02:45,480
 Oh no, it was actually a blanket.

46
00:02:45,480 --> 00:02:52,720
 Right, so one blanket and two cloaks or cloths.

47
00:02:52,720 --> 00:02:53,720
 Two pieces of cloth.

48
00:02:53,720 --> 00:02:57,730
 So the cloths would be used to make robes, the blanket or

49
00:02:57,730 --> 00:02:58,680
 the wool.

50
00:02:58,680 --> 00:03:02,370
 Maybe it was a measure of wool to be used for robes as well

51
00:03:02,370 --> 00:03:04,240
 because wool could be used

52
00:03:04,240 --> 00:03:07,240
 for robes.

53
00:03:07,240 --> 00:03:14,700
 And the wool of course was the more valuable of all the

54
00:03:14,700 --> 00:03:16,080
 items.

55
00:03:16,080 --> 00:03:20,720
 So they could easily divide the two pieces of cloth.

56
00:03:20,720 --> 00:03:25,160
 But how are they going to divide this piece of wool?

57
00:03:25,160 --> 00:03:26,160
 Cut it in half?

58
00:03:26,160 --> 00:03:29,840
 Well, it loses a lot of value when you do that.

59
00:03:29,840 --> 00:03:32,440
 So they asked this monk, they said, "Oh, here's a guy."

60
00:03:32,440 --> 00:03:33,440
 And they'd heard about him.

61
00:03:33,440 --> 00:03:34,840
 He's this guy who preaches contentment.

62
00:03:34,840 --> 00:03:39,280
 So he'll be a good person to help us figure out what the

63
00:03:39,280 --> 00:03:41,840
 most noble way of dividing up

64
00:03:41,840 --> 00:03:45,800
 these cloths is.

65
00:03:45,800 --> 00:03:47,850
 And he looked at them and he looked at this blanket and he

66
00:03:47,850 --> 00:03:48,960
 saw the blanket was actually

67
00:03:48,960 --> 00:03:49,960
 quite luxurious.

68
00:03:49,960 --> 00:03:53,120
 And so he says to them, "I'll decide this for you."

69
00:03:53,120 --> 00:03:54,760
 And he says, "Here, you take this.

70
00:03:54,760 --> 00:03:57,120
 This piece of cloth is for you.

71
00:03:57,120 --> 00:03:58,800
 This piece of cloth is for you.

72
00:03:58,800 --> 00:04:02,280
 And I'll take the blanket."

73
00:04:02,280 --> 00:04:05,880
 And he solved their problem for them.

74
00:04:05,880 --> 00:04:08,620
 Suffice to say, they weren't all that happy and they went

75
00:04:08,620 --> 00:04:10,280
 to the Buddha and complained.

76
00:04:10,280 --> 00:04:14,510
 And the Buddha said, "This monk, he was like this in a past

77
00:04:14,510 --> 00:04:15,800
 life as well."

78
00:04:15,800 --> 00:04:17,080
 And he told the Jataka story.

79
00:04:17,080 --> 00:04:19,700
 This is one case where the Dhammapada and the Jataka come

80
00:04:19,700 --> 00:04:20,400
 together.

81
00:04:20,400 --> 00:04:22,360
 Same as the last one, though.

82
00:04:22,360 --> 00:04:24,680
 I won't tell the past life story.

83
00:04:24,680 --> 00:04:28,760
 It's not all that useful.

84
00:04:28,760 --> 00:04:31,760
 But the Buddha just complained about Upananda as well.

85
00:04:31,760 --> 00:04:35,330
 He said, "This monk, you know, this is not the way a monk

86
00:04:35,330 --> 00:04:36,520
 should behave.

87
00:04:36,520 --> 00:04:39,000
 It's not the way a person should behave.

88
00:04:39,000 --> 00:04:44,720
 It's just not a...

89
00:04:44,720 --> 00:04:46,440
 It's not the way anyone should behave."

90
00:04:46,440 --> 00:04:50,480
 He said, "A man who admonishes, someone who admonishes

91
00:04:50,480 --> 00:04:53,400
 others should first direct themselves

92
00:04:53,400 --> 00:04:54,400
 in the way they should go."

93
00:04:54,400 --> 00:04:59,800
 And then he spoke the verse.

94
00:04:59,800 --> 00:05:03,160
 So the obvious lesson here is practice what you preach.

95
00:05:03,160 --> 00:05:07,760
 I think that's a fairly simple lesson.

96
00:05:07,760 --> 00:05:11,400
 It doesn't require that much explanation.

97
00:05:11,400 --> 00:05:16,310
 We all have this sense that it's rather hypocritical if one

98
00:05:16,310 --> 00:05:19,600
 is teaching morality and isn't moral.

99
00:05:19,600 --> 00:05:25,560
 One is teaching Dhamma and doesn't follow the Dhamma.

100
00:05:25,560 --> 00:05:30,240
 But there's more implications to this verse and to this

101
00:05:30,240 --> 00:05:31,520
 whole idea.

102
00:05:31,520 --> 00:05:37,120
 There's a much greater point to be made here.

103
00:05:37,120 --> 00:05:38,920
 And that is, what do we want out of life?

104
00:05:38,920 --> 00:05:42,680
 What do we want to do with our lives?

105
00:05:42,680 --> 00:05:49,660
 And maybe more importantly, what do we want to do with

106
00:05:49,660 --> 00:05:51,440
 Buddhism?

107
00:05:51,440 --> 00:05:54,870
 Before we found Buddhism, before we found the practice of

108
00:05:54,870 --> 00:05:56,560
 meditation, life was like

109
00:05:56,560 --> 00:05:59,360
 a wilderness.

110
00:05:59,360 --> 00:06:04,990
 So it's understandable that you might just have to make it

111
00:06:04,990 --> 00:06:07,120
 up as you go along.

112
00:06:07,120 --> 00:06:09,640
 Hard to know what is the right way or a good way.

113
00:06:09,640 --> 00:06:13,900
 But now we have this incredible and profound set of

114
00:06:13,900 --> 00:06:16,960
 teachings and this very direct way

115
00:06:16,960 --> 00:06:24,920
 of bettering ourselves.

116
00:06:24,920 --> 00:06:33,100
 And to just go around sharing it with others is really

117
00:06:33,100 --> 00:06:38,480
 missing the most powerful aspect

118
00:06:38,480 --> 00:06:43,040
 of the teaching.

119
00:06:43,040 --> 00:06:48,690
 And there's many ways that this missing the core comes

120
00:06:48,690 --> 00:06:49,760
 about.

121
00:06:49,760 --> 00:06:52,440
 For some people, they'll go around teaching others.

122
00:06:52,440 --> 00:06:56,320
 For some, they'll go around supporting others to practice.

123
00:06:56,320 --> 00:07:00,720
 And a lot of people who appreciate monks so much that they

124
00:07:00,720 --> 00:07:02,960
 like offering them food and

125
00:07:02,960 --> 00:07:09,960
 clothes and so on.

126
00:07:09,960 --> 00:07:21,220
 Because you can do whatever, the universe is our playground

127
00:07:21,220 --> 00:07:21,960
.

128
00:07:21,960 --> 00:07:25,720
 We can do as we like.

129
00:07:25,720 --> 00:07:31,400
 We can certainly become great teachers, famous teachers.

130
00:07:31,400 --> 00:07:35,960
 We can certainly be very kind and generous to others.

131
00:07:35,960 --> 00:07:37,520
 We can certainly help others.

132
00:07:37,520 --> 00:07:42,650
 There's even cases where unenlightened beings can lead

133
00:07:42,650 --> 00:07:45,600
 other beings to enlightenment by

134
00:07:45,600 --> 00:07:55,760
 relating and by passing on the teachings of a Buddha.

135
00:07:55,760 --> 00:07:58,450
 But we always have to add, it's a good opportunity or a

136
00:07:58,450 --> 00:08:00,640
 good example of how we have to ask ourselves

137
00:08:00,640 --> 00:08:02,720
 what we want out of life.

138
00:08:02,720 --> 00:08:03,720
 Why are we here?

139
00:08:03,720 --> 00:08:04,720
 What is our goal?

140
00:08:04,720 --> 00:08:10,830
 And in Buddhism, this verse is one of the most important

141
00:08:10,830 --> 00:08:14,640
 verses in this regard, reminding

142
00:08:14,640 --> 00:08:19,770
 us of the Buddha's idea of what we should do and what we're

143
00:08:19,770 --> 00:08:22,120
 really best off focusing

144
00:08:22,120 --> 00:08:26,840
 on is setting ourselves in what is right.

145
00:08:26,840 --> 00:08:30,940
 Teaching others, it's great for accumulating wealth and

146
00:08:30,940 --> 00:08:32,280
 fame and power.

147
00:08:32,280 --> 00:08:41,460
 But it's certainly not useful for cultivating wisdom and

148
00:08:41,460 --> 00:08:46,520
 contentment and happiness.

149
00:08:46,520 --> 00:08:51,310
 And so it speaks to a larger issue of how we tend to

150
00:08:51,310 --> 00:08:53,400
 project outward.

151
00:08:53,400 --> 00:09:01,640
 We have attachments and aversions and delusions and we

152
00:09:01,640 --> 00:09:06,600
 cling to them and we use them as our

153
00:09:06,600 --> 00:09:07,600
 jumping off point.

154
00:09:07,600 --> 00:09:12,770
 So if we're angry, that colors, we express that anger

155
00:09:12,770 --> 00:09:15,600
 outward, getting upset.

156
00:09:15,600 --> 00:09:19,970
 If we're greedy, we express that outward by grasping and

157
00:09:19,970 --> 00:09:21,960
 chasing after things.

158
00:09:21,960 --> 00:09:28,400
 If we're deluded, we use the delusion.

159
00:09:28,400 --> 00:09:34,290
 And so one way of understanding Buddhism is as a reflexive

160
00:09:34,290 --> 00:09:36,400
 practice where we look at our

161
00:09:36,400 --> 00:09:39,320
 reasons for doing things.

162
00:09:39,320 --> 00:09:42,640
 We look at the tools that we use.

163
00:09:42,640 --> 00:09:45,440
 We look at our own mind.

164
00:09:45,440 --> 00:09:50,450
 So when we're angry, we don't look outside at what we're

165
00:09:50,450 --> 00:09:51,600
 angry at.

166
00:09:51,600 --> 00:09:55,330
 We don't think about what we're going to do about the anger

167
00:09:55,330 --> 00:09:55,600
.

168
00:09:55,600 --> 00:09:57,560
 Instead we look at the anger itself.

169
00:09:57,560 --> 00:10:05,250
 When we want something, the idea of turning inward, that

170
00:10:05,250 --> 00:10:08,920
 when you, the Buddha's teaching

171
00:10:08,920 --> 00:10:09,920
 is not something.

172
00:10:09,920 --> 00:10:16,570
 It's not another tool for us to apply to the world around

173
00:10:16,570 --> 00:10:17,360
 us.

174
00:10:17,360 --> 00:10:19,640
 It's something for us to understand about ourselves.

175
00:10:19,640 --> 00:10:24,650
 So when we hear about mindfulness, it's not something for

176
00:10:24,650 --> 00:10:26,880
 us to think about and to teach

177
00:10:26,880 --> 00:10:30,720
 to others in this case.

178
00:10:30,720 --> 00:10:40,560
 It's just another example of the outward gaze and the

179
00:10:40,560 --> 00:10:43,520
 inward gaze.

180
00:10:43,520 --> 00:10:47,100
 Because it's very common for people to read about Buddhism

181
00:10:47,100 --> 00:10:48,960
 and think about Buddhism and

182
00:10:48,960 --> 00:10:51,970
 intellectualize about it to the point where they're able to

183
00:10:51,970 --> 00:10:53,560
 explain it to others, debate

184
00:10:53,560 --> 00:10:57,760
 with others, or discuss it with others.

185
00:10:57,760 --> 00:11:02,380
 There are groups of people who sit around and talk about

186
00:11:02,380 --> 00:11:03,520
 Buddhism.

187
00:11:03,520 --> 00:11:08,840
 That's an example of using the teaching outwardly.

188
00:11:08,840 --> 00:11:13,710
 That's not how, I mean it's not the most useful, it's not

189
00:11:13,710 --> 00:11:16,040
 the most useful way of living your

190
00:11:16,040 --> 00:11:20,440
 life in general really.

191
00:11:20,440 --> 00:11:25,800
 You benefit so much more from turning inward.

192
00:11:25,800 --> 00:11:30,900
 Let's say it's this sort of this quandary in Buddhism

193
00:11:30,900 --> 00:11:33,600
 because there are a lot of Buddhists

194
00:11:33,600 --> 00:11:37,640
 who believe that you should be out in the world engaged in

195
00:11:37,640 --> 00:11:39,080
 helping others.

196
00:11:39,080 --> 00:11:48,310
 And the Buddha clearly makes a stand here when he says look

197
00:11:48,310 --> 00:11:51,080
 inward first.

198
00:11:51,080 --> 00:11:54,160
 And so at the very least we separate the world in this way.

199
00:11:54,160 --> 00:11:58,850
 There is the outward practice and ideally you're using

200
00:11:58,850 --> 00:12:01,680
 compassion and love and kindness

201
00:12:01,680 --> 00:12:04,880
 and mindfulness and wisdom to relate to the world.

202
00:12:04,880 --> 00:12:08,020
 And then there's the inward practice where you're

203
00:12:08,020 --> 00:12:11,280
 cultivating love, compassion, contentment,

204
00:12:11,280 --> 00:12:15,440
 wisdom, mindfulness.

205
00:12:15,440 --> 00:12:21,410
 And if you look at it that way, it's quite clear that if

206
00:12:21,410 --> 00:12:24,800
 you start outwardly, all the

207
00:12:24,800 --> 00:12:29,080
 stuff inside that's good and bad is what you're going to

208
00:12:29,080 --> 00:12:31,680
 use, what you're going to apply to

209
00:12:31,680 --> 00:12:32,680
 the world around you.

210
00:12:32,680 --> 00:12:37,730
 And so if you do that first, you're dealing with an

211
00:12:37,730 --> 00:12:41,440
 imperfect, you end up applying the

212
00:12:41,440 --> 00:12:45,840
 bad with the good.

213
00:12:45,840 --> 00:12:48,490
 Whereas if you start inwardly and first set yourself on

214
00:12:48,490 --> 00:12:50,120
 what is right, what this means

215
00:12:50,120 --> 00:12:54,630
 is that you're able to cleanse, you're able to purify, you

216
00:12:54,630 --> 00:12:56,880
're able to refine the tools,

217
00:12:56,880 --> 00:13:02,610
 the part of the parts of you that you express in the world

218
00:13:02,610 --> 00:13:05,720
 around you, express to other

219
00:13:05,720 --> 00:13:13,580
 people and in your acts and in your speech, in your

220
00:13:13,580 --> 00:13:18,760
 relationship with the world.

221
00:13:18,760 --> 00:13:25,230
 But the idea here is to remind us of the importance of

222
00:13:25,230 --> 00:13:30,600
 experiential learning, of applying the

223
00:13:30,600 --> 00:13:33,480
 teachings to us here and now.

224
00:13:33,480 --> 00:13:37,620
 So when the Buddha talks about contentment, he really means

225
00:13:37,620 --> 00:13:39,720
 I should learn to be content,

226
00:13:39,720 --> 00:13:48,280
 I should study my discontent and fix it.

227
00:13:48,280 --> 00:13:52,110
 And so one way of explaining how meditation works is to

228
00:13:52,110 --> 00:13:54,160
 cultivate contentment.

229
00:13:54,160 --> 00:13:58,120
 I mean, it's really a very core concept.

230
00:13:58,120 --> 00:14:03,690
 Contentment means freeing yourself from desire, freeing

231
00:14:03,690 --> 00:14:06,560
 yourself from attachment.

232
00:14:06,560 --> 00:14:08,810
 And so when we talk about practicing mindfulness, that's

233
00:14:08,810 --> 00:14:10,280
 really what we're talking about.

234
00:14:10,280 --> 00:14:16,290
 Or it's one way of defining or describing mindfulness as

235
00:14:16,290 --> 00:14:19,520
 the practice that cultivates

236
00:14:19,520 --> 00:14:24,680
 contentment, where we're no longer attracted to certain

237
00:14:24,680 --> 00:14:27,920
 objects, we're no longer repulsed

238
00:14:27,920 --> 00:14:31,320
 by certain objects, the greed and the anger disappears

239
00:14:31,320 --> 00:14:35,680
 because there's clarity and wisdom

240
00:14:35,680 --> 00:14:36,680
 and contentment.

241
00:14:36,680 --> 00:14:42,580
 So when we experience our sights and sounds and smells and

242
00:14:42,580 --> 00:14:46,360
 tastes and feelings and thoughts,

243
00:14:46,360 --> 00:14:49,920
 we experience them objectively.

244
00:14:49,920 --> 00:14:54,700
 It's not meant to be something to think about or to share

245
00:14:54,700 --> 00:14:57,400
 with others or not something to

246
00:14:57,400 --> 00:15:02,280
 wear like a cloak and say, I'm a Buddhist, I'm a meditator.

247
00:15:02,280 --> 00:15:06,240
 Something to actually practice here and now.

248
00:15:06,240 --> 00:15:14,100
 If you don't do that, you're defiled as the Buddha said, n

249
00:15:14,100 --> 00:15:14,560
akiles e hi pandito, a wise

250
00:15:14,560 --> 00:15:18,650
 one is not defiled when they act, when they speak, when

251
00:15:18,650 --> 00:15:19,760
 they teach.

252
00:15:19,760 --> 00:15:27,510
 They teach purely with purity because inside their minds

253
00:15:27,510 --> 00:15:29,160
 are pure.

254
00:15:29,160 --> 00:15:35,820
 So a reminder to us, simple, it's not hard to understand,

255
00:15:35,820 --> 00:15:39,000
 but I think it's easy to lose

256
00:15:39,000 --> 00:15:41,080
 sight of what's most important.

257
00:15:41,080 --> 00:15:44,000
 It's very easy to slip out of the meditative state.

258
00:15:44,000 --> 00:15:49,530
 Really mindfulness is not an easy state for most of us to

259
00:15:49,530 --> 00:15:51,040
 cultivate.

260
00:15:51,040 --> 00:15:54,810
 We often have to remind ourselves again and again and be

261
00:15:54,810 --> 00:15:56,960
 reminded again and again to be

262
00:15:56,960 --> 00:16:01,300
 present what's happening now, to be with myself, to be

263
00:16:01,300 --> 00:16:05,080
 present and aware of my experience and

264
00:16:05,080 --> 00:16:08,640
 to see it clearly.

265
00:16:08,640 --> 00:16:12,370
 And so this technique of using the mantra of reminding

266
00:16:12,370 --> 00:16:14,880
 ourselves, this is seeing, this

267
00:16:14,880 --> 00:16:20,230
 is hearing, and so on, is for this express purpose of

268
00:16:20,230 --> 00:16:23,520
 bringing us back again and again

269
00:16:23,520 --> 00:16:26,640
 to the here and the now so that we set ourselves in the

270
00:16:26,640 --> 00:16:27,520
 right way.

271
00:16:27,520 --> 00:16:30,600
 And then whatever we do, if we help others, if we teach

272
00:16:30,600 --> 00:16:32,560
 others, if we interact with the

273
00:16:32,560 --> 00:16:35,920
 world, it's from a pure source.

274
00:16:35,920 --> 00:16:38,840
 Just like a river, the river has a source.

275
00:16:38,840 --> 00:16:42,400
 If the source is impure, it doesn't matter what part of the

276
00:16:42,400 --> 00:16:44,120
 river you drink from, it's

277
00:16:44,120 --> 00:16:46,120
 all going to be impure.

278
00:16:46,120 --> 00:16:52,730
 It's only when the source is pure that the river stream can

279
00:16:52,730 --> 00:16:54,080
 be pure.

280
00:16:54,080 --> 00:16:55,080
 So there you go.

281
00:16:55,080 --> 00:16:57,080
 That's the Dhammapada for this evening.

282
00:16:57,080 --> 00:16:59,080
 Thank you all for tuning in.

283
00:16:59,080 --> 00:17:00,080
 Wish you all the best.

284
00:17:00,080 --> 00:17:01,080
 Thank you.

285
00:17:01,080 --> 00:17:17,160
 [

286
00:17:17,160 --> 00:17:33,240
 1

287
00:17:56,240 --> 00:18:15,320
 . . . ]

288
00:18:15,320 --> 00:18:31,400
 [

289
00:18:31,400 --> 00:18:54,320
 . . . ]

290
00:18:54,320 --> 00:18:58,800
 You can't hear me in second life?

291
00:18:58,800 --> 00:19:05,800
 My bars are moving.

292
00:19:05,800 --> 00:19:13,240
 Hello, hello.

293
00:19:13,240 --> 00:19:19,000
 Testing, testing.

294
00:19:19,000 --> 00:19:29,280
 That's strange.

295
00:19:29,280 --> 00:19:31,280
 Real technical difficulties.

296
00:19:31,280 --> 00:19:41,280
 Well, I have a dot over me.

297
00:19:41,280 --> 00:19:51,160
 Oh, I have a dot, but none of you do.

298
00:19:51,160 --> 00:19:52,160
 What does that say?

299
00:19:52,160 --> 00:19:56,790
 It says I must be on some kind of other server or something

300
00:19:56,790 --> 00:19:57,160
.

301
00:19:57,160 --> 00:20:01,270
 Okay, well, I'm going to go figure out what I have to

302
00:20:01,270 --> 00:20:02,480
 figure out.

303
00:20:02,480 --> 00:20:10,430
 I think a YouTube video microphone wasn't working for some

304
00:20:10,430 --> 00:20:12,480
 odd reason.

305
00:20:12,480 --> 00:20:13,480
 I had to figure that out.

306
00:20:13,480 --> 00:20:14,480
 Okay, good night, everyone.

307
00:20:14,480 --> 00:20:19,480
 Thank you all for coming.

308
00:20:19,480 --> 00:20:20,480
 Thank you.

309
00:20:20,480 --> 00:20:21,480
 [ 1

310
00:20:21,480 --> 00:20:22,480
 . . . ]

311
00:20:22,480 --> 00:20:23,480
 [ . . . ]

312
00:20:23,480 --> 00:20:24,480
 [ . . . ]

313
00:20:24,480 --> 00:20:25,480
 [ . . . ]

314
00:20:25,480 --> 00:20:26,480
 [ . . . ]

315
00:20:26,480 --> 00:20:27,480
 [ . . . ]

316
00:20:27,480 --> 00:20:28,480
 [ . . . ]

317
00:20:28,480 --> 00:20:29,480
 [ . . . ]

318
00:20:29,480 --> 00:20:30,480
 [ . . . ]

319
00:20:30,480 --> 00:20:31,480
 [ . . . ]

