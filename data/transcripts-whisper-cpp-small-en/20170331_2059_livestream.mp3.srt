1
00:00:00,000 --> 00:00:10,160
 Okay, good evening everyone.

2
00:00:10,160 --> 00:00:18,080
 Welcome to our evening Dhamma.

3
00:00:18,080 --> 00:00:30,320
 I've been thinking a bit about society.

4
00:00:30,320 --> 00:00:39,800
 I find myself in society to a limited extent.

5
00:00:39,800 --> 00:00:45,680
 In going to university I meet people who I might otherwise

6
00:00:45,680 --> 00:00:47,760
 not meet, get a taste of the

7
00:00:47,760 --> 00:00:59,720
 diversity of character types in the world.

8
00:00:59,720 --> 00:01:02,310
 It's quite a blessing to live in a monastery in a

9
00:01:02,310 --> 00:01:04,960
 meditation center to meet only with wonderful

10
00:01:04,960 --> 00:01:10,480
 people who are keen on bettering themselves.

11
00:01:10,480 --> 00:01:18,920
 To not have to worry about things like lying or cheating or

12
00:01:18,920 --> 00:01:23,400
 stealing or manipulating.

13
00:01:23,400 --> 00:01:30,480
 To live in a world that is for the most part drama free.

14
00:01:30,480 --> 00:01:36,960
 Free from all the problems associated with ambition and

15
00:01:36,960 --> 00:01:41,080
 passion and arrogance and conceit

16
00:01:41,080 --> 00:01:45,160
 and so on.

17
00:01:45,160 --> 00:01:48,310
 Not that we all don't in the meditations enter, that there

18
00:01:48,310 --> 00:01:52,800
 aren't defilements but they're

19
00:01:52,800 --> 00:02:00,640
 on a whole greater level of refinement or subtle level.

20
00:02:00,640 --> 00:02:09,910
 When you go out in the world you get to see a taste of the

21
00:02:09,910 --> 00:02:12,720
 unmindful society and the unmindful

22
00:02:12,720 --> 00:02:22,640
 and unmindful environment.

23
00:02:22,640 --> 00:02:38,940
 Not just unmindful but filled with people whose views are

24
00:02:38,940 --> 00:02:43,760
 far from mine.

25
00:02:43,760 --> 00:02:46,700
 I think either way whether we talk about a Buddhist society

26
00:02:46,700 --> 00:02:48,000
 or whether we're talking

27
00:02:48,000 --> 00:03:00,690
 about society in general, the greater non-Buddhist or

28
00:03:00,690 --> 00:03:10,520
 pluralistic society, global society.

29
00:03:10,520 --> 00:03:17,600
 Our intentions remain the same generally, it's to get along

30
00:03:17,600 --> 00:03:23,880
, to have peace, to find happiness.

31
00:03:23,880 --> 00:03:28,970
 I think in general there is movement in societies to try

32
00:03:28,970 --> 00:03:35,480
 and find some sort of peace and happiness.

33
00:03:35,480 --> 00:03:39,980
 Sometimes it can be rather selfish, the people in power are

34
00:03:39,980 --> 00:03:42,680
 not so concerned with the unwashed

35
00:03:42,680 --> 00:03:47,010
 masses, only concerned with their own peace, their own

36
00:03:47,010 --> 00:03:48,120
 happiness.

37
00:03:48,120 --> 00:03:55,800
 So maintaining the peace means oppressing people, that kind

38
00:03:55,800 --> 00:03:57,360
 of thing.

39
00:03:57,360 --> 00:04:00,590
 Let's put it this way as Buddhists, whether we live in a

40
00:04:00,590 --> 00:04:02,600
 meditation center, whether we

41
00:04:02,600 --> 00:04:10,440
 live in society, our goal remains the same, to get along,

42
00:04:10,440 --> 00:04:14,320
 to have a peaceful society,

43
00:04:14,320 --> 00:04:19,800
 a kind and compassionate society.

44
00:04:19,800 --> 00:04:28,020
 They're the same principles that we aspire to here in the

45
00:04:28,020 --> 00:04:32,360
 meditation center of course.

46
00:04:32,360 --> 00:04:41,480
 Hopefully much more successfully in the meditation center.

47
00:04:41,480 --> 00:04:44,510
 Unless something for us to remember, something for us to

48
00:04:44,510 --> 00:04:46,400
 think of and something maybe even

49
00:04:46,400 --> 00:04:53,000
 to see as a positive outcome of our meditation practice.

50
00:04:53,000 --> 00:04:56,890
 The Buddha talked about something called the Saraniya Dham

51
00:04:56,890 --> 00:04:59,480
ma, I've talked about this before.

52
00:04:59,480 --> 00:05:02,520
 I think I'm inevitably going to repeat myself again and

53
00:05:02,520 --> 00:05:07,920
 again as I make more and more videos.

54
00:05:07,920 --> 00:05:19,960
 The Saraniya Dhamma are dhammas that make us think well of

55
00:05:19,960 --> 00:05:29,040
 each other or that

56
00:05:29,040 --> 00:05:36,460
 make us worthy of friendship and that allow us to think

57
00:05:36,460 --> 00:05:39,480
 fondly of each other and to have

58
00:05:39,480 --> 00:05:53,560
 a peaceful and harmonious society or community.

59
00:05:53,560 --> 00:05:56,500
 Six of them, six Saraniya Dhammas, simple teaching,

60
00:05:56,500 --> 00:05:58,880
 something for us to keep in mind.

61
00:05:58,880 --> 00:06:09,120
 The first one is bodily acts of kindness.

62
00:06:09,120 --> 00:06:14,530
 Now with kindness of any sort, this isn't the practice of V

63
00:06:14,530 --> 00:06:16,720
ipassana but it's in many

64
00:06:16,720 --> 00:06:21,080
 ways the outcome of a practice of insight meditation.

65
00:06:21,080 --> 00:06:27,390
 Something for us to strive to strive for is to be a kinder

66
00:06:27,390 --> 00:06:30,400
 person, to do things.

67
00:06:30,400 --> 00:06:36,160
 So when we live in a society where everyone is observant

68
00:06:36,160 --> 00:06:39,920
 and caring and outgoing, noticing

69
00:06:39,920 --> 00:06:46,060
 when there's a need and working to fulfill that need with

70
00:06:46,060 --> 00:06:47,600
 kindness.

71
00:06:47,600 --> 00:06:52,240
 Someone who is hungry, feed them food.

72
00:06:52,240 --> 00:07:01,440
 Someone who is poor, give them money.

73
00:07:01,440 --> 00:07:05,160
 Someone who is scared, provide them with shelter and so on.

74
00:07:05,160 --> 00:07:17,390
 Someone who is confused, provide them with insight, with

75
00:07:17,390 --> 00:07:19,640
 wisdom.

76
00:07:19,640 --> 00:07:21,680
 So the first one is bodily acts of kindness.

77
00:07:21,680 --> 00:07:26,040
 This is actually doing things that benefit others.

78
00:07:26,040 --> 00:07:31,270
 Giving them food, giving them help, helping old people

79
00:07:31,270 --> 00:07:34,280
 across the street, that sort of

80
00:07:34,280 --> 00:07:35,840
 thing.

81
00:07:35,840 --> 00:07:41,090
 Helping your parents, helping your grandparents, helping

82
00:07:41,090 --> 00:07:44,440
 your children, helping your friends.

83
00:07:44,440 --> 00:07:48,940
 This is how we'd expect to work in a monastery as well,

84
00:07:48,940 --> 00:07:51,360
 monks helping each other.

85
00:07:51,360 --> 00:07:54,920
 The story of these monks who didn't talk to each other.

86
00:07:54,920 --> 00:07:58,740
 They would only talk once every five days they would get

87
00:07:58,740 --> 00:07:59,760
 together.

88
00:07:59,760 --> 00:08:03,040
 And the rest of the time they wouldn't open their mouths.

89
00:08:03,040 --> 00:08:06,560
 If there was a chore they would just beckon with their

90
00:08:06,560 --> 00:08:08,640
 hands and not say anything.

91
00:08:08,640 --> 00:08:14,440
 Just say, just use the hands to beckon to them.

92
00:08:14,440 --> 00:08:20,310
 When it was time to eat, the first person there would set

93
00:08:20,310 --> 00:08:23,280
 out the seats for eating and

94
00:08:23,280 --> 00:08:24,280
 eat.

95
00:08:24,280 --> 00:08:27,940
 The last person to come back from alms round would clean up

96
00:08:27,940 --> 00:08:28,120
.

97
00:08:28,120 --> 00:08:31,630
 There was no communication, except once every five days

98
00:08:31,630 --> 00:08:33,840
 they would get together and recite

99
00:08:33,840 --> 00:08:35,840
 and memorize the Buddha's teaching.

100
00:08:35,840 --> 00:08:46,040
 Go over it again and again and talk about it.

101
00:08:46,040 --> 00:08:47,040
 But acts of kindness.

102
00:08:47,040 --> 00:08:51,640
 The second one of course is words of kindness.

103
00:08:51,640 --> 00:08:53,240
 Words spoken out of kindness.

104
00:08:53,240 --> 00:08:58,960
 So it's not just kind words like, "May you be happy."

105
00:08:58,960 --> 00:09:05,640
 But it can be advice, but given out of kindness.

106
00:09:05,640 --> 00:09:11,160
 Any speech, the intention of which is the benefit of the

107
00:09:11,160 --> 00:09:12,200
 other.

108
00:09:12,200 --> 00:09:13,680
 We speak with thoughtfulness.

109
00:09:13,680 --> 00:09:14,680
 We don't speak.

110
00:09:14,680 --> 00:09:16,680
 We try not to speak.

111
00:09:16,680 --> 00:09:20,120
 But that is out of a desire to hurt, to harm.

112
00:09:20,120 --> 00:09:25,650
 Out of a desire to assert our dominance, arrogance and

113
00:09:25,650 --> 00:09:26,920
 conceit.

114
00:09:26,920 --> 00:09:33,080
 To cultivate fear or jealousy.

115
00:09:33,080 --> 00:09:36,720
 How much of our speech is just designed to make us look

116
00:09:36,720 --> 00:09:37,360
 good.

117
00:09:37,360 --> 00:09:38,360
 Impress others.

118
00:09:38,360 --> 00:09:42,920
 Which of course is just designed in the same way to make

119
00:09:42,920 --> 00:09:44,920
 them feel inferior.

120
00:09:44,920 --> 00:09:52,520
 I think it's innocent to want people to look up to you.

121
00:09:52,520 --> 00:09:58,920
 But all it does is make them, encourage them to resent.

122
00:09:58,920 --> 00:10:03,480
 Not a very wholesome.

123
00:10:03,480 --> 00:10:10,920
 It's quite selfish.

124
00:10:10,920 --> 00:10:14,340
 Impress is praising others when you see someone doing

125
00:10:14,340 --> 00:10:16,120
 something good to be happy for them.

126
00:10:16,120 --> 00:10:21,160
 To encourage them and to compliment them in a true way.

127
00:10:21,160 --> 00:10:25,560
 Not flattering trying to make them like you.

128
00:10:25,560 --> 00:10:28,600
 But affirming the good in them.

129
00:10:28,600 --> 00:10:31,160
 That they might be wavering about, "Did I do a good thing?"

130
00:10:31,160 --> 00:10:34,360
 "Good for you, you did a good thing."

131
00:10:34,360 --> 00:10:38,360
 That's kind of kindness of speech.

132
00:10:38,360 --> 00:10:50,040
 The third one of course, kindness of thoughts.

133
00:10:50,040 --> 00:10:57,040
 We think of each other kindly.

134
00:10:57,040 --> 00:10:59,480
 That which leads to the other two.

135
00:10:59,480 --> 00:11:02,560
 To all acts and deeds of kindness of course.

136
00:11:02,560 --> 00:11:04,120
 Has to be thoughts of kindness.

137
00:11:04,120 --> 00:11:08,800
 It's actually possible to be kind with body and speech and

138
00:11:08,800 --> 00:11:10,560
 yet hate someone.

139
00:11:10,560 --> 00:11:15,920
 Or be very corrupt in mind.

140
00:11:15,920 --> 00:11:17,920
 Because we can suppress it.

141
00:11:17,920 --> 00:11:19,440
 But it's not enough.

142
00:11:19,440 --> 00:11:22,120
 It's not true and it's not real.

143
00:11:22,120 --> 00:11:27,160
 It's not sincere, right?

144
00:11:27,160 --> 00:11:29,870
 Much more important than our acts and our speeches is our

145
00:11:29,870 --> 00:11:33,960
 thoughts and our intentions

146
00:11:33,960 --> 00:11:37,960
 are our state of mind.

147
00:11:37,960 --> 00:11:41,560
 That's what we see in meditation now.

148
00:11:41,560 --> 00:11:43,920
 Ultimately we're going to become whatever we think about.

149
00:11:43,920 --> 00:11:48,880
 We're going to, not what we think about, we're going to

150
00:11:48,880 --> 00:11:52,200
 become physically and verbally.

151
00:11:52,200 --> 00:11:56,770
 Our acts and our speech are going to come inevitably from

152
00:11:56,770 --> 00:11:57,800
 our mind.

153
00:11:57,800 --> 00:12:02,360
 If our mind is impure, inevitably speech and acts are going

154
00:12:02,360 --> 00:12:03,640
 to be impure.

155
00:12:03,640 --> 00:12:04,960
 Going to be corrupt.

156
00:12:04,960 --> 00:12:09,160
 Going to be cruel, unpleasant.

157
00:12:09,160 --> 00:12:15,080
 Cause for stress and suffering.

158
00:12:15,080 --> 00:12:16,960
 So even just cultivating thoughts of love.

159
00:12:16,960 --> 00:12:20,390
 I mean this is a great sort of artificial way of

160
00:12:20,390 --> 00:12:23,200
 cultivating positive thoughts.

161
00:12:23,200 --> 00:12:24,880
 Thinking about all the people around you.

162
00:12:24,880 --> 00:12:25,880
 May they be happy.

163
00:12:25,880 --> 00:12:30,600
 May they be free from suffering.

164
00:12:30,600 --> 00:12:33,360
 And of course cultivating insight.

165
00:12:33,360 --> 00:12:40,840
 More natural, more sustainable, more true.

166
00:12:40,840 --> 00:12:42,320
 Because it makes you a nicer person.

167
00:12:42,320 --> 00:12:46,970
 It helps you see the disadvantages of being a cruel and

168
00:12:46,970 --> 00:12:50,560
 manipulative and unpleasant person.

169
00:12:50,560 --> 00:12:51,560
 You don't have to believe it.

170
00:12:51,560 --> 00:12:52,560
 You don't have to pretend.

171
00:12:52,560 --> 00:12:55,840
 You don't have to force yourself to be good.

172
00:12:55,840 --> 00:12:59,290
 If you're trying to force yourself to be pure, to be free,

173
00:12:59,290 --> 00:13:00,680
 to be enlightened.

174
00:13:00,680 --> 00:13:03,430
 If you're pushing for it, you can know that you're actually

175
00:13:03,430 --> 00:13:04,920
 quite far from enlightenment

176
00:13:04,920 --> 00:13:07,520
 and not going in the right direction.

177
00:13:07,520 --> 00:13:11,950
 It's only when you stop forcing, when you start looking,

178
00:13:11,950 --> 00:13:14,120
 and when you begin to see.

179
00:13:14,120 --> 00:13:17,120
 And you begin to see the difference between right and wrong

180
00:13:17,120 --> 00:13:18,600
 and good and bad without any

181
00:13:18,600 --> 00:13:21,880
 reference to anyone's beliefs or teachings.

182
00:13:21,880 --> 00:13:28,490
 It's only then that you can truly become a good person and

183
00:13:28,490 --> 00:13:31,520
 have pure thoughts that are

184
00:13:31,520 --> 00:13:39,360
 natural and sincere.

185
00:13:39,360 --> 00:13:48,720
 The fourth is sharing your possessions.

186
00:13:48,720 --> 00:13:53,080
 Let's say communion, let's make it more general.

187
00:13:53,080 --> 00:13:57,580
 So it refers to anything from what you get, share it with

188
00:13:57,580 --> 00:13:59,080
 the community.

189
00:13:59,080 --> 00:14:03,760
 I mean taxation, voluntary taxation I suppose.

190
00:14:03,760 --> 00:14:06,390
 People don't like many people or there are some people who

191
00:14:06,390 --> 00:14:07,920
 are critical of taxation saying

192
00:14:07,920 --> 00:14:09,920
 it's like forced.

193
00:14:09,920 --> 00:14:13,520
 But I don't look at it so much as forced.

194
00:14:13,520 --> 00:14:16,760
 Taxation is an agreement that we've come to as a society

195
00:14:16,760 --> 00:14:18,400
 that we haven't...

