 Okay, so welcome everyone to this week's episode of Monk
 Radio. Time for announcements.
 I've got a couple of announcements. One, that I hit two
 million views on YouTube,
 which is an interesting thing to think about, because it
 really isn't that big of a number.
 I think there are videos on YouTube with like 10 million
 views, one video.
 So it really doesn't have that much significance.
 But on the other hand, if you think of two million, I don't
 know, two million video views,
 I mean it says something. It says that people are
 interested in Buddhism, interested in meditation,
 and interested in pornography and masturbation, because
 that's one of my most famous videos.
 It's kind of sad and depressing in a way.
 But the number one video is about meditation. It's not the
 number one video.
 It's the number two video. So, yeah.
 I mean, it's good to laugh about such things. We take them
 too seriously and that causes a lot of the problems that
 they are blamed for.
 It's our stress and our feelings of guilt that cause most
 of the suffering.
 When we overcome that, the problems themselves are not that
 insurmountable.
 I suppose they are difficult. I mean, Buddha said sens
uality is the greatest bind that there is.
 Ropes and thongs and chains are weak in comparison to the
 bond of sensuality.
 So it's not something that you should take lightly for sure
.
 But it's not something you should feel guilty and hit
 yourself over, either.
 So, okay, anyway, on track that was two million views.
 So here's to another two million.
 And hopefully with everyone's help here, we'll have some
 good videos to start us off in that direction this week.
 Another announcement. I mentioned it yesterday, I think,
 that we have a candidate for ordination.
 And the big announcement today, different from yesterday,
 is that he's going to be flying back to Finland,
 probably flying back to Finland in the very near future in
 order to ordain quicker than we thought.
 So possibly ordain with me here in Sri Lanka before I leave
 for Thailand and then go with me to Thailand, which would
 be great.
 It would be great to have a companion. And I think there's
 a Sri Lankan monk joining us as well,
 who wants to go and meditate in the forests of Thailand,
 which would be great.
 I'm wondering why he doesn't meditate in the forest here,
 but I think it's hard to meditate in your own country
 sometimes
 because too many relatives and friends and so on.
 When you go to a foreign country, you have to take it more
 seriously.
 And the other man who's staying here, George, is leaving
 soon.
 His grandmother died and he has to go back for... well, he
 was only going to stay till Friday anyway, but he could be
 going back early.
 But the reason I mention him is because he's an interesting
 case.
 Everyone's running away, having very much difficulty
 staying in the forest.
 He was one of those people, men mainly actually, the women
 don't have such troubles.
 So last week he was quite ready to leave and then he
 suddenly had a breakthrough and now he doesn't want to
 leave.
 So he was asking me just now before I started, "What's he
 going to do? He's not going to be able to finish the course
. How's he going to continue?"
 And I said, "Well, just come back. We can talk over Skype
 or on the internet somehow and get practicing."
 But I said, "Now you've made it. You made it over the
 biggest hurdle, which is the one thing to leave hurdle."
 Once you get over that and really appreciate the meditation
 for what it is, then nothing will stop you.
 Nothing can stand in your way. You just have to, as long as
 you can continue practicing, you've faced the worst of it.
 So those are two announcements, or a few announcements.
 Apart from that, everything is continuing. Sankh Sara is
 still here.
 Anybody have anything they want to say? Any announcements
 that they have that they'd like to share?
 Anybody on our panel?
 Paulina is still planning to join us, I think, in October.
 She may be chasing her mind.
 So there's an announcement. We might have a new...
 Wonderful.
 Ah, yes, that's right. She's already made... Sorry, I'm bad
 memory.
 She's already dedicating herself to it. She's great.
 Marion also, there's another person who was here yesterday.
 She's on her retreat today.
 That's why she's not joining us, and her plan is to come as
 well.
 So lots of good things happening. There's a couple of men
 who...
 Well, we have Laurie, who I mentioned already. We've got
 other people who have expressed interest in ordaining,
 and people who just want to come to meditate in Thailand.
 So lots of good things happening.
 Meechi Jai is here, the Burmese nun. And she said to me,
 she really likes this place.
 She agrees with me on many of the aspects of how great a
 country it is to live in as a monastic.
 And so I think her plan is to come back and bring some
 friends. She has some supporters in Chiang Mai,
 who she wants to bring as well, who want to come.
 So lots of good stuff happening. Just continuing on with
 our work and practice.
 So that's announcements for this week.
