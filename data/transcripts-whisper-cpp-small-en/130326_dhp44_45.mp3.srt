1
00:00:00,000 --> 00:00:05,660
 Okay, hello and welcome back to our study of the Dhammapada

2
00:00:05,660 --> 00:00:05,760
.

3
00:00:05,760 --> 00:00:11,440
 Today we will be continuing on with verses 44 and 45.

4
00:00:11,440 --> 00:00:15,600
 It's a paraverasas which go as follows.

5
00:00:15,600 --> 00:00:21,500
 Kao imang patawing vicay sati, yamalo kang imang sadeva k

6
00:00:21,500 --> 00:00:29,820
ang. Kao dhammapadaang sudei satang, kusolopupamiva pache

7
00:00:29,820 --> 00:00:31,200
 sati.

8
00:00:31,200 --> 00:00:38,220
 Se kao imang patawing vicay sati, yamalo kang imang sadeva

9
00:00:38,220 --> 00:00:45,930
 kang. Se kao dhammapadaang sudei satang, kusolopamiva pache

10
00:00:45,930 --> 00:00:46,800
 sati.

11
00:00:46,800 --> 00:00:54,940
 Which means, it's a question and answer. The first verse is

12
00:00:54,940 --> 00:00:56,800
, kao imang patawing vicay sati.

13
00:00:56,800 --> 00:01:02,540
 Who will overcome this world, yamalo kang imang sadeva kang

14
00:01:02,540 --> 00:01:02,960
?

15
00:01:02,960 --> 00:01:07,640
 Who will overcome this world that is, who will overcome

16
00:01:07,640 --> 00:01:08,800
 this earth, sorry?

17
00:01:08,800 --> 00:01:14,490
 Who will overcome this earth, this world of yama, the Lord

18
00:01:14,490 --> 00:01:20,720
 of death, along with the angels, sadeva kang?

19
00:01:20,720 --> 00:01:26,800
 Kao dhammapadaang sudei satang, kusolopupamiva pache sati.

20
00:01:26,800 --> 00:01:28,320
 Who will bring to perfection this path of dhamma,

21
00:01:28,320 --> 00:01:33,490
 or this practice of dhamma that is well taught, just as a

22
00:01:33,490 --> 00:01:41,340
 skillful person might bring to perfection a bouquet of

23
00:01:41,340 --> 00:01:44,080
 flowers?

24
00:01:44,080 --> 00:01:48,910
 And then the answer is, se kao, which means a trainee or

25
00:01:48,910 --> 00:01:53,680
 one who is training, one who trains themselves,

26
00:01:53,680 --> 00:02:00,070
 will overcome this earth, this world of yama, together with

27
00:02:00,070 --> 00:02:01,040
 the angels,

28
00:02:01,040 --> 00:02:07,840
 together with its angels, or all celestial beings.

29
00:02:07,840 --> 00:02:12,360
 The trainer will bring to perfection this path of dhamma,

30
00:02:12,360 --> 00:02:16,720
 which is well expounded, just as a skillful person would

31
00:02:16,720 --> 00:02:24,960
 bring to perfection a bouquet of flowers.

32
00:02:24,960 --> 00:02:32,800
 So this is actually something, it's quite a profound

33
00:02:32,800 --> 00:02:34,880
 question and answer,

34
00:02:34,880 --> 00:02:40,320
 something that we can use here to just to expound the dham

35
00:02:40,320 --> 00:02:44,240
ma or as a basis for teaching the dhamma.

36
00:02:44,240 --> 00:02:48,300
 But the story, as there's a story behind all of these,

37
00:02:48,300 --> 00:02:51,680
 first we have to say where this first came from.

38
00:02:51,680 --> 00:02:57,560
 The story is that there were, it's actually a two paragraph

39
00:02:57,560 --> 00:03:01,760
 story, the story goes that there were a bunch of monks

40
00:03:01,760 --> 00:03:06,720
 who were sitting around talking about the earth.

41
00:03:06,720 --> 00:03:12,470
 And as monks are people as well, their talk tends to wander

42
00:03:12,470 --> 00:03:14,880
 if they're not careful.

43
00:03:14,880 --> 00:03:18,400
 And so their talk had wandered into worldly subjects where

44
00:03:18,400 --> 00:03:20,480
 they began to talk about the earth.

45
00:03:20,480 --> 00:03:23,840
 And so they were talking about how the earth in this

46
00:03:23,840 --> 00:03:27,200
 district was black and the earth in that district was brown

47
00:03:27,200 --> 00:03:27,200
,

48
00:03:27,200 --> 00:03:32,370
 and how this district was a very muddy earth and this

49
00:03:32,370 --> 00:03:38,560
 district had gravelly and sandy earth.

50
00:03:38,560 --> 00:03:40,970
 And the Buddha came in and asked them what they were

51
00:03:40,970 --> 00:03:42,960
 talking about and they told him and he said that.

52
00:03:42,960 --> 00:03:48,260
 He used this verse as a reminder as a means of instilling

53
00:03:48,260 --> 00:03:52,160
 some sort of sense of urgency in them.

54
00:03:52,160 --> 00:03:57,110
 He said, well that's the external earth but it really has

55
00:03:57,110 --> 00:04:00,960
 no purpose for you to consider,

56
00:04:00,960 --> 00:04:05,220
 or to concern yourselves with when compared to the inner

57
00:04:05,220 --> 00:04:07,600
 earth, the earth that is inside of you.

58
00:04:07,600 --> 00:04:11,340
 And he said the real earth is this world of death, the

59
00:04:11,340 --> 00:04:18,160
 physical realm that is subject to change

60
00:04:18,160 --> 00:04:28,160
 and is aging and fading away and this body that will

61
00:04:28,160 --> 00:04:31,920
 eventually go back to the earth.

62
00:04:31,920 --> 00:04:35,620
 The death that is ahead of us, or this world which is

63
00:04:35,620 --> 00:04:38,720
 encompassed from the world of,

64
00:04:38,720 --> 00:04:42,000
 from the lower realms all the way up to the angel realms.

65
00:04:42,000 --> 00:04:45,760
 So Yamaloka is considered to mean the lower realms of the

66
00:04:45,760 --> 00:04:49,920
 hell realms, the animal realms, the ghost realms.

67
00:04:49,920 --> 00:04:53,390
 The Deva Loka, the Deva Kang means the angel realms, the

68
00:04:53,390 --> 00:04:57,040
 human realms, the angels and the gods and so on.

69
00:04:57,040 --> 00:05:06,800
 Not in song but those three.

70
00:05:06,800 --> 00:05:10,480
 So he said, this is what you should be focusing on.

71
00:05:10,480 --> 00:05:13,710
 Here you have something that is much more important and you

72
00:05:13,710 --> 00:05:19,600
 should be concerning yourself with how to understand this

73
00:05:19,600 --> 00:05:21,360
 earth.

74
00:05:21,360 --> 00:05:26,570
 And so he gave this verse, he said, who can overcome this

75
00:05:26,570 --> 00:05:28,160
 earth?

76
00:05:28,160 --> 00:05:33,410
 He made them think about this, think about the Dhamma in

77
00:05:33,410 --> 00:05:35,200
 this way and said, who will do this?

78
00:05:35,200 --> 00:05:38,490
 Who will be able to overcome this? Certainly not the people

79
00:05:38,490 --> 00:05:41,200
 who sit around talking about useless subject.

80
00:05:41,200 --> 00:05:44,730
 Who will be able to overcome this earth? Whether it be the

81
00:05:44,730 --> 00:05:47,360
 lower realms or the higher realms.

82
00:05:47,360 --> 00:05:49,860
 Who will be able to put into practice and bring to

83
00:05:49,860 --> 00:05:55,200
 perfection this path of Dhamma, this path of truth?

84
00:05:55,200 --> 00:06:00,920
 This path to realize the truth just as a skillful person

85
00:06:00,920 --> 00:06:06,540
 would bring to perfection or make a perfect bouquet of

86
00:06:06,540 --> 00:06:07,920
 flowers.

87
00:06:07,920 --> 00:06:14,730
 I think this is useful because it, or quite profound,

88
00:06:14,730 --> 00:06:19,930
 because it asks the question of how, of who is it or how

89
00:06:19,930 --> 00:06:22,080
 can we,

90
00:06:22,080 --> 00:06:26,050
 how can we address the issues that we're faced, how can we

91
00:06:26,050 --> 00:06:28,480
 address the problem that we're faced with?

92
00:06:28,480 --> 00:06:38,910
 This suffering that we have ahead of us, the old age,

93
00:06:38,910 --> 00:06:44,180
 sickness and death, how do we address the issue of being

94
00:06:44,180 --> 00:06:45,040
 mortal?

95
00:06:45,040 --> 00:06:49,520
 Or how do we address the issue of being subject to

96
00:06:49,520 --> 00:06:50,960
 suffering?

97
00:06:50,960 --> 00:06:55,280
 And how do we, once we understand what is the path, who is

98
00:06:55,280 --> 00:07:00,870
 it or what do we need to do in order to perfect the

99
00:07:00,870 --> 00:07:01,520
 practice,

100
00:07:01,520 --> 00:07:06,400
 perfect the path to become free from suffering?

101
00:07:06,400 --> 00:07:11,670
 And the Buddha makes a direct definite statement as to what

102
00:07:11,670 --> 00:07:15,200
 it requires, what it will require for us to do that.

103
00:07:15,200 --> 00:07:18,250
 And this is the training that a person who trains

104
00:07:18,250 --> 00:07:22,080
 themselves is one who will be able to overcome the earth,

105
00:07:22,080 --> 00:07:28,400
 who will be able to overcome old age, sickness and death.

106
00:07:28,400 --> 00:07:31,520
 A person who trains their mind, who trains themselves in

107
00:07:31,520 --> 00:07:35,520
 morality, who trains themselves in concentration,

108
00:07:35,520 --> 00:07:40,130
 who trains themselves in wisdom, this is a person who will

109
00:07:40,130 --> 00:07:45,120
 be able to overcome old age, sickness and death.

110
00:07:45,120 --> 00:07:47,880
 This is a person who will be able to put into practice the

111
00:07:47,880 --> 00:07:52,740
 Buddha's teaching, who will be able to bring to perfection

112
00:07:52,740 --> 00:07:55,040
 the Buddha's teaching,

113
00:07:55,040 --> 00:08:04,720
 be able to bring to perfection this path of Dhamma.

114
00:08:04,720 --> 00:08:08,880
 The distinction that we have to make is between the mundane

115
00:08:08,880 --> 00:08:11,200
 world and the ultimate reality.

116
00:08:11,200 --> 00:08:16,260
 This is where the Buddha points them in the right direction

117
00:08:16,260 --> 00:08:16,640
.

118
00:08:16,640 --> 00:08:19,970
 An ordinary person will tend to think that there is no way

119
00:08:19,970 --> 00:08:22,720
 to overcome old age, sickness and death.

120
00:08:22,720 --> 00:08:26,110
 These are part of life. You think, well, that's part of the

121
00:08:26,110 --> 00:08:26,800
 bargain.

122
00:08:26,800 --> 00:08:31,200
 You get to enjoy life, this life that you have, and you

123
00:08:31,200 --> 00:08:35,840
 have to accept the fact that it's going to end in old age,

124
00:08:35,840 --> 00:08:37,760
 sickness and death.

125
00:08:37,760 --> 00:08:40,440
 We think in conventional sense, just as these monks were

126
00:08:40,440 --> 00:08:42,560
 thinking about the conventional earth.

127
00:08:42,560 --> 00:08:47,790
 It's actually quite an apt story because there's a parallel

128
00:08:47,790 --> 00:08:51,040
 in how we look at our lives as well.

129
00:08:51,040 --> 00:08:54,960
 We look at life as being a one shot.

130
00:08:54,960 --> 00:08:58,720
 Even people who claim to believe in the afterlife and so on

131
00:08:58,720 --> 00:09:02,080
 will tend to focus all of their efforts,

132
00:09:02,080 --> 00:09:07,910
 or most of their efforts, on finding pleasure and avoiding

133
00:09:07,910 --> 00:09:10,160
 suffering in this life,

134
00:09:10,160 --> 00:09:15,300
 trying their best to be free from sickness, trying their

135
00:09:15,300 --> 00:09:20,640
 best to obtain sensual gratification in this life.

136
00:09:20,640 --> 00:09:27,040
 Without making any thought of the bigger picture,

137
00:09:27,040 --> 00:09:31,080
 or the ultimate reality, what's really going on and what's

138
00:09:31,080 --> 00:09:32,640
 really happening.

139
00:09:32,640 --> 00:09:36,660
 You realize that actually old age, sickness and death is

140
00:09:36,660 --> 00:09:38,480
 not just an inevitability of life,

141
00:09:38,480 --> 00:09:43,500
 it's a constant reoccurrence and it comes with a cause, or

142
00:09:43,500 --> 00:09:44,880
 it comes because of a cause.

143
00:09:44,880 --> 00:09:47,680
 The cause is birth.

144
00:09:47,680 --> 00:09:52,230
 No, it's not enough to put up with old age, sickness and

145
00:09:52,230 --> 00:09:57,520
 death because that's one way of crashing against the shore.

146
00:09:57,520 --> 00:10:05,720
 If you're not able to overcome the things that lead you to

147
00:10:05,720 --> 00:10:06,640
 be born again,

148
00:10:06,640 --> 00:10:08,770
 then you have to put up with this again and again and again

149
00:10:08,770 --> 00:10:08,960
.

150
00:10:08,960 --> 00:10:13,590
 So putting up with it isn't enough or accepting it isn't

151
00:10:13,590 --> 00:10:14,560
 enough.

152
00:10:14,560 --> 00:10:23,300
 We create the cause for it to occur again and again for

153
00:10:23,300 --> 00:10:25,040
 eternity,

154
00:10:25,040 --> 00:10:29,300
 with our addiction, with our attachment, with our concern

155
00:10:29,300 --> 00:10:31,360
 for the mundane,

156
00:10:31,360 --> 00:10:36,120
 with all of our attachments, all of our obsessions, all of

157
00:10:36,120 --> 00:10:38,000
 our worries and our cares,

158
00:10:38,000 --> 00:10:42,860
 on a conventional level, we lose sight of the ultimate

159
00:10:42,860 --> 00:10:43,680
 reality.

160
00:10:43,680 --> 00:10:45,930
 We lose sight of what's really going on or what we're

161
00:10:45,930 --> 00:10:46,800
 really doing.

162
00:10:46,800 --> 00:10:49,900
 With the sight of the cycle, the bigger picture that what

163
00:10:49,900 --> 00:10:51,760
 we're creating here is actually a cycle.

164
00:10:51,760 --> 00:10:54,640
 It's not a one shot, it's not linear.

165
00:10:54,640 --> 00:10:57,650
 It's not like birth goes to death in the line and then that

166
00:10:57,650 --> 00:10:58,160
's it.

167
00:10:58,160 --> 00:11:02,780
 It's a cycle, so birth goes to death, which leads to birth

168
00:11:02,780 --> 00:11:03,360
 again,

169
00:11:03,360 --> 00:11:06,780
 just like waves crashing against the shore one after

170
00:11:06,780 --> 00:11:12,240
 another after another for forever.

171
00:11:12,240 --> 00:11:20,210
 So the Buddha points out that something needs to change

172
00:11:20,210 --> 00:11:22,960
 about the way we look at the world.

173
00:11:22,960 --> 00:11:28,660
 We can't let ourselves follow after our habits and are

174
00:11:28,660 --> 00:11:32,800
 following our heart, for example.

175
00:11:32,800 --> 00:11:36,970
 Something has to change about how we look at the world, how

176
00:11:36,970 --> 00:11:39,120
 we look at our lives.

177
00:11:39,120 --> 00:11:42,030
 This is what happens when someone comes to practice the

178
00:11:42,030 --> 00:11:43,120
 Buddha's teaching.

179
00:11:43,120 --> 00:11:47,010
 When we listen to the Dhamma, when we read the Buddha's

180
00:11:47,010 --> 00:11:47,760
 teaching,

181
00:11:47,760 --> 00:11:49,200
 we start to affect this change.

182
00:11:49,200 --> 00:11:53,500
 We realize that there's much more going on than meets the

183
00:11:53,500 --> 00:11:53,920
 eye,

184
00:11:53,920 --> 00:11:56,660
 that the things that we cling to are not actually bringing

185
00:11:56,660 --> 00:11:57,520
 us happiness.

186
00:11:57,520 --> 00:12:03,440
 They're creating a habit of clinging,

187
00:12:03,440 --> 00:12:09,080
 that the things that we bear with are not actually out of

188
00:12:09,080 --> 00:12:10,160
 patience.

189
00:12:10,160 --> 00:12:13,720
 It's actually with a lot of suffering and cultivating an

190
00:12:13,720 --> 00:12:16,560
 aversion towards things.

191
00:12:16,560 --> 00:12:21,640
 When we escape from pain by taking medication or by

192
00:12:21,640 --> 00:12:24,400
 avoiding it,

193
00:12:24,400 --> 00:12:29,490
 we actually cultivate greater and greater aversion towards

194
00:12:29,490 --> 00:12:30,560
 the pain.

195
00:12:30,560 --> 00:12:33,880
 So we start to realize that this is not the way out of

196
00:12:33,880 --> 00:12:34,880
 suffering.

197
00:12:34,880 --> 00:12:38,800
 We realize this when we begin to train ourselves, starting

198
00:12:38,800 --> 00:12:41,040
 with morality.

199
00:12:41,040 --> 00:12:49,840
 We start to realize that these habits are habit-forming and

200
00:12:49,840 --> 00:12:54,800
 become magnified the more we cultivate them.

201
00:12:54,800 --> 00:12:59,010
 And so we undertake morality, we undertake to prevent

202
00:12:59,010 --> 00:13:01,600
 ourselves, to stop ourselves

203
00:13:01,600 --> 00:13:05,680
 from following after the habits.

204
00:13:05,680 --> 00:13:10,770
 We train ourselves to be patient when we would otherwise

205
00:13:10,770 --> 00:13:12,000
 chase after something.

206
00:13:12,000 --> 00:13:14,750
 When we want something, instead of chasing after it, we

207
00:13:14,750 --> 00:13:15,440
 stop ourselves.

208
00:13:15,440 --> 00:13:19,040
 We bring ourselves back to the experience of it.

209
00:13:19,040 --> 00:13:21,530
 When you want something, you're very little involved with

210
00:13:21,530 --> 00:13:22,480
 that actual thing,

211
00:13:22,480 --> 00:13:25,980
 and you're much more involved with the thoughts and the

212
00:13:25,980 --> 00:13:28,320
 pleasures that arise in the mind.

213
00:13:28,320 --> 00:13:31,600
 And when you're able to bring yourself back,

214
00:13:31,600 --> 00:13:35,200
 the first step is bringing the mind back to reality.

215
00:13:35,200 --> 00:13:37,840
 It's called morality.

216
00:13:37,840 --> 00:13:40,790
 True morality is in the mind when you bring your mind back

217
00:13:40,790 --> 00:13:43,360
 to what's really happening,

218
00:13:43,360 --> 00:13:48,640
 what's really going on, the essence of the experience.

219
00:13:48,640 --> 00:13:52,890
 When you see something and you want it, you look at the

220
00:13:52,890 --> 00:13:54,160
 seeing and you look at the feeling

221
00:13:54,160 --> 00:13:59,780
 and you look at the wanting, and you see that it's really

222
00:13:59,780 --> 00:14:03,200
 just impersonal phenomena arising and ceasing.

223
00:14:03,200 --> 00:14:06,210
 You break it apart and you see that it's a cause for stress

224
00:14:06,210 --> 00:14:08,080
, a cause for suffering,

225
00:14:08,080 --> 00:14:11,920
 not a cause for happiness and contentment.

226
00:14:11,920 --> 00:14:16,810
 And you become content without, you become content with or

227
00:14:16,810 --> 00:14:17,600
 without.

228
00:14:17,600 --> 00:14:21,600
 And you create what we call morality.

229
00:14:21,600 --> 00:14:23,920
 And as a result of the morality, you begin to develop

230
00:14:23,920 --> 00:14:24,640
 concentration.

231
00:14:24,640 --> 00:14:26,640
 So you train yourself further.

232
00:14:26,640 --> 00:14:31,210
 Once you cultivate morality, you train yourself to keep the

233
00:14:31,210 --> 00:14:31,680
 mind.

234
00:14:31,680 --> 00:14:33,720
 Once you've brought the mind back to the present, you train

235
00:14:33,720 --> 00:14:36,800
 yourself to keep the mind focused on the present.

236
00:14:36,800 --> 00:14:38,880
 When you see something, you train yourself focused on

237
00:14:38,880 --> 00:14:39,360
 seeing.

238
00:14:39,360 --> 00:14:40,640
 When you hear something, hearing.

239
00:14:40,640 --> 00:14:42,320
 When you smell, smelling.

240
00:14:42,320 --> 00:14:45,040
 You train yourself to focus on the body, on the feelings,

241
00:14:45,040 --> 00:14:47,360
 on the thoughts, on the emotions,

242
00:14:47,360 --> 00:14:49,120
 and the senses.

243
00:14:49,120 --> 00:14:51,120
 You train yourself to focus on reality.

244
00:14:51,120 --> 00:14:54,240
 So you cultivate concentration.

245
00:14:54,240 --> 00:15:01,470
 As a result, not only are you pulling your mind back, but

246
00:15:01,470 --> 00:15:04,320
 your mind begins to be disinclined

247
00:15:04,320 --> 00:15:08,880
 to actually leave the present reality.

248
00:15:08,880 --> 00:15:11,860
 So all of us sitting here now, we're sitting, you're

249
00:15:11,860 --> 00:15:14,960
 sitting listening to this dog.

250
00:15:14,960 --> 00:15:16,240
 We have two things going on.

251
00:15:16,240 --> 00:15:19,180
 We have the ultimate reality, which is where we think we

252
00:15:19,180 --> 00:15:20,880
 are, where we'd like to be.

253
00:15:20,880 --> 00:15:22,160
 We'd like to be here.

254
00:15:22,160 --> 00:15:28,400
 And ultimate reality sounds kind of technical or special.

255
00:15:28,400 --> 00:15:31,600
 It actually just means this, what we think we're in at all

256
00:15:31,600 --> 00:15:32,240
 times.

257
00:15:32,240 --> 00:15:35,120
 So the reality of seeing, the reality of hearing, smelling,

258
00:15:35,120 --> 00:15:36,720
 tasting, feeling,

259
00:15:36,720 --> 00:15:38,960
 the reality of our experience.

260
00:15:38,960 --> 00:15:41,810
 But something else that's going on is all the activity in

261
00:15:41,810 --> 00:15:43,360
 the mind, the judgments,

262
00:15:43,360 --> 00:15:49,400
 the projections, the extrapolations, the identifications,

263
00:15:49,400 --> 00:15:50,880
 all of the concepts that arise

264
00:15:50,880 --> 00:15:53,680
 of people and places and things, all of the past and the

265
00:15:53,680 --> 00:15:54,320
 future.

266
00:15:54,320 --> 00:15:56,240
 This is all conceptual.

267
00:15:56,240 --> 00:15:59,360
 It has nothing to do with the ultimate reality.

268
00:15:59,360 --> 00:16:02,560
 So we begin to, the training that we're looking for is to

269
00:16:02,560 --> 00:16:04,800
 keep ourselves with the reality,

270
00:16:04,800 --> 00:16:06,400
 keep ourselves here.

271
00:16:06,400 --> 00:16:09,360
 Here we all are sitting together.

272
00:16:09,360 --> 00:16:15,680
 The training is to keep ourselves here and now.

273
00:16:15,680 --> 00:16:20,240
 Once we do that, this is the training and concentration.

274
00:16:20,240 --> 00:16:23,520
 The third training, the training and wisdom, is that once

275
00:16:23,520 --> 00:16:24,560
 we keep ourselves here,

276
00:16:24,560 --> 00:16:27,120
 we begin to understand here.

277
00:16:27,120 --> 00:16:28,480
 We begin to understand reality.

278
00:16:28,480 --> 00:16:32,340
 We begin to see the distinction between reality and

279
00:16:32,340 --> 00:16:33,360
 illusion.

280
00:16:33,360 --> 00:16:37,440
 We see how there's nothing essentially good or bad about

281
00:16:37,440 --> 00:16:38,080
 anything.

282
00:16:38,080 --> 00:16:40,920
 We see that all of the people and places and things, even

283
00:16:40,920 --> 00:16:43,920
 our own body, is just a concept

284
00:16:43,920 --> 00:16:48,320
 that experiences arise and sees come and go.

285
00:16:48,320 --> 00:16:55,290
 Because of wisdom, we begin to need less and less effort to

286
00:16:55,290 --> 00:16:57,880
 focus ourselves and less and

287
00:16:57,880 --> 00:16:59,640
 less effort to bring our minds back.

288
00:16:59,640 --> 00:17:03,980
 Our minds are less inclined to move away and more inclined

289
00:17:03,980 --> 00:17:07,000
 towards focusing, more inclined

290
00:17:07,000 --> 00:17:11,600
 towards peace and calm and clarity of mind.

291
00:17:11,600 --> 00:17:15,040
 Not just because we pull our minds back or try to keep our

292
00:17:15,040 --> 00:17:16,360
 minds fixed, but because we

293
00:17:16,360 --> 00:17:23,800
 understand the uselessness of chasing after illusion.

294
00:17:23,800 --> 00:17:25,400
 This is the training and wisdom.

295
00:17:25,400 --> 00:17:28,970
 We train ourselves not only to bring our minds back, not

296
00:17:28,970 --> 00:17:31,160
 only to force our minds to stay,

297
00:17:31,160 --> 00:17:37,590
 but to have our minds understand the benefit of staying,

298
00:17:37,590 --> 00:17:41,560
 the benefit of being in the present

299
00:17:41,560 --> 00:17:42,560
 moment.

300
00:17:42,560 --> 00:17:46,310
 To teach ourselves how much stress is involved with running

301
00:17:46,310 --> 00:17:48,720
 away and chasing after illusions,

302
00:17:48,720 --> 00:17:51,400
 chasing after concepts.

303
00:17:51,400 --> 00:17:52,720
 These are the three trainings.

304
00:17:52,720 --> 00:17:56,700
 This, the Buddha said, is how one both overcomes the bad

305
00:17:56,700 --> 00:17:59,040
 things in life and cultivates the

306
00:17:59,040 --> 00:18:00,160
 good things.

307
00:18:00,160 --> 00:18:01,960
 The first part is overcomes the world.

308
00:18:01,960 --> 00:18:06,080
 We chase it, the overcomes the earth, overcomes all the

309
00:18:06,080 --> 00:18:08,640
 troubles and all the difficulties

310
00:18:08,640 --> 00:18:12,550
 and all the stresses and sufferings that are inherent in

311
00:18:12,550 --> 00:18:13,200
 life.

312
00:18:13,200 --> 00:18:19,620
 How one cultivates the Dhamma Vada, the path of truth and

313
00:18:19,620 --> 00:18:23,000
 the path of righteousness and

314
00:18:23,000 --> 00:18:24,360
 brings it to perfection.

315
00:18:24,360 --> 00:18:26,790
 When you have all of the Buddha's teachings or all of the

316
00:18:26,790 --> 00:18:28,180
 good things in the world, how

317
00:18:28,180 --> 00:18:29,600
 do you bring them to perfection?

318
00:18:29,600 --> 00:18:33,150
 This is the concise summary of how to do that, is one

319
00:18:33,150 --> 00:18:35,440
 becomes a trainee, one trains one's

320
00:18:35,440 --> 00:18:38,240
 self.

321
00:18:38,240 --> 00:18:41,620
 One way of understanding Buddhism is it's a training.

322
00:18:41,620 --> 00:18:44,610
 It's not something that you can wish for or hope for or

323
00:18:44,610 --> 00:18:46,400
 something that you can be when

324
00:18:46,400 --> 00:18:48,600
 you put on robes or so on.

325
00:18:48,600 --> 00:18:51,140
 It's something that you train in.

326
00:18:51,140 --> 00:18:54,620
 Just as we train our bodies and we train our minds in a

327
00:18:54,620 --> 00:18:57,140
 worldly sense to become proficient

328
00:18:57,140 --> 00:18:59,140
 at this skill or that skill.

329
00:18:59,140 --> 00:19:03,810
 So too we train our minds, we train our hearts in the Dham

330
00:19:03,810 --> 00:19:05,840
ma in wholesomeness.

331
00:19:05,840 --> 00:19:09,420
 We train them out of unwholesomeness, unskillful states,

332
00:19:09,420 --> 00:19:11,860
 states that cause stress and suffering

333
00:19:11,860 --> 00:19:14,820
 for us and others.

334
00:19:14,820 --> 00:19:22,850
 This is the teaching in these two verses, quite a sort of p

335
00:19:22,850 --> 00:19:26,160
ithy summary of how one brings

336
00:19:26,160 --> 00:19:29,730
 the Buddha's teaching or brings all good states to

337
00:19:29,730 --> 00:19:34,440
 perfection just as one skillful flower

338
00:19:34,440 --> 00:19:41,000
 maker, florist would put together a bouquet of flowers.

339
00:19:41,000 --> 00:19:48,060
 So all of the good things in our mind, flower and we become

340
00:19:48,060 --> 00:19:54,720
 in a sense perfect through our

341
00:19:54,720 --> 00:19:58,320
 abandoning of evil and wholesome states and through the

342
00:19:58,320 --> 00:20:00,360
 cultivation of good states.

343
00:20:00,360 --> 00:20:06,200
 This is the teaching in this verse and that's the Dhammap

344
00:20:06,200 --> 00:20:07,000
ada.

345
00:20:07,000 --> 00:20:08,680
 Very much a practical teaching.

346
00:20:08,680 --> 00:20:10,950
 This is obviously something that we have to use in our

347
00:20:10,950 --> 00:20:11,680
 meditation.

348
00:20:11,680 --> 00:20:17,430
 It's something that the highest training in Buddhism is

349
00:20:17,430 --> 00:20:18,360
 here.

350
00:20:18,360 --> 00:20:23,910
 It comes from the meditative practice of morality, the med

351
00:20:23,910 --> 00:20:27,640
itative practice of concentration,

352
00:20:27,640 --> 00:20:31,360
 bringing the mind back, keeping the mind there and the med

353
00:20:31,360 --> 00:20:33,480
itative practice or training of

354
00:20:33,480 --> 00:20:37,790
 wisdom of understanding, the understanding that keeps you

355
00:20:37,790 --> 00:20:40,040
 out of illusion and delusion,

356
00:20:40,040 --> 00:20:45,790
 keeps the mind from wandering, keeps the mind in tune with

357
00:20:45,790 --> 00:20:47,040
 reality.

358
00:20:47,040 --> 00:20:51,480
 So that's the teaching for today on verses 44 and 45.

359
00:20:51,480 --> 00:20:55,810
 Thank you for tuning in and I hope you're able to put this

360
00:20:55,810 --> 00:20:58,000
 into practice and follow the

361
00:20:58,000 --> 00:20:59,120
 Dhammapada on your own.

362
00:20:59,120 --> 00:21:00,360
 Thank you.

