1
00:00:00,000 --> 00:00:03,000
 Hello, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:07,370
 Today I thought I would try to answer at least one of the

3
00:00:07,370 --> 00:00:09,000
 questions that are waiting for me.

4
00:00:09,000 --> 00:00:14,000
 And the question I'm thinking of now is on euthanasia.

5
00:00:14,000 --> 00:00:21,540
 It was asked, what is the Buddhist stance on killing

6
00:00:21,540 --> 00:00:23,000
 someone who wants to die

7
00:00:23,000 --> 00:00:30,310
 or who would perhaps be freed from suffering through their

8
00:00:30,310 --> 00:00:32,000
 own death?

9
00:00:32,000 --> 00:00:36,240
 Now I hope it's quite clear that I don't think, it should

10
00:00:36,240 --> 00:00:37,000
 be clear from what I've said before,

11
00:00:37,000 --> 00:00:39,540
 that I don't think they would be freed from suffering

12
00:00:39,540 --> 00:00:41,000
 through their own death.

13
00:00:41,000 --> 00:00:46,000
 I think that's something that I don't have to get into.

14
00:00:46,000 --> 00:00:49,000
 My views on that are pretty clear.

15
00:00:49,000 --> 00:00:52,260
 Only through giving up, clinging are you free from

16
00:00:52,260 --> 00:00:53,000
 suffering.

17
00:00:53,000 --> 00:00:58,430
 And that's really a key here that the view and the belief

18
00:00:58,430 --> 00:01:02,000
 that killing someone could somehow be better for them

19
00:01:02,000 --> 00:01:08,000
 is delusionary, delusional.

20
00:01:08,000 --> 00:01:13,480
 Because it's something that they are clinging to and it's

21
00:01:13,480 --> 00:01:15,000
 an aversion that they have towards the suffering.

22
00:01:15,000 --> 00:01:21,000
 And to indulge that is by no means of any benefit to them.

23
00:01:21,000 --> 00:01:24,500
 Any time we look at a being who is suffering greatly and we

24
00:01:24,500 --> 00:01:27,000
 think that by putting them out of their misery

25
00:01:27,000 --> 00:01:33,000
 we're somehow doing them a favor is quite mistaken.

26
00:01:33,000 --> 00:01:38,200
 When in fact some of the greatest insights and wisdom and

27
00:01:38,200 --> 00:01:40,000
 realizations,

28
00:01:40,000 --> 00:01:45,150
 are the wake up enlightenment that comes from suffering,

29
00:01:45,150 --> 00:01:48,000
 that comes from bearing with your suffering

30
00:01:48,000 --> 00:01:52,540
 and learning to overcome the clinging and the aversion that

31
00:01:52,540 --> 00:01:54,000
 we have towards it.

32
00:01:54,000 --> 00:01:56,000
 That's where the real benefit is.

33
00:01:56,000 --> 00:01:57,000
 So countless stories.

34
00:01:57,000 --> 00:02:00,420
 If you talk to people who work with dying patients in hosp

35
00:02:00,420 --> 00:02:02,000
ice care or so on,

36
00:02:02,000 --> 00:02:06,680
 they will tell you about the moment of clarity that comes

37
00:02:06,680 --> 00:02:09,000
 to a person who is suffering greatly.

38
00:02:09,000 --> 00:02:12,070
 Right before they die, not everyone, but many people will

39
00:02:12,070 --> 00:02:15,000
 have this moment where suddenly they are free.

40
00:02:15,000 --> 00:02:19,090
 And it's so clear that they've gone through something and

41
00:02:19,090 --> 00:02:21,000
 they've worked through something and they've worked it out.

42
00:02:21,000 --> 00:02:24,000
 And they're finally at peace with themselves.

43
00:02:24,000 --> 00:02:27,000
 And there's clarity of mind and they're able to move on.

44
00:02:27,000 --> 00:02:30,000
 When you kill, when you cut that off,

45
00:02:30,000 --> 00:02:32,890
 well first of all this is, especially if it's their wish

46
00:02:32,890 --> 00:02:34,000
 that they should die,

47
00:02:34,000 --> 00:02:37,500
 then you're helping them escape from the lesson, you're

48
00:02:37,500 --> 00:02:41,000
 helping them to run away from this,

49
00:02:41,000 --> 00:02:46,300
 you know temporarily from this lesson that they have to

50
00:02:46,300 --> 00:02:48,000
 help them to overcome the clinging.

51
00:02:48,000 --> 00:02:53,150
 Helping them to encourage their clinging to, their aversion

52
00:02:53,150 --> 00:02:55,000
 to pain and suffering.

53
00:02:55,000 --> 00:02:58,000
 That's the one side.

54
00:02:58,000 --> 00:03:00,650
 But I think more important is to talk about the effect of

55
00:03:00,650 --> 00:03:02,000
 killing on one's own mind

56
00:03:02,000 --> 00:03:05,630
 because that's what makes, as I've said before, an action

57
00:03:05,630 --> 00:03:07,000
 immoral or moral.

58
00:03:07,000 --> 00:03:09,000
 It's the effect that it has on your own mind.

59
00:03:09,000 --> 00:03:11,480
 It's not really the suffering that it brings to the other

60
00:03:11,480 --> 00:03:12,000
 person.

61
00:03:12,000 --> 00:03:15,470
 The most horrible thing about killing someone else is the

62
00:03:15,470 --> 00:03:18,000
 disruptive nature of killing.

63
00:03:18,000 --> 00:03:21,000
 When you kill something that wants to live,

64
00:03:21,000 --> 00:03:24,180
 it's of course a lot more disruptive than when you kill

65
00:03:24,180 --> 00:03:26,000
 someone that wants to die.

66
00:03:26,000 --> 00:03:28,000
 But nonetheless, it's an incredibly disruptive act.

67
00:03:28,000 --> 00:03:31,000
 It's a very powerful and important act.

68
00:03:31,000 --> 00:03:35,000
 For people who have never killed before,

69
00:03:35,000 --> 00:03:38,290
 if we will never kill before, it's very hard to understand

70
00:03:38,290 --> 00:03:39,000
 this.

71
00:03:39,000 --> 00:03:42,000
 I think they will generally have a natural aversion to it

72
00:03:42,000 --> 00:03:44,000
 because of the weight of the act.

73
00:03:44,000 --> 00:03:47,000
 So many people have never killed even insects and so on,

74
00:03:47,000 --> 00:03:52,000
 and really feel abhorrent to do such a thing just by nature

75
00:03:52,000 --> 00:03:52,000
.

76
00:03:52,000 --> 00:03:56,000
 And that's because of the strength of the act.

77
00:03:56,000 --> 00:03:59,430
 But it may be difficult for them to understand because they

78
00:03:59,430 --> 00:04:01,000
've never experienced.

79
00:04:01,000 --> 00:04:04,000
 On the other hand, for someone who kills often,

80
00:04:04,000 --> 00:04:08,690
 a person who murders animals and slaughters animals or

81
00:04:08,690 --> 00:04:11,000
 insects and so on,

82
00:04:11,000 --> 00:04:14,780
 it's equally difficult for them to see because they've

83
00:04:14,780 --> 00:04:18,000
 become desensitized to it.

84
00:04:18,000 --> 00:04:21,290
 I've told the story before about when I was younger, I

85
00:04:21,290 --> 00:04:23,000
 would do hunting.

86
00:04:23,000 --> 00:04:26,650
 I thought to do hunting with my father, to hunt with my

87
00:04:26,650 --> 00:04:28,000
 father and hunt deer.

88
00:04:28,000 --> 00:04:30,000
 And I killed one deer.

89
00:04:30,000 --> 00:04:34,000
 And it was really a very difficult thing to do.

90
00:04:34,000 --> 00:04:37,000
 It surprised me because I had no qualms about it.

91
00:04:37,000 --> 00:04:39,560
 I didn't think there was anything wrong with killing at the

92
00:04:39,560 --> 00:04:40,000
 time.

93
00:04:40,000 --> 00:04:43,000
 I thought, "That's a good way to get food."

94
00:04:43,000 --> 00:04:47,000
 But when it came time to actually kill the deer, my whole

95
00:04:47,000 --> 00:04:49,000
 body was shaking.

96
00:04:49,000 --> 00:04:51,000
 And it surprised me at the time.

97
00:04:51,000 --> 00:04:54,000
 I didn't understand what was going on, what was happening.

98
00:04:54,000 --> 00:04:58,960
 But of course later on when I practiced meditation and

99
00:04:58,960 --> 00:05:01,000
 really woke up

100
00:05:01,000 --> 00:05:05,330
 and had this great wake-up call as to what I was doing to

101
00:05:05,330 --> 00:05:06,000
 my mind.

102
00:05:06,000 --> 00:05:09,610
 And so many things that I had to go through and had to work

103
00:05:09,610 --> 00:05:10,000
 through

104
00:05:10,000 --> 00:05:13,640
 and all of these emotions that came up that I had to sort

105
00:05:13,640 --> 00:05:15,000
 out and realize

106
00:05:15,000 --> 00:05:18,000
 and give up and just changing my whole outlook

107
00:05:18,000 --> 00:05:22,000
 until finally there was this great transformation.

108
00:05:22,000 --> 00:05:25,000
 Sorry, I didn't talk about great things that happened to me

109
00:05:25,000 --> 00:05:25,000
.

110
00:05:25,000 --> 00:05:28,290
 But when you go through meditation and I was in a pretty

111
00:05:28,290 --> 00:05:29,000
 bad state,

112
00:05:29,000 --> 00:05:32,800
 there's a lot of cleaning that goes on, even just in the

113
00:05:32,800 --> 00:05:35,000
 first few days.

114
00:05:35,000 --> 00:05:42,000
 And so it made me realize, it helped me to realize that,

115
00:05:42,000 --> 00:05:45,000
 "Oh yes, that's what was going on."

116
00:05:45,000 --> 00:05:48,000
 There really is a great weight to killing.

117
00:05:48,000 --> 00:05:55,000
 And I think that's quite clear when you fine tune your mind

118
00:05:55,000 --> 00:05:55,000
.

119
00:05:55,000 --> 00:05:59,570
 When you start to practice meditation, your mind becomes

120
00:05:59,570 --> 00:06:02,000
 very quiet, relatively speaking,

121
00:06:02,000 --> 00:06:05,000
 and you're able to see any little thing that comes up,

122
00:06:05,000 --> 00:06:07,000
 you're able to see it much better than before.

123
00:06:07,000 --> 00:06:09,000
 Whereas when your mind is very active,

124
00:06:09,000 --> 00:06:13,700
 when you're full of defilement and greed and anger and so

125
00:06:13,700 --> 00:06:14,000
 on,

126
00:06:14,000 --> 00:06:16,000
 you can't really make head or tail of anything.

127
00:06:16,000 --> 00:06:19,570
 So it's really difficult for most people to come up with

128
00:06:19,570 --> 00:06:21,000
 any solid theory of reality

129
00:06:21,000 --> 00:06:23,750
 or solid understanding of reality when you talk about these

130
00:06:23,750 --> 00:06:24,000
 things.

131
00:06:24,000 --> 00:06:27,120
 It kind of sounds interesting, but it's really hard for

132
00:06:27,120 --> 00:06:28,000
 them to understand

133
00:06:28,000 --> 00:06:31,000
 because they've never taken the time to quiet their mind

134
00:06:31,000 --> 00:06:33,000
 and to see things clearly,

135
00:06:33,000 --> 00:06:38,000
 one by one by one, and to be able to break things apart.

136
00:06:38,000 --> 00:06:40,000
 But it's like you have a very fine tuned instrument

137
00:06:40,000 --> 00:06:42,730
 and you're able to see these things that people aren't able

138
00:06:42,730 --> 00:06:43,000
 to see.

139
00:06:43,000 --> 00:06:46,000
 And so killing becomes very abhorrent,

140
00:06:46,000 --> 00:06:50,570
 and you're able to see how even the slightest anger has a

141
00:06:50,570 --> 00:06:52,000
 great power to it,

142
00:06:52,000 --> 00:06:55,000
 let alone the disruptive power.

143
00:06:55,000 --> 00:06:58,000
 When you say something to someone and it changes their life

144
00:06:58,000 --> 00:06:59,000
 and it disrupts,

145
00:06:59,000 --> 00:07:03,000
 there's great power in it.

146
00:07:03,000 --> 00:07:07,000
 But to kill has a far greater power.

147
00:07:07,000 --> 00:07:10,000
 It's on a whole other level.

148
00:07:10,000 --> 00:07:13,440
 Even when you kill someone who wants to die, you're

149
00:07:13,440 --> 00:07:15,000
 changing their karma.

150
00:07:15,000 --> 00:07:21,000
 It's like trying to help someone who's running from the law

151
00:07:21,000 --> 00:07:21,000
,

152
00:07:21,000 --> 00:07:23,640
 for instance, someone who robbed a bank, you help to hide

153
00:07:23,640 --> 00:07:24,000
 them.

154
00:07:24,000 --> 00:07:26,000
 It's a lot of work.

155
00:07:26,000 --> 00:07:28,000
 And that's really what's going on here.

156
00:07:28,000 --> 00:07:31,000
 When you kill someone who wants to die, it's no less,

157
00:07:31,000 --> 00:07:33,550
 and it's less, but it's still on the level of killing

158
00:07:33,550 --> 00:07:35,000
 someone who doesn't want to die

159
00:07:35,000 --> 00:07:38,000
 because it's a great and weighty act to kill,

160
00:07:38,000 --> 00:07:41,000
 something that weighs on your mind.

161
00:07:41,000 --> 00:07:45,800
 There's repercussions because of the power and the

162
00:07:45,800 --> 00:07:48,000
 intensity of the act,

163
00:07:48,000 --> 00:07:50,000
 of the disruption.

164
00:07:50,000 --> 00:07:53,000
 If you think of the world, the universe as being waves,

165
00:07:53,000 --> 00:07:57,000
 energy waves, and even physical, of course, is energy.

166
00:07:57,000 --> 00:07:59,000
 If you think of it as all energy,

167
00:07:59,000 --> 00:08:05,000
 the explosion of energy that you've created, mental energy,

168
00:08:05,000 --> 00:08:08,000
 is tremendous by killing.

169
00:08:08,000 --> 00:08:10,320
 If there's someone who's never killed before, it may be

170
00:08:10,320 --> 00:08:11,000
 difficult to see,

171
00:08:11,000 --> 00:08:13,470
 and this is why people think euthanasia could be a good

172
00:08:13,470 --> 00:08:14,000
 thing.

173
00:08:14,000 --> 00:08:17,000
 Now, there's one exception that I've talked about before,

174
00:08:17,000 --> 00:08:20,000
 and I think it is an exception.

175
00:08:20,000 --> 00:08:23,810
 I'm not 100% sure, but to me, you could at least argue for

176
00:08:23,810 --> 00:08:24,000
 it,

177
00:08:24,000 --> 00:08:26,000
 and that is letting someone die.

178
00:08:26,000 --> 00:08:30,000
 I think clearly you can and should let people die

179
00:08:30,000 --> 00:08:35,070
 rather than giving them medication that's only going to

180
00:08:35,070 --> 00:08:37,000
 prolong their misery.

181
00:08:37,000 --> 00:08:41,000
 I think that's reasonable,

182
00:08:41,000 --> 00:08:44,000
 but I think also you could stop life support

183
00:08:44,000 --> 00:08:50,000
 if it was considered that they had no chance,

184
00:08:50,000 --> 00:08:53,000
 or maybe even that the mind had left.

185
00:08:53,000 --> 00:08:55,810
 Actually, apparently that's what happened with Mahasi Sayad

186
00:08:55,810 --> 00:08:56,000
aw,

187
00:08:56,000 --> 00:08:58,000
 that they said the mind is no longer working,

188
00:08:58,000 --> 00:09:00,000
 the brain is no longer functioning,

189
00:09:00,000 --> 00:09:03,480
 so they could take him off life support because he was no

190
00:09:03,480 --> 00:09:05,000
 longer there.

191
00:09:05,000 --> 00:09:09,000
 But on the other hand, this is where you could be excused,

192
00:09:09,000 --> 00:09:12,340
 but on the other hand, my teacher in Thailand gave an

193
00:09:12,340 --> 00:09:14,000
 interesting perspective on this,

194
00:09:14,000 --> 00:09:17,000
 that you can take as you like,

195
00:09:17,000 --> 00:09:21,000
 but he said, "Well, you never really know,

196
00:09:21,000 --> 00:09:24,000
 and we can say that the person is not going to come back,

197
00:09:24,000 --> 00:09:26,000
 but it's never really clear."

198
00:09:26,000 --> 00:09:29,000
 There was the case, and he told this story about someone

199
00:09:29,000 --> 00:09:30,000
 who they thought was going to die,

200
00:09:30,000 --> 00:09:33,000
 but they kept them on life support for some time,

201
00:09:33,000 --> 00:09:35,300
 and eventually they recovered and came back and lived

202
00:09:35,300 --> 00:09:37,000
 another 10 years or something,

203
00:09:37,000 --> 00:09:39,000
 and were able to do all sorts of good deeds.

204
00:09:39,000 --> 00:09:41,590
 Now his point was, if a person has the opportunity in this

205
00:09:41,590 --> 00:09:43,000
 life to do good deeds,

206
00:09:43,000 --> 00:09:46,000
 you shouldn't be so quick to cut them off,

207
00:09:46,000 --> 00:09:48,000
 because you don't know where they're going.

208
00:09:48,000 --> 00:09:51,920
 This is why we say always that this human life is very

209
00:09:51,920 --> 00:09:53,000
 precious.

210
00:09:53,000 --> 00:09:56,000
 Now the second story I gave about Matakundali,

211
00:09:56,000 --> 00:09:59,400
 the boy who was on his deathbed and had a chance to see the

212
00:09:59,400 --> 00:10:00,000
 Buddha,

213
00:10:00,000 --> 00:10:04,000
 that's just an example of how we can possibly create

214
00:10:04,000 --> 00:10:06,000
 wholesome mind states before the moment of death,

215
00:10:06,000 --> 00:10:08,000
 and you never really know.

216
00:10:08,000 --> 00:10:12,000
 If the person has a chance now as a human being,

217
00:10:12,000 --> 00:10:17,490
 if their mind is impure, then letting them die or helping

218
00:10:17,490 --> 00:10:18,000
 them to die

219
00:10:18,000 --> 00:10:23,840
 is only going to lead them or destined them to an

220
00:10:23,840 --> 00:10:26,000
 unpleasant result,

221
00:10:26,000 --> 00:10:29,000
 an unpleasant life in the future.

222
00:10:29,000 --> 00:10:33,000
 So if you can help them to stay and at least help them to

223
00:10:33,000 --> 00:10:34,000
 practice meditation

224
00:10:34,000 --> 00:10:37,000
 or listen to the Dhamma or so on,

225
00:10:37,000 --> 00:10:41,000
 or at least be with their family and learn about compassion

226
00:10:41,000 --> 00:10:42,000
 and love and so on,

227
00:10:42,000 --> 00:10:45,970
 and cultivate good thoughts, and even cultivating patience

228
00:10:45,970 --> 00:10:46,000
 with the pain,

229
00:10:46,000 --> 00:10:51,000
 which is an excellent learning tool, learning experience.

230
00:10:51,000 --> 00:10:54,990
 It's something that we should not underestimate and under

231
00:10:54,990 --> 00:10:56,000
value.

232
00:10:56,000 --> 00:10:58,780
 This can be a great thing, and it's something that we

233
00:10:58,780 --> 00:11:00,000
 should actually encourage,

234
00:11:00,000 --> 00:11:04,140
 something that is far better than to dismiss the person and

235
00:11:04,140 --> 00:11:05,000
 send them on their way,

236
00:11:05,000 --> 00:11:07,520
 not knowing where they're going to go, and not just send

237
00:11:07,520 --> 00:11:08,000
 them on their way,

238
00:11:08,000 --> 00:11:11,000
 but disrupt their life.

239
00:11:11,000 --> 00:11:16,630
 So, well, even in the case of not killing, but letting a

240
00:11:16,630 --> 00:11:18,000
 person die,

241
00:11:18,000 --> 00:11:22,000
 it can be that helping them to stay on is a good thing.

242
00:11:22,000 --> 00:11:24,000
 Now you have to judge for yourself.

243
00:11:24,000 --> 00:11:26,000
 It really depends on the individual.

244
00:11:26,000 --> 00:11:29,110
 I think I wouldn't want to stay on, and if it came down to

245
00:11:29,110 --> 00:11:30,000
 taking medication or so on,

246
00:11:30,000 --> 00:11:32,000
 I would just let myself go.

247
00:11:32,000 --> 00:11:34,000
 But you really have to judge for yourself.

248
00:11:34,000 --> 00:11:38,540
 And for a person who's not meditating for a person who hasn

249
00:11:38,540 --> 00:11:40,000
't really begun to practice,

250
00:11:40,000 --> 00:11:44,210
 it's always dangerous to let them go because you don't know

251
00:11:44,210 --> 00:11:46,000
 where they're going.

252
00:11:46,000 --> 00:11:50,120
 I guess the answer would be, if once you can see that

253
00:11:50,120 --> 00:11:51,000
 clearly they're on the right path

254
00:11:51,000 --> 00:11:54,500
 and they're ready to go, then you can help them to give up

255
00:11:54,500 --> 00:11:55,000
 medication

256
00:11:55,000 --> 00:11:59,650
 or to get off life support or so on, and say, "Go on your

257
00:11:59,650 --> 00:12:00,000
 way

258
00:12:00,000 --> 00:12:04,000
 and be reassured that they're going in the right direction.

259
00:12:04,000 --> 00:12:09,000
 Just be careful that they wouldn't be better off

260
00:12:09,000 --> 00:12:12,190
 sticking around to do more good deeds and practice more

261
00:12:12,190 --> 00:12:13,000
 meditation

262
00:12:13,000 --> 00:12:15,450
 because you don't really know where they're going in the

263
00:12:15,450 --> 00:12:16,000
 future."

264
00:12:16,000 --> 00:12:20,490
 So I hope that that gave some insight into the Buddhist

265
00:12:20,490 --> 00:12:22,000
 take on euthanasia.

266
00:12:22,000 --> 00:12:24,000
 Thanks for tuning in. All the best.

