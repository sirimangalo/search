WEBVTT

00:00:00.000 --> 00:00:05.000
 Good evening everyone. Welcome to our evening broadcast.

00:00:05.000 --> 00:00:23.000
 Today is a dhamma. Today I wanted to look at...

00:00:27.000 --> 00:00:31.000
 I want to talk about the idea of what it is we need to know

00:00:31.000 --> 00:00:31.000
.

00:00:31.000 --> 00:00:36.000
 What we need to learn from the practice.

00:00:36.000 --> 00:00:39.000
 We talk about...

00:00:39.000 --> 00:00:44.000
 Satipatanas for the purpose of attaining Vipasthana.

00:00:44.000 --> 00:00:47.000
 It's for the purpose of gaining insight.

00:00:47.000 --> 00:00:50.000
 It's for the purpose of gaining Jnana.

00:00:50.000 --> 00:00:54.000
 Jnanangudapadi.

00:00:55.000 --> 00:00:59.000
 Vijayudapadi, Panyaudapadi, Ahlokodapadi.

00:00:59.000 --> 00:01:02.000
 It's for gaining wisdom.

00:01:02.000 --> 00:01:17.000
 I think a bit of a misunderstanding is...

00:01:17.000 --> 00:01:20.000
 or a misconception of that is that...

00:01:20.000 --> 00:01:23.000
 some of there are many things that we have to learn.

00:01:23.000 --> 00:01:25.000
 It's quite complex.

00:01:25.000 --> 00:01:29.000
 A sense that we're not learning what we have to learn.

00:01:29.000 --> 00:01:31.000
 You hear about wisdom, right?

00:01:31.000 --> 00:01:34.000
 And you think, "I've been practicing for a long time.

00:01:34.000 --> 00:01:37.000
 I don't see any wisdom yet."

00:01:37.000 --> 00:01:42.000
 Because you have this idea that wisdom is something lofty.

00:01:42.000 --> 00:01:44.000
 Wisdom is something...

00:01:44.000 --> 00:01:48.000
 perhaps intellectual or complicated.

00:01:48.000 --> 00:01:50.000
 It's actually quite simple.

00:01:50.000 --> 00:01:54.000
 It's somewhat, sometimes...

00:01:54.000 --> 00:01:59.000
 well, deceivingly simple anyway.

00:01:59.000 --> 00:02:08.000
 My teacher Ajahn Tong said...

00:02:08.000 --> 00:02:13.610
 he says, "When you say 'rise' and when you watch the

00:02:13.610 --> 00:02:14.000
 stomach rise

00:02:14.000 --> 00:02:16.810
 and you say 'rising,' when you know that the stomach's

00:02:16.810 --> 00:02:19.000
 rising, that's wisdom."

00:02:20.000 --> 00:02:24.000
 And you think, "That's it?

00:02:24.000 --> 00:02:29.000
 Why am I here? I can do this at home."

00:02:29.000 --> 00:02:33.000
 It's a very important statement.

00:02:33.000 --> 00:02:36.000
 When you know that the stomach is rising, that's wisdom.

00:02:36.000 --> 00:02:41.000
 There's a problem. It's that we're forgetful.

00:02:41.000 --> 00:02:44.000
 We don't remember.

00:02:44.000 --> 00:02:48.000
 We aren't wise. We're unwise.

00:02:48.000 --> 00:02:51.000
 We have Ayoni-soma-nasi-kara.

00:02:51.000 --> 00:02:53.220
 Instead of just knowing that the stomach is rising or

00:02:53.220 --> 00:02:56.000
 knowing that seeing is seeing.

00:02:56.000 --> 00:03:00.000
 We know a lot more. We know too much.

00:03:00.000 --> 00:03:05.000
 Too much and not enough. Everything and nothing.

00:03:05.000 --> 00:03:11.000
 And I know all the details about so many things.

00:03:12.000 --> 00:03:18.000
 As a human species seem bent on learning everything,

00:03:18.000 --> 00:03:21.920
 collecting as much useless information as we can, mostly

00:03:21.920 --> 00:03:23.000
 useless.

00:03:23.000 --> 00:03:30.000
 And by useless, I mean by Buddhist standards.

00:03:30.000 --> 00:03:33.000
 Useless in the sense that they don't actually make us happy

00:03:33.000 --> 00:03:35.000
, the things we learn.

00:03:35.000 --> 00:03:38.000
 They don't actually bring peace.

00:03:38.000 --> 00:03:42.600
 How much learning we need just to do some job that ends up

00:03:42.600 --> 00:03:44.000
 fulfilling some function,

00:03:44.000 --> 00:03:51.000
 that does very little to promote well-being,

00:03:51.000 --> 00:03:58.000
 or learning skills that in the end are such a roundabout

00:03:58.000 --> 00:04:02.000
 way of promoting any sort of goodness, if at all.

00:04:02.000 --> 00:04:07.000
 And in fact often promote unwholesomeness.

00:04:09.000 --> 00:04:11.790
 How many skills, how much learning, how much knowledge is

00:04:11.790 --> 00:04:16.220
 there out there that ends up being totally useless, or

00:04:16.220 --> 00:04:18.000
 worse, harmful.

00:04:31.000 --> 00:04:36.530
 When I was young we used to play these computer games, back

00:04:36.530 --> 00:04:39.000
 before they had online computer games, we used to play.

00:04:39.000 --> 00:04:44.000
 Starcraft, Warcraft.

00:04:44.000 --> 00:04:49.920
 And you could spend hours when we'd play these battle

00:04:49.920 --> 00:04:51.000
 simulations.

00:04:51.000 --> 00:04:55.270
 And you could spend hours. We would spend all night, we

00:04:55.270 --> 00:04:57.000
 would stay awake all night playing.

00:04:59.000 --> 00:05:02.130
 I'm sure to some of you this all sounds like, "Yeah, I mean

00:05:02.130 --> 00:05:05.690
 this is what we do now, I think this is what people do now

00:05:05.690 --> 00:05:06.000
."

00:05:06.000 --> 00:05:14.190
 But you learn so much, and you get these skills, wonderful

00:05:14.190 --> 00:05:16.000
 skills, and you get really good at these games.

00:05:17.000 --> 00:05:28.000
 Or sports, or acting, or mathematics, physics.

00:05:28.000 --> 00:05:35.510
 All these wonderful things we're able to build, computers

00:05:35.510 --> 00:05:38.000
 and spacecraft.

00:05:43.000 --> 00:05:47.850
 So much learning, even languages. How much time we have to

00:05:47.850 --> 00:05:50.000
 spend learning languages just to talk to each other.

00:05:50.000 --> 00:05:54.820
 And then we die and forget it all, lose the languages

00:05:54.820 --> 00:05:58.000
 enough to gain them all again.

00:05:58.000 --> 00:06:03.650
 So now the knowledge that we hope to gain from meditation

00:06:03.650 --> 00:06:06.000
 is quite different.

00:06:07.000 --> 00:06:10.810
 We hope to come to know that, "Oh right, yes, when the

00:06:10.810 --> 00:06:13.000
 stomach rises, that's rising."

00:06:13.000 --> 00:06:18.000
 It's not good, it's not bad, it's not me, it's not mine.

00:06:18.000 --> 00:06:25.360
 But to be a little more precise, if you like to get some,

00:06:25.360 --> 00:06:30.400
 clear up the doubt, it's easy to find doubt in regards to

00:06:30.400 --> 00:06:31.000
 this.

00:06:31.000 --> 00:06:34.000
 What is the wisdom, what am I trying to learn?

00:06:35.000 --> 00:06:39.000
 One time Ajahn Tong said to me that there are four things,

00:06:39.000 --> 00:06:41.000
 there's only four things you have to learn.

00:06:41.000 --> 00:06:54.490
 The first one is called Namarupa, number one. Number two,

00:06:54.490 --> 00:06:57.000
 Tilakana.

00:06:59.000 --> 00:07:05.950
 And number three, Maga. Number four, Pala. These are the

00:07:05.950 --> 00:07:07.000
 Pali words.

00:07:07.000 --> 00:07:17.880
 Namarupa, this means, nama means the immaterial. Rupa means

00:07:17.880 --> 00:07:19.000
 material.

00:07:22.000 --> 00:07:25.380
 There are only two aspects to reality. The first thing you

00:07:25.380 --> 00:07:28.150
 have to come to understand if you want to practice

00:07:28.150 --> 00:07:31.000
 meditation is that there's only two parts to reality.

00:07:31.000 --> 00:07:35.610
 There's the immaterial and there's the material. The

00:07:35.610 --> 00:07:39.000
 material is the physical aspect of experience.

00:07:39.000 --> 00:07:43.280
 When you walk you feel the tension or the hardness or the

00:07:43.280 --> 00:07:46.000
 softness and the heat and the cold.

00:07:47.000 --> 00:07:54.000
 When you practice it you feel the tension and so on.

00:07:54.000 --> 00:07:59.650
 The movements of the body, the sensations in the body,

00:07:59.650 --> 00:08:02.000
 these are material.

00:08:02.000 --> 00:08:05.360
 When you touch something, the touching, the feeling of

00:08:05.360 --> 00:08:08.000
 hardness or softness, that's material.

00:08:11.000 --> 00:08:17.750
 But the immaterial is the knowing of it, the knowing of it

00:08:17.750 --> 00:08:23.400
 and all the concomitant qualities of the knowing when you

00:08:23.400 --> 00:08:25.000
 like and dislike and all that.

00:08:25.000 --> 00:08:30.630
 That's all material, immaterial. That's reality. That's

00:08:30.630 --> 00:08:32.000
 what's real.

00:08:33.000 --> 00:08:37.440
 The first thing you have to learn is what's real. It's the

00:08:37.440 --> 00:08:40.490
 first step in meditation. We can't progress until you're

00:08:40.490 --> 00:08:41.000
 clear.

00:08:41.000 --> 00:08:48.210
 When you move the right foot there's an experience of the

00:08:48.210 --> 00:08:55.430
 pressure and the cold or the movement of the wind and so on

00:08:55.430 --> 00:08:56.000
.

00:08:57.000 --> 00:09:01.800
 And that arises and that's physical and there's the knowing

00:09:01.800 --> 00:09:03.000
 of it as well.

00:09:03.000 --> 00:09:07.980
 When the stomach rises your mind knows. There's the stomach

00:09:07.980 --> 00:09:10.760
, there's the rising and there's the rising movement and

00:09:10.760 --> 00:09:13.000
 there's the mind but there's no stomach.

00:09:13.000 --> 00:09:18.880
 Stomach is all produced in the brain, in the mind, not the

00:09:18.880 --> 00:09:23.000
 brain, the mind. The brain also doesn't exist.

00:09:25.000 --> 00:09:29.450
 It's a concept we give rise to. If you think of the brain,

00:09:29.450 --> 00:09:31.740
 the brain doesn't exist. What do you mean the brain doesn't

00:09:31.740 --> 00:09:32.000
 exist?

00:09:32.000 --> 00:09:37.590
 Besides, there's a concept, there's no existence. The brain

00:09:37.590 --> 00:09:41.160
 is actually connected, it's just a part of the body. It

00:09:41.160 --> 00:09:45.000
 extends into the central nervous system and all that.

00:09:46.000 --> 00:09:51.910
 So such thing as the brain, it's just a concept that we

00:09:51.910 --> 00:09:59.000
 apply to certain mental and physical aspects of experience.

00:09:59.000 --> 00:10:05.030
 There's no body, there's no room, we're not sitting in a

00:10:05.030 --> 00:10:09.000
 room. There's not even any space.

00:10:10.000 --> 00:10:13.750
 Space is interesting because space is only a part of matter

00:10:13.750 --> 00:10:18.050
. It doesn't actually exist. It only comes to being because

00:10:18.050 --> 00:10:19.000
 of matter.

00:10:19.000 --> 00:10:23.390
 It's a part, a derived quality of matter. It does exist,

00:10:23.390 --> 00:10:28.260
 but exists only in regards to matter. The mind doesn't take

00:10:28.260 --> 00:10:29.000
 up space.

00:10:34.000 --> 00:10:37.580
 It's not to get too complicated. Very simply, the only

00:10:37.580 --> 00:10:41.320
 thing you have to know is that body and mind, material, imm

00:10:41.320 --> 00:10:46.000
aterial, reality is only made up of these two things.

00:10:46.000 --> 00:10:51.610
 Reality is made up of experiences. When you see something,

00:10:51.610 --> 00:10:55.460
 there's the physical light and there's the eye, then there

00:10:55.460 --> 00:10:57.000
's the knowing of it.

00:10:58.000 --> 00:11:01.900
 Sometimes with your eyes open, you don't actually see

00:11:01.900 --> 00:11:05.580
 something in front of you. Your mind is somewhere else even

00:11:05.580 --> 00:11:08.000
 though your eyes are open. The mind isn't there.

00:11:08.000 --> 00:11:13.020
 Sound, hearing requires sound, the physical, the ear, but

00:11:13.020 --> 00:11:16.410
 also requires the mind. Of course, sometimes you're

00:11:16.410 --> 00:11:19.130
 absorbed in something and someone calls your name and you

00:11:19.130 --> 00:11:22.000
 don't hear it. The mind.

00:11:23.000 --> 00:11:26.280
 That's the immaterial. Two things required for experience,

00:11:26.280 --> 00:11:30.000
 for seeing, hearing, smelling, tasting, feeling, thinking.

00:11:30.000 --> 00:11:32.000
 You need material, immaterial.

00:11:32.000 --> 00:11:38.260
 This is the first thing you have to learn. The second thing

00:11:38.260 --> 00:11:42.000
 we have to learn is the three characteristics.

00:11:43.000 --> 00:11:50.560
 "Lakkana" means three, "Lakkana" means characteristic. We

00:11:50.560 --> 00:11:53.710
're also called "Samanya Lakkana". "Samanya" means universal

00:11:53.710 --> 00:11:59.000
, basically, common to all.

00:12:00.000 --> 00:12:05.500
 It's the three characteristics of just about everything.

00:12:05.500 --> 00:12:10.990
 The three characteristics, of course, are impermanent,

00:12:10.990 --> 00:12:17.990
 instability, uncertainty, unreliability, unpredictability,

00:12:17.990 --> 00:12:20.000
 all that.

00:12:21.000 --> 00:12:30.350
 Suffering, stress, dissatisfaction, inability to satisfy,

00:12:30.350 --> 00:12:40.000
 unhappiness, non-happiness, basically not being happiness.

00:12:41.000 --> 00:12:52.000
 And non-self, uncontrollability, insubstantiality.

00:12:52.000 --> 00:12:57.710
 This is the second thing you have to learn. This is really

00:12:57.710 --> 00:13:00.000
 what you start to see through the practice.

00:13:01.000 --> 00:13:05.810
 You start to readjust your understanding about things. You

00:13:05.810 --> 00:13:10.030
 see for the first time how much suffering we're causing

00:13:10.030 --> 00:13:11.000
 ourselves.

00:13:11.000 --> 00:13:18.520
 And you start to see the mistakes you're making. You're

00:13:18.520 --> 00:13:23.070
 making a mistake when you cling, when you want, when you

00:13:23.070 --> 00:13:30.000
 expect, because reality is inconstant, unpredictable.

00:13:30.000 --> 00:13:36.180
 Unsatisfying, can't satisfy. And uncontrollable, insub

00:13:36.180 --> 00:13:39.000
stantial, not-self.

00:13:39.000 --> 00:13:46.000
 It has no entity, no substantiality of its own.

00:13:53.000 --> 00:13:56.350
 Again, these three things are not some mystery. Everyone

00:13:56.350 --> 00:13:59.630
 reads about these and they think, "I don't see those things

00:13:59.630 --> 00:14:00.000
."

00:14:00.000 --> 00:14:03.640
 I've given this talk several times about how meditators

00:14:03.640 --> 00:14:07.350
 will come and say, "I'm not progressing. I just sit here

00:14:07.350 --> 00:14:12.300
 and my mind is in chaos and it's unpleasant and I can't

00:14:12.300 --> 00:14:14.000
 control that.

00:14:15.000 --> 00:14:20.010
 How can I progress it in this way? This is progress."

00:14:20.010 --> 00:14:25.990
 Seeing that your mind is unpredictable, chaotic, is a cause

00:14:25.990 --> 00:14:28.000
 of great stress.

00:14:28.000 --> 00:14:32.390
 Not all the clinging is a cause of great stress. We're

00:14:32.390 --> 00:14:36.470
 clinging to things that can't possibly satisfy us. That's

00:14:36.470 --> 00:14:38.000
 why we're stressed.

00:14:39.000 --> 00:14:43.100
 It's out of control. You can't control, you can't predict.

00:14:43.100 --> 00:14:46.000
 Expectations have no bearing on reality.

00:14:46.000 --> 00:14:48.850
 We act as though our expectations are going to somehow

00:14:48.850 --> 00:14:52.000
 dictate reality. It's quite silly, isn't it?

00:14:52.000 --> 00:14:55.140
 Somehow because we want things to be a certain way, that

00:14:55.140 --> 00:14:57.000
 they're going to be that way.

00:14:57.000 --> 00:15:05.000
 Such an odd idea, an odd concept. That's how we act.

00:15:06.000 --> 00:15:10.790
 Though you start to change this, you start to see that

00:15:10.790 --> 00:15:16.460
 wants, expectations, it's all highly problematic. It's the

00:15:16.460 --> 00:15:18.000
 main cause of suffering.

00:15:18.000 --> 00:15:23.060
 We can't predict, we can't expect, it doesn't really turn

00:15:23.060 --> 00:15:27.000
 out the way we want. It's unreliable.

00:15:28.000 --> 00:15:32.280
 This is the main insight. This is what starts to loosen up

00:15:32.280 --> 00:15:36.000
 the mind and free us from our bondage, from this obsession

00:15:36.000 --> 00:15:39.000
 that we have with pleasure and displeasure.

00:15:39.000 --> 00:15:45.440
 Attaining pleasure and removing or destroying, avoiding

00:15:45.440 --> 00:15:47.000
 displeasure.

00:15:52.000 --> 00:16:00.000
 Until eventually we start to see things with equanimity.

00:16:00.000 --> 00:16:09.590
 So the second is these characteristics. This is the

00:16:09.590 --> 00:16:13.470
 beginning of the path. The three characteristics are really

00:16:13.470 --> 00:16:15.000
 the beginning of insight.

00:16:16.000 --> 00:16:20.910
 Once you start to see these, you can say you've started on

00:16:20.910 --> 00:16:22.000
 the path.

00:16:22.000 --> 00:16:27.360
 Or you've opened the door, let's say. Seeing these is like

00:16:27.360 --> 00:16:30.080
 the door right before, when you open the door and you say,

00:16:30.080 --> 00:16:32.000
 "That's where I want to go."

00:16:32.000 --> 00:16:36.020
 So you open the door. The next one is manga, it's the path.

00:16:36.020 --> 00:16:40.710
 And that's what you walk. That's what all the meditators

00:16:40.710 --> 00:16:42.000
 here are on.

00:16:43.000 --> 00:16:46.490
 They're cultivating the path. The first few days are just

00:16:46.490 --> 00:16:49.850
 spent aligning yourself with the path. In fact, some of you

00:16:49.850 --> 00:16:51.000
 are still on this stage.

00:16:51.000 --> 00:16:53.680
 But as you go along, you start to see the three

00:16:53.680 --> 00:16:58.300
 characteristics. It'll come. These are not things you have

00:16:58.300 --> 00:16:59.000
 to look for.

00:16:59.000 --> 00:17:07.100
 All of these insights are like the stripes on a tiger. You

00:17:07.100 --> 00:17:08.000
 don't have to look for them.

00:17:09.000 --> 00:17:12.060
 Once you see the tiger, say, "There's the tiger. Yep, I see

00:17:12.060 --> 00:17:14.420
 it's stripes." They're right there with the tiger. You don

00:17:14.420 --> 00:17:16.000
't have to look for them.

00:17:16.000 --> 00:17:19.630
 They're not hard to find. These are not complex or

00:17:19.630 --> 00:17:24.310
 difficult things to understand. They're just hidden to us

00:17:24.310 --> 00:17:28.000
 because we're blind. Because we don't look.

00:17:29.000 --> 00:17:33.130
 Once we look, we'll see. So manga is as you start to

00:17:33.130 --> 00:17:37.040
 progress and you start to see deeper the three

00:17:37.040 --> 00:17:39.000
 characteristics.

00:17:39.000 --> 00:17:42.320
 And I've talked about this before in detail, the path. I

00:17:42.320 --> 00:17:44.000
 won't go into detail here.

00:17:47.000 --> 00:17:52.690
 But basically you start to see that reality is inconstant.

00:17:52.690 --> 00:17:59.100
 It arises and ceases. It's not actually suffering itself,

00:17:59.100 --> 00:18:01.000
 but only suffering because we cling to it.

00:18:01.000 --> 00:18:04.270
 Because we have expectations about it. And if we stop that,

00:18:04.270 --> 00:18:08.610
 we start to turn our way. We start to desire and incline

00:18:08.610 --> 00:18:11.000
 towards peace and freedom.

00:18:13.000 --> 00:18:16.070
 We start to lose our desire for any sort of arisen

00:18:16.070 --> 00:18:21.000
 experience. And the mind starts to quiet.

00:18:21.000 --> 00:18:26.630
 And it starts to become quite certain and sure of the

00:18:26.630 --> 00:18:31.560
 nature of reality. It sees it so clearly and so

00:18:31.560 --> 00:18:35.770
 consistently that there's clearly nothing worth clinging to

00:18:35.770 --> 00:18:36.000
.

00:18:37.000 --> 00:18:40.300
 There's no benefit that comes from holding on. I mean, this

00:18:40.300 --> 00:18:43.000
 is what you gain, the culmination of the path.

00:18:43.000 --> 00:18:53.340
 So this is the way you need to go through. For those of you

00:18:53.340 --> 00:18:57.420
 practicing at home, it's much longer and slower and more

00:18:57.420 --> 00:18:59.000
 difficult path.

00:19:00.000 --> 00:19:03.270
 Coming here might seem more difficult. Those meditators who

00:19:03.270 --> 00:19:06.600
 are here must certainly feel how difficult it is. But it's

00:19:06.600 --> 00:19:09.000
 so much easier in comparison.

00:19:09.000 --> 00:19:13.270
 It's not years and years or lifetimes of struggling just to

00:19:13.270 --> 00:19:17.420
 gain basic insight into reality. There's so much insight

00:19:17.420 --> 00:19:19.000
 that comes from being here.

00:19:20.000 --> 00:19:24.920
 So much purification and cleansing that goes on in the mind

00:19:24.920 --> 00:19:29.120
, freeing yourself. In such a short time, it's a great

00:19:29.120 --> 00:19:30.000
 blessing.

00:19:35.000 --> 00:19:38.720
 And so all you need is patience and you have to walk the

00:19:38.720 --> 00:19:42.720
 path. This is the main portion of our undertaking is to

00:19:42.720 --> 00:19:47.230
 follow the path and to see clearly and to cultivate and

00:19:47.230 --> 00:19:52.000
 accumulate this understanding of the three characteristics

00:19:52.000 --> 00:19:55.000
 of the nature of reality is not worth clinging to.

00:19:55.000 --> 00:20:02.000
 Learning to let go.

00:20:04.000 --> 00:20:08.100
 And so the fourth is Pala. This is the fruit. When the mind

00:20:08.100 --> 00:20:12.810
 finally lets go, at the end of the path, the mind sees so

00:20:12.810 --> 00:20:16.000
 clearly it releases, it lets go.

00:20:16.000 --> 00:20:22.500
 No more seeking, no more racing out to see. What's that? I

00:20:22.500 --> 00:20:26.110
 want to see it. What's that? I want to hear it. The mind no

00:20:26.110 --> 00:20:27.000
 longer.

00:20:28.000 --> 00:20:32.930
 Oh no, what's going on over here? No longer. The mind gets

00:20:32.930 --> 00:20:36.000
 fed up and says enough, enough with you all.

00:20:39.000 --> 00:20:44.470
 And it drops, it quiets, it becomes perfectly and

00:20:44.470 --> 00:20:51.180
 completely silent. There's cessation even. Cessation of a

00:20:51.180 --> 00:20:56.000
 risen experience. This is Nimbana.

00:21:04.000 --> 00:21:06.910
 And then Nimbana is something that is un-risened. So the

00:21:06.910 --> 00:21:15.000
 mind enters into an un-risened state, which is pure peace.

00:21:15.000 --> 00:21:18.830
 This is the final wisdom. This might not sound like wisdom

00:21:18.830 --> 00:21:22.000
 actually, but it is the most powerful wisdom.

00:21:23.000 --> 00:21:27.500
 And maybe the word "wisdom" is in English. We have too much

00:21:27.500 --> 00:21:32.360
 baggage surrounding that term, but "Panya" means perfectly,

00:21:32.360 --> 00:21:37.000
 rightly, strongly, "Nya" knowledge to know.

00:21:38.000 --> 00:21:42.540
 So when you know Nimbana, Nirvana is the highest knowledge.

00:21:42.540 --> 00:21:46.840
 To know Nimbana, there's nothing greater. There's nothing

00:21:46.840 --> 00:21:51.040
 that even compares. There's nothing that's anything close

00:21:51.040 --> 00:21:54.620
 to the experience of Nimbana in terms of changing who you

00:21:54.620 --> 00:21:58.920
 are, changing your life, changing your direction, changing

00:21:58.920 --> 00:22:03.000
 your mind, freeing you from stress and suffering.

00:22:04.000 --> 00:22:08.030
 But still quite simple. There's nothing hard to understand.

00:22:08.030 --> 00:22:11.500
 It's a bit scary, I suppose. Ooh, cessation. But I don't

00:22:11.500 --> 00:22:16.000
 want to cease, I want to go. What will happen to me? Right?

00:22:16.000 --> 00:22:20.000
 If there were a "me", you know?

00:22:20.000 --> 00:22:27.070
 But there's nothing hard to understand about these things.

00:22:27.070 --> 00:22:30.000
 They're quite simple.

00:22:33.000 --> 00:22:35.300
 Well, maybe that's not true. I think they are hard to

00:22:35.300 --> 00:22:37.680
 understand, but the problem is that we make more out of

00:22:37.680 --> 00:22:39.000
 them than they actually are.

00:22:39.000 --> 00:22:42.910
 They're hard for us to understand because we act and we

00:22:42.910 --> 00:22:47.480
 function on such a more complex level. An enlightened being

00:22:47.480 --> 00:22:49.000
 is so much simpler.

00:22:49.000 --> 00:22:52.700
 If you read the Buddhist text, they seem somewhat aggrav

00:22:52.700 --> 00:22:54.000
atingly simple.

00:22:55.000 --> 00:22:57.150
 The Buddha will give a talk just about seeing, hearing,

00:22:57.150 --> 00:22:59.650
 smelling, tasting, feeling, thinking. And if you're not a

00:22:59.650 --> 00:23:03.460
 Buddhist meditator, you think, "This is dumb. This is too

00:23:03.460 --> 00:23:04.000
..."

00:23:04.000 --> 00:23:09.090
 You might think how simplistic this is or meaningless. You

00:23:09.090 --> 00:23:11.410
 just can't make head or tail of it. It's like, "Why is he

00:23:11.410 --> 00:23:13.000
 talking about seeing?"

00:23:13.000 --> 00:23:18.460
 I remember hearing about Buddhism. I was in a guidebook

00:23:18.460 --> 00:23:21.000
 when I was in Thailand and it said,

00:23:22.000 --> 00:23:24.840
 "The Buddha taught that when you walk, just walk, when you

00:23:24.840 --> 00:23:27.000
 stand, just stand," or something like that.

00:23:27.000 --> 00:23:30.340
 And I thought, "Well, that's okay." I was trying to get it,

00:23:30.340 --> 00:23:33.510
 but inside I'm thinking, "That's it? What's even talking

00:23:33.510 --> 00:23:36.000
 about? What does that mean?"

00:23:36.000 --> 00:23:39.810
 It doesn't look like wisdom. This doesn't seem anything to

00:23:39.810 --> 00:23:41.000
 do with wisdom.

00:23:41.000 --> 00:23:45.900
 I went to Thailand looking for wisdom. When I went to the

00:23:45.900 --> 00:23:50.000
 meditation center, they asked me, curiously, they asked me,

00:23:51.000 --> 00:23:53.450
 "What do you hope to get from it?" I said, "I would like to

00:23:53.450 --> 00:23:54.000
 gain wisdom."

00:23:54.000 --> 00:23:58.000
 The first thing out of my mouth was what I was looking for.

00:23:58.000 --> 00:24:04.960
 But boy, the change that went in my understanding of what

00:24:04.960 --> 00:24:13.910
 wisdom was, I realized how ignorant I was of what wisdom

00:24:13.910 --> 00:24:15.000
 really means.

00:24:16.000 --> 00:24:20.000
 It has nothing to do with knowledge, with thinking, or

00:24:20.000 --> 00:24:21.000
 concepts.

00:24:21.000 --> 00:24:25.130
 Wisdom is, when the stomach rises, you know that it's

00:24:25.130 --> 00:24:27.000
 rising. That's wisdom.

00:24:27.000 --> 00:24:34.000
 Very, very profound. Very simple.

00:24:34.000 --> 00:24:39.630
 So there you go. That's the Dhamma for tonight. Thank you

00:24:39.630 --> 00:24:41.000
 all for tuning in.

