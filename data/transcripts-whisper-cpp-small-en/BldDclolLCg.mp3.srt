1
00:00:00,000 --> 00:00:03,120
 Hello and welcome back to Ask a Monk.

2
00:00:03,120 --> 00:00:08,920
 Now I'd like to answer a question about pain in meditation.

3
00:00:08,920 --> 00:00:13,240
 When you feel a great amount of pain during your meditation

4
00:00:13,240 --> 00:00:17,320
, is it necessary to sit still

5
00:00:17,320 --> 00:00:20,120
 and ensure that you never move?

6
00:00:20,120 --> 00:00:22,700
 If a person moves, is there something wrong with that?

7
00:00:22,700 --> 00:00:27,960
 Or is the point of meditation to be able to not move?

8
00:00:27,960 --> 00:00:31,280
 Is it wrong practice if you move your body?

9
00:00:31,280 --> 00:00:33,120
 And this is a very simple question with a very simple

10
00:00:33,120 --> 00:00:34,920
 answer, but it actually has some

11
00:00:34,920 --> 00:00:35,920
 profound implications.

12
00:00:35,920 --> 00:00:40,090
 So I'm going to take a little bit of time to answer it in

13
00:00:40,090 --> 00:00:41,400
 some detail.

14
00:00:41,400 --> 00:00:46,760
 The simple answer first is yes indeed you can move.

15
00:00:46,760 --> 00:00:49,800
 There's nothing intrinsically wrong with moving your body.

16
00:00:49,800 --> 00:00:51,680
 You know, we move it all the time.

17
00:00:51,680 --> 00:00:55,290
 The problem here, of course, that we have to understand is

18
00:00:55,290 --> 00:00:56,920
 that moving based on pain

19
00:00:56,920 --> 00:00:59,680
 creates aversion towards the pain.

20
00:00:59,680 --> 00:01:04,400
 Now that's all very well in theory.

21
00:01:04,400 --> 00:01:08,070
 You shouldn't move because if you move you're moving based

22
00:01:08,070 --> 00:01:09,120
 on aversion.

23
00:01:09,120 --> 00:01:12,950
 But it can actually go the other way where you crush your

24
00:01:12,950 --> 00:01:15,120
 aversion to the pain and you

25
00:01:15,120 --> 00:01:17,080
 force yourself to sit.

26
00:01:17,080 --> 00:01:23,890
 In fact creating more tension and more stress because there

27
00:01:23,890 --> 00:01:26,000
's the forcing.

28
00:01:26,000 --> 00:01:31,280
 There's the not complying with nature.

29
00:01:31,280 --> 00:01:35,100
 And in fact sometimes nature dictates that it's necessary

30
00:01:35,100 --> 00:01:35,880
 to move.

31
00:01:35,880 --> 00:01:39,760
 The appropriate reaction, the physical reaction is to move.

32
00:01:39,760 --> 00:01:43,160
 Once you get on in the meditation, this will change.

33
00:01:43,160 --> 00:01:45,280
 First of all there will be less pain.

34
00:01:45,280 --> 00:01:48,770
 Second of all it will be, it will bring less stress and

35
00:01:48,770 --> 00:01:50,200
 tension and less of a desire to

36
00:01:50,200 --> 00:01:51,200
 move.

37
00:01:51,200 --> 00:01:53,770
 The pain will become just another sensation and the mind

38
00:01:53,770 --> 00:01:55,080
 won't say something's wrong

39
00:01:55,080 --> 00:01:56,440
 or feel that something's wrong.

40
00:01:56,440 --> 00:01:59,750
 It will just see it as a sensation and be content with it

41
00:01:59,750 --> 00:02:01,000
 and not upset by it.

42
00:02:01,000 --> 00:02:03,000
 So you won't need to move.

43
00:02:03,000 --> 00:02:06,420
 Now in the beginning, this isn't the case, in the beginning

44
00:02:06,420 --> 00:02:08,000
 it brings more stress and

45
00:02:08,000 --> 00:02:10,600
 tension and really great problems.

46
00:02:10,600 --> 00:02:16,030
 So what we should do is be mindful of it and when it first

47
00:02:16,030 --> 00:02:19,160
 comes we should certainly try

48
00:02:19,160 --> 00:02:22,880
 to be as mindful of it as possible and not move without

49
00:02:22,880 --> 00:02:25,600
 immediately moving our bodies.

50
00:02:25,600 --> 00:02:28,530
 Because this is a great teacher for us and this is what I'd

51
00:02:28,530 --> 00:02:29,640
 like to get into.

52
00:02:29,640 --> 00:02:35,150
 But the basic technique is to say to ourselves, pain, pain,

53
00:02:35,150 --> 00:02:38,320
 pain, just quietly, not out loud

54
00:02:38,320 --> 00:02:41,270
 but in the mind, just focusing on the pain and reminding

55
00:02:41,270 --> 00:02:42,880
 ourselves, hey this is just

56
00:02:42,880 --> 00:02:45,040
 pain, there's nothing wrong with it.

57
00:02:45,040 --> 00:02:46,600
 It's not a negative experience.

58
00:02:46,600 --> 00:02:51,400
 There's nothing intrinsically bad about pain.

59
00:02:51,400 --> 00:02:54,480
 The universe really doesn't care whether you're in pain or

60
00:02:54,480 --> 00:02:56,240
 not and so it's not something that

61
00:02:56,240 --> 00:02:58,960
 is intrinsically negative.

62
00:02:58,960 --> 00:03:01,790
 What is negative is your negative reactions to it, your

63
00:03:01,790 --> 00:03:03,480
 anger, your frustration, your

64
00:03:03,480 --> 00:03:06,490
 thinking that it's bad, that it's a problem, that you have

65
00:03:06,490 --> 00:03:07,960
 to do something about it.

66
00:03:07,960 --> 00:03:11,030
 So when you see it simply as pain and all of that goes away

67
00:03:11,030 --> 00:03:12,760
, this is really the truth.

68
00:03:12,760 --> 00:03:14,560
 It's something that you should try.

69
00:03:14,560 --> 00:03:17,080
 These videos are not meant to just be, oh that sounds nice,

70
00:03:17,080 --> 00:03:18,240
 I agree with that and then

71
00:03:18,240 --> 00:03:21,820
 you go home or go back to Facebook or whatever.

72
00:03:21,820 --> 00:03:23,640
 These are something you should put into practice.

73
00:03:23,640 --> 00:03:27,610
 So when I say this, try it and find out for yourself

74
00:03:27,610 --> 00:03:31,480
 because it really has wonderful consequences.

75
00:03:31,480 --> 00:03:34,030
 Any kind of pain that you come up with in your life,

76
00:03:34,030 --> 00:03:35,640
 suddenly it's no longer a great

77
00:03:35,640 --> 00:03:38,480
 problem or a great difficulty.

78
00:03:38,480 --> 00:03:41,790
 It's something that you can deal with mindfulness and

79
00:03:41,790 --> 00:03:44,280
 clarity and wisdom and not have to suffer

80
00:03:44,280 --> 00:03:46,280
 from.

81
00:03:46,280 --> 00:03:48,830
 Once that becomes too much, in the beginning that's very

82
00:03:48,830 --> 00:03:50,280
 difficult to do and you're not

83
00:03:50,280 --> 00:03:53,030
 really mindful, you're mostly in pain, pain and you're

84
00:03:53,030 --> 00:03:54,680
 really angry about it anyway.

85
00:03:54,680 --> 00:03:58,270
 So saying pain isn't really acknowledging, it's reinforcing

86
00:03:58,270 --> 00:04:00,440
 the anger and the hate.

87
00:04:00,440 --> 00:04:03,300
 So when it gets to that point where it's overwhelming and

88
00:04:03,300 --> 00:04:05,240
 you feel like you have to move, then

89
00:04:05,240 --> 00:04:06,320
 just move mindfully.

90
00:04:06,320 --> 00:04:10,290
 You can lift your leg when moving, your hand moving, say to

91
00:04:10,290 --> 00:04:12,040
 yourself, moving, placing or

92
00:04:12,040 --> 00:04:15,600
 grasping, lifting, moving, placing or you can just move

93
00:04:15,600 --> 00:04:17,600
 your leg without, you know, if you're

94
00:04:17,600 --> 00:04:23,120
 sitting in this position, just lifting, moving, placing.

95
00:04:23,120 --> 00:04:25,710
 Just do it mindfully and it becomes a meditation because

96
00:04:25,710 --> 00:04:27,640
 eventually our whole life should become

97
00:04:27,640 --> 00:04:28,640
 meditation.

98
00:04:28,640 --> 00:04:32,210
 We should try to be as mindful as we can in our daily lives

99
00:04:32,210 --> 00:04:32,280
.

100
00:04:32,280 --> 00:04:35,320
 When we eat, we're mindful, when we drink, we're mindful.

101
00:04:35,320 --> 00:04:36,640
 Everything we do, we know what we're doing.

102
00:04:36,640 --> 00:04:40,760
 We move our hands, this should be moving, moving, shaking.

103
00:04:40,760 --> 00:04:42,600
 When we talk, we should know that we're talking.

104
00:04:42,600 --> 00:04:43,920
 What is this feeling?

105
00:04:43,920 --> 00:04:45,480
 My lips moving.

106
00:04:45,480 --> 00:04:48,480
 Even to that extent, you can be mindful.

107
00:04:48,480 --> 00:04:51,070
 When you're listening, you can say hearing, hearing and

108
00:04:51,070 --> 00:04:52,600
 acknowledging the sound and you'll

109
00:04:52,600 --> 00:04:55,510
 find that you really understand the meaning much better and

110
00:04:55,510 --> 00:04:56,560
 you get less caught up in

111
00:04:56,560 --> 00:04:59,780
 your own emotions and judgments and so on.

112
00:04:59,780 --> 00:05:03,000
 So this is the basic technique.

113
00:05:03,000 --> 00:05:07,770
 Now the theory behind pain and how it's such a good teacher

114
00:05:07,770 --> 00:05:09,900
 is that eventually pain is

115
00:05:09,900 --> 00:05:15,120
 one of the best teachers that we have because pain is

116
00:05:15,120 --> 00:05:18,480
 something that we don't like.

117
00:05:18,480 --> 00:05:20,600
 It's our aversion.

118
00:05:20,600 --> 00:05:24,320
 It's our problem.

119
00:05:24,320 --> 00:05:28,070
 If it weren't for pain, if it weren't for things like pain,

120
00:05:28,070 --> 00:05:29,680
 then we would never need

121
00:05:29,680 --> 00:05:31,080
 to practice meditation.

122
00:05:31,080 --> 00:05:33,720
 We would never think to come and practice meditation.

123
00:05:33,720 --> 00:05:34,720
 Why would we?

124
00:05:34,720 --> 00:05:35,720
 What's the point?

125
00:05:35,720 --> 00:05:38,740
 Wasting all this time when we can be out enjoying life.

126
00:05:38,740 --> 00:05:41,240
 But the problem is that in our life, it's not all fun and

127
00:05:41,240 --> 00:05:41,760
 games.

128
00:05:41,760 --> 00:05:45,260
 In fact, our clinging leads us to have suffering because we

129
00:05:45,260 --> 00:05:47,040
 don't like certain things.

130
00:05:47,040 --> 00:05:49,520
 We're not content with thinking about the good things.

131
00:05:49,520 --> 00:05:52,980
 So when a bad thing comes, it makes us angry because it's

132
00:05:52,980 --> 00:05:54,760
 not what we want and so on.

133
00:05:54,760 --> 00:06:00,810
 So what we're basically doing here is learning how to live

134
00:06:00,810 --> 00:06:03,680
 with it and how to see it for

135
00:06:03,680 --> 00:06:06,520
 what it is and see it simply as another experience.

136
00:06:06,520 --> 00:06:09,530
 Not as a bad thing and not seeing other things as good

137
00:06:09,530 --> 00:06:10,240
 things.

138
00:06:10,240 --> 00:06:14,340
 And so the theory here is that most people understand, it

139
00:06:14,340 --> 00:06:17,000
 has to do with how people understand

140
00:06:17,000 --> 00:06:18,840
 suffering.

141
00:06:18,840 --> 00:06:23,080
 An ordinary person understands suffering to be, and

142
00:06:23,080 --> 00:06:25,880
 suffering here is of course the big

143
00:06:25,880 --> 00:06:26,880
 elephant in the room.

144
00:06:26,880 --> 00:06:30,120
 It's what we as Buddhists deal with.

145
00:06:30,120 --> 00:06:33,030
 It's what we talk about and what we teach and yet always

146
00:06:33,030 --> 00:06:34,760
 have a very difficult, a great

147
00:06:34,760 --> 00:06:36,800
 amount of difficulty talking about.

148
00:06:36,800 --> 00:06:40,130
 But this is because most people understand suffering to be

149
00:06:40,130 --> 00:06:41,640
 the pain and suffering to

150
00:06:41,640 --> 00:06:42,640
 be a feeling.

151
00:06:42,640 --> 00:06:44,360
 This is what is a dukkha vedana.

152
00:06:44,360 --> 00:06:47,420
 Dukkha vedana means "dukka is suffering, vedana is the

153
00:06:47,420 --> 00:06:48,280
 feeling."

154
00:06:48,280 --> 00:06:50,890
 So we understand dukkha to be a vedana, to be a feeling

155
00:06:50,890 --> 00:06:51,760
 that you get.

156
00:06:51,760 --> 00:06:53,800
 You get this feeling, that's dukkha, that's suffering.

157
00:06:53,800 --> 00:06:55,240
 You get that feeling, that's suffering.

158
00:06:55,240 --> 00:06:58,120
 You get another feeling and it's not suffering.

159
00:06:58,120 --> 00:07:03,280
 So we categorize our experiences.

160
00:07:03,280 --> 00:07:04,280
 This one is suffering.

161
00:07:04,280 --> 00:07:06,400
 It's not suffering.

162
00:07:06,400 --> 00:07:12,480
 And as a result, our means of overcoming suffering, of

163
00:07:12,480 --> 00:07:16,240
 being free from suffering is to find a

164
00:07:16,240 --> 00:07:20,210
 way to only have these experiences and to never have these

165
00:07:20,210 --> 00:07:21,200
 experiences.

166
00:07:21,200 --> 00:07:22,560
 Doesn't that sound normal?

167
00:07:22,560 --> 00:07:24,760
 That sounds, yeah, that's how we get rid of suffering.

168
00:07:24,760 --> 00:07:26,680
 That's because that's what we're told.

169
00:07:26,680 --> 00:07:27,680
 That's what we're taught.

170
00:07:27,680 --> 00:07:29,080
 And it's totally false.

171
00:07:29,080 --> 00:07:30,280
 It's totally wrong.

172
00:07:30,280 --> 00:07:32,560
 It's the wrong way to deal with suffering.

173
00:07:32,560 --> 00:07:34,000
 But this is how we do.

174
00:07:34,000 --> 00:07:35,000
 Why is it wrong?

175
00:07:35,000 --> 00:07:38,680
 Okay, so you have pain in the leg, you move your leg, and

176
00:07:38,680 --> 00:07:40,880
 then you have pain in the back,

177
00:07:40,880 --> 00:07:42,960
 and so then you have to stretch your back.

178
00:07:42,960 --> 00:07:45,540
 And then you have pain in your head, and so you have to

179
00:07:45,540 --> 00:07:46,920
 take a pill for the pain.

180
00:07:46,920 --> 00:07:49,380
 And you have pain here, pain there, and you have to take

181
00:07:49,380 --> 00:07:50,640
 another pill, or you have to

182
00:07:50,640 --> 00:07:53,400
 get an operation, and so on and so on.

183
00:07:53,400 --> 00:07:56,320
 And all this time, you're developing more and more aversion

184
00:07:56,320 --> 00:07:57,760
 to the pain until eventually

185
00:07:57,760 --> 00:08:00,280
 it becomes totally overwhelming.

186
00:08:00,280 --> 00:08:03,450
 When a person takes a pill for a headache, really what they

187
00:08:03,450 --> 00:08:05,400
're doing is reaffirming their

188
00:08:05,400 --> 00:08:07,160
 aversion towards the pain.

189
00:08:07,160 --> 00:08:09,280
 And as a result, it's going to be worse next time.

190
00:08:09,280 --> 00:08:13,500
 It's going to, yeah, it's going to be worse and worse and

191
00:08:13,500 --> 00:08:13,840
 worse.

192
00:08:13,840 --> 00:08:16,470
 Not because the pain changes, but because our attitude

193
00:08:16,470 --> 00:08:18,120
 towards it is reaffirmed as being

194
00:08:18,120 --> 00:08:21,430
 this is bad, this is bad, this is bad, bad, bad, until it

195
00:08:21,430 --> 00:08:23,320
 becomes totally unbearable.

196
00:08:23,320 --> 00:08:25,240
 These things are not static.

197
00:08:25,240 --> 00:08:26,720
 Craving is not static.

198
00:08:26,720 --> 00:08:27,960
 It builds.

199
00:08:27,960 --> 00:08:29,800
 It turns into a habit.

200
00:08:29,800 --> 00:08:34,800
 It changes our whole vibration in that direction, and it

201
00:08:34,800 --> 00:08:38,200
 changes the world around us as well.

202
00:08:38,200 --> 00:08:39,200
 Anger does as well.

203
00:08:39,200 --> 00:08:41,360
 Aversion does as well.

204
00:08:41,360 --> 00:08:46,450
 We become totally averse to these things and unable to bear

205
00:08:46,450 --> 00:08:46,960
 them.

206
00:08:46,960 --> 00:08:51,680
 So what would we come to see, and the reason why we come to

207
00:08:51,680 --> 00:08:54,120
 practice meditation is the

208
00:08:54,120 --> 00:08:57,350
 second type of suffering, the Buddha said, the dukka sabh

209
00:08:57,350 --> 00:08:57,800
ava.

210
00:08:57,800 --> 00:09:01,730
 Dukka sabhava means reality of suffering, that suffering is

211
00:09:01,730 --> 00:09:02,320
 a fact.

212
00:09:02,320 --> 00:09:04,200
 You can't escape it.

213
00:09:04,200 --> 00:09:06,200
 It's reality.

214
00:09:06,200 --> 00:09:07,480
 It's a part of reality.

215
00:09:07,480 --> 00:09:10,230
 And this is like getting old is a part of reality, getting

216
00:09:10,230 --> 00:09:11,640
 sick is a part of reality,

217
00:09:11,640 --> 00:09:13,040
 dying is a part of reality.

218
00:09:13,040 --> 00:09:21,840
 So these methods of overcoming suffering or these ways of

219
00:09:21,840 --> 00:09:25,560
 avoiding the unpleasant part

220
00:09:25,560 --> 00:09:30,900
 of reality are temporary, are ineffective, because it's

221
00:09:30,900 --> 00:09:33,680
 there, it's a part of life.

222
00:09:33,680 --> 00:09:35,080
 It's not going to go away.

223
00:09:35,080 --> 00:09:37,760
 You're not going to be to remove it.

224
00:09:37,760 --> 00:09:39,040
 You're not going to get rid of sickness.

225
00:09:39,040 --> 00:09:40,320
 You're not going to get rid of old age.

226
00:09:40,320 --> 00:09:42,360
 You're not going to get rid of death.

227
00:09:42,360 --> 00:09:44,960
 These things are going to come to you.

228
00:09:44,960 --> 00:09:48,210
 And it's this realization, when we have a situation that we

229
00:09:48,210 --> 00:09:49,720
 can't overcome, people who

230
00:09:49,720 --> 00:09:53,000
 have migraine headaches, they've taken every pill and

231
00:09:53,000 --> 00:09:55,240
 nothing works, people who are mourning

232
00:09:55,240 --> 00:09:57,080
 a person who passed away.

233
00:09:57,080 --> 00:10:00,150
 They can drink or whatever, but nothing works and they're

234
00:10:00,150 --> 00:10:02,120
 still sad, they're still thinking

235
00:10:02,120 --> 00:10:04,520
 about the person, nothing works.

236
00:10:04,520 --> 00:10:07,350
 When a person gets to this point, this is most often when

237
00:10:07,350 --> 00:10:09,400
 they begin to practice meditation.

238
00:10:09,400 --> 00:10:11,650
 When they get to the point where they realize they're

239
00:10:11,650 --> 00:10:13,080
 totally on the wrong path, when they

240
00:10:13,080 --> 00:10:18,310
 realize that this reaffirmation of greed, anger, delusion

241
00:10:18,310 --> 00:10:20,800
 is totally dragging them down

242
00:10:20,800 --> 00:10:23,870
 on the wrong path and is not helping their reactions

243
00:10:23,870 --> 00:10:26,040
 towards suffering, their ways of

244
00:10:26,040 --> 00:10:29,040
 dealing with suffering are ineffective.

245
00:10:29,040 --> 00:10:31,440
 This is the second type of suffering.

246
00:10:31,440 --> 00:10:34,070
 It's a very important realization because that's what leads

247
00:10:34,070 --> 00:10:35,160
 us, that's what leads to

248
00:10:35,160 --> 00:10:37,280
 the conclusion that we have to do something.

249
00:10:37,280 --> 00:10:38,440
 People say, "Why do you meditate?

250
00:10:38,440 --> 00:10:40,560
 What's the point wasting your time?"

251
00:10:40,560 --> 00:10:43,610
 Well, those people have not yet realized, this is what they

252
00:10:43,610 --> 00:10:44,880
 haven't yet realized.

253
00:10:44,880 --> 00:10:47,630
 For a person who has realized that, "You can't really

254
00:10:47,630 --> 00:10:48,400
 escape it.

255
00:10:48,400 --> 00:10:50,680
 You can say I go out and party and I'm happy.

256
00:10:50,680 --> 00:10:55,040
 I've tried it and I'm not able to escape suffering in the

257
00:10:55,040 --> 00:10:57,480
 way you pretend or you think you're

258
00:10:57,480 --> 00:10:58,720
 able to."

259
00:10:58,720 --> 00:11:02,240
 My experience is that suffering is a part of life.

260
00:11:02,240 --> 00:11:06,450
 It's something that we either learn to deal with or we

261
00:11:06,450 --> 00:11:09,160
 suffer more and more and more.

262
00:11:09,160 --> 00:11:11,000
 It gets worse and worse and worse.

263
00:11:11,000 --> 00:11:14,130
 So we find a way, so this is when we come to practice

264
00:11:14,130 --> 00:11:15,160
 meditation.

265
00:11:15,160 --> 00:11:18,120
 When we come to practice meditation, we realize the third

266
00:11:18,120 --> 00:11:20,360
 truth, the third type of suffering.

267
00:11:20,360 --> 00:11:25,640
 The third way of understanding this word, suffering.

268
00:11:25,640 --> 00:11:30,640
 This is that suffering is inherent in all things, just as

269
00:11:30,640 --> 00:11:33,080
 heat is inherent in fire.

270
00:11:33,080 --> 00:11:35,280
 There's no fire that is not hot.

271
00:11:35,280 --> 00:11:36,800
 Fire is hot.

272
00:11:36,800 --> 00:11:42,080
 That's, well, in a conventional sense, fire is hot.

273
00:11:42,080 --> 00:11:46,310
 By the same token, all things in the world, anything that

274
00:11:46,310 --> 00:11:48,120
 arises is suffering.

275
00:11:48,120 --> 00:11:52,800
 What we mean by this is just as fire is hot, if you hold on

276
00:11:52,800 --> 00:11:54,880
 to it, it burns you.

277
00:11:54,880 --> 00:11:58,350
 When something arises and you cling to it, it makes you

278
00:11:58,350 --> 00:11:59,120
 suffer.

279
00:11:59,120 --> 00:12:01,280
 Why this happens is basically what I've been saying.

280
00:12:01,280 --> 00:12:04,020
 When it's something good, you cling to it as good and then

281
00:12:04,020 --> 00:12:05,020
 it disappears.

282
00:12:05,020 --> 00:12:08,230
 When it disappears, you're unhappy, you're looking for it

283
00:12:08,230 --> 00:12:10,000
 because there's still the wanting,

284
00:12:10,000 --> 00:12:11,440
 there's still the clinging in your mind.

285
00:12:11,440 --> 00:12:14,550
 And in fact, you cultivate it and develop it and it comes

286
00:12:14,550 --> 00:12:16,160
 up again and again, "I want

287
00:12:16,160 --> 00:12:18,600
 this, I want this, I like this, I like this."

288
00:12:18,600 --> 00:12:23,140
 When it's gone, the craving doesn't go away, the experience

289
00:12:23,140 --> 00:12:25,080
 is gone and who knows when it'll

290
00:12:25,080 --> 00:12:26,080
 come back.

291
00:12:26,080 --> 00:12:29,360
 If it's an unpleasant experience, the craving, aversion, go

292
00:12:29,360 --> 00:12:30,920
 away, go away and pushing it

293
00:12:30,920 --> 00:12:34,560
 away with this method or that method makes it worse and so

294
00:12:34,560 --> 00:12:35,000
 on.

295
00:12:35,000 --> 00:12:38,640
 When we practice meditation, we come to realize that, "No,

296
00:12:38,640 --> 00:12:40,480
 these things are not really going

297
00:12:40,480 --> 00:12:41,480
 to make me happy.

298
00:12:41,480 --> 00:12:43,320
 Clinging to good things is not going to make me happy."

299
00:12:43,320 --> 00:12:44,320
 Why?

300
00:12:44,320 --> 00:12:45,920
 Because they come and they go.

301
00:12:45,920 --> 00:12:47,280
 They arise and they cease.

302
00:12:47,280 --> 00:12:50,360
 If my happiness depends on that, how can I ever hope to be

303
00:12:50,360 --> 00:12:50,960
 happy?

304
00:12:50,960 --> 00:12:54,290
 If say my happiness depends on this person, you come to

305
00:12:54,290 --> 00:12:56,320
 realize that that person is only

306
00:12:56,320 --> 00:12:57,320
 experiences.

307
00:12:57,320 --> 00:12:58,440
 You hear something and that's that person.

308
00:12:58,440 --> 00:13:00,440
 You see something and that's that person.

309
00:13:00,440 --> 00:13:02,560
 You think of something and that's that person.

310
00:13:02,560 --> 00:13:06,560
 But all of those experiences are impermanent and they're

311
00:13:06,560 --> 00:13:07,620
 uncertain.

312
00:13:07,620 --> 00:13:09,460
 You don't know whether they're going to say something good

313
00:13:09,460 --> 00:13:09,840
 to you.

314
00:13:09,840 --> 00:13:12,340
 Maybe someone you love says something bad to you and you

315
00:13:12,340 --> 00:13:13,240
 suffer from it.

316
00:13:13,240 --> 00:13:15,880
 And the realization that it's not sure.

317
00:13:15,880 --> 00:13:18,200
 There's no person there that's wonderful.

318
00:13:18,200 --> 00:13:20,830
 There's a bunch of experiences waiting to happen and those

319
00:13:20,830 --> 00:13:21,840
 experiences will not all

320
00:13:21,840 --> 00:13:23,200
 be pleasant.

321
00:13:23,200 --> 00:13:26,370
 When the person dies if they're a loved one, then that will

322
00:13:26,370 --> 00:13:28,120
 be a very unpleasant thing.

323
00:13:28,120 --> 00:13:31,240
 This realization we come to through our meditation practice

324
00:13:31,240 --> 00:13:33,240
, even just watching our own body,

325
00:13:33,240 --> 00:13:36,550
 watching the rise and falling on the stomach, coming to see

326
00:13:36,550 --> 00:13:38,840
 that every single thing that

327
00:13:38,840 --> 00:13:40,880
 arises has to pass away.

328
00:13:40,880 --> 00:13:43,160
 It comes and it goes.

329
00:13:43,160 --> 00:13:46,060
 Looking to see that we do it even with our stomach, rising

330
00:13:46,060 --> 00:13:47,800
 and falling, oh now it's smooth

331
00:13:47,800 --> 00:13:49,080
 and then we like it.

332
00:13:49,080 --> 00:13:51,270
 And then suddenly it changes and it's rough and

333
00:13:51,270 --> 00:13:53,200
 uncomfortable and we can't really find

334
00:13:53,200 --> 00:13:55,840
 it and we're angry and upset.

335
00:13:55,840 --> 00:13:58,430
 Even this very stupid simple object of the stomach can

336
00:13:58,430 --> 00:13:59,960
 teach us everything we need to

337
00:13:59,960 --> 00:14:03,800
 know about reality because it shows us our mind.

338
00:14:03,800 --> 00:14:07,970
 It shows us the way we project and we relate to things and

339
00:14:07,970 --> 00:14:09,920
 make more out of things than

340
00:14:09,920 --> 00:14:12,880
 they actually are.

341
00:14:12,880 --> 00:14:16,170
 Through the meditation practice we come to break down

342
00:14:16,170 --> 00:14:18,960
 people, break down things and experiences

343
00:14:18,960 --> 00:14:25,680
 into individual phenomenon that arise and come and go.

344
00:14:25,680 --> 00:14:30,250
 As a result we see that none of them are pleasant or none

345
00:14:30,250 --> 00:14:32,560
 of them are satisfying.

346
00:14:32,560 --> 00:14:34,920
 And this is what it means by suffering, that our happiness

347
00:14:34,920 --> 00:14:36,200
 should never depend on these

348
00:14:36,200 --> 00:14:37,200
 things.

349
00:14:37,200 --> 00:14:39,980
 It depends on a person, that person doesn't exist.

350
00:14:39,980 --> 00:14:42,840
 It's just the experiences coming to see that our

351
00:14:42,840 --> 00:14:45,680
 relationship with the person is just through

352
00:14:45,680 --> 00:14:48,800
 experiences which are never going to satisfy us.

353
00:14:48,800 --> 00:14:51,250
 They're only going to, the only thing they can possibly do

354
00:14:51,250 --> 00:14:52,400
 is bring more clinging and

355
00:14:52,400 --> 00:14:57,300
 craving and dissatisfaction and the need for more and so on

356
00:14:57,300 --> 00:14:57,440
.

357
00:14:57,440 --> 00:15:00,320
 And we will never truly be at peace with ourselves.

358
00:15:00,320 --> 00:15:02,600
 People think that they will but you can look at those

359
00:15:02,600 --> 00:15:04,760
 people and see that they're not really

360
00:15:04,760 --> 00:15:07,680
 truly at peace with themselves.

361
00:15:07,680 --> 00:15:09,810
 And through the meditation we come to see this and so we

362
00:15:09,810 --> 00:15:11,640
 come to gain true peace for ourselves.

363
00:15:11,640 --> 00:15:14,720
 As you see these things, you know, you'll see things

364
00:15:14,720 --> 00:15:16,760
 arising even just the stomach coming

365
00:15:16,760 --> 00:15:19,640
 and going and you'll see the clinging and how useless it is

366
00:15:19,640 --> 00:15:20,840
 to cling to it to be this

367
00:15:20,840 --> 00:15:23,280
 way or that way because it's changing.

368
00:15:23,280 --> 00:15:25,360
 You see it coming and going and that it's not under your

369
00:15:25,360 --> 00:15:25,840
 control.

370
00:15:25,840 --> 00:15:28,730
 You see impermanent suffering, non-self, these three

371
00:15:28,730 --> 00:15:29,900
 characteristics.

372
00:15:29,900 --> 00:15:33,450
 As this goes on, the fourth type of suffering or the fourth

373
00:15:33,450 --> 00:15:35,560
 way of understanding suffering

374
00:15:35,560 --> 00:15:38,000
 comes to you and that is as the truth.

375
00:15:38,000 --> 00:15:40,050
 And this is what we call, you know, you hear about the

376
00:15:40,050 --> 00:15:41,360
 noble truth, the noble truth of

377
00:15:41,360 --> 00:15:42,760
 suffering.

378
00:15:42,760 --> 00:15:44,840
 And this is the realization that we're hoping for.

379
00:15:44,840 --> 00:15:50,800
 This realization, it's not a good, it's not satisfying.

380
00:15:50,800 --> 00:15:53,560
 These are not going to make me happy.

381
00:15:53,560 --> 00:15:58,200
 This realization is the fourth type of way of understanding

382
00:15:58,200 --> 00:15:58,960
 suffering.

383
00:15:58,960 --> 00:16:01,040
 Is really the consummation of the Buddha's teaching.

384
00:16:01,040 --> 00:16:03,180
 It's just, you know, starting to see things, seeing things

385
00:16:03,180 --> 00:16:04,400
 coming and going, oh this isn't

386
00:16:04,400 --> 00:16:05,400
 satisfying that.

387
00:16:05,400 --> 00:16:08,240
 And you just realize at some point that this isn't the way

388
00:16:08,240 --> 00:16:09,440
 to find happiness.

389
00:16:09,440 --> 00:16:11,830
 It's not intellectual, of course, I'm just putting words to

390
00:16:11,830 --> 00:16:14,080
 it, but it's suddenly a boom.

391
00:16:14,080 --> 00:16:17,900
 The mind just says, no, and the mind gives up, the mind

392
00:16:17,900 --> 00:16:18,720
 lets go.

393
00:16:18,720 --> 00:16:24,360
 And at that point there's freedom, the mind is released and

394
00:16:24,360 --> 00:16:26,840
 you enter into this state

395
00:16:26,840 --> 00:16:29,520
 of total freedom which really there's no experience at all.

396
00:16:29,520 --> 00:16:31,680
 There's no seeing, hearing, smelling, tasting, feeling or

397
00:16:31,680 --> 00:16:32,320
 even thinking.

398
00:16:32,320 --> 00:16:35,670
 You're not even aware, it's like, kind of like falling

399
00:16:35,670 --> 00:16:37,680
 asleep, but it's total peace.

400
00:16:37,680 --> 00:16:40,970
 And when you come back, you realize, wow, I just totally

401
00:16:40,970 --> 00:16:41,600
 let go.

402
00:16:41,600 --> 00:16:48,480
 You know, there was no arising of anything at that point.

403
00:16:48,480 --> 00:16:51,320
 And this is, you know, we call nirvana, this realization.

404
00:16:51,320 --> 00:16:54,040
 And a person starts to realize this and looking around and

405
00:16:54,040 --> 00:16:55,760
 seeing that their happiness can't

406
00:16:55,760 --> 00:16:58,720
 come from anything outside of themselves.

407
00:16:58,720 --> 00:17:01,330
 And so they cultivate this more and more and more and start

408
00:17:01,330 --> 00:17:02,720
 to see the truth more and more

409
00:17:02,720 --> 00:17:06,860
 and more and eventually are able to become totally free

410
00:17:06,860 --> 00:17:08,320
 from suffering.

411
00:17:08,320 --> 00:17:11,950
 So the point being that it's this realization, when you

412
00:17:11,950 --> 00:17:14,040
 talk about nirvana and so on, and

413
00:17:14,040 --> 00:17:17,920
 this, see for yourself, the Buddha's teaching is to see for

414
00:17:17,920 --> 00:17:18,800
 yourself.

415
00:17:18,800 --> 00:17:21,560
 But the point which I think everyone can understand is that

416
00:17:21,560 --> 00:17:23,280
 we have to see this reality, we have

417
00:17:23,280 --> 00:17:25,890
 to really grasp this, that we're not going to find

418
00:17:25,890 --> 00:17:27,800
 happiness in the things outside of

419
00:17:27,800 --> 00:17:29,400
 ourselves.

420
00:17:29,400 --> 00:17:35,890
 And that these phenomena that arise, there's no good that

421
00:17:35,890 --> 00:17:39,160
 can come from clinging, even

422
00:17:39,160 --> 00:17:40,880
 in terms of aversion.

423
00:17:40,880 --> 00:17:44,280
 There's no good that comes from wanting to escape it or

424
00:17:44,280 --> 00:17:46,360
 wanting to even force yourself

425
00:17:46,360 --> 00:17:48,680
 to sit still in this case.

426
00:17:48,680 --> 00:17:51,160
 So it's a very simple question with a very simple answer,

427
00:17:51,160 --> 00:17:53,000
 but here are the profound implications

428
00:17:53,000 --> 00:17:55,780
 to do with suffering that help us to understand why it is

429
00:17:55,780 --> 00:17:57,520
 that we might want to sit through

430
00:17:57,520 --> 00:17:58,520
 it sometimes.

431
00:17:58,520 --> 00:18:01,990
 And if we can get that through our head, then sometimes we

432
00:18:01,990 --> 00:18:03,360
 can sit through it.

433
00:18:03,360 --> 00:18:05,770
 We say, "No, yeah, I want to move, but it's not really a

434
00:18:05,770 --> 00:18:06,400
 big deal.

435
00:18:06,400 --> 00:18:09,280
 It's not going to kill me and I'm going to learn something

436
00:18:09,280 --> 00:18:09,840
 here."

437
00:18:09,840 --> 00:18:13,200
 And as you do that, you'll see how your mind works, you'll

438
00:18:13,200 --> 00:18:15,120
 see the craving, the aversion,

439
00:18:15,120 --> 00:18:17,940
 and you'll see how the phenomenon works and how it arises

440
00:18:17,940 --> 00:18:19,520
 and ceases and see that there's

441
00:18:19,520 --> 00:18:22,190
 no good that comes from being angry about it or upset about

442
00:18:22,190 --> 00:18:22,560
 it.

443
00:18:22,560 --> 00:18:25,570
 You don't feel happier, it doesn't solve the problem, it

444
00:18:25,570 --> 00:18:26,960
 creates more aversion.

445
00:18:26,960 --> 00:18:29,260
 Moving your foot doesn't solve the problem, it's a

446
00:18:29,260 --> 00:18:30,920
 temporary solution to kind of ease

447
00:18:30,920 --> 00:18:33,480
 the pressure when it's too much for you.

448
00:18:33,480 --> 00:18:36,750
 But eventually, you're just going to say, "Why would I move

449
00:18:36,750 --> 00:18:37,760
 my foot again?

450
00:18:37,760 --> 00:18:40,060
 I've moved my foot ten times already, it didn't solve the

451
00:18:40,060 --> 00:18:40,600
 problem."

452
00:18:40,600 --> 00:18:43,200
 And eventually you give that up and you say, "Well, I'll

453
00:18:43,200 --> 00:18:44,400
 just sit through it."

454
00:18:44,400 --> 00:18:46,050
 That's what happens in meditation when you go to an

455
00:18:46,050 --> 00:18:46,800
 intensive course.

456
00:18:46,800 --> 00:18:49,120
 In the beginning, in pain here, you say, "Okay, I can fix

457
00:18:49,120 --> 00:18:49,520
 that."

458
00:18:49,520 --> 00:18:51,300
 And you put a pillow under here and then this one, and then

459
00:18:51,300 --> 00:18:52,360
 you put a pillow under here,

460
00:18:52,360 --> 00:18:55,280
 and then here you put a pillow, and here you put a pillow,

461
00:18:55,280 --> 00:18:56,720
 until eventually you just say,

462
00:18:56,720 --> 00:19:00,280
 "Oh, it's not working, it's not solving the problem."

463
00:19:00,280 --> 00:19:03,630
 And you give up, and that's the realization that we're

464
00:19:03,630 --> 00:19:04,280
 striving for.

465
00:19:04,280 --> 00:19:06,680
 You realize that more and more and more.

466
00:19:06,680 --> 00:19:09,440
 Eventually you'll realize that that's the truth, that's the

467
00:19:09,440 --> 00:19:10,640
 truth of reality, and you'll

468
00:19:10,640 --> 00:19:12,960
 let go and not cling.

469
00:19:12,960 --> 00:19:17,310
 So this is my discussion of the truth of suffering in the

470
00:19:17,310 --> 00:19:19,160
 Buddha's teaching.

471
00:19:19,160 --> 00:19:21,860
 Thanks for tuning in, and I wish you all the best that you

472
00:19:21,860 --> 00:19:23,200
're all able to put this into

473
00:19:23,200 --> 00:19:27,170
 practice and try your best to become patient and be able to

474
00:19:27,170 --> 00:19:29,440
 overcome your attachments and

475
00:19:29,440 --> 00:19:33,480
 aversions and suffering and find true peace, happiness and

476
00:19:33,480 --> 00:19:35,320
 freedom from suffering.

477
00:19:35,320 --> 00:19:36,200
 So all the best.

478
00:19:36,200 --> 00:19:37,200
 Thank you.

