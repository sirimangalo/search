1
00:00:00,000 --> 00:00:02,000
 I'm not impressed.

2
00:00:02,000 --> 00:00:08,080
 Okay, now we're really broadcasting live.

3
00:00:08,080 --> 00:00:15,890
 November 7th, 2015. Now let's make sure we've got

4
00:00:15,890 --> 00:00:17,200
 everything set up.

5
00:00:17,200 --> 00:00:22,070
 Yes, yes, yes, okay. And everything should be working

6
00:00:22,070 --> 00:00:22,960
 properly.

7
00:00:22,960 --> 00:00:24,960
 So.

8
00:00:24,960 --> 00:00:43,340
 We have a Yelp listing. We have a thanks to Anna. She

9
00:00:43,340 --> 00:00:46,320
 created a listing for the meditation center on Yelp.

10
00:00:46,880 --> 00:00:53,920
 So if you put in meditation in Hamilton, you do pop up.

11
00:00:53,920 --> 00:00:58,040
 It's a simple listing so far. You can probably put some

12
00:00:58,040 --> 00:01:02,730
 pictures up there and make it a little fancier, but it's

13
00:01:02,730 --> 00:01:04,400
 there.

14
00:01:04,400 --> 00:01:09,120
 And shortly we should be getting confirmation to get

15
00:01:09,120 --> 00:01:11,680
 everything tied up for Google as well.

16
00:01:11,680 --> 00:01:15,210
 So that'll be a great thing when you type it in and the

17
00:01:15,210 --> 00:01:19,200
 driving directions come up and all that kind of thing.

18
00:01:19,200 --> 00:01:21,200
 Just a little more verification needed.

19
00:01:21,200 --> 00:01:26,560
 Just letting people know about the meditation center.

20
00:01:26,560 --> 00:01:30,160
 Awesome. Yeah.

21
00:01:30,160 --> 00:01:34,160
 And tomorrow we have our volunteer meeting so we can maybe

22
00:01:34,160 --> 00:01:36,560
 discuss that a little bit tomorrow.

23
00:01:38,720 --> 00:01:42,080
 That's at 1. Yes.

24
00:01:42,080 --> 00:01:49,760
 Tonight we have an interesting verse.

25
00:01:49,760 --> 00:01:53,950
 It's a very deep meditative one. I encourage everyone to

26
00:01:53,950 --> 00:01:54,480
 read this.

27
00:01:54,480 --> 00:01:58,380
 Not sure how much I can talk about it, but there is to be

28
00:01:58,380 --> 00:01:58,800
 said.

29
00:01:58,800 --> 00:02:02,000
 But it's certainly worth reading.

30
00:02:05,920 --> 00:02:11,140
 It's the dart where you have a thorn in your side and then

31
00:02:11,140 --> 00:02:15,280
 to get it out you stick another thorn in your side.

32
00:02:15,280 --> 00:02:22,000
 Doesn't really help.

33
00:02:22,000 --> 00:02:27,600
 But it's interesting how he actually talks about when a

34
00:02:27,600 --> 00:02:32,640
 person is inundated by painful feelings, then they'll

35
00:02:34,640 --> 00:02:36,800
 chase after pleasant feelings. Why?

36
00:02:36,800 --> 00:02:40,790
 Because they don't know any better. They can't think of any

37
00:02:40,790 --> 00:02:44,720
 better way to get rid of unpleasant feelings than to

38
00:02:44,720 --> 00:02:50,880
 douse the fire with gasoline really.

39
00:02:50,880 --> 00:02:58,200
 To smother it with sensual desire and then lust arises. I

40
00:02:58,200 --> 00:02:59,280
 mean if you read this it's a very

41
00:02:59,280 --> 00:03:03,840
 clear exposition on the nature of

42
00:03:05,120 --> 00:03:09,540
 attachment and it's a clear exposition of the problem that

43
00:03:09,540 --> 00:03:10,640
 we face.

44
00:03:10,640 --> 00:03:15,760
 The ordinary person knows no other escape from painful

45
00:03:15,760 --> 00:03:17,040
 feelings.

46
00:03:17,040 --> 00:03:24,040
 (Sadhguru

47
00:04:33,780 --> 00:04:40,780
 Sadhguru)

48
00:04:40,780 --> 00:04:42,780
 They don't know any other way.

49
00:04:42,780 --> 00:04:50,780
 They escape from suffering feelings.

50
00:04:50,780 --> 00:04:55,820
 They don't know any other way.

51
00:04:55,820 --> 00:05:02,860
 And when they are

52
00:05:05,820 --> 00:05:11,900
 rejoicing or indulging or engaging

53
00:05:11,900 --> 00:05:19,500
 in rejoicing in central pleasure

54
00:05:30,620 --> 00:05:37,160
 Anusaya is kind of like an obsession. They become obsessed

55
00:05:37,160 --> 00:05:39,180
 with the obsession of raga

56
00:05:39,180 --> 00:05:44,180
 because Anu-seiti. Anu means to lie dormant. Anu-seiti

57
00:05:44,180 --> 00:05:48,780
 means to lie down or to lie with

58
00:05:48,780 --> 00:05:57,960
 to lie inside. It's interesting. Anu-se, how do you Anu-se

59
00:05:57,960 --> 00:05:58,380
iti?

60
00:05:58,380 --> 00:06:06,540
 You know, become obsessed. I mean the idea of Anu-se is

61
00:06:06,540 --> 00:06:07,660
 something deep.

62
00:06:07,660 --> 00:06:11,660
 So obsession is kind of like a deep attachment. Become

63
00:06:11,660 --> 00:06:14,380
 deeply involved in

64
00:06:14,380 --> 00:06:20,380
 raga. Like a lot of raga come up.

65
00:06:20,380 --> 00:06:25,820
 So do we have questions today?

66
00:06:26,780 --> 00:06:28,780
 We do.

67
00:06:28,780 --> 00:06:31,490
 "Bhante, a homeless person asked for money so that he could

68
00:06:31,490 --> 00:06:33,740
 get some food. I gave it to him and then

69
00:06:33,740 --> 00:06:37,280
 drove home. On the drive home I was very mindful the whole

70
00:06:37,280 --> 00:06:40,380
 time. It took much less effort than usual.

71
00:06:40,380 --> 00:06:42,790
 Then I thought that if I constantly gave a portion of the

72
00:06:42,790 --> 00:06:45,180
 money I earned from my job to a charity,

73
00:06:45,180 --> 00:06:47,750
 it might help me be more mindful at work as it's helping

74
00:06:47,750 --> 00:06:51,500
 other people. Will this work? Is this why

75
00:06:51,500 --> 00:06:55,350
 the Buddha recommended householders to donate a portion of

76
00:06:55,350 --> 00:06:57,660
 their money to charity? I believe I

77
00:06:57,660 --> 00:07:00,830
 heard you say this before. Thanks, Bhante. Sorry for the

78
00:07:00,830 --> 00:07:03,580
 unusually large question. Hope you can answer me."

79
00:07:03,580 --> 00:07:12,620
 Yeah, yeah. Giving it in no matter what.

80
00:07:17,660 --> 00:07:22,060
 That's about it. Definitely if you can give.

81
00:07:22,060 --> 00:07:29,020
 It's something that supports your mind. The Buddha said,

82
00:07:29,020 --> 00:07:36,920
 "If a person does good deeds they should do them again and

83
00:07:36,920 --> 00:07:37,420
 again."

84
00:07:43,900 --> 00:07:49,580
 "They should cultivate contentment in regards to those good

85
00:07:49,580 --> 00:07:49,980
 deeds."

86
00:07:49,980 --> 00:07:58,740
 For Suko-Punya Suu Chayo, the cultivation or the collecting

87
00:07:58,740 --> 00:08:00,380
 of goodness is happiness.

88
00:08:00,380 --> 00:08:06,410
 The building up of goodness, the accumulating of goodness

89
00:08:06,410 --> 00:08:08,140
 is happiness.

90
00:08:10,700 --> 00:08:14,850
 It's a mā bhikkawe bhaita punya nanga. Don't be afraid

91
00:08:14,850 --> 00:08:16,700
 because of punya, of goodness.

92
00:08:16,700 --> 00:08:24,860
 Good deeds are another word for happiness.

93
00:08:24,860 --> 00:08:31,580
 For happiness is another word for this. That is goodness.

94
00:08:31,580 --> 00:08:37,260
 No, that is to say goodness is good deeds.

95
00:08:37,260 --> 00:08:45,340
 "Should you be concerned with what people do with the money

96
00:08:45,340 --> 00:08:45,500
?"

97
00:08:45,500 --> 00:08:50,220
 Because sometimes people are asking for change in things

98
00:08:50,220 --> 00:08:51,660
 and you think, "Well,

99
00:08:51,660 --> 00:08:54,490
 if I give them money they may just use it to buy drugs or

100
00:08:54,490 --> 00:08:55,340
 whatever."

101
00:08:55,340 --> 00:09:00,950
 It can be. You have to ask yourself how important it is to

102
00:09:00,950 --> 00:09:01,100
 you.

103
00:09:06,300 --> 00:09:09,580
 If it's something that you, I mean it's not like I would

104
00:09:09,580 --> 00:09:14,140
 rest my whole religious life on or spiritual

105
00:09:14,140 --> 00:09:19,140
 development on charity to the poor or charity to a specific

106
00:09:19,140 --> 00:09:20,220
 individual.

107
00:09:20,220 --> 00:09:29,800
 It's really not all that relevant. It's relevant the sort

108
00:09:29,800 --> 00:09:31,820
 of person that you're giving it to.

109
00:09:32,460 --> 00:09:36,590
 So if there's a homeless person who is really mean and

110
00:09:36,590 --> 00:09:38,540
 nasty or really immoral.

111
00:09:38,540 --> 00:09:45,100
 And again, giving money is, as I said, a fairly low form of

112
00:09:45,100 --> 00:09:50,780
 charity. Giving money is, I mean,

113
00:09:50,780 --> 00:09:53,750
 for that reason as well because they have to do something

114
00:09:53,750 --> 00:09:55,340
 with it. You're not actually doing them

115
00:09:55,340 --> 00:09:58,310
 any favor directly. Well, you're doing them a favor but you

116
00:09:58,310 --> 00:10:00,060
're not actually giving them anything

117
00:10:00,060 --> 00:10:03,790
 useful. You're giving them something that they can use to

118
00:10:03,790 --> 00:10:05,980
 get useful things or to get harmful

119
00:10:05,980 --> 00:10:09,600
 things. So money is kind of, it's power. You're giving them

120
00:10:09,600 --> 00:10:11,180
 power as opposed to something

121
00:10:11,180 --> 00:10:15,300
 that will help them. Power doesn't always help. So money is

122
00:10:15,300 --> 00:10:18,780
 not the best way to give.

123
00:10:18,780 --> 00:10:26,980
 So it could be a concern then if you, you know, like when

124
00:10:26,980 --> 00:10:28,780
 you're in the city and people are asking

125
00:10:28,780 --> 00:10:32,140
 for change in things and if you think they're potentially

126
00:10:32,140 --> 00:10:33,900
 going to use it for drugs or for

127
00:10:33,900 --> 00:10:38,220
 alcohol, would that be the reason? I would say give a

128
00:10:38,220 --> 00:10:41,020
 little, sorry,

129
00:10:41,020 --> 00:10:43,340
 you should let me finish. Go ahead. No, no, that was it.

130
00:10:43,340 --> 00:10:46,060
 But you were going to say, would that be?

131
00:10:46,060 --> 00:10:52,780
 Well, you know, it seems like a conflict, you know, because

132
00:10:52,780 --> 00:10:55,100
 you can tell that that kind of

133
00:10:55,100 --> 00:10:57,890
 behavior would harm people. Of course, you don't know what

134
00:10:57,890 --> 00:11:00,940
 someone's going to do, but that's always

135
00:11:00,940 --> 00:11:04,030
 the concern that, you know, when you give people money that

136
00:11:04,030 --> 00:11:05,900
 they're going to use it to just further

137
00:11:05,900 --> 00:11:11,680
 harm themselves. Yeah. But that's not really your intent

138
00:11:11,680 --> 00:11:14,940
 when you're giving. It's not, I wouldn't

139
00:11:14,940 --> 00:11:17,540
 worry too much about that. But on the other hand, as I said

140
00:11:17,540 --> 00:11:19,980
, I wouldn't really put too much into it.

141
00:11:19,980 --> 00:11:24,380
 You consider the more pure part of the goodness is the

142
00:11:24,380 --> 00:11:27,420
 giving up because they asked you, not because

143
00:11:27,420 --> 00:11:30,140
 you want them to have something, but because they asked you

144
00:11:30,140 --> 00:11:32,620
 if I ask you for something, you giving

145
00:11:32,620 --> 00:11:36,330
 it to me is a relinquishing of the stinginess, right? Of

146
00:11:36,330 --> 00:11:39,020
 saying no to someone. So if someone asks

147
00:11:39,020 --> 00:11:42,770
 you for something, unless it's immoral to give it to them,

148
00:11:42,770 --> 00:11:44,940
 you give it to them, not thinking,

149
00:11:44,940 --> 00:11:49,000
 oh, this will be to their benefit. You say I give as a

150
00:11:49,000 --> 00:11:51,260
 giving up, as a not clinging.

151
00:11:51,260 --> 00:11:56,610
 Like, like, we sent her, we sent her, I gave things away

152
00:11:56,610 --> 00:11:58,780
 that he probably would have,

153
00:11:58,780 --> 00:12:02,660
 well, that's not even fair to say, but we sent her to give

154
00:12:02,660 --> 00:12:05,340
 up everything when people asked him to.

155
00:12:05,340 --> 00:12:08,160
 He was just waiting for people to ask him. People asked him

156
00:12:08,160 --> 00:12:09,420
 for things that they shouldn't have

157
00:12:09,420 --> 00:12:12,440
 asked him for. And he gave because he had made a

158
00:12:12,440 --> 00:12:15,660
 determination that if anyone asks him for something,

159
00:12:15,660 --> 00:12:21,180
 he will give. So it's two aspects of giving. Giving as a

160
00:12:21,180 --> 00:12:25,660
 intentional good deed and giving when

161
00:12:25,660 --> 00:12:30,360
 someone asks you. Two different things. Like, if you go out

162
00:12:30,360 --> 00:12:32,220
 of your way to give money to the

163
00:12:32,220 --> 00:12:37,750
 homeless, then you can be rightly questioned as to, hey, do

164
00:12:37,750 --> 00:12:40,460
 you know where your money is going?

165
00:12:40,460 --> 00:12:43,820
 That's a pretty dubious sort of spiritual practice because

166
00:12:43,820 --> 00:12:45,500
 you don't really know what's going to

167
00:12:45,500 --> 00:12:48,050
 happen to it. But if you're walking down the street and

168
00:12:48,050 --> 00:12:49,740
 someone asks you for change, I don't

169
00:12:49,740 --> 00:12:52,240
 think anyone's going to say, hey, don't give, well, people

170
00:12:52,240 --> 00:12:54,300
 will. I don't think they rightly can say,

171
00:12:54,300 --> 00:12:57,350
 hey, what are you doing giving him money? Say, well, he

172
00:12:57,350 --> 00:13:01,740
 asked me. So like, one of my students

173
00:13:01,740 --> 00:13:08,140
 had this, was being asked for money again and again for

174
00:13:08,140 --> 00:13:13,340
 this and that from a person overseas,

175
00:13:13,340 --> 00:13:15,760
 a person in another country. So they had no way of knowing

176
00:13:15,760 --> 00:13:17,180
 whether what this person was saying

177
00:13:17,180 --> 00:13:21,110
 was true. And it seemed kind of shady. And I'd heard

178
00:13:21,110 --> 00:13:25,500
 similar things of a similar nature. So I

179
00:13:25,500 --> 00:13:28,140
 said to him the same thing. I said, you know, give a little

180
00:13:28,140 --> 00:13:30,860
. And it's a different kind of practice.

181
00:13:30,860 --> 00:13:34,130
 It's about giving up. It's not the kind of thing where you

182
00:13:34,130 --> 00:13:36,060
'd want to give huge sums of money,

183
00:13:36,060 --> 00:13:39,100
 pour your life savings into it or give them everything they

184
00:13:39,100 --> 00:13:41,100
 want. But you give a little

185
00:13:41,100 --> 00:13:43,700
 and that kind of is a way of giving up. Ajahn Tong would do

186
00:13:43,700 --> 00:13:45,500
 this. People would come and ask him for

187
00:13:45,500 --> 00:13:48,820
 money and he would just give them a little. There were his

188
00:13:48,820 --> 00:13:50,700
 relatives. This really funny

189
00:13:50,700 --> 00:13:53,460
 couple, they claimed to be relatives of him. I guess they

190
00:13:53,460 --> 00:13:55,420
 were relatives of him, but they

191
00:13:55,420 --> 00:13:57,790
 milked it for all it was worth. So they would come back

192
00:13:57,790 --> 00:13:59,340
 again and again asking for money.

193
00:14:00,300 --> 00:14:04,180
 And finally, his secretary locked the doors. I was standing

194
00:14:04,180 --> 00:14:05,660
 outside because I was waiting to come in

195
00:14:05,660 --> 00:14:10,850
 for for to listen in for reporting. And they locked the

196
00:14:10,850 --> 00:14:13,100
 doors and we had to sit stand outside

197
00:14:13,100 --> 00:14:17,240
 for like half an hour while he yelled at these two people.

198
00:14:17,240 --> 00:14:19,500
 Secretary is a real bulldog.

199
00:14:27,180 --> 00:14:30,640
 Thank you, Bhante. Are calm feelings considered neutral

200
00:14:30,640 --> 00:14:33,340
 feelings? Yes.

201
00:14:33,340 --> 00:14:46,780
 That's it on questions. Well, good. Yeah.

202
00:14:46,780 --> 00:14:51,020
 Means fewer questions with lots of people, fewer people.

203
00:14:53,340 --> 00:14:59,100
 No, lots of people meditating tonight. Yeah. Fewer people

204
00:14:59,100 --> 00:14:59,660
 logged in.

205
00:14:59,660 --> 00:15:10,780
 That's fine. Are you are you looking on the website? Yeah,

206
00:15:10,780 --> 00:15:13,500
 we got the list of green and orange people.

207
00:15:13,500 --> 00:15:19,450
 Yeah. I'm not sure that that list is always accurate. It

208
00:15:19,450 --> 00:15:20,860
 seems like I don't know.

209
00:15:21,740 --> 00:15:24,260
 Well, it's only if if they're looking at this page, if they

210
00:15:24,260 --> 00:15:25,660
're on YouTube, it won't show. If

211
00:15:25,660 --> 00:15:28,020
 they're somewhere else, it's not even all the people on the

212
00:15:28,020 --> 00:15:29,820
 list. It's just people who are

213
00:15:29,820 --> 00:15:33,780
 actively have meditation that's seriamongo.org open, but it

214
00:15:33,780 --> 00:15:34,540
 should be everyone.

215
00:15:34,540 --> 00:15:40,780
 Maybe it's not accurate. Doesn't have Patrick, does it?

216
00:15:40,780 --> 00:15:46,060
 Where did he go? Yeah, that's what I wonder because

217
00:15:46,060 --> 00:15:48,860
 sometimes it shows more people meditating than

218
00:15:50,300 --> 00:15:53,250
 logged in there. So I don't know. Maybe people close out of

219
00:15:53,250 --> 00:15:54,380
 the page or something.

220
00:15:54,380 --> 00:15:58,540
 Could be maybe it doesn't work as it should.

221
00:15:58,540 --> 00:16:07,360
 I think finally last night's last night's yes, last night's

222
00:16:07,360 --> 00:16:09,820
 Dhammapada was the video was well,

223
00:16:09,820 --> 00:16:15,700
 well made. Finally, I figured out the settings of this darn

224
00:16:15,700 --> 00:16:18,060
 camera, how to get it set up

225
00:16:18,940 --> 00:16:23,450
 to overexpose the background and yet maintain a good

226
00:16:23,450 --> 00:16:24,940
 looking picture.

227
00:16:24,940 --> 00:16:35,160
 If anyone's interested. I think the group here watches your

228
00:16:35,160 --> 00:16:37,260
 Dhammapada videos no matter what,

229
00:16:37,260 --> 00:16:40,850
 you know, good quality, bad quality. I mean, the talk is

230
00:16:40,850 --> 00:16:43,020
 always wonderful, but sometimes the audio

231
00:16:43,020 --> 00:16:45,530
 is off. But I think this group here. Oh yeah, the audio

232
00:16:45,530 --> 00:16:48,140
 last night was off. See, it's not one thing,

233
00:16:48,140 --> 00:16:51,300
 it's another. The audio is off because there's this thing

234
00:16:51,300 --> 00:16:54,380
 called, I think it's called a DC loop,

235
00:16:54,380 --> 00:16:57,480
 which is weird because well, something like some kind of

236
00:16:57,480 --> 00:16:59,420
 loop. If you've got

237
00:16:59,420 --> 00:17:07,360
 somehow two connections, then it builds up a sound. So if

238
00:17:07,360 --> 00:17:09,020
 you listen to last night's Dhammapada,

239
00:17:09,020 --> 00:17:13,230
 you can hear it in the background. I've had that before and

240
00:17:13,230 --> 00:17:15,020
 I had no idea what was going on. And

241
00:17:15,020 --> 00:17:17,410
 that's because I had two things plugged in that were

242
00:17:17,410 --> 00:17:18,620
 connected to the camera.

243
00:17:18,620 --> 00:17:24,780
 This group here, we like them no matter what, but it's

244
00:17:24,780 --> 00:17:26,060
 great that they're coming out

245
00:17:26,060 --> 00:17:33,160
 better for people who are newer to it. Anyway, so good

246
00:17:33,160 --> 00:17:35,980
 night then. Thank you,

247
00:17:35,980 --> 00:17:40,780
 Robin, for your help. Thank you, Monday. Good night.

248
00:17:40,780 --> 00:17:46,540
 Good night.

