1
00:00:00,000 --> 00:00:03,840
 Okay, so welcome back to our study of the Dhammapada.

2
00:00:03,840 --> 00:00:08,140
 Today we continue on with verse number 41, which reads as

3
00:00:08,140 --> 00:00:10,560
 follows.

4
00:00:10,560 --> 00:00:16,400
 "Achirang vata yanga yo bata vin adhise sati"

5
00:00:16,400 --> 00:00:26,400
 "Tur tu ho apeta vinyan o niratang vakalingkarang"

6
00:00:26,400 --> 00:00:33,920
 Which means, "before long indeed, achirang vata"

7
00:00:33,920 --> 00:00:42,320
 "Ayanga yo this body, patuhingatiseti, will lie on the

8
00:00:42,320 --> 00:00:42,400
 earth"

9
00:00:42,400 --> 00:00:52,160
 "Chur do" means discarded, apeta vinyan o without

10
00:00:52,160 --> 00:00:52,880
 consciousness

11
00:00:52,880 --> 00:00:57,480
 or with consciousness having left, consciousness having

12
00:00:57,480 --> 00:00:58,800
 departed from it.

13
00:00:58,800 --> 00:01:03,920
 "Niratang vat kalingkarang"

14
00:01:03,920 --> 00:01:09,870
 As useless as a log, as useless as a dead piece of wood or

15
00:01:09,870 --> 00:01:11,920
 a log.

16
00:01:11,920 --> 00:01:21,040
 So this is a seemingly a little bit depressing verse, no?

17
00:01:21,040 --> 00:01:26,480
 The referring to death, "before long we will be dead"

18
00:01:26,480 --> 00:01:32,640
 This is a reminder of the negative aspects of life.

19
00:01:32,640 --> 00:01:39,760
 So this was told in regards to, it's a fairly famous story

20
00:01:39,760 --> 00:01:47,020
 and it's a fairly famous verse. This verse is used as a

21
00:01:47,020 --> 00:01:48,160
 reminder of the

22
00:01:48,160 --> 00:01:53,200
 inevitability of death and so it's often used at giving

23
00:01:53,200 --> 00:01:53,920
 discourses

24
00:01:53,920 --> 00:01:59,440
 and used in ceremonies in Buddhist, in traditional Buddhism

25
00:01:59,440 --> 00:01:59,440
.

26
00:01:59,440 --> 00:02:07,200
 So the story goes that there was a

27
00:02:07,200 --> 00:02:13,440
 a man in Sawati

28
00:02:14,080 --> 00:02:20,160
 who having listened to the Buddhist teaching became very

29
00:02:20,160 --> 00:02:24,650
 convinced of the Buddhist greatness and the greatness of

30
00:02:24,650 --> 00:02:25,600
 the teaching

31
00:02:25,600 --> 00:02:28,620
 to the point that he decided to go forth and it says "urang

32
00:02:28,620 --> 00:02:30,640
 tatwa bambad tito"

33
00:02:30,640 --> 00:02:34,960
 He gave his heart to where he gave himself up

34
00:02:34,960 --> 00:02:41,280
 to the buddhasasana and went forth.

35
00:02:41,680 --> 00:02:46,800
 Now in a very short time he became quite sick

36
00:02:46,800 --> 00:02:50,240
 for whatever reason he contracted some sort of illness and

37
00:02:50,240 --> 00:02:51,040
 had these

38
00:02:51,040 --> 00:02:55,000
 these boils all over his body kind of like the chicken box

39
00:02:55,000 --> 00:02:56,000
 or something

40
00:02:56,000 --> 00:03:00,400
 and they grew bigger and bigger and I believe it says

41
00:03:00,400 --> 00:03:05,690
 eventually his, they burst and he was covered in this

42
00:03:05,690 --> 00:03:08,080
 filthy fluid

43
00:03:08,080 --> 00:03:13,550
 and even eventually his bones broke you know just some

44
00:03:13,550 --> 00:03:14,400
 horrible sickness

45
00:03:14,400 --> 00:03:18,160
 that totally destroyed his body to the point

46
00:03:18,160 --> 00:03:24,360
 where he was about to die but he became so violent and put

47
00:03:24,360 --> 00:03:24,560
rid

48
00:03:24,560 --> 00:03:27,200
 that the monks didn't want to take care of him

49
00:03:27,200 --> 00:03:32,310
 so they left him alone and he was lying on his bed in his

50
00:03:32,310 --> 00:03:32,400
 own

51
00:03:32,400 --> 00:03:37,040
 urine and feces without anyone to look after him.

52
00:03:37,040 --> 00:03:44,000
 Now the Buddha, the commentary here says that the Buddha

53
00:03:44,000 --> 00:03:49,520
 it's his nature, it's his

54
00:03:49,520 --> 00:03:57,280
 way to twice a day to look out into the the world around

55
00:03:57,280 --> 00:03:57,920
 him to see

56
00:03:57,920 --> 00:04:01,600
 who would most benefit from his teachings

57
00:04:01,600 --> 00:04:07,050
 so sometimes he would look out through the whole universe

58
00:04:07,050 --> 00:04:07,840
 looking around

59
00:04:07,840 --> 00:04:11,680
 through the whole of the earth and in all the heavens and

60
00:04:11,680 --> 00:04:13,120
 so on

61
00:04:13,120 --> 00:04:16,560
 and then all the way back to where he was staying or

62
00:04:16,560 --> 00:04:17,600
 sometimes he would start

63
00:04:17,600 --> 00:04:22,160
 in the monastery and work his way out

64
00:04:22,160 --> 00:04:27,920
 and whoever his mind alighted upon

65
00:04:27,920 --> 00:04:31,200
 as someone who could benefit from his teachings

66
00:04:31,200 --> 00:04:36,810
 he would make his way to them and give them the teaching

67
00:04:36,810 --> 00:04:38,400
 that they needed

68
00:04:38,400 --> 00:04:43,220
 so on this day or on one day when near the point where this

69
00:04:43,220 --> 00:04:43,600
 monk was

70
00:04:43,600 --> 00:04:48,320
 getting closer to death the Buddha saw him

71
00:04:48,320 --> 00:04:52,000
 in his mind's eye and knew that besides him there was

72
00:04:52,000 --> 00:04:55,040
 besides the Buddha himself there was no one else who was

73
00:04:55,040 --> 00:04:55,680
 willing to be his

74
00:04:55,680 --> 00:04:59,280
 refuge or who could be his refuge

75
00:04:59,280 --> 00:05:03,430
 and so he headed out into the monastery this was in the

76
00:05:03,430 --> 00:05:04,160
 same monastery as the

77
00:05:04,160 --> 00:05:07,440
 Buddha was staying and he walked out and he firstly went to

78
00:05:07,440 --> 00:05:09,120
 the place where they

79
00:05:09,120 --> 00:05:14,600
 lit fires especially designated place in the monastery for

80
00:05:14,600 --> 00:05:15,440
 lighting fires and he

81
00:05:15,440 --> 00:05:19,760
 lit a fire and boiled some water and he brought the

82
00:05:19,760 --> 00:05:23,920
 water to this monk's room

83
00:05:23,920 --> 00:05:30,820
 and proceeded to or began to to help the monks who to wash

84
00:05:30,820 --> 00:05:31,200
 himself

85
00:05:31,200 --> 00:05:32,890
 and then when the monks of course when they saw the Buddha

86
00:05:32,890 --> 00:05:33,440
 come in they said

87
00:05:33,440 --> 00:05:36,000
 please oh no let us do that and so they helped

88
00:05:36,000 --> 00:05:39,760
 they helped and with everyone with the monks help they

89
00:05:39,760 --> 00:05:42,600
 were able to take his robes off and wash his robes and wash

90
00:05:42,600 --> 00:05:45,040
 him and wash the bed

91
00:05:45,040 --> 00:05:48,960
 and lie him back down in a clean robe

92
00:05:49,760 --> 00:05:53,580
 and then the Buddha gave him this teaching so this is a

93
00:05:53,580 --> 00:05:54,320
 teaching

94
00:05:54,320 --> 00:06:01,040
 specifically given to a very sick person and

95
00:06:01,040 --> 00:06:05,300
 you might even think that it's a little bit of a specific

96
00:06:05,300 --> 00:06:07,840
 teaching

97
00:06:07,840 --> 00:06:11,120
 dedicated specifically for for someone who is sick or

98
00:06:11,120 --> 00:06:14,320
 someone who is dying

99
00:06:16,560 --> 00:06:19,890
 but i think i'd like to talk a little bit about this verse

100
00:06:19,890 --> 00:06:19,920
 and how

101
00:06:19,920 --> 00:06:28,320
 how as usual how it relates to our practice but

102
00:06:28,320 --> 00:06:32,080
 i think it's an important verse to understand

103
00:06:32,080 --> 00:06:34,790
 it's important to understand that it has a broader reach

104
00:06:34,790 --> 00:06:35,920
 than simply

105
00:06:35,920 --> 00:06:38,960
 dealing with sick people

106
00:06:38,960 --> 00:06:46,190
 this very short and and brief teaching there's a very brief

107
00:06:46,190 --> 00:06:46,400
 teaching

108
00:06:46,400 --> 00:06:50,640
 in the buddhan is

109
00:06:50,640 --> 00:06:57,120
 what happens to be the one of the major reasons why people

110
00:06:57,120 --> 00:06:57,680
 begin to

111
00:06:57,680 --> 00:07:02,640
 practice the buddha's teaching so

112
00:07:02,640 --> 00:07:05,670
 so we ask ourselves why what was the use of this teaching

113
00:07:05,670 --> 00:07:06,640
 at that time

114
00:07:06,640 --> 00:07:10,240
 how is it that this teaching helped that monk it

115
00:07:10,240 --> 00:07:13,120
 because according to the text here he was able to become an

116
00:07:13,120 --> 00:07:13,840
 arahant as a

117
00:07:13,840 --> 00:07:16,520
 result of listening to this teaching in buddha just this

118
00:07:16,520 --> 00:07:18,800
 very simple teaching

119
00:07:18,800 --> 00:07:22,160
 was enough to set him on the path to become a knight and so

120
00:07:22,160 --> 00:07:24,940
 you you would understand that this was the beginning for

121
00:07:24,940 --> 00:07:25,760
 that monk

122
00:07:25,760 --> 00:07:28,800
 and it's the just as it's the beginning for all of us

123
00:07:28,800 --> 00:07:33,760
 on our path to to the realization of the truth

124
00:07:33,760 --> 00:07:37,360
 if we think back to the buddha story himself

125
00:07:37,360 --> 00:07:39,900
 one of the reasons why the buddha went forth is because he

126
00:07:39,900 --> 00:07:40,640
 saw a very sick

127
00:07:40,640 --> 00:07:46,080
 person and he realized that sickness was

128
00:07:46,080 --> 00:07:49,360
 it was something that he himself was subject to as well he

129
00:07:49,360 --> 00:07:50,080
 saw a very old

130
00:07:50,080 --> 00:07:53,520
 person he saw a very sick person he saw a dead person

131
00:07:53,520 --> 00:07:56,570
 and these were three things that that moved him to the

132
00:07:56,570 --> 00:07:58,240
 point of wanting to

133
00:07:58,240 --> 00:08:02,080
 find a way out of suffering to moved him to realize

134
00:08:02,080 --> 00:08:10,240
 that there was no point in pursuing sensual pleasures or

135
00:08:10,240 --> 00:08:13,960
 things outside of the body or even pursuing the the

136
00:08:13,960 --> 00:08:14,800
 wellness of the body

137
00:08:14,800 --> 00:08:18,640
 itself even pursuing life itself there was no

138
00:08:18,640 --> 00:08:22,880
 point in trying to extend life because eventually it would

139
00:08:22,880 --> 00:08:25,600
 be all for naught

140
00:08:25,600 --> 00:08:33,050
 and so on on the first and most the first important way to

141
00:08:33,050 --> 00:08:33,680
 understand this

142
00:08:33,680 --> 00:08:37,440
 verse is as an impetus for us to begin the practice

143
00:08:37,440 --> 00:08:40,320
 this is here we have this monk who was feeling useless

144
00:08:40,320 --> 00:08:41,920
 feeling like

145
00:08:41,920 --> 00:08:46,480
 perhaps that maybe feeling afraid or feeling

146
00:08:46,480 --> 00:08:49,280
 great suffering and depression because of his sickness

147
00:08:49,280 --> 00:08:52,810
 feeling like he was now unable to be unable to practice the

148
00:08:52,810 --> 00:08:53,120
 buddha's

149
00:08:53,120 --> 00:08:58,640
 teaching until the buddha reminded him of this

150
00:08:58,640 --> 00:09:02,330
 of the importance of now of putting this teaching into

151
00:09:02,330 --> 00:09:03,040
 practice

152
00:09:03,040 --> 00:09:07,520
 and this allowed the monk to approach things in a different

153
00:09:07,520 --> 00:09:07,920
 way

154
00:09:07,920 --> 00:09:12,640
 to begin to look at the body and the whole world as being

155
00:09:12,640 --> 00:09:13,600
 not self

156
00:09:13,600 --> 00:09:18,430
 that's not belonging to us and to begin to give up give it

157
00:09:18,430 --> 00:09:18,800
 up

158
00:09:18,800 --> 00:09:22,000
 this is how the buddha began his quest to give up

159
00:09:22,000 --> 00:09:29,120
 attachment and it's a very common reason for people to

160
00:09:29,120 --> 00:09:32,480
 give up the world or to begin meditation practices

161
00:09:32,480 --> 00:09:36,240
 when they get sick or when they realize that sickness old

162
00:09:36,240 --> 00:09:36,240
 age

163
00:09:36,240 --> 00:09:40,400
 old age sickness and death are an inevitable part of life

164
00:09:40,400 --> 00:09:43,500
 for some people this can be a great crisis the realization

165
00:09:43,500 --> 00:09:44,000
 that one day they

166
00:09:44,000 --> 00:09:46,880
 are going to have to die and all the people that they love

167
00:09:46,880 --> 00:09:51,980
 and all the things that they care for are going to leave

168
00:09:51,980 --> 00:09:52,720
 them

169
00:09:56,320 --> 00:09:59,520
 and it also happens to be the beginning of a much longer

170
00:09:59,520 --> 00:10:00,960
 teaching

171
00:10:00,960 --> 00:10:06,000
 which gets to to gets closer and closer and closer to a

172
00:10:06,000 --> 00:10:10,160
 direct realization of impermanent suffering and non-self so

173
00:10:10,160 --> 00:10:10,800
 this is really

174
00:10:10,800 --> 00:10:14,080
 the beginning of the practice or one of the

175
00:10:14,080 --> 00:10:16,940
 important beginnings if people ask why should I practice

176
00:10:16,940 --> 00:10:17,920
 meditation why should

177
00:10:17,920 --> 00:10:21,280
 I torture myself or why should I be concerned

178
00:10:21,280 --> 00:10:24,160
 about the purification of the mind about understanding the

179
00:10:24,160 --> 00:10:24,480
 mind

180
00:10:24,480 --> 00:10:29,040
 why should I be concerned about trying to

181
00:10:29,040 --> 00:10:34,320
 trying to let go of attachment why shouldn't I

182
00:10:34,320 --> 00:10:40,160
 keep clinging to these things or pursuing worldly pleasures

183
00:10:40,160 --> 00:10:42,720
 and so on

184
00:10:42,720 --> 00:10:48,960
 and it's it's this sort of teaching that really answers

185
00:10:48,960 --> 00:10:49,840
 this question

186
00:10:49,840 --> 00:10:54,320
 for us and and makes us ask that makes it turns us away

187
00:10:54,320 --> 00:10:56,160
 from our pursuit of

188
00:10:56,160 --> 00:11:00,560
 central pleasures because you begin to realize that

189
00:11:00,560 --> 00:11:05,440
 let alone those things outside of us that we might pursue

190
00:11:05,440 --> 00:11:09,600
 you know the the things outside of us

191
00:11:09,600 --> 00:11:16,000
 even our own body our own being we can't say that it

192
00:11:16,000 --> 00:11:17,280
 belongs to us

193
00:11:17,280 --> 00:11:21,530
 in the end this is this is the meaning of chudho, apetavin

194
00:11:21,530 --> 00:11:21,680
yan

195
00:11:21,680 --> 00:11:24,960
 the mind will eventually be gone from this body

196
00:11:24,960 --> 00:11:29,120
 this this very body this very being that we hold to be ours

197
00:11:29,120 --> 00:11:32,810
 that we hold to be ourselves this one thing that we cling

198
00:11:32,810 --> 00:11:33,440
 to more than

199
00:11:33,440 --> 00:11:38,000
 anything in the world even this one thing is in no way

200
00:11:38,000 --> 00:11:41,920
 ours is is in no way under our control it's not

201
00:11:41,920 --> 00:11:44,690
 something that we can keep at best you can say we're

202
00:11:44,690 --> 00:11:45,600
 renting it

203
00:11:45,600 --> 00:11:48,720
 at a high price having to feed and to clothe and to

204
00:11:48,720 --> 00:11:52,960
 care for it and having to suffer a lot because of it

205
00:11:52,960 --> 00:11:56,000
 so we're renting it kind of like renting this this thing

206
00:11:56,000 --> 00:11:56,880
 because we think it's

207
00:11:56,880 --> 00:11:59,620
 going to bring us pleasure or we're trying to find pleasure

208
00:11:59,620 --> 00:12:02,000
 based with it

209
00:12:02,000 --> 00:12:05,930
 until we realize we come to realize that actually it's not

210
00:12:05,930 --> 00:12:06,400
 worth

211
00:12:06,400 --> 00:12:10,000
 the cost of it that it's actually not something that

212
00:12:10,000 --> 00:12:14,000
 is going to bring lasting peace and happiness

213
00:12:14,000 --> 00:12:17,440
 it's not something that we can never fix that we can never

214
00:12:17,440 --> 00:12:21,440
 hold on to in fact that the the nature of

215
00:12:21,440 --> 00:12:28,480
 reality is nothing can ever possibly be so there's nothing

216
00:12:28,480 --> 00:12:30,800
 in this universe which

217
00:12:30,800 --> 00:12:35,920
 when clung to will lead to happiness the nature of

218
00:12:35,920 --> 00:12:39,570
 reality of objects of possessions is that they are

219
00:12:39,570 --> 00:12:40,400
 temporary

220
00:12:40,400 --> 00:12:45,320
 that they come and they go that all of the work that we put

221
00:12:45,320 --> 00:12:45,840
 into

222
00:12:45,840 --> 00:12:51,600
 keeping them and maintaining them is in the end

223
00:12:51,600 --> 00:12:55,520
 leads to no lasting and sustainable purpose

224
00:12:55,520 --> 00:13:00,610
 it's due to the change and the inevitability of losing

225
00:13:00,610 --> 00:13:01,600
 everything so

226
00:13:01,600 --> 00:13:04,400
 let alone all of the things in the world this this very

227
00:13:04,400 --> 00:13:05,760
 body that we have

228
00:13:05,760 --> 00:13:11,120
 is not even ourselves the body is really the the the key

229
00:13:11,120 --> 00:13:12,000
 possession

230
00:13:12,000 --> 00:13:15,600
 you know so when when we talk about the body as being

231
00:13:15,600 --> 00:13:19,140
 not tough as the body that's not being belonging to us we

232
00:13:19,140 --> 00:13:20,800
 also

233
00:13:20,800 --> 00:13:23,480
 in extent by extension understand the rest of the universe

234
00:13:23,480 --> 00:13:24,160
 the rest of the

235
00:13:24,160 --> 00:13:28,720
 world to be so when the body is not subject to our control

236
00:13:28,720 --> 00:13:29,200
 how could we

237
00:13:29,200 --> 00:13:35,200
 possibly find control and and and stability

238
00:13:35,200 --> 00:13:40,820
 in the things outside of the body but so starting from here

239
00:13:40,820 --> 00:13:41,360
 this is where we

240
00:13:41,360 --> 00:13:44,720
 begin we begin this movement inwards this is the first

241
00:13:44,720 --> 00:13:45,920
 realization among many

242
00:13:45,920 --> 00:13:50,400
 realizations where we come to realize that let alone

243
00:13:50,400 --> 00:13:55,430
 the this inevitability of death there's actually the inev

244
00:13:55,430 --> 00:13:57,840
itability of change

245
00:13:57,840 --> 00:14:02,720
 from you know on on increasingly

246
00:14:04,240 --> 00:14:08,320
 shorter intervals so the the when i was young i was one

247
00:14:08,320 --> 00:14:11,490
 one way and now i'm older and then i'm getting older when

248
00:14:11,490 --> 00:14:12,800
 our body changes and

249
00:14:12,800 --> 00:14:15,760
 and and we you know when we begin to get sick

250
00:14:15,760 --> 00:14:20,850
 no when we begin to get old when our our our physical

251
00:14:20,850 --> 00:14:22,160
 appearance or

252
00:14:22,160 --> 00:14:26,320
 our physical nature changes when the mind changes

253
00:14:26,320 --> 00:14:30,360
 sometimes we will when we find ourselves becoming addicted

254
00:14:30,360 --> 00:14:31,040
 to something or

255
00:14:31,040 --> 00:14:34,240
 attached to something or we find ourselves the mind

256
00:14:34,240 --> 00:14:34,640
 changing

257
00:14:34,640 --> 00:14:38,400
 maybe forgetting things more and so on the the change that

258
00:14:38,400 --> 00:14:39,520
 comes in the mind

259
00:14:39,520 --> 00:14:41,840
 will will happen in the body and the mind will

260
00:14:41,840 --> 00:14:44,960
 occur many times over the course of our lives

261
00:14:44,960 --> 00:14:48,000
 and and eventually getting shorter and shorter and shorter

262
00:14:48,000 --> 00:14:48,800
 until we realize

263
00:14:48,800 --> 00:14:53,120
 that actually this this idea of death is

264
00:14:53,120 --> 00:14:56,080
 really just the tip of the iceberg and actually death is

265
00:14:56,080 --> 00:14:57,360
 something that occurs

266
00:14:57,360 --> 00:15:02,320
 at every moment the this teaching is something that begins

267
00:15:02,320 --> 00:15:05,680
 us on this course to look inwards until eventually we see

268
00:15:05,680 --> 00:15:10,000
 that every moment we're born and die every moment things

269
00:15:10,000 --> 00:15:11,840
 are changing there is

270
00:15:11,840 --> 00:15:18,560
 no no reality that exists more than one moment

271
00:15:18,560 --> 00:15:24,080
 an ordinary person when they're sitting for all of us when

272
00:15:24,080 --> 00:15:24,720
 when we come to

273
00:15:24,720 --> 00:15:29,040
 practice meditation at first we see the body as being an

274
00:15:29,040 --> 00:15:32,400
 entity we think of it as being a stable

275
00:15:32,400 --> 00:15:37,760
 lasting entity and an ordinary person who who has never

276
00:15:37,760 --> 00:15:38,480
 thought about these

277
00:15:38,480 --> 00:15:42,160
 things will even will generally give rise to

278
00:15:42,160 --> 00:15:44,560
 this idea that this is something that is going to last

279
00:15:44,560 --> 00:15:45,280
 something that i can

280
00:15:45,280 --> 00:15:49,360
 depend upon then they will become negligent in terms

281
00:15:49,360 --> 00:15:54,400
 of clinging to it they will expect it to be

282
00:15:54,400 --> 00:15:56,780
 there the next moment the next day the next month the next

283
00:15:56,780 --> 00:15:57,600
 year and they will

284
00:15:57,600 --> 00:16:00,320
 plan things out so people will have plans for next

285
00:16:00,320 --> 00:16:04,540
 for tomorrow next month next year in many even many years

286
00:16:04,540 --> 00:16:05,040
 off into the

287
00:16:05,040 --> 00:16:08,160
 future they have a retirement plan they have

288
00:16:08,160 --> 00:16:11,120
 very very long you know having grandchildren and so on and

289
00:16:11,120 --> 00:16:11,600
 just just

290
00:16:11,600 --> 00:16:14,320
 plans like this

291
00:16:14,320 --> 00:16:20,400
 and and then this realization comes that

292
00:16:20,400 --> 00:16:23,250
 eventually you're going to die and for some people it comes

293
00:16:23,250 --> 00:16:23,920
 quite quickly

294
00:16:23,920 --> 00:16:25,700
 suddenly they get sick and the doctor says they have a

295
00:16:25,700 --> 00:16:27,040
 month to live

296
00:16:27,040 --> 00:16:30,240
 and this is an incredible shock that that that

297
00:16:30,240 --> 00:16:34,080
 changes their whole outlook on life suddenly from

298
00:16:34,080 --> 00:16:37,840
 from from years months or years it becomes days

299
00:16:37,840 --> 00:16:41,200
 how many what am i going to do tomorrow and and

300
00:16:41,200 --> 00:16:45,680
 realizing that you that you can't cling to things beyond

301
00:16:45,680 --> 00:16:46,720
 maybe

302
00:16:46,720 --> 00:16:50,870
 the next few days or the next the next few weeks or even

303
00:16:50,870 --> 00:16:51,520
 months

304
00:16:52,720 --> 00:16:54,740
 that's how we begin to come and practice meditation with

305
00:16:54,740 --> 00:16:55,360
 these sorts of

306
00:16:55,360 --> 00:16:58,400
 realizations realizing that we can't possibly find

307
00:16:58,400 --> 00:16:59,520
 satisfaction and in the

308
00:16:59,520 --> 00:17:03,500
 end we do have to come to terms with change with the inev

309
00:17:03,500 --> 00:17:04,160
itability of

310
00:17:04,160 --> 00:17:08,410
 losing the things that we cling to once we begin to

311
00:17:08,410 --> 00:17:10,400
 practice meditation

312
00:17:10,400 --> 00:17:15,820
 we we take this the next to to the next step of starting to

313
00:17:15,820 --> 00:17:16,160
 see

314
00:17:16,160 --> 00:17:18,740
 the mind is changing from moment to moment the body is

315
00:17:18,740 --> 00:17:19,680
 changing from moment

316
00:17:19,680 --> 00:17:24,640
 to moment that even sitting here our experience determines

317
00:17:24,640 --> 00:17:28,960
 the the reality so the reality is not this body lasting

318
00:17:28,960 --> 00:17:30,320
 from one moment to

319
00:17:30,320 --> 00:17:34,160
 moment not even from not even from moment to moment let

320
00:17:34,160 --> 00:17:35,120
 alone from days

321
00:17:35,120 --> 00:17:38,070
 day to day or week to week or months a month it's not that

322
00:17:38,070 --> 00:17:39,200
 we're going to die in

323
00:17:39,200 --> 00:17:43,680
 in days or weeks or months or years it's that actually the

324
00:17:43,680 --> 00:17:45,040
 we the the body

325
00:17:45,040 --> 00:17:48,560
 is something that dies is born and dies at every moment

326
00:17:48,560 --> 00:17:51,030
 as we begin to practice meditation you'll begin to see this

327
00:17:51,030 --> 00:17:51,760
 you begin to see

328
00:17:51,760 --> 00:17:55,630
 that actually there is no body there is only the experience

329
00:17:55,630 --> 00:17:57,040
 so when you look at

330
00:17:57,040 --> 00:17:59,460
 look at another person's body look at your body then you

331
00:17:59,460 --> 00:17:59,840
 see

332
00:17:59,840 --> 00:18:03,370
 and at that moment the body arises as a a thought in the

333
00:18:03,370 --> 00:18:03,760
 mind

334
00:18:03,760 --> 00:18:06,770
 based on the image that you've seen when you experience the

335
00:18:06,770 --> 00:18:08,000
 tension in the legs

336
00:18:08,000 --> 00:18:11,270
 the tension in the back of sitting here that's when the

337
00:18:11,270 --> 00:18:12,640
 body arises there's the

338
00:18:12,640 --> 00:18:16,000
 mind and the body in that moment once the mind moves

339
00:18:16,000 --> 00:18:16,800
 somewhere else to

340
00:18:16,800 --> 00:18:21,680
 hearing or seeing or so on that experience disappears and

341
00:18:21,680 --> 00:18:22,000
 therefore

342
00:18:22,000 --> 00:18:25,520
 the body disappears the body is at that moment gone the

343
00:18:25,520 --> 00:18:26,160
 reality of

344
00:18:26,160 --> 00:18:30,560
 the body is has ceased

345
00:18:30,560 --> 00:18:37,600
 and so this sort of this sort of realization starts with

346
00:18:37,600 --> 00:18:39,690
 what is explained in this verse so that's that's really the

347
00:18:39,690 --> 00:18:40,320
 importance of

348
00:18:40,320 --> 00:18:43,640
 this verse we should understand it as the beginnings of bud

349
00:18:43,640 --> 00:18:44,480
dhist practice or

350
00:18:44,480 --> 00:18:47,680
 as an example of of how we begin buddhist practice

351
00:18:47,680 --> 00:18:51,840
 we begin by accepting these very simple truths of life

352
00:18:51,840 --> 00:18:56,480
 that death is inevitable this body anyone who clings to

353
00:18:56,480 --> 00:18:57,360
 this body has to

354
00:18:57,360 --> 00:19:01,200
 face the question of

355
00:19:01,200 --> 00:19:05,080
 what they're going to do when at death or what is the

356
00:19:05,080 --> 00:19:06,000
 purpose of

357
00:19:06,000 --> 00:19:09,440
 clinging when when we're going to lose this body in the end

358
00:19:09,440 --> 00:19:12,640
 why would you worry why are you worrying about this body

359
00:19:12,640 --> 00:19:13,120
 why are you

360
00:19:13,120 --> 00:19:16,960
 concerned with it why are you attached to it when it's not

361
00:19:16,960 --> 00:19:21,200
 obviously going to bring you lasting satisfaction

362
00:19:21,200 --> 00:19:23,440
 in the end you're going to lose in the end this body is

363
00:19:23,440 --> 00:19:24,320
 going to lie like a

364
00:19:24,320 --> 00:19:27,200
 useless log it's a vivid image for people who cling to the

365
00:19:27,200 --> 00:19:29,520
 body because

366
00:19:29,520 --> 00:19:32,880
 we we we tend not to like to think of our

367
00:19:32,880 --> 00:19:35,740
 our body as something that we have to throw away we try our

368
00:19:35,740 --> 00:19:36,640
 best to not have

369
00:19:36,640 --> 00:19:40,320
 to lose the body to take care for it and concern about it

370
00:19:40,320 --> 00:19:43,360
 and we'll be concerned about all the limbs and organs and

371
00:19:43,360 --> 00:19:43,920
 if we lose a

372
00:19:43,920 --> 00:19:47,600
 finger if we lose a hand if we lose a leg you know this

373
00:19:47,600 --> 00:19:48,080
 would be

374
00:19:48,080 --> 00:19:51,400
 travesty if we if we go blind if we go deaf if we lose a

375
00:19:51,400 --> 00:19:52,000
 tooth

376
00:19:52,000 --> 00:19:56,320
 even if we lose a tooth it's it's an incredible

377
00:19:56,320 --> 00:20:00,960
 travesty for us because we

378
00:20:00,960 --> 00:20:03,850
 because we cling to this body as being me as being mine as

379
00:20:03,850 --> 00:20:04,560
 being stable

380
00:20:04,560 --> 00:20:08,320
 as the last verse said it's like a pot and these verses

381
00:20:08,320 --> 00:20:11,360
 all clearly go together they're along the same sort of

382
00:20:11,360 --> 00:20:13,440
 theme

383
00:20:13,440 --> 00:20:16,730
 that the body is fragile and in the end it's going to break

384
00:20:16,730 --> 00:20:16,960
 and

385
00:20:16,960 --> 00:20:21,990
 and become useless like a charred log once we begin on this

386
00:20:21,990 --> 00:20:22,960
 sort of introspection

387
00:20:22,960 --> 00:20:28,600
 and many other introspections the the idea that habits are

388
00:20:28,600 --> 00:20:29,840
 are

389
00:20:29,840 --> 00:20:32,530
 can change the whole of your being for example this

390
00:20:32,530 --> 00:20:33,600
 realization that clinging

391
00:20:33,600 --> 00:20:37,200
 leads to uh craving leads to clinging leads to

392
00:20:37,200 --> 00:20:40,480
 suffering and so on the realization that what the way we're

393
00:20:40,480 --> 00:20:44,590
 carrying out our life is unsustainable but in the end we're

394
00:20:44,590 --> 00:20:46,080
 going to lose

395
00:20:46,080 --> 00:20:49,120
 we're going to lose this due to the inevitability of things

396
00:20:49,120 --> 00:20:50,080
 like death

397
00:20:50,080 --> 00:20:54,320
 and change and this leads us inward it leads us to

398
00:20:54,320 --> 00:20:57,440
 approach life from a different point of view and

399
00:20:57,440 --> 00:21:01,440
 it's the beginning of a longer course of practice to see

400
00:21:01,440 --> 00:21:04,960
 not only that death is inevitable but that death occurs at

401
00:21:04,960 --> 00:21:06,480
 every moment

402
00:21:06,480 --> 00:21:09,920
 and that not only can we not cling to things next year or

403
00:21:09,920 --> 00:21:13,040
 10 years from now we can't even cling to things in the next

404
00:21:13,040 --> 00:21:14,320
 moment

405
00:21:14,320 --> 00:21:16,790
 that they they're going to change and they could they could

406
00:21:16,790 --> 00:21:18,000
 come at any time

407
00:21:18,000 --> 00:21:20,580
 death could come at any moment we don't know what the

408
00:21:20,580 --> 00:21:21,680
 experience is going to be

409
00:21:21,680 --> 00:21:23,680
 in the next moment once we begin to practice

410
00:21:23,680 --> 00:21:27,070
 meditation we will begin to see the mind is changing the

411
00:21:27,070 --> 00:21:28,480
 body is changing and

412
00:21:28,480 --> 00:21:31,590
 this app occurs in the beginning it occurs in terms of day

413
00:21:31,590 --> 00:21:32,400
 by day right so

414
00:21:32,400 --> 00:21:35,670
 you come here to practice meditation and today was a really

415
00:21:35,670 --> 00:21:35,920
 good day

416
00:21:35,920 --> 00:21:37,910
 so you come and tell me about it and then you're very happy

417
00:21:37,910 --> 00:21:38,720
 and then the next day

418
00:21:38,720 --> 00:21:40,560
 you come back and tell me how horrible it was

419
00:21:40,560 --> 00:21:43,810
 how you can understand how all of a sudden today was a very

420
00:21:43,810 --> 00:21:44,560
 very bad day

421
00:21:44,560 --> 00:21:47,150
 or today was a very bad day and you're thinking of quitting

422
00:21:47,150 --> 00:21:47,600
 and then

423
00:21:47,600 --> 00:21:51,230
 tomorrow's a very good day and then this happens again and

424
00:21:51,230 --> 00:21:52,320
 again until finally

425
00:21:52,320 --> 00:21:55,600
 you realize that this is the nature of life that you

426
00:21:55,600 --> 00:21:58,480
 can't depend on it and then you begin to look closer and

427
00:21:58,480 --> 00:22:01,610
 closer until you realize that it's first meditation

428
00:22:01,610 --> 00:22:02,720
 practice to meditation

429
00:22:02,720 --> 00:22:05,760
 practice you can't expect it to be the same

430
00:22:05,760 --> 00:22:09,440
 you can't cling to even a single meditation practice

431
00:22:09,440 --> 00:22:11,980
 as being dependable the next meditation practice might be

432
00:22:11,980 --> 00:22:12,880
 totally different and

433
00:22:12,880 --> 00:22:16,160
 then eventually it gets to the point of realizing you can't

434
00:22:16,160 --> 00:22:16,480
 cling

435
00:22:16,480 --> 00:22:19,270
 from one moment to the next and eventually this is how you

436
00:22:19,270 --> 00:22:19,680
 let go of

437
00:22:19,680 --> 00:22:22,320
 everything in the end you're able to let go of not

438
00:22:22,320 --> 00:22:25,490
 only your body but the mind as well and not be concerned

439
00:22:25,490 --> 00:22:26,960
 about any experience

440
00:22:26,960 --> 00:22:31,040
 realizing that this the true cause of suffering is our

441
00:22:31,040 --> 00:22:35,280
 attachment to things so it's this sort of realization that

442
00:22:35,280 --> 00:22:39,360
 sets us on them this was the benefit of this verse

443
00:22:39,360 --> 00:22:42,640
 especially for this monk who was being sick was to remind

444
00:22:42,640 --> 00:22:42,960
 him

445
00:22:42,960 --> 00:22:47,520
 and to alert him to the fact that this was

446
00:22:47,520 --> 00:22:52,480
 real and was a serious fact of life that he was going to

447
00:22:52,480 --> 00:22:53,680
 have to face now

448
00:22:53,680 --> 00:22:56,500
 and to set him on the right path and help him to come to

449
00:22:56,500 --> 00:22:57,600
 terms with this

450
00:22:57,600 --> 00:23:01,250
 and to not be lax and because when people are sick they'll

451
00:23:01,250 --> 00:23:01,760
 often

452
00:23:01,760 --> 00:23:04,300
 they think that we think the correct response is to get sad

453
00:23:04,300 --> 00:23:05,360
 and upset and cry

454
00:23:05,360 --> 00:23:09,800
 and depressed and angry and and cry and cry out for for

455
00:23:09,800 --> 00:23:11,760
 medicines and

456
00:23:11,760 --> 00:23:15,360
 you know or maybe even cling to cling more to happy things

457
00:23:15,360 --> 00:23:15,840
 and say well i've

458
00:23:15,840 --> 00:23:18,010
 got to get now i've only got so many days to live i got to

459
00:23:18,010 --> 00:23:18,800
 get as much

460
00:23:18,800 --> 00:23:22,050
 happiness in before i die not realizing that this is going

461
00:23:22,050 --> 00:23:22,640
 to make you cling

462
00:23:22,640 --> 00:23:26,640
 more and more and be less and less satisfied

463
00:23:27,760 --> 00:23:30,840
 and so so you know for the most part people get depressed

464
00:23:30,840 --> 00:23:31,200
 and

465
00:23:31,200 --> 00:23:33,810
 and and upset because they're not going to be able to cling

466
00:23:33,810 --> 00:23:34,720
 to the things that

467
00:23:34,720 --> 00:23:38,080
 they're clinging to and so here's the buddha reminding him

468
00:23:38,080 --> 00:23:42,790
 that it's too serious for us to be lax and negligent in

469
00:23:42,790 --> 00:23:44,880
 this way realize that

470
00:23:44,880 --> 00:23:47,660
 in the end the body this body will lie on you it's it's

471
00:23:47,660 --> 00:23:48,560
 something that

472
00:23:48,560 --> 00:23:50,490
 we can always come back and think about it's something

473
00:23:50,490 --> 00:23:51,360
 always useful to come

474
00:23:51,360 --> 00:23:54,640
 back and and and remind yourself that this body

475
00:23:54,640 --> 00:23:57,520
 is like a log eventually it's going to fall

476
00:23:57,520 --> 00:24:02,240
 and lie useless on on the earth just as a log

477
00:24:02,240 --> 00:24:05,600
 that's something that uh this is a reason why this verse

478
00:24:05,600 --> 00:24:06,400
 became

479
00:24:06,400 --> 00:24:09,270
 has become quite famous because it's a it's a good

480
00:24:09,270 --> 00:24:10,720
 meditation to use

481
00:24:10,720 --> 00:24:13,570
 to remind yourself from time to time whenever you're

482
00:24:13,570 --> 00:24:14,720
 clinging to the body

483
00:24:14,720 --> 00:24:17,520
 you're worried about the body or when you find yourself

484
00:24:17,520 --> 00:24:20,620
 or you can use it to test yourself to to point out to

485
00:24:20,620 --> 00:24:21,280
 yourself

486
00:24:21,280 --> 00:24:24,380
 how ridiculous is your love and your attachment to your

487
00:24:24,380 --> 00:24:25,200
 body as being

488
00:24:25,200 --> 00:24:29,600
 beautiful and wonderful and a source of stability and

489
00:24:29,600 --> 00:24:34,240
 happiness and so on so just one more verse we're going

490
00:24:34,240 --> 00:24:38,160
 through these one by one and giving using each of them to

491
00:24:38,160 --> 00:24:38,320
 give

492
00:24:38,320 --> 00:24:40,540
 us a little bit more food for thought and explore the budd

493
00:24:40,540 --> 00:24:42,080
hist teaching from

494
00:24:42,080 --> 00:24:44,990
 one more point of view so thanks for tuning in thanks for

495
00:24:44,990 --> 00:24:46,160
 listening

496
00:24:46,160 --> 00:24:50,880
 and until next time go back to our practice

