1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:28,440
 Hopefully we've got everything working now.

3
00:00:28,440 --> 00:00:37,440
 Welcome back after a few days away.

4
00:00:37,440 --> 00:00:44,440
 Meditators are diligently at work,

5
00:00:44,440 --> 00:00:53,440
 coming to the end of the practice, the big finale.

6
00:00:53,440 --> 00:01:00,130
 It's a time when it's useful to look back on where we've

7
00:01:00,130 --> 00:01:00,440
 come

8
00:01:00,440 --> 00:01:11,440
 and get a sense, therefore, thereby, of where we're going.

9
00:01:11,440 --> 00:01:16,440
 And useful, I think, for everyone else to get a sense of

10
00:01:16,440 --> 00:01:17,440
 what it is we're going through

11
00:01:17,440 --> 00:01:22,440
 when we do the meditation course.

12
00:01:22,440 --> 00:01:26,440
 Get a sense of the path that we're practicing.

13
00:01:26,440 --> 00:01:31,040
 So I thought I'd do that in brief, go through that with you

14
00:01:31,040 --> 00:01:31,440
.

15
00:01:31,440 --> 00:01:36,390
 Also just as encouragement, how far you've come, what great

16
00:01:36,390 --> 00:01:38,440
 things you've done.

17
00:01:38,440 --> 00:01:41,440
 It's my job to give you encouragement.

18
00:01:41,440 --> 00:01:44,320
 So maybe you think, "Well, I'm just saying that," but

19
00:01:44,320 --> 00:01:47,440
 really it's true.

20
00:01:47,440 --> 00:01:53,440
 And the power that you can feel and that you can see,

21
00:01:53,440 --> 00:01:56,250
 that you gain from the meditation practice, that doesn't

22
00:01:56,250 --> 00:01:57,440
 come from nowhere.

23
00:01:57,440 --> 00:02:00,440
 It comes from all the hard work you've done.

24
00:02:00,440 --> 00:02:07,920
 First in morality, the first step on the path is to

25
00:02:07,920 --> 00:02:13,440
 regulate your actions and your speech.

26
00:02:13,440 --> 00:02:18,820
 Just by not engaging in all sorts of distracting and what

27
00:02:18,820 --> 00:02:21,440
 we call unwholesome activities,

28
00:02:21,440 --> 00:02:28,600
 those activities which distract the mind, bring the mind

29
00:02:28,600 --> 00:02:32,440
 out of a state of peace.

30
00:02:32,440 --> 00:02:44,320
 Those activities that involve desire, addiction, aversion,

31
00:02:44,320 --> 00:02:50,440
 conceit, arrogance, views.

32
00:02:50,440 --> 00:02:53,620
 It's quite eye-opening when you go out into the world after

33
00:02:53,620 --> 00:02:54,440
 doing a meditation course

34
00:02:54,440 --> 00:03:02,840
 and you realize just how constantly we're bombarded by

35
00:03:02,840 --> 00:03:05,440
 challenging situations

36
00:03:05,440 --> 00:03:11,440
 and bombarded by other people's defilements and stimuli

37
00:03:11,440 --> 00:03:17,440
 that encourage our own defilements.

38
00:03:17,440 --> 00:03:19,950
 It's great work that you're doing to come here and just to

39
00:03:19,950 --> 00:03:20,440
 stay here,

40
00:03:20,440 --> 00:03:25,790
 to put yourself in a position where you're not engaging in

41
00:03:25,790 --> 00:03:28,440
 all those things.

42
00:03:28,440 --> 00:03:34,950
 And moreover to regulate your activities so that it's as

43
00:03:34,950 --> 00:03:40,440
 clear and as peaceful and as focused as possible.

44
00:03:40,440 --> 00:03:43,440
 This is why we talk about running.

45
00:03:43,440 --> 00:03:47,440
 Better not to go for a run while you're here.

46
00:03:47,440 --> 00:03:53,840
 But so many things, so many distractions we've done away

47
00:03:53,840 --> 00:03:54,440
 with,

48
00:03:54,440 --> 00:04:00,800
 just by maintaining this routine of walking and sitting

49
00:04:00,800 --> 00:04:03,440
 throughout the day.

50
00:04:03,440 --> 00:04:07,440
 So we call morality, this is the Buddhist sense of morality

51
00:04:07,440 --> 00:04:07,440
.

52
00:04:07,440 --> 00:04:12,440
 It's not about killing and stealing, not exactly.

53
00:04:12,440 --> 00:04:17,080
 There are many things we can say are immoral, but morality

54
00:04:17,080 --> 00:04:20,440
 really comes down to the focus of the mind,

55
00:04:20,440 --> 00:04:27,640
 through the focus of the body, by acting and speaking in

56
00:04:27,640 --> 00:04:36,440
 such ways that we remove the opportunity

57
00:04:36,440 --> 00:04:51,440
 or the impulse to do unwholesome actions.

58
00:04:51,440 --> 00:04:54,440
 So that's the first step because it focuses your mind.

59
00:04:54,440 --> 00:04:58,440
 So the second step is this focus that you gain.

60
00:04:58,440 --> 00:05:02,440
 It's what you're doing every moment that your mind falls.

61
00:05:02,440 --> 00:05:06,440
 You're cultivating this focus,

62
00:05:06,440 --> 00:05:12,460
 focusing your attention on that moment, one moment after

63
00:05:12,460 --> 00:05:14,440
 one moment.

64
00:05:14,440 --> 00:05:19,250
 And as it becomes habitual, as you become comfortable with

65
00:05:19,250 --> 00:05:22,440
 it or accustomed to it,

66
00:05:22,440 --> 00:05:33,440
 it gains a power, there's a strength in repetition.

67
00:05:33,440 --> 00:05:37,800
 The more and more often you repeat something, the more it

68
00:05:37,800 --> 00:05:38,440
 becomes a habit,

69
00:05:38,440 --> 00:05:47,080
 the more it becomes a part of you, and the more powerful it

70
00:05:47,080 --> 00:05:48,440
 becomes.

71
00:05:48,440 --> 00:05:52,000
 So you gain this momentum, and this is what we call

72
00:05:52,000 --> 00:05:53,440
 concentration.

73
00:05:53,440 --> 00:05:56,080
 You may not feel concentrated because your mind is still

74
00:05:56,080 --> 00:05:57,440
 trying to play tricks on you

75
00:05:57,440 --> 00:06:02,440
 and there's lots of distractions, but you have a power,

76
00:06:02,440 --> 00:06:06,140
 you have a true concentration in the sense that you're able

77
00:06:06,140 --> 00:06:07,440
 to stay present.

78
00:06:07,440 --> 00:06:15,590
 It's like riding on a bull, this rodeo bull, the challenge

79
00:06:15,590 --> 00:06:17,440
 is to stay on.

80
00:06:17,440 --> 00:06:23,870
 And so it feels like you're flailing wildly, but you're

81
00:06:23,870 --> 00:06:26,440
 staying on the bull.

82
00:06:26,440 --> 00:06:34,260
 And that's reality. Reality is not a peaceful, still forest

83
00:06:34,260 --> 00:06:35,440
 pool.

84
00:06:35,440 --> 00:06:41,700
 Reality is inconstant, unpredictable. It's unsatisfying, it

85
00:06:41,700 --> 00:06:46,440
's stressful, it's uncontrollable.

86
00:06:46,440 --> 00:06:56,040
 And so it's not about what we experience, it's about how we

87
00:06:56,040 --> 00:06:57,440
 react to it.

88
00:06:57,440 --> 00:07:00,440
 Our concentration is about not reacting to our experiences,

89
00:07:00,440 --> 00:07:08,060
 learning how to be focused naturally on whatever we

90
00:07:08,060 --> 00:07:10,440
 experience.

91
00:07:10,440 --> 00:07:13,440
 After focus comes this whole course of wisdom,

92
00:07:13,440 --> 00:07:16,710
 and that's what you've really been getting out of this

93
00:07:16,710 --> 00:07:17,440
 course.

94
00:07:17,440 --> 00:07:20,630
 Once you become focused, you start to see things about

95
00:07:20,630 --> 00:07:21,440
 yourself.

96
00:07:21,440 --> 00:07:24,050
 First you start to see who you are, what does it mean by

97
00:07:24,050 --> 00:07:24,440
 the self,

98
00:07:24,440 --> 00:07:28,440
 what do we mean when we talk about myself?

99
00:07:28,440 --> 00:07:33,450
 You start to see what's really inside, what you're really

100
00:07:33,450 --> 00:07:34,440
 made of.

101
00:07:34,440 --> 00:07:38,440
 You see that there's physical and there's mental.

102
00:07:38,440 --> 00:07:43,440
 And really what we're made up of is experiences.

103
00:07:43,440 --> 00:07:46,740
 We have experiences of seeing and hearing and smelling and

104
00:07:46,740 --> 00:07:48,440
 tasting and feeling and thinking

105
00:07:48,440 --> 00:08:01,000
 and it's physical and mental realities coming together to

106
00:08:01,000 --> 00:08:05,440
 form experience.

107
00:08:05,440 --> 00:08:08,440
 You start to see how the experiences work together,

108
00:08:08,440 --> 00:08:10,440
 how some experiences lead to other experiences.

109
00:08:10,440 --> 00:08:14,440
 An experience of pain leads you to get upset,

110
00:08:14,440 --> 00:08:22,440
 and the experience of upset leads you to suffer.

111
00:08:22,440 --> 00:08:27,440
 You see how mindfulness leads to calm, leads to clarity,

112
00:08:27,440 --> 00:08:30,440
 leads to purity.

113
00:08:30,440 --> 00:08:36,040
 You see how something desirable leads to wanting, which

114
00:08:36,040 --> 00:08:37,440
 leads to clinging,

115
00:08:37,440 --> 00:08:44,440
 which leads to suffering.

116
00:08:44,440 --> 00:08:48,610
 And you start to see really in general that nothing is

117
00:08:48,610 --> 00:08:50,440
 really worth clinging to.

118
00:08:50,440 --> 00:08:55,440
 And this is the slow and steady realization that you come

119
00:08:55,440 --> 00:08:56,440
 to clearer and clearer

120
00:08:56,440 --> 00:09:00,440
 that you've really come to at this point in the course.

121
00:09:00,440 --> 00:09:05,440
 You've seen everything. Your mind has done all its tricks.

122
00:09:05,440 --> 00:09:11,440
 Well, it's done most of its major tricks anyway.

123
00:09:11,440 --> 00:09:15,630
 And you're really starting to see that none of it's worth

124
00:09:15,630 --> 00:09:16,440
 clinging to,

125
00:09:16,440 --> 00:09:24,440
 trying to fix, trying to control, not worth it.

126
00:09:24,440 --> 00:09:27,610
 And so at this point it's really just about refining your

127
00:09:27,610 --> 00:09:28,440
 practice.

128
00:09:28,440 --> 00:09:31,440
 All of these stages of knowledge that you've come through

129
00:09:31,440 --> 00:09:32,440
 means

130
00:09:32,440 --> 00:09:35,400
 you've come to see this, the things you thought were stable

131
00:09:35,400 --> 00:09:37,440
, satisfying, controllable,

132
00:09:37,440 --> 00:09:46,440
 unstable, unpredictable, unsatisfying, uncontrollable.

133
00:09:46,440 --> 00:09:52,480
 It's not worth even trying to control them, trying to own

134
00:09:52,480 --> 00:09:55,440
 them, trying to be them.

135
00:09:55,440 --> 00:10:03,440
 And your mind pulls back and you become equanimous.

136
00:10:03,440 --> 00:10:06,210
 Once you get to this point, well, that's where now you can

137
00:10:06,210 --> 00:10:10,440
 see where we are,

138
00:10:10,440 --> 00:10:17,440
 where we're at, where we're going.

139
00:10:17,440 --> 00:10:23,490
 We talk about samsara and how the cause of suffering is our

140
00:10:23,490 --> 00:10:25,440
 craving.

141
00:10:25,440 --> 00:10:28,950
 Well, the cause of our clinging to samsara, the clinging to

142
00:10:28,950 --> 00:10:31,440
 all of this is craving.

143
00:10:31,440 --> 00:10:35,070
 And once you become equanimous, once you let go, the mind

144
00:10:35,070 --> 00:10:36,440
 really lets go.

145
00:10:36,440 --> 00:10:40,900
 So this experience of nirvana or nirvana is really just

146
00:10:40,900 --> 00:10:43,440
 when the mind is not clinging anymore,

147
00:10:43,440 --> 00:10:49,440
 it's not reaching anymore, it's not seeking anymore.

148
00:10:49,440 --> 00:10:51,780
 At the end of that seeking there's this cessation of

149
00:10:51,780 --> 00:10:52,440
 suffering,

150
00:10:52,440 --> 00:10:57,440
 there's an experience of nirvana.

151
00:10:57,440 --> 00:10:59,440
 That's where we're going.

152
00:10:59,440 --> 00:11:01,820
 Now nirvana is something you can experience just for a

153
00:11:01,820 --> 00:11:02,440
 short time,

154
00:11:02,440 --> 00:11:06,440
 a few moments even, but it really changes your perspective.

155
00:11:06,440 --> 00:11:11,530
 It's not magic, but it is a kind of a magic or it seems

156
00:11:11,530 --> 00:11:12,440
 magical

157
00:11:12,440 --> 00:11:15,440
 because once you see that then you know what true peace is.

158
00:11:15,440 --> 00:11:19,670
 And all this other happiness and pleasure that we seek in

159
00:11:19,670 --> 00:11:20,440
 the world

160
00:11:20,440 --> 00:11:25,440
 just doesn't seem meaningful anymore.

161
00:11:25,440 --> 00:11:29,430
 At this point you realize that you're at a mistake to cling

162
00:11:29,430 --> 00:11:29,440
,

163
00:11:29,440 --> 00:11:35,440
 but once you see nirvana it's not intellectual anymore.

164
00:11:35,440 --> 00:11:40,440
 Your mind is really clear, you know in your heart,

165
00:11:40,440 --> 00:11:46,320
 no doubt in your mind that sabe damana langa bhini way sa

166
00:11:46,320 --> 00:11:46,440
aya,

167
00:11:46,440 --> 00:11:53,440
 no dama indeed is worth clinging to.

168
00:11:53,440 --> 00:11:57,440
 So that's the path in brief.

169
00:11:57,440 --> 00:12:01,440
 A little bit of encouragement.

170
00:12:01,440 --> 00:12:07,550
 I'll try to come back every night and give you a little bit

171
00:12:07,550 --> 00:12:09,440
 of encouragement.

172
00:12:09,440 --> 00:12:13,690
 But thanks for coming out and appreciation for all your

173
00:12:13,690 --> 00:12:14,440
 practice.

174
00:12:14,440 --> 00:12:17,440
 You can go back and meditate.

175
00:12:17,440 --> 00:12:31,440
 [silence]

176
00:12:31,440 --> 00:12:35,440
 We have some questions on the website.

177
00:12:35,440 --> 00:12:41,440
 [silence]

178
00:12:41,440 --> 00:12:46,470
 So I recommended the middle length discourses and the great

179
00:12:46,470 --> 00:12:47,440
 disciples.

180
00:12:47,440 --> 00:12:51,520
 I recommend reading the others, yes, the long discourses,

181
00:12:51,520 --> 00:12:53,440
 numerical discourses, yes.

182
00:12:53,440 --> 00:12:57,440
 I recommend all of those.

183
00:12:57,440 --> 00:13:01,440
 But they're a little harder to get into as well,

184
00:13:01,440 --> 00:13:06,440
 so the middle length discourses is easier to get into.

185
00:13:06,440 --> 00:13:09,440
 But you know once you've finished it,

186
00:13:09,440 --> 00:13:13,240
 the long discourses is fairly easy, it's just they're quite

187
00:13:13,240 --> 00:13:14,440
 a bit longer.

188
00:13:14,440 --> 00:13:17,720
 The numerical discourses I don't expect most people will

189
00:13:17,720 --> 00:13:19,440
 read through it entirely.

190
00:13:19,440 --> 00:13:23,370
 There's a lot of repetition and it's a lot of lists and it

191
00:13:23,370 --> 00:13:24,440
's very big.

192
00:13:24,440 --> 00:13:28,110
 The, what you call anthology discourses, I'm not sure what

193
00:13:28,110 --> 00:13:28,440
 that is,

194
00:13:28,440 --> 00:13:31,740
 but it's probably the sanyutani ka, the connected disc

195
00:13:31,740 --> 00:13:32,440
ourses.

196
00:13:32,440 --> 00:13:38,440
 Connected discourses is just huge and a lot of repetition.

197
00:13:38,440 --> 00:13:43,440
 But Bhikkhu Bodhi does a good job of keeping that down.

198
00:13:43,440 --> 00:13:48,800
 And I definitely recommend it all. It just takes time to

199
00:13:48,800 --> 00:13:49,440
 study.

200
00:13:49,440 --> 00:13:54,440
 [silence]

201
00:13:54,440 --> 00:13:59,040
 I think that's really about all I have on the shelf. That's

202
00:13:59,040 --> 00:14:00,440
 interesting.

203
00:14:00,440 --> 00:14:04,440
 [silence]

204
00:14:04,440 --> 00:14:08,440
 Okay, I spoke about teaching as being efficient.

205
00:14:08,440 --> 00:14:11,490
 And the question is how does, there's no wanting other

206
00:14:11,490 --> 00:14:12,440
 people to be happy.

207
00:14:12,440 --> 00:14:16,440
 No, there's no wanting other people to be happy.

208
00:14:16,440 --> 00:14:19,440
 How does compassion relate to this?

209
00:14:19,440 --> 00:14:22,420
 Compassion is a state of mind. When we talk about the

210
00:14:22,420 --> 00:14:23,440
 Buddha's compassion,

211
00:14:23,440 --> 00:14:26,980
 we're talking about the compassion that led him to become a

212
00:14:26,980 --> 00:14:27,440
 Buddha.

213
00:14:27,440 --> 00:14:34,440
 The compassion didn't drive him anymore.

214
00:14:34,440 --> 00:14:42,440
 And actually true compassion doesn't really drive you.

215
00:14:42,440 --> 00:14:47,440
 True compassion is what allows you to help people.

216
00:14:47,440 --> 00:14:51,440
 But it's not that you're compassionate, so you go around

217
00:14:51,440 --> 00:14:52,440
 trying to change everyone,

218
00:14:52,440 --> 00:14:55,440
 trying to teach everyone.

219
00:14:55,440 --> 00:15:00,420
 So when the Buddha was invited to teach, it was compassion

220
00:15:00,420 --> 00:15:03,440
 that made him say,

221
00:15:03,440 --> 00:15:05,440
 "But it's a functional compassion."

222
00:15:05,440 --> 00:15:08,320
 I mean, it's just a part of his enlightenment. It's very

223
00:15:08,320 --> 00:15:09,440
 natural.

224
00:15:09,440 --> 00:15:13,440
 Compassion is maybe just a word that describes that aspect

225
00:15:13,440 --> 00:15:13,440
 of being enlightened

226
00:15:13,440 --> 00:15:17,440
 that leads one to not be cruel.

227
00:15:17,440 --> 00:15:19,540
 Because that's what compassion is. It's really just not

228
00:15:19,540 --> 00:15:20,440
 being cruel.

229
00:15:20,440 --> 00:15:28,970
 When someone is suffering, you act in such a way as to free

230
00:15:28,970 --> 00:15:29,440
 them.

231
00:15:29,440 --> 00:15:33,210
 When they approach you and they want help, you do what you

232
00:15:33,210 --> 00:15:37,440
 can to help them based on their wish.

233
00:15:37,440 --> 00:15:39,440
 Now the question you have is, "Well, what about the Buddha

234
00:15:39,440 --> 00:15:42,440
 telling all these arahants to go forth and teach?"

235
00:15:42,440 --> 00:15:45,430
 I mean, the only reason he did that is because he made a

236
00:15:45,430 --> 00:15:47,440
 promise to spread the teaching.

237
00:15:47,440 --> 00:15:51,480
 So it's still based on this idea that people are asking,

238
00:15:51,480 --> 00:15:53,440
 people are looking for it.

239
00:15:53,440 --> 00:15:56,440
 It has to be that way, I mean, for two reasons.

240
00:15:56,440 --> 00:15:59,390
 First of all, because it's the nature of an enlightened

241
00:15:59,390 --> 00:16:00,440
 being not to cling.

242
00:16:00,440 --> 00:16:03,440
 So you have no desire for anything.

243
00:16:03,440 --> 00:16:07,030
 But also because if you desire to teach, it doesn't really

244
00:16:07,030 --> 00:16:09,440
 desire to spread the dumb.

245
00:16:09,440 --> 00:16:20,820
 It's problematic. There's not a sincerity of reception, you

246
00:16:20,820 --> 00:16:23,440
 know, as there is.

247
00:16:23,440 --> 00:16:27,510
 Yesterday I was teaching, and I was actually quite good at

248
00:16:27,510 --> 00:16:29,440
 the waste act celebration.

249
00:16:29,440 --> 00:16:33,440
 People were quite appreciative.

250
00:16:33,440 --> 00:16:36,840
 But there's always people who seem to be taking it for

251
00:16:36,840 --> 00:16:38,440
 granted, like as though,

252
00:16:38,440 --> 00:16:41,440
 you know, I want to be there and they're like, "Okay, so

253
00:16:41,440 --> 00:16:42,440
 you want to teach me how to meditate?"

254
00:16:42,440 --> 00:16:45,710
 Or something like, "Are you going to teach me how to med

255
00:16:45,710 --> 00:16:46,440
itate?"

256
00:16:46,440 --> 00:16:54,610
 Well, if you want me to. I mean, I guess I put myself out

257
00:16:54,610 --> 00:16:57,440
 there yesterday.

258
00:16:57,440 --> 00:17:00,010
 But people seem to think that it's somehow something I want

259
00:17:00,010 --> 00:17:00,440
 to do.

260
00:17:00,440 --> 00:17:03,440
 It's not really something I want. I mean, I'm not talking,

261
00:17:03,440 --> 00:17:05,440
 I shouldn't talk too much about myself.

262
00:17:05,440 --> 00:17:12,060
 But the one thing isn't a thing. It's that people are

263
00:17:12,060 --> 00:17:14,440
 looking for it.

264
00:17:14,440 --> 00:17:17,440
 And as I said, that it's efficient. It's just a great way

265
00:17:17,440 --> 00:17:24,440
 to live your life and very helpful for your own practice.

266
00:17:24,440 --> 00:17:28,850
 Explain that bad habits are dangerous because they increase

267
00:17:28,850 --> 00:17:31,440
 the tendency to like or dislike.

268
00:17:31,440 --> 00:17:34,820
 More functional habits, cleaning the counter when it's

269
00:17:34,820 --> 00:17:37,440
 dirty. Is there any danger connected to those habits?

270
00:17:37,440 --> 00:17:43,680
 No. I mean, not theoretically, but practically most of our

271
00:17:43,680 --> 00:17:47,440
 functional habits are associated with likes and dislikes.

272
00:17:47,440 --> 00:17:50,360
 And those are the habits. Those are the aspects of the

273
00:17:50,360 --> 00:17:51,440
 habit that we want to change.

274
00:17:51,440 --> 00:17:55,050
 So a habit, as you're thinking of it, is many, many

275
00:17:55,050 --> 00:17:59,440
 different habits, as we would think of them in Buddhism.

276
00:17:59,440 --> 00:18:03,610
 So the habit of cleaning may have habitual anger associated

277
00:18:03,610 --> 00:18:04,440
 with it.

278
00:18:04,440 --> 00:18:07,720
 Why do I have to clean or this is smelly or this is dirty

279
00:18:07,720 --> 00:18:08,440
 or so on.

280
00:18:08,440 --> 00:18:13,440
 Boredom maybe. Or maybe there's a liking of it.

281
00:18:13,440 --> 00:18:17,040
 Do you like the cleanliness of the place? You like the idea

282
00:18:17,040 --> 00:18:20,440
 of having a clean house and so on.

283
00:18:20,440 --> 00:18:24,910
 And so those become habitual and they build. Those are the

284
00:18:24,910 --> 00:18:25,440
 bad habits.

285
00:18:25,440 --> 00:18:27,840
 But the habits you're talking about are a little bit more

286
00:18:27,840 --> 00:18:29,440
 complicated, complex habits.

287
00:18:29,440 --> 00:18:33,270
 So they can change. You can purify that so that you still

288
00:18:33,270 --> 00:18:34,440
 clean the counter.

289
00:18:34,440 --> 00:18:40,640
 But you do it free from the habit of disliking or liking or

290
00:18:40,640 --> 00:18:44,440
 so on or attachment of any sort.

291
00:18:44,440 --> 00:18:50,440
 Is it fair to say that Upadana requires diti-vipalasa?

292
00:18:50,440 --> 00:18:52,440
 Oh, I don't know where you're going. Sankadis are not

293
00:18:52,440 --> 00:18:56,440
 practical questions.

294
00:18:56,440 --> 00:19:00,120
 I don't have an answer. I need to not be done with scholars

295
00:19:00,120 --> 00:19:02,440
 to give you the answers to these questions.

296
00:19:02,440 --> 00:19:11,440
 I think.

297
00:19:11,440 --> 00:19:15,440
 Click and close all these.

298
00:19:15,440 --> 00:19:22,440
 I answered questions.

299
00:19:22,440 --> 00:19:26,170
 When searching you can't find any information regarding mud

300
00:19:26,170 --> 00:19:30,440
ras. Well, we don't teach mudras in my tradition.

301
00:19:30,440 --> 00:19:33,510
 Avijapachya sankara means that all kamas are caused by

302
00:19:33,510 --> 00:19:36,440
 ignorance. Yes.

303
00:19:36,440 --> 00:19:38,440
 Including the kuṣṭhala kamas. Yes.

304
00:19:38,440 --> 00:19:41,440
 How do you explain the hate to kakama?

305
00:19:41,440 --> 00:19:43,900
 I don't know. You have to talk to an abhidhamma scholar

306
00:19:43,900 --> 00:19:44,440
 again.

307
00:19:44,440 --> 00:19:53,630
 But in a general practical sense, an arahant has no karmic

308
00:19:53,630 --> 00:19:55,440
 potency.

309
00:19:55,440 --> 00:19:58,960
 The things that they do and they have a special kind of

310
00:19:58,960 --> 00:20:01,440
 mind that is just purely functional.

311
00:20:01,440 --> 00:20:06,870
 I'm doing this because it's really just because I'm doing

312
00:20:06,870 --> 00:20:08,440
 it honestly.

313
00:20:08,440 --> 00:20:13,440
 But the thing is you can't have that mind with evil deeds.

314
00:20:13,440 --> 00:20:18,440
 Because they're too complicated. They're too perverse.

315
00:20:18,440 --> 00:20:24,440
 That's the theory. It's all fairly technical.

316
00:20:24,440 --> 00:20:30,820
 Okay. Well, I answered most of them. Deleted a couple. That

317
00:20:30,820 --> 00:20:33,440
's our questions for tonight.

318
00:20:33,440 --> 00:20:37,580
 So thank you all for coming out. See you all I guess

319
00:20:37,580 --> 00:20:39,770
 tomorrow. I was thinking of maybe cutting down the days

320
00:20:39,770 --> 00:20:40,440
 that I give talks.

321
00:20:40,440 --> 00:20:46,440
 But just give short talks, no? Okay. Have a good night.

