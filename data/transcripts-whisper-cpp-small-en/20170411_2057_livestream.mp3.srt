1
00:00:00,000 --> 00:00:09,640
 Okay, good evening everyone.

2
00:00:09,640 --> 00:00:15,720
 Welcome to our evening Dhamma session.

3
00:00:15,720 --> 00:00:31,600
 Tonight I'd like to talk about the Brahma-vihara.

4
00:00:31,600 --> 00:00:36,880
 Brahma-vihara is translated often as divine abidings.

5
00:00:36,880 --> 00:00:39,040
 I think it goes beyond that.

6
00:00:39,040 --> 00:00:43,120
 I'm reluctant to translate it in that way.

7
00:00:43,120 --> 00:00:47,080
 Because divine is what we use for heaven.

8
00:00:47,080 --> 00:00:51,730
 It doesn't have anything to do with heaven, it has to do

9
00:00:51,730 --> 00:00:53,280
 with Brahma.

10
00:00:53,280 --> 00:00:57,800
 The Buddha used the word Brahma.

11
00:00:57,800 --> 00:01:00,800
 Brahma means the highest.

12
00:01:00,800 --> 00:01:05,880
 Or it means the sacred.

13
00:01:05,880 --> 00:01:12,280
 It means the ultimate in a sense.

14
00:01:12,280 --> 00:01:14,480
 The high, the holy.

15
00:01:14,480 --> 00:01:16,720
 It's often used to mean holiness.

16
00:01:16,720 --> 00:01:21,920
 So I've translated here as houses of the holy.

17
00:01:21,920 --> 00:01:24,440
 Which I thought was a saying but apparently it's just a Led

18
00:01:24,440 --> 00:01:28,200
 Zeppelin album.

19
00:01:28,200 --> 00:01:32,040
 Anyway, Brahma-vihara.

20
00:01:32,040 --> 00:01:46,280
 Brahma-vihara is dwelling or abode. Brahma is holy.

21
00:01:46,280 --> 00:01:50,280
 And these are not directly related with Vipassana.

22
00:01:50,280 --> 00:01:55,110
 But I think it's useful to look at how they relate to our

23
00:01:55,110 --> 00:01:56,760
 practice anyway.

24
00:01:56,760 --> 00:02:04,390
 And you'll see how we can relate them to insight meditation

25
00:02:04,390 --> 00:02:04,560
.

26
00:02:04,560 --> 00:02:07,580
 The first thing to talk about is how these are understood

27
00:02:07,580 --> 00:02:08,600
 in the world.

28
00:02:08,600 --> 00:02:10,600
 Often misunderstood.

29
00:02:10,600 --> 00:02:14,920
 Or misused in the world.

30
00:02:14,920 --> 00:02:21,040
 We often focus too much on the four Brahma-viharas.

31
00:02:21,040 --> 00:02:30,400
 Brahma-viharas are love or friendliness, compassion,

32
00:02:30,400 --> 00:02:44,480
 sympathy or rejoicing at others' fortune and equanimity.

33
00:02:44,480 --> 00:02:49,860
 But the way they're used often in the world is not even in

34
00:02:49,860 --> 00:02:52,800
 a terribly wholesome way.

35
00:02:52,800 --> 00:02:58,090
 Love for example or friendliness we become attached to

36
00:02:58,090 --> 00:02:58,960
 people.

37
00:02:58,960 --> 00:03:02,140
 Thinking that that's love.

38
00:03:02,140 --> 00:03:07,560
 Our friendliness can easily become affection and partiality

39
00:03:07,560 --> 00:03:11,640
 to the point where we are attached

40
00:03:11,640 --> 00:03:15,960
 and dependent on others.

41
00:03:15,960 --> 00:03:21,840
 And then vulnerable to their manipulation and vulnerable to

42
00:03:21,840 --> 00:03:24,600
 loss and vulnerable to change.

43
00:03:24,600 --> 00:03:31,840
 And we suffer as a result.

44
00:03:31,840 --> 00:03:36,370
 Compassion we can get quite upset at the suffering of

45
00:03:36,370 --> 00:03:37,360
 others.

46
00:03:37,360 --> 00:03:40,360
 Angry even.

47
00:03:40,360 --> 00:03:45,560
 We can feel sad.

48
00:03:45,560 --> 00:03:47,720
 We can feel frustrated.

49
00:03:47,720 --> 00:03:50,760
 We can get burnt out.

50
00:03:50,760 --> 00:03:53,740
 Quite disillusioned and depressed when we think about how

51
00:03:53,740 --> 00:03:55,160
 much suffering there is in

52
00:03:55,160 --> 00:03:57,280
 the world.

53
00:03:57,280 --> 00:04:06,760
 Not a very useful or wholesome state of mind.

54
00:04:06,760 --> 00:04:11,360
 And as far as sympathetic joy we can be happy for others

55
00:04:11,360 --> 00:04:14,440
 but we can also become intoxicated

56
00:04:14,440 --> 00:04:17,480
 by that.

57
00:04:17,480 --> 00:04:32,480
 Become obsessed or indulgent in the world.

58
00:04:32,480 --> 00:04:41,480
 Indulgent in thinking about or rejoicing.

59
00:04:41,480 --> 00:04:44,910
 The idea is when something good happens to someone and then

60
00:04:44,910 --> 00:04:46,400
 you go out and party about

61
00:04:46,400 --> 00:04:47,400
 it.

62
00:04:47,400 --> 00:04:51,800
 That's not what is meant by rejoicing.

63
00:04:51,800 --> 00:04:52,800
 Let's go celebrate.

64
00:04:52,800 --> 00:04:56,520
 Celebrating for example.

65
00:04:56,520 --> 00:05:00,480
 Celebrating because then it's no longer appreciation.

66
00:05:00,480 --> 00:05:02,480
 It's about no longer about appreciation.

67
00:05:02,480 --> 00:05:05,080
 It's about festivity.

68
00:05:05,080 --> 00:05:13,400
 So we go beyond what is wholesome.

69
00:05:13,400 --> 00:05:17,650
 And with equanimity of course the most obvious way that

70
00:05:17,650 --> 00:05:20,120
 this goes wrong is by not caring.

71
00:05:20,120 --> 00:05:28,220
 In the sense of not discerning right from wrong, good from

72
00:05:28,220 --> 00:05:29,280
 bad.

73
00:05:29,280 --> 00:05:33,000
 So I'm greedy and so what?

74
00:05:33,000 --> 00:05:35,720
 So I get angry so what?

75
00:05:35,720 --> 00:05:43,460
 The selfish, ignorant, individual.

76
00:05:43,460 --> 00:05:46,240
 This kind of delusion.

77
00:05:46,240 --> 00:05:47,520
 Equanimity based on delusions.

78
00:05:47,520 --> 00:05:52,080
 Quite dangerous because it's not really equanimity at all.

79
00:05:52,080 --> 00:05:53,600
 But it's a sense of equanimity.

80
00:05:53,600 --> 00:05:58,600
 A sense of not caring, lack of interest in your own benefit

81
00:05:58,600 --> 00:06:00,840
 or lack of interest in any

82
00:06:00,840 --> 00:06:05,400
 benefit whatsoever.

83
00:06:05,400 --> 00:06:07,760
 So the first thing to understand is these are not what is

84
00:06:07,760 --> 00:06:08,240
 meant.

85
00:06:08,240 --> 00:06:13,760
 Or understand the ways that these go wrong.

86
00:06:13,760 --> 00:06:18,120
 We talk about how they go right.

87
00:06:18,120 --> 00:06:25,800
 Just as insight meditators, how we put them into practice

88
00:06:25,800 --> 00:06:28,640
 in support of our practice.

89
00:06:28,640 --> 00:06:31,000
 Being friendly to each other.

90
00:06:31,000 --> 00:06:33,120
 Even in a meditation center we have to take care of each

91
00:06:33,120 --> 00:06:33,600
 other.

92
00:06:33,600 --> 00:06:37,000
 We have to take care of the resident.

93
00:06:37,000 --> 00:06:38,000
 We have to clean.

94
00:06:38,000 --> 00:06:46,080
 We have to be considerate of each other.

95
00:06:46,080 --> 00:06:53,760
 Using the toilet paper on the holder.

96
00:06:53,760 --> 00:06:58,080
 Not staying in the shower for a long, long time.

97
00:06:58,080 --> 00:06:59,640
 Cleaning up after ourselves.

98
00:06:59,640 --> 00:07:00,640
 Cleaning up after each other.

99
00:07:00,640 --> 00:07:06,800
 There's a lot of friendliness involved there.

100
00:07:06,800 --> 00:07:09,320
 But certainly living in the world and as a Buddhist this is

101
00:07:09,320 --> 00:07:10,360
 something for us to keep

102
00:07:10,360 --> 00:07:12,040
 in mind.

103
00:07:12,040 --> 00:07:14,940
 Even as a beginner meditator or someone who has just

104
00:07:14,940 --> 00:07:16,960
 started to learn about Buddhism,

105
00:07:16,960 --> 00:07:20,290
 it's an important part because it's something that protects

106
00:07:20,290 --> 00:07:21,720
 you and it provides you with

107
00:07:21,720 --> 00:07:27,160
 a sort of a guiding principle.

108
00:07:27,160 --> 00:07:28,880
 So you know that when you're angry at someone you know that

109
00:07:28,880 --> 00:07:29,880
 there's something wrong with

110
00:07:29,880 --> 00:07:30,880
 that.

111
00:07:30,880 --> 00:07:33,680
 That's not really where we're headed.

112
00:07:33,680 --> 00:07:36,090
 When you're trying to be friendly you can have a sense of

113
00:07:36,090 --> 00:07:37,280
 how friendly am I towards

114
00:07:37,280 --> 00:07:38,280
 others.

115
00:07:38,280 --> 00:07:43,390
 It can be a good way of discerning your commitment and your

116
00:07:43,390 --> 00:07:45,880
 progress and your success on the

117
00:07:45,880 --> 00:07:46,880
 path.

118
00:07:46,880 --> 00:07:52,230
 Also it's a good way to protect you and keep you on the

119
00:07:52,230 --> 00:07:53,200
 path.

120
00:07:53,200 --> 00:07:55,710
 Thinking that's not good I better not say those nasty

121
00:07:55,710 --> 00:07:57,280
 things about others or do those

122
00:07:57,280 --> 00:08:03,600
 nasty things to others.

123
00:08:03,600 --> 00:08:07,420
 Being compassionate, not wishing for others to suffer,

124
00:08:07,420 --> 00:08:09,640
 being joyful, trying your best

125
00:08:09,640 --> 00:08:13,540
 to rejoice and seeing how jealous we are when other people

126
00:08:13,540 --> 00:08:15,560
 get good things and saying to

127
00:08:15,560 --> 00:08:17,360
 yourself, "You know that's not right.

128
00:08:17,360 --> 00:08:19,360
 I should be happy for them."

129
00:08:19,360 --> 00:08:23,580
 A lot of Buddhists do these sorts of things even those that

130
00:08:23,580 --> 00:08:26,280
 don't particularly set themselves

131
00:08:26,280 --> 00:08:32,040
 in the meditation practice and equanimity trying not to be

132
00:08:32,040 --> 00:08:37,080
 upset, trying not to be reactionary

133
00:08:37,080 --> 00:08:41,990
 to other people suffering or to their causing you suffering

134
00:08:41,990 --> 00:08:42,280
.

135
00:08:42,280 --> 00:08:45,680
 Others harm you to not get upset by that.

136
00:08:45,680 --> 00:08:53,730
 Others do bad deeds to not get angry, to not hate them, to

137
00:08:53,730 --> 00:08:55,880
 try and be equanimous.

138
00:08:55,880 --> 00:09:03,180
 Above all equanimous, above all remembering that reality is

139
00:09:03,180 --> 00:09:04,800
 what it is.

140
00:09:04,800 --> 00:09:08,680
 You're getting upset about it or attached.

141
00:09:08,680 --> 00:09:17,600
 Attached to it isn't actually to anyone's benefit.

142
00:09:17,600 --> 00:09:20,500
 So that's how we in a conventional sense how we put them

143
00:09:20,500 --> 00:09:21,520
 into practice.

144
00:09:21,520 --> 00:09:25,310
 Of course then there's the practice sense, the meditative

145
00:09:25,310 --> 00:09:25,960
 sense.

146
00:09:25,960 --> 00:09:28,520
 You can actually meditate on these.

147
00:09:28,520 --> 00:09:32,470
 Tomorrow I'm asked to do a short loving kindness meditation

148
00:09:32,470 --> 00:09:32,760
.

149
00:09:32,760 --> 00:09:35,560
 That's actually why I thought of this because they're

150
00:09:35,560 --> 00:09:37,720
 having this thing called Buddha day,

151
00:09:37,720 --> 00:09:41,040
 which I'm not really sure what it means.

152
00:09:41,040 --> 00:09:45,480
 Once a year they've got this Buddha day thing.

153
00:09:45,480 --> 00:09:50,560
 Tomorrow's the day of the year.

154
00:09:50,560 --> 00:09:58,670
 I was asked to take part and do a short loving kindness

155
00:09:58,670 --> 00:10:01,160
 meditation.

156
00:10:01,160 --> 00:10:04,060
 But you can take this up as a meditation practice and then

157
00:10:04,060 --> 00:10:05,840
 it really does lead to the Brahma

158
00:10:05,840 --> 00:10:09,080
 realms.

159
00:10:09,080 --> 00:10:10,960
 Does lead to the God realms.

160
00:10:10,960 --> 00:10:19,800
 You become godlike or godly or god a god.

161
00:10:19,800 --> 00:10:23,880
 When you engage, when you fix your mind on the quality of

162
00:10:23,880 --> 00:10:25,840
 love, you fix your mind on

163
00:10:25,840 --> 00:10:28,510
 the quality of compassion and there are ways of developing

164
00:10:28,510 --> 00:10:28,960
 this.

165
00:10:28,960 --> 00:10:33,970
 You spend days and weeks and months to the point where it's

166
00:10:33,970 --> 00:10:36,280
 all of you and you have unlimited

167
00:10:36,280 --> 00:10:44,560
 love and compassion and joy and equanimity towards all

168
00:10:44,560 --> 00:10:46,200
 beings.

169
00:10:46,200 --> 00:10:51,470
 But I think the most important way for us to think about

170
00:10:51,470 --> 00:10:54,640
 these four things is not really

171
00:10:54,640 --> 00:10:58,000
 in this sense at all.

172
00:10:58,000 --> 00:11:01,150
 It's more in the sense that our practice of insight

173
00:11:01,150 --> 00:11:03,400
 meditation helps us or should lead

174
00:11:03,400 --> 00:11:08,080
 us to all four of these.

175
00:11:08,080 --> 00:11:14,870
 Meditation for in many ways is about, I mean for many of us

176
00:11:14,870 --> 00:11:17,680
 a big part of it is how it's

177
00:11:17,680 --> 00:11:21,320
 going to affect our lives in a conventional sense.

178
00:11:21,320 --> 00:11:24,030
 Because the existence of beings of course is just a

179
00:11:24,030 --> 00:11:25,920
 convention in our minds but that's

180
00:11:25,920 --> 00:11:26,920
 how we live.

181
00:11:26,920 --> 00:11:31,480
 We go home and we have to deal with people.

182
00:11:31,480 --> 00:11:35,540
 And so one of the great benefits of meditation practice is

183
00:11:35,540 --> 00:11:38,160
 that it ideally helps you cultivate

184
00:11:38,160 --> 00:11:43,600
 these or makes these more naturally a part of who you are.

185
00:11:43,600 --> 00:11:49,850
 Because you free yourself from all the things that cause us

186
00:11:49,850 --> 00:11:53,000
 to be nasty and mean to others

187
00:11:53,000 --> 00:12:02,800
 and jealous and partial and cause us to get upset.

188
00:12:02,800 --> 00:12:06,560
 So our default is to be friendly towards others.

189
00:12:06,560 --> 00:12:14,880
 I mean friendliness is just reacting appropriately to

190
00:12:14,880 --> 00:12:18,360
 someone who wishes something of you.

191
00:12:18,360 --> 00:12:23,250
 Maybe your time, maybe your support, material support,

192
00:12:23,250 --> 00:12:27,880
 maybe psychological, mental support,

193
00:12:27,880 --> 00:12:35,560
 emotional support.

194
00:12:35,560 --> 00:12:41,800
 And compassion when they're suffering.

195
00:12:41,800 --> 00:12:48,240
 When others are suffering, finding ways, it's just natural

196
00:12:48,240 --> 00:12:53,080
 when someone has the need of

197
00:12:53,080 --> 00:12:57,120
 being let out of their suffering.

198
00:12:57,120 --> 00:13:12,530
 Being healed or cured or having their problems fixed and so

199
00:13:12,530 --> 00:13:14,160
 on.

200
00:13:14,160 --> 00:13:16,490
 And by the way it's natural because this is the natural

201
00:13:16,490 --> 00:13:17,920
 inclination of the mind when you

202
00:13:17,920 --> 00:13:25,040
 see things clearly, when you have an objective, a non-re

203
00:13:25,040 --> 00:13:27,880
actionary outlook.

204
00:13:27,880 --> 00:13:32,630
 The obvious response to someone who needs your help wishing

205
00:13:32,630 --> 00:13:34,800
 for something positive or

206
00:13:34,800 --> 00:13:39,030
 wishing for something negative to be removed from their

207
00:13:39,030 --> 00:13:40,880
 lives is to help them.

208
00:13:40,880 --> 00:13:53,840
 To give them what they need and to help them out of a bad

209
00:13:53,840 --> 00:13:57,440
 situation.

210
00:13:57,440 --> 00:14:00,520
 And so rather than really focusing all our time, which a

211
00:14:00,520 --> 00:14:02,240
 lot of Buddhists do I think,

212
00:14:02,240 --> 00:14:09,260
 on developing these, working hard to develop these, we

213
00:14:09,260 --> 00:14:13,680
 should work hard to purify our minds

214
00:14:13,680 --> 00:14:17,600
 of the obstacles to all these things.

215
00:14:17,600 --> 00:14:23,130
 Because when your mind is in a natural state, your default

216
00:14:23,130 --> 00:14:26,760
 is to be kind, to be compassionate,

217
00:14:26,760 --> 00:14:29,800
 to rejoice in others, to appreciate others.

218
00:14:29,800 --> 00:14:32,010
 When someone does something good and they tell you about it

219
00:14:32,010 --> 00:14:33,320
 or they express it to you,

220
00:14:33,320 --> 00:14:37,400
 a natural reaction is to say that's good.

221
00:14:37,400 --> 00:14:42,580
 It's only our sense of low self-worth that makes us jealous

222
00:14:42,580 --> 00:14:45,340
 and makes us upset when others

223
00:14:45,340 --> 00:14:49,960
 get good things.

224
00:14:49,960 --> 00:14:54,880
 And of course the natural outcome is equanimity.

225
00:14:54,880 --> 00:14:58,870
 These are really good indicators of where we're going in a

226
00:14:58,870 --> 00:15:00,640
 conventional sense.

227
00:15:00,640 --> 00:15:04,480
 What kind of a person is a meditator?

228
00:15:04,480 --> 00:15:10,080
 What kind of a person are they?

229
00:15:10,080 --> 00:15:14,210
 A person who has all four of these, these are qualities

230
00:15:14,210 --> 00:15:16,440
 that protect our practice, but

231
00:15:16,440 --> 00:15:24,040
 they're also qualities that come from our practice.

232
00:15:24,040 --> 00:15:30,750
 A person who practices insight meditation, being mindful of

233
00:15:30,750 --> 00:15:33,920
 experience, seeing things

234
00:15:33,920 --> 00:15:42,280
 as they are.

235
00:15:42,280 --> 00:15:55,340
 It's naturally inclined to be a good person, naturally

236
00:15:55,340 --> 00:15:55,400
 inclined to be a good friend.

237
00:15:55,400 --> 00:15:58,490
 It's interesting to watch how many meditators after, or

238
00:15:58,490 --> 00:16:00,560
 hear about how many meditators after

239
00:16:00,560 --> 00:16:05,410
 they do an intensive course and go home, have a real hard

240
00:16:05,410 --> 00:16:07,160
 time fitting in.

241
00:16:07,160 --> 00:16:09,140
 They find themselves struggling and they feel like

242
00:16:09,140 --> 00:16:10,740
 meditation has actually made them a bit

243
00:16:10,740 --> 00:16:16,920
 of a pariah or a social misfit.

244
00:16:16,920 --> 00:16:20,480
 Social misfit is probably the best term.

245
00:16:20,480 --> 00:16:24,550
 And they question this sort of thing and they question the

246
00:16:24,550 --> 00:16:26,900
 benefits of it perhaps, or they

247
00:16:26,900 --> 00:16:31,500
 struggle to see what good it has been to them when in fact

248
00:16:31,500 --> 00:16:33,840
 it seems to have caused a lot

249
00:16:33,840 --> 00:16:34,840
 of stress.

250
00:16:34,840 --> 00:16:38,180
 And if a meditator has really had good results, then this

251
00:16:38,180 --> 00:16:40,080
 sort of doubt is not something that

252
00:16:40,080 --> 00:16:44,300
 cuts deep, but certainly it cuts deep in all those who

253
00:16:44,300 --> 00:16:46,920
 expect them to behave the way they

254
00:16:46,920 --> 00:16:53,140
 used to behave and find them different, find them perhaps

255
00:16:53,140 --> 00:16:59,280
 aloof or unresponsive, unattentive,

256
00:16:59,280 --> 00:17:07,360
 unfriendly even.

257
00:17:07,360 --> 00:17:12,880
 And so I think it's important to look and to understand

258
00:17:12,880 --> 00:17:16,560
 this, in this sense, to be strong

259
00:17:16,560 --> 00:17:21,560
 in the results that you've gained and to stand strong.

260
00:17:21,560 --> 00:17:23,710
 Because in fact what you're getting when you go home is a

261
00:17:23,710 --> 00:17:25,200
 whole bunch of people who don't

262
00:17:25,200 --> 00:17:31,520
 have friendliness, who don't have compassion, who are used

263
00:17:31,520 --> 00:17:34,600
 to clinging and used to crying

264
00:17:34,600 --> 00:17:40,140
 and suffering and getting angry and being jealous and

265
00:17:40,140 --> 00:17:43,040
 manipulating each other.

266
00:17:43,040 --> 00:17:45,600
 And when you don't play along, they all turn on you.

267
00:17:45,600 --> 00:17:48,080
 And it happens.

268
00:17:48,080 --> 00:17:49,920
 People are ordinary people.

269
00:17:49,920 --> 00:17:55,360
 They can't take change and they certainly can't take people

270
00:17:55,360 --> 00:18:01,440
 who are not willing to cling.

271
00:18:01,440 --> 00:18:04,880
 And everyone around you is asking you to get excited.

272
00:18:04,880 --> 00:18:06,720
 "Hey, let's go party.

273
00:18:06,720 --> 00:18:08,120
 I got a promotion.

274
00:18:08,120 --> 00:18:09,120
 Let's go party.

275
00:18:09,120 --> 00:18:11,520
 No, I'd rather just stay home."

276
00:18:11,520 --> 00:18:13,520
 Good for you, though.

277
00:18:13,520 --> 00:18:17,240
 It makes them more very angry, right?

278
00:18:17,240 --> 00:18:19,560
 Because they expect you to be festive.

279
00:18:19,560 --> 00:18:22,440
 They expect that when a good thing comes, it's not enough

280
00:18:22,440 --> 00:18:23,760
 to just appreciate it.

281
00:18:23,760 --> 00:18:31,600
 We should somehow use it as a cause to go and indulge in

282
00:18:31,600 --> 00:18:35,760
 senseless enjoyment of sensual pleasures.

283
00:18:35,760 --> 00:18:49,080
 So I mean it's perhaps part of what we have to understand

284
00:18:49,080 --> 00:18:52,760
 is that these are special states.

285
00:18:53,760 --> 00:18:55,760
 These are not the ordinary states.

286
00:18:55,760 --> 00:19:00,760
 These are not states that allow you to fit in with society.

287
00:19:00,760 --> 00:19:03,760
 Society is not built on these foundations.

288
00:19:03,760 --> 00:19:09,580
 I mean some societies somewhere might theoretically be, but

289
00:19:09,580 --> 00:19:12,760
 it's not something you find as the

290
00:19:12,760 --> 00:19:16,760
 general state of affairs.

291
00:19:16,760 --> 00:19:21,980
 Mostly society is built on competition, built on

292
00:19:21,980 --> 00:19:27,760
 manipulation, built on attachment, clinging.

293
00:19:27,760 --> 00:19:35,950
 So meditation makes you a sort of a special individual,

294
00:19:35,950 --> 00:19:37,760
 much more impervious to the sorts

295
00:19:37,760 --> 00:19:43,920
 of suffering that go on, but much less able to engage in a

296
00:19:43,920 --> 00:19:46,760
 system that's based very much

297
00:19:46,760 --> 00:19:52,160
 on the sorts of things that we're moving away from, built

298
00:19:52,160 --> 00:19:54,760
 on stress and suffering,

299
00:19:54,760 --> 00:19:58,760
 really the causes of stress and suffering.

300
00:20:01,760 --> 00:20:07,760
 And so the brahma-yahras are really a state of greatness.

301
00:20:07,760 --> 00:20:11,880
 They can be cultivated artificially by themselves and

302
00:20:11,880 --> 00:20:12,760
 directly working on them,

303
00:20:12,760 --> 00:20:17,860
 or they can come from, stem from the greatness of

304
00:20:17,860 --> 00:20:19,760
 enlightenment,

305
00:20:19,760 --> 00:20:23,100
 the greatness of the clarity of mind that comes from

306
00:20:23,100 --> 00:20:24,760
 insight meditation.

307
00:20:24,760 --> 00:20:30,210
 So it's useful to think about, I think. That's your dhamma

308
00:20:30,210 --> 00:20:31,760
 for tonight.

309
00:20:31,760 --> 00:20:34,760
 Thank you all for tuning in.

310
00:20:34,760 --> 00:20:41,760
 And I think I'm going to have to put a pause on these.

311
00:20:41,760 --> 00:20:47,760
 I'm going to have to stop broadcasting for a while.

312
00:20:47,760 --> 00:20:53,250
 I've got a period of time coming up where I have other

313
00:20:53,250 --> 00:20:54,760
 commitments,

314
00:20:54,760 --> 00:21:02,990
 so I think next broadcast will be a week today, on April 18

315
00:21:02,990 --> 00:21:03,760
th.

316
00:21:03,760 --> 00:21:09,620
 April 18th, I should be back, and then we'll have to figure

317
00:21:09,620 --> 00:21:13,760
 out what my new schedule is going to be like.

318
00:21:14,760 --> 00:21:18,330
 Meanwhile, if there are any questions tonight, please go

319
00:21:18,330 --> 00:21:20,760
 and post them on our meditation page.

320
00:21:20,760 --> 00:21:29,870
 How does one go about dealing with very talkative people,

321
00:21:29,870 --> 00:21:30,760
 such as their parents,

322
00:21:30,760 --> 00:21:34,030
 that they live with and are no longer interested in the

323
00:21:34,030 --> 00:21:34,760
 subject matter

324
00:21:34,760 --> 00:21:38,940
 of what they are talking about, but feel rude to say they

325
00:21:38,940 --> 00:21:41,760
 are not interested in what they are talking about?

326
00:21:41,760 --> 00:21:49,750
 Well, it's one thing to not be interested. It's another

327
00:21:49,750 --> 00:21:51,760
 thing to be upset about.

328
00:21:51,760 --> 00:22:00,240
 Because, I mean, if you've ever been in the forest and you

329
00:22:00,240 --> 00:22:01,760
 hear the birds talking,

330
00:22:01,760 --> 00:22:05,930
 do you get angry at the birds for what they are talking

331
00:22:05,930 --> 00:22:06,760
 about?

332
00:22:10,760 --> 00:22:14,760
 I remember sitting with Ajahn Tong, my teacher,

333
00:22:14,760 --> 00:22:21,760
 and I'd sit with him for five hours a day.

334
00:22:21,760 --> 00:22:29,750
 Sometimes it was an hour of listening to some old nun,

335
00:22:29,750 --> 00:22:30,760
 usually,

336
00:22:30,760 --> 00:22:36,760
 because the old women, old rich women usually,

337
00:22:36,760 --> 00:22:41,310
 would go to the monastery as a way of retiring, of leaving

338
00:22:41,310 --> 00:22:41,760
 home.

339
00:22:41,760 --> 00:22:46,170
 You know, society in Thailand is a little bit, well, it's

340
00:22:46,170 --> 00:22:49,760
 quite sexist, prejudice towards women,

341
00:22:49,760 --> 00:22:51,760
 and has been traditionally.

342
00:22:51,760 --> 00:22:56,290
 And so sometimes the best place for them to live in their

343
00:22:56,290 --> 00:22:57,760
 old age is in the monastery.

344
00:22:57,760 --> 00:22:59,760
 I mean, there's more to it than that.

345
00:22:59,760 --> 00:23:01,760
 Many of them are very much devoted to the practice,

346
00:23:01,760 --> 00:23:06,560
 but some of them are just, seem to be somewhat devoted to

347
00:23:06,560 --> 00:23:07,760
 chatting.

348
00:23:07,760 --> 00:23:12,950
 And so they'd come and chat with Ajahn for, it seemed like

349
00:23:12,950 --> 00:23:15,760
 an hour, maybe half an hour.

350
00:23:15,760 --> 00:23:17,760
 Probably not an hour.

351
00:23:17,760 --> 00:23:22,150
 But he never got upset, and he never tried to brush them

352
00:23:22,150 --> 00:23:22,760
 off.

353
00:23:24,760 --> 00:23:29,850
 He just sat there and listened and nodded sometimes, and

354
00:23:29,850 --> 00:23:31,760
 smiled sometimes.

355
00:23:31,760 --> 00:23:40,760
 You have to check yourself, and worry about other people.

356
00:23:40,760 --> 00:23:43,530
 Someone else is talking, well, that's the experience you

357
00:23:43,530 --> 00:23:45,760
 have, so them talking.

358
00:23:45,760 --> 00:23:52,090
 If you don't like it, if it upsets you, that's also your

359
00:23:52,090 --> 00:23:53,760
 problem, or that's your problem.

360
00:23:53,760 --> 00:24:06,760
 [silence]

361
00:24:06,760 --> 00:24:10,350
 Because I certainly wasn't as happy about all this long

362
00:24:10,350 --> 00:24:12,760
 chatting going on as my teacher was.

363
00:24:12,760 --> 00:24:16,760
 It was much harder for me to sit there and listen to them,

364
00:24:16,760 --> 00:24:18,760
 jambering on about nothing.

365
00:24:18,760 --> 00:24:20,760
 And that was my problem.

366
00:24:21,760 --> 00:24:23,760
 Quite good practice, in fact.

367
00:24:23,760 --> 00:24:27,760
 What is the Buddhist view on serendipity?

368
00:24:27,760 --> 00:24:34,770
 I don't have really a view, I mean, we can speculate that

369
00:24:34,770 --> 00:24:38,520
 what we call serendipity is often related to karma, or that

370
00:24:38,520 --> 00:24:43,410
 sort of thing, but we don't use such words like serendipity

371
00:24:43,410 --> 00:24:43,760
.

372
00:24:45,760 --> 00:24:53,480
 Serendipity is just noticing a pattern, noticing something

373
00:24:53,480 --> 00:25:03,790
 really, that it's something, some sort of coincidence

374
00:25:03,790 --> 00:25:05,760
 almost.

375
00:25:09,760 --> 00:25:14,750
 What is coincidence? Coincidence is coincident. It happened

376
00:25:14,750 --> 00:25:15,760
 that way.

377
00:25:15,760 --> 00:25:19,230
 And so in many ways we're just conditioned to see those

378
00:25:19,230 --> 00:25:21,760
 sorts of things and be excited by them.

379
00:25:21,760 --> 00:25:26,690
 When all the times there was no coincidence, we didn't

380
00:25:26,690 --> 00:25:30,760
 notice. We didn't say, "Oh, well, that's unserendipitous."

381
00:25:33,760 --> 00:25:36,870
 I mean, it's unserendipitous that you asked that question

382
00:25:36,870 --> 00:25:38,760
 and I wasn't thinking about it.

383
00:25:38,760 --> 00:25:42,130
 Or it's unserendipitous that I arrived at the bus stop and

384
00:25:42,130 --> 00:25:43,760
 there was no bus there.

385
00:25:43,760 --> 00:25:48,560
 But isn't it serendipitous that sometimes I just get to

386
00:25:48,560 --> 00:25:53,420
 their bus stop right as the bus is coming and then I notice

387
00:25:53,420 --> 00:25:53,760
 it?

388
00:25:53,760 --> 00:25:57,300
 Or just thinking about someone and they call, but many

389
00:25:57,300 --> 00:26:00,760
 times you think about someone and they don't call.

390
00:26:02,760 --> 00:26:06,890
 I mean, so there can be relationship. There can be reasons

391
00:26:06,890 --> 00:26:08,760
 that we don't really understand.

392
00:26:08,760 --> 00:26:15,760
 But quite often I think it's just a coincidence.

393
00:26:15,760 --> 00:26:20,760
 At any rate, whether there is a cause or not a cause,

394
00:26:20,760 --> 00:26:27,130
 whether it's just a random coincidence or a caused

395
00:26:27,130 --> 00:26:29,760
 coincidence,

396
00:26:30,760 --> 00:26:33,250
 it's still just a coincidence. They call the things

397
00:26:33,250 --> 00:26:34,760
 coincide.

398
00:26:34,760 --> 00:26:38,650
 And that's how we look at it in Buddhism. We see what's

399
00:26:38,650 --> 00:26:40,760
 happening for what's happening.

400
00:26:40,760 --> 00:26:44,780
 We don't try and judge it or make stories or connections

401
00:26:44,780 --> 00:26:45,760
 about it.

402
00:26:45,760 --> 00:26:54,830
 Vedana is explained as pleasant, unpleasant, and neutral,

403
00:26:54,830 --> 00:26:57,220
 but can a feeling be pleasant or unpleasant without liking

404
00:26:57,220 --> 00:26:57,760
 and disliking?

405
00:26:59,760 --> 00:27:01,960
 Pleasant or unpleasant? Well, there are actually five

406
00:27:01,960 --> 00:27:06,150
 feelings and that's why the Abhidhamma separates it out to

407
00:27:06,150 --> 00:27:06,760
 five,

408
00:27:06,760 --> 00:27:11,010
 because there can be a painful bodily feeling without disl

409
00:27:11,010 --> 00:27:11,760
iking.

410
00:27:11,760 --> 00:27:15,980
 But there cannot be a painful mental feeling without disl

411
00:27:15,980 --> 00:27:16,760
iking.

412
00:27:17,760 --> 00:27:28,760
 On the other hand, there can be a pleasant bodily feeling.

413
00:27:28,760 --> 00:27:29,580
 The bodily feeling itself doesn't arise with liking or disl

414
00:27:29,580 --> 00:27:29,760
iking.

415
00:27:29,760 --> 00:27:35,610
 But a pleasant mental feeling can arise with liking or

416
00:27:35,610 --> 00:27:37,760
 without liking.

417
00:27:37,760 --> 00:27:43,760
 For example, the kusala jitas all can arise with somanasa.

418
00:27:44,760 --> 00:27:49,500
 And there's no liking there. There's just a pleasant mental

419
00:27:49,500 --> 00:27:50,760
 feeling involved.

420
00:27:50,760 --> 00:27:53,760
 It doesn't have to be. It can also be a neutral feeling.

421
00:27:53,760 --> 00:27:59,280
 But there is the potential for a wholesome mind to arise

422
00:27:59,280 --> 00:28:02,760
 with pleasure without any liking of it.

423
00:28:02,760 --> 00:28:09,560
 On the other hand, lobha, greed, the lobha mula jitta, can

424
00:28:09,560 --> 00:28:13,760
 arise with pleasure as well.

425
00:28:13,760 --> 00:28:18,760
 So it's with liking or without liking.

426
00:28:18,760 --> 00:28:26,320
 But a mental unpleasant feeling, mental displeasure, mental

427
00:28:26,320 --> 00:28:32,040
 unpleasant feeling is always with disliking, displeasure,

428
00:28:32,040 --> 00:28:33,760
 with anger, patika.

429
00:28:36,760 --> 00:28:43,760
 But a painful physical feeling can be without disliking.

430
00:28:43,760 --> 00:28:48,410
 Or as I think physical feelings, because of how the mind

431
00:28:48,410 --> 00:28:52,190
 series works, physical feelings are always, I think,

432
00:28:52,190 --> 00:28:53,760
 without, just technically.

433
00:28:53,760 --> 00:28:56,910
 Because you have to experience it first and then react

434
00:28:56,910 --> 00:28:57,760
 mentally.

435
00:28:57,760 --> 00:29:00,940
 But the physical is never liking. There's no room for

436
00:29:00,940 --> 00:29:03,760
 liking or disliking at that point, I think.

437
00:29:04,760 --> 00:29:08,950
 But in the sequence of experience of the physical feeling,

438
00:29:08,950 --> 00:29:12,980
 there will come a moment in the mind of pleasure or

439
00:29:12,980 --> 00:29:17,760
 displeasure with liking or disliking or with wholesomeness.

440
00:29:17,760 --> 00:29:29,190
 Okay, and that's all the questions. So that's all for the

441
00:29:29,190 --> 00:29:31,760
 week. I'll be back again next Tuesday.

442
00:29:32,760 --> 00:29:35,760
 Thank you all for coming out. I wish you all the best.

443
00:29:35,760 --> 00:29:42,760
 [

