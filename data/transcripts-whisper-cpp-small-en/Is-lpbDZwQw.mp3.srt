1
00:00:00,000 --> 00:00:06,240
 Okay, so good evening. On Wednesdays I'm answering

2
00:00:06,240 --> 00:00:15,200
 questions. So, you're welcome to listen.

3
00:00:15,200 --> 00:00:18,810
 These are questions that have been asked through the

4
00:00:18,810 --> 00:00:21,680
 website. So try to keep them about meditation

5
00:00:21,680 --> 00:00:26,170
 practice. And it should be interesting for you here

6
00:00:26,170 --> 00:00:28,680
 practicing in our center.

7
00:00:30,360 --> 00:00:35,990
 Tonight's question is about the role or the place of

8
00:00:35,990 --> 00:00:40,840
 thinking about your practice.

9
00:00:40,840 --> 00:00:44,680
 Reflecting, analyzing,

10
00:00:44,680 --> 00:00:50,600
 thinking about

11
00:00:54,680 --> 00:00:59,080
 whether you're doing your practice right, analyzing your

12
00:00:59,080 --> 00:01:02,680
 practice, that sort of thing.

13
00:01:02,680 --> 00:01:08,990
 And of course there is room for this. This is something

14
00:01:08,990 --> 00:01:11,400
 that you even see in the texts

15
00:01:11,400 --> 00:01:15,650
 where the bodhisattva, when he was trying to become

16
00:01:15,650 --> 00:01:18,200
 enlightened, he had to think to himself,

17
00:01:19,160 --> 00:01:22,880
 "Wait, what if I were to do it this way instead?" To

18
00:01:22,880 --> 00:01:25,640
 correct his practice, he had to think.

19
00:01:25,640 --> 00:01:30,680
 And you see this about the monks like Ananda who had to

20
00:01:30,680 --> 00:01:32,840
 correct his practice.

21
00:01:32,840 --> 00:01:45,240
 There's a general sense that it's a part of our practice.

22
00:01:46,840 --> 00:01:50,470
 Our practice isn't just mindfulness, of course. There are

23
00:01:50,470 --> 00:01:52,440
 many other qualities like

24
00:01:52,440 --> 00:01:58,860
 ethics and just even learning the technique of the practice

25
00:01:58,860 --> 00:02:01,000
 and that sort of thing.

26
00:02:01,000 --> 00:02:06,430
 There are many practicalities like livelihood. How are you

27
00:02:06,430 --> 00:02:07,000
 going to eat?

28
00:02:07,000 --> 00:02:12,280
 Where are you going to sleep? What are you going to wear?

29
00:02:12,280 --> 00:02:17,320
 [silence]

30
00:02:17,320 --> 00:02:21,410
 One of these practicalities is the correcting of one's

31
00:02:21,410 --> 00:02:25,560
 practice, the analysis of one's practice.

32
00:02:25,560 --> 00:02:31,380
 It's called vimansa. Vimansa means wisdom really, but in

33
00:02:31,380 --> 00:02:34,680
 regards to a task that you undertake,

34
00:02:34,680 --> 00:02:40,580
 vimansa means the ability to discern right from wrong and

35
00:02:40,580 --> 00:02:43,240
 proper, improper, effective,

36
00:02:43,240 --> 00:02:47,350
 ineffective, that sort of thing. Being able to correct your

37
00:02:47,350 --> 00:02:49,000
 activities, even whether it's

38
00:02:49,000 --> 00:02:52,840
 meditation or not, you need to be able to analyze and

39
00:02:52,840 --> 00:02:57,320
 refine and correct in order to succeed in

40
00:02:57,320 --> 00:03:01,810
 whatever you do. You can't just push, plow ahead, right?

41
00:03:01,810 --> 00:03:04,600
 Sometimes there's correcting that needs to

42
00:03:04,600 --> 00:03:09,680
 be done. So in regards to thinking about meditation and

43
00:03:09,680 --> 00:03:12,600
 correcting your meditation,

44
00:03:12,600 --> 00:03:20,280
 there are, I would say, three things that are the criteria

45
00:03:20,280 --> 00:03:22,760
 for answering whether you can and you

46
00:03:22,760 --> 00:03:27,760
 should think about your practice. The first one is that

47
00:03:27,760 --> 00:03:29,720
 there is something wrong.

48
00:03:29,720 --> 00:03:37,940
 So what often happens is meditators feel like there's

49
00:03:37,940 --> 00:03:40,760
 something wrong or perceive that there's

50
00:03:40,760 --> 00:03:43,090
 something wrong with their practice when there actually isn

51
00:03:43,090 --> 00:03:46,360
't, or they misperceive that this is

52
00:03:46,360 --> 00:03:48,710
 wrong with their practice, but instead they say that is

53
00:03:48,710 --> 00:03:50,680
 wrong with their practice. What I mean is,

54
00:03:50,680 --> 00:03:53,190
 for example, they feel pain and they think that's something

55
00:03:53,190 --> 00:03:55,800
 wrong, or their mind is chaotic and they

56
00:03:55,800 --> 00:03:58,620
 think that's something wrong, or they can't control their

57
00:03:58,620 --> 00:04:00,280
 breath, their stomach, say,

58
00:04:00,280 --> 00:04:04,070
 and that's something wrong. And those aren't wrong. Those

59
00:04:04,070 --> 00:04:04,920
 are actually

60
00:04:04,920 --> 00:04:12,160
 insights or observations that we want to make about reality

61
00:04:12,160 --> 00:04:13,640
, that it is

62
00:04:16,440 --> 00:04:21,530
 inconstant, impermanent, unpredictable, that it is

63
00:04:21,530 --> 00:04:23,400
 unpleasant at times,

64
00:04:23,400 --> 00:04:29,880
 unsatisfying really all the time, and uncontrollable.

65
00:04:29,880 --> 00:04:39,710
 But there are wrong practice. Wrong practice would be when

66
00:04:39,710 --> 00:04:44,280
 you're not being mindful, when you're

67
00:04:45,000 --> 00:04:49,470
 not being mindful of some things, but not being mindful of

68
00:04:49,470 --> 00:04:50,840
 something else.

69
00:04:50,840 --> 00:04:55,710
 Of course, the problems in our practice are that what's

70
00:04:55,710 --> 00:04:59,560
 wrong is when we're practicing with anger,

71
00:04:59,560 --> 00:05:04,340
 like boredom, for example, when we're practicing with doubt

72
00:05:04,340 --> 00:05:06,760
, confusion, worry, fear,

73
00:05:08,200 --> 00:05:12,020
 craving, when we're practicing out of desire for pleasant

74
00:05:12,020 --> 00:05:14,360
 states or desire for enlightenment,

75
00:05:14,360 --> 00:05:19,220
 focusing on even the goal. If you obsess about

76
00:05:19,220 --> 00:05:23,160
 enlightenment or cling to enlightenment, well,

77
00:05:23,160 --> 00:05:29,400
 even that becomes an object of distraction, diversion, and

78
00:05:31,720 --> 00:05:38,530
 drags you down away from enlightenment. So there has to be

79
00:05:38,530 --> 00:05:40,760
 a problem. It can't be

80
00:05:40,760 --> 00:05:51,240
 that the practice is unpleasant or so on. We have to be

81
00:05:51,240 --> 00:05:54,760
 clear that our experiences are not going to

82
00:05:54,760 --> 00:05:58,080
 be a problem. There's nothing wrong with the practice just

83
00:05:58,080 --> 00:05:59,800
 because of what we experience.

84
00:05:59,800 --> 00:06:02,440
 What's wrong with the practice is how we're relating to it,

85
00:06:02,440 --> 00:06:04,920
 most often not being mindful

86
00:06:04,920 --> 00:06:07,790
 of something. And that leads to the second aspect of what

87
00:06:07,790 --> 00:06:09,720
 you should think about your practice. And

88
00:06:09,720 --> 00:06:13,560
 that is that not only there has to be something wrong, but

89
00:06:13,560 --> 00:06:16,200
 you have to be aware of what's wrong.

90
00:06:16,200 --> 00:06:21,000
 So this is another thing that goes wrong with practice is

91
00:06:21,000 --> 00:06:23,480
 we don't realize. And this is where

92
00:06:24,200 --> 00:06:28,320
 I think thinking about your practice can be quite valuable.

93
00:06:28,320 --> 00:06:30,520
 Because if you're practicing being

94
00:06:30,520 --> 00:06:34,320
 mindful, trying to be mindful of the stomach and the foot

95
00:06:34,320 --> 00:06:38,120
 and feelings and so on, you just plow

96
00:06:38,120 --> 00:06:41,050
 ahead and be mindful of what you know to be mindful of. It

97
00:06:41,050 --> 00:06:42,600
's quite common for you to start

98
00:06:42,600 --> 00:06:45,350
 to miss something. You start to feel bored, for example,

99
00:06:45,350 --> 00:06:46,840
 and you're trying to be mindful,

100
00:06:46,840 --> 00:06:51,400
 but you're so bored that it's hard. Or you have a lot of

101
00:06:51,400 --> 00:06:56,040
 doubt. You're trying to push as much and

102
00:06:56,040 --> 00:06:58,180
 do as much as you can, but you really can't put the effort

103
00:06:58,180 --> 00:07:01,560
 in because you're bored. Sorry,

104
00:07:01,560 --> 00:07:05,070
 because you're doubt. You're not convinced that it's useful

105
00:07:05,070 --> 00:07:07,960
. Or you really want to

106
00:07:08,920 --> 00:07:16,000
 do it and you like to do it, but you're lazy. And so you're

107
00:07:16,000 --> 00:07:18,680
 ignoring some part of the practice.

108
00:07:18,680 --> 00:07:23,030
 It's quite common, ignoring disliking of pain, boredom of

109
00:07:23,030 --> 00:07:26,120
 the practice, craving for something.

110
00:07:26,120 --> 00:07:29,550
 And we're not mindful of those things, maybe even just the

111
00:07:29,550 --> 00:07:32,520
 liking of the practice. If it feels good

112
00:07:32,520 --> 00:07:35,010
 and you like it, but you ignore the liking and you're

113
00:07:35,010 --> 00:07:36,280
 mindful of everything else,

114
00:07:37,240 --> 00:07:40,390
 your mindfulness is reduced and eventually becomes

115
00:07:40,390 --> 00:07:41,800
 ineffective. And you're not really

116
00:07:41,800 --> 00:07:44,310
 being mindful because you're just enjoying, because the

117
00:07:44,310 --> 00:07:45,160
 enjoyment builds.

118
00:07:45,160 --> 00:07:52,440
 And of course, leads to disappointment when eventually the

119
00:07:52,440 --> 00:07:54,840
 objects and the experience change.

120
00:07:54,840 --> 00:08:00,460
 So stepping back and thinking about your practice often

121
00:08:00,460 --> 00:08:03,800
 gives you the perspective to say, "Ah,

122
00:08:03,800 --> 00:08:07,660
 yes, I was meditating, but I was missing the boredom or I

123
00:08:07,660 --> 00:08:09,560
 was missing the doubt or I wasn't

124
00:08:09,560 --> 00:08:14,830
 mindful of usually the five hindrances." Because one or

125
00:08:14,830 --> 00:08:16,280
 another of them becomes

126
00:08:16,280 --> 00:08:21,160
 a real drag on your practice.

127
00:08:26,760 --> 00:08:31,640
 And the third quality is that you have to correct the

128
00:08:31,640 --> 00:08:35,560
 practice. And where this is most important

129
00:08:35,560 --> 00:08:38,020
 is not just you have to do something about it, but you have

130
00:08:38,020 --> 00:08:39,080
 to do the correct thing.

131
00:08:39,080 --> 00:08:42,680
 Because it's quite common as well, once we perceive a

132
00:08:42,680 --> 00:08:45,560
 problem with our practice, something wrong with

133
00:08:45,560 --> 00:08:48,130
 our practice, we correct it in the wrong way. Or we do

134
00:08:48,130 --> 00:08:51,400
 something that's actually not only ineffective,

135
00:08:51,400 --> 00:08:55,020
 but often problematic. Like if you're bored, for example, "

136
00:08:55,020 --> 00:08:58,200
Well, then maybe I'll go and enjoy

137
00:08:58,200 --> 00:09:02,110
 myself or I'll find some way to be more pleasant. I'll go

138
00:09:02,110 --> 00:09:04,600
 and talk to someone I'm bored. So I'll

139
00:09:04,600 --> 00:09:07,960
 take a break and go and avoid the challenge of dealing with

140
00:09:07,960 --> 00:09:09,960
 boredom," which is actually just

141
00:09:09,960 --> 00:09:14,510
 aversion. It's an anger-based mind state. If you have your

142
00:09:14,510 --> 00:09:17,880
 doubt, "Well, I'll go and read something,"

143
00:09:17,880 --> 00:09:20,880
 or that sort of thing, rather than challenging yourself

144
00:09:20,880 --> 00:09:23,080
 facing the doubt and being mindful of it.

145
00:09:23,080 --> 00:09:32,310
 So it is, of course, possible that we just ignore it. We

146
00:09:32,310 --> 00:09:34,760
 have a problem, we ignore it, that's right.

147
00:09:34,760 --> 00:09:40,110
 But it's also possible that we try to fix our practice and

148
00:09:40,110 --> 00:09:41,640
 do everything wrong.

149
00:09:42,920 --> 00:09:46,610
 So I would say there are sort of three ways you can fix

150
00:09:46,610 --> 00:09:48,360
 your practice, and that is

151
00:09:48,360 --> 00:09:54,830
 the ways that are problematic, that actually make it worse,

152
00:09:54,830 --> 00:09:58,920
 ways that are provisionally useful,

153
00:09:58,920 --> 00:10:02,490
 and ways that are all... and how... and the fixing of your

154
00:10:02,490 --> 00:10:04,520
 practice that's ultimately useful.

155
00:10:07,800 --> 00:10:12,410
 So ways that are actually making it worse is where you

156
00:10:12,410 --> 00:10:16,840
 avoid the problem or you attack one

157
00:10:16,840 --> 00:10:19,690
 problem with another problem, like you're really negative

158
00:10:19,690 --> 00:10:21,560
 about the practice, so you try to find

159
00:10:21,560 --> 00:10:24,660
 a way to make it more enjoyable, right? You don't like some

160
00:10:24,660 --> 00:10:26,440
 things when you try to find a way to

161
00:10:26,440 --> 00:10:31,080
 like it. Or you have doubts, so you find ways to build up

162
00:10:31,080 --> 00:10:36,680
 confidence. Now, actually, that isn't so

163
00:10:36,680 --> 00:10:42,950
 bad to talk about. That's sort of a provisional way. But

164
00:10:42,950 --> 00:10:47,640
 you might... you might force yourself

165
00:10:47,640 --> 00:10:54,990
 to believe, for example. We might... very commonly avoiding

166
00:10:54,990 --> 00:10:58,360
 is in this category, so you have

167
00:10:58,360 --> 00:11:01,940
 disliking of pain, so you find ways to not have pain, for

168
00:11:01,940 --> 00:11:02,760
 example.

169
00:11:04,520 --> 00:11:07,280
 You don't like walking meditation, so you just sit all the

170
00:11:07,280 --> 00:11:07,560
 time.

171
00:11:07,560 --> 00:11:13,400
 That sort of thing. But I think what's really interesting

172
00:11:13,400 --> 00:11:15,560
 are the provisionally useful

173
00:11:15,560 --> 00:11:20,400
 means of fixing your practice, and I think that's what a

174
00:11:20,400 --> 00:11:23,080
 lot of people often are curious about,

175
00:11:23,080 --> 00:11:29,160
 because the ultimately... ultimately beneficial way is, of

176
00:11:29,160 --> 00:11:30,360
 course, just to be mindful of what

177
00:11:30,360 --> 00:11:33,860
 you're not being mindful of. When you step back and look

178
00:11:33,860 --> 00:11:36,760
 and you realize, "Ah, well, the problem is

179
00:11:36,760 --> 00:11:41,330
 I'm ignoring this one." So really, the ultimate solution to

180
00:11:41,330 --> 00:11:47,000
 wrong practice, that you should use

181
00:11:47,000 --> 00:11:49,980
 this introspection, sort of stepping back and analyzing

182
00:11:49,980 --> 00:11:53,720
 your practice, to pinpoint, to identify.

183
00:11:54,680 --> 00:11:58,810
 The solution is to be mindful of that. That's it. It's

184
00:11:58,810 --> 00:12:02,120
 quite simple. But there are many, many

185
00:12:02,120 --> 00:12:08,290
 sort of provisionally useful techniques, and they involve

186
00:12:08,290 --> 00:12:11,720
 not trying to fix the problem or avoid

187
00:12:11,720 --> 00:12:16,470
 the problem, but to make the problem more bearable or more

188
00:12:16,470 --> 00:12:18,920
 endurable or less severe.

189
00:12:19,800 --> 00:12:24,410
 So if you're very angry, love can be... sending love and

190
00:12:24,410 --> 00:12:26,680
 compassion can be a very useful sort

191
00:12:26,680 --> 00:12:30,780
 of provisional tool. It doesn't help you learn about anger,

192
00:12:30,780 --> 00:12:32,520
 but it can make the anger more

193
00:12:32,520 --> 00:12:36,380
 manageable. If you have lust or desire, you can look at the

194
00:12:36,380 --> 00:12:38,520
 body and the parts of the body and

195
00:12:38,520 --> 00:12:41,610
 think about them, and that's quite useful. But provision

196
00:12:41,610 --> 00:12:43,560
ally, it doesn't help you learn about

197
00:12:43,560 --> 00:12:46,990
 lust and desire. It doesn't help you understand that

198
00:12:46,990 --> 00:12:50,040
 reality. It isn't the same as, for example,

199
00:12:50,040 --> 00:12:53,520
 saying "liking, liking" or "wanting, wanting" or observing

200
00:12:53,520 --> 00:12:55,640
 the object of your desire, seeing,

201
00:12:55,640 --> 00:13:00,240
 seeing or hearing or thinking. If you have doubt, you can

202
00:13:00,240 --> 00:13:04,120
 think about the Buddha or fear as well.

203
00:13:04,120 --> 00:13:08,290
 People have fear living in the forest, afraid of ghosts or

204
00:13:08,290 --> 00:13:09,720
 monsters or animals.

205
00:13:10,680 --> 00:13:13,570
 You can think about the Buddha and it gives you courage. It

206
00:13:13,570 --> 00:13:16,600
 gives you energy. It gives you faith

207
00:13:16,600 --> 00:13:21,160
 and confidence in yourself. But provisionally, it doesn't

208
00:13:21,160 --> 00:13:23,640
 help you learn about doubt and learn

209
00:13:23,640 --> 00:13:26,800
 about the states that lead to doubt and that sort of thing,

210
00:13:26,800 --> 00:13:28,680
 which the Buddha talked about in the

211
00:13:28,680 --> 00:13:37,370
 Satipatthana Suta. And there are rather provisionally

212
00:13:37,370 --> 00:13:41,880
 useful techniques to deal with things like being

213
00:13:41,880 --> 00:13:45,440
 tired. So if you're tired, you can get up and do walking

214
00:13:45,440 --> 00:13:47,640
 meditation, of course, but you can also

215
00:13:47,640 --> 00:13:52,840
 look at the light or go outside, splash water on your face.

216
00:13:52,840 --> 00:13:57,320
 So there are, I would say, an infinite

217
00:13:58,760 --> 00:14:03,380
 idea, ways you can come up with to support your practice.

218
00:14:03,380 --> 00:14:07,320
 But it comes down to your reasoning for

219
00:14:07,320 --> 00:14:11,560
 doing these things. If your reason is to avoid the problem,

220
00:14:11,560 --> 00:14:13,960
 then you end up creating a habit of

221
00:14:13,960 --> 00:14:18,320
 avoidance. So they should never be... When you step back

222
00:14:18,320 --> 00:14:20,920
 and look at your practice, you should

223
00:14:20,920 --> 00:17:46,190
 never be looking for a way to fix and get away from the

224
00:17:46,190 --> 00:14:26,600
 problem. You should be looking for a way to

225
00:14:26,600 --> 00:14:33,770
 manage it. And like a strategist, like a warlord, a marsh,

226
00:14:33,770 --> 00:14:38,920
 what do you call it, a general in the war,

227
00:14:38,920 --> 00:14:41,960
 you have to look at the battlefield and not just rush in,

228
00:14:41,960 --> 00:14:44,200
 but you have to figure out strategically

229
00:14:44,200 --> 00:14:47,730
 when to retreat and when to move forward and which ways to

230
00:14:47,730 --> 00:14:49,880
 move forward. Because our goal in

231
00:14:49,880 --> 00:14:52,720
 the meditation is ultimately to just be mindful of the

232
00:14:52,720 --> 00:14:55,480
 objects. But there's a lot of pain, for example,

233
00:14:56,920 --> 00:15:00,080
 you don't say, "I'll make it so there's no pain," but you

234
00:15:00,080 --> 00:15:03,720
 say, "I'll retreat and I'll make it so that

235
00:15:03,720 --> 00:15:06,520
 the pain is manageable and I can actually be mindful of it

236
00:15:06,520 --> 00:15:08,040
," with the idea in mind that

237
00:15:08,040 --> 00:15:13,190
 eventually I just sit through the pain, even at full force.

238
00:15:13,190 --> 00:15:16,440
 If you have anger and you say, "Well,

239
00:15:16,440 --> 00:15:19,790
 I can't deal with this anger, it's too much," then you step

240
00:15:19,790 --> 00:15:21,800
 back and you send love for a bit,

241
00:15:22,840 --> 00:15:25,090
 and you find that the anger is reduced and then you can

242
00:15:25,090 --> 00:15:26,520
 deal with it. And eventually,

243
00:15:26,520 --> 00:15:29,590
 your goal, as the general's goal in the war, is eventually

244
00:15:29,590 --> 00:15:32,520
 to defeat the enemy, not to always be

245
00:15:32,520 --> 00:15:36,340
 retreating, but sometimes you have to retreat and come back

246
00:15:36,340 --> 00:15:38,360
 at it from a different angle.

247
00:15:38,360 --> 00:15:45,500
 So absolutely there is room for thought, for thinking about

248
00:15:45,500 --> 00:15:47,240
 your practice,

249
00:15:48,280 --> 00:15:55,570
 but it should not be as a means to fix and to change things

250
00:15:55,570 --> 00:15:56,760
 so that you don't have to deal with

251
00:15:56,760 --> 00:16:01,980
 your problems. And the other thing I guess I would say is

252
00:16:01,980 --> 00:16:05,080
 that if you're going to think about your

253
00:16:05,080 --> 00:16:07,970
 practice, thinking itself can obviously become a problem.

254
00:16:07,970 --> 00:16:10,280
 And I think this is the person who asked

255
00:16:10,280 --> 00:16:13,240
 the question was fairly well aware of this, and this can be

256
00:16:13,240 --> 00:16:16,600
 a problem where you actually substitute

257
00:16:16,600 --> 00:16:19,730
 practice with thinking about the practice, and it becomes a

258
00:16:19,730 --> 00:16:22,520
 habit where you're constantly analyzing

259
00:16:22,520 --> 00:16:26,680
 and as a result, doubting. I would say where it becomes a

260
00:16:26,680 --> 00:16:28,840
 problem, you'll be able to see if

261
00:16:28,840 --> 00:16:33,270
 you're mindful because the problems arise, it arises in the

262
00:16:33,270 --> 00:16:35,400
 same way as it would in the practice.

263
00:16:35,400 --> 00:16:38,350
 You'll start to incorporate doubt. Not only are you

264
00:16:38,350 --> 00:16:40,200
 thinking about your practice, but you're

265
00:16:40,200 --> 00:16:45,030
 doubting or you're confused or you're worried or you're

266
00:16:45,030 --> 00:16:48,440
 wanting, you're desiring. You start to get

267
00:16:48,440 --> 00:16:50,900
 into the habit of thinking about your practice because I'm

268
00:16:50,900 --> 00:16:51,960
 going to find the right way. I'm going

269
00:16:51,960 --> 00:16:54,780
 to find a way to make it better. I'm going to find a way to

270
00:16:54,780 --> 00:16:55,800
 make it perfect.

271
00:16:55,800 --> 00:17:01,440
 So thinking about your practice is useful. It should be

272
00:17:01,440 --> 00:17:02,600
 done within

273
00:17:02,600 --> 00:17:07,200
 fairly strict parameters, understanding that ultimately the

274
00:17:07,200 --> 00:17:09,720
 ultimate solution

275
00:17:09,720 --> 00:17:12,290
 you should come to is be mindful of those things that you

276
00:17:12,290 --> 00:17:14,200
 catch that you weren't being mindful of.

277
00:17:14,200 --> 00:17:18,820
 Your thinking about the practice allows you to pinpoint

278
00:17:18,820 --> 00:17:21,880
 those objects, those aspects of your

279
00:17:21,880 --> 00:17:25,360
 experience that you were ignoring, that you were avoiding.

280
00:17:25,360 --> 00:17:30,520
 So that's an answer to that question,

281
00:17:30,520 --> 00:17:35,540
 the question of whether and where and how, to what extent,

282
00:17:35,540 --> 00:17:36,760
 thinking about your practice

283
00:17:37,960 --> 00:17:46,040
 is useful and beneficial. Thank you.

