1
00:00:00,000 --> 00:00:05,480
 Hello guys, when meditating I sometimes focus on feelings

2
00:00:05,480 --> 00:00:08,640
 of static or blood flow in the

3
00:00:08,640 --> 00:00:12,600
 head, skin and eyes along with breath.

4
00:00:12,600 --> 00:00:16,080
 Is this an accepted practice?

5
00:00:16,080 --> 00:00:29,760
 It's not what you focus on, it's how you focus on it.

6
00:00:29,760 --> 00:00:34,640
 And that much more determines how accepted it is.

7
00:00:34,640 --> 00:00:40,260
 So we only accept objective awareness of objects, so focus

8
00:00:40,260 --> 00:00:42,880
 isn't enough because you can focus

9
00:00:42,880 --> 00:00:47,850
 on something and still be subjective about it, still be

10
00:00:47,850 --> 00:00:49,520
 attached to it.

11
00:00:49,520 --> 00:00:55,440
 You can still conceive in regards to it, proliferate or

12
00:00:55,440 --> 00:01:00,040
 extrapolate upon it, project, give rise

13
00:01:00,040 --> 00:01:05,680
 to projections about it.

14
00:01:05,680 --> 00:01:08,800
 So all of those things are perfectly valid objects of

15
00:01:08,800 --> 00:01:10,840
 meditation, but are you actually

16
00:01:10,840 --> 00:01:15,740
 meditating because simply focusing is not enough.

17
00:01:15,740 --> 00:01:19,520
 The key is what we call sati, so it's how you understand

18
00:01:19,520 --> 00:01:20,800
 the word sati.

19
00:01:20,800 --> 00:01:25,010
 If you want I talked in a few places in a book, I have a

20
00:01:25,010 --> 00:01:27,040
 book now isn't that funny,

21
00:01:27,040 --> 00:01:28,040
 how ridiculous.

22
00:01:28,040 --> 00:01:33,360
 I wrote a book for people helping me write a book,

23
00:01:33,360 --> 00:01:37,800
 practical, what is it called, Lessons

24
00:01:37,800 --> 00:01:39,800
 in Practical Buddhism.

25
00:01:39,800 --> 00:01:43,400
 I have it on my tablet.

26
00:01:43,400 --> 00:01:49,150
 That's crazy, I think I should take it down, it's

27
00:01:49,150 --> 00:01:52,680
 embarrassing to have a book.

28
00:01:52,680 --> 00:02:00,360
 People putting my book on a tablet, that's almost scary.

29
00:02:00,360 --> 00:02:04,370
 I am happy about some of the things that are in there, not

30
00:02:04,370 --> 00:02:06,160
 all of it, but it's going to

31
00:02:06,160 --> 00:02:08,770
 take more work, but some of the things that are in there

32
00:02:08,770 --> 00:02:10,080
 are what I wanted to say.

33
00:02:10,080 --> 00:02:13,200
 So some of the things in there about sati could help you, I

34
00:02:13,200 --> 00:02:15,480
 think could really help people

35
00:02:15,480 --> 00:02:20,830
 understand this from my point of view, from our tradition,

36
00:02:20,830 --> 00:02:23,200
 what we mean and why we mean

37
00:02:23,200 --> 00:02:37,800
 what we mean about the word sati, about mindfulness.

38
00:02:37,800 --> 00:02:41,120
 The breath is a good example, I think the breath you have

39
00:02:41,120 --> 00:02:43,160
 to be careful because, well

40
00:02:43,160 --> 00:02:45,860
 not exactly careful, but you have to distinguish between

41
00:02:45,860 --> 00:02:47,680
 the conceptual breath and the reality

42
00:02:47,680 --> 00:02:51,960
 because the breath doesn't exist, it's not a reality.

43
00:02:51,960 --> 00:02:58,490
 The reality is the experience of heat, cold, pressure, acid

44
00:02:58,490 --> 00:03:01,560
ity in the body, all of these

45
00:03:01,560 --> 00:03:04,730
 experiences arise and cease at a single location, so the

46
00:03:04,730 --> 00:03:06,880
 breath doesn't come into the body,

47
00:03:06,880 --> 00:03:09,080
 that's not ultimate experience.

48
00:03:09,080 --> 00:03:15,860
 Ultimate reality is a momentary experience at some point in

49
00:03:15,860 --> 00:03:17,240
 the body.

50
00:03:17,240 --> 00:03:18,240
 .

