1
00:00:00,000 --> 00:00:02,900
 Hi everyone, I'm Skip, a volunteer for Sir Mengele

2
00:00:02,900 --> 00:00:04,940
 International, the organization that

3
00:00:04,940 --> 00:00:08,140
 supports Bonta Yutadamo in his teaching efforts.

4
00:00:08,140 --> 00:00:10,870
 Bonta Yutadamo will be turning 40 on May 9th, and we would

5
00:00:10,870 --> 00:00:12,340
 like to show our gratitude and

6
00:00:12,340 --> 00:00:16,540
 appreciation by offering him a gift, the gift of diligent

7
00:00:16,540 --> 00:00:17,540
 practice.

8
00:00:17,540 --> 00:00:20,980
 On May 9th, we are asking any who wish to please join us in

9
00:00:20,980 --> 00:00:22,780
 meditating in honor of Bonta

10
00:00:22,780 --> 00:00:25,970
 that day, perhaps dedicating more time than we usually do

11
00:00:25,970 --> 00:00:27,660
 to our practice that specific

12
00:00:27,660 --> 00:00:28,660
 day.

13
00:00:28,660 --> 00:00:31,550
 We are also posing a challenge for any and all who wish to

14
00:00:31,550 --> 00:00:32,160
 join us.

15
00:00:32,160 --> 00:00:36,680
 40 hours of meditation by May 9th in honor of 40 years of

16
00:00:36,680 --> 00:00:38,340
 life of our teacher.

17
00:00:38,340 --> 00:00:41,620
 If you begin the day this video is posted, the 40 hours equ

18
00:00:41,620 --> 00:00:43,600
ates to an average of 96 minutes

19
00:00:43,600 --> 00:00:45,800
 of meditation each day.

20
00:00:45,800 --> 00:00:49,900
 Join the Meditation Plus community at meditation.siramanglo

21
00:00:49,900 --> 00:00:53,880
.org to log your hours and chat with other meditators.

22
00:00:53,880 --> 00:00:56,580
 Thank you for helping celebrate Bonta Yutadamo's birthday

23
00:00:56,580 --> 00:00:58,600
 through the gift of diligent practice.

24
00:00:58,600 --> 00:01:00,260
 May you be well, may you be happy.

25
00:01:00,260 --> 00:01:01,260
 Thank you.

