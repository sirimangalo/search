1
00:00:00,000 --> 00:00:07,260
 Okay so today we continue our study of the Dhanapada. Today

2
00:00:07,260 --> 00:00:11,300
 we're on to verse 17.

3
00:00:11,300 --> 00:00:19,100
 No? Verse 17 and 18 are actually very similar to 15 and 16.

4
00:00:19,100 --> 00:00:19,560
 So there's not

5
00:00:19,560 --> 00:00:23,620
 going to be much to say about the verses themselves but we

6
00:00:23,620 --> 00:00:24,680
 do have some

7
00:00:24,680 --> 00:00:28,160
 interesting stories to tell. So hopefully these will be

8
00:00:28,160 --> 00:00:29,880
 stories that have meaning

9
00:00:29,880 --> 00:00:35,400
 for us. But I think they do. So this verse goes

10
00:00:35,400 --> 00:00:43,660
 "Inda Tapati, Pecha Tapati, Papakari Ubyata Tapati, Papang

11
00:00:43,660 --> 00:00:47,720
 Meekatang Ti Tapati,

12
00:00:47,720 --> 00:00:57,440
 Peyo Tapati Dukating Katoh." So the meaning is very similar

13
00:00:57,440 --> 00:01:04,200
. Here he burns or boils

14
00:01:04,200 --> 00:01:12,120
 inside. Hereafter he burns, he is burnt. The evil one is

15
00:01:12,120 --> 00:01:14,520
 burnt in both places.

16
00:01:14,520 --> 00:01:21,190
 Thinking of "Papang Meekatang, I've done evil, evil has

17
00:01:21,190 --> 00:01:22,880
 been done by me," he burns.

18
00:01:22,880 --> 00:01:32,450
 And here and even more so, "Biyo," he burns having gone to

19
00:01:32,450 --> 00:01:33,800
 suffering, having

20
00:01:33,800 --> 00:01:38,240
 gone to "Dukati," which in this case is referring to places

21
00:01:38,240 --> 00:01:41,640
 like hell and so on. In this case

22
00:01:41,640 --> 00:01:45,640
 referring to hell because this is talking about Devadatta.

23
00:01:45,640 --> 00:01:46,360
 Devadatta was a

24
00:01:46,360 --> 00:01:49,870
 cousin of the Buddhas who along with several of the

25
00:01:49,870 --> 00:01:52,280
 Buddhist relatives went

26
00:01:52,280 --> 00:01:57,740
 forth. And the story goes that the rest of the people who

27
00:01:57,740 --> 00:01:59,680
 went forth had, because

28
00:01:59,680 --> 00:02:03,030
 they had done great merit and they were the punyakari or

29
00:02:03,030 --> 00:02:04,880
 the kattapunyos as the

30
00:02:04,880 --> 00:02:08,640
 verses say, they were ones who have done good deeds in the

31
00:02:08,640 --> 00:02:10,720
 past so their minds

32
00:02:10,720 --> 00:02:12,780
 were in a good way and they were actually ordaining for the

33
00:02:12,780 --> 00:02:14,920
 right reason. When they

34
00:02:14,920 --> 00:02:19,920
 became monks they put their hearts into realization. And

35
00:02:19,920 --> 00:02:20,640
 they were actually

36
00:02:20,640 --> 00:02:24,190
 quite different. For instance, Ananda took a lot of time to

37
00:02:24,190 --> 00:02:25,280
 become enlightenment

38
00:02:25,280 --> 00:02:30,800
 because his intentions were different. He was interested in

39
00:02:30,800 --> 00:02:31,360
 remembering the

40
00:02:31,360 --> 00:02:33,560
 Buddha's teaching and looking after the Buddha and so he

41
00:02:33,560 --> 00:02:34,360
 didn't have as much

42
00:02:34,360 --> 00:02:38,760
 time for meditation. And there was Anuruddha who took time

43
00:02:38,760 --> 00:02:39,640
 as well because

44
00:02:39,640 --> 00:02:43,790
 he was caught up in magical powers. But Devadatta was so

45
00:02:43,790 --> 00:02:45,920
 caught up in his own

46
00:02:45,920 --> 00:02:50,080
 baseness of mind that he wasn't able to gain anything

47
00:02:50,080 --> 00:02:52,640
 except magical powers or

48
00:02:52,640 --> 00:02:57,300
 spiritual power from the practice. So as the rest of them

49
00:02:57,300 --> 00:02:58,920
 eventually made their

50
00:02:58,920 --> 00:03:03,240
 way to become at least sotatana and eventually arahant, Dev

51
00:03:03,240 --> 00:03:04,440
adatta only got

52
00:03:04,440 --> 00:03:10,020
 these because he was practicing tranquility only. He wasn't

53
00:03:10,020 --> 00:03:10,560
 interested in

54
00:03:10,560 --> 00:03:13,700
 insight or he wasn't putting his heart into it or he wasn't

55
00:03:13,700 --> 00:03:15,000
 able to because of

56
00:03:15,000 --> 00:03:25,080
 the evil in his heart. So as a result he gained only this

57
00:03:25,080 --> 00:03:28,860
 peace and the calm in the mind that allowed him to fly

58
00:03:28,860 --> 00:03:30,240
 through the air and

59
00:03:30,240 --> 00:03:33,990
 read people's minds or whatever he had of some magical

60
00:03:33,990 --> 00:03:35,360
 powers and he could fly

61
00:03:35,360 --> 00:03:39,760
 through the air. So he used these powers to impress the

62
00:03:39,760 --> 00:03:42,920
 king's son because all

63
00:03:42,920 --> 00:03:46,480
 the other monks were being praised and supported by the

64
00:03:46,480 --> 00:03:48,080
 people. But Devadatta,

65
00:03:48,080 --> 00:03:55,390
 also because having never done anything really good, he

66
00:03:55,390 --> 00:03:56,680
 didn't have the kind of

67
00:03:56,680 --> 00:04:01,280
 radiance that people like to praise. There were no lay

68
00:04:01,280 --> 00:04:01,840
 people looking

69
00:04:01,840 --> 00:04:06,070
 after him and also he had none of the wisdom or attainments

70
00:04:06,070 --> 00:04:07,160
 as far as

71
00:04:07,160 --> 00:04:10,720
 enlightenment was. So no one was looking for him to teach

72
00:04:10,720 --> 00:04:11,600
 and so he felt quite

73
00:04:11,600 --> 00:04:16,800
 jealous. So he tried to think of who it could be that he

74
00:04:16,800 --> 00:04:19,000
 could

75
00:04:19,000 --> 00:04:24,040
 bring under his sway and gain power over. So he thought of

76
00:04:24,040 --> 00:04:25,480
 the king's son, the

77
00:04:25,480 --> 00:04:28,280
 king he couldn't touch because the king was a sotapana, a b

78
00:04:28,280 --> 00:04:30,480
himbi sara. So he

79
00:04:30,480 --> 00:04:36,400
 went after the king's son and he used his magical powers to

80
00:04:36,400 --> 00:04:38,040
 make this

81
00:04:38,040 --> 00:04:41,560
 the prince who really didn't know the difference between

82
00:04:41,560 --> 00:04:43,440
 good and evil. To make

83
00:04:43,440 --> 00:04:45,730
 the prince think that he was enlightened and he said aye

84
00:04:45,730 --> 00:04:47,480
 and devadatta and so on.

85
00:04:47,480 --> 00:04:52,840
 He made a picture or he made an image of himself with

86
00:04:52,840 --> 00:04:55,040
 snakes and

87
00:04:55,040 --> 00:04:59,960
 flying through the air and this prince thought oh this must

88
00:04:59,960 --> 00:05:02,240
 be a real holy man.

89
00:05:02,240 --> 00:05:06,130
 So devadatta was able to get him under his sway and get him

90
00:05:06,130 --> 00:05:07,040
 to kill his father

91
00:05:07,040 --> 00:05:12,880
 and get him to help him to kill the buddha. So he did many

92
00:05:12,880 --> 00:05:13,240
 many evil

93
00:05:13,240 --> 00:05:16,480
 things. Devadatta, if you don't know the story, Devadatta

94
00:05:16,480 --> 00:05:18,240
 was a pretty bad guy.

95
00:05:18,240 --> 00:05:21,800
 If you read the jatakas you can see that it's something

96
00:05:21,800 --> 00:05:22,320
 that he's been

97
00:05:22,320 --> 00:05:27,120
 doing for a long time. So he tried, he released this wild

98
00:05:27,120 --> 00:05:28,280
 elephant to kill the

99
00:05:28,280 --> 00:05:31,400
 buddha and that didn't work. The buddha just saw this wild

100
00:05:31,400 --> 00:05:32,640
 elephant coming and

101
00:05:32,640 --> 00:05:36,950
 held out his hand and sent out loving kindness to the

102
00:05:36,950 --> 00:05:39,120
 elephant. It's something

103
00:05:39,120 --> 00:05:43,280
 that you can you can find if you're practicing meditation,

104
00:05:43,280 --> 00:05:43,840
 the area to

105
00:05:43,840 --> 00:05:48,010
 radiate loving kindness to animals. It doesn't always work

106
00:05:48,010 --> 00:05:48,840
 and if you're not as

107
00:05:48,840 --> 00:05:52,390
 powerful as the buddha, you wouldn't try it on a dog, on a

108
00:05:52,390 --> 00:05:56,160
 rabid dog, but you can feel

109
00:05:56,160 --> 00:06:01,490
 the power and so the buddha was able to extend great power

110
00:06:01,490 --> 00:06:04,040
 in this regard.

111
00:06:04,040 --> 00:06:06,400
 Then he tried to send some of these robbers after the budd

112
00:06:06,400 --> 00:06:07,440
ha. He tried to drop a

113
00:06:07,440 --> 00:06:12,920
 rock on the buddha and none of this was working. Finally,

114
00:06:12,920 --> 00:06:13,320
 when he released

115
00:06:13,320 --> 00:06:17,950
 this elephant, everyone was so scared and realized that Dev

116
00:06:17,950 --> 00:06:18,840
adatta was a

117
00:06:18,840 --> 00:06:24,600
 really evil guy, suddenly he lost all respect even from the

118
00:06:24,600 --> 00:06:27,960
 prince.

119
00:06:27,960 --> 00:06:31,930
 From that point on he had great trouble and so he had to

120
00:06:31,930 --> 00:06:32,920
 think of some other way

121
00:06:32,920 --> 00:06:37,390
 to gain power. What he thought of was that he would break

122
00:06:37,390 --> 00:06:38,040
 the buddha's

123
00:06:38,040 --> 00:06:42,290
 the sangha up. So he devised this plan that he would ask

124
00:06:42,290 --> 00:06:43,840
 the buddha to instate

125
00:06:43,840 --> 00:06:48,640
 these rules that were really strict. He said to the buddha

126
00:06:48,640 --> 00:06:49,440
 when everyone

127
00:06:49,440 --> 00:06:54,150
 was in attendance. He came up to the buddha and said, "I

128
00:06:54,150 --> 00:06:55,360
 asked that you instate

129
00:06:55,360 --> 00:06:58,560
 these five rules" and one of them was that monks should

130
00:06:58,560 --> 00:06:59,440
 always have to live in

131
00:06:59,440 --> 00:07:02,910
 the forest, that monks should always go on alms, that monks

132
00:07:02,910 --> 00:07:04,680
 should only wear rag

133
00:07:04,680 --> 00:07:09,080
 robes and so on. These were rules that the Buddha had

134
00:07:09,080 --> 00:07:11,080
 already stated that

135
00:07:11,080 --> 00:07:14,100
 certain people could undertake them and others might not

136
00:07:14,100 --> 00:07:16,040
 undertake them because

137
00:07:16,040 --> 00:07:20,120
 they're not intrinsically necessary for the practice.

138
00:07:20,120 --> 00:07:21,040
 Depending on a

139
00:07:21,040 --> 00:07:27,250
 person's situation, they might actually inhibit progress.

140
00:07:27,250 --> 00:07:28,440
 The Buddha refused to

141
00:07:28,440 --> 00:07:31,580
 lay these down and Devadatta went around saying, "Oh look,

142
00:07:31,580 --> 00:07:33,360
 who's the real

143
00:07:33,360 --> 00:07:39,330
 strict ascetic and look who's the lazy one?" So he started

144
00:07:39,330 --> 00:07:40,440
 speaking badly of

145
00:07:40,440 --> 00:07:44,170
 the buddha. At some point he also asked the buddha to step

146
00:07:44,170 --> 00:07:45,080
 down. He said, "Oh you're

147
00:07:45,080 --> 00:07:49,720
 old now, let me run the sangha." The Buddha said, "I wouldn

148
00:07:49,720 --> 00:07:50,400
't even give the

149
00:07:50,400 --> 00:07:54,850
 sangha to Sariputta or Mughalana, let alone a lickspiddle

150
00:07:54,850 --> 00:07:55,840
 like you."

151
00:07:55,840 --> 00:08:02,430
 A lickspiddle I guess is someone who lickspiddle. This is

152
00:08:02,430 --> 00:08:02,760
 what he called

153
00:08:02,760 --> 00:08:07,880
 Devadatta. It was really for the purpose because the whole

154
00:08:07,880 --> 00:08:11,080
 way along the buddha knew that Devadatta was not a good

155
00:08:11,080 --> 00:08:12,240
 person. So the question

156
00:08:12,240 --> 00:08:16,440
 always comes up, "Well why did he let Devadatta ordain?"

157
00:08:16,440 --> 00:08:17,200
 The answer is that he

158
00:08:17,200 --> 00:08:20,160
 saw from the beginning where it would lead to and he knew

159
00:08:20,160 --> 00:08:21,200
 that Devadatta had

160
00:08:21,200 --> 00:08:23,280
 been following him around and there was no way he could

161
00:08:23,280 --> 00:08:24,720
 escape. If he had let him

162
00:08:24,720 --> 00:08:27,270
 be a layperson, maybe he would have aroused an army to wipe

163
00:08:27,270 --> 00:08:27,840
 out all the

164
00:08:27,840 --> 00:08:33,330
 monks or so on. Just the pure evil that was in his heart.

165
00:08:33,330 --> 00:08:34,960
 The Buddha thought

166
00:08:34,960 --> 00:08:38,860
 that he would be a refuge for Devadatta and so he let all

167
00:08:38,860 --> 00:08:40,280
 of this happen. The

168
00:08:40,280 --> 00:08:43,210
 reason why he called him a lickspiddle was to make it very

169
00:08:43,210 --> 00:08:43,920
 clear to

170
00:08:43,920 --> 00:08:48,200
 Devadatta and to the whole world to bring out to the light

171
00:08:48,200 --> 00:08:49,720
 finally. He wasn't

172
00:08:49,720 --> 00:08:52,260
 beating around the bush because this was his last life. He

173
00:08:52,260 --> 00:08:53,080
 couldn't play games

174
00:08:53,080 --> 00:08:57,040
 anymore. So he finally told everyone after Devadatta got

175
00:08:57,040 --> 00:08:57,640
 very angry

176
00:08:57,640 --> 00:09:01,480
 and left. He told everyone he said Devadatta is evil and on

177
00:09:01,480 --> 00:09:02,280
 the wrong path

178
00:09:02,280 --> 00:09:06,430
 and so on. So of course this kind of, when you say this

179
00:09:06,430 --> 00:09:08,240
 about someone, it

180
00:09:08,240 --> 00:09:13,230
 brings about a conflict in the heart. So he got very, all

181
00:09:13,230 --> 00:09:14,560
 of his anger and hatred

182
00:09:14,560 --> 00:09:17,660
 towards the Buddha that he had built up after following the

183
00:09:17,660 --> 00:09:18,560
 Buddha from life

184
00:09:18,560 --> 00:09:24,290
 after life. It just came boiling up inside and so this is

185
00:09:24,290 --> 00:09:25,120
 where his burning

186
00:09:25,120 --> 00:09:29,970
 started. It was from the point where he decided that he was

187
00:09:29,970 --> 00:09:31,240
 going to take over

188
00:09:31,240 --> 00:09:35,580
 the Sangha and that he lost all of his magical powers. That

189
00:09:35,580 --> 00:09:38,240
's another thing. So

190
00:09:38,240 --> 00:09:45,880
 when he decided to take over the Sangha, he went around

191
00:09:45,880 --> 00:09:46,520
 asking all the

192
00:09:46,520 --> 00:09:48,930
 monks or he called all the monks together. When all the

193
00:09:48,930 --> 00:09:49,420
 monks were

194
00:09:49,420 --> 00:09:54,210
 assembled, he said whoever agrees with these five rules

195
00:09:54,210 --> 00:09:55,120
 should take a

196
00:09:55,120 --> 00:10:00,230
 ticket. So they had these tickets and some of the new monks

197
00:10:00,230 --> 00:10:00,960
 who didn't

198
00:10:00,960 --> 00:10:03,030
 know it right from wrong, they thought hey well this guy's

199
00:10:03,030 --> 00:10:04,160
 really serious.

200
00:10:04,160 --> 00:10:08,080
 Maybe the Buddha's not really serious and so they followed

201
00:10:08,080 --> 00:10:09,400
 after Devadatta.

202
00:10:09,400 --> 00:10:13,940
 Devadatta went up to Gaya Sisa and taught them. The Buddha

203
00:10:13,940 --> 00:10:15,040
 sent Sariputta

204
00:10:15,040 --> 00:10:19,060
 after him in Mughalana and Sariputta and Mughalana went up.

205
00:10:19,060 --> 00:10:20,440
 It's really kind of

206
00:10:20,440 --> 00:10:24,410
 funny how it happened. When Sariputta saw, when Devadatta

207
00:10:24,410 --> 00:10:25,320
 saw Sariputta and

208
00:10:25,320 --> 00:10:28,110
 Mughalana come, he thought oh here come the Buddha's two

209
00:10:28,110 --> 00:10:30,120
 cheap disciples. See I

210
00:10:30,120 --> 00:10:33,840
 told you eventually the whole Sangha will come around. So

211
00:10:33,840 --> 00:10:34,480
 Sariputta

212
00:10:34,480 --> 00:10:37,280
 and Mughalana didn't say anything but Devadatta thought oh

213
00:10:37,280 --> 00:10:38,080
 they've come to join

214
00:10:38,080 --> 00:10:41,050
 me and look and then Devadatta, he set himself up like the

215
00:10:41,050 --> 00:10:41,760
 Buddha and he said

216
00:10:41,760 --> 00:10:45,240
 I'm going to lie down now. Sariputta and Mughalana you

217
00:10:45,240 --> 00:10:46,560
 teach on my behalf.

218
00:10:46,560 --> 00:10:51,240
 So he lay down and because the Buddha would lie down and

219
00:10:51,240 --> 00:10:51,540
 listen

220
00:10:51,540 --> 00:10:57,360
 mindfully, Devadatta lay down and fell asleep. So Sariputta

221
00:10:57,360 --> 00:10:57,840
 and Mughalana

222
00:10:57,840 --> 00:11:00,040
 saw him fall asleep and so they turned to the monks and

223
00:11:00,040 --> 00:11:00,840
 they started teaching

224
00:11:00,840 --> 00:11:04,630
 them. Mughalana had all sorts of psychic powers that he

225
00:11:04,630 --> 00:11:05,840
 used to impress them and

226
00:11:05,840 --> 00:11:11,640
 Sariputta had this incredible wisdom and so as a result

227
00:11:11,640 --> 00:11:13,520
 they were able to

228
00:11:13,520 --> 00:11:18,750
 they were able to teach and to convert these followers of

229
00:11:18,750 --> 00:11:20,440
 Devadatta and as a

230
00:11:20,440 --> 00:11:23,710
 result allowed them to see the truth and they actually

231
00:11:23,710 --> 00:11:25,240
 became enlightened. As

232
00:11:25,240 --> 00:11:29,000
 enlightened beings they all, Sariputta said okay time but

233
00:11:29,000 --> 00:11:29,960
 let's go back to

234
00:11:29,960 --> 00:11:33,880
 where the real teaching is and so he brought all the monks

235
00:11:33,880 --> 00:11:35,560
 back to the Buddha.

236
00:11:35,560 --> 00:11:42,570
 At which point Devadatta's assistant, I forget his name,

237
00:11:42,570 --> 00:11:43,360
 woke Devadatta up

238
00:11:43,360 --> 00:11:46,310
 kicking him in the chest and saying look I told you that

239
00:11:46,310 --> 00:11:47,280
 Sariputta and Mughalana

240
00:11:47,280 --> 00:11:50,310
 are no good, what are you doing trusting them? And he

241
00:11:50,310 --> 00:11:51,880
 kicked him in the in the

242
00:11:51,880 --> 00:11:56,310
 chest and at that point Devadatta got very very sick. He

243
00:11:56,310 --> 00:11:57,920
 was quite injured by

244
00:11:57,920 --> 00:12:02,540
 that and as a result of the evil in his heart. At the point

245
00:12:02,540 --> 00:12:03,720
 when he got very very

246
00:12:03,720 --> 00:12:08,680
 sick as often happens he started to realize the evil that

247
00:12:08,680 --> 00:12:09,640
 was in his heart.

248
00:12:09,640 --> 00:12:13,220
 This is where it starts to frighten you and you can no

249
00:12:13,220 --> 00:12:13,800
 longer

250
00:12:13,800 --> 00:12:17,090
 overpower it when you're no longer in charge and you can no

251
00:12:17,090 --> 00:12:17,920
 longer run away

252
00:12:17,920 --> 00:12:21,540
 from your evil deeds. When a person is sick this is often

253
00:12:21,540 --> 00:12:22,800
 when they're good and

254
00:12:22,800 --> 00:12:27,040
 they're bad deeds become their refuge, either a good refuge

255
00:12:27,040 --> 00:12:28,200
 or their

256
00:12:28,200 --> 00:12:31,840
 prison. So in this case it became a prison and this is

257
00:12:31,840 --> 00:12:33,080
 where he finally caved in

258
00:12:33,080 --> 00:12:36,820
 and realized all the evil that he had done. He became quite

259
00:12:36,820 --> 00:12:39,000
 sick probably even

260
00:12:39,000 --> 00:12:41,320
 more so when he realized that the monks had deserted him

261
00:12:41,320 --> 00:12:42,320
 and he was now all alone

262
00:12:42,320 --> 00:12:50,920
 only with a couple of his cronies. And so he

263
00:12:50,920 --> 00:12:54,000
 desired, he asked his followers that he should like to go

264
00:12:54,000 --> 00:12:54,680
 and see the Buddha.

265
00:12:54,680 --> 00:12:59,680
 He couldn't even stand up and so he said please bring me to

266
00:12:59,680 --> 00:13:00,280
 see the Buddha and

267
00:13:00,280 --> 00:13:02,420
 they said what are you talking about you only wanted to

268
00:13:02,420 --> 00:13:03,400
 destroy him when you were

269
00:13:03,400 --> 00:13:08,800
 well and David said it's true that I wanted to destroy him

270
00:13:08,800 --> 00:13:09,560
 that the Buddha

271
00:13:09,560 --> 00:13:13,880
 never harbored any evil towards me and I'm sure he will see

272
00:13:13,880 --> 00:13:16,440
 me. And so he

273
00:13:16,440 --> 00:13:20,670
 requested please bring me and so they picked him up in his

274
00:13:20,670 --> 00:13:21,400
 bed and they

275
00:13:21,400 --> 00:13:26,420
 brought him to Jaita Vana. Now the story goes that because

276
00:13:26,420 --> 00:13:27,520
 of how evil he had

277
00:13:27,520 --> 00:13:31,140
 been and because of his treachery and breaking apart the

278
00:13:31,140 --> 00:13:32,960
 Sangha there was no

279
00:13:32,960 --> 00:13:36,870
 way that he could possibly see the Buddha. The Buddha said

280
00:13:36,870 --> 00:13:37,520
 that he was told

281
00:13:37,520 --> 00:13:40,470
 by the monks, "David Datta wants to come to see you" and

282
00:13:40,470 --> 00:13:41,640
 the Buddha said, "let him

283
00:13:41,640 --> 00:13:45,600
 come he'll never see me again" and they brought him and as

284
00:13:45,600 --> 00:13:46,680
 they were bringing

285
00:13:46,680 --> 00:13:50,200
 him people said, "oh now he's only a league away or he's

286
00:13:50,200 --> 00:13:52,120
 only a mile away"

287
00:13:52,120 --> 00:13:55,590
 and the Buddha said, "let him come right up to my doorstep

288
00:13:55,590 --> 00:13:57,120
 he'll never see me again"

289
00:13:57,120 --> 00:14:01,830
 and so they brought him on this chair all the way to Jaita

290
00:14:01,830 --> 00:14:03,560
 Vana and they

291
00:14:03,560 --> 00:14:08,610
 stopped inside the gate to take a break and to wash in the

292
00:14:08,610 --> 00:14:11,400
 pool and David

293
00:14:11,400 --> 00:14:15,560
 Datta saw them washing and so he sat up on his bed put his

294
00:14:15,560 --> 00:14:16,400
 feet on the ground

295
00:14:16,400 --> 00:14:19,040
 and when he put his feet on the ground I guess thinking

296
00:14:19,040 --> 00:14:20,200
 that he was going to walk

297
00:14:20,200 --> 00:14:26,100
 to where the Buddha was his feet sunk into the earth the

298
00:14:26,100 --> 00:14:26,720
 earth couldn't even

299
00:14:26,720 --> 00:14:29,410
 hold his weight and so they say he sunk right into the

300
00:14:29,410 --> 00:14:31,440
 earth and all the way to

301
00:14:31,440 --> 00:14:36,780
 hell or whatever and he died there and the fires of hell

302
00:14:36,780 --> 00:14:37,640
 came up and he was

303
00:14:37,640 --> 00:14:44,130
 totally torched and and arose in hell that's how the story

304
00:14:44,130 --> 00:14:45,240
 goes. Saw something

305
00:14:45,240 --> 00:14:48,970
 like that. When he rose in hell they say the story of David

306
00:14:48,970 --> 00:14:49,920
 Datta where he has to

307
00:14:49,920 --> 00:14:53,030
 go because he threat tried to attack the Buddha and tried

308
00:14:53,030 --> 00:14:54,120
 to attack the Dhamma

309
00:14:54,120 --> 00:15:01,560
 which is impossible to shake which is immovable and the D

310
00:15:01,560 --> 00:15:02,200
hamma of course

311
00:15:02,200 --> 00:15:06,060
 is eternal. They're trying to attack that truth and pervert

312
00:15:06,060 --> 00:15:07,400
 it in the form of the

313
00:15:07,400 --> 00:15:12,190
 Buddha and the Buddha's teaching. He has now transfixed

314
00:15:12,190 --> 00:15:14,280
 himself and immovable so

315
00:15:14,280 --> 00:15:18,850
 what that means is he's encased in iron with a spike going

316
00:15:18,850 --> 00:15:20,720
 from his head out

317
00:15:20,720 --> 00:15:24,530
 through his bottom from the right side out through the left

318
00:15:24,530 --> 00:15:26,280
 side and just

319
00:15:26,280 --> 00:15:32,720
 constant fire and burning transfixed and immovable for eons

320
00:15:32,720 --> 00:15:35,560
 and eons. But the plus

321
00:15:35,560 --> 00:15:38,340
 side is that the Buddha said it would have been worse for

322
00:15:38,340 --> 00:15:39,960
 him if he hadn't

323
00:15:39,960 --> 00:15:43,380
 come in contact with the Buddha. If the Buddha hadn't woke

324
00:15:43,380 --> 00:15:44,280
 him up in his way

325
00:15:44,280 --> 00:15:48,900
 because at the very end he realized and he gained wisdom

326
00:15:48,900 --> 00:15:51,040
 and understanding. So the

327
00:15:51,040 --> 00:15:54,320
 story goes that after all of the time that he spends in

328
00:15:54,320 --> 00:15:55,760
 hell he will be

329
00:15:55,760 --> 00:15:59,780
 reborn as a private Buddha at the tikka minute. So it's a

330
00:15:59,780 --> 00:16:01,560
 long story it's actually much

331
00:16:01,560 --> 00:16:05,890
 longer than that. I tried my best to abridge it but it's a

332
00:16:05,890 --> 00:16:07,420
 story of great

333
00:16:07,420 --> 00:16:12,870
 importance and one that we always talk about and remember.

334
00:16:12,870 --> 00:16:13,880
 It's a story of

335
00:16:13,880 --> 00:16:17,180
 what not to do if you become a monk. It's a sort of monk

336
00:16:17,180 --> 00:16:18,680
 that you don't want to be.

337
00:16:18,680 --> 00:16:25,600
 So the lesson is very much in accord with the verses before

338
00:16:25,600 --> 00:16:26,360
 that a

339
00:16:26,360 --> 00:16:29,400
 person who does bad deeds bad things happen to them. They

340
00:16:29,400 --> 00:16:30,960
 actually burn the

341
00:16:30,960 --> 00:16:37,680
 word dappati. They are vexed here and vexed in the future.

342
00:16:37,680 --> 00:16:42,160
 They burn really. So the

343
00:16:42,160 --> 00:16:44,520
 idea of burning in hell has to do with all of the anger

344
00:16:44,520 --> 00:16:46,320
 that's built up and the

345
00:16:46,320 --> 00:16:49,770
 result of having so much anger inside. But the anger inside

346
00:16:49,770 --> 00:16:50,640
 itself of course

347
00:16:50,640 --> 00:16:55,510
 is burning. Just another story in regards to how evil

348
00:16:55,510 --> 00:16:58,340
 people burned inside.

349
00:16:58,340 --> 00:17:03,740
 There was once a teacher, one of my teacher's teachers in

350
00:17:03,740 --> 00:17:04,940
 Thailand, I read a

351
00:17:04,940 --> 00:17:12,410
 book about it. He was very much involved in bringing Vipass

352
00:17:12,410 --> 00:17:13,440
ana meditation to

353
00:17:13,440 --> 00:17:17,850
 Thailand and also the Abhidhamma and many things that

354
00:17:17,850 --> 00:17:21,520
 Thailand wasn't

355
00:17:21,520 --> 00:17:24,180
 well equipped with. So from Burma they brought a lot

356
00:17:24,180 --> 00:17:25,320
 because in Burma of course

357
00:17:25,320 --> 00:17:28,030
 they will directly study the tepidika. These books come

358
00:17:28,030 --> 00:17:30,120
 from Burma that we get

359
00:17:30,120 --> 00:17:33,180
 here and they would actually read through and study these.

360
00:17:33,180 --> 00:17:34,160
 Whereas in Thailand

361
00:17:34,160 --> 00:17:36,970
 very little study of the tepidika is done. So he brought it

362
00:17:36,970 --> 00:17:38,760
 all over and that's

363
00:17:38,760 --> 00:17:41,840
 why now in Thailand they will actually teach Abhidhamma for

364
00:17:41,840 --> 00:17:42,640
 example and that's

365
00:17:42,640 --> 00:17:46,070
 where this meditation comes from. My teacher was sent as

366
00:17:46,070 --> 00:17:47,560
 part of a group to

367
00:17:47,560 --> 00:17:54,360
 go to Myanmar to learn meditation. But of course there were

368
00:17:54,360 --> 00:17:54,600
 people

369
00:17:54,600 --> 00:18:02,200
 who didn't like this and actually said things like, "Isn't

370
00:18:02,200 --> 00:18:02,680
 Buddhism in

371
00:18:02,680 --> 00:18:07,830
 Thailand good enough for you?" And really people who didn't

372
00:18:07,830 --> 00:18:08,840
 like Burma because

373
00:18:08,840 --> 00:18:12,350
 Burma had once attacked Thailand. Anyway actually the real

374
00:18:12,350 --> 00:18:13,640
 core of it is this

375
00:18:13,640 --> 00:18:16,210
 monk was quite popular and what he was doing was quite

376
00:18:16,210 --> 00:18:17,400
 popular with the people

377
00:18:17,400 --> 00:18:21,090
 because he would teach meditation to ordinary people or he

378
00:18:21,090 --> 00:18:22,080
 was involved with

379
00:18:22,080 --> 00:18:24,820
 it and they were teaching Abhidhamma and they were teaching

380
00:18:24,820 --> 00:18:25,560
 so many new things

381
00:18:25,560 --> 00:18:29,050
 so people were quite interested. And it was all about

382
00:18:29,050 --> 00:18:30,440
 jealousy because he

383
00:18:30,440 --> 00:18:33,900
 started getting power and he had a powerful position and

384
00:18:33,900 --> 00:18:34,920
 the other monks

385
00:18:34,920 --> 00:18:38,490
 who were vying for the same position or were trying to get

386
00:18:38,490 --> 00:18:39,800
 to the top position

387
00:18:39,800 --> 00:18:43,360
 and thought that he might be getting for it, getting it,

388
00:18:43,360 --> 00:18:45,120
 they began to attack him.

389
00:18:45,120 --> 00:18:52,610
 And eventually a group of them, apparently including the

390
00:18:52,610 --> 00:18:53,040
 Sangharaj,

391
00:18:53,040 --> 00:18:59,120
 the very supreme patriarch, although his involvement isn't

392
00:18:59,120 --> 00:19:00,000
 quite clear,

393
00:19:00,000 --> 00:19:05,440
 they accused him of having sexual intercourse with a woman

394
00:19:05,440 --> 00:19:05,680
 and they

395
00:19:05,680 --> 00:19:08,740
 actually found a woman who was willing to say that he had

396
00:19:08,740 --> 00:19:09,680
 sexual intercourse

397
00:19:09,680 --> 00:19:13,680
 with her. And I was reading this book, it was a wonderful,

398
00:19:13,680 --> 00:19:14,640
 very thick book and

399
00:19:14,640 --> 00:19:18,020
 I'm trying to read through it in Thai and when I came to

400
00:19:18,020 --> 00:19:20,680
 this part with the woman,

401
00:19:20,680 --> 00:19:24,360
 you know, and then they sent him a letter and said, "Please

402
00:19:24,360 --> 00:19:24,680
, you should

403
00:19:24,680 --> 00:19:28,000
 disrobe, you've done this terrible thing and we have a

404
00:19:28,000 --> 00:19:28,640
 witness and

405
00:19:28,640 --> 00:19:32,700
 we've gone through the motions of investigating and found

406
00:19:32,700 --> 00:19:33,120
 that you're

407
00:19:33,120 --> 00:19:36,800
 guilty, you should disrobe and leave." And he said, "If I'd

408
00:19:36,800 --> 00:19:37,480
 actually done it,

409
00:19:37,480 --> 00:19:40,760
 why should I disrobe?" Or they said, "You should, disrobe

410
00:19:40,760 --> 00:19:42,400
 means you should cease

411
00:19:42,400 --> 00:19:45,840
 to become a monk." And they even called him "nung ta" or

412
00:19:45,840 --> 00:19:46,560
 something like that,

413
00:19:46,560 --> 00:19:49,750
 that's what means a monk. They still called him by a monk

414
00:19:49,750 --> 00:19:51,720
 and he said, "Well, how could

415
00:19:51,720 --> 00:19:55,360
 I be a monk if I had done this? Why should I stop becoming

416
00:19:55,360 --> 00:19:56,200
 a monk?"

417
00:19:56,200 --> 00:19:59,520
 Because of the wording, he said, "You should cease to

418
00:19:59,520 --> 00:20:00,520
 become a monk." And he said,

419
00:20:00,520 --> 00:20:03,290
 "Well, if I did it, I would." So obviously it's just a sham

420
00:20:03,290 --> 00:20:04,200
 that they're making up.

421
00:20:04,200 --> 00:20:08,260
 But the interesting point that I wanted to make is in

422
00:20:08,260 --> 00:20:09,280
 regard to this woman,

423
00:20:09,280 --> 00:20:13,320
 as you turn the pages and you come to, he said, eventually

424
00:20:13,320 --> 00:20:14,680
 they found the woman and

425
00:20:14,680 --> 00:20:21,730
 they talked with her and after repeated trying to get her

426
00:20:21,730 --> 00:20:22,800
 to tell the truth,

427
00:20:22,800 --> 00:20:27,350
 eventually she came forth and she wrote this letter and she

428
00:20:27,350 --> 00:20:29,320
 came forward by herself.

429
00:20:29,320 --> 00:20:35,210
 And she said, "I just want to sleep at night." She said, "I

430
00:20:35,210 --> 00:20:37,520
'm sorry for what I did.

431
00:20:37,520 --> 00:20:43,860
 I realize now that a person who does evil deeds can never

432
00:20:43,860 --> 00:20:46,000
 prosper.

433
00:20:46,000 --> 00:20:48,830
 And what I've done is a horrible thing and it's given me

434
00:20:48,830 --> 00:20:50,560
 nightmares ever since.

435
00:20:50,560 --> 00:20:53,960
 I haven't been able to sleep. I've just read it. It was

436
00:20:53,960 --> 00:20:58,520
 such a heart-rending letter."

437
00:20:58,520 --> 00:21:04,240
 And she said, "That monk isn't guilty of anything and I'm

438
00:21:04,240 --> 00:21:04,760
 writing this letter

439
00:21:04,760 --> 00:21:08,340
 because I just want to sleep at night. I don't want to go

440
00:21:08,340 --> 00:21:09,200
 to hell."

441
00:21:09,200 --> 00:21:13,780
 So this kind of thing, in a person who actually has some w

442
00:21:13,780 --> 00:21:15,480
its about them

443
00:21:15,480 --> 00:21:20,750
 and is not totally corrupt as Devadatta was, when you do an

444
00:21:20,750 --> 00:21:21,240
 evil deed,

445
00:21:21,240 --> 00:21:28,040
 it can really hit you very hard and the realization comes

446
00:21:28,040 --> 00:21:32,240
 that evil really does exist.

447
00:21:32,240 --> 00:21:35,310
 For people like Devadatta, they're able to use their force

448
00:21:35,310 --> 00:21:38,680
 of will, as I said, to cover it up.

449
00:21:38,680 --> 00:21:42,220
 Most people are not able to do that and as a result have

450
00:21:42,220 --> 00:21:43,840
 some semblance of morality

451
00:21:43,840 --> 00:21:48,250
 and they realize when they do evil deeds. But eventually it

452
00:21:48,250 --> 00:21:51,000
 comes to you at any rate.

453
00:21:51,000 --> 00:21:53,630
 For Devadatta, it came when he was very sick and his mind

454
00:21:53,630 --> 00:21:56,120
 was weakened by the sickness.

455
00:21:56,120 --> 00:21:59,900
 And so as a result, he wasn't able to cover up the evil

456
00:21:59,900 --> 00:22:00,680
 deeds

457
00:22:00,680 --> 00:22:04,180
 and he was forced to see them and then he realized for

458
00:22:04,180 --> 00:22:05,880
 himself the badness.

459
00:22:05,880 --> 00:22:08,920
 Unfortunately, what's done is done. It's very difficult.

