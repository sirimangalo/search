 It says, "Bonti, I have been thinking recently, I have
 already made mention of how some of
 the Christians hand out magazines to me at the bus stop.
 Would it be wrong if I told them I'll take it if they
 listen to what I have to say?
 Not so much they are wrong, but wouldn't have any effect
 upon others anyway."
 What do you think, Larry?
 Is that wrong?
 "Yes, in a way I think it is wrong, because first of all
 they're wasting other people's
 time and as he stated, he or she, handing out one pamphlet
 is not going to make a difference
 in how someone feels about their inner beliefs in the first
 place.
 So really it is quite agitating.
 We have some people here in the state that go door to door
 from time to time handing
 out pamphlets.
 I usually just out of courtesy take them and say thank you
 and I go chunk them as soon
 as they leave the front door because I'm not going to read
 them because I know basically
 I've read enough to basically know some of their beliefs
 and I just have no interest
 if they want to believe that way fine.
 But if not, I believe the way I would want to believe.
 So I think to be polite to those individuals would be just
 to say I'd rather not take your
 your pamphlets because I really don't want your pamphlet
 and leave it at that.
 A confrontation particularly in a bus station or an airport
 is futile to begin with.
 No one is going to convince anyone else of anything.
 So they believe that, they believe in apostasizing and that
's their belief, that's fine but just
 don't take it out on me.
 Anybody else?
 Not wrong.
 I don't think it's an eye for an eye kind of thing.
 You have to listen to me and I'll listen to you.
 Fair as fair, right?
 Yeah, I would just say thanks for calling my ones.
 If it's wrong for them to be confrontational in forcing
 their views upon you or if it seems
 kind of rude and so on then adding rude to rude is not
 probably the best.
 What I find works much better is to inquire as to their
 beliefs politely.
 What I find has the best effect on people who engage in
 such blind faith in things like
 God and so on and other imaginary things is to inquire and
 actually investigate with them
 the way Socrates did.
 Not exactly the way Socrates did but politely asking and
 because they have so much stress,
 I've talked about this before, these people have so much
 stress and tension.
 This is the person who asked the question before, I don't
 remember it but I remember
 answering this somehow that they have so much stress and
 tension built up that really all
 they want is to let go.
 Not want but really all they need and really what would
 make them happy, truly happy is
 to let go and stop trying to justify something that's
 totally unjustifiable and irrational,
 trying to rationalize it.
 Here in the United States so many times, one particular
 denomination that I'm familiar
 with, they send their children around door to door.
 They sit in the car, I don't know if they came to come up
 or afraid or what but they'll
 send their children to the door with these pamphlets and
 you can't confront those kids
 because quite frankly I think they've been brainwashed.
 It's just like the church out in Missouri that their
 parents and their kids, I'm sure
 all of you are familiar with this, at least the ones here
 in the states where they're
 protesting the military funerals of soldiers that have been
 killed in action and these
 church members and their children, they've all been indoctr
inated.
 The ones here locally are not affiliated with that at all
 but I do Google them from time
 to time and find out that it happens to be one of the, I
 guess you could call it a sect,
 they wouldn't call themselves a sect but they happen to be
 a group that feel like they're
 wrong and everyone, I mean they're right and everyone else
 is wrong and the fact I've read
 some articles written by previous members of that
 particular religious sect that say
 once you are...
 We're losing you Larry.
 I think his internet term that they use in that particular
 sect but it is true too so
 it's a very sticky question.
 I just feel like personally I just really don't have the
 time for them.
 I try to get out of the situation as quickly as I can
 without being confrontational with
 them but they're just not going to convince me of their
 ways regardless of what they say
 or what they give me so to me it's just a waste of my time
 there, let them go find somebody
 that will believe what they say.
 With children it's especially difficult.
 I remember once these evangelicals came to the monastery
 and once they had me targeted
 this woman, there were some women there and then one of
 them was the leader and she totally
 cold and I said would you like this card and they said no,
 no, she said for everyone no
 we don't want your card, just in a really cold and hard way
 and then suddenly the next
 night she comes back with her husband and her two kids and
 I think I started talking
 to the kids and it was well depressing because they
 obviously have no ability to think for
 themselves and have never been taught how to use the reason
 circuits in their brain
 or in the mind, the mind's ability to investigate and they
've been totally speaking, talking
 of the brain as a filter and well their brain is like a
 vice, vice grip, keeping them locked
 in on certain predefined views.
 So I asked them and the parents said go ahead Tommy, tell
 him and then which is really sad
 because yeah what can you do with kids?
 They're so malleable that you can twist them and turn them
 in any sort of way.
 They're not strong enough to reject the things that their
 parents teach them.
 When they get older they may be able to but you still have
 to feel for these people and
 to some extent, not to a very big extent, but to some
 extent see their suffering or
 from my point of view because what happened then, I think I
've told this story before,
 the next day I said well if they're coming to visit me I
 should go visit them and then
 the next day I went to their church which was this huge
 church just down the road from
 what, Thai of Los Angeles and it was such an interesting
 experience.
 I talked with this teacher of preachers, he was a preacher
 teacher, he taught preachers,
 pastors, he taught pastors and he was suffering, it was so
 easy to see.
 I made him see, not to a great extent but he must be
 dealing with this conflict inside
 of himself every day, how difficult it is to keep this
 faith that is so ungrounded in
 any form of reality.
 To the extent that you're able to gently show them their
 own suffering, I think you do them
 a great deal of benefit.
 I don't mind if you talk to me, I don't mind if you give me
 your brochure.
 Why because I'm happy, I'm settled in who I am.
 You want to give me your brochure, sure, it's just a piece
 of paper but somehow bringing
 across to them a sense of peace and happiness and helping
 them to see that that's not possible
 to gain with blind faith, that you are free from something
 that they are not free from.
 I think this doesn't come from imposing your views on them,
 it comes from being accepting
 and open and loving and caring towards them because really
 that's generally why people
 go to such cults you might say or sects, is because of the
 love that they find, the need
 that they have for being controlled or for being taken care
 of, the need for in Judeo-Christian
 religions the need for a father figure.
 It's such a fatherly religion, if you actually read the
 things that Jesus says it's very
 much, very oppressive actually.
 Some of it's nice but a lot of it's just your father, your
 father and how you have to.
 So if you help people to see the possibility of just loving
 without needing, without needing,
 then you do them a favour.
 This is the thing, rather than seeing it as a conflict,
 rather than seeing it as a confrontation
 you should see it as a fellow human being who you might be
 able to help.
 If you think in terms of helping this person and not in a
 condescending or self-righteous
 way, but just in the way that you would help someone who
 needs food and give them some
 food, you're not judging them and you're not thinking to
 look down on them or that you're
 better than them, but just look and see if there's any way
 you can help you with children.
 If it's children bringing pamphlets to your door there's
 not much you can do but you can
 love them.
 It makes you happy and these kids come to your door and
 they want to give you some brochures
 and say thank you, I hope you have a good day.
 It's good for you, that's a good thing that you've done for
 them.
 It's not going to change them or make them doubt their
 religion, not in any significant
 way, but it's what you can do.
 You gave them, it's like you gave them a muffin or
 something.
 You gave them something that was good for them.
 It's not going to solve the world's problems, but we're not
 here to solve the world's problems.
 We're here to do good things, avoid evil things and purify
 our own minds.
 So whatever helps you to do that, that's what I would do.
 Hopefully that's what I would do.
 I might just get all self-righteous on them and start
 telling them about Buddhism and
 so on, but I should.
 I'm not saying that I practice everything that I preach,
 but do as I say, not as I maybe
 do sometimes.
 Thank you.
