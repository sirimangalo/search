1
00:00:00,000 --> 00:00:03,000
 I have a question. What is the best course of meditation

2
00:00:03,000 --> 00:00:07,070
 for someone that is very irritable and suffers from an

3
00:00:07,070 --> 00:00:08,500
 anxiety disorder?

4
00:00:08,500 --> 00:00:12,360
 I have trouble keeping still and doing sitting meditation.

5
00:00:12,360 --> 00:00:13,500
 Any advice?

6
00:00:21,500 --> 00:00:28,540
 You can start by doing lying meditation. A person who has

7
00:00:28,540 --> 00:00:43,540
 anxiety order will do much better in lying meditation, much

8
00:00:43,540 --> 00:00:46,000
 better in sitting meditation than in walking meditation,

9
00:00:46,000 --> 00:00:52,090
 and much better in lying meditation than an ordinary person

10
00:00:52,090 --> 00:00:56,070
. For most people lying meditation will put them to sleep,

11
00:00:56,070 --> 00:00:58,000
 but for someone with anxiety disorder,

12
00:00:58,000 --> 00:01:07,480
 it will actually serve to balance the energy. So if you

13
00:01:07,480 --> 00:01:17,550
 find that do sitting meditation for as long as you can, and

14
00:01:17,550 --> 00:01:19,500
 then when you are not able to sit any longer,

15
00:01:19,500 --> 00:01:25,490
 try to acknowledge the desire to move, acknowledge the

16
00:01:25,490 --> 00:01:32,200
 distractions in the mind, acknowledge the anxiety, the

17
00:01:32,200 --> 00:01:35,500
 mental upset.

18
00:01:35,500 --> 00:01:39,150
 And then when it becomes overwhelming, then do lying, lie

19
00:01:39,150 --> 00:01:42,760
 down and do lying meditation, and just the same rising,

20
00:01:42,760 --> 00:01:47,050
 falling, watching the stomach just the same, you can lie on

21
00:01:47,050 --> 00:01:49,500
 your back and so on.

22
00:01:55,500 --> 00:02:00,140
 If you are really serious about meditation, no matter what

23
00:02:00,140 --> 00:02:04,290
 your condition is, the best advice one can give is to go

24
00:02:04,290 --> 00:02:06,500
 and do a meditation course,

25
00:02:06,500 --> 00:02:12,550
 because most of the conditions we have are the most

26
00:02:12,550 --> 00:02:18,070
 important conditions to do away with are the ones on the

27
00:02:18,070 --> 00:02:19,500
 surface.

28
00:02:19,500 --> 00:02:25,210
 The conditions like anxiety may be with you for a long time

29
00:02:25,210 --> 00:02:28,800
, even after you do several meditation courses, all you

30
00:02:28,800 --> 00:02:31,800
 might find is it gets weaker and weaker, and simpler and

31
00:02:31,800 --> 00:02:32,500
 simpler.

32
00:02:32,500 --> 00:02:37,910
 But the first thing is to learn how to gain the skills to

33
00:02:37,910 --> 00:02:42,850
 learn how to deal with the anxiety and so on, and to

34
00:02:42,850 --> 00:02:46,350
 straighten your mind out about the anxiety about what it is

35
00:02:46,350 --> 00:02:50,500
, about whose it is in the sense of not being anybody's.

36
00:02:50,500 --> 00:02:56,840
 And for that you really need a meditation environment, you

37
00:02:56,840 --> 00:02:58,500
 need a course.

38
00:02:58,500 --> 00:03:08,960
 So it really doesn't matter what your condition is, try to

39
00:03:08,960 --> 00:03:18,200
 find time to go and undertake a meditation course with a

40
00:03:18,200 --> 00:03:22,500
 proficient teacher.

41
00:03:22,500 --> 00:03:26,440
 And the more time the better. A person with anxiety, the

42
00:03:26,440 --> 00:03:29,940
 best thing is to give them a long course because you don't

43
00:03:29,940 --> 00:03:32,770
 want to give them deadlines, and you don't want them to be

44
00:03:32,770 --> 00:03:34,500
 thinking about deadlines and so on.

45
00:03:34,500 --> 00:03:39,960
 You want to take it slow and easy. But in the beginning any

46
00:03:39,960 --> 00:03:43,470
 relying meditation can be quite useful, so I would and do

47
00:03:43,470 --> 00:03:45,500
 recommend that for anxiety.

