1
00:00:00,000 --> 00:00:04,370
 Okay, I understand calling myself a Buddhist isn't

2
00:00:04,370 --> 00:00:06,560
 important, but I was wondering if you

3
00:00:06,560 --> 00:00:10,910
 could suggest a more Buddhist way of living my life, daily

4
00:00:10,910 --> 00:00:12,040
 practice.

5
00:00:12,040 --> 00:00:14,950
 More specifically, in your tradition, is there a structure

6
00:00:14,950 --> 00:00:17,200
 to daily life I should be following?

7
00:00:17,200 --> 00:00:23,370
 I.e., meditation twice a day, has designated reflection

8
00:00:23,370 --> 00:00:27,280
 time, designated study time, etc.

9
00:00:27,280 --> 00:00:31,400
 I think so. You want to start? Go ahead.

10
00:00:31,400 --> 00:00:36,480
 I just had the thought, try to be mindful all the time.

11
00:00:36,480 --> 00:00:40,120
 That seems to be the most important

12
00:00:40,120 --> 00:00:44,390
 for me, but now please continue with what you wanted to say

13
00:00:44,390 --> 00:00:44,720
.

14
00:00:44,720 --> 00:00:52,120
 I find the schedule helps, hesitatingly, because it's...

15
00:00:52,120 --> 00:00:53,200
 you don't want to be beating yourself

16
00:00:53,200 --> 00:00:56,400
 up because you don't have a schedule. "Oh, I didn't med

17
00:00:56,400 --> 00:00:58,160
itate for my two hours today.

18
00:00:58,160 --> 00:01:01,750
 Oh, what a bad meditator I am. How am I ever going to

19
00:01:01,750 --> 00:01:02,720
 become a man?"

20
00:01:02,720 --> 00:01:06,780
 Wait a minute, why am I not meditating on this? Why am I

21
00:01:06,780 --> 00:01:08,560
 not mindful of this?

22
00:01:08,560 --> 00:01:12,120
 Because meditation can come at any time. But I think

23
00:01:12,120 --> 00:01:14,680
 without a schedule in the world, it's

24
00:01:14,680 --> 00:01:18,770
 easy to get caught up. You say, "I'll just be mindful

25
00:01:18,770 --> 00:01:20,160
 during my daily life." And then

26
00:01:20,160 --> 00:01:24,690
 you become less and less mindful during your daily life and

27
00:01:24,690 --> 00:01:27,160
 can get lost because you really

28
00:01:27,160 --> 00:01:33,500
 have nothing to lean on or nothing to cling to, nothing to

29
00:01:33,500 --> 00:01:36,680
 keep you strong when you're

30
00:01:36,680 --> 00:01:39,670
 training. If you're not very strong in and of yourself in

31
00:01:39,670 --> 00:01:42,800
 the world, you'll just get

32
00:01:42,800 --> 00:01:45,650
 washed away because you can say, "Be mindful." But if you

33
00:01:45,650 --> 00:01:47,120
 don't even yet know what mindfulness

34
00:01:47,120 --> 00:01:51,960
 is, how can you... it's like pulling yourself up by your

35
00:01:51,960 --> 00:01:54,560
 bootstraps, right? So in that sense,

36
00:01:54,560 --> 00:01:58,940
 especially in the beginning, a schedule I think can be

37
00:01:58,940 --> 00:02:02,000
 quite helpful. And it forces you

38
00:02:02,000 --> 00:02:06,890
 to not get involved. It gives you an excuse. If you can

39
00:02:06,890 --> 00:02:08,160
 manage to have a schedule, you

40
00:02:08,160 --> 00:02:10,540
 can say, "Well, now's my meditation time," or "Today's my

41
00:02:10,540 --> 00:02:13,320
 meditation day," or so on.

42
00:02:13,320 --> 00:02:15,950
 And if you have that schedule for yourself, you can make

43
00:02:15,950 --> 00:02:17,360
 excuses for yourself and for

44
00:02:17,360 --> 00:02:20,150
 other people why you shouldn't be doing other things.

45
00:02:20,150 --> 00:02:22,080
 Otherwise, it's easy to say, "Oh,

46
00:02:22,080 --> 00:02:25,910
 just let it go. Go with the flow and they want me to help

47
00:02:25,910 --> 00:02:28,280
 them with this or that. Well,

48
00:02:28,280 --> 00:02:32,520
 I'll make merit doing helping them with this or that." But

49
00:02:32,520 --> 00:02:33,200
 when you have a schedule, you

50
00:02:33,200 --> 00:02:35,690
 can say, "I'm sorry. At this time, I'm not free. I'm doing

51
00:02:35,690 --> 00:02:38,440
 my meditation," and so on.

52
00:02:38,440 --> 00:02:45,030
 So it helps you to actually do the practice. I found that

53
00:02:45,030 --> 00:02:46,520
 that was helpful for me when

54
00:02:46,520 --> 00:02:49,370
 I was in university, for example, that I would make sure in

55
00:02:49,370 --> 00:02:51,120
 the morning I did some meditation

56
00:02:51,120 --> 00:02:55,160
 and in the evening I did some meditation. I guess probably

57
00:02:55,160 --> 00:02:58,080
 what would be best to suggest

58
00:02:58,080 --> 00:03:02,880
 then is that to do some, but not set time limits. So some

59
00:03:02,880 --> 00:03:03,920
 days it might only be half

60
00:03:03,920 --> 00:03:07,420
 an hour. Some days it might be an hour. Some days it might

61
00:03:07,420 --> 00:03:09,400
 only be five minutes. Meditators,

62
00:03:09,400 --> 00:03:13,080
 when they leave, generally one of the most common questions

63
00:03:13,080 --> 00:03:15,120
 we get is, "How much meditation

64
00:03:15,120 --> 00:03:18,100
 should I do every day?" I obviously won't be able to every

65
00:03:18,100 --> 00:03:19,680
 day do eight hours, ten hours,

66
00:03:19,680 --> 00:03:24,110
 twelve hours a day. How much is the minimum that I should

67
00:03:24,110 --> 00:03:26,040
 do? And so after getting this

68
00:03:26,040 --> 00:03:30,990
 question often, my answer started being, "If you do zero

69
00:03:30,990 --> 00:03:33,880
 minutes, that's not enough."

70
00:03:33,880 --> 00:03:36,290
 As long as you do more than that, you can consider that you

71
00:03:36,290 --> 00:03:37,440
've done your duty for the

72
00:03:37,440 --> 00:03:41,370
 day. Do some meditation every day. I think that's useful.

73
00:03:41,370 --> 00:03:43,040
 It may seem silly because,

74
00:03:43,040 --> 00:03:45,280
 "Well, then what? One minute is enough and then I should

75
00:03:45,280 --> 00:03:47,120
 feel content." But no, the

76
00:03:47,120 --> 00:03:49,520
 point then is that you're meditating. The point then is

77
00:03:49,520 --> 00:03:51,120
 that you're intent on meditating.

78
00:03:51,120 --> 00:03:54,400
 There are people who after they leave, they will go years

79
00:03:54,400 --> 00:03:56,280
 without meditating, which is

80
00:03:56,280 --> 00:04:01,440
 unfathomable really. If you set yourself on doing some

81
00:04:01,440 --> 00:04:03,160
 every day and you don't say how

82
00:04:03,160 --> 00:04:05,760
 many it's going to be, I think you'll never come to that. I

83
00:04:05,760 --> 00:04:06,760
 think you'll always come

84
00:04:06,760 --> 00:04:10,810
 back to, "Oh, today maybe I'll do a little more," or push

85
00:04:10,810 --> 00:04:12,800
 myself because if you do five

86
00:04:12,800 --> 00:04:16,900
 minutes, you say, "Wow, that was ... my mind's still not

87
00:04:16,900 --> 00:04:19,320
 even settled." And you'll say, "Oh,

88
00:04:19,320 --> 00:04:21,640
 that was a horrible meditation." And you'll say, "That's

89
00:04:21,640 --> 00:04:22,840
 because I haven't been mindful

90
00:04:22,840 --> 00:04:27,600
 for the rest of the day." It gives you that wake up call

91
00:04:27,600 --> 00:04:31,400
 and says, "Hey, remember, you're

92
00:04:31,400 --> 00:04:36,210
 not on this earth just to die. You're not on this earth

93
00:04:36,210 --> 00:04:39,280
 just to eat, drink, and be merry

94
00:04:39,280 --> 00:04:42,800
 and then pass away having lived a useless life, that there

95
00:04:42,800 --> 00:04:44,360
's more to life than wakes

96
00:04:44,360 --> 00:04:50,010
 you up. Hey, your mind, the mind is another organ in a

97
00:04:50,010 --> 00:04:53,360
 sense. You have the muscles in

98
00:04:53,360 --> 00:04:55,710
 your body and if you don't take care of them, they become

99
00:04:55,710 --> 00:04:59,120
 like ours." Well, the mind is

100
00:04:59,120 --> 00:05:03,220
 that way as well. Hey, if you don't take care of your mind,

101
00:05:03,220 --> 00:05:05,200
 it starts to atrophy. Your mind

102
00:05:05,200 --> 00:05:12,320
 becomes weak, your mind becomes useless, your mind becomes

103
00:05:12,320 --> 00:05:18,800
 your worst enemy. And so the seeing

104
00:05:18,800 --> 00:05:21,660
 this, the seeing that when you practice even say five

105
00:05:21,660 --> 00:05:23,600
 minutes a day, you say, "I'm going

106
00:05:23,600 --> 00:05:25,810
 to at least do some every day," and you do five minutes,

107
00:05:25,810 --> 00:05:27,600
 you'll see that. You'll say,

108
00:05:27,600 --> 00:05:32,420
 "My mind has got some bad stuff in it. I've been neglecting

109
00:05:32,420 --> 00:05:35,040
 my mind." And that I think

110
00:05:35,040 --> 00:05:37,950
 helps to keep you on the path. So in regards to practice,

111
00:05:37,950 --> 00:05:39,880
 that's what I would say, do at

112
00:05:39,880 --> 00:05:43,810
 least more than, whatever is more than zero, do that much

113
00:05:43,810 --> 00:05:46,000
 per day, at least that much per

114
00:05:46,000 --> 00:05:49,850
 day. And you don't have to specify, I wouldn't specify. If

115
00:05:49,850 --> 00:05:51,120
 you want to say, "I'm going to

116
00:05:51,120 --> 00:05:54,940
 do more than zero twice a day," it's even better. And you

117
00:05:54,940 --> 00:05:56,920
 don't have to specify at least

118
00:05:56,920 --> 00:06:01,670
 before you sleep, do five minutes. And of course, the more

119
00:06:01,670 --> 00:06:04,000
 the better. But yeah, you

120
00:06:04,000 --> 00:06:08,230
 can add that in there and the more the better. So then you

121
00:06:08,230 --> 00:06:11,000
 have your schedule set up. As

122
00:06:11,000 --> 00:06:17,120
 for reflection time and study time, it so depends on your

123
00:06:17,120 --> 00:06:20,340
 lifestyle. Great. If you can

124
00:06:20,340 --> 00:06:24,440
 do study every day, as a monk, having been a monk for most

125
00:06:24,440 --> 00:06:26,720
 of the time that I was Buddhist,

126
00:06:26,720 --> 00:06:31,820
 I don't really have much to compare to. I've been studying

127
00:06:31,820 --> 00:06:33,920
 quite a bit. But even as a lay

128
00:06:33,920 --> 00:06:36,900
 person, I would always dedicate time to reading the

129
00:06:36,900 --> 00:06:39,400
 Buddhist teaching. You have this books

130
00:06:39,400 --> 00:06:44,480
 of the Buddhist teaching, try to do one sutta a day. I don

131
00:06:44,480 --> 00:06:46,160
't think designated times is generally

132
00:06:46,160 --> 00:06:49,660
 helpful in that sense, but it depends on your schedule, it

133
00:06:49,660 --> 00:06:51,520
 depends on your lifestyle. I

134
00:06:51,520 --> 00:06:54,550
 would say, do some every day, make sure that you're doing

135
00:06:54,550 --> 00:06:56,600
 some every day. If you have

136
00:06:56,600 --> 00:07:00,660
 the time and more if you have the community, then you can

137
00:07:00,660 --> 00:07:03,620
 set up a schedule and do it together

138
00:07:03,620 --> 00:07:08,400
 as a group, get everyone together and have each other's

139
00:07:08,400 --> 00:07:10,380
 support. If you're on your own,

140
00:07:10,380 --> 00:07:13,570
 I think it's difficult to expect that you're going to every

141
00:07:13,570 --> 00:07:15,560
 day do some at this time or

142
00:07:15,560 --> 00:07:17,810
 that time. But if you set a schedule that you're going to

143
00:07:17,810 --> 00:07:19,120
 try every day, every day to

144
00:07:19,120 --> 00:07:23,040
 do to read at least one sutta or something like that. I

145
00:07:23,040 --> 00:07:24,840
 mean, no, I guess the answer is

146
00:07:24,840 --> 00:07:29,160
 no, there isn't in our tradition any structured daily life,

147
00:07:29,160 --> 00:07:31,520
 except the more the better, the

148
00:07:31,520 --> 00:07:34,840
 more dhamma the better. And if you're really concerned,

149
00:07:34,840 --> 00:07:37,680
 well, the answer is to, I'm not

150
00:07:37,680 --> 00:07:43,200
 supposed to say that. The answer is to become a monk. If

151
00:07:43,200 --> 00:07:46,240
 you really want a more Buddhist

152
00:07:46,240 --> 00:07:51,390
 way of living your life, not everyone can become a monk, of

153
00:07:51,390 --> 00:07:52,400
 course. There are people

154
00:07:52,400 --> 00:07:55,470
 who have given this talk before about this man who couldn't

155
00:07:55,470 --> 00:07:56,960
 become a monk, I think, because

156
00:07:56,960 --> 00:08:05,700
 he was looking after his aged parents, Ghatakaarika, who

157
00:08:05,700 --> 00:08:07,440
 made pots. So what did he do? And I've

158
00:08:07,440 --> 00:08:10,970
 told this story many times, go down to the river, get this

159
00:08:10,970 --> 00:08:12,600
 earth that had been dug up

160
00:08:12,600 --> 00:08:16,370
 or had been washed out, find wood that had been knocked

161
00:08:16,370 --> 00:08:18,480
 down and was just useless wood

162
00:08:18,480 --> 00:08:23,420
 and make pots and fire them and sell them by the side of

163
00:08:23,420 --> 00:08:24,960
 the road. And that's how he

164
00:08:24,960 --> 00:08:26,950
 lived his life, just living by, sitting by the side of the

165
00:08:26,950 --> 00:08:28,120
 road, selling these handmade

166
00:08:28,120 --> 00:08:30,710
 pots. And when people asked, how much do you want for that

167
00:08:30,710 --> 00:08:31,800
 pot? He would say, just leave

168
00:08:31,800 --> 00:08:35,230
 what you like. If you want to leave some rice or leave some

169
00:08:35,230 --> 00:08:37,480
 beans, that's enough. Whatever

170
00:08:37,480 --> 00:08:41,170
 you think it's worth. And that's how he lived his life and

171
00:08:41,170 --> 00:08:43,360
 took care of his mother and father.

172
00:08:43,360 --> 00:08:46,790
 He was an anagami, though. So the only reason I bring it up

173
00:08:46,790 --> 00:08:48,760
 is kind of the idea of simplifying

174
00:08:48,760 --> 00:08:58,750
 your life to become more and more like the ideal solitude

175
00:08:58,750 --> 00:09:02,480
 or the state of being in a

176
00:09:02,480 --> 00:09:08,560
 meditation retreat or the life of a meditator.

177
00:09:08,560 --> 00:09:15,900
 I would only add that it might be helpful to find a group.

178
00:09:15,900 --> 00:09:19,360
 If you can't go on retreat regularly,

179
00:09:19,360 --> 00:09:28,030
 which would be the best next to becoming a monk? So and if

180
00:09:28,030 --> 00:09:30,320
 you can't go on retreat very

181
00:09:30,320 --> 00:09:35,170
 often, then it would be good to have a group maybe where

182
00:09:35,170 --> 00:09:37,800
 you can study the Dhamma with

183
00:09:37,800 --> 00:09:44,280
 or discuss the Dhamma with and can meditate together also.

184
00:09:44,280 --> 00:09:45,280
 Thank you.

185
00:09:45,280 --> 00:09:46,280
 Thank you.

