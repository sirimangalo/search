1
00:00:00,000 --> 00:00:04,840
 Good evening, everyone.

2
00:00:04,840 --> 00:00:13,680
 We're broadcasting live.

3
00:00:13,680 --> 00:00:14,680
 Broadcasting live.

4
00:00:14,680 --> 00:00:25,040
 October, November 22nd.

5
00:00:25,040 --> 00:00:32,040
 So I was talking today with one of the meditators.

6
00:00:32,040 --> 00:00:53,520
 We're talking about Sasha, of course.

7
00:00:53,520 --> 00:00:59,520
 We're talking about the Dhammapada.

8
00:00:59,520 --> 00:01:04,240
 And he's making a case for how useful it is

9
00:01:04,240 --> 00:01:07,400
 and how important it is, how beneficial it is,

10
00:01:07,400 --> 00:01:13,040
 how I keep dissing it, keep saying how useless it is,

11
00:01:13,040 --> 00:01:14,840
 how much my other videos are so much better.

12
00:01:14,840 --> 00:01:16,680
 And you should all go listen to my other--

13
00:01:16,680 --> 00:01:23,160
 go watch my other videos, because there's so much more

14
00:01:23,160 --> 00:01:24,680
 useful.

15
00:01:24,680 --> 00:01:26,240
 So he challenged that.

16
00:01:26,240 --> 00:01:29,520
 I understand.

17
00:01:29,520 --> 00:01:36,480
 But I argued back that his idea was

18
00:01:36,480 --> 00:01:42,400
 that it's good encouragement to people who are meditating.

19
00:01:42,400 --> 00:01:45,560
 But my point was, well, what if that's all people ever get?

20
00:01:45,560 --> 00:01:48,440
 When new people come to my channel, that's all they see now

21
00:01:48,440 --> 00:01:48,440
.

22
00:01:48,440 --> 00:01:49,940
 Because those are the latest videos.

23
00:01:49,940 --> 00:01:53,680
 They don't ever go back and watch my old videos,

24
00:01:53,680 --> 00:01:56,560
 get some-- find the other things I've been teaching.

25
00:01:56,560 --> 00:01:58,620
 Or maybe they look at them and say, oh, that's the old

26
00:01:58,620 --> 00:01:59,040
 stuff.

27
00:01:59,040 --> 00:02:01,520
 Let's look at what he's doing now, which I think that's

28
00:02:01,520 --> 00:02:02,360
 reason.

29
00:02:02,360 --> 00:02:04,600
 And this is all they get is the Dhammapada stories.

30
00:02:04,600 --> 00:02:08,360
 It's not really enough.

31
00:02:08,360 --> 00:02:11,480
 I have a suggestion.

32
00:02:11,480 --> 00:02:15,480
 Didn't you start Buddhism 101?

33
00:02:15,480 --> 00:02:16,240
 Yeah, yeah.

34
00:02:16,240 --> 00:02:17,900
 That's actually what I just thought today.

35
00:02:17,900 --> 00:02:19,960
 But I better finish what I should really do

36
00:02:19,960 --> 00:02:23,900
 is continue Buddhism 101, because I stopped that in a lurch

37
00:02:23,900 --> 00:02:23,920
.

38
00:02:23,920 --> 00:02:24,420
 Well, maybe--

39
00:02:24,420 --> 00:02:26,440
 Probably actually got to Buddhism.

40
00:02:26,440 --> 00:02:32,240
 Maybe two Dhammapada and one Buddhism 101 videos in a week?

41
00:02:32,240 --> 00:02:32,760
 Would that be--

42
00:02:32,760 --> 00:02:33,560
 Yeah.

43
00:02:33,560 --> 00:02:35,900
 Yeah, and Buddhism 101 can be quite extended,

44
00:02:35,900 --> 00:02:40,480
 because there's lots of good 101 topics.

45
00:02:40,480 --> 00:02:44,280
 But that may have to wait until January.

46
00:02:44,280 --> 00:02:47,080
 It doesn't have to, but maybe I will wait until January.

47
00:02:48,380 --> 00:02:51,740
 Because I can put more attention on it then.

48
00:02:51,740 --> 00:02:56,420
 I'll just quit school and really focus on teaching.

49
00:02:56,420 --> 00:03:00,140
 I mean, this week I have to write an essay about John Locke

50
00:03:00,140 --> 00:03:02,940
 and free will.

51
00:03:02,940 --> 00:03:04,340
 And Locke is pretty good, I mean,

52
00:03:04,340 --> 00:03:06,460
 as far as being able to understand him.

53
00:03:06,460 --> 00:03:09,260
 I have to write about the people who write about him.

54
00:03:09,260 --> 00:03:10,620
 And the people who write about him

55
00:03:10,620 --> 00:03:12,380
 are really hard to understand.

56
00:03:12,380 --> 00:03:15,220
 It's likely to try to out-intellectualize him or something.

57
00:03:17,200 --> 00:03:22,840
 So Locke, it's a bit challenging to read.

58
00:03:22,840 --> 00:03:25,400
 It's like they feel like they have

59
00:03:25,400 --> 00:03:26,960
 to be more challenging to read.

60
00:03:26,960 --> 00:03:32,920
 It's cool.

61
00:03:32,920 --> 00:03:36,920
 I was working on that last night, starting my essay.

62
00:03:36,920 --> 00:03:38,080
 I'll do next weekend.

63
00:03:42,800 --> 00:03:46,040
 And then exams.

64
00:03:46,040 --> 00:03:49,200
 Then after exams, I'm flying to Florida.

65
00:03:49,200 --> 00:03:52,160
 And then after Florida, I think that's a little more school

66
00:03:52,160 --> 00:03:52,660
 for me.

67
00:03:52,660 --> 00:03:56,720
 It's not worth it.

68
00:03:56,720 --> 00:04:00,080
 Not with the change, not with all this new work,

69
00:04:00,080 --> 00:04:02,600
 new teaching stuff.

70
00:04:02,600 --> 00:04:07,600
 And then we can add more hours to the appointments,

71
00:04:07,600 --> 00:04:11,040
 add some more slots.

72
00:04:11,040 --> 00:04:14,640
 It might be complicated, because then the only issue

73
00:04:14,640 --> 00:04:17,560
 with the slots right now is remembering who has told me

74
00:04:17,560 --> 00:04:18,840
 they can't make it on a week.

75
00:04:18,840 --> 00:04:22,480
 So that if they haven't told me, then I cancel them.

76
00:04:22,480 --> 00:04:26,600
 And then we don't-- and then they lose their slot.

77
00:04:26,600 --> 00:04:30,160
 But if they have told me, then I keep them.

78
00:04:30,160 --> 00:04:32,300
 And now I'm afraid to cancel, because I can't remember

79
00:04:32,300 --> 00:04:33,840
 if they told me or not.

80
00:04:33,840 --> 00:04:35,360
 And sometimes it seems like there

81
00:04:35,360 --> 00:04:36,820
 is technical difficulties.

82
00:04:36,820 --> 00:04:38,920
 And a couple of people have said they didn't get the button

83
00:04:38,920 --> 00:04:39,440
 to pop up.

84
00:04:39,440 --> 00:04:42,440
 The other thing is-- well, the button hasn't been a problem

85
00:04:42,440 --> 00:04:45,080
 lately, I don't think.

86
00:04:45,080 --> 00:04:46,240
 People might--

87
00:04:46,240 --> 00:04:48,000
 Yeah, Samantha just had a problem with it

88
00:04:48,000 --> 00:04:50,120
 this past weekend.

89
00:04:50,120 --> 00:04:53,680
 She was waiting, and it didn't pop up for her.

90
00:04:53,680 --> 00:04:55,360
 Oh.

91
00:04:55,360 --> 00:04:56,280
 I didn't know that.

92
00:04:56,280 --> 00:05:05,240
 Well, the bigger problem has been people calling me.

93
00:05:05,240 --> 00:05:08,200
 Like, just know someone called me, I didn't get a call.

94
00:05:08,200 --> 00:05:09,960
 I don't think.

95
00:05:09,960 --> 00:05:14,060
 Actually, it may have been out of the room when they called

96
00:05:14,060 --> 00:05:14,120
,

97
00:05:14,120 --> 00:05:16,580
 but it didn't notify me that someone was trying to call me

98
00:05:16,580 --> 00:05:19,160
 or had tried to call me.

99
00:05:19,160 --> 00:05:19,960
 It's really erratic.

100
00:05:19,960 --> 00:05:28,440
 That's happened quite a few times.

101
00:05:28,440 --> 00:05:37,280
 It's nice to know that the computers are reaffirming

102
00:05:37,280 --> 00:05:38,600
 the three characteristics.

103
00:05:38,600 --> 00:05:45,840
 If it was always stable, then we'd think, oh, yes.

104
00:05:45,840 --> 00:05:48,440
 What is this nonsense about impermanence,

105
00:05:48,440 --> 00:05:50,000
 suffering, and non-self?

106
00:05:50,000 --> 00:05:52,240
 Here we have something that's permanent that's stable,

107
00:05:52,240 --> 00:05:55,360
 satisfying, and controllable.

108
00:05:55,360 --> 00:05:57,560
 Well, we know that's not the case.

109
00:05:57,560 --> 00:05:58,560
 So good for us.

110
00:05:58,560 --> 00:06:00,120
 Well done, Google.

111
00:06:00,120 --> 00:06:02,120
 That's right.

112
00:06:02,120 --> 00:06:05,360
 Teaching us Buddhism.

113
00:06:05,360 --> 00:06:07,480
 David, good job.

114
00:06:07,480 --> 00:06:09,600
 One day, if someone is due for an appointment

115
00:06:09,600 --> 00:06:12,840
 but the button doesn't pop up, can they just create a--

116
00:06:12,840 --> 00:06:14,480
 just invite you to a regular hangout?

117
00:06:14,480 --> 00:06:15,960
 Would it be any different?

118
00:06:15,960 --> 00:06:17,520
 No, that's exactly what it does.

119
00:06:17,520 --> 00:06:19,080
 The button just creates a hangout

120
00:06:19,080 --> 00:06:20,720
 and tells you to invite me.

121
00:06:20,720 --> 00:06:21,880
 And then you click on Invite.

122
00:06:21,880 --> 00:06:23,560
 You have to actually still invite me.

123
00:06:23,560 --> 00:06:25,360
 It just gives you my email address.

124
00:06:25,360 --> 00:06:26,360
 You just click Invite.

125
00:06:26,360 --> 00:06:30,640
 So rather than miss an appointment,

126
00:06:30,640 --> 00:06:34,520
 someone could just start up a hangout and invite you.

127
00:06:34,520 --> 00:06:35,480
 It's good to know.

128
00:06:35,480 --> 00:06:44,720
 If we had two more people-- if we had probably four more

129
00:06:44,720 --> 00:06:47,120
 slots every day, we'll see if I can handle that.

130
00:06:47,120 --> 00:06:48,920
 Should be able to.

131
00:06:48,920 --> 00:06:51,600
 Maybe not, because some days there are other things going

132
00:06:51,600 --> 00:06:52,800
 on.

133
00:06:52,800 --> 00:06:53,440
 It's interesting.

134
00:06:53,440 --> 00:06:55,440
 Can I really add more slots every day?

135
00:06:55,440 --> 00:06:56,520
 Maybe Monday to Friday.

136
00:06:56,520 --> 00:07:00,400
 Maybe I'll keep the weekends more open.

137
00:07:00,400 --> 00:07:06,380
 And could you add another tab to the meditation website

138
00:07:06,380 --> 00:07:07,000
 here?

139
00:07:07,000 --> 00:07:09,880
 Sort of a message board for--

140
00:07:09,880 --> 00:07:12,160
 you're saying you're not sure when people tell you

141
00:07:12,160 --> 00:07:13,160
 that they can't make it.

142
00:07:13,160 --> 00:07:21,720
 For example, similar to the chat, but if somebody

143
00:07:21,720 --> 00:07:24,800
 put their message in chat, it would get lost.

144
00:07:24,800 --> 00:07:28,080
 But if you had a dedicated panel or a message

145
00:07:28,080 --> 00:07:30,160
 is strictly related to the meditation meetings,

146
00:07:30,160 --> 00:07:34,040
 that might help to keep things organized.

147
00:07:34,040 --> 00:07:35,640
 I'm just creating more work for you.

148
00:07:35,640 --> 00:07:42,560
 But it might help to keep things more organized.

149
00:07:42,560 --> 00:08:12,040
 [AUDIO OUT]

150
00:08:12,040 --> 00:08:15,040
 Maybe I could add a notes section to everyone's profile

151
00:08:15,040 --> 00:08:19,840
 that they can add notes that I could see.

152
00:08:19,840 --> 00:08:21,800
 When I click on their profile, then I'd say,

153
00:08:21,800 --> 00:08:25,720
 oh, I'm not going to be here this week, this day.

154
00:08:25,720 --> 00:08:28,000
 I'll tell them to put the note on their profile page,

155
00:08:28,000 --> 00:08:29,840
 because that's where I go right away to see,

156
00:08:29,840 --> 00:08:31,240
 is this person meditating?

157
00:08:31,240 --> 00:08:34,640
 Why isn't this person calling me?

158
00:08:34,640 --> 00:08:35,480
 That's a good--

159
00:08:35,480 --> 00:08:36,760
 Then at least tell them.

160
00:08:36,760 --> 00:08:39,480
 Especially with the holidays coming up, people may be away.

161
00:08:39,480 --> 00:08:58,280
 [AUDIO OUT]

162
00:08:58,280 --> 00:08:59,720
 What do we have for Dhamma today?

163
00:08:59,720 --> 00:09:03,760
 A good quote?

164
00:09:07,720 --> 00:09:10,080
 Yeah.

165
00:09:10,080 --> 00:09:11,440
 Let's not go there.

166
00:09:11,440 --> 00:09:12,840
 Do we have any questions?

167
00:09:12,840 --> 00:09:14,120
 We have questions.

168
00:09:14,120 --> 00:09:15,120
 All right, let's go there.

169
00:09:15,120 --> 00:09:20,640
 This is a nice, cute question.

170
00:09:20,640 --> 00:09:23,200
 Venerable sir, is it better to have a shorter teaching

171
00:09:23,200 --> 00:09:23,560
 video

172
00:09:23,560 --> 00:09:25,640
 to appease a larger group of viewers,

173
00:09:25,640 --> 00:09:28,560
 or better to have a longer teaching video for the fewer

174
00:09:28,560 --> 00:09:31,280
 viewers that care about each side story and subtlety

175
00:09:31,280 --> 00:09:33,560
 and carefully consider your every movement

176
00:09:33,560 --> 00:09:35,560
 while conveying the sattama?

177
00:09:35,560 --> 00:09:37,680
 Dharma, thank you.

178
00:09:37,680 --> 00:09:47,360
 [AUDIO OUT]

179
00:09:47,360 --> 00:09:49,960
 Is that a trick question?

180
00:09:49,960 --> 00:09:53,480
 I think it's definitely related to the comment

181
00:09:53,480 --> 00:09:56,280
 that you relayed that someone said your last Dhammapada

182
00:09:56,280 --> 00:09:59,560
 was too long.

183
00:09:59,560 --> 00:10:00,360
 Yeah, weird, huh?

184
00:10:06,280 --> 00:10:08,960
 We say in Thai, [NON-ENGLISH]

185
00:10:08,960 --> 00:10:11,800
 It's actually Pali, but in Thailand, they say it.

186
00:10:11,800 --> 00:10:12,760
 [NON-ENGLISH]

187
00:10:12,760 --> 00:10:16,760
 Means mind is--

188
00:10:16,760 --> 00:10:24,320
 I guess it means mind is manifold or is various.

189
00:10:24,320 --> 00:10:29,960
 Mind varies to each their own, basically.

190
00:10:29,960 --> 00:10:31,400
 Everyone has a different opinion.

191
00:10:31,400 --> 00:10:35,560
 There are many, many different opinions.

192
00:10:35,560 --> 00:10:36,840
 [NON-ENGLISH]

193
00:10:36,840 --> 00:10:37,560
 It's a weird word.

194
00:10:37,560 --> 00:10:39,840
 I think they're corrupting the Pali actually.

195
00:10:39,840 --> 00:10:44,680
 [NON-ENGLISH]

196
00:10:44,680 --> 00:10:45,240
 Probably.

197
00:10:45,240 --> 00:11:11,240
 [AUDIO OUT]

198
00:11:11,240 --> 00:11:12,560
 Next question?

199
00:11:12,560 --> 00:11:13,520
 Yeah.

200
00:11:13,520 --> 00:11:16,120
 Why did I answer that one already?

201
00:11:16,120 --> 00:11:17,400
 I don't think so.

202
00:11:17,400 --> 00:11:19,560
 What was it about?

203
00:11:19,560 --> 00:11:22,320
 The question is, is it better to have shorter teachings

204
00:11:22,320 --> 00:11:23,480
 that

205
00:11:23,480 --> 00:11:27,280
 appeal to a larger group or longer teachings that

206
00:11:27,280 --> 00:11:29,760
 appeal to your more dedicated followers?

207
00:11:29,760 --> 00:11:40,160
 Well, it was interesting talking last night.

208
00:11:40,160 --> 00:11:44,080
 I brought up the idea that a doctor isn't worried about

209
00:11:44,080 --> 00:11:47,160
 people who are already on the mend.

210
00:11:47,160 --> 00:11:48,920
 They only concern themselves with people

211
00:11:48,920 --> 00:11:52,640
 who will die without their care.

212
00:11:52,640 --> 00:11:56,240
 So there is that.

213
00:11:56,240 --> 00:12:00,160
 To some extent, we need to reach those people who

214
00:12:00,160 --> 00:12:03,760
 are not easily impressed.

215
00:12:03,760 --> 00:12:05,760
 Reach those people who are not already hooked.

216
00:12:05,760 --> 00:12:11,560
 [AUDIO OUT]

217
00:12:11,560 --> 00:12:13,800
 But that's not entirely fair, because to some extent,

218
00:12:13,800 --> 00:12:16,920
 we have to help those people who are actually

219
00:12:16,920 --> 00:12:18,840
 going to take the medicine.

220
00:12:18,840 --> 00:12:20,800
 You don't want to pander to people who aren't even

221
00:12:20,800 --> 00:12:23,440
 going to take the medicine.

222
00:12:23,440 --> 00:12:26,000
 Why waste all your time teaching people who aren't even

223
00:12:26,000 --> 00:12:28,240
 going to practice?

224
00:12:28,240 --> 00:12:29,000
 That's a good point.

225
00:12:33,840 --> 00:12:36,160
 Thank you, Monte.

226
00:12:36,160 --> 00:12:38,760
 I'm trying to find a good way to respond to cravings when

227
00:12:38,760 --> 00:12:38,960
 not

228
00:12:38,960 --> 00:12:40,280
 meditating.

229
00:12:40,280 --> 00:12:42,000
 Right now, it goes like this.

230
00:12:42,000 --> 00:12:44,840
 There is a sight or a thought that starts a craving.

231
00:12:44,840 --> 00:12:47,040
 The mind locks onto that sight or thought.

232
00:12:47,040 --> 00:12:48,920
 There is an unpleasant feeling.

233
00:12:48,920 --> 00:12:50,920
 I remember to be mindful.

234
00:12:50,920 --> 00:12:53,130
 There is an awareness that there is an aversion to the

235
00:12:53,130 --> 00:12:53,840
 craving.

236
00:12:53,840 --> 00:12:58,650
 I say, disliking, disliking, or averting, averting, or

237
00:12:58,650 --> 00:12:59,720
 something.

238
00:12:59,720 --> 00:13:01,720
 This goes on for as short as 30 seconds

239
00:13:01,720 --> 00:13:04,320
 or sometimes for 10 minutes or longer.

240
00:13:04,320 --> 00:13:07,710
 Rather than coming to an end, usually another sight or

241
00:13:07,710 --> 00:13:07,880
 thought

242
00:13:07,880 --> 00:13:09,480
 comes along, and the mind follows going

243
00:13:09,480 --> 00:13:11,280
 in a different direction.

244
00:13:11,280 --> 00:13:12,880
 You have often talked about a middle way

245
00:13:12,880 --> 00:13:14,720
 for responding to cravings.

246
00:13:14,720 --> 00:13:15,720
 Could you elaborate?

247
00:13:29,320 --> 00:13:34,760
 I don't quite understand what the question is.

248
00:13:34,760 --> 00:13:38,640
 Like, as though you have-- as though something's wrong

249
00:13:38,640 --> 00:13:41,440
 with what you're describing.

250
00:13:41,440 --> 00:13:46,240
 I mean, something is wrong, but you know what's wrong.

251
00:13:46,240 --> 00:13:49,040
 What's wrong is reality is wrong.

252
00:13:49,040 --> 00:13:50,960
 It's impermanent suffering in oneself,

253
00:13:50,960 --> 00:13:52,920
 and that's what you're experiencing.

254
00:13:52,920 --> 00:13:58,480
 The chaos, the unpleasantness, the unwieldiness,

255
00:13:58,480 --> 00:14:01,600
 the not how I wanted, wanted-ness.

256
00:14:01,600 --> 00:14:07,440
 Is it how you want it?

257
00:14:07,440 --> 00:14:08,560
 Is it how you'd like it?

258
00:14:08,560 --> 00:14:11,160
 No, that's it.

259
00:14:11,160 --> 00:14:12,320
 That's reality.

260
00:14:12,320 --> 00:14:12,820
 Why?

261
00:14:12,820 --> 00:14:17,560
 Because that will allow you to let go.

262
00:14:17,560 --> 00:14:22,460
 That will lead you to want to let go, not want to cling

263
00:14:22,460 --> 00:14:24,040
 anymore.

264
00:14:24,040 --> 00:14:24,840
 You're doing fine.

265
00:14:24,840 --> 00:14:32,040
 [AUDIO OUT]

266
00:14:32,040 --> 00:14:34,880
 I would recommend going back as often as possible

267
00:14:34,880 --> 00:14:36,000
 to the rising and falling.

268
00:14:36,000 --> 00:14:38,120
 It keeps you grounded.

269
00:14:38,120 --> 00:14:39,560
 Don't jump from one thing to another

270
00:14:39,560 --> 00:14:41,120
 unless you're immediately pulled.

271
00:14:41,120 --> 00:14:44,720
 If you're not pulled, then go back to the rising.

272
00:14:44,720 --> 00:14:47,040
 And then acknowledge the next thing.

273
00:14:47,040 --> 00:14:49,280
 But try to always come back to the rising and falling

274
00:14:49,280 --> 00:14:53,120
 when you can, after noting whatever you have noted,

275
00:14:53,120 --> 00:14:56,320
 whatever you have experienced.

276
00:14:56,320 --> 00:15:03,760
 So the last part of the question was,

277
00:15:03,760 --> 00:15:06,210
 you have talked about a middle way for responding to c

278
00:15:06,210 --> 00:15:07,720
ravings.

279
00:15:07,720 --> 00:15:10,720
 That's a little bit like the question last night about,

280
00:15:10,720 --> 00:15:12,000
 if you have to do something, should you

281
00:15:12,000 --> 00:15:13,680
 try to do the wholesome version of it?

282
00:15:13,680 --> 00:15:18,360
 Last night, there was a similar--

283
00:15:18,360 --> 00:15:20,680
 or maybe the night before, there was a similar question

284
00:15:20,680 --> 00:15:22,640
 about if you have to give in to something,

285
00:15:22,640 --> 00:15:26,840
 should you give in to the most wholesome version of it

286
00:15:26,840 --> 00:15:28,320
 or something similar like that?

287
00:15:28,320 --> 00:15:30,080
 [INAUDIBLE]

288
00:15:30,080 --> 00:15:32,720
 They were saying, remember to be mindful.

289
00:15:32,720 --> 00:15:35,120
 They know everything is fine.

290
00:15:35,120 --> 00:15:36,840
 That is the middle way.

291
00:15:36,840 --> 00:15:38,480
 That is the middle way I talk about.

292
00:15:38,480 --> 00:15:43,640
 So I don't think--

293
00:15:43,640 --> 00:15:45,480
 I think there may be-- yeah, I probably

294
00:15:45,480 --> 00:15:50,600
 be taking what I said last night and somebody making more

295
00:15:50,600 --> 00:15:51,040
 of it.

296
00:15:51,040 --> 00:15:54,440
 And the whole point is to be mindful.

297
00:15:54,440 --> 00:15:58,040
 If you can't, if you find yourself giving in,

298
00:15:58,040 --> 00:16:00,000
 well, then you give in.

299
00:16:00,000 --> 00:16:02,480
 It's not the middle way.

300
00:16:02,480 --> 00:16:04,920
 But it happens.

301
00:16:04,920 --> 00:16:09,400
 And then you pick yourself up and you try again.

302
00:16:09,400 --> 00:16:10,040
 We're learning.

303
00:16:10,040 --> 00:16:13,120
 We're trying to learn.

304
00:16:13,120 --> 00:16:15,120
 Trying to study, trying to understand.

305
00:16:19,920 --> 00:16:22,360
 Dear Bande, I have been meditating for a while,

306
00:16:22,360 --> 00:16:25,200
 and I feel like it's making me unenthusiastic about things

307
00:16:25,200 --> 00:16:27,840
 I formerly like to do, such as working out,

308
00:16:27,840 --> 00:16:30,000
 watching television, et cetera.

309
00:16:30,000 --> 00:16:30,800
 I want to be clear.

310
00:16:30,800 --> 00:16:32,880
 I'm not depressed, but I feel fine sitting around

311
00:16:32,880 --> 00:16:34,480
 all day doing nothing.

312
00:16:34,480 --> 00:16:35,360
 Is this natural?

313
00:16:35,360 --> 00:16:35,860
 Thanks.

314
00:16:35,860 --> 00:16:44,600
 Rape is natural.

315
00:16:44,600 --> 00:16:46,200
 Murder is natural.

316
00:16:46,200 --> 00:16:49,520
 Theft is natural.

317
00:16:49,520 --> 00:16:52,600
 Excruciating and suffering is natural.

318
00:16:52,600 --> 00:16:54,880
 Terrible evil is natural.

319
00:16:54,880 --> 00:16:56,360
 Craving is natural.

320
00:16:56,360 --> 00:16:57,600
 Addiction is natural.

321
00:16:57,600 --> 00:17:02,240
 So what do you mean by this question?

322
00:17:02,240 --> 00:17:02,840
 Is it natural?

323
00:17:02,840 --> 00:17:11,640
 Answering a question with a question.

324
00:17:11,640 --> 00:17:15,000
 And some elaboration.

325
00:17:15,000 --> 00:17:17,440
 Sometimes you have to-- when someone asks you a question,

326
00:17:17,440 --> 00:17:23,480
 you have to take a part.

327
00:17:23,480 --> 00:17:24,760
 You can't answer it directly.

328
00:17:24,760 --> 00:17:29,540
 And sometimes you have to ask a question, a counter

329
00:17:29,540 --> 00:17:30,040
 question.

330
00:17:30,040 --> 00:17:35,680
 This is one of those cases.

331
00:17:35,680 --> 00:17:45,760
 [AUDIO OUT]

332
00:17:45,760 --> 00:17:49,640
 And to follow up on the earlier question about cravings,

333
00:17:49,640 --> 00:17:51,400
 I can't follow the craving to the end.

334
00:17:51,400 --> 00:17:52,280
 Is that a problem?

335
00:17:52,280 --> 00:17:58,040
 The end of what?

336
00:17:58,040 --> 00:17:59,000
 The end of the craving.

337
00:17:59,000 --> 00:18:05,520
 Can't stick it out until the craving goes away.

338
00:18:05,520 --> 00:18:09,440
 The craving lasts actually 10 minutes?

339
00:18:09,440 --> 00:18:10,800
 Apparently, yes.

340
00:18:10,800 --> 00:18:13,200
 I don't think so.

341
00:18:13,200 --> 00:18:16,920
 You say to yourself, wanting, wanting.

342
00:18:16,920 --> 00:18:19,080
 It should go away for a moment, and then you

343
00:18:19,080 --> 00:18:20,580
 come back to the rising and falling.

344
00:18:20,580 --> 00:18:23,760
 If it comes back, then you go back to it.

345
00:18:23,760 --> 00:18:26,090
 But when it goes away, you come back to the rising and

346
00:18:26,090 --> 00:18:27,480
 falling.

347
00:18:27,480 --> 00:18:28,680
 Nothing lasts for 10 minutes.

348
00:18:33,480 --> 00:18:36,160
 If it does, then just say--

349
00:18:36,160 --> 00:18:37,800
 no, stick it out for a while, and then

350
00:18:37,800 --> 00:18:38,880
 ignore it after a while.

351
00:18:38,880 --> 00:18:47,080
 Monte, how can I make progress if I can't

352
00:18:47,080 --> 00:18:48,840
 meet with you to do a course?

353
00:18:48,840 --> 00:18:50,200
 Is there something more I can read?

354
00:18:50,200 --> 00:18:57,600
 Well, if you are meditating, which looks like you are,

355
00:18:57,600 --> 00:18:58,840
 good, green question mark.

356
00:18:58,840 --> 00:19:01,320
 [AUDIO OUT]

357
00:19:01,320 --> 00:19:12,520
 You could read the second book on how to meditate.

358
00:19:12,520 --> 00:19:14,840
 It's not quite finished, but mostly finished.

359
00:19:14,840 --> 00:19:18,560
 For most intents and purposes, it's finished.

360
00:19:18,560 --> 00:19:26,560
 That would be a good thing maybe to read.

361
00:19:26,560 --> 00:19:28,880
 [AUDIO OUT]

362
00:19:28,880 --> 00:19:35,040
 We're all filled up on the meetings, right?

363
00:19:35,040 --> 00:19:36,240
 Is there one empty spot?

364
00:19:36,240 --> 00:19:49,340
 Tuesday at 2330 UTC time, which looks like 6.30 PM Eastern

365
00:19:49,340 --> 00:19:49,840
 time.

366
00:19:49,840 --> 00:19:52,200
 [AUDIO OUT]

367
00:19:52,200 --> 00:19:57,120
 Grab it before it's gone.

368
00:19:57,120 --> 00:19:59,600
 [AUDIO OUT]

369
00:19:59,600 --> 00:20:10,120
 Just looking for the link for the how to meditate part two.

370
00:20:10,120 --> 00:20:12,600
 [AUDIO OUT]

371
00:20:12,600 --> 00:20:15,080
 [AUDIO OUT]

372
00:20:40,080 --> 00:20:42,560
 And with that, we're all caught up on questions.

373
00:20:42,560 --> 00:20:49,360
 Tomorrow we have a med mob.

374
00:20:49,360 --> 00:20:52,400
 I'm going to try a med mob, or I'm going to.

375
00:20:52,400 --> 00:20:54,520
 Maybe I'll be there alone.

376
00:20:54,520 --> 00:21:00,640
 I'll do meditation in public and see how it goes.

377
00:21:00,640 --> 00:21:02,680
 Do you have any signs?

378
00:21:02,680 --> 00:21:04,680
 No.

379
00:21:04,680 --> 00:21:06,240
 Put a little sandwich board out there

380
00:21:06,240 --> 00:21:07,640
 to explain what you're doing.

381
00:21:09,640 --> 00:21:13,880
 And put a little hat.

382
00:21:13,880 --> 00:21:15,360
 No signs.

383
00:21:15,360 --> 00:21:24,200
 No, that's the whole thing about a flash mob,

384
00:21:24,200 --> 00:21:28,720
 is you don't really even tell people what you're doing.

385
00:21:28,720 --> 00:21:29,720
 They just figure it out.

386
00:21:29,720 --> 00:21:32,200
 [AUDIO OUT]

387
00:21:32,200 --> 00:21:49,360
 OK, so that's all for tonight.

388
00:21:49,360 --> 00:21:55,520
 Thank you all for practicing with us and for tuning in.

389
00:21:55,520 --> 00:21:58,120
 Thanks, Robin, for joining me.

390
00:21:58,120 --> 00:21:59,920
 Thank you, Bonté.

391
00:21:59,920 --> 00:22:01,960
 Have a good night.

392
00:22:01,960 --> 00:22:04,440
 [AUDIO OUT]

393
00:22:04,440 --> 00:22:06,920
 [AUDIO OUT]

394
00:22:06,920 --> 00:22:09,400
 [AUDIO OUT]

