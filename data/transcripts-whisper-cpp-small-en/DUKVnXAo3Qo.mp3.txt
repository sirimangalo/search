 Here we have something unique. These men are prisoners,
 convict, and they've been engaged
 to come to help us for a day. So they're bringing sand and
 rocks and this man is offered to
 cut down this tree for me. It's already dead, I think. He's
 just going to get it out of
 the way. So this is a very welcome surprise. 20 men coming
 to do their groundwork. This
 is what has made building here really difficult. This is
 sand and rocks and cement all the
 way down here. So this will make it a lot easier to build.
 Now we have the sand just
 coming soon. Next they'll bring the rocks and today the
 engineer is coming. I said he
 was coming yesterday but it changed. So here's where we're
 going to build two cuties. I think
 the idea now is to build two rooms here, two rooms together
. Because it's a little bit
 cheaper to make two rooms joined with one wall joining them
. And people feel safer that
 way knowing that it's someone else. So we have two here and
 this one we've demolished.
 I demolished, pushed. I'm good at demolishing. Building is
 not so proficient. I think we'll
 leave this for now. This one's going to be more difficult
 because we have to join it
 with the cave wall. But eventually we'll build a third cut
ie here. So another update.
 [gunshot]
 [
