1
00:00:00,000 --> 00:00:04,430
 Hello and welcome back to our study of the Dhamtada. Today

2
00:00:04,430 --> 00:00:10,990
 we continue on with verse number 126, which reads as

3
00:00:10,990 --> 00:00:12,000
 follows.

4
00:00:12,000 --> 00:00:22,990
 Gabham meke upadjanthi nirayang papakam mino, sagang sug

5
00:00:22,990 --> 00:00:26,000
atinoyanti parinim banti anasova.

6
00:00:26,000 --> 00:00:34,670
 Which means some arise in a womb, the evil doers, papakam

7
00:00:34,670 --> 00:00:41,000
 me, papakam me, the evil doers, nirayang go to hell.

8
00:00:41,000 --> 00:00:47,050
 Sagang sugatinoyanti, the people with good destinations,

9
00:00:47,050 --> 00:00:52,800
 people who have gone to good, go to heaven. Sagang parinim

10
00:00:52,800 --> 00:00:54,000
 banti anasova.

11
00:00:54,000 --> 00:00:59,570
 Those without any taints are released, go to full release

12
00:00:59,570 --> 00:01:02,000
 parinim banti.

13
00:01:02,000 --> 00:01:10,380
 Four different destinations for four different types of

14
00:01:10,380 --> 00:01:12,000
 people. The story behind this is apropos.

15
00:01:12,000 --> 00:01:16,970
 It's one of the more poignant stories in the Dhammapada.

16
00:01:16,970 --> 00:01:25,000
 Not long but has some quite interesting ramifications.

17
00:01:25,000 --> 00:01:29,320
 So the Buddha was dwelling in Jaitavana and there was a

18
00:01:29,320 --> 00:01:33,780
 monk Tisa, which is the name of a lot of monks, so it seems

19
00:01:33,780 --> 00:01:36,000
 to have been a common name at the time.

20
00:01:36,000 --> 00:01:46,470
 But he was an arahant, it seems. And for twelve years he

21
00:01:46,470 --> 00:01:56,000
 had taken his meals in the house of a certain jeweler.

22
00:01:56,000 --> 00:01:59,500
 And they had treated him well, like mother and father the

23
00:01:59,500 --> 00:02:03,330
 text says. They were good people, they weren't enlightened

24
00:02:03,330 --> 00:02:07,300
 people, but they were good people who were very kind and

25
00:02:07,300 --> 00:02:09,000
 very devoted to him.

26
00:02:09,000 --> 00:02:15,700
 But it so happened that one day that the king, King Pasen

27
00:02:15,700 --> 00:02:23,000
adeen, had sent a jewel worth a lot of money to the jeweler,

28
00:02:23,000 --> 00:02:30,180
 telling him it should be cut and set in some manner to do

29
00:02:30,180 --> 00:02:33,570
 something with it, whatever they do with jewels, with

30
00:02:33,570 --> 00:02:36,000
 precious stones.

31
00:02:36,000 --> 00:02:41,320
 And when the messenger brought it, the jeweler was busy

32
00:02:41,320 --> 00:02:47,000
 cutting up meat, and so his hands were covered in blood.

33
00:02:47,000 --> 00:02:52,520
 But even though he picked up the jewel, and he put it in a

34
00:02:52,520 --> 00:02:58,370
 jewel box, in this box sitting on his desk, with his hands

35
00:02:58,370 --> 00:03:04,000
 covered in blood, so it had this sort of blood on it.

36
00:03:04,000 --> 00:03:08,000
 Or at least it had the smell of blood on it.

37
00:03:08,000 --> 00:03:13,680
 And so then the elder came in for alms, and the jeweler

38
00:03:13,680 --> 00:03:19,000
 went into the back to prepare food for the monk.

39
00:03:19,000 --> 00:03:23,320
 And it so happened that there was a heron, the jeweler had

40
00:03:23,320 --> 00:03:27,000
 a pet bird, a heron, that lived in the house.

41
00:03:27,000 --> 00:03:31,140
 And so happens that the heron wandered over to the desk, sm

42
00:03:31,140 --> 00:03:35,340
elt the blood, and saw this jewel, which was maybe a ruby or

43
00:03:35,340 --> 00:03:39,000
 something, and mistook it for a piece of meat.

44
00:03:39,000 --> 00:03:43,000
 And right in front of the elder ate the jewel.

45
00:03:43,000 --> 00:03:49,280
 And the elder watched as this bird popped its jewel into

46
00:03:49,280 --> 00:03:56,000
 its mouth, swallowed it whole, and then wandered away.

47
00:03:56,000 --> 00:03:59,010
 Now the jeweler came back, and this is where it gets

48
00:03:59,010 --> 00:04:03,970
 interesting, because the jeweler came back and found that

49
00:04:03,970 --> 00:04:10,960
 his jewel was missing, and started freaking out, you know,

50
00:04:10,960 --> 00:04:13,970
 and looked at the elder, but didn't want to believe, of

51
00:04:13,970 --> 00:04:15,000
 course, that the elder took it.

52
00:04:15,000 --> 00:04:20,990
 And so he went to his wife and to his son and asked them, "

53
00:04:20,990 --> 00:04:25,000
Did you take this jewel? What happened to it?"

54
00:04:25,000 --> 00:04:29,000
 And they didn't take it. And so of course he trusts them.

55
00:04:29,000 --> 00:04:33,050
 But then he says to his wife, "It must have been the monk.

56
00:04:33,050 --> 00:04:37,170
 The monk must have seen it, and I can't believe it, but it

57
00:04:37,170 --> 00:04:39,000
 must have been him."

58
00:04:39,000 --> 00:04:42,480
 And his wife scolded him and said, "How could you? Don't

59
00:04:42,480 --> 00:04:45,790
 say that. All these years he's come to see us, and he's

60
00:04:45,790 --> 00:04:50,930
 never exhibited a single flaw, let alone the evil desire

61
00:04:50,930 --> 00:04:58,000
 that would cause him to steal someone else's belongings."

62
00:04:58,000 --> 00:05:00,570
 And so the jeweler accepted this and went to ask the monk

63
00:05:00,570 --> 00:05:03,000
 and said to the monk, "Did you take the jewel?"

64
00:05:03,000 --> 00:05:08,130
 Now, you might wonder, "Well, why didn't he just say that

65
00:05:08,130 --> 00:05:11,900
 the heron took it? Why wouldn't the elder just solve the

66
00:05:11,900 --> 00:05:13,000
 problem for him?"

67
00:05:13,000 --> 00:05:16,580
 Because you see, the heron has swallowed the jewel, and the

68
00:05:16,580 --> 00:05:19,540
 only way to get it out, or the most reasonable way to get

69
00:05:19,540 --> 00:05:22,000
 it out, would be to kill the heron.

70
00:05:22,000 --> 00:05:26,340
 And so in order to protect the heron, he didn't tell the...

71
00:05:26,340 --> 00:05:30,200
 That was his reason for just saying, "No, I didn't take it

72
00:05:30,200 --> 00:05:32,000
," and keeping quiet.

73
00:05:32,000 --> 00:05:35,880
 He didn't want to get involved because he knew the

74
00:05:35,880 --> 00:05:40,000
 consequences of getting involved for the heron.

75
00:05:40,000 --> 00:05:42,590
 And so he says to him, "Look, there's no one else here. My

76
00:05:42,590 --> 00:05:45,190
 wife and sons, they say they haven't taken, so it must have

77
00:05:45,190 --> 00:05:47,000
 been you. There's no one else."

78
00:05:47,000 --> 00:05:51,240
 And the elder just stayed quiet. And so he went to his wife

79
00:05:51,240 --> 00:05:54,710
 and he said, "It's got to have been the elder. I'm going to

80
00:05:54,710 --> 00:05:57,000
 torture him and get him to tell me."

81
00:05:57,000 --> 00:05:59,140
 Because he's freaking out, really. It's this whole deal

82
00:05:59,140 --> 00:06:04,330
 with kings. The king is not to be trifled with, and of

83
00:06:04,330 --> 00:06:07,000
 course he's not going to buy that he lost it.

84
00:06:07,000 --> 00:06:12,400
 That doesn't really work. So he knows that it's going to be

85
00:06:12,400 --> 00:06:16,970
 on his head if the king finds out that he has lost the

86
00:06:16,970 --> 00:06:18,000
 jewel.

87
00:06:18,000 --> 00:06:19,870
 And he says, "So I'm going to torture him." And the wife

88
00:06:19,870 --> 00:06:23,000
 forbids him and says, "Don't ruin us."

89
00:06:23,000 --> 00:06:26,870
 She says, "It's far better for us to become slaves than to

90
00:06:26,870 --> 00:06:30,000
 lay such a charge at the door of the elder."

91
00:06:30,000 --> 00:06:34,380
 And the jeweler says, "We're all of us to become slaves. We

92
00:06:34,380 --> 00:06:38,640
 would still not be worth the value of the jewel. The jewel

93
00:06:38,640 --> 00:06:41,000
 is worth more than our slavery."

94
00:06:41,000 --> 00:06:47,000
 So speaking of slaves, that jewel, we're not just going to

95
00:06:47,000 --> 00:06:52,000
 become slaves. We'll probably be executed.

96
00:06:52,000 --> 00:06:54,880
 And so he does something curious I don't quite understand.

97
00:06:54,880 --> 00:06:57,470
 He puts a rope around the elder's head, binds it really

98
00:06:57,470 --> 00:07:02,440
 tight, and then starts hitting him on the head with a stick

99
00:07:02,440 --> 00:07:04,000
 or something.

100
00:07:04,000 --> 00:07:07,150
 "Blood streamed from the elder's head and ears and nostrils

101
00:07:07,150 --> 00:07:10,170
, and his eyes looked as though they would pop out of their

102
00:07:10,170 --> 00:07:11,000
 sockets."

103
00:07:11,000 --> 00:07:13,000
 He really got a beating.

104
00:07:13,000 --> 00:07:21,000
 And he never once said that the heron ate the darn thing.

105
00:07:21,000 --> 00:07:24,770
 And so he, overwhelmed with pain and blood coming out of

106
00:07:24,770 --> 00:07:28,370
 his orifice as he fell down on the ground as he was being

107
00:07:28,370 --> 00:07:29,000
 beaten.

108
00:07:29,000 --> 00:07:34,400
 And the heron, smelling the blood, came over and started to

109
00:07:34,400 --> 00:07:37,000
 drink this elder's blood.

110
00:07:37,000 --> 00:07:41,160
 And the jeweler saw this and he was overwhelmed with anger

111
00:07:41,160 --> 00:07:45,000
 and just consumed with his horrible evil mind state.

112
00:07:45,000 --> 00:07:51,250
 He yelled at the heron and hit it with a stick and killed

113
00:07:51,250 --> 00:07:52,000
 it.

114
00:07:52,000 --> 00:07:57,670
 And the elder saw this through his bloody haze and asked

115
00:07:57,670 --> 00:08:03,000
 the labors and said, "Is the heron dead?"

116
00:08:03,000 --> 00:08:06,500
 And the labors and said, "You're next. You're going to be

117
00:08:06,500 --> 00:08:09,270
 dead. If you don't tell me where that jewel is, you're

118
00:08:09,270 --> 00:08:11,000
 going to be dead just like it."

119
00:08:11,000 --> 00:08:13,570
 So taking that as a sign, as an answer, that this heron was

120
00:08:13,570 --> 00:08:17,470
 dead, the elder immediately said it was the heron that

121
00:08:17,470 --> 00:08:19,000
 swallowed the jewel.

122
00:08:19,000 --> 00:08:24,340
 He said, "However, had the heron not died, I would sooner

123
00:08:24,340 --> 00:08:29,460
 have died myself than have told you it became of the jewel

124
00:08:29,460 --> 00:08:30,000
."

125
00:08:30,000 --> 00:08:35,720
 And disbelievingly pulled apart the heron and opened up its

126
00:08:35,720 --> 00:08:40,000
 innards and sure enough found the jewel.

127
00:08:40,000 --> 00:08:43,000
 And yeah, you can imagine how he felt at that point.

128
00:08:43,000 --> 00:08:48,280
 He felt really, he kneeled down prostrate at the elder's

129
00:08:48,280 --> 00:08:52,750
 feet, asked his forgiveness and, "Please, please forgive me

130
00:08:52,750 --> 00:08:54,000
. This was out of ignorance."

131
00:08:54,000 --> 00:09:00,290
 And the elder, in the manner of enlightened beings, said, "

132
00:09:00,290 --> 00:09:06,190
It wasn't your fault. It wasn't my fault. It's the fault of

133
00:09:06,190 --> 00:09:08,000
 the round of Zamsara."

134
00:09:08,000 --> 00:09:11,900
 So it's a part of existence to suffer like this. This is a

135
00:09:11,900 --> 00:09:17,470
 part of the game. It's just how it goes. Totally absolving

136
00:09:17,470 --> 00:09:20,000
 the guy, right?

137
00:09:20,000 --> 00:09:25,130
 And so still mortified. The jeweler says, "Well, then

138
00:09:25,130 --> 00:09:31,240
 please continue to come here. Really, don't, please don't

139
00:09:31,240 --> 00:09:37,000
 stop coming here just because I beat you almost to death."

140
00:09:37,000 --> 00:09:44,470
 And the elder gave a really sort of moving speech. She said

141
00:09:44,470 --> 00:09:47,760
, "It was because I came into someone's, because of going

142
00:09:47,760 --> 00:09:49,000
 into people's houses."

143
00:09:49,000 --> 00:09:51,180
 Like this is actually a thing. Some monks will not go into

144
00:09:51,180 --> 00:09:54,630
 people's houses. They'll only take what's given out the

145
00:09:54,630 --> 00:09:57,000
 door because this sort of thing happens.

146
00:09:57,000 --> 00:10:00,300
 You get in trouble when you go into people's houses. And

147
00:10:00,300 --> 00:10:04,900
 the monastic rules are really interesting and you really

148
00:10:04,900 --> 00:10:08,000
 start to get a feel for wanting to keep them for reasons

149
00:10:08,000 --> 00:10:12,000
 like, not this, but this sort of thing that happens.

150
00:10:12,000 --> 00:10:16,640
 Being a monk is kind of like that. So anyway, he says, "H

151
00:10:16,640 --> 00:10:22,000
enceforth, I shall never receive alms under anyone's house."

152
00:10:22,000 --> 00:10:25,360
 But the interesting thing is whether he was trying to just

153
00:10:25,360 --> 00:10:30,450
 spare the man's feelings, but it seems like he may have

154
00:10:30,450 --> 00:10:36,900
 known that he was dying because actually he died as a

155
00:10:36,900 --> 00:10:40,000
 result of the injury.

156
00:10:40,000 --> 00:10:43,140
 But before, so what he said is he actually spoke a stanza

157
00:10:43,140 --> 00:10:45,920
 that isn't part of the Dhammapada, but it's in the

158
00:10:45,920 --> 00:10:50,060
 commentary. He said, "Food is cooked for the sage a little

159
00:10:50,060 --> 00:10:53,000
 here and a little there in one house after another.

160
00:10:53,000 --> 00:10:57,060
 I will journey about on my round for alms. A good stout leg

161
00:10:57,060 --> 00:11:01,000
 is mine." Those are the last words maybe of the elder.

162
00:11:01,000 --> 00:11:08,000
 "Not long after he spoke them, he passed into Nipat."

163
00:11:08,000 --> 00:11:12,400
 The heron was born, if you're interested, as the story goes

164
00:11:12,400 --> 00:11:15,000
, was born in the mother's womb.

165
00:11:15,000 --> 00:11:21,000
 The wife as her son or daughter.

166
00:11:21,000 --> 00:11:32,340
 The husband, when he passed away, was born in hell. And the

167
00:11:32,340 --> 00:11:34,960
 wife was born in heaven as a result of never doubting the

168
00:11:34,960 --> 00:11:37,740
 elder and trying to stop her husband from being such a jerk

169
00:11:37,740 --> 00:11:38,000
.

170
00:11:38,000 --> 00:11:44,620
 So the monks asked the Buddha about these four beings, the

171
00:11:44,620 --> 00:11:50,000
 heron, the wife, the husband and the monk.

172
00:11:50,000 --> 00:11:53,860
 Or they maybe just asked about the monk. So the Buddha said

173
00:11:53,860 --> 00:11:56,520
, "Monks, living beings here in this world, some re-enter

174
00:11:56,520 --> 00:11:57,000
 the womb.

175
00:11:57,000 --> 00:12:00,930
 Others who are evildoers go to hell. Others who have done

176
00:12:00,930 --> 00:12:03,400
 good deeds go to the world of the gods, the world of the

177
00:12:03,400 --> 00:12:04,000
 angels.

178
00:12:04,000 --> 00:12:08,350
 Or they that have rid themselves of the taints pass to Nip

179
00:12:08,350 --> 00:12:12,000
at." And so he taught this verse.

180
00:12:12,000 --> 00:12:16,810
 So the most interesting part is the part of the story where

181
00:12:16,810 --> 00:12:21,720
 the elder doesn't give up the heron, doesn't save himself

182
00:12:21,720 --> 00:12:24,000
 by condemning the heron to death.

183
00:12:24,000 --> 00:12:28,940
 Which is typical of enlightened beings. I mean, it's really

184
00:12:28,940 --> 00:12:33,280
 an understatement. It's one of the most profound beauties

185
00:12:33,280 --> 00:12:35,000
 of an enlightened being.

186
00:12:35,000 --> 00:12:41,110
 There's a purity that surprises. I mean, you think, "Okay,

187
00:12:41,110 --> 00:12:43,230
 they're pure, but surely they're not going to give up

188
00:12:43,230 --> 00:12:44,000
 themselves.

189
00:12:44,000 --> 00:12:47,160
 They're not going to allow themselves to be tortured." But

190
00:12:47,160 --> 00:12:49,000
 yes, actually, they're that pure.

191
00:12:49,000 --> 00:12:52,850
 Their minds are so pure that being beaten to death doesn't

192
00:12:52,850 --> 00:12:56,590
 bother them. It's that much strength. It really is an inv

193
00:12:56,590 --> 00:12:58,000
incibility.

194
00:12:58,000 --> 00:13:02,080
 It's an example for all of us. Ask yourself, "If I was

195
00:13:02,080 --> 00:13:06,000
 being beaten to death, how would I feel?"

196
00:13:06,000 --> 00:13:12,060
 And then, curious is what he said. I mean, the corollary is

197
00:13:12,060 --> 00:13:18,000
 that because to be upset, even when being beaten to death,

198
00:13:18,000 --> 00:13:20,000
 is to misunderstand.

199
00:13:20,000 --> 00:13:23,000
 I mean, being beaten to death is just part of reality.

200
00:13:23,000 --> 00:13:26,000
 There's no person being beaten. It's just an experience.

201
00:13:26,000 --> 00:13:30,960
 The body is being altered, and there's experiences of pain

202
00:13:30,960 --> 00:13:35,000
 arising. But there's still just experiences.

203
00:13:35,000 --> 00:13:38,630
 There's nothing good or bad about them. So he had no

204
00:13:38,630 --> 00:13:41,310
 problem forgiving this guy or even telling him that there

205
00:13:41,310 --> 00:13:42,000
 was nothing to forgive,

206
00:13:42,000 --> 00:13:44,740
 because in the eyes of an enlightened being, there really

207
00:13:44,740 --> 00:13:48,000
 is nothing to forgive. It's just part of samsara.

208
00:13:48,000 --> 00:13:52,140
 There's mind states arise, and the mind states lead to body

209
00:13:52,140 --> 00:13:57,520
 states. The body states arise, lead to mind states, and so

210
00:13:57,520 --> 00:13:58,000
 on.

211
00:13:58,000 --> 00:14:02,330
 So body is a cause for mind. Mind is a cause for body. It's

212
00:14:02,330 --> 00:14:08,000
 just how things go. There's no good and no bad.

213
00:14:08,000 --> 00:14:12,730
 In the end, but for those of us living in samsara and

214
00:14:12,730 --> 00:14:18,070
 concerned, it's worth noting that not every path leads to

215
00:14:18,070 --> 00:14:20,000
 the same destination.

216
00:14:20,000 --> 00:14:22,440
 I mean, they all still lead to samsara, actually. Going to

217
00:14:22,440 --> 00:14:25,440
 heaven is part of samsara. Going to hell is also part of s

218
00:14:25,440 --> 00:14:26,000
amsara.

219
00:14:26,000 --> 00:14:30,420
 Being born in a womb is also part of samsara. The only

220
00:14:30,420 --> 00:14:37,000
 thing that's not is how enlightened beings go to nibhana.

221
00:14:37,000 --> 00:14:44,000
 An asava, those without asava, those without pains.

222
00:14:44,000 --> 00:14:46,680
 So how this relates to our practice, I think the most

223
00:14:46,680 --> 00:14:49,640
 important point is this example that the arahant sets for

224
00:14:49,640 --> 00:14:55,990
 us, this purity, not being concerned for one's own welfare,

225
00:14:55,990 --> 00:14:59,000
 not even taking that into account.

226
00:14:59,000 --> 00:15:05,120
 Now, you could argue that in most cases, an arahant, and it

227
00:15:05,120 --> 00:15:10,580
 seems, they wouldn't be concerned in the sense of concern

228
00:15:10,580 --> 00:15:13,000
 for the other person's welfare.

229
00:15:13,000 --> 00:15:17,930
 Obviously, it's a tragedy. The tragedy is not that the arah

230
00:15:17,930 --> 00:15:21,140
ant was beaten, although it's a tragedy that he had to die,

231
00:15:21,140 --> 00:15:25,000
 because arahants are, of course, the greatest of beings.

232
00:15:25,000 --> 00:15:35,270
 But the great tragedy or a great tragedy is for the jeweler

233
00:15:35,270 --> 00:15:39,000
. He's the one who you should feel sad for, feel sorry for.

234
00:15:39,000 --> 00:15:46,500
 There's a sense that arahants are perfectly fine with death

235
00:15:46,500 --> 00:15:47,000
.

236
00:15:47,000 --> 00:15:52,990
 I mean, it leads to freedom. For an arahant, death is

237
00:15:52,990 --> 00:15:54,000
 freedom.

238
00:15:54,000 --> 00:15:59,550
 So there's no problem there. But for this jeweler, it's a

239
00:15:59,550 --> 00:16:01,000
 big problem.

240
00:16:01,000 --> 00:16:03,470
 I mean, it's the concept of how suffering doesn't lead to

241
00:16:03,470 --> 00:16:06,590
 suffering. Happiness doesn't lead to happiness. It's not

242
00:16:06,590 --> 00:16:09,000
 the case that suffering is bad.

243
00:16:09,000 --> 00:16:13,000
 Suffering is not bad because it doesn't lead to suffering,

244
00:16:13,000 --> 00:16:16,880
 which is an interesting thing. But evil is bad, because

245
00:16:16,880 --> 00:16:19,000
 evil leads to suffering.

246
00:16:19,000 --> 00:16:21,940
 I mean, this is still conventional, because again, good and

247
00:16:21,940 --> 00:16:25,340
 evil are just part of samsara in the end. True goodness is

248
00:16:25,340 --> 00:16:30,000
 rising above them, freeing yourself from attachment to good

249
00:16:30,000 --> 00:16:30,000
 deeds.

250
00:16:30,000 --> 00:16:32,040
 To good deeds, in the sense of, oh, I must do more good

251
00:16:32,040 --> 00:16:34,000
 deeds or wanting to do more good deeds, right?

252
00:16:34,000 --> 00:16:37,360
 Even attachment to that will lead you to be reborn in

253
00:16:37,360 --> 00:16:40,980
 heaven in nice places, in places that are conducive to

254
00:16:40,980 --> 00:16:42,000
 meditation.

255
00:16:42,000 --> 00:16:47,970
 But in the end, it can't be a goal in itself. But still,

256
00:16:47,970 --> 00:16:53,000
 for your information, good and evil, two different things.

257
00:16:53,000 --> 00:17:00,520
 Dhamma, the righteousness and unrighteousness don't have

258
00:17:00,520 --> 00:17:03,000
 the same result.

259
00:17:03,000 --> 00:17:07,000
 So some are born in heaven, some are born in hell.

260
00:17:07,000 --> 00:17:10,450
 For those of us who practice meditation, it's the example

261
00:17:10,450 --> 00:17:14,990
 of purifying our minds, seeing things clearly and observing

262
00:17:14,990 --> 00:17:17,000
, and really taking this into account.

263
00:17:17,000 --> 00:17:19,600
 You know, if this arahant could do that, well, we ask

264
00:17:19,600 --> 00:17:22,500
 ourselves how far we are, how much pain, how much suffering

265
00:17:22,500 --> 00:17:24,000
 are we able to tolerate?

266
00:17:24,000 --> 00:17:27,360
 When do we start complaining? When do we start getting

267
00:17:27,360 --> 00:17:32,090
 upset? How patient can we be? And what are we able to be

268
00:17:32,090 --> 00:17:35,000
 patient with in our practice?

269
00:17:35,000 --> 00:17:37,870
 So this kind of example, the Dhammapada is full of these

270
00:17:37,870 --> 00:17:41,000
 kind of things that give us good examples in our practice,

271
00:17:41,000 --> 00:17:45,000
 and so it's a good lesson for that reason.

272
00:17:45,000 --> 00:17:50,720
 It's also a curious thing because I think many people

273
00:17:50,720 --> 00:17:56,830
 wouldn't have that sense of the conscientiousness of an

274
00:17:56,830 --> 00:18:01,990
 enlightened being, to not betray someone else, to not save

275
00:18:01,990 --> 00:18:05,000
 themselves at the expense of another.

276
00:18:05,000 --> 00:18:09,110
 So, that's the Dhammapada for tonight. Thank you all for

277
00:18:09,110 --> 00:18:10,000
 tuning in.

278
00:18:10,000 --> 00:18:13,000
 Keep practicing. Be well.

