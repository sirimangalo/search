1
00:00:00,000 --> 00:00:06,360
 Okay, welcome back to our study of the Dhammapada.

2
00:00:06,360 --> 00:00:10,680
 Today we continue on with verse number 64, which reads as

3
00:00:10,680 --> 00:00:13,200
 follows.

4
00:00:13,200 --> 00:00:25,860
 Ya wajeevampi jeebalo panditang paayi rupasati naso dhamm

5
00:00:25,860 --> 00:00:32,800
ang vijanati dhammih su parasangyata

6
00:00:32,800 --> 00:00:41,140
 which means, ya wajeevampi jeebalo, a fool who for his

7
00:00:41,140 --> 00:00:46,480
 whole life, panditang paayi rupasati

8
00:00:46,480 --> 00:00:55,680
 attends upon or associates with a wise person.

9
00:00:55,680 --> 00:00:59,970
 Naso dhammang vijanati, such a person, such a fool, even

10
00:00:59,970 --> 00:01:01,800
 though they spend their whole

11
00:01:01,800 --> 00:01:07,080
 life with a wise person, doesn't realize the dhamma for

12
00:01:07,080 --> 00:01:08,680
 themselves.

13
00:01:08,680 --> 00:01:21,910
 Dhammih su parasangyata, just as the spoon in the soup,

14
00:01:21,910 --> 00:01:29,320
 doesn't taste the soup.

15
00:01:29,320 --> 00:01:33,910
 This is a story, a verse based on a story, another very

16
00:01:33,910 --> 00:01:36,320
 short story of the venerable

17
00:01:36,320 --> 00:01:40,240
 Udayi who was, I guess, a bit of a fool.

18
00:01:40,240 --> 00:01:45,410
 I'm not sure if this is Lalu Udayi or just another Udayi,

19
00:01:45,410 --> 00:01:48,040
 but it's one of those people

20
00:01:48,040 --> 00:01:51,960
 who never really caught on to the actual teaching.

21
00:01:51,960 --> 00:01:56,410
 He may not have been a bad monk, but he just kind of stuck

22
00:01:56,410 --> 00:01:58,960
 around and didn't really have

23
00:01:58,960 --> 00:02:02,030
 a clue as to what the Buddha taught and certainly hadn't

24
00:02:02,030 --> 00:02:03,760
 realized it for himself.

25
00:02:03,760 --> 00:02:07,950
 But when the elders left the dhamma hall, and there was

26
00:02:07,950 --> 00:02:10,240
 nobody around, he liked to go

27
00:02:10,240 --> 00:02:17,340
 and sit up in the dhammasana, the dhamma chair, they have a

28
00:02:17,340 --> 00:02:21,000
 special chair where someone, when

29
00:02:21,000 --> 00:02:24,540
 they're giving a dhamma talk, sits so that even a younger

30
00:02:24,540 --> 00:02:26,400
 monk would be sitting higher

31
00:02:26,400 --> 00:02:29,650
 than the senior monks for the time that they were giving

32
00:02:29,650 --> 00:02:30,880
 the dhamma talk.

33
00:02:30,880 --> 00:02:40,210
 So whoever's teaching the dhamma, they would have them sit

34
00:02:40,210 --> 00:02:44,400
 higher as a show of respect.

35
00:02:44,400 --> 00:02:47,490
 And so he used to go and sit on it and pretend like he was

36
00:02:47,490 --> 00:02:49,520
 some great elder and he knew what

37
00:02:49,520 --> 00:02:50,520
 he was doing.

38
00:02:50,520 --> 00:02:53,680
 So one day he's doing this and a bunch of visiting monks

39
00:02:53,680 --> 00:02:55,240
 come in and they see him up

40
00:02:55,240 --> 00:02:57,420
 on the dhamma chair and they think, "Oh, this must be some

41
00:02:57,420 --> 00:02:58,120
 great elder."

42
00:02:58,120 --> 00:03:05,780
 And so they all go up and they sit around him and ask him

43
00:03:05,780 --> 00:03:10,080
 all sorts of questions about

44
00:03:10,080 --> 00:03:13,360
 the aggregates and very deep subjects, about the five

45
00:03:13,360 --> 00:03:15,880
 aggregates and the 12 ayatana, the

46
00:03:15,880 --> 00:03:22,280
 12 bases, and the 18 dhatas, the 18 elements.

47
00:03:22,280 --> 00:03:25,540
 They ask him all sorts of questions and he's like, "I don't

48
00:03:25,540 --> 00:03:26,200
 know."

49
00:03:26,200 --> 00:03:32,080
 He turns out to be a fool and not have any answers.

50
00:03:32,080 --> 00:03:37,080
 And the other monks are appalled because the monastery we

51
00:03:37,080 --> 00:03:40,280
 were staying with was the monastery

52
00:03:40,280 --> 00:03:41,280
 of the Buddha.

53
00:03:41,280 --> 00:03:44,040
 So he said to themselves, "How can it be?

54
00:03:44,040 --> 00:03:45,040
 Here you are.

55
00:03:45,040 --> 00:03:46,040
 How long have you been a monk?"

56
00:03:46,040 --> 00:03:47,980
 And I guess he was an elder, so he'd been a monk for at

57
00:03:47,980 --> 00:03:49,400
 least 10 years or at least he'd

58
00:03:49,400 --> 00:03:52,330
 been there for some time and they were appalled that he had

59
00:03:52,330 --> 00:03:53,760
 been with the Buddha all that

60
00:03:53,760 --> 00:03:58,360
 time and hadn't learned anything.

61
00:03:58,360 --> 00:04:04,320
 And so criticizing him, they went over to pay respect to

62
00:04:04,320 --> 00:04:05,920
 the Buddha.

63
00:04:05,920 --> 00:04:07,920
 They waited until the appropriate time and made an

64
00:04:07,920 --> 00:04:09,360
 appointment to see the Buddha and

65
00:04:09,360 --> 00:04:11,900
 went in and paid respect to the Buddha and said, "Vendro,

66
00:04:11,900 --> 00:04:13,160
 sir, there's this monk and

67
00:04:13,160 --> 00:04:16,140
 he's sitting up in the dhamma chair and we asked him all

68
00:04:16,140 --> 00:04:17,840
 these questions and he hadn't

69
00:04:17,840 --> 00:04:18,840
 a clue.

70
00:04:18,840 --> 00:04:22,330
 And we're thinking, "How could it be that someone had been

71
00:04:22,330 --> 00:04:23,960
 with you so long, living

72
00:04:23,960 --> 00:04:29,190
 in the same monastery as the blessed one himself, still

73
00:04:29,190 --> 00:04:32,200
 wasn't able to realize the dhamma

74
00:04:32,200 --> 00:04:33,200
 for himself?"

75
00:04:33,200 --> 00:04:36,400
 So he had no clue about the dhamma.

76
00:04:36,400 --> 00:04:39,500
 And the Buddha taught them, "Give me his teaching," and

77
00:04:39,500 --> 00:04:40,720
 spoke this verse.

78
00:04:40,720 --> 00:04:46,390
 He said, "Even if a fool, even if they lived together with

79
00:04:46,390 --> 00:04:50,120
 wise people for 100 years, because

80
00:04:50,120 --> 00:04:54,130
 they're a fool, they never realize the dhamma no matter how

81
00:04:54,130 --> 00:04:56,640
 long, how much time they spend."

82
00:04:56,640 --> 00:05:01,480
 And then he said, "It's just like the spoon.

83
00:05:01,480 --> 00:05:04,600
 When you put a spoon in the soup, it never has an

84
00:05:04,600 --> 00:05:06,800
 opportunity to taste the soup, no matter

85
00:05:06,800 --> 00:05:11,920
 if you keep it in the soup for 100 years."

86
00:05:11,920 --> 00:05:14,280
 So that's the verse.

87
00:05:14,280 --> 00:05:15,760
 That's the story and that's the verse.

88
00:05:15,760 --> 00:05:20,170
 The actual teaching behind it is obviously more generally

89
00:05:20,170 --> 00:05:22,800
 applicable than just the story.

90
00:05:22,800 --> 00:05:26,200
 It's a caution to us.

91
00:05:26,200 --> 00:05:28,470
 And some people might even be surprised that this is

92
00:05:28,470 --> 00:05:29,120
 possible.

93
00:05:29,120 --> 00:05:32,410
 How could it be that you could spend so much time, even

94
00:05:32,410 --> 00:05:34,240
 your whole life, with wise people

95
00:05:34,240 --> 00:05:39,480
 and never become wise?

96
00:05:39,480 --> 00:05:43,320
 And the analogy of the spoon and the soup is quite apt.

97
00:05:43,320 --> 00:05:46,680
 It's because of the nature of the people.

98
00:05:46,680 --> 00:05:51,780
 If a person is of a nature to be cruel and lustful and

99
00:05:51,780 --> 00:05:55,440
 arrogant and deluded and all these

100
00:05:55,440 --> 00:06:02,290
 bad things, so performs unwholesome acts and is unwholesome

101
00:06:02,290 --> 00:06:05,720
 in their speech and unguarded

102
00:06:05,720 --> 00:06:14,360
 in their behavior and unwholesome in the mind.

103
00:06:14,360 --> 00:06:19,810
 If they're inclined towards unwholesomeness, then physical

104
00:06:19,810 --> 00:06:22,600
 presence doesn't help them.

105
00:06:22,600 --> 00:06:26,360
 You see this acutely with even people who can answer

106
00:06:26,360 --> 00:06:28,920
 questions and do have knowledge of

107
00:06:28,920 --> 00:06:35,600
 the Dhamma, people who have studied a lot but have never

108
00:06:35,600 --> 00:06:39,240
 practiced, can still inside

109
00:06:39,240 --> 00:06:52,920
 be incredibly clueless to the truth, unenlightened inside.

110
00:06:52,920 --> 00:06:57,140
 They actually have trouble answering questions in a cogent

111
00:06:57,140 --> 00:06:58,040
 fashion.

112
00:06:58,040 --> 00:07:01,160
 They aren't able to put together the Buddha's teaching into

113
00:07:01,160 --> 00:07:02,760
 a way that's understandable

114
00:07:02,760 --> 00:07:05,280
 because they themselves don't understand it.

115
00:07:05,280 --> 00:07:09,040
 So when asked deep questions, even people who have studied

116
00:07:09,040 --> 00:07:10,640
 a lot, it's easy to tell

117
00:07:10,640 --> 00:07:14,760
 whether they have or not practiced based on their ability

118
00:07:14,760 --> 00:07:16,760
 to explain the teaching in the

119
00:07:16,760 --> 00:07:19,440
 cogent manner.

120
00:07:19,440 --> 00:07:20,440
 Meditation is really the key.

121
00:07:20,440 --> 00:07:22,880
 I always try to bring these videos back to our meditation

122
00:07:22,880 --> 00:07:24,360
 practice and it's the meditation

123
00:07:24,360 --> 00:07:28,800
 that allows you to taste the soup, so to speak.

124
00:07:28,800 --> 00:07:33,490
 If a person isn't practicing the teachings, it's like

125
00:07:33,490 --> 00:07:36,040
 reading a map but never driving

126
00:07:36,040 --> 00:07:41,330
 or reading the driver's manual and never actually getting

127
00:07:41,330 --> 00:07:43,240
 behind the wheel.

128
00:07:43,240 --> 00:07:45,810
 Our knowledge of the Buddha's teaching and even our

129
00:07:45,810 --> 00:07:47,840
 rational understanding and agreement

130
00:07:47,840 --> 00:07:53,690
 of the Buddha's teaching, it's similar to any philosophy

131
00:07:53,690 --> 00:07:56,880
 where you will find philosophers

132
00:07:56,880 --> 00:08:00,240
 passionately arguing for a certain philosophy but not

133
00:08:00,240 --> 00:08:02,040
 following it themselves.

134
00:08:02,040 --> 00:08:09,280
 It's like doctors who smoke cigarettes or themselves have

135
00:08:09,280 --> 00:08:12,200
 poor health habits.

136
00:08:12,200 --> 00:08:16,790
 This kind of hypocrisy is one way of putting it, but it can

137
00:08:16,790 --> 00:08:19,840
 just be a superficial understanding

138
00:08:19,840 --> 00:08:21,840
 or a lack of understanding.

139
00:08:21,840 --> 00:08:28,460
 There are many reasons for this. There are monks who become

140
00:08:28,460 --> 00:08:29,040
 monks for the wrong reason.

141
00:08:29,040 --> 00:08:32,960
 They ordain because they have nowhere else to go.

142
00:08:32,960 --> 00:08:37,360
 There are social outcasts and so on.

143
00:08:37,360 --> 00:08:43,190
 In Asian countries, it's often poor people who ordain as no

144
00:08:43,190 --> 00:08:44,160
vices.

145
00:08:44,160 --> 00:08:46,990
 Novices will become novices not because they have this keen

146
00:08:46,990 --> 00:08:48,360
 interest at seven years old

147
00:08:48,360 --> 00:08:50,500
 to become enlightened but because at seven years old, they

148
00:08:50,500 --> 00:08:51,480
 don't have enough food to

149
00:08:51,480 --> 00:08:53,280
 eat and their parents can't feed them.

150
00:08:53,280 --> 00:08:57,410
 They go to the monastery and they're promised a meal and

151
00:08:57,410 --> 00:08:58,360
 shelter.

152
00:08:58,360 --> 00:09:00,760
 Getting off on the wrong foot like this, they grow up in

153
00:09:00,760 --> 00:09:01,600
 the wrong way.

154
00:09:01,600 --> 00:09:07,470
 By the time they become monks, they're trained in the wrong

155
00:09:07,470 --> 00:09:08,280
 way.

156
00:09:08,280 --> 00:09:12,560
 They're set in being a career monk.

157
00:09:12,560 --> 00:09:16,740
 As a result, they don't have the inclination or they're in

158
00:09:16,740 --> 00:09:18,760
ured, you can say, against the

159
00:09:18,760 --> 00:09:22,280
 realization.

160
00:09:22,280 --> 00:09:24,160
 Inured, I don't know if that's right.

161
00:09:24,160 --> 00:09:26,840
 We're insulated against it.

162
00:09:26,840 --> 00:09:29,320
 They're not able to realize it because of the...

163
00:09:29,320 --> 00:09:35,280
 No, they're inured, I think is the word I can...

164
00:09:35,280 --> 00:09:36,280
 They're far from it.

165
00:09:36,280 --> 00:09:41,740
 They're not able to connect with the Dhamma because their

166
00:09:41,740 --> 00:09:42,920
 way of thinking about it is

167
00:09:42,920 --> 00:09:48,380
 as a livelihood and as an intellectual pursuit or as a

168
00:09:48,380 --> 00:09:49,600
 culture.

169
00:09:49,600 --> 00:09:54,800
 There's so much ingrained in them that is wrong.

170
00:09:54,800 --> 00:09:58,040
 The Buddha said it's...

171
00:09:58,040 --> 00:10:01,930
 I think the Buddha said it was also in the commentary is

172
00:10:01,930 --> 00:10:04,200
 that it's harder to remove right

173
00:10:04,200 --> 00:10:07,760
 view than it is to remove wrong view.

174
00:10:07,760 --> 00:10:10,200
 They're paraphrasing.

175
00:10:10,200 --> 00:10:13,010
 The point is the person with wrong view, you can teach them

176
00:10:13,010 --> 00:10:14,720
 meditation quite easily because

177
00:10:14,720 --> 00:10:17,000
 their wrong view is very easy to change.

178
00:10:17,000 --> 00:10:19,050
 When they realize how wrong they are, they give up and they

179
00:10:19,050 --> 00:10:19,960
 follow the teaching.

180
00:10:19,960 --> 00:10:22,310
 But a person who has right view and who knows all the truth

181
00:10:22,310 --> 00:10:23,720
 intellectually, it's very hard

182
00:10:23,720 --> 00:10:25,740
 to teach because no matter what you say, they say, "Yes, I

183
00:10:25,740 --> 00:10:26,160
 know."

184
00:10:26,160 --> 00:10:28,200
 Yes, yes, yes, I know.

185
00:10:28,200 --> 00:10:29,840
 They don't realize that actually they don't really know

186
00:10:29,840 --> 00:10:30,840
 what they're talking about.

187
00:10:30,840 --> 00:10:39,640
 They know everything but they know nothing.

188
00:10:39,640 --> 00:10:42,980
 And there are monks who are corrupt who are just using it

189
00:10:42,980 --> 00:10:45,000
 as a livelihood or even engaging

190
00:10:45,000 --> 00:10:51,760
 in evil practices.

191
00:10:51,760 --> 00:10:54,440
 And then there are just people who never get it.

192
00:10:54,440 --> 00:10:55,440
 For most of us, it's very difficult.

193
00:10:55,440 --> 00:10:58,400
 We were talking last night about the four types of lotus.

194
00:10:58,400 --> 00:11:00,700
 There are just some people who will never get the Dhamma

195
00:11:00,700 --> 00:11:01,800
 and never realize it.

196
00:11:01,800 --> 00:11:05,400
 Maybe their minds are in the wrong place.

197
00:11:05,400 --> 00:11:07,840
 But that's too discouraging.

198
00:11:07,840 --> 00:11:09,880
 I think that's not really what we're talking about.

199
00:11:09,880 --> 00:11:12,450
 We're talking about people who don't want to learn, people

200
00:11:12,450 --> 00:11:13,640
 who are foolish, who are

201
00:11:13,640 --> 00:11:18,260
 engaged in wasting their time and who are too lazy to

202
00:11:18,260 --> 00:11:21,280
 actually put the teachings into

203
00:11:21,280 --> 00:11:22,280
 practice.

204
00:11:22,280 --> 00:11:27,430
 Again, it's not a criticism or it's not really ... we want

205
00:11:27,430 --> 00:11:29,960
 to go around pointing fingers,

206
00:11:29,960 --> 00:11:34,030
 but it's an admonishment to all of us to take this

207
00:11:34,030 --> 00:11:35,360
 seriously.

208
00:11:35,360 --> 00:11:38,320
 That we should never be complacent just because we're Buddh

209
00:11:38,320 --> 00:11:39,920
ists or obviously just because

210
00:11:39,920 --> 00:11:40,920
 we're monks.

211
00:11:40,920 --> 00:11:42,500
 I think that's often the case.

212
00:11:42,500 --> 00:11:44,460
 There are people who think that just because we're monks,

213
00:11:44,460 --> 00:11:45,080
 we're perfect.

214
00:11:45,080 --> 00:11:47,160
 Of course, then there's the opposite case.

215
00:11:47,160 --> 00:11:51,660
 People who don't ever give monks a break and are always

216
00:11:51,660 --> 00:11:54,520
 criticizing the smallest faults

217
00:11:54,520 --> 00:11:55,520
 in monks.

218
00:11:55,520 --> 00:11:59,610
 I know there's people like that who don't realize how

219
00:11:59,610 --> 00:12:02,040
 difficult it is to be a monk.

220
00:12:02,040 --> 00:12:03,600
 But it certainly isn't enough.

221
00:12:03,600 --> 00:12:06,240
 It's not enough to call ourselves Buddhist.

222
00:12:06,240 --> 00:12:07,240
 They people are the same.

223
00:12:07,240 --> 00:12:10,860
 It's very easy to become complacent, thinking that I'm

224
00:12:10,860 --> 00:12:13,120
 Buddhist now and that's enough.

225
00:12:13,120 --> 00:12:14,840
 It's certainly not enough.

226
00:12:14,840 --> 00:12:17,000
 A person, the Buddhist said a person can live their whole

227
00:12:17,000 --> 00:12:18,320
 life with a wise person and never

228
00:12:18,320 --> 00:12:19,320
 become wise.

229
00:12:19,320 --> 00:12:22,480
 It has all to do with our own practice.

230
00:12:22,480 --> 00:12:26,110
 Good companions are only useful if we emulate them, if we

231
00:12:26,110 --> 00:12:28,080
 follow them, if we learn from

232
00:12:28,080 --> 00:12:29,080
 them.

233
00:12:29,080 --> 00:12:34,510
 Sometimes we become a burden on them and they're better off

234
00:12:34,510 --> 00:12:35,920
 without us.

235
00:12:35,920 --> 00:12:40,860
 Our practice of cultivating wholesome actions, wholesome

236
00:12:40,860 --> 00:12:44,760
 speech, wholesome thoughts and cultivating

237
00:12:44,760 --> 00:12:49,080
 wisdom, the burden is all placed on us.

238
00:12:49,080 --> 00:12:52,930
 It's at every moment how we act, how we speak, how we think

239
00:12:52,930 --> 00:12:55,240
, whether we have mindfulness,

240
00:12:55,240 --> 00:13:00,480
 whether we have awareness, whether we're cultivating

241
00:13:00,480 --> 00:13:03,880
 insight and wisdom for ourselves.

242
00:13:03,880 --> 00:13:07,000
 That's the determination of whether we taste the soup.

243
00:13:07,000 --> 00:13:09,400
 Otherwise, we're just like a spoon.

244
00:13:09,400 --> 00:13:14,470
 You can be surrounded by the teachings but never realize

245
00:13:14,470 --> 00:13:17,400
 the teachings for yourself.

246
00:13:17,400 --> 00:13:19,400
 Still not a bad thing.

247
00:13:19,400 --> 00:13:22,080
 There's lots of good that can come from being surrounded by

248
00:13:22,080 --> 00:13:23,600
 the Buddhist teaching and being

249
00:13:23,600 --> 00:13:25,560
 a monk and so on.

250
00:13:25,560 --> 00:13:27,860
 But to realize the Buddhist teaching, you have to practice

251
00:13:27,860 --> 00:13:28,560
 for yourself.

252
00:13:28,560 --> 00:13:32,700
 The Buddha said, "Even the Buddha is just one who shows the

253
00:13:32,700 --> 00:13:33,320
 way.

254
00:13:33,320 --> 00:13:36,800
 It's us who have to walk the path."

255
00:13:36,800 --> 00:13:40,160
 Simple teaching, something is a reminder for us.

256
00:13:40,160 --> 00:13:44,260
 It's a nice analogy for us to remember the simile of the

257
00:13:44,260 --> 00:13:45,920
 spoon in the soup.

258
00:13:45,920 --> 00:13:48,180
 Tomorrow we have the other side of the coin, those who can

259
00:13:48,180 --> 00:13:49,000
 taste the soup.

260
00:13:49,000 --> 00:13:51,560
 So tune in, not tomorrow, next time.

261
00:13:51,560 --> 00:13:52,560
 Tune in next time.

262
00:13:52,560 --> 00:13:55,320
 Thank you all for keeping up and for the positive feedback.

263
00:13:55,320 --> 00:13:56,320
 I appreciate it.

264
00:13:56,320 --> 00:14:00,240
 And I'm glad to know that these videos are a benefit for

265
00:14:00,240 --> 00:14:02,480
 everyone out there to be able

266
00:14:02,480 --> 00:14:05,040
 to put the teachings of the Buddha into practice and find

267
00:14:05,040 --> 00:14:06,760
 true peace, happiness and freedom

268
00:14:06,760 --> 00:14:07,760
 from suffering.

