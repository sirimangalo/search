1
00:00:00,000 --> 00:00:05,400
 Good evening everyone.

2
00:00:05,400 --> 00:00:12,160
 We're broadcasting live October, November 9th, 2015.

3
00:00:12,160 --> 00:00:13,160
 November 5th.

4
00:00:13,160 --> 00:00:15,440
 5th, November 5th.

5
00:00:15,440 --> 00:00:16,440
 Yeah.

6
00:00:16,440 --> 00:00:22,720
 That's correct.

7
00:00:22,720 --> 00:00:26,760
 And I think we've got proper sound going on.

8
00:00:26,760 --> 00:00:31,200
 You want to say hello, Robin, so I can test?

9
00:00:31,200 --> 00:00:32,200
 Hello.

10
00:00:32,200 --> 00:00:33,200
 Oh, no.

11
00:00:33,200 --> 00:00:37,400
 Why aren't you coming through?

12
00:00:37,400 --> 00:00:38,400
 I'm not sure.

13
00:00:38,400 --> 00:00:39,400
 Test, test.

14
00:00:39,400 --> 00:00:41,400
 Maybe you're not loud enough.

15
00:00:41,400 --> 00:00:42,400
 Oh, wait.

16
00:00:42,400 --> 00:00:43,400
 Oh, I see.

17
00:00:43,400 --> 00:00:44,400
 There.

18
00:00:44,400 --> 00:00:45,400
 Okay.

19
00:00:45,400 --> 00:00:46,400
 Now say hello.

20
00:00:46,400 --> 00:00:47,400
 Okay.

21
00:00:47,400 --> 00:00:59,560
 That's okay.

22
00:00:59,560 --> 00:01:04,720
 Now we both should be coming through the audio broadcast.

23
00:01:04,720 --> 00:01:05,720
 Okay.

24
00:01:05,720 --> 00:01:06,720
 Questions?

25
00:01:06,720 --> 00:01:08,720
 We have questions.

26
00:01:08,720 --> 00:01:09,720
 Yes.

27
00:01:09,720 --> 00:01:13,210
 First of all, let's do the announcements first because

28
00:01:13,210 --> 00:01:15,440
 tonight we don't have a Dhammapada

29
00:01:15,440 --> 00:01:16,440
 video.

30
00:01:16,440 --> 00:01:19,480
 So what's going on here?

31
00:01:19,480 --> 00:01:20,480
 We have two things.

32
00:01:20,480 --> 00:01:24,680
 Well, actually only one thing, but I have two posters.

33
00:01:24,680 --> 00:01:28,840
 I don't think it's worth showing you posters, is it?

34
00:01:28,840 --> 00:01:31,880
 You guys want to see our new posters?

35
00:01:31,880 --> 00:01:32,880
 Yes.

36
00:01:32,880 --> 00:01:37,320
 You can comment on them.

37
00:01:37,320 --> 00:01:38,320
 Screen share.

38
00:01:38,320 --> 00:01:39,320
 There.

39
00:01:39,320 --> 00:01:48,120
 Introducing the meditation.

40
00:01:48,120 --> 00:01:53,970
 I want to get something that will hit people and make them

41
00:01:53,970 --> 00:01:56,800
 wake up and say, "Hey, why aren't

42
00:01:56,800 --> 00:01:57,800
 you all meditating?"

43
00:01:57,800 --> 00:01:58,800
 Yeah.

44
00:01:58,800 --> 00:02:03,070
 Why do you go out drinking every night or whatever it is

45
00:02:03,070 --> 00:02:04,280
 that you do?

46
00:02:04,280 --> 00:02:05,280
 That's very nice.

47
00:02:05,280 --> 00:02:06,280
 Okay.

48
00:02:06,280 --> 00:02:10,520
 So that's the first one.

49
00:02:10,520 --> 00:02:14,920
 Did it change?

50
00:02:14,920 --> 00:02:17,920
 No, not yet.

51
00:02:17,920 --> 00:02:28,560
 Well, then we'll have to stop that.

52
00:02:28,560 --> 00:02:29,560
 There.

53
00:02:29,560 --> 00:02:32,560
 Oh, very nice.

54
00:02:32,560 --> 00:02:33,760
 This one isn't me.

55
00:02:33,760 --> 00:02:34,760
 This is Carolyn.

56
00:02:34,760 --> 00:02:37,640
 One of our exec members.

57
00:02:37,640 --> 00:02:39,360
 But I did the poster for her.

58
00:02:39,360 --> 00:02:41,280
 She did a poster, but then it...

59
00:02:41,280 --> 00:02:44,200
 Well, I redid it.

60
00:02:44,200 --> 00:02:47,720
 It came out nice.

61
00:02:47,720 --> 00:02:51,100
 The McMaster Buddhism logo is a little bit like the Surim

62
00:02:51,100 --> 00:02:51,960
ungo logo.

63
00:02:51,960 --> 00:02:54,400
 That's not the McMaster Buddhism logo.

64
00:02:54,400 --> 00:02:56,760
 That's the McMaster Students' Union Clubs.

65
00:02:56,760 --> 00:02:58,760
 Oh, okay.

66
00:02:58,760 --> 00:03:01,920
 They force us to put their logo on our posters.

67
00:03:01,920 --> 00:03:06,890
 So the clubs is very hard to read, but it's not important

68
00:03:06,890 --> 00:03:09,640
 to me because their logo isn't

69
00:03:09,640 --> 00:03:15,160
 all that important.

70
00:03:15,160 --> 00:03:16,480
 But yeah, so we're having a campfire.

71
00:03:16,480 --> 00:03:18,360
 This is our one big announcement.

72
00:03:18,360 --> 00:03:19,360
 This was someone's ideas.

73
00:03:19,360 --> 00:03:23,140
 I think it was Carolyn's idea to go and have a campfire in

74
00:03:23,140 --> 00:03:25,760
 the evening and talk about Buddhism

75
00:03:25,760 --> 00:03:28,440
 and maybe do some meditation together.

76
00:03:28,440 --> 00:03:32,000
 Not in nature.

77
00:03:32,000 --> 00:03:34,510
 Monks aren't actually allowed to sit around campfires

78
00:03:34,510 --> 00:03:35,160
 together.

79
00:03:35,160 --> 00:03:36,160
 It's an interesting thing.

80
00:03:36,160 --> 00:03:39,010
 I'll have to look up whether I can take part of the camp

81
00:03:39,010 --> 00:03:39,480
fire.

82
00:03:39,480 --> 00:03:41,520
 But it's a curious rule.

83
00:03:41,520 --> 00:03:43,880
 Oh, we're not presenting.

84
00:03:43,880 --> 00:03:45,720
 There we are.

85
00:03:45,720 --> 00:03:46,720
 Okay.

86
00:03:46,720 --> 00:03:47,720
 It's a curious rule.

87
00:03:47,720 --> 00:03:50,760
 And the speculation is that it's somehow...

88
00:03:50,760 --> 00:03:54,480
 Sitting around a campfire somehow leads to negligence.

89
00:03:54,480 --> 00:03:58,280
 It's just not an appropriate behavior for a Buddhist monk.

90
00:03:58,280 --> 00:03:59,280
 Whatever.

91
00:03:59,280 --> 00:04:03,960
 I think I'll be okay with attending because I'm the only

92
00:04:03,960 --> 00:04:06,120
 monk and I'm going to talk to

93
00:04:06,120 --> 00:04:08,640
 lay people, not to talk to monks.

94
00:04:08,640 --> 00:04:11,560
 And if there happens to be a campfire there.

95
00:04:11,560 --> 00:04:12,960
 Yeah.

96
00:04:12,960 --> 00:04:16,850
 I think the idea is they're going to have s'mores or

97
00:04:16,850 --> 00:04:17,960
 something.

98
00:04:17,960 --> 00:04:28,640
 So it's sort of in the vein of lay Buddhism.

99
00:04:28,640 --> 00:04:32,750
 The Ajahn Chah group has Buddhist songs that they sing and

100
00:04:32,750 --> 00:04:35,400
 Buddhists sing alongs and Buddhist

101
00:04:35,400 --> 00:04:40,800
 plays that are humorous about Buddhism.

102
00:04:40,800 --> 00:04:46,130
 You wonder where the line is going to be drawn, but

103
00:04:46,130 --> 00:04:50,880
 sometimes you just accept the kusulupaya,

104
00:04:50,880 --> 00:04:56,240
 the skillful means to get people interested.

105
00:04:56,240 --> 00:05:00,330
 So the idea is to create a community of like-minded

106
00:05:00,330 --> 00:05:01,800
 individuals.

107
00:05:01,800 --> 00:05:06,480
 Again, this isn't my idea, but it's interesting.

108
00:05:06,480 --> 00:05:10,600
 So that's part of what we'll be doing.

109
00:05:10,600 --> 00:05:15,480
 That's next Friday.

110
00:05:15,480 --> 00:05:19,560
 So that's a couple of things that have been going on here.

111
00:05:19,560 --> 00:05:20,560
 What else?

112
00:05:20,560 --> 00:05:22,040
 Tomorrow I'm going to London.

113
00:05:22,040 --> 00:05:25,240
 No, Saturday I'm going to London.

114
00:05:25,240 --> 00:05:28,680
 Ontario to someone's house.

115
00:05:28,680 --> 00:05:34,530
 Oh, and we've got 27 of 28 slots filled on the appointments

116
00:05:34,530 --> 00:05:35,320
 page.

117
00:05:35,320 --> 00:05:36,320
 That's great.

118
00:05:36,320 --> 00:05:42,440
 That means I'm meeting with 27 different people a week.

119
00:05:42,440 --> 00:05:46,760
 That's pretty good.

120
00:05:46,760 --> 00:05:55,440
 That's great.

121
00:05:55,440 --> 00:05:57,960
 So that's all.

122
00:05:57,960 --> 00:06:03,720
 Any announcements from the volunteer group?

123
00:06:03,720 --> 00:06:06,680
 If anyone has been following along on Facebook and hasn't

124
00:06:06,680 --> 00:06:08,440
 really noticed much activity, it's

125
00:06:08,440 --> 00:06:13,810
 because most of our activity now is on a website called

126
00:06:13,810 --> 00:06:15,320
 slack.com.

127
00:06:15,320 --> 00:06:17,570
 It's kind of taken the place of those email blasts that

128
00:06:17,570 --> 00:06:18,920
 were going out and taken a place

129
00:06:18,920 --> 00:06:22,760
 with a lot of the talk on Facebook.

130
00:06:22,760 --> 00:06:26,800
 So if anyone is interested, just send me a message.

131
00:06:26,800 --> 00:06:30,370
 I'll post the information into the Meditator chat panel as

132
00:06:30,370 --> 00:06:32,280
 well because it's still a very

133
00:06:32,280 --> 00:06:33,280
 active group.

134
00:06:33,280 --> 00:06:40,720
 So kind of changed our communication means a little bit.

135
00:06:40,720 --> 00:06:43,240
 Ready for questions, Bante?

136
00:06:43,240 --> 00:06:45,240
 Yep.

137
00:06:45,240 --> 00:06:46,240
 Okay.

138
00:06:46,240 --> 00:06:51,050
 Venerable, sir, what would be your translation of Lord

139
00:06:51,050 --> 00:06:53,160
 Buddha's final words?

140
00:06:53,160 --> 00:06:54,160
 Thank you.

141
00:06:54,160 --> 00:07:02,880
 Vaya dhammasankara apamade na sampadita.

142
00:07:02,880 --> 00:07:08,080
 All formations are of a nature to cease.

143
00:07:08,080 --> 00:07:12,560
 Vaya is to fade away.

144
00:07:12,560 --> 00:07:20,830
 Apamade na sampadita, come to fulfillment or fulfill your

145
00:07:20,830 --> 00:07:24,960
 goals or become fulfilled.

146
00:07:24,960 --> 00:07:37,200
 Apamade na with or in regards to, you could say, apamade.

147
00:07:37,200 --> 00:07:43,730
 So come to fulfillment in regards to vigilance or

148
00:07:43,730 --> 00:07:46,000
 mindfulness.

149
00:07:46,000 --> 00:07:47,520
 So become sober, really.

150
00:07:47,520 --> 00:07:49,520
 Sober up if you wanted it.

151
00:07:49,520 --> 00:07:59,760
 It's sort of a crass, crass paraphrase.

152
00:07:59,760 --> 00:08:06,400
 All, all this stuff, all this stuff is going to disappear.

153
00:08:06,400 --> 00:08:10,000
 Sober up.

154
00:08:10,000 --> 00:08:11,600
 All stuff disappears.

155
00:08:11,600 --> 00:08:18,360
 All stuff, all stuff breaks.

156
00:08:18,360 --> 00:08:20,880
 All stuff breaks.

157
00:08:20,880 --> 00:08:21,880
 Stuff breaks.

158
00:08:21,880 --> 00:08:22,880
 Sober up.

159
00:08:22,880 --> 00:08:23,880
 There you go.

160
00:08:23,880 --> 00:08:29,000
 Let's put as crass as I can get.

161
00:08:29,000 --> 00:08:30,960
 There's Buddhism in a nutshell.

162
00:08:30,960 --> 00:08:31,960
 Stuff breaks.

163
00:08:31,960 --> 00:08:34,560
 Sober up.

164
00:08:34,560 --> 00:08:36,520
 That's a meme for you.

165
00:08:36,520 --> 00:08:39,640
 It's actually a fairly literal translation, interestingly.

166
00:08:39,640 --> 00:08:42,800
 Stuff, sankara is stuff.

167
00:08:42,800 --> 00:08:46,330
 It depends what you mean by stuff, but it's one way of, you

168
00:08:46,330 --> 00:08:48,560
 know, stuff is a good translation

169
00:08:48,560 --> 00:08:49,560
 of sankara.

170
00:08:49,560 --> 00:08:57,060
 Breaks, vaya means it's, you know, it's more like dissip

171
00:08:57,060 --> 00:09:01,680
ates or ceases, but fades away.

172
00:09:01,680 --> 00:09:04,040
 Breaks is, I think, good.

173
00:09:04,040 --> 00:09:12,350
 Sober because bhammada is, as to mud, mud is the root of

174
00:09:12,350 --> 00:09:12,720
 being intoxicated.

175
00:09:12,720 --> 00:09:15,280
 So bhammada is a kind of mental intoxication.

176
00:09:15,280 --> 00:09:21,890
 Up bhammada is sobering up or non-intoxication, un-intox

177
00:09:21,890 --> 00:09:22,480
icated.

178
00:09:22,480 --> 00:09:27,440
 And sampadita means it's in the sense of sam, and sam-bad.

179
00:09:27,440 --> 00:09:34,120
 Sam-bad is like stronger.

180
00:09:34,120 --> 00:09:37,200
 Sam is fully.

181
00:09:37,200 --> 00:09:41,120
 Sam-bad is not strong.

182
00:09:41,120 --> 00:09:43,440
 Bad is to become.

183
00:09:43,440 --> 00:09:45,360
 Sam-bad become full.

184
00:09:45,360 --> 00:09:53,930
 So sam-badjati is to become full or to fulfill or to

185
00:09:53,930 --> 00:09:56,040
 succeed.

186
00:09:56,040 --> 00:09:59,040
 So it does have the sense of up, I think.

187
00:09:59,040 --> 00:10:02,360
 Not exactly, but there you go.

188
00:10:02,360 --> 00:10:06,120
 There's my translation.

189
00:10:06,120 --> 00:10:08,240
 Stuff breaks sober up.

190
00:10:08,240 --> 00:10:13,760
 You can make a poster for that too.

191
00:10:13,760 --> 00:10:15,360
 Put it on Facebook.

192
00:10:15,360 --> 00:10:17,760
 Stuff breaks sober up, the Buddha.

193
00:10:17,760 --> 00:10:21,440
 And then if anyone calls you out on it, then we'll get it

194
00:10:21,440 --> 00:10:23,400
 sent to fake Buddha quotes and

195
00:10:23,400 --> 00:10:25,720
 have him go at it.

196
00:10:25,720 --> 00:10:29,640
 And then we can all laugh at him when he says it's fake.

197
00:10:29,640 --> 00:10:32,280
 No, he's a good friend.

198
00:10:32,280 --> 00:10:33,280
 Don't do that.

199
00:10:33,280 --> 00:10:36,760
 But it'd be funny to have that conversation.

200
00:10:36,760 --> 00:10:39,800
 Hello, Bante.

201
00:10:39,800 --> 00:10:44,290
 I recently attained third path by following instructions

202
00:10:44,290 --> 00:10:47,000
 from Mahasya Sayadaw's practical

203
00:10:47,000 --> 00:10:48,000
 insight meditation.

204
00:10:48,000 --> 00:10:51,270
 But I still feel pleasant sensations when I perceive

205
00:10:51,270 --> 00:10:53,640
 pleasant objects and feel sensations

206
00:10:53,640 --> 00:10:57,260
 of wanting to incline towards pleasant objects, although

207
00:10:57,260 --> 00:10:59,820
 not attached in the same way as before.

208
00:10:59,820 --> 00:11:02,650
 Could you please share your own experience post third path

209
00:11:02,650 --> 00:11:04,240
 with respect to sensual desire

210
00:11:04,240 --> 00:11:09,520
 and whether fourth path makes a difference in perceiving

211
00:11:09,520 --> 00:11:11,520
 pleasant objects?

212
00:11:11,520 --> 00:11:15,090
 Although I still experience theorizing of a thought that

213
00:11:15,090 --> 00:11:16,960
 that object is pleasant when

214
00:11:16,960 --> 00:11:22,600
 contracting pleasant objects, contacting pleasant objects.

215
00:11:22,600 --> 00:11:25,990
 I mean, it doesn't sound like you've reached the third path

216
00:11:25,990 --> 00:11:26,240
.

217
00:11:26,240 --> 00:11:29,200
 The third fruit, actually, path is just one moment.

218
00:11:29,200 --> 00:11:33,370
 So if you've attained the third path, you've also attained

219
00:11:33,370 --> 00:11:34,800
 the third fruit.

220
00:11:34,800 --> 00:11:38,800
 This path is only one moment.

221
00:11:38,800 --> 00:11:49,400
 It's followed immediately and necessarily by third fruit.

222
00:11:49,400 --> 00:11:54,460
 Any sensations of wanting to incline towards pleasant

223
00:11:54,460 --> 00:11:56,920
 objects are a sign of lobha, I mean,

224
00:11:56,920 --> 00:11:57,920
 of kamaraga.

225
00:11:57,920 --> 00:12:04,600
 So this is someone, my feeling is that it's someone who

226
00:12:04,600 --> 00:12:08,120
 hasn't attained anagami.

227
00:12:08,120 --> 00:12:12,160
 As to my own attainments, how do you know I've attained

228
00:12:12,160 --> 00:12:13,240
 third path?

229
00:12:13,240 --> 00:12:17,240
 Whoever told you that?

230
00:12:17,240 --> 00:12:24,000
 It's not something ordinary to become an anagami.

231
00:12:24,000 --> 00:12:28,560
 It's actually probably pretty rare in this day and age.

232
00:12:28,560 --> 00:12:32,810
 But I have a policy not to talk about myself, so I can't

233
00:12:32,810 --> 00:12:33,600
 answer.

234
00:12:33,600 --> 00:12:34,600
 Next question.

235
00:12:34,600 --> 00:12:40,080
 Dear Bhante, my other leg is on the path.

236
00:12:40,080 --> 00:12:44,100
 My other leg is on the path, the other one in sensual world

237
00:12:44,100 --> 00:12:44,240
.

238
00:12:44,240 --> 00:12:47,420
 When trying to progress on the path, for example,

239
00:12:47,420 --> 00:12:49,440
 practicing a lot during a few days and being

240
00:12:49,440 --> 00:12:52,640
 mostly alone, and afterwards handling lay life issues,

241
00:12:52,640 --> 00:12:54,960
 going to the bank and meeting people,

242
00:12:54,960 --> 00:12:56,880
 et cetera, I feel miserable.

243
00:12:56,880 --> 00:12:59,160
 I see how worthless everything is.

244
00:12:59,160 --> 00:13:01,820
 Then I feel I'm making people I meet feel miserable just by

245
00:13:01,820 --> 00:13:03,120
 interacting with them in

246
00:13:03,120 --> 00:13:06,820
 the way of not being capable to show my interest whatsoever

247
00:13:06,820 --> 00:13:06,960
.

248
00:13:06,960 --> 00:13:11,570
 Then making them miserable makes me want to adjust myself

249
00:13:11,570 --> 00:13:13,680
 by what I feel is creating

250
00:13:13,680 --> 00:13:16,670
 delusional ideals about reality in order to make those

251
00:13:16,670 --> 00:13:18,520
 people find me less intimidating

252
00:13:18,520 --> 00:13:21,200
 so they will feel better.

253
00:13:21,200 --> 00:13:24,440
 This seems to take me backwards on the path, so this is a

254
00:13:24,440 --> 00:13:26,320
 kind of a seesawing motion.

255
00:13:26,320 --> 00:13:27,320
 What can I do?

256
00:13:27,320 --> 00:13:30,210
 Eventually, I must totally dive into the path and let go

257
00:13:30,210 --> 00:13:32,080
 completely, but I can't do that

258
00:13:32,080 --> 00:13:33,080
 yet.

259
00:13:33,080 --> 00:13:39,160
 How to look at this situation?

260
00:13:39,160 --> 00:13:47,040
 You go from where you are.

261
00:13:47,040 --> 00:13:48,960
 The path starts from where you are.

262
00:13:48,960 --> 00:13:55,120
 That's really the important thing to get across.

263
00:13:55,120 --> 00:14:01,670
 We moan and complain because we're not able to start where

264
00:14:01,670 --> 00:14:04,560
 we want, but the path is right

265
00:14:04,560 --> 00:14:05,560
 in front of you.

266
00:14:05,560 --> 00:14:07,080
 It starts right where you are.

267
00:14:07,080 --> 00:14:09,280
 It doesn't mean you have to become a monk.

268
00:14:09,280 --> 00:14:11,920
 It's not all or nothing.

269
00:14:11,920 --> 00:14:15,960
 That would be jumping from where you are to somewhere else.

270
00:14:15,960 --> 00:14:20,330
 In fact, becoming a monk often doesn't solve the problem at

271
00:14:20,330 --> 00:14:20,960
 all.

272
00:14:20,960 --> 00:14:26,640
 The only way out is through the practice of mindfulness.

273
00:14:26,640 --> 00:14:56,480
 The Buddha's advice still applies.

274
00:14:56,480 --> 00:15:18,520
 What

275
00:15:18,520 --> 00:15:45,880
 do you

276
00:15:45,880 --> 00:16:08,240
 think

277
00:16:08,240 --> 00:16:24,600
 is

278
00:16:24,600 --> 00:16:46,680
 a

279
00:16:46,680 --> 00:17:12,760
 very

280
00:17:12,760 --> 00:17:23,360
 non-committal?

281
00:17:23,360 --> 00:17:44,720
 So

282
00:17:44,720 --> 00:17:56,360
 what

283
00:17:56,360 --> 00:18:06,800
 is

284
00:18:06,800 --> 00:18:28,880
 a

285
00:18:28,880 --> 00:18:45,240
 very

286
00:18:45,240 --> 00:19:03,640
 important

287
00:19:03,640 --> 00:19:25,720
 one

288
00:19:25,720 --> 00:19:47,800
 is

289
00:19:47,800 --> 00:20:09,840
 what

290
00:20:09,840 --> 00:20:31,880
 is

291
00:20:31,880 --> 00:20:53,920
 a

292
00:20:53,920 --> 00:21:15,960
 does

293
00:21:15,960 --> 00:21:41,600
 that

294
00:21:41,600 --> 00:21:57,960
 is

295
00:21:57,960 --> 00:22:20,000
 a

296
00:22:20,000 --> 00:22:42,040
 very

297
00:22:42,040 --> 00:23:11,080
 important

