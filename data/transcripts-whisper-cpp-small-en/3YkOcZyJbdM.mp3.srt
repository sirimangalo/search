1
00:00:00,000 --> 00:00:14,720
 Good evening everyone.

2
00:00:14,720 --> 00:00:17,840
 Welcome to our evening Dhamma.

3
00:00:17,840 --> 00:00:21,520
 I have questions and answers tonight.

4
00:00:21,520 --> 00:00:33,170
 I'm using a new microphone so hopefully this is a little

5
00:00:33,170 --> 00:00:35,080
 louder.

6
00:00:35,080 --> 00:00:40,440
 Without further ado, we'll get right into questions.

7
00:00:40,440 --> 00:00:42,680
 How do you know the difference between having right view

8
00:00:42,680 --> 00:00:45,000
 and being judgmental?

9
00:00:45,000 --> 00:00:50,400
 Let's take the question first and we'll deal with your

10
00:00:50,400 --> 00:00:52,600
 story separately.

11
00:00:52,600 --> 00:01:01,720
 First let's make sure this interface is working.

12
00:01:01,720 --> 00:01:08,720
 Okay, it is.

13
00:01:08,720 --> 00:01:14,600
 Right view and being judgmental.

14
00:01:14,600 --> 00:01:16,950
 So what right view is, why I want to take this separately

15
00:01:16,950 --> 00:01:18,120
 from your example is because

16
00:01:18,120 --> 00:01:21,800
 I don't think your example has much to do with, well, I

17
00:01:21,800 --> 00:01:24,000
 understand what you're getting

18
00:01:24,000 --> 00:01:28,040
 at but let's be clear about what right view is.

19
00:01:28,040 --> 00:01:31,920
 Right view is dealing with belief.

20
00:01:31,920 --> 00:01:37,070
 So your belief that something is true, that something is

21
00:01:37,070 --> 00:01:38,000
 right.

22
00:01:38,000 --> 00:01:42,070
 True, I guess, is of course the most simple way of

23
00:01:42,070 --> 00:01:44,640
 understanding right view.

24
00:01:44,640 --> 00:01:48,440
 In Buddhism it's belief in four truths.

25
00:01:48,440 --> 00:01:51,480
 The four noble truths, right?

26
00:01:51,480 --> 00:01:54,630
 Belief in suffering, the cause of suffering, the cessation

27
00:01:54,630 --> 00:01:56,400
 of suffering, and the path which

28
00:01:56,400 --> 00:01:58,080
 leads to the cessation of suffering.

29
00:01:58,080 --> 00:02:02,500
 So I don't know if it's being pedantic or nitpicking but I

30
00:02:02,500 --> 00:02:07,120
 like to be clear about terms.

31
00:02:07,120 --> 00:02:11,670
 Otherwise it becomes, doubt arises more easily if you're

32
00:02:11,670 --> 00:02:14,080
 not clear about your terms.

33
00:02:14,080 --> 00:02:19,110
 So your example is of being invited to a wedding and as a

34
00:02:19,110 --> 00:02:22,560
 result of practice have no interest

35
00:02:22,560 --> 00:02:25,520
 in drinking, dancing, beautifying yourself anymore.

36
00:02:25,520 --> 00:02:28,880
 So you don't want to go but feel obliged to.

37
00:02:28,880 --> 00:02:31,380
 How do you know if this is a result of your practice or is

38
00:02:31,380 --> 00:02:32,640
 it my ego using the path as

39
00:02:32,640 --> 00:02:37,680
 an excuse not to go?

40
00:02:37,680 --> 00:02:42,580
 So the only thing, the only ways to be sure about

41
00:02:42,580 --> 00:02:46,640
 situations like this is to understand

42
00:02:46,640 --> 00:02:49,960
 the moments of experience involved.

43
00:02:49,960 --> 00:02:54,610
 Now not wanting to go somewhere, I mean it's just words and

44
00:02:54,610 --> 00:02:56,760
 it's a phrase that we use and

45
00:02:56,760 --> 00:03:01,450
 language is of course limited but if you actually dislike,

46
00:03:01,450 --> 00:03:04,080
 if disliking arises in the mind,

47
00:03:04,080 --> 00:03:07,170
 an aversion arises in the mind when you think about going,

48
00:03:07,170 --> 00:03:08,920
 then that's a state of aversion

49
00:03:08,920 --> 00:03:11,760
 and that's wrong.

50
00:03:11,760 --> 00:03:13,160
 But I would say that's just aversion.

51
00:03:13,160 --> 00:03:19,310
 As far as ego, I guess there's the idea that you might be

52
00:03:19,310 --> 00:03:22,880
 trying to convince yourself,

53
00:03:22,880 --> 00:03:27,380
 use it as an excuse which I don't know that I'd exactly

54
00:03:27,380 --> 00:03:31,000
 call it ego, I'd call it manipulation,

55
00:03:31,000 --> 00:03:35,350
 manipulating yourself, lying to yourself basically, a way

56
00:03:35,350 --> 00:03:36,680
 of justifying.

57
00:03:36,680 --> 00:03:41,030
 I don't exactly see it as ego but it's a kind of a delusion

58
00:03:41,030 --> 00:03:42,080
 for sure.

59
00:03:42,080 --> 00:03:46,160
 You convince yourself that yeah, yeah it's just because I'm

60
00:03:46,160 --> 00:03:47,800
 a meditator that I don't

61
00:03:47,800 --> 00:03:48,800
 want to go.

62
00:03:48,800 --> 00:03:54,400
 I think that's common actually for a meditator to make

63
00:03:54,400 --> 00:03:58,360
 excuses for themselves because they

64
00:03:58,360 --> 00:04:01,380
 know that something is wrong but they find a way to say oh

65
00:04:01,380 --> 00:04:02,840
 well it's just because of

66
00:04:02,840 --> 00:04:05,080
 this or it's just because of that.

67
00:04:05,080 --> 00:04:08,650
 It's not terrible, I mean it's a defense mechanism but of

68
00:04:08,650 --> 00:04:10,840
 course ideally you wouldn't be doing

69
00:04:10,840 --> 00:04:14,040
 that, you'd just be aware of your state of dislike.

70
00:04:14,040 --> 00:04:17,800
 I mean an enlightened person would not dislike going to a

71
00:04:17,800 --> 00:04:20,240
 wedding, they would be clear about

72
00:04:20,240 --> 00:04:24,900
 their stance on drinking and have no interest in drinking

73
00:04:24,900 --> 00:04:25,960
 alcohol.

74
00:04:25,960 --> 00:04:29,930
 But I think in certain cases, in an arahant is a whole

75
00:04:29,930 --> 00:04:32,280
 other issue but someone who is

76
00:04:32,280 --> 00:04:36,670
 a sotapana who would never drink alcohol might still very

77
00:04:36,670 --> 00:04:38,480
 well go to a wedding.

78
00:04:38,480 --> 00:04:43,170
 In beautifying themselves, just to fit in, unless they're a

79
00:04:43,170 --> 00:04:44,600
 monk of course.

80
00:04:44,600 --> 00:04:50,080
 But if you're a lay person, I think it would be beautifying

81
00:04:50,080 --> 00:04:53,120
 in the sense of wearing a simple

82
00:04:53,120 --> 00:04:56,800
 dress or a suit or something like that.

83
00:04:56,800 --> 00:05:00,300
 Some clothes that were going to fit in and weren't going to

84
00:05:00,300 --> 00:05:02,160
 cause unnecessary distress.

85
00:05:02,160 --> 00:05:06,920
 It doesn't seem like a problem.

86
00:05:06,920 --> 00:05:10,330
 I mean to that extent, as far as beautifying I can see how

87
00:05:10,330 --> 00:05:12,040
 there would be a problem if

88
00:05:12,040 --> 00:05:16,330
 it meant about putting on excessive makeup or makeup at all

89
00:05:16,330 --> 00:05:17,200
 really.

90
00:05:17,200 --> 00:05:25,030
 Makeup's an interesting subject, I've had Thai people ask

91
00:05:25,030 --> 00:05:27,320
 me about this.

92
00:05:27,320 --> 00:05:31,030
 And I can see an argument for saying hey makeup's just

93
00:05:31,030 --> 00:05:33,320
 wrong to wear it at all as a Buddhist.

94
00:05:33,320 --> 00:05:36,220
 But of course it's not against the five precepts but if you

95
00:05:36,220 --> 00:05:38,120
 think about it as the eight precepts

96
00:05:38,120 --> 00:05:41,720
 it's really not a good thing.

97
00:05:41,720 --> 00:05:44,320
 But then there's the argument of fitting in which I think

98
00:05:44,320 --> 00:05:46,120
 applies here in the wedding.

99
00:05:46,120 --> 00:05:53,310
 You might just as well try and blend in and not make anyone

100
00:05:53,310 --> 00:05:55,840
 get angry at you.

101
00:05:55,840 --> 00:05:58,770
 But I don't think it really has to do with wrong view or

102
00:05:58,770 --> 00:05:59,720
 ego as I said.

103
00:05:59,720 --> 00:06:06,080
 Though perhaps there's something I'm not quite clear on.

104
00:06:06,080 --> 00:06:09,480
 Either way the most important is to be aware of moments,

105
00:06:09,480 --> 00:06:11,440
 moments of consciousness, in this

106
00:06:11,440 --> 00:06:18,840
 case not wanting to do something.

107
00:06:18,840 --> 00:06:21,840
 Something's wrong with the interface.

108
00:06:21,840 --> 00:06:41,320
 I know I'm logged out.

109
00:06:41,320 --> 00:06:42,320
 Where's our IT team?

110
00:06:42,320 --> 00:06:53,360
 This isn't working.

111
00:06:53,360 --> 00:07:08,400
 Computers hey?

112
00:07:08,400 --> 00:07:12,080
 So I click on a button and it starts to load and it doesn't

113
00:07:12,080 --> 00:07:13,560
 ever get anywhere.

114
00:07:13,560 --> 00:07:17,330
 Then I refresh the page and it's done what it was supposed

115
00:07:17,330 --> 00:07:17,960
 to do.

116
00:07:17,960 --> 00:07:21,920
 But only after I refresh the page.

117
00:07:21,920 --> 00:07:26,720
 Why is that?

118
00:07:26,720 --> 00:07:29,520
 Is it possible to have extreme mood swings from meditating

119
00:07:29,520 --> 00:07:30,000
 a lot?

120
00:07:30,000 --> 00:07:33,030
 Some days I feel extraordinarily blissful, everything is

121
00:07:33,030 --> 00:07:33,840
 wonderful.

122
00:07:33,840 --> 00:07:36,600
 And then the other days I feel sad and depressed.

123
00:07:36,600 --> 00:07:40,520
 And perhaps it's a mental illness.

124
00:07:40,520 --> 00:07:44,130
 Sadness and depression are by Buddhist definition mental

125
00:07:44,130 --> 00:07:46,360
 illnesses in and of themselves.

126
00:07:46,360 --> 00:07:50,200
 Anytime you're sad even a little bit that's mental illness.

127
00:07:50,200 --> 00:07:52,320
 The mind is not healthy in that moment.

128
00:07:52,320 --> 00:07:59,540
 That mind is unhealthy for life or one way of describing it

129
00:07:59,540 --> 00:08:00,080
.

130
00:08:00,080 --> 00:08:03,600
 On the other hand feeling blissful can lead to mental

131
00:08:03,600 --> 00:08:05,800
 illness as well of attachment to

132
00:08:05,800 --> 00:08:06,800
 it, liking it.

133
00:08:06,800 --> 00:08:11,350
 I would say extreme mood swings are often, I mean they can

134
00:08:11,350 --> 00:08:13,560
 just be old past habits but

135
00:08:13,560 --> 00:08:17,930
 they're more likely to be caused by appreciation of the

136
00:08:17,930 --> 00:08:21,240
 blissful states, the wonderful states,

137
00:08:21,240 --> 00:08:22,520
 the states that you like.

138
00:08:22,520 --> 00:08:31,590
 And when you like them a lot the lack of them or the

139
00:08:31,590 --> 00:08:38,560
 ordinary states become unbearable and

140
00:08:38,560 --> 00:08:42,240
 that leads to depression, sadness and so on.

141
00:08:42,240 --> 00:08:47,060
 So try to let go of your attachment to both of them, to

142
00:08:47,060 --> 00:08:49,720
 good states, bad states.

143
00:08:49,720 --> 00:08:53,680
 If you want to feel experience, true peace, true happiness,

144
00:08:53,680 --> 00:08:55,720
 it can't be dependent on anything

145
00:08:55,720 --> 00:08:59,280
 because your feelings are impermanent.

146
00:08:59,280 --> 00:09:03,130
 I mean what you're experiencing is impermanence, the fact

147
00:09:03,130 --> 00:09:05,560
 that nothing lasts forever and once

148
00:09:05,560 --> 00:09:08,920
 it's gone all you're left with is your expectations.

149
00:09:08,920 --> 00:09:26,300
 If you have expectations that it will stay when it's gone

150
00:09:26,300 --> 00:09:32,280
 then you suffer.

151
00:09:32,280 --> 00:09:40,360
 This interface is very broken, I don't know what's going on

152
00:09:40,360 --> 00:09:40,920
.

153
00:09:40,920 --> 00:10:01,000
 I should check this.

154
00:10:01,000 --> 00:10:16,680
 Nothing wrong with a token, I have no idea what that means.

155
00:10:16,680 --> 00:10:21,320
 Firefox can't establish a connection to the server at WSS

156
00:10:21,320 --> 00:10:24,440
 meditation.seriamongolo.org

157
00:10:24,440 --> 00:10:27,160
 socket.io.

158
00:10:27,160 --> 00:10:33,680
 Somebody call the IT team, something's wrong.

159
00:10:33,680 --> 00:10:38,760
 Socket.io is not working, whatever that is.

160
00:10:38,760 --> 00:10:46,360
 I'm going to answer these but I'll stop pushing buttons.

161
00:10:46,360 --> 00:10:51,360
 Who needs buttons right?

162
00:10:51,360 --> 00:10:54,080
 Does a very advanced meditator see reality as if watching a

163
00:10:54,080 --> 00:10:55,320
 movie frame by frame, each

164
00:10:55,320 --> 00:11:01,080
 frame being a jitta?

165
00:11:01,080 --> 00:11:07,610
 Not exactly a jitta because it takes many jitta to form one

166
00:11:07,610 --> 00:11:09,480
 experience.

167
00:11:09,480 --> 00:11:14,720
 So one would see experiences moment by moment.

168
00:11:14,720 --> 00:11:18,350
 But to see each moment is not possible because the seeing

169
00:11:18,350 --> 00:11:20,760
 aspect is only a part of the process.

170
00:11:20,760 --> 00:11:26,210
 The experience, the awareness of the mind itself, of what's

171
00:11:26,210 --> 00:11:28,480
 going on, the part where

172
00:11:28,480 --> 00:11:32,240
 mindfulness comes into play is only part of the experience.

173
00:11:32,240 --> 00:11:37,600
 So it's not each jitta but it is possible to.

174
00:11:37,600 --> 00:11:42,120
 It's more complicated than what you're suggesting.

175
00:11:42,120 --> 00:11:45,480
 Because the awareness is part of the experience.

176
00:11:45,480 --> 00:11:54,310
 So awareness arises and mindfulness arises but it's only

177
00:11:54,310 --> 00:11:58,800
 ever a part of the process.

178
00:11:58,800 --> 00:12:02,410
 But one certainly sees arising and sees moments of

179
00:12:02,410 --> 00:12:05,840
 experience, processes of experience coming

180
00:12:05,840 --> 00:12:09,840
 and going.

181
00:12:09,840 --> 00:12:17,810
 Okay, so brain stem surgery to remove a lesion on

182
00:12:17,810 --> 00:12:20,400
 medication.

183
00:12:20,400 --> 00:12:24,000
 Is it okay to start meditating after surgery on medication?

184
00:12:24,000 --> 00:12:27,240
 Yeah, not on medication.

185
00:12:27,240 --> 00:12:33,250
 I mean it's okay to meditate but don't expect, I would

186
00:12:33,250 --> 00:12:36,160
 expect that benefits.

187
00:12:36,160 --> 00:12:39,100
 It depends on what kind of medication, if it's brain

188
00:12:39,100 --> 00:12:40,960
 medication then you might have

189
00:12:40,960 --> 00:12:41,960
 a problem.

190
00:12:41,960 --> 00:12:45,570
 You might have a problem, I mean of course, always meditate

191
00:12:45,570 --> 00:12:47,360
 no matter what situation.

192
00:12:47,360 --> 00:12:53,400
 But yeah, let's just say yes it's fine.

193
00:12:53,400 --> 00:12:55,400
 Mindfulness is always good.

194
00:12:55,400 --> 00:12:59,760
 Satinsha ko han vikkha vesa bhatikhan vadami.

195
00:12:59,760 --> 00:13:07,760
 This I tell you monks is always beneficial.

196
00:13:07,760 --> 00:13:10,890
 How do lay meditators deal with cutting down on sleep and

197
00:13:10,890 --> 00:13:12,920
 feeling energetic the next day?

198
00:13:12,920 --> 00:13:18,850
 Well they would be meditating many hours a day and be

199
00:13:18,850 --> 00:13:22,560
 trained to have a clear mind.

200
00:13:22,560 --> 00:13:30,910
 To be constantly in a state of equanimity, of freedom from

201
00:13:30,910 --> 00:13:34,560
 stress because there's no

202
00:13:34,560 --> 00:13:35,560
 reaction.

203
00:13:35,560 --> 00:13:41,400
 So as a result there's need for much less sleep.

204
00:13:41,400 --> 00:13:45,320
 How should one read or write without losing mindfulness?

205
00:13:45,320 --> 00:13:47,340
 Well when you're reading your mind is engaged in a

206
00:13:47,340 --> 00:13:48,320
 different process.

207
00:13:48,320 --> 00:13:51,870
 Now you can intermittently be mindful but there's also

208
00:13:51,870 --> 00:13:53,960
 other activities going on like

209
00:13:53,960 --> 00:13:58,680
 reflection and the processing of the text.

210
00:13:58,680 --> 00:14:03,640
 So it's not that you can be mindful no matter what you do.

211
00:14:03,640 --> 00:14:07,980
 I would say because reading is a different mind process, at

212
00:14:07,980 --> 00:14:09,840
 the very least it's very

213
00:14:09,840 --> 00:14:10,840
 difficult.

214
00:14:10,840 --> 00:14:15,170
 But I mean once you're very trained of course mindfulness

215
00:14:15,170 --> 00:14:17,800
 can certainly be a part of it.

216
00:14:17,800 --> 00:14:20,710
 Using your emotions, using mindfulness, your reactions,

217
00:14:20,710 --> 00:14:22,040
 your thoughts about it.

218
00:14:22,040 --> 00:14:27,240
 Being aware of how it affects you.

219
00:14:27,240 --> 00:14:31,110
 Just like anything be aware of what's going on and be

220
00:14:31,110 --> 00:14:33,400
 mindful of what's going on.

221
00:14:33,400 --> 00:14:38,800
 You know in the trains of thought being mindful of them

222
00:14:38,800 --> 00:14:42,840
 thinking, thinking, reflecting.

223
00:14:42,840 --> 00:14:46,460
 Is it necessary to sleep mindfully or can mindfulness

224
00:14:46,460 --> 00:14:48,960
 prevent one from falling asleep?

225
00:14:48,960 --> 00:14:50,880
 It can prevent you from falling asleep.

226
00:14:50,880 --> 00:14:54,690
 It's possible that you stay up later because you're mindful

227
00:14:54,690 --> 00:14:54,980
.

228
00:14:54,980 --> 00:14:58,400
 Because mindfulness can give you energy it can at times.

229
00:14:58,400 --> 00:15:01,960
 Just focusing your attention can give you energy.

230
00:15:01,960 --> 00:15:05,400
 But if you sleep mindfully the thing with that is your mind

231
00:15:05,400 --> 00:15:06,920
 is already in the sort of

232
00:15:06,920 --> 00:15:08,840
 a focused state.

233
00:15:08,840 --> 00:15:13,840
 And so the sleep is much more peaceful generally with

234
00:15:13,840 --> 00:15:15,160
 practice.

235
00:15:15,160 --> 00:15:21,800
 Much more productive because your mind is already set up.

236
00:15:21,800 --> 00:15:25,680
 You set the tone for the sleep.

237
00:15:25,680 --> 00:15:27,400
 Is it possible to meditate too much?

238
00:15:27,400 --> 00:15:32,120
 If so how does one know if one is meditating too much?

239
00:15:32,120 --> 00:15:34,900
 It's not possible to be too mindful but you know if you're

240
00:15:34,900 --> 00:15:36,320
 talking about just sitting

241
00:15:36,320 --> 00:15:41,420
 and meditating if you do too much if you do a lot of it it

242
00:15:41,420 --> 00:15:44,000
 can become stressful.

243
00:15:44,000 --> 00:15:49,190
 If you're not actually being mindful as a practitioner you

244
00:15:49,190 --> 00:15:51,660
're practicing you're learning

245
00:15:51,660 --> 00:15:53,680
 how to be mindful.

246
00:15:53,680 --> 00:15:56,540
 And so a lot of the time you're probably not mindful and

247
00:15:56,540 --> 00:15:58,280
 that can get quite stressful as

248
00:15:58,280 --> 00:16:01,320
 it builds up.

249
00:16:01,320 --> 00:16:06,850
 So no I would say it's something you want to work up to and

250
00:16:06,850 --> 00:16:09,280
 over time when you're in

251
00:16:09,280 --> 00:16:13,370
 the meditation center or in a monastery over time you med

252
00:16:13,370 --> 00:16:15,640
itate more and meditate until

253
00:16:15,640 --> 00:16:22,640
 you're mindful and practicing all the time.

254
00:16:22,640 --> 00:16:24,440
 Night and day.

255
00:16:24,440 --> 00:16:26,860
 Is it okay to observe and note the heart beat as part of

256
00:16:26,860 --> 00:16:27,720
 the practice?

257
00:16:27,720 --> 00:16:30,360
 Yes that's fine.

258
00:16:30,360 --> 00:16:33,640
 You have a large gap between the falling and then the next

259
00:16:33,640 --> 00:16:34,320
 rising.

260
00:16:34,320 --> 00:16:37,950
 What we would normally tell people to do then is to say

261
00:16:37,950 --> 00:16:40,320
 rising, falling, sitting.

262
00:16:40,320 --> 00:16:44,840
 Rising, falling, sitting.

263
00:16:44,840 --> 00:16:50,760
 But noting the heart beat is fine it's not something that

264
00:16:50,760 --> 00:16:54,360
 we would use as a basic meditation

265
00:16:54,360 --> 00:17:03,480
 object but note it when you feel it.

266
00:17:03,480 --> 00:17:06,040
 And then whenever else arises in the gap you can also note

267
00:17:06,040 --> 00:17:07,680
 it but rising, falling, sitting

268
00:17:07,680 --> 00:17:09,960
 is a common one.

269
00:17:09,960 --> 00:17:14,860
 That's what we would teach people as part of our meditation

270
00:17:14,860 --> 00:17:15,800
 course.

271
00:17:15,800 --> 00:17:17,960
 Do you think that using mantras in vipassana takes some

272
00:17:17,960 --> 00:17:19,440
 amount of attention away from the

273
00:17:19,440 --> 00:17:20,440
 actual experience?

274
00:17:20,440 --> 00:17:23,840
 Didn't we have this quenstion recently?

275
00:17:23,840 --> 00:17:26,520
 I mean it purposefully takes something away from the

276
00:17:26,520 --> 00:17:27,360
 experience.

277
00:17:27,360 --> 00:17:29,560
 It stops you from seeing the details.

278
00:17:29,560 --> 00:17:32,480
 It stops you from getting caught up in the details.

279
00:17:32,480 --> 00:17:36,450
 The Buddha said, that's right in one of the suttas, the

280
00:17:36,450 --> 00:17:39,320
 Buddha said, "Nanyimita gahi nano

281
00:17:39,320 --> 00:17:45,560
 bhyanjana gahi" one doesn't grasp at the particular, the

282
00:17:45,560 --> 00:17:48,920
 signs are the particulars of the experience.

283
00:17:48,920 --> 00:17:52,240
 So seeing is just seeing.

284
00:17:52,240 --> 00:17:55,130
 So I don't think it's fair to say it takes any attention

285
00:17:55,130 --> 00:17:57,160
 away from the actual experience.

286
00:17:57,160 --> 00:18:02,080
 It's meant to remind us what it is that we're actually

287
00:18:02,080 --> 00:18:05,640
 experiencing so that we don't get

288
00:18:05,640 --> 00:18:15,240
 carried away by some extrapolation or some thought about it

289
00:18:15,240 --> 00:18:15,880
.

290
00:18:15,880 --> 00:18:18,760
 But the last question in a session, no it never happened.

291
00:18:18,760 --> 00:18:23,040
 So these questions are all from that.

292
00:18:23,040 --> 00:18:27,730
 We're doing, something very strange happened within my

293
00:18:27,730 --> 00:18:30,600
 computer so it's not very strange

294
00:18:30,600 --> 00:18:33,040
 that something should happen.

295
00:18:33,040 --> 00:18:37,280
 Computers, there's always something happening.

296
00:18:37,280 --> 00:18:40,360
 Should I distinguish between namal rupa when seeing,

297
00:18:40,360 --> 00:18:43,200
 hearing, touching, tasting and smelling?

298
00:18:43,200 --> 00:18:46,120
 You shouldn't actively, consciously do that.

299
00:18:46,120 --> 00:18:51,770
 You should just try and be mindful, seeing, seeing, hearing

300
00:18:51,770 --> 00:18:53,120
, hearing.

301
00:18:53,120 --> 00:18:57,450
 But you'll experience namal rupa, there will be awareness

302
00:18:57,450 --> 00:18:59,760
 that you're aware, which is the

303
00:18:59,760 --> 00:19:06,190
 namal and there will be the awareness of the visual, which

304
00:19:06,190 --> 00:19:07,480
 is rupa.

305
00:19:07,480 --> 00:19:12,480
 That's not important to discriminate or think about.

306
00:19:12,480 --> 00:19:14,800
 Let that happen by itself.

307
00:19:14,800 --> 00:19:16,800
 Let's try and note.

308
00:19:16,800 --> 00:19:21,680
 Okay, I'm not going to answer this one.

309
00:19:21,680 --> 00:19:22,680
 Meditate.

310
00:19:22,680 --> 00:19:26,150
 How to understand that pleasant feeling is not good or

311
00:19:26,150 --> 00:19:28,480
 positive or similarly unpleasant

312
00:19:28,480 --> 00:19:30,480
 feeling is not bad or negative.

313
00:19:30,480 --> 00:19:33,710
 Well the real question is why do we think pleasant feelings

314
00:19:33,710 --> 00:19:35,040
 are positive and why do

315
00:19:35,040 --> 00:19:38,680
 we think unpleasant feelings are negative?

316
00:19:38,680 --> 00:19:41,410
 There's no need to understand how they're not that because

317
00:19:41,410 --> 00:19:42,440
 they're not that.

318
00:19:42,440 --> 00:19:45,110
 The fact that we think this sort of, this feeling is

319
00:19:45,110 --> 00:19:47,240
 positive and this feeling is negative

320
00:19:47,240 --> 00:19:48,240
 is absurd.

321
00:19:48,240 --> 00:19:52,370
 I mean there's no rational reason for it besides that we

322
00:19:52,370 --> 00:19:54,880
 like it, besides that we have a desire

323
00:19:54,880 --> 00:19:59,560
 for one and an aversion to the other.

324
00:19:59,560 --> 00:20:03,220
 So all we're doing in meditation is seeing that it's

325
00:20:03,220 --> 00:20:04,480
 meaningless.

326
00:20:04,480 --> 00:20:06,320
 Not only meaningless but harmful.

327
00:20:06,320 --> 00:20:12,400
 This partiality sets us up for suffering.

328
00:20:12,400 --> 00:20:15,680
 Sometimes I can see what lust is, just a passing feeling.

329
00:20:15,680 --> 00:20:18,960
 I can see how stupid I was to get attracted to beauty.

330
00:20:18,960 --> 00:20:21,120
 But other times the lust takes over me.

331
00:20:21,120 --> 00:20:23,320
 At that time I'm not able to be mindful.

332
00:20:23,320 --> 00:20:26,280
 Is my mind playing tricks with me or is it something else?

333
00:20:26,280 --> 00:20:29,090
 Yeah, so the mind is complicated and so you're seeing that

334
00:20:29,090 --> 00:20:30,720
 there is no mind and this is part

335
00:20:30,720 --> 00:20:33,360
 of what is meant by non-self.

336
00:20:33,360 --> 00:20:37,040
 Is that there's no you who is this way or that way.

337
00:20:37,040 --> 00:20:44,800
 There's only habits that are built up and change over time.

338
00:20:44,800 --> 00:20:47,010
 And so through meditation you're changing your habits, you

339
00:20:47,010 --> 00:20:48,840
're building up positive habits,

340
00:20:48,840 --> 00:20:54,390
 useful habits, clear conscious objective habits that are

341
00:20:54,390 --> 00:20:57,560
 going to lead you in the direction

342
00:20:57,560 --> 00:21:03,380
 of letting go to the point where you can become free from

343
00:21:03,380 --> 00:21:04,960
 suffering.

344
00:21:04,960 --> 00:21:09,960
 And it's difficult to observe the rising and falling.

345
00:21:09,960 --> 00:21:14,000
 Well difficulty is good in meditation but if it's too

346
00:21:14,000 --> 00:21:16,200
 subtle in the beginning, I mean

347
00:21:16,200 --> 00:21:21,800
 it's just because your attention is not sharp enough.

348
00:21:21,800 --> 00:21:25,660
 I mean subtle is not a problem but non-existent would be a

349
00:21:25,660 --> 00:21:27,800
 problem but subtle is fine.

350
00:21:27,800 --> 00:21:29,800
 You just know the subtle movement.

351
00:21:29,800 --> 00:21:31,520
 It will become more clear.

352
00:21:31,520 --> 00:21:37,020
 It should become more clear as you carry on with the

353
00:21:37,020 --> 00:21:38,520
 practice.

354
00:21:38,520 --> 00:21:40,480
 The abdomen is not terribly important.

355
00:21:40,480 --> 00:21:43,240
 It's just a good object.

356
00:21:43,240 --> 00:21:47,440
 You can also just note sitting, sitting and so on.

357
00:21:47,440 --> 00:21:53,400
 Note the feelings, note everything else as well.

358
00:21:53,400 --> 00:21:56,670
 We have two questions from a meditator here who wanted to

359
00:21:56,670 --> 00:21:58,520
 have these questions be public

360
00:21:58,520 --> 00:22:01,680
 I think.

361
00:22:01,680 --> 00:22:05,780
 Is it possible to make bad karma with good intentions but

362
00:22:05,780 --> 00:22:08,040
 followed by a wrong choice?

363
00:22:08,040 --> 00:22:12,520
 I think the point is that there is no wrong choice in the

364
00:22:12,520 --> 00:22:14,240
 sense of the action.

365
00:22:14,240 --> 00:22:25,810
 What is wrong is always the moment of intention, the state

366
00:22:25,810 --> 00:22:28,000
 of mind.

367
00:22:28,000 --> 00:22:33,500
 So an arahant could still do something that results in or

368
00:22:33,500 --> 00:22:36,800
 seemingly results in suffering

369
00:22:36,800 --> 00:22:39,880
 but they had no bad intention.

370
00:22:39,880 --> 00:22:40,880
 That's where the karma is.

371
00:22:40,880 --> 00:22:44,720
 Karma is in your bad intentions or your bad states of mind.

372
00:22:44,720 --> 00:22:49,790
 States of mind that are one of three things that are

373
00:22:49,790 --> 00:22:54,240
 associated with greed, anger or delusion

374
00:22:54,240 --> 00:23:02,760
 or some mix of greed and delusion.

375
00:23:02,760 --> 00:23:05,480
 Actually they all have delusion in them.

376
00:23:05,480 --> 00:23:09,440
 Greed anger or delusion.

377
00:23:09,440 --> 00:23:17,680
 So yeah karma is not the action.

378
00:23:17,680 --> 00:23:20,880
 The word karma is actually probably not the best word.

379
00:23:20,880 --> 00:23:22,800
 The Buddha just adapted it.

380
00:23:22,800 --> 00:23:26,000
 But he said karma is your mental position.

381
00:23:26,000 --> 00:23:35,840
 It's much like Jesus where Jesus said it's not whether you

382
00:23:35,840 --> 00:23:39,360
 commit adultery, it's whether

383
00:23:39,360 --> 00:23:43,520
 you have desire in the mind, that sort of thing.

384
00:23:43,520 --> 00:23:47,320
 It's the mind that is basically the same thing.

385
00:23:47,320 --> 00:23:49,940
 I'm not saying that Jesus was like Buddha because I don't

386
00:23:49,940 --> 00:23:51,200
 believe he was but I think

387
00:23:51,200 --> 00:23:58,560
 that's an interesting aspect of Christianity.

388
00:23:58,560 --> 00:24:01,240
 Could you explain the importance of walking meditation?

389
00:24:01,240 --> 00:24:04,760
 Why should it be as long as sitting?

390
00:24:04,760 --> 00:24:10,000
 Why before sitting?

391
00:24:10,000 --> 00:24:11,920
 And not after?

392
00:24:11,920 --> 00:24:15,400
 Often people just consider it like a time for relaxing

393
00:24:15,400 --> 00:24:16,880
 after meditation.

394
00:24:16,880 --> 00:24:22,650
 Okay, so the Buddha, this is actually in my booklet I think

395
00:24:22,650 --> 00:24:23,880
, I hope.

396
00:24:23,880 --> 00:24:30,240
 The Buddha listed five benefits of walking meditation.

397
00:24:30,240 --> 00:24:35,830
 The first is that it gives you strength to walk which was

398
00:24:35,830 --> 00:24:37,360
 important.

399
00:24:37,360 --> 00:24:40,080
 Everyone walked back then.

400
00:24:40,080 --> 00:24:44,880
 The second is that it gives you patience to work.

401
00:24:44,880 --> 00:24:51,560
 So it's useful in a practical sense.

402
00:24:51,560 --> 00:24:53,910
 Just for living your daily life, walking meditation is

403
00:24:53,910 --> 00:24:55,400
 great because it's monotonous.

404
00:24:55,400 --> 00:24:59,720
 It's a monotonous activity that builds endurance and builds

405
00:24:59,720 --> 00:25:02,120
 patience and builds stamina.

406
00:25:02,120 --> 00:25:08,390
 And so any other monotonous work that you have to do is

407
00:25:08,390 --> 00:25:11,640
 improved because of it.

408
00:25:11,640 --> 00:25:15,280
 The third is it allows you to digest food.

409
00:25:15,280 --> 00:25:18,000
 Sitting around all day is not good for your digestion.

410
00:25:18,000 --> 00:25:20,520
 Walking is, I guess, better for your digestion.

411
00:25:20,520 --> 00:25:23,080
 Makes sense.

412
00:25:23,080 --> 00:25:27,880
 Number four, it leads to, it helps work out many diseases.

413
00:25:27,880 --> 00:25:31,980
 There are a lot of people who talk about, meditators who

414
00:25:31,980 --> 00:25:34,680
 talk about their health improving

415
00:25:34,680 --> 00:25:37,640
 through walking meditation.

416
00:25:37,640 --> 00:25:40,240
 But the fifth reason is probably more to the point.

417
00:25:40,240 --> 00:25:43,710
 And the fifth reason is that the Buddha said that the

418
00:25:43,710 --> 00:25:46,160
 concentration that comes, or the

419
00:25:46,160 --> 00:25:51,230
 focus that comes from walking meditation, lasts a long time

420
00:25:51,230 --> 00:25:51,560
.

421
00:25:51,560 --> 00:25:54,280
 It's long lasting, that's how he put it.

422
00:25:54,280 --> 00:25:58,970
 So there's a sort of a strength and a lasting, a staying

423
00:25:58,970 --> 00:26:01,840
 power to the concentration that

424
00:26:01,840 --> 00:26:08,220
 is useful for when you go to do sitting meditation.

425
00:26:08,220 --> 00:26:12,760
 So we do walking first because it's actually a preparation

426
00:26:12,760 --> 00:26:15,280
 for the sitting meditation.

427
00:26:15,280 --> 00:26:17,470
 You find there's quite a difference when you haven't done

428
00:26:17,470 --> 00:26:18,720
 walking and when you have done

429
00:26:18,720 --> 00:26:20,720
 walking and just go to do sitting.

430
00:26:20,720 --> 00:26:23,710
 If you've done the walking first, your mind is already in

431
00:26:23,710 --> 00:26:25,560
 quite a good state and the sitting

432
00:26:25,560 --> 00:26:30,480
 is more immediately beneficial.

433
00:26:30,480 --> 00:26:32,560
 Why we do the same amount of walking and sitting?

434
00:26:32,560 --> 00:26:35,240
 I don't suppose it's exactly necessary.

435
00:26:35,240 --> 00:26:40,200
 My teacher is quite a stickler for this.

436
00:26:40,200 --> 00:26:42,360
 All the Buddha said is to do them in alternation.

437
00:26:42,360 --> 00:26:43,360
 He said do walking.

438
00:26:43,360 --> 00:26:46,290
 I mean the Buddha clearly said do walking and sitting

439
00:26:46,290 --> 00:26:47,240
 meditation.

440
00:26:47,240 --> 00:26:50,000
 A lot of people don't pay attention to that.

441
00:26:50,000 --> 00:26:54,320
 But the only time that I know of when he talked about, when

442
00:26:54,320 --> 00:26:57,160
 he gave a regimen, a daily regimen,

443
00:26:57,160 --> 00:27:01,660
 was clearly to do walking and sitting in alternation for 20

444
00:27:01,660 --> 00:27:05,120
 hours a day, besides eating I suppose.

445
00:27:05,120 --> 00:27:13,840
 But there is no mention of one being as long as the other.

446
00:27:13,840 --> 00:27:19,230
 The idea of making them as long as each other, I think it's

447
00:27:19,230 --> 00:27:25,200
 reasonable because they're different

448
00:27:25,200 --> 00:27:29,830
 because it ensures that you're not partial to one or the

449
00:27:29,830 --> 00:27:30,720
 other.

450
00:27:30,720 --> 00:27:34,010
 You'll find that at times you're partial to walking, at

451
00:27:34,010 --> 00:27:35,720
 other times you're partial to sitting.

452
00:27:35,720 --> 00:27:38,400
 And if it was up to you to decide how much you were going

453
00:27:38,400 --> 00:27:39,960
 to do, it would be very hard

454
00:27:39,960 --> 00:27:45,670
 to break free from that partiality or that judgment,

455
00:27:45,670 --> 00:27:47,760
 judging nature.

456
00:27:47,760 --> 00:27:53,340
 So keeping them equal is sort of a force, it's artificial,

457
00:27:53,340 --> 00:27:55,680
 which is good because it

458
00:27:55,680 --> 00:28:00,780
 forces you not to follow your heart, not to follow your

459
00:28:00,780 --> 00:28:03,840
 partialities, which is very much

460
00:28:03,840 --> 00:28:08,220
 a part of the test, the practice of challenging yourself,

461
00:28:08,220 --> 00:28:10,800
 forcing you to experience things

462
00:28:10,800 --> 00:28:13,560
 that you don't like so that you can, rather than change

463
00:28:13,560 --> 00:28:15,440
 them, you can change your reaction

464
00:28:15,440 --> 00:28:18,440
 to them.

465
00:28:18,440 --> 00:28:22,880
 Okay, and that's all the questions for tonight.

466
00:28:22,880 --> 00:28:26,800
 Thank you all for coming out.

467
00:28:26,800 --> 00:28:30,230
 I really appreciate that everyone is so interested in

468
00:28:30,230 --> 00:28:32,120
 meditation and Buddhism.

469
00:28:32,120 --> 00:28:35,690
 I apologize, I do delete some of the questions and people

470
00:28:35,690 --> 00:28:37,760
 might feel that that's unfair,

471
00:28:37,760 --> 00:28:42,080
 but some of the questions are, sometimes questions are too

472
00:28:42,080 --> 00:28:45,480
 technical and I'm not really keen to

473
00:28:45,480 --> 00:28:56,210
 delve into the technical aspects because I don't find them

474
00:28:56,210 --> 00:28:58,240
 as helpful.

475
00:28:58,240 --> 00:29:02,590
 And sometimes they're questions I've answered, sometimes

476
00:29:02,590 --> 00:29:05,160
 they're not terribly practical or

477
00:29:05,160 --> 00:29:10,960
 applicable to practice.

478
00:29:10,960 --> 00:29:13,910
 Okay so that's all for tonight, thank you all for tuning in

479
00:29:13,910 --> 00:29:14,160
.

480
00:29:14,160 --> 00:29:14,720
 Have a good night.

481
00:29:14,720 --> 00:29:15,220
 Bye.

482
00:29:15,220 --> 00:29:15,720
 Bye.

483
00:29:15,720 --> 00:29:16,220
 Bye.

484
00:29:16,220 --> 00:29:16,720
 Bye.

485
00:29:16,720 --> 00:29:17,220
 Bye.

486
00:29:17,220 --> 00:29:17,720
 Bye.

487
00:29:17,720 --> 00:29:18,220
 Bye.

488
00:29:18,220 --> 00:29:18,720
 Bye.

489
00:29:18,720 --> 00:29:19,220
 Bye.

