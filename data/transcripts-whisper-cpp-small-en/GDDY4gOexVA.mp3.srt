1
00:00:00,000 --> 00:00:05,780
 Welcome back to Ask a Monk. Next question comes from Bradco

2
00:00:05,780 --> 00:00:07,040
2x.

3
00:00:07,040 --> 00:00:12,000
 Dear Yutadamo, I started with samatha meditation as I read

4
00:00:12,000 --> 00:00:13,680
 a lot that you have to calm your mind

5
00:00:13,680 --> 00:00:16,460
 before starting to practice vipassana. What is your

6
00:00:16,460 --> 00:00:18,800
 interpretation of differences between these

7
00:00:18,800 --> 00:00:23,330
 two and about which should be first? Thank you. Okay, sam

8
00:00:23,330 --> 00:00:26,160
atha and vipassana. For those of you who

9
00:00:26,160 --> 00:00:29,540
 don't know, the word samatha means tranquility. The word v

10
00:00:29,540 --> 00:00:32,400
ipassana means seeing clearly or insight.

11
00:00:32,400 --> 00:00:37,030
 When you talk about samatha meditation as something

12
00:00:37,030 --> 00:00:39,600
 separate from vipassana meditation,

13
00:00:39,600 --> 00:00:44,150
 you're taking the discussion into a post-canonical realm.

14
00:00:44,150 --> 00:00:47,360
 For those of you who are not into Buddhist

15
00:00:47,360 --> 00:00:51,640
 terminology or even early Buddhist terminology, it means

16
00:00:51,640 --> 00:00:54,080
 you're taking it out of the realm of what

17
00:00:54,080 --> 00:00:56,530
 the Buddha actually said and into the realm of

18
00:00:56,530 --> 00:00:59,680
 interpretation. Now, I follow this interpretation,

19
00:00:59,680 --> 00:01:04,760
 but only in the sense that it helps to explain the original

20
00:01:04,760 --> 00:01:07,440
 words. So I don't mind talking about

21
00:01:07,440 --> 00:01:10,660
 this, but just so that you know that from the Buddha's

22
00:01:10,660 --> 00:01:13,040
 point of view, it doesn't seem at all

23
00:01:13,040 --> 00:01:18,050
 clear that one should separate the two at all. So in that

24
00:01:18,050 --> 00:01:21,040
 sense, anyone saying that you should

25
00:01:21,040 --> 00:01:24,600
 calm your mind before you practice vipassana is on a little

26
00:01:24,600 --> 00:01:27,040
 bit of shaky ground. Now,

27
00:01:27,040 --> 00:01:31,020
 you can do it that way, and there's certainly indication

28
00:01:31,020 --> 00:01:33,360
 that this is how people meditate even

29
00:01:33,360 --> 00:01:37,200
 in the Buddha's time. But what that means in a technical

30
00:01:37,200 --> 00:01:42,320
 sense, if you're following this dichotomy,

31
00:01:42,320 --> 00:01:47,690
 dualistic or I don't know, the two kinds of meditation, one

32
00:01:47,690 --> 00:01:49,920
 being samatha, one being vipassana,

33
00:01:49,920 --> 00:01:53,560
 then the technique is to not just spend some time calming

34
00:01:53,560 --> 00:01:56,320
 your mind. That's not what it means. And

35
00:01:56,320 --> 00:01:58,720
 for those people who say, okay, practice calming your mind

36
00:01:58,720 --> 00:02:00,480
 and then practice, it means entering

37
00:02:00,480 --> 00:02:05,430
 into a trance, fixing and focusing your mind so that you

38
00:02:05,430 --> 00:02:08,640
 can see one thing very clearly. And the

39
00:02:08,640 --> 00:02:10,720
 only way you can do that is to pick a concept. And this is

40
00:02:10,720 --> 00:02:12,000
 why I'm always talking about the

41
00:02:12,000 --> 00:02:15,380
 differences between concepts and reality. Because in vipass

42
00:02:15,380 --> 00:02:17,280
ana practice, we focus on reality,

43
00:02:17,840 --> 00:02:21,090
 in tranquility meditation, and there are these kinds of

44
00:02:21,090 --> 00:02:23,040
 meditation all over the world, you'll

45
00:02:23,040 --> 00:02:26,260
 find them and this is a useful classification. That's why I

46
00:02:26,260 --> 00:02:27,840
 follow these teachings is because

47
00:02:27,840 --> 00:02:30,780
 it's useful for us to say, that's not vipassana meditation,

48
00:02:30,780 --> 00:02:32,560
 that meditation is not going to help

49
00:02:32,560 --> 00:02:35,390
 you see clearly. And we can say exactly why that is,

50
00:02:35,390 --> 00:02:37,280
 because what you're focusing on is something

51
00:02:37,280 --> 00:02:40,440
 you've created in your mind, it's not real, it doesn't have

52
00:02:40,440 --> 00:02:42,240
 any of the qualities of ultimate

53
00:02:42,240 --> 00:02:45,050
 reality. It's not the same as watching your stomach rise

54
00:02:45,050 --> 00:02:46,880
 and fall. Why? Because when you

55
00:02:46,880 --> 00:02:49,230
 watch your stomach rise and fall, it's changing all the

56
00:02:49,230 --> 00:02:51,440
 time. It's unsatisfying, it helps you to see

57
00:02:51,440 --> 00:02:55,890
 an objective reality, it helps you see things from an

58
00:02:55,890 --> 00:02:59,280
 objective point of view. You're not creating

59
00:02:59,280 --> 00:03:01,940
 this, you're not altering it, you're seeing it the way it

60
00:03:01,940 --> 00:03:04,320
 is, and that's difficult. It's a lot easier

61
00:03:04,320 --> 00:03:07,500
 to create something in your mind. So you have a color here,

62
00:03:07,500 --> 00:03:10,080
 close your eyes and in your third eye,

63
00:03:10,080 --> 00:03:12,660
 you create a color, or you focus on the chakras or whatever

64
00:03:12,660 --> 00:03:15,360
, you know, there's many kinds of

65
00:03:15,360 --> 00:03:18,600
 meditation. And so you focus on the colors, say blue, blue,

66
00:03:18,600 --> 00:03:20,880
 blue, or red, red, or white, white,

67
00:03:20,880 --> 00:03:24,580
 or whatever. Some people will focus on a light or, you know

68
00:03:24,580 --> 00:03:27,040
, many different kinds of meditation like

69
00:03:27,040 --> 00:03:30,500
 this. You create that and you focus your mind on it. Once

70
00:03:30,500 --> 00:03:35,440
 you can fix your mind only on that object,

71
00:03:35,440 --> 00:03:38,740
 so you're thinking of nothing else, and you're just totally

72
00:03:38,740 --> 00:03:41,040
 fixed on that object, that's considered to

73
00:03:41,040 --> 00:03:46,160
 be an attainment of Samatha meditation. You're actually,

74
00:03:46,160 --> 00:03:48,320
 you've tranquilized your mind to a level

75
00:03:48,320 --> 00:03:53,000
 of attainment. This is useful in many religious traditions,

76
00:03:53,000 --> 00:03:56,320
 especially in India, to gain magical

77
00:03:56,320 --> 00:03:59,060
 powers, to remember your past lives, to read people's minds

78
00:03:59,060 --> 00:04:00,560
, to see heaven and hell, to see

79
00:04:00,560 --> 00:04:04,020
 the future, whatever. Lots of crazy things can happen. You

80
00:04:04,020 --> 00:04:05,840
 can leave your body, these things are

81
00:04:06,880 --> 00:04:10,180
 talked about among meditators. Now, believe it or not, try

82
00:04:10,180 --> 00:04:13,200
 it for yourself. It's not what I teach,

83
00:04:13,200 --> 00:04:15,660
 but it's certainly out there, and there are manuals on how

84
00:04:15,660 --> 00:04:16,480
 to practice it.

85
00:04:16,480 --> 00:04:21,160
 Once you've done that, if you want to then, if you want to

86
00:04:21,160 --> 00:04:23,840
 take the Samatha first bhiva sana later

87
00:04:23,840 --> 00:04:29,230
 track, then what you do is you take that that focused state

88
00:04:29,230 --> 00:04:31,200
 and you start to apply the principles

89
00:04:31,200 --> 00:04:34,280
 that I talk about in my videos. So your mind is very

90
00:04:34,280 --> 00:04:36,800
 focused, you acknowledge that, you say to

91
00:04:36,800 --> 00:04:39,110
 yourself, focused, focused, you feel happy, you acknowledge

92
00:04:39,110 --> 00:04:40,560
 that happy, you feel calm, you acknowledge

93
00:04:40,560 --> 00:04:44,420
 that calm, calm, calm. You see the light or the color, you

94
00:04:44,420 --> 00:04:46,720
 say to yourself seeing, seeing, seeing,

95
00:04:46,720 --> 00:04:50,390
 and what's going to happen is it's going to change from a

96
00:04:50,390 --> 00:04:52,240
 stable solid state to something that's

97
00:04:52,240 --> 00:04:56,550
 changing. This helps you to see that even the most stable,

98
00:04:56,550 --> 00:04:59,200
 you know, I mean at that point your mind

99
00:04:59,200 --> 00:05:01,960
 is so refined that you can see things incredibly clearly.

100
00:05:01,960 --> 00:05:03,840
 When you say to yourself seeing, seeing,

101
00:05:03,840 --> 00:05:06,170
 seeing, you start to get it and you realize that this is

102
00:05:06,170 --> 00:05:08,080
 the nature of reality, that everything is

103
00:05:08,080 --> 00:05:10,930
 changing, that nothing is satisfying, that things are not

104
00:05:10,930 --> 00:05:13,040
 under your control, that they are not

105
00:05:13,040 --> 00:05:16,960
 self, they are not soul, and so on. And so you are able to

106
00:05:16,960 --> 00:05:19,360
 let go of them, you're able to see

107
00:05:19,360 --> 00:05:21,620
 your attachments and you're able to change the way you

108
00:05:21,620 --> 00:05:24,640
 think about things. So because your mind is so

109
00:05:24,640 --> 00:05:28,840
 clear from the, the focused state, once you apply vipassana

110
00:05:28,840 --> 00:05:32,240
 meditation on that state, it's said to

111
00:05:32,240 --> 00:05:35,580
 be very easy to attain vipassana insight, as opposed to

112
00:05:35,580 --> 00:05:37,920
 someone who doesn't take the time to do that.

113
00:05:37,920 --> 00:05:41,490
 The differences are, if you've been practicing the

114
00:05:41,490 --> 00:05:43,920
 meditation I teach, it can be more,

115
00:05:43,920 --> 00:05:49,450
 more difficult, difficult in the sense of painful, you know

116
00:05:49,450 --> 00:05:50,880
, because your mind is not focused,

117
00:05:50,880 --> 00:05:53,930
 so you're wandering all over and you have to, you have to

118
00:05:53,930 --> 00:05:58,400
 really, you have to be really strong to

119
00:05:58,400 --> 00:06:01,600
 be able to take the pain in the body, the distractions in

120
00:06:01,600 --> 00:06:03,680
 the mind, the emotions as they come up.

121
00:06:03,680 --> 00:06:07,810
 But the result is the same and of the two, we prefer this

122
00:06:07,810 --> 00:06:10,320
 one that I teach because it's easier for

123
00:06:10,320 --> 00:06:14,970
 people in your daily life to practice, it's, it's a way to

124
00:06:14,970 --> 00:06:18,240
 get started right away. And it certainly

125
00:06:18,240 --> 00:06:21,850
 is clear that the Buddha didn't have preference for an

126
00:06:21,850 --> 00:06:25,200
 incredibly structured approach, except in

127
00:06:25,200 --> 00:06:28,040
 cases where people were already well developed and

128
00:06:28,040 --> 00:06:31,360
 practicing as monks in the forest. It's pretty

129
00:06:31,360 --> 00:06:34,560
 clear that in general he taught a more dynamic meditation

130
00:06:34,560 --> 00:06:36,560
 where when you're walking, you know

131
00:06:36,560 --> 00:06:38,440
 you're walking, when you're sitting, you know you're

132
00:06:38,440 --> 00:06:42,880
 sitting and so on. But there, there are many,

133
00:06:42,880 --> 00:06:45,960
 many different types of meditation that the Buddha taught,

134
00:06:45,960 --> 00:06:47,920
 many different approaches generally

135
00:06:47,920 --> 00:06:52,910
 structured for the individual. So the Buddha could see, he

136
00:06:52,910 --> 00:06:54,720
 was, he was a very special person who could

137
00:06:54,720 --> 00:06:59,220
 see people's, their strengths and weaknesses and see what

138
00:06:59,220 --> 00:07:02,240
 they needed to, to go further. And so he

139
00:07:02,240 --> 00:07:05,450
 would give them meditation specific to their needs. And

140
00:07:05,450 --> 00:07:07,760
 that causes a lot of disagreement over what

141
00:07:07,760 --> 00:07:12,030
 actually the Buddha taught. But the most common teaching

142
00:07:12,030 --> 00:07:14,880
 that the Buddha gave and by far the most

143
00:07:14,880 --> 00:07:18,750
 core, the most essential of the Buddha's teaching is a

144
00:07:18,750 --> 00:07:21,680
 focus on reality, the physical and the mental.

145
00:07:21,680 --> 00:07:23,960
 It's called the five aggregates, if you know anything about

146
00:07:23,960 --> 00:07:27,200
 that. And focusing and doing that

147
00:07:27,200 --> 00:07:30,590
 by using the four foundations of mindfulness as we teach

148
00:07:30,590 --> 00:07:34,000
 them, the body, the feelings, the mind and

149
00:07:34,000 --> 00:07:37,230
 the dhammas or the various groups of teaching that have to

150
00:07:37,230 --> 00:07:42,160
 do with meditation. So your question as to

151
00:07:42,160 --> 00:07:46,870
 whether, what should be first, it's totally up to you. But

152
00:07:46,870 --> 00:07:50,080
 if you're going to practice samatha first,

153
00:07:50,080 --> 00:07:55,240
 in the sense that I explained it, you really need a good

154
00:07:55,240 --> 00:07:58,560
 teacher to lead you through it. Either that

155
00:07:58,560 --> 00:08:03,100
 or at worst get a manual that teaches it. The best one that

156
00:08:03,100 --> 00:08:06,080
 I know of is the Visuddhi Maga,

157
00:08:06,080 --> 00:08:09,650
 which is probably the oldest as well. The path of pur

158
00:08:09,650 --> 00:08:11,680
ification is the translation.

159
00:08:11,680 --> 00:08:15,760
 And it goes through all of those types of meditation and

160
00:08:15,760 --> 00:08:16,800
 all of the fun,

161
00:08:16,800 --> 00:08:20,020
 magical powers that can come from it. But don't sue me if

162
00:08:20,020 --> 00:08:22,080
 you go crazy following them on your own,

163
00:08:22,080 --> 00:08:26,180
 because I don't teach those things. Anyway, it's just for

164
00:08:26,180 --> 00:08:31,040
 your edification. The difference between

165
00:08:31,040 --> 00:08:35,050
 the two is quite, I think, quite profound. One of them, and

166
00:08:35,050 --> 00:08:36,800
 you don't hear this a lot,

167
00:08:36,800 --> 00:08:39,020
 samatha meditation and vipassana meditation. What is the

168
00:08:39,020 --> 00:08:40,960
 difference? So I think it should be clear

169
00:08:40,960 --> 00:08:44,130
 based on what I've said, samatha meditation is for the

170
00:08:44,130 --> 00:08:46,000
 purpose of calming the mind.

171
00:08:46,000 --> 00:08:49,790
 Vipassana meditation is for the purpose of gaining insight.

172
00:08:49,790 --> 00:08:52,480
 It doesn't mean that vipassana meditation

173
00:08:52,480 --> 00:08:55,760
 is not going to lead to tranquility. And in fact, if you

174
00:08:55,760 --> 00:08:57,840
 practice the meditation, according to the

175
00:08:57,840 --> 00:09:01,350
 videos that I teach and in this method, as is taught

176
00:09:01,350 --> 00:09:04,160
 elsewhere, you will find that your mind

177
00:09:04,160 --> 00:09:06,780
 calms down, you'll find that you're much calmer, you're

178
00:09:06,780 --> 00:09:08,480
 much stronger as an individual, and your

179
00:09:08,480 --> 00:09:12,980
 mind is clear. The point is, it's not called samatha

180
00:09:12,980 --> 00:09:16,720
 meditation, because that's not the goal.

181
00:09:16,720 --> 00:09:19,780
 Whether you feel calm or not, it's not the most important

182
00:09:19,780 --> 00:09:21,760
 part. The important part is

183
00:09:21,760 --> 00:09:24,420
 that you gain insight. That's why it's given a special name

184
00:09:24,420 --> 00:09:26,560
 of vipassana meditation. People say,

185
00:09:26,560 --> 00:09:28,640
 what are you doing, you know, calling this vipassana and

186
00:09:28,640 --> 00:09:29,920
 saying that's not vipassana,

187
00:09:29,920 --> 00:09:32,920
 that's samatha, how can you separate them? We're separating

188
00:09:32,920 --> 00:09:34,000
 them based on the goal.

189
00:09:34,000 --> 00:09:36,690
 Samatha meditation, these meditations exist. There's no

190
00:09:36,690 --> 00:09:38,720
 question about it that there are meditations

191
00:09:38,720 --> 00:09:41,630
 out there that will not lead you to enlightenment. They can

192
00:09:41,630 --> 00:09:43,840
't, because they're not focused on reality.

193
00:09:43,840 --> 00:09:47,280
 They're creating an illusion in the mind. The only way that

194
00:09:47,280 --> 00:09:49,440
 they could lead to enlightenment is,

195
00:09:49,440 --> 00:09:52,310
 as I said, if you use that to gain insight, if you use that

196
00:09:52,310 --> 00:09:54,160
 to then look at it in a way similar to

197
00:09:54,160 --> 00:09:57,970
 how we do it here, as you would anything else. And because

198
00:09:57,970 --> 00:09:59,520
 of the strength of mind,

199
00:09:59,520 --> 00:10:03,530
 you can see it clearer than you would have otherwise. So it

200
00:10:03,530 --> 00:10:05,440
's kind of taking a shortcut,

201
00:10:05,440 --> 00:10:08,690
 but having to do a lot of preparation, so not gaining

202
00:10:08,690 --> 00:10:11,200
 anything, you know, except for

203
00:10:11,200 --> 00:10:14,090
 these nice states of peace and calm and maybe some magical

204
00:10:14,090 --> 00:10:15,200
 powers along the way,

205
00:10:15,200 --> 00:10:19,540
 which is probably best suited towards for someone living in

206
00:10:19,540 --> 00:10:20,640
 the forest.

207
00:10:20,640 --> 00:10:27,040
 So what should be first? Totally up to you. If you want to

208
00:10:27,040 --> 00:10:29,120
 go that way, I talked about that.

209
00:10:30,880 --> 00:10:33,630
 And if you want to start with, with just be passionate, it

210
00:10:33,630 --> 00:10:35,760
's very clear that this was

211
00:10:35,760 --> 00:10:39,660
 the way the Buddha, the Buddha gave this as the quickest

212
00:10:39,660 --> 00:10:42,400
 way someone among came up to the Buddha

213
00:10:42,400 --> 00:10:45,690
 and said, Look, I'm old and I don't have a lot of time and

214
00:10:45,690 --> 00:10:47,920
 I know my memory is not good. I don't

215
00:10:47,920 --> 00:10:51,810
 want to learn a lot. Give me the basics of the path. And

216
00:10:51,810 --> 00:10:54,400
 the Buddha said, clear your view,

217
00:10:54,400 --> 00:10:57,200
 you know, get an understanding of what is the truth. And

218
00:10:57,200 --> 00:10:59,920
 this is just an intellectual understanding. So,

219
00:10:59,920 --> 00:11:02,640
 you know, learn a little bit about what is suffering, what

220
00:11:02,640 --> 00:11:03,840
 is the cause of suffering,

221
00:11:03,840 --> 00:11:06,290
 what is the way out of suffering and so on, you know, from

222
00:11:06,290 --> 00:11:07,760
 a from a technical point of view.

223
00:11:07,760 --> 00:11:12,760
 And, and, and purify your precepts, meaning purify your

224
00:11:12,760 --> 00:11:15,760
 morality, meaning, don't kill,

225
00:11:15,760 --> 00:11:18,150
 you know, take the vows not to kill, not to steal, not to

226
00:11:18,150 --> 00:11:19,760
 cheat, not to lie, not to take drugs and

227
00:11:19,760 --> 00:11:22,820
 alcohol, you know, purify your conduct, make sure you're

228
00:11:22,820 --> 00:11:24,960
 you're doing your ethical and moral in your

229
00:11:24,960 --> 00:11:27,870
 behavior, and then start practicing the four foundations of

230
00:11:27,870 --> 00:11:29,440
 mindfulness. This is clear,

231
00:11:29,440 --> 00:11:33,810
 there's no talk about first, you know, constantly focusing

232
00:11:33,810 --> 00:11:36,400
 your mind entering into any state,

233
00:11:36,400 --> 00:11:39,830
 you enter into these states naturally, and you enter into

234
00:11:39,830 --> 00:11:42,080
 them based on reality. So even just

235
00:11:42,080 --> 00:11:46,970
 focusing on the rising and falling, you can enter into a an

236
00:11:46,970 --> 00:11:49,520
 absorbed state for the moment

237
00:11:49,520 --> 00:11:51,960
 that you're watching it when you say step being right,

238
00:11:51,960 --> 00:11:53,920
 stepping left into the walking meditation,

239
00:11:53,920 --> 00:11:56,600
 you can enter into an absorbed state for that moment. And

240
00:11:56,600 --> 00:11:58,160
 that allows you to see that

241
00:11:58,160 --> 00:12:01,420
 reality clearly. So in that sense, you're practicing both

242
00:12:01,420 --> 00:12:04,240
 at once, you're also tranquil,

243
00:12:04,240 --> 00:12:11,050
 and you're also seeing clearly, which is the point. Okay,

244
00:12:11,050 --> 00:12:12,880
 hope that helped. Thanks for the question.

245
00:12:12,880 --> 00:12:14,080
 Keep practicing.

