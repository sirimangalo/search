1
00:00:00,000 --> 00:00:10,000
 Good evening, everyone.

2
00:00:10,000 --> 00:00:28,880
 Tonight's quote is about wealth.

3
00:00:28,880 --> 00:00:38,870
 Many of you probably are aware Buddhism is not a wealth-

4
00:00:38,870 --> 00:00:40,880
oriented religion.

5
00:00:40,880 --> 00:00:48,880
 It doesn't place much importance on wealth.

6
00:00:48,880 --> 00:00:55,640
 Can't think of any religion that does.

7
00:00:55,640 --> 00:01:04,520
 I think Buddhism especially has some very poorly pointed

8
00:01:04,520 --> 00:01:09,920
 things to say, or at least has a reputation of probably

9
00:01:09,920 --> 00:01:14,640
 being anti-wealth.

10
00:01:14,640 --> 00:01:20,230
 I don't think, I mean, monks are supposed to be monks who

11
00:01:20,230 --> 00:01:26,080
 are the spokespeople of the religion, are supposed to live

12
00:01:26,080 --> 00:01:27,640
 in poverty.

13
00:01:27,640 --> 00:01:35,830
 We aren't allowed to touch money or valuable objects like

14
00:01:35,830 --> 00:01:38,640
 gold and jewels.

15
00:01:38,640 --> 00:01:56,560
 To wear these robes, I have to eat the food that we're

16
00:01:56,560 --> 00:01:56,640
 given.

17
00:01:56,640 --> 00:02:01,640
 The poverty is a big part of Buddhism.

18
00:02:01,640 --> 00:02:15,870
 The Buddha himself said, "Santuti paramang dhan nang."

19
00:02:15,870 --> 00:02:18,640
 Contentment is the greatest wealth.

20
00:02:18,640 --> 00:02:26,640
 And that was unexpected.

21
00:02:26,640 --> 00:02:55,640
 I had my timer set right, I ended early.

22
00:02:55,640 --> 00:03:06,640
 Contentment is the greatest form of wealth.

23
00:03:06,640 --> 00:03:18,590
 That's because what wealth is supposed to do is fulfill our

24
00:03:18,590 --> 00:03:23,640
 wants, our needs.

25
00:03:23,640 --> 00:03:31,080
 And so ordinary wealth is able to temporarily fulfill our

26
00:03:31,080 --> 00:03:40,640
 wants and our desires and our needs, which our needs anyway

27
00:03:40,640 --> 00:03:47,640
 are making wealth an important thing.

28
00:03:47,640 --> 00:03:50,780
 So in this quote that we have tonight, the Buddha

29
00:03:50,780 --> 00:03:54,250
 acknowledges that wealth is good for you, it can make you

30
00:03:54,250 --> 00:03:56,940
 happy, it can make your parents happy, it can make your

31
00:03:56,940 --> 00:04:00,860
 spouse, your children, your servants and workers happy,

32
00:04:00,860 --> 00:04:04,640
 your friends and companions happy.

33
00:04:04,640 --> 00:04:12,510
 And you can use it to give charity or religious donations,

34
00:04:12,510 --> 00:04:20,640
 that's what the quote refers to, but charity in general.

35
00:04:20,640 --> 00:04:29,730
 So obviously poverty is a problem, but the dangers of that

36
00:04:29,730 --> 00:04:37,640
 wealth is that it's temporary, it disappears.

37
00:04:37,640 --> 00:04:41,930
 Wealth can be incredibly dangerous, right? The more wealthy

38
00:04:41,930 --> 00:04:45,640
 you are, the more trapped you are by it, can be.

39
00:04:45,640 --> 00:04:50,180
 In many ways, wealthy people are trapped or can be trapped

40
00:04:50,180 --> 00:04:51,640
 by their money.

41
00:04:51,640 --> 00:04:57,200
 There was an interesting story I read about people who win

42
00:04:57,200 --> 00:05:01,180
 the lottery, that apparently winning the lottery is one of

43
00:05:01,180 --> 00:05:03,640
 the worst things that can happen to you.

44
00:05:03,640 --> 00:05:08,050
 You look it up, it's quite interesting what happens to

45
00:05:08,050 --> 00:05:11,760
 people, what the statistics are for people who have won the

46
00:05:11,760 --> 00:05:22,190
 lottery, homicide, suicide, a lot of robbery, you know, you

47
00:05:22,190 --> 00:05:29,640
 lose friends, you lose family, lose your life.

48
00:05:29,640 --> 00:05:36,200
 Wealth, that kind of wealth can be dangerous, but even

49
00:05:36,200 --> 00:05:42,660
 putting that aside, worldly wealth, though it's necessary

50
00:05:42,660 --> 00:05:46,650
 to maintain livelihood, and this is where Buddhism does

51
00:05:46,650 --> 00:05:48,640
 acknowledge the need for wealth.

52
00:05:48,640 --> 00:05:54,890
 I mean, Buddhist, even monks have to have balls, they have

53
00:05:54,890 --> 00:06:01,240
 to have things, they have to have tools, things that allow

54
00:06:01,240 --> 00:06:04,640
 them to continue their life.

55
00:06:04,640 --> 00:06:12,090
 But beyond that wealth isn't actually able to provide

56
00:06:12,090 --> 00:06:18,380
 contentment, it isn't actually able to satisfy it, it isn't

57
00:06:18,380 --> 00:06:20,640
 able to bring happiness.

58
00:06:20,640 --> 00:06:25,080
 So the happiness that it can bring is the ability to live

59
00:06:25,080 --> 00:06:29,710
 one's life without worry, without fear, and potentially

60
00:06:29,710 --> 00:06:34,890
 without having to go hungry, without having to be cold or

61
00:06:34,890 --> 00:06:42,620
 hot, without having to suffer through lack of the amenities

62
00:06:42,620 --> 00:06:46,640
 or the necessities of life.

63
00:06:46,640 --> 00:06:51,660
 But as far as fulfilling our wants, our desires, there's no

64
00:06:51,660 --> 00:06:55,640
 wealth, no amount of wealth that can do that.

65
00:06:55,640 --> 00:07:03,190
 And so in terms of our wants, this is why contentment is

66
00:07:03,190 --> 00:07:07,640
 the greatest form of wealth.

67
00:07:07,640 --> 00:07:11,640
 Because that's what we're looking for from wealth, right?

68
00:07:11,640 --> 00:07:15,590
 We understand that the problem is we want something, this

69
00:07:15,590 --> 00:07:16,640
 is an issue.

70
00:07:16,640 --> 00:07:19,640
 When you want something, it's actually a problem.

71
00:07:19,640 --> 00:07:24,270
 If you don't get it, to the extent that you want it, it's

72
00:07:24,270 --> 00:07:28,640
 going to cause suffering for you until you get it.

73
00:07:28,640 --> 00:07:32,640
 If you want it alive, it can cause great suffering.

74
00:07:32,640 --> 00:07:35,050
 If you want it little, well, just a little suffering, until

75
00:07:35,050 --> 00:07:35,640
 you get it.

76
00:07:35,640 --> 00:07:39,640
 Of course, once you get it, then you're pleased.

77
00:07:39,640 --> 00:07:45,520
 But that pleasure reinforces the desire, and so you want it

78
00:07:45,520 --> 00:07:49,640
 more, and it becomes more acute.

79
00:07:49,640 --> 00:07:54,640
 And eventually you don't get what you want, and you suffer.

80
00:07:54,640 --> 00:08:01,210
 So you're constantly living your life chasing after your

81
00:08:01,210 --> 00:08:02,640
 desires.

82
00:08:02,640 --> 00:08:06,430
 Contentment, on the other hand, accomplishes the same thing

83
00:08:06,430 --> 00:08:12,400
, but it does it by going the other way, by destroying the

84
00:08:12,400 --> 00:08:13,640
 wanting.

85
00:08:13,640 --> 00:08:20,070
 When you remove the wanting without reinforcing it, without

86
00:08:20,070 --> 00:08:25,640
 following it, then you weaken it.

87
00:08:25,640 --> 00:08:29,640
 When you can find peace without the things you want,

88
00:08:29,640 --> 00:08:33,000
 when you can give up your wants and find happiness without

89
00:08:33,000 --> 00:08:34,640
 needing this or that to make you happy,

90
00:08:34,640 --> 00:08:40,640
 which is a foreign concept to many of us.

91
00:08:40,640 --> 00:08:45,300
 We're so used to finding happiness in things and

92
00:08:45,300 --> 00:08:46,640
 experiences.

93
00:08:46,640 --> 00:08:52,140
 It becomes kind of religious, or a view that we have, our

94
00:08:52,140 --> 00:08:53,640
 belief.

95
00:08:53,640 --> 00:09:00,850
 And if you challenge that, there's a tendency to react

96
00:09:00,850 --> 00:09:02,640
 quite negatively.

97
00:09:02,640 --> 00:09:10,240
 People get upset, and elegant, turned off by the things you

98
00:09:10,240 --> 00:09:11,640
're saying.

99
00:09:11,640 --> 00:09:16,890
 You say, "Find happiness by letting go, by not clinging, by

100
00:09:16,890 --> 00:09:18,640
 giving up your desires,

101
00:09:18,640 --> 00:09:20,640
 or a few people will be interested."

102
00:09:20,640 --> 00:09:23,730
 As I've talked about recently, most of us don't even

103
00:09:23,730 --> 00:09:25,640
 understand that statement.

104
00:09:25,640 --> 00:09:27,640
 We can't even comprehend it.

105
00:09:27,640 --> 00:09:32,640
 We're so fixated on our desires.

106
00:09:32,640 --> 00:09:36,630
 It's hard to even comprehend the idea of being happy

107
00:09:36,630 --> 00:09:37,640
 without.

108
00:09:37,640 --> 00:09:41,220
 So maybe that's not giving people enough credit, but

109
00:09:41,220 --> 00:09:42,640
 especially all of you here,

110
00:09:42,640 --> 00:09:46,190
 anyone who's watching this, I would think, has an

111
00:09:46,190 --> 00:09:49,640
 understanding, some understanding of the problem.

112
00:09:49,640 --> 00:09:56,010
 They've had suffering in their lives, and so they can see

113
00:09:56,010 --> 00:09:58,640
 how our wants,

114
00:09:58,640 --> 00:10:01,320
 when we want things to be a certain way, when we don't get

115
00:10:01,320 --> 00:10:02,640
 that, it's quite suffering,

116
00:10:02,640 --> 00:10:10,640
 quite a bit of suffering, great suffering.

117
00:10:10,640 --> 00:10:14,360
 And so we turn to meditation, thinking, to find contentment

118
00:10:14,360 --> 00:10:14,640
.

119
00:10:14,640 --> 00:10:16,640
 This is really what it is.

120
00:10:16,640 --> 00:10:20,640
 If this hasn't come to you, well, here's an idea,

121
00:10:20,640 --> 00:10:25,050
 a reason why meditation might be interesting, is to find

122
00:10:25,050 --> 00:10:26,640
 contentment,

123
00:10:26,640 --> 00:10:32,640
 to be able to live without need.

124
00:10:32,640 --> 00:10:34,640
 And so how does it do this?

125
00:10:34,640 --> 00:10:37,640
 Meditation teaches us objectivity.

126
00:10:37,640 --> 00:10:41,640
 It's like straightening the mind.

127
00:10:41,640 --> 00:10:45,600
 The mind is bent and crooked all out of shape, so you have

128
00:10:45,600 --> 00:10:47,640
 to strike it and straighten it.

129
00:10:47,640 --> 00:10:50,840
 And every moment that you see things as they are, you're

130
00:10:50,840 --> 00:10:51,640
 straightening the mind.

131
00:10:51,640 --> 00:10:54,190
 Not just every moment you're meditating, that's not the

132
00:10:54,190 --> 00:10:54,640
 case.

133
00:10:54,640 --> 00:10:58,640
 But while you're meditating, when you find a moment where

134
00:10:58,640 --> 00:10:59,640
 you just see something as it is,

135
00:10:59,640 --> 00:11:04,230
 when you say rising, and you're just aware of the rising,

136
00:11:04,230 --> 00:11:07,640
 that moment is a pure state.

137
00:11:07,640 --> 00:11:16,080
 And it's straight. The mind is straight, is wholesome, is

138
00:11:16,080 --> 00:11:17,640
 pure.

139
00:11:17,640 --> 00:11:23,640
 And you cultivate this, the cultivation of these states.

140
00:11:23,640 --> 00:11:31,640
 This is the path to contentment.

141
00:11:31,640 --> 00:11:34,230
 Because in that time, in that moment, there's no need for

142
00:11:34,230 --> 00:11:36,640
 anything, there's no want for anything.

143
00:11:36,640 --> 00:11:42,600
 When you cultivate these moments after moments consecut

144
00:11:42,600 --> 00:11:45,640
ively, you slowly become content.

145
00:11:45,640 --> 00:11:49,810
 Now you're dealing with so many likes and dislikes, so it's

146
00:11:49,810 --> 00:11:51,640
 not easy, and it's not very comfortable.

147
00:11:51,640 --> 00:11:54,640
 It's mostly not content.

148
00:11:54,640 --> 00:11:57,640
 But in those moments, you can see the contentment.

149
00:11:57,640 --> 00:12:00,250
 That's what we're fighting for, that's what we're working

150
00:12:00,250 --> 00:12:00,640
 for.

151
00:12:00,640 --> 00:12:04,680
 It takes time, but through practice, this is what you

152
00:12:04,680 --> 00:12:05,640
 cultivate.

153
00:12:05,640 --> 00:12:16,640
 This is the direction that you steer yourself towards.

154
00:12:16,640 --> 00:12:19,180
 So I don't want to talk too much about wealth, I don't like

155
00:12:19,180 --> 00:12:20,640
 talking about money and so on,

156
00:12:20,640 --> 00:12:29,640
 but contentment, that's where it's at.

157
00:12:29,640 --> 00:12:35,640
 So there's your dhamma bit for tonight.

158
00:12:35,640 --> 00:12:39,640
 You know, open up the hangout if anybody wants to come on

159
00:12:39,640 --> 00:12:45,640
 and ask questions, if you don't have any questions.

160
00:12:45,640 --> 00:12:50,640
 And just say goodnight.

161
00:12:50,640 --> 00:12:53,640
 I met a couple of Sri Lankan people on the street today,

162
00:12:53,640 --> 00:12:56,640
 and they're coming tomorrow morning to visit.

163
00:12:56,640 --> 00:13:00,840
 It's nice, people in the community, there actually are

164
00:13:00,840 --> 00:13:02,640
 people in this area.

165
00:13:02,640 --> 00:13:06,640
 There's a Sri Lankan monk or Bangladeshi monk in Toronto,

166
00:13:06,640 --> 00:13:09,560
 said he was going to get me in touch with the Sri Lankan

167
00:13:09,560 --> 00:13:13,640
 community here, and he never did.

168
00:13:13,640 --> 00:13:15,640
 He was very busy though.

169
00:13:15,640 --> 00:13:20,380
 Apparently there's quite a few Sri Lankan people in

170
00:13:20,380 --> 00:13:21,640
 Hamilton.

171
00:13:21,640 --> 00:13:26,760
 And there's some Western people coming out to meditate, to

172
00:13:26,760 --> 00:13:27,640
 visit.

173
00:13:27,640 --> 00:13:33,050
 So this place could become a real part of the community, to

174
00:13:33,050 --> 00:13:34,640
 see how it goes.

175
00:13:34,640 --> 00:13:39,440
 I gave a meditation booklet to the woman who serves me at

176
00:13:39,440 --> 00:13:40,640
 Tim Horton's.

177
00:13:40,640 --> 00:13:43,640
 A really good group of people at Tim Horton's, it's funny.

178
00:13:43,640 --> 00:13:49,640
 I've gotten an anagam coming out to meditate.

179
00:13:49,640 --> 00:13:52,640
 Robin, you're echoing me. Do you have something else on,

180
00:13:52,640 --> 00:13:58,640
 like YouTube or the audio stream?

181
00:13:58,640 --> 00:14:02,640
 Better now.

182
00:14:02,640 --> 00:14:05,640
 Hey Robin.

183
00:14:05,640 --> 00:14:08,640
 She doesn't hear me, I don't think.

184
00:14:08,640 --> 00:14:10,640
 Hello.

185
00:14:10,640 --> 00:14:11,640
 So, Tipante.

186
00:14:11,640 --> 00:14:12,640
 Hi.

187
00:14:12,640 --> 00:14:16,330
 Hi, sorry, it's been so long, I forgot that I have to turn

188
00:14:16,330 --> 00:14:17,640
 the other one off.

189
00:14:17,640 --> 00:14:19,640
 Sorry about that.

190
00:14:19,640 --> 00:14:21,640
 So I had a question.

191
00:14:21,640 --> 00:14:22,640
 Okay.

192
00:14:22,640 --> 00:14:26,240
 So, we have a teaching that we hear all the time, we heard

193
00:14:26,240 --> 00:14:27,640
 it pretty recently.

194
00:14:27,640 --> 00:14:32,280
 Let's see, only be seeing, let hearing only be hearing, let

195
00:14:32,280 --> 00:14:34,640
 smelling only be smelling.

196
00:14:34,640 --> 00:14:37,640
 And it sounds good.

197
00:14:37,640 --> 00:14:39,640
 But it's my...

198
00:14:39,640 --> 00:14:41,640
 I forgot that I have to turn the other one off.

199
00:14:41,640 --> 00:14:43,640
 Sorry about that.

200
00:14:43,640 --> 00:14:44,640
 I had a question.

201
00:14:44,640 --> 00:14:48,200
 You're echoing us, please, if you're not the one to hang

202
00:14:48,200 --> 00:14:50,640
 out, you have to turn the other thing off first.

203
00:14:50,640 --> 00:14:56,640
 Let's see, only be seeing.

204
00:14:56,640 --> 00:14:58,640
 Hello?

205
00:14:58,640 --> 00:15:00,640
 Okay.

206
00:15:00,640 --> 00:15:05,900
 Okay, so with this teaching, I mean it sounds good, but my

207
00:15:05,900 --> 00:15:10,820
 common sense tells me that we have to judge what comes in

208
00:15:10,820 --> 00:15:13,640
 contact with our senses to be safe,

209
00:15:13,640 --> 00:15:18,800
 if you're truly just tasting, tasting, but not judging the

210
00:15:18,800 --> 00:15:19,640
 taste.

211
00:15:19,640 --> 00:15:21,640
 You could be eating rancid food.

212
00:15:21,640 --> 00:15:26,360
 If you're smelling but not recognizing the smell, your

213
00:15:26,360 --> 00:15:28,640
 house could be burning.

214
00:15:28,640 --> 00:15:34,360
 I mean, how does this teaching work in the real world where

215
00:15:34,360 --> 00:15:38,640
 we do have to evaluate our surroundings?

216
00:15:38,640 --> 00:15:43,640
 I mean, it's not the whole truth.

217
00:15:43,640 --> 00:15:47,140
 It's not like a way of living your life, but it's the

218
00:15:47,140 --> 00:15:52,610
 necessary thing that you need to perform during a

219
00:15:52,610 --> 00:15:54,640
 meditation practice.

220
00:15:54,640 --> 00:15:58,750
 Because if you don't get to the point where seeing is just

221
00:15:58,750 --> 00:16:01,640
 saying you'll never get to the root.

222
00:16:01,640 --> 00:16:06,570
 But I mean, even an Arahant has to think and has to process

223
00:16:06,570 --> 00:16:12,640
, has to talk, has to interact with people, has to judge.

224
00:16:12,640 --> 00:16:17,610
 Seeing is not just seeing. Seeing is a pit in front of you

225
00:16:17,610 --> 00:16:20,640
 that you have to go around.

226
00:16:20,640 --> 00:16:22,640
 You don't have that in a minute.

227
00:16:22,640 --> 00:16:28,440
 I mean, it's still seeing. You see the pit and you say

228
00:16:28,440 --> 00:16:30,640
 seeing, seeing.

229
00:16:30,640 --> 00:16:35,060
 And then there's still the thoughts that arise based on the

230
00:16:35,060 --> 00:16:37,640
 pit. They arise anyway.

231
00:16:37,640 --> 00:16:44,640
 The point is your mind is straight.

232
00:16:44,640 --> 00:16:49,640
 So those thoughts are free from, "Oh my gosh, it's a pit."

233
00:16:49,640 --> 00:16:52,940
 They can still arise and they still will arise, the

234
00:16:52,940 --> 00:16:53,640
 awareness.

235
00:16:53,640 --> 00:17:00,690
 But so it's only part of the picture, those thoughts of, "

236
00:17:00,690 --> 00:17:01,640
Oh, that's a pit."

237
00:17:01,640 --> 00:17:03,640
 Well, anyway, you're not saying seeing.

238
00:17:03,640 --> 00:17:06,210
 But when you said seeing is just seeing, when you said

239
00:17:06,210 --> 00:17:09,640
 seeing, you get neutralized, experienced.

240
00:17:09,640 --> 00:17:11,640
 At least for a moment, you could still get react.

241
00:17:11,640 --> 00:17:16,060
 But the first moment of seeing the pit or the tiger or

242
00:17:16,060 --> 00:17:20,640
 whatever has neutralized the fear and the shock.

243
00:17:20,640 --> 00:17:27,030
 I mean, that being said, to some extent, there is a sense

244
00:17:27,030 --> 00:17:29,640
 of having to stay neutral.

245
00:17:29,640 --> 00:17:31,930
 Like, of course, if it's rancid food, you know that it's

246
00:17:31,930 --> 00:17:34,640
 rancid food. You have no reason to eat it.

247
00:17:34,640 --> 00:17:38,640
 But if it's a tiger, then the tiger is going to eat you.

248
00:17:38,640 --> 00:17:44,840
 Well, you run. But once it catches you, seeing, that's

249
00:17:44,840 --> 00:17:45,640
 seeing, just be seeing.

250
00:17:45,640 --> 00:17:49,640
 And then pain, just be pain.

251
00:17:49,640 --> 00:17:54,680
 So you kind of have to go in and out of this? Would that be

252
00:17:54,680 --> 00:17:55,640
 correct?

253
00:17:55,640 --> 00:18:02,880
 Not even. It's arguable that, yeah, you'd have to go a

254
00:18:02,880 --> 00:18:05,640
 little, but it's not really out of it.

255
00:18:05,640 --> 00:18:11,110
 Because your mind is pure. It's like when you walk, you're

256
00:18:11,110 --> 00:18:14,000
 walking meditation, you're only saying stepping right,

257
00:18:14,000 --> 00:18:14,640
 stepping left.

258
00:18:14,640 --> 00:18:17,950
 But you're aware of a lot else. You're aware of the

259
00:18:17,950 --> 00:18:19,640
 shifting of the body.

260
00:18:19,640 --> 00:18:22,840
 You may be aware of stray thoughts, but you don't have to

261
00:18:22,840 --> 00:18:26,770
 note them all, as long as you keep your mind straight, as

262
00:18:26,770 --> 00:18:28,640
 long as you keep your mind pure.

263
00:18:28,640 --> 00:18:35,120
 And so by noting every second or so, you know, frequently,

264
00:18:35,120 --> 00:18:39,640
 your mind will say, "Stay straight."

265
00:18:39,640 --> 00:18:46,530
 So it's important to do this, but it's not everything,

266
00:18:46,530 --> 00:18:49,640
 especially if you're light.

267
00:18:49,640 --> 00:18:53,660
 But that being said, when you actually do a meditation, or

268
00:18:53,660 --> 00:18:56,960
 the best way to understand it, the easiest way to

269
00:18:56,960 --> 00:18:59,640
 understand it is during a meditation.

270
00:18:59,640 --> 00:19:02,930
 If you want to follow it where you can follow it perfectly

271
00:19:02,930 --> 00:19:06,090
 strictly, when you're sitting with your eyes closed, that's

272
00:19:06,090 --> 00:19:09,640
 the time where you're going to look at this.

273
00:19:09,640 --> 00:19:16,680
 But all it takes is a moment. I told you this story about

274
00:19:16,680 --> 00:19:21,910
 this monk who was teaching, I was watching, and he says, "H

275
00:19:21,910 --> 00:19:24,640
earing, hearing."

276
00:19:24,640 --> 00:19:30,560
 He went into his cessation while he was teaching. It's

277
00:19:30,560 --> 00:19:34,640
 probably one of the most impressive things I've ever seen.

278
00:19:34,640 --> 00:19:38,990
 So all it was is in that moment for him, hearing was just

279
00:19:38,990 --> 00:19:46,640
 hearing, as he was teaching. And that's what it was.

280
00:19:46,640 --> 00:19:53,460
 I guess my challenge is just to make it enough of a part of

281
00:19:53,460 --> 00:19:57,640
 my day to ever get to that point.

282
00:19:57,640 --> 00:20:06,510
 One of our recent meditators finished his course, did okay,

283
00:20:06,510 --> 00:20:13,640
 finished it, but was a little bit disappointed, I think,

284
00:20:13,640 --> 00:20:15,900
 with the results, even though I was pretty sure he got

285
00:20:15,900 --> 00:20:17,640
 where he was supposed to be.

286
00:20:17,640 --> 00:20:23,460
 You can't tell. It's hard for him to tell, it's hard for me

287
00:20:23,460 --> 00:20:30,010
 to tell. But then he emailed me, and he'd gotten stuck on a

288
00:20:30,010 --> 00:20:30,640
 flight.

289
00:20:30,640 --> 00:20:35,960
 And he was on this plane for like nine hours or something

290
00:20:35,960 --> 00:20:41,600
 on the runway. It's an insane amount of time sitting on the

291
00:20:41,600 --> 00:20:42,640
 runway.

292
00:20:42,640 --> 00:20:48,100
 And what he described to me, what happened on the runway,

293
00:20:48,100 --> 00:20:53,780
 suddenly he came back and I was just listening, and I said,

294
00:20:53,780 --> 00:20:59,640
 "That sounds exactly like on the plane leaving to go home."

295
00:20:59,640 --> 00:21:01,640
 You never know when.

296
00:21:01,640 --> 00:21:06,520
 Sometimes you get so stressed during the course, or you get

297
00:21:06,520 --> 00:21:11,640
 stressed during the course and it inhibits, it prevents.

298
00:21:11,640 --> 00:21:16,840
 From your worried or near the end it can get stressful. And

299
00:21:16,840 --> 00:21:20,690
 if you let it get to you, if you're not mindful, then it

300
00:21:20,690 --> 00:21:24,640
 just happens when you relax.

301
00:21:24,640 --> 00:21:28,490
 It's like Ananda, right? Ananda was doing walking

302
00:21:28,490 --> 00:21:33,100
 meditation all night, trying to become enlightened, and

303
00:21:33,100 --> 00:21:34,640
 nothing.

304
00:21:34,640 --> 00:21:37,930
 And then it was almost dawn, and he felt like he'd gotten

305
00:21:37,930 --> 00:21:40,640
 nowhere, and he was really stressed out.

306
00:21:40,640 --> 00:21:43,310
 And then he thought, and he said, "Oh, I'm pushing too hard

307
00:21:43,310 --> 00:21:44,640
." And so he laid down.

308
00:21:44,640 --> 00:21:52,640
 And before his head touched the pillow, he was not.

309
00:21:54,640 --> 00:21:56,640
 Hey, Tom.

310
00:21:56,640 --> 00:21:58,640
 Hi, Bante.

311
00:21:58,640 --> 00:21:59,640
 You have a question?

312
00:21:59,640 --> 00:22:01,640
 No.

313
00:22:01,640 --> 00:22:05,640
 I just thought I'd show the flag.

314
00:22:05,640 --> 00:22:09,640
 All right. Well, nice to see you.

315
00:22:09,640 --> 00:22:11,640
 Good to see you.

316
00:22:11,640 --> 00:22:17,640
 Thank you, Bante.

317
00:22:17,640 --> 00:22:20,640
 Welcome. Have a good night, everyone.

318
00:22:20,640 --> 00:22:22,640
 Goodbye.

