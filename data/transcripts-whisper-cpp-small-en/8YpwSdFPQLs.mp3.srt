1
00:00:00,000 --> 00:00:06,560
 Manuel says, "If I close the eyes while meditating in the

2
00:00:06,560 --> 00:00:09,000
 evening, I enter in a dreamy state.

3
00:00:09,000 --> 00:00:12,000
 Well, if I keep them open, the mind is quite clear."

4
00:00:12,000 --> 00:00:16,200
 So the question is, "I should experience the dreamy state,

5
00:00:16,200 --> 00:00:17,000
 but risking to fall asleep,

6
00:00:17,000 --> 00:00:22,000
 or keep them open to profit of the more mind clarity."

7
00:00:22,000 --> 00:00:26,970
 It's an interesting question. I mean, you have an argument

8
00:00:26,970 --> 00:00:28,000
 there.

9
00:00:28,000 --> 00:00:32,000
 The question really is why that is the case.

10
00:00:32,000 --> 00:00:36,150
 And I suppose the answer might be that it's just a habit,

11
00:00:36,150 --> 00:00:37,000
 your habit of mind.

12
00:00:37,000 --> 00:00:40,000
 Your habit is that when you close your eyes, your mind

13
00:00:40,000 --> 00:00:43,000
 automatically says, "Okay, time to sleep."

14
00:00:43,000 --> 00:00:45,870
 "Oh, he wants to sleep now." Because it thinks you want to

15
00:00:45,870 --> 00:00:46,000
 sleep.

16
00:00:46,000 --> 00:00:49,000
 So it's just a conditioning.

17
00:00:49,000 --> 00:00:53,000
 So in the beginning, "Yeah, sure, do whatever works for you

18
00:00:53,000 --> 00:00:53,000
.

19
00:00:53,000 --> 00:00:56,000
 If you want to keep your eyes open, keep your eyes open."

20
00:00:56,000 --> 00:01:00,910
 I think the point is that eventually, especially if you're

21
00:01:00,910 --> 00:01:03,000
 in a meditation retreat,

22
00:01:03,000 --> 00:01:06,370
 or if you're in a meditation center, you won't need to do

23
00:01:06,370 --> 00:01:07,000
 that.

24
00:01:07,000 --> 00:01:10,000
 You'll find that closing your eyes is actually much better

25
00:01:10,000 --> 00:01:12,000
 because it shuts off one sense.

26
00:01:12,000 --> 00:01:16,000
 You only have to focus now on the stomach.

27
00:01:16,000 --> 00:01:18,520
 The problem with the eyes, I guess, is that we use them so

28
00:01:18,520 --> 00:01:19,000
 much.

29
00:01:19,000 --> 00:01:27,000
 So keeping them open is a great danger for our focus.

30
00:01:27,000 --> 00:01:31,000
 Our mind will be too distracted.

31
00:01:31,000 --> 00:01:34,800
 But you certainly can keep them open. There's really no

32
00:01:34,800 --> 00:01:37,000
 problem with that.

33
00:01:37,000 --> 00:01:42,120
 But you also might decide that you want to close them and

34
00:01:42,120 --> 00:01:43,000
 fight with the drowsiness

35
00:01:43,000 --> 00:01:50,160
 and alter that conditioning and look at the state of

36
00:01:50,160 --> 00:01:52,000
 attachment that's involved there.

37
00:01:52,000 --> 00:01:54,640
 I don't know, it could be anything, but certainly you have

38
00:01:54,640 --> 00:01:55,000
 an argument

39
00:01:55,000 --> 00:01:59,000
 and there's nothing wrong with keeping your eyes open.

40
00:01:59,000 --> 00:02:02,230
 The other thing I was going to say is that what you really

41
00:02:02,230 --> 00:02:04,000
 should do when you feel tired

42
00:02:04,000 --> 00:02:07,000
 is try to get up and do some walking meditation.

43
00:02:07,000 --> 00:02:10,000
 Because walking meditation will wake you up.

44
00:02:10,000 --> 00:02:13,270
 If you're working during the day it might be difficult, you

45
00:02:13,270 --> 00:02:14,000
 might feel like

46
00:02:14,000 --> 00:02:17,020
 you don't have quite the energy for it. So you can go

47
00:02:17,020 --> 00:02:18,000
 either way.

48
00:02:18,000 --> 00:02:21,000
 But that's one standard answer that we would give.

49
00:02:21,000 --> 00:02:23,000
 If you're falling asleep, don't do sitting at all.

50
00:02:23,000 --> 00:02:26,000
 Get up and do walking meditation.

