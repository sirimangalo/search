1
00:00:00,000 --> 00:00:11,720
 The reason why we practice meditation is for the purpose of

2
00:00:11,720 --> 00:00:15,000
 letting go.

3
00:00:15,000 --> 00:00:22,990
 This is a very easy, clear way of explaining the teaching

4
00:00:22,990 --> 00:00:25,400
 which the Lord Buddha gave to

5
00:00:25,400 --> 00:00:30,240
 the world, is the teaching on how to let go.

6
00:00:30,240 --> 00:00:34,260
 And this is a teaching which is very much prized, or this

7
00:00:34,260 --> 00:00:35,600
 is an idea which is very much

8
00:00:35,600 --> 00:00:40,500
 prized in the world, the concept of letting go, or being

9
00:00:40,500 --> 00:00:42,040
 able to let go.

10
00:00:42,040 --> 00:00:45,490
 So we go around telling each other that you just have to

11
00:00:45,490 --> 00:00:47,320
 let go, or you have to learn

12
00:00:47,320 --> 00:00:48,320
 to let go.

13
00:00:48,320 --> 00:00:56,640
 Well, why don't you just let go, let it go, or so on.

14
00:00:56,640 --> 00:01:02,120
 But we find, we try that the reality of it is that letting

15
00:01:02,120 --> 00:01:04,680
 go is not as simple as just

16
00:01:04,680 --> 00:01:11,200
 willing it or wishing it to be so.

17
00:01:11,200 --> 00:01:15,600
 Wishing ourselves not to hold on, not to attach.

18
00:01:15,600 --> 00:01:22,780
 And so we look at the Lord Buddha's teaching as a way to

19
00:01:22,780 --> 00:01:26,000
 really and truly let go, a way

20
00:01:26,000 --> 00:01:31,120
 to really and truly be free from our attachments and our

21
00:01:31,120 --> 00:01:33,560
 addictions to things.

22
00:01:33,560 --> 00:01:36,160
 Because it goes deeper than simply letting go.

23
00:01:36,160 --> 00:01:39,130
 It gives an explanation of why it is that we hold on in the

24
00:01:39,130 --> 00:01:40,480
 first place, and how we

25
00:01:40,480 --> 00:01:45,040
 do away with the causes of attachment.

26
00:01:45,040 --> 00:01:47,840
 This attachment is not the beginning.

27
00:01:47,840 --> 00:01:51,520
 It doesn't just come, arise by itself, it doesn't just

28
00:01:51,520 --> 00:01:53,200
 arise out of nothing.

29
00:01:53,200 --> 00:01:55,160
 It arises out of causes and conditions.

30
00:01:55,160 --> 00:01:58,520
 And so the Lord Buddha taught the causes and actions, why

31
00:01:58,520 --> 00:02:00,200
 we hold on, why we attach, and

32
00:02:00,200 --> 00:02:04,260
 ultimately why we suffer.

33
00:02:04,260 --> 00:02:07,370
 And so we have the core teaching of the Lord Buddha on

34
00:02:07,370 --> 00:02:09,480
 dependent origination, and probably

35
00:02:09,480 --> 00:02:13,000
 it is "Paticca Samuppada".

36
00:02:13,000 --> 00:02:19,530
 And it starts "Avicha Pateya Sankara" with ignorance as a

37
00:02:19,530 --> 00:02:22,720
 condition that arises, "Sankara"

38
00:02:22,720 --> 00:02:25,600
 or formations.

39
00:02:25,600 --> 00:02:29,010
 And in this case it means mental formations or volitional

40
00:02:29,010 --> 00:02:30,000
 tendencies.

41
00:02:30,000 --> 00:02:34,740
 Volition, or in Buddhist terms karma, as we've heard the

42
00:02:34,740 --> 00:02:37,480
 word karma, means an intention,

43
00:02:37,480 --> 00:02:42,200
 either a good intention or a bad intention, our intention

44
00:02:42,200 --> 00:02:44,600
 to do things, that we may get

45
00:02:44,600 --> 00:02:48,660
 a result, our wish, our desire for things to be so, to be

46
00:02:48,660 --> 00:02:50,480
 this way or be that way.

47
00:02:50,480 --> 00:02:55,040
 "Avicha Pateya Sankara" So we see ignorance.

48
00:02:55,040 --> 00:02:58,200
 "Avicha" means ignorance.

49
00:02:58,200 --> 00:03:02,460
 Ignorance is the root cause of all karma, of all wishes and

50
00:03:02,460 --> 00:03:04,400
 all desires for things to

51
00:03:04,400 --> 00:03:08,040
 be this or to be that.

52
00:03:08,040 --> 00:03:14,630
 And this is an explanation of what is the start, the very

53
00:03:14,630 --> 00:03:18,000
 beginning of attachment, comes

54
00:03:18,000 --> 00:03:20,640
 from ignorance.

55
00:03:20,640 --> 00:03:24,280
 And so the Buddhist teaching is all about learning to see

56
00:03:24,280 --> 00:03:27,120
 clearly, because of course our attachments

57
00:03:27,120 --> 00:03:29,000
 take an object.

58
00:03:29,000 --> 00:03:32,500
 When we attach, we are attaching to something, be it

59
00:03:32,500 --> 00:03:34,840
 something in our mind or something in

60
00:03:34,840 --> 00:03:39,990
 the world around us that we see or hear or smell or taste

61
00:03:39,990 --> 00:03:41,080
 or feel.

62
00:03:41,080 --> 00:03:44,800
 We hold on to these things because of, at the root, because

63
00:03:44,800 --> 00:03:45,920
 of ignorance.

64
00:03:45,920 --> 00:03:50,910
 Because ignorance leads us to desire for things, leads us

65
00:03:50,910 --> 00:03:53,800
 to create our mental intentions and

66
00:03:53,800 --> 00:03:58,980
 to act out on our intentions by hurting other people or

67
00:03:58,980 --> 00:04:02,280
 hurting ourselves or putting out

68
00:04:02,280 --> 00:04:06,530
 effort to get the things we want or to be free from the

69
00:04:06,530 --> 00:04:09,080
 things which we don't want.

70
00:04:09,080 --> 00:04:12,530
 But the actual explanation on how this then leads to

71
00:04:12,530 --> 00:04:15,160
 attachment and to suffering, we have

72
00:04:15,160 --> 00:04:16,160
 to continue on.

73
00:04:16,160 --> 00:04:20,410
 So we have ignorance leads us to act out things, leads us

74
00:04:20,410 --> 00:04:22,960
 to do things, leads us to create

75
00:04:22,960 --> 00:04:24,520
 intention in our minds.

76
00:04:24,520 --> 00:04:28,360
 But how does this intention work?

77
00:04:28,360 --> 00:04:31,000
 How does it have to do with holding on?

78
00:04:31,000 --> 00:04:37,960
 We go on to Sankara Pateya Vinyanam, consciousness.

79
00:04:37,960 --> 00:04:42,010
 With these karmas, these things which we have done in the

80
00:04:42,010 --> 00:04:44,640
 past, in say past lifetimes, then

81
00:04:44,640 --> 00:04:47,080
 there arises consciousness.

82
00:04:47,080 --> 00:04:50,050
 With these karmas as a cause, then there arises

83
00:04:50,050 --> 00:04:51,320
 consciousness.

84
00:04:51,320 --> 00:04:53,830
 So in the beginning of when we're born, there arises a

85
00:04:53,830 --> 00:04:54,800
 consciousness.

86
00:04:54,800 --> 00:04:57,240
 We don't have to think yet about past life.

87
00:04:57,240 --> 00:05:00,180
 If we start about at the moment of conception when there

88
00:05:00,180 --> 00:05:03,280
 arises consciousness in the womb,

89
00:05:03,280 --> 00:05:04,640
 this is the start of everything.

90
00:05:04,640 --> 00:05:08,840
 It comes as a cause of our intentions in the past life.

91
00:05:08,840 --> 00:05:12,000
 But we start simply here from consciousness.

92
00:05:12,000 --> 00:05:16,080
 And this is going to come back again to ignorance and to

93
00:05:16,080 --> 00:05:16,880
 karma.

94
00:05:16,880 --> 00:05:23,160
 So if we start here at Vinyana, consciousness.

95
00:05:23,160 --> 00:05:28,710
 Then we have continuing on Vinyana Pateya Namarupa, with

96
00:05:28,710 --> 00:05:39,080
 consciousness as a cause, as a condition,

97
00:05:39,080 --> 00:05:44,800
 there arises body and mind or object and subject, subject

98
00:05:44,800 --> 00:05:46,260
 and object.

99
00:05:46,260 --> 00:05:49,370
 So when there is consciousness, there is consciousness of

100
00:05:49,370 --> 00:05:50,200
 something.

101
00:05:50,200 --> 00:05:55,010
 And so when we first arise in the womb of our mother, we

102
00:05:55,010 --> 00:05:57,820
 have consciousness as our body

103
00:05:57,820 --> 00:06:01,820
 progresses and our eyes and ears and nose and tongue and

104
00:06:01,820 --> 00:06:03,780
 the body, then we have the

105
00:06:03,780 --> 00:06:06,780
 six senses.

106
00:06:06,780 --> 00:06:10,250
 And the six senses are basically Namarupa, the object and

107
00:06:10,250 --> 00:06:11,320
 the subject.

108
00:06:11,320 --> 00:06:16,080
 So the subject is the Vinyana, the knowing of the object.

109
00:06:16,080 --> 00:06:20,180
 And the object itself is a sight or a sound or a smell or a

110
00:06:20,180 --> 00:06:22,820
 taste or a feeling or a thought.

111
00:06:22,820 --> 00:06:27,580
 So with the object and subject there come the Salayatana,

112
00:06:27,580 --> 00:06:29,200
 the six senses.

113
00:06:29,200 --> 00:06:33,020
 When we have consciousness then there arises body and mind

114
00:06:33,020 --> 00:06:35,400
 and there arises the six senses.

115
00:06:35,400 --> 00:06:38,500
 Now with the six senses as the cause, seeing, hearing,

116
00:06:38,500 --> 00:06:41,160
 smelling, tasting, feeling and thinking,

117
00:06:41,160 --> 00:06:43,480
 there arises contact.

118
00:06:43,480 --> 00:06:49,640
 And this is all part of the same experience.

119
00:06:49,640 --> 00:06:53,840
 Contact means contact between the mind and the object.

120
00:06:53,840 --> 00:06:56,380
 And contact is the important part.

121
00:06:56,380 --> 00:06:59,740
 This is where we start getting back to ignorance.

122
00:06:59,740 --> 00:07:02,480
 Because when there is contact, when the mind touches the

123
00:07:02,480 --> 00:07:04,000
 eye and there arises seeing or

124
00:07:04,000 --> 00:07:07,890
 when the mind touches the ear and there arises hearing,

125
00:07:07,890 --> 00:07:10,040
 then we can see that the eye and

126
00:07:10,040 --> 00:07:12,110
 the ear and the nose and the tongue and the body and the

127
00:07:12,110 --> 00:07:13,480
 heart, they are there all the

128
00:07:13,480 --> 00:07:15,480
 time.

129
00:07:15,480 --> 00:07:18,550
 Our eyes are sometimes open and yet we don't see the things

130
00:07:18,550 --> 00:07:19,600
 in front of us.

131
00:07:19,600 --> 00:07:20,920
 Why our mind is somewhere else?

132
00:07:20,920 --> 00:07:23,080
 Our mind hasn't come in contact with the eye.

133
00:07:23,080 --> 00:07:26,640
 Where we have ears but we are oblivious to the things

134
00:07:26,640 --> 00:07:28,920
 people are saying around us.

135
00:07:28,920 --> 00:07:29,920
 Why?

136
00:07:29,920 --> 00:07:31,800
 Because our mind is occupied with something else.

137
00:07:31,800 --> 00:07:35,040
 Or smelling or tasting or feeling or thinking.

138
00:07:35,040 --> 00:07:37,740
 Sometimes we are not even aware of what we are thinking

139
00:07:37,740 --> 00:07:39,400
 because the mind is preoccupied

140
00:07:39,400 --> 00:07:45,710
 with something else or the mind is not aware of the thought

141
00:07:45,710 --> 00:07:46,160
.

142
00:07:46,160 --> 00:07:50,250
 But when there does arise the contact, so when we see

143
00:07:50,250 --> 00:07:52,720
 something and the mind goes out

144
00:07:52,720 --> 00:07:56,720
 to the eye and there is seeing, then there arises vaitana.

145
00:07:56,720 --> 00:08:01,240
 Salayatana, paasa, paatsayah vaitana.

146
00:08:01,240 --> 00:08:05,540
 With contact as a condition, there arises feeling.

147
00:08:05,540 --> 00:08:08,640
 And this is where everything starts.

148
00:08:08,640 --> 00:08:12,360
 So when we see something because of the mind and the eye

149
00:08:12,360 --> 00:08:14,640
 and the light touching the eye,

150
00:08:14,640 --> 00:08:17,390
 then there arises a feeling, a happy feeling if it is

151
00:08:17,390 --> 00:08:19,600
 something we enjoy, an unhappy feeling

152
00:08:19,600 --> 00:08:22,240
 if it is something that we don't enjoy.

153
00:08:22,240 --> 00:08:27,440
 And if it is neither nor, then there is a neutral feeling.

154
00:08:27,440 --> 00:08:30,330
 But at any rate there arises one of the three kinds of

155
00:08:30,330 --> 00:08:31,200
 feelings.

156
00:08:31,200 --> 00:08:33,450
 And this is where we come back again to what happened in

157
00:08:33,450 --> 00:08:34,720
 our past life and we are going

158
00:08:34,720 --> 00:08:39,480
 to continue the same thing over in this life where avicaya,

159
00:08:39,480 --> 00:08:42,240
 pata, avicaya, pata yasankara.

160
00:08:42,240 --> 00:08:44,610
 At the time when we feel something, if we feel happy and we

161
00:08:44,610 --> 00:08:46,200
 have ignorance, we are not

162
00:08:46,200 --> 00:08:50,520
 clearly aware that this is simply a feeling, there will

163
00:08:50,520 --> 00:08:53,680
 arise tanha, tanha which is craving.

164
00:08:53,680 --> 00:08:59,620
 So vaitana, pata yasanha, with feeling as a condition,

165
00:08:59,620 --> 00:09:01,600
 there arises craving.

166
00:09:01,600 --> 00:09:04,120
 So when we see something that we like, when we are sitting

167
00:09:04,120 --> 00:09:07,400
 here and we see something beautiful,

168
00:09:07,400 --> 00:09:10,920
 there arises craving, wanting for it.

169
00:09:10,920 --> 00:09:14,360
 If something unpleasant arises, we see or we hear or we

170
00:09:14,360 --> 00:09:16,800
 smell, then there arises a different

171
00:09:16,800 --> 00:09:22,270
 type of craving, a craving for it not to be, a craving to

172
00:09:22,270 --> 00:09:25,040
 be rid of the object, of our

173
00:09:25,040 --> 00:09:28,120
 discomfort.

174
00:09:28,120 --> 00:09:30,510
 And if we have a neutral feeling, then there arises a

175
00:09:30,510 --> 00:09:32,040
 craving for it to continue, for the

176
00:09:32,040 --> 00:09:33,720
 calm, for the peace to continue.

177
00:09:33,720 --> 00:09:39,780
 There arises a attachment to being in that way, a craving

178
00:09:39,780 --> 00:09:42,680
 towards being at peace and

179
00:09:42,680 --> 00:09:47,280
 having the peaceful feeling continue.

180
00:09:47,280 --> 00:09:52,020
 And this arises again because of avicaya, but we have to go

181
00:09:52,020 --> 00:09:54,120
 back to the beginning.

182
00:09:54,120 --> 00:09:57,260
 At the moment when feeling arises, when we feel something,

183
00:09:57,260 --> 00:09:58,840
 a happy feeling, because of

184
00:09:58,840 --> 00:10:01,800
 we or we hear or we smell, or an unhappy feeling or a

185
00:10:01,800 --> 00:10:05,360
 neutral feeling, if we don't have mindfulness,

186
00:10:05,360 --> 00:10:09,180
 if we are not simply aware that this is a feeling, this is

187
00:10:09,180 --> 00:10:11,140
 where we start to hold on.

188
00:10:11,140 --> 00:10:19,060
 Because craving leads to upatthana, upatthana, tanha, pate

189
00:10:19,060 --> 00:10:22,920
ya upatthana, upadhanam, with craving

190
00:10:22,920 --> 00:10:26,120
 as the condition there arises clinging.

191
00:10:26,120 --> 00:10:32,160
 So now we see where everything comes from.

192
00:10:32,160 --> 00:10:34,470
 Ignorance at the moment when there is feeling, when we see

193
00:10:34,470 --> 00:10:35,780
 or we hear or we smell, leads

194
00:10:35,780 --> 00:10:39,800
 to craving, wanting it to continue, not realizing that it's

195
00:10:39,800 --> 00:10:42,640
 impermanent, that it's unsatisfying,

196
00:10:42,640 --> 00:10:46,090
 that it's not under our control, not seeing it simply as a

197
00:10:46,090 --> 00:10:47,920
 part of nature, as a natural

198
00:10:47,920 --> 00:10:54,700
 phenomenon, but taking pleasure in it or becoming angry

199
00:10:54,700 --> 00:10:59,040
 about it or upset or frustrated by it.

200
00:10:59,040 --> 00:11:01,330
 And simply taking it for what it is, we will come to be

201
00:11:01,330 --> 00:11:02,360
 intoxicated by it.

202
00:11:02,360 --> 00:11:05,160
 And this is called tanha or craving.

203
00:11:05,160 --> 00:11:07,480
 With craving as a condition, then there arises clinging.

204
00:11:07,480 --> 00:11:10,600
 And this is where holding on comes from.

205
00:11:10,600 --> 00:11:14,120
 If at the moment when someone says something bad to us and

206
00:11:14,120 --> 00:11:15,840
 we find we can't let it go,

207
00:11:15,840 --> 00:11:18,930
 it's because at the moment when first the sound arose, we

208
00:11:18,930 --> 00:11:20,440
 weren't able to see that it

209
00:11:20,440 --> 00:11:23,840
 was simply a sound arising at the ear.

210
00:11:23,840 --> 00:11:26,880
 When the sound then got into our minds and there arose

211
00:11:26,880 --> 00:11:28,740
 thoughts of hatred or thoughts

212
00:11:28,740 --> 00:11:31,960
 of anger or thoughts of displeasure, then we weren't able

213
00:11:31,960 --> 00:11:33,600
 to see that these were simply

214
00:11:33,600 --> 00:11:37,900
 thoughts and emotions, and not under our control, not ours

215
00:11:37,900 --> 00:11:40,340
 to hold on to, not something that

216
00:11:40,340 --> 00:11:43,140
 we should cling to.

217
00:11:43,140 --> 00:11:45,860
 When we didn't see clearly in this way, then there became

218
00:11:45,860 --> 00:11:47,520
 craving, a wanting, a wish for

219
00:11:47,520 --> 00:11:48,520
 it to be like this.

220
00:11:48,520 --> 00:11:53,000
 And this gets back to a vicha, pachaya, sankara.

221
00:11:53,000 --> 00:11:54,000
 So it goes in circles.

222
00:11:54,000 --> 00:11:56,210
 In this life we're just continuing what we've done in the

223
00:11:56,210 --> 00:11:56,840
 past lives.

224
00:11:56,840 --> 00:11:58,760
 We're creating more karma.

225
00:11:58,760 --> 00:12:00,530
 And when we die, when we pass away, then there will be

226
00:12:00,530 --> 00:12:01,800
 another vinyana, there will be more

227
00:12:01,800 --> 00:12:02,800
 consciousness.

228
00:12:02,800 --> 00:12:06,640
 We'll be born again and again and again and again.

229
00:12:06,640 --> 00:12:09,780
 The way to become free, the way to really let go is to

230
00:12:09,780 --> 00:12:11,880
 create a vicha, which is nascent

231
00:12:11,880 --> 00:12:23,000
 science or science or understanding or wisdom, knowledge

232
00:12:23,000 --> 00:12:26,160
 you could say.

233
00:12:26,160 --> 00:12:30,160
 When there arises knowledge that the things which arise in

234
00:12:30,160 --> 00:12:32,160
 front of us are impermanent,

235
00:12:32,160 --> 00:12:35,110
 are unsatisfying and are not ours, are not under our

236
00:12:35,110 --> 00:12:37,000
 control, then we're able to see

237
00:12:37,000 --> 00:12:40,210
 them for what they are and let them come and let them go

238
00:12:40,210 --> 00:12:42,240
 and not crave for them to be this

239
00:12:42,240 --> 00:12:45,610
 way or that way, not need for it to be like this or like

240
00:12:45,610 --> 00:12:46,260
 that.

241
00:12:46,260 --> 00:12:47,760
 And so not cling to anything.

242
00:12:47,760 --> 00:12:54,480
 Of course, when we cling, then it leads us to seek out and

243
00:12:54,480 --> 00:12:58,320
 to take action, needing things

244
00:12:58,320 --> 00:13:01,480
 to be like this or be like that, when it's a pleasant thing

245
00:13:01,480 --> 00:13:03,040
, working and working hard

246
00:13:03,040 --> 00:13:06,520
 to keep the objects of our pleasure.

247
00:13:06,520 --> 00:13:08,710
 And of course, since they're impermanence, since they're

248
00:13:08,710 --> 00:13:09,960
 unsatisfying, since they're

249
00:13:09,960 --> 00:13:14,260
 not under our control, then we find ourselves often meeting

250
00:13:14,260 --> 00:13:16,760
 with things which are undesirable

251
00:13:16,760 --> 00:13:19,600
 or losing the things which are the object of our desire.

252
00:13:19,600 --> 00:13:25,100
 And of course, this is the truth of suffering, the truth of

253
00:13:25,100 --> 00:13:28,000
 what it means or the truth which

254
00:13:28,000 --> 00:13:30,600
 we have to face, which is called suffering.

255
00:13:30,600 --> 00:13:34,120
 And this is a detailed explanation of the cause of

256
00:13:34,120 --> 00:13:37,400
 suffering, that it comes from ignorance.

257
00:13:37,400 --> 00:13:40,410
 Because we can't see things simply for what they are, then

258
00:13:40,410 --> 00:13:41,860
 we crave for them either to

259
00:13:41,860 --> 00:13:43,720
 continue or for them to disappear.

260
00:13:43,720 --> 00:13:46,790
 And when we crave for them like this or like that, then it

261
00:13:46,790 --> 00:13:48,560
 leads to a clinging, it leads

262
00:13:48,560 --> 00:13:49,560
 to an addiction.

263
00:13:49,560 --> 00:13:52,160
 When we get what we want or when we are able to do away

264
00:13:52,160 --> 00:13:53,920
 with what we don't want, then it

265
00:13:53,920 --> 00:13:58,930
 becomes a part of our habit that every time that phenomenon

266
00:13:58,930 --> 00:14:01,280
 arises, we're forced to a

267
00:14:01,280 --> 00:14:05,040
 position of clinging, having a needing for it to be this

268
00:14:05,040 --> 00:14:06,960
 way or that way, not able to

269
00:14:06,960 --> 00:14:10,360
 bear with things simply as they are.

270
00:14:10,360 --> 00:14:15,870
 And this leads for us then to, leads to us then making

271
00:14:15,870 --> 00:14:18,680
 effort or putting out effort to

272
00:14:18,680 --> 00:14:22,090
 keep things the way they are, keep things in a certain way

273
00:14:22,090 --> 00:14:23,560
 and set up and control our

274
00:14:23,560 --> 00:14:26,910
 lives when of course our lives and the whole world around

275
00:14:26,910 --> 00:14:28,840
 us are not under our control.

276
00:14:28,840 --> 00:14:32,480
 There's something which are much bigger than us and

277
00:14:32,480 --> 00:14:35,360
 involves natural laws of impermanence,

278
00:14:35,360 --> 00:14:42,470
 of dissatisfaction, dissatisfaction and of uncontroll

279
00:14:42,470 --> 00:14:46,040
ability which are a part of nature,

280
00:14:46,040 --> 00:14:48,840
 which are a part of the way things are.

281
00:14:48,840 --> 00:14:51,500
 And so this leads us to suffer when we don't get what we

282
00:14:51,500 --> 00:14:53,400
 want, to suffer when we get things

283
00:14:53,400 --> 00:14:59,110
 which are unpleasant to us, when our wishes are unfulfilled

284
00:14:59,110 --> 00:15:01,120
 and so on and so on.

285
00:15:01,120 --> 00:15:05,600
 And this is an understanding of what it means to hold on.

286
00:15:05,600 --> 00:15:08,320
 So of course what it means to let go is when we do see

287
00:15:08,320 --> 00:15:09,080
 clearly.

288
00:15:09,080 --> 00:15:12,920
 When we see clearly something which arises.

289
00:15:12,920 --> 00:15:15,270
 So at the moment when we're doing walking meditation or

290
00:15:15,270 --> 00:15:16,840
 sitting meditation and an emotion

291
00:15:16,840 --> 00:15:20,010
 arises and we're able to catch the emotion and simply say

292
00:15:20,010 --> 00:15:21,880
 to ourselves, "Angry, angry

293
00:15:21,880 --> 00:15:26,440
 or wanting, wanting," and let it go.

294
00:15:26,440 --> 00:15:29,250
 Not getting to the point where we need it to be this and we

295
00:15:29,250 --> 00:15:30,560
 need it to be that or even

296
00:15:30,560 --> 00:15:34,150
 to the point where it makes an impression on our minds

297
00:15:34,150 --> 00:15:36,720
 where we like it or we are displeased

298
00:15:36,720 --> 00:15:37,720
 by it.

299
00:15:37,720 --> 00:15:41,430
 We simply let it arise and let it cease as it naturally

300
00:15:41,430 --> 00:15:43,660
 does anyway without having for

301
00:15:43,660 --> 00:15:46,480
 it to be any other way.

302
00:15:46,480 --> 00:15:49,800
 Then there arises no craving, there arises no clinging,

303
00:15:49,800 --> 00:15:51,600
 there arises no suffering.

304
00:15:51,600 --> 00:15:54,120
 Getting things to be in this way or that way.

305
00:15:54,120 --> 00:15:57,100
 And this is what it means in Buddhism, what it means to let

306
00:15:57,100 --> 00:15:57,440
 go.

307
00:15:57,440 --> 00:16:01,840
 It means to create wisdom to see clearly.

308
00:16:01,840 --> 00:16:06,040
 And so wisdom is held to be the highest virtue in Buddhism.

309
00:16:06,040 --> 00:16:09,140
 Simply letting go by itself cannot occur without seeing

310
00:16:09,140 --> 00:16:09,840
 clearly.

311
00:16:09,840 --> 00:16:13,200
 You cannot force yourself to let go of the things which you

312
00:16:13,200 --> 00:16:14,640
 like or the things which

313
00:16:14,640 --> 00:16:15,840
 you dislike.

314
00:16:15,840 --> 00:16:19,730
 This becomes some sort of repression, forcing yourself into

315
00:16:19,730 --> 00:16:21,580
 a position which is not in line

316
00:16:21,580 --> 00:16:25,220
 with your own understanding, where you force yourself to

317
00:16:25,220 --> 00:16:27,360
 believe something which you don't

318
00:16:27,360 --> 00:16:30,960
 believe and something you enjoy arises.

319
00:16:30,960 --> 00:16:34,640
 You force yourself to think that it's undesirable when in

320
00:16:34,640 --> 00:16:36,960
 fact in your own view it is desirable.

321
00:16:36,960 --> 00:16:39,730
 And this creates repression and this creates more mental

322
00:16:39,730 --> 00:16:41,200
 sickness and can even lead to

323
00:16:41,200 --> 00:16:43,200
 bodily sickness.

324
00:16:43,200 --> 00:16:45,990
 At the time when you see clearly that these things which we

325
00:16:45,990 --> 00:16:47,640
 think are desirable are actually

326
00:16:47,640 --> 00:16:51,520
 merely a cause for more suffering because of our later

327
00:16:51,520 --> 00:16:53,200
 attachment to them.

328
00:16:53,200 --> 00:16:56,340
 When we see clearly in this way then we don't wish for them

329
00:16:56,340 --> 00:16:58,000
, we don't desire for them,

330
00:16:58,000 --> 00:17:00,160
 and so we don't cling to them.

331
00:17:00,160 --> 00:17:06,480
 And so we can see the true way of letting go is actually

332
00:17:06,480 --> 00:17:10,200
 part of a larger process which

333
00:17:10,200 --> 00:17:14,800
 comes from seeing clearly that these things are not truly

334
00:17:14,800 --> 00:17:16,040
 desirable.

335
00:17:16,040 --> 00:17:20,170
 And so not desiring them and not then clinging to them, not

336
00:17:20,170 --> 00:17:22,400
 letting the attachment arise

337
00:17:22,400 --> 00:17:25,080
 in the first place.

338
00:17:25,080 --> 00:17:29,340
 And so this is an exposition of dependent origination as

339
00:17:29,340 --> 00:17:31,800
 well, of the Buddhist concept

340
00:17:31,800 --> 00:17:36,780
 of where attachment comes from, where suffering comes from,

341
00:17:36,780 --> 00:17:39,120
 and how letting go occurs and

342
00:17:39,120 --> 00:17:41,120
 how suffering therefore ceases.

343
00:17:41,120 --> 00:17:45,220
 And so this is the Dhamma which I give today, and so now we

344
00:17:45,220 --> 00:17:46,800
 practice together.

345
00:17:46,800 --> 00:17:49,950
 First we'll do mindful prostration and then walking and

346
00:17:49,950 --> 00:17:51,800
 then sitting to according to your

347
00:17:51,800 --> 00:17:52,800
 own time.

348
00:17:52,800 --> 00:17:54,880
 And we'll be here until 2 PM.

349
00:17:54,880 --> 00:17:55,200
 [

350
00:17:55,200 --> 00:17:56,200
 Pause ]

351
00:17:56,200 --> 00:17:57,200
 [ Silence ]

