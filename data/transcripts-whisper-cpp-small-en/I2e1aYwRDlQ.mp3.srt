1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:10,000
 We're broadcasting live.

3
00:00:10,000 --> 00:00:18,000
 July 6th.

4
00:00:18,000 --> 00:00:24,000
 Today's quote is about suffering.

5
00:00:24,000 --> 00:00:33,000
 It's all about suffering, isn't it?

6
00:00:33,000 --> 00:00:36,960
 It's one of the first things you hear about Buddhism is

7
00:00:36,960 --> 00:00:47,000
 that Buddha focused on suffering.

8
00:00:47,000 --> 00:00:53,320
 People who write about, talk about, or try to explain

9
00:00:53,320 --> 00:00:57,000
 Buddhism will try and defend it.

10
00:00:57,000 --> 00:01:06,000
 Couch it in fine language and nuance.

11
00:01:06,000 --> 00:01:14,550
 Which is kind of funny because it's the most important

12
00:01:14,550 --> 00:01:18,000
 aspect of Buddhism, the suffering.

13
00:01:18,000 --> 00:01:23,000
 It's the most important aspect of spiritual practice.

14
00:01:23,000 --> 00:01:31,650
 It's the most important concept or quality or idea in the

15
00:01:31,650 --> 00:01:37,000
 world, in the universe for being.

16
00:01:37,000 --> 00:01:42,480
 The Buddha was incredible that he was able to so clearly

17
00:01:42,480 --> 00:01:45,000
 identify the problem.

18
00:01:45,000 --> 00:01:51,000
 It's almost too obvious.

19
00:01:51,000 --> 00:01:59,000
 It almost doesn't even need to be said except that,

20
00:01:59,000 --> 00:02:03,000
 and of course what is the problem with suffering?

21
00:02:03,000 --> 00:02:11,620
 But it turns out that we're very good at avoiding and

22
00:02:11,620 --> 00:02:18,000
 twisting the truth to find some other problem,

23
00:02:18,000 --> 00:02:20,000
 to not have to deal with suffering.

24
00:02:20,000 --> 00:02:25,000
 If I could solve something else, there would be no problem.

25
00:02:25,000 --> 00:02:30,000
 If only I had money.

26
00:02:30,000 --> 00:02:37,280
 If only I had a job. If only I had a girlfriend or a

27
00:02:37,280 --> 00:02:40,000
 boyfriend.

28
00:02:40,000 --> 00:02:56,910
 The problem is inequality or the problem is lack of respect

29
00:02:56,910 --> 00:02:58,000
.

30
00:02:58,000 --> 00:03:07,050
 The problem is whatever problem I'm facing, whether it be

31
00:03:07,050 --> 00:03:11,000
 external or internal.

32
00:03:11,000 --> 00:03:15,000
 When in fact it's quite simple, the problem is suffering.

33
00:03:15,000 --> 00:03:20,000
 That's the problem. That's what we should be focusing on.

34
00:03:20,000 --> 00:03:24,000
 I think it's so depressing to talk about suffering, right?

35
00:03:24,000 --> 00:03:34,000
 It's how brainwashed we become into avoiding the problem.

36
00:03:34,000 --> 00:03:41,460
 You can't fix the problem because early on in life we

37
00:03:41,460 --> 00:03:43,000
 suffer.

38
00:03:43,000 --> 00:03:56,000
 We have no way of fixing it, so we learn to avoid, ignore,

39
00:03:56,000 --> 00:03:59,000
 become willfully blind towards suffering.

40
00:03:59,000 --> 00:04:01,000
 There's no other way.

41
00:04:01,000 --> 00:04:05,000
 We don't have an answer, so we learn to put up with it.

42
00:04:05,000 --> 00:04:14,000
 You often get the view that suffering is a part of life.

43
00:04:14,000 --> 00:04:16,000
 Suffering is something you can't avoid.

44
00:04:16,000 --> 00:04:18,000
 That's actually kind of wise.

45
00:04:18,000 --> 00:04:20,000
 In Buddhism we talk about that.

46
00:04:20,000 --> 00:04:26,000
 Yes, there are aspects of suffering that you can't avoid in

47
00:04:26,000 --> 00:04:26,000
 life.

48
00:04:26,000 --> 00:04:30,000
 You can't avoid getting old, you can't avoid getting sick,

49
00:04:30,000 --> 00:04:36,410
 you can't avoid dying, you can't avoid urinating, defec

50
00:04:36,410 --> 00:04:37,000
ating,

51
00:04:37,000 --> 00:04:43,720
 eating hunger, thirst, heat, cold, you can't avoid all

52
00:04:43,720 --> 00:04:47,000
 these things.

53
00:04:47,000 --> 00:04:53,000
 You can be free from suffering.

54
00:04:53,000 --> 00:04:55,000
 The Buddha asks this.

55
00:04:55,000 --> 00:05:01,000
 The quote is actually more specific.

56
00:05:01,000 --> 00:05:05,000
 These boys were tormenting fish, which, as someone noted,

57
00:05:05,000 --> 00:05:09,000
 is quite apt for National Fishing Day.

58
00:05:09,000 --> 00:05:13,000
 I don't know if it's in America, but in Canada it's hard.

59
00:05:13,000 --> 00:05:17,000
 People are going around celebrating the catching of fish.

60
00:05:17,000 --> 00:05:21,000
 It's like as though it were ice hockey or something.

61
00:05:21,000 --> 00:05:25,000
 They call it a sport.

62
00:05:25,000 --> 00:05:27,610
 Hunting would be a sport if we gave weapons to the other

63
00:05:27,610 --> 00:05:28,000
 team, right?

64
00:05:28,000 --> 00:05:31,000
 Then it would be a sport.

65
00:05:31,000 --> 00:05:35,000
 Bloodthirsty sport.

66
00:05:35,000 --> 00:05:43,440
 That would sort of sober people up if the fish had fishing

67
00:05:43,440 --> 00:05:44,000
 rods.

68
00:05:44,000 --> 00:05:47,000
 They could hook the humans.

69
00:05:47,000 --> 00:05:51,000
 It became a test of who could hook the other team,

70
00:05:51,000 --> 00:05:54,000
 who could bait the other team.

71
00:05:54,000 --> 00:05:57,660
 So you wouldn't know if you were going to eat this plate of

72
00:05:57,660 --> 00:05:58,000
 food

73
00:05:58,000 --> 00:06:02,000
 whether it was bait.

74
00:06:02,000 --> 00:06:05,000
 Wouldn't that be awful?

75
00:06:05,000 --> 00:06:09,000
 You have no way of knowing.

76
00:06:09,000 --> 00:06:12,000
 Just having to take your gut feeling whether you should eat

77
00:06:12,000 --> 00:06:12,000
 this plate of food

78
00:06:12,000 --> 00:06:18,000
 or not because there might be some invisible hook in it

79
00:06:18,000 --> 00:06:24,000
 and suddenly you're yanked into the water and drowned.

80
00:06:24,000 --> 00:06:34,000
 That's basically it.

81
00:06:34,000 --> 00:06:36,000
 So they were tormenting fish.

82
00:06:36,000 --> 00:06:38,000
 It looks like they weren't even killing them

83
00:06:38,000 --> 00:06:41,000
 or they weren't going to eat them.

84
00:06:41,000 --> 00:06:46,000
 It's just for fun because kids are like that.

85
00:06:46,000 --> 00:06:49,000
 Everyone says kids are innocent.

86
00:06:49,000 --> 00:06:51,000
 Kids are not innocent.

87
00:06:51,000 --> 00:06:52,000
 Kids are pure.

88
00:06:52,000 --> 00:06:54,000
 Kids are not pure.

89
00:06:54,000 --> 00:06:57,000
 We should just try and be a kid.

90
00:06:57,000 --> 00:07:01,000
 I can't think of anything more horrific.

91
00:07:01,000 --> 00:07:02,000
 Kids are horrible.

92
00:07:02,000 --> 00:07:04,000
 They can be so cruel.

93
00:07:04,000 --> 00:07:08,290
 But the interesting thing is it's not because they're

94
00:07:08,290 --> 00:07:09,000
 intentionally evil.

95
00:07:09,000 --> 00:07:11,000
 It's because they don't know any better.

96
00:07:11,000 --> 00:07:13,000
 They don't understand the consequences.

97
00:07:13,000 --> 00:07:18,000
 Kids have forgotten all about consequences.

98
00:07:18,000 --> 00:07:27,040
 They're born fresh and they let go completely of past

99
00:07:27,040 --> 00:07:30,000
 memories.

100
00:07:30,000 --> 00:07:32,770
 That's all they can think about is the pleasure that they

101
00:07:32,770 --> 00:07:33,000
 can get

102
00:07:33,000 --> 00:07:35,000
 wherever they can get it.

103
00:07:35,000 --> 00:07:40,000
 Kids are so happy and so simple.

104
00:07:40,000 --> 00:07:44,000
 They just want to be happy.

105
00:07:44,000 --> 00:07:49,270
 They don't realize the consequences of their quest for

106
00:07:49,270 --> 00:07:50,000
 happiness

107
00:07:50,000 --> 00:07:52,000
 and so they do terrible things.

108
00:07:52,000 --> 00:07:53,000
 Oh, let's torment some fish.

109
00:07:53,000 --> 00:07:55,000
 That'll make us happy.

110
00:07:55,000 --> 00:07:59,230
 It doesn't matter what about the fish as long as we get

111
00:07:59,230 --> 00:08:00,000
 happy.

112
00:08:00,000 --> 00:08:08,180
 The Buddha comes out and asks them, tries to point out the

113
00:08:08,180 --> 00:08:11,000
 consequences.

114
00:08:11,000 --> 00:08:17,490
 And so he does more of this, helping us to realize we're

115
00:08:17,490 --> 00:08:18,000
 not so separate

116
00:08:18,000 --> 00:08:23,000
 from our victims.

117
00:08:23,000 --> 00:08:27,000
 We're not so separate from each other.

118
00:08:27,000 --> 00:08:33,000
 And so he asks them, it's the Pali.

119
00:08:33,000 --> 00:08:41,000
 Vayatavotumhekumaraka dukasya apiyangudukan

120
00:08:41,000 --> 00:08:48,000
 Are you afraid, boys, of suffering?

121
00:08:48,000 --> 00:08:55,720
 Is suffering apiya? Apiya means not dear, undear,

122
00:08:55,720 --> 00:08:57,000
 unpleasant to you,

123
00:08:57,000 --> 00:09:04,000
 unpleasing, displeasing to you.

124
00:09:04,000 --> 00:09:08,000
 And they said, evangbande vayamamayam

125
00:09:08,000 --> 00:09:17,000
 evangbande vayamamayambande dukasya apiyangudukan

126
00:09:17,000 --> 00:09:20,000
 Yes, it is so venerable, sir.

127
00:09:20,000 --> 00:09:25,000
 We are afraid, we are afraid, of suffering.

128
00:09:25,000 --> 00:09:32,000
 Suffering is displeasing to us.

129
00:09:32,000 --> 00:09:34,000
 This is from the Udana.

130
00:09:34,000 --> 00:09:37,000
 Udana is where the Buddha gave inspired.

131
00:09:37,000 --> 00:09:43,000
 Udana is where you proclaim something.

132
00:09:43,000 --> 00:09:45,000
 It's an udana.

133
00:09:45,000 --> 00:09:49,000
 These are generally short suttas

134
00:09:49,000 --> 00:09:52,000
 where the Buddha would proclaim something.

135
00:09:52,000 --> 00:09:58,000
 Something sort of quite profound or meaningful anyway.

136
00:09:58,000 --> 00:10:00,000
 And so then he gave the udana.

137
00:10:00,000 --> 00:10:05,000
 Atakobagawa itamatang viditva tayangvila yang

138
00:10:05,000 --> 00:10:08,000
 imangudana udana is

139
00:10:08,000 --> 00:10:14,000
 At that time the Blessed One gave this exclamation.

140
00:10:14,000 --> 00:10:18,000
 This utterance.

141
00:10:18,000 --> 00:10:23,000
 Satyebayatadukasya, if you are afraid of suffering.

142
00:10:23,000 --> 00:10:29,000
 Satyebodukamapya, if suffering is displeasing to you.

143
00:10:29,000 --> 00:10:35,000
 Ma katha papa kangamang, don't do evil deeds.

144
00:10:35,000 --> 00:10:45,000
 Aviva yadivaro raho, in public or in private.

145
00:10:45,000 --> 00:10:53,000
 Satyeba papa kangamang karisattakarotava,

146
00:10:53,000 --> 00:11:00,000
 if and if evil deeds you will do or do,

147
00:11:00,000 --> 00:11:05,000
 do or will do.

148
00:11:05,000 --> 00:11:09,000
 If you are doing or will do.

149
00:11:09,000 --> 00:11:15,000
 Naodukamamudyati,

150
00:11:15,000 --> 00:11:19,000
 uvejapi alayatam

151
00:11:19,000 --> 00:11:23,000
 verses are sometimes difficult because

152
00:11:23,000 --> 00:11:26,000
 it's mangled.

153
00:11:26,000 --> 00:11:31,000
 Basically you cannot be,

154
00:11:31,000 --> 00:11:37,000
 you cannot find an escape from death, from suffering.

155
00:11:37,000 --> 00:11:48,000
 You will not be free from suffering by running away from it

156
00:11:48,000 --> 00:11:48,000
,

157
00:11:48,000 --> 00:11:59,000
 by escaping from something like that.

158
00:11:59,000 --> 00:12:01,000
 Don't do evil.

159
00:12:01,000 --> 00:12:03,000
 You don't want to suffer.

160
00:12:03,000 --> 00:12:06,000
 Quite simple.

161
00:12:06,000 --> 00:12:08,000
 Evil leads to suffering.

162
00:12:08,000 --> 00:12:10,000
 Suffering is the problem.

163
00:12:10,000 --> 00:12:12,000
 Don't do evil.

164
00:12:12,000 --> 00:12:15,000
 The world is not a complicated place.

165
00:12:15,000 --> 00:12:22,000
 All of the problems of the world can boil down to this.

166
00:12:22,000 --> 00:12:24,000
 Suffering.

167
00:12:24,000 --> 00:12:29,000
 Wealth inequality isn't the problem, it's suffering.

168
00:12:29,000 --> 00:12:33,530
 The climate isn't the problem, the environment isn't the

169
00:12:33,530 --> 00:12:34,000
 problem.

170
00:12:34,000 --> 00:12:37,120
 It isn't even a problem that you don't have any money or

171
00:12:37,120 --> 00:12:38,000
 even any food.

172
00:12:38,000 --> 00:12:42,000
 The problem is suffering.

173
00:12:42,000 --> 00:12:46,000
 Sometimes you lose your job and you still have food,

174
00:12:46,000 --> 00:12:49,000
 you still have health and you still have a place to stay,

175
00:12:49,000 --> 00:12:53,000
 but it's so much suffering.

176
00:12:53,000 --> 00:13:01,000
 You don't want to have lost your job, so you suffer.

177
00:13:01,000 --> 00:13:05,000
 And the cause of suffering is even quite simple.

178
00:13:05,000 --> 00:13:13,000
 The cause of suffering, in essence, is evil.

179
00:13:13,000 --> 00:13:16,630
 It's kind of a tautology that it's evil because it causes

180
00:13:16,630 --> 00:13:17,000
 suffering,

181
00:13:17,000 --> 00:13:20,000
 it causes suffering because it's evil.

182
00:13:20,000 --> 00:13:23,000
 It's more like it's evil because it causes suffering.

183
00:13:23,000 --> 00:13:26,000
 So the question is what is evil?

184
00:13:26,000 --> 00:13:28,000
 What is it that causes suffering?

185
00:13:28,000 --> 00:13:34,000
 This is the key, really.

186
00:13:34,000 --> 00:13:39,730
 The Buddha was equally awesome to be able to identify the

187
00:13:39,730 --> 00:13:41,000
 simple cause.

188
00:13:41,000 --> 00:13:50,040
 The cause of suffering isn't other people or isn't the

189
00:13:50,040 --> 00:13:53,000
 situation you're in.

190
00:13:53,000 --> 00:13:56,620
 The cause of suffering is not any situation, not any

191
00:13:56,620 --> 00:13:58,000
 experience.

192
00:13:58,000 --> 00:14:01,000
 The cause of suffering is our reactions to experience,

193
00:14:01,000 --> 00:14:09,000
 specifically, more directly, our attachment,

194
00:14:09,000 --> 00:14:13,000
 our desire for certain experiences.

195
00:14:13,000 --> 00:14:19,000
 It's our desires that propel us to cultivate expectations,

196
00:14:19,000 --> 00:14:27,000
 hopes, wants, ambitions,

197
00:14:27,000 --> 00:14:30,000
 and they set us up for disappointment.

198
00:14:30,000 --> 00:14:33,000
 You want things to be a certain way and then another way.

199
00:14:33,000 --> 00:14:47,000
 When you like something not having it, change.

200
00:14:47,000 --> 00:14:49,620
 When you're partial to certain experiences, other

201
00:14:49,620 --> 00:14:51,000
 experiences,

202
00:14:51,000 --> 00:14:57,000
 become unpleasant, displeasing, dukkha.

203
00:14:57,000 --> 00:15:01,000
 And then you cultivate diversion towards them.

204
00:15:01,000 --> 00:15:10,000
 They're not what you want.

205
00:15:10,000 --> 00:15:13,000
 Craving is called the cause of suffering,

206
00:15:13,000 --> 00:15:16,000
 but it still doesn't actually get to the point.

207
00:15:16,000 --> 00:15:18,480
 The interesting thing is we say craving is the cause of

208
00:15:18,480 --> 00:15:19,000
 suffering,

209
00:15:19,000 --> 00:15:21,000
 so we talk a lot about letting go.

210
00:15:21,000 --> 00:15:25,000
 If you can let go, then you'll be free from suffering.

211
00:15:25,000 --> 00:15:29,090
 But it still doesn't tell the whole picture, because how do

212
00:15:29,090 --> 00:15:30,000
 you let go?

213
00:15:30,000 --> 00:15:35,000
 I mean, it's actually quite reasonable and fairly well

214
00:15:35,000 --> 00:15:36,000
 understood

215
00:15:36,000 --> 00:15:38,880
 and in a general sense that, yeah, if you can learn to let

216
00:15:38,880 --> 00:15:39,000
 go,

217
00:15:39,000 --> 00:15:40,000
 you'll suffer less.

218
00:15:40,000 --> 00:15:42,000
 All this stressing and obsessing.

219
00:15:42,000 --> 00:15:45,000
 The question is how do you let go?

220
00:15:45,000 --> 00:15:49,160
 We haven't completed the picture because the cause of

221
00:15:49,160 --> 00:15:50,000
 craving,

222
00:15:50,000 --> 00:15:53,000
 you have to ask what is the cause of craving?

223
00:15:53,000 --> 00:15:55,000
 What is the cause of desire?

224
00:15:55,000 --> 00:15:57,000
 What is the cause of clinging to things,

225
00:15:57,000 --> 00:16:00,720
 expecting things, wanting things, needing things to be a

226
00:16:00,720 --> 00:16:02,000
 certain way?

227
00:16:02,000 --> 00:16:09,000
 It's actually, it's not some inherent evil or maliciousness

228
00:16:09,000 --> 00:16:10,000
 on our part.

229
00:16:10,000 --> 00:16:16,000
 It's simple delusion, simple ignorance.

230
00:16:16,000 --> 00:16:18,000
 You don't understand why there is suffering.

231
00:16:18,000 --> 00:16:21,000
 They're suffering because of nature,

232
00:16:21,000 --> 00:16:23,000
 because of the way nature works.

233
00:16:23,000 --> 00:16:34,000
 It's somewhat random or happen, happenstance, chance.

234
00:16:34,000 --> 00:16:41,000
 Things have turned out the way they have on women.

235
00:16:41,000 --> 00:16:44,000
 There's no rhyme or reason to the universe.

236
00:16:44,000 --> 00:16:48,000
 There's no meaning behind things.

237
00:16:48,000 --> 00:16:54,000
 There's a history, a long and terrible history.

238
00:16:54,000 --> 00:17:01,000
 But in all aspects, the universe is put together by,

239
00:17:01,000 --> 00:17:08,280
 put together haphazardly, by chance, by sort of, well, by

240
00:17:08,280 --> 00:17:14,000
 blindness.

241
00:17:14,000 --> 00:17:18,830
 And through blindness, we have created this prison for

242
00:17:18,830 --> 00:17:20,000
 ourselves,

243
00:17:20,000 --> 00:17:25,000
 created this existence that sometimes good, sometimes bad.

244
00:17:25,000 --> 00:17:27,000
 Sometimes we're really great people.

245
00:17:27,000 --> 00:17:29,000
 We can be really good to each other,

246
00:17:29,000 --> 00:17:33,000
 usually based on knowledge of cause and effect.

247
00:17:33,000 --> 00:17:37,000
 But then we forget and we lose sight of the good

248
00:17:37,000 --> 00:17:39,000
 and we become terrible people again.

249
00:17:39,000 --> 00:17:41,000
 We become addicted to things.

250
00:17:41,000 --> 00:17:43,000
 We become obsessed.

251
00:17:43,000 --> 00:17:45,000
 We don't realize we're doing it.

252
00:17:45,000 --> 00:17:48,000
 We cultivate all sorts of bad habits.

253
00:17:48,000 --> 00:17:53,000
 And we're suffering.

254
00:17:53,000 --> 00:17:56,000
 This is what you realize in meditation.

255
00:17:56,000 --> 00:17:58,000
 What am I doing to myself?

256
00:17:58,000 --> 00:18:01,000
 What have I been doing to myself?

257
00:18:01,000 --> 00:18:03,000
 How could I have been so blind?

258
00:18:03,000 --> 00:18:07,000
 This is what you realize when you start to meditate.

259
00:18:07,000 --> 00:18:09,000
 You see all your bad habits.

260
00:18:09,000 --> 00:18:23,000
 I just didn't even totally inept at this life thing.

261
00:18:23,000 --> 00:18:27,000
 Incompetent, incredible incompetence,

262
00:18:27,000 --> 00:18:34,010
 inability to define the truth, to find the right, to find

263
00:18:34,010 --> 00:18:35,000
 the good,

264
00:18:35,000 --> 00:18:38,000
 to find that which makes us happy.

265
00:18:38,000 --> 00:18:40,000
 Why can't we just be happy?

266
00:18:40,000 --> 00:18:42,000
 Why can't we find what makes us happy?

267
00:18:42,000 --> 00:18:44,000
 Why do we suffer?

268
00:18:44,000 --> 00:18:47,000
 Blindness, ignorance.

269
00:18:47,000 --> 00:18:50,000
 When you practice insight meditation, that's what happens.

270
00:18:50,000 --> 00:18:52,000
 You don't let go.

271
00:18:52,000 --> 00:18:54,000
 That's not what you're doing.

272
00:18:54,000 --> 00:18:59,000
 Letting go comes from the practice.

273
00:18:59,000 --> 00:19:00,000
 Why does it come?

274
00:19:00,000 --> 00:19:03,000
 Because you see clearly.

275
00:19:03,000 --> 00:19:05,000
 What you're doing is trying to see things,

276
00:19:05,000 --> 00:19:08,000
 just see things as they are.

277
00:19:08,000 --> 00:19:10,000
 Open your eyes.

278
00:19:10,000 --> 00:19:12,000
 Understand what you're doing to yourself.

279
00:19:12,000 --> 00:19:13,000
 You start to see.

280
00:19:13,000 --> 00:19:22,000
 You start to see what you're doing, what the mind is doing.

281
00:19:22,000 --> 00:19:23,000
 You see greed.

282
00:19:23,000 --> 00:19:24,000
 You see anger.

283
00:19:24,000 --> 00:19:25,000
 You see delusion.

284
00:19:25,000 --> 00:19:26,000
 You see conceit.

285
00:19:26,000 --> 00:19:31,000
 You see arrogance, all sorts of evil.

286
00:19:31,000 --> 00:19:32,000
 Why is it evil?

287
00:19:32,000 --> 00:19:38,000
 You can see it's causing you suffering.

288
00:19:38,000 --> 00:19:44,000
 So anyway, that's not a bit about suffering tonight.

289
00:19:44,000 --> 00:19:46,000
 It's so great to talk.

290
00:19:46,000 --> 00:19:47,000
 Such a great subject.

291
00:19:47,000 --> 00:19:50,000
 It's something we should talk about all the time.

292
00:19:50,000 --> 00:19:53,000
 We should all concentrate our efforts and our focus

293
00:19:53,000 --> 00:19:56,000
 our attention on suffering.

294
00:19:56,000 --> 00:19:57,000
 We should.

295
00:19:57,000 --> 00:20:00,000
 We should stop being such a taboo subject.

296
00:20:00,000 --> 00:20:01,000
 It's the problem.

297
00:20:01,000 --> 00:20:03,000
 It's what we should all be working on.

298
00:20:03,000 --> 00:20:05,000
 It's what the government should be working on.

299
00:20:05,000 --> 00:20:09,000
 It's what everyone, religious should be working on.

300
00:20:09,000 --> 00:20:10,000
 Forget about God.

301
00:20:10,000 --> 00:20:14,000
 God's not going to help you.

302
00:20:14,000 --> 00:20:19,000
 God's not the problem.

303
00:20:19,000 --> 00:20:22,000
 Praying is not the problem.

304
00:20:22,000 --> 00:20:25,000
 Suffering, that's the problem.

305
00:20:25,000 --> 00:20:26,000
 All right.

306
00:20:26,000 --> 00:20:30,000
 Let's look at some questions here.

307
00:20:30,000 --> 00:20:32,000
 It would really be nice if you could put the question

308
00:20:32,000 --> 00:20:33,000
 mark before the questions.

309
00:20:33,000 --> 00:20:37,000
 It's pretty easy if you figured out.

310
00:20:37,000 --> 00:20:42,000
 Make it easier for me to tell which a question.

311
00:20:42,000 --> 00:20:45,000
 Do you always associate the feeling with the thought

312
00:20:45,000 --> 00:20:48,000
 saying that this is the feeling?

313
00:20:48,000 --> 00:20:49,000
 This is the feeling?

314
00:20:49,000 --> 00:20:53,000
 That's really-- you have to be careful to be

315
00:20:53,000 --> 00:20:55,000
 grammatical in your questions.

316
00:20:55,000 --> 00:20:57,000
 I think it gives me a free pass to skip it

317
00:20:57,000 --> 00:21:03,000
 because I don't know what you're saying.

318
00:21:03,000 --> 00:21:05,000
 This feeling is from this thought.

319
00:21:05,000 --> 00:21:07,000
 Should they be a separate thing?

320
00:21:07,000 --> 00:21:09,000
 You only have to worry about what things are.

321
00:21:09,000 --> 00:21:12,000
 Don't focus too much in terms of your practice

322
00:21:12,000 --> 00:21:14,000
 on cause and effect.

323
00:21:14,000 --> 00:21:15,000
 You'll see cause and effect.

324
00:21:15,000 --> 00:21:17,000
 You'll see what leads to what.

325
00:21:17,000 --> 00:21:18,000
 That's fine.

326
00:21:18,000 --> 00:21:23,000
 But your practice should only be this is this.

327
00:21:23,000 --> 00:21:25,000
 Remember those words, this is this.

328
00:21:25,000 --> 00:21:27,000
 It's your whole practice.

329
00:21:27,000 --> 00:21:29,000
 Reminding yourself this is this.

330
00:21:29,000 --> 00:21:54,000
 Yeah, those kids' videos, if you haven't--

331
00:21:54,000 --> 00:22:01,000
 if you know of any kids who you think might be interested

332
00:22:01,000 --> 00:22:05,000
 in meditation, there's videos on how to meditate for kids.

333
00:22:05,000 --> 00:22:06,000
 It seemed to work.

334
00:22:06,000 --> 00:22:08,000
 It seemed to be beneficial.

335
00:22:08,000 --> 00:22:14,000
 How do we observe, understand, mind and mental objects?

336
00:22:14,000 --> 00:22:22,000
 Well, mental objects is just a translation of dhamma.

337
00:22:22,000 --> 00:22:25,000
 Dhamma doesn't really translate to mental objects,

338
00:22:25,000 --> 00:22:29,000
 but that's an explanatory translation.

339
00:22:29,000 --> 00:22:33,000
 Well, there's a sixth sense.

340
00:22:33,000 --> 00:22:35,000
 This is in the context of the sixth sense.

341
00:22:35,000 --> 00:22:41,000
 So seeing-- what's the object of seeing is light or forms,

342
00:22:41,000 --> 00:22:44,410
 which the object of hearing is sound, smells, tastes,

343
00:22:44,410 --> 00:22:45,000
 feelings.

344
00:22:45,000 --> 00:22:47,000
 The object of the mind is thoughts,

345
00:22:47,000 --> 00:22:55,420
 or dhamma just means something mental, something in the

346
00:22:55,420 --> 00:22:56,000
 mind.

347
00:22:56,000 --> 00:23:02,000
 I think that can be also if you see a vision in the mind

348
00:23:02,000 --> 00:23:07,000
 or if a song is going through your head,

349
00:23:07,000 --> 00:23:09,000
 then it's still, I think, called dhamma,

350
00:23:09,000 --> 00:23:11,000
 even though it was a sight or a sound.

351
00:23:11,000 --> 00:23:13,000
 Because it's not coming from the eyes,

352
00:23:13,000 --> 00:23:15,000
 it's considered to be dhamma.

353
00:23:15,000 --> 00:23:28,150
 It's suffering a direct effect of latching on the

354
00:23:28,150 --> 00:23:29,000
 artificial nature

355
00:23:29,000 --> 00:23:31,000
 of things rather than their true nature.

356
00:23:31,000 --> 00:23:41,000
 It's not all artificial nature causes suffering,

357
00:23:41,000 --> 00:23:46,000
 but it's certainly necessary.

358
00:23:46,000 --> 00:23:48,000
 If you see things truly as they are,

359
00:23:48,000 --> 00:23:51,000
 you won't latch onto them and you won't suffer.

360
00:23:51,000 --> 00:23:57,000
 So it's necessary but not sufficient, I think.

361
00:23:57,000 --> 00:24:01,000
 I would say if you see the artificial nature of things--

362
00:24:01,000 --> 00:24:04,000
 I mean, even enlightened people see,

363
00:24:04,000 --> 00:24:07,000
 enlightened beings see the artificial nature.

364
00:24:07,000 --> 00:24:09,000
 They know that this is a camera or a computer

365
00:24:09,000 --> 00:24:12,000
 or this is a monk sitting here.

366
00:24:12,000 --> 00:24:14,000
 That's artificial, but they know it.

367
00:24:14,000 --> 00:24:19,000
 They're not incapable of seeing it.

368
00:24:19,000 --> 00:24:25,000
 But the latching on, I guess, is the key here.

369
00:24:25,000 --> 00:24:28,000
 The latching onto anything is the problem.

370
00:24:28,000 --> 00:24:30,000
 It's the latching, not the artificial.

371
00:24:30,000 --> 00:24:33,000
 But we only latch because of the artificial.

372
00:24:33,000 --> 00:24:36,000
 You can't latch on when you see something as it is

373
00:24:36,000 --> 00:24:39,000
 because it arises and ceases.

374
00:24:39,000 --> 00:24:43,000
 It's absurd to think about latching onto it.

375
00:24:43,000 --> 00:24:51,000
 What are some good words for noting the transition

376
00:24:51,000 --> 00:24:56,000
 from standing to sitting and vice versa?

377
00:24:56,000 --> 00:25:02,000
 I mean, whatever-- bending, lifting, touching.

378
00:25:02,000 --> 00:25:05,000
 You should note intention.

379
00:25:05,000 --> 00:25:08,000
 If you can, try to catch the intention.

380
00:25:08,000 --> 00:25:15,000
 Wanting to stand, wanting to stand, bending, stretching,

381
00:25:15,000 --> 00:25:20,000
 wanting to sit, wanting to bending, lowering, touching.

382
00:25:20,000 --> 00:25:33,000
 Fifty-two viewers becoming-- things are really picking up.

383
00:25:33,000 --> 00:25:37,000
 I got more and more people coming to do meditation courses.

384
00:25:37,000 --> 00:25:43,030
 We really should think seriously about someday setting up

385
00:25:43,030 --> 00:25:44,000
 our own center,

386
00:25:44,000 --> 00:25:47,000
 not renting a house.

387
00:25:47,000 --> 00:25:50,000
 We should think about this.

388
00:25:50,000 --> 00:25:53,760
 We have lots of people interested, and we should talk about

389
00:25:53,760 --> 00:25:55,000
 it,

390
00:25:55,000 --> 00:25:59,000
 see if there's enough support.

391
00:25:59,000 --> 00:26:04,000
 I think-- because this isn't really stable.

392
00:26:04,000 --> 00:26:08,510
 There's-- I don't know what the situation of things is

393
00:26:08,510 --> 00:26:09,000
 going to be

394
00:26:09,000 --> 00:26:12,000
 whether we're always going to be able to rent like this.

395
00:26:12,000 --> 00:26:16,000
 And moreover, it's-- we're going to be full.

396
00:26:16,000 --> 00:26:22,000
 We're going to have four meditators throughout August

397
00:26:22,000 --> 00:26:26,000
 and into September.

398
00:26:26,000 --> 00:26:30,000
 It doesn't seem like a lot, but it's a full house.

399
00:26:30,000 --> 00:26:41,000
 So anyway, great to see such interest in these things.

400
00:26:41,000 --> 00:26:46,000
 It's great to think that this is somehow useful,

401
00:26:46,000 --> 00:26:50,720
 which reminds me, I'm probably going to try to set up a

402
00:26:50,720 --> 00:26:55,000
 testimonials page.

403
00:26:55,000 --> 00:26:57,000
 If you've been practicing meditation--

404
00:26:57,000 --> 00:27:02,130
 I'll probably make an announcement once we get it set up

405
00:27:02,130 --> 00:27:03,000
 again.

406
00:27:03,000 --> 00:27:05,000
 If you've been practicing this meditation

407
00:27:05,000 --> 00:27:09,000
 and feel like it's been beneficial to you,

408
00:27:09,000 --> 00:27:12,000
 if you could send us a testimonial--

409
00:27:12,000 --> 00:27:15,770
 this means some short words about how the meditation

410
00:27:15,770 --> 00:27:17,000
 benefited you--

411
00:27:17,000 --> 00:27:23,000
 send it to info@sirimongolo.org for now.

412
00:27:23,000 --> 00:27:27,000
 I'll try to set up a new email address or something.

413
00:27:27,000 --> 00:27:31,000
 But info works-- info@sirimongolo.org.

414
00:27:31,000 --> 00:27:33,000
 And we'll try to collect them, start collecting them,

415
00:27:33,000 --> 00:27:36,000
 and put them up on the website.

416
00:27:36,000 --> 00:27:39,000
 I also get from Facebook from time to time

417
00:27:39,000 --> 00:27:41,000
 and even YouTube from time to time,

418
00:27:41,000 --> 00:27:44,000
 so we'll try to collect them all together.

419
00:27:44,000 --> 00:27:46,000
 I think that's useful.

420
00:27:46,000 --> 00:27:52,000
 Most people decide whether we're legit,

421
00:27:52,000 --> 00:27:57,470
 that we're not some fly-by-night operation or some cult or

422
00:27:57,470 --> 00:27:59,000
 something.

423
00:27:59,000 --> 00:28:11,000
 Are some people more attached to aversion than to greed?

424
00:28:11,000 --> 00:28:16,000
 Yes, some people are more inclined towards anger.

425
00:28:16,000 --> 00:28:23,000
 How can we be sure of the accuracy of our notings?

426
00:28:23,000 --> 00:28:29,000
 Would noting falsely not consolidate delusion?

427
00:28:29,000 --> 00:28:32,000
 No, it would come from delusion, I think.

428
00:28:32,000 --> 00:28:35,000
 It's not that difficult.

429
00:28:35,000 --> 00:28:38,000
 Because if you note something like "cat, cat,"

430
00:28:38,000 --> 00:28:41,920
 it's not really clear and it's not cutting through the

431
00:28:41,920 --> 00:28:43,000
 delusion.

432
00:28:43,000 --> 00:28:48,000
 Not as well.

433
00:28:48,000 --> 00:28:51,000
 So you have to note realities.

434
00:28:51,000 --> 00:28:53,950
 If you hear something, like if you hear something and you

435
00:28:53,950 --> 00:28:55,000
 say "seeing,"

436
00:28:55,000 --> 00:28:57,000
 that's problematic, I suppose,

437
00:28:57,000 --> 00:29:00,000
 because without twisted that makes your mind.

438
00:29:00,000 --> 00:29:11,000
 [Silence]

439
00:29:11,000 --> 00:29:14,000
 All conditioned phenomena are dukkha,

440
00:29:14,000 --> 00:29:18,000
 it's still true even if you don't latch onto things.

441
00:29:18,000 --> 00:29:24,000
 Well, they're dukkha.

442
00:29:24,000 --> 00:29:26,000
 Dukkha doesn't mean they're suffering.

443
00:29:26,000 --> 00:29:32,000
 Dukkha means they're basically not worth clinging to.

444
00:29:32,000 --> 00:29:34,000
 And I know that's not a direct translation,

445
00:29:34,000 --> 00:29:36,000
 but it still relates to clinging.

446
00:29:36,000 --> 00:29:38,980
 If you don't cling to something, you can't cause yourself

447
00:29:38,980 --> 00:29:40,000
 suffering.

448
00:29:40,000 --> 00:29:46,000
 Dukkha means basically not worth clinging to.

449
00:29:46,000 --> 00:29:50,000
 That's the explanatory translation.

450
00:29:50,000 --> 00:29:55,000
 So in Arahant you could say it still has dukkha?

451
00:29:55,000 --> 00:29:56,000
 Not really.

452
00:29:56,000 --> 00:29:59,000
 Yes, in a sense, and they would admit that,

453
00:29:59,000 --> 00:30:00,000
 and the Buddha would admit that,

454
00:30:00,000 --> 00:30:03,000
 but in an ultimate sense it's not really suffering.

455
00:30:03,000 --> 00:30:06,000
 They're not suffering.

456
00:30:06,000 --> 00:30:10,000
 Dukkha means when you cling to it, it causes you suffering.

457
00:30:10,000 --> 00:30:13,000
 Why? Because it's impermanent.

458
00:30:13,000 --> 00:30:16,000
 You won't meet your expectations.

459
00:30:16,000 --> 00:30:17,000
 It's unpredictable.

460
00:30:17,000 --> 00:30:19,000
 You don't like it, well it might come.

461
00:30:19,000 --> 00:30:23,000
 You like it, well it might go.

462
00:30:23,000 --> 00:30:24,000
 It's not under your control.

463
00:30:24,000 --> 00:30:27,000
 It's a nitya dukkha anabta.

464
00:30:27,000 --> 00:30:30,000
 It means these things are not worth clinging to basically.

465
00:30:30,000 --> 00:30:33,000
 Sabe damma na langa minyue saiya.

466
00:30:33,000 --> 00:30:35,000
 That's all you need to know.

467
00:30:35,000 --> 00:30:38,000
 No damma is worth clinging to.

468
00:30:38,000 --> 00:30:46,000
 Alright, enough for tonight.

469
00:30:46,000 --> 00:30:48,000
 Thank you all for tuning in.

470
00:30:48,000 --> 00:30:51,000
 Wishing you all good practice.

471
00:30:51,000 --> 00:30:52,000
 Be well.

472
00:30:53,000 --> 00:30:54,000
 Thank you.

