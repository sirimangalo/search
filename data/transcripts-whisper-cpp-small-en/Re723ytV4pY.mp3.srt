1
00:00:00,000 --> 00:00:04,210
 Is the sitting touching technique a must in meditation? If

2
00:00:04,210 --> 00:00:08,440
 so, is it noted between breaths or instead of the breaths?

3
00:00:08,440 --> 00:00:12,540
 But there are different ways of approaching this. My

4
00:00:12,540 --> 00:00:16,000
 teacher is emphatic that it's instead of the breath and I

5
00:00:16,000 --> 00:00:17,220
 think this is correct

6
00:00:17,220 --> 00:00:20,720
 it seems the most reasonable because

7
00:00:20,720 --> 00:00:25,340
 The point is to be mindful of one thing at a time. You need

8
00:00:25,340 --> 00:00:27,440
 concentration. You need your mind to be focused

9
00:00:27,440 --> 00:00:30,470
 So when you focus on the rising you should only be aware of

10
00:00:30,470 --> 00:00:32,760
 the rising when you focus on the falling

11
00:00:32,760 --> 00:00:35,160
 You should only be aware of the falling when you focus on

12
00:00:35,160 --> 00:00:35,640
 sitting

13
00:00:35,640 --> 00:00:37,610
 You should only be aware of sitting when you focus on

14
00:00:37,610 --> 00:00:40,140
 touching you should only be aware of touching so we would

15
00:00:40,140 --> 00:00:41,560
 say rising

16
00:00:41,560 --> 00:00:46,020
 Falling and then whether the breath rises whether the

17
00:00:46,020 --> 00:00:47,600
 stomach rises and falls or not

18
00:00:47,600 --> 00:00:50,900
 However many times it rises and falls. We don't pay

19
00:00:50,900 --> 00:00:52,280
 attention to it. We

20
00:00:52,280 --> 00:00:56,200
 try as as as

21
00:00:57,320 --> 00:01:00,460
 Much time as it takes to be mindful that we're sitting once

22
00:01:00,460 --> 00:01:02,520
 you are mindful that you're sitting say to yourself

23
00:01:02,520 --> 00:01:04,840
 sitting and

24
00:01:04,840 --> 00:01:07,790
 Then once you mind once you've done that become aware of a

25
00:01:07,790 --> 00:01:10,840
 spot on your body and focus on that and say to yourself

26
00:01:10,840 --> 00:01:12,600
 touching

27
00:01:12,600 --> 00:01:16,020
 Rising and then and then go back and look and wait for the

28
00:01:16,020 --> 00:01:19,600
 next rising. So it would be instead of the breath now

29
00:01:19,600 --> 00:01:23,590
 Is it sorry? The other question is is it a must? No, it's

30
00:01:23,590 --> 00:01:24,360
 certainly not a must

31
00:01:24,840 --> 00:01:27,480
 the only reason for adding something like sitting or

32
00:01:27,480 --> 00:01:29,320
 something like touching is because

33
00:01:29,320 --> 00:01:33,640
 Your mind is becoming more powerful your mind is becoming

34
00:01:33,640 --> 00:01:39,120
 more sharp and more adept at the meditation. So

35
00:01:39,120 --> 00:01:42,870
 In ordinary daily meditation practice you can just do

36
00:01:42,870 --> 00:01:46,720
 rising falling rising flying and in fact it is enough but

37
00:01:46,720 --> 00:01:49,040
 the

38
00:01:49,040 --> 00:01:51,040
 added

39
00:01:52,880 --> 00:01:56,030
 Difficulty of going between different objects who have

40
00:01:56,030 --> 00:01:58,190
 searched two purposes one because it's many different

41
00:01:58,190 --> 00:01:58,760
 objects

42
00:01:58,760 --> 00:02:01,690
 The mind is interested in it. And so it keeps the mind from

43
00:02:01,690 --> 00:02:02,240
 running away

44
00:02:02,240 --> 00:02:05,600
 And the other one is that it forces the mind to be

45
00:02:05,600 --> 00:02:07,480
 refined

46
00:02:07,480 --> 00:02:09,480
 forces the mind to

47
00:02:09,480 --> 00:02:12,300
 Become more and more refined and focuses the practice to

48
00:02:12,300 --> 00:02:15,000
 become more and more refined because otherwise you'll never

49
00:02:15,000 --> 00:02:16,440
 get through all the different points

50
00:02:16,440 --> 00:02:18,600
 So we'll give you many different points on the body to be

51
00:02:18,600 --> 00:02:21,120
 rising falling sitting touching rising falling sitting

52
00:02:21,120 --> 00:02:21,520
 touching

53
00:02:22,640 --> 00:02:25,060
 And you wouldn't be able to do it if your mind were not

54
00:02:25,060 --> 00:02:30,840
 refined so it encourages the mind to give up and to to

55
00:02:30,840 --> 00:02:35,970
 Pay no attention to to the diversions and distractions that

56
00:02:35,970 --> 00:02:38,540
 would otherwise lead us away from the meditation

57
00:02:38,540 --> 00:02:43,640
 Because you have something to do you have work to do so it

58
00:02:43,640 --> 00:02:47,120
 forces the mind to become fine-tuned and it trains the mind

59
00:02:47,840 --> 00:02:51,420
 Just like in weightlifting you could you know train

60
00:02:51,420 --> 00:02:54,360
 yourself by just lifting a small weights

61
00:02:54,360 --> 00:02:56,480
 but

62
00:02:56,480 --> 00:02:59,790
 When when you put more and more weight on it trains you

63
00:02:59,790 --> 00:03:03,960
 further and further and it has a benefit as a result

64
00:03:03,960 --> 00:03:05,960
 you

