1
00:00:00,000 --> 00:00:04,690
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,690 --> 00:00:08,910
 Today we continue with verse number 15, which goes as

3
00:00:08,910 --> 00:00:10,000
 follows.

4
00:00:10,000 --> 00:00:17,100
 "Indaso chati, pecha so chati, papakari, umayata so chati,

5
00:00:17,100 --> 00:00:24,000
 so so chati, so vihanyati, tisvakamakilitamatano."

6
00:00:24,000 --> 00:00:29,530
 Which means, "Indaso chati, here he grieves, pecha so chati

7
00:00:29,530 --> 00:00:35,500
, hereafter he grieves, papakari, umayata so chati, the evil

8
00:00:35,500 --> 00:00:40,640
 doer, grieves in both places, so so chati, so vihanyati, he

9
00:00:40,640 --> 00:00:46,000
 grieves, he is afflicted, he is destroyed,

10
00:00:46,000 --> 00:00:53,650
 disvakamakilitamatano." Seeing the evil or the defilement

11
00:00:53,650 --> 00:00:56,000
 of his own actions.

12
00:00:56,000 --> 00:01:02,870
 This is the teaching on negative karma, on evil karma, evil

13
00:01:02,870 --> 00:01:04,000
 deeds.

14
00:01:04,000 --> 00:01:06,880
 We have two verses here, the next verse is in regards to a

15
00:01:06,880 --> 00:01:08,000
 different story.

16
00:01:08,000 --> 00:01:11,100
 So the story with this verse is that in the time of the

17
00:01:11,100 --> 00:01:13,820
 Buddha there was a pig butcher who lived right next to Veda

18
00:01:13,820 --> 00:01:15,000
 Vana,

19
00:01:15,000 --> 00:01:17,000
 where the Buddha was staying in Rajagat.

20
00:01:17,000 --> 00:01:19,780
 And when the monks would go on Amsharand, they would walk

21
00:01:19,780 --> 00:01:23,220
 past and they would hear the screams, or they would even

22
00:01:23,220 --> 00:01:25,000
 see the pigs being killed.

23
00:01:25,000 --> 00:01:30,310
 And the way the commentary explains the killing went on, is

24
00:01:30,310 --> 00:01:34,480
 this man, he would tie the pig to a post so it couldn't

25
00:01:34,480 --> 00:01:35,000
 move,

26
00:01:35,000 --> 00:01:38,680
 and then beat it with a club to make its skin all tender,

27
00:01:38,680 --> 00:01:42,000
 its flesh tender, but it was still alive.

28
00:01:42,000 --> 00:01:45,460
 And then take boiling hot water and pour it down the pigs

29
00:01:45,460 --> 00:01:51,580
 throat, and have all the feces and undigested food pour out

30
00:01:51,580 --> 00:01:53,000
 the bottom.

31
00:01:53,000 --> 00:01:56,780
 And he would continue to pour boiling water in until the

32
00:01:56,780 --> 00:02:01,100
 water that came out was clean, and meaning there was no

33
00:02:01,100 --> 00:02:03,000
 feces left inside.

34
00:02:03,000 --> 00:02:06,970
 Then he would pour the rest of the water over the pigs skin

35
00:02:06,970 --> 00:02:11,460
 to peel off the outer skin, and use the torch to burn off

36
00:02:11,460 --> 00:02:13,000
 all the pigs hair.

37
00:02:13,000 --> 00:02:16,480
 And then he would cut off the pigs head, and then it would

38
00:02:16,480 --> 00:02:17,000
 die.

39
00:02:17,000 --> 00:02:20,370
 And he would collect the blood from the pigs neck, and

40
00:02:20,370 --> 00:02:24,190
 roast the pig in its own blood, eat as much as he could

41
00:02:24,190 --> 00:02:27,000
 with his family, and sell the rest.

42
00:02:27,000 --> 00:02:30,000
 And they say for 55 years he lived his life like this.

43
00:02:30,000 --> 00:02:34,430
 And one day the monks were walking by 55 years later, and

44
00:02:34,430 --> 00:02:39,960
 they heard, they saw that today all of the doors of the

45
00:02:39,960 --> 00:02:42,000
 house were closed up.

46
00:02:42,000 --> 00:02:48,240
 And they could still hear the screams of pigs and grunting

47
00:02:48,240 --> 00:02:51,000
 noises and scuffling.

48
00:02:51,000 --> 00:02:53,400
 And so they assumed that there must be something great

49
00:02:53,400 --> 00:02:54,000
 going on.

50
00:02:54,000 --> 00:02:57,770
 And so they started talking about this, how amazing it was

51
00:02:57,770 --> 00:03:01,590
 that for 55 years this man had never learned the weight of

52
00:03:01,590 --> 00:03:03,000
 the evil of his deeds.

53
00:03:03,000 --> 00:03:08,120
 And had never even come to listen to the Buddha's teaching,

54
00:03:08,120 --> 00:03:12,000
 or come to pay respect to the Buddha at all.

55
00:03:12,000 --> 00:03:15,430
 And the Buddha heard them talking, and asked them, "What

56
00:03:15,430 --> 00:03:17,000
 are you talking about?"

57
00:03:17,000 --> 00:03:20,000
 They explained what it was.

58
00:03:20,000 --> 00:03:25,280
 And the Buddha said, "They're not." And they said, "Oh, and

59
00:03:25,280 --> 00:03:28,000
 today we see that he's, well for seven days now."

60
00:03:28,000 --> 00:03:31,300
 Seven days later we see that they have been closed up for

61
00:03:31,300 --> 00:03:34,110
 seven days, so they must be working on an even bigger

62
00:03:34,110 --> 00:03:38,000
 slaughter, or a special slaughter of some sort.

63
00:03:38,000 --> 00:03:40,470
 And the Buddha said, "No, that's not really what's going on

64
00:03:40,470 --> 00:03:41,000
 at all."

65
00:03:41,000 --> 00:03:45,500
 Seven days ago it turns out that the Chunda, this pork

66
00:03:45,500 --> 00:03:49,360
 butcher, became very ill, and his affliction went to his

67
00:03:49,360 --> 00:03:51,000
 head and he became quite deranged.

68
00:03:51,000 --> 00:03:55,550
 And as a result of the sickness and the derangement, he

69
00:03:55,550 --> 00:04:00,010
 started crawling around the house on his hands and knees

70
00:04:00,010 --> 00:04:01,000
 and grunting like a pig,

71
00:04:01,000 --> 00:04:04,000
 squeezing like a pig, and this is what they heard.

72
00:04:04,000 --> 00:04:06,750
 And the people in the house would try to restrain him, but

73
00:04:06,750 --> 00:04:08,000
 they couldn't do it.

74
00:04:08,000 --> 00:04:11,430
 Also because, as the commentary said, he began to see his

75
00:04:11,430 --> 00:04:15,000
 future, and this brought him great derangement.

76
00:04:15,000 --> 00:04:20,220
 On his deathbed, the fear and the concentration of the mind

77
00:04:20,220 --> 00:04:25,560
 at that point allowed him to see his future, and he began

78
00:04:25,560 --> 00:04:27,000
 to...

79
00:04:27,000 --> 00:04:29,000
 It drove him crazy.

80
00:04:29,000 --> 00:04:31,460
 And as a result all he could think about was the killing of

81
00:04:31,460 --> 00:04:37,000
 the pigs, and as a result, dwelt that way for seven days.

82
00:04:37,000 --> 00:04:40,460
 Then on that day, on the seventh day, it was over, and he

83
00:04:40,460 --> 00:04:42,000
 died and went to hell.

84
00:04:42,000 --> 00:04:44,000
 So the Buddha explained what was really going on.

85
00:04:44,000 --> 00:04:46,260
 And then he said to the monks, "This is how it goes. It's

86
00:04:46,260 --> 00:04:49,810
 not just in the next life that a person suffers, a person

87
00:04:49,810 --> 00:04:52,000
 who does evil deeds.

88
00:04:52,000 --> 00:04:55,990
 A person who does evil deeds suffers both in this life and

89
00:04:55,990 --> 00:04:57,000
 the next."

90
00:04:57,000 --> 00:05:03,230
 So how this relates to our practice, I think, should be

91
00:05:03,230 --> 00:05:05,000
 quite clear.

92
00:05:05,000 --> 00:05:07,480
 And it becomes quite clear when a person undertakes

93
00:05:07,480 --> 00:05:10,640
 practice, because for the first time they're able to

94
00:05:10,640 --> 00:05:14,000
 realize the full magnitude of their deeds.

95
00:05:14,000 --> 00:05:17,950
 I think, or it's clear that people who don't practice

96
00:05:17,950 --> 00:05:22,270
 meditation are unable to understand the law of karma, and

97
00:05:22,270 --> 00:05:25,700
 they think of it as some kind of belief or some kind of

98
00:05:25,700 --> 00:05:27,000
 theory.

99
00:05:27,000 --> 00:05:29,390
 And they don't really understand, because when they do evil

100
00:05:29,390 --> 00:05:31,360
 deeds they look around and they don't see anything

101
00:05:31,360 --> 00:05:32,000
 happening.

102
00:05:32,000 --> 00:05:34,580
 They think, "I killed someone, or I killed an animal, and

103
00:05:34,580 --> 00:05:36,000
 now I'm going to be killed."

104
00:05:36,000 --> 00:05:38,000
 They look around and they're not killed.

105
00:05:38,000 --> 00:05:40,350
 So they think, "Well, then there's nothing wrong with doing

106
00:05:40,350 --> 00:05:41,000
 evil deeds."

107
00:05:41,000 --> 00:05:44,770
 Because they can't see really what is the result of a good

108
00:05:44,770 --> 00:05:46,000
 or an evil deed.

109
00:05:46,000 --> 00:05:49,000
 When a person comes to practice meditation, they're

110
00:05:49,000 --> 00:05:51,080
 watching every moment, and they're seeing quite clearly

111
00:05:51,080 --> 00:05:53,000
 what are the results of their deeds.

112
00:05:53,000 --> 00:05:56,000
 When they cling to something, what is the result?

113
00:05:56,000 --> 00:05:58,480
 When they are angry or upset about something, what is the

114
00:05:58,480 --> 00:05:59,000
 result?

115
00:05:59,000 --> 00:06:00,720
 How does it affect their body? How does it affect their

116
00:06:00,720 --> 00:06:01,000
 mind?

117
00:06:01,000 --> 00:06:07,000
 How does it affect their clarity and their character?

118
00:06:07,000 --> 00:06:12,010
 And so as a result, the law of karma becomes quite clear

119
00:06:12,010 --> 00:06:14,000
 and quite evident.

120
00:06:14,000 --> 00:06:18,810
 But for evildoers, for people who are engaged in great evil

121
00:06:18,810 --> 00:06:21,000
, it's often not clear.

122
00:06:21,000 --> 00:06:26,000
 And it can often be the fact that because of their position

123
00:06:26,000 --> 00:06:29,410
 and because of good past deeds and their relative stability

124
00:06:29,410 --> 00:06:30,000
 of mind,

125
00:06:30,000 --> 00:06:35,000
 they're unable to see the small seed growing inside.

126
00:06:35,000 --> 00:06:38,570
 And moreover, they're often able, through the power of the

127
00:06:38,570 --> 00:06:42,830
 mind, to push it away and to avoid looking at it, to avoid

128
00:06:42,830 --> 00:06:44,000
 thinking about it.

129
00:06:44,000 --> 00:06:48,440
 So that they aren't aware of evil deeds until it becomes so

130
00:06:48,440 --> 00:06:51,000
 great and so gross that they can no longer hide it,

131
00:06:51,000 --> 00:06:54,140
 especially when they become sick and are on their deathbed,

132
00:06:54,140 --> 00:06:57,000
 and their power of mind becomes much weaker.

133
00:06:57,000 --> 00:07:00,080
 An ordinary person spends most of their time covering up

134
00:07:00,080 --> 00:07:01,000
 their deeds.

135
00:07:01,000 --> 00:07:05,300
 Now, it does happen that good people do bad deeds and do

136
00:07:05,300 --> 00:07:07,000
 feel guilty and feel remorseful

137
00:07:07,000 --> 00:07:11,000
 and are able to see how it's affected them.

138
00:07:11,000 --> 00:07:14,280
 When a person does, or if a person starts going down a

139
00:07:14,280 --> 00:07:16,760
 wrong path and they start to realize the nature of their

140
00:07:16,760 --> 00:07:17,000
 mind,

141
00:07:17,000 --> 00:07:20,720
 they burst out at someone and they realize that they have a

142
00:07:20,720 --> 00:07:22,000
 lot of anger inside.

143
00:07:22,000 --> 00:07:26,000
 Then they might feel guilty and that there's something that

144
00:07:26,000 --> 00:07:27,000
 should be done,

145
00:07:27,000 --> 00:07:30,900
 and as a result, they will begin to refrain from that in

146
00:07:30,900 --> 00:07:32,000
 the future.

147
00:07:32,000 --> 00:07:36,920
 But it can often be the case that, as in the case of Chunda

148
00:07:36,920 --> 00:07:40,000
, that the realization only comes when it's too late.

149
00:07:40,000 --> 00:07:43,990
 So people often ask, "Well, if there is this law of karma,

150
00:07:43,990 --> 00:07:48,170
 why do we see good people suffering and bad people prosper

151
00:07:48,170 --> 00:07:49,000
ing?"

152
00:07:49,000 --> 00:07:52,290
 But when you practice meditation, when you actually look

153
00:07:52,290 --> 00:07:55,000
 and examine what's going on inside,

154
00:07:55,000 --> 00:07:58,110
 you'll see that it's true that good people might suffer,

155
00:07:58,110 --> 00:08:00,000
 but their suffering isn't because of their goodness.

156
00:08:00,000 --> 00:08:03,540
 And it's true that evil people may prosper, but their

157
00:08:03,540 --> 00:08:06,000
 prospering is not because of their evil.

158
00:08:06,000 --> 00:08:10,000
 It can't be. And you can see this by watching clearly what,

159
00:08:10,000 --> 00:08:13,000
 as I said, happens when these mind states arise.

160
00:08:13,000 --> 00:08:16,660
 They lead to it. You can see that a good state can never

161
00:08:16,660 --> 00:08:18,000
 lead to suffering,

162
00:08:18,000 --> 00:08:23,970
 and an evil state can never lead to good or to happiness or

163
00:08:23,970 --> 00:08:26,000
 to prosperity.

164
00:08:26,000 --> 00:08:31,030
 It's only generally in this life is because of our position

165
00:08:31,030 --> 00:08:36,250
 and because of the static nature of the material body and

166
00:08:36,250 --> 00:08:38,000
 of the material world.

167
00:08:38,000 --> 00:08:41,550
 So a person might be born into a position of power or a

168
00:08:41,550 --> 00:08:44,720
 position of wealth, and as a result they can do many evil

169
00:08:44,720 --> 00:08:47,000
 deeds without great repercussions,

170
00:08:47,000 --> 00:08:50,850
 except in the mind, which of course is much more dynamic

171
00:08:50,850 --> 00:08:54,000
 and much quicker to pick up on the changes.

172
00:08:54,000 --> 00:08:57,000
 But eventually, even the physical will begin to pick it up.

173
00:08:57,000 --> 00:09:02,000
 You will lose your friends. You will gain corrupt friends.

174
00:09:02,000 --> 00:09:05,210
 Your whole environment will slowly change, but it changes a

175
00:09:05,210 --> 00:09:09,000
 lot slower, and so the results can be a lot slower coming

176
00:09:09,000 --> 00:09:12,000
 and quite a bit less evident.

177
00:09:12,000 --> 00:09:15,430
 Now at the moment of death, all of that changes because the

178
00:09:15,430 --> 00:09:18,000
 physical is removed from the equation.

179
00:09:18,000 --> 00:09:21,460
 The mind is now only depending on the mind. There is no

180
00:09:21,460 --> 00:09:24,000
 going back to seeing, hearing, smelling, tasting.

181
00:09:24,000 --> 00:09:27,920
 There's nothing based on the body. There's only the mental

182
00:09:27,920 --> 00:09:29,000
 activity.

183
00:09:29,000 --> 00:09:34,240
 So it's a repeated experience of the deeds that one has

184
00:09:34,240 --> 00:09:38,000
 done, or memories, or whatever is going on in the mind.

185
00:09:38,000 --> 00:09:41,520
 So if one has a mind that is full of corruption, then this

186
00:09:41,520 --> 00:09:44,000
 will continue repeatedly, arise in the mind,

187
00:09:44,000 --> 00:09:47,040
 thoughts of the evil deeds that we've done, or memories of

188
00:09:47,040 --> 00:09:49,000
 the evil deeds, or thoughts about the future,

189
00:09:49,000 --> 00:09:53,270
 or a feeling of where you're being pulled, the future that

190
00:09:53,270 --> 00:09:56,000
 you're developing for yourself.

191
00:09:56,000 --> 00:09:58,370
 And this is clear, many people actually experience this on

192
00:09:58,370 --> 00:10:00,000
 their death bed and can relate this.

193
00:10:00,000 --> 00:10:02,530
 Some people actually leave their bodies and can see their

194
00:10:02,530 --> 00:10:06,510
 bodies, or can see their loved ones, or so on, and find

195
00:10:06,510 --> 00:10:08,000
 themselves floating away.

196
00:10:08,000 --> 00:10:10,420
 If they have a near-death experience, they'll come back and

197
00:10:10,420 --> 00:10:13,260
 they'll be able to relate good things or bad things that

198
00:10:13,260 --> 00:10:15,000
 they saw or that happened to them.

199
00:10:15,000 --> 00:10:17,630
 And it will be based on whatever is being clung to in their

200
00:10:17,630 --> 00:10:21,910
 mind, whatever their mind is attaching importance to at

201
00:10:21,910 --> 00:10:23,000
 that time.

202
00:10:23,000 --> 00:10:27,300
 So if a person is greatly engaged in evil, evil deeds, then

203
00:10:27,300 --> 00:10:31,000
 they will be born in a place that is full of suffering

204
00:10:31,000 --> 00:10:34,000
 because of their corrupt state of mind, as the Buddha said.

205
00:10:34,000 --> 00:10:39,000
 When they see the evil, they will finally realize.

206
00:10:39,000 --> 00:10:44,000
 So in some sense it's much better to actually see the evil

207
00:10:44,000 --> 00:10:46,000
 and to feel sorrow.

208
00:10:46,000 --> 00:10:48,760
 We had a discussion about this as to whether this is what

209
00:10:48,760 --> 00:10:51,000
 the Buddha meant by "heary" and "otapa".

210
00:10:51,000 --> 00:10:53,590
 And it isn't really what he meant by "heary" and "otapa" to

211
00:10:53,590 --> 00:10:55,000
 feel shame or to feel guilt.

212
00:10:55,000 --> 00:10:58,610
 But to see that you've done bad things can be a great

213
00:10:58,610 --> 00:11:02,640
 waking up and can lead you to feel shame and to feel "heary

214
00:11:02,640 --> 00:11:04,000
" and "otapa".

215
00:11:04,000 --> 00:11:08,540
 Meaning in the future, when the opportunity presents itself

216
00:11:08,540 --> 00:11:12,000
 to do an evil deed, he will shy away from it.

217
00:11:12,000 --> 00:11:14,000
 He will recoil from it.

218
00:11:14,000 --> 00:11:18,880
 The mind will naturally incline against it because of

219
00:11:18,880 --> 00:11:24,000
 seeing the evil of it and seeing the evil nature of it.

220
00:11:24,000 --> 00:11:26,000
 And this is what we gain very much from meditation, from

221
00:11:26,000 --> 00:11:27,000
 just introspection.

222
00:11:27,000 --> 00:11:30,000
 A person just begins to come to meditate.

223
00:11:30,000 --> 00:11:34,040
 In the very beginning they will see all of what they have

224
00:11:34,040 --> 00:11:37,110
 carried with them and built up over the years through not

225
00:11:37,110 --> 00:11:38,000
 meditating.

226
00:11:38,000 --> 00:11:42,660
 They'll see all of the habits and tendencies, often a big

227
00:11:42,660 --> 00:11:46,000
 wave of defilement, of corruption.

228
00:11:46,000 --> 00:11:50,380
 At times it can be great anger and hatred. At times it can

229
00:11:50,380 --> 00:11:54,000
 be great lust and need and wanting.

230
00:11:54,000 --> 00:12:01,560
 And so they are able to realize, as it says here, they will

231
00:12:01,560 --> 00:12:04,310
 grieve in the beginning and they will feel great suffering

232
00:12:04,310 --> 00:12:05,000
 in the beginning.

233
00:12:05,000 --> 00:12:09,760
 But eventually it will be a cause for them to change their

234
00:12:09,760 --> 00:12:10,000
 way.

235
00:12:10,000 --> 00:12:13,130
 And in the future they won't want to do these things

236
00:12:13,130 --> 00:12:17,000
 because they know how much it hurts and affects their mind.

237
00:12:17,000 --> 00:12:21,530
 So this is the story of "Junda the Pork Butcher" and the

238
00:12:21,530 --> 00:12:25,020
 Buddha's teaching on the suffering that comes from doing

239
00:12:25,020 --> 00:12:26,000
 evil deeds.

240
00:12:26,000 --> 00:12:30,140
 So thanks for tuning in. This has been another episode of

241
00:12:30,140 --> 00:12:32,000
 our study of the Dhammapada.

