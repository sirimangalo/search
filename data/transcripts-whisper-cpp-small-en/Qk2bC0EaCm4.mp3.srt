1
00:00:00,000 --> 00:00:04,570
 Hello and welcome back to Ask a Monk. Today I will be

2
00:00:04,570 --> 00:00:06,000
 answering the question

3
00:00:06,000 --> 00:00:14,610
 as to what should we do when confronted by situations where

4
00:00:14,610 --> 00:00:17,640
 we feel necessary to

5
00:00:17,640 --> 00:00:24,570
 perform violent acts, specifically in terms of killing

6
00:00:24,570 --> 00:00:25,720
 would we deem to be

7
00:00:25,720 --> 00:00:33,950
 pests, those sentient beings that are causing suffering for

8
00:00:33,950 --> 00:00:36,200
 ourselves or those

9
00:00:36,200 --> 00:00:43,920
 around us or are confronting us with a situation where we

10
00:00:43,920 --> 00:00:45,840
 were put in in some

11
00:00:45,840 --> 00:00:53,320
 great physical difficulty. So the example that was given on

12
00:00:53,320 --> 00:00:54,320
 the ask.surimongalow.org

13
00:00:54,320 --> 00:01:00,040
 was about rats and in this case it's not even a physical

14
00:01:00,040 --> 00:01:09,080
 difficulty that they're giving. It's a problem with the

15
00:01:09,080 --> 00:01:09,680
 landlord.

16
00:01:09,680 --> 00:01:15,440
 So the landlord has given an ultimatum that, not an ultim

17
00:01:15,440 --> 00:01:16,520
atum, the requirement

18
00:01:16,520 --> 00:01:24,430
 that the rats be taken care of. And so the question is what

19
00:01:24,430 --> 00:01:25,520
 to do at this point.

20
00:01:25,520 --> 00:01:38,440
 So first of all, this question, really the core issue here,

21
00:01:38,440 --> 00:01:39,080
 not in

22
00:01:39,080 --> 00:01:45,000
 regards to specific instances, but the core theory that we

23
00:01:45,000 --> 00:01:45,520
 have to get

24
00:01:45,520 --> 00:01:54,840
 through or come to understand, is the difference between

25
00:01:54,840 --> 00:02:03,550
 physical well-being and mental well-being. And so the

26
00:02:03,550 --> 00:02:06,760
 problem is that we

27
00:02:06,760 --> 00:02:13,670
 quite often think more of our physical well-being than of

28
00:02:13,670 --> 00:02:14,880
 our mental well-being.

29
00:02:14,880 --> 00:02:19,080
 And we'll often place our physical well-being ahead of our

30
00:02:19,080 --> 00:02:20,160
 mental well-being,

31
00:02:20,160 --> 00:02:24,470
 not realizing that this is actually the choice we're making

32
00:02:24,470 --> 00:02:25,320
 and that by

33
00:02:25,320 --> 00:02:31,400
 performing violent acts towards other beings that we're

34
00:02:31,400 --> 00:02:34,280
 actually hurting our

35
00:02:34,280 --> 00:02:39,550
 mental health, our mental well-being. And so we will commit

36
00:02:39,550 --> 00:02:40,800
 egregious acts of

37
00:02:40,800 --> 00:02:47,430
 violence against other living beings seeking out some state

38
00:02:47,430 --> 00:02:48,360
 of physical

39
00:02:48,360 --> 00:02:53,730
 well-being for ourselves or for others. And so this goes

40
00:02:53,730 --> 00:02:55,840
 for a great number of

41
00:02:55,840 --> 00:03:01,570
 situations and this could even be extended into acts of war

42
00:03:01,570 --> 00:03:02,680
 and acts of

43
00:03:02,680 --> 00:03:06,980
 murder and assassination and so on. And so the question of

44
00:03:06,980 --> 00:03:08,240
 whether it would

45
00:03:08,240 --> 00:03:13,080
 have been right or wrong to murder someone like Adolf

46
00:03:13,080 --> 00:03:14,660
 Hitler or Osama Bin

47
00:03:14,660 --> 00:03:20,960
 Laden if you had the chance. And so this isn't exactly what

48
00:03:20,960 --> 00:03:21,240
 we're dealing

49
00:03:21,240 --> 00:03:25,770
 with here, but the underlying issue is our physical well-

50
00:03:25,770 --> 00:03:26,480
being versus

51
00:03:26,480 --> 00:03:31,080
 our mental well-being. And so in cases of pests, in cases

52
00:03:31,080 --> 00:03:33,120
 of dangerous animals, even

53
00:03:33,120 --> 00:03:42,740
 snakes and scorpions and so on, we often react unmindfully,

54
00:03:42,740 --> 00:03:44,000
 not realizing that

55
00:03:44,000 --> 00:03:49,520
 we're actually working towards our own detriment. And even

56
00:03:49,520 --> 00:03:51,360
 though by

57
00:03:51,360 --> 00:03:55,440
 acting in such a way we might further our physical well-

58
00:03:55,440 --> 00:03:56,400
being for some time,

59
00:03:56,400 --> 00:04:05,610
 we're actually causing a great amount of deterioration in

60
00:04:05,610 --> 00:04:06,480
 our mental well-being.

61
00:04:06,480 --> 00:04:12,600
 So we can ask ourselves which is more important, whether it

62
00:04:12,600 --> 00:04:12,720
's

63
00:04:12,720 --> 00:04:19,430
 better that we live a healthy and strong physical life with

64
00:04:19,430 --> 00:04:20,960
 a corrupt and

65
00:04:20,960 --> 00:04:26,440
 evil and unwholesome mind, or whether we die with a pure

66
00:04:26,440 --> 00:04:29,360
 mind. And for most

67
00:04:29,360 --> 00:04:34,270
 people because of our inability to see beyond this human

68
00:04:34,270 --> 00:04:37,480
 state, beyond this one

69
00:04:37,480 --> 00:04:43,760
 life, what we call this life, this birth, this existence,

70
00:04:43,760 --> 00:04:46,000
 because this seems to be

71
00:04:46,000 --> 00:04:49,200
 all that there is and because we're so entrenched in this

72
00:04:49,200 --> 00:04:50,220
 idea of the human

73
00:04:50,220 --> 00:04:54,720
 state as being the ultimate, that we see nothing wrong with

74
00:04:54,720 --> 00:04:55,680
 committing

75
00:04:55,680 --> 00:05:02,360
 egregious acts and even with sullying our minds for

76
00:05:02,360 --> 00:05:04,740
 immediate pleasure,

77
00:05:04,740 --> 00:05:09,470
 not realizing that we're accumulating these tendencies in

78
00:05:09,470 --> 00:05:10,840
 our minds and the

79
00:05:10,840 --> 00:05:15,870
 mind doesn't go away, that it continues on and there is no

80
00:05:15,870 --> 00:05:17,560
 end that we might

81
00:05:17,560 --> 00:05:21,160
 call death or so on. As long as we have these tendencies

82
00:05:21,160 --> 00:05:24,760
 they will increase and

83
00:05:24,760 --> 00:05:29,080
 they will ever and again lead us to conflict and suffering

84
00:05:29,080 --> 00:05:29,600
 and they're

85
00:05:29,600 --> 00:05:33,350
 actually setting us up for greater and greater suffering,

86
00:05:33,350 --> 00:05:34,640
 which is quite easy to

87
00:05:34,640 --> 00:05:37,800
 see if one is practicing the meditation, that one will see

88
00:05:37,800 --> 00:05:38,840
 that these acts are

89
00:05:38,840 --> 00:05:43,560
 unwholesome, unpleasant, not leading to positive

90
00:05:43,560 --> 00:05:45,320
 circumstances. There was a

91
00:05:45,320 --> 00:05:49,250
 question that was asked some time ago on Ask.SiriMongolaw.

92
00:05:49,250 --> 00:05:51,560
org about whether it

93
00:05:51,560 --> 00:05:55,600
 would be worth it to go to hell yourself, you know, for

94
00:05:55,600 --> 00:05:56,520
 people who believe

95
00:05:56,520 --> 00:05:59,860
 in, you know, that the mind can become so sully that it

96
00:05:59,860 --> 00:06:01,680
 goes to hell, in order to

97
00:06:01,680 --> 00:06:09,560
 save other beings, in order to prevent great violence or so

98
00:06:09,560 --> 00:06:10,400
 on. So if

99
00:06:10,400 --> 00:06:13,230
 you were to kill Osama bin Laden, Adolf Hitler, someone

100
00:06:13,230 --> 00:06:14,960
 like this, and therefore

101
00:06:14,960 --> 00:06:20,600
 were able to prevent great suffering, you know, the idea

102
00:06:20,600 --> 00:06:21,640
 was to save the world,

103
00:06:21,640 --> 00:06:28,600
 would you do it? And so the point here is that by creating

104
00:06:28,600 --> 00:06:30,120
 more violence, you

105
00:06:30,120 --> 00:06:33,580
 know, we're in a state where there is a great amount of

106
00:06:33,580 --> 00:06:34,240
 violence in the

107
00:06:34,240 --> 00:06:38,010
 world and there are beings in positions to create, to

108
00:06:38,010 --> 00:06:39,560
 inflict great violence on

109
00:06:39,560 --> 00:06:45,170
 other beings. And this is a result of our accumulated

110
00:06:45,170 --> 00:06:47,240
 tendencies to perform such

111
00:06:47,240 --> 00:06:51,840
 violent acts on each other. Now by increasing those, you're

112
00:06:51,840 --> 00:06:53,400
 not in any way

113
00:06:53,400 --> 00:06:57,130
 helping things, you know, the people who die, the people

114
00:06:57,130 --> 00:06:58,640
 who kill, they all have

115
00:06:58,640 --> 00:07:01,530
 these tendencies in them. What you do by, in this case,

116
00:07:01,530 --> 00:07:02,600
 going to hell or in any

117
00:07:02,600 --> 00:07:06,680
 case, creating unwholesome states in the mind is just

118
00:07:06,680 --> 00:07:08,480
 increasing the amount of

119
00:07:08,480 --> 00:07:11,290
 unwholesomeness in the universe. Since the beings that die,

120
00:07:11,290 --> 00:07:12,400
 they continue on, the

121
00:07:12,400 --> 00:07:16,940
 beings that kill, they continue on, and you yourself, by

122
00:07:16,940 --> 00:07:18,480
 adding to the pot, you

123
00:07:18,480 --> 00:07:24,070
 only increase the tendencies. So the important solution,

124
00:07:24,070 --> 00:07:26,640
 the solution, is to

125
00:07:26,640 --> 00:07:31,350
 decrease the amount of killing and to teach people to be

126
00:07:31,350 --> 00:07:33,240
 patient with, you

127
00:07:33,240 --> 00:07:38,800
 know, when confronted with revenge, when other beings would

128
00:07:38,800 --> 00:07:39,320
 inflict

129
00:07:39,320 --> 00:07:44,810
 violence on us to be forbearing and to put an end to it. If

130
00:07:44,810 --> 00:07:45,920
 this is, you know, if

131
00:07:45,920 --> 00:07:50,570
 this means my death, then let that be the end, to not react

132
00:07:50,570 --> 00:07:54,320
, to not reply and, you

133
00:07:54,320 --> 00:07:58,400
 know, create this cycle of revenge which actually spans lif

134
00:07:58,400 --> 00:07:59,640
etimes. A life is

135
00:07:59,640 --> 00:08:04,300
 nothing in the face of our minds, and our minds continue on

136
00:08:04,300 --> 00:08:05,360
 and carry these

137
00:08:05,360 --> 00:08:09,120
 dates with them. So this is sort of the backdrop, the

138
00:08:09,120 --> 00:08:10,200
 theory behind what I'm

139
00:08:10,200 --> 00:08:16,240
 going to talk about here, and the basic answer that no, it

140
00:08:16,240 --> 00:08:17,400
's not right.

141
00:08:17,400 --> 00:08:23,650
 And it could never be a good thing to get rid of rats, you

142
00:08:23,650 --> 00:08:24,560
 know, to kill rats

143
00:08:24,560 --> 00:08:30,260
 that are infesting your house, or even to kill parasites,

144
00:08:30,260 --> 00:08:31,920
 or to kill those, you know,

145
00:08:31,920 --> 00:08:35,960
 dangerous animals, a snake, or someone that's an animal, or

146
00:08:35,960 --> 00:08:37,200
 an intruder who is

147
00:08:37,200 --> 00:08:42,720
 threatening your family, and so on. And this is because of

148
00:08:42,720 --> 00:08:43,840
 the difference

149
00:08:43,840 --> 00:08:47,070
 between physical health and mental health, and if it means

150
00:08:47,070 --> 00:08:48,600
 that we have to, as a

151
00:08:48,600 --> 00:08:51,780
 result, suffer physically, and not just our bodies, but our

152
00:08:51,780 --> 00:08:52,320
 physical

153
00:08:52,320 --> 00:09:03,800
 surrounding might be in imperfect, suboptimal, but that our

154
00:09:03,800 --> 00:09:05,520
 minds should

155
00:09:05,520 --> 00:09:10,720
 remain healthy is far more important. And if our minds can

156
00:09:10,720 --> 00:09:11,520
 stay healthy

157
00:09:11,520 --> 00:09:14,880
 and can stay pure, then it really doesn't matter where we

158
00:09:14,880 --> 00:09:17,240
 are. And so

159
00:09:17,240 --> 00:09:21,180
 this is the first thing that we have to get through, and we

160
00:09:21,180 --> 00:09:21,760
 have to

161
00:09:21,760 --> 00:09:25,370
 understand. So then the question is, well then, what do you

162
00:09:25,370 --> 00:09:31,480
 do if you are, if it is

163
00:09:31,480 --> 00:09:34,950
 not in your best interest to perform acts of violence, well

164
00:09:34,950 --> 00:09:35,680
 then what do you do

165
00:09:35,680 --> 00:09:39,450
 when confronted with this situation? You hear it's not just

166
00:09:39,450 --> 00:09:40,600
 our well-being, we have

167
00:09:40,600 --> 00:09:45,990
 a conflict with our landlord, for example. And so, and I've

168
00:09:45,990 --> 00:09:47,500
 gotten several

169
00:09:47,500 --> 00:09:52,400
 questions in regards to this, you know, even people asking

170
00:09:52,400 --> 00:09:52,780
 about if you're

171
00:09:52,780 --> 00:09:56,730
 confronted by a violent person, what would you do? So there

172
00:09:56,730 --> 00:09:58,800
 are, I think, three

173
00:09:58,800 --> 00:10:01,030
 methods that are in line with the Buddhist situation. There

174
00:10:01,030 --> 00:10:01,640
's actually, you

175
00:10:01,640 --> 00:10:06,050
 could break it up any number of ways, but briefly, I think

176
00:10:06,050 --> 00:10:06,280
 there are

177
00:10:06,280 --> 00:10:12,580
 three suitably Buddhist responses to this sort of situation

178
00:10:12,580 --> 00:10:13,880
, whether it be

179
00:10:13,880 --> 00:10:19,090
 pests, whether it be, you know, criminals or, you know,

180
00:10:19,090 --> 00:10:22,200
 murderers or however. The first

181
00:10:22,200 --> 00:10:26,840
 is to avoid the situation. Now the Buddha did condone

182
00:10:26,840 --> 00:10:29,160
 avoiding those situations

183
00:10:29,160 --> 00:10:31,690
 that would obviously get in the way of your meditation

184
00:10:31,690 --> 00:10:32,600
 practice and your

185
00:10:32,600 --> 00:10:36,480
 mental development. And the examples he used were of

186
00:10:36,480 --> 00:10:39,440
 dangerous, you know,

187
00:10:39,440 --> 00:10:46,040
 a charging elephant. And if an elephant's charging at you,

188
00:10:46,040 --> 00:10:47,240
 you avoid it. You don't

189
00:10:47,240 --> 00:10:50,960
 stand there and seeing, seeing, you don't have to, you can

190
00:10:50,960 --> 00:10:52,160
 move to the side or so

191
00:10:52,160 --> 00:10:56,290
 on. But I think this extends to a lot of things, for

192
00:10:56,290 --> 00:10:58,000
 instance, avoiding situations,

193
00:10:58,000 --> 00:11:02,140
 avoiding, in the case of criminals or murderers, you know,

194
00:11:02,140 --> 00:11:03,280
 avoiding those areas

195
00:11:03,280 --> 00:11:08,760
 that where you're likely to be confronted with those, those

196
00:11:08,760 --> 00:11:12,560
 sorts of people. Now in

197
00:11:12,560 --> 00:11:18,850
 the case of rat infestations, one, this is probably not a

198
00:11:18,850 --> 00:11:19,920
 useful solution, but

199
00:11:19,920 --> 00:11:23,310
 it is one solution that we should all keep in mind is to

200
00:11:23,310 --> 00:11:24,200
 avoid those

201
00:11:24,200 --> 00:11:27,780
 situations where, A, you have a landlord or B, you're

202
00:11:27,780 --> 00:11:29,280
 living in a house that, that

203
00:11:29,280 --> 00:11:33,810
 is susceptible to these sorts of things. So I mean an ideal

204
00:11:33,810 --> 00:11:35,360
 form of this would be

205
00:11:35,360 --> 00:11:39,120
 to leave the home and live under a tree or live in a cave,

206
00:11:39,120 --> 00:11:40,720
 live in the forest

207
00:11:40,720 --> 00:11:44,690
 where you don't have to deal with these situations. Because

208
00:11:44,690 --> 00:11:45,880
 obviously living in

209
00:11:45,880 --> 00:11:49,420
 the household life, it's much more complicated. And you

210
00:11:49,420 --> 00:11:51,040
 know, the situation

211
00:11:51,040 --> 00:11:54,540
 with rats is one that comes up common. Another one that

212
00:11:54,540 --> 00:11:55,840
 people talk about is

213
00:11:55,840 --> 00:11:58,240
 lice. Well if you had lice, what would you do? Would you

214
00:11:58,240 --> 00:11:59,200
 not want to kill them?

215
00:11:59,200 --> 00:12:04,150
 And so one means of overcoming this is to shave your head.

216
00:12:04,150 --> 00:12:05,360
 As I've heard that

217
00:12:05,360 --> 00:12:07,670
 actually, I'm not sure if this is true or not, but I've

218
00:12:07,670 --> 00:12:08,600
 heard that that will

219
00:12:08,600 --> 00:12:11,450
 actually prevent the lice from breeding. If you shave your

220
00:12:11,450 --> 00:12:13,880
 head, they won't be

221
00:12:13,880 --> 00:12:19,000
 able to, to say that they stay at the roots of the hair and

222
00:12:19,000 --> 00:12:21,920
 so on. So these are

223
00:12:21,920 --> 00:12:24,630
 just wild examples. I mean you could think of many

224
00:12:24,630 --> 00:12:26,080
 different examples, but

225
00:12:26,080 --> 00:12:31,280
 with in the case of rats, in the case of violence and so on

226
00:12:31,280 --> 00:12:32,400
, we should

227
00:12:32,400 --> 00:12:36,790
 be careful to avoid those kinds of situations that would

228
00:12:36,790 --> 00:12:38,160
 only give rise to

229
00:12:38,160 --> 00:12:44,690
 unwholesomeness. You know, if you're a, if you're not a

230
00:12:44,690 --> 00:12:46,040
 strong person, if you're a

231
00:12:46,040 --> 00:12:50,240
 young attractive woman for example, you might want to avoid

232
00:12:50,240 --> 00:12:51,280
 dark streets at

233
00:12:51,280 --> 00:12:54,480
 night or so on. I mean not even a woman. If you're a person

234
00:12:54,480 --> 00:12:55,920
 who doesn't look fierce

235
00:12:55,920 --> 00:12:59,090
 or doesn't, you know, like me or so on, you might want to

236
00:12:59,090 --> 00:13:00,600
 avoid those situations

237
00:13:00,600 --> 00:13:04,550
 where you might get into a conflict. You know, as a monk I

238
00:13:04,550 --> 00:13:06,640
 sometimes try to avoid

239
00:13:06,640 --> 00:13:11,920
 those areas where I might be confronted with prejudice,

240
00:13:11,920 --> 00:13:13,880
 bigotry and so on. I was, I

241
00:13:13,880 --> 00:13:17,950
 was arrested and put in jail a couple of years ago simply

242
00:13:17,950 --> 00:13:19,440
 because I look

243
00:13:19,440 --> 00:13:25,410
 different and, and some people do because of their fear

244
00:13:25,410 --> 00:13:28,280
 they either made some

245
00:13:28,280 --> 00:13:31,410
 assumptions or else they were specifically trying to get

246
00:13:31,410 --> 00:13:32,080
 rid of me. I

247
00:13:32,080 --> 00:13:35,530
 don't know, but I was arrested and put in jail and it was a

248
00:13:35,530 --> 00:13:37,480
 big deal. So would have

249
00:13:37,480 --> 00:13:40,370
 probably been in my best interest to just avoid the whole

250
00:13:40,370 --> 00:13:41,560
 issue and the whole

251
00:13:41,560 --> 00:13:48,400
 situation and stay in a place that was more, more accepting

252
00:13:48,400 --> 00:13:50,480
. So this is the, the

253
00:13:50,480 --> 00:13:53,430
 first answer I think and it will work in a variety of

254
00:13:53,430 --> 00:13:54,800
 situations and it can be

255
00:13:54,800 --> 00:13:58,410
 employed in a variety of ways. Just avoid the problem. Find

256
00:13:58,410 --> 00:13:59,480
 some ways so that you,

257
00:13:59,480 --> 00:14:02,840
 you don't have to be confronted with the situation. You don

258
00:14:02,840 --> 00:14:03,200
't have to be

259
00:14:03,200 --> 00:14:06,290
 confronted with these difficult issues or you know, try to

260
00:14:06,290 --> 00:14:07,840
 restructure your life so

261
00:14:07,840 --> 00:14:13,000
 in a way so that you don't meet with this situation. The

262
00:14:13,000 --> 00:14:16,240
 second way is to find an

263
00:14:16,240 --> 00:14:20,710
 alternative, an alternative to violence and there are many

264
00:14:20,710 --> 00:14:22,480
 alternatives and I

265
00:14:22,480 --> 00:14:26,080
 think that this is a point that I always try to raise with

266
00:14:26,080 --> 00:14:28,120
 people is that killing

267
00:14:28,120 --> 00:14:33,080
 and violence are not the only way out of a situation.

268
00:14:33,080 --> 00:14:35,080
 Whether it be with pasts,

269
00:14:35,080 --> 00:14:38,900
 whether it be with murderers or criminals or so on, often

270
00:14:38,900 --> 00:14:39,640
 you know if

271
00:14:39,640 --> 00:14:41,940
 someone wants your wallet maybe you just give them your

272
00:14:41,940 --> 00:14:43,480
 wallet and that's a way of

273
00:14:43,480 --> 00:14:48,390
 dealing with the situation mindfully, you know, giving up,

274
00:14:48,390 --> 00:14:50,600
 letting go in this sense.

275
00:14:50,600 --> 00:14:53,760
 Dealing with the situation, maybe talking to the person,

276
00:14:53,760 --> 00:14:54,920
 sometimes that can work.

277
00:14:54,920 --> 00:14:59,410
 With criminals and murderers it's probably not likely to.

278
00:14:59,410 --> 00:15:00,920
 With pests, this

279
00:15:00,920 --> 00:15:07,840
 is really something that we spend far too little time on. I

280
00:15:07,840 --> 00:15:08,440
 was trying to

281
00:15:08,440 --> 00:15:15,140
 learn about how to get rid of termites and the idea came up

282
00:15:15,140 --> 00:15:15,720
, well when

283
00:15:15,720 --> 00:15:17,820
 there are termites you have to kill them. So I researched

284
00:15:17,820 --> 00:15:18,600
 it and I was trying to

285
00:15:18,600 --> 00:15:22,850
 find some information on ways of getting rid of termites

286
00:15:22,850 --> 00:15:24,360
 that doesn't

287
00:15:24,360 --> 00:15:29,280
 require you to kill them and no research has been done on

288
00:15:29,280 --> 00:15:29,840
 this as far

289
00:15:29,840 --> 00:15:32,880
 as I'm aware, maybe, but there's nothing on the internet

290
00:15:32,880 --> 00:15:35,080
 anyway. And

291
00:15:35,080 --> 00:15:37,460
 I'm betting that there's very little research being done on

292
00:15:37,460 --> 00:15:38,080
 this because

293
00:15:38,080 --> 00:15:41,500
 people don't think. They think, well when you have termites

294
00:15:41,500 --> 00:15:42,360
 you just kill them,

295
00:15:42,360 --> 00:15:45,570
 you find some way and there are many ways. They have many

296
00:15:45,570 --> 00:15:49,120
 neat and ingenious

297
00:15:49,120 --> 00:15:52,680
 ways of killing termites but there's no one has put any of

298
00:15:52,680 --> 00:15:53,120
 their

299
00:15:53,120 --> 00:15:56,910
 ingenuity into finding other solutions because no one's

300
00:15:56,910 --> 00:15:58,320
 ever thought of the

301
00:15:58,320 --> 00:16:00,840
 importance of it. When you can kill them why would you find

302
00:16:00,840 --> 00:16:01,160
 another

303
00:16:01,160 --> 00:16:05,540
 solution? I think this is really tragic because in many

304
00:16:05,540 --> 00:16:07,360
 cases the solution does

305
00:16:07,360 --> 00:16:11,560
 exist and it's not very difficult. The solution with term

306
00:16:11,560 --> 00:16:12,160
ites might be

307
00:16:12,160 --> 00:16:14,640
 simply finding a compound that they don't like. Now who

308
00:16:14,640 --> 00:16:16,200
 would have thought to

309
00:16:16,200 --> 00:16:18,650
 try to find a compound that termites don't like when you

310
00:16:18,650 --> 00:16:19,520
 can find a compound

311
00:16:19,520 --> 00:16:22,660
 that kills them, some chemical compound that drives them

312
00:16:22,660 --> 00:16:24,480
 away? I don't know of

313
00:16:24,480 --> 00:16:28,570
 any but I haven't had that much experience with termites. I

314
00:16:28,570 --> 00:16:30,320
 have had

315
00:16:30,320 --> 00:16:32,940
 experience with other animals and with ants for example

316
00:16:32,940 --> 00:16:33,880
 people will put out

317
00:16:33,880 --> 00:16:40,090
 poison to kill the ants. Now baby powder or talcum powder

318
00:16:40,090 --> 00:16:42,040
 works not as

319
00:16:42,040 --> 00:16:45,280
 well as poison obviously but from a Buddhist point of view

320
00:16:45,280 --> 00:16:45,720
 it works much

321
00:16:45,720 --> 00:16:49,490
 better than poison. You sweep the ants away and then you

322
00:16:49,490 --> 00:16:51,040
 put talcum powder down

323
00:16:51,040 --> 00:16:54,260
 in their path and I'm not sure if it's the scented talcum

324
00:16:54,260 --> 00:16:55,520
 powder or if normal

325
00:16:55,520 --> 00:16:59,500
 talcum powder will work as well but somehow it stops them,

326
00:16:59,500 --> 00:17:00,320
 the small ants

327
00:17:00,320 --> 00:17:03,430
 aren't able to cross it, even the big ants don't like it

328
00:17:03,430 --> 00:17:04,560
 because it removes

329
00:17:04,560 --> 00:17:08,630
 their scent trails and so they don't go across it. If you

330
00:17:08,630 --> 00:17:09,560
 rub it into a

331
00:17:09,560 --> 00:17:12,900
 plate, rub it across their trail it'll remove their trail

332
00:17:12,900 --> 00:17:13,780
 and they won't be

333
00:17:13,780 --> 00:17:15,780
 able to find their way and they won't come back in that

334
00:17:15,780 --> 00:17:17,360
 direction. I use

335
00:17:17,360 --> 00:17:22,790
 this a lot with ants, to a great success. If ants are

336
00:17:22,790 --> 00:17:24,840
 coming along telephone wires

337
00:17:24,840 --> 00:17:28,360
 or closed lines or so on you can put butter on the closed

338
00:17:28,360 --> 00:17:29,120
 line on the

339
00:17:29,120 --> 00:17:33,240
 telephone wire and ants won't cross butter. Not all ants

340
00:17:33,240 --> 00:17:34,600
 anyway, I do

341
00:17:34,600 --> 00:17:37,240
 believe there are some varieties of ants that eat the

342
00:17:37,240 --> 00:17:39,280
 butter but as an example

343
00:17:39,280 --> 00:17:43,400
 there are ways around this with rats. The example I gave on

344
00:17:43,400 --> 00:17:46,120
 the forum was to use

345
00:17:46,120 --> 00:17:49,890
 humane rat traps, humane mice traps, these exist, it's a

346
00:17:49,890 --> 00:17:52,280
 box and you can buy them

347
00:17:52,280 --> 00:17:55,710
 and there's bait inside, the rat goes in, the door closes

348
00:17:55,710 --> 00:17:57,140
 or the door is made in

349
00:17:57,140 --> 00:17:59,500
 such a way that they can't get out again and then you take

350
00:17:59,500 --> 00:18:00,560
 it away to the forest,

351
00:18:00,560 --> 00:18:05,740
 find someplace far far away and release the rat, end of

352
00:18:05,740 --> 00:18:08,440
 story. I do this with

353
00:18:08,440 --> 00:18:12,320
 with mosquitoes as well when you have mosquitoes in your

354
00:18:12,320 --> 00:18:12,920
 home, in your

355
00:18:12,920 --> 00:18:18,190
 tent, in wherever you are, you take a cup, you close, you

356
00:18:18,190 --> 00:18:19,000
 put the mosquito in the

357
00:18:19,000 --> 00:18:21,400
 cup, you take it outside and you do this again and again if

358
00:18:21,400 --> 00:18:22,400
 you have a closed off

359
00:18:22,400 --> 00:18:25,540
 space where the mosquitoes don't come. So finding

360
00:18:25,540 --> 00:18:28,480
 intelligent ways to deal with

361
00:18:28,480 --> 00:18:32,970
 pests I think is incredibly important. In fact part of me

362
00:18:32,970 --> 00:18:34,480
 would like to set up a

363
00:18:34,480 --> 00:18:37,460
 wiki page, probably I will end up doing this, a wiki page

364
00:18:37,460 --> 00:18:38,560
 for just this sort of

365
00:18:38,560 --> 00:18:43,890
 thing where people can post their good ideas for how to

366
00:18:43,890 --> 00:18:44,600
 deal with

367
00:18:44,600 --> 00:18:47,760
 difficult situations in a Buddhist way. So not just pests

368
00:18:47,760 --> 00:18:48,840
 but also how to deal

369
00:18:48,840 --> 00:18:54,220
 with questions that you get or how to deal with this sort

370
00:18:54,220 --> 00:18:55,200
 of person or that

371
00:18:55,200 --> 00:18:58,720
 sort of person, how to deal with this situation, how to

372
00:18:58,720 --> 00:18:59,840
 deal with so many of

373
00:18:59,840 --> 00:19:02,580
 the issues that we're confronted with that all people and

374
00:19:02,580 --> 00:19:03,560
 all religions are

375
00:19:03,560 --> 00:19:08,000
 confronted with and have to find some way to make it jive

376
00:19:08,000 --> 00:19:08,720
 with their

377
00:19:08,720 --> 00:19:15,160
 understanding of ethics and practice. So this is the second

378
00:19:15,160 --> 00:19:16,840
 method is to deal

379
00:19:16,840 --> 00:19:20,650
 with it, find a way an alternative means of dealing with

380
00:19:20,650 --> 00:19:21,920
 the situation that

381
00:19:21,920 --> 00:19:27,080
 doesn't require violence. And one note I'd make on that is

382
00:19:27,080 --> 00:19:28,040
 that sometimes in

383
00:19:28,040 --> 00:19:33,060
 self-defense it is even according to the Buddha proper to

384
00:19:33,060 --> 00:19:34,760
 resort to limited

385
00:19:34,760 --> 00:19:37,930
 amounts of violence. So if someone's attacking you, you

386
00:19:37,930 --> 00:19:38,840
 know to push them out

387
00:19:38,840 --> 00:19:42,270
 of the way or to hit them enough so that you can you can

388
00:19:42,270 --> 00:19:44,080
 run away, even monks are

389
00:19:44,080 --> 00:19:46,390
 allowed to do this. We're not allowed to hit someone but we

390
00:19:46,390 --> 00:19:46,960
're allowed to hit

391
00:19:46,960 --> 00:19:50,590
 someone in self-defense in order to get away. So if it

392
00:19:50,590 --> 00:19:53,120
 means that you have to

393
00:19:53,120 --> 00:19:57,710
 you have to perform some limited act of violence in order

394
00:19:57,710 --> 00:19:59,320
 to escape or in order

395
00:19:59,320 --> 00:20:03,540
 to wake up the the attacker or so on or to find a way to

396
00:20:03,540 --> 00:20:05,480
 change the situation.

397
00:20:05,480 --> 00:20:09,640
 You know even putting the attacker into an armlock or

398
00:20:09,640 --> 00:20:10,920
 whatever if you know

399
00:20:10,920 --> 00:20:14,910
 karate or you know kung fu or some martial arts to be able

400
00:20:14,910 --> 00:20:16,360
 to change the

401
00:20:16,360 --> 00:20:22,200
 situation and avoid again avoiding the greater act of

402
00:20:22,200 --> 00:20:23,880
 violence then to a

403
00:20:23,880 --> 00:20:29,080
 limited extent because it's not something that is designed

404
00:20:29,080 --> 00:20:31,200
 to

405
00:20:31,200 --> 00:20:35,540
 it's not something that is designed to harm it's in self-

406
00:20:35,540 --> 00:20:36,480
defense and it's

407
00:20:36,480 --> 00:20:42,680
 designed to allow you to escape the situation. Then that is

408
00:20:42,680 --> 00:20:43,600
 permitted

409
00:20:43,600 --> 00:20:48,160
 provided that it doesn't inflict fatal harm on the other

410
00:20:48,160 --> 00:20:48,680
 person

411
00:20:48,680 --> 00:20:53,200
 or on the other being. So there might be a case where

412
00:20:53,200 --> 00:20:55,440
 limited amounts of

413
00:20:55,440 --> 00:21:00,880
 violence done not in the intention of hurting or killing or

414
00:21:00,880 --> 00:21:01,880
 seriously

415
00:21:01,880 --> 00:21:05,310
 harming the other person but simply in the interest of self

416
00:21:05,310 --> 00:21:06,520
-defense and for the

417
00:21:06,520 --> 00:21:10,650
 purposes of escaping might be or are considered to be

418
00:21:10,650 --> 00:21:12,760
 allowed. This is a way

419
00:21:12,760 --> 00:21:18,150
 of dealing with the issue. The third answer which I think

420
00:21:18,150 --> 00:21:19,240
 we should also keep

421
00:21:19,240 --> 00:21:21,340
 in mind and this goes back to what I was saying in the

422
00:21:21,340 --> 00:21:22,640
 beginning is to accept and

423
00:21:22,640 --> 00:21:27,090
 to let go of the situation and I think the issue of rats

424
00:21:27,090 --> 00:21:29,880
 and the landlord is an

425
00:21:29,880 --> 00:21:35,330
 interesting example of this because sometimes we have to

426
00:21:35,330 --> 00:21:36,600
 think outside the

427
00:21:36,600 --> 00:21:41,290
 box and we have to look outside of our situation and not

428
00:21:41,290 --> 00:21:43,040
 get confined to a or

429
00:21:43,040 --> 00:21:47,130
 b mentality where if I don't do this that is going to

430
00:21:47,130 --> 00:21:49,520
 happen because often

431
00:21:49,520 --> 00:21:52,920
 when we let things go when we're mindful when we're aware

432
00:21:52,920 --> 00:21:54,040
 of the situation

433
00:21:54,040 --> 00:21:58,880
 there's a C alternative arises almost magically and A and B

434
00:21:58,880 --> 00:21:59,840
 disappear

435
00:21:59,840 --> 00:22:03,980
 completely. So it may be in the case where we're confronted

436
00:22:03,980 --> 00:22:05,600
 by an assault

437
00:22:05,600 --> 00:22:12,680
 someone assaulting us sometimes when we're mindful it can

438
00:22:12,680 --> 00:22:13,360
 happen that when

439
00:22:13,360 --> 00:22:18,550
 we're mindful when we're aware and when we're meditating on

440
00:22:18,550 --> 00:22:20,200
 the situation when

441
00:22:20,200 --> 00:22:23,990
 we're taking it as a Buddhist practice you know people are

442
00:22:23,990 --> 00:22:24,880
 saying what would

443
00:22:24,880 --> 00:22:27,480
 you just say pain when someone's hitting you I think yes

444
00:22:27,480 --> 00:22:29,200
 that's a good

445
00:22:29,200 --> 00:22:33,680
 it's a perfectly reasonable response to the situation and

446
00:22:33,680 --> 00:22:36,720
 can often have magical

447
00:22:36,720 --> 00:22:42,320
 results where people have found that suddenly the whole

448
00:22:42,320 --> 00:22:43,000
 situation

449
00:22:43,000 --> 00:22:47,770
 changed and they weren't they were no longer the victim now

450
00:22:47,770 --> 00:22:48,320
 they were in

451
00:22:48,320 --> 00:22:52,120
 control and the other person was forced really due to the

452
00:22:52,120 --> 00:22:53,400
 power of presence

453
00:22:53,400 --> 00:22:56,220
 because the mind is such a powerful thing much more

454
00:22:56,220 --> 00:22:57,560
 powerful than the body

455
00:22:57,560 --> 00:23:02,860
 and simply the presence of someone who is mindful is is

456
00:23:02,860 --> 00:23:04,400
 really the greatest

457
00:23:04,400 --> 00:23:08,920
 weapon there is and it's something that can truly overcome

458
00:23:08,920 --> 00:23:10,400
 these situations so

459
00:23:10,400 --> 00:23:14,360
 in the case of the rats and the landlord it might be that

460
00:23:14,360 --> 00:23:15,480
 simply by

461
00:23:15,480 --> 00:23:19,360
 being mindful and watching the situation unfold and

462
00:23:19,360 --> 00:23:20,600
 allowing the

463
00:23:20,600 --> 00:23:22,930
 consequences if the the landlord says they're going to

464
00:23:22,930 --> 00:23:24,680
 throw you out and you

465
00:23:24,680 --> 00:23:27,460
 know you simply say to the landlord I'm Buddhist and I don

466
00:23:27,460 --> 00:23:29,040
't kill and and so you

467
00:23:29,040 --> 00:23:31,280
 know do what you will and if it means that we have to do

468
00:23:31,280 --> 00:23:32,920
 that we have to come

469
00:23:32,920 --> 00:23:37,560
 to some sort of conflict and so be it letting things go

470
00:23:37,560 --> 00:23:39,720
 holding on to what is

471
00:23:39,720 --> 00:23:42,330
 really and truly important which is your mental health and

472
00:23:42,330 --> 00:23:43,480
 mental well-being and

473
00:23:43,480 --> 00:23:47,700
 the truth because it's it's being untrue to yourself to

474
00:23:47,700 --> 00:23:49,440
 perform violence on other

475
00:23:49,440 --> 00:23:52,820
 beings when you yourself don't want to to feel such

476
00:23:52,820 --> 00:23:54,800
 violence when you yourself

477
00:23:54,800 --> 00:23:59,520
 don't or wish for that not to happen to you so if someone's

478
00:23:59,520 --> 00:24:00,200
 going to kill you

479
00:24:00,200 --> 00:24:04,040
 well the only reason you'd kill them first is because you

480
00:24:04,040 --> 00:24:05,240
 yourself don't want

481
00:24:05,240 --> 00:24:07,800
 to die and so you're being just as hypocritical as the

482
00:24:07,800 --> 00:24:09,480
 other person you're

483
00:24:09,480 --> 00:24:12,990
 inflicting something on other beings that you yourself

484
00:24:12,990 --> 00:24:14,080
 would not wish for and

485
00:24:14,080 --> 00:24:17,720
 so it's something that is is against harmony and is against

486
00:24:17,720 --> 00:24:18,560
 the truth it's

487
00:24:18,560 --> 00:24:22,600
 against reality and it's going to create as I said

488
00:24:22,600 --> 00:24:23,640
 corruption in the mind

489
00:24:23,640 --> 00:24:28,330
 something that we can do much better without in this and we

490
00:24:28,330 --> 00:24:29,240
 can do with much

491
00:24:29,240 --> 00:24:33,060
 less than there is already in the world so something that

492
00:24:33,060 --> 00:24:35,040
 we should strive to to

493
00:24:35,040 --> 00:24:38,730
 do away with rather than increase so simply by being

494
00:24:38,730 --> 00:24:40,440
 mindful by being aware

495
00:24:40,440 --> 00:24:43,390
 of the situation whether it be a violent situation whether

496
00:24:43,390 --> 00:24:44,240
 it be a difficult

497
00:24:44,240 --> 00:24:49,720
 situation whatever the you know whether it be some lice or

498
00:24:49,720 --> 00:24:50,320
 something you know

499
00:24:50,320 --> 00:24:54,150
 even just sticking with the itching and so the pain of

500
00:24:54,150 --> 00:24:56,360
 having lice it should be

501
00:24:56,360 --> 00:24:59,960
 a part not all of the answer but should be at least a part

502
00:24:59,960 --> 00:25:01,240
 of the answer of

503
00:25:01,240 --> 00:25:05,070
 course we can find other other solutions and something that

504
00:25:05,070 --> 00:25:06,920
 stops the lice from

505
00:25:06,920 --> 00:25:14,620
 from or repels the lice and so repels the the the assailant

506
00:25:14,620 --> 00:25:16,000
 but doesn't cause

507
00:25:16,000 --> 00:25:19,810
 more suffering than then is warranted or isn't of the

508
00:25:19,810 --> 00:25:22,000
 purpose of the purpose of

509
00:25:22,000 --> 00:25:29,160
 causing death or fatal or permanent damn physical damage or

510
00:25:29,160 --> 00:25:30,240
 suffering to the

511
00:25:30,240 --> 00:25:34,160
 other being so I hope this has been helpful I think this is

512
00:25:34,160 --> 00:25:34,720
 an important

513
00:25:34,720 --> 00:25:37,300
 subject and I'm glad to talk about it I've been meaning to

514
00:25:37,300 --> 00:25:38,040
 make a video on

515
00:25:38,040 --> 00:25:41,820
 this subject for a while so there you have it thanks for

516
00:25:41,820 --> 00:25:42,920
 tuning in and all the

