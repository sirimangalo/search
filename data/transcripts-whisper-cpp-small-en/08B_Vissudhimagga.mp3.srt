1
00:00:00,000 --> 00:00:08,260
 Now, suppose you have all five jhanas and also all alubavaj

2
00:00:08,260 --> 00:00:11,360
ara jhanas and also you can

3
00:00:11,360 --> 00:00:16,760
 experience what we call supernormal knowledge or abhinya.

4
00:00:16,760 --> 00:00:24,820
 Abhinya is a specially developed fifth jhana and abhinyas

5
00:00:24,820 --> 00:00:28,960
 are those that enable yogis to

6
00:00:28,960 --> 00:00:33,500
 see things far away and to hear sounds far away and so on

7
00:00:33,500 --> 00:00:36,920
 or to perform some miracles.

8
00:00:36,920 --> 00:00:48,600
 Now, suppose you want to hear the sounds far away.

9
00:00:48,600 --> 00:00:55,880
 Now, suppose you have all five jhanas and also all alubavaj

10
00:00:55,880 --> 00:00:59,560
ara jhanas and also you can

11
00:00:59,560 --> 00:01:04,960
 experience what we call supernormal knowledge or abhinya.

12
00:01:04,960 --> 00:01:12,970
 Abhinya is a specially developed fifth jhana and abhinyas

13
00:01:12,970 --> 00:01:17,040
 are those that enable yogis to

14
00:01:17,040 --> 00:01:22,010
 see things far away and to hear sounds far away and so on

15
00:01:22,010 --> 00:01:25,000
 or to perform some miracles.

16
00:01:25,000 --> 00:01:32,680
 Now, suppose you want to hear the sounds far away and it

17
00:01:32,680 --> 00:01:36,560
 depends on how large you extend

18
00:01:36,560 --> 00:01:40,560
 the kasimha sign.

19
00:01:40,560 --> 00:01:44,510
 If you extend the kasimha sign as big as San Francisco City

20
00:01:44,510 --> 00:01:46,520
, then you may hear sounds in

21
00:01:46,520 --> 00:01:51,000
 San Francisco City and not outside.

22
00:01:51,000 --> 00:01:55,690
 So the ten kasimhas among these forty meditation subjects

23
00:01:55,690 --> 00:01:58,360
 need be extended for it is within

24
00:01:58,360 --> 00:02:01,880
 just so much space as one is intent upon.

25
00:02:01,880 --> 00:02:06,650
 That means one covers, one extends over with the kasimha

26
00:02:06,650 --> 00:02:08,760
 that one can hear sounds with

27
00:02:08,760 --> 00:02:12,380
 the divine ear element, see visible objects with the divine

28
00:02:12,380 --> 00:02:13,880
 eye and know the minds of

29
00:02:13,880 --> 00:02:15,720
 other beings with the mind.

30
00:02:15,720 --> 00:02:23,870
 So before you experience the supernormal knowledge, you

31
00:02:23,870 --> 00:02:29,280
 have to practice, you have to extend the

32
00:02:29,280 --> 00:02:34,240
 sign, the kantabhat sign.

33
00:02:34,240 --> 00:02:40,780
 That means you are defining the area within which your

34
00:02:40,780 --> 00:02:45,680
 supernormal knowledge will apply.

35
00:02:45,680 --> 00:02:52,040
 So they need be extended.

36
00:02:52,040 --> 00:02:54,800
 Mindfulness occupied with the body and the ten kinds of

37
00:02:54,800 --> 00:02:56,440
 foulness need not be extended.

38
00:02:56,440 --> 00:02:58,640
 So these subjects need not be extended.

39
00:02:58,640 --> 00:02:59,640
 Why?

40
00:02:59,640 --> 00:03:01,000
 Because they have a definite location.

41
00:03:01,000 --> 00:03:07,650
 You have to define these things and so you cannot extend

42
00:03:07,650 --> 00:03:10,920
 them and if you extend them

43
00:03:10,920 --> 00:03:14,280
 there is no benefit because there is no benefit in it.

44
00:03:14,280 --> 00:03:17,470
 The definiteness of their location will become clear in

45
00:03:17,470 --> 00:03:19,840
 explaining the method of development.

46
00:03:19,840 --> 00:03:24,610
 Now when the other comes to explaining how to practice

47
00:03:24,610 --> 00:03:27,320
 meditation on dead bodies and

48
00:03:27,320 --> 00:03:31,840
 so on and it will become clear.

49
00:03:31,840 --> 00:03:34,920
 If the letters are extended, it is only a quantity of

50
00:03:34,920 --> 00:03:37,280
 corpses that is extended with regard to

51
00:03:37,280 --> 00:03:39,720
 foulness meditation.

52
00:03:39,720 --> 00:03:44,890
 You will extend the corpses, I mean in your mind, corpses

53
00:03:44,890 --> 00:03:47,320
 but there is no benefit.

54
00:03:47,320 --> 00:03:49,880
 And it is said in answer to the question of Soba Gita, "The

55
00:03:49,880 --> 00:03:51,680
 perception of visible forms

56
00:03:51,680 --> 00:03:52,680
 is quite clear."

57
00:03:52,680 --> 00:03:53,680
 Blessed one.

58
00:03:53,680 --> 00:03:56,680
 "The perception of bones is not clear.

59
00:03:56,680 --> 00:03:59,190
 For here the perception of visible form is called quite

60
00:03:59,190 --> 00:04:00,640
 clear in the sense of extension

61
00:04:00,640 --> 00:04:02,120
 of the sign.

62
00:04:02,120 --> 00:04:05,150
 When the perception of bones is called not quite clear,

63
00:04:05,150 --> 00:04:06,800
 perception of bones has to do

64
00:04:06,800 --> 00:04:10,880
 with foulness meditation.

65
00:04:10,880 --> 00:04:13,400
 In the sense it is non-extension.

66
00:04:13,400 --> 00:04:16,900
 For the words, "I was intent upon this whole earth with the

67
00:04:16,900 --> 00:04:18,640
 perception of a skeleton that

68
00:04:18,640 --> 00:04:21,480
 was uttered by one elder."

69
00:04:21,480 --> 00:04:25,740
 Now they are said in the manner of appearance to one who

70
00:04:25,740 --> 00:04:28,520
 has acquired that perception.

71
00:04:28,520 --> 00:04:33,980
 That means one who has acquired that perception before and

72
00:04:33,980 --> 00:04:37,320
 now he extends this perception.

73
00:04:37,320 --> 00:04:40,200
 So it is alright.

74
00:04:40,200 --> 00:04:45,760
 Because he is not practicing to get that perception.

75
00:04:45,760 --> 00:04:50,600
 He has already got that perception and so he extends it.

76
00:04:50,600 --> 00:04:53,830
 For just as in Dhammasoka's time the Karabhika but uttered

77
00:04:53,830 --> 00:04:55,520
 his sweet song when it saw its

78
00:04:55,520 --> 00:04:59,020
 own reflection in the looking glass walls all round and

79
00:04:59,020 --> 00:05:00,960
 perceived Karabhika in every

80
00:05:00,960 --> 00:05:04,400
 direction so the elder, Singala Peeta thought, when he saw

81
00:05:04,400 --> 00:05:06,560
 the sign appearing in all directions

82
00:05:06,560 --> 00:05:09,670
 through his acquisition of perception of a skeleton that

83
00:05:09,670 --> 00:05:11,120
 the whole earth was covered

84
00:05:11,120 --> 00:05:12,920
 with bones."

85
00:05:12,920 --> 00:05:16,520
 And then there is the footnote on Karabhika's bird.

86
00:05:16,520 --> 00:05:22,240
 It's interesting but it's difficult to believe.

87
00:05:22,240 --> 00:05:27,800
 It's a kind of bird and it is said that its sound is very

88
00:05:27,800 --> 00:05:30,200
 sweet and so the queen asked

89
00:05:30,200 --> 00:05:35,990
 the community whose voice or whose sound is sweet as the

90
00:05:35,990 --> 00:05:39,200
 community said the Karabhika

91
00:05:39,200 --> 00:05:45,530
 but so the queen wanted to listen to the sound of the Karab

92
00:05:45,530 --> 00:05:49,640
hika but and so she asked her

93
00:05:49,640 --> 00:05:53,920
 king, King Asoka to bring a Karabhika bird.

94
00:05:53,920 --> 00:05:58,320
 So what Asoka did was just send a cage.

95
00:05:58,320 --> 00:06:02,960
 The cage flew through the air and they lay down near the

96
00:06:02,960 --> 00:06:05,320
 bird and the bird got in the

97
00:06:05,320 --> 00:06:10,800
 cage and the cage flew back to the city and so on.

98
00:06:10,800 --> 00:06:14,320
 But after reaching the city he would not utter his sound

99
00:06:14,320 --> 00:06:16,240
 because he was depressed.

100
00:06:16,240 --> 00:06:23,390
 So the king asked why the bird did not make any sound so

101
00:06:23,390 --> 00:06:29,120
 they said because he was lonely.

102
00:06:29,120 --> 00:06:32,760
 If the bird had companions then he would make noise.

103
00:06:32,760 --> 00:06:37,260
 So the king put the mirrors around him and so the bird saw

104
00:06:37,260 --> 00:06:39,640
 his images in the mirror and

105
00:06:39,640 --> 00:06:44,900
 he thought there were other birds so he was happy and so he

106
00:06:44,900 --> 00:06:46,440
 made a sound.

107
00:06:46,440 --> 00:06:51,700
 And the queen when she heard the sound she was so pleased

108
00:06:51,700 --> 00:06:54,040
 and she was very happy and

109
00:06:54,040 --> 00:06:59,940
 dwelling on that happiness or practicing meditation on that

110
00:06:59,940 --> 00:07:03,600
 happiness she practiced with Vasana

111
00:07:03,600 --> 00:07:08,520
 and she became a stream winner established in the fruition

112
00:07:08,520 --> 00:07:10,800
 of stream entry that means

113
00:07:10,800 --> 00:07:15,760
 she became a swordavanna.

114
00:07:15,760 --> 00:07:19,000
 This is just an example.

115
00:07:19,000 --> 00:07:25,250
 Just as the Karabhika bird saw many other birds in the

116
00:07:25,250 --> 00:07:29,160
 mirror and so make sound the elder

117
00:07:29,160 --> 00:07:32,120
 here when he saw the sign appearing in all directions

118
00:07:32,120 --> 00:07:33,960
 through his acquisition of the

119
00:07:33,960 --> 00:07:37,180
 perception of a skeleton he thought the whole earth was

120
00:07:37,180 --> 00:07:38,560
 covered with bones.

121
00:07:38,560 --> 00:07:42,200
 So it appeared to him as that.

122
00:07:42,200 --> 00:07:51,920
 It is not that he extended the casino sign.

123
00:07:51,920 --> 00:07:55,680
 And the next paragraph it is if that is so then is what is

124
00:07:55,680 --> 00:07:57,680
 called the measurelessness

125
00:07:57,680 --> 00:08:01,800
 of the object of Jhana produced on foulness contradicted.

126
00:08:01,800 --> 00:08:09,180
 Now the Jhana produced on foulness is mentioned as measure

127
00:08:09,180 --> 00:08:13,200
less or it is mentioned as with

128
00:08:13,200 --> 00:08:23,680
 measure so is that contradicted then the answer is no.

129
00:08:23,680 --> 00:08:30,330
 When a person looks at the small corpse then his object is

130
00:08:30,330 --> 00:08:33,520
 said to be with measure and

131
00:08:33,520 --> 00:08:38,010
 if he looks at a big corpse then his object is said to be

132
00:08:38,010 --> 00:08:40,800
 measureless although it is not

133
00:08:40,800 --> 00:08:45,960
 really measureless but measureless here means large so the

134
00:08:45,960 --> 00:08:51,120
 large object and small object.

135
00:08:51,120 --> 00:09:04,050
 And then paragraph 115 as regards the immaterial states as

136
00:09:04,050 --> 00:09:06,000
 objects.

137
00:09:06,000 --> 00:09:11,230
 It should read as regards objects of the immaterial states

138
00:09:11,230 --> 00:09:14,720
 not immaterial states as objects but

139
00:09:14,720 --> 00:09:20,840
 objects of the immaterial states.

140
00:09:20,840 --> 00:09:25,610
 This need not be extended since it is the mere removal of

141
00:09:25,610 --> 00:09:26,920
 the casino.

142
00:09:26,920 --> 00:09:31,520
 So with regard to the objects of Aruba Vajrayana.

143
00:09:31,520 --> 00:09:38,160
 Now the object of the first Aruba Vajrayana is what

144
00:09:38,160 --> 00:09:43,240
 infinite space so that cannot be extended

145
00:09:43,240 --> 00:09:46,180
 because it is nothing.

146
00:09:46,180 --> 00:09:51,120
 It is obtained through the removal of casino and removal of

147
00:09:51,120 --> 00:09:54,080
 casino means not paying attention

148
00:09:54,080 --> 00:09:55,800
 to the casino sign.

149
00:09:55,800 --> 00:10:00,850
 First there is casino sign in his mind and then he stops

150
00:10:00,850 --> 00:10:04,000
 paying attention to that casino

151
00:10:04,000 --> 00:10:07,910
 sign so the casino sign disappears and in the place of that

152
00:10:07,910 --> 00:10:09,800
 casino sign just the space

153
00:10:09,800 --> 00:10:15,520
 remains so that space is space so that cannot be extended.

154
00:10:15,520 --> 00:10:29,560
 If he extends it nothing further happens so nothing will

155
00:10:29,560 --> 00:10:32,560
 happen and consciousness need

156
00:10:32,560 --> 00:10:36,170
 not be extended actually consciousness should not be or

157
00:10:36,170 --> 00:10:38,320
 could not be extended since it is

158
00:10:38,320 --> 00:10:42,270
 a state consisting in an individual essence that means it

159
00:10:42,270 --> 00:10:44,000
 is a paramata it is a it is

160
00:10:44,000 --> 00:10:48,150
 an ultimate reality a reality which has its own

161
00:10:48,150 --> 00:10:52,040
 characteristic or individual essence.

162
00:10:52,040 --> 00:10:56,790
 Only the concept can be extended not the real the ultimate

163
00:10:56,790 --> 00:10:59,520
 reality ultimate reality is just

164
00:10:59,520 --> 00:11:06,100
 ultimate reality and it does not lend itself to to being

165
00:11:06,100 --> 00:11:07,760
 extended.

166
00:11:07,760 --> 00:11:11,420
 So this consciousness or the first consciousness cannot be

167
00:11:11,420 --> 00:11:13,960
 the first Aruba Vajrayana consciousness

168
00:11:13,960 --> 00:11:18,550
 cannot be extended and the disappearance of consciousness

169
00:11:18,550 --> 00:11:20,800
 need not be extended because

170
00:11:20,800 --> 00:11:24,800
 actually it is concept and it is non-existent.

171
00:11:24,800 --> 00:11:28,620
 So what is non-existent cannot be extended and then the

172
00:11:28,620 --> 00:11:30,740
 last one the base consisting

173
00:11:30,740 --> 00:11:34,350
 of nether perception or non-perception here also the object

174
00:11:34,350 --> 00:11:36,080
 of the base consisting of

175
00:11:36,080 --> 00:11:40,130
 nether perception or non-perception need not be extended

176
00:11:40,130 --> 00:11:42,240
 since it is it true is a state

177
00:11:42,240 --> 00:11:44,840
 consisting in an individual essence.

178
00:11:44,840 --> 00:11:53,880
 Now do you remember the object of the fourth Aruba Vajray

179
00:11:53,880 --> 00:11:54,520
ana?

180
00:11:54,520 --> 00:11:59,240
 The object of the fourth Aruba Vajrayana is the third Aruba

181
00:11:59,240 --> 00:12:01,400
 Vajrayana consciousness.

182
00:12:01,400 --> 00:12:05,410
 So the third Aruba Vajrayana consciousness is again an

183
00:12:05,410 --> 00:12:07,920
 ultimate reality having its own

184
00:12:07,920 --> 00:12:13,700
 individual essence so it cannot be extended because it is

185
00:12:13,700 --> 00:12:16,840
 not not a concept so they cannot

186
00:12:16,840 --> 00:12:25,770
 be extended so this is as to whether it can be extended or

187
00:12:25,770 --> 00:12:26,080
 not.

188
00:12:26,080 --> 00:12:30,930
 And then as to the object paragraph 117 of this 40

189
00:12:30,930 --> 00:12:34,800
 meditation subjects 22 have counterpart

190
00:12:34,800 --> 00:12:37,880
 sign as object.

191
00:12:37,880 --> 00:12:53,880
 Now on the chart under the column nimitta counterpart sign

192
00:12:53,880 --> 00:13:01,160
 PT means counterpart sign.

193
00:13:01,160 --> 00:13:10,460
 So it says 22 have counterpart signs 10 casinos 10 asubas

194
00:13:10,460 --> 00:13:13,880
 or fallness meditation and these

195
00:13:13,880 --> 00:13:17,600
 two.

196
00:13:17,600 --> 00:13:20,400
 Counterpart sign that is to say the 10 casinos the 10 kinds

197
00:13:20,400 --> 00:13:22,120
 of fallness mindfulness of breathing

198
00:13:22,120 --> 00:13:25,920
 and mindfulness occupied with the body the rest do not have

199
00:13:25,920 --> 00:13:27,880
 counterpart signs as object.

200
00:13:27,880 --> 00:13:31,560
 And 12 have states consisting in individual essence as

201
00:13:31,560 --> 00:13:33,320
 object that is to say eight of

202
00:13:33,320 --> 00:13:36,910
 the 10 recollections except mindfulness of breathing and

203
00:13:36,910 --> 00:13:38,720
 mindfulness occupied with the

204
00:13:38,720 --> 00:13:41,660
 body the perception of the passiveness and nutriment the

205
00:13:41,660 --> 00:13:43,320
 defining of the four elements

206
00:13:43,320 --> 00:13:46,110
 the base consisting of boundless consciousness and the base

207
00:13:46,110 --> 00:13:47,680
 consisting neither perception

208
00:13:47,680 --> 00:13:51,800
 or non perception.

209
00:13:51,800 --> 00:13:59,220
 So they have the ultimate reality as object and the 22 have

210
00:13:59,220 --> 00:14:02,720
 counterpart signs as object

211
00:14:02,720 --> 00:14:05,220
 that is to say the 10 casinos the 10 kinds of fallen as

212
00:14:05,220 --> 00:14:06,520
 mindfulness of breathing and

213
00:14:06,520 --> 00:14:10,090
 mindfulness of mindfulness occupied with the body while the

214
00:14:10,090 --> 00:14:12,400
 remaining six have not so classifiable

215
00:14:12,400 --> 00:14:13,400
 objects.

216
00:14:13,400 --> 00:14:18,360
 So these are the description of the objects of meditation

217
00:14:18,360 --> 00:14:21,080
 in different ways and the age

218
00:14:21,080 --> 00:14:25,230
 of mobile objects in the early states through the

219
00:14:25,230 --> 00:14:31,440
 counterpart though the counterpart is stationary

220
00:14:31,440 --> 00:14:34,680
 that is to say the festering the bleeding the warm first

221
00:14:34,680 --> 00:14:36,560
 yet mindfulness of breathing

222
00:14:36,560 --> 00:14:39,730
 and water casino the fire casino the air casino and in the

223
00:14:39,730 --> 00:14:41,600
 case of light casino the object

224
00:14:41,600 --> 00:14:44,160
 consisting of a circle of sunlight etc.

225
00:14:44,160 --> 00:14:48,570
 So they are shaking objects they can be shaking objects the

226
00:14:48,570 --> 00:14:50,920
 rest have mobile objects.

227
00:14:50,920 --> 00:14:58,750
 Now they have shaking objects only in the preliminary stage

228
00:14:58,750 --> 00:15:01,560
 but when when the yogi reaches

229
00:15:01,560 --> 00:15:05,600
 the counterpart counterpart sign states then they are

230
00:15:05,600 --> 00:15:08,840
 stationary so it is only in the preliminary

231
00:15:08,840 --> 00:15:16,740
 stage that they and they have shaking objects or mobile

232
00:15:16,740 --> 00:15:19,240
 objects and as to the plane that

233
00:15:19,240 --> 00:15:23,410
 means as to the different 31 planes of existence 12 namely

234
00:15:23,410 --> 00:15:25,840
 the 10 kinds of fallen as mindfulness

235
00:15:25,840 --> 00:15:29,130
 occupied with the body and perception of repulsiveness in

236
00:15:29,130 --> 00:15:32,360
 new to men do not occur among deity.

237
00:15:32,360 --> 00:15:50,600
 That mean that in the fourth jar the breathing stops.

238
00:15:50,600 --> 00:16:02,040
 We come to that in the description of the breathing

239
00:16:02,040 --> 00:16:08,440
 meditation so that's right and

240
00:16:08,440 --> 00:16:14,990
 then as to apprehending that means as to taking the object

241
00:16:14,990 --> 00:16:18,320
 by by sight by by hearing by seeing

242
00:16:18,320 --> 00:16:23,550
 and so on so here according to sight touch and hearsay

243
00:16:23,550 --> 00:16:27,440
 hearsay means just hearing something

244
00:16:27,440 --> 00:16:31,650
 and these 19 that is to say nine casinos omitting the air

245
00:16:31,650 --> 00:16:34,240
 casino and the 10 kinds of fallenness

246
00:16:34,240 --> 00:16:38,530
 must be apprehended by sight so that means you look at

247
00:16:38,530 --> 00:16:41,600
 something and practice meditation

248
00:16:41,600 --> 00:16:44,080
 the meaning is that in the early stage the assigned must be

249
00:16:44,080 --> 00:16:45,360
 apprehended by constantly

250
00:16:45,360 --> 00:16:48,770
 looking with the eye in the case of mindfulness occupied

251
00:16:48,770 --> 00:16:50,960
 with the body the five parts ending

252
00:16:50,960 --> 00:16:55,690
 with skin must be apprehended by sight and the rest by

253
00:16:55,690 --> 00:16:58,840
 hearsay head hair body hair nails

254
00:16:58,840 --> 00:17:05,280
 teeth skin these you look at with your eyes and practice

255
00:17:05,280 --> 00:17:08,840
 meditation on them and the others

256
00:17:08,840 --> 00:17:14,150
 some some you cannot see like liver or intestines and other

257
00:17:14,150 --> 00:17:17,520
 things so that you practice through

258
00:17:17,520 --> 00:17:25,560
 hearsay mindfulness of breathing must be apprehended by

259
00:17:25,560 --> 00:17:27,720
 touch so when you break this mindfulness

260
00:17:27,720 --> 00:17:32,210
 meditation you keep your mind here and be mindful of the

261
00:17:32,210 --> 00:17:34,560
 sensation of touch here the

262
00:17:34,560 --> 00:17:41,790
 air going going in and out of the nostrils the air casino

263
00:17:41,790 --> 00:17:45,200
 by sight and touch it will

264
00:17:45,200 --> 00:17:49,610
 become clearer when we come to the description of how to

265
00:17:49,610 --> 00:17:52,480
 practice air casino sometimes you

266
00:17:52,480 --> 00:17:59,310
 look at something moving say branches of tree or a banner

267
00:17:59,310 --> 00:18:05,160
 in the wind and you and you practice air

268
00:18:05,160 --> 00:18:09,250
 casino on that but sometimes the wind is blowing against

269
00:18:09,250 --> 00:18:11,920
 your body and you have the feeling of

270
00:18:11,920 --> 00:18:16,570
 touch here and so to concentrate on this and concentrate on

271
00:18:16,570 --> 00:18:19,560
 the wind air element here and so

272
00:18:19,560 --> 00:18:23,660
 in that case you practice by the sense of touch the

273
00:18:23,660 --> 00:18:27,400
 remaining 18 by hearsay that means just by

274
00:18:27,400 --> 00:18:32,400
 hearing the divine abiding of equanimity and the four imm

275
00:18:32,400 --> 00:18:35,520
aterial states are not apprehendable by a

276
00:18:35,520 --> 00:18:41,900
 beginner so you cannot practice upika and the four aruba v

277
00:18:41,900 --> 00:18:47,680
ajirajanas at the beginning because in

278
00:18:47,680 --> 00:18:51,180
 order to get aruba vajirajanas you must have you must you

279
00:18:51,180 --> 00:18:53,720
 must have got the five ruba vajirajanas

280
00:18:53,720 --> 00:19:01,320
 and in order to practice real upika upika brahma vihara

281
00:19:01,320 --> 00:19:05,320
 then you have to break you have to have

282
00:19:05,320 --> 00:19:12,720
 practiced the first three loving kindness confession and

283
00:19:12,720 --> 00:19:18,160
 sympathetic joy so as a as a real divine

284
00:19:18,160 --> 00:19:22,040
 abiding equanimity cannot be practiced at the beginning

285
00:19:22,040 --> 00:19:24,440
 only after you have practiced the other

286
00:19:24,440 --> 00:19:32,770
 three can you practice equanimity and then as to condition

287
00:19:32,770 --> 00:19:37,200
 the ten casinos I mean nine casinos

288
00:19:37,200 --> 00:19:42,290
 omitting the space casino conditions for immaterial states

289
00:19:42,290 --> 00:19:44,880
 that means if you want to get aruba

290
00:19:44,880 --> 00:19:49,930
 vajirajanas and you practice one of the nine casinos o

291
00:19:49,930 --> 00:19:53,440
mitting the space casino because you

292
00:19:53,440 --> 00:20:01,600
 have to practice the removing of the casino object and get

293
00:20:01,600 --> 00:20:05,920
 the space so space cannot be removed to

294
00:20:05,920 --> 00:20:11,200
 space is space so space casino is exempted from from those

295
00:20:11,200 --> 00:20:14,520
 that are conditions for immaterial

296
00:20:14,520 --> 00:20:18,440
 state or aruba vajirajanas the ten casinos are conditions

297
00:20:18,440 --> 00:20:20,720
 for the kinds of direct knowledge

298
00:20:20,720 --> 00:20:26,380
 so if you want to get the direct knowledge or abhinia that

299
00:20:26,380 --> 00:20:30,200
 means supernormal power then you

300
00:20:30,200 --> 00:20:36,050
 practice first one of the ten casinos and actually if you

301
00:20:36,050 --> 00:20:39,120
 want to get different different

302
00:20:39,120 --> 00:20:49,960
 results then you you practice different different casinos

303
00:20:49,960 --> 00:20:54,960
 you know if you want to shake something

304
00:20:54,960 --> 00:21:02,450
 suppose you want to shake the shake the city hall building

305
00:21:02,450 --> 00:21:08,240
 by your supernormal power then first you

306
00:21:08,240 --> 00:21:14,220
 you must practice water casino or air casino but not the

307
00:21:14,220 --> 00:21:18,040
 the earth casino if you practice earth

308
00:21:18,040 --> 00:21:22,820
 casino and try to shake it you it will not shake there is a

309
00:21:22,820 --> 00:21:25,840
 story of a novice who went up to heaven

310
00:21:25,840 --> 00:21:30,760
 I mean the abode of God and he said I will I will shake

311
00:21:30,760 --> 00:21:34,520
 your mansion and he he tried to shake it and

312
00:21:34,520 --> 00:21:39,790
 he could not so he is so the the celestial names make fun

313
00:21:39,790 --> 00:21:43,040
 of him and so he was ashamed and he went

314
00:21:43,040 --> 00:21:46,440
 back to his teacher and he told his teacher that he was

315
00:21:46,440 --> 00:21:49,120
 ashamed by the names because he he could

316
00:21:49,120 --> 00:21:54,870
 not shake the shake their mansion and he asked his teacher

317
00:21:54,870 --> 00:21:58,320
 why so his teacher said look at something

318
00:21:58,320 --> 00:22:03,560
 there so a a cow dung was floating in the in the river so

319
00:22:03,560 --> 00:22:06,960
 he got the hint and so next time he went

320
00:22:06,960 --> 00:22:11,770
 he went back to the the celestial abode and this time he he

321
00:22:11,770 --> 00:22:14,680
 practiced water magic I mean water

322
00:22:14,680 --> 00:22:20,520
 casino first and then maybe maybe mansion shake so

323
00:22:20,520 --> 00:22:26,080
 according to what you want from from the casinos

324
00:22:26,080 --> 00:22:28,560
 you have to practice different kinds of casino and they are

325
00:22:28,560 --> 00:22:30,480
 they are mentioned in the later chapters

326
00:22:30,480 --> 00:22:37,400
 so the ten casinos are conditions for the kinds of direct

327
00:22:37,400 --> 00:22:40,440
 knowledge three divine abidings and

328
00:22:40,440 --> 00:22:43,210
 conditions for the fourth divine abiding so the fourth

329
00:22:43,210 --> 00:22:45,080
 divine abiding cannot be practiced

330
00:22:45,080 --> 00:22:49,280
 at the beginning but only after the first three each lower

331
00:22:49,280 --> 00:22:51,880
 immaterial state is a condition for

332
00:22:51,880 --> 00:22:54,870
 each higher one the base consisting of neither perception

333
00:22:54,870 --> 00:22:56,880
 or non-perception is a condition for

334
00:22:56,880 --> 00:23:01,770
 the attainment of cessation that means cessation of mental

335
00:23:01,770 --> 00:23:05,360
 activities cessation of perception and

336
00:23:05,360 --> 00:23:11,520
 feeling actually cessation of mental activities all are

337
00:23:11,520 --> 00:23:15,720
 conditions for living in bliss that means

338
00:23:15,720 --> 00:23:19,820
 living in bliss in this very life for insight and for the

339
00:23:19,820 --> 00:23:23,360
 fortunate kinds of becoming that means for

340
00:23:23,360 --> 00:23:27,810
 a good life in the future as to suitability to temperament

341
00:23:27,810 --> 00:23:30,240
 from their importance here the

342
00:23:30,240 --> 00:23:32,610
 exposition should be understood according to what is

343
00:23:32,610 --> 00:23:36,320
 suitable to the temperament and then it describes

344
00:23:36,320 --> 00:23:44,870
 which subjects of meditation suitable for which kind of

345
00:23:44,870 --> 00:23:59,320
 temperament yes practice the third before

346
00:23:59,320 --> 00:24:12,470
 the first right but the the normal procedure is to practice

347
00:24:12,470 --> 00:24:19,360
 the loving kindness first and then

348
00:24:19,360 --> 00:24:24,440
 practice the second one and the third one and this this

349
00:24:24,440 --> 00:24:28,320
 this means you practice so that you get jhana

350
00:24:28,320 --> 00:24:33,700
 from this practice if you do not get to the to the state

351
00:24:33,700 --> 00:24:37,800
 state of jhana then even equanimity you can

352
00:24:37,800 --> 00:24:46,130
 practice but here it is meant for jhana because the equanim

353
00:24:46,130 --> 00:24:50,680
ity leads to the fifth jhana so in

354
00:24:50,680 --> 00:24:53,770
 order to get the fifth jhana you need to you have the first

355
00:24:53,770 --> 00:24:56,400
 again that and fourth jhana and those

356
00:24:56,400 --> 00:25:00,870
 can be obtained through the practice of the lower three on

357
00:25:00,870 --> 00:25:03,360
 the other three divine abiding

358
00:25:03,360 --> 00:25:08,720
 you reach Jhana but not not to the fifth jhana you read to

359
00:25:08,720 --> 00:25:14,360
 the fourth jhana owner and by the

360
00:25:14,360 --> 00:25:22,140
 practice of equanimity you reach the fifth jhana and now

361
00:25:22,140 --> 00:25:29,120
 the different kinds of meditation suitable

362
00:25:29,120 --> 00:25:34,370
 for different types of temperaments and all this has been

363
00:25:34,370 --> 00:25:37,560
 stated in the form of direct opposition

364
00:25:37,560 --> 00:25:41,890
 and complete suitability that means if it says this this

365
00:25:41,890 --> 00:25:46,000
 meditation is suitable for this temperament

366
00:25:46,000 --> 00:25:49,570
 it means that this meditation is the direct opposite of

367
00:25:49,570 --> 00:25:52,520
 that temperament and it is very suitable for

368
00:25:52,520 --> 00:25:57,210
 it but it does not mean that you cannot practice other

369
00:25:57,210 --> 00:26:00,200
 meditation so but there is actually no

370
00:26:00,200 --> 00:26:05,300
 profitable development that does not suppress greed etc and

371
00:26:05,300 --> 00:26:07,480
 help faith and so on so in fact

372
00:26:07,480 --> 00:26:13,270
 you can practice any meditation but here the meditations

373
00:26:13,270 --> 00:26:16,560
 subjects of meditation and temperaments

374
00:26:16,560 --> 00:26:23,420
 are given to show that they are the direct opposite of the

375
00:26:23,420 --> 00:26:26,600
 temperaments and they are the

376
00:26:26,600 --> 00:26:34,610
 most suitable suppose I am of deluded temperament then the

377
00:26:34,610 --> 00:26:39,280
 most suitable meditation for me is

378
00:26:39,280 --> 00:26:43,380
 breathing meditation but that does not mean that I cannot

379
00:26:43,380 --> 00:26:47,760
 practice any other meditation because any

380
00:26:47,760 --> 00:26:54,130
 meditation will help me to suppress mental defilements and

381
00:26:54,130 --> 00:27:01,280
 to develop wholesome mental states even in

382
00:27:01,280 --> 00:27:06,060
 the sodas Bhumbura was advising Rahula and other persons to

383
00:27:06,060 --> 00:27:08,920
 practice different kinds of meditation

384
00:27:08,920 --> 00:27:12,350
 not only one meditation but different kinds of meditation

385
00:27:12,350 --> 00:27:16,080
 to one and only person so any meditation

386
00:27:16,080 --> 00:27:20,240
 can be practiced by anyone but if you want to get the best

387
00:27:20,240 --> 00:27:23,760
 out of it then you choose the one which

388
00:27:23,760 --> 00:27:32,120
 is most suitable for your temperament all casinos a object

389
00:27:32,120 --> 00:27:37,400
 implied seeing consciousness

390
00:27:37,400 --> 00:27:47,510
 before I'm right yes see Cassie Cassie Cassina's should be

391
00:27:47,510 --> 00:27:52,720
 practiced first by looking at them so

392
00:27:52,720 --> 00:27:55,560
 when you look at them then you have to be seeing

393
00:27:55,560 --> 00:27:58,680
 consciousness and then you try to memorize or you

394
00:27:58,680 --> 00:28:02,850
 try to to take it into your mind that means you you close

395
00:28:02,850 --> 00:28:06,000
 your eyes and try to take that off the

396
00:28:06,000 --> 00:28:10,500
 image so when you can get the image clearly in your mind

397
00:28:10,500 --> 00:28:14,080
 then you are said to get and the

398
00:28:14,080 --> 00:28:23,710
 what sign learning they call learning sign actually the

399
00:28:23,710 --> 00:28:31,640
 cross sign and after you you you get the

400
00:28:31,640 --> 00:28:36,760
 learning sign and you don't you you no longer need the

401
00:28:36,760 --> 00:28:40,320
 actual actual disk actual object but

402
00:28:40,320 --> 00:28:45,960
 you develop you you dwell on the on the sign you you get in

403
00:28:45,960 --> 00:28:49,640
 your mind so at that moment at that

404
00:28:49,640 --> 00:28:56,210
 time on it is not seeing consciousness but the other manor

405
00:28:56,210 --> 00:28:59,800
 water I mean through mind or not through

406
00:28:59,800 --> 00:29:06,140
 not through I door so first first through I door you look

407
00:29:06,140 --> 00:29:10,480
 at the the Cassina and practice meditation

408
00:29:10,480 --> 00:29:16,440
 and after you get the learning sign then your meditation is

409
00:29:16,440 --> 00:29:19,680
 through mind or you see through

410
00:29:19,680 --> 00:29:29,390
 mind but not through the eye and then say dedicating

411
00:29:29,390 --> 00:29:35,920
 oneself to the blessed one or to the teacher that

412
00:29:35,920 --> 00:29:42,160
 means giving or relinquishing oneself and to them do do to

413
00:29:42,160 --> 00:29:45,480
 the blessed one or to the voter it's all

414
00:29:45,480 --> 00:29:55,840
 right but to the teacher I do not recommend because not all

415
00:29:55,840 --> 00:30:00,640
 teachers are to be trusted

416
00:30:00,640 --> 00:30:09,450
 considering what's happening these days so on the page 119

417
00:30:09,450 --> 00:30:13,280
 said when he dedicates himself to a

418
00:30:13,280 --> 00:30:17,940
 teacher I run and push this my person to you so I give up I

419
00:30:17,940 --> 00:30:23,560
 give myself up to you it may be

420
00:30:23,560 --> 00:30:29,740
 dangerous if a teacher has what to call our terrier motives

421
00:30:29,740 --> 00:30:33,200
 so it is better to to give yourself to

422
00:30:33,200 --> 00:30:42,020
 the Buddha not to the teacher these days okay and then

423
00:30:42,020 --> 00:30:49,520
 sincere inclination a resolution I think

424
00:30:49,520 --> 00:30:57,850
 they are not so difficult now in paragraph 128 about four

425
00:30:57,850 --> 00:31:01,840
 lines for it is one of such sincere

426
00:31:01,840 --> 00:31:05,510
 inclination which arrives at one of the three kinds of

427
00:31:05,510 --> 00:31:08,480
 enlightenment that means enlightenment

428
00:31:08,480 --> 00:31:16,460
 as a Buddha enlightenment as a but jigabuda and

429
00:31:16,460 --> 00:31:16,560
 enlightenment as an arahant so these are three

430
00:31:16,560 --> 00:31:20,660
 kinds of enlightenment and then six kinds of inclination

431
00:31:20,660 --> 00:31:23,320
 lead to the maturing of the enlightenment

432
00:31:23,320 --> 00:31:27,470
 of the bodhisattvas these may be something like a parameter

433
00:31:27,470 --> 00:31:36,320
 is found in Mahayana is non-greet

434
00:31:36,320 --> 00:31:40,890
 non-delusion and so on these are the six inclinations I

435
00:31:40,890 --> 00:31:43,720
 mean six regularly six qualities

436
00:31:43,720 --> 00:31:50,000
 which was bodhisattvas especially develop so with the

437
00:31:50,000 --> 00:31:54,120
 inclination to non-greet bodhisattvas see the

438
00:31:54,120 --> 00:31:57,950
 fruit for see the fourth in greed with the inclination to

439
00:31:57,950 --> 00:32:00,160
 non-hate bodhisattvas see the

440
00:32:00,160 --> 00:32:06,390
 fourth in hate and so on they are not found although it is

441
00:32:06,390 --> 00:32:09,700
 a quotation we cannot trace this

442
00:32:09,700 --> 00:32:14,470
 quotation to any any text available nowadays so some some

443
00:32:14,470 --> 00:32:19,600
 text may have been lost or it may refer

444
00:32:19,600 --> 00:32:24,160
 to you some some other some sort has not belonging to Ther

445
00:32:24,160 --> 00:32:35,640
avada and then the last paragraph 132

446
00:32:35,640 --> 00:32:43,060
 apprehend the sign now the in in Pali the word nimitta is

447
00:32:43,060 --> 00:32:44,360
 used in different different meanings

448
00:32:44,360 --> 00:32:48,680
 so here apprehend the sign means just paying paying close

449
00:32:48,680 --> 00:32:51,400
 attention to what you hear from the teacher

450
00:32:51,400 --> 00:32:54,850
 so this is the preceding clause this is the subsequent

451
00:32:54,850 --> 00:32:57,160
 clause this is the meaning this is

452
00:32:57,160 --> 00:33:00,400
 the intention this is a simile and so on so paying closer

453
00:33:00,400 --> 00:33:03,320
 attention and trying to understand first

454
00:33:03,320 --> 00:33:07,510
 hear the words of the the the teacher and then trying to

455
00:33:07,510 --> 00:33:10,360
 understand it is called here apprehending

456
00:33:10,360 --> 00:33:13,890
 the sign it is not like apprehending the sign when you

457
00:33:13,890 --> 00:33:16,720
 practice meditation so apprehending of

458
00:33:16,720 --> 00:33:20,730
 signs will come later in chapter chapter 4 so here

459
00:33:20,730 --> 00:33:24,160
 apprehending the sign means just paying

460
00:33:24,160 --> 00:33:29,530
 close attention to to what the teacher said so this is

461
00:33:29,530 --> 00:33:33,600
 preceding clause this is subsequent clause

462
00:33:33,600 --> 00:33:36,770
 this is its meaning and so on when he listens attentively

463
00:33:36,770 --> 00:33:38,800
 apprehending the sign in this way

464
00:33:38,800 --> 00:33:44,420
 his meditation subject is well apprehended so he knows he

465
00:33:44,420 --> 00:33:47,480
 knows what he he what he should know

466
00:33:47,480 --> 00:33:51,330
 about meditation and and because of that he successfully

467
00:33:51,330 --> 00:33:54,120
 attains distinction attains distinction

468
00:33:54,120 --> 00:34:00,180
 means attains jhanas attains supernormal knowledge and att

469
00:34:00,180 --> 00:34:05,640
ains enlightenment but not others not

470
00:34:05,640 --> 00:34:12,020
 otherwise but not others he will successfully attain

471
00:34:12,020 --> 00:34:16,280
 distinction but not others who do not

472
00:34:16,280 --> 00:34:20,360
 listen attentively and apprehend the sign and so on so

473
00:34:20,360 --> 00:34:24,800
 otherwise should be corrected to others not

474
00:34:24,800 --> 00:34:33,020
 otherwise on page 121 first line he successfully attains

475
00:34:33,020 --> 00:34:37,800
 distinction but not others on this page

476
00:34:45,800 --> 00:34:54,120
 okay that is the end of that chapter so we are we are still

477
00:34:54,120 --> 00:35:05,440
 preparing preparing is not not not

478
00:35:05,440 --> 00:35:11,240
 over yet you have to you have to avoid 18 faulty monaster

479
00:35:11,240 --> 00:35:14,320
ies and find a suitable place for

480
00:35:14,320 --> 00:35:23,830
 meditation it's interesting about the parmita said

481
00:35:23,830 --> 00:35:30,320
 something that almost works it works for

482
00:35:30,320 --> 00:35:37,360
 them and the end of two is a little bit of a stretch but

483
00:35:37,360 --> 00:35:42,080
 yeah interesting thank you very much

484
00:35:42,080 --> 00:35:47,490
 we're going to continue in March sometime in March we'll do

485
00:35:47,490 --> 00:35:51,880
 another 12 weeks and that will go from

486
00:35:51,880 --> 00:36:01,070
 we hope from chapter four chapter eight through chapter

487
00:36:01,070 --> 00:36:09,080
 eight is there something we can start

488
00:36:09,080 --> 00:36:24,280
 reading it in the meeting yes

489
00:36:24,280 --> 00:36:37,480
 okay

490
00:36:37,480 --> 00:36:48,680
 yeah

491
00:36:48,680 --> 00:36:59,880
 yeah

492
00:36:59,880 --> 00:37:09,880
 yeah

493
00:37:09,880 --> 00:37:25,080
 yeah

494
00:37:25,080 --> 00:37:35,080
 yeah

