1
00:00:00,000 --> 00:00:04,520
 Okay, so today we'll be continuing to talk about the

2
00:00:04,520 --> 00:00:10,840
 The Mangala, which we stopped sometimes

3
00:00:10,840 --> 00:00:22,670
 Just to recap, the Mangala are those things that do away

4
00:00:22,670 --> 00:00:27,560
 with evil. So we've gone through

5
00:00:29,560 --> 00:00:36,330
 three stanzas. And now we come up to some that are not so

6
00:00:36,330 --> 00:00:44,160
 directly related to the meditation practice.

7
00:00:44,160 --> 00:00:48,660
 So we'll try to skip quickly through them, but in order to

8
00:00:48,660 --> 00:00:51,080
 go, in order to be complete,

9
00:00:51,080 --> 00:00:55,480
 we'll go through them in order. And there are things that

10
00:00:55,480 --> 00:00:58,560
 you can say on these blessings about

11
00:00:58,560 --> 00:01:08,080
 meditation. These are blessings that generally relate to

12
00:01:08,080 --> 00:01:12,840
 people who live in the house, people

13
00:01:12,840 --> 00:01:18,360
 who live ordinary lives and have jobs and make money and

14
00:01:18,360 --> 00:01:21,840
 employ people or are employed by other

15
00:01:21,840 --> 00:01:29,390
 people. But the general purpose of these blessings is to

16
00:01:29,390 --> 00:01:34,680
 create peace and harmony just the same. And

17
00:01:34,680 --> 00:01:46,850
 for people who are looking to make their way closer to a

18
00:01:46,850 --> 00:01:51,480
 meditative state of life, who find

19
00:01:51,480 --> 00:02:03,830
 themselves surrounded by obstacles or surrounded by worldly

20
00:02:03,830 --> 00:02:10,080
 activities. These are very important

21
00:02:10,080 --> 00:02:18,000
 ways to create peace and harmony and a meditative state or

22
00:02:18,000 --> 00:02:22,880
 a meditative way of life that will bring

23
00:02:22,880 --> 00:02:28,170
 anyone closer to the Buddha's teaching. If someone's goal

24
00:02:28,170 --> 00:02:31,080
 is to ordain, then it will bring them closer

25
00:02:31,080 --> 00:02:36,450
 to that goal because of the peace and the harmony that it

26
00:02:36,450 --> 00:02:40,360
 brings. If someone's goal is to live a

27
00:02:40,360 --> 00:02:42,980
 meditative life as a lay person, then these are the sorts

28
00:02:42,980 --> 00:02:45,200
 of things that will allow. Besides the

29
00:02:45,200 --> 00:02:48,270
 things that we've already talked about, here are more

30
00:02:48,270 --> 00:02:51,560
 things that are useful for all of us. As

31
00:02:51,560 --> 00:03:00,600
 monastics, some of them aren't even directly useful. But

32
00:03:00,600 --> 00:03:04,760
 they are those things that we have to keep

33
00:03:04,760 --> 00:03:08,340
 in mind and the general principles that we have to keep in

34
00:03:08,340 --> 00:03:10,360
 mind in terms of creating peace and

35
00:03:10,360 --> 00:03:17,620
 harmony. So the next ones are matapitu upatthana, putatara

36
00:03:17,620 --> 00:03:19,120
 dara sasangahu, anakula jakamanta,

37
00:03:19,120 --> 00:03:25,500
 nithamangalamutamam. So caring for one's parents, caring

38
00:03:25,500 --> 00:03:27,960
 for one's mother, caring for one's father,

39
00:03:27,960 --> 00:03:34,730
 having good relations with one's relative, or having a good

40
00:03:34,730 --> 00:03:40,480
 relationship with one's spouse and

41
00:03:40,480 --> 00:03:47,920
 children, and doing work that is uncomplicated or that is

42
00:03:47,920 --> 00:03:54,760
 not crooked, having straight affairs,

43
00:03:54,760 --> 00:04:01,170
 affairs that are not crooked or not complicated. So this is

44
00:04:01,170 --> 00:04:03,840
 the next dance, and it goes with the

45
00:04:03,840 --> 00:04:07,400
 next one. So we might as well add them all in here. The

46
00:04:07,400 --> 00:04:10,200
 next one is dana-naccha dhammacharya ajha,

47
00:04:10,200 --> 00:04:17,120
 yatakana-naccha sangahu, anau-jhani kamman, vithamangamutam

48
00:04:17,120 --> 00:04:22,280
. Dana means charity, dhammacharya,

49
00:04:22,280 --> 00:04:28,930
 the practice of the dhammacharya, yatakana-naccha sangahu,

50
00:04:28,930 --> 00:04:32,440
 having good relationships with all of

51
00:04:32,440 --> 00:04:39,140
 one's relatives, and anau-jhani kammani, performing deeds

52
00:04:39,140 --> 00:04:44,560
 that are unblame, not blameworthy, that are

53
00:04:44,560 --> 00:04:49,910
 free from blameness. So what we'll see here is now a

54
00:04:49,910 --> 00:04:54,080
 progression, and Buddha begins in these

55
00:04:54,080 --> 00:04:57,470
 stanzas by talking about those things that will be useful

56
00:04:57,470 --> 00:05:01,320
 specifically in the lay life, but we can

57
00:05:01,320 --> 00:05:03,760
 see towards the end it starts, or in the second stanza it

58
00:05:03,760 --> 00:05:08,440
 starts to get a little bit more involved

59
00:05:08,440 --> 00:05:12,230
 with the meditation. So we'll go through all of these today

60
00:05:12,230 --> 00:05:14,800
. They're all quite similar actually.

61
00:05:14,800 --> 00:05:18,140
 Matapita upatthana, this is something, looking after your

62
00:05:18,140 --> 00:05:20,400
 parents, is something that meditators

63
00:05:20,400 --> 00:05:28,500
 will quite often overlook, forget about, or even disdain. I

64
00:05:28,500 --> 00:05:33,280
've even had arguments with people who

65
00:05:33,280 --> 00:05:41,420
 have explained their view that their parents only gave

66
00:05:41,420 --> 00:05:46,000
 birth to them out of desire, out of lust for

67
00:05:46,000 --> 00:05:50,970
 each other, and they only cared for us in their womb

68
00:05:50,970 --> 00:05:55,000
 because they wanted to, and it was all because

69
00:05:55,000 --> 00:05:58,780
 of their desire to do so, and therefore we don't owe them

70
00:05:58,780 --> 00:06:01,440
 anything. We never asked them to give

71
00:06:01,440 --> 00:06:05,390
 birth to us, and even when we were growing up we didn't ask

72
00:06:05,390 --> 00:06:09,280
 them to take care of us. Of course,

73
00:06:09,280 --> 00:06:15,050
 some people have the misfortune to have bad parents,

74
00:06:15,050 --> 00:06:17,800
 parents who didn't look after their children,

75
00:06:17,800 --> 00:06:20,930
 didn't perform the duties towards their children, didn't

76
00:06:20,930 --> 00:06:23,320
 take care of them and maybe even abused

77
00:06:23,320 --> 00:06:28,340
 them. And so these people will argue or have at least

78
00:06:28,340 --> 00:06:31,920
 either argue directly or at the very least

79
00:06:31,920 --> 00:06:35,670
 have a difficult time appreciating this teaching or

80
00:06:35,670 --> 00:06:39,040
 figuring out how they themselves, because of

81
00:06:39,040 --> 00:06:44,130
 their history, can put into practice this teaching. So with

82
00:06:44,130 --> 00:06:47,880
 all of these we have to understand the

83
00:06:47,880 --> 00:06:51,410
 situation that we find ourselves in. Scientifically

84
00:06:51,410 --> 00:06:54,880
 speaking, it's correct to say that, or biologically

85
00:06:54,880 --> 00:07:03,040
 speaking, that human beings procreate based on instinct in

86
00:07:03,040 --> 00:07:09,680
 the mind, and there's nothing really

87
00:07:09,680 --> 00:07:13,740
 special about the relationship between a parent and a child

88
00:07:13,740 --> 00:07:15,680
. If you break down the chemical

89
00:07:15,680 --> 00:07:20,260
 composition of the parents and the child and all beings, it

90
00:07:20,260 --> 00:07:22,760
's only the genes that get passed on by

91
00:07:22,760 --> 00:07:27,930
 our parents. There's no part of our existence from a

92
00:07:27,930 --> 00:07:31,840
 materialist point of view that gives us some

93
00:07:31,840 --> 00:07:37,220
 responsibility towards our parents. This is from a material

94
00:07:37,220 --> 00:07:39,920
ist point of view, but from an experiential

95
00:07:39,920 --> 00:07:45,080
 point of view, we have to account for our conception in the

96
00:07:45,080 --> 00:07:50,120
 womb of our parents, which from a point

97
00:07:50,120 --> 00:07:54,440
 of view of experience is not at all an obvious outcome of

98
00:07:54,440 --> 00:07:57,160
 the deeds in this life. It's not

99
00:07:57,160 --> 00:08:01,700
 obvious to us how, I mean our goals and our dreams, we're

100
00:08:01,700 --> 00:08:05,360
 not preparing ourselves not

101
00:08:05,360 --> 00:08:09,600
 intentionally to be born in a human womb. It's very

102
00:08:09,600 --> 00:08:12,600
 difficult to understand how it is that we

103
00:08:12,600 --> 00:08:18,600
 come to be born in a womb again at all. And the answer to

104
00:08:18,600 --> 00:08:24,920
 this question is a very important part

105
00:08:24,920 --> 00:08:29,270
 of why we have such an important duty towards our, why we

106
00:08:29,270 --> 00:08:32,640
 have such a profound relationship to our

107
00:08:32,640 --> 00:08:38,620
 parents. Even before arguing this, what we should point out

108
00:08:38,620 --> 00:08:41,920
 is that it's clearly the case for a

109
00:08:41,920 --> 00:08:44,800
 person who comes to practice meditation. This is why this

110
00:08:44,800 --> 00:08:46,920
 one actually does have some relationship

111
00:08:46,920 --> 00:08:50,280
 to meditation practice. Anyone who comes to practice

112
00:08:50,280 --> 00:08:52,920
 meditation who does have a bad relationship

113
00:08:52,920 --> 00:08:59,570
 towards their parents will be struck by the feelings of

114
00:08:59,570 --> 00:09:04,360
 guilt and shame and even anger or sadness

115
00:09:04,360 --> 00:09:09,360
 towards their parents if they've had a bad relationship.

116
00:09:09,360 --> 00:09:11,480
 Anyone who's had a very good

117
00:09:11,480 --> 00:09:14,440
 relationship with their parents or who has had very loving

118
00:09:14,440 --> 00:09:17,200
 and kind parents will come to feel

119
00:09:17,200 --> 00:09:24,100
 great love and appreciation for their parents first and

120
00:09:24,100 --> 00:09:28,360
 foremost. And it will really, for a new

121
00:09:28,360 --> 00:09:31,490
 meditator, it would be quite a surprise that this should be

122
00:09:31,490 --> 00:09:33,520
 the case because they didn't come with

123
00:09:33,520 --> 00:09:37,160
 the intention of cultivating love towards their parents

124
00:09:37,160 --> 00:09:39,800
 directly or for dealing with the issues

125
00:09:39,800 --> 00:09:45,900
 involved with their parents directly perhaps, but they will

126
00:09:45,900 --> 00:09:51,240
 be shocked by how deep this relationship

127
00:09:51,240 --> 00:09:56,830
 really goes. So it doesn't have to be argued that this

128
00:09:56,830 --> 00:10:01,120
 relationship is an important one. It just has

129
00:10:01,120 --> 00:10:04,640
 to be understood why it's an important one from the point

130
00:10:04,640 --> 00:10:07,080
 of view of a meditator. It is important

131
00:10:07,080 --> 00:10:10,150
 to argue it with people who have never practiced meditation

132
00:10:10,150 --> 00:10:12,240
, but perhaps the easiest way to argue

133
00:10:12,240 --> 00:10:15,670
 it is to point to people who have meditated and explain

134
00:10:15,670 --> 00:10:18,160
 that, "Listen, when you do take the time

135
00:10:18,160 --> 00:10:24,380
 to look at your own mind, you're going to realize how

136
00:10:24,380 --> 00:10:28,800
 important, how profound effect this relationship

137
00:10:28,800 --> 00:10:33,440
 has on your mind." But just briefly to explain the theory

138
00:10:33,440 --> 00:10:36,520
 behind why this occurs, it has to do with

139
00:10:36,520 --> 00:10:40,340
 the theory or the fact of us being born as a human being.

140
00:10:40,340 --> 00:10:42,920
 To be born as a human being, you can't just

141
00:10:42,920 --> 00:10:47,000
 arise in a lotus flower. Apparently in the Buddhist time,

142
00:10:47,000 --> 00:10:50,040
 there are cases of people who are just arose

143
00:10:50,040 --> 00:10:53,440
 spontaneously in the lotus flower, or stories or legends,

144
00:10:53,440 --> 00:10:56,440
 whether they're true or not. I don't know.

145
00:10:56,440 --> 00:11:03,360
 But what we do see, 99 out of 100 times or 99 million out

146
00:11:03,360 --> 00:11:09,000
 of 100 million times or more, is human

147
00:11:09,000 --> 00:11:12,670
 beings being born in a womb. As I said from an experiential

148
00:11:12,670 --> 00:11:14,320
 point of view, that doesn't really

149
00:11:14,320 --> 00:11:18,880
 make a lot of sense. Why should we be born into the womb of

150
00:11:18,880 --> 00:11:21,040
 another from the point of view of

151
00:11:21,040 --> 00:11:31,510
 experience? So the point of it is, our minds and our lives

152
00:11:31,510 --> 00:11:36,960
 are very much geared towards this

153
00:11:36,960 --> 00:11:40,970
 reproductive cycle. This is why shortly after we're born,

154
00:11:40,970 --> 00:11:43,320
 maybe five or ten years, we begin to have

155
00:11:43,320 --> 00:11:47,950
 interest in the opposite gender in general, or in an

156
00:11:47,950 --> 00:11:51,920
 ordinary, or for the most part, of course,

157
00:11:51,920 --> 00:11:57,440
 homosexuality and so on, it is a part of the human state.

158
00:11:57,440 --> 00:12:00,200
 But for the majority of humanity,

159
00:12:00,200 --> 00:12:05,360
 there is this heterosexual reproductive desire. Even for

160
00:12:05,360 --> 00:12:07,600
 people who are not attracted to the

161
00:12:07,600 --> 00:12:11,680
 opposite gender, there will often arise desire to have

162
00:12:11,680 --> 00:12:15,400
 children. The male species, the male gender,

163
00:12:15,400 --> 00:12:20,140
 will often have desires to protect others, and the female

164
00:12:20,140 --> 00:12:23,840
 will often have desires to develop

165
00:12:23,840 --> 00:12:27,540
 stability or a nest or a home or so on. So in this way, we

166
00:12:27,540 --> 00:12:29,280
 behave a lot like animals,

167
00:12:29,280 --> 00:12:35,930
 and the materialist scientist will explain this in terms of

168
00:12:35,930 --> 00:12:39,480
 genes and so on. We explain it in

169
00:12:39,480 --> 00:12:43,020
 terms of habits that are accumulated from life to life. So

170
00:12:43,020 --> 00:12:45,800
 our experience cultivates habit. From

171
00:12:45,800 --> 00:12:48,200
 an experiential point of view, looking at the human state,

172
00:12:48,200 --> 00:12:49,720
 you have to say this is quite an

173
00:12:49,720 --> 00:12:54,600
 intense, intensely contrived state. It's not at all obvious

174
00:12:54,600 --> 00:12:57,560
. You wouldn't think, "Well, if you have

175
00:12:57,560 --> 00:13:00,590
 experience, if you start with just basic raw experience,

176
00:13:00,590 --> 00:13:02,160
 that somehow you're going to get

177
00:13:02,160 --> 00:13:05,170
 beings being born inside of each other's bodies." It's

178
00:13:05,170 --> 00:13:09,800
 quite a contrived state. And so our very

179
00:13:09,800 --> 00:13:16,260
 existence, as in this state, relies and depends on two

180
00:13:16,260 --> 00:13:25,480
 people, depends on our parents. And this

181
00:13:25,480 --> 00:13:29,850
 is where our mind is at. If we deny, like if we have

182
00:13:29,850 --> 00:13:33,320
 parents and they've taken care of us and so on,

183
00:13:33,320 --> 00:13:40,790
 and we deny that, we reject that. We're basically rejecting

184
00:13:40,790 --> 00:13:46,440
 the very foundation of our very being,

185
00:13:46,440 --> 00:13:50,230
 you know, this brain, this body. This is the implication of

186
00:13:50,230 --> 00:13:52,400
 what it means to inherit them

187
00:13:52,400 --> 00:13:55,990
 from your parents, is that we have a very strong connection

188
00:13:55,990 --> 00:14:02,760
 to these people. So this is just some

189
00:14:02,760 --> 00:14:07,020
 ideas of where this comes from, that really our parents are

190
00:14:07,020 --> 00:14:09,280
 very much a part of who we are. And

191
00:14:09,280 --> 00:14:16,580
 we chose really to be born in their womb. We came to this

192
00:14:16,580 --> 00:14:20,960
 through a cultivation of habits and

193
00:14:20,960 --> 00:14:25,270
 relationships in past lives. When you reject this, you

194
00:14:25,270 --> 00:14:29,200
 reject harmony and stability. So what I would

195
00:14:29,200 --> 00:14:33,290
 like to explain for many of these is whether or not you

196
00:14:33,290 --> 00:14:36,520
 understand that it's truly wholesome or

197
00:14:36,520 --> 00:14:39,880
 unwholesome to do either way, to ignore you, to leave

198
00:14:39,880 --> 00:14:42,240
 behind your parents or to take care of them.

199
00:14:42,240 --> 00:14:45,240
 When you live in the world, if you're going to have

200
00:14:45,240 --> 00:14:48,880
 children of your own, if you want to live in the

201
00:14:48,880 --> 00:14:53,980
 world and have a stable life, you have to play by the rules

202
00:14:53,980 --> 00:14:57,120
. If we become anarchists and reject

203
00:14:57,120 --> 00:15:02,140
 societal norms of some sort, or the basic norms of what it

204
00:15:02,140 --> 00:15:06,680
 means to be a human being, then all

205
00:15:06,680 --> 00:15:12,800
 that we'll find is chaos and confusion. And many problems

206
00:15:12,800 --> 00:15:16,960
 will arise. So some sort of system of

207
00:15:16,960 --> 00:15:25,510
 stability has to be appreciated, has to be honored. And the

208
00:15:25,510 --> 00:15:29,800
 care that we take for our parents is a

209
00:15:29,800 --> 00:15:32,380
 very important part of this. It's something that brings

210
00:15:32,380 --> 00:15:34,400
 stability to the world. When people stop

211
00:15:34,400 --> 00:15:38,290
 taking care of their parents, then of course the whole

212
00:15:38,290 --> 00:15:41,400
 system breaks down. Parents will begin to

213
00:15:41,400 --> 00:15:45,460
 neglect their children or neglect their children's

214
00:15:45,460 --> 00:15:49,400
 education, will not have any interest in taking

215
00:15:49,400 --> 00:15:53,470
 care of their children and the cycle breaks down. Of course

216
00:15:53,470 --> 00:15:57,000
, it goes either both ways. Our future,

217
00:15:57,000 --> 00:15:59,850
 when we have children, they will not think to take care of

218
00:15:59,850 --> 00:16:05,360
 us having seen the example that we've set

219
00:16:05,360 --> 00:16:10,070
 for them. And society becomes quite upset. Then we stop

220
00:16:10,070 --> 00:16:13,000
 respecting elders in general and we stop

221
00:16:13,000 --> 00:16:16,840
 respecting experience. Nowadays, people will often think it

222
00:16:16,840 --> 00:16:19,120
's intelligence that is more important,

223
00:16:19,120 --> 00:16:23,650
 and we'll laugh at our parents and their old ways. And it's

224
00:16:23,650 --> 00:16:25,880
 only when we get old ourselves that we

225
00:16:25,880 --> 00:16:31,960
 realize how important wisdom and experience really are. So

226
00:16:31,960 --> 00:16:34,760
 care for your parents is not only is it a

227
00:16:34,760 --> 00:16:39,080
 very wholesome thing to do to take care of them, to be

228
00:16:39,080 --> 00:16:42,520
 grateful, but it's also something that leads

229
00:16:42,520 --> 00:16:44,690
 to stability and harmony in the world. On top of that,

230
00:16:44,690 --> 00:16:46,680
 there is something to be said about gratitude.

231
00:16:46,680 --> 00:16:49,170
 This is the argument I've had before, is people don't

232
00:16:49,170 --> 00:16:52,040
 understand this word gratitude. They don't

233
00:16:52,040 --> 00:16:55,960
 understand why they should be grateful towards others. Grat

234
00:16:55,960 --> 00:16:58,240
itude is, and in this sense, gratitude,

235
00:16:58,240 --> 00:17:02,510
 you have to just understand what gratitude is like. Grat

236
00:17:02,510 --> 00:17:05,560
itude and ingratitude. In gratitude is a

237
00:17:05,560 --> 00:17:10,910
 shriveling mind state. It's a mind state full of anger and

238
00:17:10,910 --> 00:17:16,880
 aversion and discord. And it's a selfish

239
00:17:16,880 --> 00:17:20,690
 mind state because you have this logical one-to-one

240
00:17:20,690 --> 00:17:24,200
 relationship where someone has done something for

241
00:17:24,200 --> 00:17:27,330
 you and you have the opportunity to do something for them.

242
00:17:27,330 --> 00:17:31,200
 When you don't do that, you create

243
00:17:31,200 --> 00:17:34,390
 friction, you create tension, and you create suffering in

244
00:17:34,390 --> 00:17:36,200
 your mind. This is something that

245
00:17:36,200 --> 00:17:39,280
 you have to see, but meditators become more and more

246
00:17:39,280 --> 00:17:43,000
 grateful as they practice. And so if we have

247
00:17:43,000 --> 00:17:46,560
 gained whatever benefit we have gained from other people,

248
00:17:46,560 --> 00:17:48,600
 we tend to be very keen to pay it back,

249
00:17:48,600 --> 00:17:54,170
 even if the other people are in general mean and evil

250
00:17:54,170 --> 00:17:59,200
 people. Even in that case, we think more and

251
00:17:59,200 --> 00:18:02,560
 more about the good that they may have done for us and less

252
00:18:02,560 --> 00:18:04,400
 and less about the bad things that they've

253
00:18:04,400 --> 00:18:06,960
 done. So if your parents have done bad things for you, the

254
00:18:06,960 --> 00:18:08,720
 meditator will still find themselves

255
00:18:08,720 --> 00:18:12,910
 wishing and having great love towards their parents for

256
00:18:12,910 --> 00:18:16,400
 whatever good that they did do for them. This

257
00:18:16,400 --> 00:18:21,940
 is how the meditator's mind starts to incline. It inclines

258
00:18:21,940 --> 00:18:30,440
 away from revengeful and reactionary

259
00:18:30,440 --> 00:18:36,680
 mind states and it inclines towards grateful and loving

260
00:18:36,680 --> 00:18:41,840
 mind state. So this is actually quite

261
00:18:41,840 --> 00:18:44,060
 important for meditators and it's important for people

262
00:18:44,060 --> 00:18:45,800
 thinking to come to practice meditation,

263
00:18:45,800 --> 00:18:48,690
 who should realize that starting with their parents, they

264
00:18:48,690 --> 00:18:50,640
 should clear up all their relationships.

265
00:18:50,640 --> 00:18:55,290
 They should create a stable life where they are, at least

266
00:18:55,290 --> 00:19:01,600
 in terms of settling all their affairs

267
00:19:01,600 --> 00:19:04,500
 with all of their relationships. So if you have a problem

268
00:19:04,500 --> 00:19:06,200
 with your parents, don't run away to

269
00:19:06,200 --> 00:19:10,610
 come meditate. It won't work. Many meditators run away for

270
00:19:10,610 --> 00:19:13,680
 that reason. Settle your affairs first

271
00:19:13,680 --> 00:19:15,820
 and then come and meditate so that it won't bother you when

272
00:19:15,820 --> 00:19:17,240
 you're practicing meditation.

273
00:19:17,240 --> 00:19:21,670
 Now there is one sort of exception. I mean, monks are not

274
00:19:21,670 --> 00:19:25,120
 expected to have direct contact with their

275
00:19:25,120 --> 00:19:31,210
 parents. We're not expected to take care of them when they

276
00:19:31,210 --> 00:19:34,320
're old or so on. We're not expected to

277
00:19:34,320 --> 00:19:38,540
 have much relationship with anyone at all. A monk can go

278
00:19:38,540 --> 00:19:41,160
 off in the forest and leave the world behind.

279
00:19:41,160 --> 00:19:47,820
 But even as monastics, we shouldn't take this too literally

280
00:19:47,820 --> 00:19:50,000
 and think that if our parents are in

281
00:19:50,000 --> 00:19:56,290
 need, we should avoid contact with such worldly people,

282
00:19:56,290 --> 00:19:59,400
 even if our parents are worldly sorts

283
00:19:59,400 --> 00:20:03,130
 of people. The Buddha was quite clear on this and it's a

284
00:20:03,130 --> 00:20:06,960
 good example of how, rather than seeing

285
00:20:06,960 --> 00:20:11,530
 relationships as an obstacle to our practice, seeing them

286
00:20:11,530 --> 00:20:14,440
 as our practice, monks are allowed

287
00:20:14,440 --> 00:20:18,840
 to take care of their parents. There was a monk who was

288
00:20:18,840 --> 00:20:23,920
 praised by the Buddha for taking care of his

289
00:20:23,920 --> 00:20:27,170
 parents. So anyway, this is the first one. Taking care of

290
00:20:27,170 --> 00:20:30,360
 your parents, of course, what needs to be

291
00:20:30,360 --> 00:20:32,690
 said, the core of this blessing, this is just explaining

292
00:20:32,690 --> 00:20:34,480
 why it's important, but the core of

293
00:20:34,480 --> 00:20:36,490
 the blessing, what does it mean to take care of your

294
00:20:36,490 --> 00:20:38,480
 parents? It doesn't necessarily mean that you

295
00:20:38,480 --> 00:20:41,350
 have to give them medicine and you have to bathe them and

296
00:20:41,350 --> 00:20:43,080
 clothe them or however, when they get

297
00:20:43,080 --> 00:20:46,430
 older or at any time. The Buddha said that in this way, you

298
00:20:46,430 --> 00:20:49,200
 can never pay back your parents. If your

299
00:20:49,200 --> 00:20:52,480
 parents have, even to the extent that your parents have

300
00:20:52,480 --> 00:20:54,560
 given birth to you, the extent that they

301
00:20:54,560 --> 00:20:59,720
 carried, your mother carried you in her womb for nine

302
00:20:59,720 --> 00:21:03,320
 months and your father was protecting her

303
00:21:03,320 --> 00:21:06,500
 during that time and the love that they gave and the years

304
00:21:06,500 --> 00:21:09,440
 and years that they put into raising you.

305
00:21:09,440 --> 00:21:14,360
 The Buddha said it's very difficult to pay back such

306
00:21:14,360 --> 00:21:19,680
 parents. Of course, if your father ran away

307
00:21:19,680 --> 00:21:23,930
 after your mother got pregnant and your mother had no

308
00:21:23,930 --> 00:21:26,600
 interest in you and abandoned you when you

309
00:21:26,600 --> 00:21:29,210
 were born and so on, it's a little more difficult to see,

310
00:21:29,210 --> 00:21:30,920
 even though we understand this to be

311
00:21:30,920 --> 00:21:34,810
 karmic and it still points to the strength of the

312
00:21:34,810 --> 00:21:42,440
 relationship. The Buddha said the only way to

313
00:21:42,440 --> 00:21:46,390
 clear this up is with the Dhamma, even if your parents are

314
00:21:46,390 --> 00:21:49,320
 mean and evil people. If you think

315
00:21:49,320 --> 00:21:53,490
 that your parents have done nothing for you or so little or

316
00:21:53,490 --> 00:21:55,520
 have done so much bad to you,

317
00:21:55,520 --> 00:22:03,850
 that the good that they've done is totally outweighed. Even

318
00:22:03,850 --> 00:22:08,240
 still, the teaching applies to

319
00:22:08,240 --> 00:22:11,590
 administer to them with the Dhamma. If you still have a

320
00:22:11,590 --> 00:22:14,120
 relationship with your parents or have an

321
00:22:14,120 --> 00:22:17,880
 abusive relationship with anyone, a bad relationship, or if

322
00:22:17,880 --> 00:22:19,880
 you have a good relationship, if your parents

323
00:22:19,880 --> 00:22:22,760
 have done great things and you feel somehow you want to pay

324
00:22:22,760 --> 00:22:25,360
 them back, somehow you want to show

325
00:22:25,360 --> 00:22:28,880
 your gratitude or you have the chance. The gratitude that

326
00:22:28,880 --> 00:22:30,480
 we should show is in the Dhamma,

327
00:22:30,480 --> 00:22:35,570
 that we should practice and we should teach and we should

328
00:22:35,570 --> 00:22:40,960
 set an example and we should encourage

329
00:22:40,960 --> 00:22:44,720
 them in the practice. We should be a support for them to

330
00:22:44,720 --> 00:22:47,320
 find peace, happiness, and freedom from

331
00:22:47,320 --> 00:22:50,570
 suffering and not just in a material sense. Of course, this

332
00:22:50,570 --> 00:22:52,640
 goes with anyone. Happiness in a

333
00:22:52,640 --> 00:22:56,010
 material sense can easily be bought, can easily be found in

334
00:22:56,010 --> 00:22:57,760
 the world at least for some time,

335
00:22:57,760 --> 00:23:02,000
 but happiness in the spiritual sense takes much more. It

336
00:23:02,000 --> 00:23:04,080
 takes wisdom, it takes mindfulness,

337
00:23:04,080 --> 00:23:07,790
 it takes diligence, it takes patience, it takes contentment

338
00:23:07,790 --> 00:23:10,200
, it takes many qualities that can

339
00:23:10,200 --> 00:23:12,890
 only be gained through the practice of meditation. So the

340
00:23:12,890 --> 00:23:14,440
 best thing you can do for anyone,

341
00:23:14,440 --> 00:23:17,410
 especially your parents, is to encourage them in the

342
00:23:17,410 --> 00:23:21,240
 practice of meditation or help support them in

343
00:23:21,240 --> 00:23:25,460
 the practice. Okay, so this is the first one. Then we have

344
00:23:25,460 --> 00:23:27,600
 in terms of, we'll just lump them

345
00:23:27,600 --> 00:23:31,570
 all together because here we have in terms of one's spouse

346
00:23:31,570 --> 00:23:34,720
 and children and relatives and the

347
00:23:34,720 --> 00:23:39,130
 commentary even goes further and says this is even in terms

348
00:23:39,130 --> 00:23:42,040
 of our employees and our employers and

349
00:23:42,040 --> 00:23:45,990
 really everyone that we have relationships with. It could

350
00:23:45,990 --> 00:23:48,400
 be our fellow employees, it could be

351
00:23:48,400 --> 00:23:51,350
 fellow students who are studying, it could be fellow mon

352
00:23:51,350 --> 00:23:55,440
astics in the world, this harmony in

353
00:23:55,440 --> 00:24:02,000
 the community. And on the one hand, many of these as I said

354
00:24:02,000 --> 00:24:04,920
 are not directly related to meditation,

355
00:24:04,920 --> 00:24:11,630
 but if you look at it another way, these are really an

356
00:24:11,630 --> 00:24:17,480
 indication of the mental development

357
00:24:17,480 --> 00:24:21,990
 of the individuals. If someone doesn't have good

358
00:24:21,990 --> 00:24:26,520
 relationships with other people, then you might

359
00:24:26,520 --> 00:24:32,330
 want to assume that that person has not a very good

360
00:24:32,330 --> 00:24:39,080
 meditation practice. If someone is constantly

361
00:24:39,080 --> 00:24:46,260
 causing friction and strife in their family, arguing and b

362
00:24:46,260 --> 00:24:49,880
ickering and backbiting and so on,

363
00:24:49,880 --> 00:24:58,010
 and jealous and stingy and so on. With their relatives,

364
00:24:58,010 --> 00:25:00,760
 with their husbands who fight with

365
00:25:00,760 --> 00:25:05,370
 wives and parents who fight with children and employers who

366
00:25:05,370 --> 00:25:07,840
 scold their, treat their employees

367
00:25:07,840 --> 00:25:12,080
 poorly, employees who cheat their employers, friends who

368
00:25:12,080 --> 00:25:16,440
 backbite and gossip and so on. All

369
00:25:16,440 --> 00:25:22,560
 of this is because of a lack of mental development. And so

370
00:25:22,560 --> 00:25:25,720
 the important point I want to make is that

371
00:25:25,720 --> 00:25:29,740
 we can't separate our meditation practice from our life. A

372
00:25:29,740 --> 00:25:31,680
 beginner meditator will often want

373
00:25:31,680 --> 00:25:35,870
 to discard the whole world and give up all of their

374
00:25:35,870 --> 00:25:39,320
 relationships. So when a person comes to

375
00:25:39,320 --> 00:25:41,660
 bother them in their meditation, they might even yell at

376
00:25:41,660 --> 00:25:44,080
 them or scold them, "I'm trying to meditate,

377
00:25:44,080 --> 00:25:50,250
 why are you bothering me?" And this can go on for some time

378
00:25:50,250 --> 00:25:52,840
 until they finally realize that

379
00:25:52,840 --> 00:25:57,200
 relationships, as I said, should not be seen as an obstacle

380
00:25:57,200 --> 00:25:59,360
 to the practice. They should be seen as

381
00:25:59,360 --> 00:26:01,850
 the practice. When someone comes to you, the Buddha was

382
00:26:01,850 --> 00:26:05,240
 clear about this. He said, "When people come

383
00:26:05,240 --> 00:26:07,820
 to me, I think what can I do to teach them that will make

384
00:26:07,820 --> 00:26:11,480
 them go away the quickest?" And so when

385
00:26:11,480 --> 00:26:15,210
 we hear this, we think, "Well, just tell them to go away."

386
00:26:15,210 --> 00:26:17,600
 But it actually doesn't work that way.

387
00:26:17,600 --> 00:26:22,700
 We've all tried that. As meditators, we've all tried to

388
00:26:22,700 --> 00:26:26,000
 push people away. And we think, "Well,

389
00:26:26,000 --> 00:26:29,860
 that's the way. Get rid of them. Tell them, "Look, if you

390
00:26:29,860 --> 00:26:33,080
 don't like meditating, I don't want to have

391
00:26:33,080 --> 00:26:38,080
 anything to do with you." But relationships have to be

392
00:26:38,080 --> 00:26:41,720
 solved. There are not. The fact that there is

393
00:26:41,720 --> 00:26:46,590
 a relationship with parents or relatives or anyone is

394
00:26:46,590 --> 00:26:49,000
 because of some past karma that we have with

395
00:26:49,000 --> 00:26:52,410
 them. So while it might be possible for us to go off in the

396
00:26:52,410 --> 00:26:54,600
 forest and send loving kindness and

397
00:26:54,600 --> 00:26:58,960
 never be bothered by anyone, if it happens that in the

398
00:26:58,960 --> 00:27:01,880
 course of our quest to find such peace,

399
00:27:01,880 --> 00:27:05,320
 we are bothered by other people, then we have to treat them

400
00:27:05,320 --> 00:27:08,720
 with dignity and respect and give them

401
00:27:08,720 --> 00:27:13,900
 the time and the support that they need. Otherwise, they'll

402
00:27:13,900 --> 00:27:17,800
 never leave us. I've even tried this with

403
00:27:17,800 --> 00:27:20,620
 evil people. There was one monk who was always causing

404
00:27:20,620 --> 00:27:22,520
 problems and I tried to just shut him

405
00:27:22,520 --> 00:27:27,690
 off and it just fueled the fire. It's like in India when

406
00:27:27,690 --> 00:27:30,120
 you go to Bodh Gaya or you go to these

407
00:27:30,120 --> 00:27:33,490
 Buddhist places and the beggars come. And the worst thing

408
00:27:33,490 --> 00:27:35,720
 you can do besides giving them something is

409
00:27:35,720 --> 00:27:39,690
 to say no. If you give something, you get a thousand of

410
00:27:39,690 --> 00:27:42,000
 them swarming and then you're in

411
00:27:42,000 --> 00:27:46,260
 real trouble. But if you say no, no, no, then they continue

412
00:27:46,260 --> 00:27:48,960
 to pester you. This is the way of people.

413
00:27:48,960 --> 00:27:53,560
 It's only by being patient and kind. So in India, the

414
00:27:53,560 --> 00:27:55,960
 example of India is a good one, I think,

415
00:27:55,960 --> 00:27:58,650
 because there's such greed in these even these little kids

416
00:27:58,650 --> 00:28:01,720
 who are actually not without,

417
00:28:01,720 --> 00:28:03,590
 they're going to school and they have a little bit of money

418
00:28:03,590 --> 00:28:06,680
, I think. But they found how easy it is

419
00:28:06,680 --> 00:28:11,750
 to get money from dumb tourists. So they come up and they

420
00:28:11,750 --> 00:28:15,680
 ask you for money and money. And many

421
00:28:15,680 --> 00:28:19,270
 people just say no, no, no, and they never stop because for

422
00:28:19,270 --> 00:28:21,280
 them it's a game or else it's serious

423
00:28:21,280 --> 00:28:23,420
 and they really do need the money. But what I found was

424
00:28:23,420 --> 00:28:25,200
 that when you just give them a big hug and

425
00:28:25,200 --> 00:28:27,790
 say, "What's your name?" Learn a little bit of their

426
00:28:27,790 --> 00:28:30,120
 language even. And suddenly they're a totally

427
00:28:30,120 --> 00:28:32,630
 different person and they say what their name is and you

428
00:28:32,630 --> 00:28:34,520
 ask them what they're studying in school

429
00:28:34,520 --> 00:28:38,180
 and so on. And then they go away. You like you untied the

430
00:28:38,180 --> 00:28:40,320
 knot and there was never a problem in

431
00:28:40,320 --> 00:28:43,600
 the first place. It's kind of like they talk about aikido

432
00:28:43,600 --> 00:28:45,840
 or jijitsu or these kind of ways of

433
00:28:45,840 --> 00:28:51,760
 redirecting energy. Anyway, relationships are an important

434
00:28:51,760 --> 00:28:54,360
 part of our practice and we all have

435
00:28:54,360 --> 00:28:56,610
 them even as monastics. At the very least, we have a

436
00:28:56,610 --> 00:28:58,680
 relationship with our teachers or with our

437
00:28:58,680 --> 00:29:01,560
 students and these relationships are very important and

438
00:29:01,560 --> 00:29:02,640
 should be treated as.

