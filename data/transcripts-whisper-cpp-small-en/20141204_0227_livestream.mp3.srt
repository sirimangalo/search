1
00:00:00,000 --> 00:00:09,620
 Okay, good morning. I'm broadcasting today from Chiang Mai

2
00:00:09,620 --> 00:00:13,240
 city. So the sounds here are

3
00:00:13,240 --> 00:00:18,640
 a little bit different. We still have the birds and we

4
00:00:18,640 --> 00:00:22,600
 still have the trees and the

5
00:00:22,600 --> 00:00:29,580
 leaves but competing with the sound of the motorcycles and

6
00:00:29,580 --> 00:00:33,840
 the cars and people. Which

7
00:00:33,840 --> 00:00:40,930
 is fine. It's all just sound. This sound, that sound. This

8
00:00:40,930 --> 00:00:47,200
 morning we were talking,

9
00:00:47,200 --> 00:00:53,560
 I was talking with the monks here and we were talking about

10
00:00:53,560 --> 00:00:59,840
 some of the morality problems

11
00:00:59,840 --> 00:01:14,170
 that go on in monasteries. Which is interesting because it

12
00:01:14,170 --> 00:01:16,720
 was my intention to talk this morning

13
00:01:16,720 --> 00:01:26,240
 about on the radio or here on the internet about morality.

14
00:01:26,240 --> 00:01:27,320
 I believe somewhere the Buddha

15
00:01:27,320 --> 00:01:31,210
 said, "Si, Lang, Lo, Ke, Anu, Tarang." I think I'm pretty

16
00:01:31,210 --> 00:01:33,360
 sure it was the Buddha. We

17
00:01:33,360 --> 00:01:38,290
 have in the Pali anyway, "Si, Lang, yeah." Maybe it's in

18
00:01:38,290 --> 00:01:41,840
 the Jataka or something. "Si,

19
00:01:41,840 --> 00:01:47,520
 Lang, Lo, Ke, Anu, Tarang." Morality is Anutra. "Utra"

20
00:01:47,520 --> 00:01:50,440
 means has something greater than it.

21
00:01:50,440 --> 00:01:54,280
 "Anu, Tarang" means has nothing greater than it. "Lo, Ke"

22
00:01:54,280 --> 00:01:58,800
 in the world. "Si, Lang," morality

23
00:01:58,800 --> 00:02:02,930
 has nothing greater than it in itself in the world. There's

24
00:02:02,930 --> 00:02:05,120
 nothing greater than morality

25
00:02:05,120 --> 00:02:12,120
 in the world. Because in the world we're talking about,

26
00:02:12,120 --> 00:02:15,000
 there's different, "Loka" can refer

27
00:02:15,000 --> 00:02:19,250
 to different things, but in this case it most likely refers

28
00:02:19,250 --> 00:02:21,600
 to "Sattaloka." "Sattaloka"

29
00:02:21,600 --> 00:02:32,260
 means the world of beings, world in terms of the realms

30
00:02:32,260 --> 00:02:38,200
 that beings exist in. In the

31
00:02:38,200 --> 00:02:42,010
 realm of beings you're dealing with relationships and

32
00:02:42,010 --> 00:02:45,200
 interactions between beings. So morality

33
00:02:45,200 --> 00:02:51,910
 refers to actions and speech which are the most important

34
00:02:51,910 --> 00:02:59,080
 in our relationships. One thing

35
00:02:59,080 --> 00:03:04,070
 all governments and all structures, organizations have to

36
00:03:04,070 --> 00:03:07,600
 consider is morality. Really the first

37
00:03:07,600 --> 00:03:14,980
 thing and most important thing that any organization should

38
00:03:14,980 --> 00:03:19,720
 consider is ethics, morality, how to

39
00:03:19,720 --> 00:03:25,270
 be good to each other, how to not harm one another, how to

40
00:03:25,270 --> 00:03:28,360
 not create chaos. And people

41
00:03:28,360 --> 00:03:34,180
 who have morality are special because of their moral and

42
00:03:34,180 --> 00:03:38,720
 ethical bent, leads them to be surrounded

43
00:03:38,720 --> 00:03:42,710
 by good people, leads them to cultivate sort of a

44
00:03:42,710 --> 00:03:46,160
 protection because people will protect

45
00:03:46,160 --> 00:04:00,760
 them and support them and defend them from physical and

46
00:04:00,760 --> 00:04:06,840
 verbal harm. So morality is really,

47
00:04:06,840 --> 00:04:11,480
 it's all you need to live in peace in this world. It's all

48
00:04:11,480 --> 00:04:13,520
 we need in the world to live

49
00:04:13,520 --> 00:04:18,120
 in peace if we have morality. Everything else should fall

50
00:04:18,120 --> 00:04:20,600
 into place because we wouldn't

51
00:04:20,600 --> 00:04:30,430
 have people stealing or hoarding. It would lead to absence

52
00:04:30,430 --> 00:04:36,120
 of corruption. People wouldn't

53
00:04:36,120 --> 00:04:42,830
 take more than their share or take that much. Others

54
00:04:42,830 --> 00:04:47,520
 deserve. They wouldn't hurt or put

55
00:04:47,520 --> 00:04:55,010
 each other in danger. The catch here of course is that

56
00:04:55,010 --> 00:04:59,160
 morality relies upon something. Morality

57
00:04:59,160 --> 00:05:03,080
 isn't something that you can just create in the world. In

58
00:05:03,080 --> 00:05:05,160
 fact the interesting thing is

59
00:05:05,160 --> 00:05:11,400
 morality actually relies upon wisdom. You need wisdom to

60
00:05:11,400 --> 00:05:14,600
 have morality. A person has

61
00:05:14,600 --> 00:05:18,360
 to understand what is right and what is wrong and why it's

62
00:05:18,360 --> 00:05:20,480
 right and why it's wrong. The

63
00:05:20,480 --> 00:05:25,530
 problems with acting immoral. Problems with acting immoral,

64
00:05:25,530 --> 00:05:27,080
 this is a big part of what

65
00:05:27,080 --> 00:05:30,360
 we mean by wisdom and why we say wisdom instead of

66
00:05:30,360 --> 00:05:33,440
 intelligence because morality doesn't rely

67
00:05:33,440 --> 00:05:39,480
 upon book knowledge. It relies on something most of us

68
00:05:39,480 --> 00:05:43,280
 vaguely understand or imprecisely

69
00:05:43,280 --> 00:05:48,380
 understand as wisdom. They call someone wise when they know

70
00:05:48,380 --> 00:05:50,680
 what's right and not just from

71
00:05:50,680 --> 00:05:55,430
 book knowledge but they really have wisdom. They really

72
00:05:55,430 --> 00:05:57,920
 know when they can explain and

73
00:05:57,920 --> 00:06:02,460
 when it's from the heart. So it's a tough thing actually to

74
00:06:02,460 --> 00:06:04,560
 be truly moral. You need

75
00:06:04,560 --> 00:06:09,450
 to be a developed person. You need to have cultivated

76
00:06:09,450 --> 00:06:11,280
 wisdom. And then the problem is

77
00:06:11,280 --> 00:06:16,690
 that wisdom itself depends on something. You can't just

78
00:06:16,690 --> 00:06:20,200
 cultivate wisdom. Wisdom depends

79
00:06:20,200 --> 00:06:26,120
 on concentration. Focus. Maybe not concentration so much

80
00:06:26,120 --> 00:06:30,000
 but focus. And you have to understand,

81
00:06:30,000 --> 00:06:34,270
 think deeply about the word focus. It doesn't just mean to

82
00:06:34,270 --> 00:06:36,760
 grasp something and not let go.

83
00:06:36,760 --> 00:06:44,240
 Focus means to have a clear picture. To not have a blurry

84
00:06:44,240 --> 00:06:48,800
 mind. Not have a fuzzy picture,

85
00:06:48,800 --> 00:06:56,410
 a fuzzy mind. To sharpen the mind. To straighten the mind.

86
00:06:56,410 --> 00:07:01,520
 To clear the lens of the mind. So

87
00:07:01,520 --> 00:07:09,650
 we're not partial or ignorant or arrogant or any of the

88
00:07:09,650 --> 00:07:13,320
 many things. And this is required

89
00:07:13,320 --> 00:07:16,290
 to bring wisdom because only when your mind is clear can

90
00:07:16,290 --> 00:07:18,080
 you understand these things.

91
00:07:18,080 --> 00:07:22,370
 What is right and what is wrong. What is good, what is bad.

92
00:07:22,370 --> 00:07:24,520
 What is a benefit, what is not

93
00:07:24,520 --> 00:07:33,930
 a benefit. What is a detriment. But then the problem comes

94
00:07:33,930 --> 00:07:36,600
 full circle because focus depends

95
00:07:36,600 --> 00:07:41,510
 upon something. Focus doesn't come into being by itself.

96
00:07:41,510 --> 00:07:44,040
 Focus depends on what depends on

97
00:07:44,040 --> 00:07:48,620
 morality. So you have this problem and you find that these

98
00:07:48,620 --> 00:07:51,000
 three things actually depend

99
00:07:51,000 --> 00:07:57,890
 on each other. They arise based on each other. So what do

100
00:07:57,890 --> 00:07:59,840
 you do? How do you bring about

101
00:07:59,840 --> 00:08:05,030
 one or the other? It's actually not a problem in the way I

102
00:08:05,030 --> 00:08:07,240
 stated it. It just seems like

103
00:08:07,240 --> 00:08:10,840
 a problem so this is a question that comes up. But the

104
00:08:10,840 --> 00:08:12,560
 truth is the Eightfold Noble Path

105
00:08:12,560 --> 00:08:16,070
 is these three things or the three trainings they

106
00:08:16,070 --> 00:08:19,120
 compromise. They make up the Eightfold

107
00:08:19,120 --> 00:08:25,160
 Noble Path. If you know the Eightfold Noble Path the first

108
00:08:25,160 --> 00:08:28,200
 two are wisdom, the right view

109
00:08:28,200 --> 00:08:35,280
 and right thought. The next three are morality, right

110
00:08:35,280 --> 00:08:40,720
 speech, right action, right livelihood.

111
00:08:40,720 --> 00:08:48,570
 And the last three are wisdom, our concentration, right

112
00:08:48,570 --> 00:08:57,520
 effort, right mindfulness and right concentration.

113
00:08:57,520 --> 00:09:00,680
 And they go in a circle. These eight things can be made in

114
00:09:00,680 --> 00:09:02,400
 a circle because once you have

115
00:09:02,400 --> 00:09:05,990
 right concentration it improves your view, right focus. It

116
00:09:05,990 --> 00:09:08,080
 improves your view which improves

117
00:09:08,080 --> 00:09:12,780
 your thoughts and so on. So the question is where do you

118
00:09:12,780 --> 00:09:15,520
 start? The answer is actually

119
00:09:15,520 --> 00:09:20,130
 you can start anywhere. In truth, if the Eightfold Noble

120
00:09:20,130 --> 00:09:23,120
 Path is like a wheel then the practice

121
00:09:23,120 --> 00:09:28,500
 is like one of those, you know in the olden days children

122
00:09:28,500 --> 00:09:31,120
 would take a stick and they

123
00:09:31,120 --> 00:09:36,050
 would hit the wheel, right hit the wheel and make it turn

124
00:09:36,050 --> 00:09:38,520
 and this was a game they'd run

125
00:09:38,520 --> 00:09:42,040
 along after the wheel. The Eightfold Noble Path is like

126
00:09:42,040 --> 00:09:44,080
 that. It doesn't matter where

127
00:09:44,080 --> 00:09:48,240
 you hit it, you start it turning. The important thing is to

128
00:09:48,240 --> 00:09:50,480
 start it turning, to start the

129
00:09:50,480 --> 00:09:54,050
 wheel turning. Otherwise it won't go anywhere. But once the

130
00:09:54,050 --> 00:09:57,720
 Eightfold Noble Path starts turning,

131
00:09:57,720 --> 00:10:03,200
 faster and faster and faster. This is called Pupanga Manga.

132
00:10:03,200 --> 00:10:09,400
 Pupanga Manga means part and

133
00:10:09,400 --> 00:10:14,720
 Puba means the prior or previous. So this is the precursor

134
00:10:14,720 --> 00:10:18,520
 path, the part of the path that

135
00:10:18,520 --> 00:10:23,170
 comes before the Noble Path. So to cultivate the Noble Path

136
00:10:23,170 --> 00:10:24,640
 you have to do what is called

137
00:10:24,640 --> 00:10:29,580
 Pupanga Manga. Pupanga Manga just means speeding up the

138
00:10:29,580 --> 00:10:32,920
 wheel until it gets going so perfectly

139
00:10:32,920 --> 00:10:36,840
 that we call it the Noble Path and that moment when it's

140
00:10:36,840 --> 00:10:39,440
 just right, just perfect then it's

141
00:10:39,440 --> 00:10:50,190
 able to cut the defilements. But this means that to start

142
00:10:50,190 --> 00:10:52,240
 you start anywhere. It doesn't

143
00:10:52,240 --> 00:10:57,320
 matter which place you start because the whole wheel turns.

144
00:10:57,320 --> 00:10:58,280
 Another way of looking at it

145
00:10:58,280 --> 00:11:00,750
 is you have to turn the whole wheel. You can't just pick

146
00:11:00,750 --> 00:11:02,240
 one or the other. You can't say

147
00:11:02,240 --> 00:11:04,770
 today I'm going to work on right speech and only right

148
00:11:04,770 --> 00:11:06,440
 speech or today I'm going to work

149
00:11:06,440 --> 00:11:09,510
 on right effort and only right effort. It's actually much

150
00:11:09,510 --> 00:11:11,160
 more complicated than that.

151
00:11:11,160 --> 00:11:14,030
 If you look at some of the Buddha's deep teachings on the

152
00:11:14,030 --> 00:11:16,200
 Eightfold Noble Path, they talk about

153
00:11:16,200 --> 00:11:23,100
 different parts surrounding different parts and what is the

154
00:11:23,100 --> 00:11:25,800
 basis. But the two places

155
00:11:25,800 --> 00:11:33,480
 where it seems that the Buddha placed the most emphasis on

156
00:11:33,480 --> 00:11:37,400
 starting on are right view,

157
00:11:37,400 --> 00:11:44,530
 right view and right mindfulness. Right view you need

158
00:11:44,530 --> 00:11:48,000
 because that gives you an idea of

159
00:11:48,000 --> 00:11:52,760
 morality. So you have right view and then morality. So

160
00:11:52,760 --> 00:11:55,080
 right view means you have to

161
00:11:55,080 --> 00:11:59,390
 learn something first. You have to get a right idea whether

162
00:11:59,390 --> 00:12:01,440
 it's in the beginning just from

163
00:12:01,440 --> 00:12:07,940
 listening. Usually when a person comes to meditate for the

164
00:12:07,940 --> 00:12:11,440
 first time you have to explain and

165
00:12:11,440 --> 00:12:14,870
 sometimes answer their questions. Even once in a while you

166
00:12:14,870 --> 00:12:16,480
 have to debate or argue with

167
00:12:16,480 --> 00:12:21,990
 them until they can straighten out their view because

168
00:12:21,990 --> 00:12:25,240
 otherwise when they practice they'll

169
00:12:25,240 --> 00:12:30,240
 practice wrong. Their practice will be fruitless, maybe

170
00:12:30,240 --> 00:12:33,720
 even harmful because without right view

171
00:12:33,720 --> 00:12:39,160
 it's not right practice. Do things that they shouldn't do,

172
00:12:39,160 --> 00:12:41,440
 thinking that's what they should

173
00:12:41,440 --> 00:12:43,740
 do, they won't do things that they should do, thinking that

174
00:12:43,740 --> 00:12:44,920
's something they shouldn't

175
00:12:44,920 --> 00:12:52,100
 do. That kind of thing. And right mindfulness because

176
00:12:52,100 --> 00:12:53,480
 mindfulness really cuts through the

177
00:12:53,480 --> 00:12:58,200
 whole hateful noble path. If you have mindfulness then it

178
00:12:58,200 --> 00:13:00,920
 creates a different type of right

179
00:13:00,920 --> 00:13:05,560
 view. It creates a deeper right view in the sense of really

180
00:13:05,560 --> 00:13:07,880
 understanding what is right

181
00:13:07,880 --> 00:13:12,780
 and what is wrong. Really understanding, seeing the Four

182
00:13:12,780 --> 00:13:15,680
 Noble Truths for yourself. It creates

183
00:13:15,680 --> 00:13:18,830
 morality because you're aware of what you're doing, what

184
00:13:18,830 --> 00:13:23,000
 you're saying. It creates concentration.

185
00:13:23,000 --> 00:13:27,160
 So often the Buddha would focus on mindfulness as though

186
00:13:27,160 --> 00:13:30,360
 you could practice mindfulness first.

187
00:13:30,360 --> 00:13:33,820
 The point is when you're really mindful there's not an

188
00:13:33,820 --> 00:13:35,920
 opportunity to be immoral and there's

189
00:13:35,920 --> 00:13:43,040
 no chance for the arising of wrong view if you really are

190
00:13:43,040 --> 00:13:45,440
 mindful. But other times it's

191
00:13:45,440 --> 00:13:48,700
 even not even so clear because many times the Buddha did

192
00:13:48,700 --> 00:13:51,240
 just say to start with morality.

193
00:13:51,240 --> 00:13:55,840
 You know? And quite often we talk about starting with

194
00:13:55,840 --> 00:13:59,640
 morality before you practice meditation,

195
00:13:59,640 --> 00:14:04,670
 practice morality. So in his teachings he emphasized, seems

196
00:14:04,670 --> 00:14:06,720
 he emphasized right view

197
00:14:06,720 --> 00:14:11,350
 and right mindfulness most often. But at the same time you

198
00:14:11,350 --> 00:14:13,760
 could start with morality. The

199
00:14:13,760 --> 00:14:18,080
 point is you have to cover them all. You have to cover the

200
00:14:18,080 --> 00:14:20,480
 whole eightfold noble path and

201
00:14:20,480 --> 00:14:25,540
 the best way to do this is to understand it as morality,

202
00:14:25,540 --> 00:14:28,520
 concentration and wisdom. Wherever

203
00:14:28,520 --> 00:14:34,570
 you start you can start anywhere because all eight come

204
00:14:34,570 --> 00:14:36,600
 together. On the other hand if

205
00:14:36,600 --> 00:14:39,270
 you're missing one of them, missing one part of it, you can

206
00:14:39,270 --> 00:14:40,720
't roll the wheel. The wheel

207
00:14:40,720 --> 00:14:48,220
 won't turn unless the wheel is complete. So there you go,

208
00:14:48,220 --> 00:14:51,520
 just some thoughts about the

209
00:14:51,520 --> 00:14:55,000
 eightfold noble path. Thoughts about morality but more

210
00:14:55,000 --> 00:14:57,080
 deeply about the eightfold noble

211
00:14:57,080 --> 00:15:03,120
 path. It's like a wheel. Some more food for thought. That's

212
00:15:03,120 --> 00:15:05,040
 the dhamma for today. Thank

213
00:15:05,040 --> 00:15:11,190
 you all for tuning in and for practicing together. May you

214
00:15:11,190 --> 00:15:16,200
 all be well and to our practice may

215
00:15:16,200 --> 00:15:23,280
 we purify our minds and attain enlightenment and act in a

216
00:15:23,280 --> 00:15:27,440
 way conducive for the happiness

217
00:15:27,440 --> 00:15:32,950
 and well-being of all beings. May all beings be happy and

218
00:15:32,950 --> 00:15:57,440
 well.

