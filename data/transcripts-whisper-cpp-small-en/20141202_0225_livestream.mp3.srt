1
00:00:00,000 --> 00:00:18,120
 Okay, we're broadcasting again.

2
00:00:18,120 --> 00:00:22,120
 Still in Jomtong this morning.

3
00:00:22,120 --> 00:00:34,560
 We run out early on alms.

4
00:00:34,560 --> 00:00:53,880
 I got my food for the day.

5
00:00:53,880 --> 00:00:58,560
 Alms is an interesting tradition.

6
00:00:58,560 --> 00:01:10,560
 People wonder why is it that monks have an alms bowl.

7
00:01:10,560 --> 00:01:21,680
 Why do we go through the village seeking out food?

8
00:01:21,680 --> 00:01:27,680
 When I was in Sri Lanka there was a man who came from

9
00:01:27,680 --> 00:01:32,560
 America who wanted to become a monk.

10
00:01:32,560 --> 00:01:33,920
 In the end he changed his mind.

11
00:01:33,920 --> 00:01:39,560
 He said he couldn't imagine ever walking on alms round.

12
00:01:39,560 --> 00:01:45,140
 After seeing the alms round it scared him away or turned

13
00:01:45,140 --> 00:01:47,680
 him off or something.

14
00:01:47,680 --> 00:02:00,880
 Because it's so foreign to our culture.

15
00:02:00,880 --> 00:02:08,010
 Becoming a monk is about leaving the world, you know,

16
00:02:08,010 --> 00:02:11,360
 giving up everything.

17
00:02:11,360 --> 00:02:18,290
 It wasn't the Buddha who came up with the idea of relying

18
00:02:18,290 --> 00:02:23,360
 upon the kindness of others.

19
00:02:23,360 --> 00:02:40,070
 But it was this dilemma for someone who wishes to leave the

20
00:02:40,070 --> 00:02:42,360
 world behind.

21
00:02:42,360 --> 00:02:50,480
 Because society has one main purpose and that's to provide

22
00:02:50,480 --> 00:02:54,360
 the necessities of life.

23
00:02:54,360 --> 00:03:00,210
 And the question is if you intend on leaving that all

24
00:03:00,210 --> 00:03:05,360
 behind, how then do you survive?

25
00:03:05,360 --> 00:03:15,940
 That is actually a basic dilemma for an ascetic or a recl

26
00:03:15,940 --> 00:03:17,360
use.

27
00:03:17,360 --> 00:03:24,360
 It's actually a dilemma for most spiritual practitioners.

28
00:03:24,360 --> 00:03:36,220
 How do you balance your need to survive with your drive to

29
00:03:36,220 --> 00:03:41,360
 become enlightened?

30
00:03:41,360 --> 00:03:52,430
 And so the Buddha's solution was to, as far as possible,

31
00:03:52,430 --> 00:03:59,360
 rely upon that which is cast away or that which is free.

32
00:03:59,360 --> 00:04:05,360
 Free in the sense of not costing anyone anything.

33
00:04:05,360 --> 00:04:16,730
 So for robes, for clothing, the idea was to take refuse,

34
00:04:16,730 --> 00:04:26,360
 discarded cloth,

35
00:04:26,360 --> 00:04:36,360
 and eat leftover food.

36
00:04:36,360 --> 00:04:52,920
 In the case of India, it was food that was designated for

37
00:04:52,920 --> 00:04:59,360
 spiritual sacrifice.

38
00:04:59,360 --> 00:05:09,360
 So people at that time were keen to support religious

39
00:05:09,360 --> 00:05:15,360
 seekers or religious teachers.

40
00:05:15,360 --> 00:05:27,360
 And so there was a supply in the sense.

41
00:05:27,360 --> 00:05:35,360
 There was this resource from the point of view of the monks

42
00:05:35,360 --> 00:05:37,360
.

43
00:05:37,360 --> 00:05:42,820
 Meaning is that there was no begging involved or no

44
00:05:42,820 --> 00:05:45,360
 coercion involved.

45
00:05:45,360 --> 00:05:53,360
 It was available. There were people who wanted to give,

46
00:05:53,360 --> 00:05:57,360
 people looking to give.

47
00:05:57,360 --> 00:06:10,360
 And so here was something that was readily available.

48
00:06:10,360 --> 00:06:14,360
 But the dilemma has a deeper significance in Buddhism.

49
00:06:14,360 --> 00:06:19,040
 This is the dilemma that, as the Buddha said, "Sabhe sattah

50
00:06:19,040 --> 00:06:21,360
 aha re tithika."

51
00:06:21,360 --> 00:06:30,360
 All beings subsist upon food, rely upon food, or exist.

52
00:06:30,360 --> 00:06:41,360
 Tithika means they stay alive, they maintain themselves.

53
00:06:41,360 --> 00:06:55,360
 Just for maintenance, just to maintain their existence.

54
00:06:55,360 --> 00:06:56,360
 They do so only with food.

55
00:06:56,360 --> 00:07:04,360
 It can only be done with food.

56
00:07:04,360 --> 00:07:13,750
 And so ordinary food actually plays a significant role in

57
00:07:13,750 --> 00:07:18,360
 Buddhism as to the other requisites.

58
00:07:18,360 --> 00:07:22,490
 The idea of reflecting before you partake of those things

59
00:07:22,490 --> 00:07:24,360
 that you can't do without.

60
00:07:24,360 --> 00:07:36,360
 Food is one of the few things that beings can't do without.

61
00:07:36,360 --> 00:07:41,420
 Because of that it becomes a focal point of our struggle to

62
00:07:41,420 --> 00:07:47,360
 free ourselves from desire,

63
00:07:47,360 --> 00:07:58,360
 for yourself from addiction, from need, from dependency.

64
00:07:58,360 --> 00:08:01,720
 I guess there are many things in life that clearly we could

65
00:08:01,720 --> 00:08:02,360
 do without,

66
00:08:02,360 --> 00:08:07,300
 and this clearly served no purpose, rather than to feed our

67
00:08:07,300 --> 00:08:09,360
 addictions.

68
00:08:09,360 --> 00:08:19,480
 The things that bring us pleasure serve really no intrinsic

69
00:08:19,480 --> 00:08:23,360
 purpose, no real purpose.

70
00:08:23,360 --> 00:08:26,580
 They don't make you happier, or more content, or more

71
00:08:26,580 --> 00:08:31,360
 peaceful, they just cultivate an addiction.

72
00:08:31,360 --> 00:08:35,360
 So these ones from a Buddhist point of view are easy to do.

73
00:08:35,360 --> 00:08:44,360
 You just stop doing them. You can't do that with food.

74
00:08:44,360 --> 00:08:59,360
 And so the Buddhist texts outline four types of food that

75
00:08:59,360 --> 00:09:04,360
 we have to come to terms with.

76
00:09:04,360 --> 00:09:14,360
 The word food, ahara, means that which brings ahara.

77
00:09:14,360 --> 00:09:24,730
 It means to bring some results that which nourishes beings

78
00:09:24,730 --> 00:09:25,360
 of all types,

79
00:09:25,360 --> 00:09:32,360
 who subsist upon different types of nourishment.

80
00:09:32,360 --> 00:09:37,770
 There are four things in this sort that make up a good base

81
00:09:37,770 --> 00:09:39,360
 for reflection,

82
00:09:39,360 --> 00:09:44,360
 something that we should reflect upon.

83
00:09:44,360 --> 00:09:53,360
 The first one is course physical nutriment.

84
00:09:53,360 --> 00:10:01,360
 And this is the food that we, as we know it.

85
00:10:01,360 --> 00:10:10,760
 When we eat, food is really the, it's an example that can

86
00:10:10,760 --> 00:10:14,360
 be extended to all things that we partake,

87
00:10:14,360 --> 00:10:18,360
 because it's the most base.

88
00:10:18,360 --> 00:10:30,850
 If we partake in food, if we can be mindful, keep our minds

89
00:10:30,850 --> 00:10:41,360
 free from attachment and aversion and delusion.

90
00:10:41,360 --> 00:10:51,830
 It's a base practice that we can extend to all the other

91
00:10:51,830 --> 00:10:56,360
 aspects of our life.

92
00:10:56,360 --> 00:11:01,360
 So when we eat, we try to just know that we're eating.

93
00:11:01,360 --> 00:11:04,060
 It seems like something a meditator shouldn't have to

94
00:11:04,060 --> 00:11:05,360
 concern themselves with.

95
00:11:05,360 --> 00:11:10,270
 It's even how basic eating is, it actually plays a

96
00:11:10,270 --> 00:11:12,360
 significant role,

97
00:11:12,360 --> 00:11:18,360
 should play a significant role, eating meditation.

98
00:11:18,360 --> 00:11:25,380
 Many meditation centers will explain the technique of

99
00:11:25,380 --> 00:11:26,360
 eating,

100
00:11:26,360 --> 00:11:32,360
 because of how basic how essential food is.

101
00:11:32,360 --> 00:11:46,360
 It also therefore becomes a huge source of great attachment

102
00:11:46,360 --> 00:11:46,360
.

103
00:11:46,360 --> 00:11:49,850
 Not just attachment to life and living, but attachment to

104
00:11:49,850 --> 00:11:54,360
 taste and health and nourishment,

105
00:11:54,360 --> 00:12:02,360
 all these things, partiality, texture.

106
00:12:02,360 --> 00:12:08,230
 Our cravings for food go a lot deeper than many of us are

107
00:12:08,230 --> 00:12:09,360
 aware.

108
00:12:09,360 --> 00:12:12,710
 It's another good thing about the Aum's Round, is that you

109
00:12:12,710 --> 00:12:13,360
're made aware of this,

110
00:12:13,360 --> 00:12:26,510
 because you no longer can rely upon a steady stream of

111
00:12:26,510 --> 00:12:31,360
 desirable food.

112
00:12:31,360 --> 00:12:38,780
 Sometimes all you get are some fruits or some sweets,

113
00:12:38,780 --> 00:12:42,360
 sometimes you might not get anything.

114
00:12:42,360 --> 00:12:47,360
 No matter what, you're no longer able to choose,

115
00:12:47,360 --> 00:12:53,520
 and so you see this very base, almost animalistic desire

116
00:12:53,520 --> 00:12:59,360
 that we have to attach for food.

117
00:12:59,360 --> 00:13:03,360
 It's quite shocking to see how strong the attachment is,

118
00:13:03,360 --> 00:13:06,640
 but when you think about it, it makes quite sense that this

119
00:13:06,640 --> 00:13:11,040
 would be one of the main things for us to have to deal with

120
00:13:11,040 --> 00:13:11,360
.

121
00:13:11,360 --> 00:13:19,780
 So we reflect before we eat, saying, "I'm not eating for

122
00:13:19,780 --> 00:13:33,360
 fun, I'm not eating for enjoyment,

123
00:13:33,360 --> 00:13:42,360
 I'm getting fat, becoming intoxicated, I'm just eating to

124
00:13:42,360 --> 00:13:43,360
 maintain my body,

125
00:13:43,360 --> 00:13:48,360
 to remove the feeling of hunger and to create new feelings,

126
00:13:48,360 --> 00:13:56,360
 of being stuffed or contented, just to live the holy life,

127
00:13:56,360 --> 00:13:59,890
 just that I might follow my spiritual path, that's why I'm

128
00:13:59,890 --> 00:14:00,360
 eating."

129
00:14:00,360 --> 00:14:04,510
 And then when we eat, we are mindful of it as just being

130
00:14:04,510 --> 00:14:07,360
 elements, being the fore elements,

131
00:14:07,360 --> 00:14:18,360
 the hardness and the softness and taste and so on.

132
00:14:18,360 --> 00:14:22,360
 We practice it as a meditation and see it as non-self,

133
00:14:22,360 --> 00:14:27,280
 see the experiences arising, which frees you completely

134
00:14:27,280 --> 00:14:31,360
 from this cycle of attachment and addiction.

135
00:14:31,360 --> 00:14:34,360
 It's quite a relief, frees you from this.

136
00:14:34,360 --> 00:14:37,360
 You see how much stress it is to be addicted to food

137
00:14:37,360 --> 00:14:41,740
 and how much stress is involved with just the simple act of

138
00:14:41,740 --> 00:14:42,360
 eating,

139
00:14:42,360 --> 00:14:51,360
 and how freeing it is to just be mindful of the food.

140
00:14:51,360 --> 00:14:53,360
 This is the first type of food.

141
00:14:53,360 --> 00:14:56,510
 The second type of food that we have to be aware of is

142
00:14:56,510 --> 00:15:02,360
 called passa, which means contact.

143
00:15:02,360 --> 00:15:09,360
 Contact refers to the contact between the body and the mind

144
00:15:09,360 --> 00:15:09,360
,

145
00:15:09,360 --> 00:15:22,620
 at the eye, the ear, the nose, the tongue, the body and the

146
00:15:22,620 --> 00:15:27,360
 heart and the mind.

147
00:15:27,360 --> 00:15:35,360
 Passa refers to the cause of experience.

148
00:15:35,360 --> 00:15:39,360
 It reminds us that reality is experience-based,

149
00:15:39,360 --> 00:15:45,250
 and it is our experiences that give rise to all of the good

150
00:15:45,250 --> 00:15:48,360
 and bad things in the world.

151
00:15:48,360 --> 00:15:58,360
 Passa is a food in that it nourishes our existence.

152
00:15:58,360 --> 00:16:03,360
 It's the base of our meditation practice, passa.

153
00:16:03,360 --> 00:16:15,360
 Seeing, hearing, smelling, tasting, feeling, thinking.

154
00:16:15,360 --> 00:16:21,200
 Everything in the universe can be condensed down to these

155
00:16:21,200 --> 00:16:22,360
 six things,

156
00:16:22,360 --> 00:16:26,360
 or just one thing, passa, contact.

157
00:16:26,360 --> 00:16:31,950
 Contact of the eye and the light and the mind and the ear

158
00:16:31,950 --> 00:16:36,360
 and the sound and the mind.

159
00:16:36,360 --> 00:16:40,360
 Come together and create experience.

160
00:16:40,360 --> 00:16:56,360
 [birds chirping]

161
00:16:56,360 --> 00:17:06,840
 The third type of food is nousanjaitana, which refers to

162
00:17:06,840 --> 00:17:08,360
 karma,

163
00:17:08,360 --> 00:17:13,800
 or what literally means the intentions or the volitions in

164
00:17:13,800 --> 00:17:15,360
 the mind,

165
00:17:15,360 --> 00:17:19,360
 and the bent of the mind, of a specific mind,

166
00:17:19,360 --> 00:17:26,320
 whether it's bent upon liking or disliking, judging or

167
00:17:26,320 --> 00:17:34,360
 simple mindfulness, simple awareness.

168
00:17:34,360 --> 00:17:42,120
 This is where real nourishment comes, because this nour

169
00:17:42,120 --> 00:17:48,360
ishes all of our pleasure and pain that we experience.

170
00:17:48,360 --> 00:17:59,020
 It nourishes all of the mental aspects of our life, the

171
00:17:59,020 --> 00:18:01,360
 good and the bad.

172
00:18:01,360 --> 00:18:06,610
 It nourishes all of our delusions and addictions and it

173
00:18:06,610 --> 00:18:09,360
 nourishes our identity,

174
00:18:09,360 --> 00:18:15,790
 it nourishes the self, it nourishes all of the conflict and

175
00:18:15,790 --> 00:18:16,360
 friction

176
00:18:16,360 --> 00:18:20,360
 and all of the harmony and love that we have in our lives.

177
00:18:20,360 --> 00:18:26,360
 All of these things are nourished by our mental intention.

178
00:18:26,360 --> 00:18:30,360
 We create our future.

179
00:18:30,360 --> 00:18:41,360
 We're creating our existence for the forever.

180
00:18:41,360 --> 00:18:45,360
 This is how we've come to where we are now.

181
00:18:45,360 --> 00:18:55,130
 The way forward is going to depend very much upon our

182
00:18:55,130 --> 00:19:01,360
 intentions, our mind state.

183
00:19:01,360 --> 00:19:04,360
 You can see a definite cause and effect relationship here.

184
00:19:04,360 --> 00:19:07,360
 When you cultivate greed, you become greedy.

185
00:19:07,360 --> 00:19:09,360
 When you cultivate anger, you become angry.

186
00:19:09,360 --> 00:19:12,360
 When you cultivate delusion, you become deluded.

187
00:19:12,360 --> 00:19:15,360
 When you cultivate love, you become loving.

188
00:19:15,360 --> 00:19:20,360
 When you cultivate mindfulness, you become mindful.

189
00:19:20,360 --> 00:19:25,360
 This is food that feeds your mind.

190
00:19:25,360 --> 00:19:28,360
 So you are what you eat.

191
00:19:28,360 --> 00:19:30,360
 Nothing is more true.

192
00:19:30,360 --> 00:19:36,360
 It is nowhere more true than in the mind.

193
00:19:36,360 --> 00:19:42,360
 So what do you partake of in your mind?

194
00:19:42,360 --> 00:19:46,360
 Just like physical food.

195
00:19:46,360 --> 00:19:51,360
 If the wrong, if your mind gets in the wrong,

196
00:19:51,360 --> 00:19:54,360
 bent, you become sick.

197
00:19:54,360 --> 00:19:57,640
 If your mind becomes sick, just as your body would become

198
00:19:57,640 --> 00:20:05,360
 sick with the wrong type of food.

199
00:20:06,360 --> 00:20:13,360
 The fourth type of food is minyana.

200
00:20:13,360 --> 00:20:21,360
 Minyana refers to the mind.

201
00:20:21,360 --> 00:20:26,360
 Because the mind comes first.

202
00:20:26,360 --> 00:20:30,870
 Without the mind, it doesn't matter what sort of, obviously

203
00:20:30,870 --> 00:20:34,360
 it doesn't matter what sort of experiences.

204
00:20:34,360 --> 00:20:42,500
 What sort of phenomena arise is the mind that is the

205
00:20:42,500 --> 00:20:44,360
 catalyst.

206
00:20:44,360 --> 00:20:51,360
 Even with contact at the eye, the ear, the nose, and so on.

207
00:20:51,360 --> 00:20:57,360
 Remember that it's the mind that is the key.

208
00:20:57,360 --> 00:21:13,360
 Mano bhagamadam, the mind comes first.

209
00:21:13,360 --> 00:21:18,360
 So in our practice we're not worried so much about our body

210
00:21:18,360 --> 00:21:18,360
.

211
00:21:18,360 --> 00:21:25,130
 We're not worried about starvation or hunger or sickness or

212
00:21:25,130 --> 00:21:27,360
 death of the body.

213
00:21:27,360 --> 00:21:30,360
 The mind is far more important.

214
00:21:30,360 --> 00:21:38,830
 Because if you feed the body then you can maintain it for

215
00:21:38,830 --> 00:21:40,360
 this life.

216
00:21:40,360 --> 00:21:48,140
 But if you feed the mind, if you give the mind proper nour

217
00:21:48,140 --> 00:21:49,360
ishment,

218
00:21:49,360 --> 00:21:59,360
 the benefits don't end with your death.

219
00:21:59,360 --> 00:22:08,360
 Feed the mind and raise so much about the body.

220
00:22:08,360 --> 00:22:20,380
 We say, I believe the Buddha said, we look after our bodies

221
00:22:20,380 --> 00:22:23,360
 so we're willing to put up,

222
00:22:23,360 --> 00:22:33,500
 we're willing to give up, sacrifice our material wealth for

223
00:22:33,500 --> 00:22:35,360
 our bodies.

224
00:22:35,360 --> 00:22:43,610
 But we're willing even to give up, sacrifice part of our

225
00:22:43,610 --> 00:22:47,360
 body to save our lives.

226
00:22:47,360 --> 00:22:52,320
 If it means giving up treasure, we're treasure to save our

227
00:22:52,320 --> 00:22:58,360
 body parts, save our organs.

228
00:22:58,360 --> 00:23:00,360
 We're willing to do that.

229
00:23:00,360 --> 00:23:06,450
 If we have to give up a part of our body to save our life,

230
00:23:06,450 --> 00:23:15,360
 we're also willing to do that.

231
00:23:15,360 --> 00:23:17,770
 What we don't usually reflect upon is that there's

232
00:23:17,770 --> 00:23:22,360
 something more worth saving than even our lives.

233
00:23:22,360 --> 00:23:29,550
 Because of our inability to see beyond this one life, we

234
00:23:29,550 --> 00:23:34,360
 take life to be sort of the most valuable.

235
00:23:34,360 --> 00:23:39,910
 But even still, there's clearly, for those who are wise and

236
00:23:39,910 --> 00:23:41,360
 introspective,

237
00:23:41,360 --> 00:23:45,460
 there's clearly something more valuable than life, and that

238
00:23:45,460 --> 00:23:47,360
's the mind.

239
00:23:47,360 --> 00:23:54,780
 Yojavasasatanti way, for someone who even lives a hundred

240
00:23:54,780 --> 00:23:55,360
 years,

241
00:23:55,360 --> 00:24:02,360
 ekahang jivitang seyo pasang utayapayana.

242
00:24:02,360 --> 00:24:07,250
 Better is it to live just one day, seeing the truth of life

243
00:24:07,250 --> 00:24:07,360
,

244
00:24:07,360 --> 00:24:15,360
 seeing the arising and ceasing phenomena.

245
00:24:15,360 --> 00:24:22,090
 Seeing the mind arise and see things as they really are, to

246
00:24:22,090 --> 00:24:25,360
 break free from delusion.

247
00:24:25,360 --> 00:24:33,280
 Better to live one day undiluted, than to live a hundred

248
00:24:33,280 --> 00:24:36,360
 days diluted.

249
00:24:36,360 --> 00:24:45,360
 Anyway, just some food for thought in this morning.

250
00:24:45,360 --> 00:24:48,880
 Try to give a little bit of dhamma every day, maybe not as

251
00:24:48,880 --> 00:24:51,360
 long as the past few,

252
00:24:51,360 --> 00:24:56,880
 but at least five or ten minutes every day, so at least a

253
00:24:56,880 --> 00:24:58,360
 good start.

254
00:24:58,360 --> 00:25:01,360
 Anyway, thanks everyone for tuning in.

255
00:25:01,360 --> 00:25:05,560
 I'm wishing you all the best in your meditation and your

256
00:25:05,560 --> 00:25:06,360
 lives. Be well.

