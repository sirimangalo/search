1
00:00:00,000 --> 00:00:15,320
 Okay, good evening everyone.

2
00:00:15,320 --> 00:00:21,000
 Again, still trying to get the audio at the proper volume.

3
00:00:21,000 --> 00:00:23,320
 Hopefully it's better tonight.

4
00:00:23,320 --> 00:00:38,320
 Let me know if it's not.

5
00:00:38,320 --> 00:00:51,770
 So tonight, the first set of dhammas in the satipatahana s

6
00:00:51,770 --> 00:00:52,320
utta,

7
00:00:52,320 --> 00:00:57,320
 when we get to the section on dhammas,

8
00:00:57,320 --> 00:01:04,320
 the first set is the hindrances.

9
00:01:04,320 --> 00:01:09,320
 And you've got to think there's a reason for that.

10
00:01:09,320 --> 00:01:12,320
 Before all of their dhammas,

11
00:01:12,320 --> 00:01:16,320
 for someone who's practicing satipatahana,

12
00:01:16,320 --> 00:01:21,320
 practicing mindfulness,

13
00:01:21,320 --> 00:01:25,320
 the first thing outside of the body,

14
00:01:25,320 --> 00:01:28,320
 the feelings, the mind,

15
00:01:28,320 --> 00:01:30,320
 is the hindrances,

16
00:01:30,320 --> 00:01:34,320
 the things that will get in your way.

17
00:01:34,320 --> 00:01:36,320
 Certainly whatever the reason,

18
00:01:36,320 --> 00:01:39,320
 the actual reason for that is,

19
00:01:39,320 --> 00:01:42,910
 it certainly is useful to give them to meditators in the

20
00:01:42,910 --> 00:01:43,320
 beginning

21
00:01:43,320 --> 00:01:51,320
 because that's when of course they're the strongest.

22
00:01:51,320 --> 00:01:58,320
 Our ordinary relationship with our emotions is problematic.

23
00:01:58,320 --> 00:02:05,320
 It's backwards really.

24
00:02:05,320 --> 00:02:10,320
 When we like something or we want something,

25
00:02:10,320 --> 00:02:16,320
 we take it as evidence or an indication

26
00:02:16,320 --> 00:02:18,320
 that we need to get something,

27
00:02:18,320 --> 00:02:29,320
 that we should obtain the object of our desire.

28
00:02:29,320 --> 00:02:34,320
 When we dislike something or are angry at something,

29
00:02:34,320 --> 00:02:49,280
 we take it as an indication that we should do whatever we

30
00:02:49,280 --> 00:02:55,320
 can to get rid of it.

31
00:02:55,320 --> 00:03:00,210
 When we are tired, we take it as an indication that we

32
00:03:00,210 --> 00:03:02,320
 should sleep.

33
00:03:02,320 --> 00:03:05,320
 When we're distracted or restless,

34
00:03:05,320 --> 00:03:09,940
 we take it as a sign that we should get up and do something

35
00:03:09,940 --> 00:03:10,320
.

36
00:03:10,320 --> 00:03:14,320
 Not even intellectually, we react.

37
00:03:14,320 --> 00:03:22,320
 Our whole being is conditioned to react in a specific way,

38
00:03:22,320 --> 00:03:24,320
 to react in this way.

39
00:03:24,320 --> 00:03:28,320
 When we are confused,

40
00:03:28,320 --> 00:03:31,530
 when we have doubt about something, we take it as an

41
00:03:31,530 --> 00:03:32,320
 indication

42
00:03:32,320 --> 00:03:39,320
 that we should stop what we're doing, reject it.

43
00:03:39,320 --> 00:03:46,520
 We live our lives trying to fit the world around our

44
00:03:46,520 --> 00:03:47,320
 expectations,

45
00:03:47,320 --> 00:03:52,320
 around our personality.

46
00:03:52,320 --> 00:03:55,260
 How many times have we made decisions based on our

47
00:03:55,260 --> 00:03:56,320
 personality,

48
00:03:56,320 --> 00:03:59,320
 based on our likes and dislikes?

49
00:03:59,320 --> 00:04:01,320
 We think that's natural, right?

50
00:04:01,320 --> 00:04:05,320
 If you don't like something, why would you seek it out?

51
00:04:05,320 --> 00:04:11,400
 If you like something, of course, that's where you should

52
00:04:11,400 --> 00:04:15,320
 focus your efforts.

53
00:04:15,320 --> 00:04:21,890
 Unfortunately, the results of this are not as positive as

54
00:04:21,890 --> 00:04:25,320
 we might think

55
00:04:25,320 --> 00:04:30,320
 when you chase after what you want, what you like.

56
00:04:30,320 --> 00:04:33,320
 You find yourself wound tighter and tighter,

57
00:04:33,320 --> 00:04:36,320
 bound tighter and tighter to that,

58
00:04:36,320 --> 00:04:40,320
 or those things that you like.

59
00:04:40,320 --> 00:04:43,320
 And your happiness becomes more and more dependent on them.

60
00:04:43,320 --> 00:04:46,320
 This is how the addiction cycle works.

61
00:04:46,320 --> 00:04:49,960
 When we're angry, averse to things, we become vulnerable to

62
00:04:49,960 --> 00:04:50,320
 them,

63
00:04:50,320 --> 00:04:57,310
 more and more averse, more and more fragile or vulnerable

64
00:04:57,310 --> 00:05:00,320
 to their arising,

65
00:05:00,320 --> 00:05:04,320
 more and more susceptible to anger, disappointment,

66
00:05:04,320 --> 00:05:12,320
 displeasure when they arise, or when they can't be removed.

67
00:05:12,320 --> 00:05:16,320
 When we're tired, if we just sleep whenever we're tired,

68
00:05:16,320 --> 00:05:21,030
 contrary to popular opinion, that doesn't actually give you

69
00:05:21,030 --> 00:05:22,320
 more energy.

70
00:05:22,320 --> 00:05:24,830
 There's a real problem with this for meditators who come to

71
00:05:24,830 --> 00:05:25,320
 practice

72
00:05:25,320 --> 00:05:30,900
 because psychologically they have this idea that if you're

73
00:05:30,900 --> 00:05:32,320
 tired you should sleep,

74
00:05:32,320 --> 00:05:37,320
 and that somehow makes you less of a tired person.

75
00:05:37,320 --> 00:05:41,320
 And in fact, it's somewhat the opposite.

76
00:05:41,320 --> 00:05:44,320
 The more quick you are to give in to sleep,

77
00:05:44,320 --> 00:05:50,320
 the more tired you become as an individual.

78
00:05:50,320 --> 00:05:54,320
 To some extent, of course, depending on your workload,

79
00:05:54,320 --> 00:05:57,320
 your body does need sleep.

80
00:05:57,320 --> 00:06:03,320
 But not to get tired, and if you're very, very mindful,

81
00:06:03,320 --> 00:06:07,320
 and you're far less sleep, and you're far less susceptible

82
00:06:07,320 --> 00:06:09,320
 to drowsiness

83
00:06:09,320 --> 00:06:16,320
 than most people.

84
00:06:16,320 --> 00:06:19,270
 If you're restless and you just get up and go, you become

85
00:06:19,270 --> 00:06:20,320
 restless,

86
00:06:20,320 --> 00:06:24,320
 you become distracted as an individual,

87
00:06:24,320 --> 00:06:27,320
 reacting, reacting to our emotions,

88
00:06:27,320 --> 00:06:29,320
 trying to fit the world around our emotions

89
00:06:29,320 --> 00:06:38,320
 rather than learn to have our emotions fit the world.

90
00:06:38,320 --> 00:06:42,320
 And to see how our emotions are really the biggest reason

91
00:06:42,320 --> 00:06:42,320
 for us

92
00:06:42,320 --> 00:06:45,320
 not being able to fit into the world properly,

93
00:06:45,320 --> 00:06:50,320
 or without stress or suffering.

94
00:06:50,320 --> 00:06:54,320
 When we have doubt or confusion for always doubting,

95
00:06:54,320 --> 00:06:56,320
 then we just become someone who doubts everything.

96
00:06:56,320 --> 00:07:00,320
 We can never accomplish anything of any great significance

97
00:07:00,320 --> 00:07:03,320
 because the habit of doubt comes up.

98
00:07:03,320 --> 00:07:10,320
 And unless we're able to see quick results,

99
00:07:10,320 --> 00:07:14,320
 well, even if we are, in fact, we find ourselves doubting

100
00:07:14,320 --> 00:07:18,100
 even those things that are right in front of us clearly

101
00:07:18,100 --> 00:07:20,320
 observable.

102
00:07:20,320 --> 00:07:22,320
 This happens for meditators.

103
00:07:22,320 --> 00:07:24,800
 They'll come, they'll get good results, and then they'll

104
00:07:24,800 --> 00:07:25,320
 start doubting.

105
00:07:25,320 --> 00:07:27,960
 One day they'll have great results, and be sure of the

106
00:07:27,960 --> 00:07:28,320
 practice.

107
00:07:28,320 --> 00:07:32,160
 The next day they forget all about those results, and the

108
00:07:32,160 --> 00:07:33,320
 doubt comes back.

109
00:07:33,320 --> 00:07:45,320
 And the doubt overrides the good results.

110
00:07:45,320 --> 00:07:47,320
 So we look at these differently.

111
00:07:47,320 --> 00:07:53,310
 Most of our emotions, the ones mentioned anyway, are hindr

112
00:07:53,310 --> 00:07:55,320
ances.

113
00:07:55,320 --> 00:07:59,120
 The point being, there are positive emotions, there are

114
00:07:59,120 --> 00:08:00,320
 good emotions,

115
00:08:00,320 --> 00:08:03,320
 but there are also bad emotions.

116
00:08:03,320 --> 00:08:11,320
 And we don't take it that our emotions are, or should be,

117
00:08:11,320 --> 00:08:19,370
 or are the proper impetus for action, for speech, even for

118
00:08:19,370 --> 00:08:22,320
 thought.

119
00:08:22,320 --> 00:08:27,320
 We rather wish to cultivate emotions that are conducive

120
00:08:27,320 --> 00:08:36,320
 to wholesome speech, the deeds, thought.

121
00:08:36,320 --> 00:08:39,320
 And so meditation, as with all other things,

122
00:08:39,320 --> 00:08:46,480
 it's the act of retrospection or reflection, looking back

123
00:08:46,480 --> 00:08:47,320
 on yourself

124
00:08:47,320 --> 00:08:53,320
 rather than allowing the emotions to guide us.

125
00:08:53,320 --> 00:08:57,110
 We guide ourselves back to the emotions, and we learn about

126
00:08:57,110 --> 00:08:58,320
 them, we study them.

127
00:08:58,320 --> 00:09:01,560
 Because there's another way that people deal with emotions,

128
00:09:01,560 --> 00:09:03,320
 they reject them,

129
00:09:03,320 --> 00:09:07,320
 they suppress them, they try to avoid them.

130
00:09:07,320 --> 00:09:12,070
 Desire's bad, okay, I'll just get really upset every time I

131
00:09:12,070 --> 00:09:13,320
 want something.

132
00:09:13,320 --> 00:09:16,320
 Anger's bad, well I'll feel really bad about that then.

133
00:09:16,320 --> 00:09:20,320
 I'll get angry at the fact that I'm angry.

134
00:09:20,320 --> 00:09:24,520
 If I'm tired, well then I'll try to force myself to stay

135
00:09:24,520 --> 00:09:25,320
 awake.

136
00:09:25,320 --> 00:09:31,320
 Tire myself out by working really hard.

137
00:09:31,320 --> 00:09:35,320
 If I'm distracted, well then I'll force myself.

138
00:09:35,320 --> 00:09:42,320
 I'll exert myself with lots of effort.

139
00:09:42,320 --> 00:09:46,040
 I'll apply the effort to suppress the effort, so that might

140
00:09:46,040 --> 00:09:46,320
 work.

141
00:09:46,320 --> 00:09:52,320
 You can somehow stop yourself from being distracted.

142
00:09:52,320 --> 00:09:55,320
 And if I have doubt, well I'll just force myself to believe

143
00:09:55,320 --> 00:09:55,320
,

144
00:09:55,320 --> 00:10:00,360
 which of course is the best way to cultivate long-term

145
00:10:00,360 --> 00:10:01,320
 doubt.

146
00:10:01,320 --> 00:10:06,320
 It doesn't help the doubt at all.

147
00:10:06,320 --> 00:10:08,950
 In the long term it just makes you realize how you never

148
00:10:08,950 --> 00:10:09,320
 really understood

149
00:10:09,320 --> 00:10:15,320
 or had any good basis for confidence.

150
00:10:15,320 --> 00:10:17,320
 It prevents you from gaining true confidence.

151
00:10:17,320 --> 00:10:20,620
 What helps you gain true confidence and helps overcome all

152
00:10:20,620 --> 00:10:21,320
 of these really,

153
00:10:21,320 --> 00:10:24,320
 is observation of them.

154
00:10:24,320 --> 00:10:29,320
 So as for the hindrances, the Buddha called them hindrances

155
00:10:29,320 --> 00:10:29,320
.

156
00:10:29,320 --> 00:10:31,320
 But like all the other things in the Satyabhata,

157
00:10:31,320 --> 00:10:33,320
 and Sutra, all the other aspects of our experience,

158
00:10:33,320 --> 00:10:35,320
 he said we should be mindful of them.

159
00:10:35,320 --> 00:10:41,920
 See how they arise, see how they see, watch them, observe

160
00:10:41,920 --> 00:10:46,320
 them, study them.

161
00:10:46,320 --> 00:10:51,190
 The mind is full of anger, you know, this is the mind full

162
00:10:51,190 --> 00:10:52,320
 of anger.

163
00:10:52,320 --> 00:10:56,840
 The mind is full of lust, passion, you know, this is the

164
00:10:56,840 --> 00:10:58,320
 mind full of passion.

165
00:10:58,320 --> 00:11:01,590
 And you know the arising, so that means you know also the

166
00:11:01,590 --> 00:11:03,320
 things that give rise to them.

167
00:11:03,320 --> 00:11:06,320
 And if you see something, that will give rise to liking,

168
00:11:06,320 --> 00:11:07,320
 something beautiful,

169
00:11:07,320 --> 00:11:10,320
 something attractive. So you know seeing as well.

170
00:11:10,320 --> 00:11:14,690
 And if you note this process, you're able to understand the

171
00:11:14,690 --> 00:11:16,320
 cause and the effect,

172
00:11:16,320 --> 00:11:21,550
 you see what it leads to, you see the nature, the

173
00:11:21,550 --> 00:11:25,320
 impression that you get from it.

174
00:11:25,320 --> 00:11:30,140
 How desire is something that clouds the mind, it colors the

175
00:11:30,140 --> 00:11:31,320
 mind.

176
00:11:31,320 --> 00:11:33,320
 Anger is something that inflames the mind.

177
00:11:33,320 --> 00:11:41,320
 Drowsiness is something that stifles the mind and so on.

178
00:11:41,320 --> 00:11:44,320
 And so we note them just like everything else.

179
00:11:44,320 --> 00:11:50,320
 The five hindrances, the five hindrances in brief, liking,

180
00:11:50,320 --> 00:11:55,320
 disliking, drowsiness, distraction and doubt.

181
00:11:55,320 --> 00:12:00,320
 These five things, as I've been talking about,

182
00:12:00,320 --> 00:12:02,920
 these are really the most important advice to give to medit

183
00:12:02,920 --> 00:12:03,320
ator,

184
00:12:03,320 --> 00:12:09,960
 important aspect of the Dhamma to focus on as a beginner

185
00:12:09,960 --> 00:12:11,320
 meditator.

186
00:12:11,320 --> 00:12:15,320
 Because in the beginning you're overwhelmed by these.

187
00:12:15,320 --> 00:12:18,170
 I mean throughout the course they'll come up, but in the

188
00:12:18,170 --> 00:12:19,320
 very beginning,

189
00:12:19,320 --> 00:12:21,320
 these are what are going to get in your way.

190
00:12:21,320 --> 00:12:25,470
 This is what a new meditator really has to be vigilant

191
00:12:25,470 --> 00:12:26,320
 about.

192
00:12:26,320 --> 00:12:29,230
 If your practice is not going well, there's only five

193
00:12:29,230 --> 00:12:30,320
 reasons really.

194
00:12:30,320 --> 00:12:35,320
 It's one of these five.

195
00:12:35,320 --> 00:12:43,320
 Want liking or wanting, disliking, which includes boredom,

196
00:12:43,320 --> 00:12:50,320
 fear, depression, sadness, drowsiness, tiredness,

197
00:12:50,320 --> 00:12:51,320
 distraction.

198
00:12:51,320 --> 00:12:56,320
 Distraction is also worry, included in here.

199
00:12:56,320 --> 00:13:02,720
 Things prevent you from focusing the mind on your work, on

200
00:13:02,720 --> 00:13:09,320
 your task, and doubt, doubt or confusion.

201
00:13:09,320 --> 00:13:12,150
 These are the only reasons why you really fail and why it's

202
00:13:12,150 --> 00:13:14,320
 hard to practice meditation.

203
00:13:14,320 --> 00:13:16,320
 It's important to look at these.

204
00:13:16,320 --> 00:13:20,320
 If these don't exist, because mindfulness can be mindful,

205
00:13:20,320 --> 00:13:22,320
 you can be mindful of anything, whatever happens.

206
00:13:22,320 --> 00:13:26,320
 You don't have to stick to some formula.

207
00:13:26,320 --> 00:13:31,320
 And so without these, you can adapt, you can be flexible

208
00:13:31,320 --> 00:13:32,320
 when experiences change,

209
00:13:32,320 --> 00:13:34,320
 you be mindful of the new experiences.

