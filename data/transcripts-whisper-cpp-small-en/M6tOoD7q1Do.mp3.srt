1
00:00:00,000 --> 00:00:03,800
 Welcome back to our study of the Dhammapada. Today we

2
00:00:03,800 --> 00:00:11,000
 continue on with verse 166, which reads as follows.

3
00:00:11,000 --> 00:00:22,010
 Ata datang parathena bahunapi na hape ye. Ata datang mabin

4
00:00:22,010 --> 00:00:27,000
 yaya sadatapasutosya.

5
00:00:27,000 --> 00:00:41,010
 Which means what is important or useful, what is good for

6
00:00:41,010 --> 00:00:44,000
 oneself.

7
00:00:44,000 --> 00:00:50,820
 One should never abandon or neglect what is good for

8
00:00:50,820 --> 00:00:55,000
 oneself in favor of what is good for someone else,

9
00:00:55,000 --> 00:01:07,000
 even though it might be much, even though it may be more.

10
00:01:07,000 --> 00:01:15,950
 Fully knowing what is good for oneself, one should be

11
00:01:15,950 --> 00:01:21,000
 intent upon one's own good.

12
00:01:21,000 --> 00:01:28,000
 Selfish, one should be selfish. It's basically it.

13
00:01:28,000 --> 00:01:31,150
 Well there's context to this first of all, but there is, we

14
00:01:31,150 --> 00:01:33,000
've talked about this before,

15
00:01:33,000 --> 00:01:37,130
 and there are interesting sort of philosophical

16
00:01:37,130 --> 00:01:40,000
 implications of this, surrounding this,

17
00:01:40,000 --> 00:01:44,000
 to understand why it might actually be the right teaching.

18
00:01:44,000 --> 00:01:48,210
 Of course we tend to think it is because it's promoted as

19
00:01:48,210 --> 00:01:51,000
 being something the Buddha taught,

20
00:01:51,000 --> 00:01:59,090
 and we have faith in him, so let's explore this together,

21
00:01:59,090 --> 00:02:04,000
 and see how it's useful for our meditation of course.

22
00:02:04,000 --> 00:02:07,810
 So the story goes, and this is a great story, it's a short

23
00:02:07,810 --> 00:02:13,000
 story as usual, but it's a memorable one,

24
00:02:13,000 --> 00:02:24,000
 one that I used recently and used before to make a point.

25
00:02:24,000 --> 00:02:29,960
 So the Buddha spent 45 years of his life teaching other

26
00:02:29,960 --> 00:02:31,000
 people,

27
00:02:31,000 --> 00:02:37,570
 and I remember from our sutta study on Saturday, that he

28
00:02:37,570 --> 00:02:39,000
 didn't want to teach,

29
00:02:39,000 --> 00:02:42,000
 he wasn't inclined to teach, but he was invited to teach,

30
00:02:42,000 --> 00:02:48,000
 and so he did, he spent 45 years tirelessly teaching.

31
00:02:48,000 --> 00:02:52,010
 That's really the interesting thing about it, he was quite

32
00:02:52,010 --> 00:02:54,000
 sort of self-absorbed,

33
00:02:54,000 --> 00:02:57,500
 probably the wrong word for it, but it's the best word we

34
00:02:57,500 --> 00:03:02,000
 have, or best sort of word in English.

35
00:03:02,000 --> 00:03:06,560
 He was very much fixed and focused on happiness really, on

36
00:03:06,560 --> 00:03:09,000
 the peace that he had attained,

37
00:03:09,000 --> 00:03:15,030
 without any interest in teaching, it just didn't, and no

38
00:03:15,030 --> 00:03:20,000
 reason occurred to him to teach.

39
00:03:20,000 --> 00:03:24,050
 And so that sort of life that he had set out for him would

40
00:03:24,050 --> 00:03:29,000
 have been quite peaceful and carefree,

41
00:03:29,000 --> 00:03:38,340
 of course, but without any requirements put on him, would

42
00:03:38,340 --> 00:03:44,000
 have been a life of living in peace and meditation.

43
00:03:44,000 --> 00:03:50,200
 And so clearly that was fine for him, that was easy to

44
00:03:50,200 --> 00:03:53,000
 understand how that would be considered to be

45
00:03:53,000 --> 00:03:57,510
 perfectly natural to incline in that direction, but then as

46
00:03:57,510 --> 00:03:59,000
 soon as he was invited to teach,

47
00:03:59,000 --> 00:04:03,570
 he inclined in a different direction, towards a life that

48
00:04:03,570 --> 00:04:06,000
 was the complete opposite.

49
00:04:06,000 --> 00:04:11,830
 And I think that says something important about the idea of

50
00:04:11,830 --> 00:04:13,000
 not caring.

51
00:04:13,000 --> 00:04:17,770
 The Buddha was such that it would have been fine either way

52
00:04:17,770 --> 00:04:21,000
, and the 45 years he did spend were,

53
00:04:21,000 --> 00:04:28,000
 in many ways, externally chaotic and stressful.

54
00:04:28,000 --> 00:04:33,440
 I mean, they must have been physically stressful on the

55
00:04:33,440 --> 00:04:34,000
 Buddha,

56
00:04:34,000 --> 00:04:37,870
 and there must have been a lot more pain and discomfort

57
00:04:37,870 --> 00:04:44,000
 physically, tiring, how tiring it must have been.

58
00:04:44,000 --> 00:04:50,920
 And that was just as fine with him, there was no preference

59
00:04:50,920 --> 00:04:53,000
 in that sense.

60
00:04:53,000 --> 00:04:58,960
 But at the end of the 45 years he was ready to leave, ready

61
00:04:58,960 --> 00:05:00,000
 to be done,

62
00:05:00,000 --> 00:05:03,900
 and he made a determination that this was it, that he wasn

63
00:05:03,900 --> 00:05:08,000
't going to try and live out his life to a hundred years,

64
00:05:08,000 --> 00:05:12,000
 about 80 years enough.

65
00:05:12,000 --> 00:05:19,500
 And so he looked at his students and his monks, female

66
00:05:19,500 --> 00:05:21,000
 monks, male monks,

67
00:05:21,000 --> 00:05:25,920
 female lay disciples, male lay disciples, they had all

68
00:05:25,920 --> 00:05:30,000
 become quite established.

69
00:05:30,000 --> 00:05:37,000
 And the Sanghos was by that time quite prominent in India,

70
00:05:37,000 --> 00:05:41,250
 and well established such that he knew it would carry his

71
00:05:41,250 --> 00:05:45,000
 teachings far into the future.

72
00:05:45,000 --> 00:05:50,070
 So he thought, "Well, I've done what was asked, so that's

73
00:05:50,070 --> 00:05:51,000
 that."

74
00:05:51,000 --> 00:05:55,410
 And he declared, he made known to his students that in four

75
00:05:55,410 --> 00:05:58,250
 months he would be passing away into Parinibhana, that

76
00:05:58,250 --> 00:05:59,000
 would be it.

77
00:05:59,000 --> 00:06:03,650
 They'd never see him again. No one would ever see him again

78
00:06:03,650 --> 00:06:04,000
.

79
00:06:04,000 --> 00:06:10,930
 He would never have any further contact with existence of

80
00:06:10,930 --> 00:06:12,000
 any sort.

81
00:06:12,000 --> 00:06:16,750
 Which was disturbing, to say the least, to many of his

82
00:06:16,750 --> 00:06:18,000
 followers.

83
00:06:18,000 --> 00:06:26,070
 Those monks who were not yet seen this truth for themselves

84
00:06:26,070 --> 00:06:32,490
 were quite disturbed and saddened by, and perturbed in the

85
00:06:32,490 --> 00:06:34,000
 sense that

86
00:06:34,000 --> 00:06:38,000
 it made them want to spend all their time with Buddha.

87
00:06:38,000 --> 00:06:41,460
 Rather than doing anything else, they thought, "Well, for

88
00:06:41,460 --> 00:06:43,980
 four months, if that's going to be it, then we'd better

89
00:06:43,980 --> 00:06:46,000
 just stay with him all the time

90
00:06:46,000 --> 00:06:52,000
 so we can learn as much as we can."

91
00:06:52,000 --> 00:06:56,360
 And so they did this. This was their thinking, "Well, if

92
00:06:56,360 --> 00:06:58,000
 the Buddha's going to pass away in four months,

93
00:06:58,000 --> 00:07:03,000
 we'd better spend all our time around him. This is it."

94
00:07:03,000 --> 00:07:06,000
 Seems reasonable, I think, no?

95
00:07:06,000 --> 00:07:14,000
 But one monk, one monk in the monk's name was Ata Datta.

96
00:07:14,000 --> 00:07:20,230
 Ata Datta means, Ata Datta, the "D" is just superfluous, it

97
00:07:20,230 --> 00:07:22,000
's not meaningful.

98
00:07:22,000 --> 00:07:29,500
 Ata means self, and Ata means benefit or meaning or purpose

99
00:07:29,500 --> 00:07:30,000
.

100
00:07:30,000 --> 00:07:35,000
 In this case, purpose or benefit, I suppose.

101
00:07:35,000 --> 00:07:39,000
 Interest, one's own interest in that sense. So Ata is self.

102
00:07:39,000 --> 00:07:42,990
 One who has one's own interest, one who looks after one's

103
00:07:42,990 --> 00:07:48,440
 own interest, or one who is selfish, self-centered, self-

104
00:07:48,440 --> 00:07:49,000
focused,

105
00:07:49,000 --> 00:07:53,000
 focused on what is good for oneself.

106
00:07:53,000 --> 00:07:57,950
 He thought to himself, "Now the Buddha's going to pass away

107
00:07:57,950 --> 00:07:59,000
 in four months.

108
00:07:59,000 --> 00:08:04,490
 I should spend all my time meditating away from the Buddha

109
00:08:04,490 --> 00:08:05,000
."

110
00:08:05,000 --> 00:08:11,110
 Well, he said it quite better than that. He said, "Ahan ch

111
00:08:11,110 --> 00:08:22,000
am hi avita rago, I indeed am not free from passion."

112
00:08:22,000 --> 00:08:29,690
 He says, "I will put out effort for becoming an arahant

113
00:08:29,690 --> 00:08:33,720
 while the Buddha is still alive, while the Buddha is still

114
00:08:33,720 --> 00:08:34,000
 here."

115
00:08:34,000 --> 00:08:46,040
 "Dharaman nayiva arahatata ya asatarya dharaman nayiva arah

116
00:08:46,040 --> 00:08:52,000
atata ya vayas vayamisan."

117
00:08:52,000 --> 00:08:54,550
 And so he never went to say the Buddha for these four

118
00:08:54,550 --> 00:08:57,000
 months, or for some part of these four months.

119
00:08:57,000 --> 00:09:02,000
 He just spent all his time meditating.

120
00:09:02,000 --> 00:09:05,160
 And the other monks who woke up early in the morning and

121
00:09:05,160 --> 00:09:09,540
 immediately went to the Buddha's kutti to wait for him to

122
00:09:09,540 --> 00:09:13,000
 come out and stay with him and learn from him,

123
00:09:13,000 --> 00:09:16,430
 they saw this and they said, "Hey, why aren't you... what's

124
00:09:16,430 --> 00:09:18,770
 wrong with you that you don't ever come and see the Buddha

125
00:09:18,770 --> 00:09:19,000
?"

126
00:09:19,000 --> 00:09:21,000
 They noticed that he never went to see the Buddha.

127
00:09:21,000 --> 00:09:23,480
 It was like he was avoiding the Buddha, in fact, whereas

128
00:09:23,480 --> 00:09:26,670
 before he would have maybe gone to pay respect or listen to

129
00:09:26,670 --> 00:09:28,000
 the Buddha's teaching,

130
00:09:28,000 --> 00:09:31,000
 he just didn't go to see the Buddha.

131
00:09:31,000 --> 00:09:33,000
 So they thought, "What's with this guy?"

132
00:09:33,000 --> 00:09:36,000
 And it looks like they dragged him to the Buddha.

133
00:09:36,000 --> 00:09:41,000
 They actually picked him up and took him to the Buddha.

134
00:09:41,000 --> 00:09:44,420
 And the Buddha said, "Why are you bringing this monk? Why

135
00:09:44,420 --> 00:09:48,000
 are you bringing him here?"

136
00:09:48,000 --> 00:09:54,340
 And they said, "Oh, one day this monk is doing this. He's

137
00:09:54,340 --> 00:09:57,000
 off. He's avoiding you."

138
00:09:57,000 --> 00:10:01,460
 And the Buddha looked at him and said, "Why are you doing

139
00:10:01,460 --> 00:10:05,000
 this? Why are you behaving in this way?"

140
00:10:05,000 --> 00:10:08,730
 And so he told them, he said, "Bhante, you have said that

141
00:10:08,730 --> 00:10:12,000
 in four months you're going to pass into Parinibhana.

142
00:10:12,000 --> 00:10:18,320
 I thought, 'As long as you're still here, I will strive for

143
00:10:18,320 --> 00:10:22,000
 the attainment of our hunch.'"

144
00:10:22,000 --> 00:10:27,580
 And the Buddha looks at him and, let's see, I've heard that

145
00:10:27,580 --> 00:10:29,000
 he actually says sadhu.

146
00:10:29,000 --> 00:10:35,000
 It's one of the few times.

147
00:10:35,000 --> 00:10:37,000
 What does it say?

148
00:10:37,000 --> 00:10:42,000
 Yeah, the Buddha satatasa sadhukarangatva.

149
00:10:42,000 --> 00:10:44,990
 So I don't know that he brought his hands up, probably not,

150
00:10:44,990 --> 00:10:48,000
 but sadhukarangatva.

151
00:10:48,000 --> 00:10:58,000
 He gave him praise, basically. He gave his approval.

152
00:10:58,000 --> 00:11:03,000
 And then he said, "Bhikkhu vayasamayi sinayo ati."

153
00:11:03,000 --> 00:11:13,000
 For who? Whoever has love for me, basically.

154
00:11:13,000 --> 00:11:17,470
 Whoever has love for me, they should act in the same way as

155
00:11:17,470 --> 00:11:21,000
 this monk who has his own benefit at heart.

156
00:11:21,000 --> 00:11:24,680
 And that's how he got the name, that's how he got the name,

157
00:11:24,680 --> 00:11:26,000
 ata-datta,

158
00:11:26,000 --> 00:11:29,360
 one who looks after their own benefit, and then he taught

159
00:11:29,360 --> 00:11:30,000
 this verse.

160
00:11:30,000 --> 00:11:36,000
 Ata-datta-nbratina and so on.

161
00:11:36,000 --> 00:11:42,000
 So there's a few lessons here.

162
00:11:42,000 --> 00:11:46,030
 The first one, because the story isn't exactly in regards

163
00:11:46,030 --> 00:11:51,240
 to choosing between one's own benefit and the benefit of

164
00:11:51,240 --> 00:11:52,000
 others.

165
00:11:52,000 --> 00:11:58,850
 What's one's own interest and what's in the interest of

166
00:11:58,850 --> 00:12:01,000
 other people.

167
00:12:01,000 --> 00:12:10,380
 It's more about choosing what is truly useful and

168
00:12:10,380 --> 00:12:21,000
 beneficial over what is actually just superficial.

169
00:12:21,000 --> 00:12:24,950
 And this sort of teaching is quite important for cultural

170
00:12:24,950 --> 00:12:26,000
 Buddhists.

171
00:12:26,000 --> 00:12:30,940
 Buddhist societies can be quite wonderful in terms of

172
00:12:30,940 --> 00:12:33,000
 supporting Buddhism,

173
00:12:33,000 --> 00:12:42,990
 providing a sort of a nest or a protection with the culture

174
00:12:42,990 --> 00:12:43,000
,

175
00:12:43,000 --> 00:12:47,560
 because it becomes a part of society and this protection of

176
00:12:47,560 --> 00:12:51,000
 Buddhism and this revering of Buddhism.

177
00:12:51,000 --> 00:12:56,720
 But it's very easy and it's a sort of a slippery slope to

178
00:12:56,720 --> 00:13:04,000
 eventually see greater importance in the cultural aspects

179
00:13:04,000 --> 00:13:09,120
 than the more what we might call religious aspects, or the

180
00:13:09,120 --> 00:13:16,000
 more important aspects to put a fine point on it.

181
00:13:16,000 --> 00:13:20,610
 And so we can see that sort of thing happening here where

182
00:13:20,610 --> 00:13:26,000
 the monks had become quite comfortable with what had become

183
00:13:26,000 --> 00:13:29,000
 what we might call Buddhism and the Buddha.

184
00:13:29,000 --> 00:13:37,280
 And so they revered him and they revered him so much that

185
00:13:37,280 --> 00:13:42,000
 they forgot what was important.

186
00:13:42,000 --> 00:13:46,540
 And so this sort of teaching, this story along with this

187
00:13:46,540 --> 00:13:51,190
 verse is very important as Buddhism grows and as we carry

188
00:13:51,190 --> 00:13:53,000
 Buddhism,

189
00:13:53,000 --> 00:13:56,180
 it's important that we carry teachings like this with us

190
00:13:56,180 --> 00:14:03,280
 that remind us what aspects of life are important and which

191
00:14:03,280 --> 00:14:06,000
 are superfluous.

192
00:14:06,000 --> 00:14:14,950
 I think it shows sort of a higher sense of purpose and that

193
00:14:14,950 --> 00:14:21,000
 we sometimes get quite carried away with appearance,

194
00:14:21,000 --> 00:14:29,680
 what is expected of us, how we want to look and how we want

195
00:14:29,680 --> 00:14:36,000
 other people to see us and how we feel obligated to others,

196
00:14:36,000 --> 00:14:40,000
 how we feel like we fit into our roles in society.

197
00:14:40,000 --> 00:14:46,110
 I mean, it speaks to a greater sort of going against the

198
00:14:46,110 --> 00:14:47,000
 stream,

199
00:14:47,000 --> 00:14:50,830
 not doing things just because that's what you think is

200
00:14:50,830 --> 00:14:52,000
 expected of you,

201
00:14:52,000 --> 00:14:56,770
 and being bold enough to really say, "No, this isn't

202
00:14:56,770 --> 00:15:02,000
 actually useful. This isn't actually beneficial."

203
00:15:02,000 --> 00:15:07,650
 Because it seems insensitive of this monk to stop caring

204
00:15:07,650 --> 00:15:14,000
 about the Buddha and stop hanging around with the Buddha.

205
00:15:14,000 --> 00:15:17,000
 On the one hand, I mean, it's not the only thing here.

206
00:15:17,000 --> 00:15:20,000
 There's another sense that these monks thought that,

207
00:15:20,000 --> 00:15:23,640
 "Hey, this is useful. It's good for us to learn all we can

208
00:15:23,640 --> 00:15:25,000
 from the Buddha."

209
00:15:25,000 --> 00:15:28,210
 When in fact the Buddha just kept telling him, "Go meditate

210
00:15:28,210 --> 00:15:30,000
, go meditate."

211
00:15:30,000 --> 00:15:33,560
 You know, like people who watch all my videos but never

212
00:15:33,560 --> 00:15:35,000
 actually meditate.

213
00:15:35,000 --> 00:15:40,000
 I've got 1,000, I've got 1,500 videos on YouTube.

214
00:15:40,000 --> 00:15:44,510
 If you've watched them all, you've watched too many. If you

215
00:15:44,510 --> 00:15:49,000
've watched half of them, you've watched too many.

216
00:15:49,000 --> 00:15:51,440
 I mean, I appreciate that people are interested in the

217
00:15:51,440 --> 00:15:52,000
 teachings,

218
00:15:52,000 --> 00:15:59,040
 but this is a good example for us, reminding us what is

219
00:15:59,040 --> 00:16:01,000
 really useful.

220
00:16:01,000 --> 00:16:07,000
 And this monk certainly found it.

221
00:16:07,000 --> 00:16:12,270
 I mean, listening to teachings is easier, makes you feel

222
00:16:12,270 --> 00:16:13,000
 good, right?

223
00:16:13,000 --> 00:16:15,750
 Meditation, you see these people here who come to do

224
00:16:15,750 --> 00:16:23,000
 meditation courses, it's very difficult.

225
00:16:23,000 --> 00:16:26,720
 But it's the difference between the conceptual and the real

226
00:16:26,720 --> 00:16:27,000
.

227
00:16:27,000 --> 00:16:31,000
 Meditation deals with the real, it deals with reality.

228
00:16:31,000 --> 00:16:38,000
 It has a power that study and listening, learning doesn't.

229
00:16:38,000 --> 00:16:42,820
 It has the power to reach into the very mechanics of

230
00:16:42,820 --> 00:16:49,000
 experience and change them and refine them.

231
00:16:49,000 --> 00:16:54,010
 Refine them simply by seeing them more clearly, not making

232
00:16:54,010 --> 00:16:57,000
 mistakes based on ignorance.

233
00:16:57,000 --> 00:17:01,580
 Seeing the mistakes that we make based on ignorance, based

234
00:17:01,580 --> 00:17:05,000
 on our imperfect understanding of experience.

235
00:17:05,000 --> 00:17:07,730
 So that's the first part, this difference between what is

236
00:17:07,730 --> 00:17:09,000
 useful and not useful.

237
00:17:09,000 --> 00:17:12,020
 The second part, of course, is this idea, beneficial the

238
00:17:12,020 --> 00:17:14,000
 weather, beneficial the self.

239
00:17:14,000 --> 00:17:19,050
 And I guess that sort of relates to the idea of what's

240
00:17:19,050 --> 00:17:21,000
 expected of you.

241
00:17:21,000 --> 00:17:24,930
 More than expected is kind of this implication that you're

242
00:17:24,930 --> 00:17:27,000
 disrespecting the Buddha.

243
00:17:27,000 --> 00:17:30,000
 You're not fulfilling your obligation.

244
00:17:30,000 --> 00:17:32,660
 And in fact there may have been a sense of obligation, okay

245
00:17:32,660 --> 00:17:35,650
, every day everybody has to go and the Buddha is going to

246
00:17:35,650 --> 00:17:38,000
 give a talk, you have to go listen, the Buddha comes out,

247
00:17:38,000 --> 00:17:41,000
 you have to go and pay respect to him, this and this.

248
00:17:41,000 --> 00:17:44,110
 If you don't go and see the Buddha, it's considered to be

249
00:17:44,110 --> 00:17:45,000
 not so good.

250
00:17:45,000 --> 00:17:47,000
 And the Buddha didn't think so.

251
00:17:47,000 --> 00:17:49,000
 The Buddha didn't agree with this.

252
00:17:49,000 --> 00:17:54,170
 He discounted the concerns of these monks and said, "Look,

253
00:17:54,170 --> 00:17:59,000
 if you love me, stop following this carcass around."

254
00:17:59,000 --> 00:18:02,000
 This one monk who followed the Buddha everywhere, he said,

255
00:18:02,000 --> 00:18:08,000
 "What are you doing following this carcass around?"

256
00:18:08,000 --> 00:18:13,900
 He stopped following the carcass, stopped following this

257
00:18:13,900 --> 00:18:19,380
 rotten putrid thing with nine holes exuding all sorts of

258
00:18:19,380 --> 00:18:24,000
 filth through nine holes and the skin as well.

259
00:18:24,000 --> 00:18:30,000
 He stopped following this bag of garbage around.

260
00:18:30,000 --> 00:18:33,000
 Stop doing what's not useful.

261
00:18:33,000 --> 00:18:40,810
 Stop buying into the expectations and these roles and these

262
00:18:40,810 --> 00:18:49,000
 artifices that we set up and ask yourself what's important.

263
00:18:49,000 --> 00:18:51,000
 I'd like to meditate but I can't.

264
00:18:51,000 --> 00:18:53,000
 This reason, that reason.

265
00:18:53,000 --> 00:18:58,000
 We're caught up in so much.

266
00:18:58,000 --> 00:19:01,130
 We get caught up in so much and we think, "Oh, I'll just

267
00:19:01,130 --> 00:19:04,890
 put meditation aside because I have all these other

268
00:19:04,890 --> 00:19:07,000
 commitments and so on."

269
00:19:07,000 --> 00:19:12,880
 We're not denouncing such things but we have to understand

270
00:19:12,880 --> 00:19:19,000
 that if you don't do it, it won't get done.

271
00:19:19,000 --> 00:19:28,950
 You don't get substitution points for being a good Buddhist

272
00:19:28,950 --> 00:19:31,000
 and so on.

273
00:19:31,000 --> 00:19:34,500
 But more interesting, especially in regards to this verse,

274
00:19:34,500 --> 00:19:36,000
 is this real question.

275
00:19:36,000 --> 00:19:41,570
 Well, is it really better or always better to look out for

276
00:19:41,570 --> 00:19:47,360
 your own interest instead of suppose teaching someone else

277
00:19:47,360 --> 00:19:49,000
 meditation?

278
00:19:49,000 --> 00:19:52,080
 At other times the Buddha has made clear that helping

279
00:19:52,080 --> 00:19:54,000
 others is helping yourself.

280
00:19:54,000 --> 00:19:56,460
 When you help yourself, you're helping others. When you

281
00:19:56,460 --> 00:19:59,000
 help others, you're helping yourself.

282
00:19:59,000 --> 00:20:05,700
 And how that works is it's a kindness. It's being nice to

283
00:20:05,700 --> 00:20:08,000
 someone. It's a cultivation of goodness to help others.

284
00:20:08,000 --> 00:20:13,000
 So that's a support for your own practice.

285
00:20:13,000 --> 00:20:23,960
 And so there's not as much of a dichotomy here or a

286
00:20:23,960 --> 00:20:25,000
 divergence here.

287
00:20:25,000 --> 00:20:30,150
 It's not as much of an either/or situation. We talk about

288
00:20:30,150 --> 00:20:33,000
 being selfish and self-centered.

289
00:20:33,000 --> 00:20:37,180
 But to be clear, it means doing what's in your own best

290
00:20:37,180 --> 00:20:42,430
 interest, which only sounds problematic until you realize

291
00:20:42,430 --> 00:20:46,000
 that helping others is helping yourself.

292
00:20:46,000 --> 00:20:50,600
 And really the point is your focus. What is your focus? Is

293
00:20:50,600 --> 00:20:54,950
 your focus to cure the sufferings of all the living beings

294
00:20:54,950 --> 00:20:56,000
 on earth?

295
00:20:56,000 --> 00:21:02,150
 Well, it's probably a problematic goal considering the

296
00:21:02,150 --> 00:21:07,000
 numbers and the types of beings out there.

297
00:21:07,000 --> 00:21:09,000
 It's a losing cause, a losing battle.

298
00:21:09,000 --> 00:21:12,600
 But if your focus and your intention is to become

299
00:21:12,600 --> 00:21:16,910
 enlightened, it really doesn't matter whether you help

300
00:21:16,910 --> 00:21:19,000
 others or help yourself.

301
00:21:19,000 --> 00:21:24,450
 If your focus and your intent in doing all these things or

302
00:21:24,450 --> 00:21:28,000
 whatever you do, the more things the better.

303
00:21:28,000 --> 00:21:33,390
 The greater your focus on helping yourself, it all becomes

304
00:21:33,390 --> 00:21:35,000
 the practice.

305
00:21:35,000 --> 00:21:40,150
 Eating becomes meditation, right? So teaching becomes, if

306
00:21:40,150 --> 00:21:49,970
 not meditation, it becomes a support for your meditation as

307
00:21:49,970 --> 00:21:54,000
 you help others.

308
00:21:54,000 --> 00:21:58,480
 And so the final thing that I would say about this is in

309
00:21:58,480 --> 00:22:03,180
 regards to paying respect to the Buddha, because the Buddha

310
00:22:03,180 --> 00:22:07,380
 also says here that someone who loves me, if they wish to

311
00:22:07,380 --> 00:22:09,000
 pay respect to me,

312
00:22:09,000 --> 00:22:17,960
 it's not through garlands and flowers and incense, they

313
00:22:17,960 --> 00:22:26,000
 should worship me through their practice of the Dhamma.

314
00:22:26,000 --> 00:22:27,230
 And this teaching of course is elsewhere, it's in the Mahap

315
00:22:27,230 --> 00:22:34,570
arin Yibana Sut, it's one of the last things the Buddha says

316
00:22:34,570 --> 00:22:42,000
, flowers, candles, incense, not really the way.

317
00:22:42,000 --> 00:22:52,540
 I thought I had more to say about that, but that's really

318
00:22:52,540 --> 00:22:56,000
 the main point.

319
00:22:56,000 --> 00:23:07,180
 Right, is that the practice which the Buddha recommends, it

320
00:23:07,180 --> 00:23:12,810
's not all these many things that we talk about in Buddhism,

321
00:23:12,810 --> 00:23:19,000
 giving charity,

322
00:23:19,000 --> 00:23:26,440
 things like communal harmony and being good to other people

323
00:23:26,440 --> 00:23:28,000
 and so on.

324
00:23:28,000 --> 00:23:33,520
 The recommendation here is, what I want to say is, this is

325
00:23:33,520 --> 00:23:36,320
 why we come to a meditation center and do a meditation

326
00:23:36,320 --> 00:23:37,000
 retreat,

327
00:23:37,000 --> 00:23:39,830
 this is explicitly the sort of thing the Buddha recommended

328
00:23:39,830 --> 00:23:46,000
, not to just be a good Buddhist or even learn to meditate.

329
00:23:46,000 --> 00:23:51,930
 Really ideally, all of the people that I teach on the

330
00:23:51,930 --> 00:23:56,320
 internet and get interested and start to pick up this type

331
00:23:56,320 --> 00:23:57,000
 of meditation,

332
00:23:57,000 --> 00:24:02,780
 ideally they find a way some day, some how to do what Ata D

333
00:24:02,780 --> 00:24:07,000
atta did, this monk who got the name Ata Datta,

334
00:24:07,000 --> 00:24:10,730
 who really understood what is to his benefit, what was the

335
00:24:10,730 --> 00:24:14,430
 greatest thing to do and went off and practiced on their

336
00:24:14,430 --> 00:24:15,000
 own.

337
00:24:15,000 --> 00:24:20,810
 I don't know if it's clear, but the idea is that we really

338
00:24:20,810 --> 00:24:24,000
 can only benefit ourselves.

339
00:24:24,000 --> 00:24:27,110
 When you talk about benefiting others, what's the best you

340
00:24:27,110 --> 00:24:28,000
 can do for them?

341
00:24:28,000 --> 00:24:32,170
 You can talk about healing their wounds or giving them food

342
00:24:32,170 --> 00:24:35,000
 to eat or money or shelter, all those things,

343
00:24:35,000 --> 00:24:42,380
 and that's useful, beneficial, but it's not nearly as

344
00:24:42,380 --> 00:24:47,100
 beneficial as them going off and practicing meditation to

345
00:24:47,100 --> 00:24:48,000
 become enlightened.

346
00:24:48,000 --> 00:24:51,210
 Obviously there's a hierarchy of needs that need to be

347
00:24:51,210 --> 00:24:55,650
 taken care of, but you talk about what has the greatest

348
00:24:55,650 --> 00:24:57,000
 benefit and the greatest result,

349
00:24:57,000 --> 00:25:00,000
 it's helping oneself.

350
00:25:00,000 --> 00:25:02,680
 And so whether you help yourself or whether you help other

351
00:25:02,680 --> 00:25:05,880
 people to help yourself, the focus always has to be on this

352
00:25:05,880 --> 00:25:09,000
, the helping of oneself.

353
00:25:09,000 --> 00:25:16,160
 And if that's your focus, that's the way to become free

354
00:25:16,160 --> 00:25:25,000
 from suffering, that's the best way to be.

355
00:25:25,000 --> 00:25:28,600
 And if you take it to its logical conclusion, it means

356
00:25:28,600 --> 00:25:33,600
 going off on your own and cultivating proper and intensive

357
00:25:33,600 --> 00:25:36,000
 meditation practice.

358
00:25:36,000 --> 00:25:40,850
 So this verse relates to our practice, for those of you who

359
00:25:40,850 --> 00:25:42,000
 are here doing the course,

360
00:25:42,000 --> 00:25:46,830
 to be encouraged that you are ata-datta, you are people who

361
00:25:46,830 --> 00:25:49,000
 know your own benefit.

362
00:25:49,000 --> 00:25:52,810
 You've done something that's very difficult, and people

363
00:25:52,810 --> 00:25:56,550
 watching here on the internet often maybe wish they could

364
00:25:56,550 --> 00:26:02,000
 do, maybe don't even feel up to doing.

365
00:26:02,000 --> 00:26:07,390
 But you found the path to truly help yourselves and to dive

366
00:26:07,390 --> 00:26:11,000
 right into what is to your own benefit.

367
00:26:11,000 --> 00:26:15,770
 It's to be commended, and it's reason why I have great

368
00:26:15,770 --> 00:26:21,000
 respect for anyone who does it, having done it myself.

369
00:26:21,000 --> 00:26:25,560
 It's a great and profound thing, so at the least you should

370
00:26:25,560 --> 00:26:29,000
 give encouragement to those of us who are here,

371
00:26:29,000 --> 00:26:33,000
 and encouragement to those who haven't come yet.

372
00:26:33,000 --> 00:26:37,190
 This is the kind of thing that makes one a true

373
00:26:37,190 --> 00:26:41,000
 practitioner of the Buddhist teaching.

374
00:26:41,000 --> 00:26:44,110
 So that's the Dhammapada for tonight. Thank you all for

375
00:26:44,110 --> 00:26:45,000
 tuning in.

