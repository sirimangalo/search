1
00:00:00,000 --> 00:00:04,070
 After he has sat down, he has sat down, if his teacher or

2
00:00:04,070 --> 00:00:08,000
 preceptor arrives while the meal is still unfinished,

3
00:00:08,000 --> 00:00:13,190
 so he sat down on a plate on the seat and then say he is

4
00:00:13,190 --> 00:00:19,200
 eating, then if a teacher or preceptor arrives or comes in,

5
00:00:19,200 --> 00:00:23,840
 it is allowable for him to get up and do the duties.

6
00:00:23,840 --> 00:00:28,370
 Actually, it is not only allowable, he should or he must

7
00:00:28,370 --> 00:00:33,720
 get up and do the duties, because we have duties to our

8
00:00:33,720 --> 00:00:36,560
 preceptors.

9
00:00:36,560 --> 00:00:39,440
 So if we are sitting and a preceptor or a teacher comes in,

10
00:00:39,440 --> 00:00:45,040
 we must stand up and greet him and then do whatever we can

11
00:00:45,040 --> 00:00:46,560
 for his comfort.

12
00:00:46,560 --> 00:00:51,500
 So if a monk is sitting and eating and at that time his

13
00:00:51,500 --> 00:00:57,520
 teacher or preceptor comes, what must he do?

14
00:00:57,520 --> 00:01:02,640
 And he is one sitting eater.

15
00:01:02,640 --> 00:01:07,440
 So he should get up and do the duty.

16
00:01:07,440 --> 00:01:15,290
 And after getting up, he must not eat again. But the elder

17
00:01:15,290 --> 00:01:18,910
 T.B. Regechula Abhaya said he should either keep his seat

18
00:01:18,910 --> 00:01:20,400
 and finish his meal,

19
00:01:20,400 --> 00:01:24,250
 or if he gets up, he should leave the rest of his meal in

20
00:01:24,250 --> 00:01:27,440
 order not to break the ascetic practice.

21
00:01:27,440 --> 00:01:33,500
 So he could do either of the two. He should ignore his duty

22
00:01:33,500 --> 00:01:39,110
 to his teachers and go on eating, or he should get up and

23
00:01:39,110 --> 00:01:47,440
 do duties to his teachers and forfeit the meal.

24
00:01:47,440 --> 00:01:51,280
 And this is one whose meal is still unfinished.

25
00:01:51,280 --> 00:01:55,510
 Therefore, he should let him do the duties, but in that

26
00:01:55,510 --> 00:01:59,120
 case, he should not eat the rest of the meal.

27
00:01:59,120 --> 00:02:13,120
 So this is what the elder said.

28
00:02:13,120 --> 00:02:21,810
 For how long would one undertake these practices? As long

29
00:02:21,810 --> 00:02:26,960
 as one wants to. There is no fixed duration of time.

30
00:02:26,960 --> 00:02:28,960
 Is this unusual?

31
00:02:28,960 --> 00:02:32,960
 Now it is very unusual.

32
00:02:32,960 --> 00:02:34,960
 Do these practices?

33
00:02:34,960 --> 00:02:36,960
 Right. Monks do not practice this much now.

34
00:02:36,960 --> 00:02:45,680
 Out of 13, the ones monks practice most is one eating at

35
00:02:45,680 --> 00:02:53,080
 one sitting and one bowl eating. We have not come to one

36
00:02:53,080 --> 00:02:54,800
 bowl eating.

37
00:02:54,800 --> 00:03:01,850
 And sometimes staying at a cemetery and sometimes under a

38
00:03:01,850 --> 00:03:06,800
 tree, but not for long.

39
00:03:06,800 --> 00:03:13,600
 Do you choose one at a time or two or three common

40
00:03:13,600 --> 00:03:15,760
 practices?

41
00:03:15,760 --> 00:03:19,070
 You can practice two or three or four. We will come to that

42
00:03:19,070 --> 00:03:19,760
 later.

43
00:03:19,760 --> 00:03:25,040
 Because it is said that if you can get a suitable place, an

44
00:03:25,040 --> 00:03:28,960
 open air place, and then close to a cemetery or any

45
00:03:28,960 --> 00:03:32,800
 cemetery, then you can practice all 13 at the same time.

46
00:03:32,800 --> 00:03:39,530
 And the elder Mahakasappa is said to be the one who

47
00:03:39,530 --> 00:03:44,800
 practices all 13 practices all through his life.

48
00:03:44,800 --> 00:03:57,480
 He is the foremost among those who undertake the ascetic

49
00:03:57,480 --> 00:04:00,800
 practices.

50
00:04:00,800 --> 00:04:06,920
 So one who is strict may not take anything more than the

51
00:04:06,920 --> 00:04:12,240
 food that he has laid his hand on, whether it is little or

52
00:04:12,240 --> 00:04:12,800
 much.

53
00:04:12,800 --> 00:04:15,920
 And if people bring him ghee, etc., thinking the elder has

54
00:04:15,920 --> 00:04:19,910
 eaten nothing, while these are allowable for the purpose of

55
00:04:19,910 --> 00:04:22,800
 medicine, they are not so for the purpose of food.

56
00:04:22,800 --> 00:04:25,120
 As medicine, they are allowable, but not as food. The

57
00:04:25,120 --> 00:04:30,040
 medium one may take more as long as the meal in the bowl is

58
00:04:30,040 --> 00:04:31,800
 not exhausted.

59
00:04:31,800 --> 00:04:37,800
 For he is called one who stops when the food is finished.

60
00:04:37,800 --> 00:04:42,060
 The mild one may eat as long as he does not get up on his

61
00:04:42,060 --> 00:04:42,800
 seat.

62
00:04:42,800 --> 00:04:46,080
 He is either one who stops with the water because he eats

63
00:04:46,080 --> 00:04:49,430
 until he takes water for washing the bowl or one who stops

64
00:04:49,430 --> 00:04:50,800
 with his session.

65
00:04:50,800 --> 00:04:56,500
 Because he eats until he gets up. So there is a joke among

66
00:04:56,500 --> 00:05:02,040
 monks that you can sit from the morning until noon and eat

67
00:05:02,040 --> 00:05:05,800
 as much as you like.

68
00:05:05,800 --> 00:05:18,570
 If you do not break your position, sitting position, then

69
00:05:18,570 --> 00:05:25,800
 you can sit from morning till noon.

70
00:05:25,800 --> 00:05:30,790
 And in the benefits, he has little affliction and little

71
00:05:30,790 --> 00:05:35,000
 sickness because he eats less. He has lightness, strength

72
00:05:35,000 --> 00:05:35,800
 and a heavy life.

73
00:05:35,800 --> 00:05:39,970
 There is no contravening rules about food that is not what

74
00:05:39,970 --> 00:05:44,800
 is left over from a meal. I'll talk about it later.

75
00:05:44,800 --> 00:05:49,200
 Craving for taste is eliminated. His life conforms to the

76
00:05:49,200 --> 00:05:51,800
 principles of fewness of wishes and so on.

77
00:05:51,800 --> 00:05:57,800
 So this is one session eater or one sitting eater.

78
00:05:57,800 --> 00:06:07,800
 The next one is one bowl eating. It's not so easy. You use

79
00:06:07,800 --> 00:06:11,800
 only one bowl when you eat.

80
00:06:11,800 --> 00:06:18,160
 So when at the time of drinking rice kruel, the bowl food

81
00:06:18,160 --> 00:06:22,800
 eater gets curry that is put in a dish.

82
00:06:22,800 --> 00:06:26,790
 He can first either eat the curry or drink the rice kruel.

83
00:06:26,790 --> 00:06:29,800
 Not both at the same time.

84
00:06:29,800 --> 00:06:35,800
 So drink rice kruel and after finishing it, eat rice, fish

85
00:06:35,800 --> 00:06:36,800
 curry.

86
00:06:36,800 --> 00:06:43,800
 If he puts it in the rice kruel, the rice kruel becomes rep

87
00:06:43,800 --> 00:06:50,800
ulsive when a curry made with cured fish is put into it.

88
00:06:50,800 --> 00:06:56,300
 In Burma, we have what we call fish paste. It's very smelly

89
00:06:56,300 --> 00:06:56,800
.

90
00:06:56,800 --> 00:06:59,740
 So it is allowable to do this only in order to use it

91
00:06:59,740 --> 00:07:01,800
 without making it repulsive.

92
00:07:01,800 --> 00:07:05,300
 Consequently, this is said with reference to such curry as

93
00:07:05,300 --> 00:07:08,800
 that. But what is unrepulsive such as honey, sugar, etc.

94
00:07:08,800 --> 00:07:10,800
 should be put into it.

95
00:07:10,800 --> 00:07:14,800
 And in taking it, he should take the right amount.

96
00:07:14,800 --> 00:07:20,560
 It is allowable to take green vegetables with one hand and

97
00:07:20,560 --> 00:07:21,800
 eat them.

98
00:07:21,800 --> 00:07:30,060
 But he should not do so. Not unless he does. But he should

99
00:07:30,060 --> 00:07:33,800
 not do so. But should be put into the bowl.

100
00:07:33,800 --> 00:07:39,800
 So unless he does not, it is also not correct here.

101
00:07:39,800 --> 00:07:44,980
 Although he can take the vegetables, put the vegetables in

102
00:07:44,980 --> 00:07:48,800
 his hand and eat it, it is not probable for him to do so.

103
00:07:48,800 --> 00:07:51,800
 So he should put it in the bowl.

104
00:07:51,800 --> 00:07:55,330
 Because a second vessel has been refused, it is not

105
00:07:55,330 --> 00:07:57,800
 allowable to use anything else.

106
00:07:57,800 --> 00:08:01,590
 Not even the leaf of a tree. Sometimes people use a leaf of

107
00:08:01,590 --> 00:08:06,600
 a tree as a bowl or as a receptacle. Even that is not

108
00:08:06,600 --> 00:08:08,800
 allowed.

109
00:08:08,800 --> 00:08:11,160
 And there are three grades. For one who is strict, except

110
00:08:11,160 --> 00:08:14,430
 at the time of eating sugar cane, it is not allowed while

111
00:08:14,430 --> 00:08:16,800
 eating to throw rubbish away.

112
00:08:16,800 --> 00:08:20,800
 Maybe some bones or something. Rubbish.

113
00:08:20,800 --> 00:08:24,230
 And it is not allowed while eating to break up rice lumps,

114
00:08:24,230 --> 00:08:25,800
 fish, meat and cakes.

115
00:08:25,800 --> 00:08:30,040
 The rubbish should be thrown away and the rice lumps, etc.

116
00:08:30,040 --> 00:08:32,800
 are broken up before sitting to eat.

117
00:08:32,800 --> 00:08:36,020
 The medium one is allowed to break them up with one hand

118
00:08:36,020 --> 00:08:38,800
 while eating and he is called hand acetic.

119
00:08:38,800 --> 00:08:42,040
 The mild one is called a bowl acetic. Anything that can be

120
00:08:42,040 --> 00:08:45,800
 put into his bowl, he is allowed while eating to break up.

121
00:08:45,800 --> 00:08:56,800
 That is rice lumps, etc. with his hand or with his teeth.

122
00:08:56,800 --> 00:09:00,150
 The moment any one of these three agrees to be a second

123
00:09:00,150 --> 00:09:02,800
 vessel, his acetic practices program.

124
00:09:02,800 --> 00:09:06,800
 So he could use only one bowl.

125
00:09:06,800 --> 00:09:13,830
 Then in our country, after taking milk, we drink water. It

126
00:09:13,830 --> 00:09:15,800
 is customary.

127
00:09:15,800 --> 00:09:19,510
 So when we want to drink water, we put the water in the

128
00:09:19,510 --> 00:09:21,800
 bowl and drink from the bowl.

129
00:09:21,800 --> 00:09:29,520
 So when I saw Zen people eating in a bowl and then wash the

130
00:09:29,520 --> 00:09:33,800
 bowl and drink the water, I was reminded of this practice.

131
00:09:33,800 --> 00:09:36,800
 It may have some connection with this practice.

132
00:09:36,800 --> 00:09:41,800
 Here, the practice is to have only one bowl.

133
00:09:41,800 --> 00:09:46,930
 You put everything in one bowl and drink from this bowl,

134
00:09:46,930 --> 00:09:53,800
 including water and other beverages.

135
00:09:53,800 --> 00:09:56,860
 In the benefits, craving for a variety of tastes is

136
00:09:56,860 --> 00:09:59,800
 eliminated, excessiveness of which yes, it is abandoned.

137
00:09:59,800 --> 00:10:03,800
 He sees the purpose and the right amount in the nutrient.

138
00:10:03,800 --> 00:10:08,800
 Now, the right amount should go.

139
00:10:08,800 --> 00:10:17,800
 What this man here sees is the mere purpose in taking food.

140
00:10:17,800 --> 00:10:26,870
 Now, Buddha said, "You must take food not to be proud of

141
00:10:26,870 --> 00:10:37,970
 themselves, not to make merriment, but just enough to keep

142
00:10:37,970 --> 00:10:41,800
 him alive so that he could practice Buddha's teachings."

143
00:10:41,800 --> 00:10:44,800
 So that is the purpose in food.

144
00:10:44,800 --> 00:10:50,210
 So here also, I think, food may be better than nutriment,

145
00:10:50,210 --> 00:10:52,800
 although it is not wrong.

146
00:10:52,800 --> 00:10:58,270
 So the purpose of taking food is not to beautify oneself,

147
00:10:58,270 --> 00:11:02,800
 not to take pride in one's strength and so on.

148
00:11:02,800 --> 00:11:09,770
 Here, he eats only in one bowl, and so he cannot have that

149
00:11:09,770 --> 00:11:13,800
 kind of pride in other things.

150
00:11:13,800 --> 00:11:18,310
 He is not bothered with carrying sausas, etc., about his

151
00:11:18,310 --> 00:11:21,390
 life conforms to the principles of fewness of wishes and so

152
00:11:21,390 --> 00:11:21,800
 on.

153
00:11:21,800 --> 00:11:28,110
 Now, one word is left here, untranslated, and that is, he

154
00:11:28,110 --> 00:11:30,800
 is not distracted while eating.

155
00:11:30,800 --> 00:11:34,690
 Because he uses only one bowl, he doesn't have to be

156
00:11:34,690 --> 00:11:38,800
 looking for other bowls, so he is not distracted.

157
00:11:38,800 --> 00:11:47,090
 That is the translation of that word is missing in the

158
00:11:47,090 --> 00:11:49,800
 translation.

159
00:11:49,800 --> 00:11:51,800
 So this is one bowl either.

160
00:11:51,800 --> 00:11:57,800
 The next one, it is difficult to understand the next one,

161
00:11:57,800 --> 00:12:02,800
 later food refusers practice.

162
00:12:02,800 --> 00:12:17,860
 Now, when a monk eats, and if he refuses to take some more,

163
00:12:17,860 --> 00:12:28,800
 then he must not eat other food after changing his posture.

164
00:12:28,800 --> 00:12:33,680
 Suppose I am eating sitting, and I am eating, and somebody

165
00:12:33,680 --> 00:12:36,800
 comes taking something and offers something to me,

166
00:12:36,800 --> 00:12:40,800
 and I say, "No, I don't want that. It's enough."

167
00:12:40,800 --> 00:12:46,870
 Then if I have done so, I can eat all until I finish my

168
00:12:46,870 --> 00:12:49,800
 meal, that is while sitting.

169
00:12:49,800 --> 00:12:55,830
 But if I stand up or if I walk and want to eat again, then

170
00:12:55,830 --> 00:12:57,800
 I cannot eat.

171
00:12:57,800 --> 00:13:05,570
 There is some kind of an act of winning to perform in order

172
00:13:05,570 --> 00:13:07,800
 for me to eat.

173
00:13:07,800 --> 00:13:12,800
 So that is what is meant here. Later food refuses me.

174
00:13:12,800 --> 00:13:21,700
 Now, as soon as I sit down, I cannot refuse. I am not said

175
00:13:21,700 --> 00:13:23,800
 to be refusing anything.

176
00:13:23,800 --> 00:13:30,020
 But after eating something, after eating one lump of food,

177
00:13:30,020 --> 00:13:37,920
 and then I refuse, then I cannot eat other food if I change

178
00:13:37,920 --> 00:13:40,800
 my posture.

179
00:13:40,800 --> 00:13:49,600
 Now, this monk who undertakes this ascetic practice cannot

180
00:13:49,600 --> 00:13:56,800
 take food after having made it allowable for him.

181
00:13:56,800 --> 00:14:05,600
 Now, suppose I refuse the offer, then if I want to eat

182
00:14:05,600 --> 00:14:12,800
 after breaking this posture, then I must take that food to

183
00:14:12,800 --> 00:14:13,800
 another monk,

184
00:14:13,800 --> 00:14:18,960
 and let him eat a little and say, "That's enough for me.

185
00:14:18,960 --> 00:14:24,800
 That means it is left over of him, and then I can eat it."

186
00:14:24,800 --> 00:14:28,400
 It is something like a punishment. You refuse and then you

187
00:14:28,400 --> 00:14:31,930
 know you want to eat, so you must eat another person's

188
00:14:31,930 --> 00:14:32,800
 leftover.

189
00:14:32,800 --> 00:14:36,800
 Something like that.

190
00:14:36,800 --> 00:14:43,410
 So a monk who does not undertake this ascetic practice can

191
00:14:43,410 --> 00:14:47,800
 eat that way if he wants to eat more.

192
00:14:47,800 --> 00:14:55,740
 But a monk who undertakes this practice must not eat in any

193
00:14:55,740 --> 00:14:56,800
 way.

194
00:14:56,800 --> 00:15:03,040
 He must avoid picking up the food and going to other monk

195
00:15:03,040 --> 00:15:07,800
 and have him make it leftover. He cannot do that.

196
00:15:07,800 --> 00:15:11,800
 So that is what is meant in here.

197
00:15:11,800 --> 00:15:17,800
 The translation is not so convincing here.

198
00:15:17,800 --> 00:15:22,640
 The words in the square brackets, I don't know where he got

199
00:15:22,640 --> 00:15:23,800
 them from.

200
00:15:23,800 --> 00:15:29,800
 They don't help much in understanding.

201
00:15:29,800 --> 00:15:37,800
 So there are three grades here.

202
00:15:37,800 --> 00:15:42,490
 There is no showing that he has it enough with respect to

203
00:15:42,490 --> 00:15:43,800
 the first lung,

204
00:15:43,800 --> 00:15:48,420
 but there is when he refuses more while that is being

205
00:15:48,420 --> 00:15:49,800
 swallowed.

206
00:15:49,800 --> 00:15:56,130
 That means in the rule it is said that a monk who refuses

207
00:15:56,130 --> 00:15:57,800
 while eating,

208
00:15:57,800 --> 00:16:06,800
 or a monk who refuses after he has started eating.

209
00:16:06,800 --> 00:16:13,800
 So if he has not eaten at all, there can be no refusal.

210
00:16:13,800 --> 00:16:18,800
 But he eats one lung and then the next lung he refuses.

211
00:16:18,800 --> 00:16:23,800
 So there can be refusal only at the second and other lungs.

212
00:16:23,800 --> 00:16:27,800
 So one who is strict has just shown that he has it enough.

213
00:16:27,800 --> 00:16:29,800
 That means he has refused.

214
00:16:29,800 --> 00:16:33,950
 He does not eat the second lung and so on. So he must stop

215
00:16:33,950 --> 00:16:34,800
 there.

216
00:16:34,800 --> 00:16:37,800
 Only one monk for that finish.

217
00:16:37,800 --> 00:16:41,000
 The medium one eats also that food with respect to which he

218
00:16:41,000 --> 00:16:45,800
 has shown that he has it enough.

219
00:16:45,800 --> 00:16:51,800
 So the medium one could eat, go on eating.

220
00:16:51,800 --> 00:16:56,950
 But the mild one goes on eating until he gets up from his

221
00:16:56,950 --> 00:16:57,800
 seat.

222
00:16:57,800 --> 00:17:01,800
 That means he could eat as much as he likes.

223
00:17:01,800 --> 00:17:12,800
 Unless provided he does not change his posture.

224
00:17:12,800 --> 00:17:17,800
 So it involves a certain rule.

225
00:17:17,800 --> 00:17:21,890
 And that rule is that if you have refused to accept

226
00:17:21,890 --> 00:17:22,800
 something,

227
00:17:22,800 --> 00:17:25,970
 and then if you want to eat it again, then you must do

228
00:17:25,970 --> 00:17:29,800
 something if you change posture.

229
00:17:29,800 --> 00:17:36,390
 That is why monks do not want to say no when something is

230
00:17:36,390 --> 00:17:38,800
 offered to them.

231
00:17:38,800 --> 00:17:40,800
 He may accept it and he may not eat it.

232
00:17:40,800 --> 00:17:45,920
 But he doesn't want to say no because that amounts to

233
00:17:45,920 --> 00:17:46,800
 refusal.

234
00:17:46,800 --> 00:17:51,800
 And so he could not eat later.

235
00:17:51,800 --> 00:18:00,800
 And there is something like talking in a roundabout way.

236
00:18:00,800 --> 00:18:07,560
 And in Burmese we have a certain expression for that which

237
00:18:07,560 --> 00:18:10,800
 goes around that rule.

238
00:18:10,800 --> 00:18:16,110
 And so when someone offers something to me and I want to

239
00:18:16,110 --> 00:18:17,800
 say no, I will not say no.

240
00:18:17,800 --> 00:18:21,870
 But I will say something like, directly translate it, I

241
00:18:21,870 --> 00:18:24,800
 will say it is complete or something like that.

242
00:18:24,800 --> 00:18:30,800
 But sometimes lay people don't know the monks' language.

243
00:18:30,800 --> 00:18:35,800
 So there is often a problem.

244
00:18:35,800 --> 00:18:39,800
 I will say it is complete or something like that.

245
00:18:39,800 --> 00:18:40,800
 But he doesn't know that.

246
00:18:40,800 --> 00:18:49,800
 He doesn't know that I refuse to accept.

247
00:18:49,800 --> 00:18:51,800
 So it is better to accept it.

248
00:18:51,800 --> 00:18:54,800
 And you can read it.

249
00:18:54,800 --> 00:18:59,800
 So this is the later food refusers' practice.

250
00:18:59,800 --> 00:19:09,800
 So this practice involves a rule in the Bhatimoka.

251
00:19:09,800 --> 00:19:11,800
 Okay.

252
00:19:11,800 --> 00:19:14,800
 I think we should stop here.

253
00:19:14,800 --> 00:19:34,800
 We will have to take two weeks to finish this chapter.

254
00:19:34,800 --> 00:19:40,800
 Are the others from the Bhatimoksha too?

255
00:19:40,800 --> 00:19:43,800
 Are the other practices from the Bhatimoksha?

256
00:19:43,800 --> 00:19:44,800
 No.

257
00:19:44,800 --> 00:19:46,800
 These practices are not from Bhatimoka.

258
00:19:46,800 --> 00:19:48,800
 But you said this one was.

259
00:19:48,800 --> 00:19:49,800
 But it involves.

260
00:19:49,800 --> 00:19:54,800
 This one involves Bhatimoka rule.

261
00:19:54,800 --> 00:20:01,530
 Because the Bhatimoka rule is that I must not eat if I have

262
00:20:01,530 --> 00:20:02,800
 refused.

263
00:20:02,800 --> 00:20:05,800
 Although I have refused, if I want to eat later,

264
00:20:05,800 --> 00:20:09,800
 then I can have it made left over by another monk.

265
00:20:09,800 --> 00:20:16,800
 But if I keep this practice, then I cannot do that.

266
00:20:16,800 --> 00:20:21,800
 So it would seem from this that fasting is not permissible

267
00:20:21,800 --> 00:20:25,800
 as a practice.

268
00:20:25,800 --> 00:20:27,800
 Fasting?

269
00:20:27,800 --> 00:20:28,800
 Not eating.

270
00:20:28,800 --> 00:20:31,800
 Not eating.

271
00:20:31,800 --> 00:20:34,800
 Or not eating altogether?

272
00:20:34,800 --> 00:20:36,800
 Is drinking water?

273
00:20:36,800 --> 00:20:38,800
 Ah, no.

274
00:20:38,800 --> 00:20:40,800
 That is not...

275
00:20:40,800 --> 00:20:42,800
 Unacceptable aesthetic practice.

276
00:20:42,800 --> 00:20:44,800
 Right.

277
00:20:44,800 --> 00:20:49,800
 Even for one or two or three days or seven...

278
00:20:49,800 --> 00:20:51,800
 It's okay to fast if you want to.

279
00:20:51,800 --> 00:20:56,800
 But not as a practice.

280
00:20:56,800 --> 00:21:04,800
 Because one has to eat to keep himself alive.

281
00:21:04,800 --> 00:21:09,800
 So fasting, in Buddhism, is fasting for half a day.

282
00:21:09,800 --> 00:21:12,800
 When lay people eat precepts,

283
00:21:12,800 --> 00:21:19,800
 then they do not eat from noon until the next morning.

284
00:21:19,800 --> 00:21:23,800
 Does it happen that you don't get food?

285
00:21:23,800 --> 00:21:29,500
 Does it happen that you don't get food when you go out for

286
00:21:29,500 --> 00:21:30,800
 alms?

287
00:21:30,800 --> 00:21:34,240
 Whether you go for alms or not, you are not to eat after

288
00:21:34,240 --> 00:21:34,800
 noon.

289
00:21:34,800 --> 00:21:36,800
 No, each time.

290
00:21:36,800 --> 00:21:39,800
 Sometimes when you go out for alms, do you not get food in

291
00:21:39,800 --> 00:21:39,800
 the morning?

292
00:21:39,800 --> 00:21:41,800
 When you try to go...

293
00:21:41,800 --> 00:21:43,800
 Somebody not give it to you?

294
00:21:43,800 --> 00:21:46,660
 Sometimes you eat out of noon because no one gives you food

295
00:21:46,660 --> 00:21:46,800
.

296
00:21:46,800 --> 00:21:47,800
 You don't eat.

297
00:21:47,800 --> 00:21:49,800
 Then you must go without food.

298
00:21:49,800 --> 00:21:56,800
 You cannot eat after noon on any account.

299
00:21:56,800 --> 00:22:08,800
 Sometimes when traveling, sometimes I had to skip meals.

300
00:22:08,800 --> 00:22:10,800
 You know, when you board a plane.

301
00:22:10,800 --> 00:22:15,800
 They don't serve meals until afternoon.

302
00:22:15,800 --> 00:22:20,800
 The answer to that question is, "Where?"

303
00:22:20,800 --> 00:22:23,800
 In Burma, you will never have that problem.

304
00:22:23,800 --> 00:22:24,800
 That's right.

305
00:22:24,800 --> 00:22:29,800
 Because in Burma, you will always get enough to eat.

306
00:22:29,800 --> 00:22:32,800
 People are very willing.

307
00:22:32,800 --> 00:22:36,800
 They are glad to be able to give to monks.

308
00:22:36,800 --> 00:22:41,800
 So it's not a problem in Buddhist countries,

309
00:22:41,800 --> 00:22:45,800
 but here, in other countries, it's a problem.

310
00:22:45,800 --> 00:22:50,800
 Yes.

311
00:22:50,800 --> 00:22:55,080
 I don't know if you've seen this movie about the heart of

312
00:22:55,080 --> 00:22:55,800
 Burma.

313
00:22:55,800 --> 00:23:00,800
 This Japanese soldier.

314
00:23:00,800 --> 00:23:04,130
 At that time, I don't think he was really properly even

315
00:23:04,130 --> 00:23:04,800
 obtained,

316
00:23:04,800 --> 00:23:07,800
 but he knows that the way he can get through initially.

317
00:23:07,800 --> 00:23:11,800
 So when I see him, I'm not an independent problem.

318
00:23:11,800 --> 00:23:16,790
 Later on, he really got into it and he don't get properly

319
00:23:16,790 --> 00:23:18,800
 obtained.

320
00:23:18,800 --> 00:23:25,800
 Very interesting movie.

321
00:23:25,800 --> 00:23:35,800
 Another question that I think you asked about the ropes.

322
00:23:35,800 --> 00:23:39,130
 They are done commercially and also, I think we have one

323
00:23:39,130 --> 00:23:40,800
 season of festival

324
00:23:40,800 --> 00:23:46,800
 where we have people who stitch the same program.

325
00:23:46,800 --> 00:23:57,800
 No, actually, what Libibu do is to make cloth, not ropes.

326
00:23:57,800 --> 00:24:02,800
 They call it rope material.

327
00:24:02,800 --> 00:24:08,800
 Now is the season for offering katina ropes.

328
00:24:08,800 --> 00:24:17,800
 Katina rope is different from ordinary ropes.

329
00:24:17,800 --> 00:24:27,800
 The difference is we must make the rope in one day.

330
00:24:27,800 --> 00:24:33,890
 Suppose a labors comes to a monastery and offer not a

331
00:24:33,890 --> 00:24:35,800
 finished rope,

332
00:24:35,800 --> 00:24:39,800
 but a cloth to be used as katina rope.

333
00:24:39,800 --> 00:24:44,800
 If we accept it today, then we must finish it today

334
00:24:44,800 --> 00:24:47,800
 until it becomes a rope.

335
00:24:47,800 --> 00:24:49,800
 That is in the olden days,

336
00:24:49,800 --> 00:24:53,800
 where when no ready-made ropes are available

337
00:24:53,800 --> 00:24:56,800
 or they don't want to offer ready-made ropes.

338
00:24:56,800 --> 00:25:01,180
 In that case, all the monks and the monastery must work

339
00:25:01,180 --> 00:25:01,800
 together.

340
00:25:01,800 --> 00:25:07,460
 Some are boiling a die, some are stitching, some are

341
00:25:07,460 --> 00:25:08,800
 cutting and so on.

342
00:25:08,800 --> 00:25:15,800
 Then everybody must lend a hand.

343
00:25:15,800 --> 00:25:21,800
 Following that tradition, nowadays in Burma,

344
00:25:21,800 --> 00:25:25,800
 people have a festival weaving cloth.

345
00:25:25,800 --> 00:25:35,800
 They transfer the expression unstead to their weaving.

346
00:25:35,800 --> 00:25:40,800
 They try to weave, suppose,

347
00:25:40,800 --> 00:25:47,800
 from 6 o'clock this evening before dawn next morning.

348
00:25:47,800 --> 00:25:49,800
 They try to weave.

349
00:25:49,800 --> 00:25:55,800
 There is a contest, the girls enter this contest,

350
00:25:55,800 --> 00:26:00,800
 weaving cloth for ropes.

351
00:26:00,800 --> 00:26:04,800
 I don't know how they decide the winner.

352
00:26:04,800 --> 00:26:10,800
 So actually, the word "unsteal" is used among monks.

353
00:26:10,800 --> 00:26:13,800
 That means you must make the rope.

354
00:26:13,800 --> 00:26:16,800
 On the very day you accept the cloth,

355
00:26:16,800 --> 00:26:22,800
 it must not consteal and go over to the next day.

356
00:26:22,800 --> 00:26:31,800
 In Burmese we call "unsteal rope"

357
00:26:31,800 --> 00:26:37,800
 and "unsteal rope" means "a rope made on the same day".

358
00:26:37,800 --> 00:26:42,800
 But now people say they are offering unsteal rope

359
00:26:42,800 --> 00:26:47,520
 but actually what they do is weave cloth and offer to the

360
00:26:47,520 --> 00:26:47,800
 monks.

361
00:26:47,800 --> 00:26:49,800
 The monks have to do that.

362
00:26:49,800 --> 00:26:51,800
 But now we are in a better shape

363
00:26:51,800 --> 00:26:55,910
 because there are ready-made roofs and so we don't have to

364
00:26:55,910 --> 00:26:56,800
 do anything.

365
00:26:56,800 --> 00:27:02,570
 But sometimes people wanted monks to do something as they

366
00:27:02,570 --> 00:27:04,800
 did in the olden days.

367
00:27:04,800 --> 00:27:10,800
 When I was living in my country, it's a kind.

368
00:27:10,800 --> 00:27:13,800
 A certain head of a monastery said,

369
00:27:13,800 --> 00:27:18,800
 "Why not do something like they did in the olden days?"

370
00:27:18,800 --> 00:27:23,800
 So he had people brought cloth to the monastery

371
00:27:23,800 --> 00:27:25,800
 and then everybody had to work.

372
00:27:25,800 --> 00:27:33,800
 Oh, it's a great work because even the smallest of the rope

373
00:27:33,800 --> 00:27:33,800
,

374
00:27:33,800 --> 00:27:39,800
 because you have to die two or three times, not one time.

375
00:27:39,800 --> 00:27:46,800
 And if there were rains, it would be very difficult.

376
00:27:46,800 --> 00:27:53,210
 But it was lucky because in Upper Bama, in Sagan, there was

377
00:27:53,210 --> 00:27:54,800
 not much rain.

378
00:27:54,800 --> 00:28:00,800
 So we were able to finish the rope in time.

379
00:28:00,800 --> 00:28:10,080
 But it's not really dry, but a little damp, but it could be

380
00:28:10,080 --> 00:28:12,800
 used as a rope.

381
00:28:12,800 --> 00:28:16,800
 That is why there are now commercial ropes.

382
00:28:16,800 --> 00:28:21,800
 And so monks do not know how to make ropes now.

383
00:28:21,800 --> 00:28:27,800
 So the dimension of the parts and then how to stitch them,

384
00:28:27,800 --> 00:28:33,800
 monks don't know now. Most monks don't know.

385
00:28:33,800 --> 00:28:37,960
 And when I came to this country first, people asked me to

386
00:28:37,960 --> 00:28:39,800
 order ropes from Bama.

387
00:28:39,800 --> 00:28:42,800
 I said, "Why not make ropes here?"

388
00:28:42,800 --> 00:28:52,050
 So I gave them a dimension of the rope and they made it

389
00:28:52,050 --> 00:28:53,800
 here.

390
00:28:53,800 --> 00:28:57,800
 The ropes are rectangular. Is that it?

391
00:28:57,800 --> 00:28:59,800
 Yes.

392
00:28:59,800 --> 00:29:08,630
 Actually, according to our books, they represent the

393
00:29:08,630 --> 00:29:10,800
 pattern of the fields.

394
00:29:10,800 --> 00:29:16,800
 Once Buddha was travelling and he was up a mountain,

395
00:29:16,800 --> 00:29:19,800
 and he looked down and he saw the fields.

396
00:29:19,800 --> 00:29:24,150
 So the fields are small fields in India, not like fields in

397
00:29:24,150 --> 00:29:25,800
 the United States.

398
00:29:25,800 --> 00:29:28,960
 Here they use machines and so these are maybe two or three

399
00:29:28,960 --> 00:29:29,800
 miles long.

400
00:29:29,800 --> 00:29:34,800
 They are a field maybe about 10, 20 yards wide and so on.

401
00:29:34,800 --> 00:29:44,070
 So Buddha saw these boundaries of the fields, kazin, kazin.

402
00:29:44,070 --> 00:29:45,800
 What do you call those?

403
00:29:45,800 --> 00:29:50,800
 Rose, beds, beds, rows.

404
00:29:50,800 --> 00:29:53,800
 No, no, the edges.

405
00:29:53,800 --> 00:29:56,800
 So Buddha saw this and Buddha saw Anana.

406
00:29:56,800 --> 00:30:01,290
 "Anana, could you make a rope like this?" Anana said, "Yes

407
00:30:01,290 --> 00:30:01,800
."

408
00:30:01,800 --> 00:30:08,060
 And so Anana made the rope, which looks like the pattern of

409
00:30:08,060 --> 00:30:09,800
 the field.

410
00:30:09,800 --> 00:30:22,520
 It came to me. And so we usually have five sections for

411
00:30:22,520 --> 00:30:24,800
 this rope.

412
00:30:24,800 --> 00:30:29,800
 You cannot see these pieces, right? The sections.

413
00:30:29,800 --> 00:30:31,800
 These people are closed.

414
00:30:31,800 --> 00:30:36,410
 This is one section and here are the two. And the second

415
00:30:36,410 --> 00:30:41,800
 section. The third section is

416
00:30:41,800 --> 00:30:44,920
 actually wider than the other two. So there are five

417
00:30:44,920 --> 00:30:45,800
 sections.

418
00:30:45,800 --> 00:30:46,800
 But there are strips?

419
00:30:46,800 --> 00:30:51,800
 Yeah. And then there is a small room here.

420
00:30:51,800 --> 00:30:56,800
 So two rooms in one section.

421
00:30:56,800 --> 00:31:09,800
 And these stitches must be cut, actually.

422
00:31:09,800 --> 00:31:12,800
 Cut and then stitched together again.

423
00:31:12,800 --> 00:31:17,800
 But nowadays they just make a particle. They do not cut.

424
00:31:17,800 --> 00:31:22,800
 They fold, they just fold it in. Stitched.

425
00:31:22,800 --> 00:31:27,620
 So the idea behind this is that you are getting these cloth

426
00:31:27,620 --> 00:31:28,800
s from whatever you get.

427
00:31:28,800 --> 00:31:29,800
 That's right.

428
00:31:29,800 --> 00:31:35,150
 So you may not get the right size. You have to pick up

429
00:31:35,150 --> 00:31:36,800
 small pieces of cloth

430
00:31:36,800 --> 00:31:41,800
 and then make them into a rope.

431
00:31:41,800 --> 00:31:48,800
 Did everybody follow the same pattern?

432
00:31:48,800 --> 00:31:58,800
 Oh, yes. And this has more sections.

433
00:31:58,800 --> 00:32:03,860
 The number of sections is odd. Seven, nine, thirteen,

434
00:32:03,860 --> 00:32:04,800
 fifteen.

435
00:32:04,800 --> 00:32:08,800
 It's like, okay, sir. This is kind of a miniature rope.

436
00:32:08,800 --> 00:32:09,800
 That's right.

437
00:32:09,800 --> 00:32:10,800
 Miniature of that.

438
00:32:10,800 --> 00:32:15,800
 Yeah, miniature of this.

439
00:32:15,800 --> 00:32:18,800
 The stitches are...

440
00:32:18,800 --> 00:32:19,800
 Patterns and strips are called.

441
00:32:19,800 --> 00:32:22,800
 But we have borders. Do you have borders?

442
00:32:22,800 --> 00:32:23,800
 Yes.

443
00:32:23,800 --> 00:32:25,800
 I have the borders.

444
00:32:25,800 --> 00:32:26,800
 Yeah.

445
00:32:26,800 --> 00:32:27,800
 These are the borders.

446
00:32:27,800 --> 00:32:28,800
 And you even have the...

447
00:32:28,800 --> 00:32:29,800
 Oh, and corners too.

448
00:32:29,800 --> 00:32:31,800
 And corners, like we have corners.

449
00:32:31,800 --> 00:32:33,800
 Same corner.

450
00:32:33,800 --> 00:32:35,800
 And this is used...

451
00:32:35,800 --> 00:32:38,800
 We put some buttons here.

452
00:32:38,800 --> 00:32:46,800
 And when I wear this way,

453
00:32:46,800 --> 00:32:52,180
 actually they must be tied together or there must be a

454
00:32:52,180 --> 00:32:52,800
 button

455
00:32:52,800 --> 00:32:57,800
 and put it in here.

456
00:32:57,800 --> 00:33:01,800
 Or I can just tie them together.

457
00:33:01,800 --> 00:33:18,800
 So when I walk, the lower government won't show.

458
00:33:18,800 --> 00:33:23,380
 But yeah, I don't do this because sometimes it got caught

459
00:33:23,380 --> 00:33:26,800
 in some door of the car or something.

460
00:33:26,800 --> 00:33:37,800
 Okay.

461
00:33:37,800 --> 00:34:04,800
 Okay.

