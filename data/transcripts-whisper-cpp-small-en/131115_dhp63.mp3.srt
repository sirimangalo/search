1
00:00:00,000 --> 00:00:05,000
 Hi and welcome back to our study of the Dhammapada.

2
00:00:05,000 --> 00:00:09,640
 Tonight we continue on with verse number 63, which reads as

3
00:00:09,640 --> 00:00:12,000
 follows.

4
00:00:12,000 --> 00:00:24,530
 "Yobalu manyati balayang pandito vapite nasu balocha pand

5
00:00:24,530 --> 00:00:30,000
ita mani sahwe baloti ujati"

6
00:00:30,000 --> 00:00:34,000
 Which translates as follows.

7
00:00:34,000 --> 00:00:41,100
 "Yobalu manyati balayang" The fool who knows that they are

8
00:00:41,100 --> 00:00:42,000
 a fool.

9
00:00:42,000 --> 00:00:48,370
 "Pandito vapite nasu" For that reason, or to that extent,

10
00:00:48,370 --> 00:00:52,000
 they can be called wise.

11
00:00:52,000 --> 00:01:02,530
 "Bhalocha pandita mani bhat" The fool who thinks of

12
00:01:02,530 --> 00:01:06,000
 themselves as wise, "sahwe baloti ujati"

13
00:01:06,000 --> 00:01:12,000
 That person indeed is called a fool.

14
00:01:12,000 --> 00:01:19,000
 So it's one of the more important Dhammapada verses.

15
00:01:19,000 --> 00:01:22,000
 I don't know, more memorable I suppose.

16
00:01:22,000 --> 00:01:24,000
 It's a powerful one.

17
00:01:24,000 --> 00:01:28,640
 Especially for those of us beginning the path and feeling

18
00:01:28,640 --> 00:01:31,000
 discouraged sometimes.

19
00:01:31,000 --> 00:01:33,890
 Anyway, we'll go through the story and then we'll talk

20
00:01:33,890 --> 00:01:35,000
 about the verse.

21
00:01:35,000 --> 00:01:37,000
 The story goes, it's a very short story actually.

22
00:01:37,000 --> 00:01:39,420
 We're going to find several of the Dhammapada stories are

23
00:01:39,420 --> 00:01:40,000
 quite short.

24
00:01:40,000 --> 00:01:43,000
 So there's not much to say about them.

25
00:01:43,000 --> 00:01:48,420
 When the Buddha was dwelling in Sawati there were these two

26
00:01:48,420 --> 00:01:54,000
 thieves who decided that they would go off to the monastery

27
00:01:54,000 --> 00:01:59,690
 and try to rob the people who came to listen to the Buddha

28
00:01:59,690 --> 00:02:02,000
's teaching.

29
00:02:02,000 --> 00:02:07,000
 So here in Sawati there was a big city, walled city.

30
00:02:07,000 --> 00:02:10,000
 You can even still see the walls if you go now.

31
00:02:10,000 --> 00:02:15,000
 To visit, that's all that's left is a big field with walls

32
00:02:15,000 --> 00:02:16,000
 and ruins.

33
00:02:16,000 --> 00:02:19,000
 The ruins are just covered mounds of who knows what.

34
00:02:19,000 --> 00:02:23,390
 If you go up on these bhagodas you can see the walls all

35
00:02:23,390 --> 00:02:25,000
 around the city.

36
00:02:25,000 --> 00:02:31,050
 So there would have been rich people and high class people

37
00:02:31,050 --> 00:02:32,000
 who would go to see the Buddha

38
00:02:32,000 --> 00:02:36,000
 and hear the Buddha teach.

39
00:02:36,000 --> 00:02:39,490
 Because they would be so intent upon what the Buddha was

40
00:02:39,490 --> 00:02:42,240
 saying they'd often just lose track of their personal

41
00:02:42,240 --> 00:02:43,000
 belongings,

42
00:02:43,000 --> 00:02:48,000
 lose their care for their possessions.

43
00:02:48,000 --> 00:02:50,000
 It would actually be easy picking.

44
00:02:50,000 --> 00:02:53,370
 This happens in Thailand actually, or it has happened in

45
00:02:53,370 --> 00:02:54,000
 Thailand.

46
00:02:54,000 --> 00:02:57,990
 So they say when you go to hear the monks talk, keep your

47
00:02:57,990 --> 00:03:00,000
 belongings close to you.

48
00:03:00,000 --> 00:03:06,000
 If you're going to meditate with your purse in your hands

49
00:03:06,000 --> 00:03:06,000
 or something like that.

50
00:03:06,000 --> 00:03:15,500
 Because unfortunately a byproduct of letting go is the not

51
00:03:15,500 --> 00:03:18,000
 worrying too much about your belongings.

52
00:03:18,000 --> 00:03:22,760
 So the best advice is to leave your valuables at home when

53
00:03:22,760 --> 00:03:26,000
 you go to the monastery.

54
00:03:26,000 --> 00:03:30,920
 In large crowds, you know, everyone sitting around med

55
00:03:30,920 --> 00:03:32,000
itating.

56
00:03:32,000 --> 00:03:34,710
 So they got there, these two thieves went and knew the

57
00:03:34,710 --> 00:03:36,000
 Buddha was giving a talk.

58
00:03:36,000 --> 00:03:41,000
 And so they went and they saw the crowd and they split up

59
00:03:41,000 --> 00:03:48,000
 to go and pickpocket the belongings of the audience.

60
00:03:48,000 --> 00:03:51,460
 But one of them, when he heard the Buddha speak, he stopped

61
00:03:51,460 --> 00:03:52,000
 for a second,

62
00:03:52,000 --> 00:03:56,000
 kind of looking and scoping out his target.

63
00:03:56,000 --> 00:04:00,000
 Then he stopped and listened to what the Buddha had to say.

64
00:04:00,000 --> 00:04:02,370
 And he became so enchanted by what the Buddha said that he

65
00:04:02,370 --> 00:04:04,000
 just forgot all about stealing

66
00:04:04,000 --> 00:04:06,250
 and he really listened to what the Buddha had to say and

67
00:04:06,250 --> 00:04:08,000
 got the deeper meaning

68
00:04:08,000 --> 00:04:11,000
 and realized there was a higher purpose to life.

69
00:04:11,000 --> 00:04:18,210
 And that theft of a few coins was meaningless in the whole

70
00:04:18,210 --> 00:04:21,000
 grand scheme of things.

71
00:04:21,000 --> 00:04:24,770
 And so he sat down and he started meditating and based on

72
00:04:24,770 --> 00:04:25,000
 the Buddha's teaching,

73
00:04:25,000 --> 00:04:30,850
 he was able to become a sodapana, just sitting there

74
00:04:30,850 --> 00:04:33,000
 listening.

75
00:04:33,000 --> 00:04:36,850
 And so at the end of the night, he just went back home

76
00:04:36,850 --> 00:04:38,000
 without stealing anything.

77
00:04:38,000 --> 00:04:41,800
 The other guy went, found his target, stole some, I think

78
00:04:41,800 --> 00:04:44,000
 it was five gold coins,

79
00:04:44,000 --> 00:04:50,440
 pocketed them, went back home and had his wife cook him up

80
00:04:50,440 --> 00:04:52,000
 some rich food

81
00:04:52,000 --> 00:05:00,000
 and he had gotten the dough, gotten the spoils, and so he

82
00:05:00,000 --> 00:05:01,000
 had his wife prepare a meal

83
00:05:01,000 --> 00:05:07,160
 with buying groceries or however and preparing a meal for

84
00:05:07,160 --> 00:05:08,000
 him.

85
00:05:08,000 --> 00:05:10,230
 The other guy went home and knowing he had no money and he

86
00:05:10,230 --> 00:05:12,000
 had nothing to show for it,

87
00:05:12,000 --> 00:05:15,000
 he went home and ate nothing.

88
00:05:15,000 --> 00:05:22,000
 And then the thief who actually stole found out about this,

89
00:05:22,000 --> 00:05:22,000
 you know,

90
00:05:22,000 --> 00:05:24,470
 they were bragging to each other, so he went up and asked

91
00:05:24,470 --> 00:05:25,000
 him what he had gotten

92
00:05:25,000 --> 00:05:28,000
 and he didn't get anything and he looked at him and he said

93
00:05:28,000 --> 00:05:28,000
,

94
00:05:28,000 --> 00:05:33,000
 "Oh," he said, "what I got from this was wisdom."

95
00:05:33,000 --> 00:05:37,000
 And the first thief says, "Oh, really? Why is that?

96
00:05:37,000 --> 00:05:39,000
 You're so wise you can't even feed your family.

97
00:05:39,000 --> 00:05:42,000
 Look at me, what I've got. You call yourself wise.

98
00:05:42,000 --> 00:05:47,150
 Who's the one who's able to feed, who's able to put a meal

99
00:05:47,150 --> 00:05:49,000
 on the table?"

100
00:05:49,000 --> 00:05:54,000
 And the other thief, ex-thief, looked at his ex-friend

101
00:05:54,000 --> 00:05:57,000
 and shook his head and thought to himself,

102
00:05:57,000 --> 00:06:01,000
 "Look at this guy, this fool who thinks he's a wise man,

103
00:06:01,000 --> 00:06:03,000
 not realizing how foolish he is."

104
00:06:03,000 --> 00:06:05,000
 And so he went to the Buddha, he went back to Jetha one

105
00:06:05,000 --> 00:06:09,000
 where the Buddha was staying in the monastery,

106
00:06:09,000 --> 00:06:11,000
 and related this to the Buddha and said,

107
00:06:11,000 --> 00:06:15,760
 "It's amazing how foolish people, what they think passes

108
00:06:15,760 --> 00:06:17,000
 for wisdom."

109
00:06:17,000 --> 00:06:19,000
 And that's the story the Buddha then told us.

110
00:06:19,000 --> 00:06:23,000
 He said, "That's very much the case.

111
00:06:23,000 --> 00:06:26,000
 The true fool thinks they're wise,

112
00:06:26,000 --> 00:06:29,000
 but a person, if they're able to know that they're foolish,

113
00:06:29,000 --> 00:06:34,000
 to that extent you can call them wise."

114
00:06:34,000 --> 00:06:37,590
 This is an important verse because it highlights the

115
00:06:37,590 --> 00:06:39,000
 importance of wisdom,

116
00:06:39,000 --> 00:06:44,000
 the importance of truth, of understanding the truth.

117
00:06:44,000 --> 00:06:49,000
 True wisdom means understanding the truth.

118
00:06:49,000 --> 00:06:56,220
 It doesn't mean being able to lie and cheat and connive or

119
00:06:56,220 --> 00:06:59,000
 plunder and so on.

120
00:06:59,000 --> 00:07:04,310
 There's a difference between worldly intelligence and

121
00:07:04,310 --> 00:07:05,000
 wisdom.

122
00:07:05,000 --> 00:07:10,280
 And the key here that does have some interesting

123
00:07:10,280 --> 00:07:11,000
 implications

124
00:07:11,000 --> 00:07:19,000
 is that knowledge is always better.

125
00:07:19,000 --> 00:07:24,000
 Many people are shocked when they hear that in Buddhism

126
00:07:24,000 --> 00:07:28,000
 we say it's better to know that what you're doing is wrong.

127
00:07:28,000 --> 00:07:35,470
 So we have this intuitive idea that if you commit a bad

128
00:07:35,470 --> 00:07:36,000
 deed,

129
00:07:36,000 --> 00:07:42,000
 we talked about this on Sunday at the MBBCA,

130
00:07:42,000 --> 00:07:46,010
 if we have this intuitive notion that if someone performs

131
00:07:46,010 --> 00:07:48,000
 an unwholesome deed,

132
00:07:48,000 --> 00:07:50,000
 not knowing that it's unwholesome,

133
00:07:50,000 --> 00:07:54,580
 that they're somehow less responsible, less culpable for

134
00:07:54,580 --> 00:07:56,000
 their behavior.

135
00:07:56,000 --> 00:07:59,000
 Whereas a person who does something knowing it's wrong,

136
00:07:59,000 --> 00:08:02,010
 we always say, "You should know better. You know better

137
00:08:02,010 --> 00:08:03,000
 than that."

138
00:08:03,000 --> 00:08:07,000
 So if the younger sibling steals something, they say,

139
00:08:07,000 --> 00:08:09,000
 "He doesn't know any better."

140
00:08:09,000 --> 00:08:10,000
 And so they let him offer that.

141
00:08:10,000 --> 00:08:14,000
 But the older sibling who knows better,

142
00:08:14,000 --> 00:08:17,000
 this is how it goes in courts as well, right?

143
00:08:17,000 --> 00:08:22,000
 The kid who's 12 to 18 doesn't know any better,

144
00:08:22,000 --> 00:08:25,270
 under 18 doesn't know any better, but once you're 18, you

145
00:08:25,270 --> 00:08:26,000
 know better.

146
00:08:26,000 --> 00:08:27,000
 And so you're punished.

147
00:08:27,000 --> 00:08:31,100
 So we get this idea that somehow that would be the case for

148
00:08:31,100 --> 00:08:32,000
 karma.

149
00:08:32,000 --> 00:08:36,260
 It would be less karmically potent to do something not

150
00:08:36,260 --> 00:08:38,000
 knowing that it's wrong.

151
00:08:38,000 --> 00:08:42,000
 And that's really what this verse is talking about.

152
00:08:42,000 --> 00:08:48,000
 That if all you know is that you're a fool,

153
00:08:48,000 --> 00:08:50,000
 that's better than not knowing you're a fool

154
00:08:50,000 --> 00:08:53,000
 and going about doing things thinking you're wise.

155
00:08:53,000 --> 00:08:56,000
 This story aptly illustrates that point,

156
00:08:56,000 --> 00:09:00,000
 that the person who knows what they're doing is wrong,

157
00:09:00,000 --> 00:09:04,000
 even if they're engaging in it.

158
00:09:04,000 --> 00:09:06,000
 The idea is that they will engage in it hesitantly.

159
00:09:06,000 --> 00:09:09,350
 But a person who thinks they're wise in doing will not only

160
00:09:09,350 --> 00:09:10,000
 perform evil deeds,

161
00:09:10,000 --> 00:09:14,000
 but will boast and brag about them.

162
00:09:14,000 --> 00:09:18,000
 An act of theft, whether you know it's wrong or not,

163
00:09:18,000 --> 00:09:21,000
 is a corrupting act.

164
00:09:21,000 --> 00:09:26,000
 It's a violation of other people's property,

165
00:09:26,000 --> 00:09:31,000
 other people's very being.

166
00:09:31,000 --> 00:09:39,000
 And if you can't see that,

167
00:09:39,000 --> 00:09:43,550
 the intent to do it, the zest and the zeal in performing

168
00:09:43,550 --> 00:09:44,000
 the act,

169
00:09:44,000 --> 00:09:47,280
 is much higher than if you can see that you're actually

170
00:09:47,280 --> 00:09:48,000
 violating the person's being.

171
00:09:48,000 --> 00:09:52,000
 If you know that what you're doing is reprehensible,

172
00:09:52,000 --> 00:09:55,240
 there's that force against you, the internal force acting

173
00:09:55,240 --> 00:09:56,000
 against you.

174
00:09:56,000 --> 00:10:00,000
 Now, the opposite holds for doing good deeds,

175
00:10:00,000 --> 00:10:04,000
 which is an interesting truth,

176
00:10:04,000 --> 00:10:08,000
 that if a person performs good deeds,

177
00:10:08,000 --> 00:10:10,760
 they're more powerful when you know that they're good deeds

178
00:10:10,760 --> 00:10:11,000
.

179
00:10:11,000 --> 00:10:13,000
 Obviously, if you know something that is good for you,

180
00:10:13,000 --> 00:10:16,000
 is beneficial,

181
00:10:16,000 --> 00:10:19,000
 you're more inclined to perform it,

182
00:10:19,000 --> 00:10:22,000
 you have more confidence in performing it,

183
00:10:22,000 --> 00:10:28,000
 you're more content and interested and zealous about it.

184
00:10:28,000 --> 00:10:30,000
 Whereas if you don't know that something's good,

185
00:10:30,000 --> 00:10:33,230
 your parents drag you off to the monastery to listen to the

186
00:10:33,230 --> 00:10:34,000
 Buddha's teaching,

187
00:10:34,000 --> 00:10:38,000
 or drag you off to give offerings to the monk,

188
00:10:38,000 --> 00:10:41,000
 to give food to the monk,

189
00:10:41,000 --> 00:10:44,000
 or if someone's nagging you to give charity,

190
00:10:44,000 --> 00:10:49,000
 or you're keeping the precepts out of,

191
00:10:49,000 --> 00:10:53,000
 keeping religious precepts out of some kind of tradition,

192
00:10:53,000 --> 00:10:56,000
 traditional compulsion or so on.

193
00:10:56,000 --> 00:10:59,000
 This is substandard to actually,

194
00:10:59,000 --> 00:11:03,000
 meaning it actually performing it from the heart.

195
00:11:03,000 --> 00:11:07,430
 So the point in both cases is knowledge is of greatest

196
00:11:07,430 --> 00:11:09,000
 importance.

197
00:11:09,000 --> 00:11:12,000
 It's a catchphrase that you can use for people,

198
00:11:12,000 --> 00:11:15,000
 and if someone boasts about bad deeds, you say,

199
00:11:15,000 --> 00:11:19,000
 "Well, I may not be perfect, but at least I know my fault."

200
00:11:19,000 --> 00:11:22,870
 This is really one of the most dangerous flaws that there

201
00:11:22,870 --> 00:11:23,000
 is,

202
00:11:23,000 --> 00:11:27,000
 along with wrong view or belief in wrong view.

203
00:11:27,000 --> 00:11:29,000
 It is a form of wrong view.

204
00:11:29,000 --> 00:11:33,000
 If you believe that what you're doing is,

205
00:11:33,000 --> 00:11:35,000
 if you believe that bad deeds are a good thing,

206
00:11:35,000 --> 00:11:39,000
 and that you're somehow wise in your foolishness,

207
00:11:39,000 --> 00:11:42,000
 that's more dangerous than the deeds themselves,

208
00:11:42,000 --> 00:11:45,000
 because that's what's going to compel you to

209
00:11:45,000 --> 00:11:52,000
 perform the deeds with enthusiasm and perpetually.

210
00:11:52,000 --> 00:11:54,000
 So what does it mean to be a fool?

211
00:11:54,000 --> 00:12:00,140
 A fool is someone who acts, speaks, and thinks unwholesome

212
00:12:00,140 --> 00:12:01,000
ly.

213
00:12:01,000 --> 00:12:04,000
 So they perform unwholesome deeds with the body,

214
00:12:04,000 --> 00:12:07,000
 they kill, they steal, they cheat,

215
00:12:07,000 --> 00:12:10,000
 they perform unwholesome deeds with speech,

216
00:12:10,000 --> 00:12:14,000
 they lie, they gossip, they scold,

217
00:12:14,000 --> 00:12:19,000
 and they prattle or chatter,

218
00:12:19,000 --> 00:12:22,000
 and they perform unwholesome deeds with mind.

219
00:12:22,000 --> 00:12:25,270
 It means their minds are full of greed and anger and

220
00:12:25,270 --> 00:12:26,000
 delusion,

221
00:12:26,000 --> 00:12:31,000
 ill will and conceit and so on.

222
00:12:31,000 --> 00:12:35,000
 So we all have these tendencies, right?

223
00:12:35,000 --> 00:12:38,000
 We may not all kill and steal and lie and so on,

224
00:12:38,000 --> 00:12:43,000
 but we all have the anger, the greed and the delusion.

225
00:12:43,000 --> 00:12:47,750
 Until you become an Arahant, these things exist in the mind

226
00:12:47,750 --> 00:12:48,000
.

227
00:12:48,000 --> 00:12:54,000
 That's not the worst, that's not the most important

228
00:12:54,000 --> 00:12:59,720
 or the most horrific fact of the most horrific evil that

229
00:12:59,720 --> 00:13:01,000
 there is.

230
00:13:01,000 --> 00:13:03,000
 Worse than actually, it's not the real problem,

231
00:13:03,000 --> 00:13:06,000
 it's not the most compelling problem.

232
00:13:06,000 --> 00:13:09,000
 The most compelling problem is our knowledge

233
00:13:09,000 --> 00:13:11,000
 of what these are doing to ourselves.

234
00:13:11,000 --> 00:13:13,000
 A person who is angry, perpetually angry,

235
00:13:13,000 --> 00:13:16,190
 if they know that they're angry, there's a potential to

236
00:13:16,190 --> 00:13:17,000
 work it out.

237
00:13:17,000 --> 00:13:20,000
 If they know that it's a bad thing to be angry,

238
00:13:20,000 --> 00:13:24,000
 if a person is self-righteously angry,

239
00:13:24,000 --> 00:13:26,000
 these are the people you have to watch out for,

240
00:13:26,000 --> 00:13:30,000
 or the people who feel that there's benefit to being angry,

241
00:13:30,000 --> 00:13:33,000
 people who think that there's benefit to killing,

242
00:13:33,000 --> 00:13:35,000
 there's benefit to stealing and so on.

243
00:13:35,000 --> 00:13:36,000
 This is the problem.

244
00:13:36,000 --> 00:13:38,740
 A person who steals knowing it's wrong and feeling guilty

245
00:13:38,740 --> 00:13:39,000
 about it,

246
00:13:39,000 --> 00:13:42,230
 but steals a loaf of bread to feed their family, for

247
00:13:42,230 --> 00:13:43,000
 example.

248
00:13:43,000 --> 00:13:53,540
 This is an unwholesome act, but nothing compared to

249
00:13:53,540 --> 00:13:55,000
 stealing,

250
00:13:55,000 --> 00:13:58,000
 not realizing there's anything wrong with it.

251
00:13:58,000 --> 00:14:08,000
 Stealing from poor people, for example.

252
00:14:08,000 --> 00:14:12,000
 So how does this relate to our meditation?

253
00:14:12,000 --> 00:14:14,000
 It relates directly to insight meditation,

254
00:14:14,000 --> 00:14:18,000
 because insight meditation is what allows you to see this.

255
00:14:18,000 --> 00:14:21,580
 The first step in insight meditation is not removing of

256
00:14:21,580 --> 00:14:24,000
 greed, anger and delusion.

257
00:14:24,000 --> 00:14:28,190
 It focuses on one aspect of delusion, and that's the wrong

258
00:14:28,190 --> 00:14:29,000
 view aspect.

259
00:14:29,000 --> 00:14:34,220
 The first step towards becoming wise is realizing that you

260
00:14:34,220 --> 00:14:35,000
're a fool.

261
00:14:35,000 --> 00:14:36,000
 That's really it.

262
00:14:36,000 --> 00:14:39,000
 This verse is very important for us at the beginning,

263
00:14:39,000 --> 00:14:42,000
 because this is our first goal, our first step.

264
00:14:42,000 --> 00:14:45,000
 Our first step is to see what we're doing wrong,

265
00:14:45,000 --> 00:14:48,000
 to realize that we're not doing the right things,

266
00:14:48,000 --> 00:14:52,000
 that what we're doing is leading us to suffer.

267
00:14:52,000 --> 00:14:57,000
 In fact, deep down, that's really the final step as well.

268
00:14:57,000 --> 00:15:00,000
 In the beginning, it's intellectual realization,

269
00:15:00,000 --> 00:15:02,000
 but realization that killing is wrong is the other one.

270
00:15:02,000 --> 00:15:07,000
 Eventually, it's just realization that all greed, all anger

271
00:15:07,000 --> 00:15:08,000
 and all delusion,

272
00:15:08,000 --> 00:15:15,000
 is causing us suffering, is harmful to our mind.

273
00:15:15,000 --> 00:15:24,460
 So this constant and systematic observation, introspection,

274
00:15:24,460 --> 00:15:27,000
 and analysis,

275
00:15:27,000 --> 00:15:31,000
 that comes from just reminding ourselves

276
00:15:31,000 --> 00:15:46,000
 and staying objective about our experience.

277
00:15:46,000 --> 00:15:48,000
 This allows us to see what we're doing wrong

278
00:15:48,000 --> 00:15:53,000
 and allows us to change our behavior.

279
00:15:53,000 --> 00:15:56,000
 So in the beginning, to know that we're foolish

280
00:15:56,000 --> 00:15:58,000
 and then to do something about it.

281
00:15:58,000 --> 00:16:01,810
 In fact, it really is deep down, it's the only step that

282
00:16:01,810 --> 00:16:04,000
 needs to be accomplished.

283
00:16:04,000 --> 00:16:06,550
 Every moment that you see that you're doing something

284
00:16:06,550 --> 00:16:07,000
 foolish,

285
00:16:07,000 --> 00:16:10,000
 that what you're doing is truly foolish at that moment.

286
00:16:10,000 --> 00:16:13,000
 When you see that this reaction of yours to things,

287
00:16:13,000 --> 00:16:17,410
 when you react, when you overreact, simple real life

288
00:16:17,410 --> 00:16:18,000
 examples,

289
00:16:18,000 --> 00:16:21,050
 when you get anxious, if I get anxious that I have to talk

290
00:16:21,050 --> 00:16:22,000
 in front of you,

291
00:16:22,000 --> 00:16:24,000
 so I give you this talk and I feel anxious.

292
00:16:24,000 --> 00:16:27,000
 When I see how ridiculous that is,

293
00:16:27,000 --> 00:16:29,720
 how it's not helpful, it's not reasonable, it's not a

294
00:16:29,720 --> 00:16:31,000
 proper response,

295
00:16:31,000 --> 00:16:36,000
 I say that's really a silly thing to do, it's foolish.

296
00:16:36,000 --> 00:16:43,000
 Seeing that, that wisdom, that is what changes my behavior.

297
00:16:43,000 --> 00:16:48,000
 Next time I have no reason to act in that way,

298
00:16:48,000 --> 00:16:51,000
 no reason to set my mind in that direction.

299
00:16:51,000 --> 00:16:53,900
 When you truly realize that it's wrong, your mind doesn't

300
00:16:53,900 --> 00:16:55,000
 go in that direction.

301
00:16:55,000 --> 00:16:59,030
 This is the key to Buddhism, key to the Buddha's teaching,

302
00:16:59,030 --> 00:17:00,000
 this truth,

303
00:17:00,000 --> 00:17:04,000
 that knowledge is really true knowledge, true wisdom,

304
00:17:04,000 --> 00:17:10,000
 true understanding of the mistakes that we're making,

305
00:17:10,000 --> 00:17:24,000
 knowledge of the problem with our reactions.

306
00:17:24,000 --> 00:17:29,400
 This is the way out of suffering, this knowledge, knowledge

307
00:17:29,400 --> 00:17:30,000
 of suffering,

308
00:17:30,000 --> 00:17:34,000
 knowledge that what we're doing is causing us suffering.

309
00:17:34,000 --> 00:17:37,560
 So very important verse, maybe even more important than it

310
00:17:37,560 --> 00:17:38,000
 appears,

311
00:17:38,000 --> 00:17:41,230
 but that's a very catchy one and it's one that we should

312
00:17:41,230 --> 00:17:42,000
 always think of

313
00:17:42,000 --> 00:17:46,990
 and you can flaunt it to people who would say such things

314
00:17:46,990 --> 00:17:47,000
 as

315
00:17:47,000 --> 00:17:55,550
 when you're goody two shoes and who brag and boast about

316
00:17:55,550 --> 00:17:57,000
 their bad deeds,

317
00:17:57,000 --> 00:18:00,630
 their unwholesomeness, their treachery, their trickery and

318
00:18:00,630 --> 00:18:01,000
 so on.

319
00:18:01,000 --> 00:18:06,000
 They shouldn't boast but it's a reminder for people,

320
00:18:06,000 --> 00:18:08,930
 it's a good thing to remind our friends, to remind

321
00:18:08,930 --> 00:18:11,000
 ourselves.

322
00:18:11,000 --> 00:18:14,000
 We don't have to be perfect, not at the beginning.

323
00:18:14,000 --> 00:18:18,000
 The path to perfection is to realize your imperfections.

324
00:18:18,000 --> 00:18:21,000
 The path to become wise is to realize your foolishness.

325
00:18:21,000 --> 00:18:22,000
 That's it.

326
00:18:22,000 --> 00:18:24,000
 That's the path.

327
00:18:24,000 --> 00:18:30,570
 So very important and I think I've about beaten that horse

328
00:18:30,570 --> 00:18:31,000
 dead.

329
00:18:31,000 --> 00:18:33,690
 So there's the verse for tonight. Thank you all for

330
00:18:33,690 --> 00:18:34,000
 listening

331
00:18:34,000 --> 00:18:36,000
 and now we'll do some meditation.

332
00:18:36,000 --> 00:18:38,790
 Wishing you all peace, happiness and freedom from suffering

333
00:18:38,790 --> 00:18:39,000
.

