1
00:00:00,000 --> 00:00:04,000
 Hi and welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:07,770
 So today's question is, how would a layperson begin to

3
00:00:07,770 --> 00:00:09,540
 resolve the conflict created by the

4
00:00:09,540 --> 00:00:13,120
 fact that societies in general expect and to some degree

5
00:00:13,120 --> 00:00:15,100
 demand that the people inside

6
00:00:15,100 --> 00:00:18,310
 them must work a job that usually flies in the face of

7
00:00:18,310 --> 00:00:20,040
 their better judgment?

8
00:00:20,040 --> 00:00:23,640
 This is from Tony in Fremantle.

9
00:00:23,640 --> 00:00:27,380
 Well Tony, I think it's important to understand the

10
00:00:27,380 --> 00:00:30,720
 difference between expectations and demands

11
00:00:30,720 --> 00:00:35,170
 in regards to what society expects and what society demands

12
00:00:35,170 --> 00:00:36,440
 because you'll find that in

13
00:00:36,440 --> 00:00:40,240
 general there's an incredible discrepancy and society

14
00:00:40,240 --> 00:00:42,640
 demands, literally demands a lot

15
00:00:42,640 --> 00:00:47,030
 less of us than we might think but it expects generally a

16
00:00:47,030 --> 00:00:49,500
 lot more than we would like.

17
00:00:49,500 --> 00:00:53,280
 And so you'll find that in order just to get by and just to

18
00:00:53,280 --> 00:00:55,560
 live in society and be a Buddhist

19
00:00:55,560 --> 00:01:00,810
 meditator and live a peaceful life, you don't need to do

20
00:01:00,810 --> 00:01:03,560
 that much and what you'll find

21
00:01:03,560 --> 00:01:07,940
 instead is people will be looking at you funny and you will

22
00:01:07,940 --> 00:01:10,720
 not fit in with society and you'll

23
00:01:10,720 --> 00:01:14,680
 find yourself living alone most of the time.

24
00:01:14,680 --> 00:01:21,080
 That's the truth of being a Buddhist or being a meditator

25
00:01:21,080 --> 00:01:24,320
 is you don't make, you're not

26
00:01:24,320 --> 00:01:28,020
 much fun at parties.

27
00:01:28,020 --> 00:01:30,840
 You live alone and you're looking inside.

28
00:01:30,840 --> 00:01:35,900
 Your way of behaving is not one that is exciting to other

29
00:01:35,900 --> 00:01:39,520
 people because you're contemplating

30
00:01:39,520 --> 00:01:46,160
 yourself, you're looking inside more often than out.

31
00:01:46,160 --> 00:01:50,110
 And so I would resolve the conflict by separating the two,

32
00:01:50,110 --> 00:01:52,400
 by seeing what society demands and

33
00:01:52,400 --> 00:01:55,800
 what demands are placed on you by the people who you

34
00:01:55,800 --> 00:01:58,280
 support, say your parents or your

35
00:01:58,280 --> 00:02:08,930
 family and the demands of law and civil requirements, civil

36
00:02:08,930 --> 00:02:15,280
 contracts and so on that you have.

37
00:02:15,280 --> 00:02:19,610
 And as far as working jobs, you think of the job as being

38
00:02:19,610 --> 00:02:22,080
 something that it's not meant

39
00:02:22,080 --> 00:02:23,360
 to bring meaning.

40
00:02:23,360 --> 00:02:25,680
 The job that you're doing is meant to bring money for you.

41
00:02:25,680 --> 00:02:29,760
 So in the time of the Buddha there was a man who sold pots

42
00:02:29,760 --> 00:02:32,160
 or not in the time of the Buddha,

43
00:02:32,160 --> 00:02:36,970
 the Buddha gave this story of in ancient times a man who

44
00:02:36,970 --> 00:02:39,800
 sold pots and he wouldn't ask for

45
00:02:39,800 --> 00:02:41,000
 money even for the pots.

46
00:02:41,000 --> 00:02:43,070
 He would put these pots on the side of the road and when

47
00:02:43,070 --> 00:02:44,280
 people asked how much is this

48
00:02:44,280 --> 00:02:46,600
 pot, how much is that pot, he would just say, "Oh, you know

49
00:02:46,600 --> 00:02:47,920
, leave some rice, leave some

50
00:02:47,920 --> 00:02:50,600
 beans and take whichever one you like."

51
00:02:50,600 --> 00:02:56,840
 And he lived his life incredibly pure and simple and yet he

52
00:02:56,840 --> 00:02:59,960
 didn't have any problem getting

53
00:02:59,960 --> 00:03:00,960
 by.

54
00:03:00,960 --> 00:03:05,760
 It was sort of a mode of life that most people would find

55
00:03:05,760 --> 00:03:08,840
 perhaps even repulsive but he was

56
00:03:08,840 --> 00:03:14,480
 able to do what was absolutely necessary.

57
00:03:14,480 --> 00:03:19,410
 And another thing you'll find is that if you stick to your

58
00:03:19,410 --> 00:03:24,520
 guns, if you're really set on

59
00:03:24,520 --> 00:03:28,880
 virtue and honesty and purity and as a result feel like you

60
00:03:28,880 --> 00:03:31,320
're being pushed out of certain

61
00:03:31,320 --> 00:03:34,060
 positions and so on, you'll actually find that in the long

62
00:03:34,060 --> 00:03:35,440
 run people respect you more

63
00:03:35,440 --> 00:03:36,440
 for it.

64
00:03:36,440 --> 00:03:40,810
 When you refuse to do certain jobs that undertake certain

65
00:03:40,810 --> 00:03:43,720
 employment that is immoral, you'll

66
00:03:43,720 --> 00:03:47,610
 find that in the short term you do suffer because you have

67
00:03:47,610 --> 00:03:50,040
 to give up certain opportunities

68
00:03:50,040 --> 00:03:52,800
 which immoral people would be able to take advantage of.

69
00:03:52,800 --> 00:03:55,950
 But you'll find in the long run that of course what you get

70
00:03:55,950 --> 00:03:57,880
 is people trust you and you find

71
00:03:57,880 --> 00:04:02,120
 that you're surrounded by people who appreciate you.

72
00:04:02,120 --> 00:04:04,920
 You'll find that people appreciate the work that you do.

73
00:04:04,920 --> 00:04:06,560
 They say good things about you.

74
00:04:06,560 --> 00:04:10,080
 They recommend you to other people and that actually the

75
00:04:10,080 --> 00:04:12,120
 idea that people who are honest

76
00:04:12,120 --> 00:04:15,580
 and moral aren't able to get by in the world is really a

77
00:04:15,580 --> 00:04:17,720
 myth and it's something that is

78
00:04:17,720 --> 00:04:21,450
 of course perpetuated by people who are immoral and who

79
00:04:21,450 --> 00:04:24,400
 think that, who feel perhaps threatened

80
00:04:24,400 --> 00:04:31,030
 by the idea that someone would be able to get by without

81
00:04:31,030 --> 00:04:34,560
 having to do evil things.

82
00:04:34,560 --> 00:04:38,180
 And the other thing is that in the long run you're going to

83
00:04:38,180 --> 00:04:39,920
 want to leave society.

84
00:04:39,920 --> 00:04:44,120
 It's clear everyone since the Buddha's time has said that

85
00:04:44,120 --> 00:04:47,240
 living in society is like a dusty

86
00:04:47,240 --> 00:04:54,940
 road full of crowded with people traveling and coming and

87
00:04:54,940 --> 00:04:55,640
 going.

88
00:04:55,640 --> 00:04:59,760
 And the homeless life or leaving society is like the open

89
00:04:59,760 --> 00:05:00,280
 air.

90
00:05:00,280 --> 00:05:05,180
 It's a life that's free from all of the hustle and bustle

91
00:05:05,180 --> 00:05:08,120
 of the streets and the highways.

92
00:05:08,120 --> 00:05:10,980
 And so if you can't become a monk, if you can't go to live

93
00:05:10,980 --> 00:05:12,440
 in a monastery, you can at

94
00:05:12,440 --> 00:05:18,890
 least begin to become a recluse or to leave society, to

95
00:05:18,890 --> 00:05:23,560
 find the way to look inside yourself.

96
00:05:23,560 --> 00:05:27,440
 If this is your choice is to spend your life contemplating

97
00:05:27,440 --> 00:05:29,400
 the teachings of the Buddha

98
00:05:29,400 --> 00:05:34,620
 or coming to understand more about yourself and more about

99
00:05:34,620 --> 00:05:37,360
 the experiential reality, then

100
00:05:37,360 --> 00:05:39,400
 you do have to start leaving society.

101
00:05:39,400 --> 00:05:47,040
 And so the true resolution of the conflict is to leave the

102
00:05:47,040 --> 00:05:49,400
 battlefield.

103
00:05:49,400 --> 00:05:52,700
 Don't engage in this uphill struggle, what they call a rat

104
00:05:52,700 --> 00:05:53,280
 race.

105
00:05:53,280 --> 00:05:54,920
 You'll leave it behind.

106
00:05:54,920 --> 00:05:56,240
 If you can become a monk, great.

107
00:05:56,240 --> 00:06:00,510
 If not, then at least get involved with Buddhist monaster

108
00:06:00,510 --> 00:06:03,120
ies or meditation centers or start

109
00:06:03,120 --> 00:06:08,610
 a meditation group and build up a circle of friends, a

110
00:06:08,610 --> 00:06:12,120
 circle of companions, people who

111
00:06:12,120 --> 00:06:17,210
 can support you and you can support each other and try to

112
00:06:17,210 --> 00:06:19,520
 put it all behind you.

113
00:06:19,520 --> 00:06:23,830
 In society, in this world, it doesn't seem to have done a

114
00:06:23,830 --> 00:06:26,000
 heck of a lot for the spiritual

115
00:06:26,000 --> 00:06:27,600
 well-being of humankind.

116
00:06:27,600 --> 00:06:32,720
 In fact, it seems the more social we are, the more thick

117
00:06:32,720 --> 00:06:35,560
 and deep our defilements become.

118
00:06:35,560 --> 00:06:39,920
 So you can never hope to change the world.

119
00:06:39,920 --> 00:06:42,860
 You have to look inside and change yourself and by

120
00:06:42,860 --> 00:06:45,760
 extension, everyone around you benefits.

121
00:06:45,760 --> 00:06:48,920
 Okay, so I hope that helps and I wish you all the best on

122
00:06:48,920 --> 00:06:49,800
 your path.

123
00:06:49,800 --> 00:06:50,160
 Good luck.

