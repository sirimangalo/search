1
00:00:00,000 --> 00:00:04,560
 Good evening everyone.

2
00:00:04,560 --> 00:00:11,520
 I'm broadcasting live April 7th.

3
00:00:11,520 --> 00:00:21,920
 Today's quote has actually got quite a bit in it.

4
00:00:21,920 --> 00:00:27,360
 Even though it's a simple message.

5
00:00:27,360 --> 00:00:38,640
 This is from the Ithivuttaka.

6
00:00:38,640 --> 00:00:47,970
 The Buddha says, "Even should one, Sankhatikarni jeepi bhik

7
00:00:47,970 --> 00:00:51,880
ve, even if one should the corner

8
00:00:51,880 --> 00:00:59,200
 of my robe, Gahitva, grasp, take hold of, even if one

9
00:00:59,200 --> 00:01:02,120
 having taken hold of the corner of

10
00:01:02,120 --> 00:01:10,760
 my robe, Pitito, Pitito, Anubhando, Asa, should follow

11
00:01:10,760 --> 00:01:15,760
 after me, Pitito, Pitito, follow behind

12
00:01:15,760 --> 00:01:19,320
 my back, one after the other.

13
00:01:19,320 --> 00:01:33,280
 Pade, Pade, Pada, Padaang, Pade, Padaang, Kipanto, stepping

14
00:01:33,280 --> 00:01:38,680
 in my footsteps, putting

15
00:01:38,680 --> 00:01:41,120
 down his feet in my footsteps."

16
00:01:41,120 --> 00:01:47,440
 It's literally getting about, just about as close as you

17
00:01:47,440 --> 00:01:51,120
 could to the Buddha, following

18
00:01:51,120 --> 00:01:52,120
 him everywhere.

19
00:01:52,120 --> 00:02:04,770
 If he is, Amvijalu, with craving, with great desires, full

20
00:02:04,770 --> 00:02:12,600
 of desire, Tittimbasarago, Kame

21
00:02:12,600 --> 00:02:26,540
 Suttibasarago, with sharp, extreme lust for sensuality, Bia

22
00:02:26,540 --> 00:02:31,000
 Panatito, with a mind full

23
00:02:31,000 --> 00:02:37,910
 of ill will, a mind of ill will, angry at other people,

24
00:02:37,910 --> 00:02:41,080
 hating other people, with only

25
00:02:41,080 --> 00:02:44,600
 bitter thoughts for others.

26
00:02:44,600 --> 00:02:58,290
 Madhuta Manasang, with corrupted mental formations, with

27
00:02:58,290 --> 00:03:07,240
 thoughts that are corrupted, thinking

28
00:03:07,240 --> 00:03:09,590
 to harm others and to manipulate others, to oppress and

29
00:03:09,590 --> 00:03:10,320
 abuse others.

30
00:03:10,320 --> 00:03:18,400
 Muthasati, with confused mindfulness, confused recognition,

31
00:03:18,400 --> 00:03:22,280
 the reaction, reacting to things

32
00:03:22,280 --> 00:03:26,480
 in a confused way, without wisdom, without clarity.

33
00:03:26,480 --> 00:03:33,120
 When things arise, reacting, based on being in the dark,

34
00:03:33,120 --> 00:03:37,240
 based on habit, based on ignorance.

35
00:03:37,240 --> 00:03:41,600
 So it gives rise to greed and anger.

36
00:03:41,600 --> 00:03:50,570
 Asampajano, without full comprehension, without full, well,

37
00:03:50,570 --> 00:03:54,200
 the song is like comprehensive,

38
00:03:54,200 --> 00:04:01,480
 but is full, jaa is knowledge.

39
00:04:01,480 --> 00:04:05,040
 Without full knowledge.

40
00:04:05,040 --> 00:04:11,280
 Asamahito, without concentration or focus, without level-

41
00:04:11,280 --> 00:04:14,440
headedness, so disturbed.

42
00:04:14,440 --> 00:04:21,290
 Their mind is not impartial, objective, able to see things

43
00:04:21,290 --> 00:04:22,840
 as they are.

44
00:04:22,840 --> 00:04:26,900
 As I said, there's a lot in here because it looks like a

45
00:04:26,900 --> 00:04:29,160
 simple quote, but each of these

46
00:04:29,160 --> 00:04:34,240
 words has very important meaning to them.

47
00:04:34,240 --> 00:04:37,880
 Vibhanta-chito, what does that mean?

48
00:04:37,880 --> 00:04:40,880
 It's just more desire, no?

49
00:04:40,880 --> 00:04:50,680
 Vibhanta, a wandering mind, confused mind, Vibhanta, Vibham

50
00:04:50,680 --> 00:04:54,560
ati, to roam, to stray.

51
00:04:54,560 --> 00:04:58,720
 Vibhanta, with a wandering mind, the mind that doesn't stay

52
00:04:58,720 --> 00:05:00,560
 focused, the mind that goes

53
00:05:00,560 --> 00:05:06,000
 hither and thither, thinking about this and that.

54
00:05:06,000 --> 00:05:14,970
 Pakatindryo, with unguarded, untrained, sort of run-of-the-

55
00:05:14,970 --> 00:05:18,960
mill, maybe it doesn't mean

56
00:05:18,960 --> 00:05:25,590
 that, but Pakatim can mean ordinary, but here it means ungu

57
00:05:25,590 --> 00:05:28,880
arded or untrained senses.

58
00:05:28,880 --> 00:05:33,270
 So the senses are our doors by which we experience things,

59
00:05:33,270 --> 00:05:36,440
 but the problem is we don't just experience

60
00:05:36,440 --> 00:05:40,120
 things, we react to them.

61
00:05:40,120 --> 00:05:41,760
 So this is untrained.

62
00:05:41,760 --> 00:05:44,760
 We're based on habit and memory and experience.

63
00:05:44,760 --> 00:05:49,970
 We react to every little thing in a very specific way that

64
00:05:49,970 --> 00:05:53,600
 often is a cause for great suffering

65
00:05:53,600 --> 00:06:01,920
 for us and others, all cause for bad karma.

66
00:06:01,920 --> 00:06:07,830
 So all of these things, so this, imagine this person

67
00:06:07,830 --> 00:06:11,360
 holding on to the Buddha's coat-tails,

68
00:06:11,360 --> 00:06:18,820
 so to speak, walking in his footsteps, but he's got all

69
00:06:18,820 --> 00:06:20,480
 these things.

70
00:06:20,480 --> 00:06:26,200
 He's full of greed, has many desires, has craving or lust

71
00:06:26,200 --> 00:06:29,480
 for sensuality, has ill will,

72
00:06:29,480 --> 00:06:33,920
 has a corrupted thoughts, mental activity.

73
00:06:33,920 --> 00:06:41,860
 He's confused, is with a muddled awareness without

74
00:06:41,860 --> 00:06:47,880
 comprehension and understanding, without

75
00:06:47,880 --> 00:06:55,290
 focus or samahito, without this kind of balanced, calm mind

76
00:06:55,290 --> 00:06:55,320
.

77
00:06:55,320 --> 00:07:01,360
 We bantatito with wandering mind and with untrained

78
00:07:01,360 --> 00:07:03,080
 faculties.

79
00:07:03,080 --> 00:07:04,080
 So what about him?

80
00:07:04,080 --> 00:07:05,080
 What about her?

81
00:07:05,080 --> 00:07:06,080
 Atakoso Arakawa.

82
00:07:06,080 --> 00:07:07,080
 Such a person.

83
00:07:07,080 --> 00:07:08,080
 Well, such a person.

84
00:07:08,080 --> 00:07:09,080
 Arakawa, my home.

85
00:07:09,080 --> 00:07:10,080
 He is very far from me.

86
00:07:10,080 --> 00:07:11,080
 My home to me.

87
00:07:11,080 --> 00:07:12,080
 He's far away from me.

88
00:07:12,080 --> 00:07:13,080
 He's as low very far from me.

89
00:07:13,080 --> 00:07:14,080
 Ahan chatasa, and me from him or her.

90
00:07:14,080 --> 00:07:15,080
 Dangisa heito, what's the cause of that?

91
00:07:15,080 --> 00:07:16,080
 Ahaan chatasa, what's the cause of that?

92
00:07:16,080 --> 00:07:17,080
 Ahaan chatasa, what's the cause of that?

93
00:07:17,080 --> 00:07:18,080
 Ahaan chatasa, what's the cause of that?

94
00:07:18,080 --> 00:07:19,080
 Ahaan chatasa, what's the cause of that?

95
00:07:19,080 --> 00:07:20,080
 Ahaan chatasa, what's the cause of that?

96
00:07:20,080 --> 00:07:21,080
 Ahaan chatasa, what's the cause of that?

97
00:07:21,080 --> 00:07:22,080
 Ahaan chatasa, what's the cause of that?

98
00:07:22,080 --> 00:07:23,080
 Ahaan chatasa, what's the cause of that?

99
00:07:23,080 --> 00:07:24,080
 Ahaan chatasa, what's the cause of that?

100
00:07:24,080 --> 00:07:25,080
 Ahaan chatasa, what's the cause of that?

101
00:07:25,080 --> 00:07:46,920
 Ahaan chatasa, what's the cause of that?

102
00:07:46,920 --> 00:07:51,720
 For the Dhamma, because that Bhikkhu, that monk doesn't see

103
00:07:51,720 --> 00:07:52,120
.

104
00:07:52,120 --> 00:07:55,120
 They don't see the Dhamma.

105
00:07:55,120 --> 00:08:01,120
 Dhammang apasanto namang pasati.

106
00:08:01,120 --> 00:08:11,840
 Not seeing the Dhamma, they don't see me.

107
00:08:11,840 --> 00:08:18,550
 So the meaning here is that, in two ways, having all those

108
00:08:18,550 --> 00:08:19,840
 things is a sign of course

109
00:08:19,840 --> 00:08:21,160
 of not seeing the Dhamma.

110
00:08:21,160 --> 00:08:25,400
 It's really sort of the definition of not seeing the Dhamma

111
00:08:25,400 --> 00:08:27,480
 because seeing the Dhamma

112
00:08:27,480 --> 00:08:36,150
 is to see things as they are and to live life impartially,

113
00:08:36,150 --> 00:08:40,160
 objectively, wisely.

114
00:08:40,160 --> 00:08:43,000
 And all of these things are not that.

115
00:08:43,000 --> 00:08:47,530
 But also the way of explaining it is that these things stop

116
00:08:47,530 --> 00:08:49,480
 us from seeing truth.

117
00:08:49,480 --> 00:08:51,480
 These get in the way.

118
00:08:51,480 --> 00:08:55,480
 You can't see the Dhamma if you have all of these things.

119
00:08:55,480 --> 00:08:59,480
 Maybe you live with the Buddha.

120
00:08:59,480 --> 00:09:04,060
 That's why I had Jan Tong send my teacher send, because he

121
00:09:04,060 --> 00:09:05,480
's very famous, you know,

122
00:09:05,480 --> 00:09:10,480
 in Thailand, or fairly famous in Thailand.

123
00:09:10,480 --> 00:09:13,480
 And I guess now he's very, very famous.

124
00:09:13,480 --> 00:09:18,530
 We are coming to visit him thousands of people on his

125
00:09:18,530 --> 00:09:21,480
 birthday just pouring in.

126
00:09:21,480 --> 00:09:31,470
 And so he said, "Well, going to see noble people, going to

127
00:09:31,470 --> 00:09:37,480
 live with them, or even wait

128
00:09:37,480 --> 00:09:40,860
 on them, or care for them, or attend to them as their

129
00:09:40,860 --> 00:09:42,480
 student, and as their attendant."

130
00:09:42,480 --> 00:09:48,480
 It's not the same as becoming a noble one yourself.

131
00:09:48,480 --> 00:09:50,480
 It's a simple thing to say.

132
00:09:50,480 --> 00:09:58,770
 It's one of those quotes that got published and passed

133
00:09:58,770 --> 00:10:00,480
 around.

134
00:10:00,480 --> 00:10:05,520
 So this is an important quote and an important sort of

135
00:10:05,520 --> 00:10:08,480
 teaching in the context of religion,

136
00:10:08,480 --> 00:10:10,480
 because a lot of people end up doing that.

137
00:10:10,480 --> 00:10:13,840
 They think that by associating with good people, that's

138
00:10:13,840 --> 00:10:14,480
 enough.

139
00:10:14,480 --> 00:10:18,760
 And the Buddha did say that associating with good people is

140
00:10:18,760 --> 00:10:20,480
 the whole of the holy life.

141
00:10:20,480 --> 00:10:24,010
 But the thing about it is that wise people tend to say

142
00:10:24,010 --> 00:10:25,480
 things like this.

143
00:10:25,480 --> 00:10:30,480
 They say, "Look, it's not about following me around.

144
00:10:30,480 --> 00:10:32,480
 It's about changing yourself."

145
00:10:32,480 --> 00:10:35,090
 And of course, the great thing about associating with wise

146
00:10:35,090 --> 00:10:38,480
 people is that they teach these things.

147
00:10:38,480 --> 00:10:42,480
 They push you to become a better person.

148
00:10:42,480 --> 00:10:56,480
 They don't let you walk around holding onto their robes.

149
00:10:56,480 --> 00:10:59,840
 And then there's the idea about being far and being near to

150
00:10:59,840 --> 00:11:03,480
 the Buddha, which kind of

151
00:11:03,480 --> 00:11:15,560
 has greater implications in terms of the idea of space and

152
00:11:15,560 --> 00:11:19,480
 the concepts of reality.

153
00:11:19,480 --> 00:11:23,480
 What is real?

154
00:11:23,480 --> 00:11:31,640
 We think of space as being important, location being

155
00:11:31,640 --> 00:11:32,480
 important,

156
00:11:32,480 --> 00:11:39,110
 when in fact it's really just a part of the impression that

157
00:11:39,110 --> 00:11:41,480
 we have of things.

158
00:11:41,480 --> 00:11:46,480
 Someone on Facebook today, an old friend from high school,

159
00:11:46,480 --> 00:11:49,480
 he posted, he said,

160
00:11:49,480 --> 00:11:54,480
 "There's nothing wrong with my brain.

161
00:11:54,480 --> 00:11:57,910
 The anxiety I'm experiencing is just a reflection of all

162
00:11:57,910 --> 00:11:58,480
 the things that are

163
00:11:58,480 --> 00:12:03,480
 messed up in the world."

164
00:12:03,480 --> 00:12:06,480
 And I replied and I said, "The problem is that the world is

165
00:12:06,480 --> 00:12:08,480
 all in your brain."

166
00:12:08,480 --> 00:12:10,480
 Because it's really true, you know?

167
00:12:10,480 --> 00:12:12,480
 I mean, it's not even in the mind.

168
00:12:12,480 --> 00:12:18,330
 The mind gets everything from the brain, which projects

169
00:12:18,330 --> 00:12:21,480
 everything and creates things.

170
00:12:21,480 --> 00:12:23,370
 Like they did a study that said, when you look at something

171
00:12:23,370 --> 00:12:24,480
, you don't look at the

172
00:12:24,480 --> 00:12:26,480
 whole object.

173
00:12:26,480 --> 00:12:28,630
 So we think in the room around us, there's all these

174
00:12:28,630 --> 00:12:30,480
 objects that we're seeing.

175
00:12:30,480 --> 00:12:33,480
 But in fact, we only see very small bits of it.

176
00:12:33,480 --> 00:12:37,670
 We only see enough to be able to create the picture in our

177
00:12:37,670 --> 00:12:38,480
 minds.

178
00:12:38,480 --> 00:12:44,210
 And they did tests on people that they were able to trick

179
00:12:44,210 --> 00:12:45,480
 them into not being able to

180
00:12:45,480 --> 00:12:47,480
 see something on the pictures.

181
00:12:47,480 --> 00:12:50,060
 They changed something, but you couldn't see it because you

182
00:12:50,060 --> 00:12:50,480
 weren't actually

183
00:12:50,480 --> 00:12:51,480
 seeing the picture.

184
00:12:51,480 --> 00:12:56,480
 You weren't actually seeing.

185
00:12:56,480 --> 00:13:07,180
 The light is only the impetus or the spark, then the inst

186
00:13:07,180 --> 00:13:09,480
igation that leads the mind

187
00:13:09,480 --> 00:13:13,480
 to create the image.

188
00:13:13,480 --> 00:13:18,860
 Anyway, the point of how that relates is the world is

189
00:13:18,860 --> 00:13:21,480
 really all in our heads, all in

190
00:13:21,480 --> 00:13:28,480
 our minds, as you have to say.

191
00:13:28,480 --> 00:13:32,820
 So this is how, I mean, this kind of theory, right, of some

192
00:13:32,820 --> 00:13:34,480
 people would challenge it

193
00:13:34,480 --> 00:13:35,480
 and say that's not true.

194
00:13:35,480 --> 00:13:38,990
 The world does exist out there, whether we experience it or

195
00:13:38,990 --> 00:13:39,480
 not.

196
00:13:39,480 --> 00:13:50,920
 But under this theory, the idea of mind being primary, this

197
00:13:50,920 --> 00:13:52,480
 is how the whole idea of

198
00:13:52,480 --> 00:13:58,480
 rebirth and karma works because we create these experiences

199
00:13:58,480 --> 00:14:00,480
 and create these

200
00:14:00,480 --> 00:14:10,480
 existences. And so it's how we understand our relationships

201
00:14:10,480 --> 00:14:10,480
 with others, why we're

202
00:14:10,480 --> 00:14:17,050
 brought into contact with the people around us, people who

203
00:14:17,050 --> 00:14:24,480
 we meet in life.

204
00:14:24,480 --> 00:14:30,480
 It's because we see with them, where we see them.

205
00:14:30,480 --> 00:14:33,480
 We see in the same way as them.

206
00:14:33,480 --> 00:14:36,440
 And so to be close to the Buddha, the only way to be close

207
00:14:36,440 --> 00:14:37,480
 to the Buddha is to see in

208
00:14:37,480 --> 00:14:42,480
 the way he sees, to see things the way he sees them.

209
00:14:42,480 --> 00:14:46,520
 Because the way you see things, the way you look at things,

210
00:14:46,520 --> 00:14:47,480
 the way you react to

211
00:14:47,480 --> 00:14:49,480
 things, that's the whole of who you are.

212
00:14:49,480 --> 00:14:51,480
 That's what makes you who you are.

213
00:14:51,480 --> 00:14:57,480
 That's what determines your life, your future.

214
00:14:57,480 --> 00:15:00,430
 So really that's all we're doing in meditation, is learning

215
00:15:00,430 --> 00:15:01,480
 to see things the

216
00:15:01,480 --> 00:15:06,480
 way the Buddha saw things, which is as they are.

217
00:15:06,480 --> 00:15:10,760
 Why we call him the Buddha is because he awoke to things as

218
00:15:10,760 --> 00:15:11,480
 they are, where he came

219
00:15:11,480 --> 00:15:15,480
 to understand things as they are.

220
00:15:15,480 --> 00:15:19,720
 We claim that we don't see things as they are, we react to

221
00:15:19,720 --> 00:15:20,480
 them.

222
00:15:20,480 --> 00:15:23,010
 I mean, we see things as they are, of course, but we do

223
00:15:23,010 --> 00:15:24,480
 much more than that.

224
00:15:24,480 --> 00:15:30,090
 As soon as you see something as it is, you get lost in how

225
00:15:30,090 --> 00:15:31,480
 you want it to be,

226
00:15:31,480 --> 00:15:36,880
 how you want it not to be, what you think of it, what it

227
00:15:36,880 --> 00:15:38,480
 makes you think of,

228
00:15:38,480 --> 00:15:46,480
 God in the past, future, not seeing things as they are.

229
00:15:46,480 --> 00:15:50,480
 Because we don't see that, we can't see the Buddha.

230
00:15:50,480 --> 00:15:54,110
 It's very far from the Buddha, where they were right next

231
00:15:54,110 --> 00:15:54,480
 to him.

232
00:15:54,480 --> 00:15:58,640
 And then he says, "Yojana sataye chepiso bhikkhu e bhikkhu

233
00:15:58,640 --> 00:16:01,480
 wihareya."

234
00:16:01,480 --> 00:16:08,480
 Suppose a bhikkhu monk, suppose there was a monk living a

235
00:16:08,480 --> 00:16:09,480
 hundred,

236
00:16:09,480 --> 00:16:15,900
 a hundred yojana, just like a league, a hundred yojana is

237
00:16:15,900 --> 00:16:16,480
 like 16 kilometers

238
00:16:16,480 --> 00:16:20,520
 or something, I don't know, a hundred yojana, a hundred far

239
00:16:20,520 --> 00:16:21,480
 distances.

240
00:16:21,480 --> 00:16:27,480
 So chahouti, and then he is not any of those things.

241
00:16:27,480 --> 00:16:32,830
 So it's anabijadhu, not with many desires, kamisu, natiim

242
00:16:32,830 --> 00:16:36,480
basara, saarago.

243
00:16:36,480 --> 00:16:40,480
 Actually, I'm not sure which one the kamisu goes with.

244
00:16:40,480 --> 00:16:48,480
 Anabijadhu kamisu, maybe it's that way.

245
00:16:48,480 --> 00:16:50,480
 Not with many desires in regards.

246
00:16:50,480 --> 00:16:54,480
 No, I think it's in the kamisu, natiimbasara, no.

247
00:16:54,480 --> 00:17:01,520
 Not with sharp lust, with strong lust in regards to sens

248
00:17:01,520 --> 00:17:02,480
uality.

249
00:17:02,480 --> 00:17:10,480
 Abiyapan, abiyapanachito, abiyapanachito, abiyapanachito,

250
00:17:10,480 --> 00:17:14,480
 without, without ill will.

251
00:17:14,480 --> 00:17:21,480
 Apadur tamanasankapu, with uncorrupted mental formations,

252
00:17:21,480 --> 00:17:33,830
 upatitasati, with established mindfulness, with a mind that

253
00:17:33,830 --> 00:17:35,480
 is able to grasp things.

254
00:17:35,480 --> 00:17:38,970
 There's a way of grasping, when you say in English, we use

255
00:17:38,970 --> 00:17:40,480
 the word grasp in two ways.

256
00:17:40,480 --> 00:17:44,480
 Because if you grasp something, it means you understand it.

257
00:17:44,480 --> 00:17:47,550
 Not like grasping something, like grasping onto something,

258
00:17:47,550 --> 00:17:48,480
 or grasping for something.

259
00:17:48,480 --> 00:17:51,480
 Grasping in general.

260
00:17:51,480 --> 00:17:53,480
 When you grasp something, it means you understand it.

261
00:17:53,480 --> 00:17:55,480
 And that's what we do in the meditation.

262
00:17:55,480 --> 00:17:58,480
 Every moment, trying to grasp it, just for a moment.

263
00:17:58,480 --> 00:18:00,480
 Just grasp that seeing.

264
00:18:00,480 --> 00:18:03,480
 And you grasp it, and you say, "Ah yes, that's seeing."

265
00:18:03,480 --> 00:18:06,550
 Instead of saying, "That's good, that's bad, that's me,

266
00:18:06,550 --> 00:18:07,480
 that's mine."

267
00:18:07,480 --> 00:18:11,480
 And so on.

268
00:18:11,480 --> 00:18:19,870
 Sampajano, fully comprehending, or full awareness, full

269
00:18:19,870 --> 00:18:21,480
 knowledge, full and right knowledge.

270
00:18:21,480 --> 00:18:26,480
 Samahito, well balanced, or well focused.

271
00:18:26,480 --> 00:18:37,480
 Ekangajito, with one point, with a one-pointed mind.

272
00:18:37,480 --> 00:18:48,480
 Sanguttindryo, with trained and restrained senses.

273
00:18:48,480 --> 00:18:52,480
 Atakoso santi kewa mai hang.

274
00:18:52,480 --> 00:18:57,860
 That person is as though in my presence, ahan chatasa, and

275
00:18:57,860 --> 00:18:58,480
 me and his.

276
00:18:58,480 --> 00:19:01,480
 Tangi sehetu, which is the cause.

277
00:19:01,480 --> 00:19:05,480
 Tamang hi so bhikve bhikubhasati.

278
00:19:05,480 --> 00:19:11,960
 For that monk sees the dhamma, dhamma-bhasanto-mang-bhasati

279
00:19:11,960 --> 00:19:12,480
.

280
00:19:12,480 --> 00:19:18,480
 Seeing the dhamma, he sees me.

281
00:19:18,480 --> 00:19:26,480
 There you go.

282
00:19:26,480 --> 00:19:34,480
 That's the dhamma for tonight.

283
00:19:34,480 --> 00:19:37,480
 Does anyone have any questions?

284
00:19:37,480 --> 00:19:40,480
 I think I won't make you come on and hang out.

285
00:19:40,480 --> 00:19:43,480
 I'll just go back to text questions.

286
00:19:43,480 --> 00:19:46,480
 You know what I was thinking?

287
00:19:46,480 --> 00:19:51,170
 What we could do is instead of doing questions and videos

288
00:19:51,170 --> 00:19:52,480
 about their answers,

289
00:19:52,480 --> 00:19:57,050
 is we could set up some kind of page where people could

290
00:19:57,050 --> 00:19:58,480
 vote on topics.

291
00:19:58,480 --> 00:20:01,480
 Just like one-word topics.

292
00:20:01,480 --> 00:20:06,480
 And do a video on this, and people could vote on it.

293
00:20:06,480 --> 00:20:10,480
 And I can do a video on certain topics.

294
00:20:10,480 --> 00:20:14,480
 Maybe that would work well.

295
00:20:14,480 --> 00:20:25,480
 Someone has to make such a page.

296
00:20:25,480 --> 00:20:35,480
 If there's any questions, I'll take text questions tonight.

297
00:20:35,480 --> 00:20:38,480
 meditation.siri-mongoloa.org.

298
00:20:38,480 --> 00:20:45,330
 Those of you over at YouTube, you have to find our

299
00:20:45,330 --> 00:20:47,480
 meditation page.

300
00:20:47,480 --> 00:20:50,480
 And they have to be live, unfortunately.

301
00:20:50,480 --> 00:20:55,860
 I'm not going to go digging back through the past chat and

302
00:20:55,860 --> 00:20:58,480
 find dead comments.

303
00:21:00,480 --> 00:21:22,480
 I'm going to go to the next one.

304
00:21:22,480 --> 00:21:28,480
 About New York, huh?

305
00:21:28,480 --> 00:21:32,480
 Well, I posted some events on Facebook.

306
00:21:32,480 --> 00:21:37,480
 So I shared some events that were shared with me.

307
00:21:37,480 --> 00:21:43,570
 So I think that's what you've got to do, is go to those

308
00:21:43,570 --> 00:21:44,480
 events.

309
00:21:44,480 --> 00:21:47,480
 Let me see.

310
00:21:47,480 --> 00:21:57,480
 There's an April 16th event at Rockaway Beach.

311
00:21:57,480 --> 00:22:02,480
 90-16 Rockaway Beach Boulevard.

312
00:22:02,480 --> 00:22:07,480
 1-1-6-9-3 Rockaway Beach Queens.

313
00:22:07,480 --> 00:22:10,480
 It's at 3 p.m. on April 16th.

314
00:22:10,480 --> 00:22:14,480
 I don't know. I'm just getting the answers you guys are.

315
00:22:14,480 --> 00:22:17,480
 I didn't plan any of this.

316
00:22:17,480 --> 00:22:20,480
 And then I'm doing a neon meditation.

317
00:22:20,480 --> 00:22:30,480
 7 p.m. at Light Bright Neon 232 3rd Street, C102, Brooklyn.

318
00:22:30,480 --> 00:22:37,480
 That's April 19th, 7 p.m.

319
00:22:37,480 --> 00:22:41,480
 I think like 17th and 18th.

320
00:22:41,480 --> 00:22:46,680
 There's other stuff that I'm not actually the teacher all

321
00:22:46,680 --> 00:22:47,480
 these days.

322
00:22:47,480 --> 00:22:51,480
 I don't know, because these are the only ones I got.

323
00:22:51,480 --> 00:22:58,480
 16th, 19th, and the 20th, I don't know, but the 21st.

324
00:22:58,480 --> 00:23:01,480
 That's Dharma Punks, I think.

325
00:23:01,480 --> 00:23:03,480
 No?

326
00:23:03,480 --> 00:23:07,480
 Maybe.

327
00:23:07,480 --> 00:23:11,480
 Yeah, Dharma Punks in Manhattan.

328
00:23:11,480 --> 00:23:13,480
 Don't know, you have to find the Dharma Punks.

329
00:23:13,480 --> 00:23:16,480
 Ah, please.

330
00:23:16,480 --> 00:23:19,480
 A Lila Yoga Dharma and Wellness.

331
00:23:19,480 --> 00:23:22,480
 That's the place, I think.

332
00:23:22,480 --> 00:23:30,480
 And then April 22nd,

333
00:23:30,480 --> 00:23:33,480
 Fort Green, Brooklyn.

334
00:23:33,480 --> 00:23:43,480
 And Lotus Yoga and Vegan Cafe.

335
00:23:43,480 --> 00:23:46,480
 These are all on my page now.

336
00:23:46,480 --> 00:24:05,480
 How is the mind so powerful yet?

337
00:24:05,480 --> 00:24:12,480
 So I think you're trying to say naive.

338
00:24:12,480 --> 00:24:15,480
 Naive is not spelled that way, but that's okay.

339
00:24:15,480 --> 00:24:20,480
 I think you're saying naive.

340
00:24:20,480 --> 00:24:25,180
 Well, powerful doesn't mean that it's in control of the

341
00:24:25,180 --> 00:24:28,480
 power, right?

342
00:24:28,480 --> 00:24:32,860
 The mind is not in control of its own power, that's really

343
00:24:32,860 --> 00:24:35,480
 the problem.

344
00:24:35,480 --> 00:24:39,480
 And powerful doesn't mean good.

345
00:24:39,480 --> 00:24:44,480
 So the mind ends up, I mean, the mind can be very wise,

346
00:24:44,480 --> 00:24:52,480
 you know, and the mind can be very smart, clever, and so on

347
00:24:52,480 --> 00:24:52,480
.

348
00:24:52,480 --> 00:24:56,480
 But it can also be very naive, so it twists itself up

349
00:24:56,480 --> 00:25:02,480
 and ends up creating its own, creating problems for itself

350
00:25:02,480 --> 00:25:05,480
 because it has no direction.

351
00:25:05,480 --> 00:25:13,480
 The mind lacks its training.

352
00:25:13,480 --> 00:25:20,480
 Training is a very specific thing.

353
00:25:20,480 --> 00:25:27,480
 The mind that is just going randomly is very unlikely

354
00:25:27,480 --> 00:25:30,480
 to even think about training itself.

355
00:25:30,480 --> 00:25:34,930
 So as a result, you see that most of us don't think of

356
00:25:34,930 --> 00:25:36,480
 training ourselves.

357
00:25:36,480 --> 00:25:40,010
 I really didn't. I was looking, I went to Asia looking for

358
00:25:40,010 --> 00:25:40,480
 wisdom,

359
00:25:40,480 --> 00:25:44,030
 looking for insight, but didn't think I'd have to work for

360
00:25:44,030 --> 00:25:44,480
 it.

361
00:25:44,480 --> 00:25:46,480
 Most people don't.

362
00:25:46,480 --> 00:25:49,040
 Most people who come to meditate don't even think of it as

363
00:25:49,040 --> 00:25:49,480
 work.

364
00:25:49,480 --> 00:25:53,840
 They think, yeah, meditation, that'll be like a pill I can

365
00:25:53,840 --> 00:25:54,480
 swallow

366
00:25:54,480 --> 00:25:57,990
 and I'll just be, I'll sit down on my mat and poof, I'll be

367
00:25:57,990 --> 00:25:58,480
 happy, right?

368
00:25:58,480 --> 00:26:03,480
 And they're like, oh crap, this is tough.

369
00:26:03,480 --> 00:26:06,730
 A lot of people don't like the meditation that I teach when

370
00:26:06,730 --> 00:26:07,480
 I first teach it.

371
00:26:07,480 --> 00:26:10,480
 You can't teach this to everyone. Not everyone's going to

372
00:26:10,480 --> 00:26:10,480
 appreciate it.

373
00:26:10,480 --> 00:26:13,480
 I'm sure that'll be the case in New York.

374
00:26:13,480 --> 00:26:16,560
 There'll be many people who are not very happy about what I

375
00:26:16,560 --> 00:26:17,480
'm teaching.

376
00:26:17,480 --> 00:26:21,620
 I mean, they won't say it, but it won't stick with most

377
00:26:21,620 --> 00:26:22,480
 people.

378
00:26:22,480 --> 00:26:27,480
 So people want comfort. They want an escape.

379
00:26:27,480 --> 00:26:31,700
 It's hard to get people to grow up and learn to look at

380
00:26:31,700 --> 00:26:33,480
 their problems.

381
00:26:33,480 --> 00:26:38,740
 That's maybe harsh language, but I don't mean, I try and

382
00:26:38,740 --> 00:26:40,480
 don't take that too hard

383
00:26:40,480 --> 00:26:44,480
 and let's not be too critical, but it really is the case.

384
00:26:44,480 --> 00:26:48,480
 I'm not condescending or looking down on such people.

385
00:26:48,480 --> 00:26:52,930
 I mean, I was the same and we're all in this position where

386
00:26:52,930 --> 00:26:53,480
 we just,

387
00:26:53,480 --> 00:26:57,110
 we have to grow up, you know, we have to start really

388
00:26:57,110 --> 00:26:58,480
 taking the bull by the horns

389
00:26:58,480 --> 00:27:02,480
 and stop running away and stop fooling ourselves.

390
00:27:02,480 --> 00:27:06,330
 We're going to be able to just ignore the problems and we

391
00:27:06,330 --> 00:27:08,480
're going to escape our problems.

392
00:27:08,480 --> 00:27:12,270
 We have to really, you know, facing your problems is the

393
00:27:12,270 --> 00:27:13,480
 hardest thing.

394
00:27:13,480 --> 00:27:21,420
 Anyway, kind of went off on the tangent there, but the mind

395
00:27:21,420 --> 00:27:26,480
 is just complex, I suppose.

396
00:27:26,480 --> 00:27:31,480
 And as a result, it kind of does to itself.

397
00:27:31,480 --> 00:27:37,040
 Right. And it's like the ocean is powerful, I suppose you

398
00:27:37,040 --> 00:27:37,480
 could say,

399
00:27:37,480 --> 00:27:40,550
 but most of the time the ocean doesn't, you know, doesn't

400
00:27:40,550 --> 00:27:41,480
 do anything.

401
00:27:41,480 --> 00:27:44,380
 You say the ocean is powerful. What the heck does that mean

402
00:27:44,380 --> 00:27:44,480
?

403
00:27:44,480 --> 00:27:48,810
 You go to the ocean. Ooh, ah, it's not doing anything

404
00:27:48,810 --> 00:27:49,480
 powerful, right?

405
00:27:49,480 --> 00:27:53,480
 But then you have a tsunami, right? Or you have a hurricane

406
00:27:53,480 --> 00:27:53,480
.

407
00:27:53,480 --> 00:27:59,480
 And then you see the power of the ocean.

408
00:27:59,480 --> 00:28:02,470
 When the ocean is in its natural state, the mind in its

409
00:28:02,470 --> 00:28:04,480
 natural state is diffuse.

410
00:28:04,480 --> 00:28:11,480
 It's full of contradictions that use up all its energy.

411
00:28:11,480 --> 00:28:16,180
 So if you, when you train your mind, you have all this

412
00:28:16,180 --> 00:28:17,480
 energy that's very powerful.

413
00:28:17,480 --> 00:28:39,480
 You have to concentrate it.

414
00:28:39,480 --> 00:28:42,860
 Simon, I'm not sure if you're an English native speaker,

415
00:28:42,860 --> 00:28:43,480
 but the word grow,

416
00:28:43,480 --> 00:28:47,840
 the words grow up in English, it's an idiom. It doesn't

417
00:28:47,840 --> 00:28:48,480
 mean get older.

418
00:28:48,480 --> 00:28:55,480
 It means stop being juvenile, stop being immature.

419
00:28:55,480 --> 00:28:58,480
 There's nothing to do with age.

420
00:28:58,480 --> 00:29:12,480
 Grow up means stop being immature.

421
00:29:14,480 --> 00:29:20,260
 Anyway, if that's all, I'll say goodnight. I've still got

422
00:29:20,260 --> 00:29:21,480
 stuff.

423
00:29:21,480 --> 00:29:24,900
 This Sunday we're having a storytelling, and some of you

424
00:29:24,900 --> 00:29:28,480
 know about this storytelling thing we're having on Sunday

425
00:29:28,480 --> 00:29:31,830
 at the spectator auditorium. If any of you are local, you

426
00:29:31,830 --> 00:29:33,480
're welcome to come on over.

427
00:29:33,480 --> 00:29:37,480
 I'll be telling a story from the Jataka about a bird.

428
00:29:37,480 --> 00:29:40,480
 It's the one I told in Second Life recently, I think.

429
00:29:40,480 --> 00:29:46,970
 It's about a bird, a bird that stays by its tree, stays in

430
00:29:46,970 --> 00:29:51,480
 its tree even after the tree kind of dries up, dies.

431
00:29:51,480 --> 00:29:56,480
 And it stays there out of gratitude to the tree.

432
00:29:56,480 --> 00:30:03,550
 The story, the event is about relating religion to the

433
00:30:03,550 --> 00:30:06,480
 environment because there's the issues of climate change.

434
00:30:06,480 --> 00:30:11,570
 How do our religions relate and deal with climate change

435
00:30:11,570 --> 00:30:13,020
 and relate to the environment and protection of the

436
00:30:13,020 --> 00:30:13,480
 environment?

437
00:30:13,480 --> 00:30:19,990
 The moral of this story is about contentment, really, which

438
00:30:19,990 --> 00:30:24,480
 is Buddhism's probably the best way that Buddhism relates

439
00:30:24,480 --> 00:30:26,480
 to environmental protection.

440
00:30:26,480 --> 00:30:34,400
 And so on is not being greedy, not taking more than is

441
00:30:34,400 --> 00:30:37,480
 appropriate, that kind of thing.

442
00:30:37,480 --> 00:30:45,480
 Not going to seek out and also to be appreciative.

443
00:30:45,480 --> 00:30:49,470
 So the idea of appreciating the environment, this bird

444
00:30:49,470 --> 00:30:55,270
 stays with the tree and appreciates it and is content with

445
00:30:55,270 --> 00:30:55,480
 it.

446
00:30:55,480 --> 00:31:00,380
 So it's about the Buddha tells the story to a monk who gets

447
00:31:00,380 --> 00:31:04,480
 kind of upset because a village burns down.

448
00:31:04,480 --> 00:31:07,480
 So you're staying and depending on his village for the

449
00:31:07,480 --> 00:31:10,480
 rains retreat, the village burned down, I think.

450
00:31:10,480 --> 00:31:15,480
 So it was very hard for him to get food.

451
00:31:15,480 --> 00:31:17,870
 And so he told us the Buddha and the Buddha said, oh, you

452
00:31:17,870 --> 00:31:19,480
 know, don't just go buy food.

453
00:31:19,480 --> 00:31:24,720
 Food isn't a good indicator when you get the requisite. So

454
00:31:24,720 --> 00:31:29,480
 it's not a good indicator of what a good place is.

455
00:31:29,480 --> 00:31:35,480
 If you can meditate well there, you should stay there.

456
00:31:35,480 --> 00:31:39,150
 And then he told the story of this bird that stayed with

457
00:31:39,150 --> 00:31:42,480
 this tree because it was it had it was content.

458
00:31:42,480 --> 00:31:45,760
 Even when the tree died anyway, it's a little bit longer

459
00:31:45,760 --> 00:31:46,480
 than that.

460
00:31:46,480 --> 00:31:50,400
 You have to hear me tell the story, but it's just the

461
00:31:50,400 --> 00:31:51,480
 simple thing.

462
00:31:51,480 --> 00:31:53,480
 So I got to prepare for that kind of.

463
00:31:53,480 --> 00:31:57,480
 And then I got two exams next week and then I'm done.

464
00:31:57,480 --> 00:32:00,480
 And then off to New York.

465
00:32:00,480 --> 00:32:03,770
 And then it's already almost May and then in May we've got

466
00:32:03,770 --> 00:32:07,480
 one event, the Buddha's birthday in Mississauga.

467
00:32:07,480 --> 00:32:12,480
 And then in June I'm off to Asia for a month.

468
00:32:12,480 --> 00:32:17,480
 And then back here for the rains.

469
00:32:17,480 --> 00:32:21,160
 Lots of people interested in coming to meditate all the way

470
00:32:21,160 --> 00:32:22,480
 into like November,

471
00:32:22,480 --> 00:32:29,950
 which is a bit awkward because I can't think that far ahead

472
00:32:29,950 --> 00:32:30,480
.

473
00:32:30,480 --> 00:32:34,940
 I mean, because in the past I've had trouble thinking far

474
00:32:34,940 --> 00:32:35,480
 ahead.

475
00:32:35,480 --> 00:32:37,480
 Now it seems pretty stable.

476
00:32:37,480 --> 00:32:41,470
 So maybe we can start tentatively accepting long term

477
00:32:41,470 --> 00:32:42,480
 reservations.

478
00:32:42,480 --> 00:32:46,480
 I just hesitant to do so because plans change.

479
00:32:46,480 --> 00:32:49,480
 The future is uncertain.

480
00:32:49,480 --> 00:32:53,480
 I could end up moving back to Asia or something.

481
00:32:53,480 --> 00:32:57,480
 Not likely.

482
00:32:57,480 --> 00:33:02,160
 I think we can start to be a little more accepting of

483
00:33:02,160 --> 00:33:06,480
 future plans of the sort these days.

484
00:33:06,480 --> 00:33:10,480
 But we maybe need a bigger place.

485
00:33:10,480 --> 00:33:14,540
 We have to start thinking about finding a bigger house in

486
00:33:14,540 --> 00:33:15,480
 the area.

487
00:33:15,480 --> 00:33:20,120
 I know it's not nice to have to move, but this house really

488
00:33:20,120 --> 00:33:20,480
 isn't.

489
00:33:20,480 --> 00:33:22,480
 I mean, it's fairly big.

490
00:33:22,480 --> 00:33:24,480
 It's just not well laid out for meditations.

491
00:33:24,480 --> 00:33:27,700
 And there are houses close to the university that have like

492
00:33:27,700 --> 00:33:28,480
 six bedrooms

493
00:33:28,480 --> 00:33:31,670
 because they were built and they were renovated to house

494
00:33:31,670 --> 00:33:32,480
 students.

495
00:33:32,480 --> 00:33:36,170
 So you're trying to fit as many students in houses as you

496
00:33:36,170 --> 00:33:36,480
 can,

497
00:33:36,480 --> 00:33:40,480
 which kind of fits our situation.

498
00:33:40,480 --> 00:33:45,480
 We got six bedroom houses that are fairly reasonable.

499
00:33:45,480 --> 00:33:49,480
 We should maybe talk about that anyway.

500
00:33:49,480 --> 00:33:54,480
 Anyway, that's all for tonight.

501
00:33:54,480 --> 00:33:56,480
 Thank you guys.

502
00:33:56,480 --> 00:33:57,480
 Thanks for coming out.

503
00:33:57,480 --> 00:33:59,480
 Have a good night.

