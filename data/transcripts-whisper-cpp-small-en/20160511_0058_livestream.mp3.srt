1
00:00:00,000 --> 00:00:07,000
 Evening everyone.

2
00:00:07,000 --> 00:00:21,000
 I'm broadcasting live May 10th.

3
00:00:21,000 --> 00:00:36,800
 Tonight's quote is on the Buddha, the Dhamma, and the San

4
00:00:36,800 --> 00:00:38,000
gha.

5
00:00:38,000 --> 00:00:52,000
 It's from the Terigatha, the verses of the elder monks.

6
00:00:52,000 --> 00:01:01,250
 We have recorded verses that were spoken by the various a

7
00:01:01,250 --> 00:01:10,000
rahants as poetry and reflection on their attainments.

8
00:01:10,000 --> 00:01:23,710
 We have stories behind each one of these. This one is from

9
00:01:23,710 --> 00:01:25,000
 a monk called Tehkichakari.

10
00:01:25,000 --> 00:01:33,000
 The one who does "Kicha".

11
00:01:33,000 --> 00:01:38,000
 Let's see if we can look at this.

12
00:01:38,000 --> 00:01:44,400
 He was the son of a Brahmin Subandhu, so called because he

13
00:01:44,400 --> 00:01:50,400
 was brought safely into the world with the aid of

14
00:01:50,400 --> 00:01:53,000
 physicians.

15
00:01:53,000 --> 00:02:02,000
 I don't understand the word.

16
00:02:02,000 --> 00:02:07,610
 When he was grown up, his father incurred the jealousy and

17
00:02:07,610 --> 00:02:14,000
 suspicion of a minister, Chanaka, Chanaka, in Chandaguta,

18
00:02:14,000 --> 00:02:18,000
 who had him through. His father was put in prison.

19
00:02:18,000 --> 00:02:22,350
 And so the son, Tehkichakari, in his fright fled, and

20
00:02:22,350 --> 00:02:25,000
 taking refuge with a forest-dwelling monk,

21
00:02:25,000 --> 00:02:37,290
 entered the order and dwelt in the open air, never sleeping

22
00:02:37,290 --> 00:02:44,000
 and heedless of heat and cold.

23
00:02:44,000 --> 00:02:57,380
 Many are the reasons why people become monks. Nagasena says

24
00:02:57,380 --> 00:02:59,000
, King Melinda asks him, "Why did you become a monk?"

25
00:02:59,000 --> 00:03:02,000
 He says, "Did you become a monk to become an arah?"

26
00:03:02,000 --> 00:03:06,760
 He says, "Oh, I was young." He kind of embarrassed the way

27
00:03:06,760 --> 00:03:08,000
 he says it.

28
00:03:08,000 --> 00:03:15,340
 Some people become monks for the wrong reason, sometimes

29
00:03:15,340 --> 00:03:16,000
 for the wrong reason,

30
00:03:16,000 --> 00:03:21,000
 but then find the right reason after they've ordained.

31
00:03:21,000 --> 00:03:27,990
 But it's an important part of society, I think, to have

32
00:03:27,990 --> 00:03:36,000
 this institution,

33
00:03:36,000 --> 00:03:48,990
 this option for people who are keen, who are at the stage,

34
00:03:48,990 --> 00:03:50,000
 at a point in their lives,

35
00:03:50,000 --> 00:04:00,040
 or in their journey in samsara, that they are able and

36
00:04:00,040 --> 00:04:02,000
 willing,

37
00:04:02,000 --> 00:04:09,990
 that they are turned off by the world, having this ability

38
00:04:09,990 --> 00:04:19,000
 to choose to dedicate yourself to meditation

39
00:04:19,000 --> 00:04:27,390
 and study and teaching of spiritual truths and the path to

40
00:04:27,390 --> 00:04:30,000
 enlightenment and so on.

41
00:04:30,000 --> 00:04:34,290
 I mean, not only are these sort of people important, people

42
00:04:34,290 --> 00:04:38,000
 who have the time and the energy and the knowledge

43
00:04:38,000 --> 00:04:45,150
 and the practice to teach, but also for those who are

44
00:04:45,150 --> 00:04:51,000
 seeking it, those who can't find a place in the world,

45
00:04:51,000 --> 00:05:02,350
 who have an earnest wish to become a spiritual person and

46
00:05:02,350 --> 00:05:08,000
 to live a reclusive life.

47
00:05:08,000 --> 00:05:10,760
 Anyway, so this monk's verses are about the Buddha, the D

48
00:05:10,760 --> 00:05:12,000
hamma and the Sangha.

49
00:05:12,000 --> 00:05:20,000
 He says, "Buddham apamayyam anusara passanau"

50
00:05:20,000 --> 00:05:29,410
 One should reflect with a good heart, with a pure heart on

51
00:05:29,410 --> 00:05:35,000
 the Buddha who is immeasurable.

52
00:05:35,000 --> 00:05:44,000
 "Pitya puta saree roho hi siddh sattattamudagoh"

53
00:05:44,000 --> 00:05:59,370
 One's whole being puta means like permeated, with rapture,

54
00:05:59,370 --> 00:06:03,000
 with exaltation, with this excitement,

55
00:06:03,000 --> 00:06:07,000
 this energy, this rapture.

56
00:06:07,000 --> 00:06:09,000
 "Ho hi siddh sattattamudagoh"

57
00:06:09,000 --> 00:06:16,000
 One will always be uplifted, will be constantly uplifted.

58
00:06:16,000 --> 00:06:19,200
 The same with the Dhamma and the Sangha, these are just

59
00:06:19,200 --> 00:06:22,000
 reflecting on the Buddha, the Dhamma and the Sangha,

60
00:06:22,000 --> 00:06:28,350
 just thinking about these things, just reminiscing or rever

61
00:06:28,350 --> 00:06:30,000
ing.

62
00:06:30,000 --> 00:06:33,720
 Just that, not even practicing it, they're so powerful that

63
00:06:33,720 --> 00:06:39,000
 even worshipping them is of great benefit.

64
00:06:39,000 --> 00:06:47,700
 It makes you uplifted, undago, undago, makes you uplifted,

65
00:06:47,700 --> 00:06:51,000
 sattattam, continuously.

66
00:06:51,000 --> 00:06:55,050
 So if you keep the Buddha, the Dhamma and the Sangha in

67
00:06:55,050 --> 00:06:58,000
 mind, you will never be depressed.

68
00:06:58,000 --> 00:07:03,360
 Once I told this story several times about this woman in

69
00:07:03,360 --> 00:07:09,000
 the US who was in a very bad situation.

70
00:07:09,000 --> 00:07:12,560
 So she was doing some meditation, but she was basically

71
00:07:12,560 --> 00:07:15,000
 trapped in somebody else's house,

72
00:07:15,000 --> 00:07:19,270
 not physically, but for all intents and purposes quite

73
00:07:19,270 --> 00:07:21,000
 trapped where she was,

74
00:07:21,000 --> 00:07:26,260
 in a very bad situation, and they were very fundamentalist,

75
00:07:26,260 --> 00:07:31,000
 non-Buddhist religious people.

76
00:07:31,000 --> 00:07:38,080
 So I told her also to recite some chanting to the Buddha in

77
00:07:38,080 --> 00:07:40,000
 the Dhamma and the Sangha, just short chants.

78
00:07:40,000 --> 00:07:43,060
 She had a Buddha image and she was doing this, and then the

79
00:07:43,060 --> 00:07:46,000
 family confiscated the Buddha image.

80
00:07:46,000 --> 00:07:50,280
 But the power of it, they were going to destroy this Buddha

81
00:07:50,280 --> 00:07:54,000
 image, but the power of her practice

82
00:07:54,000 --> 00:07:59,830
 and her devotion to the Buddha, coupled with their hatred

83
00:07:59,830 --> 00:08:03,000
 and vilification of the Buddha,

84
00:08:03,000 --> 00:08:05,560
 she had to find someone to take this Buddha image away or

85
00:08:05,560 --> 00:08:07,000
 they were going to destroy it.

86
00:08:07,000 --> 00:08:11,920
 So she found someone to take it, and when they came to take

87
00:08:11,920 --> 00:08:15,000
 it, they talked to her.

88
00:08:15,000 --> 00:08:20,040
 Out of mutual appreciation, they found a job for her and

89
00:08:20,040 --> 00:08:21,000
 she moved.

90
00:08:21,000 --> 00:08:23,480
 All because of this Buddha image and because of her

91
00:08:23,480 --> 00:08:26,000
 practice, she was able to, by chance,

92
00:08:26,000 --> 00:08:32,860
 meet these people who were also, I guess, Buddhist or at

93
00:08:32,860 --> 00:08:37,000
 least amicable towards Buddhism,

94
00:08:37,000 --> 00:08:42,000
 and was able to leave the house right away.

95
00:08:42,000 --> 00:08:46,480
 It was quite an inspiring story, all because of her

96
00:08:46,480 --> 00:08:50,000
 chanting, indirectly, of course.

97
00:08:50,000 --> 00:08:55,000
 Oil and water can't mix good and evil.

98
00:08:55,000 --> 00:09:04,000
 A hard time staying in the same house.

99
00:09:04,000 --> 00:09:06,570
 So it's important to, this is one of the protective med

100
00:09:06,570 --> 00:09:08,000
itations, meditating on the Buddha,

101
00:09:08,000 --> 00:09:10,000
 the Dhamma and the Sangha.

102
00:09:10,000 --> 00:09:13,000
 It protects you, keeps you safe.

103
00:09:13,000 --> 00:09:22,000
 It keeps your mind strong and vibrant, ecstatic, energetic.

104
00:09:22,000 --> 00:09:25,000
 Something worth doing.

105
00:09:25,000 --> 00:09:28,000
 Worth reading about the Buddha, reading about his life,

106
00:09:28,000 --> 00:09:31,200
 worth going to India to see where the Buddha lived, all

107
00:09:31,200 --> 00:09:34,000
 these things to make the Buddha

108
00:09:34,000 --> 00:09:43,000
 part of your life and a part of your conscious experience.

109
00:09:43,000 --> 00:09:48,000
 So anyway, not too much to say about that.

110
00:09:48,000 --> 00:09:51,000
 Do we have questions tonight?

111
00:09:51,000 --> 00:10:00,000
 See a bunch of comments.

112
00:10:00,000 --> 00:10:03,530
 I'm going to skip questions that don't have the cue in

113
00:10:03,530 --> 00:10:04,000
 front,

114
00:10:04,000 --> 00:10:07,290
 because otherwise it just makes it so much easier to skim

115
00:10:07,290 --> 00:10:08,000
 through.

116
00:10:08,000 --> 00:10:11,310
 So they don't have the special cue in front, special

117
00:10:11,310 --> 00:10:12,000
 question mark.

118
00:10:12,000 --> 00:10:14,000
 I just want to answer.

119
00:10:14,000 --> 00:10:17,000
 Well, I won't go looking for it.

120
00:10:17,000 --> 00:10:20,000
 And Bobo says I said his question was stupid.

121
00:10:20,000 --> 00:10:23,000
 I didn't really say that, did I?

122
00:10:23,000 --> 00:10:26,000
 Maybe I joked about it or something.

123
00:10:26,000 --> 00:10:30,000
 Do you ever read great works from later Buddhist masters

124
00:10:30,000 --> 00:10:32,000
 like Nagarjuna?

125
00:10:32,000 --> 00:10:35,000
 No, not so much.

126
00:10:35,000 --> 00:10:41,000
 But I have heard good things about Nagarjuna, so...

127
00:10:41,000 --> 00:10:43,000
 I wouldn't be against reading it.

128
00:10:43,000 --> 00:10:45,380
 I mean, I think that's the sort of Mahayana stuff that I'd

129
00:10:45,380 --> 00:10:48,000
 actually be interested in.

130
00:10:48,000 --> 00:10:55,590
 As opposed to East Asian Mahayana stuff, which is quite unp

131
00:10:55,590 --> 00:10:58,000
alatable to me.

132
00:10:58,000 --> 00:11:01,640
 I imagine I'd probably have some critical, be fairly

133
00:11:01,640 --> 00:11:07,000
 critical of Nagarjuna personally, but I don't know.

134
00:11:07,000 --> 00:11:12,570
 I'm sure I'd find some of it impressive, as I understand he

135
00:11:12,570 --> 00:11:17,000
 was a fairly impressive person.

136
00:11:17,000 --> 00:11:22,000
 No other questions?

137
00:11:22,000 --> 00:11:27,000
 I had a lot last night, we must have gone through them all.

138
00:11:27,000 --> 00:11:31,000
 Michael tells me, one of my videos goes on Reddit.

139
00:11:31,000 --> 00:11:38,000
 It got on Reddit in the past few days or something.

140
00:11:38,000 --> 00:11:42,000
 And it's very highly ranked or something like that.

141
00:11:42,000 --> 00:11:48,000
 Under the meditation subreddit.

142
00:11:48,000 --> 00:11:51,000
 Why did Ananda take so long to become an Arahant?

143
00:11:51,000 --> 00:11:55,000
 Because he was very busy with looking after the Buddha.

144
00:11:55,000 --> 00:11:58,290
 He spent all his time caring for the Buddha that he didn't

145
00:11:58,290 --> 00:12:00,000
 actually meditate much.

146
00:12:00,000 --> 00:12:02,750
 And he was concerned about this himself, but the Buddha

147
00:12:02,750 --> 00:12:03,000
 said,

148
00:12:03,000 --> 00:12:10,710
 "You'll be concerned after I attain Bhairni Bhaṇṇa, you

149
00:12:10,710 --> 00:12:12,000
 will become an Arahant."

150
00:12:12,000 --> 00:12:17,000
 What's the name of the chanting you mentioned?

151
00:12:17,000 --> 00:12:30,000
 Well, reflection on the Buddha, the Dhamma and the Sangha.

152
00:12:30,000 --> 00:12:32,220
 So if you look at any good Buddhist, Theravada Buddhist

153
00:12:32,220 --> 00:12:33,000
 chanting book,

154
00:12:33,000 --> 00:12:38,000
 you'll find these three things, "Ithipiso, Swagato, Sūtāt

155
00:12:38,000 --> 00:12:39,000
ipāṇu."

156
00:12:39,000 --> 00:12:42,000
 If you want to look it up, it's in the Dajjika Sutta.

157
00:12:42,000 --> 00:12:45,490
 It's actually from a sutta, so the Buddha actually recited

158
00:12:45,490 --> 00:12:49,000
 these as an example for us.

159
00:12:49,000 --> 00:12:55,990
 The top of the banner or the banner protection or something

160
00:12:55,990 --> 00:12:56,000
.

161
00:12:56,000 --> 00:13:07,000
 Dajjanga.

162
00:13:07,000 --> 00:13:09,000
 Little Buddha, yes, I did see it.

163
00:13:09,000 --> 00:13:13,000
 And in fact, when I was before I was a monk,

164
00:13:13,000 --> 00:13:20,470
 my lay teacher had me stitch together the Buddha story

165
00:13:20,470 --> 00:13:24,000
 parts of Little Buddha.

166
00:13:24,000 --> 00:13:27,000
 So it has a good Buddha story of the Buddha.

167
00:13:27,000 --> 00:13:30,000
 But again, that movie stops as soon as he becomes a Buddha.

168
00:13:30,000 --> 00:13:34,500
 And so it's like very, so very Mahayana in a sense that

169
00:13:34,500 --> 00:13:36,000
 doesn't care about the Buddha in his life.

170
00:13:36,000 --> 00:13:39,350
 It's all about how to become a Buddha, which is

171
00:13:39,350 --> 00:13:42,000
 disappointing from the point of view of those of us

172
00:13:42,000 --> 00:13:46,000
 who appreciate the 45 years that came after.

173
00:13:46,000 --> 00:13:53,670
 Much more, not much more, but consider it to be actually

174
00:13:53,670 --> 00:13:57,000
 much more important.

175
00:13:57,000 --> 00:14:00,840
 You know, they always skipped the important part from my

176
00:14:00,840 --> 00:14:02,000
 point of view.

177
00:14:02,000 --> 00:14:16,000
 [silence]

178
00:14:16,000 --> 00:14:22,040
 Yeah, the Buddha story part was not entirely according to

179
00:14:22,040 --> 00:14:28,000
 the texts, but it wasn't bad.

180
00:14:28,000 --> 00:14:32,710
 I mean, I'm a little older, I'm 37 yesterday, so 20 years

181
00:14:32,710 --> 00:14:35,000
 ago, isn't all that.

182
00:14:35,000 --> 00:14:40,000
 But I guess I must have seen it after I became Buddhist.

183
00:14:40,000 --> 00:14:57,000
 So sometime in 2000, 2001 probably.

184
00:14:57,000 --> 00:15:09,000
 [silence]

185
00:15:09,000 --> 00:15:12,000
 All right, well let's call it there.

186
00:15:12,000 --> 00:15:16,000
 Wish you all a good night, good practice.

187
00:15:16,000 --> 00:15:19,000
 Oh, I got one more.

188
00:15:19,000 --> 00:15:21,570
 I was hoping you would address the discussion of doing

189
00:15:21,570 --> 00:15:23,000
 deeds without expecting in return

190
00:15:23,000 --> 00:15:26,000
 in regards to last night's quote of doers rejoicing.

191
00:15:26,000 --> 00:15:29,000
 I did, didn't I? Last night I talked about it.

192
00:15:29,000 --> 00:15:35,000
 Maybe you disagree, but you can do it again.

193
00:15:35,000 --> 00:15:38,430
 If you do deeds without expecting anything in return, it's,

194
00:15:38,430 --> 00:15:41,000
 you know, what good is it?

195
00:15:41,000 --> 00:15:48,000
 Why are you doing them?

196
00:15:48,000 --> 00:15:54,000
 I guess I suppose an arahant acts in that way,

197
00:15:54,000 --> 00:15:57,000
 but that's because they've done what needs to be done.

198
00:15:57,000 --> 00:16:00,490
 For anyone who hasn't done what needs to be done, they have

199
00:16:00,490 --> 00:16:02,000
 to do things expecting a result,

200
00:16:02,000 --> 00:16:04,000
 looking for a result, you know, purposefully.

201
00:16:04,000 --> 00:16:06,000
 So doing good deeds is purposeful.

202
00:16:06,000 --> 00:16:10,440
 An arahant has no purpose, so they do deeds without, but

203
00:16:10,440 --> 00:16:14,000
 they don't purposefully do,

204
00:16:14,000 --> 00:16:19,000
 they just do things as a matter of course.

205
00:16:19,000 --> 00:16:21,510
 Or you could put it that way, that they do deeds without

206
00:16:21,510 --> 00:16:22,000
 expecting anything,

207
00:16:22,000 --> 00:16:25,520
 but that's the way of someone who's already done what needs

208
00:16:25,520 --> 00:16:29,000
 to be done, kattankar niyam.

209
00:16:29,000 --> 00:16:33,020
 We have not yet done what needs to be done, so that's why

210
00:16:33,020 --> 00:16:36,000
 we do good deeds.

211
00:16:36,000 --> 00:16:38,000
 But that's not even really the point of the quote.

212
00:16:38,000 --> 00:16:41,200
 The quote isn't that you do, isn't saying that you do

213
00:16:41,200 --> 00:16:43,000
 something expecting to rejoice,

214
00:16:43,000 --> 00:16:46,000
 it's saying that when you do good deeds, you do rejoice.

215
00:16:46,000 --> 00:16:49,000
 And it's not quite even, yeah, I mean you feel happy.

216
00:16:49,000 --> 00:16:56,680
 Happiness comes from doing good deeds whether you want it

217
00:16:56,680 --> 00:16:58,000
 or not.

218
00:16:58,000 --> 00:17:02,180
 And that's what it's saying, it's not saying you should do

219
00:17:02,180 --> 00:17:06,000
 good deeds wanting to feel happy.

220
00:17:06,000 --> 00:17:09,000
 It's saying if you do good deeds, you feel happy.

221
00:17:09,000 --> 00:17:13,930
 I mean it's a way of explaining the difference between good

222
00:17:13,930 --> 00:17:15,000
 and evil.

223
00:17:15,000 --> 00:17:20,000
 Evil leads to suffering, idasocchiti, peychasocchiti.

224
00:17:20,000 --> 00:17:28,890
 They weep, they sorrow here, they're sad here, they're sad

225
00:17:28,890 --> 00:17:32,000
 in the next life.

226
00:17:32,000 --> 00:17:40,000
 Those who do evil deeds.

227
00:17:40,000 --> 00:17:44,000
 Maybe that clears it up a little.

228
00:17:44,000 --> 00:17:45,000
 You're welcome.

229
00:17:45,000 --> 00:17:47,000
 I didn't really call your question stupid, did I?

230
00:17:47,000 --> 00:17:50,000
 I don't do that.

231
00:17:50,000 --> 00:17:59,000
 I know I can be insensitive sometimes, I apologize.

232
00:17:59,000 --> 00:18:03,000
 I blame it all on my parents, I didn't go to public school.

233
00:18:03,000 --> 00:18:06,030
 I was home schooled through my childhood, so I don't know

234
00:18:06,030 --> 00:18:08,000
 how to deal with people.

235
00:18:08,000 --> 00:18:23,000
 I've never been good with social mores and norms.

236
00:18:23,000 --> 00:18:27,000
 I didn't say it was either, I don't believe you.

237
00:18:27,000 --> 00:18:35,000
 I think you're making this up.

238
00:18:35,000 --> 00:18:38,000
 It's in the past, so I'm not going to dwell.

239
00:18:38,000 --> 00:18:41,000
 I just should you.

240
00:18:41,000 --> 00:18:43,000
 Okay, good night everyone.

241
00:18:43,000 --> 00:18:48,000
 Thank you all for tuning in, wishing you all the best.

242
00:18:50,000 --> 00:18:51,000
 Thank you.

243
00:18:52,000 --> 00:18:53,000
 Thank you.

244
00:18:54,000 --> 00:18:55,000
 Thank you.

