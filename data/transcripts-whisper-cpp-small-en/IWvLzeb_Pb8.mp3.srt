1
00:00:00,000 --> 00:00:05,000
 Okay, so this is a question that's fairly practical.

2
00:00:05,000 --> 00:00:09,470
 What did the Buddha, and what do you or other teachers

3
00:00:09,470 --> 00:00:12,200
 recommend in order to generate greater motivation to

4
00:00:12,200 --> 00:00:17,000
 practice when one's dedication and enthusiasm is waning?

5
00:00:17,000 --> 00:00:22,000
 I think the biggest thing that the Buddha recommended...

6
00:00:22,000 --> 00:00:25,000
 No, I don't even know if it's the Buddha...

7
00:00:25,000 --> 00:00:28,620
 The biggest thing that I can think of, and it's actually

8
00:00:28,620 --> 00:00:32,530
 from the commentary, is to associate yourself with people

9
00:00:32,530 --> 00:00:35,000
 who have the qualities that you lack.

10
00:00:35,000 --> 00:00:38,460
 I mean, this is based very much on the Buddha's famous sut

11
00:00:38,460 --> 00:00:43,130
ta where he tells Ananda that living with association with

12
00:00:43,130 --> 00:00:47,670
 good people is the entirety of the holy life, the entirety

13
00:00:47,670 --> 00:00:49,000
 of the spiritual life.

14
00:00:49,000 --> 00:00:51,610
 Ananda says he figures it's about half, and the Buddha says

15
00:00:51,610 --> 00:00:57,000
, "Shaman, you don't say that, it's the entirety of it."

16
00:00:57,000 --> 00:01:04,000
 So, association and...

17
00:01:04,000 --> 00:01:10,130
 That doesn't just mean talking with them, but even hanging

18
00:01:10,130 --> 00:01:14,990
 around such people will lead you to cultivate the same

19
00:01:14,990 --> 00:01:17,000
 sorts of states.

20
00:01:17,000 --> 00:01:20,450
 So, what do I do with my students? I mean, you encourage

21
00:01:20,450 --> 00:01:21,000
 them.

22
00:01:21,000 --> 00:01:27,000
 It often depends very much on what's getting in their way.

23
00:01:27,000 --> 00:01:30,720
 It can be doubts. If doubts are getting in their way, then

24
00:01:30,720 --> 00:01:32,000
 they need an explanation.

25
00:01:32,000 --> 00:01:34,880
 I mean, or sorry, if it's intellectual doubts, then they

26
00:01:34,880 --> 00:01:36,000
 need an explanation.

27
00:01:36,000 --> 00:01:39,680
 Or they need an answer, something that at least tells them,

28
00:01:39,680 --> 00:01:43,630
 that shows them that their intellectual pursuits are

29
00:01:43,630 --> 00:01:46,000
 useless and unbeneficial.

30
00:01:46,000 --> 00:01:52,020
 If it's doubt about themselves, or feelings of discour

31
00:01:52,020 --> 00:01:55,150
agement, doubt about their practice, like, "My practice is

32
00:01:55,150 --> 00:01:58,000
 not going well," or, "I'm useless, I'm not good at this,"

33
00:01:58,000 --> 00:02:02,270
 then they need some reassurance, and they need an argument,

34
00:02:02,270 --> 00:02:04,000
 you know, to...

35
00:02:04,000 --> 00:02:08,000
 A way of helping them to see things in another way.

36
00:02:08,000 --> 00:02:14,000
 Discussion is very important.

37
00:02:14,000 --> 00:02:18,400
 If, or often just a Dhamma talk, giving them some kind of

38
00:02:18,400 --> 00:02:20,000
 encouragement.

39
00:02:20,000 --> 00:02:24,940
 So, if they feel discouraged in the sense, like, they aren

40
00:02:24,940 --> 00:02:30,520
't so interested in meditation, then you have to give them a

41
00:02:30,520 --> 00:02:32,900
 talk explaining about the things that they are interested

42
00:02:32,900 --> 00:02:33,000
 in,

43
00:02:33,000 --> 00:02:37,060
 and showing that those things are perhaps not as meaningful

44
00:02:37,060 --> 00:02:41,570
 as they thought they were, and talking about the importance

45
00:02:41,570 --> 00:02:43,000
 of meditation.

46
00:02:43,000 --> 00:02:48,330
 So, Dhamma talks can help, you know, talks about, you know,

47
00:02:48,330 --> 00:02:52,810
 the benefits of renouncing and giving up, and the

48
00:02:52,810 --> 00:02:57,000
 disadvantages of clinging,

49
00:02:57,000 --> 00:03:03,000
 talks that are encouraging, discussion that is encouraging.

50
00:03:03,000 --> 00:03:05,430
 You know, the best thing, of course, is to meditate,

51
00:03:05,430 --> 00:03:08,000
 because meditation helps you want to meditate more.

52
00:03:08,000 --> 00:03:13,500
 The more you meditate, the more you become interested in it

53
00:03:13,500 --> 00:03:17,000
, the more benefit you see from it.

54
00:03:17,000 --> 00:03:21,090
 Anybody else? What do you think motivates people? What do

55
00:03:21,090 --> 00:03:29,000
 we do to motivate ourselves and motivate others?

56
00:03:29,000 --> 00:03:39,000
 What do you do when you don't want to practice?

57
00:03:39,000 --> 00:03:43,050
 I think, like, when it comes to sloth and torpor rising

58
00:03:43,050 --> 00:03:46,130
 into mind, a couple of simple things that you can do to

59
00:03:46,130 --> 00:03:49,030
 overcome those, to give yourself a little bit more

60
00:03:49,030 --> 00:03:50,000
 motivation.

61
00:03:50,000 --> 00:03:55,730
 Like, go out into cold air, some water on your face, some

62
00:03:55,730 --> 00:04:04,370
 changing positions during meditation, or if you're not

63
00:04:04,370 --> 00:04:12,620
 sitting formally, you can just put your awareness on the

64
00:04:12,620 --> 00:04:15,000
 tiredness, on the lazy feeling,

65
00:04:15,000 --> 00:04:19,650
 on the feeling of not wanting to meditate, things like that

66
00:04:19,650 --> 00:04:20,000
.

67
00:04:20,000 --> 00:04:23,580
 Yeah, I don't know so much about, you know, if you're just

68
00:04:23,580 --> 00:04:25,960
 tired, because people can really want to meditate even

69
00:04:25,960 --> 00:04:30,000
 though they're tired, but it can be very discouraging.

70
00:04:30,000 --> 00:04:35,010
 And I think it's more the discouragement that's being asked

71
00:04:35,010 --> 00:04:36,000
 over here.

72
00:04:36,000 --> 00:04:38,490
 You know, how do you deal with being discouraged? The best

73
00:04:38,490 --> 00:04:41,380
 thing is to have a teacher and to have a community, because

74
00:04:41,380 --> 00:04:43,000
 when you do, all of these things will come,

75
00:04:43,000 --> 00:04:48,000
 you'll have talks, you'll have discussions and so on.

76
00:04:48,000 --> 00:04:50,840
 But one interesting thing about what you're talking about

77
00:04:50,840 --> 00:04:52,000
 is tricks, right?

78
00:04:52,000 --> 00:04:54,000
 Sometimes you have tricks to make yourself more motivated.

79
00:04:54,000 --> 00:04:57,440
 Vayak actually hears commenting about doing Metta

80
00:04:57,440 --> 00:05:00,000
 meditation, which is, yeah, another good trick.

81
00:05:00,000 --> 00:05:03,720
 Do something to give you encouragement. Think about the

82
00:05:03,720 --> 00:05:04,000
 Buddha.

83
00:05:04,000 --> 00:05:05,790
 We talked about this yesterday, right? Something that's

84
00:05:05,790 --> 00:05:08,000
 very encouraging is to think about the Buddha.

85
00:05:08,000 --> 00:05:11,360
 The Buddha said, "Whenever you feel afraid, you're off in

86
00:05:11,360 --> 00:05:14,000
 the forest and suddenly you feel afraid."

87
00:05:14,000 --> 00:05:17,490
 Say to yourself, "Ithipisova Bhagavat," think about the

88
00:05:17,490 --> 00:05:19,000
 Buddha and the fear will go away immediately, and it does.

89
00:05:19,000 --> 00:05:24,330
 It's quite impressive how the thinking about the Buddha can

90
00:05:24,330 --> 00:05:27,870
 make you fearless, because you start realizing how fearless

91
00:05:27,870 --> 00:05:37,000
, remembering how fearless you are.

92
00:05:37,000 --> 00:05:43,280
 Okay, if anybody wants to pick through the comments, wait,

93
00:05:43,280 --> 00:05:44,000
 stop this one.

