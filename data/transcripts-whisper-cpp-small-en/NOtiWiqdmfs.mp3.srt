1
00:00:00,000 --> 00:00:04,920
 Welcome back to Ask a Monk. Next question comes from Dilong

2
00:00:04,920 --> 00:00:07,000
 on Deck who asks

3
00:00:07,000 --> 00:00:11,000
 "Do you ever miss the former life you had in the States?

4
00:00:11,000 --> 00:00:12,160
 Sometimes do you ever have

5
00:00:12,160 --> 00:00:15,820
 the urges or desires to partake in things most Americans

6
00:00:15,820 --> 00:00:17,240
 consider normal?

7
00:00:17,240 --> 00:00:21,050
 Like say going out to eat at a restaurant or maybe just

8
00:00:21,050 --> 00:00:24,080
 going to a movie?"

9
00:00:25,080 --> 00:00:30,940
 First of all the funny thing is I'm not American and the

10
00:00:30,940 --> 00:00:35,000
 only life of any length

11
00:00:35,000 --> 00:00:38,530
 that I've known in America is my life as a monk which I don

12
00:00:38,530 --> 00:00:40,320
't particularly miss

13
00:00:40,320 --> 00:00:43,930
 that's not I think your question but you know as a monk in

14
00:00:43,930 --> 00:00:45,360
 America I was arrested

15
00:00:45,360 --> 00:00:48,840
 and put in jail and accosted by the police and so on so it

16
00:00:48,840 --> 00:00:50,640
 was a difficult

17
00:00:50,640 --> 00:00:54,840
 life but I think you're asking more about my life in Canada

18
00:00:54,840 --> 00:00:56,760
 because I'm from Canada.

19
00:00:56,760 --> 00:01:01,440
 So in general I don't like to answer questions about my

20
00:01:01,440 --> 00:01:02,600
 personal life or my

21
00:01:02,600 --> 00:01:07,750
 personal path and so on I don't think that's really the

22
00:01:07,750 --> 00:01:09,880
 point but the point of

23
00:01:09,880 --> 00:01:13,280
 this question I guess is in regards to what to do about

24
00:01:13,280 --> 00:01:14,440
 these urges and

25
00:01:14,440 --> 00:01:19,260
 curiosity of what a monk does about these things and how a

26
00:01:19,260 --> 00:01:20,200
 monk overcomes

27
00:01:20,200 --> 00:01:25,280
 these or a meditator and I think that's key the the only

28
00:01:25,280 --> 00:01:26,800
 reason that I'm doing

29
00:01:26,800 --> 00:01:29,610
 what I'm doing now is because I started practicing

30
00:01:29,610 --> 00:01:30,960
 meditation and I think

31
00:01:30,960 --> 00:01:35,120
 without meditation you could never overcome these these

32
00:01:35,120 --> 00:01:35,800
 desires these

33
00:01:35,800 --> 00:01:40,550
 urges wanting to do useless things wanting to do things

34
00:01:40,550 --> 00:01:43,120
 that only bring

35
00:01:43,120 --> 00:01:48,230
 ephemeral pleasures but but in the end leave you only

36
00:01:48,230 --> 00:01:50,840
 wanting more and with a

37
00:01:50,840 --> 00:01:57,420
 muddled and confused state of mind you know the urges come

38
00:01:57,420 --> 00:01:59,600
 up but I think

39
00:01:59,600 --> 00:02:04,040
 clearly through the practice of meditation you are able to

40
00:02:04,040 --> 00:02:05,120
 separate the

41
00:02:05,120 --> 00:02:15,830
 parts of the attachment or the desire you know a lot for me

42
00:02:15,830 --> 00:02:16,000
 a lot of the old things

43
00:02:16,000 --> 00:02:24,400
 that I used to do you know I guess sort of what was normal

44
00:02:24,400 --> 00:02:24,880
 but more the

45
00:02:24,880 --> 00:02:28,040
 things that I was passionate about let's put it that way

46
00:02:28,040 --> 00:02:31,440
 from myself I was a rock

47
00:02:31,440 --> 00:02:37,920
 punk guitarist rock guitarist in a band I was into rock

48
00:02:37,920 --> 00:02:40,080
 climbing and some sports

49
00:02:40,080 --> 00:02:46,250
 and and so on and it was really difficult to give those up

50
00:02:46,250 --> 00:02:47,800
 but on the one hand but

51
00:02:47,800 --> 00:02:52,170
 also incredibly easy it was interesting because at the on

52
00:02:52,170 --> 00:02:53,160
 the one hand there's

53
00:02:53,160 --> 00:02:57,410
 this desire that comes up to to do these things and on the

54
00:02:57,410 --> 00:02:59,000
 other hand I think

55
00:02:59,000 --> 00:03:02,580
 that's a condition phenomenon that's something that I've

56
00:03:02,580 --> 00:03:03,440
 you know developed

57
00:03:03,440 --> 00:03:07,730
 over the years it's the same as anger it's the same as many

58
00:03:07,730 --> 00:03:09,240
 of our negative

59
00:03:09,240 --> 00:03:15,900
 or you know un wholesome emotions it's something that that

60
00:03:15,900 --> 00:03:16,840
 doesn't lead to you

61
00:03:16,840 --> 00:03:19,060
 know just because you have it doesn't mean you have to

62
00:03:19,060 --> 00:03:19,880
 follow it doesn't mean

63
00:03:19,880 --> 00:03:22,870
 there's any benefit in following it and this is what we

64
00:03:22,870 --> 00:03:24,080
 really miss we think

65
00:03:24,080 --> 00:03:27,170
 that when anger arises it means someone's done something

66
00:03:27,170 --> 00:03:28,800
 bad to you and therefore

67
00:03:28,800 --> 00:03:32,690
 you have to get back at them or or attack or you know do

68
00:03:32,690 --> 00:03:34,160
 whatever it takes

69
00:03:34,160 --> 00:03:39,270
 to get rid of the unpleasant situation and the same goes

70
00:03:39,270 --> 00:03:41,000
 for our desires we

71
00:03:41,000 --> 00:03:46,850
 have been trained and conditioned to believe that our

72
00:03:46,850 --> 00:03:49,040
 desires are a perfectly

73
00:03:49,040 --> 00:03:52,940
 valid reason for us to chase after things if you want

74
00:03:52,940 --> 00:03:55,040
 something that's a reason

75
00:03:55,040 --> 00:03:58,840
 for you to go and get it through the meditation practice

76
00:03:58,840 --> 00:04:01,320
 this changes because

77
00:04:01,320 --> 00:04:05,390
 you you know you look at it I mean it's really quite

78
00:04:05,390 --> 00:04:08,040
 obvious actually that you

79
00:04:08,040 --> 00:04:12,100
 know the the desire is leading you to attachment and

80
00:04:12,100 --> 00:04:14,000
 addiction why would you

81
00:04:14,000 --> 00:04:19,480
 want to cultivate that clearly it's a conditioned state

82
00:04:19,480 --> 00:04:21,080
 that is wrong that is

83
00:04:21,080 --> 00:04:26,750
 un unprofitable and so instead of following it you don't

84
00:04:26,750 --> 00:04:28,280
 repress it but

85
00:04:28,280 --> 00:04:32,520
 you you you look at it and you you know you stay with it so

86
00:04:32,520 --> 00:04:33,720
 you want something

87
00:04:33,720 --> 00:04:37,330
 and yeah you want something and this is something that is

88
00:04:37,330 --> 00:04:38,080
 very difficult it's

89
00:04:38,080 --> 00:04:42,110
 it's the key to your life and you know you're not going to

90
00:04:42,110 --> 00:04:42,680
 be able to do it

91
00:04:42,680 --> 00:04:44,720
 it's very difficult it's it's the key to the Buddhist

92
00:04:44,720 --> 00:04:46,200
 teaching is to be able to

93
00:04:46,200 --> 00:04:51,440
 see the one thing to understand it to see that the the the

94
00:04:51,440 --> 00:04:53,880
 phenomenon for what

95
00:04:53,880 --> 00:04:56,330
 it is it's something that has arisen and it's something

96
00:04:56,330 --> 00:04:58,000
 that will cease rather

97
00:04:58,000 --> 00:05:02,450
 than then extrapolating upon it and saying therefore I have

98
00:05:02,450 --> 00:05:03,360
 to this I have

99
00:05:03,360 --> 00:05:06,670
 to that I have to chase after it it's something that I

100
00:05:06,670 --> 00:05:07,960
 think most people don't

101
00:05:07,960 --> 00:05:11,300
 want to do but that's based on their conditioning I think

102
00:05:11,300 --> 00:05:12,360
 if we were honest

103
00:05:12,360 --> 00:05:16,620
 with ourselves we would you know and that's the thing can

104
00:05:16,620 --> 00:05:18,840
 you be truly and

105
00:05:18,840 --> 00:05:23,360
 and completely honest with yourself which most of us can't

106
00:05:23,360 --> 00:05:26,920
 we're we're you

107
00:05:26,920 --> 00:05:30,560
 know we're perfectly happy to dilute ourselves that we that

108
00:05:30,560 --> 00:05:31,240
 were somehow

109
00:05:31,240 --> 00:05:36,060
 meeting with happiness and so and this is because we don't

110
00:05:36,060 --> 00:05:37,680
 have the ability to

111
00:05:37,680 --> 00:05:41,920
 the clarity of mind the focus of mind the ability to see

112
00:05:41,920 --> 00:05:43,400
 things as they are

113
00:05:43,400 --> 00:05:47,670
 based on our lack of meditation practice this is why in the

114
00:05:47,670 --> 00:05:48,720
 beginning it's very

115
00:05:48,720 --> 00:05:51,980
 important to do an intensive meditation course as opposed

116
00:05:51,980 --> 00:05:53,040
 to you know doing a

117
00:05:53,040 --> 00:05:58,160
 little bit at a time because the intensity of it not the

118
00:05:58,160 --> 00:05:58,920
 intensity but

119
00:05:58,920 --> 00:06:04,210
 the intensive nature that it's it's completely meditative

120
00:06:04,210 --> 00:06:05,720
 allows you to pull

121
00:06:05,720 --> 00:06:11,540
 yourself up out of the mire of the conditioning and allow

122
00:06:11,540 --> 00:06:12,760
 you to come up

123
00:06:12,760 --> 00:06:17,350
 and onto dry land so to speak and and be able to look down

124
00:06:17,350 --> 00:06:18,920
 so that when the

125
00:06:18,920 --> 00:06:21,650
 conditions come up you're able to be might be more

126
00:06:21,650 --> 00:06:23,200
 objective because for most

127
00:06:23,200 --> 00:06:27,120
 of us it's it's just impossible the desire comes up and we

128
00:06:27,120 --> 00:06:27,680
're gone already

129
00:06:27,680 --> 00:06:32,680
 we have no skills in this in regards to seeing things

130
00:06:32,680 --> 00:06:34,600
 clearly as they are that's

131
00:06:34,600 --> 00:06:38,770
 that's what the meditation practice is for that's why it's

132
00:06:38,770 --> 00:06:41,040
 so important so I

133
00:06:41,040 --> 00:06:45,640
 hope that helps I think that's I've said talked about it

134
00:06:45,640 --> 00:06:46,600
 before the the key is

135
00:06:46,600 --> 00:06:50,540
 breaking it up into its components and being able to

136
00:06:50,540 --> 00:06:52,760
 separate them that this

137
00:06:52,760 --> 00:06:56,660
 one is the problem this one is not a problem when you when

138
00:06:56,660 --> 00:06:58,160
 you want something

139
00:06:58,160 --> 00:07:01,400
 you know it's it's a problem in the sense that it's

140
00:07:01,400 --> 00:07:03,120
 suffering but until you

141
00:07:03,120 --> 00:07:08,540
 act upon it it's not going to cause it's not going to cause

142
00:07:08,540 --> 00:07:09,840
 addiction you know if

143
00:07:09,840 --> 00:07:13,730
 you if you watch the wanting the pleasure and then the the

144
00:07:13,730 --> 00:07:14,720
 stress that is

145
00:07:14,720 --> 00:07:20,120
 involved and so on then it's not really a problem you come

146
00:07:20,120 --> 00:07:21,440
 to see that it's a

147
00:07:21,440 --> 00:07:24,470
 problem you will come to see oh this is not good and you

148
00:07:24,470 --> 00:07:26,160
 you'll let go of it and

149
00:07:26,160 --> 00:07:31,440
 you won't give rise to it anymore that's really the key and

150
00:07:31,440 --> 00:07:32,920
 and and this this

151
00:07:32,920 --> 00:07:37,700
 realization I guess before the the wanting goes away is the

152
00:07:37,700 --> 00:07:39,080
 realization

153
00:07:39,080 --> 00:07:42,620
 that that it's only wanting this is the first goal in the

154
00:07:42,620 --> 00:07:44,160
 meditation practice

155
00:07:44,160 --> 00:07:46,730
 it's not the final rule but the first goal is the

156
00:07:46,730 --> 00:07:48,480
 realization that these

157
00:07:48,480 --> 00:07:55,120
 states that we have inside are not a reason to to to act it

158
00:07:55,120 --> 00:07:57,200
's not good for us

159
00:07:57,200 --> 00:08:01,440
 to act upon them the realization that and just because I

160
00:08:01,440 --> 00:08:02,520
 want something doesn't

161
00:08:02,520 --> 00:08:08,820
 mean I should go and get it the wanting the wanting of

162
00:08:08,820 --> 00:08:09,520
 things is not a good

163
00:08:09,520 --> 00:08:15,600
 reason to chase after them and and which leads to this

164
00:08:15,600 --> 00:08:17,160
 ability to to examine the

165
00:08:17,160 --> 00:08:22,880
 wanting and to come to and to teach yourself the true

166
00:08:22,880 --> 00:08:24,240
 nature of the desires

167
00:08:24,240 --> 00:08:28,450
 and therefore to let them go peacefully it's not about

168
00:08:28,450 --> 00:08:30,000
 repression it's not like

169
00:08:30,000 --> 00:08:33,550
 I still want to go rock climbing but I'm just repressing it

170
00:08:33,550 --> 00:08:35,760
 it's you know coming

171
00:08:35,760 --> 00:08:42,160
 to realize that desire for anything is really quite useless

172
00:08:42,160 --> 00:08:43,360
 it doesn't lead to

173
00:08:43,360 --> 00:08:49,220
 peace and happiness and freedom from suffering difficult

174
00:08:49,220 --> 00:08:51,720
 and probably

175
00:08:51,720 --> 00:08:55,450
 undesirable for most people but that's why most people go

176
00:08:55,450 --> 00:08:56,720
 around and around

177
00:08:56,720 --> 00:09:02,690
 and around that's the point really so thanks for the

178
00:09:02,690 --> 00:09:05,480
 question all the best

