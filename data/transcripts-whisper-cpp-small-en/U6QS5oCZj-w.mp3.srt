1
00:00:00,000 --> 00:00:04,030
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,030 --> 00:00:05,200
 Today we continue on with

3
00:00:05,200 --> 00:00:10,480
 verse number 107 which reads as follows

4
00:00:10,480 --> 00:00:16,640
 "Yaucha wasa satang jantu aging pari ca rehvane ekansha bh

5
00:00:16,640 --> 00:00:21,440
avitatanam muhutampi pu jayee saayee

6
00:00:21,440 --> 00:00:31,760
 vapu jana sayyau yan jay wasa satang utang" which means

7
00:00:32,800 --> 00:00:40,800
 and one might and a being no and whatever being might

8
00:00:40,800 --> 00:00:46,720
 aging pari ca rehvane live in the forest

9
00:00:46,720 --> 00:00:51,480
 tending to a fire so one might live in a forest for a

10
00:00:51,480 --> 00:00:52,320
 hundred years

11
00:00:52,320 --> 00:00:57,680
 "ekansha bhavitatanam" this is the same as yesterday's

12
00:00:57,680 --> 00:00:58,320
 verse

13
00:01:00,000 --> 00:01:04,920
 "muhutampi pu jayee" one should pay homage to one of the

14
00:01:04,920 --> 00:01:06,400
 one who has developed themselves

15
00:01:06,400 --> 00:01:14,510
 just for one moment "sayee vapu jana sayyau" this puja is

16
00:01:14,510 --> 00:01:18,480
 greater greater than the one who

17
00:01:18,480 --> 00:01:24,460
 for a hundred years practices this sacrifice fire sacrifice

18
00:01:24,460 --> 00:01:27,760
 now not a not a terribly

19
00:01:28,720 --> 00:01:33,140
 um meaningful verse for those of us who aren't familiar

20
00:01:33,140 --> 00:01:36,720
 with living in the forest tending to a

21
00:01:36,720 --> 00:01:40,400
 fire but we can generalize and talk about it the story as

22
00:01:40,400 --> 00:01:42,800
 well is quite similar to yesterday's

23
00:01:42,800 --> 00:01:46,240
 it's almost identical the difference here is the saree puta

24
00:01:46,240 --> 00:01:48,400
 instead of going to talk to his uncle

25
00:01:48,400 --> 00:01:50,560
 he goes and talks to his his nephew

26
00:01:53,440 --> 00:01:56,980
 and he asks his nephew so nephew his nephew is also a brah

27
00:01:56,980 --> 00:01:59,440
min his whole family of course was brahmin

28
00:01:59,440 --> 00:02:05,990
 he says what sort of king brahmina gusudang karosi what

29
00:02:05,990 --> 00:02:09,280
 sort of wholesomeness or good deeds do you do

30
00:02:09,280 --> 00:02:14,380
 and so he says oh every month "mase mase ekang ekang pass

31
00:02:14,380 --> 00:02:17,520
ung" every month i kill a

32
00:02:18,800 --> 00:02:25,490
 having killed a passu a livestock like a pet of some sort

33
00:02:25,490 --> 00:02:28,560
 some kind of of domesticated animal

34
00:02:28,560 --> 00:02:34,260
 or some animal a sheep maybe a goat probably "agimbari par

35
00:02:34,260 --> 00:02:40,800
icharami" i use it to tend to a fire

36
00:02:40,800 --> 00:02:45,010
 or burn it in a fire maybe make a burnt offering it sounds

37
00:02:45,010 --> 00:02:47,280
 like he just burns it in a fire

38
00:02:48,480 --> 00:02:54,500
 kind of an abramaic sort of practice it's what the jew the

39
00:02:54,500 --> 00:02:58,480
 jewish people would do it's very similar

40
00:02:58,480 --> 00:03:04,230
 but fire sacrifice seems to have originally been burning uh

41
00:03:04,230 --> 00:03:06,080
 goats and so on

42
00:03:06,080 --> 00:03:12,700
 all over the world somehow this was a a big thing to do and

43
00:03:12,700 --> 00:03:15,360
 then he says uh

44
00:03:16,800 --> 00:03:25,950
 for what purpose do you do that and he says oh for the same

45
00:03:25,950 --> 00:03:27,520
 as before oh for the

46
00:03:27,520 --> 00:03:34,800
 brahmaloka mago kiriso i have heard or it is said that this

47
00:03:34,800 --> 00:03:37,920
 is the path to the brahmaloka

48
00:03:37,920 --> 00:03:42,170
 and sahri puta says who told you that same as yesterday

49
00:03:42,170 --> 00:03:43,600
 same as last time

50
00:03:44,800 --> 00:03:49,050
 and again it was my teachers so for the first one it was

51
00:03:49,050 --> 00:03:51,360
 for last one it was easy to kind of get a

52
00:03:51,360 --> 00:03:54,990
 sense that his teachers were these naked ascetics last time

53
00:03:54,990 --> 00:03:57,120
 right he was giving every month giving a

54
00:03:57,120 --> 00:04:00,630
 thousand pieces of money worth of food or whatever maybe

55
00:04:00,630 --> 00:04:04,720
 even just money directly to these naked

56
00:04:04,720 --> 00:04:08,270
 ascetics and so one would think that they were probably his

57
00:04:08,270 --> 00:04:10,480
 teachers who were teaching in this

58
00:04:10,480 --> 00:04:13,250
 now in this case the question is you know why are his

59
00:04:13,250 --> 00:04:15,040
 teachers teaching such things

60
00:04:15,040 --> 00:04:19,520
 there's various reasons and we can extrapolate this um to

61
00:04:19,520 --> 00:04:21,920
 sort of generalize in terms of

62
00:04:21,920 --> 00:04:25,510
 religion why do people teach such ridiculous things i mean

63
00:04:25,510 --> 00:04:27,600
 it's it's understandable why we

64
00:04:27,600 --> 00:04:31,550
 why people believe them we just tend to believe things

65
00:04:31,550 --> 00:04:34,320
 based on tradition or based on the authority

66
00:04:34,320 --> 00:04:37,760
 of our teachers even though we ourselves have no sense of

67
00:04:37,760 --> 00:04:40,880
 the causal relationship so this is why

68
00:04:40,880 --> 00:04:45,310
 religions do such a wide array of practices because we're

69
00:04:45,310 --> 00:04:47,520
 just told to buy our priests

70
00:04:47,520 --> 00:04:50,670
 or buy our leaders the question is why do the leaders teach

71
00:04:50,670 --> 00:04:51,360
 such things

72
00:04:51,360 --> 00:04:57,950
 sometimes it's just for gain i mean i've seen it even in

73
00:04:57,950 --> 00:05:00,000
 buddhism people come to

74
00:05:00,800 --> 00:05:08,010
 the monasteries the temples and they ask the teacher and

75
00:05:08,010 --> 00:05:09,200
 what should i do someone passed

76
00:05:09,200 --> 00:05:12,250
 away what should i do and i've seen teachers you can just i

77
00:05:12,250 --> 00:05:13,920
've seen monks you can just see

78
00:05:13,920 --> 00:05:16,960
 there the wheels turning in their head kind of kind of in a

79
00:05:16,960 --> 00:05:18,560
 bit of panic like oh uh

80
00:05:18,560 --> 00:05:22,440
 scrambling scrambling to find something that that looks or

81
00:05:22,440 --> 00:05:26,080
 sounds kind of mystical i mean it really

82
00:05:26,080 --> 00:05:29,810
 comes down to that just making something up on the spur of

83
00:05:29,810 --> 00:05:32,960
 the moment to appease people's desire

84
00:05:32,960 --> 00:05:37,990
 for some ritual something to solve their problems uh sri d

85
00:05:37,990 --> 00:05:43,520
hammika a very um wonderful sort of

86
00:05:43,520 --> 00:05:49,570
 populist uh buddhist monk and teacher from sri lanka he

87
00:05:49,570 --> 00:05:52,560
 said someone asked him about these

88
00:05:52,560 --> 00:05:55,210
 amulets that monks give out and he said well they have

89
00:05:55,210 --> 00:05:57,600
 absolutely no meaning but he said sometimes

90
00:05:57,600 --> 00:06:02,050
 sometimes for people who uh who are new or who don't really

91
00:06:02,050 --> 00:06:04,000
 have a good understanding of cause

92
00:06:04,000 --> 00:06:07,620
 and effect you have to give them something so he took a pen

93
00:06:07,620 --> 00:06:10,480
 he says you see this i want you to keep

94
00:06:10,480 --> 00:06:12,830
 it hold on to it and it will keep you safe he said

95
00:06:12,830 --> 00:06:15,680
 sometimes you have to do that that's a sort

96
00:06:15,680 --> 00:06:18,940
 of like a like dumbo and his feather if you ever saw the

97
00:06:18,940 --> 00:06:21,440
 disney movie sometimes people need their

98
00:06:21,440 --> 00:06:27,520
 magical feather so there is there is a defense of it but um

99
00:06:27,520 --> 00:06:32,000
 i don't think it's a very strong defense

100
00:06:32,000 --> 00:06:36,420
 especially when it becomes one's uh main absolutely no

101
00:06:36,420 --> 00:06:39,360
 defense when it becomes one's main religious

102
00:06:39,360 --> 00:06:45,590
 practice and um not only is there no defense but it's it's

103
00:06:45,590 --> 00:06:49,840
 uh blameworthy when it involves unwholesome

104
00:06:49,840 --> 00:06:52,490
 clearly unwholesome acts like it's one thing to tell people

105
00:06:52,490 --> 00:06:52,720
 that

106
00:06:52,720 --> 00:06:58,650
 um offering food to a statue is going to be to their

107
00:06:58,650 --> 00:07:00,800
 benefit or pouring water

108
00:07:00,800 --> 00:07:04,830
 on the root of a tree is going to to do this or do that i

109
00:07:04,830 --> 00:07:08,640
 mean that's kind of innocent and who

110
00:07:08,640 --> 00:07:13,220
 knows maybe the angels will get involved and and help out

111
00:07:13,220 --> 00:07:15,440
 with it if they if they you know there's

112
00:07:15,440 --> 00:07:21,560
 this interesting thing uh buddhist in sri lanka are very

113
00:07:21,560 --> 00:07:25,360
 keen to pour water at the roots of the

114
00:07:25,360 --> 00:07:28,510
 bode tree so the the poor water to to water the bode tree

115
00:07:28,510 --> 00:07:30,640
 they'll go around it's a very important

116
00:07:30,640 --> 00:07:35,020
 ceremony and they do this with the understanding that it uh

117
00:07:35,020 --> 00:07:38,320
 leads to pregnancy and it apparently

118
00:07:38,320 --> 00:07:41,650
 has some measure of success i mean it's anecdotal and

119
00:07:41,650 --> 00:07:44,400
 scientists may be able to study it and find

120
00:07:44,400 --> 00:07:48,540
 that in fact it there is no correlation but um it's an

121
00:07:48,540 --> 00:07:51,360
 interesting idea that there might be some

122
00:07:51,360 --> 00:07:54,000
 correlation because how would that relate to the buddhist

123
00:07:54,000 --> 00:07:55,920
 teaching it's certainly not a buddhist

124
00:07:55,920 --> 00:07:58,760
 teaching that such a thing is possible and yet we have in

125
00:07:58,760 --> 00:08:01,120
 the dhammapada stories remember the first

126
00:08:01,120 --> 00:08:03,820
 story that we did i don't know if i actually brought up

127
00:08:03,820 --> 00:08:05,680
 brought up the whole backstory but

128
00:08:05,680 --> 00:08:10,240
 this guy does basically that he goes to a tree and and he

129
00:08:10,240 --> 00:08:13,520
 uh he cleans up around this great tree

130
00:08:13,520 --> 00:08:17,230
 in the forest and puts up banners and flags and a wall and

131
00:08:17,230 --> 00:08:20,320
 a wall around it to protect it and then

132
00:08:20,320 --> 00:08:23,270
 makes a promise to the tree that if he gets a son or a

133
00:08:23,270 --> 00:08:25,360
 daughter he'll come back and do

134
00:08:25,360 --> 00:08:28,560
 pay great homage and respect to the tree and sure enough

135
00:08:28,560 --> 00:08:31,200
 his wife gives gives birth right

136
00:08:31,200 --> 00:08:34,760
 quite quickly after that so people from Sri Lanka have told

137
00:08:34,760 --> 00:08:36,880
 me that this actually works

138
00:08:36,880 --> 00:08:39,980
 and i was trying to figure out exactly how it might work in

139
00:08:39,980 --> 00:08:42,000
 reality because there has to be

140
00:08:42,000 --> 00:08:44,630
 a causal relationship it can't just be magic there's no

141
00:08:44,630 --> 00:08:47,520
 such thing and it's not it's not really um

142
00:08:47,520 --> 00:08:51,960
 it's not not accepted in buddhism so the idea that somehow

143
00:08:51,960 --> 00:08:54,240
 you could do some ritual and that

144
00:08:54,240 --> 00:08:56,760
 then it could work the only way it could work and i'm

145
00:08:56,760 --> 00:08:58,240
 thinking it actually could

146
00:08:58,240 --> 00:09:03,480
 is uh if the angels got involved see because uh there's got

147
00:09:03,480 --> 00:09:05,440
 to be something to do with angels

148
00:09:05,440 --> 00:09:11,430
 hanging out at the bodhi tree and uh if the lay people come

149
00:09:11,430 --> 00:09:14,240
 to the bodhi tree and and they do this

150
00:09:14,240 --> 00:09:17,480
 and they make an sincere wish and they have a sincerely

151
00:09:17,480 --> 00:09:19,760
 good heart it's kind of like the angels

152
00:09:19,760 --> 00:09:23,110
 can look down and say oh that's those are nice people well

153
00:09:23,110 --> 00:09:25,360
 my lifespan is almost up i think all

154
00:09:25,360 --> 00:09:28,860
 maybe they even hang out looking for parents and when the

155
00:09:28,860 --> 00:09:31,120
 angels know that their lifespan is almost

156
00:09:31,120 --> 00:09:34,510
 up they try to find suitable parents to to go and be reborn

157
00:09:34,510 --> 00:09:36,560
 i mean something like that somehow the

158
00:09:36,560 --> 00:09:40,820
 angels the devas get involved but i give that only as an

159
00:09:40,820 --> 00:09:44,000
 example of how there might be some causal

160
00:09:44,000 --> 00:09:47,450
 relationship but but there has to be something like that or

161
00:09:47,450 --> 00:09:49,680
 else it's just ridiculous and so for

162
00:09:49,680 --> 00:09:53,170
 the most part it is especially it's beyond ridiculous when

163
00:09:53,170 --> 00:09:55,120
 it comes down to killing animals

164
00:09:55,120 --> 00:09:57,840
 killing your fellow living beings and saying that's somehow

165
00:09:57,840 --> 00:10:00,320
 the path to the brahma world so

166
00:10:00,320 --> 00:10:03,540
 sari puta rightly says to him he says who taught you your

167
00:10:03,540 --> 00:10:05,440
 teachers and he says your teachers

168
00:10:05,440 --> 00:10:08,970
 happened to clue and so the reason why the teachers might

169
00:10:08,970 --> 00:10:11,120
 have been teaching it may have just been

170
00:10:11,120 --> 00:10:15,980
 because you see if if you sound like you know what you're

171
00:10:15,980 --> 00:10:17,680
 doing when if you set up all these

172
00:10:17,680 --> 00:10:21,900
 complex rituals people think oh well this one this person

173
00:10:21,900 --> 00:10:24,320
 we need him because he leads us in these

174
00:10:24,320 --> 00:10:27,260
 very important rituals and only he knows the right rituals

175
00:10:27,260 --> 00:10:28,960
 when in fact they just make them up

176
00:10:29,920 --> 00:10:33,140
 and so i think that's what you get i'm pretty sure that's

177
00:10:33,140 --> 00:10:35,840
 what you get in a lot of the the

178
00:10:35,840 --> 00:10:39,290
 rituals at the time of the buddha and and even after the

179
00:10:39,290 --> 00:10:42,000
 buddha but but definitely very much

180
00:10:42,000 --> 00:10:45,710
 before the buddha came around like we've studied these in

181
00:10:45,710 --> 00:10:47,760
 university when i was taking indian

182
00:10:47,760 --> 00:10:52,100
 religion many years ago and some of the rituals are just

183
00:10:52,100 --> 00:10:55,520
 silly like they they take it seems like

184
00:10:55,520 --> 00:10:58,240
 originally they were they were horrific these horrific

185
00:10:58,240 --> 00:11:01,120
 sacrifices a lot of killing and so they

186
00:11:01,120 --> 00:11:04,670
 would take a goat up to the altar and cut its head off and

187
00:11:04,670 --> 00:11:07,520
 there was something about burying a live

188
00:11:07,520 --> 00:11:10,330
 turtle under the altar and just burying it alive and and

189
00:11:10,330 --> 00:11:13,680
 killing it suffocating it under the altar i

190
00:11:13,680 --> 00:11:17,110
 mean for no reason i mean why would you bury a live turtle

191
00:11:17,110 --> 00:11:19,840
 under under a stone altar but it was

192
00:11:19,840 --> 00:11:23,220
 an important part of building the altar and then there was

193
00:11:23,220 --> 00:11:25,440
 butter that had to be had to be poured

194
00:11:25,440 --> 00:11:29,960
 into the fire and that kind of thing and it evolved so that

195
00:11:29,960 --> 00:11:32,080
 eventually probably with the advent of

196
00:11:32,080 --> 00:11:36,290
 buddhism giantism and a lot of the movements that were anti

197
00:11:36,290 --> 00:11:40,080
-torture anti-cruelty eventually when by

198
00:11:40,080 --> 00:11:43,620
 the time we came to the form that we study it today what

199
00:11:43,620 --> 00:11:45,840
 they do is they bring a goat to the

200
00:11:45,840 --> 00:11:49,520
 altar and they have to show the altar to the goat like the

201
00:11:49,520 --> 00:11:51,600
 goat has to see the altar and then they

202
00:11:51,600 --> 00:11:54,820
 take the goat away there's a there's a sacrificial pole

203
00:11:54,820 --> 00:11:57,360
 there's a special wooden pole that has to be

204
00:11:57,360 --> 00:12:00,800
 there as well and they have to bring the goat and show the

205
00:12:00,800 --> 00:12:02,800
 goat that pole so as long as the

206
00:12:02,800 --> 00:12:05,620
 goat sees the pole they've they've done enough then they

207
00:12:05,620 --> 00:12:07,600
 take the goat away and you can imagine

208
00:12:07,600 --> 00:12:10,360
 some guys saying oh well we can't kill them anymore well

209
00:12:10,360 --> 00:12:11,920
 let's just say let's just tell

210
00:12:11,920 --> 00:12:14,290
 everyone oh it's enough that the goat sees the pole i mean

211
00:12:14,290 --> 00:12:16,320
 it's something that they just come up with

212
00:12:16,320 --> 00:12:19,970
 say oh no no people say well we don't really want to kill

213
00:12:19,970 --> 00:12:21,040
 the goat oh well

214
00:12:21,040 --> 00:12:24,180
 it's okay as long as he sees the post somehow that's

215
00:12:24,180 --> 00:12:24,960
 meaningful

216
00:12:24,960 --> 00:12:28,420
 it's no longer about killing so i mean good on them that

217
00:12:28,420 --> 00:12:30,960
 they're that their ridiculous sacrifices

218
00:12:30,960 --> 00:12:34,750
 have become innocent that's that's one step in the right

219
00:12:34,750 --> 00:12:37,360
 direction the other thing is they

220
00:12:37,360 --> 00:12:40,150
 instead of burying a turtle under the altar they take a

221
00:12:40,150 --> 00:12:42,160
 lump of butter and they bury it under the

222
00:12:42,160 --> 00:12:45,630
 altar it's a substitute for a live turtle which is a good

223
00:12:45,630 --> 00:12:48,400
 thing for all the poor little tutor turtles

224
00:12:48,400 --> 00:12:55,030
 but definitely innocent doesn't mean beneficial worthwhile

225
00:12:55,030 --> 00:12:58,400
 and and in this guy's case he's got

226
00:12:58,400 --> 00:13:02,240
 real problems with his killing every month killing a living

227
00:13:02,240 --> 00:13:04,560
 being and feeding it to the fire like if

228
00:13:04,560 --> 00:13:07,600
 it was just butter he was feeding to the fire then he could

229
00:13:07,600 --> 00:13:09,440
 say well that's kind of innocent

230
00:13:09,440 --> 00:13:11,780
 but even still it's not the way to the brahma world it

231
00:13:11,780 --> 00:13:13,520
 doesn't have any connection with being

232
00:13:13,520 --> 00:13:19,280
 reborn as a brahma brahma as a god so it takes him to see

233
00:13:19,280 --> 00:13:21,760
 the buddha and the buddha buddha

234
00:13:21,760 --> 00:13:25,930
 he tells the buddha what he does and the buddha says you

235
00:13:25,930 --> 00:13:29,120
 could do that for 100 years every month

236
00:13:30,640 --> 00:13:36,100
 and it wouldn't be worth the thousandth part if you were to

237
00:13:36,100 --> 00:13:37,600
 just pay respect to

238
00:13:37,600 --> 00:13:43,490
 an enlightened being for one moment so again talking about

239
00:13:43,490 --> 00:13:47,200
 homage and the buddha specifies my

240
00:13:47,200 --> 00:13:53,860
 students so it is a bold claim and as i said last time it

241
00:13:53,860 --> 00:13:57,920
 seems like a bit of a biased claim at first

242
00:13:58,560 --> 00:14:01,190
 blush because anyone could say that well of course everyone

243
00:14:01,190 --> 00:14:02,640
's going to say their own students

244
00:14:02,640 --> 00:14:05,840
 but it has to be true at some point you know if your

245
00:14:05,840 --> 00:14:08,240
 students are enlightened and if the

246
00:14:08,240 --> 00:14:11,660
 the um if your people really are if you're talking about a

247
00:14:11,660 --> 00:14:13,600
 group of people who really are

248
00:14:13,600 --> 00:14:17,080
 enlightened then the buddha wasn't afraid of making these

249
00:14:17,080 --> 00:14:19,200
 bold and sort of bragging claims

250
00:14:19,200 --> 00:14:25,010
 boastful claims um because if you look at it another way of

251
00:14:25,010 --> 00:14:26,720
 course it's it's leaving you wide

252
00:14:26,720 --> 00:14:30,520
 open to attack and the buddha fully welcomed what he's

253
00:14:30,520 --> 00:14:33,760
 saying he's really it's called the lion's roar

254
00:14:33,760 --> 00:14:37,450
 he was really putting it out there he's he's making a claim

255
00:14:37,450 --> 00:14:40,240
 a boast and challenge it's a challenge

256
00:14:40,240 --> 00:14:45,030
 you know prove me wrong that's basically saying uh it's it

257
00:14:45,030 --> 00:14:48,080
's claiming something very very boldly

258
00:14:48,080 --> 00:14:51,990
 and that's really what the the purpose and the meaning

259
00:14:51,990 --> 00:14:54,640
 there is that's not the buddha bragging

260
00:14:54,640 --> 00:14:57,960
 or forget gain of any sort i mean there's no sense that the

261
00:14:57,960 --> 00:15:00,720
 buddha even had any need i mean

262
00:15:00,720 --> 00:15:04,680
 even if you don't follow buddhism there was no real um

263
00:15:04,680 --> 00:15:08,000
 evidence to support that when he was well

264
00:15:08,000 --> 00:15:14,690
 taken care of and well supported um but here is what he's

265
00:15:14,690 --> 00:15:18,480
 doing is instead making just this bold

266
00:15:18,480 --> 00:15:23,150
 claim that could then be challenged and of course the brah

267
00:15:23,150 --> 00:15:25,520
min would have to be impressed by that

268
00:15:25,520 --> 00:15:27,750
 because he couldn't challenge it and looking at the budd

269
00:15:27,750 --> 00:15:29,920
hist monks he would have to agree that

270
00:15:29,920 --> 00:15:33,630
 oh yes indeed these there's something special about many of

271
00:15:33,630 --> 00:15:35,760
 these monks who have developed

272
00:15:35,760 --> 00:15:38,800
 themselves and so that's what the verse actually says bhav

273
00:15:38,800 --> 00:15:41,760
itatthana to one if you pay homage to one

274
00:15:41,760 --> 00:15:47,180
 or to those who are of developed self so again not to get

275
00:15:47,180 --> 00:15:49,920
 too much into it um i think the more

276
00:15:49,920 --> 00:15:52,930
 important aspect of this that differentiates it from the

277
00:15:52,930 --> 00:15:54,880
 last one is talking about sacrifice

278
00:15:54,880 --> 00:15:58,240
 which i've done but i think we can extrapolate that to talk

279
00:15:58,240 --> 00:16:01,520
 about religious practices in general

280
00:16:01,520 --> 00:16:04,660
 you know it's not it's not enough that we have a religious

281
00:16:04,660 --> 00:16:06,480
 practice and i think there's a lot of

282
00:16:06,480 --> 00:16:09,540
 this in the world just because something is a spiritual

283
00:16:09,540 --> 00:16:11,520
 practice or religious practice doesn't

284
00:16:11,520 --> 00:16:15,170
 make it really all that valuable um and certainly an

285
00:16:15,170 --> 00:16:18,000
 understanding that the different different

286
00:16:18,000 --> 00:16:21,240
 spiritual practices have different values so it's not to

287
00:16:21,240 --> 00:16:23,280
 say that either you're practicing

288
00:16:23,280 --> 00:16:26,750
 to become enlightened or you're doing useless things there

289
00:16:26,750 --> 00:16:28,880
 are certain religious practices

290
00:16:28,880 --> 00:16:32,690
 that are valuable but just not as valuable so feeding of

291
00:16:32,690 --> 00:16:35,280
 sacrificial fire is pretty useless

292
00:16:36,080 --> 00:16:38,950
 but practicing loving kindness for example is quite

293
00:16:38,950 --> 00:16:41,920
 valuable practicing charity giving money

294
00:16:41,920 --> 00:16:46,670
 to the poor is valuable it's less valuable than well for

295
00:16:46,670 --> 00:16:50,240
 example paying homage to one who deserves

296
00:16:50,240 --> 00:16:55,590
 it but even paying homage to one who is worthy of homage is

297
00:16:55,590 --> 00:16:58,720
 far less valuable than actually becoming

298
00:16:58,720 --> 00:17:02,160
 worthy of respect yourself my teacher said that he said

299
00:17:03,840 --> 00:17:07,530
 going to see an enlightened being or paying homage to an

300
00:17:07,530 --> 00:17:11,680
 enlightened being is not worth the smallest

301
00:17:11,680 --> 00:17:14,340
 part of becoming practicing to become an enlightened being

302
00:17:14,340 --> 00:17:15,680
 yourself it's one of

303
00:17:15,680 --> 00:17:18,900
 it's in a very famous talk that he gave on the force at the

304
00:17:18,900 --> 00:17:23,120
 force of dibhatana um but yeah he's

305
00:17:23,120 --> 00:17:28,760
 he made that very important statement that yeah it's good

306
00:17:28,760 --> 00:17:31,760
 but it's not worth the smallest part

307
00:17:32,640 --> 00:17:37,670
 it's far less good than than actually practicing to become

308
00:17:37,670 --> 00:17:41,120
 one yourself so it's a matter of degrees

309
00:17:41,120 --> 00:17:44,190
 and that's something for us to keep in mind in our

310
00:17:44,190 --> 00:17:47,920
 spiritual practice that some people will put a

311
00:17:47,920 --> 00:17:50,860
 lot of emphasis on chanting some people put a lot of

312
00:17:50,860 --> 00:17:52,960
 emphasis on charity or social work

313
00:17:52,960 --> 00:17:56,940
 and it's all about our priorities some people put a lot of

314
00:17:56,940 --> 00:17:58,320
 emphasis on study

315
00:17:59,200 --> 00:18:02,630
 and in the end we have to ask ourselves what is our goal

316
00:18:02,630 --> 00:18:04,000
 and we have to do those things

317
00:18:04,000 --> 00:18:08,000
 to engage put primary emphasis on those things that lead us

318
00:18:08,000 --> 00:18:08,880
 to that goal

319
00:18:08,880 --> 00:18:13,000
 for just giving and giving and giving what is the purpose

320
00:18:13,000 --> 00:18:14,960
 of that if we're just chanting and

321
00:18:14,960 --> 00:18:18,440
 chanting or this or that what is the purpose of all these

322
00:18:18,440 --> 00:18:22,000
 things and and on the other hand if we

323
00:18:22,000 --> 00:18:24,540
 if we do have a set purpose and we do things for that

324
00:18:24,540 --> 00:18:25,040
 purpose

325
00:18:28,960 --> 00:18:31,360
 we have to differentiate between those things that actually

326
00:18:31,360 --> 00:18:32,160
 lead to the purpose

327
00:18:32,160 --> 00:18:36,900
 and but but then there are a wide array of things that can

328
00:18:36,900 --> 00:18:39,360
 help us that can actually be a benefit

329
00:18:39,360 --> 00:18:42,480
 to help us realize our goals like charity can be useful for

330
00:18:42,480 --> 00:18:44,880
 meditation morality of course is

331
00:18:44,880 --> 00:18:49,090
 essential for meditation these kind of things study is also

332
00:18:49,090 --> 00:18:52,800
 quite important but putting them in context

333
00:18:54,160 --> 00:18:58,640
 so many religious practices have to be taken as a support

334
00:18:58,640 --> 00:19:01,280
 rather than a main goal or a main

335
00:19:01,280 --> 00:19:06,020
 practice a main focus anyway things to consider it's just

336
00:19:06,020 --> 00:19:09,680
 some of the ideas that arise based on

337
00:19:09,680 --> 00:19:13,710
 this verse and the importance of quality rather than

338
00:19:13,710 --> 00:19:17,360
 quantity you can do a lot a lot a lot of deeds

339
00:19:17,360 --> 00:19:20,180
 but it doesn't make any of them good or worthwhile you can

340
00:19:20,180 --> 00:19:21,920
 work for a thousand years for a hundred

341
00:19:21,920 --> 00:19:25,810
 years work very very hard and have nothing to show for it

342
00:19:25,810 --> 00:19:28,400
 or meaningless things to show for it

343
00:19:28,400 --> 00:19:31,930
 whereas you can just for one moment do the right thing and

344
00:19:31,930 --> 00:19:35,920
 have it worth far more than those

345
00:19:35,920 --> 00:19:38,130
 hundred years that's basically what's being said you're

346
00:19:38,130 --> 00:19:39,040
 very useful teaching

347
00:19:39,040 --> 00:19:43,530
 anyway so that's the dhamma fadah for tonight thank you for

348
00:19:43,530 --> 00:19:45,680
 tuning in wishing you all the best

