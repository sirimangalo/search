1
00:00:00,000 --> 00:00:03,650
 I'm considering ordaining and I want to know how much

2
00:00:03,650 --> 00:00:05,860
 interaction with family members is proper.

3
00:00:05,860 --> 00:00:10,580
 I've seen the passage in the Anguttra Nikaya 232, "My

4
00:00:10,580 --> 00:00:12,740
 family worries they will not see me as a monk."

5
00:00:12,740 --> 00:00:16,080
 I actually looked this up and if your numbering is the same

6
00:00:16,080 --> 00:00:19,540
 as mine it's something to do with gratefulness.

7
00:00:19,540 --> 00:00:24,100
 232, if your numbering is actually the book of twos, number

8
00:00:24,100 --> 00:00:25,900
 32, then it's gratefulness.

9
00:00:25,900 --> 00:00:29,620
 So I assume you're saying, "Wouldn't it be ungrateful if I

10
00:00:29,620 --> 00:00:31,160
 avoided my parents?"

11
00:00:31,160 --> 00:00:34,180
 Now if you're going by some other, maybe the poly-English,

12
00:00:34,180 --> 00:00:35,700
 poly-text society numbering,

13
00:00:35,700 --> 00:00:40,980
 then I have no idea what that suit is about.

14
00:00:40,980 --> 00:00:45,030
 Numbering is difficult because there's so many different

15
00:00:45,030 --> 00:00:46,180
 numberings.

16
00:00:46,180 --> 00:00:49,140
 So you may be worried about being grateful, but anyway, we

17
00:00:49,140 --> 00:00:51,500
'll take the question as it is.

18
00:00:51,500 --> 00:00:55,620
 I can't think of any instance where interaction with family

19
00:00:55,620 --> 00:00:57,340
 members is improper.

20
00:00:57,340 --> 00:01:00,580
 There are certainly many instances where the Buddha gave

21
00:01:00,580 --> 00:01:02,620
 exceptions for family members.

22
00:01:02,620 --> 00:01:06,910
 For example, your parents, you're allowed to give them food

23
00:01:06,910 --> 00:01:07,660
 first.

24
00:01:07,660 --> 00:01:10,620
 If you get food, even before you've eaten any, you're

25
00:01:10,620 --> 00:01:11,860
 allowed to give to your parents.

26
00:01:11,860 --> 00:01:14,420
 You're allowed to ask for medicine.

27
00:01:14,420 --> 00:01:18,150
 You're allowed to actually go to someone's house and beg

28
00:01:18,150 --> 00:01:21,260
 and actually ask them for medicine.

29
00:01:21,260 --> 00:01:25,440
 Of course, there's no offense, but you shouldn't just go up

30
00:01:25,440 --> 00:01:27,540
 to a stranger and ask them.

31
00:01:27,540 --> 00:01:29,820
 The meaning is it would be someone who supports you.

32
00:01:29,820 --> 00:01:31,300
 So you can go stand in front of their house.

33
00:01:31,300 --> 00:01:32,460
 If they say, "What do you need?"

34
00:01:32,460 --> 00:01:35,460
 You can say, "Look, my parents are sick and I need medicine

35
00:01:35,460 --> 00:01:36,140
 for them."

36
00:01:36,140 --> 00:01:38,540
 This is actually allowed.

37
00:01:38,540 --> 00:01:43,380
 You're allowed to go to visit your parents during the rains

38
00:01:43,380 --> 00:01:43,660
.

39
00:01:43,660 --> 00:01:45,320
 During the rains, you're normally not allowed to travel,

40
00:01:45,320 --> 00:01:46,260
 but you're allowed to go visit

41
00:01:46,260 --> 00:01:47,660
 your parents.

42
00:01:47,660 --> 00:01:50,430
 Even if you're not invited, if there's some reason and you

43
00:01:50,430 --> 00:01:51,780
 know that they're sick but

44
00:01:51,780 --> 00:01:55,000
 they don't send a messenger for you, but you find out that

45
00:01:55,000 --> 00:01:56,620
 they're sick, you're able

46
00:01:56,620 --> 00:02:01,170
 to go to them and stay overnight, up to six nights, this

47
00:02:01,170 --> 00:02:02,660
 kind of thing.

48
00:02:02,660 --> 00:02:08,900
 There's an obvious potential for relationships.

49
00:02:08,900 --> 00:02:12,740
 That being said, it's not advisable to get too close to

50
00:02:12,740 --> 00:02:15,420
 family members, besides your parents

51
00:02:15,420 --> 00:02:16,780
 especially.

52
00:02:16,780 --> 00:02:22,130
 Getting involved with brothers, sisters, and cousins and so

53
00:02:22,130 --> 00:02:25,420
 on, just because they're relatives

54
00:02:25,420 --> 00:02:29,320
 can be dangerous because the reason for associating has

55
00:02:29,320 --> 00:02:32,020
 nothing to do with the Dhamma often.

56
00:02:32,020 --> 00:02:35,690
 That being the case, something that we should regard with

57
00:02:35,690 --> 00:02:36,820
 certain care.

58
00:02:36,820 --> 00:02:39,370
 Now, your parents, of course, the idea of being grateful to

59
00:02:39,370 --> 00:02:40,380
 them and giving something

60
00:02:40,380 --> 00:02:44,710
 back to them inspires us often to want to meditate with

61
00:02:44,710 --> 00:02:47,100
 them or encourage them in good

62
00:02:47,100 --> 00:02:56,440
 deeds and stay with them to hopefully rub off on them, for

63
00:02:56,440 --> 00:02:58,460
 example.

64
00:02:58,460 --> 00:03:00,740
 There's nothing wrong with that.

65
00:03:00,740 --> 00:03:05,030
 I would say basically what you can tell your family is

66
00:03:05,030 --> 00:03:07,420
 there's no need to worry.

67
00:03:07,420 --> 00:03:09,780
 They will always be able to see you as a monk.

68
00:03:09,780 --> 00:03:12,290
 It's not because you're a monk that they won't be able to

69
00:03:12,290 --> 00:03:12,860
 see you.

70
00:03:12,860 --> 00:03:16,420
 It might be because you're off in the forest in Thailand.

71
00:03:16,420 --> 00:03:19,630
 That being the case, yes, they may not see you for many

72
00:03:19,630 --> 00:03:21,580
 years until you have the support

73
00:03:21,580 --> 00:03:24,740
 and the ability to return or until they have the ability to

74
00:03:24,740 --> 00:03:25,940
 come to visit you.

75
00:03:25,940 --> 00:03:28,260
 That's something you have to deal with.

76
00:03:28,260 --> 00:03:30,660
 Of course, for Thai people, this is a non-issue or for

77
00:03:30,660 --> 00:03:32,500
 Asian people in general, it's usually

78
00:03:32,500 --> 00:03:36,600
 a non-issue because the person will ordain nearby and so

79
00:03:36,600 --> 00:03:38,980
 they're able to actually visit

80
00:03:38,980 --> 00:03:40,260
 with each other.

81
00:03:40,260 --> 00:03:44,490
 But there's certainly nothing about being a monk that

82
00:03:44,490 --> 00:03:46,820
 requires you to be apart from

83
00:03:46,820 --> 00:03:52,210
 your parents at the very least and really your relatives in

84
00:03:52,210 --> 00:03:53,340
 general.

85
00:03:53,340 --> 00:03:55,830
 There's nothing to say that we have to be apart from people

86
00:03:55,830 --> 00:03:56,660
 as Buddhists.

87
00:03:56,660 --> 00:03:59,840
 We're to try to cultivate seclusion as a general way of

88
00:03:59,840 --> 00:04:01,900
 living our lives, but you can do that

89
00:04:01,900 --> 00:04:03,580
 when you're around your relatives as well.

90
00:04:03,580 --> 00:04:06,060
 You go to visit them and you're in your room a lot of the

91
00:04:06,060 --> 00:04:06,540
 time.

92
00:04:06,540 --> 00:04:08,950
 You come out to visit, to talk, and then you go back and

93
00:04:08,950 --> 00:04:10,380
 rest and do your meditation in

94
00:04:10,380 --> 00:04:13,580
 your room.

95
00:04:13,580 --> 00:04:18,540
 There's no incompatibility there.

96
00:04:18,540 --> 00:04:20,510
 But there is a nice story in the Visuddhi Maga that I would

97
00:04:20,510 --> 00:04:21,580
 encourage you to read about

98
00:04:21,580 --> 00:04:26,690
 this monk who was so intent on solitude that many years

99
00:04:26,690 --> 00:04:29,260
 later after he'd ordained, he went

100
00:04:29,260 --> 00:04:35,220
 back to see his parents and they didn't recognize him.

101
00:04:35,220 --> 00:04:38,000
 They fed him and supported him for three months and he

102
00:04:38,000 --> 00:04:39,700
 never told them he was their son and

103
00:04:39,700 --> 00:04:42,060
 they never found out that he was their son.

104
00:04:42,060 --> 00:04:44,660
 So they gave him this robe at the end and sent him back and

105
00:04:44,660 --> 00:04:46,020
 said, "Could you give this

106
00:04:46,020 --> 00:04:47,020
 to my son?

107
00:04:47,020 --> 00:04:49,660
 I know he's in the monastery that you're headed to."

108
00:04:49,660 --> 00:04:54,650
 He headed off and halfway along the way he met his teacher

109
00:04:54,650 --> 00:04:56,660
 going to visit this village

110
00:04:56,660 --> 00:04:58,420
 where his parents were.

111
00:04:58,420 --> 00:05:01,020
 So he gave the robe to his teacher and said, "Look, I'm

112
00:05:01,020 --> 00:05:02,820
 much happier back at my monastery.

113
00:05:02,820 --> 00:05:06,140
 I'm going to go back there and live in seclusion."

114
00:05:06,140 --> 00:05:11,850
 The teacher looked at the robe and didn't quite understand,

115
00:05:11,850 --> 00:05:14,020
 but he went on and came to the

116
00:05:14,020 --> 00:05:19,650
 village and the mother saw this monk's teacher coming alone

117
00:05:19,650 --> 00:05:22,820
 and thought that that meant that

118
00:05:22,820 --> 00:05:25,930
 her son had passed away because she assumed that the son

119
00:05:25,930 --> 00:05:27,700
 would be with the teacher.

120
00:05:27,700 --> 00:05:31,160
 So she started crying and the teacher said, "What's wrong?"

121
00:05:31,160 --> 00:05:36,810
 She explained how she'd given the robe to be given to her

122
00:05:36,810 --> 00:05:37,620
 son.

123
00:05:37,620 --> 00:05:39,780
 So he pulled out the robe and he said, "Was it this robe?"

124
00:05:39,780 --> 00:05:41,140
 He said, "That was your son.

125
00:05:41,140 --> 00:05:45,350
 You spent three months with him, but he's so dedicated to

126
00:05:45,350 --> 00:05:47,340
 seclusion he didn't want to

127
00:05:47,340 --> 00:05:49,060
 get involved with his relatives."

128
00:05:49,060 --> 00:05:51,760
 So that's the kind of story you maybe don't want to tell

129
00:05:51,760 --> 00:05:53,460
 your parents if you're looking

130
00:05:53,460 --> 00:05:55,580
 for their permission to ordain.

131
00:05:55,580 --> 00:05:59,990
 But he was the example of a very ardent meditator and the

132
00:05:59,990 --> 00:06:02,460
 Buddha, one of the first things he

133
00:06:02,460 --> 00:06:06,990
 did was go back to see his father and teach his, not one of

134
00:06:06,990 --> 00:06:09,880
 the first things, but eventually

135
00:06:09,880 --> 00:06:14,810
 he did go back to see his father and teach his wife and his

136
00:06:14,810 --> 00:06:16,260
 son and so on.

137
00:06:16,260 --> 00:06:18,180
 So there's certainly nothing against that.

138
00:06:18,180 --> 00:06:21,600
 And following by that example, we can certainly try our

139
00:06:21,600 --> 00:06:23,800
 best to help them out to realize the

140
00:06:23,800 --> 00:06:24,980
 goodness that we've realized.

