1
00:00:00,000 --> 00:00:08,260
 Good evening everyone.

2
00:00:08,260 --> 00:00:21,120
 Broadcasting live May 1st 2016.

3
00:00:21,120 --> 00:00:28,530
 Today's quote is from the Anguttara Nikaya again. This time

4
00:00:28,530 --> 00:00:30,640
 the book of fours.

5
00:00:30,640 --> 00:00:38,800
 And again the quote only singles out part of the Sutta.

6
00:00:38,800 --> 00:00:48,660
 Again this isn't the words of the Buddha, this is the words

7
00:00:48,660 --> 00:00:50,960
 of Ananda.

8
00:00:50,960 --> 00:01:00,000
 Ananda was the attendant and cousin of the Buddha.

9
00:01:00,000 --> 00:01:05,360
 And he spent much of the Buddha's later life caring for him

10
00:01:05,360 --> 00:01:07,840
 and following him around

11
00:01:07,840 --> 00:01:13,890
 listening to and gathering and remembering the Buddha's

12
00:01:13,890 --> 00:01:15,600
 teachings.

13
00:01:15,600 --> 00:01:21,920
 And a very good memory it seems.

14
00:01:21,920 --> 00:01:27,910
 So unsurprising that people would come to him for the Dham

15
00:01:27,910 --> 00:01:29,920
ma perhaps after the Buddha passed away

16
00:01:29,920 --> 00:01:39,560
 because Ananda is one of the great monks who outlived the

17
00:01:39,560 --> 00:01:39,760
 Buddha.

18
00:01:39,760 --> 00:01:43,870
 Sariputta and Mughalana the Buddha's two cheap disciples

19
00:01:43,870 --> 00:01:46,400
 passed away before the Buddha did.

20
00:01:46,400 --> 00:01:54,640
 But Ananda lived on. Ananda lived to 120 so the legend goes

21
00:01:54,640 --> 00:01:54,640
.

22
00:01:54,640 --> 00:02:09,200
 And before he died he knew that both sides of his family

23
00:02:09,200 --> 00:02:12,720
 were going to fight over his relics.

24
00:02:12,720 --> 00:02:17,070
 So if he died when he was sick and dying he went to stay

25
00:02:17,070 --> 00:02:17,840
 with his family.

26
00:02:17,840 --> 00:02:23,040
 But both families wanted him to stay with them.

27
00:02:23,040 --> 00:02:28,650
 I'm maybe just making this up but at the end result is he

28
00:02:28,650 --> 00:02:31,120
 flew up into the air and

29
00:02:31,120 --> 00:02:36,960
 spontaneously combusted and made a determination so that

30
00:02:36,960 --> 00:02:40,720
 half of his relic, his bones,

31
00:02:40,720 --> 00:02:44,480
 went to on either side of the fence kind of thing.

32
00:02:44,480 --> 00:02:48,420
 Something like that, the river maybe, I think it was a

33
00:02:48,420 --> 00:02:49,200
 river.

34
00:02:49,200 --> 00:02:54,080
 There's a lot of stories like this.

35
00:02:54,080 --> 00:02:56,960
 But Ananda is a special monk.

36
00:02:56,960 --> 00:03:03,390
 What we, the idea we get about him is he was very

37
00:03:03,390 --> 00:03:08,880
 compassionate and interested in helping people

38
00:03:08,880 --> 00:03:16,000
 who were maybe had a hard time or he was moved to

39
00:03:16,000 --> 00:03:19,120
 compassion in ways that other monks maybe weren't.

40
00:03:19,120 --> 00:03:27,010
 And so he was moved to compassion, moved by compassion to

41
00:03:27,010 --> 00:03:28,400
 speak up for the bhikkhunis

42
00:03:28,400 --> 00:03:32,480
 even after the Buddha cautioned against ordaining women.

43
00:03:32,480 --> 00:03:38,240
 Ananda kind of pushed the Buddha to ordain women.

44
00:03:38,240 --> 00:03:43,740
 Of course that's a whole issue but there is an argument to

45
00:03:43,740 --> 00:03:48,000
 be had for why the Buddha was hesitant.

46
00:03:48,000 --> 00:03:55,040
 I mean whether you agree or not, the point being that it

47
00:03:55,040 --> 00:03:56,960
 makes it more difficult.

48
00:03:56,960 --> 00:03:59,960
 You can say well that's no excuse and it really isn't in

49
00:03:59,960 --> 00:04:06,640
 the end but certainly when you're trying to be celibate

50
00:04:06,640 --> 00:04:11,920
 and the majority of those trying to be celibate are men,

51
00:04:11,920 --> 00:04:14,880
 are heterosexual men.

52
00:04:14,880 --> 00:04:18,580
 Bringing women into the mix makes it more difficult so when

53
00:04:18,580 --> 00:04:19,440
 it was hesitant.

54
00:04:19,440 --> 00:04:24,040
 And also in India women were perhaps sheltered so there's

55
00:04:24,040 --> 00:04:24,960
 arguments.

56
00:04:24,960 --> 00:04:31,070
 But Ananda was not having any of it and really kind of

57
00:04:31,070 --> 00:04:39,440
 convinced the Buddha to ask.

58
00:04:39,440 --> 00:04:43,960
 And he taught laymen, laywomen. He was big on teaching

59
00:04:43,960 --> 00:04:47,280
 people. He was really helpful towards women especially.

60
00:04:47,280 --> 00:04:50,070
 Actually got him in some trouble sometimes because women

61
00:04:50,070 --> 00:04:53,280
 would take fancy to them.

62
00:04:53,280 --> 00:04:57,520
 But he was never moved by that.

63
00:04:57,520 --> 00:05:03,480
 He even claimed that he never had any sexual desire for a

64
00:05:03,480 --> 00:05:09,280
 woman as a monk.

65
00:05:10,400 --> 00:05:15,620
 Anyway, oh this is beside the point. There are these four

66
00:05:15,620 --> 00:05:16,800
 Anandas.

67
00:05:16,800 --> 00:05:21,870
 So they came to Ananda and Ananda just taught them these

68
00:05:21,870 --> 00:05:22,480
 four.

69
00:05:22,480 --> 00:05:29,530
 What are called the Pari Sundi Padanayangan. Pari Sundi

70
00:05:29,530 --> 00:05:31,680
 means purity.

71
00:05:31,680 --> 00:05:40,320
 Padanya. Padanya means something you should strive for.

72
00:05:40,320 --> 00:05:45,360
 Anga is just members or things.

73
00:05:45,360 --> 00:05:49,760
 So are these four things regarding which you should strive

74
00:05:49,760 --> 00:05:53,280
 for the purity of, for the purification of.

75
00:05:53,280 --> 00:05:58,240
 So four things you should try to purify.

76
00:05:58,240 --> 00:06:05,100
 Should strive to purify. Katamani, Tatari, what are these

77
00:06:05,100 --> 00:06:06,000
 four?

78
00:06:06,000 --> 00:06:12,480
 Number one, Seelapari Sundi Padanayangan.

79
00:06:12,480 --> 00:06:20,640
 The striving for the purification of virtue.

80
00:06:20,640 --> 00:06:25,760
 And that's where the quote comes from.

81
00:06:25,760 --> 00:06:34,640
 Number two, Tchitapari Sundi Padanayangan.

82
00:06:34,640 --> 00:06:42,720
 Striving for the purification of mind.

83
00:06:42,720 --> 00:06:50,480
 Number three, Tchitapari Sundi Padanayangan.

84
00:06:50,480 --> 00:06:55,920
 Striving for the purification of view.

85
00:06:55,920 --> 00:07:01,120
 And number four, Mwimutti Pari Sundi Padanayangan.

86
00:07:01,120 --> 00:07:10,240
 Striving for the purification of release, freedom.

87
00:07:10,240 --> 00:07:15,010
 So these four we should strive to purify. Strive to

88
00:07:15,010 --> 00:07:18,720
 complete.

89
00:07:18,720 --> 00:07:22,640
 So with the first one that the quote talks about is pur

90
00:07:22,640 --> 00:07:25,440
ification of virtue.

91
00:07:25,440 --> 00:07:30,640
 Seelap.

92
00:07:30,640 --> 00:07:37,600
 And we talked a little bit about this last night.

93
00:07:37,600 --> 00:07:43,280
 The precepts and rules and keeping rules and

94
00:07:43,280 --> 00:07:47,540
 in general rules, if you have the right rules, they tend to

95
00:07:47,540 --> 00:07:48,640
 be a good guide

96
00:07:48,640 --> 00:07:53,010
 for your behavior. And so we purify them. We look and see

97
00:07:53,010 --> 00:07:53,840
 which rules were

98
00:07:53,840 --> 00:07:58,160
 breaking, which rules we tend to break, and

99
00:07:58,160 --> 00:08:03,040
 then we work to stop breaking those rules.

100
00:08:03,040 --> 00:08:08,640
 What is impure in regards to or are unfulfilled in regards

101
00:08:08,640 --> 00:08:10,000
 to our

102
00:08:10,000 --> 00:08:16,400
 upparepurangwa, paripuris Puri Sami. I will fulfill those

103
00:08:16,400 --> 00:08:22,640
 virtues or those rules, specifically dealing with rules,

104
00:08:22,640 --> 00:08:28,720
 sikapada, sikapada, that are not fulfilled. I would strive

105
00:08:28,720 --> 00:08:30,160
 to fulfill them.

106
00:08:30,160 --> 00:08:33,680
 So some people keep the five precepts and then

107
00:08:33,680 --> 00:08:35,940
 they don't keep the eight precepts and then they strive to

108
00:08:35,940 --> 00:08:36,640
 keep the eight

109
00:08:36,640 --> 00:08:40,080
 precepts. Some people keep the eight precepts and

110
00:08:40,080 --> 00:08:44,160
 then they strive to keep the ten precepts and then some

111
00:08:44,160 --> 00:08:46,480
 people keep the ten precepts and then they

112
00:08:46,480 --> 00:08:51,120
 strive to become a monk and keep many more precepts.

113
00:08:51,120 --> 00:08:53,600
 And then you have the precepts and then you break them and

114
00:08:53,600 --> 00:08:54,560
 then you strive not

115
00:08:54,560 --> 00:08:57,280
 to break them.

116
00:08:57,280 --> 00:09:02,160
 You work to purify them. And then there's this curious

117
00:09:02,160 --> 00:09:07,920
 statement, those that are fulfilled,

118
00:09:07,920 --> 00:09:21,520
 I will support

119
00:09:21,520 --> 00:09:28,960
 with wisdom, support those that are are fulfilled

120
00:09:28,960 --> 00:09:36,720
 with wisdom, here or there or or again and again or

121
00:09:36,720 --> 00:09:39,840
 where appropriate.

122
00:09:39,840 --> 00:09:43,020
 So the commentary doesn't give much explanation of what

123
00:09:43,020 --> 00:09:44,080
 this means but I

124
00:09:44,080 --> 00:09:46,880
 guess it means

125
00:09:46,880 --> 00:09:52,480
 because it's easy to keep the letter of the rule, right?

126
00:09:52,480 --> 00:09:55,880
 And so you can say, oh I'm keeping all these rules, fine,

127
00:09:55,880 --> 00:09:56,640
 like you can keep the

128
00:09:56,640 --> 00:10:00,560
 five precepts and still be, you can still be a bad person

129
00:10:00,560 --> 00:10:00,880
 keeping

130
00:10:00,880 --> 00:10:05,760
 the five precepts. So it's about filling in the gaps

131
00:10:05,760 --> 00:10:09,520
 and it's about fulfilling the purpose of the precepts with

132
00:10:09,520 --> 00:10:11,200
 wisdom.

133
00:10:11,200 --> 00:10:13,720
 So you might not kill but you can still be very angry and

134
00:10:13,720 --> 00:10:14,960
 you can still want to

135
00:10:14,960 --> 00:10:20,000
 hurt or even kill and you might not steal but you still

136
00:10:20,000 --> 00:10:20,320
 covet

137
00:10:20,320 --> 00:10:24,240
 that of others and are jealous and so on.

138
00:10:25,200 --> 00:10:29,280
 You don't cheat but you still feel desire and

139
00:10:29,280 --> 00:10:34,720
 you don't have the understanding of hurting others, of what

140
00:10:34,720 --> 00:10:35,600
 it means to hurt

141
00:10:35,600 --> 00:10:38,880
 others and what is the result of hurting others.

142
00:10:38,880 --> 00:10:42,230
 So you use wisdom to augment that because keeping the

143
00:10:42,230 --> 00:10:42,960
 precepts,

144
00:10:42,960 --> 00:10:46,960
 there's a sort of, there's this idea that even

145
00:10:46,960 --> 00:10:50,510
 if it's hard, if it's painful, keeping the precepts is a

146
00:10:50,510 --> 00:10:52,320
 good thing.

147
00:10:52,320 --> 00:10:55,280
 And people might question that, they think well if it's,

148
00:10:55,280 --> 00:10:59,230
 if whatever you're doing is causing you suffering, why

149
00:10:59,230 --> 00:11:00,560
 would you do it?

150
00:11:00,560 --> 00:11:04,720
 But then we can ask the question, suppose there's an

151
00:11:04,720 --> 00:11:07,360
 alcoholic or a drug addict

152
00:11:07,360 --> 00:11:10,630
 and if they stop taking drugs and they go through

153
00:11:10,630 --> 00:11:11,520
 withdrawal,

154
00:11:11,520 --> 00:11:15,600
 would you say that's a reason to go back to taking drugs?

155
00:11:15,600 --> 00:11:15,600
 Of course not.

156
00:11:15,600 --> 00:11:18,550
 And that's really all this is, it's a kind of a withdrawal.

157
00:11:18,550 --> 00:11:18,960
 When you keep

158
00:11:18,960 --> 00:11:22,320
 precepts, when you keep these rules that say the eight

159
00:11:22,320 --> 00:11:23,520
 precepts are so on, it's

160
00:11:23,520 --> 00:11:28,720
 quite unpleasant for the first time.

161
00:11:28,720 --> 00:11:31,760
 This is because you're going through withdrawal.

162
00:11:31,760 --> 00:11:36,320
 It doesn't mean that it's wrong, in fact it's a good sign,

163
00:11:36,320 --> 00:11:38,860
 a sign that you're actually dealing with the problem

164
00:11:38,860 --> 00:11:41,600
 instead of placating it with

165
00:11:41,600 --> 00:11:44,320
 your addiction.

166
00:11:45,920 --> 00:11:52,080
 So that's the first one, "Sila Parisa Dhi Madhan Yanga."

167
00:11:52,080 --> 00:11:58,400
 Second one is the thing that should be,

168
00:11:58,400 --> 00:12:02,160
 ought to be purified,

169
00:12:02,160 --> 00:12:09,360
 which we call the mind, the "jitta Parisa" the mind,

170
00:12:09,360 --> 00:12:13,190
 or not the mind-mind. It's a difficult one to translate

171
00:12:13,190 --> 00:12:14,240
 because literally it does

172
00:12:14,240 --> 00:12:20,480
 mean the mind or minds, but it's not exactly that, it's the

173
00:12:20,480 --> 00:12:24,390
 purification of mind states because it's not the end of the

174
00:12:24,390 --> 00:12:24,720
 line,

175
00:12:24,720 --> 00:12:29,480
 right? "Jitta Parisa Dhi" is just, is actually just a

176
00:12:29,480 --> 00:12:32,000
 concentration.

177
00:12:32,000 --> 00:12:36,000
 So your mind is pure, that's great, but it's not your whole

178
00:12:36,000 --> 00:12:36,400
 mind.

179
00:12:36,400 --> 00:12:40,880
 It's not the mind in an abstract sense, it's just a mind in

180
00:12:40,880 --> 00:12:45,840
 in a absolute sense, in the sense of this mind being pure,

181
00:12:45,840 --> 00:12:48,080
 this moment of mind.

182
00:12:48,080 --> 00:12:51,840
 So as you practice meditation you'll find,

183
00:12:51,840 --> 00:12:54,550
 after some time, after you become proficient, you'll find

184
00:12:54,550 --> 00:12:55,200
 these states

185
00:12:55,200 --> 00:12:58,720
 arising where you're peaceful, where you're pure,

186
00:12:58,720 --> 00:13:01,680
 where your mind just feels completely free from

187
00:13:01,680 --> 00:13:07,710
 attachment or craving or aversion. Your mind will be in a

188
00:13:07,710 --> 00:13:08,960
 clear state, in a pure

189
00:13:08,960 --> 00:13:11,360
 state.

190
00:13:11,360 --> 00:13:15,440
 And we talk about the four jhanas and they're just

191
00:13:15,440 --> 00:13:20,320
 explanations about these sorts of states of mind where

192
00:13:20,320 --> 00:13:23,760
 you don't have any liking or disliking or drowsiness or

193
00:13:23,760 --> 00:13:27,040
 distraction or doubt.

194
00:13:27,040 --> 00:13:33,520
 No pleasant, no like, no judgment, no reaction, just purity

195
00:13:33,520 --> 00:13:35,760
 of mind.

196
00:13:35,760 --> 00:13:38,270
 And that's quite useful because it allows you to see

197
00:13:38,270 --> 00:13:38,960
 clearly if you

198
00:13:38,960 --> 00:13:42,640
 then focus that on reality and be able to see things in

199
00:13:42,640 --> 00:13:43,440
 ways that you

200
00:13:43,440 --> 00:13:46,880
 weren't able to see things when you were judging them,

201
00:13:46,880 --> 00:13:51,290
 when you were reacting to them. So that's really what the

202
00:13:51,290 --> 00:13:54,560
 meditation is all about.

203
00:13:54,560 --> 00:14:03,360
 Meditation is getting yourself in the frame of mind

204
00:14:03,360 --> 00:14:05,790
 so that you can accomplish the third one which is pur

205
00:14:05,790 --> 00:14:08,160
ification of view,

206
00:14:08,160 --> 00:14:12,920
 views and opinions, clearing yourselves up, clearing

207
00:14:12,920 --> 00:14:16,720
 yourself up, your mind up

208
00:14:16,720 --> 00:14:24,560
 in regards to your beliefs, your opinions, your

209
00:14:24,560 --> 00:14:28,000
 the things you hold to be true.

210
00:14:29,600 --> 00:14:32,880
 And this is really the heart of the matter because

211
00:14:32,880 --> 00:14:37,600
 it's not about calming yourself down, it's about

212
00:14:37,600 --> 00:14:42,490
 rightening yourself, purifying the source because the

213
00:14:42,490 --> 00:14:43,840
 source of our mind

214
00:14:43,840 --> 00:14:47,360
 states, the source which determines whether we're

215
00:14:47,360 --> 00:14:51,440
 going to have a wholesome mind or an unwholesome

216
00:14:51,440 --> 00:14:57,360
 mind, a pure mind or an impure mind is our view, our

217
00:14:57,360 --> 00:15:02,290
 outlook. So if you have the view that certain things are

218
00:15:02,290 --> 00:15:03,200
 worth attaining,

219
00:15:03,200 --> 00:15:05,890
 you'll strive after them. If you have this view that

220
00:15:05,890 --> 00:15:07,920
 certain things are

221
00:15:07,920 --> 00:15:11,520
 worth avoiding, you'll avoid them.

222
00:15:11,520 --> 00:15:15,720
 If the view that it's right to good to be angry and greedy

223
00:15:15,720 --> 00:15:16,800
 and deluded, then

224
00:15:16,800 --> 00:15:20,080
 you'll do things that give rise to those

225
00:15:20,080 --> 00:15:24,240
 those states and you'll give rise to those things.

226
00:15:24,720 --> 00:15:30,750
 If you have views that are views that greed, anger and

227
00:15:30,750 --> 00:15:32,400
 delusion are

228
00:15:32,400 --> 00:15:35,760
 wrong or you see them as being a problem, then you're less

229
00:15:35,760 --> 00:15:36,720
 likely to give rise

230
00:15:36,720 --> 00:15:41,280
 to those emotions. So it's about purifying your view and in

231
00:15:41,280 --> 00:15:46,230
 fact it's not about attaining any one view or any outlook,

232
00:15:46,230 --> 00:15:46,880
 it's not about

233
00:15:46,880 --> 00:15:51,840
 acquiring an outlook, it's about acquiring an understanding

234
00:15:51,840 --> 00:15:52,400
 of things

235
00:15:52,400 --> 00:15:56,560
 as they are. So it's not really a view in the end,

236
00:15:56,560 --> 00:16:00,390
 except in the literal sense that you're able to view things

237
00:16:00,390 --> 00:16:01,920
 as they are.

238
00:16:01,920 --> 00:16:04,940
 Your view is in line with reality. That's really all we're

239
00:16:04,940 --> 00:16:06,080
 aiming for. We're not

240
00:16:06,080 --> 00:16:10,400
 aiming to claim anything. The only claim we make is

241
00:16:10,400 --> 00:16:15,120
 that if you look, you'll see things as they are and once

242
00:16:15,120 --> 00:16:18,600
 you see things as they are, you will agree with us that

243
00:16:18,600 --> 00:16:18,960
 they are

244
00:16:18,960 --> 00:16:22,960
 this way, that we can all come to an agreement

245
00:16:22,960 --> 00:16:26,490
 on the way things are. A big part of this, as I've

246
00:16:26,490 --> 00:16:29,120
 mentioned before, is

247
00:16:29,120 --> 00:16:32,820
 a paradigm shift and it's what I tried to explain in the

248
00:16:32,820 --> 00:16:34,400
 second volume

249
00:16:34,400 --> 00:16:37,930
 on how to meditate and I think the first chapter, one of

250
00:16:37,930 --> 00:16:40,320
 the first chapters,

251
00:16:40,320 --> 00:16:44,960
 is that we tend to look at reality in terms of

252
00:16:44,960 --> 00:16:50,160
 the world around us. Obviously, I mean who doesn't, right?

253
00:16:50,160 --> 00:16:53,000
 If you ask what is real, well this room, you know, we're

254
00:16:53,000 --> 00:16:54,240
 sitting in this room but

255
00:16:54,240 --> 00:16:58,690
 in fact that's really just a projection. All of that is

256
00:16:58,690 --> 00:17:00,000
 still dependent upon

257
00:17:00,000 --> 00:17:05,680
 something more basic and that's where we shift our focus.

258
00:17:05,680 --> 00:17:08,880
 So instead of looking at the world in terms of

259
00:17:08,880 --> 00:17:12,180
 the room around us, we look at it in terms of our

260
00:17:12,180 --> 00:17:13,120
 perception

261
00:17:13,120 --> 00:17:16,160
 of what then becomes the room around us and so our

262
00:17:16,160 --> 00:17:18,240
 experience,

263
00:17:18,240 --> 00:17:23,150
 we base reality on experience and that's the beginning of

264
00:17:23,150 --> 00:17:24,640
 right view.

265
00:17:24,640 --> 00:17:30,160
 Because without that, there's no way we can

266
00:17:30,160 --> 00:17:34,400
 be sure that we're all going to come to the same conclusion

267
00:17:34,400 --> 00:17:38,880
 because the room here is abstract. Some people think it's

268
00:17:38,880 --> 00:17:43,120
 nice, some don't like it. Our perception of the room can be

269
00:17:43,120 --> 00:17:43,360
 quite

270
00:17:43,360 --> 00:17:45,830
 different. Some people looking at it from one direction,

271
00:17:45,830 --> 00:17:47,360
 some people from another.

272
00:17:47,360 --> 00:17:50,640
 This is what leads to

273
00:17:50,640 --> 00:17:57,920
 in the world it leads to so much conflict and confusion and

274
00:17:57,920 --> 00:18:03,040
 misunderstanding and so on. But you can't have that when

275
00:18:03,040 --> 00:18:03,360
 you look

276
00:18:03,360 --> 00:18:07,810
 at experience. Experience is more basic and it doesn't

277
00:18:07,810 --> 00:18:08,240
 change

278
00:18:08,240 --> 00:18:12,240
 based on your perception of it. It is what it is. You might

279
00:18:12,240 --> 00:18:12,880
 want it to be

280
00:18:12,880 --> 00:18:18,800
 otherwise but it is what is real.

281
00:18:18,800 --> 00:18:23,440
 So that's what comes and goes and changes.

282
00:18:23,440 --> 00:18:31,520
 That's a ditthi parisuddhi.

283
00:18:31,520 --> 00:18:35,840
 At number four, wimutti parisuddhi is purification of

284
00:18:35,840 --> 00:18:37,200
 freedom or purification

285
00:18:37,200 --> 00:18:42,080
 through freedom maybe. It's the fourth. Once you have right

286
00:18:42,080 --> 00:18:42,400
 view,

287
00:18:42,400 --> 00:18:46,400
 right view is for what? Right view is for freedom.

288
00:18:46,400 --> 00:18:49,590
 We want to see things as they are so we can free ourselves

289
00:18:49,590 --> 00:18:51,200
 from

290
00:18:51,200 --> 00:18:54,520
 the prison that we've placed ourselves in. This prison of

291
00:18:54,520 --> 00:18:56,080
 desire and aversion

292
00:18:56,080 --> 00:19:04,640
 and delusion. We trap ourselves. We bind ourselves to

293
00:19:04,640 --> 00:19:07,380
 certain the way we think things should be and the way we

294
00:19:07,380 --> 00:19:10,480
 think things shouldn't be

295
00:19:10,480 --> 00:19:15,600
 by the things we think I am and the things I am not.

296
00:19:15,600 --> 00:19:20,560
 The things I want to be, the things I don't want to be.

297
00:19:20,560 --> 00:19:31,600
 And so this fourth one, Ananda says

298
00:19:33,520 --> 00:19:38,360
 with the other three, with purification of sīla, jita and

299
00:19:38,360 --> 00:19:40,640
 ditthi,

300
00:19:40,640 --> 00:19:44,800
 one

301
00:19:44,800 --> 00:19:51,600
 rajjaniye sudhammī sujitāng wirajitī. One becomes disp

302
00:19:51,600 --> 00:19:52,960
assionate about

303
00:19:52,960 --> 00:19:58,400
 dhammas, about things that one might be passionate about.

304
00:19:58,400 --> 00:20:02,240
 wimo janiye sudhammī sujitāng wimojitī. One frees

305
00:20:02,240 --> 00:20:04,080
 oneself from dhammas, from

306
00:20:04,080 --> 00:20:08,960
 things that one should free oneself from.

307
00:20:08,960 --> 00:20:14,080
 And having done so, samāwimutting pusatī. One touches or

308
00:20:14,080 --> 00:20:15,840
 attains

309
00:20:15,840 --> 00:20:19,760
 right release, right freedom. Ayam uchitī wimutti parīsud

310
00:20:19,760 --> 00:20:20,640
ī. This is called

311
00:20:20,640 --> 00:20:26,960
 purification through release or unreleased.

312
00:20:26,960 --> 00:20:36,160
 So these are the four parīsudī padān yangānī

313
00:20:36,160 --> 00:20:46,800
 that were taught by that blessed one who is nose and

314
00:20:46,800 --> 00:20:52,000
 sees and enlightened self, self-enlightened

315
00:20:52,000 --> 00:20:57,200
 Buddha. They were taught for the purification of

316
00:20:57,200 --> 00:21:01,440
 beings, for the overcoming of sorrow, lamentation and

317
00:21:01,440 --> 00:21:05,120
 despair, for the

318
00:21:05,120 --> 00:21:08,880
 eradication, for the overcoming of

319
00:21:08,880 --> 00:21:18,560
 samātikāmāya, for the destruction of physical pain and

320
00:21:18,560 --> 00:21:20,880
 mental pain,

321
00:21:20,880 --> 00:21:25,140
 and for giving up or freeing oneself, for attaining the

322
00:21:25,140 --> 00:21:26,720
 right path and for

323
00:21:26,720 --> 00:21:32,080
 seeing for oneself nirbāṇḍa.

324
00:21:32,080 --> 00:21:36,610
 That's a standard. That's actually what the Buddha taught.

325
00:21:36,610 --> 00:21:36,880
 It's a quote from

326
00:21:36,880 --> 00:21:39,920
 the Buddha from the Satipatāna sūta.

327
00:21:39,920 --> 00:21:44,960
 Satānān wisudīya sokaparī devānāṁ samatikāmāya.

328
00:21:44,960 --> 00:21:57,620
 dukkha dōmanasānam atangamāya, nāyasa adikāmāya, nimb

329
00:21:57,620 --> 00:21:57,840
ānasā sācikīyayāya.

330
00:21:57,840 --> 00:22:07,600
 So four good things, another way of looking at the path.

331
00:22:07,600 --> 00:22:10,860
 There's a lot of these where the Buddha would, Ananda's

332
00:22:10,860 --> 00:22:11,600
 taking this from the

333
00:22:11,600 --> 00:22:16,800
 Buddha's teaching. This is four things that really

334
00:22:16,800 --> 00:22:22,320
 condense the path, because when you're teaching

335
00:22:22,320 --> 00:22:26,000
 you have to remind people of the roadmap, the direction,

336
00:22:26,000 --> 00:22:31,200
 where are we going? And this is a complete roadmap.

337
00:22:31,200 --> 00:22:35,120
 You start with seela and then with morality you

338
00:22:35,120 --> 00:22:38,880
 train yourself in right behavior and right speech

339
00:22:38,880 --> 00:22:43,040
 to keep yourself ethical, moral, virtuous, and then you

340
00:22:43,040 --> 00:22:46,600
 begin to focus the mind and so you practice meditation. As

341
00:22:46,600 --> 00:22:47,840
 you do that

342
00:22:47,840 --> 00:22:51,450
 you start to see things clearly. You start to see the four

343
00:22:51,450 --> 00:22:52,080
 noble truths

344
00:22:52,080 --> 00:22:58,240
 in the end. You see nāmārūpa, you see three

345
00:22:58,240 --> 00:23:01,760
 characteristics, you see the four noble truths.

346
00:23:01,760 --> 00:23:06,080
 And then finally vimuttīya become free.

347
00:23:06,560 --> 00:23:10,880
 Quite simple. It's good to have these guides because it

348
00:23:10,880 --> 00:23:13,760
 keeps us, reminds us of

349
00:23:13,760 --> 00:23:19,040
 very straight and straightforward nature of the

350
00:23:19,040 --> 00:23:21,680
 Buddha's teaching, not getting caught up in details or

351
00:23:21,680 --> 00:23:22,800
 getting

352
00:23:22,800 --> 00:23:26,560
 off track. It's so easy for us to get, let our minds

353
00:23:26,560 --> 00:23:32,460
 allow us to wander and get lost and on the wrong path, a

354
00:23:32,460 --> 00:23:34,320
 path that leads to

355
00:23:34,320 --> 00:23:37,720
 stress and suffering. It's good for us to remember these

356
00:23:37,720 --> 00:23:40,000
 things.

357
00:23:40,000 --> 00:23:45,360
 Okay so that's the nāmārūpa for this evening.

358
00:23:45,360 --> 00:23:53,360
 If you have any questions.

359
00:23:53,360 --> 00:24:10,400
 [Music]

360
00:24:10,400 --> 00:24:12,650
 A guy who couldn't quit talking even when another guy was

361
00:24:12,650 --> 00:24:13,760
 shooting dung into his

362
00:24:13,760 --> 00:24:24,800
 phone. Huh, that's from the jātakas.

363
00:24:24,800 --> 00:24:28,080
 Which jātaka now?

364
00:24:28,080 --> 00:24:35,760
 Are fully enlightened beings amoral? Good question.

365
00:24:35,760 --> 00:24:42,320
 No I wouldn't. I guess I wouldn't say that.

366
00:24:42,960 --> 00:24:46,960
 [Music]

367
00:24:46,960 --> 00:24:52,610
 I mean it really depends how you look at it and it's just

368
00:24:52,610 --> 00:24:54,320
 words right?

369
00:24:54,320 --> 00:24:59,440
 So some people might want to say that.

370
00:25:06,560 --> 00:25:11,550
 Some people might want to say that but they're actually

371
00:25:11,550 --> 00:25:12,720
 quite moral.

372
00:25:12,720 --> 00:25:17,120
 Enlightened being keeps the precepts purely

373
00:25:17,120 --> 00:25:20,640
 and will not break from them.

374
00:25:20,640 --> 00:25:27,490
 But that's really because there's a sense that morality is

375
00:25:27,490 --> 00:25:29,520
 intrinsic to

376
00:25:29,520 --> 00:25:38,240
 reality. Reality is not immoral. Reality is

377
00:25:38,240 --> 00:25:44,050
 moral. It's from a dhammapada. I don't think the story

378
00:25:44,050 --> 00:25:47,440
 itself. Maybe it is.

379
00:25:47,440 --> 00:25:54,960
 Pretty sure it's from the jātakas though. Let me see.

380
00:25:54,960 --> 00:25:59,370
 I'm pretty sure it's a jātaka story. It may be in the dham

381
00:25:59,370 --> 00:26:00,560
mapada as well.

382
00:26:00,560 --> 00:26:05,040
 Or it may, I may be wrong. I can't remember. But

383
00:26:05,040 --> 00:26:09,200
 I know the story. I can't think off the top of my head.

384
00:26:09,200 --> 00:26:11,760
 Exact.

385
00:26:11,760 --> 00:26:20,400
 Jātaka. Well the jātakas are on the internet. Just read

386
00:26:20,400 --> 00:26:22,240
 them.

387
00:26:22,240 --> 00:26:28,080
 Read them and tell us which one it is. There's only 547 of

388
00:26:28,080 --> 00:26:28,080
 them.

389
00:26:28,080 --> 00:26:31,920
 547.

390
00:26:31,920 --> 00:26:38,800
 I can probably find them here.

391
00:26:38,800 --> 00:26:42,480
 I can find them there.

392
00:26:42,480 --> 00:26:48,160
 [Silence]

393
00:26:48,160 --> 00:26:53,840
 [Silence]

394
00:26:53,840 --> 00:26:57,840
 [Silence]

395
00:26:57,840 --> 00:27:01,840
 [Silence]

396
00:27:01,840 --> 00:27:05,840
 [Silence]

397
00:27:05,840 --> 00:27:09,840
 [Silence]

398
00:27:09,840 --> 00:27:13,840
 [Silence]

399
00:27:13,840 --> 00:27:25,520
 [Silence]

400
00:27:25,520 --> 00:27:31,120
 Here. When we drop the ball of dham.

401
00:27:31,120 --> 00:27:43,120
 [Silence]

402
00:27:43,120 --> 00:27:46,880
 No, there's too many of them. Wait, they're there.

403
00:27:46,880 --> 00:27:53,760
 [Silence]

404
00:27:53,760 --> 00:27:57,620
 It is in the dhammapada. Anyway, if anybody finds it, you

405
00:27:57,620 --> 00:27:59,360
 want to please pass along.

406
00:27:59,360 --> 00:28:07,840
 [Silence]

407
00:28:07,840 --> 00:28:14,320
 It was, yeah, I'm pretty sure because it starts with

408
00:28:14,320 --> 00:28:20,320
 trying to get this guy to be quiet. And so every time he

409
00:28:20,320 --> 00:28:22,720
 talks, he should

410
00:28:22,720 --> 00:28:27,090
 ball of dham into his mouth. And I'm pretty sure it's a

411
00:28:27,090 --> 00:28:27,760
 cause for

412
00:28:27,760 --> 00:28:33,280
 telling a story of the past. Pretty sure it's a cause for a

413
00:28:33,280 --> 00:28:33,840
 jātaka.

414
00:28:33,840 --> 00:28:39,440
 For telling a jātaka. Anyway, yes, we should purify our

415
00:28:39,440 --> 00:28:41,520
 virtue.

416
00:28:41,520 --> 00:28:44,720
 I have to work on it.

417
00:28:44,720 --> 00:28:57,680
 [Silence]

418
00:28:57,680 --> 00:29:01,200
 Anyway, any other questions?

419
00:29:03,040 --> 00:29:08,480
 Not. And say have a good night.

420
00:29:08,480 --> 00:29:11,520
 See you all tomorrow.

421
00:29:11,520 --> 00:29:15,200
 Okay, goodnight.

422
00:29:15,200 --> 00:29:20,080
 [Silence]

