 [ The
 Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 [The Lord's Prayer]
 Having done good deeds in the past is a blessing because of
 the effect that it has on the mind.
 Having done good deeds in the past has the effect of
 calming and purifying the mind.
 In fact, the word "good deeds" is a translation of the word
 "punya".
 Punya is a word which means "to purify".
 "Santanang puneti, wisodeeti, punang punyang"
 It's called bhunya because it purifies, it cleans the
 consciousness, because it cleans one's being.
 The good things which we do, for instance, generosity,
 giving support, giving help, giving items which are of use
 to other people,
 giving with a free heart, and so on. Morality, avoiding
 doing bad deeds, avoiding killing,
 refraining from killing, from stealing from adultery, from
 lying, refraining from drugs and alcohol.
 Or even for meditators, taking on the life of chastity, not
 eating in the afternoon, avoiding entertainment and
 decoration,
 avoiding refraining from higher luxurious paths, and so on.
 For novices, ten ros. For monks, 227 ros.
 This is all morality, it's a kind of bhunya, it's a kind of
 way of purifying one's mind, purifying oneself.
 When we've done this in the past, our minds will be calm,
 we have morality, our minds will be stable.
 Even when we give support to other people, when we give
 support or help or even teach to other people,
 this is a kind of a gift that just purifies our mind.
 And the third type of bhunya, of good deed that we can do
 is meditation.
 For all of us, we can feel already that this is true.
 When you've practiced meditation in the past, it's a
 blessing in the ultimate sense.
 Because it purifies and cleanses the mind. When the mind is
 purified and cleansed already,
 one can live one's life in peace and happiness and comfort
 and well-being.
 One doesn't have to be bothered by greed or anger or
 delusion.
 This mind is clear, one's mind is pure.
 This is by having done these things as a good deed.
 Some people, they only look to a short distance.
 They practice meditation and they have to suffer and they
 have to fight with all sorts of unpleasant conditions.
 They think that meditation has no purpose.
 Really, they don't see that they're actually fighting or
 wrestling with themselves,
 wrestling with all of the bad things which exist inside.
 Working hard to be patient and overcome these bad and
 wholesome states.
 And that having done this, pupayi takata, having done this
 already, in the future,
 their life will be blessed. They'll be blessed with peace,
 with happiness, with well-being.
 The second, the fifth blessing of all.
 The sixth is ata sama bhannithi, to set oneself on the
 right path, on the right course.
 And of course, the right path, the right course of action,
 the right course to take in one's life,
 is the path of the eightfold noble path.
 Again, noble means it's something which leads one to be
 noble,
 which wants one's mind to be free from greed, free from
 anger, free from delusion,
 free from jealousy and stinginess and concede the wrong
 view and so on.
 It's the path which leads one's mind to be pure.
 It's the path which leads one's mind to be pure and it has
 eight parts.
 Eight parts, but in brief, the eightfold noble path can be
 summarized up in three parts.
 The first part, morality.
 Morality is right action, right speech, right speech, right
 action, right livelihood.
 It's morality. Concentration, concentration is right effort
, right mindfulness, right concentration.
 And the third one, wisdom. Wisdom is right view and right
 thought.
 So the eight parts of the Eightfold Noble Path, in brief,
 to make it easy to understand,
 is set in three parts. Morality, keeping the body and the
 speech pure,
 and keeping the mind from running off into defilement.
 Guarding the mind from falling into evil.
 Guarding the mind, keeping it with the body, keeping it
 with the feelings, the mind, the fedamas.
 The moral and concentration, keeping the mind fixed on the
 object.
 Once the mind is not running here, not running there,
 in one's body and one's speech are calm or quiet or
 tranquil,
 one can develop great states of concentration.
 One's mind becomes fixed, becomes focused, becomes set on a
 single object.
 We acknowledge rising, it's fixed on the rising, we
 acknowledge falling, it's fixed on the falling.
 This is called lakanupani jana.
 It's meditation on the three characteristics.
 Meditation on the impermanence, meditation on suffering,
 meditation on jana.
 It's jana based on impermanence, based on suffering, based
 on non-self.
 To see the truth about reality, it's a kind of jana, it's
 concentration.
 And wisdom, when we acknowledge rising and falling,
 the wisdom which arises, when we focus and see impermanent
 suffering and non-self,
 this is the wisdom part.
 It leads us to see also the Four Noble Truths.
 When we see impermanent suffering and non-self,
 we come to let go of the cause of suffering.
 It means we come to abandon craving for the body, craving
 for the mind,
 craving for the whole world around us.
 The mind becomes set and established, fixed like a stone,
 like a rock.
 The mind becomes solid like a rock, unmoved by the winds of
 change, unmoved by the ways of the world.
 The mind becomes fixed and firm, not running here and there
 after all of the objects of the sense.
 The mind sees the truth of suffering, is the cause of
 suffering.
 By abandoning the cause, one follows the right path.
 By abandoning craving, this is following the right path.
 The mind has morality, concentration and wisdom.
 And one reaches the goal of life which is the cessation of
 suffering,
 the end of suffering, no more suffering.
 No more sorrow, lamentation and despair.
 No more bodily suffering, no more mental suffering.
 No more not getting what we want, no more getting what we
 don't want,
 no more having our wishes unfulfilled.
 In the end we feel more suffering, no more body suffering,
 no more mind suffering.
 We will be in peace and happiness at all times.
 So these three are three more blessings among the 38
 blessings that the Lord would have.
 You can see that these ones have a lot to do with Vipassana
 meditation.
 We practice meditation, it's important to live in the right
 place.
 When we practice meditation our minds become pure and we
 follow the right path.
 The pinnacle of the Lord Buddha's teaching is Vipassana
 meditation.
 Vipassana to see clearly is what the Lord Buddha taught.
 That we should not be confused or misunderstanding about
 the things around us or the things inside of ourselves.
 So here is all the more reason that we continue to practice
 Vipassana meditation
 so that we come to gain the highest blessings in the world,
 the highest blessings which exist in an ultimate sense.
