1
00:00:00,000 --> 00:00:04,560
 Drowsiness, question and answer.

2
00:00:04,560 --> 00:00:08,940
 Falling asleep while meditating.

3
00:00:08,940 --> 00:00:10,540
 Question.

4
00:00:10,540 --> 00:00:14,380
 How can I deal with falling asleep when meditating?

5
00:00:14,380 --> 00:00:16,860
 I would like to use meditation for the purpose of being

6
00:00:16,860 --> 00:00:17,660
 rested,

7
00:00:17,660 --> 00:00:22,020
 but I also still want to be alert.

8
00:00:22,020 --> 00:00:24,180
 Answer.

9
00:00:24,180 --> 00:00:26,700
 I would like to caution that your intention to rest

10
00:00:26,700 --> 00:00:30,500
 may be the reason why you are falling asleep.

11
00:00:30,500 --> 00:00:33,120
 If that is the only intention that you go into the

12
00:00:33,120 --> 00:00:34,760
 meditation with,

13
00:00:34,760 --> 00:00:38,360
 then that is the direction your mind will take.

14
00:00:38,360 --> 00:00:40,620
 With the intention to feel rested,

15
00:00:40,620 --> 00:00:44,780
 your mind will not incline towards cultivating effort.

16
00:00:44,780 --> 00:00:47,940
 Rather than becoming more awake or alert,

17
00:00:47,940 --> 00:00:51,640
 it will tend towards the only way that it knows how to rest

18
00:00:51,640 --> 00:00:51,640
,

19
00:00:51,640 --> 00:00:54,380
 which is to fall asleep.

20
00:00:54,380 --> 00:00:57,880
 Meditation practiced properly certainly does improve your

21
00:00:57,880 --> 00:00:59,540
 mind's restfulness,

22
00:00:59,540 --> 00:01:01,980
 but it can only do this if you are alert enough

23
00:01:01,980 --> 00:01:05,780
 to confront the causes of your exhaustion.

24
00:01:05,780 --> 00:01:09,280
 The purpose of meditating is to understand reality,

25
00:01:09,280 --> 00:01:12,440
 especially relating to the difficulties and challenges

26
00:01:12,440 --> 00:01:15,700
 you face in your mind and your life in general,

27
00:01:15,700 --> 00:01:19,040
 which requires you to pay attention.

28
00:01:19,040 --> 00:01:23,600
 Try to re-evaluate what you expect to get out of meditation

29
00:01:23,600 --> 00:01:23,600
.

30
00:01:23,600 --> 00:01:28,980
 Resting is not the purpose of practicing mindfulness.

31
00:01:28,980 --> 00:01:30,900
 That being said,

32
00:01:30,900 --> 00:01:34,410
 even meditators who understand what it is that meditation

33
00:01:34,410 --> 00:01:34,820
 is for

34
00:01:34,820 --> 00:01:37,960
 and use it for the purpose of coming to understand reality

35
00:01:37,960 --> 00:01:39,380
 as it is,

36
00:01:39,380 --> 00:01:44,300
 may find themselves feeling drowsy and even falling asleep.

37
00:01:44,300 --> 00:01:47,340
 The Buddha taught seven ways of dealing with drowsiness

38
00:01:47,340 --> 00:01:49,620
 in the Pachala Yamana Sutta,

39
00:01:49,620 --> 00:01:54,100
 or Pachala Sutta of the Nghuttara Nikaya Book of Sevens

40
00:01:54,100 --> 00:01:59,980
 that one could use to overcome various kinds of drowsiness.

41
00:01:59,980 --> 00:02:03,220
 The first method the Buddha taught is to change or examine

42
00:02:03,220 --> 00:02:06,180
 the object of your attention.

43
00:02:06,180 --> 00:02:09,210
 One of the most obvious reasons why someone might become d

44
00:02:09,210 --> 00:02:09,540
rowsy

45
00:02:09,540 --> 00:02:13,580
 is because their mind has begun to wander.

46
00:02:13,580 --> 00:02:18,020
 You may begin meditating by focusing on a specific object,

47
00:02:18,020 --> 00:02:20,540
 but over time your mind may start to wander

48
00:02:20,540 --> 00:02:24,170
 and slowly you may fall into a trance-like state that is

49
00:02:24,170 --> 00:02:25,860
 bordering on sleep

50
00:02:25,860 --> 00:02:29,780
 and eventually leading you to actually nod off.

51
00:02:29,780 --> 00:02:31,140
 When that is the case,

52
00:02:31,140 --> 00:02:35,170
 it is important to refocus your attention on the original

53
00:02:35,170 --> 00:02:37,700
 object of meditation.

54
00:02:37,700 --> 00:02:40,940
 Be careful not to let yourself fall into reflection on

55
00:02:40,940 --> 00:02:42,180
 speculative thoughts

56
00:02:42,180 --> 00:02:45,540
 that lead your mind to wander.

57
00:02:45,540 --> 00:02:48,530
 It is often the case that we remember things that we are

58
00:02:48,530 --> 00:02:50,740
 worried or concerned about

59
00:02:50,740 --> 00:02:53,540
 and they lead our minds to wander and speculate

60
00:02:53,540 --> 00:02:57,220
 and eventually get tired and fall asleep.

61
00:02:57,220 --> 00:02:59,840
 The Buddha said to be careful not to give those sorts of

62
00:02:59,840 --> 00:03:01,260
 thoughts any ground

63
00:03:01,260 --> 00:03:04,990
 and to try not to cultivate the states of mind that lead to

64
00:03:04,990 --> 00:03:06,700
 drowsiness.

65
00:03:06,700 --> 00:03:10,740
 The best way to accomplish this task is through mindfulness

66
00:03:10,740 --> 00:03:10,740
,

67
00:03:10,740 --> 00:03:15,180
 especially mindfulness of the drowsiness itself.

68
00:03:15,180 --> 00:03:18,210
 Instead of allowing the drowsiness to lead you away from

69
00:03:18,210 --> 00:03:19,660
 the present moment,

70
00:03:19,660 --> 00:03:23,410
 focus on the drowsiness and look at it as the present

71
00:03:23,410 --> 00:03:24,420
 object.

72
00:03:24,420 --> 00:03:28,350
 Being mindful in this way breaks the cycle that increases

73
00:03:28,350 --> 00:03:29,620
 the drowsiness

74
00:03:29,620 --> 00:03:31,500
 and if you are persistent,

75
00:03:31,500 --> 00:03:35,740
 sometimes the drowsiness will disappear by itself.

76
00:03:35,740 --> 00:03:39,130
 The attention and the alert awareness of the drowsiness is

77
00:03:39,130 --> 00:03:41,340
 the opposite of drowsiness

78
00:03:41,340 --> 00:03:45,910
 and so the drowsiness decreases because of the change in

79
00:03:45,910 --> 00:03:47,660
 one's mind state.

80
00:03:47,660 --> 00:03:51,340
 The second method, if mindfulness does not work,

81
00:03:51,340 --> 00:03:54,240
 is to recall the teachings that the Buddha taught or that

82
00:03:54,240 --> 00:03:55,740
 your teacher gave you

83
00:03:55,740 --> 00:03:58,860
 and to go over them in your mind.

84
00:03:58,860 --> 00:04:02,340
 For instance, think about the four foundations of

85
00:04:02,340 --> 00:04:03,180
 mindfulness,

86
00:04:03,180 --> 00:04:07,740
 the body, the feelings, the mind, and the various stomas

87
00:04:07,740 --> 00:04:08,940
 like the hindrances,

88
00:04:08,940 --> 00:04:12,300
 the senses, and so on.

89
00:04:12,300 --> 00:04:14,540
 You can consider the body.

90
00:04:14,540 --> 00:04:17,660
 Think about your level of awareness of the body.

91
00:04:17,660 --> 00:04:20,310
 Ask yourself if you are actually able to watch the

92
00:04:20,310 --> 00:04:21,580
 movements of the stomach

93
00:04:21,580 --> 00:04:25,260
 or be aware of the sitting posture.

94
00:04:25,260 --> 00:04:28,920
 You can ask yourself whether you are mindful of the

95
00:04:28,920 --> 00:04:29,580
 feelings.

96
00:04:29,580 --> 00:04:32,990
 When there is a painful feeling, are you actually paying

97
00:04:32,990 --> 00:04:33,980
 attention to it,

98
00:04:33,980 --> 00:04:38,380
 saying to yourself, "Pain, pain?"

99
00:04:38,380 --> 00:04:41,590
 If you feel happy or calm, are you actually paying

100
00:04:41,590 --> 00:04:42,220
 attention

101
00:04:42,220 --> 00:04:45,700
 or are you instead letting it drag you down into a state of

102
00:04:45,700 --> 00:04:46,700
 lethargy,

103
00:04:46,700 --> 00:04:49,900
 fatigue, and drowsiness?

104
00:04:49,900 --> 00:04:54,380
 The same goes for the mind and mind objects.

105
00:04:54,380 --> 00:04:58,920
 Refer back to the teachings and reflect on them in your

106
00:04:58,920 --> 00:04:59,660
 mind.

107
00:04:59,660 --> 00:05:02,940
 Examine the teachings and relate them back to your practice

108
00:05:02,940 --> 00:05:07,020
 and compare your practice to the teachings.

109
00:05:07,020 --> 00:05:10,760
 If you do this, first, just thinking about these good

110
00:05:10,760 --> 00:05:11,100
 things

111
00:05:11,100 --> 00:05:15,730
 might energize the mind, reminding you of what you should

112
00:05:15,730 --> 00:05:16,540
 be doing.

113
00:05:16,540 --> 00:05:22,980
 And second, it will, of course, allow you to adjust your

114
00:05:22,980 --> 00:05:24,460
 practice.

115
00:05:24,460 --> 00:05:28,150
 If reflection does not work, the third way to deal with the

116
00:05:28,150 --> 00:05:28,860
 drowsiness

117
00:05:28,860 --> 00:05:32,140
 is to actually recite the teachings.

118
00:05:32,140 --> 00:05:35,260
 This can be quite useful for laypeople.

119
00:05:35,260 --> 00:05:39,260
 For example, when you are driving.

120
00:05:39,260 --> 00:05:43,340
 Mindfulness practice while driving, especially at night,

121
00:05:43,340 --> 00:05:47,240
 can unfortunately involve periods of drowsiness and even

122
00:05:47,240 --> 00:05:49,020
 nodding off.

123
00:05:49,020 --> 00:05:53,150
 In such cases, it is safer to switch to reciting the

124
00:05:53,150 --> 00:05:55,180
 Buddhist teachings.

125
00:05:55,180 --> 00:05:59,180
 Recitation is not really an intellectual exercise.

126
00:05:59,180 --> 00:06:01,980
 It is something akin to singing,

127
00:06:01,980 --> 00:06:05,260
 like when non-meditators turn on the radio and sing along

128
00:06:05,260 --> 00:06:08,620
 when they are driving late at night.

129
00:06:08,620 --> 00:06:12,860
 Recitation of the Buddhist teaching will similarly wake you

130
00:06:12,860 --> 00:06:12,860
 up,

131
00:06:12,860 --> 00:06:16,460
 but also, because it is the Buddhist teachings,

132
00:06:16,460 --> 00:06:20,440
 it will stimulate you and give you the encouragement you

133
00:06:20,440 --> 00:06:21,500
 might need.

134
00:06:21,500 --> 00:06:25,010
 Recitation inclines the mind towards the Buddha and his

135
00:06:25,010 --> 00:06:25,820
 teachings

136
00:06:25,820 --> 00:06:30,060
 and the meditation practice.

137
00:06:30,060 --> 00:06:33,810
 If recitation does not work, the fourth method the Buddha

138
00:06:33,810 --> 00:06:36,460
 taught is more physical.

139
00:06:36,460 --> 00:06:40,860
 The Buddha suggested pulling your ears, rubbing your arms,

140
00:06:40,860 --> 00:06:44,620
 massaging yourself and maybe stretching a bit to wake up

141
00:06:44,620 --> 00:06:48,830
 to get the blood flowing and to give yourself some physical

142
00:06:48,830 --> 00:06:49,660
 energy.

143
00:06:49,660 --> 00:06:53,020
 Some people might even go so far as to practice yoga,

144
00:06:53,020 --> 00:06:56,780
 which could be a very useful technique for waking you up,

145
00:06:56,780 --> 00:06:59,660
 but because it is a different sort of spiritual practice,

146
00:06:59,660 --> 00:07:02,780
 I would not recommend extensive practice of yoga

147
00:07:02,780 --> 00:07:06,780
 in combination with insight meditation.

148
00:07:06,780 --> 00:07:11,030
 Still, there is nothing inherently harmful in practicing

149
00:07:11,030 --> 00:07:11,420
 yoga,

150
00:07:11,420 --> 00:07:14,140
 and it may well be beneficial in moderation

151
00:07:14,140 --> 00:07:18,280
 for the purposes of building energy necessary to practice

152
00:07:18,280 --> 00:07:20,140
 meditation.

153
00:07:20,140 --> 00:07:23,500
 In any case, the Buddha advised massaging,

154
00:07:23,500 --> 00:07:29,340
 rubbing, and pulling your ears, and so on.

155
00:07:29,340 --> 00:07:31,900
 If physical intervention does not work,

156
00:07:31,900 --> 00:07:35,660
 the fifth method to deal with drowsiness is to stand up,

157
00:07:35,660 --> 00:07:40,380
 splash water on your face, and look in all directions.

158
00:07:40,380 --> 00:07:44,620
 Drowsiness usually comes at night, so up at the stars.

159
00:07:44,620 --> 00:07:47,740
 This sort of external focus can help break the imbalance

160
00:07:47,740 --> 00:07:49,260
 between concentration

161
00:07:49,260 --> 00:07:52,860
 and effort, making your mind more energetic,

162
00:07:52,860 --> 00:07:56,700
 throwing off the weight of lethargy and drowsiness.

163
00:07:56,700 --> 00:07:59,870
 One of the reasons for walking meditation is that it helps

164
00:07:59,870 --> 00:08:02,060
 cultivate effort.

165
00:08:02,060 --> 00:08:05,990
 If you only practice sitting meditation, it is not uncommon

166
00:08:05,990 --> 00:08:07,740
 to feel drowsy.

167
00:08:07,740 --> 00:08:10,380
 If you do walking first and then sitting,

168
00:08:10,380 --> 00:08:15,370
 you will generally find the sitting meditation more

169
00:08:15,370 --> 00:08:16,780
 energetic.

170
00:08:16,780 --> 00:08:20,540
 The sixth method is a specific meditation technique

171
00:08:20,540 --> 00:08:24,540
 to think of night as day and to resolve on an awareness of

172
00:08:24,540 --> 00:08:24,940
 light,

173
00:08:24,940 --> 00:08:28,540
 even though it is dark, as perception of darkness can be a

174
00:08:28,540 --> 00:08:31,820
 trigger for drowsiness in the mind.

175
00:08:31,820 --> 00:08:35,340
 The idea is that by imagining light with your eyes closed,

176
00:08:35,340 --> 00:08:39,500
 it will trigger more energetic mind states.

177
00:08:39,500 --> 00:08:42,380
 If you practice meditation for extended periods,

178
00:08:42,380 --> 00:08:47,260
 you may actually begin to see bright lights unintentionally

179
00:08:47,260 --> 00:08:47,260
.

180
00:08:47,260 --> 00:08:51,330
 Some meditators become distracted by such lights, colors,

181
00:08:51,330 --> 00:08:53,020
 or even pictures.

182
00:08:53,020 --> 00:08:56,220
 Do we have to remind them not to be distracted by them?

183
00:08:56,220 --> 00:09:01,180
 To just remind themselves by saying, seeing, seeing,

184
00:09:01,180 --> 00:09:05,020
 remembering that they are just visual stimuli.

185
00:09:05,020 --> 00:09:08,920
 They are not magical and certainly not the path that leads

186
00:09:08,920 --> 00:09:10,300
 to enlightenment.

187
00:09:10,300 --> 00:09:15,630
 They can, however, provide the benefit of bringing about

188
00:09:15,630 --> 00:09:16,940
 energy and effort.

189
00:09:16,940 --> 00:09:19,180
 If none of these methods work,

190
00:09:19,180 --> 00:09:22,340
 the Buddha said one should appreciate the persistence of

191
00:09:22,340 --> 00:09:25,740
 the fatigued state and lie down.

192
00:09:25,740 --> 00:09:29,400
 The Buddha's way of lying down was not on the stomach or

193
00:09:29,400 --> 00:09:29,820
 back,

194
00:09:29,820 --> 00:09:33,370
 but on one side, propping the head up with the hand on the

195
00:09:33,370 --> 00:09:36,380
 head and elbow on the floor.

196
00:09:36,380 --> 00:09:39,420
 This is known as the lion's posture.

197
00:09:39,420 --> 00:09:43,350
 It is a technique that meditators and monks also use to

198
00:09:43,350 --> 00:09:44,780
 keep them awake.

199
00:09:44,780 --> 00:09:47,900
 If you start to fall asleep, your head starts to fall off

200
00:09:47,900 --> 00:09:48,700
 your arm

201
00:09:48,700 --> 00:09:51,660
 and you wake up quite easily.

202
00:09:51,660 --> 00:09:55,580
 It can also be quite painful in the beginning.

203
00:09:55,580 --> 00:09:59,370
 It is a physical technique that you have to develop, just

204
00:09:59,370 --> 00:10:01,980
 like sitting cross-legged.

205
00:10:01,980 --> 00:10:06,520
 The Buddha also noted, however, that you have to accept

206
00:10:06,520 --> 00:10:08,860
 that you might fall asleep.

207
00:10:08,860 --> 00:10:12,510
 If it is not the proper time to sleep, you can lie down and

208
00:10:12,510 --> 00:10:14,300
 make the determination,

209
00:10:14,300 --> 00:10:18,860
 "I am going to get up in so many minutes or so many hours."

210
00:10:18,860 --> 00:10:22,060
 If it is time to sleep, you say to yourself,

211
00:10:22,060 --> 00:10:25,580
 "I am going to sleep for so many minutes or so many hours,"

212
00:10:25,580 --> 00:10:30,220
 or "I am going to get up at such and such a time."

213
00:10:30,220 --> 00:10:33,890
 Before you go to sleep, think only about the time of

214
00:10:33,890 --> 00:10:34,540
 getting up

215
00:10:34,540 --> 00:10:38,730
 so that when that time comes, your mind will wake you up at

216
00:10:38,730 --> 00:10:39,900
 that time.

217
00:10:39,900 --> 00:10:44,540
 It is amazing how powerful the mind can be in this regard.

218
00:10:44,540 --> 00:10:47,660
 You will often find yourself waking up a few minutes before

219
00:10:47,660 --> 00:10:50,140
 the time that you were going to wake up

220
00:10:50,140 --> 00:10:55,500
 or after however many minutes you intended to sleep.

221
00:10:55,500 --> 00:10:59,250
 Once the time has come to get up, the Buddha said to

222
00:10:59,250 --> 00:11:00,620
 resolve in your mind,

223
00:11:00,620 --> 00:11:03,980
 "I will not give in to drowsiness. I will not become

224
00:11:03,980 --> 00:11:08,460
 attached to the pleasure that comes from lying down."

225
00:11:08,460 --> 00:11:11,400
 One of the great addictions that we cling to is the

226
00:11:11,400 --> 00:11:15,020
 addiction to lying down and to sleep.

227
00:11:15,020 --> 00:11:18,780
 We like to lie down whenever we can and sleep a lot because

228
00:11:18,780 --> 00:11:22,220
 of the physical pleasure that comes from it.

229
00:11:22,220 --> 00:11:25,590
 The problem with this physical pleasure, of course, is that

230
00:11:25,590 --> 00:11:27,420
 it is not happiness.

231
00:11:27,420 --> 00:11:31,310
 It is not the case that the more you sleep, the more happy

232
00:11:31,310 --> 00:11:32,220
 you feel.

233
00:11:32,220 --> 00:11:36,710
 With too much sleep, you in fact feel more depressed, leth

234
00:11:36,710 --> 00:11:38,540
argic, and distracted.

235
00:11:38,540 --> 00:11:43,780
 You feel less energetic, less alert, and less at peace with

236
00:11:43,780 --> 00:11:44,780
 yourself.

237
00:11:44,780 --> 00:11:48,900
 With this last technique, you cannot just lie down and say,

238
00:11:48,900 --> 00:11:52,300
 "I am going to go to sleep and fall asleep."

239
00:11:52,300 --> 00:11:56,480
 You have to be strict about it, saying to yourself, "I am

240
00:11:56,480 --> 00:11:59,740
 going to set a definite period of time to sleep,

241
00:11:59,740 --> 00:12:03,300
 and when I wake up, I am going to get up and go on with my

242
00:12:03,300 --> 00:12:04,380
 practice."

