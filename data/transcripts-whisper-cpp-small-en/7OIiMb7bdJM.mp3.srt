1
00:00:00,000 --> 00:00:05,120
 Hello everyone, just a short announcement that we'll be

2
00:00:05,120 --> 00:00:08,680
 starting to broadcast Monk radio

3
00:00:08,680 --> 00:00:10,400
 from my YouTube channel.

4
00:00:10,400 --> 00:00:14,470
 So that's why you're being redirected here if you've gone

5
00:00:14,470 --> 00:00:17,200
 to radio.seriamongolo.org.

6
00:00:17,200 --> 00:00:21,080
 This YouTube channel is, my YouTube channel is now going to

7
00:00:21,080 --> 00:00:22,880
 hopefully be the location

8
00:00:22,880 --> 00:00:28,210
 for future Monk radio broadcasts because we're now able to

9
00:00:28,210 --> 00:00:30,840
 use Google's facilities for that

10
00:00:30,840 --> 00:00:31,840
 purpose.

11
00:00:31,840 --> 00:00:37,340
 So, I hope to see you there this evening, 7.30pm Sri Lankan

12
00:00:37,340 --> 00:00:39,680
 time, which is 2pm UTC.

13
00:00:39,680 --> 00:00:44,580
 You'll have to figure out the appropriate time in your area

14
00:00:44,580 --> 00:00:44,920
.

15
00:00:44,920 --> 00:00:46,520
 So, I hope to see you there.

16
00:00:46,520 --> 00:00:49,940
 It's the same time as normal, so if you've been visiting,

17
00:00:49,940 --> 00:00:51,920
 if you've been joining before,

18
00:00:51,920 --> 00:00:54,400
 then hopefully it will be a smooth transition.

19
00:00:54,400 --> 00:00:56,520
 So, looking forward to it.

20
00:00:56,520 --> 00:00:57,920
 I'll see you all later.

