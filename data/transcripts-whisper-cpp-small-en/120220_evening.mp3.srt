1
00:00:00,000 --> 00:00:10,210
 So, in order to make this a little more formal, the book,

2
00:00:10,210 --> 00:00:13,000
 this book is a part of the tipitaka.

3
00:00:13,000 --> 00:00:20,190
 The tipitaka is what you see behind me here, in the middle,

4
00:00:20,190 --> 00:00:27,000
 and on the left side, different versions.

5
00:00:27,000 --> 00:00:30,000
 The tipitaka and the commentaries and some commentaries.

6
00:00:30,000 --> 00:00:33,000
 What that means is the teaching of the Buddha.

7
00:00:33,000 --> 00:00:37,000
 In brief, this is the teaching of the Buddha.

8
00:00:37,000 --> 00:00:40,160
 So it may seem odd that in a meditation course I should

9
00:00:40,160 --> 00:00:44,530
 pull out a book because it seems like maybe we're going to

10
00:00:44,530 --> 00:00:45,000
 study.

11
00:00:45,000 --> 00:00:50,700
 If you study, then you have to think. Thinking can get in

12
00:00:50,700 --> 00:00:53,000
 the way of meditating.

13
00:00:53,000 --> 00:00:55,940
 But what you'll see, I'm going to try to go through

14
00:00:55,940 --> 00:00:58,000
 actually here. I think this is a good idea.

15
00:00:58,000 --> 00:01:01,000
 We picked up the Anguttra Nikaya.

16
00:01:01,000 --> 00:01:07,090
 Anguttra Nikaya is a book of teachings about lists of

17
00:01:07,090 --> 00:01:08,000
 things.

18
00:01:08,000 --> 00:01:12,000
 And so there's not much thinking involved.

19
00:01:12,000 --> 00:01:16,760
 And what you should see is that actually for the most part

20
00:01:16,760 --> 00:01:21,000
 it's direct meditation instruction,

21
00:01:21,000 --> 00:01:25,000
 advice and exhortation.

22
00:01:25,000 --> 00:01:28,290
 Listening to the Dhamma is not for the purpose of giving

23
00:01:28,290 --> 00:01:30,560
 you wisdom, it's for the purpose of giving you

24
00:01:30,560 --> 00:01:33,000
 encouragement

25
00:01:33,000 --> 00:01:38,610
 and giving you the map by which you can, like a treasure

26
00:01:38,610 --> 00:01:47,000
 map, you can take it to find the goal which is wisdom.

27
00:01:47,000 --> 00:01:51,700
 Just the Buddha said, "Akata roh tathagata." The tathagatas

28
00:01:51,700 --> 00:01:54,000
 are, the Buddhas are only ones who show the way.

29
00:01:54,000 --> 00:02:00,000
 They only point the way. We have to walk ourselves.

30
00:02:00,000 --> 00:02:04,040
 So that's what this is going to be. It's a way of setting

31
00:02:04,040 --> 00:02:07,000
 us in the right direction.

32
00:02:07,000 --> 00:02:13,000
 It means setting our minds in the right direction.

33
00:02:13,000 --> 00:02:16,130
 Because you start to learn as you stay in the meditation

34
00:02:16,130 --> 00:02:19,000
 center longer because we're free from all the diversions.

35
00:02:19,000 --> 00:02:23,000
 So you start to see how the mind plays tricks on you.

36
00:02:23,000 --> 00:02:27,000
 It starts to build up defilements, attachments.

37
00:02:27,000 --> 00:02:33,000
 It starts to build up aversions and delusions.

38
00:02:33,000 --> 00:02:40,450
 Very easy to become obsessed and overwhelmed by the

39
00:02:40,450 --> 00:02:42,000
 emotions in the mind.

40
00:02:42,000 --> 00:02:47,000
 This is why I said about Mara. Mara tries to trick you.

41
00:02:47,000 --> 00:02:53,000
 Rachael had quite a bit of Mara this course.

42
00:02:53,000 --> 00:02:59,200
 Sometimes it tricked her, sometimes she managed to trick

43
00:02:59,200 --> 00:03:00,000
 you.

44
00:03:00,000 --> 00:03:03,230
 But I point it out because this is really the goal of the

45
00:03:03,230 --> 00:03:06,000
 practice. This is great to hear.

46
00:03:06,000 --> 00:03:11,230
 It's a great thing for us to hear. I'm not worried about

47
00:03:11,230 --> 00:03:12,000
 you.

48
00:03:12,000 --> 00:03:15,000
 Because we hear the fight that the Buddha made.

49
00:03:15,000 --> 00:03:19,150
 If you ever see those movies where the Buddha fought Mara,

50
00:03:19,150 --> 00:03:22,000
 now you understand kind of what it means.

51
00:03:22,000 --> 00:03:26,000
 Fight Mara, fight Mara.

52
00:03:26,000 --> 00:03:32,000
 These things I have inside.

53
00:03:32,000 --> 00:03:34,350
 So we'll go through here. I'm just going to pick out the

54
00:03:34,350 --> 00:03:38,000
 things that I think are most useful, most pertinent.

55
00:03:38,000 --> 00:03:42,500
 This is the list of ones. So these lists are short. Each of

56
00:03:42,500 --> 00:03:44,000
 them has one entry.

57
00:03:44,000 --> 00:03:47,000
 Easy to remember.

58
00:03:47,000 --> 00:03:52,870
 So here the Buddha says, "Nahang bhikkave annyang ikadam m

59
00:03:52,870 --> 00:03:54,000
ampi."

60
00:03:54,000 --> 00:03:59,610
 "O bhikkus," the Buddha uses the word bhikkhu because bhikk

61
00:03:59,610 --> 00:04:04,000
hu means a monk, or a beggar even.

62
00:04:04,000 --> 00:04:11,000
 But it's the word the Buddha used to refer to the monk.

63
00:04:11,000 --> 00:04:16,930
 And that's because in the audience there would be monks and

64
00:04:16,930 --> 00:04:20,000
 then there would be lay people.

65
00:04:20,000 --> 00:04:24,320
 But he put the monks at the top. What he means is all of

66
00:04:24,320 --> 00:04:28,000
 you. Monks and everyone else.

67
00:04:28,000 --> 00:04:33,960
 But it also has some connotation in regards to the meaning

68
00:04:33,960 --> 00:04:36,000
 of the word bhikkhu.

69
00:04:36,000 --> 00:04:40,830
 That he wasn't always just talking about monks. Because bh

70
00:04:40,830 --> 00:04:44,000
ikkhu can mean many different things.

71
00:04:44,000 --> 00:04:47,350
 A monk is someone who stays alone. So the word monk in

72
00:04:47,350 --> 00:04:49,000
 English doesn't have to mean someone who has robes.

73
00:04:49,000 --> 00:04:52,520
 It could just mean someone who stays by themselves. A monk

74
00:04:52,520 --> 00:04:59,000
 comes from Monon. Just stay alone.

75
00:04:59,000 --> 00:05:04,000
 The same bhikkhu can mean a beggar, one who begs for food.

76
00:05:04,000 --> 00:05:09,030
 Same as the English word beg, it's bhikkhu. It's the same

77
00:05:09,030 --> 00:05:11,000
 language.

78
00:05:11,000 --> 00:05:15,000
 But it can also mean bhayang ikaditi. That's what they say.

79
00:05:15,000 --> 00:05:19,540
 They say bhayang bhikkhu sees the fierceness. You see how

80
00:05:19,540 --> 00:05:21,000
 fearsome the mind can be.

81
00:05:21,000 --> 00:05:27,000
 This is important. He's going to talk about the mind.

82
00:05:27,000 --> 00:05:32,000
 He says, do you all see how fearsome the mind can be?

83
00:05:32,000 --> 00:05:35,550
 You who have the potential to see how fearsome the mind can

84
00:05:35,550 --> 00:05:36,000
 be.

85
00:05:36,000 --> 00:05:41,000
 He says, I tell you nahang bhikkhve annyang ekadamam.

86
00:05:41,000 --> 00:05:47,000
 Samanupasa ami. I don't see any single dhamma.

87
00:05:47,000 --> 00:05:52,990
 Single thing. Dhamma here just means something that really

88
00:05:52,990 --> 00:05:54,000
 exists.

89
00:05:54,000 --> 00:06:01,000
 It's a part of reality.

90
00:06:01,000 --> 00:06:10,530
 I don't see any one thing, yang iywang abhavitang akamaniy

91
00:06:10,530 --> 00:06:11,000
ang hoti.

92
00:06:11,000 --> 00:06:20,000
 That is, when you don't train it, is useless.

93
00:06:20,000 --> 00:06:26,460
 Akamaniyang means, can't be used for anything. You can't do

94
00:06:26,460 --> 00:06:29,000
 anything with it.

95
00:06:29,000 --> 00:06:34,290
 When untrained, I don't know of anything that when unt

96
00:06:34,290 --> 00:06:40,000
rained is as useless as the untrained mind,

97
00:06:40,000 --> 00:06:46,000
 or undeveloped, abhavitam, it's undeveloped.

98
00:06:46,000 --> 00:06:49,960
 So we think if you don't develop your body, or if you don't

99
00:06:49,960 --> 00:06:51,000
 develop your reflexes,

100
00:06:51,000 --> 00:06:56,000
 or train yourself in this way or that way, it's useless.

101
00:06:56,000 --> 00:07:05,000
 But the Buddha said, yatitang bhikkhve yat.

102
00:07:05,000 --> 00:07:14,000
 Yatagidang bhikkhve jitang, jitang is the mind.

103
00:07:14,000 --> 00:07:26,000
 The mind that is undeveloped is useless.

104
00:07:26,000 --> 00:07:31,880
 So this is our first list. How many things in it? One. The

105
00:07:31,880 --> 00:07:34,000
 mind.

106
00:07:34,000 --> 00:07:37,320
 Then the Buddha goes on to say, I don't know of any one

107
00:07:37,320 --> 00:07:38,000
 thing.

108
00:07:38,000 --> 00:07:45,810
 This is the curious thing. Next list. I don't know any one

109
00:07:45,810 --> 00:07:55,000
 thing that when developed is so useful as the mind.

110
00:07:55,000 --> 00:08:10,870
 The developed mind, I say, is useful. The things that are

111
00:08:10,870 --> 00:08:15,000
 useful, the developed mind is useful.

112
00:08:15,000 --> 00:08:22,860
 So this is a very simple teaching. But it's the basis of

113
00:08:22,860 --> 00:08:26,000
 our training here.

114
00:08:26,000 --> 00:08:30,690
 The mind is that which knows an object, that part of our

115
00:08:30,690 --> 00:08:33,000
 experience right here and now that is aware,

116
00:08:33,000 --> 00:08:39,000
 aware of the sound of my voice. It's what lets you see,

117
00:08:39,000 --> 00:08:41,000
 what lets you hear.

118
00:08:41,000 --> 00:08:45,670
 Without the mind you'd be like a dead person. A dead person

119
00:08:45,670 --> 00:08:49,000
 has eyes and ears and nose and tongue and body.

120
00:08:49,000 --> 00:08:56,120
 But they don't see, hear, smell, taste, feel because they

121
00:08:56,120 --> 00:08:58,000
 have no mind.

122
00:08:58,000 --> 00:09:04,350
 But even people who have a mind, who have an undeveloped

123
00:09:04,350 --> 00:09:05,000
 mind.

124
00:09:05,000 --> 00:09:10,860
 What's it like when your mind is undeveloped? When you live

125
00:09:10,860 --> 00:09:12,000
 at home?

126
00:09:12,000 --> 00:09:17,580
 When you work in the world? When you deal with other people

127
00:09:17,580 --> 00:09:18,000
?

128
00:09:18,000 --> 00:09:25,000
 When you interact with society?

129
00:09:25,000 --> 00:09:28,120
 We spend a lot of our lives training ourselves, training

130
00:09:28,120 --> 00:09:29,000
 our minds.

131
00:09:29,000 --> 00:09:36,000
 Fasting our minds into shape, you might say. Forcing our

132
00:09:36,000 --> 00:09:36,000
 minds to be in this shape or that shape.

133
00:09:36,000 --> 00:09:40,000
 So we've really trained our minds even before we come here.

134
00:09:40,000 --> 00:09:53,000
 We've trained our minds to fit into our idea of who we are.

135
00:09:53,000 --> 00:09:57,070
 You know how sometimes when you look in the mirror after a

136
00:09:57,070 --> 00:10:00,000
 long time you don't recognize yourself?

137
00:10:00,000 --> 00:10:03,630
 Because you don't realize who you really are, you don't

138
00:10:03,630 --> 00:10:06,000
 realize what you really look like.

139
00:10:06,000 --> 00:10:11,000
 And after a while you'd say, "Is that really me?"

140
00:10:11,000 --> 00:10:14,640
 So what we do with our mind is we build it up into this

141
00:10:14,640 --> 00:10:16,000
 idea of who we are.

142
00:10:16,000 --> 00:10:18,590
 We say, "I'm this person." And everyone else is looking at

143
00:10:18,590 --> 00:10:22,000
 us like, "Wow, that person's all stressed out and miserable

144
00:10:22,000 --> 00:10:22,000
."

145
00:10:22,000 --> 00:10:26,070
 And we think, "Oh, everyone else thinks I'm a wonderful

146
00:10:26,070 --> 00:10:27,000
 person."

147
00:10:27,000 --> 00:10:32,000
 And maybe we can trick some people as well.

148
00:10:32,000 --> 00:10:39,350
 But we train our mind by clinging, by forcing, by trying to

149
00:10:39,350 --> 00:10:44,000
 make our minds fit into the idea of who we should be.

150
00:10:44,000 --> 00:10:52,990
 "I'm not that kind of person, I don't accept this part of

151
00:10:52,990 --> 00:10:55,000
 myself."

152
00:10:55,000 --> 00:10:57,650
 But then when we come here it's like we're looking in the

153
00:10:57,650 --> 00:11:01,500
 mirror, we open it up and we say, "Oh, that's who I really

154
00:11:01,500 --> 00:11:02,000
 am.

155
00:11:02,000 --> 00:11:05,900
 I'm down underneath this." And you realize you're just

156
00:11:05,900 --> 00:11:07,000
 pretending.

157
00:11:07,000 --> 00:11:11,230
 You realize that all the work you've done to train yourself

158
00:11:11,230 --> 00:11:14,000
 hasn't really brought you anywhere.

159
00:11:14,000 --> 00:11:19,410
 You haven't met with much success. It's just a facade, it's

160
00:11:19,410 --> 00:11:23,000
 just what you show to other people.

161
00:11:23,000 --> 00:11:25,350
 So what the Buddha means by training here isn't the

162
00:11:25,350 --> 00:11:31,880
 training that we do in the world, it's just forcing the

163
00:11:31,880 --> 00:11:36,000
 mind to be this way or that way.

164
00:11:36,000 --> 00:11:40,000
 But even in that sense you can see how it becomes useful.

165
00:11:40,000 --> 00:11:46,530
 If you're working in business, it's really admirable

166
00:11:46,530 --> 00:11:51,000
 sometimes to see in business people how clever they can be,

167
00:11:51,000 --> 00:11:55,480
 how efficient they can be, how they've trained their minds

168
00:11:55,480 --> 00:11:57,000
 in a certain way.

169
00:11:57,000 --> 00:12:03,000
 Sometimes they can be very, very brutally efficient.

170
00:12:03,000 --> 00:12:06,380
 At a point where they cause great suffering for other

171
00:12:06,380 --> 00:12:07,000
 beings.

172
00:12:07,000 --> 00:12:10,670
 But they have great efficiency because their mind works

173
00:12:10,670 --> 00:12:12,000
 like a computer.

174
00:12:12,000 --> 00:12:16,000
 People who use computers as well, computer programmers,

175
00:12:16,000 --> 00:12:20,990
 when you learn to program the computer, it's amazing what

176
00:12:20,990 --> 00:12:23,000
 you can do.

177
00:12:23,000 --> 00:12:27,000
 The mind is incredible, even without meditation.

178
00:12:27,000 --> 00:12:31,660
 You can train your mind. In Burma there are monks who can

179
00:12:31,660 --> 00:12:35,000
 remember the whole of the tipitaka.

180
00:12:35,000 --> 00:12:39,000
 They can remember it in their mind.

181
00:12:39,000 --> 00:12:43,530
 I tried learning just very bits and pieces. I started to

182
00:12:43,530 --> 00:12:45,000
 learn one suvta.

183
00:12:45,000 --> 00:12:47,000
 It's amazing as you go through it.

184
00:12:47,000 --> 00:12:50,650
 This one monk, he looked at me and he said, "Where do you

185
00:12:50,650 --> 00:12:56,000
 keep it? Where do you keep all that?"

186
00:12:56,000 --> 00:13:00,230
 He was actually a meditation teacher. He wasn't just a

187
00:13:00,230 --> 00:13:02,000
 simple monk.

188
00:13:02,000 --> 00:13:06,000
 But he found it very interesting that the mind can keep it

189
00:13:06,000 --> 00:13:07,000
 because the mind isn't keeping it.

190
00:13:07,000 --> 00:13:10,000
 There's no room in the brain for all that.

191
00:13:10,000 --> 00:13:15,000
 But the mind has the power to make connections.

192
00:13:15,000 --> 00:13:19,100
 Maybe there is room in the brain, but the brain has to

193
00:13:19,100 --> 00:13:21,000
 store it in a certain way.

194
00:13:21,000 --> 00:13:26,600
 The mind changes the layout of the brain in such a way that

195
00:13:26,600 --> 00:13:30,000
 it can remember these things.

196
00:13:30,000 --> 00:13:34,160
 It can be of great use. It can be of use in business. It

197
00:13:34,160 --> 00:13:36,000
 can be of use in society.

198
00:13:36,000 --> 00:13:40,000
 When you learn to teach, for example, I used to give talks.

199
00:13:40,000 --> 00:13:45,270
 When I gave talks, five years ago, six years ago, I think I

200
00:13:45,270 --> 00:13:46,000
 deleted them all.

201
00:13:46,000 --> 00:13:52,280
 But I used to have them on my website. I deleted them

202
00:13:52,280 --> 00:13:55,000
 because they're quite dry.

203
00:13:55,000 --> 00:13:56,830
 And now some people, sometimes I give a talk and people say

204
00:13:56,830 --> 00:14:00,000
, "Oh, it was a good talk."

205
00:14:00,000 --> 00:14:04,000
 This is the training of the mind. It's not bragging.

206
00:14:04,000 --> 00:14:13,180
 After years and years of doing something, your mind becomes

207
00:14:13,180 --> 00:14:15,000
 trained.

208
00:14:15,000 --> 00:14:22,000
 One of the special qualities of what we're doing here is

209
00:14:22,000 --> 00:14:27,000
 that it's reaching the very root of the mind,

210
00:14:27,000 --> 00:14:30,000
 the very root of our experience.

211
00:14:30,000 --> 00:14:34,830
 What I mean is that when you train in business or when you

212
00:14:34,830 --> 00:14:39,000
 train in society or giving talks,

213
00:14:39,000 --> 00:14:46,070
 you have to use the building blocks of your experience to

214
00:14:46,070 --> 00:14:49,000
 undertake that act.

215
00:14:49,000 --> 00:14:51,610
 For instance, when you're giving a talk, there's many

216
00:14:51,610 --> 00:14:53,000
 things involved with it.

217
00:14:53,000 --> 00:14:56,000
 When you're up on stage giving a talk, it's not easy.

218
00:14:56,000 --> 00:14:59,330
 I'm giving talks in front of very large crowds because they

219
00:14:59,330 --> 00:15:00,000
 always want the Western,

220
00:15:00,000 --> 00:15:03,090
 "Oh, look, he can speak Thai," for instance. "Let him give

221
00:15:03,090 --> 00:15:04,000
 a talk."

222
00:15:04,000 --> 00:15:08,750
 I've got pictures of it in front of the whole city of Ch

223
00:15:08,750 --> 00:15:11,000
iang Mai in Thailand.

224
00:15:11,000 --> 00:15:14,970
 Not the whole city, but many, many Buddhists. It was a big

225
00:15:14,970 --> 00:15:16,000
 celebration.

226
00:15:16,000 --> 00:15:19,190
 First the two big monks gave talks and then I was the third

227
00:15:19,190 --> 00:15:23,000
 one at midnight and everyone was asleep.

228
00:15:23,000 --> 00:15:26,120
 When I got to get in there was a big throne. They put me up

229
00:15:26,120 --> 00:15:29,000
 on this Dhamma throne.

230
00:15:29,000 --> 00:15:33,000
 It wasn't a very good talk because I was nervous and I didn

231
00:15:33,000 --> 00:15:35,000
't speak Thai very well.

232
00:15:35,000 --> 00:15:38,990
 But you have to deal with it. You deal with the nervousness

233
00:15:38,990 --> 00:15:41,000
 and then you forget something

234
00:15:41,000 --> 00:15:43,080
 and you feel embarrassed and you lose your train of thought

235
00:15:43,080 --> 00:15:45,000
 or you say something

236
00:15:45,000 --> 00:15:50,730
 and it's the wrong thing or it sounds dumb and then the

237
00:15:50,730 --> 00:15:53,000
 audience starts fidgeting and you think,

238
00:15:53,000 --> 00:15:56,000
 "Oh, are they really listening to me?"

239
00:15:56,000 --> 00:16:00,590
 There's all of this going on in your mind and remembering

240
00:16:00,590 --> 00:16:02,000
 things in order.

241
00:16:02,000 --> 00:16:06,130
 And see what happens. The biggest thing is that you do get

242
00:16:06,130 --> 00:16:07,000
 distracted.

243
00:16:07,000 --> 00:16:12,560
 You get distracted by these emotions, worry and maybe you

244
00:16:12,560 --> 00:16:15,000
 get angry or frustrated.

245
00:16:15,000 --> 00:16:19,160
 And so maybe you get off track and you start spouting

246
00:16:19,160 --> 00:16:20,000
 nonsense.

247
00:16:20,000 --> 00:16:23,200
 It can be based on delusion, it can be based on your own

248
00:16:23,200 --> 00:16:25,000
 laziness, the greed in the mind.

249
00:16:25,000 --> 00:16:30,000
 Just like having fun and goofing off and laughing and so on

250
00:16:30,000 --> 00:16:30,000
.

251
00:16:30,000 --> 00:16:33,810
 And then you lose your train of thought. So the skills are

252
00:16:33,810 --> 00:16:35,000
 all there.

253
00:16:35,000 --> 00:16:40,500
 Meditation is working on those skills. It's working on the

254
00:16:40,500 --> 00:16:43,000
 basis of the mind.

255
00:16:43,000 --> 00:16:47,400
 This is why the Buddha said, "Once the mind is trained it's

256
00:16:47,400 --> 00:16:50,000
 like a pure piece of cloth."

257
00:16:50,000 --> 00:16:55,360
 Maybe you don't want a pure piece of white cloth but the

258
00:16:55,360 --> 00:16:57,000
 choice when you're using cloth

259
00:16:57,000 --> 00:17:01,190
 is either start with a pure piece or start with a dirty

260
00:17:01,190 --> 00:17:02,000
 piece.

261
00:17:02,000 --> 00:17:04,540
 If you start with a dirty piece you can't make anything out

262
00:17:04,540 --> 00:17:05,000
 of it.

263
00:17:05,000 --> 00:17:09,020
 You put it in red dye, yellow dye, blue dye, make a shirt,

264
00:17:09,020 --> 00:17:10,000
 make a vest.

265
00:17:10,000 --> 00:17:16,430
 It looks ugly no matter what. The dye doesn't take, sewing

266
00:17:16,430 --> 00:17:18,000
 it is useless.

267
00:17:18,000 --> 00:17:20,320
 But when you have a pure piece of cloth you can put it in

268
00:17:20,320 --> 00:17:21,000
 any color.

269
00:17:21,000 --> 00:17:24,420
 It doesn't matter what color. You can make a dress, you can

270
00:17:24,420 --> 00:17:25,000
 make a shirt,

271
00:17:25,000 --> 00:17:27,000
 you can make a flag, whatever you want to make.

272
00:17:27,000 --> 00:17:30,000
 You have to start with a pure piece of cloth.

273
00:17:30,000 --> 00:17:34,020
 This is why he said it is because what we're doing is at

274
00:17:34,020 --> 00:17:36,000
 the very basic level.

275
00:17:36,000 --> 00:17:38,450
 You don't have to be a monk to appreciate the benefits of

276
00:17:38,450 --> 00:17:39,000
 meditation,

277
00:17:39,000 --> 00:17:42,000
 the benefits of the Buddha's teaching.

278
00:17:42,000 --> 00:17:49,100
 At the time of the Buddha even the kings, his kings would

279
00:17:49,100 --> 00:17:51,000
 appreciate the benefit.

280
00:17:51,000 --> 00:17:55,700
 King Bimbisara for example, he changed his whole way of

281
00:17:55,700 --> 00:17:58,000
 being after he met the Buddha.

282
00:17:58,000 --> 00:18:03,020
 To the point where his son was under the influence of an

283
00:18:03,020 --> 00:18:04,000
 evil monk

284
00:18:04,000 --> 00:18:07,000
 and his son asked him to give me the kingdom.

285
00:18:07,000 --> 00:18:11,000
 No, his son was plotting to kill him.

286
00:18:11,000 --> 00:18:14,000
 He said, "Why does he want to kill me?

287
00:18:14,000 --> 00:18:17,570
 He wants to kill you because he wants your kingdom. Well,

288
00:18:17,570 --> 00:18:19,000
 give it to him."

289
00:18:19,000 --> 00:18:25,000
 He said, "Give him your kingdom."

290
00:18:25,000 --> 00:18:28,000
 Why? Because he had become a sordapada.

291
00:18:28,000 --> 00:18:32,000
 He had understood the Buddha's teaching for himself,

292
00:18:32,000 --> 00:18:35,560
 understood that nothing is worth going into. His mind was

293
00:18:35,560 --> 00:18:38,000
 trained like that.

294
00:18:38,000 --> 00:18:45,340
 He was able to deal with anything, even his own son wanting

295
00:18:45,340 --> 00:18:48,000
 to kill him.

296
00:18:48,000 --> 00:18:53,000
 This is what kamaniyang means. You can use it for anything.

297
00:18:53,000 --> 00:18:57,000
 Because life is not easy.

298
00:18:57,000 --> 00:19:00,000
 There are many difficult things we have to do.

299
00:19:00,000 --> 00:19:04,000
 Studying is a good example. I'll give you a good anecdote.

300
00:19:04,000 --> 00:19:08,000
 You don't believe me? I'm sure you all believe me. You've

301
00:19:08,000 --> 00:19:08,000
 been practicing.

302
00:19:08,000 --> 00:19:10,000
 You don't need my word on it.

303
00:19:10,000 --> 00:19:14,360
 But after I finished my first meditation course, I went

304
00:19:14,360 --> 00:19:17,000
 back and changed my whole life.

305
00:19:17,000 --> 00:19:22,750
 I gave up all my music collections, my guitar, all my

306
00:19:22,750 --> 00:19:26,000
 sports equipment.

307
00:19:26,000 --> 00:19:34,000
 I went to live in a monastery, this little monastery. I

308
00:19:34,000 --> 00:19:34,000
 went to university.

309
00:19:34,000 --> 00:19:38,000
 That year in university, I took eleven courses.

310
00:19:38,000 --> 00:19:44,150
 At the end of the year, nine A pluses, one A and an A minus

311
00:19:44,150 --> 00:19:45,000
.

312
00:19:45,000 --> 00:19:49,000
 The A minus is because the teacher didn't like Buddhism.

313
00:19:49,000 --> 00:19:53,000
 We didn't get along very well.

314
00:19:53,000 --> 00:19:57,270
 That's an example. Because every day in meditation, study

315
00:19:57,270 --> 00:19:59,000
 and meditation, I learned how to study.

316
00:19:59,000 --> 00:20:01,000
 I re-taught myself how to study.

317
00:20:01,000 --> 00:20:06,000
 Before that, I would be party, party, party, cram.

318
00:20:06,000 --> 00:20:09,000
 You can't do this. So you force it all in.

319
00:20:09,000 --> 00:20:13,000
 You say, "Learn, learn, learn." It doesn't work.

320
00:20:13,000 --> 00:20:18,000
 When you study and meditate, you learn how the mind works.

321
00:20:18,000 --> 00:20:22,000
 You start to say, "Oh, yep, my mind is full now."

322
00:20:22,000 --> 00:20:25,000
 When your mind is full, you stop studying.

323
00:20:25,000 --> 00:20:29,000
 Take a meditation way. And you know exactly.

324
00:20:29,000 --> 00:20:32,490
 You're so clear in the mind that you know exactly when it's

325
00:20:32,490 --> 00:20:34,000
 time to start again.

326
00:20:34,000 --> 00:20:36,000
 It's very easy, actually.

327
00:20:36,000 --> 00:20:40,350
 Studying becomes easy, world becomes easy, work becomes

328
00:20:40,350 --> 00:20:41,000
 easy.

329
00:20:41,000 --> 00:20:47,000
 Everything becomes easy when the mind is trained.

330
00:20:49,000 --> 00:20:53,000
 Life becomes easy.

331
00:20:53,000 --> 00:20:58,290
 In fact, if you train yourself to a great extent,

332
00:20:58,290 --> 00:21:04,000
 eventually life becomes, they say, a blessing.

333
00:21:04,000 --> 00:21:08,730
 For many people, life is a curse. Some people want to kill

334
00:21:08,730 --> 00:21:10,000
 themselves.

335
00:21:10,000 --> 00:21:14,000
 Life is a curse for them because their mind is untrained.

336
00:21:14,000 --> 00:21:19,610
 Anything comes in, "Oh, it's terrible. They can't handle it

337
00:21:19,610 --> 00:21:20,000
."

338
00:21:20,000 --> 00:21:22,000
 They get upset. They don't know how to deal with it.

339
00:21:22,000 --> 00:21:26,000
 They don't have a means of dealing with it.

340
00:21:26,000 --> 00:21:31,350
 If you train yourself in meditation, your life becomes a

341
00:21:31,350 --> 00:21:33,000
 blessing for the world.

342
00:21:33,000 --> 00:21:37,690
 If you've seen people who have been practicing for 10, 20,

343
00:21:37,690 --> 00:21:40,000
 30, 40, 50 years,

344
00:21:40,000 --> 00:21:44,000
 they become a blessing for people.

345
00:21:44,000 --> 00:21:48,180
 In Thailand, some meditation teachers here, these

346
00:21:48,180 --> 00:21:51,000
 meditation teachers who have been for 20 years teaching,

347
00:21:51,000 --> 00:21:56,000
 30 years our teacher, 60 years or something teaching,

348
00:21:56,000 --> 00:22:01,200
 and now everywhere he goes his life is just a blessing for

349
00:22:01,200 --> 00:22:03,000
 himself and for other people.

350
00:22:03,000 --> 00:22:09,000
 He's able to help a great number of people.

351
00:22:09,000 --> 00:22:12,500
 And the world becomes, they say, the world becomes your oy

352
00:22:12,500 --> 00:22:13,000
ster.

353
00:22:13,000 --> 00:22:15,000
 There's anything you can do.

354
00:22:15,000 --> 00:22:17,000
 What are we talking about?

355
00:22:17,000 --> 00:22:25,000
 We're talking about this course I'm conducting over Skype.

356
00:22:25,000 --> 00:22:27,000
 That's interesting.

357
00:22:27,000 --> 00:22:32,000
 How easy it all becomes, how easy life becomes.

358
00:22:32,000 --> 00:22:40,140
 You have this challenge, some student, and he's practicing

359
00:22:40,140 --> 00:22:41,000
 in this way.

360
00:22:41,000 --> 00:22:43,000
 "Okay, how do I deal with it?"

361
00:22:43,000 --> 00:22:45,710
 Whereas before he'd be afraid, "Well, if I say the wrong

362
00:22:45,710 --> 00:22:51,000
 thing, maybe he'll think I'm not a good teacher."

363
00:22:51,000 --> 00:23:00,760
 When you train your mind, you realize, "What if he thinks I

364
00:23:00,760 --> 00:23:01,000
'm...

365
00:23:02,000 --> 00:23:04,960
 a bad teacher? What if I give a talk tonight and you think

366
00:23:04,960 --> 00:23:07,000
 I'm a useless teacher?"

367
00:23:07,000 --> 00:23:09,000
 And then you run away and you leave.

368
00:23:09,000 --> 00:23:12,790
 I've had students, when I was in Thailand, many students

369
00:23:12,790 --> 00:23:15,000
 ran away and I was devastated.

370
00:23:15,000 --> 00:23:17,890
 One time I told all the meditators, because they said, "Doi

371
00:23:17,890 --> 00:23:20,000
 Su Tepp," you couldn't control them.

372
00:23:20,000 --> 00:23:23,930
 They tried, they would meet up and talk, and sometimes a

373
00:23:23,930 --> 00:23:25,000
 man and a woman,

374
00:23:25,000 --> 00:23:28,000
 like up on the view, like a romantic view, and you'd think,

375
00:23:28,000 --> 00:23:31,000
 "Oh no, we're going to get in trouble for this."

376
00:23:31,000 --> 00:23:34,440
 And so I would tell them, "No talking, no talking," and I

377
00:23:34,440 --> 00:23:37,000
 would see them and I would tell them to talk.

378
00:23:37,000 --> 00:23:39,000
 And they all just ran away.

379
00:23:39,000 --> 00:23:41,000
 They said, "We don't want this."

380
00:23:41,000 --> 00:23:46,560
 And I was devastated, and I said, "Oh no, all my students

381
00:23:46,560 --> 00:23:48,000
 ran away."

382
00:23:48,000 --> 00:23:51,000
 "Are you there during that time?"

383
00:23:51,000 --> 00:23:53,000
 She was at Doi Su Tepp.

384
00:23:53,000 --> 00:23:56,710
 And I was at the view, sometimes we would have a man and a

385
00:23:56,710 --> 00:24:00,000
 woman sitting under the moon.

386
00:24:00,000 --> 00:24:04,710
 And they'd think, "Oh, the average is not going to be happy

387
00:24:04,710 --> 00:24:06,000
 about this."

388
00:24:06,000 --> 00:24:09,130
 You're one of the good ones, that's why I don't remember

389
00:24:09,130 --> 00:24:10,000
 you so well.

390
00:24:10,000 --> 00:24:13,000
 But I do, I remember you in the morning.

391
00:24:13,000 --> 00:24:22,000
 I was just expecting someone else.

392
00:24:22,000 --> 00:24:24,420
 So when you train the mind, you don't suffer from these

393
00:24:24,420 --> 00:24:25,000
 things.

394
00:24:25,000 --> 00:24:29,790
 We don't suffer from the vicissitudes of life, when things

395
00:24:29,790 --> 00:24:31,000
 change.

396
00:24:31,000 --> 00:24:33,000
 And life becomes easy.

397
00:24:33,000 --> 00:24:36,420
 You deal with people and they don't like what you do, and

398
00:24:36,420 --> 00:24:39,000
 you make mistakes.

399
00:24:39,000 --> 00:24:44,000
 Even an Arahant can make mistakes, I'm sure of it.

400
00:24:44,000 --> 00:24:47,290
 But when an Arahant makes mistakes, I've seen Anjan, my

401
00:24:47,290 --> 00:24:49,000
 teacher, make mistakes.

402
00:24:49,000 --> 00:24:51,000
 And it's wonderful to see him make a mistake.

403
00:24:51,000 --> 00:24:53,000
 He doesn't care.

404
00:24:53,000 --> 00:24:59,000
 He doesn't try to hide it, but he doesn't feel bad.

405
00:24:59,000 --> 00:25:01,000
 He doesn't hate himself for it.

406
00:25:01,000 --> 00:25:11,000
 He says, "Oh, okay, it's not like that."

407
00:25:11,000 --> 00:25:12,000
 And he moves on.

408
00:25:12,000 --> 00:25:14,000
 There's no attachment.

409
00:25:14,000 --> 00:25:18,920
 He'll give a talk, and sometimes his talks are a mess,

410
00:25:18,920 --> 00:25:20,000
 really.

411
00:25:20,000 --> 00:25:22,270
 He'll give a talk, and then he'll forget where he was, and

412
00:25:22,270 --> 00:25:24,000
 he'll start talking about something else.

413
00:25:24,000 --> 00:25:28,270
 Well, he's gotten old, but he's also very tired all the

414
00:25:28,270 --> 00:25:29,000
 time.

415
00:25:29,000 --> 00:25:30,000
 From too much work.

416
00:25:30,000 --> 00:25:33,150
 So I've heard that I've got recordings of the talk, and

417
00:25:33,150 --> 00:25:34,000
 suddenly...

418
00:25:34,000 --> 00:25:36,000
 But it was always good.

419
00:25:36,000 --> 00:25:40,460
 Because he'd stop halfway through, and if you're a study

420
00:25:40,460 --> 00:25:42,000
 monk, you'd get fed up with him.

421
00:25:42,000 --> 00:25:45,000
 You'd think, "I can't even remember a list of five things.

422
00:25:45,000 --> 00:25:47,000
 What kind of a talk is this?"

423
00:25:47,000 --> 00:25:49,150
 He'd go through three, and suddenly he's talking about

424
00:25:49,150 --> 00:25:50,000
 something else.

425
00:25:50,000 --> 00:25:52,000
 But it was all Dhamma.

426
00:25:52,000 --> 00:25:55,000
 So, okay, he's at three, and then boom! It's changing.

427
00:25:55,000 --> 00:25:59,000
 This is a sign that this person is still clinging.

428
00:25:59,000 --> 00:26:01,470
 So, okay, so now he's talking about something else, and it

429
00:26:01,470 --> 00:26:03,000
 always was good.

430
00:26:03,000 --> 00:26:08,000
 In the end, it was, "Let's practice meditation together."

431
00:26:08,000 --> 00:26:12,000
 So this is what it means, that Kamaniyau, Akkamaniyau.

432
00:26:12,000 --> 00:26:15,000
 Train your mind, is what he's saying.

433
00:26:15,000 --> 00:26:22,000
 If your mind is trained, this is what is truly useful.

434
00:26:22,000 --> 00:26:26,270
 There's nothing more useful in the world than a trained

435
00:26:26,270 --> 00:26:27,000
 mind.

436
00:26:27,000 --> 00:26:30,000
 And the trained mind brings happiness.

437
00:26:30,000 --> 00:26:34,300
 So this is very brief, and part of the idea is to just show

438
00:26:34,300 --> 00:26:36,000
 you what is Buddhism.

439
00:26:36,000 --> 00:26:38,000
 These people wonder what is Buddhism.

440
00:26:38,000 --> 00:26:41,000
 Now you see, "Wow, the Buddhism, the Buddha was...

441
00:26:41,000 --> 00:26:46,000
 We're practicing, this comes from the Buddha."

442
00:26:46,000 --> 00:26:50,000
 So now we know, because some people will say,

443
00:26:50,000 --> 00:26:53,000
 "Meditation and Buddhism."

444
00:26:53,000 --> 00:26:56,090
 Some people even say, "I don't want to get into Buddhism, I

445
00:26:56,090 --> 00:26:59,000
'm just interested in meditation."

446
00:26:59,000 --> 00:27:02,200
 Some people even get to that, but they get the view, the

447
00:27:02,200 --> 00:27:03,000
 opposite view,

448
00:27:03,000 --> 00:27:05,590
 where they say they're not interested in Buddhism, they

449
00:27:05,590 --> 00:27:10,000
 just want to practice meditation.

450
00:27:10,000 --> 00:27:14,190
 And it's not that Buddhism is meditation, and meditation is

451
00:27:14,190 --> 00:27:15,000
 Buddhism,

452
00:27:15,000 --> 00:27:24,000
 but where they meet, that is the core.

453
00:27:24,000 --> 00:27:26,000
 That's what's most important.

454
00:27:26,000 --> 00:27:30,080
 What's most important is not cultural Buddhism, or

455
00:27:30,080 --> 00:27:34,000
 traditional Buddhism, or scholarly Buddhism,

456
00:27:34,000 --> 00:27:40,290
 all meditations that have nothing to do with this, with the

457
00:27:40,290 --> 00:27:44,000
 training of the mind.

458
00:27:44,000 --> 00:27:47,560
 If it's just for the purposes of feeling peaceful for a

459
00:27:47,560 --> 00:27:51,000
 while, or for seeing special things,

460
00:27:51,000 --> 00:27:56,000
 reading people's minds flying through the air.

461
00:27:56,000 --> 00:28:00,620
 And it's not true meditation, not the most important

462
00:28:00,620 --> 00:28:02,000
 meditation.

463
00:28:02,000 --> 00:28:05,000
 So now you can see the Buddha-the Buddha teach.

464
00:28:05,000 --> 00:28:08,430
 Well, we have at the very beginning of this book, Kamanyang

465
00:28:08,430 --> 00:28:10,000
-akkamanyang.

466
00:28:10,000 --> 00:28:14,000
 What is most useful, or what is most useless?

467
00:28:14,000 --> 00:28:16,000
 The trained mind is most useful.

468
00:28:16,000 --> 00:28:18,000
 The most useful thing in the world.

469
00:28:18,000 --> 00:28:21,000
 And the untrained mind is the most useless.

470
00:28:21,000 --> 00:28:26,000
 So when we practice, when our mind is in the present moment

471
00:28:26,000 --> 00:28:26,000
,

472
00:28:26,000 --> 00:28:29,600
 is seeing things clearly as they are, you can see for

473
00:28:29,600 --> 00:28:30,000
 yourself,

474
00:28:30,000 --> 00:28:32,770
 you don't need my word for it, you've all been practicing

475
00:28:32,770 --> 00:28:37,000
 enough to know this is the truth.

476
00:28:37,000 --> 00:28:39,530
 There's nothing more important than this training that we

477
00:28:39,530 --> 00:28:40,000
're doing.

478
00:28:40,000 --> 00:28:48,260
 And you can feel for yourself how awesome is the power of

479
00:28:48,260 --> 00:28:52,000
 just one moment of seeing things as they are.

480
00:28:52,000 --> 00:28:54,000
 To the point that the Buddha said,

481
00:28:54,000 --> 00:28:57,520
 "One moment seeing things as they are is better than a

482
00:28:57,520 --> 00:29:01,000
 hundred years of not seeing things as they are."

483
00:29:01,000 --> 00:29:05,300
 The person lives a hundred years but never sees things as

484
00:29:05,300 --> 00:29:07,000
 they are.

485
00:29:07,000 --> 00:29:12,730
 And better they were to only live one day and see things as

486
00:29:12,730 --> 00:29:14,000
 they are.

487
00:29:14,000 --> 00:29:18,000
 So, pep talk.

488
00:29:18,000 --> 00:29:20,000
 Now we get on to the meditation practice.

489
00:29:20,000 --> 00:29:25,000
 Start with mindful frustration and walking in that sitting.

