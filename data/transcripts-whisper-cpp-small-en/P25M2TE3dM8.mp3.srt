1
00:00:00,000 --> 00:00:03,960
 "When I sit and say to myself, 'Sitting,' I have to do a

2
00:00:03,960 --> 00:00:05,720
 quick body scan, at least

3
00:00:05,720 --> 00:00:08,800
 of my legs and spine, in order to really know that I'm

4
00:00:08,800 --> 00:00:09,960
 indeed sitting.

5
00:00:09,960 --> 00:00:13,530
 Isn't that quick body scan somewhat contrary to a one-point

6
00:00:13,530 --> 00:00:14,240
ed mind?

7
00:00:14,240 --> 00:00:17,440
 Is one label really enough?"

8
00:00:17,440 --> 00:00:19,440
 You don't really have to do that.

9
00:00:19,440 --> 00:00:23,780
 I would say it's not really the object or exactly proper

10
00:00:23,780 --> 00:00:25,080
 meditation.

11
00:00:25,080 --> 00:00:27,000
 It's much more forceful.

12
00:00:27,000 --> 00:00:30,950
 You could in the beginning and you could say that it's at

13
00:00:30,950 --> 00:00:32,920
 least calming the mind down or

14
00:00:32,920 --> 00:00:34,320
 focusing the mind.

15
00:00:34,320 --> 00:00:38,440
 Because remember, none of that's wrong in tranquility

16
00:00:38,440 --> 00:00:39,680
 meditation.

17
00:00:39,680 --> 00:00:43,670
 If your purpose is simply to calm the mind, you can force

18
00:00:43,670 --> 00:00:45,880
 it, push it, knead it, mold

19
00:00:45,880 --> 00:00:51,240
 it into shape.

20
00:00:51,240 --> 00:00:53,900
 Concentration meditation is not for the purpose of

21
00:00:53,900 --> 00:00:55,200
 cultivating insight.

22
00:00:55,200 --> 00:00:58,480
 In a sense, we can use that, at least in the beginning.

23
00:00:58,480 --> 00:01:04,470
 A meditator in Vipassana meditation might calm themselves

24
00:01:04,470 --> 00:01:06,960
 down by doing these body scans,

25
00:01:06,960 --> 00:01:08,920
 by just knowing that they're sitting.

26
00:01:08,920 --> 00:01:12,240
 In Vipassana meditation, the point is not the sitting.

27
00:01:12,240 --> 00:01:15,760
 Sitting is a description of those feelings that you have.

28
00:01:15,760 --> 00:01:18,650
 The fact that you're aware of the tension in your back or

29
00:01:18,650 --> 00:01:20,260
 the pressure on the floor,

30
00:01:20,260 --> 00:01:23,720
 at that moment when you say sitting, that's enough.

31
00:01:23,720 --> 00:01:27,010
 You're aware of that experience and you call that

32
00:01:27,010 --> 00:01:28,720
 experience sitting.

33
00:01:28,720 --> 00:01:31,770
 It's simply important to recognize this is a physical

34
00:01:31,770 --> 00:01:32,720
 phenomenon.

35
00:01:32,720 --> 00:01:35,620
 It's not so important to be aware that this is a sitting

36
00:01:35,620 --> 00:01:36,320
 posture.

37
00:01:36,320 --> 00:01:38,940
 Put your mind on the whole body, the entire body, and say

38
00:01:38,940 --> 00:01:40,360
 to yourself, "Sitting."

39
00:01:40,360 --> 00:01:49,340
 The recognition that that is a sitting posture comes on its

40
00:01:49,340 --> 00:01:54,120
 own and comes unexpectedly.

41
00:01:54,120 --> 00:01:59,160
 It's the sannya part, the recognition that this is sitting.

42
00:01:59,160 --> 00:02:01,320
 Sannya is impermanent.

43
00:02:01,320 --> 00:02:05,190
 What you're seeing that leads you to say that you have to

44
00:02:05,190 --> 00:02:07,020
 do this in order to push it in

45
00:02:07,020 --> 00:02:10,480
 order for it to come is that it's non-self.

46
00:02:10,480 --> 00:02:11,560
 Sometimes it does.

47
00:02:11,560 --> 00:02:13,160
 Sometimes you immediately know that you're sitting.

48
00:02:13,160 --> 00:02:16,000
 You recognize, "Oh, this is a sitting posture."

49
00:02:16,000 --> 00:02:18,550
 Sometimes you look and look and there's no "ping, this is

50
00:02:18,550 --> 00:02:19,240
 sitting."

51
00:02:19,240 --> 00:02:21,520
 You just never are aware that it's sitting.

52
00:02:21,520 --> 00:02:23,400
 This is because it's not self.

53
00:02:23,400 --> 00:02:27,240
 You can't force yourself to know that's what you're seeing.

54
00:02:27,240 --> 00:02:30,920
 That is the whole purpose of practicing.

55
00:02:30,920 --> 00:02:31,920
 That's what you're going to find.

56
00:02:31,920 --> 00:02:34,830
 You're going to find that sometimes you are aware of the

57
00:02:34,830 --> 00:02:35,600
 feelings.

58
00:02:35,600 --> 00:02:38,320
 Sometimes you're aware of this feeling.

59
00:02:38,320 --> 00:02:39,320
 Sometimes you're aware of that feeling.

60
00:02:39,320 --> 00:02:41,520
 Sometimes you're aware of the recognition of the feeling or

61
00:02:41,520 --> 00:02:42,880
 putting it together as sitting.

62
00:02:42,880 --> 00:02:44,560
 Sometimes you're not.

63
00:02:44,560 --> 00:02:48,120
 That's the point of the practice, not to perfect it.

64
00:02:48,120 --> 00:02:49,120
 This is key.

65
00:02:49,120 --> 00:02:52,000
 I did a video on this that I think is most important, one

66
00:02:52,000 --> 00:02:53,760
 that we should emphasize again

67
00:02:53,760 --> 00:02:58,570
 and again that often meditators very often come to the

68
00:02:58,570 --> 00:03:01,960
 conclusion that they're practicing

69
00:03:01,960 --> 00:03:06,110
 incorrectly or they're unable to practice specifically

70
00:03:06,110 --> 00:03:08,280
 because they're seeing what we

71
00:03:08,280 --> 00:03:09,400
 want them to see.

72
00:03:09,400 --> 00:03:10,400
 They're seeing impermanence.

73
00:03:10,400 --> 00:03:11,400
 They're seeing suffering.

74
00:03:11,400 --> 00:03:13,720
 They're seeing non-self.

75
00:03:13,720 --> 00:03:18,560
 I would say much more important is the practice than

76
00:03:18,560 --> 00:03:20,320
 perfecting it.

77
00:03:20,320 --> 00:03:22,940
 Perfecting it's not going to happen, not in inside

78
00:03:22,940 --> 00:03:23,840
 meditation.

