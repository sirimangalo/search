1
00:00:00,000 --> 00:00:12,000
 Okay, good evening everyone.

2
00:00:12,000 --> 00:00:22,080
 Welcome to our evening Dhamma session.

3
00:00:22,080 --> 00:00:27,780
 So on Sunday we had a big celebration of the Buddha's

4
00:00:27,780 --> 00:00:31,080
 birthday in Mississauga.

5
00:00:31,080 --> 00:00:35,940
 And it's quite impressive every year all the different

6
00:00:35,940 --> 00:00:40,080
 Buddhist communities come out and

7
00:00:40,080 --> 00:00:49,080
 put on costumes, traditional costumes and

8
00:00:49,080 --> 00:01:00,080
 have a parade and chanting and cultural displays, tents,

9
00:01:00,080 --> 00:01:06,600
 tents displaying and promoting all the many different

10
00:01:06,600 --> 00:01:12,080
 Buddhist organizations around.

11
00:01:12,080 --> 00:01:19,630
 And so last year I submitted a cultural display or an

12
00:01:19,630 --> 00:01:23,080
 exhibition, part of the show.

13
00:01:23,080 --> 00:01:28,080
 And I just put down meditation.

14
00:01:28,080 --> 00:01:32,550
 Because it seemed to me that, as I said when I got up on

15
00:01:32,550 --> 00:01:34,080
 stage, I forgot about it.

16
00:01:34,080 --> 00:01:38,230
 When I finally saw the program, I saw that I was in the

17
00:01:38,230 --> 00:01:42,080
 program, I thought there'd been a mistake.

18
00:01:42,080 --> 00:01:49,080
 So after all these dancing groups, and here I was in line,

19
00:01:49,080 --> 00:01:54,400
 got up on stage, sat there in front of, oh, maybe a

20
00:01:54,400 --> 00:01:56,080
 thousand, couple thousand people.

21
00:01:56,080 --> 00:02:02,080
 Maybe a thousand, probably not more, maybe less.

22
00:02:02,080 --> 00:02:09,080
 And I just taught a little meditation.

23
00:02:09,080 --> 00:02:15,370
 What I said was, you know, when the Buddha passed away,

24
00:02:15,370 --> 00:02:21,720
 before he passed away, all the angels from all over the

25
00:02:21,720 --> 00:02:28,170
 universe came down and brought with them flowers from

26
00:02:28,170 --> 00:02:29,080
 heaven.

27
00:02:29,080 --> 00:02:33,740
 You know, heavenly flowers are not like flowers from the

28
00:02:33,740 --> 00:02:35,080
 human world.

29
00:02:35,080 --> 00:02:37,640
 Their beauty is, of course, beyond anything that we have

30
00:02:37,640 --> 00:02:38,080
 here.

31
00:02:38,080 --> 00:02:42,980
 And they scattered them throughout the forest. This is what

32
00:02:42,980 --> 00:02:46,080
 the legends say.

33
00:02:46,080 --> 00:02:51,560
 And the Buddha saw this and he thought to himself, this isn

34
00:02:51,560 --> 00:02:56,770
't, well he said to Ananda, this isn't how you honor someone

35
00:02:56,770 --> 00:02:57,080
.

36
00:02:57,080 --> 00:03:01,080
 This is now how you honor a Buddha.

37
00:03:01,080 --> 00:03:05,350
 And the commenter, there's this one commenter that says,

38
00:03:05,350 --> 00:03:09,080
 what the Buddha was thinking was,

39
00:03:09,080 --> 00:03:15,350
 "Ya bahi ima jata soo parisa" for as long as my fourfold

40
00:03:15,350 --> 00:03:24,520
 followers, so monks and nuns and laymen and laywomen, "mam

41
00:03:24,520 --> 00:03:28,080
imaya pati pati bhujaya bhuji santi"

42
00:03:28,080 --> 00:03:35,190
 as long as they pay homage to me through the homage of

43
00:03:35,190 --> 00:03:37,080
 practice.

44
00:03:37,080 --> 00:03:43,080
 "Pati pati bhujaya"

45
00:03:43,080 --> 00:03:50,250
 "Tawamamasa asanangna patmati bhuna chandho yiwa wirho ji

46
00:03:50,250 --> 00:03:51,080
 sati"

47
00:03:51,080 --> 00:04:01,360
 "Wirho ji sati will shine, will shine light on this world,

48
00:04:01,360 --> 00:04:07,880
 will continue to shine just like the full moon in the

49
00:04:07,880 --> 00:04:16,080
 middle of the sky shines on the earth."

50
00:04:16,080 --> 00:04:24,890
 So when we talk about paying homage to the Buddha, which is

51
00:04:24,890 --> 00:04:31,080
 something we talk about and something we think about,

52
00:04:31,080 --> 00:04:36,200
 there's many reasons for that. One is simply because of

53
00:04:36,200 --> 00:04:41,080
 what a beautiful and pure person he was.

54
00:04:41,080 --> 00:04:44,790
 If you have a chance to read his teachings or even read

55
00:04:44,790 --> 00:04:50,290
 about his life, it's quite remarkable, the depth of his ren

56
00:04:50,290 --> 00:04:58,080
unciation, his compassion, his dedication to truth,

57
00:04:58,080 --> 00:05:07,080
 his dedication to peace, to bringing peace to the earth.

58
00:05:07,080 --> 00:05:12,590
 But mostly we pay respect because through our practice we

59
00:05:12,590 --> 00:05:21,300
've come to appreciate what he's given to us, what he's done

60
00:05:21,300 --> 00:05:23,080
 for us.

61
00:05:23,080 --> 00:05:29,470
 And by paying homage to him, it's such an affirmation of

62
00:05:29,470 --> 00:05:36,660
 what a good thing we've found, this expression of gratitude

63
00:05:36,660 --> 00:05:37,080
.

64
00:05:37,080 --> 00:05:41,160
 And so of course, as with any famous person, there's always

65
00:05:41,160 --> 00:05:44,080
 many, many people who come and pay homage to him.

66
00:05:44,080 --> 00:05:49,670
 And the Buddha was concerned that before he passed away

67
00:05:49,670 --> 00:05:53,400
 people should be clear that it wasn't through flowers or

68
00:05:53,400 --> 00:06:00,480
 candles or incense or bowing down or doing chanting or any

69
00:06:00,480 --> 00:06:02,080
 of this,

70
00:06:02,080 --> 00:06:05,080
 that one truly pays homage to him.

71
00:06:05,080 --> 00:06:11,310
 He said through practicing the Dhamma, whatever person, it

72
00:06:11,310 --> 00:06:16,590
 doesn't matter if they're a monk or a nun or an ordinary

73
00:06:16,590 --> 00:06:19,080
 man, ordinary woman,

74
00:06:19,080 --> 00:06:23,390
 whoever practices the Dhamma in line with the Dhamma, to

75
00:06:23,390 --> 00:06:30,200
 realize the Dhamma, such a person pays the proper respect

76
00:06:30,200 --> 00:06:34,080
 and homage to the Buddha.

77
00:06:34,080 --> 00:06:39,790
 There was a story of this monk who liked to look at the

78
00:06:39,790 --> 00:06:41,080
 Buddha.

79
00:06:41,080 --> 00:06:45,480
 He liked to follow the Buddha around and was just in great

80
00:06:45,480 --> 00:06:50,080
 bliss because the Buddha was quite beautiful apparently.

81
00:06:50,080 --> 00:06:56,520
 So this monk, Vakali, he liked to walk around after the

82
00:06:56,520 --> 00:06:59,080
 Buddha and wherever the Buddha was he would just sit there

83
00:06:59,080 --> 00:07:01,080
 and stare at the Buddha.

84
00:07:01,080 --> 00:07:07,210
 He was kind of infatuated or well, he was very happy to see

85
00:07:07,210 --> 00:07:11,080
 the Buddha and gave him great bliss.

86
00:07:11,080 --> 00:07:13,950
 The Buddha saw, well, this isn't the way. He's not going to

87
00:07:13,950 --> 00:07:15,080
 become enlightened this way.

88
00:07:15,080 --> 00:07:19,080
 And so the Buddha said to him famous words.

89
00:07:19,080 --> 00:07:23,260
 He said, "Yoko Vakali Dhammang Pasati, Soumang Pasati" or

90
00:07:23,260 --> 00:07:25,080
 something like that.

91
00:07:25,080 --> 00:07:32,570
 I can't remember the exact following. "Whoever sees the D

92
00:07:32,570 --> 00:07:33,080
hamma sees me."

93
00:07:33,080 --> 00:07:38,080
 That's basically what he said.

94
00:07:38,080 --> 00:07:43,080
 So it always comes back to the Dhamma or the Dharma.

95
00:07:43,080 --> 00:07:47,310
 And so this is what I talked about on Sunday. I said, I'd

96
00:07:47,310 --> 00:07:50,080
 like to share with you a little bit of Dharma,

97
00:07:50,080 --> 00:07:55,310
 five dharmas for us to reflect upon. And I think this is a

98
00:07:55,310 --> 00:07:58,080
 really useful thing for all of us to hear.

99
00:07:58,080 --> 00:08:01,250
 And those of you who are coming to the end of your course,

100
00:08:01,250 --> 00:08:05,080
 as well as those of you who have maybe never practiced

101
00:08:05,080 --> 00:08:06,080
 meditation.

102
00:08:06,080 --> 00:08:10,080
 Remember what it is we're practicing. What is the Dharma?

103
00:08:10,080 --> 00:08:14,080
 The Dharma is reality.

104
00:08:14,080 --> 00:08:19,080
 Dharma in this sense means something that really exists.

105
00:08:19,080 --> 00:08:24,920
 Dharma means to hold. So it holds an existence, unlike

106
00:08:24,920 --> 00:08:29,080
 imagination, which doesn't actually hold anything.

107
00:08:29,080 --> 00:08:35,370
 When you think about a dragon or you think about the Easter

108
00:08:35,370 --> 00:08:36,080
 Bunny,

109
00:08:36,080 --> 00:08:44,680
 they don't hold any reality. They don't hold up in reality.

110
00:08:44,680 --> 00:08:45,080
 Dharam is the word.

111
00:08:45,080 --> 00:08:49,080
 Whereas a Dharma is truly real.

112
00:08:49,080 --> 00:08:53,610
 Real in what sense? Well, real in the sense that you can

113
00:08:53,610 --> 00:08:55,080
 experience it.

114
00:08:55,080 --> 00:08:58,290
 What are the dharmas, these five dharmas? They're adapted

115
00:08:58,290 --> 00:09:00,080
 from the four satipatanas.

116
00:09:00,080 --> 00:09:04,080
 But let's just talk about them as the five dharmas.

117
00:09:04,080 --> 00:09:09,450
 The first one is the body. So you can try this. Close your

118
00:09:09,450 --> 00:09:10,080
 eyes.

119
00:09:10,080 --> 00:09:19,080
 And reflect on your body. There is physical reality.

120
00:09:19,080 --> 00:09:22,790
 There is the hardness and the softness of the seat that you

121
00:09:22,790 --> 00:09:26,080
're sitting on. That's a feeling.

122
00:09:26,080 --> 00:09:29,080
 There's the heat and the cold in the room around you.

123
00:09:29,080 --> 00:09:35,310
 Tension in your back and in your legs. Maybe tension in

124
00:09:35,310 --> 00:09:37,080
 your head.

125
00:09:37,080 --> 00:09:41,080
 This is all physical.

126
00:09:41,080 --> 00:09:44,210
 So the Buddha taught us to be mindful if you're sitting, to

127
00:09:44,210 --> 00:09:45,080
 say to yourself,

128
00:09:45,080 --> 00:09:50,140
 "Sitting, sitting, standing, standing, standing, walking,

129
00:09:50,140 --> 00:09:53,080
 lying, lying."

130
00:09:53,080 --> 00:10:00,080
 Or whoever your body is deported.

131
00:10:00,080 --> 00:10:06,980
 And we do this, we practice meditation in order to stay

132
00:10:06,980 --> 00:10:10,080
 close to reality.

133
00:10:10,080 --> 00:10:15,080
 As we do that, our minds are going to stay with the body.

134
00:10:15,080 --> 00:10:19,080
 And you're going to learn about reality.

135
00:10:19,080 --> 00:10:23,080
 It sounds kind of banal, I think. If you've never practiced

136
00:10:23,080 --> 00:10:23,080
 meditation,

137
00:10:23,080 --> 00:10:26,080
 you don't realize the significance of doing that.

138
00:10:26,080 --> 00:10:28,080
 First of all, how challenging it is.

139
00:10:28,080 --> 00:10:33,080
 But second of all, how important that challenge is.

140
00:10:33,080 --> 00:10:37,080
 How much you learn about yourself when you do just that,

141
00:10:37,080 --> 00:10:41,080
 when you look at the body.

142
00:10:41,080 --> 00:10:43,610
 Because with the body you see all these other realities,

143
00:10:43,610 --> 00:10:44,080
 right?

144
00:10:44,080 --> 00:10:48,420
 The second reality is the feelings. So you start to see

145
00:10:48,420 --> 00:10:52,080
 pain, and you'll see happiness, so you'll see calm.

146
00:10:52,080 --> 00:10:57,080
 Some people feel very calm when they meditate.

147
00:10:57,080 --> 00:11:01,040
 Some people feel a lot of pain when they meditate. You'll

148
00:11:01,040 --> 00:11:03,080
 see all this.

149
00:11:03,080 --> 00:11:07,720
 And pain like the body or feelings, happiness like the body

150
00:11:07,720 --> 00:11:08,080
.

151
00:11:08,080 --> 00:11:13,080
 All these feelings, they teach you about yourself.

152
00:11:13,080 --> 00:11:17,280
 You get to see how you react to feelings. You get to see

153
00:11:17,280 --> 00:11:23,080
 how you interact with feelings.

154
00:11:23,080 --> 00:11:27,800
 When you focus on the pain and say pain, pain, you're

155
00:11:27,800 --> 00:11:29,080
 learning about feelings,

156
00:11:29,080 --> 00:11:33,280
 and therefore you're learning about yourself and how you

157
00:11:33,280 --> 00:11:36,080
 relate to the feeling.

158
00:11:36,080 --> 00:11:51,080
 The third reality is the mind.

159
00:11:51,080 --> 00:11:54,750
 As you focus on the body and the feelings, you're also

160
00:11:54,750 --> 00:11:57,080
 going to notice that you're thinking.

161
00:11:57,080 --> 00:12:00,000
 You'll notice your thoughts more clearly, thoughts about

162
00:12:00,000 --> 00:12:01,080
 the past or future,

163
00:12:01,080 --> 00:12:06,080
 or thoughts about the present or thoughts of imagination.

164
00:12:06,080 --> 00:12:10,080
 You'll see them clearly.

165
00:12:10,080 --> 00:12:13,510
 You'll be able to watch them, you'll be able to learn about

166
00:12:13,510 --> 00:12:14,080
 them.

167
00:12:14,080 --> 00:12:20,020
 What do we think about? How often do we think? How long do

168
00:12:20,020 --> 00:12:21,080
 we think?

169
00:12:21,080 --> 00:12:25,670
 How obsessed do we get in thoughts? How we react to our

170
00:12:25,670 --> 00:12:29,080
 thoughts, how we react to our memories,

171
00:12:29,080 --> 00:12:32,860
 all the memories that come up when you meditate, how we

172
00:12:32,860 --> 00:12:35,080
 react to them.

173
00:12:35,080 --> 00:12:41,200
 You'll start to see this. You'll see relationships between

174
00:12:41,200 --> 00:12:43,080
 the dharmas.

175
00:12:43,080 --> 00:12:50,810
 The fourth reality is our reactions, our emotions, not just

176
00:12:50,810 --> 00:12:53,080
 our reactions, but our mind state.

177
00:12:53,080 --> 00:12:58,080
 Liking and disliking is our reactions.

178
00:12:58,080 --> 00:13:08,080
 Abortum, sadness, fear, depression, frustration.

179
00:13:08,080 --> 00:13:16,270
 On the other side, desire, wishfulness, loneliness, pining

180
00:13:16,270 --> 00:13:19,080
 away over things.

181
00:13:19,080 --> 00:13:23,810
 Then there's drowsiness and distraction and doubt and

182
00:13:23,810 --> 00:13:27,080
 confusion and worry and all those.

183
00:13:27,080 --> 00:13:32,080
 These are all real. These are very important.

184
00:13:32,080 --> 00:13:35,950
 The most remarkable thing about the meditation is how it

185
00:13:35,950 --> 00:13:38,080
 works to free you from these,

186
00:13:38,080 --> 00:13:41,790
 how it works to sort out your emotions, sort out your mind

187
00:13:41,790 --> 00:13:43,080
 states,

188
00:13:43,080 --> 00:13:48,120
 but if you're worried and if you get good and learn the ins

189
00:13:48,120 --> 00:13:50,080
 and outs of your mind,

190
00:13:50,080 --> 00:13:53,750
 just saying to yourself, "Worried, worried," and thinking

191
00:13:53,750 --> 00:13:55,080
 about,

192
00:13:55,080 --> 00:13:57,510
 "Knowledging the thinking about whatever is making you

193
00:13:57,510 --> 00:13:59,080
 worried," and so on.

194
00:13:59,080 --> 00:14:04,570
 You work it out. You're able to free yourself from the

195
00:14:04,570 --> 00:14:06,080
 worry, from the stress,

196
00:14:06,080 --> 00:14:16,300
 from boredom, sadness, fear, anxiety, depression, even

197
00:14:16,300 --> 00:14:19,080
 addiction,

198
00:14:19,080 --> 00:14:24,080
 just by being mindful, not even by trying to fix anything.

199
00:14:24,080 --> 00:14:28,650
 The greatness of reality is that when you see it, when you

200
00:14:28,650 --> 00:14:29,080
 see it,

201
00:14:29,080 --> 00:14:32,080
 you free yourself from the power it has over you.

202
00:14:32,080 --> 00:14:36,800
 You free yourself from all the suffering that comes from

203
00:14:36,800 --> 00:14:38,080
 ignorance.

204
00:14:38,080 --> 00:14:41,840
 That's really the major quality of ignorance is that it

205
00:14:41,840 --> 00:14:43,080
 makes you suffer

206
00:14:43,080 --> 00:14:45,670
 because of course no one wants to suffer, so if you

207
00:14:45,670 --> 00:14:47,080
 actually knew the truth,

208
00:14:47,080 --> 00:14:52,080
 you would never suffer. It's the brilliance of it.

209
00:14:52,080 --> 00:15:01,080
 No one else makes us suffer. We cause ourselves suffering.

210
00:15:01,080 --> 00:15:07,080
 And the fifth dharma is the senses, seeing, hearing,

211
00:15:07,080 --> 00:15:14,080
 smelling, tasting, feeling.

212
00:15:14,080 --> 00:15:20,120
 This is the outside, the external. The rest of them are

213
00:15:20,120 --> 00:15:21,080
 mostly internal,

214
00:15:21,080 --> 00:15:26,080
 but the doors to the external are the senses.

215
00:15:26,080 --> 00:15:30,710
 So this is where we easily get lost. As soon as you open

216
00:15:30,710 --> 00:15:31,080
 your eyes,

217
00:15:31,080 --> 00:15:35,710
 your mind is leaping out through your eyes into all the

218
00:15:35,710 --> 00:15:39,080
 things that you experience.

219
00:15:39,080 --> 00:15:46,090
 So we use mindfulness as a guard, mindfulness as a door

220
00:15:46,090 --> 00:15:47,080
keeper.

221
00:15:47,080 --> 00:15:52,080
 And you see, say to yourself, seeing, hearing, hearing,

222
00:15:52,080 --> 00:16:03,080
 smelling, smelling, tasting, so on.

223
00:16:03,080 --> 00:16:08,080
 And you learn about the reality of your own experience.

224
00:16:08,080 --> 00:16:11,080
 You learn everything you need to know about the universe.

225
00:16:11,080 --> 00:16:18,080
 It's quite remarkable.

226
00:16:18,080 --> 00:16:35,080
 You see the Buddha. You come to see, see Nibbana, see the

227
00:16:35,080 --> 00:16:38,080
 Buddha.

228
00:16:38,080 --> 00:16:43,430
 And you pay the highest homage, the highest tribute, the

229
00:16:43,430 --> 00:16:45,080
 highest respect.

230
00:16:45,080 --> 00:16:49,890
 And I mean, what's most important about that is that you

231
00:16:49,890 --> 00:16:51,080
 feel reassured.

232
00:16:51,080 --> 00:16:55,080
 That people like the Buddha are really, this is, you know,

233
00:16:55,080 --> 00:16:57,080
 people as wise as that,

234
00:16:57,080 --> 00:17:01,080
 praise that which you're doing now.

235
00:17:01,080 --> 00:17:11,450
 When you feel kind of low about yourself, or you're feeling

236
00:17:11,450 --> 00:17:13,080
 inadequate,

237
00:17:13,080 --> 00:17:21,740
 you can be reassured and proud or confident, encouraged by

238
00:17:21,740 --> 00:17:23,080
 the fact

239
00:17:23,080 --> 00:17:26,080
 that what you're doing was praised by the Buddha.

240
00:17:26,080 --> 00:17:28,080
 What you're doing is what the Buddha taught us to do,

241
00:17:28,080 --> 00:17:32,380
 not to do all these religious ceremonies with flowers and

242
00:17:32,380 --> 00:17:34,080
 candles and incense.

243
00:17:34,080 --> 00:17:37,550
 That's all fine, but it's not really, well, it's not at all

244
00:17:37,550 --> 00:17:40,080
 what the Buddha taught us to do.

245
00:17:40,080 --> 00:17:45,800
 Buddha said the best way to pay respect is through the Dham

246
00:17:45,800 --> 00:17:46,080
ma.

247
00:17:46,080 --> 00:17:53,080
 And the best way to be sure you're doing the right thing.

248
00:17:53,080 --> 00:17:57,330
 After I gave the talk, someone came from one of the Chinese

249
00:17:57,330 --> 00:17:59,080
 monasteries, I think,

250
00:17:59,080 --> 00:18:02,080
 and he invited me. They wanted me to go to their monastery

251
00:18:02,080 --> 00:18:03,080
 and teach meditation.

252
00:18:03,080 --> 00:18:06,420
 And he said, "I learned so much. I was only up there for

253
00:18:06,420 --> 00:18:08,080
 less than five minutes,

254
00:18:08,080 --> 00:18:11,080
 maybe five minutes." And he said, "Oh, I learned so much.

255
00:18:11,080 --> 00:18:14,080
 It was quite interesting."

256
00:18:14,080 --> 00:18:16,860
 The Dhamma is not something hard to teach. It's not

257
00:18:16,860 --> 00:18:20,080
 something hard to understand.

258
00:18:20,080 --> 00:18:24,080
 But we get lost and sidetracked.

259
00:18:24,080 --> 00:18:38,080
 So much of what we call Buddhism is just...

