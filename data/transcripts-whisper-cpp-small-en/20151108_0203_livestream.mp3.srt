1
00:00:00,000 --> 00:00:07,000
 Okay, now we're really broadcasting live.

2
00:00:07,000 --> 00:00:10,000
 November 7th, 2015.

3
00:00:10,000 --> 00:00:15,000
 Now let's make sure we've got everything set up.

4
00:00:15,000 --> 00:00:18,000
 Yes, yes, yes, okay.

5
00:00:18,000 --> 00:00:29,000
 And everything should be working properly.

6
00:00:29,000 --> 00:00:36,000
 So,

7
00:00:36,000 --> 00:00:38,000
 we have a yelp listing.

8
00:00:38,000 --> 00:00:42,380
 We have a thanks to Anna, she created a listing for the

9
00:00:42,380 --> 00:00:47,880
 meditation center on Yelp. So if you put in meditation in

10
00:00:47,880 --> 00:00:51,000
 Hamilton, you do pop up.

11
00:00:51,000 --> 00:00:55,310
 So, it's a simple listing so far, can probably put some

12
00:00:55,310 --> 00:01:00,040
 pictures up there and make it a little fancier but it's

13
00:01:00,040 --> 00:01:02,000
 there.

14
00:01:02,000 --> 00:01:06,640
 And shortly we should be getting confirmation to get

15
00:01:06,640 --> 00:01:11,060
 everything tied up for Google as well so that'll be a great

16
00:01:11,060 --> 00:01:14,210
 thing when, when you type it in in the driving directions

17
00:01:14,210 --> 00:01:16,000
 come up and all that kind of thing.

18
00:01:16,000 --> 00:01:22,000
 Just a little more verification needed.

19
00:01:22,000 --> 00:01:26,000
 Just letting people know about the meditation center.

20
00:01:26,000 --> 00:01:27,000
 Awesome.

21
00:01:27,000 --> 00:01:29,000
 Yeah.

22
00:01:29,000 --> 00:01:33,930
 And tomorrow we have our volunteer meeting so we can maybe

23
00:01:33,930 --> 00:01:37,000
 discuss that a little bit tomorrow.

24
00:01:37,000 --> 00:01:38,000
 That's one.

25
00:01:38,000 --> 00:01:45,000
 Yes.

26
00:01:45,000 --> 00:01:49,000
 Tonight we have an interesting verse.

27
00:01:49,000 --> 00:01:53,000
 Deep meditative one I encourage everyone to read this.

28
00:01:53,000 --> 00:01:57,120
 Not sure how much I can talk about it, what there is to be

29
00:01:57,120 --> 00:02:04,000
 said, but it's certainly worth reading.

30
00:02:04,000 --> 00:02:11,290
 It's a part where you have a thorn in your side, and then

31
00:02:11,290 --> 00:02:18,000
 to get it out, you stick another thorn in your side.

32
00:02:18,000 --> 00:02:23,000
 Doesn't really help.

33
00:02:23,000 --> 00:02:27,560
 It's interesting how he actually talks about when a person

34
00:02:27,560 --> 00:02:32,250
 is inundated by painful feelings then they'll chase after

35
00:02:32,250 --> 00:02:36,000
 pleasant feelings. Why?

36
00:02:36,000 --> 00:02:55,000
 Because they don't know any better. They can't think of any

37
00:02:55,000 --> 00:02:55,000
 better way to get rid of unpleasant feelings than to douse

38
00:02:55,000 --> 00:02:55,000
 the fire with gasoline really, to smother it with sensual

39
00:02:55,000 --> 00:02:55,000
 desire.

40
00:02:55,000 --> 00:03:01,100
 And lust arises, and maybe read this, it's a very clear

41
00:03:01,100 --> 00:03:07,350
 exposition on the nature of attachment and so it's a clear

42
00:03:07,350 --> 00:03:12,000
 exposition of the problem that we face.

43
00:03:12,000 --> 00:03:26,400
 An ordinary person knows no other escape from painful

44
00:03:26,400 --> 00:03:29,000
 feelings.

45
00:03:29,000 --> 00:03:29,540
 An ordinary person knows no other escape from painful

46
00:03:29,540 --> 00:03:47,000
 feelings than to douse the fire with sensual desire.

47
00:03:47,000 --> 00:03:57,000
 [Hindi]

48
00:03:57,000 --> 00:04:02,870
 Other than kamasoka, I read that wrong. Confused I was

49
00:04:02,870 --> 00:04:05,000
 reading the wrong way.

50
00:04:05,000 --> 00:04:17,000
 It's hard to read because it's so wide.

51
00:04:17,000 --> 00:04:22,520
 "Nahi so bhikkue, bhajanati asutva putudjano" An ordinary

52
00:04:22,520 --> 00:04:29,770
 worldling who is uninstructed doesn't know any other way

53
00:04:29,770 --> 00:04:38,040
 than kamasoka, the pleasure of sensuality for the dukkhai,

54
00:04:38,040 --> 00:04:46,000
 the nayani, the sahrona, the escape from the

55
00:04:46,000 --> 00:04:56,000
 suffering feelings. They don't know any other way.

56
00:04:56,000 --> 00:05:08,060
 And when they are abhinanda, when they are rejoicing or

57
00:05:08,060 --> 00:05:18,000
 indulging or engaging, rejoicing in central pleasure,

58
00:05:18,000 --> 00:05:28,000
 "yosukayoe dana raga anusayo so anusayti"

59
00:05:28,000 --> 00:05:33,000
 So anusay is kind of like an obsession.

60
00:05:33,000 --> 00:05:43,810
 They become obsessed with the obsession of raga because an

61
00:05:43,810 --> 00:05:51,000
usayti. Anu means to lie dormant. Anusayti means to lie down

62
00:05:51,000 --> 00:05:51,000
 or to lie with, to lie inside.

63
00:05:51,000 --> 00:06:07,520
 Anusayti is something deep. So obsession is kind of like a

64
00:06:07,520 --> 00:06:09,000
 deep attachment.

65
00:06:09,000 --> 00:06:22,000
 So I think I'm deeply involved in raga.

66
00:06:22,000 --> 00:06:24,000
 Do we have questions today?

67
00:06:24,000 --> 00:06:26,000
 We do.

68
00:06:26,000 --> 00:06:29,060
 "Bhante, a homeless person asked for money so that he could

69
00:06:29,060 --> 00:06:32,670
 get some food. I gave it to him and then drove home. On the

70
00:06:32,670 --> 00:06:35,000
 drive home I was very mindful the whole time.

71
00:06:35,000 --> 00:06:39,150
 It took much less effort than usual. Then I thought that if

72
00:06:39,150 --> 00:06:42,060
 I constantly gave a portion of the money I earned from my

73
00:06:42,060 --> 00:06:45,000
 job to a charity, it might help me be more mindful at work

74
00:06:45,000 --> 00:06:47,000
 as it's helping other people.

75
00:06:47,000 --> 00:06:50,000
 Will this work? Is this why the Buddha recommended

76
00:06:50,000 --> 00:06:54,000
 householders to donate a portion of their money to charity?

77
00:06:54,000 --> 00:06:57,550
 I believe I heard you say this before. Thanks Bhante. Sorry

78
00:06:57,550 --> 00:07:07,000
 for the unusually large question. Hope you can answer me."

79
00:07:07,000 --> 00:07:15,000
 Yeah, giving is good no matter what.

80
00:07:15,000 --> 00:07:24,000
 That's about it. Definitely if you can give.

81
00:07:24,000 --> 00:07:28,290
 It's something that supports your mind. The Buddha said, "P

82
00:07:28,290 --> 00:07:32,000
unyantje purisoka yiraka yirathenang punak punang."

83
00:07:32,000 --> 00:07:38,390
 If a person does good deeds, they should do them again and

84
00:07:38,390 --> 00:07:45,370
 again. "Tamhichanda yirata" should cultivate contentment in

85
00:07:45,370 --> 00:07:49,000
 regards to those good deeds.

86
00:07:49,000 --> 00:07:56,470
 For "sukho punyasuchayo" the cultivation or the collecting

87
00:07:56,470 --> 00:08:02,700
 of goodness is happiness. The building up of goodness, the

88
00:08:02,700 --> 00:08:08,000
 accumulating of goodness is happiness.

89
00:08:08,000 --> 00:08:16,260
 "Mabhikave bahita punyanang" Don't be afraid because of pun

90
00:08:16,260 --> 00:08:19,630
ya, of goodness. "Sukhasetang bhikave adivachanange didang

91
00:08:19,630 --> 00:08:20,000
 punyanim."

92
00:08:20,000 --> 00:08:24,000
 Good deeds are another word for happiness.

93
00:08:24,000 --> 00:08:32,970
 Happiness is another word for this. That is goodness. That

94
00:08:32,970 --> 00:08:40,000
 is to say goodness is good deeds.

95
00:08:40,000 --> 00:08:44,230
 Should you be concerned with what people do with the money?

96
00:08:44,230 --> 00:08:48,940
 Sometimes people are asking for change in things. If they

97
00:08:48,940 --> 00:08:52,800
 give the money, they may just use it to buy drugs or

98
00:08:52,800 --> 00:08:54,000
 whatever.

99
00:08:54,000 --> 00:09:09,970
 You can be. You have to ask yourself how important it is to

100
00:09:09,970 --> 00:09:12,480
 you. If it's something that you... It's not like I would

101
00:09:12,480 --> 00:09:14,010
 rest my whole religious life on or spiritual development on

102
00:09:14,010 --> 00:09:19,000
 charity to the poor or charity to a specific individual.

103
00:09:19,000 --> 00:09:27,190
 But it's really not all that relevant. It's relevant the

104
00:09:27,190 --> 00:09:31,700
 sort of person that you're giving it to. So if there's a

105
00:09:31,700 --> 00:09:37,980
 homeless person who is really mean and nasty or really

106
00:09:37,980 --> 00:09:39,000
 immoral.

107
00:09:39,000 --> 00:09:47,290
 And again, giving money is, as I said, a fairly low form of

108
00:09:47,290 --> 00:09:50,660
 charity. Giving money is for that reason as well because

109
00:09:50,660 --> 00:09:53,120
 they have to do something with it. You're not actually

110
00:09:53,120 --> 00:09:55,000
 doing them any favor directly.

111
00:09:55,000 --> 00:09:57,510
 Well, you're doing them a favor, but you're not actually

112
00:09:57,510 --> 00:10:00,550
 giving them anything useful. You're giving them something

113
00:10:00,550 --> 00:10:03,350
 that they can use to get useful things or to get harmful

114
00:10:03,350 --> 00:10:04,000
 things.

115
00:10:04,000 --> 00:10:07,920
 So money is kind of its power. You're giving them power as

116
00:10:07,920 --> 00:10:11,840
 opposed to something that will help them. Power doesn't

117
00:10:11,840 --> 00:10:20,000
 always help. So money is not the best way to give.

118
00:10:20,000 --> 00:10:24,420
 So it could be a concern then if you...like when you're in

119
00:10:24,420 --> 00:10:28,450
 the city and people are asking for change in things. And if

120
00:10:28,450 --> 00:10:32,840
 you think they're potentially going to use it for drugs or

121
00:10:32,840 --> 00:10:36,000
 alcohol, would that be a reason not?

122
00:10:36,000 --> 00:10:40,000
 I would say give a little...sorry, let you finish. Go ahead

123
00:10:40,000 --> 00:10:40,000
.

124
00:10:40,000 --> 00:10:42,000
 No, no, that was it.

125
00:10:42,000 --> 00:10:45,000
 But you were going to say would that be...?

126
00:10:45,000 --> 00:10:49,540
 Well, you know, it seems like a conflict, you know, because

127
00:10:49,540 --> 00:10:54,030
 you can tell that that kind of behavior would harm people.

128
00:10:54,030 --> 00:10:56,000
 Of course, you don't know what someone's going to do.

129
00:10:56,000 --> 00:11:00,370
 But that's always the concern that, you know, when you give

130
00:11:00,370 --> 00:11:04,390
 people money that they're going to use it to just further

131
00:11:04,390 --> 00:11:06,000
 harm themselves.

132
00:11:06,000 --> 00:11:09,000
 Yeah.

133
00:11:09,000 --> 00:11:12,160
 But that's not really your intent when you're giving. It's

134
00:11:12,160 --> 00:11:14,370
 not...I wouldn't worry too much about that. But on the

135
00:11:14,370 --> 00:11:16,700
 other hand, as I said, I wouldn't really put too much into

136
00:11:16,700 --> 00:11:17,000
 it.

137
00:11:17,000 --> 00:11:20,520
 You consider the more pure part of the goodness is the

138
00:11:20,520 --> 00:11:24,520
 giving up because they asked you, not because you want them

139
00:11:24,520 --> 00:11:28,000
 to have something, but because they asked you.

140
00:11:28,000 --> 00:11:30,840
 If I ask you for something, you giving it to me is a

141
00:11:30,840 --> 00:11:34,210
 relinquishing of the stinginess, right, of saying no to

142
00:11:34,210 --> 00:11:35,000
 someone.

143
00:11:35,000 --> 00:11:39,450
 So if someone asks you for something, unless it's immoral

144
00:11:39,450 --> 00:11:42,610
 to give it to them, you give it to them not thinking, oh,

145
00:11:42,610 --> 00:11:44,000
 this will be to their benefit.

146
00:11:44,000 --> 00:11:50,000
 You say, I give as a giving up, as a not clinging.

147
00:11:50,000 --> 00:11:54,420
 Like, like, we sent her, we sent her, I gave things away

148
00:11:54,420 --> 00:11:57,000
 that you probably would have.

149
00:11:57,000 --> 00:12:00,410
 Well, that's not even fair to say, but we sent her to give

150
00:12:00,410 --> 00:12:03,900
 up everything when people asked him to. He was just waiting

151
00:12:03,900 --> 00:12:05,000
 for people to ask him.

152
00:12:05,000 --> 00:12:07,480
 People asked him for things that they shouldn't have asked

153
00:12:07,480 --> 00:12:08,000
 him for.

154
00:12:08,000 --> 00:12:12,130
 And he gave because he had made a determination that if

155
00:12:12,130 --> 00:12:16,000
 anyone asks him for something, he would give.

156
00:12:16,000 --> 00:12:21,570
 So it's two aspects of giving, giving as a intentional good

157
00:12:21,570 --> 00:12:27,000
 deed and giving when someone asks you two different things.

158
00:12:27,000 --> 00:12:31,250
 Like, if you go out of your way to give money to the

159
00:12:31,250 --> 00:12:36,350
 homeless, then you can be rightly questioned as to, hey, do

160
00:12:36,350 --> 00:12:38,000
 you know where your money is going?

161
00:12:38,000 --> 00:12:41,230
 That's a pretty dubious sort of spiritual practice because

162
00:12:41,230 --> 00:12:44,000
 you don't really know what's going to happen to it.

163
00:12:44,000 --> 00:12:46,400
 But if you're walking down the street and someone asks you

164
00:12:46,400 --> 00:12:49,000
 for change, I don't think anyone's going to say, hey, don't

165
00:12:49,000 --> 00:12:49,000
 give.

166
00:12:49,000 --> 00:12:52,000
 Well, people will. I don't think they rightly can say, hey,

167
00:12:52,000 --> 00:12:54,000
 what are you doing giving him money?

168
00:12:54,000 --> 00:12:59,250
 Say, well, he asked me. So it's like one of my students had

169
00:12:59,250 --> 00:13:06,530
 this, was being, was being asked for money again and again

170
00:13:06,530 --> 00:13:11,340
 for this and that from a person overseas, a person in

171
00:13:11,340 --> 00:13:12,000
 another country.

172
00:13:12,000 --> 00:13:14,200
 So they had no way of knowing whether what this person was

173
00:13:14,200 --> 00:13:18,000
 saying was true. And it seemed, it seemed kind of shady.

174
00:13:18,000 --> 00:13:22,380
 And I'd heard similar things of a similar nature. So I, I

175
00:13:22,380 --> 00:13:26,670
 said to him the same thing. I said, you know, give a little

176
00:13:26,670 --> 00:13:27,000
.

177
00:13:27,000 --> 00:13:29,650
 And it's a different kind of practice. It's about giving up

178
00:13:29,650 --> 00:13:30,000
.

179
00:13:30,000 --> 00:13:33,150
 It's not the kind of thing where you'd want to give huge

180
00:13:33,150 --> 00:13:36,510
 sums of money, pour your life savings into it or give them

181
00:13:36,510 --> 00:13:38,000
 everything they want.

182
00:13:38,000 --> 00:13:40,390
 But you give a little and that kind of is a way of giving

183
00:13:40,390 --> 00:13:42,000
 up. Ajahn Tong would do this.

184
00:13:42,000 --> 00:13:44,820
 People would come and ask him for money and he would just

185
00:13:44,820 --> 00:13:46,000
 give them a little.

186
00:13:46,000 --> 00:13:49,510
 There were his relatives, this really funny couple. They

187
00:13:49,510 --> 00:13:51,000
 claimed to be relatives of him.

188
00:13:51,000 --> 00:13:53,190
 I guess they were relatives of him, but they milked it for

189
00:13:53,190 --> 00:13:56,680
 all it was worth. So they would come back again and again,

190
00:13:56,680 --> 00:13:58,000
 asking for money.

191
00:13:58,000 --> 00:14:02,590
 And finally, his secretary locked the doors. I was standing

192
00:14:02,590 --> 00:14:06,670
 outside because I was waiting to come in for for to listen

193
00:14:06,670 --> 00:14:08,000
 in for reporting.

194
00:14:08,000 --> 00:14:11,960
 And they locked the doors and we had to stand outside for

195
00:14:11,960 --> 00:14:16,000
 like half an hour while he yelled at these two people.

196
00:14:16,000 --> 00:14:25,000
 Secretary is a real bulldog.

197
00:14:25,000 --> 00:14:31,000
 Thank you, are calm feelings considered neutral feelings?

198
00:14:31,000 --> 00:14:41,000
 Yes.

199
00:14:41,000 --> 00:14:43,000
 That's it on questions.

200
00:14:43,000 --> 00:14:48,500
 Well, good. Yeah. Lots of questions means fewer questions

201
00:14:48,500 --> 00:14:51,000
 with lots of people, fewer people.

202
00:14:51,000 --> 00:14:56,000
 No, lots of people meditating tonight. Yeah.

203
00:14:56,000 --> 00:15:01,000
 Fewer people logged in.

204
00:15:01,000 --> 00:15:03,000
 That's fine.

205
00:15:03,000 --> 00:15:08,000
 Are you are you looking on the website?

206
00:15:08,000 --> 00:15:12,000
 Yeah, we got the list of green and orange people.

207
00:15:12,000 --> 00:15:14,000
 Yeah.

208
00:15:14,000 --> 00:15:17,700
 I'm not sure that that list is always accurate. It seems

209
00:15:17,700 --> 00:15:19,000
 like I don't know.

210
00:15:19,000 --> 00:15:21,520
 It's only if if they're looking at this page, if they're on

211
00:15:21,520 --> 00:15:24,000
 YouTube, it won't show if they're somewhere else.

212
00:15:24,000 --> 00:15:27,120
 It's not even all the people on the list. It's just people

213
00:15:27,120 --> 00:15:31,440
 who are actively have meditation that's very much open, but

214
00:15:31,440 --> 00:15:34,000
 it should be everyone.

215
00:15:34,000 --> 00:15:37,000
 Maybe it's not accurate.

216
00:15:37,000 --> 00:15:40,000
 Doesn't have Patrick, does it?

217
00:15:40,000 --> 00:15:42,000
 Where did he go?

218
00:15:42,000 --> 00:15:46,010
 Yeah, that's what I wonder because sometimes it shows more

219
00:15:46,010 --> 00:15:50,000
 people meditating then logged in there. So I don't know.

220
00:15:50,000 --> 00:15:54,000
 Maybe people close out of the page or something.

221
00:15:54,000 --> 00:16:01,000
 Maybe it doesn't work as it should.

222
00:16:01,000 --> 00:16:05,350
 I think finally last night's last night's yes last night's

223
00:16:05,350 --> 00:16:10,000
 Dama Padau was the video was well, well made.

224
00:16:10,000 --> 00:16:15,000
 And finally I figured out the settings of this darn camera

225
00:16:15,000 --> 00:16:21,280
 to get it set up to overexpose the background and yet

226
00:16:21,280 --> 00:16:27,000
 maintain a good looking picture.

227
00:16:27,000 --> 00:16:30,000
 If anyone's interested.

228
00:16:30,000 --> 00:16:34,170
 The group here watches your Dama Padau videos no matter

229
00:16:34,170 --> 00:16:38,000
 what, you know, good quality, bad quality.

230
00:16:38,000 --> 00:16:40,280
 I mean, the talk is always wonderful, but sometimes the

231
00:16:40,280 --> 00:16:44,290
 audio is off. But I think this group, the audio last night

232
00:16:44,290 --> 00:16:45,000
 was off.

233
00:16:45,000 --> 00:16:48,000
 See, it's not one thing. It's another.

234
00:16:48,000 --> 00:16:51,570
 The audio is off because there's this thing called, I think

235
00:16:51,570 --> 00:16:54,710
 it's called a DC loop, which is weird because well,

236
00:16:54,710 --> 00:16:59,870
 something like some kind of loop. If you've got somehow two

237
00:16:59,870 --> 00:17:05,000
 connections, then it builds up a sound.

238
00:17:05,000 --> 00:17:08,400
 So if you listen to last night's Dama Padau, you can hear

239
00:17:08,400 --> 00:17:10,000
 it in the background.

240
00:17:10,000 --> 00:17:12,000
 I've had that before and I had no idea what was going on.

241
00:17:12,000 --> 00:17:16,520
 And it's because I had two things plugged in that were

242
00:17:16,520 --> 00:17:19,000
 connected to the camera.

243
00:17:19,000 --> 00:17:23,000
 So this group here we we like them no matter what. But it's

244
00:17:23,000 --> 00:17:29,130
 great that they're coming out better for people who are

245
00:17:29,130 --> 00:17:31,000
 newer to it that.

246
00:17:31,000 --> 00:17:35,000
 So good night then. Thank you, Robin, for your help.

247
00:17:35,000 --> 00:17:37,000
 Thank you.

248
00:17:37,000 --> 00:17:39,000
 Good night.

249
00:17:39,000 --> 00:17:41,000
 Thank you.

