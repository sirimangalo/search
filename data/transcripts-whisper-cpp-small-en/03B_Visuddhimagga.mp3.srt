1
00:00:00,000 --> 00:00:14,000
 In Vinaya, they are called medicine. Oil, ghee oil, honey,

2
00:00:14,000 --> 00:00:22,000
 and molasses.

3
00:00:23,000 --> 00:00:31,500
 Sapte nawanita, teelamadu panita, butter ghee molasses, oil

4
00:00:31,500 --> 00:00:32,000
, honey.

5
00:00:32,000 --> 00:00:39,000
 These five are termed medicine in Vinaya.

6
00:00:39,000 --> 00:00:45,000
 So they can be taken as medicine.

7
00:00:45,000 --> 00:00:50,000
 So a monk can take honey in the afternoon.

8
00:00:50,000 --> 00:01:01,000
 Ghee in the afternoon, but not as food, only as medicine.

9
00:01:01,000 --> 00:01:17,000
 On the next page, the word molasses appear.

10
00:01:18,000 --> 00:01:22,000
 What about the end of the first paragraph?

11
00:01:22,000 --> 00:01:26,730
 "Any requisite for life consisting of oil, honey, molasses,

12
00:01:26,730 --> 00:01:28,000
 ghee, etc."

13
00:01:28,000 --> 00:01:31,530
 That is allowed by a medical man as suitable for the sick

14
00:01:31,530 --> 00:01:33,000
 is what this meant.

15
00:01:33,000 --> 00:01:43,440
 Now, in paragraph 97, the second part, the explanation of

16
00:01:43,440 --> 00:01:47,000
 the word "patcheya" in Bali.

17
00:01:48,000 --> 00:01:52,300
 The word meaning here is this, because breathing things go,

18
00:01:52,300 --> 00:01:57,560
 move, proceed, using what they use, independence on these

19
00:01:57,560 --> 00:01:59,000
 ropes, etc.

20
00:01:59,000 --> 00:02:03,000
 These ropes, etc. are therefore called requisites.

21
00:02:06,000 --> 00:02:15,570
 In fact, instead of saying move, proceed, we should say

22
00:02:15,570 --> 00:02:17,000
 live.

23
00:02:17,000 --> 00:02:23,000
 Breathing things means beings, living beings.

24
00:02:23,000 --> 00:02:28,000
 So living beings go or living beings live or are alive.

25
00:02:32,000 --> 00:02:38,060
 So, independence on these ropes and other things, using

26
00:02:38,060 --> 00:02:39,000
 them.

27
00:02:39,000 --> 00:02:45,000
 That is why they are called "patcheya" in Bali.

28
00:02:45,000 --> 00:02:48,970
 But it is not important whether you don't understand the

29
00:02:48,970 --> 00:02:51,000
 meaning of what "patcheya" is.

30
00:02:51,000 --> 00:02:57,000
 If you know that it means "requisites", then it's enough.

31
00:02:57,000 --> 00:03:06,200
 Now, in the next paragraph, the fourfold virtue has to be

32
00:03:06,200 --> 00:03:12,000
 undertaken by means of faith and so on.

33
00:03:12,000 --> 00:03:17,000
 So, in this paragraph, "For that is accomplished by faith,

34
00:03:17,000 --> 00:03:20,690
 since the announcing of training precepts is outside the

35
00:03:20,690 --> 00:03:22,000
 disciple's province."

36
00:03:22,000 --> 00:03:24,000
 It is very important.

37
00:03:24,000 --> 00:03:27,000
 That's why we cannot change the rules.

38
00:03:27,000 --> 00:03:31,240
 We cannot add anything to the rules or we cannot take

39
00:03:31,240 --> 00:03:33,000
 anything out of the rules.

40
00:03:33,000 --> 00:03:37,530
 Because the announcing or laying down the rules is not in

41
00:03:37,530 --> 00:03:40,000
 the province of disciples.

42
00:03:40,000 --> 00:03:45,000
 Disciples cannot do that. Only the Buddha can do that.

43
00:03:46,000 --> 00:03:54,680
 So, if a monk has no faith or no devotion to the Buddha and

44
00:03:54,680 --> 00:04:00,380
 his teachings, then he will not want to follow or to keep

45
00:04:00,380 --> 00:04:02,000
 all these rules.

46
00:04:02,000 --> 00:04:07,570
 That is why the particular restraint or the first virtue is

47
00:04:07,570 --> 00:04:13,440
 to be undertaken by means of faith, by means of devotion to

48
00:04:13,440 --> 00:04:15,000
 the Buddha.

49
00:04:16,000 --> 00:04:19,990
 And the evidence here is the refusal of the request to

50
00:04:19,990 --> 00:04:25,000
 allow disciples to announce training precepts.

51
00:04:25,000 --> 00:04:29,410
 And that is not... those in the brackets are to be taken

52
00:04:29,410 --> 00:04:30,000
 out.

53
00:04:30,000 --> 00:04:39,000
 Refusal of the request to announce training precepts. Not

54
00:04:39,000 --> 00:04:43,000
 to allow disciples to know.

55
00:04:43,000 --> 00:04:46,000
 It refers to Vinaya.

56
00:04:46,000 --> 00:04:56,150
 Now, Saribuddha asked Buddha how the dispensation of

57
00:04:56,150 --> 00:05:00,000
 different Buddha's fair.

58
00:05:00,000 --> 00:05:05,390
 Some dispensation of some Buddha's last long and some did

59
00:05:05,390 --> 00:05:07,000
 not last long.

60
00:05:07,000 --> 00:05:14,000
 And so, Buddha answered that there were precepts and so on.

61
00:05:14,000 --> 00:05:19,000
 So, Saribuddha requested the Buddha to lay down rules.

62
00:05:19,000 --> 00:05:22,990
 So, please lay down rules so that the dispensation of

63
00:05:22,990 --> 00:05:25,000
 Buddha can endure long.

64
00:05:25,000 --> 00:05:30,000
 Then Buddha said, "No, it is not yet time."

65
00:05:32,000 --> 00:05:36,700
 And that means you do not know when to lay down rules, but

66
00:05:36,700 --> 00:05:39,000
 I know the Buddha meant.

67
00:05:39,000 --> 00:05:45,000
 So, laying down a rule is not in the province of disciples.

68
00:05:45,000 --> 00:05:51,440
 So, the words and the brackets are not in the original but

69
00:05:51,440 --> 00:05:56,000
 put by the translator, but wrongly.

70
00:05:56,000 --> 00:06:00,320
 So, here is the... the evidence here is the refusal of the

71
00:06:00,320 --> 00:06:06,000
 request to announce training precepts or to lay down rules.

72
00:06:06,000 --> 00:06:11,760
 Having therefore undertaken through faith the training

73
00:06:11,760 --> 00:06:16,000
 precepts without exception as announced,

74
00:06:16,000 --> 00:06:20,000
 one should completely perfect them without regard for...

75
00:06:20,000 --> 00:06:23,000
 without regard even for life.

76
00:06:23,000 --> 00:06:26,000
 So, let's put the word even here.

77
00:06:26,000 --> 00:06:31,520
 Even for life. For this is said, as a hand-gut of eggs or

78
00:06:31,520 --> 00:06:34,000
 as a yak of tail,

79
00:06:34,000 --> 00:06:38,000
 or like a darling child or like an only eye.

80
00:06:38,000 --> 00:06:42,000
 So, you who are engaged, your virtue do perfect.

81
00:06:42,000 --> 00:06:48,000
 Be prudent. Be prudent really means be fond of your virtue.

82
00:06:50,000 --> 00:06:53,000
 At all times. And ever scrupulous.

83
00:06:53,000 --> 00:06:56,310
 Scrupulous really means in the Pali, but the Pali word

84
00:06:56,310 --> 00:06:59,000
 really means have respect for the rules,

85
00:06:59,000 --> 00:07:02,000
 have respect for the Buddha, Dhamma, Sangha and so on.

86
00:07:02,000 --> 00:07:12,500
 And then we have the story. So, the story is not difficult

87
00:07:12,500 --> 00:07:14,000
 to understand.

88
00:07:15,000 --> 00:07:19,920
 Now, he augmented his insight. I know, I hope you

89
00:07:19,920 --> 00:07:23,000
 understand that. He augmented his insight.

90
00:07:23,000 --> 00:07:27,000
 What's that?

91
00:07:27,000 --> 00:07:35,000
 He saw into the future.

92
00:07:35,000 --> 00:07:38,000
 Actually, he practiced Vipassana meditation.

93
00:07:44,000 --> 00:07:48,140
 And the second story. Also, they born another elder in Dham

94
00:07:48,140 --> 00:07:53,000
vapani island with stringed creepers and made him lie down.

95
00:07:53,000 --> 00:07:58,000
 When a forest fire came and the creepers were not cut, no.

96
00:07:58,000 --> 00:08:02,020
 Here it should be. When the creepers came, without cutting

97
00:08:02,020 --> 00:08:05,000
 the creepers, he established insight.

98
00:08:05,000 --> 00:08:08,000
 Because he was born with creepers.

99
00:08:08,000 --> 00:08:12,080
 And when the forest fire came, he could easily cut the

100
00:08:12,080 --> 00:08:14,000
 creepers and escape.

101
00:08:14,000 --> 00:08:17,270
 But to cut the creepers means to break the rule of the

102
00:08:17,270 --> 00:08:18,000
 Buddha.

103
00:08:18,000 --> 00:08:22,550
 So, he would sooner give up his life than break the rules

104
00:08:22,550 --> 00:08:24,000
 of the Buddha.

105
00:08:24,000 --> 00:08:30,780
 So, without cutting the creepers, he practiced meditation.

106
00:08:30,780 --> 00:08:33,000
 And he became an arahant.

107
00:08:34,000 --> 00:08:39,720
 So, when the forest fire came, without cutting the creepers

108
00:08:39,720 --> 00:08:43,210
, he established insight and attained niburness

109
00:08:43,210 --> 00:08:44,000
 simultaneously with his death.

110
00:08:44,000 --> 00:08:49,280
 So, simultaneously, Vireya means almost simultaneously. Not

111
00:08:49,280 --> 00:08:51,000
 at the same moment.

112
00:08:51,000 --> 00:08:57,000
 But, you know, mind works very fast and so it is almost

113
00:08:57,000 --> 00:09:01,000
 simultaneous. So, he became an arahant.

114
00:09:01,000 --> 00:09:14,000
 So, the advice given with regard to this story is,

115
00:09:14,000 --> 00:09:17,990
 "Maining the rules of contact pure, renouncing life if

116
00:09:17,990 --> 00:09:21,400
 there be need, rather than break virtues restrained by the

117
00:09:21,400 --> 00:09:23,000
 world's severe decrease."

118
00:09:23,000 --> 00:09:28,060
 So, you should give up your life rather than breaking the

119
00:09:28,060 --> 00:09:31,000
 rules laid down by the Buddha.

120
00:09:31,000 --> 00:09:39,000
 Okay. Do you have any questions?

121
00:09:42,000 --> 00:09:46,340
 I'm a little curious in reading all of this about

122
00:09:46,340 --> 00:09:51,990
 essentially about asceticism and how ascetic loses his life

123
00:09:51,990 --> 00:09:55,520
 among the traditional story of the Buddha renouncing asc

124
00:09:55,520 --> 00:09:56,000
eticism.

125
00:09:56,000 --> 00:10:01,000
 How are those two related?

126
00:10:01,000 --> 00:10:07,440
 Now, when they decided that Buddha renounced asceticism, it

127
00:10:07,440 --> 00:10:13,290
 means he renounced the unnecessarily inflicting pain on

128
00:10:13,290 --> 00:10:17,000
 oneself or self-motification.

129
00:10:21,000 --> 00:10:26,250
 For example, when he was in the forest practicing to become

130
00:10:26,250 --> 00:10:31,000
 the Buddha, he reduced his food little by little.

131
00:10:31,000 --> 00:10:38,440
 First, he went out for alms and ate food. And then he took

132
00:10:38,440 --> 00:10:43,000
 fruit from trees and ate them.

133
00:10:43,000 --> 00:10:49,370
 And later, he took only the fruits that have fallen. And

134
00:10:49,370 --> 00:10:55,200
 later on, he took the fruit only from the tree under which

135
00:10:55,200 --> 00:10:57,000
 he was living.

136
00:10:57,000 --> 00:11:02,810
 And so, little by little, he reduced eating so that his

137
00:11:02,810 --> 00:11:07,000
 body became very thin and emaciated.

138
00:11:07,000 --> 00:11:13,000
 That is an unnecessarily inflicting suffering on himself.

139
00:11:13,000 --> 00:11:18,610
 So, that kind of asceticism Buddha refused or Buddha denied

140
00:11:18,610 --> 00:11:19,000
.

141
00:11:19,000 --> 00:11:25,520
 But the ascetic practices given here are not that severe,

142
00:11:25,520 --> 00:11:28,000
 not that rigorous.

143
00:11:28,000 --> 00:11:34,430
 Like eating in one bowl only or going out for alms and then

144
00:11:34,430 --> 00:11:39,000
 when going for alms, you do not skip any house and so on.

145
00:11:39,000 --> 00:11:45,100
 So, these are called asceticism, ascetic practices, but not

146
00:11:45,100 --> 00:11:50,680
 like those practiced by the other hermit or sages during

147
00:11:50,680 --> 00:11:52,000
 his time.

148
00:11:53,000 --> 00:12:03,270
 We will come to the ascetic practices in the second chapter

149
00:12:03,270 --> 00:12:04,000
.

150
00:12:04,000 --> 00:22:25,590
 One thing I want to say is, when I went to a Zen center in

151
00:22:25,590 --> 00:12:18,000
 Japan, after eating in their bowls,

152
00:12:18,000 --> 00:12:23,670
 they rinsed the bowl and then they drained the water from

153
00:12:23,670 --> 00:12:25,000
 the bowls.

154
00:12:25,000 --> 00:12:33,580
 So, it reminded me of one of the ascetic practices, the T

155
00:12:33,580 --> 00:12:37,000
ear of Water monks practice.

156
00:12:37,000 --> 00:12:41,300
 And there is eating in one bowl only. So, if you have only

157
00:12:41,300 --> 00:12:43,000
 to use one bowl,

158
00:12:43,000 --> 00:12:48,000
 then you eat in that bowl and you drink in that bowl.

159
00:12:48,000 --> 00:13:01,070
 So, I think there are some practices carried to countries

160
00:13:01,070 --> 00:13:04,000
 far away from India and then may have changed a little.

161
00:13:04,000 --> 00:13:13,000
 And so, it seemed to become a very different practice.

162
00:13:13,000 --> 00:13:19,000
 But I think there is something common in both practices.

163
00:13:19,000 --> 00:13:21,950
 When we eat in a Zen dao, we eat that way with bowls and we

164
00:13:21,950 --> 00:13:23,000
 wash our bowls.

165
00:13:23,000 --> 00:13:32,330
 And also, you are always served and it is like alms. You

166
00:13:32,330 --> 00:13:36,000
 just, food comes and it is served to you.

167
00:13:36,000 --> 00:13:40,380
 We don't do begging, but in the way we eat in a Zen dao, it

168
00:13:40,380 --> 00:13:42,000
 is very similar.

169
00:13:42,000 --> 00:13:48,750
 That's right. And when monks are invited to houses of lay

170
00:13:48,750 --> 00:13:53,000
 people during the time of the Buddha,

171
00:13:53,000 --> 00:14:01,170
 or even at the present time in Sri Lanka, they sit, the

172
00:14:01,170 --> 00:14:05,000
 monks sit on the floor, say in a row,

173
00:14:05,000 --> 00:14:10,370
 and lay people take food and then put in their bowls one by

174
00:14:10,370 --> 00:14:15,000
 one, like you are eating in Zen centers.

175
00:14:15,000 --> 00:14:20,830
 So, that is the practice done in India and also in Sri

176
00:14:20,830 --> 00:14:27,000
 Lanka. But in Burma, it is different.

177
00:14:27,000 --> 00:14:35,000
 So, we can see many similarities or many common practices

178
00:14:35,000 --> 00:14:40,000
 that have become a little changed,

179
00:14:40,000 --> 00:14:48,000
 depending on the country and on the people, on the races.

180
00:14:48,000 --> 00:14:53,000
 But the original intention seems to be there in every...

181
00:14:53,000 --> 00:14:58,830
 That's right. These are all men, men to not do, means, what

182
00:14:58,830 --> 00:15:01,000
 do you call this, the shake off.

183
00:15:01,000 --> 00:15:06,830
 It is the shake off defilements that these practices have

184
00:15:06,830 --> 00:15:08,000
 to be taken.

185
00:15:08,000 --> 00:15:12,920
 Futeness of want and no attachment to food and all these

186
00:15:12,920 --> 00:15:14,000
 things.

187
00:15:14,000 --> 00:15:23,390
 So, only by practicing that particular act, I guess, keep

188
00:15:23,390 --> 00:15:29,000
 reminding yourself of why you are eating and so on.

189
00:15:29,000 --> 00:15:31,000
 That's right.

190
00:15:31,000 --> 00:15:33,000
 Or the taste of beautified...

191
00:15:33,000 --> 00:15:41,700
 You always have to be on your guard to avoid mental defile

192
00:15:41,700 --> 00:15:47,000
ments from coming to your mind.

193
00:15:47,000 --> 00:15:51,150
 Many people talk about what they don't like their job or

194
00:15:51,150 --> 00:15:54,000
 they don't feel good about their livelihood,

195
00:15:54,000 --> 00:16:00,480
 but we can see here how monks are warned or have to be

196
00:16:00,480 --> 00:16:06,000
 careful about exactly how they...

197
00:16:06,000 --> 00:16:10,700
 So, in a lot of ways, that's another reflection about how

198
00:16:10,700 --> 00:16:13,000
 in our more complex society,

199
00:16:13,000 --> 00:16:17,000
 how we act, how we can be careful about our behavior.

200
00:16:17,000 --> 00:16:22,370
 So, that may be the exact same way, but how in each act,

201
00:16:22,370 --> 00:16:25,000
 how easy it is to be selfish.

202
00:16:25,000 --> 00:16:30,000
 Even if it's not in a very little thing.

203
00:16:39,000 --> 00:16:47,000
 So, try to put Buddhism in some way in everyday life.

204
00:16:47,000 --> 00:16:48,000
 Mindful.

205
00:16:48,000 --> 00:16:51,000
 That's right.

206
00:16:51,000 --> 00:16:59,520
 And in one of the sodas in Angutra Nikaya, the daily

207
00:16:59,520 --> 00:17:01,000
 reflections are given there.

208
00:17:01,000 --> 00:17:08,000
 A monk or a layperson must make these reflections.

209
00:17:08,000 --> 00:17:13,000
 That means I'm getting old and I cannot avoid getting old

210
00:17:13,000 --> 00:17:15,000
 and I'll get deceased

211
00:17:15,000 --> 00:17:17,890
 and I cannot avoid that I want to die and I cannot get away

212
00:17:17,890 --> 00:17:19,000
 from it and so on.

213
00:17:19,000 --> 00:17:23,430
 So, these reflections that we made every day, both by monks

214
00:17:23,430 --> 00:17:25,000
 and laypeople,

215
00:17:25,000 --> 00:17:30,350
 so that they can get rid of pride in their youth, pride in

216
00:17:30,350 --> 00:17:31,000
 their health,

217
00:17:31,000 --> 00:17:37,000
 and pride in their belongings and so on.

218
00:17:38,000 --> 00:17:43,000
 That's a very good soda, both for monks and laymen.

219
00:17:43,000 --> 00:17:44,000
 Which he faces.

220
00:17:44,000 --> 00:17:50,000
 This is in the Angutra, the gradual sayings.

221
00:17:50,000 --> 00:17:58,000
 I don't remember the name of the soda.

222
00:17:58,000 --> 00:18:02,760
 But do you think that might be, there might be some problem

223
00:18:02,760 --> 00:18:03,000
,

224
00:18:03,000 --> 00:18:07,000
 might not be for everyone,

225
00:18:07,000 --> 00:18:12,000
 some person with a depressive tendency, you know,

226
00:18:12,000 --> 00:18:16,590
 you do that too much, you might just go over the golden

227
00:18:16,590 --> 00:18:17,000
 rule.

228
00:18:17,000 --> 00:18:18,000
 That's right.

229
00:18:18,000 --> 00:18:20,000
 No, no.

230
00:18:20,000 --> 00:18:21,000
 How do you...

231
00:18:21,000 --> 00:18:24,000
 You have to follow the, what do you call that, middle way.

232
00:18:24,000 --> 00:18:26,000
 Golden rule.

233
00:18:29,000 --> 00:18:35,000
 These reflections are to be made not to get depressed,

234
00:18:35,000 --> 00:18:41,000
 it is to get rid of pride in yourself or in your appearance

235
00:18:41,000 --> 00:18:44,000
 or in your body or whatever.

236
00:18:44,000 --> 00:18:50,390
 So, it is for the purpose of getting rid of undesirable

237
00:18:50,390 --> 00:18:53,000
 mental traits

238
00:18:53,000 --> 00:18:56,000
 that these have to be practiced.

239
00:18:56,000 --> 00:19:00,000
 Not to be carried too far.

240
00:19:00,000 --> 00:19:08,440
 You know, once when Buddha thought about the foulness of

241
00:19:08,440 --> 00:19:11,000
 the body,

242
00:19:11,000 --> 00:19:15,000
 monks got so, so, what do you call,

243
00:19:15,000 --> 00:19:18,000
 so disgusted with their bodies that they kill themselves

244
00:19:18,000 --> 00:19:21,000
 and they ask other people to kill them.

245
00:19:21,000 --> 00:19:25,000
 That really happened during the time of the Buddha.

246
00:19:25,000 --> 00:19:27,870
 And Buddha knew that it would happen but he couldn't avoid

247
00:19:27,870 --> 00:19:28,000
 it

248
00:19:28,000 --> 00:19:33,000
 because in the common race it is explained that the karma

249
00:19:33,000 --> 00:19:34,000
 they did in the past,

250
00:19:34,000 --> 00:19:38,000
 they did together in the past,

251
00:19:38,000 --> 00:19:42,000
 got the opportunity to give result at that time.

252
00:19:42,000 --> 00:19:45,000
 And so, Buddha thought if they had to die,

253
00:19:45,000 --> 00:19:49,000
 let them die with this kind of meditation,

254
00:19:49,000 --> 00:19:55,000
 it would help them to get a good rebirth.

255
00:19:55,000 --> 00:20:00,000
 So Buddha taught the foulness of the body meditation

256
00:20:00,000 --> 00:20:02,000
 and then he said,

257
00:20:02,000 --> 00:20:06,470
 "I must not be approached by any monk for fifteen days, two

258
00:20:06,470 --> 00:20:07,000
 weeks.

259
00:20:07,000 --> 00:20:09,000
 I want to be alone.

260
00:20:09,000 --> 00:20:13,000
 Only the monk who brings food must approach me."

261
00:20:13,000 --> 00:20:18,000
 So after fifteen days he came out of his seclusion

262
00:20:18,000 --> 00:20:21,400
 and then asked Ananda, "Why Ananda? There are a few monks

263
00:20:21,400 --> 00:20:22,000
 now."

264
00:20:22,000 --> 00:20:30,000
 Ananda said, "Because you taught that meditation."

265
00:20:30,000 --> 00:20:35,200
 And so Ananda asked Buddha to teach some other kind of

266
00:20:35,200 --> 00:20:36,000
 meditation.

267
00:20:36,000 --> 00:20:40,000
 So he taught the breathing meditation at that time.

268
00:20:40,000 --> 00:20:42,000
 That's a true story.

269
00:20:42,000 --> 00:20:48,000
 That's in the book of discipline.

270
00:20:48,000 --> 00:20:53,000
 If you want to read the book of discipline,

271
00:20:53,000 --> 00:20:56,000
 I cannot tell the page,

272
00:20:56,000 --> 00:21:06,000
 but it's the third of the first four defeat rules,

273
00:21:06,000 --> 00:21:08,000
 rules which entail defeat.

274
00:21:08,000 --> 00:21:14,000
 The third one is not to kill human beings.

275
00:21:14,000 --> 00:21:21,000
 Killing human beings is a grave offence for the monks.

276
00:21:21,000 --> 00:21:29,000
 So that story is given there.

277
00:21:29,000 --> 00:21:38,000
 Thank you.

278
00:21:38,000 --> 00:21:42,990
 So please read to the end of the first chapter for next

279
00:21:42,990 --> 00:21:45,000
 week.

280
00:21:45,000 --> 00:22:00,000
 Thank you.

281
00:22:00,000 --> 00:22:00,320
 [

282
00:22:00,320 --> 00:22:25,320
 Pause ]

