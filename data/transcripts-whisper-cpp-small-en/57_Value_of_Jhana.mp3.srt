1
00:00:00,000 --> 00:00:03,600
 Value of Jhana question.

2
00:00:03,600 --> 00:00:09,290
 Dear you, Tada more, how much value do you put on the

3
00:00:09,290 --> 00:00:10,560
 practice of Jhana?

4
00:00:10,560 --> 00:00:16,480
 Did not the Buddha advocate Jhana practice at least up to a

5
00:00:16,480 --> 00:00:19,520
 point, and does it not have a use

6
00:00:19,520 --> 00:00:26,680
 in deepening Vipassana practice? Answer. The word Jhana is

7
00:00:26,680 --> 00:00:29,920
 a highly debated one among modern

8
00:00:29,920 --> 00:00:35,470
 Theravada Buddhists. Not the meaning of the word, but its

9
00:00:35,470 --> 00:00:38,080
 use and place in Buddhism.

10
00:00:38,080 --> 00:00:42,870
 I think a lot of the controversy comes from the fact that

11
00:00:42,870 --> 00:00:45,600
 we apply too much meaning to the word.

12
00:00:45,600 --> 00:00:51,850
 The word Jhana means meditation or focusing or absorption.

13
00:00:51,850 --> 00:00:55,760
 It means fixing the mind on an object.

14
00:00:56,560 --> 00:01:00,880
 And in truth, that definition applies to all meditation.

15
00:01:00,880 --> 00:01:04,240
 This is why the Buddha said

16
00:01:04,240 --> 00:01:08,910
 there is no wisdom without Jhana and there is no Jhana

17
00:01:08,910 --> 00:01:13,120
 without wisdom. Because true wisdom comes

18
00:01:13,120 --> 00:01:17,830
 only through proper meditation practice and only those

19
00:01:17,830 --> 00:01:20,320
 practices that lead to wisdom

20
00:01:21,120 --> 00:01:28,150
 are proper to be called meditation. The Theravada tradition

21
00:01:28,150 --> 00:01:31,360
 categorizes meditation as being of two

22
00:01:31,360 --> 00:01:36,690
 types. The first type is called Samatha meditation where

23
00:01:36,690 --> 00:01:40,400
 one focuses on a single conceptual object

24
00:01:40,400 --> 00:01:46,400
 created in the mind. For example, the Buddha or a color.

25
00:01:46,400 --> 00:01:50,800
 The object of this type of meditation

26
00:01:50,800 --> 00:01:56,110
 is not real and as a result it cannot bring wisdom or

27
00:01:56,110 --> 00:01:59,120
 understanding about reality.

28
00:01:59,120 --> 00:02:04,980
 It will bring great states of calm and so it is called Sam

29
00:02:04,980 --> 00:02:08,320
atha or tranquility meditation.

30
00:02:08,320 --> 00:02:15,150
 This type of meditation can be useful as a precursor to Vip

31
00:02:15,150 --> 00:02:17,200
assana because it

32
00:02:17,200 --> 00:02:22,580
 calms and strengthens the mind. It can also be used to

33
00:02:22,580 --> 00:02:26,560
 attain extraordinary mental powers

34
00:02:26,560 --> 00:02:32,140
 of various sorts but it does not lead directly to freedom

35
00:02:32,140 --> 00:02:33,520
 from suffering.

36
00:02:33,520 --> 00:02:39,890
 To become free from suffering requires a different type of

37
00:02:39,890 --> 00:02:43,520
 Jhana called Vipassana Jhana.

38
00:02:44,720 --> 00:02:51,130
 Vipassana Jhana means seeing clearly meditation. When you

39
00:02:51,130 --> 00:02:55,520
 practice Vipassana you also focus on

40
00:02:55,520 --> 00:03:01,610
 an object and so it can also be called Jhana. Your mind is

41
00:03:01,610 --> 00:03:05,520
 focused on and clearly aware of a single

42
00:03:05,520 --> 00:03:11,980
 object. When we say to ourselves rising we are clearly

43
00:03:11,980 --> 00:03:16,400
 aware of the stomach rising. When we say

44
00:03:16,400 --> 00:03:22,100
 falling we are clearly aware of the falling. Through this

45
00:03:22,100 --> 00:03:25,360
 practice the mind also gives up

46
00:03:25,360 --> 00:03:30,560
 the mental hindrances of liking, disliking, drowsiness,

47
00:03:30,560 --> 00:03:35,920
 distraction and doubt. It becomes fixed

48
00:03:35,920 --> 00:03:42,120
 and focused and so you can say it enters Jhana. It enters

49
00:03:42,120 --> 00:03:44,320
 the Vipassana Jhana.

50
00:03:46,080 --> 00:03:51,450
 The real difference between Samatha meditation and Vipass

51
00:03:51,450 --> 00:03:55,360
ana meditation is that Samatha takes

52
00:03:55,360 --> 00:04:00,640
 a conceptual object and Vipassana meditation takes reality

53
00:04:00,640 --> 00:04:05,520
 as an object. Anything that arises

54
00:04:05,520 --> 00:04:10,440
 in the present moment whether it be in body, feeling,

55
00:04:10,440 --> 00:04:14,720
 thoughts, states of mind or sensory

56
00:04:14,720 --> 00:04:20,480
 experience. A lot of the argument and debate about Jhana

57
00:04:20,480 --> 00:04:24,400
 misses the point. Worrying about

58
00:04:24,400 --> 00:04:29,560
 terms and ideas can distract us from the goal which is to

59
00:04:29,560 --> 00:04:33,520
 understand ultimate reality and become

60
00:04:33,520 --> 00:04:35,360
 free from suffering.

