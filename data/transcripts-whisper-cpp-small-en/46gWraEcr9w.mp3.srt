1
00:00:00,000 --> 00:00:07,300
 Okay, good evening everyone and welcome to our evening Dham

2
00:00:07,300 --> 00:00:18,720
ma session.

3
00:00:18,720 --> 00:00:24,000
 Tonight I'd like to talk a little bit about Samsara.

4
00:00:24,000 --> 00:00:31,520
 Samsara is a word, I think it's one of the more commonly

5
00:00:31,520 --> 00:00:33,000
 known words in Buddhism.

6
00:00:33,000 --> 00:00:36,440
 Even if you're not a Buddhist, you know a little bit about

7
00:00:36,440 --> 00:00:37,000
 Buddhism.

8
00:00:37,000 --> 00:00:45,000
 You've probably heard this word before.

9
00:00:45,000 --> 00:00:54,350
 We have in the Dhammapada, Chaturanangaryasachanam, I think

10
00:00:54,350 --> 00:00:56,000
 it's in the Dhammapada actually, I'm not sure.

11
00:00:56,000 --> 00:01:02,020
 Chaturanangaryasachanam yathabhutangadasana, sangsaritang

12
00:01:02,020 --> 00:01:08,000
 di gamatanam dasutasv yavajathis.

13
00:01:08,000 --> 00:01:16,200
 Not seeing these four noble truths, we have wandered on in

14
00:01:16,200 --> 00:01:19,000
 this, we have wandered on for a long time.

15
00:01:19,000 --> 00:01:31,940
 Dheegam, Dheegamandhanam, called this long road in Samsara,

16
00:01:31,940 --> 00:01:43,000
 dasutasv yavajathis, from life to life, birth to birth.

17
00:01:43,000 --> 00:01:49,000
 So Samsara is the universe as we know it.

18
00:01:49,000 --> 00:01:53,000
 It doesn't actually refer to the physical universe.

19
00:01:53,000 --> 00:02:01,000
 There is no physical universe in Buddhism, it's not real.

20
00:02:01,000 --> 00:02:05,960
 Physical space is something that is dependent on or derived

21
00:02:05,960 --> 00:02:09,840
 from matter, which of course is still dependent on

22
00:02:09,840 --> 00:02:11,000
 experience.

23
00:02:11,000 --> 00:02:15,340
 So in a sense, in the world, the universe and all the

24
00:02:15,340 --> 00:02:19,760
 planets and so on, it's only there because we experience it

25
00:02:19,760 --> 00:02:20,000
.

26
00:02:20,000 --> 00:02:28,000
 It only exists in the context of our experience.

27
00:02:28,000 --> 00:02:35,610
 And quantum physics has something to say along those lines,

28
00:02:35,610 --> 00:02:37,000
 I think.

29
00:02:37,000 --> 00:02:45,320
 What sangsara is in Buddhism is this rebirth, the path of

30
00:02:45,320 --> 00:02:53,000
 the individual, the path of being birth and death, really.

31
00:02:53,000 --> 00:03:02,000
 Samsara is really all about being born again.

32
00:03:02,000 --> 00:03:09,610
 Samsara means when you die, you start all over, or you

33
00:03:09,610 --> 00:03:14,000
 continue on in whatever way.

34
00:03:14,000 --> 00:03:17,350
 And so we talk about the many realms of rebirth, there are

35
00:03:17,350 --> 00:03:20,350
 the 31 realms in Theravada Buddhism, but I think that's

36
00:03:20,350 --> 00:03:22,000
 just a convention.

37
00:03:22,000 --> 00:03:26,000
 My guess is you can subdivide into many, many more realms.

38
00:03:26,000 --> 00:03:30,700
 The hell realms, for example, there are many, many

39
00:03:30,700 --> 00:03:34,000
 different hells that one can go to.

40
00:03:34,000 --> 00:03:37,620
 But at the top you have the Brahma realms, which if you

41
00:03:37,620 --> 00:03:41,000
 practice Samatha meditation, you go there.

42
00:03:41,000 --> 00:03:45,000
 Below that you have the Angel realms, the Deva realms,

43
00:03:45,000 --> 00:03:48,000
 where if you do lots of super good deeds,

44
00:03:48,000 --> 00:03:51,870
 you're a very kind and gentle and generous person, moral

45
00:03:51,870 --> 00:03:53,000
 and ethical.

46
00:03:53,000 --> 00:03:59,000
 You go to these heavens, and then we have the Human realms.

47
00:03:59,000 --> 00:04:05,850
 If you're reasonably ethical and moral and good, you'll be

48
00:04:05,850 --> 00:04:08,000
 born as a human.

49
00:04:08,000 --> 00:04:10,920
 You have the animal realms, and if you're full of delusion,

50
00:04:10,920 --> 00:04:12,000
 you'll go there.

51
00:04:12,000 --> 00:04:16,050
 We have the ghost realms, if you're full of greed, you'll

52
00:04:16,050 --> 00:04:17,000
 go there.

53
00:04:17,000 --> 00:04:23,440
 We have the hell realms, where if you're full of anger,

54
00:04:23,440 --> 00:04:36,000
 fear, spite, hatred, malice, all this, you'll go to hell.

55
00:04:36,000 --> 00:04:43,650
 In a conventional sense, this is what samsara means. We

56
00:04:43,650 --> 00:04:49,000
 wander around aimlessly, really.

57
00:04:49,000 --> 00:04:54,160
 We have goals and aims from time to time, but they're short

58
00:04:54,160 --> 00:05:00,000
-sighted and meaningless in the larger context of samsara,

59
00:05:00,000 --> 00:05:02,000
 of our wandering from life to life.

60
00:05:02,000 --> 00:05:06,610
 Sometimes we're angels, sometimes we're gods. Often we're

61
00:05:06,610 --> 00:05:15,760
 humans, and often we're animals, ghosts, and sometimes we

62
00:05:15,760 --> 00:05:20,000
're born to hell.

63
00:05:20,000 --> 00:05:25,320
 It's somewhat of a foreign paradigm, I think, for the

64
00:05:25,320 --> 00:05:27,000
 modern world.

65
00:05:27,000 --> 00:05:29,950
 So we've become accustomed to thinking in terms of the

66
00:05:29,950 --> 00:05:34,000
 physical world, and this conceptual world, really.

67
00:05:34,000 --> 00:05:42,160
 We conceive of space and time, and we have all sorts of

68
00:05:42,160 --> 00:05:49,000
 formulas and theories about space-time.

69
00:05:49,000 --> 00:05:52,730
 And in fact, both space and time are not quite what we

70
00:05:52,730 --> 00:05:57,340
 think they are, and not really what we think they are at

71
00:05:57,340 --> 00:05:58,000
 all.

72
00:05:58,000 --> 00:06:02,560
 Space is just a derivation of matter, and time is just

73
00:06:02,560 --> 00:06:07,560
 about the changes inherent in the present moment, which is

74
00:06:07,560 --> 00:06:10,000
 the only time that really exists.

75
00:06:10,000 --> 00:06:19,480
 So if you become familiar with this experiential paradigm

76
00:06:19,480 --> 00:06:27,400
 of being reborn again and again, this way of looking at the

77
00:06:27,400 --> 00:06:30,000
 world,

78
00:06:30,000 --> 00:06:33,410
 it really puts everything into perspective in a way that

79
00:06:33,410 --> 00:06:35,000
 many of us don't think.

80
00:06:35,000 --> 00:06:37,180
 I think for a lot of people it goes well beyond their

81
00:06:37,180 --> 00:06:39,000
 reasons for practicing meditation.

82
00:06:39,000 --> 00:06:41,980
 You don't find many people nowadays saying, "Hey, I'm going

83
00:06:41,980 --> 00:06:45,200
 to go meditate so I can be free from all the many realms of

84
00:06:45,200 --> 00:06:46,000
 rebirth."

85
00:06:46,000 --> 00:06:49,050
 Most of us don't even have a conception of the other, or

86
00:06:49,050 --> 00:06:59,000
 other realms, or being reborn even at all, perhaps.

87
00:06:59,000 --> 00:07:04,570
 Nonetheless, it being the nature of reality as understood

88
00:07:04,570 --> 00:07:09,520
 by the Buddhists and by the Buddha, it's useful for us to

89
00:07:09,520 --> 00:07:12,000
 think of it as the larger picture.

90
00:07:12,000 --> 00:07:15,200
 In a conventional sense, it's useful because it helps us

91
00:07:15,200 --> 00:07:18,000
 really understand the depth of our practice.

92
00:07:18,000 --> 00:07:20,990
 That, yes, it's about stress reduction. Yes, it's about

93
00:07:20,990 --> 00:07:23,000
 overcoming our defilements.

94
00:07:23,000 --> 00:07:28,640
 Ultimately, there's a broader context that we've been

95
00:07:28,640 --> 00:07:42,430
 traveling, we've been wandering on ad infinitum, ad infinit

96
00:07:42,430 --> 00:07:44,000
um.

97
00:07:44,000 --> 00:07:49,240
 With no end in sight, no beginning in sight, we don't know

98
00:07:49,240 --> 00:07:56,000
 how long we've been going. Perhaps it's eternal.

99
00:07:56,000 --> 00:08:00,000
 And in the end it becomes meaningless.

100
00:08:00,000 --> 00:08:10,440
 So you realize that all of this is quite exciting, really,

101
00:08:10,440 --> 00:08:13,000
 to think about all the many realms you can be born in.

102
00:08:13,000 --> 00:08:19,120
 That's something new, but as you consider, as you learn and

103
00:08:19,120 --> 00:08:24,290
 as you meditate, you start to see that it's all just

104
00:08:24,290 --> 00:08:26,000
 wandering on.

105
00:08:26,000 --> 00:08:31,000
 It's all meaningless, pointless, useless.

106
00:08:31,000 --> 00:08:35,200
 But again, I think that goes far beyond what most people

107
00:08:35,200 --> 00:08:40,000
 think about when they undertake the practice of meditation.

108
00:08:40,000 --> 00:08:43,160
 Fortunately, you don't have to think that big. It's

109
00:08:43,160 --> 00:08:46,570
 something that's profound on the level of a Buddha to think

110
00:08:46,570 --> 00:08:52,550
 about these things and to really realize that this is

111
00:08:52,550 --> 00:08:56,000
 enough, that they've had enough with samsara.

112
00:08:56,000 --> 00:08:59,250
 But samsara, that's just the conceptual nature of it. In

113
00:08:59,250 --> 00:09:04,350
 reality, samsara, if you want to go deeper, it's just the

114
00:09:04,350 --> 00:09:05,000
 cycle.

115
00:09:05,000 --> 00:09:08,230
 We often hear about the cycle of birth, old age, sickness

116
00:09:08,230 --> 00:09:11,000
 and death. That's what we're talking about.

117
00:09:11,000 --> 00:09:15,110
 That no matter where we go, there is always going to be

118
00:09:15,110 --> 00:09:18,000
 this being starting on a new journey.

119
00:09:18,000 --> 00:09:23,980
 Going through the motions of learning and adapting and

120
00:09:23,980 --> 00:09:29,700
 clinging and settling down in some sort of measure of

121
00:09:29,700 --> 00:09:31,000
 stability,

122
00:09:31,000 --> 00:09:40,600
 only to be uprooted again at the moment of death and be

123
00:09:40,600 --> 00:09:49,000
 cast out and sent on our way to wander again.

124
00:09:49,000 --> 00:09:52,710
 So if we want to understand it, a much easier way to

125
00:09:52,710 --> 00:09:56,850
 understand it is this way in terms of one life. It's still

126
00:09:56,850 --> 00:10:00,000
 conceptual, but much more useful.

127
00:10:00,000 --> 00:10:04,000
 Much more useful to think about what it means to be alive.

128
00:10:04,000 --> 00:10:10,000
 No matter where we're born, what is it? What is involved?

129
00:10:10,000 --> 00:10:12,870
 When we look and we see we're making the same mistakes

130
00:10:12,870 --> 00:10:16,110
 again and again. If this has been infinite, if we're doing

131
00:10:16,110 --> 00:10:19,930
 this forever, it's really quite shameful that we're still

132
00:10:19,930 --> 00:10:22,000
 making so many mistakes.

133
00:10:22,000 --> 00:10:32,850
 And it's certainly a sign that this isn't the ideal. This

134
00:10:32,850 --> 00:10:38,000
 can't be as good as it gets.

135
00:10:38,000 --> 00:10:41,930
 But if we really want to understand samsara, of course, we

136
00:10:41,930 --> 00:10:44,000
 have to go beyond concepts.

137
00:10:44,000 --> 00:10:48,360
 And so the ultimate way, the third way we can understand s

138
00:10:48,360 --> 00:10:53,000
amsara is in terms of moment to moment wandering on of sorts

139
00:10:53,000 --> 00:10:53,000
.

140
00:10:53,000 --> 00:10:58,530
 This mindless zombie-like state that we find ourselves in

141
00:10:58,530 --> 00:11:04,000
 where we go through the motions of indulging and clinging

142
00:11:04,000 --> 00:11:10,600
 and judging and avoiding and running and chasing the good

143
00:11:10,600 --> 00:11:13,000
 and the bad,

144
00:11:13,000 --> 00:11:18,850
 out of delusion and out of blindness, and not seen clearly,

145
00:11:18,850 --> 00:11:23,000
 because we don't see the Four Noble Truths.

146
00:11:23,000 --> 00:11:25,920
 That we don't even have to think about being reborn or

147
00:11:25,920 --> 00:11:31,060
 going to this realm or that realm. Ultimately it's all just

148
00:11:31,060 --> 00:11:33,000
 experience.

149
00:11:33,000 --> 00:11:41,460
 No matter where we go, we're not going to be free from the

150
00:11:41,460 --> 00:11:46,000
 senses, from experience.

151
00:11:46,000 --> 00:11:51,130
 And so this is really what we come to understand through

152
00:11:51,130 --> 00:11:53,000
 the meditation.

153
00:11:53,000 --> 00:11:56,000
 Reality is just made up of experiences.

154
00:11:56,000 --> 00:11:58,700
 We continue on and on and we wander on and on. We wander

155
00:11:58,700 --> 00:12:03,000
 through experiences without any real rhyme or reason.

156
00:12:03,000 --> 00:12:05,000
 That's what you start to see in meditation, right?

157
00:12:05,000 --> 00:12:08,000
 Oh, and wander around like a lost person.

158
00:12:08,000 --> 00:12:13,300
 And you start to cultivate this sense of direction, a

159
00:12:13,300 --> 00:12:22,000
 compass, a way to go, a path.

160
00:12:22,000 --> 00:12:26,350
 You start to see the right way, the right path, not as

161
00:12:26,350 --> 00:12:30,650
 going here or going there, but as stopping, as not

162
00:12:30,650 --> 00:12:35,600
 wandering anymore, as heading in the right direction, if

163
00:12:35,600 --> 00:12:43,000
 you will, in the sense of actually not going anywhere.

164
00:12:43,000 --> 00:12:46,670
 So we focus on the five aggregates, the four satipatthana,

165
00:12:46,670 --> 00:12:50,000
 the six senses, however you want to describe it.

166
00:12:50,000 --> 00:12:53,000
 But we focus on experience.

167
00:12:53,000 --> 00:12:56,920
 And so our act of being objective, of reminding ourselves

168
00:12:56,920 --> 00:12:59,000
 and of learning to see clearly.

169
00:12:59,000 --> 00:13:05,250
 This is about really understanding, well, seeing through

170
00:13:05,250 --> 00:13:09,000
 the concept of samsara entirely.

171
00:13:09,000 --> 00:13:13,600
 This is what we mean by what we call samsara is just our

172
00:13:13,600 --> 00:13:20,000
 own delusions and our own conceptions of our experiences,

173
00:13:20,000 --> 00:13:23,530
 which turn out to be impermanent, unsatisfying, uncontroll

174
00:13:23,530 --> 00:13:30,000
able, not belonging to us,

175
00:13:30,000 --> 00:13:35,840
 but being amenable to our wishes, our expectations, and we

176
00:13:35,840 --> 00:13:41,270
 see that we're lost, that our ambitions and our drives and

177
00:13:41,270 --> 00:13:45,840
 our wants and our wishes are all merely a cause for more

178
00:13:45,840 --> 00:13:48,000
 stress and suffering.

179
00:13:48,000 --> 00:13:51,000
 And so we start to settle down.

180
00:13:51,000 --> 00:13:54,000
 We don't have to worry about leaving samsara.

181
00:13:54,000 --> 00:13:59,000
 Samsara just leaves you as you let go of it.

182
00:13:59,000 --> 00:14:04,000
 Samsara leaves you. That's the truth.

183
00:14:04,000 --> 00:14:10,150
 Become less and less inclined to pull it in, to take it in,

184
00:14:10,150 --> 00:14:12,000
 to build it up.

185
00:14:12,000 --> 00:14:19,770
 Let's and less inclined to create samsara around you until

186
00:14:19,770 --> 00:14:27,000
 eventually you stop, cease, and liberate yourself.

187
00:14:27,000 --> 00:14:30,790
 There you go. There's the dhamma for tonight. A little bit

188
00:14:30,790 --> 00:14:32,000
 about samsara.

189
00:14:32,000 --> 00:14:37,000
 Thank you all for tuning in. Have a good night.

