 To teach in a way that their students can apprehend.
 I mean, I guess I would even go so far as to say, if you
 want to be loved by your students,
 make sure you're cultivating mindfulness, you know,
 because you want to be present and you don't want to come
 across as egotistical,
 you don't want to have an agenda, you don't want to be
 cruel,
 and, you know, giving overly critical and overly harsh
 grades is not helping anybody.
 I had one professor, it seemed like she just wanted to
 teach me a lesson,
 because I was Buddhist, I think it was a religious study,
 it was really a ridiculous situation, but I felt that she
 was actively,
 anyway, she was very critical and many years ago, vind
ictive it felt like.
 But students do respond to the compassion and to kindness
 and to
 a professor who seems to be working for the students
 in the sense of really understanding that they're there to
 help the students learn.
 Not just to pad their egos, boost their egos, not just to
 make money.
 Again, Saranīya Dhamma, if you want harmony, you've got to
 work at it,
 and cultivate it in your students as well.
 Anyone who teaches anything is always in a position to
 impart values,
 impart their own personality to their students.
 So that's a heavy burden.
 Yeah, well I don't think you really have to respect
 teachers who are unkind or cruel.
 Not any more than you respect everyone, I mean, we should
 respect everyone to some extent.
 But there's a difference between respecting everyone and
 esteeming them.
 I wouldn't esteem someone who is cruel.
 I had an argument with this recently, I mean, I'm sort of
 of the school that if they're my teacher,
 I'm going to respect the position and respect them as a
 teacher
 in the sense of giving them the honor that they deserve,
 you know, the honor that comes.
 So I will hold them up in a technical position of
 superiority.
 I won't interrupt their class and I won't disrupt their
 class.
 I think that's important and useful.
