 Good evening everyone.
 Welcome to our evening dum.
 So I wanted to take tonight to express a sort of
 appreciation for everyone,
 for all of our meditation community, for all of you people
 out there interested in
 Buddhism, who have taken up study and practice of this
 incredible, wonderful, marvelous,
 miraculous set of teachings and practices.
 And just to remark upon how special this moment is, for all
 of us, how to remind us one and
 all of what it's taken for us to get here, and how special
 is this moment.
 It's like one of those, this is like one of those grand
 events, like the inauguration
 of a great monument or some once in a lifetime experience
 or event.
 The Buddha said, "Ganoma upachaga, don't let the moment
 pass you by."
 That's because this moment is special, and it's a moment in
 time that has a great significance
 to all of us, has great meaning and great potential, if we
 take advantage of it, if
 we take this opportunity, if we don't squander our time, if
 we don't allow ourselves to become
 distracted and diverted and caught up in the blinding power
 of defilement.
 We have great potential in this moment.
 And conventionally, before you even talk about the power of
 now, what it took for us to get
 here today is quite impressive.
 The first thing that's special about this moment is that it
's the time of the Buddha.
 This is a time when we have the teachings of a fully
 enlightened Buddha, which for those
 of you new to the study of Buddhism, it may not seem all
 that significant, but for those
 of us who have studied deeply or practiced deeply, the
 teachings of the Buddha, it has
 a profound significance.
 This is just some ordinary teaching, nagamadomo nigamasad
amo.
 This is not the teaching of one village or city or one
 civilization.
 This is the Dhamma of the whole world.
 This is the universal teachings of truth, and they're not
 always here.
 They're not always available.
 In fact, the vast majority of time spent in samsara is in
 situations where there is no
 Buddha.
 There are world periods without the arising of a Buddha.
 Just as there are places where no one has ever heard of the
 Buddha, even when the Buddha
 was alive, if you were born at that time, but you were born
 in Canada, or if you were
 born anywhere but India, chances are you'd never hear of
 let alone meet the Buddha.
 But we're born in a time when even Canada has heard of the
 Buddha's teaching, it has
 spread.
 In a sense, we're luckier as Canadians to be born today
 than to have been born 2,500
 years ago.
 Canada wouldn't have been a good place to learn Buddhism.
 Not that there weren't civilizations here, it's just they
 weren't Buddhist.
 It takes a Buddha, it's not something that comes up in a
 few days, and it's not something
 you get every year.
 There's a new Buddha coming out, every generation has its
 own Buddha, it's not like that.
 Our Buddha took four asankaya, four uncountable eons it
 took him, plus another hundred thousand
 countable eons, great eons.
 An asankaya is something you can't count, it's this measure
 that ironically you can count
 how many there are.
 Somehow you can count that there are four asankaya, but you
 can't count how long an
 asankaya is.
 So it must be marked by something, at the end of an asank
aya something changes.
 I'm not up on all this, I don't quite know.
 And it's a huge unfathomable amount of time.
 And we had to do all sorts of powerful acts and sacrifices,
 giving up his eyes, giving
 up his life, renouncing the world, renouncing his family,
 being killed and tortured, walking
 through fire to become a Buddha.
 And so here we have in the time of a very special
 individual whose teachings have come
 through the veil of darkness that has kept us confused and
 unseen.
 It's a very rare opportunity, that's the first aspect, the
 first reason why this is
 a special moment.
 The second reason is that well, not only are we born in the
 time of the Buddha, but we're
 also born as humans.
 So there are lots of beings around that will never
 understand the Buddha's teachings.
 We could have been born quite easily as a deer or a cat, a
 dog, a mosquito, a worm.
 Could have been born as many different things.
 We could have been born as a Brahma and never met with a
 Buddha because we were lost in
 the Brahma realms or never had the opportunity to become
 enlightened.
 Being born as a human or as an angel is a very rare thing.
 To just rise up out of the ordinary animal realm is a very
 rare thing.
 Right now we think it's quite common because there are
 seven or eight billion of us.
 We could argue that that's due to the goodness of teachings
 like the Buddha that's allowed
 us to prosper, but even seven or eight billion isn't all
 that much.
 You compare it to the number of animals, the number of ants
 and mosquitoes, and the number
 of other beings, and the beings in hell that are probably
 countless.
 The ability to be born as a human being is very rare.
 We've achieved it.
 We've made it out of the depths of despair and suffering of
 the animal realms, the lower
 realm.
 Third, we are in a position to practice the Buddha's
 teachings.
 Many people who are caught up, there's lots of people who
 even want to practice Buddhism
 but don't have the opportunity.
 They're caught up in their lives.
 Perhaps they're sick or in debt.
 Maybe they're married and have children and are in a
 position with young children where
 they can't come to practice meditation.
 Maybe they have a job that doesn't allow them to get away.
 The life of human beings is a difficult life.
 I know of people, I've talked to people who are in a place
 where there is no Buddhism
 and there's no chance to go to a Buddhist monastery or to
 learn meditation.
 It's great now that we have these online courses, we have
 people coming and doing at least a
 non-intensive practice, enough to give them courage and
 give them hope and tide them over
 while they cultivate the necessary conditions to actually
 come and do a course.
 We've had several people who have done that and eventually
 found a way to come and finish
 the course here.
 The third reason why this is special is we've all come
 together, most special for our resident
 meditators who have done this incredible thing to take time
 out of an ordinarily busy life
 and dedicate 20, 30 days straight to just training the mind
, the pure unadulterated
 goodness.
 The third reason is that we've got the opportunity and the
 fourth reason is that we've actually
 taken the opportunity.
 This is what it means by not letting the moment pass you by
.
 The Buddha said there are many people, there are a few
 people in this world who even think
 to do things like meditation, who are moved by things that
 they should be moved by.
 Most people aren't even moved when they think of death,
 when they think of sickness.
 They can't understand why we don't just engage and indulge
 in sensual pleasures.
 They can't understand why one would come here and do a
 meditation course, would take time
 out of their lives or try to change their lives, strive to
 free themselves from craving
 when there's so much pleasure to be had out there.
 They hear about things like old age sickness, death, they
 look at the anger and the greed
 and the delusion that they're building inside and they aren
't threatened by it.
 They don't feel disturbed by it.
 There are people like that.
 If they feel disturbed, they ignore it.
 They don't allow themselves to become moved.
 Very few are the people who actually discern this
 precarious state for what it is, that
 at any moment things could change and we could be dumped
 headlong into suffering for a variety
 of reasons and in fact will be as life changes.
 Very few people actually see the suffering for what it is,
 see how much stress is involved
 in chasing after the objects of our desires.
 People see this but among those people who see these
 problematic aspects of ordinary
 life, very few are those who actually do something about it
.
 So it's worth feeling good about, taking time to reflect
 how far we've come and be encouraged
 by how special this is and how powerful it is.
 So the real point is that we've come to a position where we
 can take advantage of all
 these supportive, good luck that we've had, all these good
 conditions, favorable conditions
 and make this moment into something special.
 That all these supportive conditions come together to allow
 us, all of them are required
 just for this one simple act of seeing things clearly, vip
assana.
 All these things that are there for everyone else but that
 they never see, right under
 our noses, right here in front of us but that we never see
 clearly.
 They're all there but we can't see them, we don't have the
 supportive conditions.
 Well now we've come together and we have the supportive
 condition.
 We have the opportunity to see body as body, feelings as
 feelings, thoughts as thoughts,
 emotions as emotions, senses as senses.
 To see impermanence, to see suffering, to see non-self, to
 free ourselves from craving
 and clinging to things that can't possibly satisfy us, to
 strive for liberation and a
 state of mind that is unsullied by the ocean or the defile
ment of samsara.
 So it's quite a special opportunity that we have is what I
'm trying to say.
 I like to take this tonight to do a sort of appreciation
 and to offer this as a sort of
 dhamma.
 That the most important thing is the moment.
 Don't let the moment pass you by.
 There's a lot that's gone into this moment.
 This moment in time.
 So care for it.
 Care for it.
 I think in the Missudimaga it talks about a person who is
 rocking a baby and in the olden
 times they would have a baby cradle on a string.
 Maybe even nowadays they do this as well.
 So you have to pull the string and you have to be very
 careful to keep the string going.
 Or the baby will wake up.
 Our practice has to be cared for and carefully tended to
 like a sleeping baby to keep it
 on kilter.
 To keep it online and in line and progressing smoothly.
 To constantly be cautious and careful like a sick person
 looking after their pain in
 their body.
 Not that we have to feel sick or it should be sickening or
 anything.
 But we should think of ourselves as someone who has to take
 care of something because
 the potential for suffering.
 So a sick person, if they're not careful with how they move
 their body they'll feel great
 pain.
 Likewise a meditator in moving their body and speaking and
 everything they do and even
 in their thinking.
 You have to be careful to not go off track or they'll lose
 their equilibrium.
 They'll lose their direction.
 Treat this moment with care.
 It's very valuable.
 Priceless.
 So there you go.
 There's the bit of dhamma for tonight.
 Thank you all for coming out.
 I'm happy to take questions if there are any.
 Thank you.
 Thank you.
 The reason one cannot be enlightened as a brahma in the bra
hma realms is that because
 there one cannot take the force at Deepatthana.
 Why are you planning on going to the brahma realms?
 As I understand, yes some of the brahma realms one cannot
 become enlightened.
 I'm not quite clear why.
 But I think Mahasya Sayadaw says it's something about how
 they aren't able to see impermanence
 because it's too stable.
 You have to know curious, curious, wondering, wondering.
 That wasn't the point of my talk.
 Such a terrible teacher, no?
 Won't answer your questions.
 I did kinda.
 Yeah, right.
 It might just be the Arupa Brahma realms that you can't
 practice.
 Because there are certainly brahma realms where you can
 practice, the Anagami realms.
 The Arupa Brahma realms are like real God states where you
're totally out of it, totally
 entranced with no form, only mind.
 Think.
 All right, well if there are no more questions, we'll go
 back to the
 next one.
 Thanks.
