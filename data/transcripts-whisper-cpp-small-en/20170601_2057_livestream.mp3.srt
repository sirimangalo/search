1
00:00:00,000 --> 00:00:09,960
 Okay, good evening everyone.

2
00:00:09,960 --> 00:00:21,720
 Welcome to our evening Dhamma session.

3
00:00:21,720 --> 00:00:31,720
 The topic of tonight's talk is failure.

4
00:00:31,720 --> 00:00:36,720
 It's a word we're all afraid of, right?

5
00:00:36,720 --> 00:00:40,720
 Afraid of being a failure.

6
00:00:40,720 --> 00:00:51,720
 Ever felt like a failure in general?

7
00:00:51,720 --> 00:00:57,720
 We place our hopes and our dreams on success,

8
00:00:57,720 --> 00:01:00,720
 as do other people.

9
00:01:00,720 --> 00:01:06,720
 Place their hopes and dreams on our success, our parents.

10
00:01:06,720 --> 00:01:13,180
 Sometimes some people have intense pressure put on them by

11
00:01:13,180 --> 00:01:16,720
 their parents to succeed.

12
00:01:16,720 --> 00:01:23,720
 And if they don't,

13
00:01:23,720 --> 00:01:28,720
 they're made to feel like a failure.

14
00:01:28,720 --> 00:01:32,720
 Sometimes society puts pressures on us

15
00:01:32,720 --> 00:01:40,720
 to succeed our neighbors, our friends.

16
00:01:40,720 --> 00:01:48,720
 So maybe everyone else is succeeding and boasting or

17
00:01:48,720 --> 00:01:51,720
 proud of their achievements,

18
00:01:51,720 --> 00:01:58,720
 and we feel like a failure because we've achieved nothing.

19
00:01:58,720 --> 00:02:06,720
 It's a mental illness of sorts, this need to succeed and

20
00:02:06,720 --> 00:02:14,720
 this attachment to failure or feeling like a failure.

21
00:02:14,720 --> 00:02:18,710
 Because of course in Buddhism there's not really such a

22
00:02:18,710 --> 00:02:23,720
 thing as failure.

23
00:02:23,720 --> 00:02:31,720
 Or maybe more that we're always failing.

24
00:02:31,720 --> 00:02:34,720
 We fail until we succeed.

