1
00:00:00,000 --> 00:00:18,560
 I'm broadcasting live March 9th.

2
00:00:18,560 --> 00:00:41,120
 Today we have a quote about the body.

3
00:00:41,120 --> 00:00:44,120
 Bodily purity.

4
00:00:44,120 --> 00:00:50,280
 How do you clean the body?

5
00:00:50,280 --> 00:00:51,280
 How do you clean the body?

6
00:00:51,280 --> 00:01:01,400
 It's a loaded question from a Buddhist point of view.

7
00:01:01,400 --> 00:01:06,870
 We were talking on Monday about, we've just been reading

8
00:01:06,870 --> 00:01:09,240
 chapters 20 to 25 of the Lotus

9
00:01:09,240 --> 00:01:12,080
 Sutra.

10
00:01:12,080 --> 00:01:26,710
 And in one of those chapters there's a monk or I think a

11
00:01:26,710 --> 00:01:34,160
 monk who, a man, somebody who

12
00:01:34,160 --> 00:01:43,630
 lights their body on fire as an offering to the Buddha,

13
00:01:43,630 --> 00:01:49,480
 burns themselves completely.

14
00:01:49,480 --> 00:01:56,110
 And then there's another person who burns their forearm,

15
00:01:56,110 --> 00:02:04,120
 burns off their forearm completely.

16
00:02:04,120 --> 00:02:08,040
 And then makes some kind of a vow and they come back.

17
00:02:08,040 --> 00:02:11,740
 I mean, honestly I have less and less good to say about

18
00:02:11,740 --> 00:02:13,920
 this Lotus Sutra the more I read

19
00:02:13,920 --> 00:02:16,920
 it.

20
00:02:16,920 --> 00:02:22,890
 But got an interesting conversation going about the nature

21
00:02:22,890 --> 00:02:25,320
 of the body according to

22
00:02:25,320 --> 00:02:34,510
 Buddhism because a common thought or train of thought for

23
00:02:34,510 --> 00:02:39,720
 spiritual people, especially

24
00:02:39,720 --> 00:02:44,000
 in the West, is that the body is somehow sacred.

25
00:02:44,000 --> 00:02:55,150
 It should work to purify the body, you should guard the

26
00:02:55,150 --> 00:02:59,120
 body as a vessel.

27
00:02:59,120 --> 00:03:03,080
 And so the idea that spiritual beings would use part of

28
00:03:03,080 --> 00:03:04,960
 their body as an offering, I mean

29
00:03:04,960 --> 00:03:10,920
 it's actually common in Mahayana even today to offer like a

30
00:03:10,920 --> 00:03:12,120
 finger.

31
00:03:12,120 --> 00:03:15,270
 You just stick a finger into a candle and burn it up and it

32
00:03:15,270 --> 00:03:19,400
's an offering to the Buddha.

33
00:03:19,400 --> 00:03:21,680
 So we're arguing whether it is an offering because you're

34
00:03:21,680 --> 00:03:22,960
 not actually giving anything

35
00:03:22,960 --> 00:03:26,720
 to anybody, nobody's benefiting from your burning of a

36
00:03:26,720 --> 00:03:30,800
 finger or whether it's just a

37
00:03:30,800 --> 00:03:39,510
 measure of your determination, devotion, it's a measure of

38
00:03:39,510 --> 00:03:40,260
 devotion.

39
00:03:40,260 --> 00:03:46,180
 But the idea that you should do this, whether or not Buddh

40
00:03:46,180 --> 00:03:48,920
ists like me would agree with

41
00:03:48,920 --> 00:03:56,790
 doing that, we at least can agree that using your body in

42
00:03:56,790 --> 00:04:01,440
 some way to benefit someone else

43
00:04:01,440 --> 00:04:06,530
 or using your body in any way really is a lot like using

44
00:04:06,530 --> 00:04:08,340
 any other tool.

45
00:04:08,340 --> 00:04:11,160
 We don't see the body as somehow special.

46
00:04:11,160 --> 00:04:15,900
 I mean it's categorically different from other things

47
00:04:15,900 --> 00:04:19,200
 because there's experiences surrounding

48
00:04:19,200 --> 00:04:28,120
 it, direct experience, experience of pain and pleasure.

49
00:04:28,120 --> 00:04:34,000
 But as an entity, the body is something you can give away,

50
00:04:34,000 --> 00:04:37,560
 organ donation, blood donation,

51
00:04:37,560 --> 00:04:41,290
 these sorts of things I think are very much in line with

52
00:04:41,290 --> 00:04:42,280
 Buddhism.

53
00:04:42,280 --> 00:04:47,690
 We talked about this in Thailand, how it's giving blood, it

54
00:04:47,690 --> 00:04:49,480
's a great thing.

55
00:04:49,480 --> 00:04:52,300
 All the monks and nuns I went with them, we went together

56
00:04:52,300 --> 00:04:53,800
 to the hospital to give blood

57
00:04:53,800 --> 00:05:04,320
 because we consider that to be a great thing that we could

58
00:05:04,320 --> 00:05:05,480
 do.

59
00:05:05,480 --> 00:05:08,840
 It's right or wrong for monks and nuns to give blood, I'm

60
00:05:08,840 --> 00:05:13,320
 not sure, but we all did it.

61
00:05:13,320 --> 00:05:19,130
 But the idea is that the body is just another thing and

62
00:05:19,130 --> 00:05:23,320
 more to the point, it's inherently

63
00:05:23,320 --> 00:05:26,280
 impure.

64
00:05:26,280 --> 00:05:27,960
 There's nothing pure about the body.

65
00:05:27,960 --> 00:05:32,660
 If you want to get technical, it's made up of the four

66
00:05:32,660 --> 00:05:35,640
 elements and so there's nothing

67
00:05:35,640 --> 00:05:40,000
 disgusting about it technically.

68
00:05:40,000 --> 00:05:50,180
 But that being said, it's made up of all the numbers of

69
00:05:50,180 --> 00:05:53,680
 things that are, at the very least,

70
00:05:53,680 --> 00:06:00,560
 without the ability to provide satisfaction, but that in

71
00:06:00,560 --> 00:06:04,040
 general are considered repulsive.

72
00:06:04,040 --> 00:06:08,270
 You're going to clean out the body when you have to get rid

73
00:06:08,270 --> 00:06:10,840
 of all the blood and pus and

74
00:06:10,840 --> 00:06:17,720
 snot and bile and urine and feces.

75
00:06:17,720 --> 00:06:28,880
 In a conventional sense, the body is terribly impure.

76
00:06:28,880 --> 00:06:33,570
 But in another sense, and that's in the sense that the

77
00:06:33,570 --> 00:06:37,140
 Buddha is talking here, it is possible

78
00:06:37,140 --> 00:06:39,560
 to purify the body.

79
00:06:39,560 --> 00:06:42,360
 It's totally not what most people would think.

80
00:06:42,360 --> 00:06:46,690
 It's a completely different way of looking at purity and

81
00:06:46,690 --> 00:06:48,520
 looking at the body.

82
00:06:48,520 --> 00:06:51,200
 So how do you purify the body?

83
00:06:51,200 --> 00:06:53,480
 Stop killing.

84
00:06:53,480 --> 00:06:56,480
 Right?

85
00:06:56,480 --> 00:07:04,620
 Purity and impurity have nothing to do with the nature of

86
00:07:04,620 --> 00:07:06,760
 the thing.

87
00:07:06,760 --> 00:07:11,610
 Purity and impurity in regards to the physical realm have

88
00:07:11,610 --> 00:07:14,440
 everything to do with the use.

89
00:07:14,440 --> 00:07:18,560
 Don't purify the body.

90
00:07:18,560 --> 00:07:20,160
 Don't use it to kill.

91
00:07:20,160 --> 00:07:23,680
 Don't use it to steal.

92
00:07:23,680 --> 00:07:31,500
 Don't use it to commit sexual misconduct, to commit

93
00:07:31,500 --> 00:07:33,520
 adultery.

94
00:07:33,520 --> 00:07:38,560
 To commit immoral acts of body.

95
00:07:38,560 --> 00:07:44,120
 Purity for the material realm is in the use you give.

96
00:07:44,120 --> 00:07:47,060
 So this goes not only for the body, but it goes for all

97
00:07:47,060 --> 00:07:48,480
 other things we use.

98
00:07:48,480 --> 00:07:51,880
 The body is in this way not categorically different.

99
00:07:51,880 --> 00:07:55,000
 It's just another thing that we use.

100
00:07:55,000 --> 00:08:00,060
 If you want to talk about purity of your possessions and

101
00:08:00,060 --> 00:08:01,840
 purity of robes.

102
00:08:01,840 --> 00:08:03,800
 Are my robes pure?

103
00:08:03,800 --> 00:08:08,870
 They're 100% cotton as far as I know, but that's not what

104
00:08:08,870 --> 00:08:10,520
 makes them pure.

105
00:08:10,520 --> 00:08:19,520
 They're pure if I use them for pure purposes.

106
00:08:19,520 --> 00:08:24,680
 If I use them for play or for fun or if I enjoy them or if

107
00:08:24,680 --> 00:08:27,280
 I find robes that are only

108
00:08:27,280 --> 00:08:31,140
 robes that are beautiful or so I pick my robes based on how

109
00:08:31,140 --> 00:08:33,400
 beautiful and how comfortable

110
00:08:33,400 --> 00:08:41,660
 and how pleasing they are, then that's impure no matter

111
00:08:41,660 --> 00:08:45,680
 what they're made of.

112
00:08:45,680 --> 00:08:50,510
 If I use my food, the food that I get, if I use it for

113
00:08:50,510 --> 00:08:53,560
 enjoyment or for intoxication

114
00:08:53,560 --> 00:09:02,280
 or for fattening up, I use it for excess.

115
00:09:02,280 --> 00:09:04,280
 It's impure food.

116
00:09:04,280 --> 00:09:05,800
 That's what makes food impure.

117
00:09:05,800 --> 00:09:09,680
 It's not because it's got this ingredient or that

118
00:09:09,680 --> 00:09:10,960
 ingredient.

119
00:09:10,960 --> 00:09:15,880
 A shelter is pure because of how you use it.

120
00:09:15,880 --> 00:09:20,220
 To use it to hide your evil deeds or do you use it for se

121
00:09:20,220 --> 00:09:25,080
clusion to cultivate meditation?

122
00:09:25,080 --> 00:09:30,490
 In medicine, do you use it to heal the body or do you use

123
00:09:30,490 --> 00:09:34,480
 it to become addicted to painkillers

124
00:09:34,480 --> 00:09:45,120
 or to avoid the problems, to avoid suffering?

125
00:09:45,120 --> 00:09:49,540
 And by extension everything else that we use, including the

126
00:09:49,540 --> 00:09:50,200
 body.

127
00:09:50,200 --> 00:09:53,360
 So what are you using the body for?

128
00:09:53,360 --> 00:09:58,240
 You have this body as a tool or you're using it to do evil

129
00:09:58,240 --> 00:10:01,040
 things or are you using it to

130
00:10:01,040 --> 00:10:02,040
 gain purity?

131
00:10:02,040 --> 00:10:09,840
 Are you using it as a tool, as a vehicle?

132
00:10:09,840 --> 00:10:15,760
 Doing walking meditation, doing sitting meditation?

133
00:10:15,760 --> 00:10:18,800
 Are you using it for the right purposes?

134
00:10:18,800 --> 00:10:21,080
 Then it's pure.

135
00:10:21,080 --> 00:10:26,680
 Then the body is pure.

136
00:10:26,680 --> 00:10:32,680
 So simple dhamma for tonight.

137
00:10:32,680 --> 00:10:40,920
 I'm sorry I haven't been all that.

138
00:10:40,920 --> 00:10:43,640
 I really want to do these sessions.

139
00:10:43,640 --> 00:10:48,390
 Do them often and try to get back into the video but this

140
00:10:48,390 --> 00:10:50,880
 month is going to be really,

141
00:10:50,880 --> 00:10:51,880
 really busy.

142
00:10:51,880 --> 00:10:56,430
 I made a mistake of taking two upper year courses even

143
00:10:56,430 --> 00:10:58,160
 though I'm only taking three

144
00:10:58,160 --> 00:10:59,160
 courses.

145
00:10:59,160 --> 00:11:08,470
 Two of them are, I've got two essays, two papers to write

146
00:11:08,470 --> 00:11:09,840
 and the peace symposium we're

147
00:11:09,840 --> 00:11:13,560
 organizing.

148
00:11:13,560 --> 00:11:20,020
 Yeah, so hopefully I'll still have time to do all this but

149
00:11:20,020 --> 00:11:22,800
 the next week is going to

150
00:11:22,800 --> 00:11:23,800
 be pretty harsh.

151
00:11:23,800 --> 00:11:29,380
 I'm going to have to buckle down and do some work,

152
00:11:29,380 --> 00:11:32,680
 especially because I missed a week.

153
00:11:32,680 --> 00:11:36,760
 Anyway that's none of your concern.

154
00:11:36,760 --> 00:11:42,840
 I'll try and be here as much as possible.

155
00:11:42,840 --> 00:11:46,360
 Maybe we'll do some more dhammapada but we'll see.

156
00:11:46,360 --> 00:11:49,480
 See how it goes.

157
00:11:49,480 --> 00:11:52,080
 Anyway, thank you all for tuning in.

158
00:11:52,080 --> 00:11:56,560
 Oh wait, right next part.

159
00:11:56,560 --> 00:12:01,730
 I'm going to post the hangout so anybody who wants to come

160
00:12:01,730 --> 00:12:05,120
 on the hangout can come on.

161
00:12:05,120 --> 00:12:09,160
 We've got news tonight, Robin just finished her course.

162
00:12:09,160 --> 00:12:10,160
 She's here.

163
00:12:10,160 --> 00:12:15,040
 Many of you know Robin, so she's Robin.

164
00:12:15,040 --> 00:12:17,040
 We let her out of her room today.

165
00:12:17,040 --> 00:12:20,520
 Doesn't she look happy?

166
00:12:20,520 --> 00:12:26,320
 She's still meditating so I don't want to bother her.

167
00:12:26,320 --> 00:12:33,840
 Maybe we'll have a chance to talk before she leaves.

168
00:12:33,840 --> 00:12:37,640
 So come on if you have questions, come on the hangout.

169
00:12:37,640 --> 00:12:42,640
 I just posted it at meditation.siri-mungalode.org.

170
00:12:42,640 --> 00:12:48,640
 We've got 10 viewers on YouTube, small group.

171
00:12:48,640 --> 00:12:51,960
 A lot of meditators which is nice.

172
00:12:51,960 --> 00:12:56,160
 We've got a long list of meditators tonight.

173
00:12:56,160 --> 00:13:10,360
 All right, well if no one's coming on I'm going to go.

174
00:13:10,360 --> 00:13:12,280
 I'll be back tomorrow.

175
00:13:12,280 --> 00:13:16,280
 Have a good night everybody.

176
00:13:16,280 --> 00:13:18,000
 Bye.

177
00:13:18,000 --> 00:13:23,360
 [ Silence ]

178
00:13:23,360 --> 00:13:25,360
 [ Silence ]

