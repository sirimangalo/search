1
00:00:00,000 --> 00:00:03,200
 OK, good evening, everyone.

2
00:00:03,200 --> 00:00:07,120
 Welcome to our live broadcast, November 24.

3
00:00:07,120 --> 00:00:14,280
 So this daily thing is working out well.

4
00:00:14,280 --> 00:00:21,080
 Today is-- excuse me, the shaving day in Thailand.

5
00:00:21,080 --> 00:00:25,800
 Tomorrow's the full moon, I think.

6
00:00:25,800 --> 00:00:27,320
 Which means-- or maybe today.

7
00:00:27,320 --> 00:00:31,480
 It depends on which-- there's two traditions in Thailand.

8
00:00:31,480 --> 00:00:35,480
 One strict and one not so strict.

9
00:00:35,480 --> 00:00:38,600
 One strict and one traditional, I think you could say.

10
00:00:38,600 --> 00:00:43,760
 Because technically, sometimes the moon is full after

11
00:00:43,760 --> 00:00:44,320
 midnight.

12
00:00:44,320 --> 00:00:46,880
 Sometimes it's full before midnight.

13
00:00:46,880 --> 00:00:52,880
 But the traditional group goes more by calculations

14
00:00:52,880 --> 00:00:55,440
 than by exact hours.

15
00:00:55,440 --> 00:01:01,040
 So it's almost two ways of calculating.

16
00:01:01,040 --> 00:01:03,200
 I don't know which one I'm saying tomorrow is.

17
00:01:03,200 --> 00:01:05,440
 But many of the full moons are still the same.

18
00:01:05,440 --> 00:01:10,560
 Shaving today.

19
00:01:10,560 --> 00:01:17,680
 Shaving once a month.

20
00:01:17,680 --> 00:01:19,200
 It's a big thing in Thailand.

21
00:01:19,200 --> 00:01:21,080
 Some monks do shave more regularly.

22
00:01:21,080 --> 00:01:24,280
 In Sri Lanka, they shave seemingly every couple days

23
00:01:24,280 --> 00:01:25,080
 of every week.

24
00:01:25,080 --> 00:01:30,800
 It's arguable which one's better.

25
00:01:30,800 --> 00:01:34,160
 If you shave once a week, or once every few days,

26
00:01:34,160 --> 00:01:35,160
 you can shave everything.

27
00:01:35,160 --> 00:01:38,600
 Shave your beard and your head all at once.

28
00:01:38,600 --> 00:01:40,000
 And then it never gets so long.

29
00:01:40,000 --> 00:01:43,000
 It's a bit of a drag because every month--

30
00:01:43,000 --> 00:01:48,120
 shaving once a month, and it's a lot of hair to shave.

31
00:01:48,120 --> 00:01:50,480
 Seems like less fuss to shave once a month.

32
00:01:52,320 --> 00:01:56,920
 I'll probably get the inevitable question, why do monks

33
00:01:56,920 --> 00:01:58,320
 shave?

34
00:01:58,320 --> 00:01:59,240
 I think it's cleaner.

35
00:01:59,240 --> 00:02:04,320
 It's less fuss.

36
00:02:04,320 --> 00:02:07,240
 There's no ego attachment to the hair and hairstyle

37
00:02:07,240 --> 00:02:11,450
 because monks were styling their hair at the beginning,

38
00:02:11,450 --> 00:02:11,920
 sort of.

39
00:02:11,920 --> 00:02:14,560
 And that was why they would have instated the rule not

40
00:02:14,560 --> 00:02:16,920
 to let it get longer.

41
00:02:16,920 --> 00:02:20,400
 We can get this long, two fingers long.

42
00:02:20,400 --> 00:02:26,360
 Or two months.

43
00:02:26,360 --> 00:02:29,480
 If you haven't shaved in two months, you have to shave.

44
00:02:29,480 --> 00:02:35,360
 If it gets longer than two fingers, then you have to shave.

45
00:02:35,360 --> 00:02:37,760
 So that also apparently goes for the beard,

46
00:02:37,760 --> 00:02:39,960
 or technically goes for the beard.

47
00:02:39,960 --> 00:02:41,800
 Some monks were testing that.

48
00:02:41,800 --> 00:02:45,160
 There are monks who let their beard grow out.

49
00:02:45,160 --> 00:02:48,440
 But still two months, you have to shave your beard.

50
00:02:48,440 --> 00:02:50,840
 That's quite a lot of beard, though, after two months.

51
00:02:50,840 --> 00:02:53,880
 That's quite a lot of beard.

52
00:02:53,880 --> 00:02:56,040
 Mostly we don't go by that one.

53
00:02:56,040 --> 00:02:59,120
 If I go by that one in Thailand, you

54
00:02:59,120 --> 00:03:01,840
 have a real thing against facial hair in Thailand

55
00:03:01,840 --> 00:03:06,520
 because most Thai people have very little facial hair.

56
00:03:06,520 --> 00:03:11,240
 But still, you're a rogue if you have facial hair.

57
00:03:11,240 --> 00:03:14,640
 Big on appearances, fortunately.

58
00:03:14,640 --> 00:03:23,120
 [AUDIO OUT]

59
00:03:23,120 --> 00:03:25,640
 So good evening.

60
00:03:25,640 --> 00:03:28,520
 Today I taught two people how to meditate, or one person,

61
00:03:28,520 --> 00:03:29,440
 I guess.

62
00:03:29,440 --> 00:03:34,000
 This friend I'm talking about, my university friend,

63
00:03:34,000 --> 00:03:37,360
 got one of her friends to come and learn

64
00:03:37,360 --> 00:03:39,720
 how to meditate, who also is struggling.

65
00:03:39,720 --> 00:03:42,280
 She suffers from many of the same issues that she does.

66
00:03:44,040 --> 00:03:45,040
 Wow.

67
00:03:45,040 --> 00:03:46,560
 That was a very good meditating.

68
00:03:46,560 --> 00:03:49,320
 This new person who's picked up like nothing.

69
00:03:49,320 --> 00:03:53,580
 Some people, most people, most of us, and I include myself

70
00:03:53,580 --> 00:03:55,680
 in this, aren't able to pick it up very quickly.

71
00:03:55,680 --> 00:03:58,720
 But the odd person gets it really quickly,

72
00:03:58,720 --> 00:04:01,920
 like her synchronicity.

73
00:04:01,920 --> 00:04:02,760
 It's a simple thing.

74
00:04:02,760 --> 00:04:04,800
 You can tell when they do the walking,

75
00:04:04,800 --> 00:04:08,360
 she's able to say step, step.

76
00:04:08,360 --> 00:04:11,240
 So she's in time with-- that's rare, actually.

77
00:04:11,240 --> 00:04:14,100
 For someone who hasn't done it, it's actually a bit of a

78
00:04:14,100 --> 00:04:15,160
 skill.

79
00:04:15,160 --> 00:04:19,920
 But I was impressed by just that simple.

80
00:04:19,920 --> 00:04:21,880
 And then we did-- she was sat very still

81
00:04:21,880 --> 00:04:24,520
 when I led her through the meditation,

82
00:04:24,520 --> 00:04:28,200
 and afterwards she was able to do it.

83
00:04:28,200 --> 00:04:30,120
 And you go one person at a time.

84
00:04:30,120 --> 00:04:37,480
 And I like to see if she keeps it up,

85
00:04:37,480 --> 00:04:46,800
 to bring her a book about this charity thing

86
00:04:46,800 --> 00:04:49,960
 that we're looking at.

87
00:04:49,960 --> 00:05:10,440
 [INAUDIBLE]

88
00:05:10,440 --> 00:05:36,920
 [INAUDIBLE]

89
00:05:36,920 --> 00:05:39,440
 I was really thinking about helping humans.

90
00:05:39,440 --> 00:05:39,940
 OK.

91
00:05:39,940 --> 00:05:47,640
 Yeah, I guess humans are important too.

92
00:05:47,640 --> 00:05:55,040
 They have to look after the dogs after all.

93
00:05:55,040 --> 00:05:57,040
 Yes.

94
00:05:57,040 --> 00:05:57,600
 And the cats.

95
00:05:57,600 --> 00:06:03,080
 You have to praise the cats.

96
00:06:03,080 --> 00:06:06,280
 You have to slave over the cats.

97
00:06:06,280 --> 00:06:08,520
 It's their role in life.

98
00:06:08,520 --> 00:06:22,280
 So what are you thinking to do for the children's home?

99
00:06:22,280 --> 00:06:25,160
 Shall we do an online campaign?

100
00:06:25,160 --> 00:06:28,400
 Or what were you thinking?

101
00:06:28,400 --> 00:06:30,120
 I don't know if I can say yes to that.

102
00:06:35,960 --> 00:06:36,560
 I guess so.

103
00:06:36,560 --> 00:06:41,840
 I guess I can say that's sort of a neat idea, because--

104
00:06:41,840 --> 00:06:45,600
 but I just can't talk about money.

105
00:06:45,600 --> 00:06:47,080
 Sure.

106
00:06:47,080 --> 00:06:51,080
 Can you tell me the name of the organization?

107
00:06:51,080 --> 00:06:51,880
 So we can all--

108
00:06:51,880 --> 00:06:55,400
 I guess I decided on one, but I guess it's--

109
00:06:55,400 --> 00:06:58,230
 Sundamali, I guess we can just decide on this children's

110
00:06:58,230 --> 00:07:00,000
 home.

111
00:07:00,000 --> 00:07:03,120
 Well, Sundamali is watching.

112
00:07:03,120 --> 00:07:04,840
 She watches this.

113
00:07:04,840 --> 00:07:06,080
 She did watch last night.

114
00:07:06,080 --> 00:07:09,840
 Childrenshome.org.

115
00:07:09,840 --> 00:07:15,920
 Childrenshome.org.

116
00:07:15,920 --> 00:07:17,520
 They also have something over the holidays.

117
00:07:17,520 --> 00:07:20,280
 It's a nice website.

118
00:07:20,280 --> 00:07:23,960
 It's 1892.

119
00:07:23,960 --> 00:07:24,560
 We're in Tampa.

120
00:07:24,560 --> 00:07:30,280
 We could just go for a tour.

121
00:07:30,280 --> 00:07:33,720
 I could give a gift, and they could go for a tour.

122
00:07:33,720 --> 00:07:35,240
 VIP tours.

123
00:07:35,240 --> 00:07:37,200
 So what's a VIP tour?

124
00:07:37,200 --> 00:07:39,200
 You have to give a certain amount to become a VIP.

125
00:07:39,200 --> 00:08:05,040
 [AUDIO OUT]

126
00:08:05,040 --> 00:08:07,700
 Looks like they're having a big event on Saturday, December

127
00:08:07,700 --> 00:08:08,120
 12,

128
00:08:08,120 --> 00:08:10,160
 but you'll be there a little after that.

129
00:08:10,160 --> 00:08:15,480
 Yeah, that's a bit early.

130
00:08:15,480 --> 00:08:22,320
 Well, we could give, but that's an open house.

131
00:08:22,320 --> 00:08:25,400
 Make a donation from our list of most needed items.

132
00:08:25,400 --> 00:08:33,000
 So they have a wish list of some sort.

133
00:08:33,000 --> 00:08:35,280
 There's a way to find out how to become a VIP.

134
00:08:35,280 --> 00:08:37,720
 What's the criteria?

135
00:08:37,720 --> 00:08:40,080
 Then we'd have a goal, right?

136
00:08:40,080 --> 00:08:40,560
 Yeah.

137
00:08:40,560 --> 00:08:44,360
 How you can help.

138
00:08:44,360 --> 00:08:55,360
 Financial donation.

139
00:08:55,360 --> 00:09:00,480
 We're going to make a gift in honor of someone you care

140
00:09:00,480 --> 00:09:02,200
 about.

141
00:09:02,200 --> 00:09:03,320
 Become part of a surrogate.

142
00:09:03,320 --> 00:09:16,680
 [AUDIO OUT]

143
00:09:16,680 --> 00:09:20,160
 The benefactor is $1,000 a year for five years.

144
00:09:20,160 --> 00:09:26,120
 Let's see if we can probably--

145
00:09:26,120 --> 00:09:40,360
 [AUDIO OUT]

146
00:09:40,360 --> 00:09:44,920
 Well, they have a list of their most urgent needs.

147
00:09:44,920 --> 00:09:51,360
 So we could ask people if they'd like to send these items.

148
00:09:51,360 --> 00:09:52,880
 Although I don't think you'd probably

149
00:09:52,880 --> 00:09:54,480
 want to have to carry them down there.

150
00:09:54,480 --> 00:09:56,640
 It would be better if we could send them

151
00:09:56,640 --> 00:10:00,920
 to someone in the area.

152
00:10:00,920 --> 00:10:02,600
 But on their most urgent needs, they're

153
00:10:02,600 --> 00:10:06,680
 looking for women's and men's hoodies

154
00:10:06,680 --> 00:10:10,320
 and personal care items like body wash and hair products

155
00:10:10,320 --> 00:10:12,840
 and things, batteries.

156
00:10:22,360 --> 00:10:25,200
 Oh, and they have a much longer list of ongoing needs,

157
00:10:25,200 --> 00:10:29,090
 just all sorts of personal care items and recreational

158
00:10:29,090 --> 00:10:29,800
 items.

159
00:10:29,800 --> 00:10:39,400
 So we could either look for donations of items

160
00:10:39,400 --> 00:10:47,720
 or do an online campaign or donate gift cards.

161
00:10:47,720 --> 00:10:48,960
 We've got a lot of options here.

162
00:10:50,960 --> 00:10:53,440
 Yeah.

163
00:10:53,440 --> 00:10:57,400
 And back to school supplies.

164
00:10:57,400 --> 00:10:59,940
 I'm trying to think of something that could get my family

165
00:10:59,940 --> 00:11:00,480
 involved

166
00:11:00,480 --> 00:11:02,960
 or to be able to give as a gift to them.

167
00:11:02,960 --> 00:11:12,920
 I want to get on their behalf.

168
00:11:12,920 --> 00:11:14,920
 It would be the best way to give on someone's.

169
00:11:14,920 --> 00:11:23,980
 If we had a bunch of supplies, then we could all together

170
00:11:23,980 --> 00:11:24,440
 deliver it.

171
00:11:24,440 --> 00:11:37,720
 Yeah, we would just need a place to send them down there

172
00:11:37,720 --> 00:11:39,960
 so you wouldn't have to try to carry them all with you.

173
00:11:39,960 --> 00:11:46,840
 Mm-hmm.

174
00:11:46,840 --> 00:11:48,560
 There's urgent needs in November.

175
00:11:48,560 --> 00:11:53,640
 Batteries.

176
00:11:53,640 --> 00:12:11,480
 [ Pause ]

177
00:12:11,480 --> 00:12:14,360
 Because they actually serve children and adults.

178
00:12:14,360 --> 00:12:19,800
 [ Pause ]

179
00:12:19,800 --> 00:12:21,800
 They suggest having a donation drive.

180
00:12:21,800 --> 00:12:26,390
 Which kind of sounds like maybe what we're talking about

181
00:12:26,390 --> 00:12:26,920
 maybe.

182
00:12:26,920 --> 00:12:50,200
 [ Pause ]

183
00:12:50,200 --> 00:12:53,320
 When you're ready, Bante, there are a couple of questions.

184
00:12:53,320 --> 00:12:53,960
 All right.

185
00:12:53,960 --> 00:12:55,880
 Let's get back to the number one.

186
00:12:55,880 --> 00:12:56,360
 Okay.

187
00:12:56,360 --> 00:12:58,910
 I know it was starting to feel like a volunteer meeting

188
00:12:58,910 --> 00:13:00,400
 there for a moment.

189
00:13:00,400 --> 00:13:03,210
 At the moment someone dies, how long do they stay near

190
00:13:03,210 --> 00:13:03,960
 their body?

191
00:13:03,960 --> 00:13:06,440
 Are they still aware of their former selves?

192
00:13:06,440 --> 00:13:09,120
 How long before they are reborn?

193
00:13:09,120 --> 00:13:10,920
 I have no idea.

194
00:13:10,920 --> 00:13:17,960
 It's not -- I'm not qualified to answer that question.

195
00:13:17,960 --> 00:13:22,880
 Those questions are all good questions.

196
00:13:22,880 --> 00:13:25,840
 Find someone who knows.

197
00:13:25,840 --> 00:13:32,200
 Some of it's -- the Buddha has passed things down.

198
00:13:32,200 --> 00:13:35,800
 But not to that extent.

199
00:13:35,800 --> 00:13:39,120
 We have stories that have been passed down.

200
00:13:39,120 --> 00:13:39,960
 Spirits.

201
00:13:39,960 --> 00:13:44,000
 There was one spirit hanging out by its dead body.

202
00:13:44,000 --> 00:13:47,680
 And a monk came and took the cloth off the body

203
00:13:47,680 --> 00:13:50,120
 because they usually wrapped them up in a white cloth

204
00:13:50,120 --> 00:13:54,200
 and throw them in the rubbish in the charnel ground.

205
00:13:54,200 --> 00:13:57,480
 He pulled the cloth off thinking it would make a nice robe

206
00:13:57,480 --> 00:13:58,120
 cloth.

207
00:13:58,120 --> 00:14:00,880
 And the spirit got really upset and went back into the body

208
00:14:00,880 --> 00:14:01,960
 and stood up

209
00:14:01,960 --> 00:14:05,120
 and chased after them.

210
00:14:05,120 --> 00:14:08,240
 Chased them all the way back to his kutti.

211
00:14:08,240 --> 00:14:11,080
 Where he closed the door and the body fell down --

212
00:14:11,080 --> 00:14:18,840
 felt drop down against the door and the spirit left.

213
00:14:18,840 --> 00:14:20,560
 >> He wanted his cloth back.

214
00:14:20,560 --> 00:14:24,120
 >> A real Buddhist zombie story.

215
00:14:24,120 --> 00:14:27,200
 He wanted the cloth back.

216
00:14:27,200 --> 00:14:29,760
 So there was something -- the Buddha instated a rule as a

217
00:14:29,760 --> 00:14:30,840
 result of that.

218
00:14:30,840 --> 00:14:32,960
 I can't remember what the rule was.

219
00:14:32,960 --> 00:14:38,080
 I think there was a rule instated like making sure the body

220
00:14:38,080 --> 00:14:39,760
's really dead

221
00:14:39,760 --> 00:14:42,240
 or something like that.

222
00:14:42,240 --> 00:14:46,020
 It was a simple -- it wasn't about don't take cloths from

223
00:14:46,020 --> 00:14:46,600
 dead bodies.

224
00:14:46,600 --> 00:14:49,040
 It's really a neat thing to do.

225
00:14:49,040 --> 00:14:51,700
 And it's not considered stealing because really you're dead

226
00:14:51,700 --> 00:14:52,200
, dude.

227
00:14:52,200 --> 00:14:55,080
 Move on. Get over it.

228
00:14:55,080 --> 00:14:58,160
 Or something.

229
00:14:58,160 --> 00:15:02,070
 Some sort of minor rule that he instated to sort of make

230
00:15:02,070 --> 00:15:07,320
 sure you didn't get chased by zombies.

231
00:15:07,320 --> 00:15:10,520
 >> Maybe cover the body up with something else?

232
00:15:10,520 --> 00:15:15,080
 >> No. I don't remember.

233
00:15:15,080 --> 00:15:19,900
 >> Dear Bhante, the more I give up doubts regarding the

234
00:15:19,900 --> 00:15:20,800
 practice,

235
00:15:20,800 --> 00:15:23,480
 the more I understand the practice, the more I dedicate

236
00:15:23,480 --> 00:15:25,040
 myself to the practice,

237
00:15:25,040 --> 00:15:28,840
 and the more I let go, the more there develops an issue

238
00:15:28,840 --> 00:15:29,920
 that could probably be seen

239
00:15:29,920 --> 00:15:32,540
 as relating to what you described as the second imperfect

240
00:15:32,540 --> 00:15:33,840
ion of insight,

241
00:15:33,840 --> 00:15:35,880
 the imperfection of knowledge.

242
00:15:35,880 --> 00:15:39,120
 What has been happening lately is a kind of circle or sees

243
00:15:39,120 --> 00:15:40,840
aw motion or something.

244
00:15:40,840 --> 00:15:46,080
 I attain more clarity regarding Anicca, Dukha, and Anata.

245
00:15:46,080 --> 00:15:48,900
 I start to see with greater and greater clarity the useless

246
00:15:48,900 --> 00:15:50,480
ness and worthlessness of aspects

247
00:15:50,480 --> 00:15:54,400
 of myself, of other people, of several activities I've been

248
00:15:54,400 --> 00:15:56,680
 occupied with throughout my life,

249
00:15:56,680 --> 00:16:00,000
 of the goals I've had in my life, of the goals other people

250
00:16:00,000 --> 00:16:01,240
 have, et cetera.

251
00:16:01,240 --> 00:16:03,600
 This makes me want to practice even more and dedicate

252
00:16:03,600 --> 00:16:04,480
 myself even more.

253
00:16:04,480 --> 00:16:05,720
 And I do that.

254
00:16:05,720 --> 00:16:08,510
 But soon afterwards, when indulging in the many activities

255
00:16:08,510 --> 00:16:09,680
 as before,

256
00:16:09,680 --> 00:16:12,990
 in dealing with the same people as before, which tends to

257
00:16:12,990 --> 00:16:14,040
 be bound to happen due

258
00:16:14,040 --> 00:16:17,110
 to being a layperson living in the world, my experience

259
00:16:17,110 --> 00:16:17,920
 with these activities

260
00:16:17,920 --> 00:16:21,260
 and people is now different because of how the practice has

261
00:16:21,260 --> 00:16:22,600
 affected the mind.

262
00:16:22,600 --> 00:16:25,040
 Everything is now so much easier and smoother, and

263
00:16:25,040 --> 00:16:26,200
 sometimes it even feels

264
00:16:26,200 --> 00:16:29,320
 like I could clearly accomplish anything.

265
00:16:29,320 --> 00:16:32,730
 This tends to entice or allure very strongly to play the

266
00:16:32,730 --> 00:16:34,400
 mundane game of life,

267
00:16:34,400 --> 00:16:37,990
 to play this mundane game of life for at least a bit longer

268
00:16:37,990 --> 00:16:38,440
, as it feels

269
00:16:38,440 --> 00:16:41,240
 like I'm now almost looking at these activities and

270
00:16:41,240 --> 00:16:41,880
 interactions

271
00:16:41,880 --> 00:16:45,200
 from the outside as some kind of super user.

272
00:16:45,200 --> 00:16:48,250
 Of course, this doesn't last for very long, but it makes

273
00:16:48,250 --> 00:16:49,440
 things difficult.

274
00:16:49,440 --> 00:16:52,710
 Even when it happens, I still always kind of remember the

275
00:16:52,710 --> 00:16:54,840
 importance of the practice

276
00:16:54,840 --> 00:16:58,040
 and the worthlessness of the things I mentioned earlier.

277
00:16:58,040 --> 00:17:00,960
 But at least this slows things down considerably.

278
00:17:00,960 --> 00:17:02,680
 Can you talk about this?

279
00:17:02,680 --> 00:17:04,480
 Thank you.

280
00:17:04,480 --> 00:17:06,800
 >> I think Robin just didn't talk about that.

281
00:17:06,800 --> 00:17:09,680
 What's the question?

282
00:17:09,680 --> 00:17:13,010
 I'm sorry, Robin, that you had to -- that's not really fair

283
00:17:13,010 --> 00:17:14,760
, is it?

284
00:17:14,760 --> 00:17:17,240
 I mean, it's neat to hear about, but --

285
00:17:17,240 --> 00:17:20,560
 >> Yeah. I think we probably understand when he's talking

286
00:17:20,560 --> 00:17:22,360
 about that big change between --

287
00:17:22,360 --> 00:17:23,720
 >> Ask a question.

288
00:17:23,720 --> 00:17:24,120
 >> Yeah.

289
00:17:24,120 --> 00:17:25,720
 >> I think you needed to talk about something.

290
00:17:25,720 --> 00:17:29,600
 I mean, normally, Robin, you're going to -- but I have pity

291
00:17:29,600 --> 00:17:30,360
 on Robin

292
00:17:30,360 --> 00:17:33,240
 that she has to talk -- has to say it all.

293
00:17:33,240 --> 00:17:33,920
 >> That's okay.

294
00:17:33,920 --> 00:17:37,040
 I don't mind reading long questions, you know, if it's

295
00:17:37,040 --> 00:17:37,760
 helpful.

296
00:17:37,760 --> 00:17:39,760
 >> Well, in the end, there was no question.

297
00:17:39,760 --> 00:17:41,080
 What's the question?

298
00:17:41,080 --> 00:17:42,280
 I mean, good for you.

299
00:17:42,280 --> 00:17:47,700
 It's great to hear about your practice, but no, we don't --

300
00:17:47,700 --> 00:17:48,800
 it's not fair.

301
00:17:48,800 --> 00:17:49,960
 You have to ask a question.

302
00:17:49,960 --> 00:17:52,680
 It should be short and concise and simple.

303
00:17:52,680 --> 00:17:54,080
 Easy to understand.

304
00:17:54,080 --> 00:17:57,880
 Is that person still around?

305
00:17:57,880 --> 00:17:59,640
 >> Yes.

306
00:17:59,640 --> 00:18:03,080
 >> Well, then ask a question and make it simple.

307
00:18:03,080 --> 00:18:04,880
 Keep it simple.

308
00:18:04,880 --> 00:18:10,560
 Those are the rules.

309
00:18:10,560 --> 00:18:15,860
 And we don't need so much background, really, because you

310
00:18:15,860 --> 00:18:17,120
'll find that when you do come

311
00:18:17,120 --> 00:18:21,340
 up with a question, if there is one, that you didn't really

312
00:18:21,340 --> 00:18:21,680
 need

313
00:18:21,680 --> 00:18:23,760
 to give all the background details.

314
00:18:23,760 --> 00:18:27,560
 You have to learn to be mindful.

315
00:18:27,560 --> 00:18:30,400
 Don't think so much.

316
00:18:30,400 --> 00:18:34,080
 It sounds like there's maybe too much mental activity.

317
00:18:34,080 --> 00:18:34,800
 Do you have a question?

318
00:18:34,800 --> 00:18:38,720
 Ask it. Zen thinks Zen.

319
00:18:38,720 --> 00:18:41,200
 You don't be hit with a stick.

320
00:18:41,200 --> 00:18:45,360
 >> We'll have to implement the Twitter limitation there.

321
00:18:45,360 --> 00:18:46,840
 What's that, 120 characters?

322
00:18:46,840 --> 00:18:49,870
 >> I see it was originally at something like, what, 400

323
00:18:49,870 --> 00:18:51,240
 characters or something?

324
00:18:51,240 --> 00:18:55,560
 200, and then people, oh, no, it's too short.

325
00:18:55,560 --> 00:18:59,720
 >> Now, how many posts did that person use just to ask that

326
00:18:59,720 --> 00:19:01,000
 not a question?

327
00:19:01,000 --> 00:19:02,560
 >> It was just two.

328
00:19:02,560 --> 00:19:05,600
 >> Oh, Timo, this is the guy who's coming in December.

329
00:19:05,600 --> 00:19:07,840
 >> Mm-hmm.

330
00:19:07,840 --> 00:19:11,610
 So he's going to put a little more information there, I

331
00:19:11,610 --> 00:19:12,520
 believe.

332
00:19:12,520 --> 00:19:20,800
 >> No, there's no need for background.

333
00:19:20,800 --> 00:19:23,120
 Just ask a question.

334
00:19:23,120 --> 00:19:25,370
 See, and if you can't formulate a question, then you've got

335
00:19:25,370 --> 00:19:25,920
 a problem.

336
00:19:25,920 --> 00:19:27,840
 That's the reason.

337
00:19:27,840 --> 00:19:32,630
 It's a litmus test that you still have, still not clear in

338
00:19:32,630 --> 00:19:33,360
 your mind,

339
00:19:33,360 --> 00:19:35,080
 which means that's your problem.

340
00:19:35,080 --> 00:19:38,660
 You have to start thinking, thinking, and looking at your

341
00:19:38,660 --> 00:19:40,120
 mind, and maybe read my

342
00:19:40,120 --> 00:19:43,320
 booklet again to figure out if you're actually meditating

343
00:19:43,320 --> 00:19:44,000
 correctly,

344
00:19:44,000 --> 00:19:48,000
 because it's easy to get meditating on the wrong path.

345
00:19:50,360 --> 00:19:52,720
 Maybe you have states of bliss or happiness.

346
00:19:52,720 --> 00:19:55,700
 Look at the ten imperfections of insight and see if you

347
00:19:55,700 --> 00:19:56,920
 have any of the other ones.

348
00:19:56,920 --> 00:19:58,200
 Feel happy or calm.

349
00:19:58,200 --> 00:20:01,560
 If you're powerful, well, that's a confidence.

350
00:20:01,560 --> 00:20:03,880
 That's sundehydes.

351
00:20:03,880 --> 00:20:07,480
 Andimoka, I think, is the defilement.

352
00:20:07,480 --> 00:20:16,230
 So you have to say happy, happy, or confident, confident,

353
00:20:16,230 --> 00:20:17,040
 knowing, knowing,

354
00:20:17,040 --> 00:20:17,560
 anything.

355
00:20:17,560 --> 00:20:25,880
 [BLANK_AUDIO]

356
00:20:25,880 --> 00:20:29,610
 >> I noticed on a calendar, Bonté, that tomorrow is Anapan

357
00:20:29,610 --> 00:20:30,760
asati Day?

358
00:20:30,760 --> 00:20:32,000
 Is that- >> I don't know.

359
00:20:32,000 --> 00:20:34,480
 >> Is that a different tradition?

360
00:20:34,480 --> 00:20:39,680
 [BLANK_AUDIO]

361
00:20:39,680 --> 00:20:40,440
 >> I don't know.

362
00:20:40,440 --> 00:20:42,240
 [BLANK_AUDIO]

363
00:20:42,240 --> 00:20:43,880
 >> Which is the full moon day.

364
00:20:43,880 --> 00:20:46,120
 >> Mm-hm.

365
00:20:46,120 --> 00:20:49,650
 >> I guess it's what they say when they say the Buddha

366
00:20:49,650 --> 00:20:52,000
 taught the Anapanasati sutta.

367
00:20:52,000 --> 00:20:57,240
 So those people who are keen on Anapanasati have created a

368
00:20:57,240 --> 00:20:58,760
 day.

369
00:20:58,760 --> 00:20:59,760
 >> Okay.

370
00:20:59,760 --> 00:21:11,520
 [BLANK_AUDIO]

371
00:21:11,520 --> 00:21:12,920
 >> Which I sounded kind of grumpy there.

372
00:21:12,920 --> 00:21:13,960
 I didn't mean to sound grumpy.

373
00:21:13,960 --> 00:21:14,760
 It's a good thing.

374
00:21:14,760 --> 00:21:15,880
 It's good to have that.

375
00:21:15,880 --> 00:21:18,410
 It's a little, I guess a little bit grumpy because it's

376
00:21:18,410 --> 00:21:19,400
 like, wow, really.

377
00:21:19,400 --> 00:21:23,480
 It's not really a holiday like Magabuja or

378
00:21:23,480 --> 00:21:27,320
 Wysakapuja or Asalahapuja.

379
00:21:27,320 --> 00:21:28,240
 These are the three big ones.

380
00:21:28,240 --> 00:21:33,680
 Is there, they're old and traditional and meaningful.

381
00:21:33,680 --> 00:21:35,720
 To have a day just for the sutta, well, it's nice.

382
00:21:35,720 --> 00:21:38,780
 There's nothing wrong with it, but it's not on the level of

383
00:21:38,780 --> 00:21:40,240
 a real Buddhist holiday.

384
00:21:40,240 --> 00:21:43,720
 >> Is there anything special that they do in Thailand on

385
00:21:43,720 --> 00:21:44,560
 that day?

386
00:21:45,880 --> 00:21:47,730
 >> No, I mean, this is, I don't know who which group this

387
00:21:47,730 --> 00:21:48,160
 is.

388
00:21:48,160 --> 00:21:51,800
 I think it's Thalesa or Bhikkhu that popularized that.

389
00:21:51,800 --> 00:21:52,440
 I don't know.

390
00:21:52,440 --> 00:21:53,840
 Maybe it's his group.

391
00:21:53,840 --> 00:21:55,120
 Maybe the Dama Yutani Kai.

392
00:21:55,120 --> 00:21:56,640
 I don't know.

393
00:21:56,640 --> 00:21:57,140
 >> Okay.

394
00:21:57,140 --> 00:22:02,360
 [BLANK_AUDIO]

395
00:22:02,360 --> 00:22:04,760
 >> Certainly not a big thing in Thailand as far as I know.

396
00:22:04,760 --> 00:22:14,760
 [BLANK_AUDIO]

397
00:22:14,760 --> 00:22:28,100
 >> Do you think it is important to keep attention on body

398
00:22:28,100 --> 00:22:28,800
 in your daily life?

399
00:22:28,800 --> 00:22:31,440
 [BLANK_AUDIO]

400
00:22:31,440 --> 00:22:34,360
 Well, not meditating rather than thoughts.

401
00:22:34,360 --> 00:22:38,380
 One can understand Anicha, Dukkha, Anatta through thoughts

402
00:22:38,380 --> 00:22:39,120
 too.

403
00:22:39,120 --> 00:22:42,640
 Is while not meditating, question is while not meditating,

404
00:22:42,640 --> 00:22:45,630
 can one give importance to thoughts or should one confine

405
00:22:45,630 --> 00:22:47,160
 oneself to the body?

406
00:22:47,160 --> 00:22:49,960
 >> Well, the body's easier when you're not meditating,

407
00:22:49,960 --> 00:22:51,480
 the body's easier to be mindful.

408
00:22:51,480 --> 00:22:55,070
 Thoughts can be a bit overwhelming, but certainly you

409
00:22:55,070 --> 00:22:55,960
 should be mindful of all

410
00:22:55,960 --> 00:22:58,880
 four suttipatthana during your life.

411
00:23:00,440 --> 00:23:04,430
 We tend to recommend doing the body, it's easier when you

412
00:23:04,430 --> 00:23:05,440
're standing, walking,

413
00:23:05,440 --> 00:23:07,760
 sitting in line, and that's just easier to be mindful of.

414
00:23:07,760 --> 00:23:11,560
 And that's for most people.

415
00:23:11,560 --> 00:23:14,640
 For some people, the mind is easier.

416
00:23:14,640 --> 00:23:18,810
 Some people benefit more from focusing on one or another of

417
00:23:18,810 --> 00:23:19,600
 the suttipatthana.

418
00:23:19,600 --> 00:23:25,960
 So, it's always, there's no rules like that, should this,

419
00:23:25,960 --> 00:23:26,480
 should that.

420
00:23:29,280 --> 00:23:33,690
 Suttipatthana are like four weapons that you have in your

421
00:23:33,690 --> 00:23:35,920
 gorilla soldier,

422
00:23:35,920 --> 00:23:42,140
 fighting a messy war, and so it's not, it's not, it's not

423
00:23:42,140 --> 00:23:43,800
 neat, it's messy.

424
00:23:43,800 --> 00:23:46,680
 Sometimes like this, sometimes like that,

425
00:23:46,680 --> 00:23:50,400
 you have to change tactics depending on the enemy and so on

426
00:23:50,400 --> 00:23:50,400
,

427
00:23:50,400 --> 00:23:53,600
 depending on the situation.

428
00:23:53,600 --> 00:23:57,680
 You have to be clever like a boxer to know when to duck and

429
00:23:57,680 --> 00:23:58,640
 when to weave and

430
00:23:58,640 --> 00:24:01,080
 when to punch and when to jab and so on.

431
00:24:01,080 --> 00:24:07,480
 And you have to know the long game, you have to be patient.

432
00:24:07,480 --> 00:24:12,240
 Okay, expect a knockout in one punch.

433
00:24:12,240 --> 00:24:23,110
 It's kind of like a war of attrition, it's not that, what

434
00:24:23,110 --> 00:24:23,920
 is that, what is that?

435
00:24:23,920 --> 00:24:28,000
 You bleed each other, you bleed your enemies to death.

436
00:24:29,440 --> 00:24:31,750
 Starve your enemies to death, that's really what it is, we

437
00:24:31,750 --> 00:24:33,920
're starving our defilements.

438
00:24:33,920 --> 00:24:38,280
 >> I'm gonna note them to death.

439
00:24:38,280 --> 00:24:48,280
 >> [LAUGH]

440
00:24:48,280 --> 00:25:01,800
 >> What is this, can it be that I've answered all the

441
00:25:01,800 --> 00:25:02,920
 questions possible and

442
00:25:02,920 --> 00:25:04,080
 no one has more questions?

443
00:25:05,120 --> 00:25:08,160
 Our viewership is down, right, fewer people are watching,

444
00:25:08,160 --> 00:25:09,600
 which is fine.

445
00:25:09,600 --> 00:25:15,960
 It's expected, you know, who's got time to watch some funny

446
00:25:15,960 --> 00:25:17,440
 monk for every day.

447
00:25:17,440 --> 00:25:23,160
 Got things they need to be doing, it's fine.

448
00:25:23,160 --> 00:25:24,400
 This is just sort of a hello.

449
00:25:24,400 --> 00:25:28,580
 Tomorrow we'll do another Dhammapada, but again, that's not

450
00:25:28,580 --> 00:25:30,080
 gonna be broadcast live.

451
00:25:30,080 --> 00:25:30,960
 It'll be audio live.

452
00:25:34,720 --> 00:25:36,560
 Tomorrow, yeah, tomorrow is another Dhammapada.

453
00:25:36,560 --> 00:25:43,840
 That's enough, that's all, good night, thank you.

454
00:25:43,840 --> 00:25:45,640
 >> There was one more question if you had time.

455
00:25:45,640 --> 00:25:52,470
 Hi, Bante, is an Arahant protected from physical harm only

456
00:25:52,470 --> 00:25:54,200
 during meditating or always?

457
00:25:54,200 --> 00:25:58,890
 >> They're not protected from physical harm even when med

458
00:25:58,890 --> 00:25:59,960
itating.

459
00:25:59,960 --> 00:26:04,970
 Now, there's these stories about when you enter into Jhana

460
00:26:04,970 --> 00:26:05,240
 or

461
00:26:05,240 --> 00:26:09,720
 Samapati of some sort, you're invincible.

462
00:26:09,720 --> 00:26:14,880
 But that's nothing to do with being an Arahant, per se.

463
00:26:14,880 --> 00:26:17,390
 It's to do with the state of attainment, the state of

464
00:26:17,390 --> 00:26:18,000
 meditation.

465
00:26:18,000 --> 00:26:23,410
 Even non-Arahants could theoretically have that sort of

466
00:26:23,410 --> 00:26:25,040
 imperviousness.

467
00:26:25,040 --> 00:26:28,980
 It's not exactly or directly related to their state of

468
00:26:28,980 --> 00:26:30,600
 being an Arahant.

469
00:26:30,600 --> 00:26:32,760
 It's the state of meditation that they go into.

470
00:26:32,760 --> 00:26:38,960
 >> Didn't the Buddha hurt his foot or have some sort of an

471
00:26:38,960 --> 00:26:40,840
 injury after he was enlightened?

472
00:26:40,840 --> 00:26:45,440
 >> Yeah, Mughalana, the most powerful meditative monk who

473
00:26:45,440 --> 00:26:46,280
 had the most powerful

474
00:26:46,280 --> 00:26:48,920
 attainment was beaten almost to death.

475
00:26:48,920 --> 00:26:53,840
 Had all the bones in his body broken, apparently.

476
00:26:53,840 --> 00:26:55,120
 He had horrible karma.

477
00:26:55,120 --> 00:26:58,760
 He had tortured his parents, beaten his parents,

478
00:26:58,760 --> 00:27:00,360
 because he didn't want to take care of them.

479
00:27:00,360 --> 00:27:02,320
 [LAUGH]

480
00:27:02,320 --> 00:27:03,160
 Went to hell for that.

481
00:27:03,160 --> 00:27:09,440
 >> This is a follow up question from Timu.

482
00:27:09,440 --> 00:27:11,980
 I guess I was looking for help regarding the seesaw motion

483
00:27:11,980 --> 00:27:12,960
 between the practice and

484
00:27:12,960 --> 00:27:14,000
 the lay life.

485
00:27:14,000 --> 00:27:17,080
 I think the question seemed kind of conceited.

486
00:27:17,080 --> 00:27:19,320
 Also, sorry, Robin, for the long questions.

487
00:27:19,320 --> 00:27:20,080
 Don't be sorry.

488
00:27:20,080 --> 00:27:21,280
 >> It wasn't even a question.

489
00:27:21,280 --> 00:27:22,040
 Where's your question?

490
00:27:23,760 --> 00:27:28,800
 >> The question is, what sort of help can you give me for

491
00:27:28,800 --> 00:27:28,840
 understanding,

492
00:27:28,840 --> 00:27:33,040
 for dealing with the seesaw motion between the practice and

493
00:27:33,040 --> 00:27:33,760
 the lay life?

494
00:27:33,760 --> 00:27:40,840
 >> I don't know.

495
00:27:40,840 --> 00:27:42,880
 I don't like general help questions either.

496
00:27:42,880 --> 00:27:45,720
 Is there too complicated?

497
00:27:45,720 --> 00:27:48,440
 It's like you want me to write, you're not alone.

498
00:27:48,440 --> 00:27:49,880
 Many people ask these sorts of questions.

499
00:27:49,880 --> 00:27:51,680
 But it's like you want me to plan your life out.

500
00:27:53,680 --> 00:27:56,120
 General advice, it's too difficult.

501
00:27:56,120 --> 00:27:59,340
 I don't know you, I don't know how to read the Sigalawada

502
00:27:59,340 --> 00:27:59,920
 Sutta,

503
00:27:59,920 --> 00:28:01,120
 give you some general advice.

504
00:28:01,120 --> 00:28:04,520
 I'm digging Nikaya 31, I think.

505
00:28:04,520 --> 00:28:08,800
 Sigalawada Sutta, really good sutta, but there's a lot.

506
00:28:08,800 --> 00:28:13,200
 Because it's not that simple.

507
00:28:13,200 --> 00:28:15,040
 I can't just tell you something that's going to help you

508
00:28:15,040 --> 00:28:15,520
 with that.

509
00:28:15,520 --> 00:28:20,030
 You have to give specific examples, like is this right, is

510
00:28:20,030 --> 00:28:20,720
 that right?

511
00:28:21,840 --> 00:28:23,960
 And I think maybe to some extent you already know the

512
00:28:23,960 --> 00:28:25,320
 answer to some of the

513
00:28:25,320 --> 00:28:29,080
 questions that you might ask, so that you're avoiding them.

514
00:28:29,080 --> 00:28:32,680
 You have to just look at the individual aspects of your

515
00:28:32,680 --> 00:28:32,880
 life.

516
00:28:32,880 --> 00:28:38,240
 Dealing with people, feeling egotistical, feeling ambitious

517
00:28:38,240 --> 00:28:39,600
, feeling,

518
00:28:39,600 --> 00:28:42,560
 that's why I'm not egotistical, I don't know you said that.

519
00:28:42,560 --> 00:28:44,280
 Ambitious was kind of like you were saying.

520
00:28:44,280 --> 00:28:48,240
 And you know that that's ambition, right?

521
00:28:50,400 --> 00:28:52,440
 How to deal with things is always just meditating.

522
00:28:52,440 --> 00:28:54,080
 It's always just going to be learned to meditate,

523
00:28:54,080 --> 00:28:56,000
 learn how to see them clearly, learn how to let go.

524
00:28:56,000 --> 00:29:04,400
 But the details of living lay life don't.

525
00:29:04,400 --> 00:29:07,310
 I mean the ultimate advice, it just comes down to become a

526
00:29:07,310 --> 00:29:08,120
 monk.

527
00:29:08,120 --> 00:29:13,200
 If you want real answers, not an easy thing to do.

528
00:29:13,200 --> 00:29:21,600
 [BLANK_AUDIO]

529
00:29:21,600 --> 00:29:24,600
 Apologies, probably not satisfying, but.

530
00:29:24,600 --> 00:29:29,080
 [BLANK_AUDIO]

531
00:29:29,080 --> 00:29:31,840
 >> He's very grateful for the how to meditate books.

532
00:29:31,840 --> 00:29:33,120
 >> Okay, well good.

533
00:29:33,120 --> 00:29:38,200
 [BLANK_AUDIO]

534
00:29:38,200 --> 00:29:40,120
 Okay, enough, good night.

535
00:29:40,120 --> 00:29:42,560
 [BLANK_AUDIO]

536
00:29:42,560 --> 00:29:43,360
 >> Thank you, Robin.

537
00:29:43,360 --> 00:29:45,880
 [BLANK_AUDIO]

538
00:29:45,880 --> 00:29:48,240
 >> Thank you, Ponte, good night.

539
00:29:48,240 --> 00:29:49,600
 >> We don't have another question, do we?

540
00:29:49,600 --> 00:29:51,600
 [BLANK_AUDIO]

541
00:29:51,600 --> 00:29:53,160
 Are you, not that you see?

542
00:29:53,160 --> 00:29:54,960
 >> No.

543
00:29:54,960 --> 00:29:55,760
 >> Okay.

544
00:29:55,760 --> 00:29:57,760
 >> Okay, good night, Ponte, thank you.

545
00:29:57,760 --> 00:30:05,760
 [BLANK_AUDIO]

