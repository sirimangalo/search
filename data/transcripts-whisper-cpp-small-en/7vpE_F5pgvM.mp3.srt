1
00:00:00,000 --> 00:00:05,000
 And the cause for suffering, when it's said, he explained

2
00:00:05,000 --> 00:00:07,000
 that the cause for suffering is our thinking.

3
00:00:07,000 --> 00:00:10,730
 Our griefs are one thing. We want things to be a certain

4
00:00:10,730 --> 00:00:12,000
 way and we don't want them to be another way.

5
00:00:12,000 --> 00:00:15,000
 They have to be this way and again.

6
00:00:15,000 --> 00:00:20,180
 He said when this ceases, when there is no more one thing,

7
00:00:20,180 --> 00:00:22,000
 then no more attachment.

8
00:00:22,000 --> 00:00:25,150
 No more attachment to anything, then there's no more

9
00:00:25,150 --> 00:00:26,000
 suffering.

10
00:00:28,000 --> 00:00:30,000
 I always say this is like a bird.

11
00:00:30,000 --> 00:00:34,000
 You can imagine a bird plating to the side of it.

12
00:00:34,000 --> 00:00:38,000
 We're like that bird plating to the side of the cliff.

13
00:00:38,000 --> 00:00:42,310
 We're like a fallen off this cliff and we're plating to the

14
00:00:42,310 --> 00:00:43,000
 edge.

15
00:00:43,000 --> 00:00:47,000
 Because we're so afraid we're going to fall.

16
00:00:47,000 --> 00:00:50,000
 Even beings are like this, we're always plating to the edge

17
00:00:50,000 --> 00:00:50,000
.

18
00:00:50,000 --> 00:00:53,360
 We're plating because we're afraid to let go and to end the

19
00:00:53,360 --> 00:00:54,000
 disappear.

20
00:00:54,000 --> 00:00:58,000
 We're going to be here teaching this on an attachment.

21
00:00:58,000 --> 00:01:04,000
 We'll never survive. We'll never be happy.

22
00:01:04,000 --> 00:01:07,980
 All of my things will disappear on one lane, on the

23
00:01:07,980 --> 00:01:09,000
 attachment.

24
00:01:09,000 --> 00:01:14,000
 But we don't realize that we're like a bird.

25
00:01:14,000 --> 00:01:19,000
 We're not like a animal on the cliff or actually a bird.

26
00:01:19,000 --> 00:01:22,000
 As soon as we let go, we can fly.

27
00:01:23,000 --> 00:01:25,000
 We can fly above all of them.

28
00:01:25,000 --> 00:01:31,000
 So meditation is teaching us to fly.

29
00:01:31,000 --> 00:01:34,400
 We can fly above all of the divisions, all of the

30
00:01:34,400 --> 00:01:37,000
 attachments and the images.

31
00:01:37,000 --> 00:01:39,000
 All of our likes and dislikes.

32
00:01:39,000 --> 00:01:41,000
 We let go of them.

33
00:01:41,000 --> 00:01:43,000
 We can fly above them.

34
00:01:43,000 --> 00:01:46,000
 We're like a bird flying free.

35
00:01:46,000 --> 00:01:48,000
 We leave no trace.

36
00:01:50,000 --> 00:01:53,000
 This is the bird. There's no trace in this guy.

37
00:01:53,000 --> 00:01:56,310
 We leave no trace. We don't hurt her. It causes problems to

38
00:01:56,310 --> 00:01:57,000
 change.

39
00:01:57,000 --> 00:02:01,000
 This is the basis of the first disc function.

40
00:02:01,000 --> 00:02:04,000
 This is what's most important about tonight.

41
00:02:04,000 --> 00:02:07,000
 This was the night where we were taught this first topic.

42
00:02:07,000 --> 00:02:09,000
 This is the most profound new chant.

43
00:02:09,000 --> 00:02:12,000
 Something that we always look forward back to.

44
00:02:12,000 --> 00:02:15,000
 We always try to get to.

45
00:02:17,000 --> 00:02:20,410
 Tonight we won't do that chant. They will just do a brief

46
00:02:20,410 --> 00:02:21,000
 chant and then sort of sing.

47
00:02:21,000 --> 00:02:25,000
 Okay, so that's an explanation.

48
00:02:25,000 --> 00:02:27,000
 Tonight is the first teaching.

49
00:02:27,000 --> 00:02:30,000
 And I guess, sorry, one more thing that's important about

50
00:02:30,000 --> 00:02:30,000
 tonight.

51
00:02:30,000 --> 00:02:34,670
 This was the talk that you've raised to the first person in

52
00:02:34,670 --> 00:02:37,000
 the world who understands this teaching.

53
00:02:37,000 --> 00:02:42,000
 And that is one of the five months.

54
00:02:42,000 --> 00:02:46,130
 You actually take an understanding of the practice sitting

55
00:02:46,130 --> 00:02:47,000
 right there.

56
00:02:47,000 --> 00:02:52,000
 And the multivocal is the only one who understands this.

57
00:02:52,000 --> 00:02:55,000
 You practice and you realize it.

58
00:02:55,000 --> 00:02:59,000
 And you say that there is no point in any kind of thing.

59
00:02:59,000 --> 00:03:02,000
 So that's the calling of the C.S.E.

60
00:03:02,000 --> 00:03:08,000
 Understand the words of the M.T. some day and sometimes

61
00:03:08,000 --> 00:03:08,000
 they will go down.

62
00:03:09,000 --> 00:03:12,000
 This is what we're trying to do right now.

63
00:03:12,000 --> 00:03:18,720
 This means that everything that is born, everything that

64
00:03:18,720 --> 00:03:20,000
 arises has to cease.

65
00:03:20,000 --> 00:03:25,610
 There's nothing in existence which has arisen, but not this

66
00:03:25,610 --> 00:03:26,000
 way.

67
00:03:26,000 --> 00:03:30,000
 So for this reason there's nothing left to do.

68
00:03:30,000 --> 00:03:33,270
 And you're able to let go and realize that even there are

69
00:03:33,270 --> 00:03:35,000
 fragments and boundaries.

70
00:03:35,000 --> 00:03:39,000
 For reasons that they couldn't.

71
00:03:39,000 --> 00:03:41,000
 You can't open.

72
00:03:41,000 --> 00:03:43,000
 No need.

73
00:03:43,000 --> 00:03:48,000
 So on this night, this is the night that I have witnessed

74
00:03:48,000 --> 00:03:51,170
 the night when the Tibetan someone wonders there is much of

75
00:03:51,170 --> 00:03:52,000
 a Soviet who successfully passed

76
00:03:52,000 --> 00:03:55,000
 one of his teachings.

77
00:03:55,000 --> 00:03:58,000
 All the setting and motion of the thing is after this.

78
00:03:58,000 --> 00:04:02,000
 Nobody can stop the motion of the thing.

79
00:04:02,000 --> 00:04:05,000
 Even to this day it's no more.

80
00:04:05,000 --> 00:04:08,320
 That was the day when the Buddha said, "We are still

81
00:04:08,320 --> 00:04:10,000
 rolling today."

82
00:04:10,000 --> 00:04:14,000
 Still, even the peace and happiness of the people.

83
00:04:14,000 --> 00:04:19,000
 So this is what we celebrate.

84
00:04:27,000 --> 00:04:31,150
 So next up we're going to take the five percent of the

85
00:04:31,150 --> 00:04:50,580
 scholars who are going to be chanting in the

86
00:04:50,580 --> 00:04:57,580
 morning.

87
00:04:57,580 --> 00:05:07,580
 So we're going to take the incense, candles and the flower.

88
00:05:07,580 --> 00:05:15,580
 How many incense are there?

89
00:05:15,580 --> 00:05:16,580
 Three.

90
00:05:16,580 --> 00:05:17,580
 How many incense?

91
00:05:17,580 --> 00:05:18,580
 Three.

92
00:05:18,580 --> 00:05:21,580
 How many candles?

93
00:05:21,580 --> 00:05:30,580
 One.

94
00:05:30,580 --> 00:05:32,580
 How many flowers?

95
00:05:32,580 --> 00:05:39,580
 That's okay.

96
00:05:39,580 --> 00:05:47,580
 What color is this flower anyway?

97
00:05:47,580 --> 00:05:49,960
 The important thing is that there's different color flowers

98
00:05:49,960 --> 00:05:51,580
 and I'll explain them why.

99
00:05:51,580 --> 00:05:54,580
 You should remember to never put one color.

100
00:05:54,580 --> 00:05:58,580
 You have to put more than one color.

101
00:05:58,580 --> 00:06:00,580
 These three, what did they represent?

102
00:06:00,580 --> 00:06:02,580
 Anyone?

103
00:06:02,580 --> 00:06:08,580
 So...

104
00:06:08,580 --> 00:06:10,580
 Anyone know?

105
00:06:10,580 --> 00:06:12,580
 Anybody?

106
00:06:12,580 --> 00:06:30,580
 I said this last time.

107
00:06:30,580 --> 00:06:31,580
 Okay, there are three.

