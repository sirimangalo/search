1
00:00:00,000 --> 00:00:08,500
 OK.

2
00:00:08,500 --> 00:00:09,500
 Hello, everyone.

3
00:00:09,500 --> 00:00:16,600
 We're broadcasting live daily broadcast.

4
00:00:16,600 --> 00:00:19,400
 Say hello.

5
00:00:19,400 --> 00:00:20,880
 I need to provide some dhamma.

6
00:00:28,240 --> 00:00:31,000
 Do you have any dhamma for us today, Robin?

7
00:00:31,000 --> 00:00:33,760
 There is no dhamma, but it is Thanksgiving in the United

8
00:00:33,760 --> 00:00:36,520
 States, so everyone is asleep after eating all their turkey

9
00:00:36,520 --> 00:00:36,520
,

10
00:00:36,520 --> 00:00:38,360
 I think.

11
00:00:38,360 --> 00:00:40,880
 I had three people out wish me happy Thanksgiving today.

12
00:00:40,880 --> 00:00:43,480
 And I don't know if it was the right thing to say,

13
00:00:43,480 --> 00:00:45,720
 but I said we don't have Thanksgiving again.

14
00:00:45,720 --> 00:00:47,840
 Or we had it last month.

15
00:00:47,840 --> 00:00:49,640
 Certainly.

16
00:00:49,640 --> 00:00:52,800
 I mean, not that it would mean anything to me anyway,

17
00:00:52,800 --> 00:00:53,800
 probably.

18
00:00:53,800 --> 00:00:57,080
 But still, I think it's kind of funny

19
00:00:57,080 --> 00:01:00,280
 that people are wishing us Thanksgiving.

20
00:01:00,280 --> 00:01:05,960
 If I was in America, I guess people would.

21
00:01:05,960 --> 00:01:08,120
 It would be proper, right?

22
00:01:08,120 --> 00:01:10,920
 Yeah, Thanksgiving in America is one of those holidays

23
00:01:10,920 --> 00:01:12,840
 that's not controversial.

24
00:01:12,840 --> 00:01:14,920
 There's so much ridiculous controversy

25
00:01:14,920 --> 00:01:19,840
 with other holidays, like Donald Trump is all up in our--

26
00:01:19,840 --> 00:01:22,370
 You don't think Thanksgiving has any controversy associated

27
00:01:22,370 --> 00:01:23,040
?

28
00:01:23,040 --> 00:01:23,540
 No.

29
00:01:23,540 --> 00:01:25,440
 OK, let me step back on that.

30
00:01:25,440 --> 00:01:29,640
 Of course it does because of the Native American thing.

31
00:01:29,640 --> 00:01:33,510
 But I was just thinking of the more political type of

32
00:01:33,510 --> 00:01:33,800
 things,

33
00:01:33,800 --> 00:01:37,440
 like Donald Trump was up in arms because Starbucks

34
00:01:37,440 --> 00:01:41,000
 has a plain red cup for their holiday cup this year.

35
00:01:41,000 --> 00:01:44,280
 It doesn't say Merry Christmas or Happy Holidays

36
00:01:44,280 --> 00:01:45,440
 or anything like that.

37
00:01:45,440 --> 00:01:48,280
 And he was actually kind of going on and on about it,

38
00:01:48,280 --> 00:01:49,840
 how it was such a terrible thing.

39
00:01:49,840 --> 00:01:51,280
 And when he's president, everyone's

40
00:01:51,280 --> 00:01:52,440
 going to say Merry Christmas.

41
00:01:52,440 --> 00:01:54,780
 So it gets kind of ridiculous with holidays.

42
00:01:54,780 --> 00:01:57,950
 But Thanksgiving, other than the whole Native American

43
00:01:57,950 --> 00:01:58,200
 thing,

44
00:01:58,200 --> 00:02:00,920
 is a pretty mild holiday where everybody's

45
00:02:00,920 --> 00:02:03,040
 celebrating the same thing, more or less.

46
00:02:03,040 --> 00:02:04,400
 And--

47
00:02:04,400 --> 00:02:06,200
 I imagine the First Nations people would

48
00:02:06,200 --> 00:02:08,000
 disagree with you on that one.

49
00:02:08,000 --> 00:02:09,640
 I think so, too.

50
00:02:09,640 --> 00:02:14,280
 That's not very pleasant.

51
00:02:14,280 --> 00:02:17,040
 I mean, the idea of Thanksgiving is pretty awesome.

52
00:02:17,040 --> 00:02:20,990
 It's just how it arose, I think, or what it's associated

53
00:02:20,990 --> 00:02:21,360
 with.

54
00:02:21,360 --> 00:02:26,840
 It's somehow being thankful for having taken over

55
00:02:26,840 --> 00:02:30,880
 some other people's land, invaded.

56
00:02:30,880 --> 00:02:34,040
 It's a celebration of an invasion of--

57
00:02:34,040 --> 00:02:35,940
 I mean, not quite invasion because people here

58
00:02:35,940 --> 00:02:38,980
 didn't have the idea of land ownership, but really

59
00:02:38,980 --> 00:02:43,740
 invasion, an invasion like an invasive species that just

60
00:02:43,740 --> 00:02:57,040
 comes by force and by sheer inertia, steam rollers

61
00:02:57,040 --> 00:02:57,920
 over another culture.

62
00:02:57,920 --> 00:03:06,480
 I mean, to passively wipe out another species to me

63
00:03:06,480 --> 00:03:11,200
 doesn't seem problematic.

64
00:03:11,200 --> 00:03:20,880
 Like if it's just, oh, they eventually

65
00:03:20,880 --> 00:03:23,280
 became the dominant group.

66
00:03:23,280 --> 00:03:24,740
 But that's not really how it happened.

67
00:03:24,740 --> 00:03:31,320
 There was forced conversion and really theft and treachery,

68
00:03:31,320 --> 00:03:35,310
 trickery to get their lands and to get them to sign

69
00:03:35,310 --> 00:03:37,760
 horribly

70
00:03:37,760 --> 00:03:45,720
 unfair treaties and bad stuff from what I hear.

71
00:03:45,720 --> 00:03:59,680
 In the name of imperialism, the queen, the king, slavery,

72
00:03:59,680 --> 00:04:09,440
 all of that stuff, human beings can be so much more

73
00:04:09,440 --> 00:04:12,760
 noble than other animals.

74
00:04:12,760 --> 00:04:14,560
 They can be so much more evil as well.

75
00:04:14,560 --> 00:04:21,360
 They were also convinced that their way was right.

76
00:04:21,360 --> 00:04:27,440
 Well, the pilgrims, the settlers,

77
00:04:27,440 --> 00:04:29,240
 they were so convinced that their way was right

78
00:04:29,240 --> 00:04:31,400
 that--

79
00:04:31,400 --> 00:04:32,600
 Well, I don't know.

80
00:04:32,600 --> 00:04:33,880
 That's part of it.

81
00:04:33,880 --> 00:04:38,760
 But I think that's maybe from a religious point of view

82
00:04:38,760 --> 00:04:40,200
 or a cultural point of view.

83
00:04:40,200 --> 00:04:43,040
 But there was a lot of greed involved, I think, as well.

84
00:04:43,040 --> 00:04:45,280
 Just sheer opportunism.

85
00:04:45,280 --> 00:04:48,560
 Oh, these people are naive in a sense,

86
00:04:48,560 --> 00:04:50,520
 which I guess you could say they were.

87
00:04:50,520 --> 00:04:52,880
 But it just means that they were simple people

88
00:04:52,880 --> 00:04:57,160
 and weren't worried about land ownership and mineral rights

89
00:04:57,160 --> 00:04:57,160
.

90
00:04:59,160 --> 00:05:05,760
 Things like forests, cutting down trees.

91
00:05:05,760 --> 00:05:10,880
 They didn't rape and pillage the land either.

92
00:05:10,880 --> 00:05:15,880
 And so when people came and saw all these natural resources

93
00:05:15,880 --> 00:05:19,500
 that were maybe starting to run short in Europe, I don't

94
00:05:19,500 --> 00:05:20,120
 know.

95
00:05:20,120 --> 00:05:23,640
 But they were harder to get at.

96
00:05:23,640 --> 00:05:29,520
 But oh, gold and whatever is land.

97
00:05:29,520 --> 00:05:36,320
 I agree.

98
00:05:36,320 --> 00:05:42,200
 Greed is the biggest reason for war.

99
00:05:42,200 --> 00:05:47,680
 Usually the least publicized because everyone

100
00:05:47,680 --> 00:05:49,680
 tries to excuse it with other reasons.

101
00:05:49,680 --> 00:05:56,560
 But deep down, most wars have been fought out of greed,

102
00:05:56,560 --> 00:05:57,280
 sheer greed.

103
00:05:57,280 --> 00:06:10,440
 So on that pleasant note--

104
00:06:10,440 --> 00:06:13,160
 So I guess Thanksgiving is a little more controversial

105
00:06:13,160 --> 00:06:14,520
 than I was thinking.

106
00:06:14,520 --> 00:06:16,560
 I think so.

107
00:06:16,560 --> 00:06:19,760
 I mean, the concept is awesome.

108
00:06:19,760 --> 00:06:23,880
 It's just-- it used to be called Columbus Day, didn't it?

109
00:06:23,880 --> 00:06:25,320
 Or is that a different day?

110
00:06:25,320 --> 00:06:26,440
 That's a different day.

111
00:06:26,440 --> 00:06:27,040
 Different day.

112
00:06:27,040 --> 00:06:29,350
 You've got a day that celebrates this horrible, horrible

113
00:06:29,350 --> 00:06:31,600
 person.

114
00:06:31,600 --> 00:06:35,200
 Well, maybe the horrible were pretty bad.

115
00:06:35,200 --> 00:06:37,280
 Well, the day that we have this Columbus Day,

116
00:06:37,280 --> 00:06:41,040
 I believe, is the Canadian Thanksgiving.

117
00:06:41,040 --> 00:06:42,880
 But our Thanksgiving is a month later.

118
00:06:46,040 --> 00:06:48,680
 Yeah, you celebrate the guy who--

119
00:06:48,680 --> 00:06:50,800
 he was an opportunist, apparently,

120
00:06:50,800 --> 00:06:53,760
 just coming looking for gold.

121
00:06:53,760 --> 00:06:55,440
 And he thought he'd reached India.

122
00:06:55,440 --> 00:06:56,960
 So that's why he called them Indians.

123
00:06:56,960 --> 00:07:03,920
 Anyway, this is very--

124
00:07:03,920 --> 00:07:06,280
 skirting the edge of what is not Dhamma.

125
00:07:06,280 --> 00:07:10,520
 So you better jump back in and see what--

126
00:07:10,520 --> 00:07:11,800
 do we have any--

127
00:07:11,800 --> 00:07:14,360
 the quote is kind of neat.

128
00:07:14,360 --> 00:07:15,920
 Talks about grasping and worry.

129
00:07:15,920 --> 00:07:21,240
 So we have the five aggregates.

130
00:07:21,240 --> 00:07:23,120
 And that's what we cling to.

131
00:07:23,120 --> 00:07:25,680
 Neat clinging is always done to one of the five

132
00:07:25,680 --> 00:07:29,720
 or combination of five concepts that have arisen

133
00:07:29,720 --> 00:07:33,960
 based on the five aggregates.

134
00:07:33,960 --> 00:07:35,920
 So we cling to the body.

135
00:07:35,920 --> 00:07:38,640
 And when it changes, we worry.

136
00:07:38,640 --> 00:07:40,800
 We-- it upsets us.

137
00:07:40,800 --> 00:07:41,800
 It disturbs us.

138
00:07:41,800 --> 00:07:44,600
 It's not just the body, but other physical forms as well.

139
00:07:44,600 --> 00:07:47,360
 Our belongings, our possessions.

140
00:07:47,360 --> 00:07:52,840
 My robe, these little ties on the end of the robe.

141
00:07:52,840 --> 00:07:58,520
 These little ties here.

142
00:07:58,520 --> 00:08:04,600
 And one of them up here ripped yesterday, or ripped out.

143
00:08:04,600 --> 00:08:10,160
 Just sew it back.

144
00:08:10,160 --> 00:08:11,160
 That didn't upset me.

145
00:08:11,160 --> 00:08:13,000
 It wasn't a big deal.

146
00:08:13,000 --> 00:08:15,720
 It's easy to get upset.

147
00:08:15,720 --> 00:08:17,200
 I remember, I think, when I first

148
00:08:17,200 --> 00:08:20,560
 got this robe, like a few days later,

149
00:08:20,560 --> 00:08:22,640
 it got some stain on it or something.

150
00:08:22,640 --> 00:08:28,600
 When I got--

151
00:08:28,600 --> 00:08:35,720
 we cling to things.

152
00:08:35,720 --> 00:08:37,120
 We cling to our possessions.

153
00:08:37,120 --> 00:08:39,200
 We cling to people.

154
00:08:39,200 --> 00:08:44,600
 We cling to form.

155
00:08:44,600 --> 00:08:45,520
 We cling to feelings.

156
00:08:45,520 --> 00:08:50,560
 We cling to pleasant feelings.

157
00:08:50,560 --> 00:08:56,040
 We try to contain them any way possible.

158
00:08:56,040 --> 00:08:59,240
 And we're constantly on guard against painful feelings.

159
00:08:59,240 --> 00:09:04,120
 Often specific painful feelings.

160
00:09:04,120 --> 00:09:07,120
 So if someone has maybe a bad back,

161
00:09:07,120 --> 00:09:10,240
 and they're constantly worried about--

162
00:09:10,240 --> 00:09:15,600
 constantly, but again, and again, and again.

163
00:09:15,600 --> 00:09:18,320
 There's a little bit of pain, and immediately,

164
00:09:18,320 --> 00:09:21,360
 oh, no, my back problem becomes an obsession.

165
00:09:21,360 --> 00:09:34,920
 We get very much caught up in attachment to feelings,

166
00:09:34,920 --> 00:09:39,120
 our attachment to perceptions, recognition.

167
00:09:39,120 --> 00:09:42,720
 I mean, this one, not always by itself,

168
00:09:42,720 --> 00:09:51,840
 but it's recognition that's the trigger for much

169
00:09:51,840 --> 00:09:54,520
 of our addiction, much of our aversion.

170
00:09:54,520 --> 00:09:56,520
 We recognize something.

171
00:09:56,520 --> 00:10:00,000
 If you didn't recognize things, you

172
00:10:00,000 --> 00:10:02,640
 wouldn't recognize a spider.

173
00:10:02,640 --> 00:10:04,040
 People who are afraid of spiders,

174
00:10:04,040 --> 00:10:05,880
 if you didn't recognize that that was a spider,

175
00:10:05,880 --> 00:10:08,200
 it could walk all over you and say,

176
00:10:08,200 --> 00:10:09,840
 you'd have no idea what was going on,

177
00:10:09,840 --> 00:10:12,480
 but you'd feel something and no, just a feeling.

178
00:10:12,480 --> 00:10:21,550
 If it weren't for recognition, if it weren't for our

179
00:10:21,550 --> 00:10:22,400
 perceptions

180
00:10:22,400 --> 00:10:28,320
 of things, we couldn't give rise to liking or disliking,

181
00:10:28,320 --> 00:10:31,960
 because we couldn't associate.

182
00:10:33,840 --> 00:10:39,640
 So you see a piece of cheesecake,

183
00:10:39,640 --> 00:10:41,240
 and you associate it with cheesecake.

184
00:10:41,240 --> 00:10:45,840
 You associate it with tastes and textures and feelings,

185
00:10:45,840 --> 00:10:48,240
 and so you feel happy just to see it.

186
00:10:48,240 --> 00:10:51,180
 But if you didn't know what it was, you couldn't recognize

187
00:10:51,180 --> 00:10:51,520
 it.

188
00:10:51,520 --> 00:10:56,880
 So recognition is actually quite neutral,

189
00:10:56,880 --> 00:11:01,800
 but we cling to this--

190
00:11:01,800 --> 00:11:04,760
 we are greedy for that simple recognition,

191
00:11:04,760 --> 00:11:06,160
 because there are many things that

192
00:11:06,160 --> 00:11:07,520
 aren't inherently pleasant.

193
00:11:07,520 --> 00:11:11,880
 Seeing a piece of cheesecake isn't inherently pleasant.

194
00:11:11,880 --> 00:11:15,440
 Seeing a spider isn't inherently unpleasant.

195
00:11:15,440 --> 00:11:20,200
 Because we react to them, because we cling to that,

196
00:11:20,200 --> 00:11:24,750
 the recognition of a spider is the moment when we get upset

197
00:11:24,750 --> 00:11:24,920
.

198
00:11:26,920 --> 00:11:30,160
 And then sankara, we cling to them.

199
00:11:30,160 --> 00:11:38,040
 Cling to the sankaras of comparison

200
00:11:38,040 --> 00:11:39,480
 when you compare some things.

201
00:11:39,480 --> 00:11:45,520
 So if you look at your body and you're really fat like me,

202
00:11:45,520 --> 00:11:47,200
 and you start to worry about your weight,

203
00:11:47,200 --> 00:11:51,600
 and like I have to go on a diet, stop eating so much.

204
00:11:51,600 --> 00:11:54,160
 I'll get fat.

205
00:11:56,080 --> 00:11:58,840
 Everywhere I go, people always tell me I'm getting thinner.

206
00:11:58,840 --> 00:12:00,200
 It's funny, whenever I see people,

207
00:12:00,200 --> 00:12:01,320
 I haven't seen them for a while.

208
00:12:01,320 --> 00:12:04,000
 They keep telling me I'm getting thinner.

209
00:12:04,000 --> 00:12:05,920
 People have been telling me for 15 years

210
00:12:05,920 --> 00:12:07,000
 that I'm getting thinner.

211
00:12:07,000 --> 00:12:12,720
 We worry about these things.

212
00:12:12,720 --> 00:12:15,000
 I'm too thin, I'm too fat.

213
00:12:15,000 --> 00:12:16,640
 Some people, it becomes an obsession,

214
00:12:16,640 --> 00:12:20,840
 and so they become bulimic or anorexic.

215
00:12:20,840 --> 00:12:24,440
 I'm too tall, I'm too short.

216
00:12:24,440 --> 00:12:30,120
 Cling to our thoughts about things.

217
00:12:30,120 --> 00:12:33,560
 We cling to our emotions.

218
00:12:33,560 --> 00:12:36,440
 I'm depressed, and then I identify with it.

219
00:12:36,440 --> 00:12:43,320
 Or I like something, and it's like, I like it.

220
00:12:43,320 --> 00:12:45,920
 You become attached to this desire, and you say, yes,

221
00:12:45,920 --> 00:12:49,120
 got to get that desire, got to follow that desire whenever

222
00:12:49,120 --> 00:12:51,920
 it comes.

223
00:12:51,920 --> 00:12:54,640
 This needs to worry and stress when

224
00:12:54,640 --> 00:13:00,640
 you can't get what you want, when you're denied.

225
00:13:00,640 --> 00:13:05,760
 It needs to anger, to grief, to sorrow, to suffering.

226
00:13:05,760 --> 00:13:07,520
 In consciousness, well, consciousness

227
00:13:07,520 --> 00:13:09,000
 is another one of the neutral ones.

228
00:13:09,000 --> 00:13:13,280
 There's nothing ostensibly problematic about consciousness,

229
00:13:13,280 --> 00:13:13,760
 right?

230
00:13:13,760 --> 00:13:21,120
 You accept that consciousness is like a capsule for all the

231
00:13:21,120 --> 00:13:21,560
 rest.

232
00:13:21,560 --> 00:13:24,790
 If it weren't for consciousness, you couldn't have

233
00:13:24,790 --> 00:13:25,960
 judgments,

234
00:13:25,960 --> 00:13:27,360
 you couldn't have any of the problems.

235
00:13:27,360 --> 00:13:34,400
 It's like if you don't have a spider's web,

236
00:13:34,400 --> 00:13:36,160
 you don't have a spider, no?

237
00:13:36,160 --> 00:13:39,920
 You don't have an ant home, you don't have ants.

238
00:13:39,920 --> 00:13:46,760
 You don't have a breathing ground for, let's say,

239
00:13:46,760 --> 00:13:49,200
 for bacteria or something.

240
00:13:49,200 --> 00:13:53,280
 Like a rusty nail.

241
00:13:53,280 --> 00:13:56,240
 You know how everyone tells you you have to be very careful

242
00:13:56,240 --> 00:13:56,240
,

243
00:13:56,240 --> 00:13:57,920
 you don't step on a rusty nail?

244
00:13:57,920 --> 00:14:01,680
 And if you do, you have to get it cleaned out,

245
00:14:01,680 --> 00:14:03,480
 and you have to get a shot or something.

246
00:14:03,480 --> 00:14:07,440
 You have to get a shot in case you step on a rusty nail.

247
00:14:07,440 --> 00:14:08,880
 When I was younger, I did step.

248
00:14:08,880 --> 00:14:11,320
 I'd have done a farm and lots of rusty nails around.

249
00:14:11,320 --> 00:14:16,680
 I did step on a rusty nail, but I had my tetanus shot.

250
00:14:16,680 --> 00:14:21,760
 When I was in California, I stepped on a rusty something.

251
00:14:21,760 --> 00:14:23,280
 Well, not a metal something.

252
00:14:23,280 --> 00:14:26,520
 I was walking barefoot in Tarzana.

253
00:14:26,520 --> 00:14:32,000
 I went every day to a French restaurant.

254
00:14:32,000 --> 00:14:35,440
 It was the most exotic alms around ever.

255
00:14:35,440 --> 00:14:37,040
 Every day, I would go on alms around,

256
00:14:37,040 --> 00:14:43,240
 and I would go to a French restaurant and a Thai restaurant

257
00:14:43,240 --> 00:14:43,840
.

258
00:14:43,840 --> 00:14:49,160
 And I think some Thai people stopped me on the way as well.

259
00:14:49,160 --> 00:14:54,880
 And then back at the house, some people at the house

260
00:14:54,880 --> 00:14:57,200
 would offer alms as well.

261
00:14:57,200 --> 00:15:00,520
 So every day, I was having waffles or pancakes

262
00:15:00,520 --> 00:15:04,600
 with mounds of real--

263
00:15:04,600 --> 00:15:06,600
 it was high quality.

264
00:15:06,600 --> 00:15:14,200
 This is LA sort of Hollywood, high-so French restaurant,

265
00:15:14,200 --> 00:15:17,440
 because it was owned actually by Thai people.

266
00:15:17,440 --> 00:15:19,480
 But one day, I stepped on a piece of metal,

267
00:15:19,480 --> 00:15:21,400
 and they took me right away to the--

268
00:15:21,400 --> 00:15:22,120
 it was funny.

269
00:15:22,120 --> 00:15:23,760
 They took me to the emergency room.

270
00:15:23,760 --> 00:15:33,480
 And it ended up costing like $500 to what?

271
00:15:33,480 --> 00:15:34,280
 Can't remember.

272
00:15:34,280 --> 00:15:36,240
 Just to have it checked out and then get me a tetanus

273
00:15:36,240 --> 00:15:38,720
 on the thing.

274
00:15:38,720 --> 00:15:40,560
 It was like $500.

275
00:15:40,560 --> 00:15:42,000
 That sounds about right, yes.

276
00:15:42,000 --> 00:15:47,400
 To walk in the door, yes, $500.

277
00:15:47,400 --> 00:15:50,240
 Americans.

278
00:15:50,240 --> 00:15:51,840
 Crazy people, crazy country.

279
00:15:51,840 --> 00:16:00,440
 But my point there was totally off track is--

280
00:16:00,440 --> 00:16:02,200
 there's nothing wrong with rust.

281
00:16:02,200 --> 00:16:03,440
 So it got me thinking.

282
00:16:03,440 --> 00:16:08,280
 I had always thought that rust give you tetanus.

283
00:16:08,280 --> 00:16:10,160
 Rust gives you lochjaw, because if you

284
00:16:10,160 --> 00:16:14,020
 get rust in your bloodstream, it must cause what we call lo

285
00:16:14,020 --> 00:16:14,400
chjaw

286
00:16:14,400 --> 00:16:16,520
 or tetanus, where your body--

287
00:16:16,520 --> 00:16:19,640
 it's a really horrible sickness.

288
00:16:19,640 --> 00:16:21,160
 But it's not actually the case.

289
00:16:21,160 --> 00:16:25,560
 There's a virus known as virus bacteria.

290
00:16:25,560 --> 00:16:29,800
 And the bacteria lives in the rusty metal,

291
00:16:29,800 --> 00:16:33,560
 because rust makes holes in metal.

292
00:16:33,560 --> 00:16:35,360
 So you have a metal nail.

293
00:16:35,360 --> 00:16:38,160
 And when the rust creates pockets,

294
00:16:38,160 --> 00:16:40,000
 and pockets where bacteria can live--

295
00:16:40,000 --> 00:16:43,320
 and one of the bacteria that likes to live in those pockets

296
00:16:43,320 --> 00:16:46,760
 or is potentially living in those pockets

297
00:16:46,760 --> 00:16:49,760
 is the tetanus bacteria--

298
00:16:49,760 --> 00:16:51,600
 I can't remember what they're called.

299
00:16:51,600 --> 00:16:54,160
 And it's just so perfect, because it's

300
00:16:54,160 --> 00:16:56,800
 like injecting yourself with those bacteria

301
00:16:56,800 --> 00:17:00,200
 when you get a nail right deep into your bloodstream.

302
00:17:00,200 --> 00:17:01,520
 That is why you get it.

303
00:17:01,520 --> 00:17:02,520
 You don't always get it.

304
00:17:02,520 --> 00:17:04,080
 It's certainly not from the rest.

305
00:17:04,080 --> 00:17:07,400
 So my point being, consciousness, you

306
00:17:07,400 --> 00:17:08,800
 could argue it's not a problem.

307
00:17:08,800 --> 00:17:10,400
 It's neutral.

308
00:17:10,400 --> 00:17:12,720
 But consciousness is like this rusty nail.

309
00:17:12,720 --> 00:17:17,440
 It's got lots of bad things potentially in it.

310
00:17:17,440 --> 00:17:19,280
 It's very nature.

311
00:17:19,280 --> 00:17:23,920
 Allows it to be a vessel for bad things.

312
00:17:23,920 --> 00:17:27,320
 There's an analogy for you.

313
00:17:27,320 --> 00:17:28,760
 So that's the Dhamma.

314
00:17:28,760 --> 00:17:32,400
 There is some Dhamma today.

315
00:17:32,400 --> 00:17:35,520
 Good teaching.

316
00:17:35,520 --> 00:17:38,200
 You've got to stand in awe of the Buddha,

317
00:17:38,200 --> 00:17:42,240
 how powerful his teachings are.

318
00:17:42,240 --> 00:17:44,920
 I always sound like a religious zealot.

319
00:17:44,920 --> 00:17:47,080
 And I never thought I would be a religious zealot,

320
00:17:47,080 --> 00:17:50,440
 because I always made fun of religious zealots.

321
00:17:50,440 --> 00:17:53,840
 But if they were right, then they

322
00:17:53,840 --> 00:17:57,320
 would be justified to be zealous.

323
00:17:57,320 --> 00:18:02,200
 We're right.

324
00:18:02,200 --> 00:18:08,480
 Don't mean it quite like that.

325
00:18:08,480 --> 00:18:11,560
 It's not worth gloating over for sure.

326
00:18:11,560 --> 00:18:12,360
 But it is awesome.

327
00:18:12,360 --> 00:18:18,840
 Oh, Fernando sent me some subtitles.

328
00:18:18,840 --> 00:18:20,120
 OK, let me do that now.

329
00:18:22,720 --> 00:18:24,360
 Let's do it while we're online.

330
00:18:24,360 --> 00:18:25,880
 He sent me Spanish subtitles.

331
00:18:25,880 --> 00:18:27,440
 This is an example to everyone.

332
00:18:27,440 --> 00:18:31,840
 Wait, no, don't go.

333
00:18:31,840 --> 00:18:37,000
 So what we're going to do--

334
00:18:37,000 --> 00:18:40,640
 find the video.

335
00:18:40,640 --> 00:18:43,600
 [VIDEO PLAYBACK]

336
00:18:43,600 --> 00:18:55,320
 We're still got this old series on how to meditate.

337
00:18:55,320 --> 00:18:59,840
 This one is how to meditate.

338
00:18:59,840 --> 00:19:01,320
 [INAUDIBLE]

339
00:19:01,320 --> 00:19:08,800
 [INAUDIBLE]

340
00:19:10,240 --> 00:19:13,560
 We should really redo these videos again, no?

341
00:19:13,560 --> 00:19:15,080
 How many years old already?

342
00:19:15,080 --> 00:19:19,560
 Set a video language.

343
00:19:19,560 --> 00:19:21,400
 Which language?

344
00:19:21,400 --> 00:19:23,200
 English.

345
00:19:23,200 --> 00:19:26,160
 English or English UK?

346
00:19:26,160 --> 00:19:27,920
 That's funny.

347
00:19:27,920 --> 00:19:30,320
 There's two language choices, three language choices--

348
00:19:30,320 --> 00:19:35,160
 English and English United Kingdom and French Canada.

349
00:19:35,160 --> 00:19:38,520
 Why is America the default English now?

350
00:19:38,520 --> 00:19:41,000
 British aren't English enough?

351
00:19:41,000 --> 00:19:42,920
 Is this through Google?

352
00:19:42,920 --> 00:19:44,680
 Yeah.

353
00:19:44,680 --> 00:19:46,800
 English United Kingdom, that's the default.

354
00:19:46,800 --> 00:19:53,720
 So we've already published several.

355
00:19:53,720 --> 00:19:55,800
 I remember I did a thing back in the day

356
00:19:55,800 --> 00:19:58,560
 where I got people to send me subtitles.

357
00:19:58,560 --> 00:20:01,720
 So we've got English, Indonesian, Portuguese,

358
00:20:01,720 --> 00:20:04,600
 Swedish, and Vietnamese.

359
00:20:04,600 --> 00:20:13,200
 So I'm going to add Fernando's Spanish subtitles.

360
00:20:13,200 --> 00:20:15,720
 Download them.

361
00:20:15,720 --> 00:20:16,720
 Save them.

362
00:20:16,720 --> 00:20:22,400
 How do you access these, Bante?

363
00:20:22,400 --> 00:20:24,640
 Are these on your channel as well?

364
00:20:24,640 --> 00:20:26,680
 You go to CC, I think.

365
00:20:26,680 --> 00:20:27,920
 Oh, OK.

366
00:20:27,920 --> 00:20:32,240
 So we've got Spanish, Latin American, Spanish Mexico,

367
00:20:32,240 --> 00:20:35,000
 and Spanish Spain.

368
00:20:35,000 --> 00:20:36,800
 Fernando's from Spain, I think, right?

369
00:20:36,800 --> 00:20:38,000
 That's what I'm going to say.

370
00:20:38,000 --> 00:20:39,840
 I think Fernando's from Mexico.

371
00:20:39,840 --> 00:20:41,280
 Oh.

372
00:20:41,280 --> 00:20:42,680
 No.

373
00:20:42,680 --> 00:20:43,480
 Is Fernando there?

374
00:20:43,480 --> 00:20:46,120
 Fernando, which country is it?

375
00:20:46,120 --> 00:20:51,160
 Well, per the flags, meditation site Mexico.

376
00:20:51,160 --> 00:20:54,040
 Which language are you using, Spanish Spain or Spanish

377
00:20:54,040 --> 00:20:56,640
 Mexican?

378
00:20:56,640 --> 00:20:58,240
 They're not that different, are they?

379
00:21:01,680 --> 00:21:02,560
 I don't know.

380
00:21:02,560 --> 00:21:06,240
 Most people in my area that speak Spanish are from Puerto

381
00:21:06,240 --> 00:21:06,760
 Rico.

382
00:21:06,760 --> 00:21:10,080
 About 35% of the people in my town are from Puerto Rico.

383
00:21:10,080 --> 00:21:11,680
 And that's a little different as well.

384
00:21:11,680 --> 00:21:23,720
 Fernando said Latin America.

385
00:21:23,720 --> 00:21:24,200
 So what?

386
00:21:24,200 --> 00:21:25,400
 That's different from Spain?

387
00:21:29,720 --> 00:21:33,200
 Spanish, Latin America.

388
00:21:33,200 --> 00:21:33,680
 There we are.

389
00:21:33,680 --> 00:21:38,000
 Upload a file.

390
00:21:52,920 --> 00:22:03,680
 [SPEAKING SPANISH]

391
00:22:03,680 --> 00:22:05,320
 And this is the premier video.

392
00:22:05,320 --> 00:22:16,400
 [SPEAKING SPANISH]

393
00:22:16,400 --> 00:22:22,920
 And premier dugar de parabra meditation, qued.

394
00:22:22,920 --> 00:22:25,720
 Qued.

395
00:22:25,720 --> 00:22:27,560
 Qued.

396
00:22:27,560 --> 00:22:30,360
 That must be can.

397
00:22:30,360 --> 00:22:37,680
 Very much like Latin.

398
00:22:37,680 --> 00:22:39,760
 Anyway, qued is probably can.

399
00:22:43,680 --> 00:22:46,680
 You know, 29% of English comes from Latin.

400
00:22:46,680 --> 00:22:49,960
 Learn that this way.

401
00:22:49,960 --> 00:23:05,320
 When Latin is so impossible, it comes from [SPEAKING SPAN

402
00:23:05,320 --> 00:23:07,840
ISH]

403
00:23:07,840 --> 00:23:09,560
 OK, is that--

404
00:23:09,560 --> 00:23:12,520
 I think-- oh, no, it's not published.

405
00:23:12,520 --> 00:23:16,440
 OK, it's up.

406
00:23:16,440 --> 00:23:18,920
 Check it out.

407
00:23:18,920 --> 00:23:21,120
 Check it out, everyone.

408
00:23:21,120 --> 00:23:22,800
 Go to my first tab and meditate it.

409
00:23:22,800 --> 00:23:23,760
 Meditate it here.

410
00:23:23,760 --> 00:23:29,120
 I need to change your language.

411
00:23:29,120 --> 00:23:34,920
 Voila.

412
00:23:34,920 --> 00:23:35,880
 Begin the day.

413
00:23:35,880 --> 00:23:39,000
 Hi.

414
00:23:39,000 --> 00:23:41,320
 Welcome to the series of videos.

415
00:23:41,320 --> 00:23:46,560
 Oh, I have to remove that.

416
00:23:46,560 --> 00:23:53,680
 You know, it has a thing talking about the DVD.

417
00:23:53,680 --> 00:23:56,760
 That's why people are still contacting me about the DVD.

418
00:23:56,760 --> 00:23:59,040
 Because these videos, I'll talk about it.

419
00:23:59,040 --> 00:24:02,960
 I'll have a thing about it.

420
00:24:02,960 --> 00:24:04,400
 To remove that.

421
00:24:04,400 --> 00:24:05,920
 We're not doing the DVDs anymore.

422
00:24:05,920 --> 00:24:07,400
 [CLICK]

423
00:24:07,400 --> 00:24:24,680
 I think the website may mention the DVD as well.

424
00:24:24,680 --> 00:24:25,680
 So--

425
00:24:25,680 --> 00:24:27,440
 Shouldn't get rid of it.

426
00:24:27,440 --> 00:24:30,080
 Yeah.

427
00:24:30,080 --> 00:24:34,440
 It also mentions about the download again of BitTorrent.

428
00:24:34,440 --> 00:24:36,840
 Is that still--

429
00:24:36,840 --> 00:24:38,000
 I don't know.

430
00:24:38,000 --> 00:24:39,440
 Is that still current?

431
00:24:39,440 --> 00:24:41,360
 I don't think we can get rid of it.

432
00:24:41,360 --> 00:24:42,280
 It's all on YouTube.

433
00:24:42,280 --> 00:24:43,040
 That's good enough.

434
00:24:43,040 --> 00:24:43,960
 Who needs a DVD?

435
00:24:43,960 --> 00:24:56,160
 Yeah, it says our how to meditate DVD

436
00:24:56,160 --> 00:24:58,760
 is available via BitTorrent.

437
00:24:58,760 --> 00:25:02,840
 So I'll just get rid of that.

438
00:25:02,840 --> 00:25:04,320
 [CLICK]

439
00:25:04,320 --> 00:25:25,040
 OK, so how are we doing?

440
00:25:25,040 --> 00:25:26,880
 Any questions?

441
00:25:26,880 --> 00:25:27,640
 No.

442
00:25:27,640 --> 00:25:31,040
 You have answered all the questions, Juan Te.

443
00:25:31,040 --> 00:25:32,680
 Finally come to that point, though.

444
00:25:32,680 --> 00:25:36,920
 That's a good thing, right?

445
00:25:36,920 --> 00:25:41,200
 It's a good thing.

446
00:25:41,200 --> 00:25:45,000
 It's fine.

447
00:25:45,000 --> 00:25:54,160
 Tomorrow we're going to try the meditation mob thing again.

448
00:25:54,160 --> 00:25:55,720
 We'll see if that works.

449
00:25:55,720 --> 00:25:56,440
 How that works.

450
00:26:00,000 --> 00:26:03,360
 This time people are actually going to come out.

451
00:26:03,360 --> 00:26:05,680
 What else?

452
00:26:05,680 --> 00:26:11,440
 Oh, I think Aruna mentioned that the robe arrived.

453
00:26:11,440 --> 00:26:12,240
 Sorry?

454
00:26:12,240 --> 00:26:14,640
 I believe Aruna mentioned that the robe arrived.

455
00:26:14,640 --> 00:26:18,040
 Yeah, the robe got here.

456
00:26:18,040 --> 00:26:20,120
 Robes, I guess, it's a set, right?

457
00:26:20,120 --> 00:26:20,620
 Yes.

458
00:26:20,620 --> 00:26:24,100
 [BARKING]

459
00:26:24,100 --> 00:26:39,940
 OK, well, I'm going to go then.

460
00:26:39,940 --> 00:26:43,340
 Thank you all for coming out, for tuning in,

461
00:26:43,340 --> 00:26:44,780
 for practicing with us.

462
00:26:44,780 --> 00:26:48,100
 Big, long list of people.

463
00:26:48,100 --> 00:26:49,380
 Have a good night.

464
00:26:49,380 --> 00:26:52,140
 And thank you, Robin, for joining me.

465
00:26:52,140 --> 00:26:53,980
 Thank you, Pante.

466
00:26:53,980 --> 00:26:55,540
 You're welcome.

467
00:26:57,300 --> 00:26:59,780
 [LAUGHTER]

