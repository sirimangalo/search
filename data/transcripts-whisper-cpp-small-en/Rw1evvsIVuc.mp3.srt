1
00:00:00,000 --> 00:00:04,430
 Hello and welcome back to our study of the Nama Pada. Today

2
00:00:04,430 --> 00:00:06,000
 we continue on with verse

3
00:00:06,000 --> 00:00:13,000
 164 which reads as follows.

4
00:00:13,000 --> 00:00:22,250
 Yosasanang arahatang ariyanang dhammaji vinam patikosati d

5
00:00:22,250 --> 00:00:24,000
ummedho

6
00:00:24,000 --> 00:00:36,000
 titing misaya pabhikam palani katakaseva ata gataya pānti

7
00:00:36,000 --> 00:00:47,000
 which means yo patikosati, yo dummedho patikosati, whatever

8
00:00:47,000 --> 00:00:53,000
 fool, whatever person

9
00:00:53,000 --> 00:01:08,000
 insults the religion of the enlightened ones, of the noble

10
00:01:08,000 --> 00:01:13,000
 religion, noble teaching.

11
00:01:13,000 --> 00:01:25,970
 The dhammaji vinam, the religion of those who live the holy

12
00:01:25,970 --> 00:01:27,000
 life,

13
00:01:27,000 --> 00:01:32,000
 live according to righteousness, the ariyans.

14
00:01:32,000 --> 00:01:45,700
 Whoever insults it, the fool, dummedha, titing misaya, tit

15
00:01:45,700 --> 00:01:49,000
ing misaya pabhikam,

16
00:01:49,000 --> 00:01:57,000
 because of the evil views, dependent on their evil views.

17
00:01:57,000 --> 00:02:04,000
 So they have evil views, so they insult the noble way.

18
00:02:04,000 --> 00:02:13,450
 Just like the fruit of the katakari, palani katakaseva, ata

19
00:02:13,450 --> 00:02:15,000
 gataya pānti,

20
00:02:15,000 --> 00:02:19,380
 they bear fruit, the bearing of fruit or the fruit that

21
00:02:19,380 --> 00:02:21,000
 comes from their act,

22
00:02:21,000 --> 00:02:27,000
 destroys themselves.

23
00:02:27,000 --> 00:02:32,330
 So there's this reed, the kataka reed, that is so, when it

24
00:02:32,330 --> 00:02:33,000
 bears fruit,

25
00:02:33,000 --> 00:02:37,000
 the fruit is so heavy that the reed snaps.

26
00:02:37,000 --> 00:02:42,810
 Such a person who insults the reviles the good way, the

27
00:02:42,810 --> 00:02:45,000
 noble way,

28
00:02:45,000 --> 00:02:51,000
 and has evil views, destroys themselves.

29
00:02:51,000 --> 00:02:56,000
 Just like that, the fruit of their evil.

30
00:02:56,000 --> 00:03:02,000
 So this was taught in regards to the elder kala.

31
00:03:02,000 --> 00:03:08,000
 Kala was a monk who lived in Sawati.

32
00:03:08,000 --> 00:03:16,000
 And the story goes that there was a certain woman,

33
00:03:16,000 --> 00:03:21,000
 maybe an elderly woman, who looked after him

34
00:03:21,000 --> 00:03:24,000
 with the tenderness of a mother for a son.

35
00:03:24,000 --> 00:03:28,000
 So she took great care of him.

36
00:03:28,000 --> 00:03:32,350
 And one day, her neighbors went to hear the Buddha teach

37
00:03:32,350 --> 00:03:34,000
 the dhamma,

38
00:03:34,000 --> 00:03:38,490
 and they said, "Oh, how wonderful is it to hear the

39
00:03:38,490 --> 00:03:42,000
 teachings of the Buddha, how great is it?"

40
00:03:42,000 --> 00:03:44,510
 And so hearing this, she says to the elder, she said, "What

41
00:03:44,510 --> 00:03:45,000
 do you think?

42
00:03:45,000 --> 00:03:49,000
 Should I go and listen to the Buddha teach someday?"

43
00:03:49,000 --> 00:03:53,000
 And they said, "No, no, no, you shouldn't."

44
00:03:53,000 --> 00:03:56,000
 And again, she started thinking about this, and so she

45
00:03:56,000 --> 00:03:56,000
 asked him again,

46
00:03:56,000 --> 00:03:59,280
 "Well, why not?" And he said, "Oh, no, no, it's not worth

47
00:03:59,280 --> 00:04:00,000
 your time."

48
00:04:00,000 --> 00:04:03,940
 He wouldn't give her a good reason, but he kept dissuading

49
00:04:03,940 --> 00:04:05,000
 her from it.

50
00:04:05,000 --> 00:04:12,720
 And we're told that his reasoning was that if she went to

51
00:04:12,720 --> 00:04:14,000
 hear the Buddha teach,

52
00:04:14,000 --> 00:04:17,000
 she would have no use for him.

53
00:04:17,000 --> 00:04:19,650
 Here he was providing her with her only source of the dham

54
00:04:19,650 --> 00:04:20,000
ma.

55
00:04:20,000 --> 00:04:23,000
 Well, if she went to see the Buddha, he knew it.

56
00:04:23,000 --> 00:04:27,000
 He saw what happened then when they heard the Buddha teach.

57
00:04:27,000 --> 00:04:29,000
 They got great faith from the Buddha.

58
00:04:29,000 --> 00:04:34,000
 And then in comparison, they would look at the other monks

59
00:04:34,000 --> 00:04:34,000
 and say,

60
00:04:34,000 --> 00:04:38,050
 "These other monks, they would only have eyes for the

61
00:04:38,050 --> 00:04:39,000
 Buddha."

62
00:04:39,000 --> 00:04:45,000
 This happened a lot in my monastery. It's kind of funny.

63
00:04:45,000 --> 00:04:51,230
 Because our headmaster is just so esteemed that most of the

64
00:04:51,230 --> 00:04:52,000
 other monks actually starved

65
00:04:52,000 --> 00:04:58,020
 or were very poorly taken care of. It was very hard to

66
00:04:58,020 --> 00:05:00,000
 survive in some ways

67
00:05:00,000 --> 00:05:05,010
 as a monk in my monastery because everybody was a very rich

68
00:05:05,010 --> 00:05:06,000
 monastery,

69
00:05:06,000 --> 00:05:12,000
 but all the money and support went only to the top.

70
00:05:12,000 --> 00:05:23,720
 So for those monks looking for gain, it was very hard to be

71
00:05:23,720 --> 00:05:27,000
 well off as a monk there.

72
00:05:27,000 --> 00:05:29,560
 Whereas, I mean, that might sound strange because, of

73
00:05:29,560 --> 00:05:32,000
 course, monks are supposed to be very content

74
00:05:32,000 --> 00:05:34,000
 with little or nothing.

75
00:05:34,000 --> 00:05:39,760
 But in some places in Thailand, monks were able to be very

76
00:05:39,760 --> 00:05:42,000
 well taken care of.

77
00:05:42,000 --> 00:05:47,580
 So this monk was quite well taken care of and didn't want

78
00:05:47,580 --> 00:05:49,000
 to lose this.

79
00:05:49,000 --> 00:05:52,300
 There's also a reason why sometimes monks would leave a

80
00:05:52,300 --> 00:05:53,000
 monastery

81
00:05:53,000 --> 00:05:54,000
 and go off and start their own.

82
00:05:54,000 --> 00:05:57,000
 It's because when you're alone, well, you're the only monk

83
00:05:57,000 --> 00:06:00,720
 and the only religious teacher, it's very easy to get along

84
00:06:00,720 --> 00:06:01,000
.

85
00:06:01,000 --> 00:06:03,710
 And this is all, certainly I don't want to give you the

86
00:06:03,710 --> 00:06:06,000
 idea that I'm condoning this

87
00:06:06,000 --> 00:06:12,490
 or encouraging it. It's great danger and it's very wrong to

88
00:06:12,490 --> 00:06:15,000
 be even thinking in this sort of way.

89
00:06:15,000 --> 00:06:21,000
 This monk was very much in the wrong for his thought

90
00:06:21,000 --> 00:06:24,000
 and much more for his actions in dissuading this woman.

91
00:06:24,000 --> 00:06:32,990
 Finally, she had enough and she had her daughter bring food

92
00:06:32,990 --> 00:06:34,000
 to the monk

93
00:06:34,000 --> 00:06:37,000
 and take care of this monk.

94
00:06:37,000 --> 00:06:42,000
 Do whatever you can to make sure he's happy.

95
00:06:42,000 --> 00:06:49,000
 I'm going to go to Jaita on it and hear the Buddha teach.

96
00:06:49,000 --> 00:06:53,080
 And so the elder was in this, I guess he was staying in the

97
00:06:53,080 --> 00:06:55,000
 monastery in the city.

98
00:06:55,000 --> 00:06:58,000
 There would be, I guess, places to stay in the city.

99
00:06:58,000 --> 00:07:01,490
 And when this monk saw him coming, when this monk saw her

100
00:07:01,490 --> 00:07:04,000
 coming, bringing him food,

101
00:07:04,000 --> 00:07:07,000
 he asked her, "Oh, where's your mother?"

102
00:07:07,000 --> 00:07:15,000
 She said, "Oh, she's gone to hear the Buddha teach."

103
00:07:15,000 --> 00:07:21,240
 And as soon as the monk heard this, he got consumed with

104
00:07:21,240 --> 00:07:23,000
 anger and fear

105
00:07:23,000 --> 00:07:29,180
 that she had disregarded his words, disregarded his evil

106
00:07:29,180 --> 00:07:30,000
 advice,

107
00:07:30,000 --> 00:07:34,000
 and he ran or went quite quickly to Jaita Vana.

108
00:07:34,000 --> 00:07:39,800
 And when he saw this woman listening to the Buddha teach

109
00:07:39,800 --> 00:07:41,000
 the Dhamma,

110
00:07:41,000 --> 00:07:45,740
 this anger and just blind range, because can you imagine

111
00:07:45,740 --> 00:07:47,000
 having the audacity

112
00:07:47,000 --> 00:07:52,770
 to do what he did, which is, he goes to the Buddha and says

113
00:07:52,770 --> 00:07:54,000
 to the Buddha,

114
00:07:54,000 --> 00:07:57,520
 "Bhante, this stupid woman does not understand your subtle

115
00:07:57,520 --> 00:08:02,000
 discourse on the Dhamma."

116
00:08:02,000 --> 00:08:04,000
 How awful, no?

117
00:08:04,000 --> 00:08:11,210
 In front of this woman scold her like that and discouraged

118
00:08:11,210 --> 00:08:14,000
 the Buddha from teaching

119
00:08:14,000 --> 00:08:18,350
 as though the Buddha didn't know how to teach people to

120
00:08:18,350 --> 00:08:20,000
 their own level.

121
00:08:20,000 --> 00:08:23,180
 And he says, "Teacher instead," he asked the audacity to

122
00:08:23,180 --> 00:08:26,000
 tell the Buddha what to teach her.

123
00:08:26,000 --> 00:08:31,670
 "Teacher the duty of almsgiving and moral precepts, Dhamma

124
00:08:31,670 --> 00:08:33,000
 and Sila."

125
00:08:33,000 --> 00:08:36,410
 Basically what he's saying is, "Don't teach her anything

126
00:08:36,410 --> 00:08:37,000
 deep."

127
00:08:37,000 --> 00:08:40,320
 Why? Because I can't teach her anything deep because I'm an

128
00:08:40,320 --> 00:08:43,000
 evil, vicious, foolish monk

129
00:08:43,000 --> 00:08:49,000
 and I'm basically useless.

130
00:08:49,000 --> 00:08:54,520
 So if you teach her the great Dhamma, what use will she

131
00:08:54,520 --> 00:08:55,000
 have for me?

132
00:08:55,000 --> 00:08:58,110
 I mean, he doesn't say this, but this is of course the un

133
00:08:58,110 --> 00:08:59,000
spoken reason.

134
00:08:59,000 --> 00:09:02,460
 It's because he's really a terrible person and he doesn't

135
00:09:02,460 --> 00:09:04,000
 want the comparison.

136
00:09:04,000 --> 00:09:07,520
 So if the Buddha just teaches her simple things, things

137
00:09:07,520 --> 00:09:10,000
 that won't actually make her enlightened,

138
00:09:10,000 --> 00:09:12,350
 of course the worst for him would be if she were to become

139
00:09:12,350 --> 00:09:13,000
 enlightened

140
00:09:13,000 --> 00:09:17,000
 because then she'd turn around and look at him and say,

141
00:09:17,000 --> 00:09:23,000
 "Why are you teaching me, you useless man?"

142
00:09:23,000 --> 00:09:28,010
 But if the Buddha were only to teach her charity and

143
00:09:28,010 --> 00:09:31,000
 morality, then he'd have a chance.

144
00:09:31,000 --> 00:09:34,370
 Because those are simple things that simple people can

145
00:09:34,370 --> 00:09:35,000
 teach.

146
00:09:35,000 --> 00:09:37,970
 It's not easy to teach about vipassana or insight

147
00:09:37,970 --> 00:09:39,000
 meditation.

148
00:09:39,000 --> 00:09:43,000
 It's not easy to teach people mindfulness.

149
00:09:43,000 --> 00:09:46,420
 It's easy to say the word mindfulness, be mindful, but to

150
00:09:46,420 --> 00:09:48,000
 actually understand it.

151
00:09:48,000 --> 00:09:51,580
 You know, all these things that we talk about, the paradigm

152
00:09:51,580 --> 00:09:54,000
 shift and the way of looking at the world

153
00:09:54,000 --> 00:09:59,000
 in terms of ultimate reality, seeing experiential reality

154
00:09:59,000 --> 00:10:00,000
 and understanding.

155
00:10:00,000 --> 00:10:04,810
 It's not just intelligence. It takes vision. It takes

156
00:10:04,810 --> 00:10:06,000
 insight.

157
00:10:06,000 --> 00:10:12,510
 It takes concentration or focus, which means your mind has

158
00:10:12,510 --> 00:10:14,000
 to be to some extent pure.

159
00:10:14,000 --> 00:10:18,220
 If your mind is full of evil, unwholesome desires, it's

160
00:10:18,220 --> 00:10:20,000
 very difficult to think even,

161
00:10:20,000 --> 00:10:26,040
 to resonate with concepts like impermanence, suffering, non

162
00:10:26,040 --> 00:10:27,000
-self.

163
00:10:27,000 --> 00:10:30,000
 Not easy.

164
00:10:30,000 --> 00:10:35,000
 So he compounds his evil by not only dissuading this woman

165
00:10:35,000 --> 00:10:36,000
 from hearing the Buddha,

166
00:10:36,000 --> 00:10:40,090
 but now reviling her in front of the Buddha and ordering

167
00:10:40,090 --> 00:10:41,000
 the Buddha,

168
00:10:41,000 --> 00:10:45,000
 instructing the Buddha on how to teach.

169
00:10:45,000 --> 00:10:48,000
 Very evil.

170
00:10:48,000 --> 00:10:52,000
 So the Buddha looks at him and says,

171
00:10:52,000 --> 00:10:57,000
 "Mogapuri" is, I'm assuming, the word he uses, let's see.

172
00:10:57,000 --> 00:11:01,000
 Oh, he says, "Dupanyo" you stupid person.

173
00:11:01,000 --> 00:11:05,210
 "Panya" is wisdom. "Dupanya" means someone who has no

174
00:11:05,210 --> 00:11:06,000
 wisdom.

175
00:11:06,000 --> 00:11:11,000
 "Papikanditinisaaya buddhanamsasanampatikosati"

176
00:11:11,000 --> 00:11:14,000
 He's curious that the Buddha uses these words.

177
00:11:14,000 --> 00:11:19,200
 He says, "Based on your evil views, you revile the religion

178
00:11:19,200 --> 00:11:21,000
 of the Buddhas."

179
00:11:21,000 --> 00:11:24,720
 Because he's not actually. That's one thing he's not

180
00:11:24,720 --> 00:11:26,000
 explicitly doing.

181
00:11:26,000 --> 00:11:32,000
 And so this speech is meant to connect it to the verse.

182
00:11:32,000 --> 00:11:34,000
 And if you look at this, you might think,

183
00:11:34,000 --> 00:11:37,000
 just the commentator trying to fit a different story

184
00:11:37,000 --> 00:11:40,000
 in with this verse, which is totally unrelated.

185
00:11:40,000 --> 00:11:45,220
 You might think that way, because the verse is about rev

186
00:11:45,220 --> 00:11:46,000
iling the Dhamma,

187
00:11:46,000 --> 00:11:48,000
 which is an important concept that we'll talk about.

188
00:11:48,000 --> 00:11:55,360
 But the story is about evil and the fear of losing one's

189
00:11:55,360 --> 00:11:56,000
 gain, really.

190
00:11:56,000 --> 00:12:01,000
 Losing one's income, the fear of the Dhamma.

191
00:12:01,000 --> 00:12:07,000
 But we can be charitable to the commentator.

192
00:12:07,000 --> 00:12:10,000
 And there's an easy way to understand this.

193
00:12:10,000 --> 00:12:14,390
 The Buddha is trying to, and in many cases it appears that

194
00:12:14,390 --> 00:12:16,000
 this is the way it was,

195
00:12:16,000 --> 00:12:20,000
 the Buddha was trying to redirect things.

196
00:12:20,000 --> 00:12:24,260
 Often the Buddha's response to a problem is not to address

197
00:12:24,260 --> 00:12:25,000
 it head on.

198
00:12:25,000 --> 00:12:31,490
 Because just arguing with someone or debating or fighting

199
00:12:31,490 --> 00:12:33,000
 with someone

200
00:12:33,000 --> 00:12:38,000
 may mean that in the end you're right and they're wrong.

201
00:12:38,000 --> 00:12:40,000
 But it may not fix the problem.

202
00:12:40,000 --> 00:12:42,000
 What is the true problem here?

203
00:12:42,000 --> 00:12:45,000
 And so the Buddha hits the heart of the matter

204
00:12:45,000 --> 00:12:52,000
 by saying, "In doing this you are insulting the Dhamma.

205
00:12:52,000 --> 00:12:58,310
 You are reviling it. You are in some way attacking the Dham

206
00:12:58,310 --> 00:12:59,000
ma."

207
00:12:59,000 --> 00:13:04,440
 So rather than actually point out that he's just afraid of

208
00:13:04,440 --> 00:13:06,000
 losing his gain,

209
00:13:06,000 --> 00:13:12,600
 he says, "Really what's at stake, what's at work here, at

210
00:13:12,600 --> 00:13:15,000
 playing, at work here,

211
00:13:15,000 --> 00:13:23,000
 is your evil outlook, your corrupt way of looking at things

212
00:13:23,000 --> 00:13:23,000
.

213
00:13:23,000 --> 00:13:26,000
 And you're only going to hurt yourself.

214
00:13:26,000 --> 00:13:30,400
 You think this is to your gain to prevent this woman from

215
00:13:30,400 --> 00:13:32,000
 hearing the Dhamma,

216
00:13:32,000 --> 00:13:37,000
 but it's only to your loss."

217
00:13:37,000 --> 00:13:42,900
 So he that attacks the Dhamma, because that's what's going

218
00:13:42,900 --> 00:13:44,000
 on,

219
00:13:44,000 --> 00:13:47,000
 this woman is hearing the Dhamma, this is the transmission

220
00:13:47,000 --> 00:13:48,000
 of the Dhamma,

221
00:13:48,000 --> 00:13:52,540
 the spreading, the arising, the growth, the planting of the

222
00:13:52,540 --> 00:13:54,000
 seed of the Dhamma.

223
00:13:54,000 --> 00:13:56,000
 This is what's happening.

224
00:13:56,000 --> 00:14:05,000
 And he's attacking it like a soldier attacking a fortress.

225
00:14:05,000 --> 00:14:09,800
 He's attacking the spreading of the Dhamma, the Dhamma-das

226
00:14:09,800 --> 00:14:13,000
ana, the teaching of the Dhamma.

227
00:14:13,000 --> 00:14:17,690
 So you can say that he's attacking, that he's still hard to

228
00:14:17,690 --> 00:14:19,000
 see how he's insulting the Dhamma,

229
00:14:19,000 --> 00:14:24,000
 but you can think of it as an insult to...

230
00:14:24,000 --> 00:14:27,280
 Well, first off, suggest that the Buddha doesn't know how

231
00:14:27,280 --> 00:14:28,000
 to teach.

232
00:14:28,000 --> 00:14:36,850
 Second off, to focus or to insist on the teachings only of

233
00:14:36,850 --> 00:14:40,000
 morality and charity,

234
00:14:40,000 --> 00:14:46,000
 which though useful and quite Buddhist are by no means deep

235
00:14:46,000 --> 00:14:49,000
 or profound or conducive

236
00:14:49,000 --> 00:14:55,000
 or leading them in and of themselves to enlightenment.

237
00:14:55,000 --> 00:14:58,000
 And then the Buddha teaches this verse.

238
00:14:58,000 --> 00:15:01,000
 So that's the story.

239
00:15:01,000 --> 00:15:03,000
 So what lessons we can gain?

240
00:15:03,000 --> 00:15:06,490
 Again, I think we can look at the two different sides of

241
00:15:06,490 --> 00:15:07,000
 this,

242
00:15:07,000 --> 00:15:10,000
 the story side and the verse side.

243
00:15:10,000 --> 00:15:15,000
 In reference to the story, it's more related to evil views

244
00:15:15,000 --> 00:15:20,000
 and how people make use of Buddhism.

245
00:15:20,000 --> 00:15:29,260
 This monk is clearly not only greedy, but allowing his

246
00:15:29,260 --> 00:15:30,000
 greed.

247
00:15:30,000 --> 00:15:33,000
 I know that's the wrong word, but let's use it for now.

248
00:15:33,000 --> 00:15:40,000
 Allowing his greed to inform his actions.

249
00:15:40,000 --> 00:15:44,000
 So why I say allowing is probably the wrong word is because

250
00:15:44,000 --> 00:15:50,000
 it's more like based on his desire.

251
00:15:50,000 --> 00:15:51,000
 And this is the key point.

252
00:15:51,000 --> 00:15:55,000
 This is an important point to talk about.

253
00:15:55,000 --> 00:16:01,350
 It's common to think, I mean, an ordinary understanding of

254
00:16:01,350 --> 00:16:02,000
 desire

255
00:16:02,000 --> 00:16:07,000
 that exists independent of our other emotions.

256
00:16:07,000 --> 00:16:12,000
 You want something and that's it.

257
00:16:12,000 --> 00:16:17,000
 You want it and either you get it or you don't get it.

258
00:16:17,000 --> 00:16:21,270
 We don't connect that the wanting, that our desires are

259
00:16:21,270 --> 00:16:26,000
 very much related to our aversion

260
00:16:26,000 --> 00:16:35,000
 and our fear and our jealousy, arrogance, conceit.

261
00:16:35,000 --> 00:16:43,000
 That our desires corrupt our minds.

262
00:16:43,000 --> 00:16:46,080
 We have this idea that I can want things and still be a

263
00:16:46,080 --> 00:16:49,000
 kind and nice and generous person to others.

264
00:16:49,000 --> 00:16:53,000
 That it doesn't actually diminish my purity of mind.

265
00:16:53,000 --> 00:16:57,470
 We don't connect. We don't make a connection, generally

266
00:16:57,470 --> 00:16:58,000
 speaking.

267
00:16:58,000 --> 00:17:00,570
 The fact that I want things, the fact that I like certain

268
00:17:00,570 --> 00:17:01,000
 things,

269
00:17:01,000 --> 00:17:04,730
 what does that have to do with whether I'm an evil or a

270
00:17:04,730 --> 00:17:06,000
 good person?

271
00:17:06,000 --> 00:17:10,000
 What does that have to do with how I treat others?

272
00:17:10,000 --> 00:17:15,090
 And this is an important example, or it's a good example of

273
00:17:15,090 --> 00:17:16,000
 this important concept,

274
00:17:16,000 --> 00:17:18,000
 that there is a connection.

275
00:17:18,000 --> 00:17:23,000
 That as with all things, this is the reason for the Buddha

276
00:17:23,000 --> 00:17:26,000
 to take up the concept of karma

277
00:17:26,000 --> 00:17:30,000
 and apply the word karma in a Buddhist sense.

278
00:17:30,000 --> 00:17:34,000
 To show that there are consequences.

279
00:17:34,000 --> 00:17:36,430
 The consequences that we didn't realize, there are

280
00:17:36,430 --> 00:17:37,000
 connections,

281
00:17:37,000 --> 00:17:44,000
 causal connections between things like desire and cruelty.

282
00:17:44,000 --> 00:17:47,000
 How awful this man is to this woman.

283
00:17:47,000 --> 00:17:51,260
 Can you imagine knowing what we know about how great the

284
00:17:51,260 --> 00:17:53,000
 Buddha's teaching is?

285
00:17:53,000 --> 00:17:58,820
 What a horrible thing to do to prevent, to do whatever you

286
00:17:58,820 --> 00:18:01,000
 can to prevent someone from hearing it.

287
00:18:01,000 --> 00:18:05,000
 And how blind you have to be. It's easy to see.

288
00:18:05,000 --> 00:18:10,040
 This man's focus was so strongly on the things that he

289
00:18:10,040 --> 00:18:11,000
 liked.

290
00:18:11,000 --> 00:18:15,000
 He had cultivated such an addiction that it blinded him.

291
00:18:15,000 --> 00:18:17,300
 That he probably didn't even think about whether it was

292
00:18:17,300 --> 00:18:18,000
 good or evil.

293
00:18:18,000 --> 00:18:22,000
 His mind was so clouded.

294
00:18:22,000 --> 00:18:24,000
 That is what you see, you know?

295
00:18:24,000 --> 00:18:26,000
 It's one thing to want something.

296
00:18:26,000 --> 00:18:31,720
 And to know that you're feeling that you don't want to

297
00:18:31,720 --> 00:18:35,000
 share it with someone else.

298
00:18:35,000 --> 00:18:40,000
 That you don't want to lose it for someone else's gain.

299
00:18:40,000 --> 00:18:46,900
 Basically, to put it in general terms, if this woman gains

300
00:18:46,900 --> 00:18:48,000
 something,

301
00:18:48,000 --> 00:18:51,000
 then he's going to lose something.

302
00:18:51,000 --> 00:18:55,000
 So, ordinarily we weigh these things.

303
00:18:55,000 --> 00:19:00,350
 We think, "Oh, if this woman goes to the Buddha, I'm going

304
00:19:00,350 --> 00:19:03,000
 to lose my support."

305
00:19:03,000 --> 00:19:08,110
 But of course, an ordinary human being with even an ounce

306
00:19:08,110 --> 00:19:12,000
 or a shred of decency would feel ashamed

307
00:19:12,000 --> 00:19:17,520
 and acknowledge that it's much more important that this

308
00:19:17,520 --> 00:19:21,000
 woman goes to talk to the Buddha.

309
00:19:21,000 --> 00:19:27,000
 But you see this. It gets to the point where it blinds you.

310
00:19:27,000 --> 00:19:30,000
 I think you have to say that it's on another level.

311
00:19:30,000 --> 00:19:33,000
 And this is why the Buddha brings up the idea of views.

312
00:19:33,000 --> 00:19:36,000
 Because your views blind you.

313
00:19:36,000 --> 00:19:40,530
 This person's view, and it's an odd word, but if we think

314
00:19:40,530 --> 00:19:43,000
 of it more as inability to see his blindness.

315
00:19:43,000 --> 00:19:46,000
 He is blinded, right? He can't see.

316
00:19:46,000 --> 00:19:49,000
 His view is this. This is his view.

317
00:19:49,000 --> 00:19:58,000
 And in being blind, or because of his blindness,

318
00:19:58,000 --> 00:20:06,000
 he does whatever he can to stop this woman from benefiting.

319
00:20:06,000 --> 00:20:16,000
 But the point for us, I mean, this is a good lesson for us

320
00:20:16,000 --> 00:20:18,000
 to think of this man.

321
00:20:18,000 --> 00:20:28,000
 A much better lesson is to ponder and consider the

322
00:20:28,000 --> 00:20:35,000
 immediate relationship between things like desire

323
00:20:35,000 --> 00:20:44,240
 and our lack of compassion, stinginess, jealousy, avarice,

324
00:20:44,240 --> 00:20:51,000
 envy, these kind of things, and cruelty.

325
00:20:51,000 --> 00:20:56,300
 That you can't just go ahead and want and want and want and

326
00:20:56,300 --> 00:20:59,000
 still be a kind and gentle person.

327
00:20:59,000 --> 00:21:02,500
 I mean, actually the way it is, the more you want it, it

328
00:21:02,500 --> 00:21:04,000
 starts to suck away.

329
00:21:04,000 --> 00:21:07,000
 It starts to eat away at your goodness.

330
00:21:07,000 --> 00:21:11,030
 You can be a good person and start to want and to like

331
00:21:11,030 --> 00:21:12,000
 things.

332
00:21:12,000 --> 00:21:16,680
 But your wants and your needs, the reason why we don't see

333
00:21:16,680 --> 00:21:22,000
 this is because ordinary life is full of evil.

334
00:21:22,000 --> 00:21:27,200
 Ordinary human life, we don't think of it this way. And it

335
00:21:27,200 --> 00:21:30,200
 only really makes sense if you actually take the time to

336
00:21:30,200 --> 00:21:33,000
 meditate and become sensitive.

337
00:21:33,000 --> 00:21:35,990
 Because that's what it is. If you're living in a cesspool,

338
00:21:35,990 --> 00:21:43,000
 you aren't sensitive to dirt, to filth.

339
00:21:43,000 --> 00:21:49,860
 But once you begin to purify your mind, like a person who's

340
00:21:49,860 --> 00:21:54,000
 left the cesspool, taken a shower,

341
00:21:54,000 --> 00:21:57,460
 and when they go anywhere near the cesspool, they're

342
00:21:57,460 --> 00:22:02,000
 completely revolted, you become more sensitive to it.

343
00:22:02,000 --> 00:22:04,630
 I mean, good people are more. This is what we talked about

344
00:22:04,630 --> 00:22:08,000
 last time. Good people are quite sensitive to evil.

345
00:22:08,000 --> 00:22:14,000
 Evil people are not sensitive to evil. They're quite

346
00:22:14,000 --> 00:22:20,000
 sensitive to good and allergic to it, we might say.

347
00:22:20,000 --> 00:22:23,000
 And this is a really important point with craving.

348
00:22:23,000 --> 00:22:27,660
 Craving isn't so, for desire isn't so much about the

349
00:22:27,660 --> 00:22:29,000
 consequences.

350
00:22:29,000 --> 00:22:32,150
 You know, in terms of, you know, later you might not get

351
00:22:32,150 --> 00:22:35,000
 what you want, you'll be upset.

352
00:22:35,000 --> 00:22:39,000
 That's important, but much more important is the very act,

353
00:22:39,000 --> 00:22:42,990
 the very nature of the act, or the state of wanting

354
00:22:42,990 --> 00:22:44,000
 something.

355
00:22:44,000 --> 00:22:50,000
 And that it corrupts the mind. It has an immediate effect,

356
00:22:50,000 --> 00:22:54,720
 and it has a direct relationship with your clarity, your

357
00:22:54,720 --> 00:22:58,000
 purity, your goodness of heart.

358
00:22:58,000 --> 00:23:02,360
 That's a part of what you see in meditation. You can see

359
00:23:02,360 --> 00:23:07,160
 and feel directly how wrong it is to cling to things, to

360
00:23:07,160 --> 00:23:08,000
 desire things,

361
00:23:08,000 --> 00:23:12,070
 which seems so foreign to most people. There's this idea

362
00:23:12,070 --> 00:23:16,000
 that it's part of being human, and it's more static, right?

363
00:23:16,000 --> 00:23:19,530
 We think of it as static. I like X, as though you've always

364
00:23:19,530 --> 00:23:23,130
 liked X, and you always will like X, and it's just part of

365
00:23:23,130 --> 00:23:24,000
 who you are.

366
00:23:24,000 --> 00:23:27,940
 Which is not at all the case. It's a habit. By reinforcing

367
00:23:27,940 --> 00:23:30,000
 it, you're strengthening it.

368
00:23:30,000 --> 00:23:33,960
 And it's pulling on everything else. It's taking energy

369
00:23:33,960 --> 00:23:37,390
 away from good things you can be doing, and it's corrupting

370
00:23:37,390 --> 00:23:38,000
 them.

371
00:23:38,000 --> 00:23:43,030
 It's lowering, it's reducing your interest in being a good

372
00:23:43,030 --> 00:23:44,000
 person.

373
00:23:44,000 --> 00:23:49,530
 So, this is the sorts of things that are interesting in

374
00:23:49,530 --> 00:23:52,000
 regards to the story.

375
00:23:52,000 --> 00:24:00,950
 The verse, again related though it is, does clearly teach a

376
00:24:00,950 --> 00:24:08,220
 different lesson, but related, and it's related to the idea

377
00:24:08,220 --> 00:24:10,000
 of karma.

378
00:24:10,000 --> 00:24:21,180
 And it offers an interesting idea that there's something

379
00:24:21,180 --> 00:24:32,000
 especially bad about evil done in relation to Buddhism.

380
00:24:32,000 --> 00:24:36,580
 That's the superficial explanation, the superficial

381
00:24:36,580 --> 00:24:44,140
 description. But what it's really saying is that when you

382
00:24:44,140 --> 00:24:47,000
 oppose good things,

383
00:24:47,000 --> 00:24:55,420
 your opposition to good things is resulting, results in

384
00:24:55,420 --> 00:25:00,210
 evil proportionate to the goodness of the thing that you're

385
00:25:00,210 --> 00:25:01,000
 opposing.

386
00:25:01,000 --> 00:25:06,000
 That's really the Buddhist dog or doctrine.

387
00:25:06,000 --> 00:25:09,190
 In Buddhism, what's the most pure and beautiful and

388
00:25:09,190 --> 00:25:15,050
 wonderful and good thing? Of course, it's the teaching of

389
00:25:15,050 --> 00:25:16,000
 the Buddha,

390
00:25:16,000 --> 00:25:19,620
 not just in its theoretical state, but in terms of the

391
00:25:19,620 --> 00:25:21,000
 dissemination.

392
00:25:21,000 --> 00:25:26,090
 Anyone who gets in the way of someone hearing the Buddha's

393
00:25:26,090 --> 00:25:27,000
 teaching,

394
00:25:27,000 --> 00:25:30,040
 I mean, is understandably thought to be doing one of the

395
00:25:30,040 --> 00:25:32,000
 most horrific things possible.

396
00:25:32,000 --> 00:25:36,690
 Not the most, but it's got to be up there to cut someone

397
00:25:36,690 --> 00:25:45,220
 off, to purposefully prevent someone from such a powerful

398
00:25:45,220 --> 00:25:47,000
 good.

399
00:25:47,000 --> 00:25:54,140
 It's a special sort of evil. It takes for this man to allow

400
00:25:54,140 --> 00:26:00,800
 his desires to lead him to actually ignore this woman's

401
00:26:00,800 --> 00:26:04,000
 kind of prevent her to be so blind.

402
00:26:04,000 --> 00:26:10,000
 It takes blindness as the point. Beyond ordinary blindness,

403
00:26:10,000 --> 00:26:15,540
 it takes a mindset is really what dithi, dithi is the word

404
00:26:15,540 --> 00:26:16,000
 view,

405
00:26:16,000 --> 00:26:19,470
 that's what it means, but it relates to mindset. This

406
00:26:19,470 --> 00:26:23,000
 person just is perverse, really.

407
00:26:23,000 --> 00:26:28,000
 Their mind is corrupt or perverse. Twisted is the point.

408
00:26:28,000 --> 00:26:35,000
 The way they look at the world is so mixed up, messed up.

409
00:26:35,000 --> 00:26:37,560
 That's really what view is all about. That's what this is

410
00:26:37,560 --> 00:26:42,000
 teaching. It's teaching about views.

411
00:26:42,000 --> 00:26:45,380
 It's more than one thing. It's teaching about the special

412
00:26:45,380 --> 00:26:47,000
 evil of reviling the Buddha,

413
00:26:47,000 --> 00:26:56,000
 reviling the Dhamma, the Sangha, of attacking them.

414
00:26:56,000 --> 00:26:59,460
 It's talking about the evil that's required to do that, the

415
00:26:59,460 --> 00:27:05,000
 evil of views out-twisted you have to be.

416
00:27:05,000 --> 00:27:10,320
 That's how this all relates to meditation and how views

417
00:27:10,320 --> 00:27:14,550
 come into play and what views really mean in Buddhism and

418
00:27:14,550 --> 00:27:16,000
 why they're such an important thing.

419
00:27:16,000 --> 00:27:19,440
 The views that we're concerned about, the concept of views,

420
00:27:19,440 --> 00:27:22,000
 is the twisted way of looking at the world.

421
00:27:22,000 --> 00:27:24,750
 We had this question about what is right view. Theoret

422
00:27:24,750 --> 00:27:30,000
ically, right view is anything that's in line with reality.

423
00:27:30,000 --> 00:27:33,440
 But that's not really what we're concerned about. We're

424
00:27:33,440 --> 00:27:36,000
 concerned with obtaining noble view,

425
00:27:36,000 --> 00:27:40,040
 which really means a straight relationship with reality,

426
00:27:40,040 --> 00:27:43,600
 where you see things, you connect with reality, this is

427
00:27:43,600 --> 00:27:46,000
 this, it is what it is.

428
00:27:46,000 --> 00:27:53,000
 It sounds simple. That is right view. So hard.

429
00:27:53,000 --> 00:27:57,310
 We think, well, there must be something more to it than

430
00:27:57,310 --> 00:27:59,230
 that. The problem is there's so much more to it than that,

431
00:27:59,230 --> 00:28:03,000
 and all that more to it is what's wrong.

432
00:28:03,000 --> 00:28:07,000
 We make so much out of everything and twist things.

433
00:28:07,000 --> 00:28:09,880
 It's really two different things. You can make more of

434
00:28:09,880 --> 00:28:13,000
 something than it is, like, "This is good, this is bad."

435
00:28:13,000 --> 00:28:16,000
 That's already twisted, never mind.

436
00:28:16,000 --> 00:28:20,000
 As soon as you see something is good or bad, it's twisted.

437
00:28:20,000 --> 00:28:23,120
 You can make something more out of something like, say, "

438
00:28:23,120 --> 00:28:26,000
This is a hand." It's not actually a hand.

439
00:28:26,000 --> 00:28:29,210
 It's light touching your eye. Even that is making a little

440
00:28:29,210 --> 00:28:31,000
 more out of it. It's seeing.

441
00:28:31,000 --> 00:28:35,550
 This is seeing. But you think it's a hand. That's making

442
00:28:35,550 --> 00:28:38,210
 more out of it than it actually is. That's not really a

443
00:28:38,210 --> 00:28:39,000
 problem.

444
00:28:39,000 --> 00:28:43,920
 It's potentially leading to problems. But the real problem

445
00:28:43,920 --> 00:28:46,700
 is when you get twisted and you say, "Oh, what a beautiful

446
00:28:46,700 --> 00:28:49,000
 hand. What an ugly hand.

447
00:28:49,000 --> 00:28:52,350
 What a stupid thing for him to do to raise his hand. Or, "

448
00:28:52,350 --> 00:28:56,000
Oh, I really like that. He's doing that."

449
00:28:56,000 --> 00:28:59,000
 Maybe you think it's funny, or maybe you think it's clever,

450
00:28:59,000 --> 00:29:02,000
 and you start to twist it.

451
00:29:02,000 --> 00:29:05,000
 Maybe you think you're clever because you understand it.

452
00:29:05,000 --> 00:29:07,000
 Maybe you think you're more clever because you could have

453
00:29:07,000 --> 00:29:10,000
 thought of a better way to explain it, right?

454
00:29:10,000 --> 00:29:15,000
 So much twisting and turning.

455
00:29:15,000 --> 00:29:19,630
 Maybe we can tie this into the idea of the reed. It's like

456
00:29:19,630 --> 00:29:22,420
 you have this reed, and if the reed is straight, it can

457
00:29:22,420 --> 00:29:24,000
 bear all that fruit.

458
00:29:24,000 --> 00:29:29,000
 But when it's twisted, it snaps.

459
00:29:29,000 --> 00:29:32,690
 The twisting of the mind. What the twisting of the mind

460
00:29:32,690 --> 00:29:37,530
 really does is it makes you twisted. You can't see the body

461
00:29:37,530 --> 00:29:38,000
.

462
00:29:38,000 --> 00:29:42,530
 It's like you become mentally a hunchback, is what I'm

463
00:29:42,530 --> 00:29:47,000
 trying to say. It's like you look at someone whose body is,

464
00:29:47,000 --> 00:29:52,000
 whose back is not straight.

465
00:29:52,000 --> 00:29:55,920
 This is the way the mind goes. The mind, it coils in on

466
00:29:55,920 --> 00:30:02,000
 itself. It recoils. Evil is like that.

467
00:30:02,000 --> 00:30:06,090
 It weighs heavily on the mind, is what the Buddha says. It

468
00:30:06,090 --> 00:30:10,000
 weighs so heavily on the mind that it breaks the mind.

469
00:30:10,000 --> 00:30:15,600
 Here's a good example. This man was clearly so consumed by

470
00:30:15,600 --> 00:30:19,000
 evil that he did the unthinkable.

471
00:30:19,000 --> 00:30:21,490
 There are worse things he could have done, like he could

472
00:30:21,490 --> 00:30:24,000
 have come and tried to kill the Buddha or something.

473
00:30:24,000 --> 00:30:26,760
 "Oh no, maybe if I kill the Buddha, she won't be able to

474
00:30:26,760 --> 00:30:30,000
 hear his teachings." That would have been worse.

475
00:30:30,000 --> 00:30:36,000
 But it's still hard to fathom the gall of this man.

476
00:30:36,000 --> 00:30:42,990
 And clearly caused by evil. This isn't just a fairy tale.

477
00:30:42,990 --> 00:30:45,000
 This is happening all the time.

478
00:30:45,000 --> 00:30:55,440
 Today there was a mass shooting in Las Vegas. I think it

479
00:30:55,440 --> 00:30:57,000
 had something to do with that.

480
00:30:57,000 --> 00:31:01,070
 Apparently, I don't really follow the news, but something

481
00:31:01,070 --> 00:31:04,840
 to do with a gambling debt, which is very apropos to this

482
00:31:04,840 --> 00:31:06,000
 sort of story.

483
00:31:06,000 --> 00:31:12,660
 Can you imagine? Incredible. Incredible to think. Assuming

484
00:31:12,660 --> 00:31:16,610
 that this is what I heard or what I read was what really

485
00:31:16,610 --> 00:31:18,000
 happened.

486
00:31:18,000 --> 00:31:27,000
 Someone became so consumed by greed and anger at their loss

487
00:31:27,000 --> 00:31:34,480
 that they went out and shot hundreds of people or something

488
00:31:34,480 --> 00:31:35,000
.

489
00:31:35,000 --> 00:31:42,200
 This isn't a fairy tale. This is reality. There are very

490
00:31:42,200 --> 00:31:45,000
 real consequences to our evil.

491
00:31:45,000 --> 00:31:50,550
 And you think, "Wow, you're overstating it. My evil is not

492
00:31:50,550 --> 00:31:53,000
 like that man's evil."

493
00:31:53,000 --> 00:31:58,440
 It's only a matter of degree. Evil is evil. And that's why

494
00:31:58,440 --> 00:31:59,000
 we call it evil.

495
00:31:59,000 --> 00:32:01,930
 We don't beat around the bush and say, "It's okay. Everyone

496
00:32:01,930 --> 00:32:06,150
 likes things. It's not really evil." It is. It really is

497
00:32:06,150 --> 00:32:07,000
 evil.

498
00:32:07,000 --> 00:32:11,030
 It's evil to want anything. It's just a matter of degree.

499
00:32:11,030 --> 00:32:12,000
 And what we don't get.

500
00:32:12,000 --> 00:32:15,210
 We think it's okay to like, just don't get out of hand. It

501
00:32:15,210 --> 00:32:18,000
's all out of hand. It's all wrong.

502
00:32:18,000 --> 00:32:21,000
 There's no good that comes from wanting or liking things.

503
00:32:21,000 --> 00:32:24,000
 It's a hard pill to swallow.

504
00:32:24,000 --> 00:32:29,440
 But for someone who's mindful, there's no other way. You

505
00:32:29,440 --> 00:32:34,000
 can't see things that way anymore.

506
00:32:34,000 --> 00:32:39,000
 That wanting could be in any way good. You can't deny the

507
00:32:39,000 --> 00:32:42,000
 fact. You don't want to deny the fact.

508
00:32:42,000 --> 00:32:45,670
 You free yourself from the blindness of thinking that there

509
00:32:45,670 --> 00:32:52,000
's some good to be had from liking or wanting.

510
00:32:52,000 --> 00:32:57,110
 So, that's the Dhammapada for tonight. Thank you all for

511
00:32:57,110 --> 00:32:58,000
 tuning in.

