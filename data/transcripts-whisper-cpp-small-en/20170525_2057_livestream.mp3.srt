1
00:00:00,000 --> 00:00:10,440
 Okay, good evening everyone.

2
00:00:10,440 --> 00:00:19,840
 Welcome to our evening demo session.

3
00:00:19,840 --> 00:00:44,880
 One very well known description of the Buddhist teaching is

4
00:00:44,880 --> 00:00:49,440
 as the middle way.

5
00:00:49,440 --> 00:00:57,340
 And so all of us are on the middle way, trying to find the

6
00:00:57,340 --> 00:01:01,240
 middle way.

7
00:01:01,240 --> 00:01:08,640
 Sometimes it's described in that way.

8
00:01:08,640 --> 00:01:16,210
 Just famously in terms of not torturing yourself, but not

9
00:01:16,210 --> 00:01:19,360
 becoming indulgent.

10
00:01:19,360 --> 00:01:22,180
 I think that applies very well to the meditation practice.

11
00:01:22,180 --> 00:01:26,000
 It's something that is an important lesson for us during

12
00:01:26,000 --> 00:01:28,680
 the meditation course.

13
00:01:28,680 --> 00:01:30,480
 Don't torture yourself.

14
00:01:30,480 --> 00:01:33,720
 Don't force it.

15
00:01:33,720 --> 00:01:36,320
 Don't even force yourself not to force it.

16
00:01:36,320 --> 00:01:37,320
 Right?

17
00:01:37,320 --> 00:01:43,000
 You realize that forcing it isn't even under your control.

18
00:01:43,000 --> 00:01:52,000
 This isn't something that you can just steamroll over.

19
00:01:52,000 --> 00:01:53,000
 Don't torture yourself.

20
00:01:53,000 --> 00:01:56,320
 But at the same time don't become complacent.

21
00:01:56,320 --> 00:02:00,410
 And so it seems actually it eventually becomes a razor's

22
00:02:00,410 --> 00:02:03,120
 edge, when there's only really one

23
00:02:03,120 --> 00:02:04,120
 way.

24
00:02:04,120 --> 00:02:05,120
 There's no leeway.

25
00:02:05,120 --> 00:02:12,370
 If you want to really be in the middle way, anupagama, you

26
00:02:12,370 --> 00:02:15,520
 have to not go at all in either

27
00:02:15,520 --> 00:02:17,920
 direction.

28
00:02:17,920 --> 00:02:23,630
 So what we're looking for is this state of balance in a

29
00:02:23,630 --> 00:02:26,560
 sense on a razor's edge.

30
00:02:26,560 --> 00:02:32,670
 And balance is a really good way to understand the middle

31
00:02:32,670 --> 00:02:34,320
 way as well.

32
00:02:34,320 --> 00:02:39,900
 And balance doesn't mean a little bit of indulgence and a

33
00:02:39,900 --> 00:02:42,480
 little bit of torture.

34
00:02:42,480 --> 00:02:46,430
 Balance means to find a state that is free from such things

35
00:02:46,430 --> 00:02:48,240
, like this state that is

36
00:02:48,240 --> 00:03:06,760
 not in either one direction.

