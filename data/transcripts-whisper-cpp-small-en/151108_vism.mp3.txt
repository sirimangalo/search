 Thank you.
 So we are on page 311 in the Visuddhi Maga Chapter 9, The
 Divine Abidings, and we're
 on Section 91.
 And Aurora, would you start us off?
 Yes.
 In general, now having thus known these divine abidings,
 told by the Divine One, supremely
 wise, there is this general explanation to concerning them
 that he should recognize.
 Now as to the meaning, firstly, of loving kindness,
 compassion, gladness, and equanimity,
 it fattens, fattens, machati, mityati.
 Thus it is loving kindness, metta, is the solvent, sanyah
ati, is the meaning, also it
 comes about with respect to a friend, mitta, or it is
 behavior towards a friend, thus it
 is loving kindness, metta.
 When there is suffering in others, it causes karupdi, good
 people's heart, to be moved.
 Kampana, thus it is compassion, karuna, or alternatively it
 combats ginnati, others'
 suffering attacks, and demolishes it.
 It is compassion, or alternatively it is scattered karianti
 upon those who suffered.
 It is extended to them by privation, thus it is compassion,
 karuna, those endowed with
 it are glad, mudanti, or it so is glad, mudati, or it is
 the mere act of being glad, mudana,
 thus it is gladness, mudita, mudita.
 It looks on at upikanti, abandoning such an interestedness
 as thinking may they be free
 from enmity and having recourse to neutrality, thus it is
 equanimity, upika.
 When we pay respect to the Buddha, is that an instance of
 metta?
 How do we identify that quality of the mind?
 Which quality?
 We are talking about respect to the Buddha.
 Here it also comes about with respect to a friend.
 This is the use of the word respect in an indiomatically
 English way, means in regards
 to a friend, in relation to.
 With respect to means in relation to.
 There is nothing to do with paying respect.
 So the feeling of respect has nothing to do with the pure
 words.
 Is that correct Madhav?
 I wouldn't say it has nothing to do with it, but it is not
 one of the four brahma-vihayras.
 As to the characteristic, etc., loving kindness is
 characterized here as promoting the aspect
 of welfare.
 Its function is to prefer welfare.
 It is manifested as the removal of annoyance.
 Ex proximate cause is seeing loveableness in beings.
 It succeeds when it makes ill will subside and it fails
 when it produces selfless affection.
 Compassion is characterized as promoting the aspect of all
aying suffering.
 Its function resides in not bearing others suffering.
 It is manifested as non-cruelty.
 Its proximate cause is to see helplessness in those
 overwhelmed by suffering.
 It succeeds when it makes cruelty subside and it fails when
 it produces sorrow.
 I can't remember whether we've had this yet.
 You see that he's talking about four different things.
 We're going to see this throughout the script, the text, if
 we haven't seen it already.
 There are four aspects in the Abhidhamma to each Dhamma.
 So each of these Dhammas is now being discussed.
 If you study the Abhidhamma, you already have a familiarity
 with this.
 But it's the function, the manifestation, the proximate
 cause and I think maybe the characteristic,
 the first one as to whether it succeeds and fails, that's
 extra.
 So the characteristic, the function, the manifestation and
 the proximate cause is the four aspects
 that make up a description in the Abhidhamma.
 So that's quite standard and we'll see that again.
 So Banthe, regarding Karuna, now if we see someone here,
 should it always be that we
 should go and help if we are to successfully practice Kar
una?
 Or if we don't help, does that mean we don't have Karuna at
 that moment?
 Yeah, I think that's correct.
 But we have to qualify that by saying it's not a sign of un
wholesomeness if you don't
 cultivate Karuna.
 But for someone who is dedicated to this practice, I think
 it would be appropriate to go and
 help at every instance, every occasion.
 Now if someone's not that keen on it or that interested in
 it, they have no need to go
 and help just anyone.
 But there are cases where not going to help someone leads
 to cruelty.
 When you decide not to, that's out of cruelty or cruelty
 arises or Karuna is often used
 to counteract cruelty that's innate in us.
 But it doesn't mean that just because you don't help
 someone means you're cruel.
 I mean, we'd have to see exactly what's going on, but I
 think that's the case.
 There's also a certain aspect of wisdom when it's
 appropriate to help, when it's actually
 going to be helpful, when it's of greater benefit to the
 other person than it is of
 detriment to you, that kind of thing.
 It may also help to subside cruelty within you, the kind of
 cruelty that, for instance,
 if you as a child, you might feel like stepping on the
 hands of playing with magnifying glasses.
 That is also a cruelty, right?
 Well, that's outright torture.
 What I mean by cruelty is just the kind of anger feeling
 that arises when you refuse
 to help someone, kind of disregard in the sense you get
 kind of angry at the thought
 of helping them.
 I see.
 But anger is the opposite of anger is metta, right, Bantu?
 So cruelty should be a little different?
 No, it's still a kind of anger.
 For example, the opposite of metta is hatred.
 Hatred is one type of anger, so there's hatred and cruelty,
 but they're both anger-based.
 For example, a kid playing with hands may not necessarily
 have anger towards the hands.
 He just wants to have some fun.
 Right, yeah, that's a good point.
 You can be cruel for other reasons.
 It could be explained that way.
 I guess I'm not entirely clear.
 They're a bit different.
 Metta, if I remember correctly, is called the adosa.
 So it is actually the exact opposite of anger.
 Yeah, so you may be right.
 There's a bit of a difference there.
 But I still think actual cruelty is...
 No, I guess that is a good point, that cruelty could be mo
ja-based.
 Thank you.
 Marilyn, are you able to read 95-4s?
 Oh, sorry, I just noticed you were muted there.
 Penelope, are you able to read 95-4s?
 Okay, yeah, it takes a little bit to get the microphone syn
ced and all that.
 95, gladness is characterized as gladdening, produced by
 other success.
 Its function resides in being unenvious.
 It is manifested as the elimination of aversion, boredom.
 Its proximate cause is seeing beings' success.
 It succeeds when it makes aversion, boredom, subside, and
 it fails when it produces merriment.
 Equanimity is characterized as promoting the aspect of
 neutrality towards beings.
 Its function is to see equality in beings.
 It is manifested as the quieting of resentment and approval
.
 Its proximate cause is seeing ownership of deeds, comma,
 thus beings are owners of their
 deeds, whose if not theirs is the choice by which they will
 become happy, or will get
 free from suffering, or will not fall away from the success
 they have reached.
 It occurs when it makes resentment and approval subside and
 it fails when it produces the
 continuity of unknowing, which is that world-minded
 indifference of ignorance based on the house-type.
 Purpose, the general purpose of these four divine bindings
 is the bliss of insight and
 excellent form of future existence.
 That particular to each is respectively the warding of ill-
will and so on.
 For here loving-kindness has the purpose of warding of ill-
will, while others have the
 respective purposes of warding of cruelty, aversion,
 boredom, and greed or resentment.
 And this is said too, for this is the escape from ill-will,
 friends, that is to say, the
 mind deliverance of loving-kindness.
 For this is the escape from cruelty, friends, that is to
 say, the mind deliverance of compassion.
 For this is the escape from boredom, friends, that is to
 say, the mind deliverance of gladness.
 For this is the escape from greed, friends, that is to say,
 the mind deliverance of equanimity.
 The near and far enemies, and here each one has two enemies
, one near and one far.
 The divine abiding of loving-kindness has greed as its near
 enemy.
 Since both share in seeing virtues, greed behaves like a
 foe who keeps close by a man, and it
 easily finds an opportunity.
 So loving-kindness should be well protected from it, and
 ill-will, which is this similar
 to the similar greed, is its far enemy, like a foe in sc
ones, in a rug, widen, widenness.
 So loving-kindness must be practiced free from fear of that
.
 For it is not possible to practice loving-kindness and feel
 anger simultaneously.
 Compassion has grief based on the home life for its near
 enemy, since both share in seeing
 failure.
 Such grief has been described in the way beginning, when a
 man either regards a sub-privation,
 failure to obtain visual objects cognizable by the eye that
 are sought after, desired,
 agreeable, gratifying, and associated with worldliness, or
 when he recalls those formerly
 obtained that are past, ceased, and changed, then grief
 arises in him.
 Such grief as this is called grief based on the home life,
 and cruelty, which is dissimilar
 to the similar grief, is its far enemy.
 So compassion must be practiced free from fear of that, for
 it is not possible to practice
 compassion and be cruel to breathing things simultaneously.
 Gladness has joy based on home life as its near enemy,
 since both share in seeing success.
 Such joy has been described in the way beginning, when a
 man either regards his gain, the obtaining
 of visible objects cognizable by the eye that are sought,
 and associated with worldliness,
 or recalls those formerly obtained that are past, ceased,
 and changed, then joy arises
 in him.
 Such joy as this is called joy based on the home life, and
 aversion, boredom, which is
 dissimilar to the similar joy as its far enemy.
 So gladness should be practiced free from fear of that, for
 it is not possible to practice
 gladness and be discontented with remote abodes and things
 connected with a higher profitable
ness simultaneously.
 Meaning on seeing a visible object with the eye, equanimity
 arises in the foolish, infatuated,
 ordinary man.
 In the untaught, ordinary man who has not conquered his
 limitation, who has not conquered
 future, gamma, result, who is unperceiving of danger, such
 equanimity, as this does not
 surmount the visible object, such equanimity, as this is
 called equanimity based on the
 home life, as greed and sentiment, which are dissimilar to
 the similar unknowing and its
 far enemies.
 Therefore equanimity must be practiced free from fear of
 that, for it is not possible
 to look on with equanimity and be inflamed with greed or be
 resentful simultaneously.
 Now zeal, consisting in desire to act, is the beginning of
 all these things.
 One of the hindrances, etc., is the middle.
 Absorption is the end.
 Their object is a single living being or many living beings
, as a mental object consisting
 in a concept.
 The order in extension.
 The extension of the object takes place either in excess or
 in absorption.
 Here is the order of it.
 Just as a skilled plowman first delimits an area and then
 does his plowing, so first a
 single dwelling should be delimited and loving kindness
 develops towards all beings there
 in the way beginning, in this dwelling may all beings be
 free from enmity.
 When his mind has become malleable and wieldy with respect
 to that, he can then delimit
 two dwellings.
 Next he can successively delimit three, four, five, six,
 seven, eight, nine, ten, one street,
 half the village, the whole village, the district, the
 kingdom, one direction and so on up to
 one world's wear or even beyond that and develop loving
 kindness towards the beings in such
 areas.
 Likewise with compassion and so on.
 This is the order in extending here.
 The outcome, just as the immaterial states are the outcome
 of the casinos and the base
 consisting of neither perception nor non-perception is the
 outcome of all concentration and fruition
 attainment is the outcome of insight and the attainment of
 succession is the outcome of
 serenity coupled with insight.
 So the divinability is the outcome of the first three
 divine abidings.
 For just as the gable rafters cannot be placed in the air
 without having first set up the
 scaffolding and built the framework of beams, so it is not
 possible to develop the fourth
 Jhana in the fourth divine abiding without having already
 developed the third Jhana in
 the earlier three divine abidings.
 And here it may be asked by why, but why are loving
 kindness, compassion, gladness and
 equanimity called divine abidings and why are they only
 four and what is their order and
 why are they called special states in the abidama?
 It may be replied, the divineness of the abiding Brahma Vih
a Ratha should be understood here
 in the sense of west and in the sense of immaculate for
 these abidings are the best in being the
 right attitude towards beings.
 And just as Brahma gods abide with immaculate minds, so the
 meditators who associate themselves
 with these abidings abide on an equal footing with Brahma
 gods.
 So they are called divine abidings in the sense of best and
 in the sense of immaculate.
 Here are the answers to the questions beginning with why
 are they only four?
 Their number four is due to path to purity and other sets
 of four, their order to their
 aim.
 As welfare and the rest, their scope is found to be immeas
urable, so measureless states their
 name.
 For among these, loving kindness is the way to purity for
 one who has much ill will, compassion
 is that for one who has much cruelty, gladness is that for
 one who has much aversion or boredom,
 and equanimity is that for one who has much greed.
 Also attention given to beings is only fourfold, that is to
 say as bringing welfare, as removing
 suffering, as being glad at their success, and as uncon
cerned, that is to say impartial
 neutrality.
 And one abiding in the measureless states should practice
 loving kindness and the rest
 like a mother with four sons, namely a child, an invalid,
 one in the flush of youth, and
 one busy with his own affairs.
 For she wants the child to grow up, wants the invalid to
 get well, wants the one in
 the flush of youth to enjoy for long the benefits of youth,
 and is not at all bothered about
 the one who is busy with his own affairs.
 That is why the measureless states are only four, as due to
 paths to purity and other
 sets of four.
 That's a really good passage, something to quote.
 I had a question on the third one, they, you know, they Kar
una, they're referring to it
 strictly as aversion and boredom.
 I've seen it described as, you know, combating jealousy as
 well and envy.
 Does that come into this as well?
 You can, Karuna is number two.
 Which one are you talking about?
 Oh, I'm sorry, I meant, Mudita, number three.
 Yeah, it's interesting that aversion and boredom are the
 two translations.
 I can take a look and see what the actual Pali is.
 Thank you.
 I bet it's badika, which would mean sort of not aversion,
 but being against someone, how
 do you say that?
 I mean, I'm not saying that I'm not saying that I'm not
 saying that I'm not saying that
 I'm not saying that I'm not saying that I'm not saying that
 I'm not saying that I'm not
 saying that I'm not saying that I'm not saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm saying that I'm saying that I'm saying that I'm saying
 that I'm saying that I'm saying that I'm saying that I'm
 saying that I'm saying that I'm saying that I'm saying that
 I'm
 saying that I'm not quite sure what is meant there because
 in the Arupa Janas they have different specific objects.
 But there is a sense that you're using the original object
 as a base I don't know. It's not quite clear to me.
 Thank you, Bante.
 Okay, thanks everyone. See you again next week. We'll get
 on to the next chapter.
 Bye, everyone.
 Thank you.
