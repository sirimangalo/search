1
00:00:00,000 --> 00:00:05,000
 [ Pause ]

2
00:00:05,000 --> 00:00:31,000
 [ Pause ]

3
00:00:32,000 --> 00:00:44,000
 [ Pause ]

4
00:00:44,000 --> 00:00:50,000
 Today's quote purports to the belief of the Jātaka.

5
00:00:50,000 --> 00:00:55,450
 But it's either not a direct quote or that's not the source

6
00:00:55,450 --> 00:00:56,000
.

7
00:00:56,000 --> 00:00:59,940
 It doesn't really matter. It's a good quote. Hopefully it

8
00:00:59,940 --> 00:01:02,000
 is something the Buddha said.

9
00:01:02,000 --> 00:01:15,000
 But what we do have is a quote from the Vidura Jātaka.

10
00:01:15,000 --> 00:01:24,610
 I haven't really gone into the context of the quote but it

11
00:01:24,610 --> 00:01:28,000
 has to do with

12
00:01:28,000 --> 00:01:34,460
 the

13
00:01:34,460 --> 00:01:43,560
 King of a person who is able to hang out with the King

14
00:01:43,560 --> 00:01:50,000
 because kings can be difficult to deal with.

15
00:01:50,000 --> 00:01:56,000
 So it's an admonishment.

16
00:01:56,000 --> 00:02:00,000
 The quote that we have is much more universal.

17
00:02:00,000 --> 00:02:06,000
 It says bend like a bow and be as defiant as a bamboo and

18
00:02:06,000 --> 00:02:10,000
 you will not be at odds with anyone.

19
00:02:10,000 --> 00:02:14,000
 So the first part is correct.

20
00:02:14,000 --> 00:02:16,000
 We have

21
00:02:16,000 --> 00:02:24,000
 [ Pause ]

22
00:02:24,000 --> 00:02:27,000
 The wise bend like a bow.

23
00:02:27,000 --> 00:02:31,000
 [ Pause ]

24
00:02:31,000 --> 00:02:37,000
 And indeed like bamboo,

25
00:02:37,000 --> 00:02:48,000
 and car supple, flexible.

26
00:02:48,000 --> 00:02:54,000
 [ Pause ]

27
00:02:54,000 --> 00:03:03,000
 In such a way one can dwell with the King.

28
00:03:03,000 --> 00:03:09,260
 So I'm not entirely convinced that the wisdom of the saying

29
00:03:09,260 --> 00:03:12,000
 sounds good.

30
00:03:12,000 --> 00:03:15,500
 But I'm not entirely convinced that it's something the

31
00:03:15,500 --> 00:03:20,000
 Buddha would have endorsed for his monks

32
00:03:20,000 --> 00:03:23,420
 because it's retelling a story if this is where it comes

33
00:03:23,420 --> 00:03:24,000
 from.

34
00:03:24,000 --> 00:03:30,720
 It's retelling a story of how to live, how I think a father

35
00:03:30,720 --> 00:03:33,000
 admonished his son.

36
00:03:33,000 --> 00:03:40,750
 I do better research on it but it's about how to live with

37
00:03:40,750 --> 00:03:42,000
 the King.

38
00:03:42,000 --> 00:03:45,000
 And so when you live with the King there are compromises

39
00:03:45,000 --> 00:03:48,310
 that need to be made and that's why it might not be the

40
00:03:48,310 --> 00:03:51,000
 best advice for us as spiritual practitioners.

41
00:03:51,000 --> 00:03:54,420
 Now if you have to go live with the King probably couldn't

42
00:03:54,420 --> 00:03:57,000
 realize, don't blow against the wind.

43
00:03:57,000 --> 00:04:01,390
 I just mean to say that there's potential room for blowing

44
00:04:01,390 --> 00:04:07,000
 against the wind, for standing strong.

45
00:04:07,000 --> 00:04:13,780
 That you shouldn't always bend over backwards to try and

46
00:04:13,780 --> 00:04:16,000
 please others.

47
00:04:16,000 --> 00:04:22,400
 But I think there's an argument to be made that in the case

48
00:04:22,400 --> 00:04:27,000
 of dealing with Kings, at least in a conventional sense

49
00:04:27,000 --> 00:04:28,000
 this is good advice.

50
00:04:28,000 --> 00:04:31,370
 But a lot of the Jataka's are just that, they're

51
00:04:31,370 --> 00:04:33,000
 conventional advice.

52
00:04:33,000 --> 00:04:37,670
 And sometimes it points to the necessity to compromise

53
00:04:37,670 --> 00:04:45,110
 yourself and your spiritual practice in order to maintain

54
00:04:45,110 --> 00:04:54,000
 worldly status and position and even harmony.

55
00:04:54,000 --> 00:04:59,020
 A person who lives their life as a monk is going to have a

56
00:04:59,020 --> 00:05:02,000
 hard time living in society.

57
00:05:02,000 --> 00:05:07,380
 And maybe that's part of the interesting situation I find

58
00:05:07,380 --> 00:05:10,000
 myself here in the West with.

59
00:05:10,000 --> 00:05:16,190
 That often gets in conflict with people who think that I'm

60
00:05:16,190 --> 00:05:21,850
 going to act as an ordinary person, not that I'm not a

61
00:05:21,850 --> 00:05:30,000
 person, but as a monk we live by different set of rules,

62
00:05:30,000 --> 00:05:33,000
 different set of ideas.

63
00:05:33,000 --> 00:05:39,820
 But if you are a rigid, which monks have to be in their

64
00:05:39,820 --> 00:05:45,340
 practice of the rules, there's no way I can live with a

65
00:05:45,340 --> 00:05:47,000
 king for example.

66
00:05:47,000 --> 00:05:53,650
 I'd have to bend over, I'd have to be able to bend and bend

67
00:05:53,650 --> 00:05:58,000
 to his will and be pliant and so on.

68
00:05:58,000 --> 00:06:02,300
 But there is a way of course by which this verse can be

69
00:06:02,300 --> 00:06:06,970
 understood as being universal and that's in a sense going

70
00:06:06,970 --> 00:06:08,000
 with the flow.

71
00:06:08,000 --> 00:06:12,740
 Not purposefully trying to stir things up or cause problems

72
00:06:12,740 --> 00:06:17,000
, being intent upon harmony.

73
00:06:17,000 --> 00:06:28,670
 Samanga Ratu, Samanga, Samanga, Samanga Rati, the one who

74
00:06:28,670 --> 00:06:33,370
 puts aside one's own desires for the betterment of the

75
00:06:33,370 --> 00:06:34,000
 group.

76
00:06:34,000 --> 00:06:37,560
 There's a story of three monks who dwelled together for the

77
00:06:37,560 --> 00:06:41,070
 rains and the Buddha asked them how they managed to live so

78
00:06:41,070 --> 00:06:43,000
 well together and they said,

79
00:06:43,000 --> 00:06:50,000
 "We just put aside what we wanted to do."

80
00:06:50,000 --> 00:06:55,690
 And thought about what was best for the group or waited to

81
00:06:55,690 --> 00:06:58,000
 see what the others wanted to do.

82
00:06:58,000 --> 00:07:05,000
 So no one was pushing or obstinate in their own desires and

83
00:07:05,000 --> 00:07:08,000
 their own intention.

84
00:07:08,000 --> 00:07:20,610
 And this has quite often a problem in religious circles

85
00:07:20,610 --> 00:07:35,000
 where people are obstinate and unamenable to change.

86
00:07:35,000 --> 00:07:40,660
 So in monasteries you often have this result conflict

87
00:07:40,660 --> 00:07:42,000
 arising.

88
00:07:42,000 --> 00:07:48,850
 They say with lay people there's a quote, I think it's from

89
00:07:48,850 --> 00:07:51,000
 somewhere in the tepidica,

90
00:07:51,000 --> 00:07:56,920
 the Buddha says or somebody says that among lay people

91
00:07:56,920 --> 00:08:02,000
 conflict usually arises based on sensuality.

92
00:08:02,000 --> 00:08:05,940
 So everybody wants to, more than one person wanting the

93
00:08:05,940 --> 00:08:07,000
 same thing and fighting over it,

94
00:08:07,000 --> 00:08:11,400
 the kids fighting over things, married couples fighting

95
00:08:11,400 --> 00:08:18,380
 over their needs and their wants and their friends and

96
00:08:18,380 --> 00:08:19,000
 coworkers,

97
00:08:19,000 --> 00:08:21,960
 that kind of thing, all fighting over, out of greed mostly,

98
00:08:21,960 --> 00:08:24,000
 out of desire for the same things,

99
00:08:24,000 --> 00:08:29,920
 and competition and conflict of desires and that kind of

100
00:08:29,920 --> 00:08:31,000
 thing.

101
00:08:31,000 --> 00:08:37,760
 With religious people conflict usually arises based on

102
00:08:37,760 --> 00:08:41,000
 views, arguing over views.

103
00:08:41,000 --> 00:08:46,400
 So this is how schisms arise, this is how religious

104
00:08:46,400 --> 00:08:50,000
 conflict comes about and so on.

105
00:08:50,000 --> 00:08:56,950
 But even simpler, in a monastery a monk will often get an

106
00:08:56,950 --> 00:09:00,000
 opinion about something

107
00:09:00,000 --> 00:09:04,820
 and make you very angry because you have an opinion about

108
00:09:04,820 --> 00:09:11,000
 other people.

109
00:09:11,000 --> 00:09:15,850
 You're unable to tolerate the ways of others, sometimes

110
00:09:15,850 --> 00:09:20,000
 with, sometimes simply as a matter of partiality

111
00:09:20,000 --> 00:09:25,600
 you believe things should be done a certain way and they're

112
00:09:25,600 --> 00:09:26,000
 not.

113
00:09:26,000 --> 00:09:32,880
 But sometimes with good reason, sometimes monks are

114
00:09:32,880 --> 00:09:36,000
 breaking rules and so on.

115
00:09:36,000 --> 00:09:39,410
 So in terms of principles I think you have to be careful

116
00:09:39,410 --> 00:09:41,000
 with this kind of saying.

117
00:09:41,000 --> 00:09:45,860
 It's not proper to compromise your principles, but

118
00:09:45,860 --> 00:09:51,000
 sometimes you have to compromise your opinions.

119
00:09:51,000 --> 00:09:55,230
 You have to be willing to be pliant and you have to be open

120
00:09:55,230 --> 00:09:56,000
-minded.

121
00:09:56,000 --> 00:09:59,990
 And sometimes even not as far as compromising your

122
00:09:59,990 --> 00:10:01,000
 principles,

123
00:10:01,000 --> 00:10:05,920
 but as far as being accepting of others who keep different

124
00:10:05,920 --> 00:10:07,000
 principles.

125
00:10:07,000 --> 00:10:10,470
 So monks who break rules for example, it doesn't really

126
00:10:10,470 --> 00:10:13,000
 help the situation to get angry at them

127
00:10:13,000 --> 00:10:17,380
 or to blame them or to scold them, ridicule them, vilify

128
00:10:17,380 --> 00:10:21,000
 them, which often happens.

129
00:10:21,000 --> 00:10:26,200
 Breaking rules also often happens and so it's an example of

130
00:10:26,200 --> 00:10:29,000
 if we were to go around

131
00:10:29,000 --> 00:10:33,140
 vilifying the monks who broke rules, where we never get

132
00:10:33,140 --> 00:10:34,000
 anything done

133
00:10:34,000 --> 00:10:36,840
 and we just create more and more conflict, it wouldn't

134
00:10:36,840 --> 00:10:38,000
 solve the problems,

135
00:10:38,000 --> 00:10:41,000
 you have to be more creative than that.

136
00:10:41,000 --> 00:10:50,000
 This is where being pliant and bendable is quite useful.

137
00:10:50,000 --> 00:10:59,430
 And of course that applies to any situation where you don't

138
00:10:59,430 --> 00:11:01,000
 want to compromise your principles,

139
00:11:01,000 --> 00:11:04,670
 but you often have to be quiet about other people's

140
00:11:04,670 --> 00:11:08,000
 seemingly lack of principles,

141
00:11:08,000 --> 00:11:14,000
 their rude behavior.

142
00:11:14,000 --> 00:11:19,000
 You have to be wise.

143
00:11:19,000 --> 00:11:22,000
 You have to think about what's going to solve the problem.

144
00:11:22,000 --> 00:11:26,010
 It's always important to be right, it's not always wise to

145
00:11:26,010 --> 00:11:28,000
 try and let other people know that you're right

146
00:11:28,000 --> 00:11:31,000
 or convince other people that you're right.

147
00:11:31,000 --> 00:11:34,000
 Sometimes it is, sometimes it's not.

148
00:11:34,000 --> 00:11:39,000
 Mindfulness helps you decide.

149
00:11:39,000 --> 00:11:43,000
 As usual, mindfulness is the key.

150
00:11:43,000 --> 00:11:45,350
 If you're mindful you don't have to worry too much about

151
00:11:45,350 --> 00:11:46,000
 these things

152
00:11:46,000 --> 00:11:50,000
 because you're naturally bamboo, but you don't break.

153
00:11:50,000 --> 00:11:54,000
 Bamboo is an interesting plant and it's a useful comparison

154
00:11:54,000 --> 00:11:54,000
 here

155
00:11:54,000 --> 00:11:57,490
 because if you've ever tried to break bamboo like this, it

156
00:11:57,490 --> 00:11:59,000
 doesn't happen.

157
00:11:59,000 --> 00:12:03,220
 You're more likely to, it eventually will break, but you'll

158
00:12:03,220 --> 00:12:05,000
 end up cutting your hands as a result.

159
00:12:05,000 --> 00:12:09,000
 If you've ever tried to cut bamboo, it's not bamboo.

160
00:12:09,000 --> 00:12:12,640
 I grew up in Northern Ontario and you broke sticks like

161
00:12:12,640 --> 00:12:13,000
 this.

162
00:12:13,000 --> 00:12:17,000
 I walk into the forest, you broke sticks.

163
00:12:17,000 --> 00:12:21,310
 When I tried to do that with a flimsy little piece of

164
00:12:21,310 --> 00:12:22,000
 bamboo,

165
00:12:22,000 --> 00:12:26,000
 bamboo is a good example of bending but not breaking.

166
00:12:26,000 --> 00:12:28,000
 That's, I think, a useful analogy.

167
00:12:28,000 --> 00:12:32,320
 In most circumstances you should bend, but in most

168
00:12:32,320 --> 00:12:35,000
 circumstances I think you shouldn't break,

169
00:12:35,000 --> 00:12:39,210
 which would be compromising yourself or allowing people to

170
00:12:39,210 --> 00:12:41,000
 really take advantage of you.

171
00:12:41,000 --> 00:12:45,540
 Sometimes you have to give a little, let people go a little

172
00:12:45,540 --> 00:12:46,000
 ways,

173
00:12:46,000 --> 00:12:50,580
 but to genuinely let people take advantage of you usually

174
00:12:50,580 --> 00:12:53,000
 isn't to their benefit or your own.

175
00:12:53,000 --> 00:12:56,160
 Letting people walk all over you is just bad karma for the

176
00:12:56,160 --> 00:12:57,000
 other person,

177
00:12:57,000 --> 00:13:01,000
 so it's not ideal in any sense.

178
00:13:01,000 --> 00:13:05,930
 But there are times where if you're being forced into

179
00:13:05,930 --> 00:13:07,000
 something,

180
00:13:07,000 --> 00:13:10,800
 where people are forcing their will upon you, sometimes you

181
00:13:10,800 --> 00:13:13,000
 have to be patient and bend,

182
00:13:13,000 --> 00:13:15,000
 but don't break.

183
00:13:15,000 --> 00:13:18,320
 Don't let it get to you, I think, is another aspect of this

184
00:13:18,320 --> 00:13:19,000
.

185
00:13:19,000 --> 00:13:25,000
 Don't let other people's problems become your problems.

186
00:13:25,000 --> 00:13:30,810
 This is like the bodhisattva, bodhisattva when he was

187
00:13:30,810 --> 00:13:32,000
 reckless

188
00:13:32,000 --> 00:13:39,300
 and these women came to listen to him, saw him in the

189
00:13:39,300 --> 00:13:41,000
 forest and came to talk to him,

190
00:13:41,000 --> 00:13:44,000
 to listen to what he had to say and he taught them.

191
00:13:44,000 --> 00:13:50,000
 And then it turns out these were like the king's harem.

192
00:13:50,000 --> 00:13:53,460
 And the king came and saw them sitting at the feet of the

193
00:13:53,460 --> 00:13:54,000
 Buddha

194
00:13:54,000 --> 00:13:58,000
 and thought he was seducing his women.

195
00:13:58,000 --> 00:14:04,040
 So he got his men to cut off his ears and cut off his hands

196
00:14:04,040 --> 00:14:07,000
 and cut off his feet.

197
00:14:07,000 --> 00:14:10,550
 And the bodhisattva kept saying, "My patience isn't in my

198
00:14:10,550 --> 00:14:15,000
 feet, my patience isn't in my hand."

199
00:14:15,000 --> 00:14:18,000
 And then he ended up killing him.

200
00:14:18,000 --> 00:14:23,440
 But the bodhisattva, he got to the point, he actually didn

201
00:14:23,440 --> 00:14:25,000
't kill him outright.

202
00:14:25,000 --> 00:14:31,000
 He just cut off all his limbs and ears.

203
00:14:31,000 --> 00:14:35,000
 And then the earth swallowed him up, the king.

204
00:14:35,000 --> 00:14:38,000
 And then the bodhisattva died.

205
00:14:38,000 --> 00:14:41,000
 But he died preaching patience.

206
00:14:41,000 --> 00:14:45,440
 Which is an example of not letting someone else's problem

207
00:14:45,440 --> 00:14:47,000
 become your problem.

208
00:14:47,000 --> 00:14:51,000
 And he never did. He never got angry or upset.

209
00:14:51,000 --> 00:14:54,000
 But he had no... It's not like he could get out of this.

210
00:14:54,000 --> 00:14:59,000
 He was being held down and tortured.

211
00:14:59,000 --> 00:15:04,000
 So not having any way out, he was just patient with it.

212
00:15:04,000 --> 00:15:07,000
 Didn't let it become his problem.

213
00:15:07,000 --> 00:15:11,550
 I think an example we wouldn't normally associate with

214
00:15:11,550 --> 00:15:14,000
 bending but not breaking.

215
00:15:14,000 --> 00:15:16,000
 Maybe people thinking he should have struggled,

216
00:15:16,000 --> 00:15:19,000
 or he should have yelled at the king, or got angry,

217
00:15:19,000 --> 00:15:23,000
 or thought about how bad it was or so on.

218
00:15:23,000 --> 00:15:28,000
 But I think it's that's a good example of this idea.

219
00:15:28,000 --> 00:15:32,000
 You shouldn't let it become your problem.

220
00:15:32,000 --> 00:15:34,000
 Anyway.

221
00:15:34,000 --> 00:15:37,000
 Raman, you want to say something?

222
00:15:37,000 --> 00:15:41,000
 He's leaving tomorrow.

223
00:15:41,000 --> 00:15:47,000
 I love how your course was this time.

224
00:15:47,000 --> 00:15:52,000
 My course was very good.

225
00:15:52,000 --> 00:16:00,000
 It was the advanced course, which was a little scary

226
00:16:00,000 --> 00:16:04,000
 because I felt like I barely passed the foundation course

227
00:16:04,000 --> 00:16:07,000
 and I didn't know what an advanced course would be.

228
00:16:07,000 --> 00:16:09,470
 And then you let me know that it's actually a review of the

229
00:16:09,470 --> 00:16:11,000
 foundation course.

230
00:16:11,000 --> 00:16:13,000
 So that was good.

231
00:16:13,000 --> 00:16:16,000
 It was a little more comfortable this time around.

232
00:16:16,000 --> 00:16:19,000
 It was good. A little more familiar.

233
00:16:21,000 --> 00:16:23,000
 But the more systematic as well.

234
00:16:23,000 --> 00:16:28,000
 I mean, I really don't know what the process is yet,

235
00:16:28,000 --> 00:16:31,000
 but you can see there's a process every day.

236
00:16:31,000 --> 00:16:33,000
 You can see the steps.

237
00:16:33,000 --> 00:16:35,000
 You don't know what the steps are.

238
00:16:35,000 --> 00:16:37,000
 You don't know what the steps have.

239
00:16:37,000 --> 00:16:41,000
 And it's definitely a little more systematic.

240
00:16:41,000 --> 00:16:44,000
 And of course, you can eliminate it.

241
00:16:44,000 --> 00:16:46,000
 Yeah, that helped.

242
00:16:46,000 --> 00:16:49,000
 Okay.

243
00:16:49,000 --> 00:16:52,000
 And then you want to say the group?

244
00:16:52,000 --> 00:16:56,000
 No.

245
00:16:56,000 --> 00:16:58,000
 Is there something I'd like to talk about?

246
00:16:58,000 --> 00:17:02,000
 You wanted to mention that you might need a steward here.

247
00:17:02,000 --> 00:17:04,000
 Oh, right.

248
00:17:04,000 --> 00:17:06,000
 You wanted me to do a steward.

249
00:17:06,000 --> 00:17:08,000
 I agree.

250
00:17:08,000 --> 00:17:13,000
 So I should talk about that later.

251
00:17:13,000 --> 00:17:15,000
 Thank you.

252
00:17:15,000 --> 00:17:18,140
 I think it would probably be better for you to give the

253
00:17:18,140 --> 00:17:22,000
 description of what you're looking for.

254
00:17:22,000 --> 00:17:31,000
 So...

255
00:17:31,000 --> 00:17:36,000
 So...

256
00:17:36,000 --> 00:17:41,220
 Yeah, we're looking for somebody who wants to come and live

257
00:17:41,220 --> 00:17:42,000
 here.

258
00:17:42,000 --> 00:17:46,000
 Um...

259
00:17:46,000 --> 00:17:49,000
 Why again?

260
00:17:49,000 --> 00:17:56,000
 Well, it would definitely help if there was someone to just

261
00:17:56,000 --> 00:17:57,000
 kind of take care of all the details,

262
00:17:57,000 --> 00:18:00,330
 because you as the teacher and as the monk here, you're not

263
00:18:00,330 --> 00:18:01,000
 really a householder,

264
00:18:01,000 --> 00:18:03,000
 and there's a household here.

265
00:18:03,000 --> 00:18:04,000
 Right.

266
00:18:04,000 --> 00:18:08,000
 There's the need for someone to attend to all those details

267
00:18:08,000 --> 00:18:08,000
,

268
00:18:08,000 --> 00:18:11,820
 like taking out the trash and, you know, just general

269
00:18:11,820 --> 00:18:14,000
 maintenance, cleaning,

270
00:18:14,000 --> 00:18:18,330
 you know, looking after the needs of the meditators when

271
00:18:18,330 --> 00:18:20,000
 they have just little things.

272
00:18:20,000 --> 00:18:23,700
 We've been doing some organization here, trying to put some

273
00:18:23,700 --> 00:18:24,000
 labels on,

274
00:18:24,000 --> 00:18:27,280
 like where different supplies are located so meditators can

275
00:18:27,280 --> 00:18:28,000
 find them,

276
00:18:28,000 --> 00:18:31,490
 but there'll still be, you know, little questions and

277
00:18:31,490 --> 00:18:32,000
 things,

278
00:18:32,000 --> 00:18:35,000
 and you're not always here, you're a student, so...

279
00:18:35,000 --> 00:18:37,490
 I mean, there's certain things that monks shouldn't be

280
00:18:37,490 --> 00:18:38,000
 doing.

281
00:18:38,000 --> 00:18:42,090
 We're kind of circumscribed as to what we can do for lay

282
00:18:42,090 --> 00:18:43,000
 people.

283
00:18:43,000 --> 00:18:45,000
 We're not allowed to work for lay people,

284
00:18:45,000 --> 00:18:48,370
 and so having meditators puts us in a bit of an interesting

285
00:18:48,370 --> 00:18:49,000
 situation,

286
00:18:49,000 --> 00:18:52,450
 because, well, on the one hand, they're very much like

287
00:18:52,450 --> 00:18:53,000
 monks,

288
00:18:53,000 --> 00:18:57,160
 but on the other hand, they're still lay people, and it

289
00:18:57,160 --> 00:18:58,000
 shows,

290
00:18:58,000 --> 00:19:02,000
 and as a result, we can't get too close.

291
00:19:02,000 --> 00:19:05,540
 So yeah, taking out trash, that kind of thing, I could take

292
00:19:05,540 --> 00:19:06,000
 out trash,

293
00:19:06,000 --> 00:19:08,750
 but to do it for lay people, you know, take out lay people

294
00:19:08,750 --> 00:19:09,000
's trash

295
00:19:09,000 --> 00:19:13,000
 starts to get a little bit, you know, too close.

296
00:19:13,000 --> 00:19:16,160
 So it would be good if, you know, meditators could do these

297
00:19:16,160 --> 00:19:17,000
 kinds of things.

298
00:19:17,000 --> 00:19:19,000
 Absolutely, it would help greatly.

299
00:19:19,000 --> 00:19:22,590
 I think, yes, that's why I agree to it, is because it would

300
00:19:22,590 --> 00:19:23,000
 help greatly

301
00:19:23,000 --> 00:19:29,000
 to have someone here to arrange many things.

302
00:19:29,000 --> 00:19:35,000
 We have issues around food.

303
00:19:35,000 --> 00:19:40,700
 If anybody wants to come on and ask questions while we're

304
00:19:40,700 --> 00:19:42,000
 talking.

305
00:19:42,000 --> 00:19:45,940
 It would be nice if someone could be here to make sure

306
00:19:45,940 --> 00:19:47,000
 there's

307
00:19:47,000 --> 00:19:54,000
 the food situation is going okay, and bedding, and water.

308
00:19:54,000 --> 00:20:01,000
 I mean, I can do some of these things, but not all of them.

309
00:20:01,000 --> 00:20:07,960
 And yeah, it would just be good to have an extra pair of

310
00:20:07,960 --> 00:20:09,000
 hands.

311
00:20:09,000 --> 00:20:13,000
 Little things, like the doorbell rings during the day, you

312
00:20:13,000 --> 00:20:13,000
 know.

313
00:20:13,000 --> 00:20:15,000
 The phone as well when I'm not here.

314
00:20:15,000 --> 00:20:17,660
 The doorbells ringing, you're at school, meditators are

315
00:20:17,660 --> 00:20:19,000
 trying to meditate it.

316
00:20:19,000 --> 00:20:22,000
 Just an extra pair of useful hands.

317
00:20:22,000 --> 00:20:28,000
 Yeah.

318
00:20:28,000 --> 00:20:32,000
 So if there's anybody out there who'd like to come and stay

319
00:20:32,000 --> 00:20:32,000
 with us

320
00:20:32,000 --> 00:20:36,660
 for an extended period of time, I think we could host one

321
00:20:36,660 --> 00:20:42,000
 person on an extended basis.

322
00:20:42,000 --> 00:20:47,000
 Now it would be an extensive bedding process, and you'd

323
00:20:47,000 --> 00:20:48,000
 probably have to prove yourself

324
00:20:48,000 --> 00:20:52,000
 as a meditator, at least on a basic level.

325
00:20:52,000 --> 00:20:58,000
 That's a meditator in our tradition.

326
00:20:58,000 --> 00:21:02,790
 You'd have to prove your sanity, and I don't know what else

327
00:21:02,790 --> 00:21:05,000
, but

328
00:21:05,000 --> 00:21:07,000
 I mean, it's not going to be rigorous.

329
00:21:07,000 --> 00:21:11,000
 It's not like we're going to pass a test or anything, but

330
00:21:11,000 --> 00:21:15,000
 just want to make sure that you're a good fit.

331
00:21:15,000 --> 00:21:22,000
 Tom, you've got to turn off YouTube.

332
00:21:22,000 --> 00:21:25,070
 If you come on the Hangout, you've got to turn off the

333
00:21:25,070 --> 00:21:26,000
 YouTube stream.

334
00:21:26,000 --> 00:21:30,000
 Or whatever stream you've got open.

335
00:21:30,000 --> 00:21:33,000
 Here, you can left.

336
00:21:33,000 --> 00:21:35,000
 You turned off the wrong stream.

337
00:21:35,000 --> 00:21:36,000
 You turned off the wrong stream.

338
00:21:36,000 --> 00:21:40,000
 That's easy enough to do.

339
00:21:40,000 --> 00:21:44,450
 If you're listening to the audio or watching a YouTube

340
00:21:44,450 --> 00:21:45,000
 video,

341
00:21:45,000 --> 00:21:51,000
 you have to turn it off before you come here.

342
00:21:51,000 --> 00:21:55,000
 I don't think Tom has questions.

343
00:21:55,000 --> 00:22:09,000
 Tom's just one of the people who comes on.

344
00:22:09,000 --> 00:22:29,000
 So no questions tonight?

345
00:22:29,000 --> 00:22:30,000
 All right.

346
00:22:30,000 --> 00:22:32,000
 Have a good night, everyone.

347
00:22:32,000 --> 00:22:35,000
 Thank you all for tuning in.

348
00:22:35,000 --> 00:22:38,000
 See you tomorrow.

349
00:22:38,000 --> 00:22:39,000
 Bye-bye.

350
00:22:41,000 --> 00:22:42,000
 [

