1
00:00:00,000 --> 00:00:12,000
 So today is finally an English session.

2
00:00:12,000 --> 00:00:25,350
 Tonight I thought I'd go over what we mean by the word

3
00:00:25,350 --> 00:00:29,680
 meditation and a little bit about

4
00:00:29,680 --> 00:00:36,320
 how we practice meditation.

5
00:00:36,320 --> 00:00:42,220
 For those of you who don't speak English, you can just sit

6
00:00:42,220 --> 00:00:45,000
 in meditation and try to

7
00:00:45,000 --> 00:00:47,000
 understand what's being said.

8
00:00:47,000 --> 00:00:56,600
 It's a chance for us to hear.

9
00:00:56,600 --> 00:01:18,360
 So the word meditation, as I've said before, can mean many

10
00:01:18,360 --> 00:01:20,600
 different things to many different

11
00:01:20,600 --> 00:01:24,760
 people.

12
00:01:24,760 --> 00:01:30,000
 For most of us we get a sense of meditation as a kind of

13
00:01:30,000 --> 00:01:35,080
 altered state of mind, a heightened

14
00:01:35,080 --> 00:01:44,080
 state of awareness or an alternate experience.

15
00:01:44,080 --> 00:01:47,880
 But really the word meditation, it just means to consider

16
00:01:47,880 --> 00:01:49,680
 or to mull over or to keep in

17
00:01:49,680 --> 00:02:04,440
 mind to dwell upon, to solve problems and so on.

18
00:02:04,440 --> 00:02:10,540
 It means when we dwell on something or when we consider

19
00:02:10,540 --> 00:02:13,240
 something, it can be referring

20
00:02:13,240 --> 00:02:20,440
 to any object that we take into consideration.

21
00:02:20,440 --> 00:02:25,400
 It could be a problem that we have that we meditate on.

22
00:02:25,400 --> 00:02:44,320
 It could be an object of worship, an object of power.

23
00:02:44,320 --> 00:02:50,640
 It could be many different things.

24
00:02:50,640 --> 00:02:56,530
 With the understanding that this is somehow going to bring

25
00:02:56,530 --> 00:02:59,600
 about a resolution or a result

26
00:02:59,600 --> 00:03:05,680
 of some sort.

27
00:03:05,680 --> 00:03:11,000
 For many people, meditation simply means this state of

28
00:03:11,000 --> 00:03:14,240
 consideration or awareness of an

29
00:03:14,240 --> 00:03:19,070
 object or even without an object, simply a state where the

30
00:03:19,070 --> 00:03:21,840
 mind is in an altered reality.

31
00:03:21,840 --> 00:03:26,690
 So we understand that meditation is this practice that

32
00:03:26,690 --> 00:03:30,440
 makes your mind very calm and peaceful.

33
00:03:30,440 --> 00:03:35,470
 This is just sort of an idea that we've developed about

34
00:03:35,470 --> 00:03:37,000
 meditation.

35
00:03:37,000 --> 00:03:39,420
 And for most people it would be quite surprising for them

36
00:03:39,420 --> 00:03:40,960
 or it is quite surprising for them

37
00:03:40,960 --> 00:03:45,670
 to learn that in meditation your mind need not be calm and

38
00:03:45,670 --> 00:03:47,840
 peaceful all the time.

39
00:03:47,840 --> 00:03:59,130
 Of course, meditation just means to consider or to ponder

40
00:03:59,130 --> 00:04:05,360
 or to take into one's awareness.

41
00:04:05,360 --> 00:04:08,830
 Now this is very critical in the practice of vipassana

42
00:04:08,830 --> 00:04:12,720
 meditation because vipassana meditation,

43
00:04:12,720 --> 00:04:16,460
 the meditation to see clearly, we have to see clearly many

44
00:04:16,460 --> 00:04:18,760
 bad things, especially in

45
00:04:18,760 --> 00:04:22,360
 Buddhism, the Buddha said, we have to understand suffering.

46
00:04:22,360 --> 00:04:27,830
 We have to do fully and totally, completely understand

47
00:04:27,830 --> 00:04:29,280
 suffering.

48
00:04:29,280 --> 00:04:32,310
 We can't do that when we're in this peaceful, calm state,

49
00:04:32,310 --> 00:04:34,320
 mostly because this peaceful, calm

50
00:04:34,320 --> 00:04:41,180
 state has nothing to do with our ordinary state of

51
00:04:41,180 --> 00:04:45,120
 awareness or existence.

52
00:04:45,120 --> 00:04:49,040
 So in Buddhism we separate meditation out into two general

53
00:04:49,040 --> 00:04:50,120
 categories.

54
00:04:50,120 --> 00:04:52,680
 And there are different ways of understanding these two

55
00:04:52,680 --> 00:04:53,840
 general categories.

56
00:04:53,840 --> 00:04:56,740
 One way is just simply to say one way, one type of

57
00:04:56,740 --> 00:04:58,840
 meditation is for the purpose and

58
00:04:58,840 --> 00:05:06,980
 ultimate goal of calm or focus, focused, controlled, cont

59
00:05:06,980 --> 00:05:11,200
rived states of awareness, sort of like

60
00:05:11,200 --> 00:05:15,280
 we call transcendental meditation.

61
00:05:15,280 --> 00:05:21,520
 And the other is for the purpose of seeing clearly.

62
00:05:21,520 --> 00:05:27,350
 Now when we split them up in this way, it doesn't mean that

63
00:05:27,350 --> 00:05:31,400
 insight meditation is going to not

64
00:05:31,400 --> 00:05:35,080
 lead to peace and calm or that in the practice of calm you

65
00:05:35,080 --> 00:05:37,520
 won't come to understand things.

66
00:05:37,520 --> 00:05:43,160
 What it means is that this is the purpose of the meditation

67
00:05:43,160 --> 00:05:43,520
.

68
00:05:43,520 --> 00:05:53,920
 Another way of looking at these two types of meditation is

69
00:05:53,920 --> 00:05:56,800
 the object that they take,

70
00:05:56,800 --> 00:05:59,470
 because there are certain objects that when taken under

71
00:05:59,470 --> 00:06:01,160
 consideration can't lead to real

72
00:06:01,160 --> 00:06:07,060
 and true wisdom, real and true understanding.

73
00:06:07,060 --> 00:06:09,450
 So many people would say all meditation is the same and all

74
00:06:09,450 --> 00:06:12,560
 leads in the same direction,

75
00:06:12,560 --> 00:06:15,030
 it's either you're either meditating or you're not, there's

76
00:06:15,030 --> 00:06:16,000
 no different types.

77
00:06:16,000 --> 00:06:20,630
 But as I said meditation, it takes an object and the

78
00:06:20,630 --> 00:06:24,600
 different objects do flavor the meditation

79
00:06:24,600 --> 00:06:26,880
 in a different way.

80
00:06:26,880 --> 00:06:29,490
 Why are there certain objects that don't lead to wisdom,

81
00:06:29,490 --> 00:06:30,720
 don't lead to insight?

82
00:06:30,720 --> 00:06:33,780
 It's because there are certain objects in meditation, many

83
00:06:33,780 --> 00:06:35,400
 objects, there's an infinite

84
00:06:35,400 --> 00:06:39,310
 number of objects in meditation that you can take that are

85
00:06:39,310 --> 00:06:40,200
 not real.

86
00:06:40,200 --> 00:06:44,360
 They're something that you create in your mind.

87
00:06:44,360 --> 00:06:48,550
 And because you create them in your mind, they can't give

88
00:06:48,550 --> 00:06:52,160
 you any understanding of reality,

89
00:06:52,160 --> 00:06:57,440
 just as our dreams or our fantasies, no matter how much we

90
00:06:57,440 --> 00:06:59,800
 dream and fantasize, it's not

91
00:06:59,800 --> 00:07:07,720
 reality and it can't help us in any way in our life.

92
00:07:07,720 --> 00:07:12,270
 So when we focus on a light, when people focus on a ball of

93
00:07:12,270 --> 00:07:19,320
 light in their minds or a color,

94
00:07:19,320 --> 00:07:23,960
 focus on object or a deity or so on, these are all things

95
00:07:23,960 --> 00:07:26,160
 that we create in our minds,

96
00:07:26,160 --> 00:07:28,360
 even God.

97
00:07:28,360 --> 00:07:31,250
 Because even if there were a God, we wouldn't have any clue

98
00:07:31,250 --> 00:07:32,680
 what the God would be like,

99
00:07:32,680 --> 00:07:37,560
 so we just create this idea in our minds.

100
00:07:37,560 --> 00:07:41,520
 And this leads to great peace and great happiness and it

101
00:07:41,520 --> 00:07:44,080
 leads many people to take religion

102
00:07:44,080 --> 00:07:48,200
 very seriously because they interpret their experience to

103
00:07:48,200 --> 00:07:51,240
 be a religious experience, either

104
00:07:51,240 --> 00:07:56,850
 an experience of God or an experience of oneness with

105
00:07:56,850 --> 00:08:02,120
 everything, experience of enlightenment.

106
00:08:02,120 --> 00:08:04,520
 And yet these experiences are temporary.

107
00:08:04,520 --> 00:08:09,200
 They're something that comes and goes.

108
00:08:09,200 --> 00:08:13,360
 Because there's no reflection on the reality of the

109
00:08:13,360 --> 00:08:16,600
 situation, reflecting on the concept

110
00:08:16,600 --> 00:08:21,090
 that we've created in our minds, then there's not

111
00:08:21,090 --> 00:08:24,720
 necessarily any gaining of insight.

112
00:08:24,720 --> 00:08:28,020
 Now it's certainly possible to look at, say, the states of

113
00:08:28,020 --> 00:08:29,560
 calm and come to see that they're

114
00:08:29,560 --> 00:08:34,520
 impermanent and you come to realize something about these

115
00:08:34,520 --> 00:08:37,800
 things and then insight can begin

116
00:08:37,800 --> 00:08:38,800
 therefrom.

117
00:08:38,800 --> 00:08:42,930
 But it's important to understand that there are indeed med

118
00:08:42,930 --> 00:08:44,960
itations that won't lead you

119
00:08:44,960 --> 00:08:50,840
 to come to understand reality because they are not real.

120
00:08:50,840 --> 00:08:53,720
 Transcendental meditation might be very nice but it doesn't

121
00:08:53,720 --> 00:08:55,000
 tell you much about how to

122
00:08:55,000 --> 00:09:04,920
 deal with the everyday situations that we're faced with.

123
00:09:04,920 --> 00:09:08,550
 And yet this is how many people approach problems, of

124
00:09:08,550 --> 00:09:09,360
 course.

125
00:09:09,360 --> 00:09:13,400
 When we have a problem with a person, a place or a thing,

126
00:09:13,400 --> 00:09:15,680
 we try to escape that person,

127
00:09:15,680 --> 00:09:19,400
 that place or that thing right away.

128
00:09:19,400 --> 00:09:21,870
 And so this kind of meditation is actually quite appealing

129
00:09:21,870 --> 00:09:23,160
 to people because they think

130
00:09:23,160 --> 00:09:26,760
 the way to overcome something is to escape it, just to run

131
00:09:26,760 --> 00:09:28,520
 away from it, just to push

132
00:09:28,520 --> 00:09:32,400
 it away.

133
00:09:32,400 --> 00:09:36,200
 There's no one out there telling us that we should actually

134
00:09:36,200 --> 00:09:38,120
 face and come to understand

135
00:09:38,120 --> 00:09:43,720
 and learn something about the unpleasant situation or

136
00:09:43,720 --> 00:09:48,080
 unpleasant experience, the problem.

137
00:09:48,080 --> 00:09:50,040
 This is where insight meditation comes in.

138
00:09:50,040 --> 00:09:52,600
 This is where we really understand the word meditation.

139
00:09:52,600 --> 00:09:56,000
 When you have a problem, you should meditate on it.

140
00:09:56,000 --> 00:10:00,880
 This is something that they would say in worldly circles.

141
00:10:00,880 --> 00:10:04,570
 This is where I believe the word meditation first came into

142
00:10:04,570 --> 00:10:05,080
 use.

143
00:10:05,080 --> 00:10:09,490
 It's not a Buddhist word, it's a word that we use to convey

144
00:10:09,490 --> 00:10:11,440
 the meaning of kamatana,

145
00:10:11,440 --> 00:10:14,810
 which really doesn't mean meditation, or bhavana, which

146
00:10:14,810 --> 00:10:16,880
 also doesn't mean meditation.

147
00:10:16,880 --> 00:10:29,250
 Kamatana means sort of like a fixed story, a firmly

148
00:10:29,250 --> 00:10:30,520
 established or specific task that

149
00:10:30,520 --> 00:10:31,520
 we do.

150
00:10:31,520 --> 00:10:36,480
 It means localizing your activities.

151
00:10:36,480 --> 00:10:40,000
 Stop doing so many different things and then our actions

152
00:10:40,000 --> 00:10:42,320
 are fixed and focused upon a certain

153
00:10:42,320 --> 00:10:45,320
 object.

154
00:10:45,320 --> 00:10:53,400
 It has more to do with our acts and our behavior than it

155
00:10:53,400 --> 00:10:59,000
 has to do with any consideration.

156
00:10:59,000 --> 00:11:02,900
 There are words that mean consideration, but they're not

157
00:11:02,900 --> 00:11:04,600
 used in this sentence.

158
00:11:04,600 --> 00:11:11,360
 The word meditation, it would mean to mull over a problem

159
00:11:11,360 --> 00:11:14,000
 as I understand it.

160
00:11:14,000 --> 00:11:18,030
 This is really very close to what is meant by insight

161
00:11:18,030 --> 00:11:20,640
 meditation because we're going

162
00:11:20,640 --> 00:11:23,220
 to be looking at the very problems and difficulties and

163
00:11:23,220 --> 00:11:25,040
 issues that we would normally want to

164
00:11:25,040 --> 00:11:28,000
 run away from.

165
00:11:28,000 --> 00:11:31,960
 We have to look at all of the aspects of the problem.

166
00:11:31,960 --> 00:11:33,600
 We have to come to see it clearly.

167
00:11:33,600 --> 00:11:40,020
 This is why this type of meditation doesn't generally

168
00:11:40,020 --> 00:11:44,080
 guarantee calm states of mind.

169
00:11:44,080 --> 00:11:47,790
 You see it's after a whole different type of calm, which is

170
00:11:47,790 --> 00:11:49,400
 this ability to ride the

171
00:11:49,400 --> 00:11:54,170
 waves, the ability to go with the flow so that when these

172
00:11:54,170 --> 00:11:56,680
 unpleasant situations come

173
00:11:56,680 --> 00:12:00,200
 about the mind is not displeased.

174
00:12:00,200 --> 00:12:05,330
 It's like there's this analogy that was Shanti Deva, if I

175
00:12:05,330 --> 00:12:08,080
 remember correctly, gave.

176
00:12:08,080 --> 00:12:14,670
 He said, "If the world is covered in glass, you have two

177
00:12:14,670 --> 00:12:16,080
 choices.

178
00:12:16,080 --> 00:12:22,550
 You can cover it over with leather or you can wear sandals

179
00:12:22,550 --> 00:12:23,080
."

180
00:12:23,080 --> 00:12:27,080
 In a way this is an apt comparison with these two types of

181
00:12:27,080 --> 00:12:28,320
 meditation.

182
00:12:28,320 --> 00:12:35,120
 When we have unpleasant situations, we can cover them over

183
00:12:35,120 --> 00:12:38,080
 and change them so that we

184
00:12:38,080 --> 00:12:42,100
 only have a nice situation or we can change our own minds

185
00:12:42,100 --> 00:12:44,320
 so that the situations don't

186
00:12:44,320 --> 00:12:47,760
 bother us.

187
00:12:47,760 --> 00:12:50,910
 Instead of having to work hard to make it always be a

188
00:12:50,910 --> 00:12:53,040
 pleasant situation, always be

189
00:12:53,040 --> 00:12:57,760
 a happy situation, a happy feeling in the mind.

190
00:12:57,760 --> 00:13:03,030
 Instead come to be content with whatever feeling, whatever

191
00:13:03,030 --> 00:13:05,400
 experience comes to us.

192
00:13:05,400 --> 00:13:06,760
 This is like wearing sandals.

193
00:13:06,760 --> 00:13:11,950
 We're able to deal with every situation because our mind is

194
00:13:11,950 --> 00:13:12,880
 valid.

195
00:13:12,880 --> 00:13:15,480
 It really means we have wisdom.

196
00:13:15,480 --> 00:13:18,440
 We have understanding.

197
00:13:18,440 --> 00:13:25,840
 Understand that everything is simply arising and ceasing.

198
00:13:25,840 --> 00:13:28,470
 Being caught up in it is not going to lead you to happiness

199
00:13:28,470 --> 00:13:28,680
.

200
00:13:28,680 --> 00:13:32,550
 Getting attached and excited about it is not going to lead

201
00:13:32,550 --> 00:13:33,680
 you to peace.

202
00:13:33,680 --> 00:13:36,650
 It's only going to lead you to more bother because it's not

203
00:13:36,650 --> 00:13:38,280
 stable, it's not sure, and

204
00:13:38,280 --> 00:13:44,840
 it's not under your control.

205
00:13:44,840 --> 00:13:48,320
 So in Buddhism we really do focus on this second type of

206
00:13:48,320 --> 00:13:49,440
 meditation.

207
00:13:49,440 --> 00:13:50,840
 It doesn't mean that we won't feel calm.

208
00:13:50,840 --> 00:13:54,100
 It doesn't mean that there's no tranquility involved in the

209
00:13:54,100 --> 00:13:54,880
 practice.

210
00:13:54,880 --> 00:13:56,680
 It just means that's not our goal.

211
00:13:56,680 --> 00:13:59,930
 Even tranquility, instead of looking at, say, an object

212
00:13:59,930 --> 00:14:01,880
 that would bring us tranquility,

213
00:14:01,880 --> 00:14:03,980
 we're going to look at the tranquility itself, and it might

214
00:14:03,980 --> 00:14:04,440
 come up.

215
00:14:04,440 --> 00:14:05,640
 It should come up.

216
00:14:05,640 --> 00:14:08,160
 It's part of the spectrum of experience.

217
00:14:08,160 --> 00:14:11,300
 But when we feel tranquil, we're going to look at the

218
00:14:11,300 --> 00:14:12,400
 tranquility.

219
00:14:12,400 --> 00:14:15,760
 We're going to consider it.

220
00:14:15,760 --> 00:14:20,480
 We're going to think over upon it.

221
00:14:20,480 --> 00:14:24,240
 And we're going to be very specific about this because we

222
00:14:24,240 --> 00:14:26,000
're not concerned about the

223
00:14:26,000 --> 00:14:27,680
 very details of it.

224
00:14:27,680 --> 00:14:30,240
 We're just concerned about what it is.

225
00:14:30,240 --> 00:14:32,160
 What does it mean to say that there's pain?

226
00:14:32,160 --> 00:14:34,960
 What does it mean to say that there's happiness?

227
00:14:34,960 --> 00:14:36,520
 What does it mean to say that there's anger?

228
00:14:36,520 --> 00:14:38,120
 What does it mean to say that there's greed?

229
00:14:38,120 --> 00:14:42,050
 And so we're going to just look at these and understand

230
00:14:42,050 --> 00:14:44,040
 them for what they are.

231
00:14:44,040 --> 00:14:46,910
 When we feel happy, we just say to ourselves, "Happy, happy

232
00:14:46,910 --> 00:14:47,600
, happy."

233
00:14:47,600 --> 00:14:50,800
 We're just meditating on this happiness.

234
00:14:50,800 --> 00:14:54,850
 When we feel pain, we're going to meditate on the pain, and

235
00:14:54,850 --> 00:14:56,760
 say to ourselves, "Pain,

236
00:14:56,760 --> 00:14:57,760
 pain."

237
00:14:57,760 --> 00:14:59,830
 And our mind will just know pain, and we'll just know

238
00:14:59,830 --> 00:15:01,880
 happiness, and we'll just be aware

239
00:15:01,880 --> 00:15:04,320
 of that one object.

240
00:15:04,320 --> 00:15:05,320
 There's no room.

241
00:15:05,320 --> 00:15:07,480
 This is where we have this nice word called mindfulness.

242
00:15:07,480 --> 00:15:10,530
 It's kind of interesting how these words that were totally

243
00:15:10,530 --> 00:15:12,240
 used for something completely

244
00:15:12,240 --> 00:15:15,920
 different actually fit right well into Buddhist thought,

245
00:15:15,920 --> 00:15:16,920
 even though they don't translate

246
00:15:16,920 --> 00:15:21,240
 the words that they're supposed to translate directly.

247
00:15:21,240 --> 00:15:25,220
 Meditation isn't a direct translation for any Pali word

248
00:15:25,220 --> 00:15:27,320
 that we normally use, that we'd

249
00:15:27,320 --> 00:15:31,680
 ever translate that way, and mindfulness also is not.

250
00:15:31,680 --> 00:15:37,440
 But mindfulness, having this full mind, or being fully

251
00:15:37,440 --> 00:15:40,960
 aware, or keeping fully in mind,

252
00:15:40,960 --> 00:15:47,960
 or having a full mind in this one sense, of the object.

253
00:15:47,960 --> 00:15:50,740
 So when we say to ourselves, "Pain, pain," our mind is full

254
00:15:50,740 --> 00:15:50,960
.

255
00:15:50,960 --> 00:15:54,160
 This is why it's kind of an interesting word.

256
00:15:54,160 --> 00:15:58,490
 Our mind is full, and we're fully aware of this object and

257
00:15:58,490 --> 00:15:59,860
 nothing else.

258
00:15:59,860 --> 00:16:04,610
 So there's no room for the normal diversification, making

259
00:16:04,610 --> 00:16:07,280
 more of it than it actually is.

260
00:16:07,280 --> 00:16:08,280
 This is bad pain.

261
00:16:08,280 --> 00:16:10,920
 This is my pain.

262
00:16:10,920 --> 00:16:12,600
 This is a problem.

263
00:16:12,600 --> 00:16:16,630
 When situations arise, we are able to break them down into

264
00:16:16,630 --> 00:16:18,200
 their components.

265
00:16:18,200 --> 00:16:21,220
 We see something we don't like, it's just seeing.

266
00:16:21,220 --> 00:16:23,240
 We hear something we don't like, it's just hearing.

267
00:16:23,240 --> 00:16:28,200
 We smell something we don't like, and so on.

268
00:16:28,200 --> 00:16:32,440
 We have no room for anything but the direct experience.

269
00:16:32,440 --> 00:16:37,760
 This is what it means to be mindful in this sense.

270
00:16:37,760 --> 00:16:41,280
 The original word of course didn't mean that.

271
00:16:41,280 --> 00:16:43,290
 This is something that the Buddha was very good at, and I

272
00:16:43,290 --> 00:16:46,960
 think he'd very much appreciate

273
00:16:46,960 --> 00:16:50,020
 giving words new meaning, because language is such a

274
00:16:50,020 --> 00:16:52,200
 cultural thing, and it's built up

275
00:16:52,200 --> 00:16:57,140
 around our ways of looking at the world, that there ends up

276
00:16:57,140 --> 00:17:00,080
 being no good word for the quality

277
00:17:00,080 --> 00:17:04,320
 that you're trying to describe, and you have to make do.

278
00:17:04,320 --> 00:17:09,110
 And there are many words that are used to mean ultimates,

279
00:17:09,110 --> 00:17:11,520
 that we have to remake, like

280
00:17:11,520 --> 00:17:14,140
 how the Buddha changed the word dharma, because it was such

281
00:17:14,140 --> 00:17:15,560
 an important word, or karma, because

282
00:17:15,560 --> 00:17:19,080
 it was another important word.

283
00:17:19,080 --> 00:17:21,830
 And so he had to explain to me what was the true dharma,

284
00:17:21,830 --> 00:17:23,320
 what was the true karma.

285
00:17:23,320 --> 00:17:29,640
 Brahmana, Sammana, all these words he changed.

286
00:17:29,640 --> 00:17:36,120
 So here we're giving this word a new meaning.

287
00:17:36,120 --> 00:17:43,770
 Mindfulness means your mind is full, or you're fully encomp

288
00:17:43,770 --> 00:17:47,280
assed by this one object.

289
00:17:47,280 --> 00:17:49,860
 And because the object is impermanent, it's coming and

290
00:17:49,860 --> 00:17:51,560
 going, it's changing, you're going

291
00:17:51,560 --> 00:17:52,560
 to see that.

292
00:17:52,560 --> 00:17:56,020
 And you're going to say that you're going to realize to

293
00:17:56,020 --> 00:17:58,440
 yourself that it's not satisfying,

294
00:17:58,440 --> 00:18:00,890
 that whatever you thought about this, that you were going

295
00:18:00,890 --> 00:18:02,200
 to change it, or make it good,

296
00:18:02,200 --> 00:18:09,120
 or make it perfect, was an illusion, was a delusion.

297
00:18:09,120 --> 00:18:16,000
 And you're going to let go of any idea that it's you or

298
00:18:16,000 --> 00:18:17,320
 yours.

299
00:18:17,320 --> 00:18:21,600
 And we'll come to see that really every experience is just

300
00:18:21,600 --> 00:18:24,520
 a part of nature, a part of the reality

301
00:18:24,520 --> 00:18:27,040
 of the universe.

302
00:18:27,040 --> 00:18:31,680
 There's no eye involved in seeing or hearing or smelling or

303
00:18:31,680 --> 00:18:34,240
 tasting or feeling or thinking.

304
00:18:34,240 --> 00:18:37,960
 All these things just come and go and come and go.

305
00:18:37,960 --> 00:18:43,630
 And the mind will begin to let go and loosen up, ease up,

306
00:18:43,630 --> 00:18:46,520
 until it finally really lets

307
00:18:46,520 --> 00:18:47,520
 go.

308
00:18:47,520 --> 00:18:50,340
 And when the mind really lets go, this is what the Buddha

309
00:18:50,340 --> 00:18:51,920
 called nirvana, nirvana.

310
00:18:51,920 --> 00:18:54,840
 This is what we're aiming for in the practice.

311
00:18:54,840 --> 00:18:57,610
 Just easing up and loosening up, just coming to see things

312
00:18:57,610 --> 00:18:59,120
 for what they are when they're

313
00:18:59,120 --> 00:19:00,520
 seeing, it's only seeing.

314
00:19:00,520 --> 00:19:03,480
 When there's hearing, it's only hearing.

315
00:19:03,480 --> 00:19:07,960
 Smelling, tasting, feeling, thinking, it's only an

316
00:19:07,960 --> 00:19:09,280
 experience.

317
00:19:09,280 --> 00:19:12,740
 When the mind loosens up, all of these attachments and add

318
00:19:12,740 --> 00:19:14,840
ictions, that's all they are.

319
00:19:14,840 --> 00:19:19,160
 Misunderstanding of the reality of our experience.

320
00:19:19,160 --> 00:19:26,680
 And when we overcome this, the mind lets go.

321
00:19:26,680 --> 00:19:30,360
 When the mind lets go, the mind is free.

322
00:19:30,360 --> 00:19:36,450
 When the mind is free, then you can say to yourself, "I'm

323
00:19:36,450 --> 00:19:37,480
 free.

324
00:19:37,480 --> 00:19:40,440
 Nothing left to be done here."

325
00:19:40,440 --> 00:19:45,560
 No more running around chasing after things that are not

326
00:19:45,560 --> 00:19:48,360
 going to bring us happiness or

327
00:19:48,360 --> 00:19:49,360
 peace.

328
00:19:49,360 --> 00:19:52,480
 This is the true freedom that we're looking for.

329
00:19:52,480 --> 00:19:56,880
 This is what we really mean by meditation in a Buddhist

330
00:19:56,880 --> 00:19:57,760
 sense.

331
00:19:57,760 --> 00:20:05,720
 This consideration of reality or reflection on reality as

332
00:20:05,720 --> 00:20:09,520
 it is, until we finally see

333
00:20:09,520 --> 00:20:13,400
 it just simply for what it is.

334
00:20:13,400 --> 00:20:14,400
 We're not creating anything.

335
00:20:14,400 --> 00:20:18,840
 It's not an altered state of mind.

336
00:20:18,840 --> 00:20:21,080
 In fact, it's the unaltered state of mind.

337
00:20:21,080 --> 00:20:26,370
 The Lord Buddha said, "Bhagavasarangi dhang, pika vai jit

338
00:20:26,370 --> 00:20:26,840
ang."

339
00:20:26,840 --> 00:20:34,480
 He said, "Monks, this here mind is radiant.

340
00:20:34,480 --> 00:20:38,120
 The mind is originally radiant.

341
00:20:38,120 --> 00:20:41,160
 The unaltered state of mind is perfect.

342
00:20:41,160 --> 00:20:43,860
 It's knowing that we're sitting, knowing that we're walking

343
00:20:43,860 --> 00:20:47,040
, or so on, or even the

344
00:20:47,040 --> 00:20:49,600
 non-arising of any experience.

345
00:20:49,600 --> 00:20:52,120
 It's perfect.

346
00:20:52,120 --> 00:20:56,950
 It's just that when we experience, then there gives rise to

347
00:20:56,950 --> 00:20:58,360
 defilements.

348
00:20:58,360 --> 00:21:04,350
 There are these akhanduka kilesa, these defilements that

349
00:21:04,350 --> 00:21:08,160
 are guests, or they are visitors.

350
00:21:08,160 --> 00:21:13,080
 They come in on occasion from time to time.

351
00:21:13,080 --> 00:21:17,760
 This is what makes the mind defile.

352
00:21:17,760 --> 00:21:21,870
 Just like a polished gem, you can cover it up with dirt

353
00:21:21,870 --> 00:21:23,680
 until it looks ugly.

354
00:21:23,680 --> 00:21:29,800
 But when you wash it, when you clean it, it gets back to

355
00:21:29,800 --> 00:21:32,440
 its unaltered form.

356
00:21:32,440 --> 00:21:34,280
 The mind is very much the same.

357
00:21:34,280 --> 00:21:38,040
 Actually, the mind is perfect.

358
00:21:38,040 --> 00:21:40,640
 There's no defilement in the mind.

359
00:21:40,640 --> 00:21:45,000
 It's a visiting state.

360
00:21:45,000 --> 00:21:46,680
 Defilement is something that comes into the mind.

361
00:21:46,680 --> 00:21:48,520
 This is just one way of talking.

362
00:21:48,520 --> 00:21:53,090
 Actually, in ultimate reality, they say, "The mind would be

363
00:21:53,090 --> 00:21:54,120
 defiled."

364
00:21:54,120 --> 00:21:57,960
 One mind state, or one moment of defiled.

365
00:21:57,960 --> 00:22:01,720
 But here we're talking in a general sense, our mind.

366
00:22:01,720 --> 00:22:05,960
 It's a mind that we call us, or we call I.

367
00:22:05,960 --> 00:22:09,420
 It's actually fine when you know things, when you're

368
00:22:09,420 --> 00:22:11,720
 walking around, when you're talking,

369
00:22:11,720 --> 00:22:14,840
 when you're doing whatever.

370
00:22:14,840 --> 00:22:18,040
 It's just that from time to time, we take things seriously.

371
00:22:18,040 --> 00:22:21,960
 We take things more seriously than we should.

372
00:22:21,960 --> 00:22:25,520
 We make more of some of things than we really are.

373
00:22:25,520 --> 00:22:28,990
 In fact, most of the time we do this, because most of the

374
00:22:28,990 --> 00:22:31,360
 time we're in this semi-deluded

375
00:22:31,360 --> 00:22:32,360
 state.

376
00:22:32,360 --> 00:22:37,200
 We're not really aware of reality.

377
00:22:37,200 --> 00:22:41,020
 This should be a good guideline for us, of what we're

378
00:22:41,020 --> 00:22:42,160
 aiming for.

379
00:22:42,160 --> 00:22:45,950
 It's just to see why is it that we suffer, why is it that

380
00:22:45,950 --> 00:22:48,120
 we give rise to defilement.

381
00:22:48,120 --> 00:22:52,800
 It's no other reason than we don't see things as they are.

382
00:22:52,800 --> 00:22:56,800
 Here we practice meditation as to consider these things and

383
00:22:56,800 --> 00:22:58,440
 come to see them clearer

384
00:22:58,440 --> 00:23:01,520
 than we saw them before.

385
00:23:01,520 --> 00:23:03,640
 It doesn't mean that the defilements won't arise, but it

386
00:23:03,640 --> 00:23:04,720
 means it will come to understand

387
00:23:04,720 --> 00:23:07,720
 them more and more, until they become weaker and weaker and

388
00:23:07,720 --> 00:23:09,160
 they don't have a hold over

389
00:23:09,160 --> 00:23:10,160
 us.

390
00:23:10,160 --> 00:23:13,240
 In an event, the defilements will not arise, because we'll

391
00:23:13,240 --> 00:23:14,800
 be able to see things perfectly

392
00:23:14,800 --> 00:23:18,960
 crystal clear.

393
00:23:18,960 --> 00:23:23,630
 This is the teaching that I would like to offer tonight, a

394
00:23:23,630 --> 00:23:26,680
 warm-up to meditation practice.

395
00:23:26,680 --> 00:23:30,120
 Thanks for coming.

396
00:23:30,120 --> 00:23:31,120
 1

