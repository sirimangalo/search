1
00:00:00,000 --> 00:00:04,870
 Welcome back to Ask a Monk. Next question comes from Oscar

2
00:00:04,870 --> 00:00:08,320
 Smack who asks, "Is it important

3
00:00:08,320 --> 00:00:11,770
 to go out of our way to apologize to those who we may have

4
00:00:11,770 --> 00:00:13,640
 hurt or been in conflict with

5
00:00:13,640 --> 00:00:19,430
 in the past, even if these people are no longer necessarily

6
00:00:19,430 --> 00:00:22,760
 a part of our lives?" There are

7
00:00:22,760 --> 00:00:27,230
 Buddhist texts that talk about this sort of thing as being

8
00:00:27,230 --> 00:00:29,440
 an important precursor to the

9
00:00:29,440 --> 00:00:35,470
 meditation practice, that if we currently have issues with

10
00:00:35,470 --> 00:00:38,120
 people, we should try our

11
00:00:38,120 --> 00:00:42,120
 best to resolve those. I mean, it's part of a broader issue

12
00:00:42,120 --> 00:00:44,720
 of hindrances to the meditation

13
00:00:44,720 --> 00:00:48,490
 practice. Many people, when they undertake intensive

14
00:00:48,490 --> 00:00:51,480
 meditation practice, they find themselves

15
00:00:51,480 --> 00:00:57,100
 unable to complete the course due to feelings of regret and

16
00:00:57,100 --> 00:01:01,400
 attachment of various sorts,

17
00:01:01,400 --> 00:01:04,810
 attachment even, many kinds of different kinds of

18
00:01:04,810 --> 00:01:07,560
 attachment, one of which is attachment

19
00:01:07,560 --> 00:01:13,560
 to feelings of guilt. It's certainly not the most important

20
00:01:13,560 --> 00:01:17,480
 thing to seek forgiveness from

21
00:01:17,480 --> 00:01:21,480
 people who we may have hurt, but it can help. Obviously, it

22
00:01:21,480 --> 00:01:23,400
 can help the other person if

23
00:01:23,400 --> 00:01:31,240
 they are feeling wronged to know that you understand the

24
00:01:31,240 --> 00:01:35,120
 wrong because part of the attachment

25
00:01:35,120 --> 00:01:40,280
 that we have to these things is the feeling that it's an

26
00:01:40,280 --> 00:01:43,440
 ego thing, but people who don't

27
00:01:43,440 --> 00:01:48,460
 meditate will have this feeling that the other person is in

28
00:01:48,460 --> 00:01:50,920
 the wrong and that they have

29
00:01:50,920 --> 00:01:54,520
 to be punished and so on. They have to be made to

30
00:01:54,520 --> 00:01:57,400
 understand what they have done wrong

31
00:01:57,400 --> 00:02:02,230
 and we feel the satisfaction from punishing people. So when

32
00:02:02,230 --> 00:02:04,360
 you are able to let them know

33
00:02:04,360 --> 00:02:08,800
 that you understand that what you've done is wrong, that

34
00:02:08,800 --> 00:02:11,160
 can be very helpful. I think

35
00:02:11,160 --> 00:02:21,510
 simply telling people that you're sorry, apologizing to

36
00:02:21,510 --> 00:02:28,120
 them is in many ways insufficient. Often

37
00:02:28,120 --> 00:02:31,750
 we say we're sorry and then we go out and do the same thing

38
00:02:31,750 --> 00:02:33,320
 again and we find this a

39
00:02:33,320 --> 00:02:38,300
 lot. I've found often, even though I might be sincere about

40
00:02:38,300 --> 00:02:40,800
 my apology, that people aren't

41
00:02:40,800 --> 00:02:45,660
 able to accept it or it doesn't have a real effect on them,

42
00:02:45,660 --> 00:02:48,320
 they still have the attachment

43
00:02:48,320 --> 00:02:53,200
 in their minds. Often times, I feel really bad about it and

44
00:02:53,200 --> 00:02:54,240
 I go and apologize and the

45
00:02:54,240 --> 00:02:57,810
 other person is confused and doesn't understand or thinks I

46
00:02:57,810 --> 00:02:59,840
'm being ridiculous and I'm still

47
00:02:59,840 --> 00:03:05,690
 holding on to this. That's really crucial. The most

48
00:03:05,690 --> 00:03:08,040
 important thing is of course to let

49
00:03:08,040 --> 00:03:15,240
 go of the situation by both sides. Sometimes it helps to

50
00:03:15,240 --> 00:03:18,720
 let go to be able to make some

51
00:03:18,720 --> 00:03:22,850
 sort of contact but I think often times if you go back and

52
00:03:22,850 --> 00:03:25,200
 make further contact, it can

53
00:03:25,200 --> 00:03:32,710
 lead to more conflict obviously unless the people involved

54
00:03:32,710 --> 00:03:35,560
 have changed. I don't think

55
00:03:35,560 --> 00:03:38,990
 it's necessary all the time to find all the people who you

56
00:03:38,990 --> 00:03:40,760
've wronged or who you've had

57
00:03:40,760 --> 00:03:45,220
 conflict with. I think that when you start to practice

58
00:03:45,220 --> 00:03:47,480
 meditation, obviously the truth

59
00:03:47,480 --> 00:03:52,870
 is when you start to practice meditation, you're going to

60
00:03:52,870 --> 00:03:55,120
 start to realize some of the

61
00:03:55,120 --> 00:04:00,250
 things that you've done wrong and you're going to feel bad

62
00:04:00,250 --> 00:04:02,080
 about it. Most important is for

63
00:04:02,080 --> 00:04:08,180
 you to overcome that, to realize what has been done wrong

64
00:04:08,180 --> 00:04:10,960
 but be able to let go of the

65
00:04:10,960 --> 00:04:15,590
 whole situation and let go of the ego and the self. Also

66
00:04:15,590 --> 00:04:17,140
 meaning letting go of feelings

67
00:04:17,140 --> 00:04:22,800
 of guilt. It's certainly not a positive thing to feel

68
00:04:22,800 --> 00:04:25,160
 guilty about bad things you've done.

69
00:04:25,160 --> 00:04:29,450
 It's important to understand that they were wrong and

70
00:04:29,450 --> 00:04:32,480
 understand the wisdom, to know that

71
00:04:32,480 --> 00:04:36,410
 it was something wrong and to not want to ever do it again.

72
00:04:36,410 --> 00:04:38,280
 But that doesn't come from

73
00:04:38,280 --> 00:04:41,660
 punishing yourself and hating yourself and feeling bad

74
00:04:41,660 --> 00:04:46,360
 about the things that you've done.

75
00:04:46,360 --> 00:04:51,290
 The most important thing is to let go and that's only going

76
00:04:51,290 --> 00:04:53,880
 to come really from changing

77
00:04:53,880 --> 00:04:58,250
 who you are and I think that really is the thing that the

78
00:04:58,250 --> 00:05:00,480
 person who you've heard could

79
00:05:00,480 --> 00:05:06,210
 wish the most is that you change. Obviously they feel that

80
00:05:06,210 --> 00:05:09,200
 you're a person, you have issues

81
00:05:09,200 --> 00:05:13,200
 and problems and that has caused you to hurt them. They're

82
00:05:13,200 --> 00:05:15,080
 probably angry at you and so

83
00:05:15,080 --> 00:05:20,180
 on. If they find out that you've changed, they'll be

84
00:05:20,180 --> 00:05:23,080
 delighted. They don't want to be

85
00:05:23,080 --> 00:05:25,600
 told that you were right in what you did. They want to find

86
00:05:25,600 --> 00:05:27,240
 out that you've changed and you

87
00:05:27,240 --> 00:05:31,730
 would never do such thing again and they can feel vind

88
00:05:31,730 --> 00:05:35,960
icated that their suffering was right

89
00:05:35,960 --> 00:05:41,690
 in the fact that you did something wrong. As a result, they

90
00:05:41,690 --> 00:05:44,360
'll feel better that you've

91
00:05:44,360 --> 00:05:50,460
 learned from your mistake and that they don't have to feel

92
00:05:50,460 --> 00:05:53,560
 bad and they'll feel angry at

93
00:05:53,560 --> 00:05:56,320
 you anymore because you're a new person. I think that's

94
00:05:56,320 --> 00:05:58,120
 much more important but it certainly

95
00:05:58,120 --> 00:06:02,870
 can help especially if we've really hurt someone or really

96
00:06:02,870 --> 00:06:06,280
 developed some sort of unwholesomeness

97
00:06:06,280 --> 00:06:09,640
 towards another person for our own benefit. As I said,

98
00:06:09,640 --> 00:06:12,000
 because it can otherwise be a hindrance

99
00:06:12,000 --> 00:06:16,000
 in our meditation, we feel guilty about the bad things that

100
00:06:16,000 --> 00:06:17,520
 we've done. I think in the

101
00:06:17,520 --> 00:06:20,200
 end, this guilt is something that we have to let go of. It

102
00:06:20,200 --> 00:06:21,440
's something that we have

103
00:06:21,440 --> 00:06:24,820
 to overcome. It's something that according to the Buddhist

104
00:06:24,820 --> 00:06:26,720
 teaching is unwholesome. It's

105
00:06:26,720 --> 00:06:33,850
 not a positive thing. It doesn't lead to true understanding

106
00:06:33,850 --> 00:06:37,000
 and it doesn't lead to true

107
00:06:37,000 --> 00:06:42,910
 change but the understanding that what you've done is wrong

108
00:06:42,910 --> 00:06:45,640
 immediately affects a change

109
00:06:45,640 --> 00:06:48,540
 and causes you to never want to do those sorts of things

110
00:06:48,540 --> 00:06:51,000
 again. Thanks for the question.

111
00:06:51,000 --> 00:06:51,640
 I hope that helps.

