 Okay, good evening everyone.
 Welcome back to our live broadcast, evening Dharma session.
 So continuing on from last night, I thought we'd move on to
 the Buddha's second discourse.
 When learning about the Buddha, it's quite common for the
 story to end at his enlightenment,
 sometimes maybe end at his first discourse, but it's far
 more interesting what happened after his enlightenment and
 after his first discourse.
 When he first taught, the five ascetics were impressed, but
 only one of them became a sotapana.
 Only one of them really understood what the Buddha was
 talking about.
 The others may have had some idea of how to put it into
 practice, but they weren't able to put it into practice and
 understand it and realize the teaching for themselves while
 the Buddha was teaching.
 So the Buddha had to stay with them, and he stayed with
 them for five days, working with them individually,
 addressing the individual challenges.
 So,
 So,
 So,
 And helping through their meditation until all five of them
 became a sotapana.
 At which point the Buddha taught the second discourse. So
 here was the same audience, but now they were in a position
 to really understand the Buddha's teaching on a deeper
 level.
 And so something we have something that this tells us is
 that this teaching is not really a beginner teaching.
 If you really want to understand the second discourse of
 the Buddha, you really have to have done some intensive
 meditation.
 I've taken the time to really get a clear understanding of
 reality.
 So for those of you who are here practicing intensively, it
'll be quite useful, I think.
 For those of you at home, well, for some of you, you've
 been practicing for some time. For those of you who may be
 new to the teaching, I'll try to give a sort of a more
 basic and easy to understand introductory teaching.
 So I'll use this. It's still a good basis for understanding
 the Buddha's idea of meditation.
 So the second discourse is about the five aggregates. For
 many of you, I'm sure, are familiar with this idea, this
 set of concepts that the Buddha laid out.
 And so the idea behind this suta is not to address the
 question of whether there is a self or there isn't a self,
 because that question is not useful.
 It's not actually an answerable question. It's just like
 asking, "Is there a God or isn't there a God?" It's perhaps
 even worse because with God, ideally, theoretically, you
 could have some contact with God.
 And God might speak to you or do something, but the self
 really is meaningless.
 But if I were to tell you that there is a self, what would
 that mean? Right? Is there any way for you to verify
 whether there is a self or not?
 It's always just going to be your belief. It's always just
 going to be your belief.
 If I tell you there isn't a self, what are you going to do
 with that either? Right?
 Is there a way for you to understand that there is no self,
 to verify that there is no self? Just because you haven't
 seen one, just because you haven't found one, right?
 It can never be something... It's not a question that's am
enable to understanding and verification.
 The self is not a falsifiable claim, so it's not a
 scientific claim, meaning you can't verify or deny it.
 So we don't ask that question. It's simply not a useful
 question.
 Instead, we look at what is there, and we try to understand
 it.
 And one of the very important ways of understanding the
 things that do exist, that we can experience, that we can
 verify as coming into being,
 is whether they are self, whether there's any reason to
 think of them as self, because whether there is a self or
 not isn't important.
 But we do cling to many, many things as me, mine, under my
 control, without even thinking about them, "Hey, this is me
, this is mine."
 We use them, we interact with them in a way that makes them
, in a way that that implies, or there's an implicit belief
 or conception of them as self.
 And so the first thing the Buddha does is make this jump
 from me and mine and the self and the being to our
 experiences.
 I mean, he's dealing with people who have begun to
 understand experience, so they're familiar with this.
 When he asks form, when he says about form, "Rupa," he says
, "Form is not self."
 And these monks have been meditating on form, they've been
 meditating on the movements of their bodies and the
 movements of the body during the breath and so on.
 They've been meditating on the heat and the cold. All of
 this is the physical, the form aspect of experience.
 And we take form to be self, right? Instead of experiencing
 it as moments of heat and cold and hardness and softness
 and so on,
 we experience it as our body. This is a body, my body. Yes.
 All of these experiences when they come together, this is
 me, this is mine, this I am. It's where this conception.
 When we look at the body, when we look at what's really
 there, we see experiences that arise and cease and these
 experiences are not me or not mine.
 How do we know this? Because they're not amenable to our
 control. They cause affliction.
 If something were truly me and mine, then we should be able
 to say, "Hey, let it be this, let it not be this."
 We should be able to say, "I don't want to be cold, I'm not
 going to be cold."
 We should be able to say, "I don't want to be short, I want
 to be tall, I don't want to be fat, I want to be thin."
 Which is probably really, I don't want to get into it.
 I mean, it's a problem with attachment to the body and
 attachment to our body because it's not something that
 actually exists.
 And we get really hung up on it in so many ways.
 The second one is feelings.
 We don't know.
