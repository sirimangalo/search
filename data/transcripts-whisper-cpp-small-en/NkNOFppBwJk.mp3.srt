1
00:00:00,000 --> 00:00:05,120
 I really appreciate your video uploads and that you are

2
00:00:05,120 --> 00:00:05,760
 informing us about

3
00:00:05,760 --> 00:00:10,960
 Buddhism. I meditate regularly and I am an agnostic. To

4
00:00:10,960 --> 00:00:11,760
 what extent

5
00:00:11,760 --> 00:00:17,440
 supports implies Buddhism, the concept of God. I made a

6
00:00:17,440 --> 00:00:19,840
 video a long, well, several

7
00:00:19,840 --> 00:00:25,360
 years ago about my understanding of God. God is in the same

8
00:00:25,360 --> 00:00:26,000
 category as

9
00:00:26,000 --> 00:00:32,220
 the self in that they can't logically exist. If you're

10
00:00:32,220 --> 00:00:32,880
 talking about

11
00:00:32,880 --> 00:00:40,720
 intellectually, they can't possibly exist. Because reality

12
00:00:40,720 --> 00:00:41,520
 is based on

13
00:00:41,520 --> 00:00:45,440
 existence, that reality is only seeing, hearing, smelling,

14
00:00:45,440 --> 00:00:46,240
 tasting, feeling,

15
00:00:46,240 --> 00:00:49,810
 thinking, that's reality. Even if God were to come and

16
00:00:49,810 --> 00:00:52,480
 stand before you, it's

17
00:00:52,480 --> 00:00:57,690
 impossible that you can be sure that that's God. You see,

18
00:00:57,690 --> 00:00:58,640
 because you're only

19
00:00:58,640 --> 00:01:01,760
 seeing something. If you hear God, it's impossible that you

20
00:01:01,760 --> 00:01:02,160
 should know that

21
00:01:02,160 --> 00:01:06,430
 you're hearing God, because it's just hearing. If God

22
00:01:06,430 --> 00:01:08,560
 strikes you over the head

23
00:01:08,560 --> 00:01:11,710
 with a lightning bolt or something, or lifts you up into

24
00:01:11,710 --> 00:01:14,320
 the air and shows you

25
00:01:14,320 --> 00:01:17,610
 the wonder of His creation, you still can't be sure that

26
00:01:17,610 --> 00:01:19,240
 that's God, because

27
00:01:19,240 --> 00:01:22,770
 it's still only seeing, hearing, smelling, tasting, feeling

28
00:01:22,770 --> 00:01:25,080
, and thinking. In this

29
00:01:25,080 --> 00:01:30,800
 sense, you can never know that God exists in the way that

30
00:01:30,800 --> 00:01:31,520
 people say that they

31
00:01:31,520 --> 00:01:35,360
 know that God exists, because what they know is an extrap

32
00:01:35,360 --> 00:01:36,360
olation of experience.

33
00:01:36,360 --> 00:01:39,270
 They don't really know it. And this is very important,

34
00:01:39,270 --> 00:01:40,360
 because this is how the

35
00:01:40,360 --> 00:01:45,840
 mind works. The mind will never react based on something

36
00:01:45,840 --> 00:01:47,200
 that it doesn't know.

37
00:01:47,200 --> 00:01:55,720
 It only reacts based on what it knows, which means the only

38
00:01:55,720 --> 00:01:56,160
 way the mind

39
00:01:56,160 --> 00:02:00,830
 can let go is to actually experience that something is

40
00:02:00,830 --> 00:02:03,960
 unsatisfying, is a cause

41
00:02:03,960 --> 00:02:07,430
 for stress and suffering. The only way one can become free

42
00:02:07,430 --> 00:02:08,920
 from suffering is

43
00:02:08,920 --> 00:02:13,800
 to see it as suffering. So the only one way this person can

44
00:02:13,800 --> 00:02:16,360
 come to an end of

45
00:02:16,360 --> 00:02:26,670
 samsara, of craving and clinging and striving, is to see

46
00:02:26,670 --> 00:02:28,680
 things as they are.

47
00:02:28,680 --> 00:02:32,520
 And this has nothing to do with God, because seeing God is

48
00:02:32,520 --> 00:02:33,640
 just seeing. The

49
00:02:33,640 --> 00:02:37,010
 soul is the same, self is the same, it has no place in

50
00:02:37,010 --> 00:02:39,560
 experience. It's just an

51
00:02:39,560 --> 00:02:45,090
 concept, an idea that comes up based on our experience,

52
00:02:45,090 --> 00:02:46,760
 that we have some

53
00:02:46,760 --> 00:02:50,900
 volition, that we can make choices, that at any given

54
00:02:50,900 --> 00:02:52,560
 moment there is a decision

55
00:02:52,560 --> 00:02:55,240
 that is made. So we therefore give rise to the idea that

56
00:02:55,240 --> 00:02:56,400
 there is someone behind

57
00:02:56,400 --> 00:03:02,520
 that volition, which, you know, it's important to

58
00:03:02,520 --> 00:03:03,320
 understand that we're not

59
00:03:03,320 --> 00:03:08,430
 saying that there is no volition, that there is no decision

60
00:03:08,430 --> 00:03:09,600
. It's just that

61
00:03:09,600 --> 00:03:15,010
 reality doesn't admit of entities like God or self or the

62
00:03:15,010 --> 00:03:16,440
 doer, in any

63
00:03:16,440 --> 00:03:20,530
 sense. It's just our understanding, the way we talk about

64
00:03:20,530 --> 00:03:21,600
 reality, the way we

65
00:03:21,600 --> 00:03:26,010
 conceive of reality, is so off track that we can't

66
00:03:26,010 --> 00:03:28,280
 understand when we say that

67
00:03:28,280 --> 00:03:31,830
 there is the doing but there is no doer. You say, you know,

68
00:03:31,830 --> 00:03:32,680
 if there is doing there

69
00:03:32,680 --> 00:03:35,730
 must be the doer, for example. Now your question is about

70
00:03:35,730 --> 00:03:37,200
 God, it's a little bit

71
00:03:37,200 --> 00:03:40,810
 different, but same sort of answer that entities don't

72
00:03:40,810 --> 00:03:42,560
 exist in reality. All that

73
00:03:42,560 --> 00:03:46,840
 exists is experience.

