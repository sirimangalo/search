1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:12,580
 Today we continue on with verse 253, which reads as follows

3
00:00:12,580 --> 00:00:13,000
.

4
00:00:13,000 --> 00:00:29,720
 Paravanjanupasisa nityamudjanasanino asavatasavadhanti aras

5
00:00:29,720 --> 00:00:33,000
o asavakaya

6
00:00:34,000 --> 00:00:44,250
 which means for one who is focused on the transgressions of

7
00:00:44,250 --> 00:00:45,000
 others,

8
00:00:50,000 --> 00:01:04,000
 always perceiving fault for such a one the asava increase.

9
00:01:04,000 --> 00:01:12,000
 They are very far from the destruction of the asava.

10
00:01:16,000 --> 00:01:18,960
 So this I think is our first verse that there really isn't

11
00:01:18,960 --> 00:01:20,000
 a story behind.

12
00:01:20,000 --> 00:01:27,000
 There is rather simply a person, a monk,

13
00:01:27,000 --> 00:01:33,100
 the monk who came to be known as Ujjanasanyi, one who perce

14
00:01:33,100 --> 00:01:34,000
ives fault.

15
00:01:34,000 --> 00:01:42,200
 And well I guess the story is that he went around perce

16
00:01:42,200 --> 00:01:44,000
iving fault in all the monks.

17
00:01:45,000 --> 00:01:49,300
 They didn't wear their robes right, they didn't perform

18
00:01:49,300 --> 00:01:50,000
 their duties right,

19
00:01:50,000 --> 00:01:53,320
 they didn't act right, they didn't speak right, dwelling

20
00:01:53,320 --> 00:01:56,000
 always on the faults of others.

21
00:01:56,000 --> 00:02:01,590
 The monks brought it to the attention of the Buddha and the

22
00:02:01,590 --> 00:02:06,000
 Buddha taught this verse. That's it.

23
00:02:09,000 --> 00:02:17,010
 So the simple lesson that's in pattern with the verse 252

24
00:02:17,010 --> 00:02:22,000
 and with this chapter of finding fault,

25
00:02:22,000 --> 00:02:27,000
 the lesson is that finding fault is not a good thing.

26
00:02:27,000 --> 00:02:35,000
 But there's much more to it than that of course.

27
00:02:35,000 --> 00:02:41,820
 We have to answer the question as to what's wrong with

28
00:02:41,820 --> 00:02:45,000
 finding fault in others.

29
00:02:45,000 --> 00:02:53,000
 And in fact we have to qualify it because it's not true

30
00:02:53,000 --> 00:02:56,000
 that finding fault in others is always wrong.

31
00:02:56,000 --> 00:03:01,000
 And that's not exactly what the Buddha says in this verse.

32
00:03:01,000 --> 00:03:10,920
 If we want to find out what's bad, why is it bad to find

33
00:03:10,920 --> 00:03:13,000
 fault for example,

34
00:03:13,000 --> 00:03:19,640
 we have to be able to as usual distinguish between

35
00:03:19,640 --> 00:03:23,000
 conventional reality and ultimate reality.

36
00:03:24,000 --> 00:03:27,860
 When we say something is bad or good in conventional

37
00:03:27,860 --> 00:03:32,000
 reality there's meaning behind that, certainly.

38
00:03:32,000 --> 00:03:37,790
 Taking this example in particular, finding fault in other

39
00:03:37,790 --> 00:03:41,000
 monks can be a very good thing.

40
00:03:41,000 --> 00:03:44,300
 It's good for communal harmony to point out each other's

41
00:03:44,300 --> 00:03:47,000
 faults. At the end of the reigns,

42
00:03:48,000 --> 00:03:52,450
 when the rain has stopped and the monks are ready to head

43
00:03:52,450 --> 00:04:00,000
 off, before they head off there's an occasion for informing

44
00:04:00,000 --> 00:04:00,000
 each other

45
00:04:00,000 --> 00:04:03,700
 of anything you might have done wrong during your three

46
00:04:03,700 --> 00:04:05,000
 month retreat.

47
00:04:05,000 --> 00:04:10,110
 You invite, the monks invite each other to point out their

48
00:04:10,110 --> 00:04:13,000
 faults, their transgressions.

49
00:04:13,000 --> 00:04:35,200
 But of course in practice, because of the categorical

50
00:04:35,200 --> 00:04:39,360
 difference between ultimate reality and conventional

51
00:04:39,360 --> 00:04:40,000
 reality,

52
00:04:41,000 --> 00:04:45,100
 in practice pointing out the faults of others often doesn't

53
00:04:45,100 --> 00:04:50,000
 go very well. If it's not done well.

54
00:04:50,000 --> 00:04:58,720
 And as this verse points out, done well or not, if it's not

55
00:04:58,720 --> 00:05:01,000
 done with the right intentions,

56
00:05:02,000 --> 00:05:09,620
 if it's done purposefully in the sense of making it one's

57
00:05:09,620 --> 00:05:11,000
 practice,

58
00:05:11,000 --> 00:05:15,620
 and by extension not just making it one's practice to find

59
00:05:15,620 --> 00:05:20,000
 faults in others but in general, making it one's practice

60
00:05:20,000 --> 00:05:23,000
 to exist in the external.

61
00:05:25,000 --> 00:05:29,590
 So the obvious problem with, on an ultimate level with

62
00:05:29,590 --> 00:05:37,350
 finding fault in others is the corruption, the impurities

63
00:05:37,350 --> 00:05:44,730
 of mind that generally and quite often take control of the

64
00:05:44,730 --> 00:05:48,000
 mind when one enters into this fault finding mind.

65
00:05:49,000 --> 00:05:54,000
 The asa as the Buddha said.

66
00:05:54,000 --> 00:06:01,470
 Things like greed, you can be manipulative, wanting to

67
00:06:01,470 --> 00:06:08,540
 manipulate people for your own benefit, finding fault in

68
00:06:08,540 --> 00:06:10,000
 them.

69
00:06:11,000 --> 00:06:16,350
 Anger, you can want to hurt others, finding fault in them

70
00:06:16,350 --> 00:06:19,000
 to make them feel suffering.

71
00:06:19,000 --> 00:06:26,000
 And delusion can be simply out of conceit or ignorance.

72
00:06:26,000 --> 00:06:30,000
 Lack of consideration.

73
00:06:30,000 --> 00:06:34,250
 I mean, not quite often of course conceit, maybe low self-

74
00:06:34,250 --> 00:06:37,000
esteem, of course high self-esteem.

75
00:06:38,000 --> 00:06:41,560
 You think you're better than someone worse than them and so

76
00:06:41,560 --> 00:06:47,710
 on, to make reason for going external, wanting to show your

77
00:06:47,710 --> 00:06:53,000
 good, your own worth, for example.

78
00:06:53,000 --> 00:06:59,790
 But there's an important point about being fixated on the

79
00:06:59,790 --> 00:07:03,000
 external as opposed to the internal.

80
00:07:04,000 --> 00:07:07,760
 Fixated on your own faults, that's of course a problem, but

81
00:07:07,760 --> 00:07:12,280
 it lacks the problem or the danger of focusing on the

82
00:07:12,280 --> 00:07:17,170
 faults of others because we can say that finding faults in

83
00:07:17,170 --> 00:07:19,000
 others is helpful.

84
00:07:19,000 --> 00:07:23,300
 If you find fault in others, well that's a good thing for

85
00:07:23,300 --> 00:07:24,000
 them.

86
00:07:24,000 --> 00:07:28,200
 It also makes you feel happy that you've done them a

87
00:07:28,200 --> 00:07:30,000
 service perhaps.

88
00:07:31,000 --> 00:07:34,770
 It actually benefits them. It's to say they take it in the

89
00:07:34,770 --> 00:07:36,000
 right way and even if they take it in the right way,

90
00:07:36,000 --> 00:07:39,180
 whether they're actually able to practice in such a way to

91
00:07:39,180 --> 00:07:43,000
 better themselves, to change their behavior.

92
00:07:43,000 --> 00:07:46,890
 It can be very important to let people know, but it's not

93
00:07:46,890 --> 00:07:49,000
 the most important thing.

94
00:07:50,000 --> 00:07:55,710
 The practice of seeking good in the external is fraught

95
00:07:55,710 --> 00:08:00,610
 with danger, with problem. You can't be sure what the

96
00:08:00,610 --> 00:08:02,000
 result is going to be.

97
00:08:02,000 --> 00:08:07,200
 The direct result is not actually the greatest benefit. At

98
00:08:07,200 --> 00:08:11,470
 best it can be a catalyst for others to implement the

99
00:08:11,470 --> 00:08:14,000
 change on a personal level.

100
00:08:19,000 --> 00:08:29,530
 Moreover, it's by its very nature a difficult, challenging

101
00:08:29,530 --> 00:08:34,000
 experience to keep pure.

102
00:08:34,000 --> 00:08:37,410
 If you consider the difference, when you look at your own

103
00:08:37,410 --> 00:08:40,000
 faults, you're looking at experience.

104
00:08:40,000 --> 00:08:42,360
 Suppose you get angry and you know that anger is a fault

105
00:08:42,360 --> 00:08:44,000
 and maybe you feel bad about that.

106
00:08:45,000 --> 00:08:47,720
 That's not a good thing. It's not good to be angry at your

107
00:08:47,720 --> 00:08:49,000
 anger, for example.

108
00:08:49,000 --> 00:08:54,300
 But at the very least, it's a very primary experience. It's

109
00:08:54,300 --> 00:08:56,000
 based on experiences.

110
00:08:56,000 --> 00:08:58,770
 When you focus on someone else's problems, you don't

111
00:08:58,770 --> 00:09:02,000
 actually experience anger if they're angry, let's say.

112
00:09:02,000 --> 00:09:05,780
 You want to tell someone about their anger. First, you have

113
00:09:05,780 --> 00:09:07,680
 to process the experience. You have to think about the

114
00:09:07,680 --> 00:09:08,000
 person.

115
00:09:09,000 --> 00:09:10,590
 You have to know what you're going to say, how you're going

116
00:09:10,590 --> 00:09:13,600
 to say it. Your mind is in a complex state because of the

117
00:09:13,600 --> 00:09:15,000
 conceptual nature.

118
00:09:15,000 --> 00:09:18,000
 You have to conceive of the other person.

119
00:09:18,000 --> 00:09:24,000
 This is why it's prime breeding ground for unwholesomeness,

120
00:09:24,000 --> 00:09:27,000
 because we get lost in the concepts.

121
00:09:27,000 --> 00:09:31,280
 We're no longer truly focused on our state of mind. We're

122
00:09:31,280 --> 00:09:33,000
 too much focused on the other person.

123
00:09:34,000 --> 00:09:37,840
 So any bad habits we might have that we haven't worked out

124
00:09:37,840 --> 00:09:41,000
 ourselves are easily able to overcome us.

125
00:09:41,000 --> 00:09:51,750
 This is why our work on the external level is quite often

126
00:09:51,750 --> 00:09:59,000
 unsuccessful, tainted by our own, our own development.

127
00:10:02,000 --> 00:10:06,170
 And also why the Buddha reminded us or encouraged us to

128
00:10:06,170 --> 00:10:08,000
 focus on our own faults.

129
00:10:08,000 --> 00:10:11,920
 If everyone focuses on their own faults, of course there

130
00:10:11,920 --> 00:10:15,000
 are far fewer faults to find in each other.

131
00:10:15,000 --> 00:10:19,320
 But more importantly, it's the work that can actually be

132
00:10:19,320 --> 00:10:20,000
 done.

133
00:10:20,000 --> 00:10:25,300
 You can easily find the faults, your own faults, much more

134
00:10:25,300 --> 00:10:29,870
 easier, much more easier than you can work out the faults

135
00:10:29,870 --> 00:10:31,000
 of others.

136
00:10:32,000 --> 00:10:38,000
 [Silence]

137
00:10:38,000 --> 00:10:42,140
 And this relates to this word asa wah. I didn't translate

138
00:10:42,140 --> 00:10:45,780
 it purposefully because it's not such a hard word to

139
00:10:45,780 --> 00:10:47,000
 understand.

140
00:10:47,000 --> 00:10:52,390
 It's just a bit hard to understand the Buddha's use of it

141
00:10:52,390 --> 00:10:55,000
 without explanation.

142
00:10:56,000 --> 00:11:01,130
 It's those words that because it hasn't become a word used

143
00:11:01,130 --> 00:11:05,630
 in this way in English, we don't really have a good catch

144
00:11:05,630 --> 00:11:06,000
 word.

145
00:11:06,000 --> 00:11:08,880
 All the translations I've seen I don't think are very

146
00:11:08,880 --> 00:11:11,000
 accurate at all. They're not very literal.

147
00:11:11,000 --> 00:11:15,540
 It refers to defilements, but it's not a word that means

148
00:11:15,540 --> 00:11:17,000
 defilements.

149
00:11:17,000 --> 00:11:20,180
 Corruptions, impurities of the mind, things that cause

150
00:11:20,180 --> 00:11:23,000
 suffering, problems for us, mental problems.

151
00:11:24,000 --> 00:11:28,000
 It's a way of describing mental problems and in fact a way,

152
00:11:28,000 --> 00:11:32,000
 a word that the Buddha used quite often, far more often

153
00:11:32,000 --> 00:11:37,000
 than we actually teach this word.

154
00:11:37,000 --> 00:11:42,610
 So it's not a word that means defilement. It's a way of

155
00:11:42,610 --> 00:11:46,000
 describing defilements as being outflowings.

156
00:11:47,000 --> 00:11:53,380
 Asa wah means literally something that pours out or leaks

157
00:11:53,380 --> 00:11:55,000
 or oozes.

158
00:11:55,000 --> 00:12:03,740
 If you think of a leaky pot or an infected wound, if it's

159
00:12:03,740 --> 00:12:08,600
 infected, it will never stop oozing. It will never stop

160
00:12:08,600 --> 00:12:09,000
 leaking.

161
00:12:10,000 --> 00:12:16,040
 But I think the real meaning of this word, why the Buddha

162
00:12:16,040 --> 00:12:23,190
 used it, is related to this idea of not just externalizing,

163
00:12:23,190 --> 00:12:27,000
 but going outside, going beyond, leaking.

164
00:12:30,000 --> 00:12:35,790
 Going beyond the pure, simple, real, reality-based state of

165
00:12:35,790 --> 00:12:40,000
 mind where you experience things as they are.

166
00:12:40,000 --> 00:12:43,540
 This phrase, "experience things as they are," is a very

167
00:12:43,540 --> 00:12:46,000
 powerful and important statement.

168
00:12:46,000 --> 00:12:51,120
 It's a bold claim that that's what we do in Buddhism. We

169
00:12:51,120 --> 00:12:54,110
 try to experience things that it's not a very hard thing to

170
00:12:54,110 --> 00:12:55,000
 understand.

171
00:12:56,000 --> 00:13:01,750
 But it's very important. Buddhism is not about, and

172
00:13:01,750 --> 00:13:05,390
 meditation is not about sequestering yourself away from

173
00:13:05,390 --> 00:13:07,000
 reality by any means.

174
00:13:07,000 --> 00:13:12,580
 It's about trying to experience reality deeper, more

175
00:13:12,580 --> 00:13:18,000
 fundamental than the external conceptual world.

176
00:13:19,000 --> 00:13:22,860
 We try to focus on the real world of relationships and

177
00:13:22,860 --> 00:13:27,210
 think that people who meditate are leaving behind that, but

178
00:13:27,210 --> 00:13:29,000
 in fact it's quite the opposite.

179
00:13:29,000 --> 00:13:33,130
 As we externalize ourselves, we ignore so much of what's

180
00:13:33,130 --> 00:13:35,000
 real about ourselves.

181
00:13:36,000 --> 00:13:41,470
 We ignore and we lose track of our emotions and therefore

182
00:13:41,470 --> 00:13:47,400
 are unable to navigate our relationships purely because our

183
00:13:47,400 --> 00:13:52,860
 minds are caught up in whatever whim is our habit of the

184
00:13:52,860 --> 00:13:54,000
 moment.

185
00:14:01,000 --> 00:14:04,880
 So this state of experiencing things as they really are is

186
00:14:04,880 --> 00:14:06,000
 like the pot.

187
00:14:06,000 --> 00:14:09,990
 And when we go outside of that, when we interpret, when we

188
00:14:09,990 --> 00:14:13,720
 extrapolate, when we see something that isn't there or

189
00:14:13,720 --> 00:14:18,000
 react in a way that is unwarranted, that's an asava.

190
00:14:18,000 --> 00:14:22,690
 That's the meaning of asava, going beyond. When we're not

191
00:14:22,690 --> 00:14:29,000
 strong enough or pure enough to see things as they are,

192
00:14:30,000 --> 00:14:33,220
 we see them as bad or good or me or mine, we go beyond. We

193
00:14:33,220 --> 00:14:36,680
 leak. That's the usage of the word asava and the Buddha

194
00:14:36,680 --> 00:14:41,400
 used it quite often, much more often than we actually hear

195
00:14:41,400 --> 00:14:44,000
 this word asava in talks.

196
00:14:44,000 --> 00:14:49,200
 I think also because it's mistranslated, not maliciously so

197
00:14:49,200 --> 00:14:53,790
, but in a way that hides the meaning here. It's a very

198
00:14:53,790 --> 00:14:56,000
 technical, I think technical term.

199
00:14:57,000 --> 00:14:59,710
 Because the word of course defilement or impurity, it's

200
00:14:59,710 --> 00:15:03,710
 emotionally charged. But on a technical level, it seems

201
00:15:03,710 --> 00:15:08,730
 that the Buddha preferred when speaking technically to talk

202
00:15:08,730 --> 00:15:16,600
 about it simply as leaking, as overflowing, not being in a

203
00:15:16,600 --> 00:15:23,000
 good state, a healthy or a composed state.

204
00:15:24,000 --> 00:15:28,500
 It's a way of describing what defilements do. They lead us

205
00:15:28,500 --> 00:15:31,440
 outward. They lead us to cling to things, to run away from

206
00:15:31,440 --> 00:15:35,000
 things. They lead us away from our equilibrium.

207
00:15:40,000 --> 00:15:45,400
 There are four types of asava. I'll just talk about these

208
00:15:45,400 --> 00:15:49,690
 because it gives us an idea of how the Buddha used this

209
00:15:49,690 --> 00:15:54,600
 word and the Buddha's teachings on what's wrong, what goes

210
00:15:54,600 --> 00:15:57,000
 wrong when we find fault with others.

211
00:15:58,000 --> 00:16:03,270
 Again, this idea of focusing on the faults of others and

212
00:16:03,270 --> 00:16:09,190
 obsessing and always interested in the faults of others. It

213
00:16:09,190 --> 00:16:12,000
's really that which is the leak.

214
00:16:13,000 --> 00:16:16,960
 It's not the telling people about their faults or even

215
00:16:16,960 --> 00:16:21,280
 seeing the faults of others. It's where the mind becomes

216
00:16:21,280 --> 00:16:25,950
 obsessed, where your intention, your inclination is rather

217
00:16:25,950 --> 00:16:28,000
 than seeing reality.

218
00:16:29,000 --> 00:16:34,400
 What's wrong with reality is in a very conceptual and

219
00:16:34,400 --> 00:16:39,070
 complex state of finding the faults in beings other than

220
00:16:39,070 --> 00:16:44,440
 yourself and pointing them out to others and talking about

221
00:16:44,440 --> 00:16:47,260
 them, maybe gossiping about them, telling other people

222
00:16:47,260 --> 00:16:50,000
 about them behind their back and so on and so on.

223
00:16:51,000 --> 00:16:56,280
 The state of mind behind all of those actions is complex

224
00:16:56,280 --> 00:16:59,000
 and highly problematic.

225
00:17:00,000 --> 00:17:06,580
 The four reasons why one might leak in this way are for

226
00:17:06,580 --> 00:17:13,600
 sensuality, kama asava, because you want something from

227
00:17:13,600 --> 00:17:18,000
 someone so you manipulate them.

228
00:17:19,000 --> 00:17:26,200
 If I point out what they're doing wrong, they'll rely upon

229
00:17:26,200 --> 00:17:31,140
 me. Maybe if we're vying for a position in employment, for

230
00:17:31,140 --> 00:17:35,150
 example, maybe I tell other people about their faults or I

231
00:17:35,150 --> 00:17:38,100
 make them feel like they don't deserve it and then it will

232
00:17:38,100 --> 00:17:41,000
 allow me to get what I want.

233
00:17:42,000 --> 00:17:46,620
 Maybe more money or so on. You can manipulate people by bel

234
00:17:46,620 --> 00:17:48,000
ittling them.

235
00:17:48,000 --> 00:17:57,330
 Bhava asava is, I guess, more related to fault finding. Bh

236
00:17:57,330 --> 00:18:04,000
ava asava is relating to states or existence.

237
00:18:05,000 --> 00:18:08,410
 The meaning here is wanting something to be a certain way

238
00:18:08,410 --> 00:18:12,000
 or not be a certain way. It relates to manipulation.

239
00:18:12,000 --> 00:18:16,530
 You manipulate people or you point out people's faults

240
00:18:16,530 --> 00:18:20,790
 because you don't want them to be a certain way or you want

241
00:18:20,790 --> 00:18:23,000
 them to be a certain way.

242
00:18:24,000 --> 00:18:27,800
 We want our children to be a certain way so we point out

243
00:18:27,800 --> 00:18:30,000
 what they're doing wrong.

244
00:18:30,000 --> 00:18:35,060
 Wrong is such an interesting concept as well. It relates to

245
00:18:35,060 --> 00:18:40,000
 the third, which is ditasava, our idea of what is wrong.

246
00:18:41,000 --> 00:18:45,450
 So the reason why we leak and in this case why we find why

247
00:18:45,450 --> 00:18:49,990
 we obsess about the faults of others relates to our idea

248
00:18:49,990 --> 00:18:54,690
 things must be this way and our very wrong view that things

249
00:18:54,690 --> 00:18:58,000
 must be a certain way in the first place.

250
00:18:58,000 --> 00:19:01,440
 Finding fault is very much tied up with thinking things

251
00:19:01,440 --> 00:19:03,000
 must be a certain way.

252
00:19:04,000 --> 00:19:07,310
 You find this in Buddhist monasteries. Often when monks

253
00:19:07,310 --> 00:19:10,630
 have done their due diligence and learned about the way mon

254
00:19:10,630 --> 00:19:14,000
asteries should be, then they get quite frustrated and upset

255
00:19:14,000 --> 00:19:17,000
 when they find out that monasteries aren't that way.

256
00:19:17,000 --> 00:19:20,880
 Organizations like our meditation organization, we often

257
00:19:20,880 --> 00:19:24,950
 deal with this, people frustration that things aren't a

258
00:19:24,950 --> 00:19:28,000
 certain way or should be a certain way.

259
00:19:28,000 --> 00:19:33,000
 It's really a wrong, it's related to wrong view.

260
00:19:35,000 --> 00:19:36,940
 In reality things not should be a certain way or shouldn't

261
00:19:36,940 --> 00:19:40,000
 be a certain way, they are a certain way.

262
00:19:40,000 --> 00:19:43,600
 And the real problem in the world is that we can't

263
00:19:43,600 --> 00:19:48,020
 understand things as being the way they are. We can't see

264
00:19:48,020 --> 00:19:52,470
 things as being that way. We see them as being the wrong

265
00:19:52,470 --> 00:19:56,000
 way and some other way as being the right way.

266
00:19:57,000 --> 00:20:00,010
 And so we're constantly trying to fix and make things

267
00:20:00,010 --> 00:20:01,000
 better and so on.

268
00:20:01,000 --> 00:20:06,240
 And it's that fixing mindset state that makes us leak. We

269
00:20:06,240 --> 00:20:11,100
 go out, we ooze all sorts of frustrations, angers and

270
00:20:11,100 --> 00:20:14,000
 desires and ambitions and so on.

271
00:20:17,000 --> 00:20:21,070
 And finally, a ouijasva, one big reason, perhaps the

272
00:20:21,070 --> 00:20:24,750
 biggest and overarching reason why we leak is just

273
00:20:24,750 --> 00:20:26,000
 ignorance.

274
00:20:26,000 --> 00:20:31,660
 Not realizing the suffering that comes from getting caught

275
00:20:31,660 --> 00:20:38,000
 up in anything, clinging to anything, wanting for anything.

276
00:20:41,000 --> 00:20:52,000
 And so the lesson for our practice is the reminder of this

277
00:20:52,000 --> 00:20:59,720
 composed state and the danger of leaking, the danger of

278
00:20:59,720 --> 00:21:04,000
 letting our minds go beyond what is real.

279
00:21:05,000 --> 00:21:09,560
 Anytime we get caught up in interactions with the external

280
00:21:09,560 --> 00:21:14,130
 world at all, even simple things like eating, working,

281
00:21:14,130 --> 00:21:18,350
 cleaning, we run the risk because of the complexity, we run

282
00:21:18,350 --> 00:21:24,900
 the risk of leaving behind reality and getting caught up in

283
00:21:24,900 --> 00:21:28,000
 manipulation, reaction and so on.

284
00:21:29,000 --> 00:21:32,570
 And so if we're going to engage with other people at all or

285
00:21:32,570 --> 00:21:37,280
 other things at all, we have to be very careful, like

286
00:21:37,280 --> 00:21:42,750
 carrying a fragile pot or vase or valuable possession,

287
00:21:42,750 --> 00:21:48,150
 fragile possession, to carry it with care and not let it

288
00:21:48,150 --> 00:21:49,000
 break.

289
00:21:52,000 --> 00:21:56,110
 As the Buddha likened, and this is probably a relation to

290
00:21:56,110 --> 00:22:00,290
 the asa, the concept of the asa wise, Buddha likened it to

291
00:22:00,290 --> 00:22:03,000
 a man who was carrying a big pot of oil.

292
00:22:03,000 --> 00:22:09,000
 And he said, imagine there was a man who was told he had to

293
00:22:09,000 --> 00:22:14,860
 carry a big pot of oil full to the brim and he had to carry

294
00:22:14,860 --> 00:22:18,000
 it throughout the entire city.

295
00:22:19,000 --> 00:22:23,360
 And they had they ordered a man with a sword to walk behind

296
00:22:23,360 --> 00:22:24,000
 him.

297
00:22:24,000 --> 00:22:28,500
 And if he let any one drop of oil spill, that man was to

298
00:22:28,500 --> 00:22:32,000
 cut off this, this other man's head.

299
00:22:34,000 --> 00:22:38,250
 And the Buddha said, now that man, do you think he would be

300
00:22:38,250 --> 00:22:42,700
 distracted by any singing or dancing or amusement happening

301
00:22:42,700 --> 00:22:46,690
 as you walk through the city or anyone calling out to him

302
00:22:46,690 --> 00:22:51,230
 or any beautiful woman or man or anything that might

303
00:22:51,230 --> 00:22:53,000
 distract him good food to eat?

304
00:22:54,000 --> 00:22:57,400
 And no venerable sir, yes. He said, well, you should think

305
00:22:57,400 --> 00:23:01,220
 like this. You should be as careful as that man. You really

306
00:23:01,220 --> 00:23:06,900
 have to. Our pot is very fragile, easily broken, of course

307
00:23:06,900 --> 00:23:09,000
 easily put back together.

308
00:23:09,000 --> 00:23:11,470
 It's not the end of the world when you get distracted,

309
00:23:11,470 --> 00:23:12,000
 obviously.

310
00:23:15,000 --> 00:23:19,960
 It's just your concentration that's broken and you can

311
00:23:19,960 --> 00:23:24,550
 regain it and build it and fortify it through the practice

312
00:23:24,550 --> 00:23:26,000
 of mindfulness.

313
00:23:26,000 --> 00:23:30,750
 Reminds us about the importance of our practice in worldly

314
00:23:30,750 --> 00:23:34,720
 things as well when relating to others to not get into the

315
00:23:34,720 --> 00:23:39,500
 fault finding mind for all these reasons because of greed,

316
00:23:39,500 --> 00:23:41,000
 because of fear, because of

317
00:23:44,000 --> 00:23:52,940
 feeling, feeling in inferior or superior, because of views

318
00:23:52,940 --> 00:23:58,000
 or desires, because of ignorance. Do not be ignorant.

319
00:23:58,000 --> 00:24:03,000
 Do have a sense of what's right and what's important.

320
00:24:06,000 --> 00:24:09,720
 And when you do engage with others, whether it be finding

321
00:24:09,720 --> 00:24:15,430
 fault or praise, you do so mindfully with a sense of it

322
00:24:15,430 --> 00:24:20,160
 being appropriate without any clinging, without any

323
00:24:20,160 --> 00:24:23,810
 attachment and letting it go and continuing on once it's

324
00:24:23,810 --> 00:24:24,000
 done.

325
00:24:27,000 --> 00:24:29,660
 So an important lesson, a good opportunity to talk about

326
00:24:29,660 --> 00:24:33,450
 the asavat, which are very important teaching. So that's

327
00:24:33,450 --> 00:24:37,000
 the Dhammapada for tonight. Thank you all for listening.

328
00:24:37,000 --> 00:24:41,000
 www.mooji.org

