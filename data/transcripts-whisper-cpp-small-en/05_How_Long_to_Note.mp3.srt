1
00:00:00,000 --> 00:00:04,820
 How long to note? Question. For how long should one note a

2
00:00:04,820 --> 00:00:08,640
 single phenomena? Answer. Unwholesome

3
00:00:08,640 --> 00:00:14,830
 phenomena such as liking, disliking, worry, fear, doubt,

4
00:00:14,830 --> 00:00:18,120
 etc. should be noted until they

5
00:00:18,120 --> 00:00:23,270
 go away. Unwholesome triggering phenomena like pain,

6
00:00:23,270 --> 00:00:26,440
 pleasure, or any object one finds

7
00:00:26,440 --> 00:00:30,830
 trusting, exciting, fearsome, or otherwise productive of

8
00:00:30,830 --> 00:00:32,720
 judgment of any kind should

9
00:00:32,720 --> 00:00:36,430
 be noted for as long as the mind remains interested or

10
00:00:36,430 --> 00:00:39,240
 until they disappear of their own accord,

11
00:00:39,240 --> 00:00:42,720
 whichever comes first. In the case of neutral phenomena,

12
00:00:42,720 --> 00:00:44,800
 one can still note them repeatedly

13
00:00:44,800 --> 00:00:52,500
 in order to develop familiarity with their nature of

14
00:00:52,500 --> 00:00:58,400
 arising and passing away.

