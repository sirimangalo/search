 Something like the shape, yeah, the theme, the earth, the
 round earth, you look at it,
 but you do not concentrate on its characteristic, the
 hardness or softness, and you do not look
 at the color, you do not take care of the color, but just
 what is called earth by people.
 And then you look at it and then you say, you say it many
 times, so here it said,
 "That conceptual state can be called by anyone he likes
 among the names for earth."
 You know, there are many words for the earth, and there are
 synonyms,
 so you can use any synonym for the earth. In English there
 may be not as many as there are in Pali language.
 So in Pali there is the word "pachowi" and then "mahi" and
 then "meidini" and "bumi",
 "wasudha" and "wasundra". So there are many words for the
 earth in English.
 So any one of them can be used. You look at the dish and
 you say, "And Pali, pachowi, pachowi, pachowi"
 or say, "Wasudha, wasudha, wasudha". Whatever word you say,
 it just means the earth.
 Whichever suits his manner of perception. Still, "earth" is
 also, that is on page 130,
 still "earth" is also a name that is obvious. The synonym
 really means, "earth" is a name that is commonly used.
 So you can just use the word "pachowi" and not the other
 words. That is what is meant here.
 So, the "earth" is not, I think also is not needed here.
 Still, "earth" is a name that is obvious.
 So it can be developed with the obvious one by saying, "
earth", that means "pachowi", "pachowi", "bachowi".
 And then you look at it, open your eyes and say, "bachowi",
 I mean, "earth", "earth", "earth".
 And then you close your eyes and try to memorize it. Then
 you open the eyes again
 and then close your eyes. So, in this way, you practice
 meditation.
 You try to memorize this image, the sign or image of the
 earth, until you get it in your mind,
 until you can see it in your mind without looking at it.
 And that is the time when you are said to have got the, it
 is called learning sign here.
 That means the abstract sign or, let us say, memorized sign
.
 So, after you get the memorized sign, then you leave that
 place and go to your own place and practice there.
 And that is because if you look at this and practice again
 and again,
 it is said that you cannot get the counterpart sign.
 Counterpart sign is finer than the memorized sign.
 So, in order to get the counterpart sign, you have to
 practice it in your mind and not looking at the thing
 itself.
 And in order not to see the thing, not to look at the thing
, you get away from it also.
 Because if it is close to it, you cannot have looking at it
.
 So, you just leave it there and go back to your own place
 and practice meditation there.
 Memorizing or seeing the sign in your mind and dwelling on
 it again and again and again,
 until it becomes refined and changed into what is called a
 counterpart sign.
 But you are not memorizing, you are not visualizing earth.
 Yes, you are.
 The word earth?
 Oh, no, not the word. No. But the thing. The thing we call
 earth.
 So, the part of it is your mental image.
 Mental image, that's right. And the first mental image is
 called the real learning sign.
 And that image appears as the real thing is.
 And then you dwell on it again and again in your mind and
 it changes into a counterpart sign.
 When it changes into a counterpart sign, it is refined and
 so it is very smooth
 and very shiny.
 And during the memorized sign, whatever faults there are on
 the disk also appear in your mind.
 But when you reach the stage of the counterpart sign, these
 defects or the faults of the gazina disappear
 and it becomes just like a polished mirror.
 Then you dwell on it again and again until you reach first
 the excess concentration
 and then giana concentration.
 You know there are two kinds of concentration, excess
 concentration and absorption concentration.
 So, excess concentration is the one you get before you
 reach the stage of giana.
 So, they are described in paragraph 32, 33 and so on.
 So, there are two kinds of concentration, excess
 concentration and absorption concentration.
 The mind becomes concentrated in two ways.
 That is on the plane of excess and on the plane of obtain
ment.
 Obtainment really means giana concentration.
 The mind becomes concentrated on the plane of excess by the
 abandoning of the hindrances.
 When you can get rid of hindrances, sensual desire, anger,
 sloth and tower, agitation and remorse and doubt,
 these are the five hindrances.
 So, when you can get rid of these five hindrances and your
 mind is on the object,
 then you are said to get to the plane of excess
 concentration.
 And when you reach the giana stage, then you are said to
 obtain the absorption concentration.
 So, the difference between these two kinds of concentration
 is this.
 The factors are not strong in excess.
 So, during the time of excess concentration, the factors
 means the mental states accompanying that consciousness of
 practice and meditation.
 These factors are not strong enough.
 Because they are not strong enough, your mind lets us into
 life continuum of Bhavanga
 and then go to the object again and then lets to Bhavanga
 again.
 Because the factors, they are not so strong.
 When you reach the giana stage, then your mind can be on
 that object for a whole day, for a whole night without
 being distracted with any other object.
 Because they are strong at that time, the mental states
 accompanying the consciousness.
 So, these are the two kinds of concentration.
 So, when you reach the excess or giana concentration, then
 you reach to one stage of the practice of meditation.
 But before, if you can get to the absorption stage easily,
 it is good.
 But if you cannot, then you have to do something here.
 And the thing, what you have to do here is guarding the
 sign.
 That means you have got the counterpart sign.
 And you have to keep it in your mind, not let it slip away
 from your mind.
 And you have to do something to guard the sign.
 And actually you have to avoid seven things which are unsu
itable.
 And seven things which are suitable.
 And seven things which are unsuitable are the abode, resort
, speech, person, food, climate and the posture.
 If living in a place, you practice meditation and you lose
 the sign, then it is not a good place for you.
 It is not suitable for you.
 So, something like that with the others too.
 Resort really means a village where monks go for alms.
 That is called a resort.
 So, it should not be too far.
 And here also on page 133, paragraph 37, there is a word, K
-O-S-E, kosa, one kosa and a half.
 One kosa is about two miles.
 So, one kosa and a half means about three miles.
 And then speech, speech means talking.
 So, if you talk too much and if you talk nonsense, you will
 lose what you have got, the counter-bud sign.
 So, you have to be restraining in your talks.
 And then person, one not given to aimless talk, who has the
 special qualities of vaju etc.
 The acquaintances with whom the unconcentrated man becomes
 concentrated or the concentrated man more so is suitable.
 One who is much concerned with his body, pays too much
 attention to his body.
 And who is addicted to aimless talk is unsuitable.
 Such person, unsuitable person.
 So, you have to avoid that.
 When you say, "Pays too much attention to the body," what's
 too much attention?
 Does that mean not exercising the body?
 In the footnote you said that one who is occupied with
 exercising and caring for the body.
 That means be too much concerned with the exercise or too
 much concerned with his body.
 But you need to have some kind of exercise to keep your
 body in good shape.
 Do yoga practices?
 I don't know, but I think yoga practices are good for
 keeping your body in good shape.
 It may be permissible to practice yoga, postures or
 whatever with concentration or mindfulness or meditation.
 But some people are too much concerned about their bodies.
 I think that is what is meant here.
 Because we have to pay proper attention to our physical
 bodies.
 If our physical bodies are not in good shape, we cannot
 practice meditation.
 So, such persons are to be avoided because even the
 attainment, even the jhanas can disappear if you associate
 with such persons.
 Who talk aimlessly and who pay too much attention to their
 bodies and so on.
 And then food.
 For some, sweet food is suitable and for others, sour food
 is suitable and so on.
 So you need to get suitable food to keep the counter-bust
 sign from disappearing from you.
 And climate, a good climate suits one, a warm climate,
 another.
 That is why we need some kind of comfort to practice
 meditation.
 We need to eat food which is suitable for us and then we
 need to be in a suitable climate, not too cold or not too
 warm and so on.
 And then posture.
 Now you can choose a posture which is best for you.
 Walking or sitting, but maybe not lying down.
 If walking suits you, then sitting suits you.
 You can practice sitting, but here, lying down is also
 given.
 Is that something that the person, the monk decides for
 himself or the teacher decides for himself?
 Here, the monk decides for himself.
 You have to try it for three days, like the airport, for
 three days each.
 So try walking three days, sitting three days and so on,
 and then decide which is best for you and then you do that.
 So if after avoiding the seven which are not suitable and
 taking which are suitable and you cannot get jhana or
 absorption,
 then you have to go through these ten skills in absorption.
 And the first one is making the basis clean.
 Actually, this is keeping your body clean and also keeping
 the place clean.
 So internal cleanliness and external cleanliness.
 So meditation room needs to be clean and neat and also our
 body should be clean.
 That is why a few pages before it said that you take bath
 and then you practice meditation.
 I think it is also important, keeping the basis clean.
 So when our bodies are clean, then we have more chance to
 get concentration.
 And so also when the place is clean and neat and not too
 crowded with many things, then it is conducive to
 meditation or concentration.
 So making the basis clean.
 And number two is maintaining balanced faculties.
 It is very important.
 Now, when we practice meditation, there are said to be five
 faculties or five working factors and they are faith or
 confidence, effort, mindfulness, concentration and
 understanding.
 These are the five faculties.
 So these five faculties should be balanced so that we get a
 good concentration or we get the absorption or jhana and
 also if we practice vipassana, the realization of truth.
 So there must be balancing of these faculties.
 If any one of these faculties is in excess, then the others
 cannot do their functions well.
 So they must be functioning well when they are working
 together.
 So one must not exceed another.
 And especially now, the so in that case, the faith for God,
 you should be modified either.
 If there is too much faith, too much confidence, then you
 have to reduce it by reviewing the individual essence of
 the state.
 That means if you have too much faith on the Buddha or if
 you have too much faith on your teacher, then you have to
 review the Buddha or the teacher in the impermanence of
 these.
 They are not permanent and they will go away one day and so
 you get less attachment or less devotion to them.
 Because if there is too much devotion, then you cannot
 practice meditation.
 And if there is too much energy, you cannot practice
 meditation either.
 And so there are two stories given here. One is of Elder W
arkali and the other is of Elder Sorna.
 Elder Warkali is the one who had too much faith in the
 Buddha, who was too devoted to the Buddha that he did not
 practice meditation.
 So the Buddha had to shake him up and the Buddha dismissed
 him, "Don't come to me."
 "What is the good of looking at my putrid body?" or
 something like that. And the Buddha dismissed him.
 So he was so sorry that he decided to kill himself by
 jumping off a cliff.
 So when he was about to jump, the Buddha sent his race and
 then preached to him.
 And so that is the story of Warkali. I brought the story of
 Warkali here.
 In case you don't have this book at the library, I don't
 know. Here's Buddhist Legends.
 I think we do. I'll put it on reserve. Buddhist Legends?
 Yes. And the other story is about Elder Sorna, who made too
 much effort
 and so could not achieve anything in the beginning.
 He was a son of a very rich man and it is said that he was
 very soft in his body and so very delicate.
 And so delicate that the hairs grow on his palms and the
 soles of his feet, it is said.
 So when he became a monk, he said, "I must make much effort
 in order to achieve attainment."
 And so he woke up and down during the night and he put too
 much effort,
 walking up and down, that he couldn't get any concentration
 and there were blisters on his soles.
 And so after practicing for some time, he decided to leave
 the aura and go back to late life.
 So Buddha knew this and Buddha went to him and talked to
 him with the simile of a harp or something.
 If the strings are too tight, you cannot get good sound.
 And if they are too loose, you cannot get good sound and so
 on.
 So this can be found in the Book of Discipline and Gradual
 Sayings.
 I will give you a piece, please write them down if you want
 to read the story in full.
 Book of Discipline Part 4, Volume 4, Page 2, 236 and
 following.
 And Gradual Sayings, Volume 3, Page 266 and following.
 266?
 Yes.
 Book of Discipline, 4th Volume, 236.
 So when one is too strong, the others cannot perform their
 respective functions, not several respective functions.
 They have their respective functions.
 There is one function and effort has another function and
 so on.
 So when one is too strong, the other cannot perform their
 respective functions.
 And particularly, regalment is balancing faith with
 understanding and concentration with energy.
 These are the most important.
 So balancing faith with understanding and concentration
 with energy.
 If you have too much faith, then you will believe anything.
 You will believe in what you ought not to believe.
 You will be led astray by those who deceive people.
 And if your understanding is too strong, if you are too
 wise, then you tend to become cunning.
 Or maybe tricky.
 So these two have to be balanced. Not too much faith and
 not too much understanding or wisdom.
 And then concentration and energy should also be balanced.
 If there is too much concentration, you tend to become lazy
.
 And if there is too much energy, you tend to become
 agitated.
 And in both ways, you lose concentration.
 Now, paragraph 48, that is important.
 Because first, the author said that there should be balance
 of faculties.
 And then, particularly, recommended is the balancing of
 faith with understanding and concentration with energy.
 Now, he is going to give another explanation, different
 from the above paragraph.
 So here, the translation is not what it should be.
 So the paragraph 48 says differently from what has been
 said above.
 So what has been said above is that faith should be
 balanced with understanding and concentration should be
 balanced with energy.
 But here, it is said that a person who is practicing Sam
atha meditation, faith, even strong, is suitable or
 permissible.
 So if faith is stronger than other factors, it is still
 good, permissible.
 Because by faith, he will make effort and get what he wants
, that is, concentration and absorption or jhana.
 And then, with regard to concentration and understanding,
 not concentration and energy, here, concentration and
 understanding.
 For one, practicing Samatha meditation, concentration can
 be stronger.
 When you are practicing Samatha meditation, then
 concentration, stronger is permissible, stronger is good.
 Because by this strong concentration, you will get to
 absorption, you will get to the jhana stage.
 And for one working on insight that is practicing Vipassana
 meditation, understanding stronger is permissible.
 Understanding should be stronger, could be stronger.
 Because with that understanding or penetrative knowledge,
 he will reach the penetration of characteristics, that
 means the impermanence, suffering and soullessness.
 So, for one who is practicing Samatha meditation, his
 stronger concentration is permissible.
 And for one who is practicing Vipassana meditation,
 stronger understanding is permissible.
 But with the balancing of the two, he reaches absorption as
 well.
 We can ask, "What if they are balanced?" It's okay.
 When they are balanced, he can get absorption as well. He
 can get jhana absorption.
 Now, when you practice meditation and you are trying to get
 jhana, then there can be stronger concentration. It's okay.
 But when you practice meditation to get to the super
 mundane attainment, then the balancing of the faculties is
 a must.
 So, this is what is meant in this paragraph.
 The translation here is not quite like that. So, it is
 different from what has been said above.
 The faculties should be balanced. But in this paragraph, it
 says, "Even though they are not balanced, not perfectly
 balanced, it's still okay."
 Then, what about mindfulness? Mindfulness is needed
 everywhere. Mindfulness is like a seasoning of salt in all
 dishes.
 So, there is no instance where mindfulness is in excess. So
, mindfulness is always needed.
 The concentration used in 48 and 47 is shamatha. It's not j
hana or samadhi.
 So, concentration in 47 is samadhi. But in 48, one working
 on concentration means one practicing samatha meditation.
 For him, samadhi can be stronger than pañña.
 Concentration. Samadhi, jhana and shamatha can be
 translated as concentration in different contexts.
 And they are all slightly different.
 I was asking, I was trying to see which the translation was
 using.
 In Bali, samatha and samadhi can be synonymous. It is a
 concentration of mind.
 Samatha is translated as tranquility. But samatha and samad
hi can be synonymous.
 But samadhi is maybe wider than samatha because in Vipass
ana also we need samadhi.
 So, samadhi is the ability of the mind to be stuck to the
 object, to be on the object.
 And samatha is defined as stealing of hindrances or getting
 rid of hindrances.
 But they are used anonymously in some contexts, samatha and
 samadhi.
 And concentration here sometimes means samatha and
 sometimes it really means samadhi.
 And samatha has a fixed object and samadhi can have either
 a fixed or a variant object.
 That's right, yes. Because samadhi in Vipassana meditation
 doesn't have fixed object.
 Although if you can be on a fixed object it is better.
 But since when you practice Vipassana meditation you have
 to be mindful of everything
 that becomes prominent at the present moment then the
 object cannot be fixed.
 And Jhana in these definitions is a particularly developed
 kind of samatha concentration.
 Yes, Jhana is in the realm of samatha meditation.
 Then there are others, skill in the sign and then the exact
 of mind on and location when it should be accepted and so
 on.
 Now, on our way we talk about the book called the way of
 mindfulness.
 Now, for some details with regard to some paragraphs I
 would like to refer to that book.
 So, for details on paragraph 54 you may read that book, the
 way of mindfulness by Soumatiya.
 S-O-M-A. So, I'll give you a page number also. 175 of that
 book.
 I won't put these books on reserve downstairs.
 I think we do but I don't check. I'm pretty sure we do.
 And then for paragraph 55 also then that book page 181 and
 so on.
 And in that paragraph 1, 2, 3, 4th line, the mundane and
 supermundane distinctions.
 Mundane distinctions means attainment of Jhana.
 And supermundane distinctions means attainment of
 enlightenment.
 Distinction here means Jhana and enlightenment.
 And then for paragraph 56 you may read that book, way of
 mindfulness page 185.
 185.
 And then for paragraph 60, way of mindfulness 186 and
 paragraph 61, way of mindfulness 187.
 And paragraph 62, 189.
 So, here I describe how to exert the mind on occasions and
 how to restrain, how to suppress the mind on occasions.
 And so, if a person cannot get absorption just by avoiding
 the seven that are unsuitable,
 then he should do these ten things.
 Skill and absorption means skill in getting or reaching
 absorption.
 So, these ten things he should do.
 And even at that stage, after acquiring skill in the ten,
 after acquiring the ten skills,
 he could not reach the Jhana stage and he should not
 despair.
 He should do what is called equalizing the effort of energy
 and concentration.
 So, the verses just mean that.
 Let me see where.
 Here, no matter how small or tall, a man was in temperament
, noticed how his mind can plan.
 Energy and serenity always he couples each to each.
 So, he should balance these two.
 And then the commentator gives five similes.
 In this edition, the word is four, not five, but it should
 be five.
 I think you have five there on page 141.
 It is corrected in the other edition.
 But in that book, you have four and not five.
 Four?
 But it should be five.
 You can just...
 Five is correct.
 Because if you read them, you get five similes there.
 So, the first is the bee getting pollen and the second is
 the medical student practicing surgery on a lotus leaf.
 And the third is taking the spider's thread or spider's web
.
 And the fourth is what?
 The ship.
 The skipper.
 The skipper, yes.
 And the fifth is...
 All two.
 So, there are five similes given here.
 And also, at the beginning of the first simile, one is too
 clever bee.
 Now, there is a variant reading which means just not clever
.
 So, we can take any.
 Even if you take too clever, it doesn't hurt the meaning.
 So, when a too clever bee or just when a not clever bee,
 the same with the other similes.
 So, it gives these five similes.
 And these five similes are just to teach us that we must
 not be too eager or too lax in our efforts.
 We must equalize or we must balance concentration with
 energy.
 And if we can keep energy and concentration on the same
 level,
 then we will get what we are striving for or we will get
 the absorption stage.
 So, next we will go to the first genre.
 The reason that the first meditation is on the Earth, Cass
ina,
 is because it's an easy concentration.
 Yes, yes.
 It's a natural concentration or it's...
 Yes, for those who have had experience in the past, they
 come naturally.
 They just look at the Earth and then they call the sign.
 And with other types of Cassinas, sometimes you just look
 at a pond or water and then you get the sign.
 So, it's just like natural.
 Is there some usual length of color to develop absorption
 using the system or is there a great variation?
 There may be variation between one individual and another.
 It depends on how he practices and also how much past
 experience he has.
 Are there pictures of Cassinas so we can see a typical Cass
ina object?
 I have a color Cassina. I can bring it next time or next
 week.
 Because color Cassina is easier to make than Earth Cassina.
 You find a board and then paint it blue, yellow, red or
 white.
 There is one lady who is actually practicing color Cassina.
 She lives in Marin County.
 She wanted to practice Mita meditation.
 And so I suggested she practice the White Cassina
 meditation first
 and then get the conceptualized image of that disc.
 And then after getting that, she could put anybody in that
 disc and then practice loving kindness with that person.
 That's effective.
 I don't know how far she has gone in this meditation, but
 she said she was doing that.
 Now getting the mental image clearly, mentally, that's
 different from seeing the counterpart sign.
 There are two signs. The first one is called memorized sign
.
 You first look at it and you memorize it and then you close
 your eyes and you can see as clearly as you use your eyes
 open.
 But at that stage, your image is the exact image of the
 disc.
 If there are defects on the disc, they appear in your
 memory too.
 And then you concentrate on that memorized image.
 That is, you close your eyes and you look at that image
 again and again and then it becomes more refined.
 And so gradually the faults disappear and it becomes smooth
 and clean.
 So that is the stage where you get the counterpart sign. So
 that is called the counterpart sign.
 So it's transformed.
 That's right.
 You mean into a relationship experience with the concent
rator.
 So actually the two signs differ in degree of clarity.
 It's not so conceptualization. The second one is more
 conceptualized.
 It is, yeah.
 And the counterpart sign is also a conception.
 It is not a reality because it is born of a perception,
 born of your mind.
 So it exists in your mind and so it is a kind of concept.
 Is it something that you just know you have when you see it
 or attain it?
 Yes, yeah.
 Because you alone know that you have that. Because it's in
 your mind.
 So whenever you come down and you close your eyes and memor
ize it, then it will appear to you.
 Is this a meditation that's done just at a particular time
 or is it something that can be done, say, for the rest of
 your life?
 No, not rest of your life, but just to reach this
 particular time and to reach a certain state of absorption.
 Because the Buddha's wish is for people to practice samatha
 meditation as a basis for vipassana meditation.
 So first you practice samatha meditation and then you
 concentrate or you contemplate on the practice itself or on
 the other objects as impermanence and so on.
 So you go to vipassana after you practice samatha
 meditation.
 Okay.
 Okay.
 Okay.
