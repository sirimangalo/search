1
00:00:00,000 --> 00:00:04,800
 I'm going to get our meditators to come and sit and listen.

2
00:00:04,800 --> 00:00:13,400
 Hello, we're now live on YouTube, December 4th.

3
00:00:13,400 --> 00:00:16,840
 We've just finished the Dhammapada video, so now the rest

4
00:00:16,840 --> 00:00:19,880
 of this is going to be about

5
00:00:19,880 --> 00:00:22,600
 lots of different things potentially.

6
00:00:22,600 --> 00:00:27,570
 Maybe some questions, maybe some news, maybe some

7
00:00:27,570 --> 00:00:30,360
 announcements of our own.

8
00:00:30,360 --> 00:00:32,940
 But I was thinking, as I was saying, I have two meditators

9
00:00:32,940 --> 00:00:33,360
 now.

10
00:00:33,360 --> 00:00:34,660
 This is my big news.

11
00:00:34,660 --> 00:00:36,910
 I have two international, we're now an international,

12
00:00:36,910 --> 00:00:39,680
 officially international meditation center

13
00:00:39,680 --> 00:00:42,960
 because we have two meditators from two different countries

14
00:00:42,960 --> 00:00:44,320
 that are not Canada.

15
00:00:44,320 --> 00:00:47,320
 We have one meditator from Austria and one meditator from

16
00:00:47,320 --> 00:00:48,000
 Finland.

17
00:00:48,000 --> 00:00:52,240
 Now that's impressive.

18
00:00:52,240 --> 00:00:53,240
 That's great.

19
00:00:53,240 --> 00:00:57,160
 So way to go everyone to make this a reality.

20
00:00:57,160 --> 00:00:59,470
 We're not using someone else's meditation center, someone

21
00:00:59,470 --> 00:01:00,800
 else's monastery, we're not

22
00:01:00,800 --> 00:01:02,400
 relying on them.

23
00:01:02,400 --> 00:01:07,240
 We put this together as a community and we now have two med

24
00:01:07,240 --> 00:01:08,120
itators.

25
00:01:08,120 --> 00:01:10,400
 We have three actually, if you count.

26
00:01:10,400 --> 00:01:12,440
 I don't know which we should.

27
00:01:12,440 --> 00:01:15,040
 He's from Sri Lanka.

28
00:01:15,040 --> 00:01:16,800
 So we have three different countries.

29
00:01:16,800 --> 00:01:21,140
 On top of that, we had two visitors tonight, one from Sri

30
00:01:21,140 --> 00:01:23,240
 Lanka, one from Canada.

31
00:01:23,240 --> 00:01:31,720
 I think Sri Lanka, I think is Admali and Sarah came, two

32
00:01:31,720 --> 00:01:35,080
 friends.

33
00:01:35,080 --> 00:01:36,160
 They meditated with us.

34
00:01:36,160 --> 00:01:41,780
 They listened to our chanting and then meditated and then

35
00:01:41,780 --> 00:01:42,640
 left.

36
00:01:42,640 --> 00:01:45,450
 But I'm going to make it a point that Monday, Wednesday,

37
00:01:45,450 --> 00:01:47,320
 Friday, they can come up and listen

38
00:01:47,320 --> 00:01:48,320
 to the Dhamma Pādhi.

39
00:01:48,320 --> 00:01:49,320
 It's useful, right?

40
00:01:49,320 --> 00:01:53,770
 Then they get a Dhamma talk, something light to kind of

41
00:01:53,770 --> 00:01:56,400
 relieve the burden of the heavy

42
00:01:56,400 --> 00:01:57,400
 meditation.

43
00:01:57,400 --> 00:02:03,240
 It's nice to have that light counterpoint, some stories for

44
00:02:03,240 --> 00:02:05,400
 them to think about.

45
00:02:05,400 --> 00:02:10,050
 I have to remind me, Robin, if you have to ask me next

46
00:02:10,050 --> 00:02:13,000
 Monday, are the meditators there?

47
00:02:13,000 --> 00:02:20,160
 There's only one that one woman from Austria is leaving

48
00:02:20,160 --> 00:02:21,640
 Monday.

49
00:02:21,640 --> 00:02:27,880
 He should come listen to the other one.

50
00:02:27,880 --> 00:02:34,720
 So do you have any announcements?

51
00:02:34,720 --> 00:02:39,200
 You're talking about books, Book Thing is moving along?

52
00:02:39,200 --> 00:02:42,920
 It is, yes.

53
00:02:42,920 --> 00:02:46,960
 The book project is moving along very well.

54
00:02:46,960 --> 00:02:52,000
 We are up to $877, which is awesome.

55
00:02:52,000 --> 00:02:54,770
 That'll make a nice gift for the children's home because it

56
00:02:54,770 --> 00:02:56,280
 far exceeds the cost of printing

57
00:02:56,280 --> 00:02:57,280
 of the booklets.

58
00:02:57,280 --> 00:02:58,280
 So it's wonderful.

59
00:02:58,280 --> 00:03:23,480
 Do you have a couple of questions?

60
00:03:23,480 --> 00:03:24,480
 Okay, I'm ready.

61
00:03:24,480 --> 00:03:25,480
 Okay.

62
00:03:25,480 --> 00:03:32,480
 Sorry, just give me one minute.

63
00:03:32,480 --> 00:03:36,480
 Sure.

64
00:03:36,480 --> 00:04:02,480
 Okay, something started and it's better I don't do it.

65
00:04:02,480 --> 00:04:14,520
 I'll wait and do it later.

66
00:04:14,520 --> 00:04:15,920
 Go for questions first.

67
00:04:15,920 --> 00:04:16,920
 Okay.

68
00:04:16,920 --> 00:04:17,920
 Hello, Bante.

69
00:04:17,920 --> 00:04:21,040
 I have a question unrelated to meditation.

70
00:04:21,040 --> 00:04:23,960
 You learned this practice in a part of the world that is

71
00:04:23,960 --> 00:04:24,760
 tropical.

72
00:04:24,760 --> 00:04:27,540
 Why when you move to a much colder environment would you be

73
00:04:27,540 --> 00:04:29,080
 constrained to wear the flimsy

74
00:04:29,080 --> 00:04:32,080
 robe that are plenty in Sri Lanka?

75
00:04:32,080 --> 00:04:35,820
 It seems to put an unnecessary hardship on the monk and

76
00:04:35,820 --> 00:04:37,680
 could in fact be lethal.

77
00:04:37,680 --> 00:04:43,810
 Yeah, I mean, if you were to wear Sri Lankan robes, could

78
00:04:43,810 --> 00:04:46,480
 potentially be lethal.

79
00:04:46,480 --> 00:04:50,300
 Those robes are like tissue paper.

80
00:04:50,300 --> 00:04:54,530
 This robe on the other hand is quite thick, could be even

81
00:04:54,530 --> 00:04:56,980
 thicker, could be made of wool.

82
00:04:56,980 --> 00:04:59,670
 And in fact, I have another robe that is thicker and is

83
00:04:59,670 --> 00:05:01,520
 made of wool and keeps me perfectly

84
00:05:01,520 --> 00:05:04,840
 warm, even in Canada winter.

85
00:05:04,840 --> 00:05:08,480
 No, it's funny.

86
00:05:08,480 --> 00:05:13,380
 I had this funny sort of conversation with, remember I was

87
00:05:13,380 --> 00:05:16,040
 telling you about this Chinese

88
00:05:16,040 --> 00:05:20,020
 monk who came to visit the abbot of this monastery in

89
00:05:20,020 --> 00:05:22,160
 Toronto, good friend.

90
00:05:22,160 --> 00:05:29,430
 It's funny how we get so stuck on our, it's kind of proud,

91
00:05:29,430 --> 00:05:31,080
 you know?

92
00:05:31,080 --> 00:05:33,430
 That in the sense of thinking we're doing it the right way

93
00:05:33,430 --> 00:05:35,000
 because he does things differently

94
00:05:35,000 --> 00:05:37,120
 as a Mahayanaman.

95
00:05:37,120 --> 00:05:39,520
 And it was kind of a bit of a turn off.

96
00:05:39,520 --> 00:05:45,120
 I kind of accept it because I've seen it so many times.

97
00:05:45,120 --> 00:05:53,880
 But it started because he wears special sandals that

98
00:05:53,880 --> 00:05:57,400
 actually have holes.

99
00:05:57,400 --> 00:06:03,380
 So they don't go all around, but they cover the soles of

100
00:06:03,380 --> 00:06:06,480
 the feet and they have a heel.

101
00:06:06,480 --> 00:06:08,240
 But they have slots in them.

102
00:06:08,240 --> 00:06:10,270
 And I was looking at it and I said, "Well, how do you avoid

103
00:06:10,270 --> 00:06:10,800
 the snow?

104
00:06:10,800 --> 00:06:12,280
 Because the snow will get in through those."

105
00:06:12,280 --> 00:06:15,480
 And he said, "Well, you'd be very careful."

106
00:06:15,480 --> 00:06:18,530
 And he said, "But no, I said, so all of monks in your

107
00:06:18,530 --> 00:06:20,400
 tradition have to wear those?"

108
00:06:20,400 --> 00:06:22,800
 And he said, "Yes, all year round, that's all you can wear.

109
00:06:22,800 --> 00:06:25,640
 Yeah, that's all we wear."

110
00:06:25,640 --> 00:06:36,760
 And he said the same thing about the robes.

111
00:06:36,760 --> 00:06:40,830
 His whole idea of the robes was like it was the same kind

112
00:06:40,830 --> 00:06:41,200
 of thing.

113
00:06:41,200 --> 00:06:43,400
 Why do you have to wear that specific type of robe?

114
00:06:43,400 --> 00:06:46,360
 Why don't you wear robes that are more like lay people in--

115
00:06:46,360 --> 00:06:47,800
" He didn't question like that,

116
00:06:47,800 --> 00:06:48,800
 but he was implying.

117
00:06:48,800 --> 00:06:52,490
 He said, "Oh, well, we wear robes that are similar to

118
00:06:52,490 --> 00:06:54,440
 people of modern times."

119
00:06:54,440 --> 00:06:57,210
 And he said, "Well, we wear robes that are similar to

120
00:06:57,210 --> 00:06:58,720
 people of modern times."

121
00:06:58,720 --> 00:07:02,520
 And he said, "Well, we wear robes that are similar to

122
00:07:02,520 --> 00:07:03,280
 people of modern times."

123
00:07:03,280 --> 00:07:06,200
 And he said, "Well, we wear robes that are similar to

124
00:07:06,200 --> 00:07:07,680
 people of modern times."

125
00:07:07,680 --> 00:07:10,930
 And he said, "Well, we wear robes that are similar to

126
00:07:10,930 --> 00:07:12,720
 people of modern times."

127
00:07:12,720 --> 00:07:15,870
 And he said, "Well, we wear robes that are similar to

128
00:07:15,870 --> 00:07:17,520
 people of modern times."

129
00:07:17,520 --> 00:07:20,530
 And he said, "Well, we wear robes that are similar to

130
00:07:20,530 --> 00:07:22,240
 people of modern times."

131
00:07:22,240 --> 00:07:27,400
 I think it's a fair criticism.

132
00:07:27,400 --> 00:07:33,150
 What was my point exactly there was in relation to your

133
00:07:33,150 --> 00:07:34,520
 question?

134
00:07:34,520 --> 00:07:41,000
 But the point is really this.

135
00:07:41,000 --> 00:07:45,050
 This isn't a specific type of thing that monks in Sri Lanka

136
00:07:45,050 --> 00:07:47,120
 wear because it's in Sri Lanka

137
00:07:47,120 --> 00:07:48,960
 or because it's in the tropic.

138
00:07:48,960 --> 00:07:56,200
 There is a very important non-biased, non-specific, non-

139
00:07:56,200 --> 00:08:01,840
culturally based reason for wearing robes.

140
00:08:01,840 --> 00:08:04,960
 It's the most simple piece of cloth that you can wear.

141
00:08:04,960 --> 00:08:06,300
 That's defensible, I think.

142
00:08:06,300 --> 00:08:08,240
 You want to disagree with the importance of that?

143
00:08:08,240 --> 00:08:09,240
 Well, fine.

144
00:08:09,240 --> 00:08:10,800
 There's nothing to do with culture.

145
00:08:10,800 --> 00:08:13,380
 It's because we're wearing two rectangles, three

146
00:08:13,380 --> 00:08:16,080
 potentially, but they're perfect rectangles.

147
00:08:16,080 --> 00:08:17,280
 There's nothing special.

148
00:08:17,280 --> 00:08:19,200
 There's not allowed sleeves.

149
00:08:19,200 --> 00:08:21,000
 We're not allowed buttons.

150
00:08:21,000 --> 00:08:23,560
 The closest thing we have to button is something like a

151
00:08:23,560 --> 00:08:24,600
 button to keep it.

152
00:08:24,600 --> 00:08:28,820
 There is something like a button, but it's just pieces of

153
00:08:28,820 --> 00:08:30,620
 thick thread, thick string

154
00:08:30,620 --> 00:08:35,160
 that make a button and a loop, but it's still just string.

155
00:08:35,160 --> 00:08:40,330
 And that's allowed because it helps keep it closed and so

156
00:08:40,330 --> 00:08:40,920
 on.

157
00:08:40,920 --> 00:08:42,240
 But that's it.

158
00:08:42,240 --> 00:08:44,480
 And so that really keeps you...

159
00:08:44,480 --> 00:08:52,310
 I mean, it keeps you humble, it keeps you focused, you know

160
00:08:52,310 --> 00:08:53,760
, that all you have is a rectangle,

161
00:08:53,760 --> 00:08:55,880
 two rectangles.

162
00:08:55,880 --> 00:08:58,760
 So it's much more about the shape than it is about the

163
00:08:58,760 --> 00:09:00,640
 thickness or the practicality

164
00:09:00,640 --> 00:09:04,360
 or so on, because this robe can be just as practical.

165
00:09:04,360 --> 00:09:08,400
 I've been through the winter in Winnipeg wearing just these

166
00:09:08,400 --> 00:09:09,600
 kind of robes.

167
00:09:09,600 --> 00:09:12,570
 And people say, "Oh, no, many monks have given up the robes

168
00:09:12,570 --> 00:09:12,840
."

169
00:09:12,840 --> 00:09:16,610
 Even in the Theravada tradition, they've started to even

170
00:09:16,610 --> 00:09:19,520
 make up new ideas, new special uniforms,

171
00:09:19,520 --> 00:09:21,560
 and the Sri Lankans have their new special uniforms.

172
00:09:21,560 --> 00:09:24,160
 They don't wear robes.

173
00:09:24,160 --> 00:09:25,780
 They started to wear shirts.

174
00:09:25,780 --> 00:09:32,880
 And even monks in Los Angeles have started wearing shirts

175
00:09:32,880 --> 00:09:35,000
 in the summer.

176
00:09:35,000 --> 00:09:36,280
 What's up with that?

177
00:09:36,280 --> 00:09:39,000
 It's become easier.

178
00:09:39,000 --> 00:09:42,930
 And part of it, I think, is this desire to look like

179
00:09:42,930 --> 00:09:46,040
 everyone else, which is not proper.

180
00:09:46,040 --> 00:09:49,520
 We're different, where we've taken on a vow to be

181
00:09:49,520 --> 00:09:51,480
 externally different.

182
00:09:51,480 --> 00:09:52,480
 And it's external.

183
00:09:52,480 --> 00:09:53,480
 It's not internal.

184
00:09:53,480 --> 00:09:56,040
 A monk isn't internally different from a lay person.

185
00:09:56,040 --> 00:09:57,040
 They're still a person.

186
00:09:57,040 --> 00:09:58,040
 They still have defilements.

187
00:09:58,040 --> 00:10:00,560
 They still have issues that they have to deal with.

188
00:10:00,560 --> 00:10:04,080
 But they've taken on an external restraint, constraint.

189
00:10:04,080 --> 00:10:08,680
 And so to give that up, you're no longer a monk.

190
00:10:08,680 --> 00:10:11,680
 You're no longer following this tradition.

191
00:10:11,680 --> 00:10:12,680
 And it's external.

192
00:10:12,680 --> 00:10:13,880
 It's not the most important thing.

193
00:10:13,880 --> 00:10:16,080
 I've said many times, being a monk isn't important.

194
00:10:16,080 --> 00:10:18,840
 But if you're going to do it, it's useful.

195
00:10:18,840 --> 00:10:22,780
 The external life of a monk is an important system that the

196
00:10:22,780 --> 00:10:24,400
 Buddha put in place.

197
00:10:24,400 --> 00:10:25,400
 People say it's just concepts.

198
00:10:25,400 --> 00:10:28,470
 Well, yeah, but it's the Buddha's concept of what a

199
00:10:28,470 --> 00:10:30,520
 religious person should be like.

200
00:10:30,520 --> 00:10:32,520
 They should wear this kind of cloth and so on.

201
00:10:32,520 --> 00:10:35,600
 They should be limited to cloth.

202
00:10:35,600 --> 00:10:37,470
 Someone asked me recently, "Why can't you wear black cloth

203
00:10:37,470 --> 00:10:37,720
?"

204
00:10:37,720 --> 00:10:38,720
 I said, "What color cloth?"

205
00:10:38,720 --> 00:10:41,040
 She said, "Well, we can't wear black, for example."

206
00:10:41,040 --> 00:10:42,440
 Or she said, "Can you wear black?"

207
00:10:42,440 --> 00:10:43,440
 I said, "No, why not?"

208
00:10:43,440 --> 00:10:48,210
 And the answer is because black is a-- it could be

209
00:10:48,210 --> 00:10:50,440
 something of value.

210
00:10:50,440 --> 00:10:52,900
 This kind of color, the color that we're supposed to be

211
00:10:52,900 --> 00:10:54,800
 wearing is kind of puke-y color.

212
00:10:54,800 --> 00:10:57,440
 It's the color that no one would want to wear on purpose.

213
00:10:57,440 --> 00:11:00,360
 Because you want something that's not going to make you

214
00:11:00,360 --> 00:11:02,040
 think, "Oh, I have this wonderful,

215
00:11:02,040 --> 00:11:03,880
 beautiful robe."

216
00:11:03,880 --> 00:11:06,650
 We have to-- when you first get the robe, you have to mark

217
00:11:06,650 --> 00:11:07,120
 it.

218
00:11:07,120 --> 00:11:08,480
 I had to mark this one.

219
00:11:08,480 --> 00:11:12,340
 Before I put it on, I had to mark it with a pen, just as a

220
00:11:12,340 --> 00:11:14,400
 symbol that it's no longer

221
00:11:14,400 --> 00:11:15,520
 pure.

222
00:11:15,520 --> 00:11:18,660
 Keep in my mind the fact that-- or to make it clear in my

223
00:11:18,660 --> 00:11:20,280
 mind that this is a soiled

224
00:11:20,280 --> 00:11:23,920
 piece of cloth.

225
00:11:23,920 --> 00:11:28,670
 And so there's many reasons to stick to the way that the

226
00:11:28,670 --> 00:11:30,120
 Buddha had it.

227
00:11:30,120 --> 00:11:33,300
 There's nothing to do with this country, that country, hot

228
00:11:33,300 --> 00:11:36,640
 or cold, because it can be thick.

229
00:11:36,640 --> 00:11:39,220
 And so this argument that you can't wear these robes

230
00:11:39,220 --> 00:11:41,000
 because they're too thin, you have to

231
00:11:41,000 --> 00:11:43,240
 switch to wearing coats, is ridiculous.

232
00:11:43,240 --> 00:11:47,230
 Because I can put on my thick robe and walk through a snow

233
00:11:47,230 --> 00:11:50,200
storm, 20-- minus 20, minus

234
00:11:50,200 --> 00:11:53,200
 30 I was in in Winnipeg, walking down the street.

235
00:11:53,200 --> 00:11:58,080
 It was cold, but so was everyone else.

236
00:11:58,080 --> 00:12:02,920
 And perfectly livable.

237
00:12:02,920 --> 00:12:06,640
 Not terribly comfortable, but that's normal.

238
00:12:06,640 --> 00:12:08,640
 So I own it.

239
00:12:08,640 --> 00:12:09,640
 I don't know.

240
00:12:09,640 --> 00:12:11,680
 It's not a meditation question.

241
00:12:11,680 --> 00:12:15,640
 I apologize for going on about it.

242
00:12:15,640 --> 00:12:19,580
 It's important in that sense from a monastic point of view,

243
00:12:19,580 --> 00:12:21,640
 looking at it as a monastic.

244
00:12:21,640 --> 00:12:22,960
 My teachers commented on this.

245
00:12:22,960 --> 00:12:26,180
 I remember there was a Mahayana monk came to visit us who

246
00:12:26,180 --> 00:12:28,040
 wore pants, and under his robe

247
00:12:28,040 --> 00:12:30,880
 he wore pants.

248
00:12:30,880 --> 00:12:37,420
 And Ajahn Tom looked at him and said, this is the end of

249
00:12:37,420 --> 00:12:38,880
 the robes.

250
00:12:38,880 --> 00:12:41,650
 The end of the robes means that we have a prophecy that

251
00:12:41,650 --> 00:12:43,320
 eventually Buddhism is going,

252
00:12:43,320 --> 00:12:46,160
 of course, to disappear from the world.

253
00:12:46,160 --> 00:12:49,990
 And the last sign is when the robes disappear, when there's

254
00:12:49,990 --> 00:12:52,200
 nobody living even the external

255
00:12:52,200 --> 00:12:53,200
 life anymore.

256
00:12:53,200 --> 00:12:56,440
 The internal life will disappear first.

257
00:12:56,440 --> 00:12:59,150
 But the very final sign that Buddhism has disappeared is

258
00:12:59,150 --> 00:13:00,920
 when people stop wearing robes.

259
00:13:00,920 --> 00:13:05,160
 So it's a bad sign when the monks have already, even in the

260
00:13:05,160 --> 00:13:07,520
 Theravada tradition, have already

261
00:13:07,520 --> 00:13:11,400
 sort of put aside the importance of the robes.

262
00:13:11,400 --> 00:13:13,200
 They put aside the importance of the bowl.

263
00:13:13,200 --> 00:13:16,640
 There's much less going on alms round.

264
00:13:16,640 --> 00:13:21,520
 In Sri Lanka, very few monks go on alms round, which is a

265
00:13:21,520 --> 00:13:21,960
 shame.

266
00:13:21,960 --> 00:13:27,910
 Especially when Sri Lankan people are so happy to give alms

267
00:13:27,910 --> 00:13:28,240
.

268
00:13:28,240 --> 00:13:32,780
 Poor people would give what they had, but that people would

269
00:13:32,780 --> 00:13:34,320
 give a piece of portion

270
00:13:34,320 --> 00:13:36,600
 of their food is just quite impressive.

271
00:13:36,600 --> 00:13:42,960
 It's a great spiritual practice to go through the village.

272
00:13:42,960 --> 00:13:49,960
 Anyway, I hope that answers your question.

273
00:13:49,960 --> 00:13:55,000
 I would like to understand why it is improper to eat

274
00:13:55,000 --> 00:13:56,480
 afternoon.

275
00:13:56,480 --> 00:13:57,480
 Thank you so much.

276
00:13:57,480 --> 00:14:00,880
 Well, it's not about afternoon per se.

277
00:14:00,880 --> 00:14:04,600
 It's about eating enough just to survive.

278
00:14:04,600 --> 00:14:07,060
 So you could argue, well, why not?

279
00:14:07,060 --> 00:14:09,200
 What's wrong with eating at 1 p.m.?

280
00:14:09,200 --> 00:14:13,000
 I think the deal is, what do you need to survive?

281
00:14:13,000 --> 00:14:14,480
 What's the most important?

282
00:14:14,480 --> 00:14:17,440
 What's essential and what's unessential?

283
00:14:17,440 --> 00:14:19,480
 And so eating in the afternoon isn't really all that

284
00:14:19,480 --> 00:14:20,760
 essential because you're not going

285
00:14:20,760 --> 00:14:22,760
 to put it to much use.

286
00:14:22,760 --> 00:14:25,410
 Eating in the morning, however, is essential because you

287
00:14:25,410 --> 00:14:26,760
 need it to get you through the

288
00:14:26,760 --> 00:14:29,120
 day, through all the work that you have to do.

289
00:14:29,120 --> 00:14:32,680
 I mean, even if that work is just meditating.

290
00:14:32,680 --> 00:14:37,220
 It works actually quite perfectly, especially when you have

291
00:14:37,220 --> 00:14:39,560
 the Buddha's allowance to have

292
00:14:39,560 --> 00:14:45,050
 juice in the afternoon, pure, strained juice without any

293
00:14:45,050 --> 00:14:47,220
 pulp or fruit in it.

294
00:14:47,220 --> 00:14:49,720
 It's just enough.

295
00:14:49,720 --> 00:14:53,120
 You have a good meal in the morning.

296
00:14:53,120 --> 00:14:55,310
 And then in the afternoon, when you start to get a little

297
00:14:55,310 --> 00:14:56,480
 tired, you have some juice

298
00:14:56,480 --> 00:14:59,890
 and the sugar in the juice perks you up just enough to

299
00:14:59,890 --> 00:15:02,200
 continue through the afternoon and

300
00:15:02,200 --> 00:15:10,440
 into the evening and then tide you over until morning.

301
00:15:10,440 --> 00:15:13,720
 So it's just more about getting, parsing it down to the

302
00:15:13,720 --> 00:15:15,400
 bare minimum so you don't have

303
00:15:15,400 --> 00:15:19,840
 to worry about food, so you don't have to get involved with

304
00:15:19,840 --> 00:15:21,840
 storing food and so on.

305
00:15:21,840 --> 00:15:24,350
 But imagine if we ate in the evening, then it would be

306
00:15:24,350 --> 00:15:25,960
 about storing food or going out

307
00:15:25,960 --> 00:15:28,480
 for food again.

308
00:15:28,480 --> 00:15:31,960
 Really cuts into your meditation practice.

309
00:15:31,960 --> 00:15:37,960
 Hello, Bante.

310
00:15:37,960 --> 00:15:41,480
 In my home, my mom does not believe in Buddhism.

311
00:15:41,480 --> 00:15:45,880
 I cannot practice sitting and walking meditation at all.

312
00:15:45,880 --> 00:15:54,360
 Perhaps should I put it in my daily routine?

313
00:15:54,360 --> 00:15:56,640
 I think they're asking what their daily routine should be

314
00:15:56,640 --> 00:15:57,960
 if they're not allowed to practice

315
00:15:57,960 --> 00:16:00,840
 sitting and walking meditation.

316
00:16:00,840 --> 00:16:05,960
 You can't tell me you're not allowed to sit down.

317
00:16:05,960 --> 00:16:08,850
 You could sit down and close your eyes, sit in a chair in

318
00:16:08,850 --> 00:16:10,680
 order to make it look like you're

319
00:16:10,680 --> 00:16:12,360
 meditating.

320
00:16:12,360 --> 00:16:13,640
 Maybe you can't do walking meditation.

321
00:16:13,640 --> 00:16:19,040
 Well, then just go for a walk outside or something.

322
00:16:19,040 --> 00:16:22,910
 It's really quite a sad state and it's something that you

323
00:16:22,910 --> 00:16:25,400
 really probably want to work at.

324
00:16:25,400 --> 00:16:28,120
 Send your mom loving kindness.

325
00:16:28,120 --> 00:16:29,120
 Send your mom good thoughts.

326
00:16:29,120 --> 00:16:30,120
 Make sure you be happy.

327
00:16:30,120 --> 00:16:31,120
 Make sure you're well.

328
00:16:31,120 --> 00:16:32,120
 Make sure you be free from suffering.

329
00:16:32,120 --> 00:16:34,120
 Do that after your meditation.

330
00:16:34,120 --> 00:16:38,480
 And absolutely just close your eyes and meditate.

331
00:16:38,480 --> 00:16:41,800
 At the same time, yes, incorporate it into your daily life.

332
00:16:41,800 --> 00:16:45,460
 But I also would recommend sending love to your mother,

333
00:16:45,460 --> 00:16:46,840
 wishing her to be happy and free

334
00:16:46,840 --> 00:16:51,170
 because that'll break up that horrible, horrible mind state

335
00:16:51,170 --> 00:16:53,320
 that wants to prevent you from

336
00:16:53,320 --> 00:16:55,680
 meditating, it sounds like.

337
00:16:55,680 --> 00:16:58,400
 Because it's not important that she believes in it.

338
00:16:58,400 --> 00:17:02,510
 The question is whether she allows you to do walking and

339
00:17:02,510 --> 00:17:04,400
 sitting, which seems pretty

340
00:17:04,400 --> 00:17:07,400
 innocuous to me.

341
00:17:07,400 --> 00:17:20,640
 And with that, we are caught up on questions.

342
00:17:20,640 --> 00:17:22,080
 And with that, it's 10 p.m.

343
00:17:22,080 --> 00:17:23,080
 So we've done an hour.

344
00:17:23,080 --> 00:17:28,000
 It's a good time to stop.

345
00:17:28,000 --> 00:17:29,000
 Thanks everyone.

346
00:17:29,000 --> 00:17:31,440
 Thank you, Robin, for helping.

347
00:17:31,440 --> 00:17:33,000
 Thank you, Bante.

348
00:17:33,000 --> 00:17:35,000
 Have a good night.

349
00:17:35,000 --> 00:17:36,440
 See you tomorrow.

350
00:17:36,440 --> 00:17:37,440
 Good night.

351
00:17:37,440 --> 00:17:37,440
 Thank you.

352
00:17:37,440 --> 00:17:38,440
 Thank you.

353
00:17:38,440 --> 00:17:39,440
 Thank you.

354
00:17:39,440 --> 00:17:40,440
 Thank you.

