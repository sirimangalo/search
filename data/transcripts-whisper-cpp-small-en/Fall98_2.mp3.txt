 [
 you may want to know more about that.]
 There are two kinds of latency of mental defilements.
 The first one is latency in the mental continuum of beings,
 and the other is latency in the
 objects.
 And the latency of mental defilements, we must understand
 the mental defilements first.
 So what are the mental defilements?
 They are called mental defilements because they smear the
 mind or they make our minds
 dirty.
 So they are called mental defilements.
 And the explanation given in the commentaries is that they
 are the ones that afflict the
 mind or that torment the mind.
 So they are tormentors of mind or those that afflict the
 mind.
 So they are called Kilesa and Pali, those that defile or
 those that are defiled.
 So this word mental defilement comes to be used for the
 word Kilesa and Pali.
 Now there are ten mental defilements or ten Kilesas.
 So they begin with greed, hate, delusion, pride, wrong view
, doubt.
 And then wrong view and doubt.
 So the other four are sloth and then restlessness.
 And shamelessness in doing unwholesome deeds and fear
lessness in doing unwholesome deeds.
 You don't need to know all these ten one by one.
 So please note that they are just the mental states that
 make our minds dirty.
 So whenever one or more of these mental states arise in our
 minds, then our minds are said
 to be impure.
 We are said to have unwholesome mind or unwholesome
 consciousness.
 And the ultimate aim of Vipassana is to get rid of these
 mental defilements.
 Now purification of beings or purification of mind simply
 means eradication of these
 mental defilements.
 Once the mental defilements are eradicated and they no
 longer arise in the mind, then
 mind becomes totally pure.
 Sometimes these mental defilements really arise in our
 minds.
 For example, when we are angry, the anger arises in our
 mind.
 When we are attached to something, then there is greed or
 detachment in our mind and so
 on.
 So when they get the necessary conditions, then they arise
 in our minds.
 Now when they arise in our minds, we can do something so
 that they just stay there at
 the arising stage and they do not proceed to the stage of
 breaking the precepts or transgression.
 Both I am angry with the person.
 So when I'm angry with him, then anger has arisen in my
 mind.
 I can control that anger and not let it to reach the stage
 of transgression.
 That means to quarrel with him, to hit him, or even to kill
 him.
 So I can control my anger, not to reach that stage.
 So there are now, we see two stages of mental defilements
 that arise in our mind and then
 that reach the stage of action, doing something depending
 on that mental defilement.
 Do not live in our mind constantly, not always.
 Only once in a while they arise in our minds.
 So when they do not arise in our minds, where do they live?
 They are said to be lying dormant in our minds.
 But that does not mean that they have arisen and then they
 lie under the surface or something
 like that.
 What it means is there is a liability for them to arise.
 So there is a potential for them to arise when the
 conditions are met.
 Three levels of mental defilements, the latent level and
 then arising or coming up level,
 that means coming up to the surface, and then acting level
 or transgression level.
 So there are these three levels of mental defilements.
 Three levels, the latent level is the most difficult to
 destroy or to eradicate.
 Since there are three levels of mental defilements, Buddha
 gives us three weapons to deal with
 each one of these levels.
 And these three weapons are seela, moral purity, samadhi,
 concentration and wisdom, I mean
 bhanya, wisdom.
 When you take precepts and you really keep them, then you
 will not reach the transgression
 stage.
 Even though you are angry, you will not do anything out of
 anger, you will not kill,
 or even if you are attached to something, if you really
 desire something, you will not
 take it forcefully, you will not steal it because you have
 taken the precepts.
 So taking the precepts helps you to deal with that mental
 defilement at transgression level.
 And actually that is the easiest of the three to get rid of
, I mean to prevent.
 You just take the precepts and you keep them and even
 though you are attached to something,
 you will not take it by force, you will not steal it.
 Even though you are angry, you will not act upon anger and
 fight with the person or even
 kill him because you have taken upon yourself the precepts
 of not killing, not stealing
 and so on.
 So Sila can help us to avoid transgression, but it cannot
 help us to prevent mental defilements
 from arising in our minds.
 Now please note that Sila is actually the control of bodily
 actions and vocal actions,
 or Sila controls your body and your speech, not your mind.
 Although mind is involved when we keep precepts, when we
 try to avoid killing and so on, the
 Sila's task is to control the bodily actions and vocal
 actions, to prevent mental defilements
 from reaching the arising stage.
 We need to do some other thing and that is some
 concentration or meditation because meditation
 deals with our mind.
 Now when you meditate, you just sit and you try to control
 your mind or you try to be
 mindful of the object at the present moment.
 So by the practice of meditation, we will be able to
 prevent the mental defilements from
 reaching the arising stage.
 Two kinds of meditation, samatha or tranquility meditation
 and vipassana meditation.
 So both meditations can help us to keep these mental defile
ments away from the stage of
 arising, but then keeping them away is not absolute or not
 total, not permanent.
 So by the practice of samatha meditation or vipassana
 meditation, we are able to keep
 them away for just for some time.
 We cannot keep them away altogether from our minds.
 Because a person practices samatha meditation and he gets j
hanas and abhinyas or supernormal
 knowledges.
 So by the practice of samatha and by the attainment of jhan
as and supernormal knowledges, he is
 able to keep these mental defilements away from a longer
 period of time, maybe months
 or even years.
 But they are not eradicated.
 The mental defilements are not destroyed altogether.
 And so when they meet with necessary conditions, then these
 mental defilements will arise in
 their minds again.
 So in one of his first existences, bodhisattva was a hermit
.
 So as a hermit, he lived in the Himalayas and he practiced
 meditation and he got jhanas
 and supernormal powers.
 So one day he went to the city and met the king and the
 king was pleased with him.
 And so the king asked him to stay at the royal garden.
 And also the king offered food to him every day.
 So the hermit went to the palace every day to accept food.
 One time there was a rebellion and the king had to go out
 and suppress it.
 So before he went out, he asked his queen to take care of
 the hermit.
 So in his absence, the hermit went to the palace to receive
 food as usual.
 And whenever he went, he went through the air and he
 entered through the window.
 So one day the queen bathed and then put on very fine and
 smooth clothes, garments.
 And she was waiting for the hermit to come.
 Maybe she was lying down.
 Then the hermit came and he entered through the window.
 And when the queen heard the noise of his robes, she caught
 up.
 And unfortunately, the garment fell off and the hermit saw
 the exposed body of the queen.
 And so when he saw the queen in that condition, the mental
 defilements, long suppressed, just
 came up to him.
 So he was not able to control himself.
 So by the help of Samatha meditation, one can keep the
 mental defilements away for some
 time only and not for all time.
 Like in this story, these mental defilements can just arise
 when there are conditions for
 them.
 This reminds me of a story that was told in Burma and that
 was popular in Burma.
 Now about 200 years ago, there was a king and he had a
 minister who was very clever.
 So that king had a cat.
 He trained his cat so well that when he did the service to
 the Buddha statue, when he
 bow down to the statue and did the recitation, the cat hold
 the candle for him in his hands.
 So he was very proud of that cat and so he told about the
 cat to the minister because
 he expected the minister to praise the cat.
 But the minister said, "Your Majesty, that is because the
 cat has not seen what he likes."
 So the king was displeased.
 But at night, I mean, that minister caught a mouse during
 the day and he took it to where
 the king was doing his service to the Buddha statue and
 then he released the mouse.
 So the mouse ran in front of the cat and as soon as they
 get saw the mouse, he throw away
 the candle and ran after the mouse.
 We Basna can also prevent the mental defilements from
 reaching the stage of arising.
 Now when you make notes of the objects or when you are
 mindful of the objects or when
 you practice mindfulness, mental defilements do not get
 chance to enter your mind.
 So when you practice Vibhasana meditation, you are able to
 keep the mental defilements
 away but just for a moment.
 The moment you stop practicing mindfulness, they can come
 in.
 So by the practice of Samatha meditation or Vibhasana
 meditation, people can keep these
 mental defilements away from them for some time only or for
 a moment only, not for good.
 Now the deepest level or the latent level of the mental def
ilements can be eradicated
 by enlightenment only or by the path consciousness only.
 So when a person becomes enlightened or when a person att
ains enlightenment, a type of
 consciousness arises in that person, a type of
 consciousness that he has never experienced
 before.
 So that consciousness is called path consciousness and that
 consciousness has the power to destroy
 the mental defilements altogether so that they do not arise
 again in his mind.
 So that is total abandonment or eradication of mental def
ilements.
 So the eradication of mental defilements or the removal of
 the deepest level of the mental
 defilement can be achieved only through the path
 consciousness.
 But in order to reach the path consciousness, what must one
 do?
 Practice Vibhasana.
 So Vibhasana, strictly speaking, Vibhasana does not
 eradicate mental defilements altogether,
 but the path consciousness arises as an outcome of Vibhas
ana practice.
 For Vibhasana, there can be no path consciousness and so
 there can be no attainment of enlightenment.
 So in order to deal with the deepest level of mental defile
ments, in order to get rid
 of the deepest level of mental defilements, we practice Vib
hasana meditation.
 So the mental defilements and the deepest level or the
 latent level are the most difficult
 to get rid of and they are said to be latent in our mental
 continuum.
 So they are not the ones that have already arisen, but they
 are something like a potential
 in our minds for them to arise.
 We can take an example of a box of matches.
 Now you keep the box of matches from young children and
 from small children.
 Why?
 Because there is a possibility that they may play with it
 and they may get burned.
 Or in other words, there is potential fire in the box of
 matches.
 In the same way, although mental defilements are not always
 present, not always arising
 in our minds, there is a potential in our minds for them to
 arise.
 And that potential, that liability is what we call the
 latent stage or latent states.
 So they have not reached the stage of arising and
 continuing, but there is a tendency or
 the potential for them to arise and that is what we call
 the latent states.
 Can we say that fire exists in the matchbox?
 No.
 If it does, it will be burning.
 So fire is not existent in the matchbox, but there is a
 liability that when someone strikes
 the match, the fire will be produced.
 So in the same way, mental defilements not always exist in
 our minds, but there is a
 liability that they will arise when the necessary
 conditions are met.
 Although they arise only when the conditions are met, we
 say that they are with us or we
 are with them.
 Because so long as we have not destroyed them altogether,
 so long as we have not eradicated
 them, we are said to have these mental defilements.
 Say a person who smokes.
 So although at the moment he is not smoking, if you ask him
, "Do you smoke?"
 then he will say, "I do."
 Because he has this habit of smoking and he has not given
 up this smoking.
 So he will say, "I smoke."
 So in the same way, if somebody asks us whether we have
 mental defilements, we will say yes.
 Although at this very moment, I hope we have no mental def
ilements in our minds.
 We are practicing Dharma.
 I am giving a Dharma talk and you are listening to the
 Dharma talk.
 And so there are no mental defilements in our minds.
 But if somebody asks us, "Are you free from mental defile
ments?" we will say no.
 Dying dormant in our minds is what we call latency in the
 mental continuum.
 So the mental defilements, although they do not reach the
 real stage of arising, they
 are said to be lying dormant in our minds.
 And that lying dormant is what we call the level of latency
 or latency in the mental
 continuum of beings.
 Another kind of latency and that is latency in the objects.
 Now we encounter many kinds of objects, material objects as
 well as mental objects.
 So whatever the object is, if we are not mindful, we, as it
 were, put the mental defilements
 into those objects.
 That means, suppose I see a beautiful object, so I like it
 and so I have attachment to that
 object.
 So when I have attachment to that object, it is actually I
 am injecting these mental
 defilements into that object.
 So that is what we call latency of mental defilements in
 the object.
 Actually mental defilements are in the mind and not in the
 object.
 But since I have taken this object as a desirable object
 and I have an attachment to this object,
 it is as though I have put or inject the mental defilements
 in this object.
 So once mental defilement is put in the object, it stays
 there always.
 That means every time you encounter this object, it comes
 along with that mental defilement
 you have put in.
 So that is called latency in the objects.
 And when we have injected mental defilements into the
 objects, since they are mental defilements,
 they lead us to action and mental defilements lead us to un
wholesome actions.
 Since I am attached to this object, I will protect it and
 even at the risk of my life,
 I will protect it, something like that.
 And if somebody comes and I will fight with him or
 something like that, so it leads to
 action, the mental defilement or the attachment to the
 object or aversion to the object.
 So these lead to the action and these actions are called k
ama.
 So when there is kama, there is the result of kama as the
 rebirth in, in this case, rebirth
 in woeful states.
 So every time we put the mental defilement in the object,
 we are prolonging our stay
 in this samsara.
 We are creating our own suffering because the result of ak
usala is always undesirable
 or unpleasant.
 So we must understand that every time we put a mental def
ilement in the object, we are
 prolonging our stay in this samsara.
 The practice of mindfulness or the practice of vipassana
 can help us to prevent injecting
 mental defilements in the objects.
 Because when we are mindful and we say seeing, seeing,
 seeing or hearing, hearing and so
 on, we do not take this object as desirable or undesirable.
 So when we do not take this as desirable or undesirable, we
 will not be attached to it
 or we will not have aversion towards it.
 So the practice of mindfulness can help us not to, not to
 put mental defilements into
 the objects.
 So when you practice vipassana meditation, you practice
 mindfulness.
 And in the beginning, you may not even see the object
 clearly, but as your practice strengthens,
 as your concentration becomes stronger, you will be able to
 see the objects clearly and
 you will be able to see the conditionality of the objects
 and you will be able to see
 that these objects come and go and so you see their imper
manence and so on.
 So when you see these characteristics of all condition
 phenomena, that is impermanence,
 suffering and non-soul nature of things, you will not be
 attached to that object or you
 will not hate that object and so on.
 And so you are able to avoid putting mental defilements
 into that object.
 So when you do not put in mental defilements in the object,
 there will be no acting depending
 on the mental defilements simply because there are no
 mental defilements.
 So when there is no acting, that means when there is no
 action, when there is no karma,
 there will be no result of karma.
 And so the future lives or future aggregates that would
 happen if you cannot prevent putting
 mental defilements in the object will not arise.
 So that is you are cutting the flow of rebirths, so you are
 cutting short the samsara, your
 samsara.
 So with the help of Vipassana, we can prevent putting
 mental defilements in the object or
 we can prevent the latency of mental defilements in the
 object.
 And as we go along, we will be able to prevent this more
 and more and when our practice of
 Vipassana matures, then as a result of the practice of Vip
assana or as the outcome of
 Vipassana, the path consciousness will arise.
 So when path consciousness arises, then it is able to do
 away with even the deepest level
 of mental defilements.
 So only when path consciousness arises can all mental def
ilements be eradicated so that
 they do not arise again in that person.
 And when we say the mental defilements are destroyed or
 removed or eradicated, we mean
 that they are rendered incapable of arising again.
 Not that they are there and then we destroy them.
 When mental defilements arise in our mind, there can be no
 Vipassana or there can be
 no path consciousness.
 So when there is no path consciousness, there can be no
 eradication of mental defilements.
 So the eradication of mental defilements really means
 rendering them incapable of arising
 again arising in the future.
 So that is what we should understand by the expression
 removal of mental defilements or
 abandonment of mental defilements or eradication of mental
 defilements.
 Now when you are able to avoid putting mental defilements
 in the objects, it is because
 you are able to see the object as it is without any
 additions of your own.
 So you see the object and you take the object as it is and
 not taking it as a disbeautiful
 object or it is an ugly object or this object I like or
 this object I don't like.
 So without putting anything onto the object, you just take
 the object as it is.
 That is why you are able to avoid injecting mental defile
ments into the object and that
 being able to take the object as it is is described as an
 achievement of the yogis.
 Now Buddha taught that things are impermanent.
 But if we think an object is permanent, that means we are
 adding something unreal to that
 object.
 Although the object is impermanent, we are putting the
 permanency onto the object.
 So in that case, we are taking the object not as it is but
 as we like it to be.
 So when we take something impermanent to be permanent, we
 are adding this permanency which
 is unreal to the object.
 But when you are able to apply mindfulness to the object,
 you are able to avoid that.
 And that ability to avoid adding anything unreal onto the
 object is described as a great
 achievement of a yogi.
 Now when we think an object to be permanent, which is
 really impermanent, that means we
 are not satisfied with this object.
 And so we are taking something that is real, taking away
 something real from the object.
 We want to take this impermanency out of the object and
 then in its place we put in the
 permanency.
 So when we take some object to be some impermanent object
 to be permanent, we are in one way
 taking away something that is real from the object.
 So there are two things, adding something unreal to the
 object and taking away something
 real from the object.
 And I think that is what we always do.
 We always put something unreal onto the object and we want
 to take away what is real from
 the object because we want things to be beautiful, things
 to be permanent and so on.
 And so to avoid adding something unreal onto the object and
 to avoid taking something real
 away from the object is a very difficult one.
 And it can be achieved only through mindfulness meditation,
 only through constant observation,
 only through vipassana.
 We are able to prevent adding something unreal and to take
 away something real from the object.
 We are following the advice given by the Buddha.
 The advice given by the Buddha is let there be seeing with
 regard to what is seen and
 so on.
 So in other words that means take the object as it is
 without any additions.
 Buddha gave this advice to two people, one on different
 locations, so one is an ascetic,
 came to the Buddha.
 He wanted to learn from the Buddha and so it is said that
 he walked the whole night.
 When he reached the monastery Buddha had already gone out
 to the village for alms.
 So he asked the monks where the Buddha was and when they
 told him that Buddha was on
 his alms round he went after the Buddha and sought him out
 and when he met the Buddha
 he just said, "Bhandi, please teach me something in brief
 so that I can practice."
 And Buddha said, "It's not the time to ask a question or to
 answer a question.
 I am on the alms round, so you just wait."
 But he was persistent.
 He insisted that Buddha gave him the answer for three times
.
 So after three times he said, "Bhandi, we do not know
 whether I will die next moment
 or not or you will die next moment or not, so please teach
 me now so that I can practice."
 So you know why when going on alms round Buddha just gave
 this advice, very short advice and
 let us become a very famous advice and every practitioner
 of Vibhavasana knows this advice
 given by the Buddha and that is, "Let there be seeing with
 regard to what is seen, let
 there be hearing with regard to what is heard, let there be
 sensing with regard to what is
 sensed, that means what is smelled, what is tasted, what is
 touched and let there be just
 thinking with regard to what is thought."
 So this is a very short advice given by the Buddha and it
 was enough for that ascetic because
 he was a gift, it wasn't actually, although he belonged to
 another teaching.
 So he practiced following this short advice of the Buddha
 and it is said that before Buddha
 reached back to the monastery he became an Arahant and then
 he came to the Buddha and asked to
 ordain him but Buddha knew that he could not become a monk
 in that life.
 So Buddha said, "We do not ordain those who have no robes
 and bowls and so you go and
 fetch the robes and bowls and so he went out to fetch robes
 and bowls and he was attacked
 by a cow and then he died."
 So when you see something and you stop at just seeing, you
 are putting an end to suffering
 regarding that object.
 So regarding that object, since you have no attachment or
 no aversion towards the object,
 you do not get any unwholesome state from that object and
 so you are able to avoid karma
 which will send you to the four waffle states and so the
 ability to take the object as it
 is is actually the ability to make an end of suffering.
 If we can take every object as it is then the putting an
 end of suffering will be complete
 but we are not able to do that now.
 So with regard to objects we may be able to stop at just
 seeing, hearing and so on.
 But even I think that that is desirable than not getting
 anything at all.
 So this is the immediate result that we get from the
 practice of vipassana.
 Now people think that they don't get immediate results.
 Nowadays people are very oriented towards getting results.
 But this is the immediate result you get from the practice
 of vipassana or the mindfulness
 meditation.
 So you don't get attached to the object or you don't have
 aversion or anger towards the
 object.
 And so with regard to that particular object there is no
 more suffering for you.
 So you are making an end of suffering regarding that object
.
 So if you can make the end of suffering regarding every
 object then your freedom from suffering
 will be complete.
 So please do not think that even if you do not get great
 results from the practice you
 are getting this immediate result every time, every moment
 you apply mindfulness to the
 object.
 And to achieve this you just need to pay attention to the
 object and take it as it is without
 any additions.
 So it sounds very simple but it needs a determination and
 strenuous effort to achieve this.
 So by the practice of mindfulness or by constant
 observation may we be able to take the objects
 as they are and avoid mental defilements regarding these
 objects so that ultimately say we are
 able to realize the total purity of mind.
 Thank you.
