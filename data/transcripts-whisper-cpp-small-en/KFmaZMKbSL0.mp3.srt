1
00:00:00,000 --> 00:00:02,000
 He was a virtually person.

2
00:00:02,000 --> 00:00:09,700
 And so he practiced torturing himself for six years on Kuk

3
00:00:09,700 --> 00:00:11,000
arakiriya.

4
00:00:11,000 --> 00:00:14,720
 If you go to India you can see the place where he practiced

5
00:00:14,720 --> 00:00:15,000
.

6
00:00:15,000 --> 00:00:18,000
 He practiced up in the hills, in the caves.

7
00:00:18,000 --> 00:00:21,000
 Now there's a Tibetan one to spend it on.

8
00:00:26,000 --> 00:00:29,770
 And then he realized that this was a no-use, so he gave it

9
00:00:29,770 --> 00:00:30,000
 up.

10
00:00:30,000 --> 00:00:34,000
 And he went down to the village below and went for food.

11
00:00:34,000 --> 00:00:50,640
 And the other five

12
00:00:50,640 --> 00:00:57,640
 burned up all the defilements inside because they believed

13
00:00:57,640 --> 00:00:58,470
 that when you torture yourself, when you hurt yourself, you

14
00:00:58,470 --> 00:01:00,640
 burn up the evil that's inside.

15
00:01:00,640 --> 00:01:04,640
 See, today in India they used to believe this.

16
00:01:04,640 --> 00:01:12,550
 And so they left and the Buddha decided that he had to find

17
00:01:12,550 --> 00:01:13,640
 the middle way.

18
00:01:13,640 --> 00:01:21,060
 And he went back to indulging in the central pleasures like

19
00:01:21,060 --> 00:01:21,640
 he had been to pass on with the positive one for years.

20
00:01:21,640 --> 00:01:31,090
 And when he went back to meet these five monks, they didn't

21
00:01:31,090 --> 00:01:31,640
 want to listen.

22
00:01:31,640 --> 00:01:34,480
 They didn't even want to greet him. He was their teacher

23
00:01:34,480 --> 00:01:35,640
 from the very beginning.

24
00:01:36,640 --> 00:01:45,430
 So normally they would have to receive his robe, they would

25
00:01:45,430 --> 00:01:46,640
 receive his bowl, and put out a seat for him.

26
00:01:46,640 --> 00:01:50,640
 And you stand up to greet him.

27
00:01:50,640 --> 00:01:54,640
 And they said, "We're not going to do any of those things.

28
00:01:54,640 --> 00:01:56,640
 We see him coming. As in all here he comes.

29
00:01:56,640 --> 00:01:59,640
 Let's not stand up. Let's not get anything in his bowl.

30
00:01:59,640 --> 00:02:02,640
 Let's not receive him like a teacher. We'll put out a seat

31
00:02:02,640 --> 00:02:04,640
 for him and he can sit on his floor."

32
00:02:04,640 --> 00:02:13,820
 And so the Buddha who had walked 120 miles or 120

33
00:02:13,820 --> 00:02:30,740
 kilometers in the beginning of the

34
00:02:30,740 --> 00:02:38,480
 day, he had to come and see the Buddha. He had to come and

35
00:02:38,480 --> 00:02:38,840
 see the Buddha.

36
00:02:38,840 --> 00:02:44,180
 And when he got there, this is how they were going to

37
00:02:44,180 --> 00:02:45,840
 receive him.

38
00:02:45,840 --> 00:02:53,410
 But as he got close, his wonderful presence and his

39
00:02:53,410 --> 00:02:56,840
 demeanor made them decide that they could

40
00:02:56,840 --> 00:03:00,980
 decide in their minds all of what the Buddha was doing. He

41
00:03:00,980 --> 00:03:02,840
 had to stand up.

42
00:03:02,840 --> 00:03:06,190
 One of them took his bowl, one of them took his robe, one

43
00:03:06,190 --> 00:03:07,840
 of them took a fan and started

44
00:03:07,840 --> 00:03:13,330
 standing. And then when he sat down on the seat that they

45
00:03:13,330 --> 00:03:22,840
 had prepared, they addressed him by his family.

46
00:03:22,840 --> 00:03:28,160
 They didn't address him like a teacher anymore. They were

47
00:03:28,160 --> 00:03:29,840
 going to address him as a teacher.

48
00:03:29,840 --> 00:03:36,840
 Now they can see that he had gone away from the time.

49
00:03:36,840 --> 00:03:41,840
 And it's amazing that even though they felt his presence,

50
00:03:41,840 --> 00:03:45,840
 even though they knew how special he had become,

51
00:03:45,840 --> 00:03:53,810
 he still didn't do so actively. And so he said to them, he

52
00:03:53,810 --> 00:03:58,260
 said, "It's not proper to address someone who has become

53
00:03:58,260 --> 00:03:58,840
 holy like."

54
00:03:58,840 --> 00:04:04,220
 He just went him up to him. The way he said it is, he's not

55
00:04:04,220 --> 00:04:08,840
 just going to use them but also to make it those things.

56
00:04:08,840 --> 00:04:12,950
 In a roundabout way that he had become enlightened. He

57
00:04:12,950 --> 00:04:14,840
 called me like, calling me Gautama.

58
00:04:14,840 --> 00:04:20,840
 It's not the way to impress the holy enlightened Buddha.

59
00:04:20,840 --> 00:04:25,810
 So he just cut right through his visit, claiming that he

60
00:04:25,810 --> 00:04:28,840
 was a holy enlightened Buddha.

61
00:04:28,840 --> 00:04:35,840
 And they replied to him, "Oh Gautama, Samana Gautama."

62
00:04:35,840 --> 00:04:38,400
 Even when you were torturing yourself, you could eat up in

63
00:04:38,400 --> 00:04:42,840
 life. Now that you've gone back to eating food,

64
00:04:42,840 --> 00:04:45,060
 because torturing them, when they hurt themselves, they

65
00:04:45,060 --> 00:04:47,840
 wouldn't eat or they'd eat once a week or they'd eat.

66
00:04:47,840 --> 00:04:53,270
 But not enough food to be healthy. Some people would starve

67
00:04:53,270 --> 00:04:54,840
 themselves until they die.

68
00:04:54,840 --> 00:04:57,960
 But normally they try to starve themselves just so much

69
00:04:57,960 --> 00:05:02,840
 that they could just stay alive and not be healthy.

70
00:05:02,840 --> 00:05:06,120
 So they could starve themselves in life. When they die,

71
00:05:06,120 --> 00:05:06,840
 they have to stop hurting themselves.

72
00:05:06,840 --> 00:05:10,460
 They want them to stay alive. Because they felt this really

73
00:05:10,460 --> 00:05:12,840
 hurt the whole influence of the food.

74
00:05:12,840 --> 00:05:15,460
 And it's really when you torture yourself, when you stop

75
00:05:15,460 --> 00:05:18,430
 eating or are paying for your history and taking the

76
00:05:18,430 --> 00:05:20,840
 violence to find yourself,

77
00:05:20,840 --> 00:05:26,180
 that's only difficult. And yet they still claim life is

78
00:05:26,180 --> 00:05:26,840
 good.

79
00:05:26,840 --> 00:05:32,140
 This was the only way to become enlightened. To see if you

80
00:05:32,140 --> 00:05:33,840
've given that up, you must now be indulging in attention.

81
00:05:33,840 --> 00:05:37,060
 And again the Buddha said, "Don't address me as a torte-mas

82
00:05:37,060 --> 00:05:38,840
que."

83
00:05:38,840 --> 00:05:42,710
 And I found the supreme enlightenment. If you listen to

84
00:05:42,710 --> 00:05:46,180
 what I have to say, I can teach you the way I look like

85
00:05:46,180 --> 00:05:46,840
 this.

