1
00:00:00,000 --> 00:00:06,040
 Giving up Noting In the tradition I have been practicing, B

2
00:00:06,040 --> 00:00:06,560
anteguna

3
00:00:06,560 --> 00:00:11,490
 Ratana has told me not to note mental phenomena such as

4
00:00:11,490 --> 00:00:14,440
 thinking, itching, etc. because it

5
00:00:14,440 --> 00:00:18,570
 is likely you will become fixated on the words and fixated

6
00:00:18,570 --> 00:00:20,960
 on the mental noting, but I have

7
00:00:20,960 --> 00:00:23,640
 noticed you teach this practice.

8
00:00:23,640 --> 00:00:27,590
 Would you eventually have people deviate from this practice

9
00:00:27,590 --> 00:00:29,680
 to examine the mental phenomena

10
00:00:29,680 --> 00:00:32,320
 and the sensation without notation?

11
00:00:32,320 --> 00:00:33,320
 No.

12
00:00:33,320 --> 00:00:36,870
 People often ask if there will be a point when I tell them

13
00:00:36,870 --> 00:00:38,120
 to stop noting.

14
00:00:38,120 --> 00:00:39,480
 And the answer is no.

15
00:00:39,480 --> 00:00:41,120
 There is no such point.

16
00:00:41,120 --> 00:00:44,100
 There are so many different opinions about noting

17
00:00:44,100 --> 00:00:45,000
 meditation.

18
00:00:45,000 --> 00:00:49,930
 Some people say noting is samatha meditation, not noting is

19
00:00:49,930 --> 00:00:51,160
 vipassana.

20
00:00:51,160 --> 00:00:55,880
 Others I have heard say not noting is samatha, noting is v

21
00:00:55,880 --> 00:00:56,800
ipassana.

22
00:00:56,800 --> 00:01:00,360
 I have heard people say noting inhibits or prevents you

23
00:01:00,360 --> 00:01:02,760
 from developing concentration.

24
00:01:02,760 --> 00:01:05,980
 I have heard people say noting gives you too much

25
00:01:05,980 --> 00:01:07,320
 concentration.

26
00:01:07,320 --> 00:01:10,450
 Everyone has an opinion about it and it is quite funny to

27
00:01:10,450 --> 00:01:12,240
 listen to all these opinions.

28
00:01:12,240 --> 00:01:16,130
 The Buddha's teaching is aepasical which means it lets you

29
00:01:16,130 --> 00:01:18,080
 come and see for yourself.

30
00:01:18,080 --> 00:01:21,350
 If it does cause you to fixate on the words then you have

31
00:01:21,350 --> 00:01:23,120
 come and seen and you should

32
00:01:23,120 --> 00:01:25,400
 go and see another teacher instead.

33
00:01:25,400 --> 00:01:28,940
 I have never seen that for myself or with my students.

34
00:01:28,940 --> 00:01:31,940
 If you are fixating on the words then you are not

35
00:01:31,940 --> 00:01:34,600
 practicing meditation and you should

36
00:01:34,600 --> 00:01:36,520
 acknowledge the fixating.

37
00:01:36,520 --> 00:01:39,840
 If you are investigating say investigating.

38
00:01:39,840 --> 00:01:43,400
 In situations where you think you are fixating or you feel

39
00:01:43,400 --> 00:01:45,840
 you are fixating it is more important

40
00:01:45,840 --> 00:01:49,200
 to look at the feeling and the judgment of the practice.

41
00:01:49,200 --> 00:01:51,720
 That is almost always the problem.

42
00:01:51,720 --> 00:01:55,260
 It may be that your practice is fine but then doubt arises

43
00:01:55,260 --> 00:01:57,240
 because we are generally prone

44
00:01:57,240 --> 00:01:58,540
 to doubt things.

45
00:01:58,540 --> 00:02:01,360
 When the doubt comes up acknowledge the doubting and you

46
00:02:01,360 --> 00:02:03,160
 will see that just like everything

47
00:02:03,160 --> 00:02:04,920
 else it disappears.

48
00:02:04,920 --> 00:02:07,740
 How can you fixate on the words if you are watching the

49
00:02:07,740 --> 00:02:09,400
 doubting and you are saying to

50
00:02:09,400 --> 00:02:11,880
 yourself "doubting, doubting".

51
00:02:11,880 --> 00:02:15,840
 After noting you will notice that the doubting has ceased.

52
00:02:15,840 --> 00:02:19,240
 Isn't that impermanence what we are trying to see?

53
00:02:19,240 --> 00:02:22,190
 If all you can see is words you have a problem with your

54
00:02:22,190 --> 00:02:24,280
 understanding of the practice.

55
00:02:24,280 --> 00:02:27,620
 I remember when I first started all I saw were words in my

56
00:02:27,620 --> 00:02:28,240
 head.

57
00:02:28,240 --> 00:02:32,260
 As a very visual person if I noted seeing, seeing there

58
00:02:32,260 --> 00:02:34,660
 would be those words in my head.

59
00:02:34,660 --> 00:02:37,260
 My initial practice was crazy practice.

60
00:02:37,260 --> 00:02:40,540
 I heard them talking about the middle way and so I tried to

61
00:02:40,540 --> 00:02:41,880
 envision this line down

62
00:02:41,880 --> 00:02:45,040
 the middle of my body and I was trying to open this up.

63
00:02:45,040 --> 00:02:48,480
 I saw this white line and I was like trying to get into the

64
00:02:48,480 --> 00:02:50,000
 middle way somehow.

65
00:02:50,000 --> 00:02:53,920
 The teacher at the time said "practice is like a hammer"

66
00:02:53,920 --> 00:02:55,720
 and then I got this big headache

67
00:02:55,720 --> 00:02:59,340
 and so I started to visualize my brain and I was imagining

68
00:02:59,340 --> 00:03:01,480
 hitting it with a hammer trying

69
00:03:01,480 --> 00:03:03,120
 to break it open.

70
00:03:03,120 --> 00:03:06,300
 Everything this teacher said to me I was just visualizing

71
00:03:06,300 --> 00:03:06,560
 it.

72
00:03:06,560 --> 00:03:09,030
 It was pretty terrible but it had nothing to do with the

73
00:03:09,030 --> 00:03:10,620
 noting practice and everything

74
00:03:10,620 --> 00:03:13,800
 to do with my understanding or lack thereof.

75
00:03:13,800 --> 00:03:17,610
 Once I started appreciating the noting as simply a reminder

76
00:03:17,610 --> 00:03:19,440
 meant to prevent reactions

77
00:03:19,440 --> 00:03:22,720
 I was able to separate the craziness of my head from the

78
00:03:22,720 --> 00:03:24,400
 practice that was leading me

79
00:03:24,400 --> 00:03:25,400
 to tame it.

80
00:03:25,400 --> 00:03:28,490
 If it were the case that noting made you fixated on the

81
00:03:28,490 --> 00:03:31,080
 words themselves then mantra meditation

82
00:03:31,080 --> 00:03:34,770
 in general would be useless and the word mantra would never

83
00:03:34,770 --> 00:03:36,880
 have come into popular usage.

84
00:03:36,880 --> 00:03:41,340
 In reality however a mantra does have power and does focus

85
00:03:41,340 --> 00:03:43,640
 your mind on an object whatever

86
00:03:43,640 --> 00:03:45,240
 the object might be.

87
00:03:45,240 --> 00:03:48,630
 If noting caused you to fixate on the words then the visud

88
00:03:48,630 --> 00:03:50,320
imaga would be wrong when it

89
00:03:50,320 --> 00:03:53,880
 describes again and again the use of these words.

90
00:03:53,880 --> 00:03:57,690
 According to the visudimaga when you practice the earth kas

91
00:03:57,690 --> 00:03:59,640
ina you focus on a disc of earth

92
00:03:59,640 --> 00:04:03,610
 and repeat one of the various names of earth like vatavi or

93
00:04:03,610 --> 00:04:05,600
 wumi repeating it a hundred

94
00:04:05,600 --> 00:04:08,120
 times or a thousand times.

95
00:04:08,120 --> 00:04:11,980
 The visudimaga is absolutely clear about the use of a

96
00:04:11,980 --> 00:04:12,880
 mantra.

97
00:04:12,880 --> 00:04:17,350
 When you practice the white kasina you repeat to yourself

98
00:04:17,350 --> 00:04:19,120
 white white white.

99
00:04:19,120 --> 00:04:22,780
 When you practice metta meditation you repeat a mantra of

100
00:04:22,780 --> 00:04:23,680
 kindness.

101
00:04:23,680 --> 00:04:28,200
 When you practice the 32 constituent parts you say the name

102
00:04:28,200 --> 00:04:29,760
 of each part of the body

103
00:04:29,760 --> 00:04:33,830
 in order or you pick one like focusing on the hair and

104
00:04:33,830 --> 00:04:35,200
 saying hair hair.

105
00:04:35,200 --> 00:04:38,490
 This is accepted meditation practice since ancient times

106
00:04:38,490 --> 00:04:40,180
 and with good reason because

107
00:04:40,180 --> 00:04:43,400
 mantras have great power and benefit to the mind.

108
00:04:43,400 --> 00:04:48,100
 When the Buddha said "Gecento bhag getchami di bhag chanati

109
00:04:48,100 --> 00:04:49,920
" which means "one going

110
00:04:49,920 --> 00:04:54,620
 one knows I am going" it is not "one knows the going" one

111
00:04:54,620 --> 00:04:56,320
 knows fully and completely

112
00:04:56,320 --> 00:04:57,720
 I am going.

113
00:04:57,720 --> 00:05:01,220
 So there arises a clear thought about the experience.

114
00:05:01,220 --> 00:05:02,960
 This is not something new.

115
00:05:02,960 --> 00:05:06,450
 The Buddha taught such practice and the visudimaga

116
00:05:06,450 --> 00:05:09,440
 describes explicitly that this is what is

117
00:05:09,440 --> 00:05:11,400
 meant by meditating.

118
00:05:11,400 --> 00:05:14,750
 Meditation means to focus one's attention on the object

119
00:05:14,750 --> 00:05:16,600
 just as it is without diversity

120
00:05:16,600 --> 00:05:17,600
 of thought.

121
00:05:17,600 --> 00:05:21,120
 To see white as white, to see blue as blue, to see the

122
00:05:21,120 --> 00:05:22,920
 earth as earth and so on.

123
00:05:22,920 --> 00:05:25,760
 The mantra is the tool used to see things this way.

124
00:05:25,760 --> 00:05:26,760
 This is the tool used to see the world.

125
00:05:26,760 --> 00:05:27,760
 This is the tool used to see the world.

