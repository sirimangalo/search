1
00:00:00,000 --> 00:00:02,000
 Noting Thinking

2
00:00:02,000 --> 00:00:07,040
 Some teachers say to note thinking twice and then go back

3
00:00:07,040 --> 00:00:10,220
 to the rising and falling when our minds wander in

4
00:00:10,220 --> 00:00:11,000
 meditation.

5
00:00:11,000 --> 00:00:15,300
 Others say to note thinking until the thought or wandering

6
00:00:15,300 --> 00:00:19,130
 falls away and then go back to the abdomen, which is more

7
00:00:19,130 --> 00:00:20,000
 precise.

8
00:00:20,000 --> 00:00:23,910
 Also, some teachers teach to only note thoughts that take

9
00:00:23,910 --> 00:00:27,510
 you completely away from the rising and falling and not

10
00:00:27,510 --> 00:00:29,000
 note the background thoughts.

11
00:00:29,000 --> 00:00:33,710
 First of all, thinking is not something that generally

12
00:00:33,710 --> 00:00:36,000
 lasts more than a couple of notings.

13
00:00:36,000 --> 00:00:39,700
 What lasts is the attachment to the thought, whether you

14
00:00:39,700 --> 00:00:41,000
 like it or dislike it.

15
00:00:41,000 --> 00:00:45,420
 Or if it is interesting for you, that is what is going to

16
00:00:45,420 --> 00:00:48,450
 keep it coming back and make it seem like the thought is

17
00:00:48,450 --> 00:00:49,000
 persisting.

18
00:00:49,000 --> 00:00:52,000
 Thoughts are actually quite fleeting.

19
00:00:52,000 --> 00:00:55,980
 So if you are just focusing on the thought, noting two or

20
00:00:55,980 --> 00:01:00,000
 three times is really enough or sometimes even once.

21
00:01:00,000 --> 00:01:02,000
 Then go back to the rising and falling.

22
00:01:02,000 --> 00:01:05,220
 If it does keep coming back, then you should try to note

23
00:01:05,220 --> 00:01:08,000
 the attachment that is causing it to return.

24
00:01:08,000 --> 00:01:13,340
 You can note liking, liking, disliking, disliking, or

25
00:01:13,340 --> 00:01:16,000
 whatever the attachment is.

26
00:01:16,000 --> 00:01:19,650
 As far as only noting thoughts that take you completely

27
00:01:19,650 --> 00:01:22,720
 away from the rising and falling, you could do it either

28
00:01:22,720 --> 00:01:23,000
 way.

29
00:01:23,000 --> 00:01:27,000
 When you are walking, we tend to give people some leeway.

30
00:01:27,000 --> 00:01:29,540
 My teacher explained two different approaches you could

31
00:01:29,540 --> 00:01:30,000
 adopt.

32
00:01:30,000 --> 00:01:34,550
 You could stop walking at every thought, or you could just

33
00:01:34,550 --> 00:01:39,000
 bring your mind back to the foot and continue going.

34
00:01:39,000 --> 00:01:42,170
 And when you stop at the end of the walking path, then you

35
00:01:42,170 --> 00:01:45,000
 could practice being mindful of the thoughts.

36
00:01:45,000 --> 00:01:48,390
 The point is to be present and mindful of our experiences

37
00:01:48,390 --> 00:01:50,000
 in the present moment.

38
00:01:50,000 --> 00:01:53,840
 When you are sitting, it is reasonable to suggest noting

39
00:01:53,840 --> 00:01:55,000
 all thoughts.

40
00:01:55,000 --> 00:01:58,510
 Thinking is a very important part of who we are, and an

41
00:01:58,510 --> 00:02:02,000
 understanding of the nature of thoughts is very beneficial.

42
00:02:02,000 --> 00:02:05,200
 Sometimes you catch your thoughts when they are already

43
00:02:05,200 --> 00:02:06,000
 finished.

44
00:02:06,000 --> 00:02:08,640
 You find you have been thinking for a while, and at the

45
00:02:08,640 --> 00:02:12,360
 very end you realize, "Oh, I was thinking for a long time

46
00:02:12,360 --> 00:02:13,000
 there."

47
00:02:13,000 --> 00:02:16,000
 And then you remind yourself, "Thinking, thinking."

48
00:02:16,000 --> 00:02:19,000
 Sometimes you catch yourself in the middle of a thought.

49
00:02:19,000 --> 00:02:22,230
 So in the middle of the thought, you say, "Thinking,

50
00:02:22,230 --> 00:02:23,000
 thinking."

51
00:02:23,000 --> 00:02:25,950
 With practice, you might catch a thought at the very

52
00:02:25,950 --> 00:02:28,000
 beginning when it first arises.

53
00:02:28,000 --> 00:02:31,690
 The practice of catching thoughts sooner and sooner is an

54
00:02:31,690 --> 00:02:34,000
 important part of the training.

55
00:02:34,000 --> 00:02:37,450
 The ability to catch a thought when it first arises shows a

56
00:02:37,450 --> 00:02:40,000
 clear and mindful state of awareness.

57
00:02:40,000 --> 00:02:44,720
 Of course, this is usually something you can only train in

58
00:02:44,720 --> 00:02:47,000
 intensive meditation courses.

59
00:02:47,000 --> 00:02:51,520
 Outside of intensive practice, you might think so much that

60
00:02:51,520 --> 00:02:55,000
 it is better to note distracted, distracted,

61
00:02:55,000 --> 00:02:57,450
 and then bring your attention back to the rising and

62
00:02:57,450 --> 00:03:00,380
 falling, not worrying too much about the individual

63
00:03:00,380 --> 00:03:01,000
 thoughts.

64
00:03:01,000 --> 00:03:04,750
 The important thing is to also note attachments to thoughts

65
00:03:04,750 --> 00:03:07,000
, just as with everything else.

66
00:03:07,000 --> 00:03:11,590
 Often, problems we have with noting are not so much caused

67
00:03:11,590 --> 00:03:15,000
 by the object itself, but with our reactions to it,

68
00:03:15,000 --> 00:03:17,000
 our likes and dislikes, etc.

69
00:03:17,000 --> 00:03:20,000
 Such reactions are called hindrances.

70
00:03:20,000 --> 00:03:23,800
 It is worth learning more about the five hindrances, liking

71
00:03:23,800 --> 00:03:28,000
, disliking, drowsiness, distraction, and doubt,

72
00:03:28,000 --> 00:03:31,000
 as they really do get in the way of our progress.

