1
00:00:00,000 --> 00:00:17,360
 Good evening everyone from Broadcasting Live, May 21st 2016

2
00:00:17,360 --> 00:00:17,360
.

3
00:00:17,360 --> 00:00:21,640
 So I think there was a bit of confusion or maybe even

4
00:00:21,640 --> 00:00:25,920
 disagreement about when is the

5
00:00:25,920 --> 00:00:33,920
 Wesaka-tulcha or Wesaka, the Buddha's birthday.

6
00:00:33,920 --> 00:00:39,810
 We counted according to the full moon so the celebration is

7
00:00:39,810 --> 00:00:42,920
 on the full moon in Wesaka.

8
00:00:42,920 --> 00:00:53,260
 Wesaka is the month or Wesaka I guess, Wesaka maybe, Wesaka

9
00:00:53,260 --> 00:00:53,480
 in Sanskrit.

10
00:00:53,480 --> 00:00:54,840
 That's the name of the month.

11
00:00:54,840 --> 00:01:07,880
 So on the full moon which is today was about three hours

12
00:01:07,880 --> 00:01:12,680
 ago, four hours ago.

13
00:01:12,680 --> 00:01:15,680
 So this is Wesaka-pulcha.

14
00:01:15,680 --> 00:01:21,560
 Pulcha means the celebration or the holiday.

15
00:01:21,560 --> 00:01:29,560
 This is the holiday or the holy day of Wesaka.

16
00:01:29,560 --> 00:01:33,920
 And it said that that was the day that the bodhisattva was

17
00:01:33,920 --> 00:01:34,640
 born.

18
00:01:34,640 --> 00:01:38,900
 It's the day the bodhisattva became a Buddha and it's the

19
00:01:38,900 --> 00:01:42,240
 day the Buddha passed into Parinibhana.

20
00:01:42,240 --> 00:01:48,640
 That's what they say.

21
00:01:48,640 --> 00:01:53,450
 So it's a good opportunity to talk today I think we'll

22
00:01:53,450 --> 00:01:55,880
 ignore the quote on our website

23
00:01:55,880 --> 00:01:57,880
 whatever that is.

24
00:01:57,880 --> 00:02:05,240
 Actually it's a really good quote but maybe we can delve

25
00:02:05,240 --> 00:02:07,520
 into it later.

26
00:02:07,520 --> 00:02:10,920
 We'll talk a little bit about the Buddha.

27
00:02:10,920 --> 00:02:12,640
 Why a Buddha?

28
00:02:12,640 --> 00:02:15,520
 What's important about a Buddha?

29
00:02:15,520 --> 00:02:19,040
 Why Buddhism?

30
00:02:19,040 --> 00:02:25,080
 Why do we have to be so concerned about our minds?

31
00:02:25,080 --> 00:02:30,120
 Why do we have to work so hard to train our minds?

32
00:02:30,120 --> 00:02:35,630
 There's so much good in the world, so much happiness in the

33
00:02:35,630 --> 00:02:37,360
 world, so much pleasure in

34
00:02:37,360 --> 00:02:39,720
 the world.

35
00:02:39,720 --> 00:02:43,880
 Why can't we just be content the way things are?

36
00:02:43,880 --> 00:02:45,880
 Isn't that enough?

37
00:02:45,880 --> 00:02:54,040
 Isn't that good enough?

38
00:02:54,040 --> 00:03:05,120
 The answer is it's not all happiness and pleasure in this

39
00:03:05,120 --> 00:03:07,120
 world.

40
00:03:07,120 --> 00:03:09,320
 We're not perfect.

41
00:03:09,320 --> 00:03:13,800
 We have problems.

42
00:03:13,800 --> 00:03:16,920
 We get angry.

43
00:03:16,920 --> 00:03:20,080
 We get obsessed.

44
00:03:20,080 --> 00:03:24,750
 We get anxious and worried and afraid and arrogant and

45
00:03:24,750 --> 00:03:27,640
 conceited and we make mistakes.

46
00:03:27,640 --> 00:03:28,760
 We hurt ourselves.

47
00:03:28,760 --> 00:03:31,760
 We hurt others.

48
00:03:31,760 --> 00:03:36,680
 We are confused and afraid and get lost.

49
00:03:36,680 --> 00:03:38,040
 We make mistakes.

50
00:03:38,040 --> 00:03:41,480
 We make bad decisions.

51
00:03:41,480 --> 00:03:42,480
 We suffer.

52
00:03:42,480 --> 00:03:43,480
 We feel pain.

53
00:03:43,480 --> 00:03:44,480
 We get sick.

54
00:03:44,480 --> 00:03:45,480
 We get old.

55
00:03:45,480 --> 00:03:46,480
 We die.

56
00:03:46,480 --> 00:03:51,080
 These are all parts of life, a part of life.

57
00:03:51,080 --> 00:03:53,440
 They're there.

58
00:03:53,440 --> 00:03:55,440
 They exist.

59
00:03:55,440 --> 00:04:00,760
 They are.

60
00:04:00,760 --> 00:04:03,060
 And anyone who thinks that these are just an inevitable

61
00:04:03,060 --> 00:04:04,200
 part of life that we should

62
00:04:04,200 --> 00:04:13,480
 just accept and take the good with the bad, as though there

63
00:04:13,480 --> 00:04:17,320
 were no need to delve deeper

64
00:04:17,320 --> 00:04:19,720
 or to respond.

65
00:04:19,720 --> 00:04:27,560
 There was no need to train, no need to better oneself.

66
00:04:27,560 --> 00:04:34,170
 There's a person who's misleading themselves because really

67
00:04:34,170 --> 00:04:37,760
 that's our whole life.

68
00:04:37,760 --> 00:04:43,460
 Our lives from the time that we're born are spent learning

69
00:04:43,460 --> 00:04:44,800
 just this.

70
00:04:44,800 --> 00:04:49,040
 How to better ourselves.

71
00:04:49,040 --> 00:04:52,550
 Not just means become better people, but to be happier

72
00:04:52,550 --> 00:04:53,320
 people.

73
00:04:53,320 --> 00:04:54,740
 How can we find happiness?

74
00:04:54,740 --> 00:04:57,000
 This is what our life is about.

75
00:04:57,000 --> 00:05:02,620
 It's about learning and learning how to cope and deal with

76
00:05:02,620 --> 00:05:04,080
 suffering.

77
00:05:04,080 --> 00:05:09,830
 We're bombarded by so many kinds of challenge and

78
00:05:09,830 --> 00:05:12,840
 difficulty and stress and suffering from

79
00:05:12,840 --> 00:05:14,960
 childhood.

80
00:05:14,960 --> 00:05:16,400
 Our whole lives have been this way.

81
00:05:16,400 --> 00:05:20,960
 This doesn't start when you come to practice meditation.

82
00:05:20,960 --> 00:05:23,680
 We've been asking these questions and finding answers to

83
00:05:23,680 --> 00:05:24,880
 them our whole lives.

84
00:05:24,880 --> 00:05:27,480
 That's what life is.

85
00:05:27,480 --> 00:05:32,040
 Life isn't ever just about a vacation or enjoyment.

86
00:05:32,040 --> 00:05:37,150
 If you want to enjoy, you have to figure out how to enjoy,

87
00:05:37,150 --> 00:05:39,480
 how to be happy, how to acquire

88
00:05:39,480 --> 00:05:46,180
 the things that you want, how to escape the things that you

89
00:05:46,180 --> 00:05:47,920
 don't want.

90
00:05:47,920 --> 00:05:54,000
 We spend our lives bettering ourselves.

91
00:05:54,000 --> 00:05:57,080
 One of them is mostly we don't find the answers.

92
00:05:57,080 --> 00:05:59,080
 Mostly we stumble.

93
00:05:59,080 --> 00:06:02,320
 We're blind.

94
00:06:02,320 --> 00:06:04,080
 We're not equipped.

95
00:06:04,080 --> 00:06:11,840
 We're not capable of finding the answers ourselves.

96
00:06:11,840 --> 00:06:17,360
 So every so often when being arises in the world, we are

97
00:06:17,360 --> 00:06:18,600
 capable.

98
00:06:18,600 --> 00:06:25,080
 We are all a lesser or greater faculties.

99
00:06:25,080 --> 00:06:29,720
 There's nothing to be ashamed of.

100
00:06:29,720 --> 00:06:34,560
 It's not to despair, but to be realistic.

101
00:06:34,560 --> 00:06:40,700
 We're none of us fully cognizant of our situation, fully

102
00:06:40,700 --> 00:06:44,560
 wise to the ways of our minds and the

103
00:06:44,560 --> 00:06:47,640
 ways of the world.

104
00:06:47,640 --> 00:06:48,640
 We stumble.

105
00:06:48,640 --> 00:06:49,640
 We make mistakes.

106
00:06:49,640 --> 00:06:50,640
 We're confused.

107
00:06:50,640 --> 00:06:55,840
 We're ignorant to so many things.

108
00:06:55,840 --> 00:06:59,160
 But a person arises who is not ignorant, who is able

109
00:06:59,160 --> 00:07:01,800
 through their own striving, through

110
00:07:01,800 --> 00:07:06,840
 their own effort to find the answers.

111
00:07:06,840 --> 00:07:11,320
 And then they share it.

112
00:07:11,320 --> 00:07:13,920
 They spread this teaching.

113
00:07:13,920 --> 00:07:19,080
 Buddhism is essential.

114
00:07:19,080 --> 00:07:21,930
 Buddhism by its very definition, and I think the word

115
00:07:21,930 --> 00:07:23,880
 Buddhism it sounds, but it's just

116
00:07:23,880 --> 00:07:25,400
 one of the many religions.

117
00:07:25,400 --> 00:07:29,800
 That's not Buddhism is not the word Buddha.

118
00:07:29,800 --> 00:07:34,200
 It isn't a name like Charlie or Frank.

119
00:07:34,200 --> 00:07:38,880
 Buddha means one who is awake or one who knows.

120
00:07:38,880 --> 00:07:40,640
 One who understands reality.

121
00:07:40,640 --> 00:07:43,720
 That's what Buddha means.

122
00:07:43,720 --> 00:07:50,190
 So it shouldn't be a specialized religion where some people

123
00:07:50,190 --> 00:07:52,920
 will become Buddhist and

124
00:07:52,920 --> 00:07:53,920
 some people won't.

125
00:07:53,920 --> 00:07:56,630
 Some people become Christians and some people become

126
00:07:56,630 --> 00:07:58,560
 Buddhist and it's up to you to choose.

127
00:07:58,560 --> 00:07:59,920
 It's not really like that.

128
00:07:59,920 --> 00:08:05,170
 Buddhism is for everyone of any religion, of any persuasion

129
00:08:05,170 --> 00:08:07,440
, anyone living any kind

130
00:08:07,440 --> 00:08:11,240
 of life.

131
00:08:11,240 --> 00:08:14,450
 Their success and their failure, their happiness and their

132
00:08:14,450 --> 00:08:16,560
 suffering, it's going to be directly

133
00:08:16,560 --> 00:08:21,460
 related to how close they are to Buddha, how close they are

134
00:08:21,460 --> 00:08:23,320
 to awakening, how close they

135
00:08:23,320 --> 00:08:38,600
 are to science, to knowledge, to understanding, to the

136
00:08:38,600 --> 00:08:41,160
 answers, to living their life in a

137
00:08:41,160 --> 00:08:48,680
 way that makes them happy.

138
00:08:48,680 --> 00:08:50,800
 We think we're happy.

139
00:08:50,800 --> 00:08:56,520
 We have happiness and we think so much about happiness.

140
00:08:56,520 --> 00:09:00,080
 You might say we're obsessed.

141
00:09:00,080 --> 00:09:02,000
 You don't see it until you come to meditate, but when you

142
00:09:02,000 --> 00:09:03,080
 come to meditate you see how

143
00:09:03,080 --> 00:09:07,960
 obsessed we are and it seems like well you can just get

144
00:09:07,960 --> 00:09:09,200
 what you want.

145
00:09:09,200 --> 00:09:11,120
 Why do I have to come to meditate?

146
00:09:11,120 --> 00:09:14,040
 Just get what I want.

147
00:09:14,040 --> 00:09:20,480
 But it's not that simple, you can't always get what you

148
00:09:20,480 --> 00:09:21,520
 want.

149
00:09:21,520 --> 00:09:25,960
 It's not just a trade saying.

150
00:09:25,960 --> 00:09:26,960
 It's beyond that.

151
00:09:26,960 --> 00:09:36,920
 You often get what you don't want to such a degree that you

152
00:09:36,920 --> 00:09:38,840
 moan and you wail and you

153
00:09:38,840 --> 00:09:43,970
 lament and you cry and you scream and you shake your fists

154
00:09:43,970 --> 00:09:46,320
 and you beat your breasts

155
00:09:46,320 --> 00:09:53,720
 and all these things.

156
00:09:53,720 --> 00:09:57,710
 People go crazy and kill themselves because they can't take

157
00:09:57,710 --> 00:09:58,960
 the suffering.

158
00:09:58,960 --> 00:09:59,960
 This is part of life.

159
00:09:59,960 --> 00:10:01,800
 It's part of our daily life.

160
00:10:01,800 --> 00:10:10,560
 We make mistakes because suffering for ourselves and others

161
00:10:10,560 --> 00:10:10,960
.

162
00:10:10,960 --> 00:10:13,400
 Buddhism is not optional.

163
00:10:13,400 --> 00:10:16,880
 Buddhism is the right way to live our lives.

164
00:10:16,880 --> 00:10:21,270
 We're trying, we're trying always to live our lives in a

165
00:10:21,270 --> 00:10:22,480
 better way.

166
00:10:22,480 --> 00:10:25,520
 Happiness for us, happiness for others.

167
00:10:25,520 --> 00:10:26,760
 We can't succeed.

168
00:10:26,760 --> 00:10:28,400
 We can't do it.

169
00:10:28,400 --> 00:10:32,400
 We fail mostly.

170
00:10:32,400 --> 00:10:35,560
 Maybe we get by so we can be happy.

171
00:10:35,560 --> 00:10:38,280
 But that happiness is qualified.

172
00:10:38,280 --> 00:10:46,670
 It's balanced with a lot of stress and suffering and angst

173
00:10:46,670 --> 00:10:50,800
 and anxiety and anguish.

174
00:10:50,800 --> 00:10:54,560
 And that's not something you should settle for.

175
00:10:54,560 --> 00:10:56,080
 It's not something we ever settle for.

176
00:10:56,080 --> 00:10:57,080
 We don't.

177
00:10:57,080 --> 00:11:01,640
 It's not how the world works.

178
00:11:01,640 --> 00:11:03,120
 We're always learning.

179
00:11:03,120 --> 00:11:04,280
 We're always striving.

180
00:11:04,280 --> 00:11:09,440
 We're always aiming for the top.

181
00:11:09,440 --> 00:11:11,080
 We all want to be happy all the time.

182
00:11:11,080 --> 00:11:12,080
 There's no question.

183
00:11:12,080 --> 00:11:15,340
 I think it's impossible, but that's how we live our lives.

184
00:11:15,340 --> 00:11:21,560
 We're always trying to be happy.

185
00:11:21,560 --> 00:11:26,460
 The claim we make is that it's not quite as easy as it

186
00:11:26,460 --> 00:11:29,040
 sounds or as it seems, but that

187
00:11:29,040 --> 00:11:30,920
 there is a way.

188
00:11:30,920 --> 00:11:35,880
 If you work, if you work to straighten your mind of all of

189
00:11:35,880 --> 00:11:38,280
 its crookedness and all of

190
00:11:38,320 --> 00:11:48,550
 its clinginess, to straighten and purify your mind, train

191
00:11:48,550 --> 00:11:53,680
 yourself to see things as they

192
00:11:53,680 --> 00:11:57,680
 are, not how you wish they were.

193
00:11:57,680 --> 00:12:05,320
 Train yourself to accept, not accept, but tolerate.

194
00:12:05,320 --> 00:12:11,370
 To stand strong in the face of anything, to be undisturbed

195
00:12:11,370 --> 00:12:14,720
 by the vicissitudes of life.

196
00:12:14,720 --> 00:12:16,880
 You can train yourself in that.

197
00:12:16,880 --> 00:12:17,880
 You're invincible.

198
00:12:17,880 --> 00:12:21,720
 You can be happy all the time.

199
00:12:21,720 --> 00:12:25,480
 You can be at peace no matter what comes.

200
00:12:25,480 --> 00:12:32,630
 You can conquer all of life's problems and challenges and

201
00:12:32,630 --> 00:12:34,920
 difficulties.

202
00:12:34,920 --> 00:12:37,240
 God was a very special person.

203
00:12:37,240 --> 00:12:44,470
 Today we honor his memory as someone who taught something

204
00:12:44,470 --> 00:12:48,920
 that for those who have practiced

205
00:12:48,920 --> 00:12:55,160
 it clearly does lead to peace, happiness, and freedom from

206
00:12:55,160 --> 00:12:56,720
 suffering.

207
00:12:56,720 --> 00:12:58,920
 It leads, it's an honest path.

208
00:12:58,920 --> 00:13:01,040
 It's a straight path.

209
00:13:01,040 --> 00:13:03,760
 When you're no longer crooked, you're no longer fooling

210
00:13:03,760 --> 00:13:04,440
 yourself.

211
00:13:04,440 --> 00:13:07,520
 You're no longer deluding yourself.

212
00:13:07,520 --> 00:13:10,480
 You're no longer making mistakes.

213
00:13:10,480 --> 00:13:17,710
 You're no longer choosing the wrong response to problems,

214
00:13:17,710 --> 00:13:21,560
 to challenges, or you respond

215
00:13:21,560 --> 00:13:31,760
 properly, mindfully, purely, peacefully.

216
00:13:31,760 --> 00:13:35,000
 So the three qualities of the Buddha that we remember on

217
00:13:35,000 --> 00:13:39,760
 this day are, first of all,

218
00:13:39,760 --> 00:13:40,760
 his wisdom.

219
00:13:40,760 --> 00:13:44,360
 This is what we're talking about.

220
00:13:44,360 --> 00:13:49,640
 The Buddha understood the truth and he understood the way

221
00:13:49,640 --> 00:13:51,560
 to find the truth.

222
00:13:51,560 --> 00:13:54,360
 He was able to teach the truth.

223
00:13:54,360 --> 00:13:59,440
 Great wisdom to understand and to see.

224
00:13:59,440 --> 00:14:01,520
 And it's very essence.

225
00:14:01,520 --> 00:14:03,120
 What does it mean to be wise?

226
00:14:03,120 --> 00:14:07,520
 It's a very specific thing in Buddhism.

227
00:14:07,520 --> 00:14:11,430
 It means to see everything that arises and ceases,

228
00:14:11,430 --> 00:14:14,600
 everything that exists in this world,

229
00:14:14,600 --> 00:14:18,560
 everything that arises is not permanent.

230
00:14:18,560 --> 00:14:21,640
 It's not stable.

231
00:14:21,640 --> 00:14:27,720
 It's not predictable.

232
00:14:27,720 --> 00:14:31,240
 That everything that arises is unsatisfying.

233
00:14:31,240 --> 00:14:34,480
 It's not a source of happiness.

234
00:14:34,480 --> 00:14:37,160
 Happiness can't come from a thing.

235
00:14:37,160 --> 00:14:40,760
 No thing can make you happy because it's impermanent.

236
00:14:40,760 --> 00:14:43,200
 It won't satisfy you.

237
00:14:43,200 --> 00:14:49,480
 If you try to find happiness in it, you will suffer

238
00:14:49,480 --> 00:14:53,960
 disappointment when it's gone.

239
00:14:53,960 --> 00:14:58,260
 And that everything in the world inside of ourselves and

240
00:14:58,260 --> 00:15:00,360
 the world around us, it doesn't

241
00:15:00,360 --> 00:15:09,640
 belong to us, doesn't have its own entity or existence.

242
00:15:09,640 --> 00:15:15,960
 It doesn't belong to a thing, doesn't belong to anyone,

243
00:15:15,960 --> 00:15:19,800
 which means you can't control.

244
00:15:19,800 --> 00:15:21,920
 There's no control over things.

245
00:15:21,920 --> 00:15:31,490
 Nothing you can control and change so that you keep it away

246
00:15:31,490 --> 00:15:31,760
.

247
00:15:31,760 --> 00:15:34,000
 Everything is in a constant flux.

248
00:15:34,000 --> 00:15:38,900
 Seeing, hearing, smelling, tasting, feeling, thinking, that

249
00:15:38,900 --> 00:15:40,760
's really all there is.

250
00:15:40,760 --> 00:15:44,040
 When you see a person, the person is real.

251
00:15:44,040 --> 00:15:45,440
 The seeing is real.

252
00:15:45,440 --> 00:15:48,080
 When you hear the person, the hearing is real.

253
00:15:48,080 --> 00:15:50,960
 When you taste food, the food isn't real.

254
00:15:50,960 --> 00:15:55,760
 It's the tasting and the feeling.

255
00:15:55,760 --> 00:15:56,760
 It's quite simple.

256
00:15:56,760 --> 00:15:59,680
 Reality is actually quite innocuous.

257
00:15:59,680 --> 00:16:04,760
 There's nothing dangerous or scary.

258
00:16:04,760 --> 00:16:10,700
 You're being confronted by a scary situation and in the end

259
00:16:10,700 --> 00:16:14,120
 it only comes down to the senses.

260
00:16:14,120 --> 00:16:17,850
 If you're in an argument with someone and you get

261
00:16:17,850 --> 00:16:19,760
 frightened because of how loud they

262
00:16:19,760 --> 00:16:24,920
 are and you get angry because of how vicious they are, you

263
00:16:24,920 --> 00:16:27,120
 remember it's only seeing, hearing,

264
00:16:27,120 --> 00:16:30,880
 smelling, tasting, feeling, thinking.

265
00:16:30,880 --> 00:16:33,200
 Nothing good comes from clinging to it.

266
00:16:33,200 --> 00:16:38,880
 It's impermanent, unsatisfying, uncontrollable.

267
00:16:38,880 --> 00:16:45,080
 You getting caught up in it doesn't do any good.

268
00:16:45,080 --> 00:16:48,760
 So the first is the wisdom that the Buddha taught.

269
00:16:48,760 --> 00:16:53,270
 All of the teachings on the Four Foundations of Mindfulness

270
00:16:53,270 --> 00:16:55,800
 and how to see things clearly.

271
00:16:55,800 --> 00:17:00,880
 The second is the purity of the Buddha.

272
00:17:00,880 --> 00:17:05,480
 So through his wisdom he found purity and that's what we

273
00:17:05,480 --> 00:17:07,040
 all strive for.

274
00:17:07,040 --> 00:17:11,270
 We strive through our wisdom and understanding to have the

275
00:17:11,270 --> 00:17:13,600
 same pure mind of the Buddha.

276
00:17:13,600 --> 00:17:17,520
 We don't get greedy or we don't get angry or we don't get

277
00:17:17,520 --> 00:17:19,640
 afraid or anxious or depressed

278
00:17:19,640 --> 00:17:30,910
 or worried or confused, arrogant, conceited, sad,

279
00:17:30,910 --> 00:17:32,720
 frustrated.

280
00:17:32,720 --> 00:17:35,800
 None of these things come to us.

281
00:17:35,800 --> 00:17:42,720
 We have a mind that is well trained, that is well tamed.

282
00:17:42,720 --> 00:17:46,480
 We have peace.

283
00:17:46,480 --> 00:17:50,660
 And the third quality of the Buddha that's not universal

284
00:17:50,660 --> 00:17:52,760
 and it's not something we all

285
00:17:52,760 --> 00:17:55,920
 strive for is his compassion.

286
00:17:55,920 --> 00:17:58,810
 We don't all strive for the same level of compassion of the

287
00:17:58,810 --> 00:17:59,440
 Buddha.

288
00:17:59,440 --> 00:18:02,240
 Some do.

289
00:18:02,240 --> 00:18:05,890
 But we praise the Buddha for it and we thank him for it and

290
00:18:05,890 --> 00:18:07,040
 we revere him.

291
00:18:07,040 --> 00:18:11,480
 We feel gratitude because he didn't have to teach.

292
00:18:11,480 --> 00:18:15,900
 He didn't even have to spend all the time learning,

293
00:18:15,900 --> 00:18:18,920
 becoming capable of teaching.

294
00:18:18,920 --> 00:18:23,370
 He had opportunities to follow the teachings of another

295
00:18:23,370 --> 00:18:25,320
 Buddha but he didn't.

296
00:18:25,320 --> 00:18:29,040
 Instead he went on to cultivate his own perfection so he

297
00:18:29,040 --> 00:18:31,560
 could find it by himself in a time when

298
00:18:31,560 --> 00:18:34,600
 there was no Buddha.

299
00:18:34,600 --> 00:18:38,850
 And so that's what we're left with. That's what allows us

300
00:18:38,850 --> 00:18:41,760
 now, 2,500 years later, to

301
00:18:41,760 --> 00:18:46,590
 still practice because he took the time to cultivate his

302
00:18:46,590 --> 00:18:49,360
 own perfection so that he could

303
00:18:49,360 --> 00:18:51,280
 teach on his own.

304
00:18:51,280 --> 00:18:56,250
 But many people who try to do that, who try to emulate his

305
00:18:56,250 --> 00:18:58,400
 perfection and many people

306
00:18:58,400 --> 00:19:03,260
 who don't, who are content with following his teachings,

307
00:19:03,260 --> 00:19:06,200
 there are those who are discontent

308
00:19:06,200 --> 00:19:10,820
 and who actually take the time to stay around in samsara

309
00:19:10,820 --> 00:19:14,680
 for lifetime after lifetime, cultivating

310
00:19:14,680 --> 00:19:18,170
 their own perfections and working to better themselves to

311
00:19:18,170 --> 00:19:19,600
 the point where they too can

312
00:19:19,600 --> 00:19:22,680
 be a Buddha.

313
00:19:22,680 --> 00:19:24,600
 That's what our Buddha did.

314
00:19:24,600 --> 00:19:31,080
 So we're respecting for that.

315
00:19:31,080 --> 00:19:33,890
 Today is a day to think about the Buddha and to practice

316
00:19:33,890 --> 00:19:35,320
 the Buddha's teaching.

317
00:19:35,320 --> 00:19:38,570
 The Buddha said if we care for him we will practice his

318
00:19:38,570 --> 00:19:39,440
 teachings.

319
00:19:39,440 --> 00:19:43,180
 So let's all take this as an opportunity to better

320
00:19:43,180 --> 00:19:46,080
 ourselves and the respect for the Buddha

321
00:19:46,080 --> 00:19:48,080
 to practice his teachings.

322
00:19:48,080 --> 00:20:00,760
 So that's the dhamma for tonight.

323
00:20:00,760 --> 00:20:04,400
 I wonder what we got here.

324
00:20:04,400 --> 00:20:05,760
 Some questions.

325
00:20:05,760 --> 00:20:12,560
 We're going to restructure this site.

326
00:20:12,560 --> 00:20:16,760
 Finally in the next week we're going to recreate this site

327
00:20:16,760 --> 00:20:18,160
 in a new format.

328
00:20:18,160 --> 00:20:24,870
 It'll look a little nicer and it'll be a little more

329
00:20:24,870 --> 00:20:28,640
 professional, I guess.

330
00:20:28,640 --> 00:20:31,930
 Is taking refuge in the triple gem required for stream

331
00:20:31,930 --> 00:20:32,600
 entry?

332
00:20:32,600 --> 00:20:37,000
 If not, then what is the minimum requirement for stream

333
00:20:37,000 --> 00:20:37,560
 entry?

334
00:20:37,560 --> 00:20:40,410
 Someone who is a Sottapana has perfect faith in the Buddha,

335
00:20:40,410 --> 00:20:41,920
 the dhamma and the Sangha.

336
00:20:41,920 --> 00:20:47,220
 So it's not about actually a ceremony like taking refuge,

337
00:20:47,220 --> 00:20:49,600
 but they do naturally.

338
00:20:49,600 --> 00:20:53,360
 But all that's required for stream entry is seeing nirvana.

339
00:20:53,360 --> 00:20:56,160
 That's what happens.

340
00:20:56,160 --> 00:21:00,290
 Seeing the three characteristics so clearly that the mind

341
00:21:00,290 --> 00:21:02,280
 lets go enters into nirvana

342
00:21:02,280 --> 00:21:03,600
 for the first time.

343
00:21:03,600 --> 00:21:24,480
 That's what makes someone a stream entry, a Sottapana.

344
00:21:24,480 --> 00:21:27,860
 How do you stay in a peaceful state without being

345
00:21:27,860 --> 00:21:30,840
 distracted, especially in society?

346
00:21:30,840 --> 00:21:34,200
 Well, come and do a meditation course.

347
00:21:34,200 --> 00:21:36,640
 We'll teach you how.

348
00:21:36,640 --> 00:21:38,280
 It's not something I can just say to you.

349
00:21:38,280 --> 00:21:40,280
 I should read my booklet if you haven't.

350
00:21:40,280 --> 00:21:43,410
 If you come and do a meditation course with us, stay with

351
00:21:43,410 --> 00:21:43,800
 us.

352
00:21:43,800 --> 00:21:44,800
 We'll feed you.

353
00:21:44,800 --> 00:21:48,440
 How does you teach you all for free?

354
00:21:48,440 --> 00:21:54,400
 And then you'll have your answer.

355
00:21:54,400 --> 00:21:56,920
 Does Bhanga mean vanishing without remainder?

356
00:21:56,920 --> 00:22:00,600
 If so, does it go against the view that energy can neither

357
00:22:00,600 --> 00:22:02,520
 be created nor destroyed?

358
00:22:02,520 --> 00:22:07,600
 Sangha, you and your questions.

359
00:22:07,600 --> 00:22:10,320
 Why do you torture me so?

360
00:22:10,320 --> 00:22:13,160
 Bhanga, yes.

361
00:22:13,160 --> 00:22:17,960
 Bhanga means vanishing without remainder.

362
00:22:17,960 --> 00:22:19,640
 And yes, I guess it goes against.

363
00:22:19,640 --> 00:22:25,960
 I mean, I think it does go against the view that energy can

364
00:22:25,960 --> 00:22:29,760
 neither be created nor destroyed.

365
00:22:29,760 --> 00:22:31,440
 But it's a whole, it's a different worldview.

366
00:22:31,440 --> 00:22:33,680
 We don't talk in terms of energy.

367
00:22:33,680 --> 00:22:35,440
 Energy isn't a thing according to Buddhism.

368
00:22:35,440 --> 00:22:37,600
 It doesn't exist.

369
00:22:37,600 --> 00:22:45,780
 All that exists are experiences, and experiences arise and

370
00:22:45,780 --> 00:22:47,120
 cease.

371
00:22:47,120 --> 00:22:49,590
 What's the official name of the tradition of Mahasya Sayad

372
00:22:49,590 --> 00:22:49,920
aw?

373
00:22:49,920 --> 00:22:55,160
 We call it the Mahasya Sayadaw tradition.

374
00:22:55,160 --> 00:22:59,480
 Some people call it Satipatthana Vipassana.

375
00:22:59,480 --> 00:23:01,480
 Some people call it Theravada.

376
00:23:01,480 --> 00:23:14,280
 Burmese Vipassana, Burmese Satipatthana.

377
00:23:14,280 --> 00:23:21,200
 I think Guadalupe has an autocorrect problem.

378
00:23:21,200 --> 00:23:27,160
 Do the bushes have all the 32 marks of heat, man?

379
00:23:27,160 --> 00:23:31,000
 You have to be careful before you submit.

380
00:23:31,000 --> 00:23:37,320
 I think you mean, do the Buddhas have all the 32 marks of

381
00:23:37,320 --> 00:23:39,160
 the great man?

382
00:23:39,160 --> 00:23:59,360
 Buddha apparently has 32 marks, 32 characteristics.

383
00:23:59,360 --> 00:24:00,960
 You guys don't have to stick around for this.

384
00:24:00,960 --> 00:24:04,040
 You can if you want, but I'm just answering questions.

385
00:24:04,040 --> 00:24:05,040
 Okay.

386
00:24:05,040 --> 00:24:10,640
 I've got two meditators here.

387
00:24:10,640 --> 00:24:27,760
 One Michael and one meditator.

388
00:24:27,760 --> 00:24:31,000
 This week we'll try to work on the website.

389
00:24:31,000 --> 00:24:34,960
 Saturday, next Saturday is the waysuck celebration.

390
00:24:34,960 --> 00:24:37,880
 This is Saga.

391
00:24:37,880 --> 00:24:46,520
 And then a few days after that I'm off to Thailand.

392
00:24:46,520 --> 00:24:50,890
 We've got someone coming to stay in the house while I'm

393
00:24:50,890 --> 00:24:51,680
 away.

394
00:24:51,680 --> 00:24:56,600
 Jason who's here in Hamilton, he's on our Buddhism

395
00:24:56,600 --> 00:24:58,280
 Association.

396
00:24:58,280 --> 00:25:00,280
 He said he'll come and stay in the house while I'm away.

397
00:25:00,280 --> 00:25:01,280
 Which is good.

398
00:25:01,280 --> 00:25:02,280
 Good to hear.

399
00:25:02,280 --> 00:25:06,440
 But we still need someone who can come long term to stay

400
00:25:06,440 --> 00:25:07,320
 with us.

401
00:25:07,320 --> 00:25:11,730
 Someone who can help look after the meditators, who wants

402
00:25:11,730 --> 00:25:14,160
 to spend some time, months in a

403
00:25:14,160 --> 00:25:15,160
 monastery.

404
00:25:15,160 --> 00:25:20,210
 Someone who doesn't have a dog that they have to go and

405
00:25:20,210 --> 00:25:21,680
 look after.

406
00:25:21,680 --> 00:25:25,160
 Does a point of view mean it can't be changed or discarded?

407
00:25:25,160 --> 00:25:33,200
 Or does there always have to be a point of view in all

408
00:25:33,200 --> 00:25:35,760
 situations?

409
00:25:35,760 --> 00:25:37,760
 You're making my brain hurt.

410
00:25:37,760 --> 00:25:42,480
 I didn't know about that question.

411
00:25:42,480 --> 00:25:45,560
 A view is a belief.

412
00:25:45,560 --> 00:25:48,520
 So you can have a view that is in line with reality.

413
00:25:48,520 --> 00:25:52,240
 You can have a view that is out of line with reality.

414
00:25:52,240 --> 00:25:55,040
 You don't always have a view.

415
00:25:55,040 --> 00:25:58,600
 Sometimes you do something out of habit, without a view.

416
00:25:58,600 --> 00:26:02,960
 That it's right or wrong or good or bad, etc.

417
00:26:02,960 --> 00:26:04,880
 But views do arise.

418
00:26:04,880 --> 00:26:05,880
 They're occasional.

419
00:26:05,880 --> 00:26:07,560
 Not every mind has a view in it.

420
00:26:07,560 --> 00:26:18,600
 So not every experience has a view.

421
00:26:18,600 --> 00:26:21,240
 We have an uptick today.

422
00:26:21,240 --> 00:26:23,240
 40 people watching today.

423
00:26:23,240 --> 00:26:25,880
 That's a high.

424
00:26:25,880 --> 00:26:31,400
 When I was in Sri Lanka I used to get 40, 50 I think.

425
00:26:31,400 --> 00:26:35,170
 But since I've been back in Canada, it's rare to get 40

426
00:26:35,170 --> 00:26:36,000
 people.

427
00:26:36,000 --> 00:26:39,000
 So hello everyone.

428
00:26:39,000 --> 00:26:42,760
 Welcome.

429
00:26:42,760 --> 00:26:44,480
 If you're wondering where the questions are, if you're

430
00:26:44,480 --> 00:26:46,080
 watching this on YouTube, maybe

431
00:26:46,080 --> 00:26:48,840
 watching it later even.

432
00:26:48,840 --> 00:26:50,300
 Also what we do is send it around our own website at

433
00:26:50,300 --> 00:26:54,880
 meditation.siri-mangalo.org.

434
00:26:54,880 --> 00:26:58,550
 And that's where people are chatting and asking questions

435
00:26:58,550 --> 00:27:00,440
 and meditating together.

436
00:27:00,440 --> 00:27:04,620
 We also have online meditation courses, so people sign up

437
00:27:04,620 --> 00:27:05,640
 for those.

438
00:27:05,640 --> 00:27:11,640
 And meditate together online as well.

439
00:27:11,640 --> 00:27:12,640
 Okay.

440
00:27:12,640 --> 00:27:23,640
 So I'm going to say goodnight to everyone.

441
00:27:23,640 --> 00:27:27,880
 I'm sure you're all happy with Sakapocha.

442
00:27:27,880 --> 00:27:31,240
 Happy full moon.

443
00:27:31,240 --> 00:27:31,840
 See you all later.

444
00:27:31,840 --> 00:27:39,760
 Bye.

445
00:27:39,760 --> 00:27:45,680
 Bye.

