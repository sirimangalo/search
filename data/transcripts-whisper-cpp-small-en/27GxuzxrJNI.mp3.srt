1
00:00:00,000 --> 00:00:04,170
 Hello and welcome back to Ask a Monk. Today I'll be

2
00:00:04,170 --> 00:00:05,400
 answering the question as

3
00:00:05,400 --> 00:00:11,280
 to what a Buddhist does when someone they know dies, which

4
00:00:11,280 --> 00:00:12,680
 is not

5
00:00:12,680 --> 00:00:16,770
 directly related to the meditation practice, but as Buddh

6
00:00:16,770 --> 00:00:17,320
ists and

7
00:00:17,320 --> 00:00:20,760
 meditators in the Buddhist tradition, this is something

8
00:00:20,760 --> 00:00:22,680
 that we, an experience

9
00:00:22,680 --> 00:00:28,830
 that we have to deal with, that for most of us will be on

10
00:00:28,830 --> 00:00:30,720
 the extreme end,

11
00:00:30,720 --> 00:00:34,210
 something that is quite difficult to deal with and to come

12
00:00:34,210 --> 00:00:35,800
 to terms with.

13
00:00:35,800 --> 00:00:40,330
 So simply meditating in a way is not really sufficient. So

14
00:00:40,330 --> 00:00:40,880
 there are things

15
00:00:40,880 --> 00:00:45,010
 that we should talk about in regards to this. From a

16
00:00:45,010 --> 00:00:46,560
 practical point of view, a

17
00:00:46,560 --> 00:00:51,270
 Buddhist point of view, I would say there are three things

18
00:00:51,270 --> 00:00:53,720
 that we should do, or

19
00:00:53,720 --> 00:00:58,260
 that we should keep in mind, three duties as Buddhists, or

20
00:00:58,260 --> 00:01:00,040
 three points to keep in

21
00:01:00,040 --> 00:01:03,070
 mind when a person we know dies. The first one is that we

22
00:01:03,070 --> 00:01:04,840
 should never ever

23
00:01:04,840 --> 00:01:10,650
 grieve or think that grieving is somehow appropriate,

24
00:01:10,650 --> 00:01:12,720
 somehow useful, somehow good.

25
00:01:12,720 --> 00:01:17,220
 The reason why we grieve is because it's great suffering

26
00:01:17,220 --> 00:01:18,640
 for us. You can think of

27
00:01:18,640 --> 00:01:22,320
 a small child who has to stay with a babysitter the first

28
00:01:22,320 --> 00:01:23,440
 time when their

29
00:01:23,440 --> 00:01:27,850
 parents go away, will often cry all night thinking that

30
00:01:27,850 --> 00:01:29,880
 they're not

31
00:01:29,880 --> 00:01:33,790
 even thinking that their parents have left, but simply

32
00:01:33,790 --> 00:01:35,400
 suffering terribly

33
00:01:35,400 --> 00:01:38,040
 because of the clinging that they have, that they've

34
00:01:38,040 --> 00:01:39,320
 developed as a small child

35
00:01:39,320 --> 00:01:42,730
 for their parents. So there's this terrible suffering that

36
00:01:42,730 --> 00:01:45,800
 goes on at loss,

37
00:01:45,800 --> 00:01:49,680
 which really carries over into everything in our lives.

38
00:01:49,680 --> 00:01:50,680
 People cry over many

39
00:01:50,680 --> 00:01:54,190
 different things. Now the point being that we tend to cling

40
00:01:54,190 --> 00:01:55,120
 to people a lot

41
00:01:55,120 --> 00:01:59,320
 more than we cling to any other object, really. It's there,

42
00:01:59,320 --> 00:01:59,520
 people are,

43
00:01:59,520 --> 00:02:03,800
 other people are things that we can interact with, that

44
00:02:03,800 --> 00:02:06,360
 have great deep

45
00:02:06,360 --> 00:02:08,910
 meaning for us in our lives, and so we cling to them

46
00:02:08,910 --> 00:02:11,080
 greatly. This is why we

47
00:02:11,080 --> 00:02:17,830
 feel suffering when a person passes away. It's not based on

48
00:02:17,830 --> 00:02:20,640
 any logic or reason

49
00:02:20,640 --> 00:02:26,550
 that a person should grieve, and we should really get this

50
00:02:26,550 --> 00:02:26,920
 point across

51
00:02:26,920 --> 00:02:33,560
 that there is no reason to mourn. It is something that is

52
00:02:33,560 --> 00:02:34,160
 based on our

53
00:02:34,160 --> 00:02:39,700
 addiction. The reason why we cry is because of the

54
00:02:39,700 --> 00:02:41,640
 chemicals involved with

55
00:02:41,640 --> 00:02:47,620
 crying. Crying is a reaction that we have developed as a

56
00:02:47,620 --> 00:02:49,960
 species to deal with

57
00:02:49,960 --> 00:02:54,190
 difficult situations. It's something that releases pleasure

58
00:02:54,190 --> 00:02:55,480
 chemicals and

59
00:02:55,480 --> 00:02:59,430
 endorphins, I think it makes you feel calm and happy and

60
00:02:59,430 --> 00:03:00,820
 peaceful, so it

61
00:03:00,820 --> 00:03:06,120
 becomes an addiction actually. This is how grieving works.

62
00:03:06,120 --> 00:03:06,960
 It can be

63
00:03:06,960 --> 00:03:10,710
 crying, some people take to alcohol or any sort of

64
00:03:10,710 --> 00:03:11,560
 addiction, but

65
00:03:11,560 --> 00:03:14,830
 crying is again another addiction. We should never think

66
00:03:14,830 --> 00:03:15,600
 that this is

67
00:03:15,600 --> 00:03:18,520
 somehow important or a part of the process, a part of the

68
00:03:18,520 --> 00:03:19,560
 grieving process.

69
00:03:19,560 --> 00:03:24,040
 Many people will go on for years suffering because a person

70
00:03:24,040 --> 00:03:24,560
 has passed

71
00:03:24,560 --> 00:03:30,140
 away. The point is that there's no benefit to it. You don't

72
00:03:30,140 --> 00:03:30,780
 help the

73
00:03:30,780 --> 00:03:34,080
 person who passed away, you don't help yourself, you don't

74
00:03:34,080 --> 00:03:34,840
 do something that

75
00:03:34,840 --> 00:03:38,130
 they would want for you, and there's no one who wants their

76
00:03:38,130 --> 00:03:39,440
 loved ones to

77
00:03:39,440 --> 00:03:47,290
 suffer, to mourn, to have to be without. In the end it's

78
00:03:47,290 --> 00:03:47,720
 really just

79
00:03:47,720 --> 00:03:51,020
 useless, and in fact worse than useless, it drags you down

80
00:03:51,020 --> 00:03:52,280
 in many ways. It drags

81
00:03:52,280 --> 00:04:01,720
 you down into a cycle of suffering and depression and also

82
00:04:01,720 --> 00:04:02,240
 addiction.

83
00:04:02,240 --> 00:04:05,840
 It makes you become, because you start to find a way out,

84
00:04:05,840 --> 00:04:06,680
 you try to find a

85
00:04:06,680 --> 00:04:11,250
 way to block it out and not have to think about it, which

86
00:04:11,250 --> 00:04:11,920
 gives rise to

87
00:04:11,920 --> 00:04:16,700
 many different types of addiction. Now this brings up a

88
00:04:16,700 --> 00:04:19,800
 good point that often

89
00:04:19,800 --> 00:04:23,920
 people are unable to follow this advice. This first point,

90
00:04:23,920 --> 00:04:26,480
 important as it is, it's

91
00:04:26,480 --> 00:04:30,760
 best phrased as don't see it as a good thing, because if

92
00:04:30,760 --> 00:04:31,200
 you say to people

93
00:04:31,200 --> 00:04:35,620
 don't mourn, well unless they had no attachment to the

94
00:04:35,620 --> 00:04:36,240
 person they're going

95
00:04:36,240 --> 00:04:40,440
 to feel sad. I've given this talk before at funerals and

96
00:04:40,440 --> 00:04:41,320
 then after you

97
00:04:41,320 --> 00:04:44,760
 give a talk the people come up and cry. So you think well

98
00:04:44,760 --> 00:04:45,840
 why did I explain to

99
00:04:45,840 --> 00:04:49,460
 them about not to cry and how it's not really useful or any

100
00:04:49,460 --> 00:04:50,920
 benefit, but they

101
00:04:50,920 --> 00:04:55,000
 didn't intend to. You can't blame people for crying when a

102
00:04:55,000 --> 00:05:00,480
 loved one dies. So it's

103
00:05:00,480 --> 00:05:03,380
 but some people go to the next level and think yes, yes

104
00:05:03,380 --> 00:05:04,520
 this is good and this is

105
00:05:04,520 --> 00:05:07,320
 important and so they encourage it in themselves and they

106
00:05:07,320 --> 00:05:08,600
 encourage the

107
00:05:08,600 --> 00:05:13,140
 phenomenon and so they end up crying more and they think of

108
00:05:13,140 --> 00:05:13,920
 it as somehow a

109
00:05:13,920 --> 00:05:16,730
 good thing and so they can get caught up and actually waste

110
00:05:16,730 --> 00:05:17,520
 a lot of time and

111
00:05:17,520 --> 00:05:25,320
 energy doing so. But this also raises another point that we

112
00:05:25,320 --> 00:05:28,080
 most often get

113
00:05:28,080 --> 00:05:31,160
 ourselves into this position. Why when a person dies we can

114
00:05:31,160 --> 00:05:32,080
 go on for days and

115
00:05:32,080 --> 00:05:37,250
 weeks and months and even years mourning is because we've

116
00:05:37,250 --> 00:05:37,480
 been

117
00:05:37,480 --> 00:05:42,360
 negligent, we've been heedless in regards to our mind. We

118
00:05:42,360 --> 00:05:42,760
 haven't been

119
00:05:42,760 --> 00:05:45,480
 paying attention to our attachments and you know we live

120
00:05:45,480 --> 00:05:46,600
 our lives thinking that

121
00:05:46,600 --> 00:05:49,740
 attaching to other people is good. It's intimacy and

122
00:05:49,740 --> 00:05:51,760
 relationships and love

123
00:05:51,760 --> 00:05:55,990
 and so on. But this really has nothing to do with love, it

124
00:05:55,990 --> 00:05:56,440
 has to do

125
00:05:56,440 --> 00:05:59,830
 with our own clinging. When you love someone it's not what

126
00:05:59,830 --> 00:06:00,720
 they can do for

127
00:06:00,720 --> 00:06:03,870
 you and what they bring to you and the great things that

128
00:06:03,870 --> 00:06:04,560
 they give to

129
00:06:04,560 --> 00:06:08,850
 you. That's a kind of, we use the word love, but it really

130
00:06:08,850 --> 00:06:09,920
 is an attachment.

131
00:06:09,920 --> 00:06:13,890
 Love is when you wish for good things for other people and

132
00:06:13,890 --> 00:06:15,080
 there's nothing good

133
00:06:15,080 --> 00:06:18,510
 that comes from crying. You don't somehow respect the

134
00:06:18,510 --> 00:06:20,960
 person's memory by crying by

135
00:06:20,960 --> 00:06:23,560
 bringing yourself suffering. You don't do something that

136
00:06:23,560 --> 00:06:24,800
 they would want you to do

137
00:06:24,800 --> 00:06:28,370
 or that they would want for you. So this is the first point

138
00:06:28,370 --> 00:06:29,800
. But

139
00:06:29,800 --> 00:06:32,430
 no, what I was going to say is that for this reason because

140
00:06:32,430 --> 00:06:33,280
 we get ourselves

141
00:06:33,280 --> 00:06:38,210
 into this mess, part of this is even before a person dies

142
00:06:38,210 --> 00:06:39,880
 we should be quite

143
00:06:39,880 --> 00:06:43,450
 careful with our relationships. Our relationships should be

144
00:06:43,450 --> 00:06:44,440
 meaningful but

145
00:06:44,440 --> 00:06:47,650
 the meaning should come from our love for each other, our

146
00:06:47,650 --> 00:06:48,280
 wish for each

147
00:06:48,280 --> 00:06:53,800
 other to be happy. And in fact our wish for all beings to

148
00:06:53,800 --> 00:06:55,040
 be happy. The only way

149
00:06:55,040 --> 00:06:59,400
 we can really be purely have pure love for other beings is

150
00:06:59,400 --> 00:07:00,360
 to have it be

151
00:07:00,360 --> 00:07:04,230
 impartial. Where it's not that we don't love anyone, we end

152
00:07:04,230 --> 00:07:05,200
 up loving everyone.

153
00:07:05,200 --> 00:07:08,990
 This is the Buddha said that we should strive to love all

154
00:07:08,990 --> 00:07:10,120
 beings just as a

155
00:07:10,120 --> 00:07:15,080
 mother might love her only child. And if we can get to this

156
00:07:15,080 --> 00:07:16,400
 point, if we really

157
00:07:16,400 --> 00:07:19,140
 have love for people it won't matter who comes and who goes

158
00:07:19,140 --> 00:07:20,840
. Once it's true love

159
00:07:20,840 --> 00:07:26,900
 then the next person you see is a loved one for you, is the

160
00:07:26,900 --> 00:07:27,360
 one you love.

161
00:07:27,360 --> 00:07:33,720
 And it continues on when they go, there's nothing in love

162
00:07:33,720 --> 00:07:34,000
 that

163
00:07:34,000 --> 00:07:37,260
 says they need to do anything for you or be anything for

164
00:07:37,260 --> 00:07:38,200
 you. So when

165
00:07:38,200 --> 00:07:41,750
 they're gone there's no suffering, there's no stress, there

166
00:07:41,750 --> 00:07:42,480
's no

167
00:07:42,480 --> 00:07:46,960
 sadness. So this is an important reason why we should be

168
00:07:46,960 --> 00:07:47,520
 practicing

169
00:07:47,520 --> 00:07:51,690
 meditation because when we don't practice meditation we

170
00:07:51,690 --> 00:07:52,680
 cling and cling

171
00:07:52,680 --> 00:07:56,750
 and cling and we set ourselves up for so much stress and

172
00:07:56,750 --> 00:07:58,160
 suffering that we see

173
00:07:58,160 --> 00:08:01,220
 all around us. Many people are living their lives in great

174
00:08:01,220 --> 00:08:02,680
 peace and happiness

175
00:08:02,680 --> 00:08:07,020
 simply because they haven't met with the inevitable crash,

176
00:08:07,020 --> 00:08:08,040
 the inevitable loss,

177
00:08:08,040 --> 00:08:12,490
 loss of youth when they get old, so many things that they

178
00:08:12,490 --> 00:08:13,600
 can't enjoy and

179
00:08:13,600 --> 00:08:18,360
 suddenly they're confronted with old age or sickness when

180
00:08:18,360 --> 00:08:18,840
 people get

181
00:08:18,840 --> 00:08:23,920
 cancer and eventually death. The death of loved ones or the

182
00:08:23,920 --> 00:08:24,120
 death of one's

183
00:08:24,120 --> 00:08:28,420
 own death was totally unprepared for. Meditation practice

184
00:08:28,420 --> 00:08:29,320
 is to allow us to

185
00:08:29,320 --> 00:08:36,880
 have meaningful but non-attached relationships with other

186
00:08:36,880 --> 00:08:37,760
 people where we

187
00:08:37,760 --> 00:08:43,590
 give and we spread the goodness all around us and try our

188
00:08:43,590 --> 00:08:46,720
 best to

189
00:08:46,720 --> 00:08:51,530
 make other people happy and to bring peace to them instead

190
00:08:51,530 --> 00:08:52,200
 of wishing for

191
00:08:52,200 --> 00:08:56,460
 them to bring good things to us, which is of course a

192
00:08:56,460 --> 00:08:58,400
 totally uncertain something

193
00:08:58,400 --> 00:09:02,740
 we can't depend on. So this is the first point, is that it

194
00:09:02,740 --> 00:09:03,440
's really

195
00:09:03,440 --> 00:09:06,440
 useless to grieve and it's something that as Buddhists we

196
00:09:06,440 --> 00:09:07,360
 should not do, we

197
00:09:07,360 --> 00:09:12,040
 should on the other hand spend our lives thinking about the

198
00:09:12,040 --> 00:09:12,760
 inevitability of

199
00:09:12,760 --> 00:09:16,660
 death and this brings me to the second point that when a

200
00:09:16,660 --> 00:09:17,960
 person dies it should

201
00:09:17,960 --> 00:09:21,640
 be a reminder to us and the Buddha was very clear about

202
00:09:21,640 --> 00:09:23,560
 this for monks when a

203
00:09:23,560 --> 00:09:28,520
 person dies and even for lay people when a person dies that

204
00:09:28,520 --> 00:09:28,760
 we

205
00:09:28,760 --> 00:09:34,040
 should consider this to be a sign, a sign that we too will

206
00:09:34,040 --> 00:09:34,800
 have to go, we

207
00:09:34,800 --> 00:09:37,800
 should be clear that this is something that we can't avoid,

208
00:09:37,800 --> 00:09:38,800
 it's a fact of life.

209
00:09:38,800 --> 00:09:42,880
 So the contemplation on the reality of death is a very

210
00:09:42,880 --> 00:09:45,720
 important part of our

211
00:09:45,720 --> 00:09:55,000
 acceptance when a person dies. So first of all the

212
00:09:55,000 --> 00:09:56,120
 realization that

213
00:09:56,120 --> 00:09:58,760
 this was a part of that person's life and that's a part of

214
00:09:58,760 --> 00:10:00,880
 nature and not

215
00:10:00,880 --> 00:10:04,650
 getting caught up in this idea of a person or this one life

216
00:10:04,650 --> 00:10:05,640
 or this

217
00:10:05,640 --> 00:10:08,820
 being existed and now they're gone or so on. But thinking

218
00:10:08,820 --> 00:10:10,040
 in terms of reality, in

219
00:10:10,040 --> 00:10:15,320
 terms of our experience and that this is the nature of

220
00:10:15,320 --> 00:10:16,160
 reality that all

221
00:10:16,160 --> 00:10:19,660
 things that arise in our mind, the idea of a person will

222
00:10:19,660 --> 00:10:20,440
 eventually cease.

223
00:10:20,440 --> 00:10:26,130
 Everything that we cling to, a person, a place, a thing,

224
00:10:26,130 --> 00:10:28,520
 even an ideology,

225
00:10:28,520 --> 00:10:34,880
 eventually we will have to give it up, nothing will last

226
00:10:34,880 --> 00:10:36,080
 forever. And so we use

227
00:10:36,080 --> 00:10:44,800
 death as a really good wake-up call for us to realize the

228
00:10:44,800 --> 00:10:45,200
 truth of

229
00:10:45,200 --> 00:10:48,710
 reality and also to reflect on our own mortality. When a

230
00:10:48,710 --> 00:10:50,400
 person dies we should

231
00:10:50,400 --> 00:10:54,470
 take that as a warning to ourselves, we should watch and

232
00:10:54,470 --> 00:10:56,080
 see and look and

233
00:10:56,080 --> 00:10:59,360
 think of the person who is dead and think this will also

234
00:10:59,360 --> 00:11:00,960
 come to me one day,

235
00:11:00,960 --> 00:11:04,940
 will I be ready for it? As would as we, this is an

236
00:11:04,940 --> 00:11:07,160
 important, not only is it a

237
00:11:07,160 --> 00:11:09,750
 good meditation and useful for ourselves but it's a really

238
00:11:09,750 --> 00:11:10,680
 good way to come to

239
00:11:10,680 --> 00:11:13,520
 terms with the person's death because instead of grieving

240
00:11:13,520 --> 00:11:14,600
 or thinking oh now

241
00:11:14,600 --> 00:11:20,920
 that person's gone, you realize that you remind yourself

242
00:11:20,920 --> 00:11:21,360
 that this is

243
00:11:21,360 --> 00:11:25,500
 reality and the real problem is our attachments to a

244
00:11:25,500 --> 00:11:26,880
 specific state of being,

245
00:11:26,880 --> 00:11:30,140
 of a person, a place, a thing being like this, not being

246
00:11:30,140 --> 00:11:31,240
 like that or being with

247
00:11:31,240 --> 00:11:36,070
 us and not being away from us. Once we start to look at

248
00:11:36,070 --> 00:11:37,040
 reality, you know, and

249
00:11:37,040 --> 00:11:41,800
 look at the experience of reality from moment to moment now

250
00:11:41,800 --> 00:11:43,200
, what's happening

251
00:11:43,200 --> 00:11:48,160
 and so on, then the idea of how things should be or how

252
00:11:48,160 --> 00:11:49,880
 things are, could be or

253
00:11:49,880 --> 00:11:53,400
 how things used to be or how things will be or so on is

254
00:11:53,400 --> 00:11:55,280
 really irrelevant and it

255
00:11:55,280 --> 00:11:58,160
 doesn't have the same hold on us. We're able to live here

256
00:11:58,160 --> 00:11:59,320
 and now in peace and

257
00:11:59,320 --> 00:12:02,710
 happiness and that here and now extends forever of course.

258
00:12:02,710 --> 00:12:03,880
 When you're able to be

259
00:12:03,880 --> 00:12:06,790
 happy here and now then you're able to be happy for

260
00:12:06,790 --> 00:12:08,600
 eternity and that's the

261
00:12:08,600 --> 00:12:14,000
 reality of life. So that's number two, is that we should

262
00:12:14,000 --> 00:12:15,200
 use it as an opportunity

263
00:12:15,200 --> 00:12:18,640
 to reflect on our own mortality and reflect on the inev

264
00:12:18,640 --> 00:12:19,920
itability of death,

265
00:12:19,920 --> 00:12:22,610
 that it comes to all beings and we should remind ourselves

266
00:12:22,610 --> 00:12:23,240
 that this is

267
00:12:23,240 --> 00:12:28,480
 really the reality of that person and it shouldn't actually

268
00:12:28,480 --> 00:12:32,080
 be of any

269
00:12:32,080 --> 00:12:36,360
 positive or negative significance, it should be seen as the

270
00:12:36,360 --> 00:12:37,040
 way things are.

271
00:12:37,040 --> 00:12:41,560
 We should not be happy or unhappy about it, should not be

272
00:12:41,560 --> 00:12:43,440
 regretful or mourning or

273
00:12:43,440 --> 00:12:46,770
 so on. This is what we should do, is we should take it as

274
00:12:46,770 --> 00:12:48,480
 an opportunity to

275
00:12:48,480 --> 00:12:53,440
 reflect on the way things go in life. Now this is more for

276
00:12:53,440 --> 00:12:54,600
 ourselves, for the

277
00:12:54,600 --> 00:12:57,690
 person who passed away, most people want to know what we

278
00:12:57,690 --> 00:12:59,120
 should do for the person.

279
00:12:59,120 --> 00:13:02,840
 This is the third point, that as Buddhists we should do

280
00:13:02,840 --> 00:13:04,080
 things on behalf of

281
00:13:04,080 --> 00:13:07,730
 the person who has passed away. We should, whatever good

282
00:13:07,730 --> 00:13:09,080
 things that person

283
00:13:09,080 --> 00:13:13,380
 undertook or even if they didn't do any good things, we

284
00:13:13,380 --> 00:13:15,040
 should carry on their

285
00:13:15,040 --> 00:13:24,640
 name by or celebrate their life by giving, by building

286
00:13:24,640 --> 00:13:26,320
 things, by

287
00:13:26,320 --> 00:13:30,170
 building things that will be of use to people, dedicating

288
00:13:30,170 --> 00:13:31,800
 our works to them and

289
00:13:31,800 --> 00:13:35,590
 so on, dedicating books or whatever we do, dedicating our

290
00:13:35,590 --> 00:13:37,720
 work, our good deeds to

291
00:13:37,720 --> 00:13:40,760
 that person, be it things that they did in their life and

292
00:13:40,760 --> 00:13:42,080
 we can continue on or

293
00:13:42,080 --> 00:13:47,120
 just things done in their name with some mention of them.

294
00:13:47,120 --> 00:13:50,280
 Because this, this is

295
00:13:50,280 --> 00:13:52,850
 really a way of celebrating the person's life and it's a

296
00:13:52,850 --> 00:13:54,200
 way of bringing meaning

297
00:13:54,200 --> 00:13:56,680
 to that person's life, it's a way of saying that this

298
00:13:56,680 --> 00:13:57,800
 person's life had real

299
00:13:57,800 --> 00:14:02,560
 meaning. If this person hadn't lived then these good deeds

300
00:14:02,560 --> 00:14:03,120
 wouldn't come.

301
00:14:03,120 --> 00:14:06,380
 We do these deeds especially for that person and therefore

302
00:14:06,380 --> 00:14:07,240
 they are those

303
00:14:07,240 --> 00:14:11,240
 person's good deeds that have been done. It's a way of

304
00:14:11,240 --> 00:14:14,000
 respecting and celebrating

305
00:14:14,000 --> 00:14:17,880
 and honoring the person who passed away. It's also a way of

306
00:14:17,880 --> 00:14:18,960
 coming to terms with

307
00:14:18,960 --> 00:14:22,460
 the death in the best way possible, celebrating the person

308
00:14:22,460 --> 00:14:23,120
's life, saying,

309
00:14:23,120 --> 00:14:27,290
 "This person passed away." Most people, the best, the best

310
00:14:27,290 --> 00:14:27,960
 they can do is to

311
00:14:27,960 --> 00:14:30,290
 remember the good things the person did and say, "Oh yeah,

312
00:14:30,290 --> 00:14:30,960
 I was such a good

313
00:14:30,960 --> 00:14:33,170
 person." So they celebrate and they have a party and they

314
00:14:33,170 --> 00:14:34,160
 get drunk and so on and

315
00:14:34,160 --> 00:14:37,110
 that's it. But that's not really celebrating a person's

316
00:14:37,110 --> 00:14:38,320
 life. When you

317
00:14:38,320 --> 00:14:40,600
 celebrate a person's life it should be all the good things

318
00:14:40,600 --> 00:14:41,480
 that they did should

319
00:14:41,480 --> 00:14:45,160
 be continued on or goodness should be done in their name.

320
00:14:45,160 --> 00:14:46,320
 So if it's a person

321
00:14:46,320 --> 00:14:49,310
 who means something to you and you think their life had

322
00:14:49,310 --> 00:14:50,560
 meaning then you should

323
00:14:50,560 --> 00:14:53,900
 do something. It may not be a lot but you should make

324
00:14:53,900 --> 00:14:55,520
 effort to do something in

325
00:14:55,520 --> 00:15:01,520
 their name and that will be your way of finding closure and

326
00:15:01,520 --> 00:15:02,680
 of ending that

327
00:15:02,680 --> 00:15:06,670
 person's life on a meaningful note. Saying, "This person

328
00:15:06,670 --> 00:15:07,640
 died and the result

329
00:15:07,640 --> 00:15:12,520
 was this. We have done this on behalf of that person."

330
00:15:12,520 --> 00:15:14,120
 Sometimes what that means is

331
00:15:14,120 --> 00:15:18,420
 an inheritance that we get. We use part of the inheritance

332
00:15:18,420 --> 00:15:19,560
 to honor the person,

333
00:15:19,560 --> 00:15:24,240
 to do something, not because out of duty or something but

334
00:15:24,240 --> 00:15:25,680
 as a good deed

335
00:15:25,680 --> 00:15:28,350
 for us and a good deed for them and something bringing

336
00:15:28,350 --> 00:15:29,480
 goodness to the world

337
00:15:29,480 --> 00:15:33,140
 on behalf of that person. What you find more often, which

338
00:15:33,140 --> 00:15:34,240
 is really the worst

339
00:15:34,240 --> 00:15:37,010
 thing a person could possibly do when another person dies,

340
00:15:37,010 --> 00:15:38,800
 is people fight over

341
00:15:38,800 --> 00:15:43,270
 the inheritance of their parents or people who passed away,

342
00:15:43,270 --> 00:15:44,240
 which is really

343
00:15:44,240 --> 00:15:48,440
 a horrible thing but apparently goes on quite often. And

344
00:15:48,440 --> 00:15:49,520
 this is something as

345
00:15:49,520 --> 00:15:52,930
 Buddhists we should guard against totally. We should never

346
00:15:52,930 --> 00:15:53,600
 think of a

347
00:15:53,600 --> 00:15:59,240
 person's, another person's possessions as somehow belonging

348
00:15:59,240 --> 00:16:00,160
 to us or that somehow

349
00:16:00,160 --> 00:16:03,680
 we deserve them. Never. We should never look at a person

350
00:16:03,680 --> 00:16:04,480
 and think, "This person

351
00:16:04,480 --> 00:16:07,120
 owes me something." As Buddhists we should never think like

352
00:16:07,120 --> 00:16:09,440
 that. So the idea, even

353
00:16:09,440 --> 00:16:12,670
 if your parents, you help them or whatever, you should

354
00:16:12,670 --> 00:16:13,360
 never think that

355
00:16:13,360 --> 00:16:15,650
 somehow that's going to give you some inheritance and you

356
00:16:15,650 --> 00:16:16,340
're going to get

357
00:16:16,340 --> 00:16:19,180
 something. We should be very careful to guard against this

358
00:16:19,180 --> 00:16:19,880
 because it will crop

359
00:16:19,880 --> 00:16:24,200
 up and when they die you'll find yourself fighting and

360
00:16:24,200 --> 00:16:26,080
 breaking apart your family

361
00:16:26,080 --> 00:16:28,750
 really. Could you imagine what your parents would, what the

362
00:16:28,750 --> 00:16:29,200
 parents would

363
00:16:29,200 --> 00:16:32,080
 think when they see their children you know at each other's

364
00:16:32,080 --> 00:16:33,560
 throats over things

365
00:16:33,560 --> 00:16:36,580
 that they actually left behind, that their parents had to

366
00:16:36,580 --> 00:16:37,360
 throw away that's

367
00:16:37,360 --> 00:16:40,520
 really garbage. It could be money, it could be a hole in

368
00:16:40,520 --> 00:16:42,040
 possession, it's all

369
00:16:42,040 --> 00:16:44,620
 garbage because in the end they had to leave it behind and

370
00:16:44,620 --> 00:16:45,600
 you have to leave it

371
00:16:45,600 --> 00:16:50,450
 behind. Much better is whatever good things that they have

372
00:16:50,450 --> 00:16:51,360
 you can take some

373
00:16:51,360 --> 00:16:54,350
 of it if it's useful to support you but making sure that

374
00:16:54,350 --> 00:16:55,680
 everyone else is taken

375
00:16:55,680 --> 00:16:59,550
 care of and giving things in their name. You know if the

376
00:16:59,550 --> 00:17:01,960
 least you do is giving

377
00:17:01,960 --> 00:17:07,970
 up some some hold that you have on what you know this this

378
00:17:07,970 --> 00:17:09,080
 fights that people

379
00:17:09,080 --> 00:17:16,000
 have over inheritance in order that harmony might be

380
00:17:16,000 --> 00:17:17,080
 bestowed upon your

381
00:17:17,080 --> 00:17:21,220
 family. For the sake of harmony and out of respect for the

382
00:17:21,220 --> 00:17:21,560
 person who

383
00:17:21,560 --> 00:17:24,680
 passed away I'm not going to fight over this. If that's the

384
00:17:24,680 --> 00:17:24,960
 least you

385
00:17:24,960 --> 00:17:27,860
 then you've done a great thing in their memory and you've

386
00:17:27,860 --> 00:17:28,760
 honored their memory.

387
00:17:28,760 --> 00:17:33,540
 So you know there are many ways that you can do this but

388
00:17:33,540 --> 00:17:34,440
 this could be the

389
00:17:34,440 --> 00:17:39,960
 least is to not for goodness sake don't fight over people

390
00:17:39,960 --> 00:17:41,160
 who passed away fight

391
00:17:41,160 --> 00:17:46,000
 over their belongings. It's a terrible terrible thing and

392
00:17:46,000 --> 00:17:46,960
 be very careful not

393
00:17:46,960 --> 00:17:49,660
 to let that happen to you because it might come into the

394
00:17:49,660 --> 00:17:50,560
 mind we can never be

395
00:17:50,560 --> 00:17:54,000
 sure we might find ourselves doing that. So these are three

396
00:17:54,000 --> 00:17:54,960
 points that I think

397
00:17:54,960 --> 00:17:57,660
 you should keep in mind when a person you know passes away

398
00:17:57,660 --> 00:17:59,160
 as a Buddhist or as a

399
00:17:59,160 --> 00:18:03,130
 person who follows these sorts of teachings and really any

400
00:18:03,130 --> 00:18:03,960
 good teachings

401
00:18:03,960 --> 00:18:07,720
 it's nothing specifically to do with with Buddhism. So hope

402
00:18:07,720 --> 00:18:08,920
 this helps. Thanks

403
00:18:08,920 --> 00:18:12,080
 for tuning in and all the best.

