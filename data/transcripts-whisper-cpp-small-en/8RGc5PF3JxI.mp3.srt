1
00:00:00,000 --> 00:00:24,880
 Good evening everyone.

2
00:00:24,880 --> 00:00:33,440
 Welcome to evening dhamma.

3
00:00:33,440 --> 00:00:36,240
 We finished talking about the purifications.

4
00:00:36,240 --> 00:00:46,610
 Today I thought we'd go back to go over the satipatthana s

5
00:00:46,610 --> 00:00:51,360
utta, a discourse on the foundations

6
00:00:51,360 --> 00:01:07,400
 of mindfulness.

7
00:01:07,400 --> 00:01:11,590
 So what we're practicing is insight meditation, which is

8
00:01:11,590 --> 00:01:13,920
 the insight part is a lot of what

9
00:01:13,920 --> 00:01:16,920
 we've gone over in the past days.

10
00:01:16,920 --> 00:01:20,920
 But it's based on the four foundations of mindfulness.

11
00:01:20,920 --> 00:01:27,200
 So the actual practice we do is mindfulness.

12
00:01:27,200 --> 00:01:34,400
 And when presented, it's quite a simple practice.

13
00:01:34,400 --> 00:01:40,380
 Remind yourself when you see something, you remind yourself

14
00:01:40,380 --> 00:01:42,100
 seeing, seeing.

15
00:01:42,100 --> 00:01:51,120
 When you feel pain, you remind yourself pain, pain.

16
00:01:51,120 --> 00:01:54,630
 When you have thoughts, you remind yourself thinking,

17
00:01:54,630 --> 00:01:55,480
 thinking.

18
00:01:55,480 --> 00:01:59,950
 Emotions, you remind yourself liking, liking or disliking,

19
00:01:59,950 --> 00:02:01,960
 disliking, disliking.

20
00:02:01,960 --> 00:02:03,240
 And that's your mantra.

21
00:02:03,240 --> 00:02:06,760
 You repeat it to yourself.

22
00:02:06,760 --> 00:02:10,920
 That's the practice.

23
00:02:10,920 --> 00:02:12,400
 There's not a lot more to it.

24
00:02:12,400 --> 00:02:18,320
 I mean, practically speaking, that's really all you need.

25
00:02:18,320 --> 00:02:21,450
 But intellectually to help us understand why are we doing

26
00:02:21,450 --> 00:02:23,400
 this, to help give us confidence

27
00:02:23,400 --> 00:02:28,680
 and to remind us to be mindful.

28
00:02:28,680 --> 00:02:33,120
 There's always a lot of explanation we can do.

29
00:02:33,120 --> 00:02:43,120
 We can give and reassurance through explanation.

30
00:02:43,120 --> 00:02:47,180
 Giving talks to meditators is often just about reminding

31
00:02:47,180 --> 00:02:49,840
 them and giving them encouragement

32
00:02:49,840 --> 00:02:52,680
 rather than actually imparting any kind of information.

33
00:02:52,680 --> 00:02:55,680
 It's not a lot of information you need.

34
00:02:55,680 --> 00:03:02,280
 But sometimes there are reminders and they need things to

35
00:03:02,280 --> 00:03:05,080
 be pointed out to you and certainly

36
00:03:05,080 --> 00:03:13,760
 need to be reassured.

37
00:03:13,760 --> 00:03:18,740
 So the satipatthana-sutta gives us the, it lays out the

38
00:03:18,740 --> 00:03:21,600
 practice in some detail about

39
00:03:21,600 --> 00:03:27,400
 how to practice the four satipatthana.

40
00:03:27,400 --> 00:03:32,210
 It starts by telling where the sutta was taught in the kuru

41
00:03:32,210 --> 00:03:35,040
, the land of the kurus, which is

42
00:03:35,040 --> 00:03:42,090
 interesting but only from an intellectual academic point of

43
00:03:42,090 --> 00:03:43,120
 view.

44
00:03:43,120 --> 00:03:46,450
 What's useful is the next section where the Buddha says, e

45
00:03:46,450 --> 00:03:51,320
ka yanoa yang bhikkhu e maggo.

46
00:03:51,320 --> 00:04:01,400
 This is the eka yanoa magga.

47
00:04:01,400 --> 00:04:07,600
 A lot has been said and argued about this phrase.

48
00:04:07,600 --> 00:04:11,120
 It's quite often translated as the only way.

49
00:04:11,120 --> 00:04:15,720
 This is the only way.

50
00:04:15,720 --> 00:04:19,700
 I suppose literally it doesn't actually mean that but it

51
00:04:19,700 --> 00:04:21,720
 does appear to be in some way

52
00:04:21,720 --> 00:04:26,000
 what the Buddha is saying.

53
00:04:26,000 --> 00:04:31,170
 The language might not be so strong as saying, there's no

54
00:04:31,170 --> 00:04:33,360
 other way than this.

55
00:04:33,360 --> 00:04:36,040
 In another place the Buddha did use such language to talk

56
00:04:36,040 --> 00:04:37,480
 about the eightfold path.

57
00:04:37,480 --> 00:04:41,240
 He said, ese wamago nattanyo.

58
00:04:41,240 --> 00:04:44,200
 Nattanyo means there is no other.

59
00:04:44,200 --> 00:04:49,720
 This is the path there is no other.

60
00:04:49,720 --> 00:04:54,360
 Even that could be interpreted and is interpreted

61
00:04:54,360 --> 00:04:55,960
 differently.

62
00:04:55,960 --> 00:04:58,390
 It's hard to imagine though that anyone could become

63
00:04:58,390 --> 00:05:02,440
 enlightened without practicing mindfulness.

64
00:05:02,440 --> 00:05:04,560
 If you want to see you have to look.

65
00:05:04,560 --> 00:05:06,880
 Without looking you can't see.

66
00:05:06,880 --> 00:05:11,560
 Without seeing you can't know.

67
00:05:11,560 --> 00:05:15,490
 This is looking in some form or another if you're not

68
00:05:15,490 --> 00:05:17,240
 paying attention.

69
00:05:17,240 --> 00:05:20,630
 If you're not present it's hard to imagine how you could

70
00:05:20,630 --> 00:05:22,560
 possibly become enlightened

71
00:05:22,560 --> 00:05:30,720
 to cultivate understanding about reality.

72
00:05:30,720 --> 00:05:37,240
 The commentary offers several definitions of eka yanoa, eka

73
00:05:37,240 --> 00:05:38,920
 yanoa magga.

74
00:05:38,920 --> 00:05:45,800
 The first one just means that it's one way.

75
00:05:45,800 --> 00:05:48,800
 Not quite clear what that means.

76
00:05:48,800 --> 00:05:53,160
 A single way.

77
00:05:53,160 --> 00:05:59,280
 It's not a double way.

78
00:05:59,280 --> 00:06:01,960
 The Buddha didn't teach many ways.

79
00:06:01,960 --> 00:06:04,920
 He only taught one way.

80
00:06:04,920 --> 00:06:08,940
 So it's not a question of whether there might be other ways

81
00:06:08,940 --> 00:06:11,720
 but there is only this one way.

82
00:06:11,720 --> 00:06:13,200
 This way is only one way.

83
00:06:13,200 --> 00:06:17,360
 It's not either do this or do that.

84
00:06:17,360 --> 00:06:19,120
 It really is just be mindful.

85
00:06:19,120 --> 00:06:22,370
 The way of the Buddha is the way of being mindful or else

86
00:06:22,370 --> 00:06:23,960
 the way of mindfulness is

87
00:06:23,960 --> 00:06:32,000
 just one way.

88
00:06:32,000 --> 00:06:35,240
 But then it gives some alternative definitions.

89
00:06:35,240 --> 00:06:37,800
 That one actually doesn't mean a lot.

90
00:06:37,800 --> 00:06:43,050
 So it's eka yanoa magga because one has to practice for

91
00:06:43,050 --> 00:06:44,280
 oneself.

92
00:06:44,280 --> 00:06:49,930
 It says it could mean that it's the way that has to be

93
00:06:49,930 --> 00:06:52,280
 practiced by one.

94
00:06:52,280 --> 00:06:55,680
 No one can practice for you.

95
00:06:55,680 --> 00:07:03,380
 Unlike other spiritual traditions where you can be saved by

96
00:07:03,380 --> 00:07:04,800
 others.

97
00:07:04,800 --> 00:07:20,960
 Buddhist soteriology is you must save yourself.

98
00:07:20,960 --> 00:07:22,880
 You must find freedom for yourself.

99
00:07:22,880 --> 00:07:30,080
 No one can change your mind and medicines and drugs.

100
00:07:30,080 --> 00:07:36,070
 A lot of people argue that psychedelic drugs are going to

101
00:07:36,070 --> 00:07:39,280
 be a support for the practice.

102
00:07:39,280 --> 00:07:41,880
 Not really.

103
00:07:41,880 --> 00:07:43,960
 There are things that can support.

104
00:07:43,960 --> 00:07:48,280
 Other people can support you on your path.

105
00:07:48,280 --> 00:07:53,360
 But in the end it's got to be.

106
00:07:53,360 --> 00:08:04,320
 You've got to practice on your own.

107
00:08:04,320 --> 00:08:07,880
 Or alternatively it's the way of the one.

108
00:08:07,880 --> 00:08:09,560
 Meaning it's the way of the Buddha.

109
00:08:09,560 --> 00:08:14,760
 You won't find this way anywhere else.

110
00:08:14,760 --> 00:08:19,310
 It's kind of remarkable that no one else ever taught

111
00:08:19,310 --> 00:08:22,800
 mindfulness the way the Buddha did.

112
00:08:22,800 --> 00:08:26,440
 Remarkable I guess because now it's everywhere.

113
00:08:26,440 --> 00:08:29,720
 Everyone is talking about mindfulness.

114
00:08:29,720 --> 00:08:31,900
 I don't know if anyone could claim that there were other

115
00:08:31,900 --> 00:08:33,280
 teachers besides the Buddha who

116
00:08:33,280 --> 00:08:35,320
 ever taught.

117
00:08:35,320 --> 00:08:38,420
 It's an interesting point when people try to say that all

118
00:08:38,420 --> 00:08:40,080
 religions are basically the

119
00:08:40,080 --> 00:08:41,080
 same.

120
00:08:41,080 --> 00:08:46,160
 Well, did anyone ever get so far as to teach mindfulness

121
00:08:46,160 --> 00:08:48,320
 besides the Buddha?

122
00:08:48,320 --> 00:08:49,600
 Do you ever see Jesus Christ?

123
00:08:49,600 --> 00:08:51,600
 Is there anything in the Bible?

124
00:08:51,600 --> 00:08:53,800
 The Christian Bible?

125
00:08:53,800 --> 00:08:55,800
 Anything in the Quran?

126
00:08:55,800 --> 00:09:03,220
 I don't even think you see that sort of thing in any other

127
00:09:03,220 --> 00:09:06,040
 Asian religions.

128
00:09:06,040 --> 00:09:08,760
 Thinking about Hinduism.

129
00:09:08,760 --> 00:09:09,760
 All the Upanishads.

130
00:09:09,760 --> 00:09:12,200
 Is there anything about mindfulness?

131
00:09:12,200 --> 00:09:13,200
 The Jainism?

132
00:09:13,200 --> 00:09:14,200
 The Jains?

133
00:09:14,200 --> 00:09:19,200
 It's an interesting question.

134
00:09:19,200 --> 00:09:21,950
 Of course we would generally just dismiss it and say of

135
00:09:21,950 --> 00:09:22,760
 course not.

136
00:09:22,760 --> 00:09:24,680
 None of these people were Buddhas.

137
00:09:24,680 --> 00:09:28,400
 And that's interesting because we don't normally associate

138
00:09:28,400 --> 00:09:30,840
 that with the Buddha's omnipotence

139
00:09:30,840 --> 00:09:33,200
 or the Buddha's great wisdom.

140
00:09:33,200 --> 00:09:36,900
 Yes, it was mindfulness that was the great thing that the

141
00:09:36,900 --> 00:09:37,840
 Buddha...

142
00:09:37,840 --> 00:09:40,690
 I mean it was a part of the great thing that the Buddha

143
00:09:40,690 --> 00:09:41,360
 taught.

144
00:09:41,360 --> 00:09:49,360
 It's the practice that the Buddha realized was important.

145
00:09:49,360 --> 00:09:52,200
 The Buddha realized was the path.

146
00:09:52,200 --> 00:10:00,320
 So it's the way of the Buddha.

147
00:10:00,320 --> 00:10:12,950
 Or it just means it is the way that goes in one direction, 

148
00:10:12,950 --> 00:10:14,040
ikang ayatin.

149
00:10:14,040 --> 00:10:17,840
 It means it only leads to nibhana.

150
00:10:17,840 --> 00:10:22,840
 You practice this and it doesn't lead anywhere else.

151
00:10:22,840 --> 00:10:27,350
 Practicing mindfulness couldn't lead you to hell for

152
00:10:27,350 --> 00:10:28,400
 example.

153
00:10:28,400 --> 00:10:32,960
 Mindfulness couldn't give any bad result.

154
00:10:32,960 --> 00:10:38,500
 Unlike other potentially wholesome states like effort,

155
00:10:38,500 --> 00:10:40,360
 concentration.

156
00:10:40,360 --> 00:10:42,760
 This is why it's so hard to talk about wrong mindfulness.

157
00:10:42,760 --> 00:10:46,840
 It's because mindfulness is always good.

158
00:10:46,840 --> 00:10:50,040
 Mindfulness only leads in the right direction.

159
00:10:50,040 --> 00:10:56,200
 Only leads to good things.

160
00:10:56,200 --> 00:11:09,480
 So what are these good things that it leads to?

161
00:11:09,480 --> 00:11:12,160
 I think, I may talk about it quite a bit and I know my

162
00:11:12,160 --> 00:11:14,440
 teacher does, but I think Buddhists

163
00:11:14,440 --> 00:11:18,000
 in general aren't really...

164
00:11:18,000 --> 00:11:20,760
 I think we should talk about why we're practicing

165
00:11:20,760 --> 00:11:21,800
 mindfulness.

166
00:11:21,800 --> 00:11:27,000
 You don't often hear it associated when people use the word

167
00:11:27,000 --> 00:11:28,560
 mindfulness.

168
00:11:28,560 --> 00:11:31,400
 Talk about Buddhist mindfulness.

169
00:11:31,400 --> 00:11:34,040
 We don't often hear about the reasons for practicing

170
00:11:34,040 --> 00:11:34,960
 mindfulness.

171
00:11:34,960 --> 00:11:38,790
 And the Buddha, first thing he said was why we're

172
00:11:38,790 --> 00:11:40,080
 practicing.

173
00:11:40,080 --> 00:11:45,460
 He gave these five reasons.

174
00:11:45,460 --> 00:11:51,720
 So this is the path that leads to these five things.

175
00:11:51,720 --> 00:11:55,620
 It sort of sets the tone and tells you what the Buddha's

176
00:11:55,620 --> 00:11:56,640
 focus was.

177
00:11:56,640 --> 00:11:59,680
 What was the Buddha teaching?

178
00:11:59,680 --> 00:12:02,440
 What was he preaching?

179
00:12:02,440 --> 00:12:06,560
 What was the purpose of his teachings?

180
00:12:06,560 --> 00:12:17,900
 What was the goal he had in mind when he taught mindfulness

181
00:12:17,900 --> 00:12:18,640
?

182
00:12:18,640 --> 00:12:20,880
 So the Buddha had these five reasons.

183
00:12:20,880 --> 00:12:24,240
 First is the purification of beings.

184
00:12:24,240 --> 00:12:34,520
 He taught mindfulness because it purifies.

185
00:12:34,520 --> 00:12:38,410
 Purification of course, purification is something we see

186
00:12:38,410 --> 00:12:43,480
 across the various religious traditions.

187
00:12:43,480 --> 00:12:47,490
 In India, it's a famous example, they would purify

188
00:12:47,490 --> 00:12:49,880
 themselves through water.

189
00:12:49,880 --> 00:12:53,680
 In Christianity baptism, I believe it must be something to

190
00:12:53,680 --> 00:12:55,480
 do with the purification through

191
00:12:55,480 --> 00:12:56,480
 water.

192
00:12:56,480 --> 00:13:01,240
 There are ritual purifications, purification through smoke,

193
00:13:01,240 --> 00:13:03,600
 that kind of thing in different

194
00:13:03,600 --> 00:13:10,880
 traditions.

195
00:13:10,880 --> 00:13:18,440
 There's purification by confession and so on.

196
00:13:18,440 --> 00:13:21,760
 So thought the purification comes through mindfulness.

197
00:13:21,760 --> 00:13:23,560
 This is what we just went through, right?

198
00:13:23,560 --> 00:13:26,040
 The seven purifications.

199
00:13:26,040 --> 00:13:28,440
 So first of all, he was very much concerned with pur

200
00:13:28,440 --> 00:13:29,160
ification.

201
00:13:29,160 --> 00:13:31,960
 It's a point to make about the Buddha.

202
00:13:31,960 --> 00:13:34,440
 It's not unique.

203
00:13:34,440 --> 00:13:36,590
 Purification was actually a big thing in the time of the

204
00:13:36,590 --> 00:13:37,080
 Buddha.

205
00:13:37,080 --> 00:13:41,480
 This was a key word that people were using it seems as a

206
00:13:41,480 --> 00:13:43,800
 reason for why they would go

207
00:13:43,800 --> 00:13:50,000
 off into the forest.

208
00:13:50,000 --> 00:13:52,400
 They were trying to free themselves from the defilements of

209
00:13:52,400 --> 00:13:53,240
 ordinary life.

210
00:13:53,240 --> 00:14:00,920
 You get so defiled, sullied by desires and conflicts,

211
00:14:00,920 --> 00:14:06,440
 delusion and arrogance and attachment

212
00:14:06,440 --> 00:14:08,880
 and all sorts of things.

213
00:14:08,880 --> 00:14:10,560
 So how to purify yourself?

214
00:14:10,560 --> 00:14:16,320
 This was the question and here was the Buddha's answer.

215
00:14:16,320 --> 00:14:20,560
 Purification comes through mindfulness.

216
00:14:20,560 --> 00:14:26,660
 Not from avoiding or rejecting or suppressing our mind

217
00:14:26,660 --> 00:14:30,840
 states, but understanding them.

218
00:14:30,840 --> 00:14:36,360
 I mean there's something very profound, very important

219
00:14:36,360 --> 00:14:39,560
 about the Satipatthana Sutta in

220
00:14:39,560 --> 00:14:45,230
 that even the bad states when it talks about the hindrances

221
00:14:45,230 --> 00:14:48,240
, the mind states that are clearly

222
00:14:48,240 --> 00:14:53,040
 troubling and troublesome, it talks about being mindful of

223
00:14:53,040 --> 00:14:55,160
 them, not running away or

224
00:14:55,160 --> 00:14:59,280
 trying to change them.

225
00:14:59,280 --> 00:15:02,520
 And that's how the Buddha said true purification comes

226
00:15:02,520 --> 00:15:04,940
 about because of course through seeing

227
00:15:04,940 --> 00:15:10,490
 them clearly as they are one no longer reacts and through

228
00:15:10,490 --> 00:15:13,600
 not reacting they don't have any

229
00:15:13,600 --> 00:15:18,800
 power.

230
00:15:18,800 --> 00:15:31,660
 The second result of practicing mindfulness is Dukkha Dom

231
00:15:31,660 --> 00:15:36,720
ana Sanam Atankamaaya.

232
00:15:36,720 --> 00:15:43,770
 The second one is Soka Paridewa Namsamatikamaaya to

233
00:15:43,770 --> 00:15:50,000
 overcome sorrow, lamentation, despair,

234
00:15:50,000 --> 00:16:00,640
 all kinds of problems.

235
00:16:00,640 --> 00:16:04,350
 And the implication here is that purification or a pure

236
00:16:04,350 --> 00:16:06,560
 mind is what allows one to solve

237
00:16:06,560 --> 00:16:07,560
 all of one's problems.

238
00:16:07,560 --> 00:16:10,980
 I mean this is very much one and the same.

239
00:16:10,980 --> 00:16:21,090
 When your mind is pure then the mind is not caught up with

240
00:16:21,090 --> 00:16:26,200
 depression and anxiety and

241
00:16:26,200 --> 00:16:36,080
 despair and disappointment.

242
00:16:36,080 --> 00:16:38,960
 There's a key difference here between trying to get what

243
00:16:38,960 --> 00:16:40,680
 you want, trying to be happy all

244
00:16:40,680 --> 00:16:47,300
 the time through accomplishing or achieving all of your

245
00:16:47,300 --> 00:16:50,280
 life goals and desires.

246
00:16:50,280 --> 00:16:57,290
 It's quite different from between that and learning to free

247
00:16:57,290 --> 00:17:00,400
 yourself from the desires,

248
00:17:00,400 --> 00:17:08,290
 free yourself from the need for the need for satiation or

249
00:17:08,290 --> 00:17:14,720
 satisfaction or the need for always

250
00:17:14,720 --> 00:17:21,320
 getting what you want.

251
00:17:21,320 --> 00:17:32,120
 So these are the two alternative ways of always being happy

252
00:17:32,120 --> 00:17:37,520
 to always get what you want or

253
00:17:37,520 --> 00:17:42,760
 to learn to give up wanting.

254
00:17:42,760 --> 00:17:46,840
 So when your mind is pure there's no room for, there's no

255
00:17:46,840 --> 00:17:49,200
 potential for depression,

256
00:17:49,200 --> 00:18:08,220
 anxiety, no potential for disappointment, no potential for

257
00:18:08,220 --> 00:18:18,360
 mourning loss, fearing loss.

258
00:18:18,360 --> 00:18:37,640
 The third result is freedom from suffering.

259
00:18:37,640 --> 00:18:41,830
 Just very much seems to be talking about in a very

260
00:18:41,830 --> 00:18:43,680
 practical sense.

261
00:18:43,680 --> 00:18:48,920
 You feel pain, what's the cure for pain?

262
00:18:48,920 --> 00:18:51,440
 Mindfulness.

263
00:18:51,440 --> 00:18:55,620
 You have mental suffering, what's the cure for mental

264
00:18:55,620 --> 00:18:56,800
 suffering?

265
00:18:56,800 --> 00:18:58,960
 Mindfulness.

266
00:18:58,960 --> 00:19:02,730
 It's quite curious, it's not what we, especially for

267
00:19:02,730 --> 00:19:05,840
 physical pain, we certainly don't think

268
00:19:05,840 --> 00:19:08,720
 that being mindful of the pain is going to cure it.

269
00:19:08,720 --> 00:19:12,020
 And in fact it's quite clear that when you begin to say

270
00:19:12,020 --> 00:19:13,960
 pain, pain doesn't get rid of

271
00:19:13,960 --> 00:19:19,800
 the pain.

272
00:19:19,800 --> 00:19:23,810
 Even with mental anguish in the beginning it seems quite

273
00:19:23,810 --> 00:19:26,000
 clear that mindfulness isn't

274
00:19:26,000 --> 00:19:27,920
 going to do anything about it.

275
00:19:27,920 --> 00:19:36,580
 Sometimes when you're mindful the anguish, the despair, the

276
00:19:36,580 --> 00:19:39,760
 sadness gets worse.

277
00:19:39,760 --> 00:19:43,040
 It's a different way of solving your problems.

278
00:19:43,040 --> 00:19:48,480
 The idea is to see that they're not actually problems.

279
00:19:48,480 --> 00:19:51,760
 Even your problems.

280
00:19:51,760 --> 00:19:55,850
 When you're sad about something, sadness is a perception,

281
00:19:55,850 --> 00:19:57,520
 hey this is a problem.

282
00:19:57,520 --> 00:20:02,040
 It's inherently problem seeking.

283
00:20:02,040 --> 00:20:07,050
 The mind state that is sad, even the mind state that is in

284
00:20:07,050 --> 00:20:10,000
 pain is inherently negative.

285
00:20:10,000 --> 00:20:19,520
 It's a negative reaction, this is bad, that is bad.

286
00:20:19,520 --> 00:20:21,740
 And that can't exist with mindfulness.

287
00:20:21,740 --> 00:20:27,560
 When you're mindful negativity can't exist.

288
00:20:27,560 --> 00:20:31,300
 Because of that, and it sounds incredibly naive or

289
00:20:31,300 --> 00:20:34,200
 simplistic to say, but it's simple,

290
00:20:34,200 --> 00:20:40,160
 it's not simplistic, it's that there's no suffering.

291
00:20:40,160 --> 00:20:51,240
 When you're mindful, when you stop being negative, you can

292
00:20:51,240 --> 00:20:53,720
't suffer.

293
00:20:53,720 --> 00:21:00,750
 We talk about overcoming or freeing ourselves from physical

294
00:21:00,750 --> 00:21:03,720
 and mental suffering.

295
00:21:03,720 --> 00:21:07,480
 We mean freeing but in a very different way.

296
00:21:07,480 --> 00:21:12,860
 It's not about some magic trick that makes the pain go away

297
00:21:12,860 --> 00:21:13,280
.

298
00:21:13,280 --> 00:21:17,820
 It's about looking at that negativity, hey this is pain,

299
00:21:17,820 --> 00:21:20,000
 this is bad, or hey this is,

300
00:21:20,000 --> 00:21:25,960
 experience is bad, makes me angry or sad or frustrated.

301
00:21:25,960 --> 00:21:31,780
 And changing that so that someone's yelling at you, it's

302
00:21:31,780 --> 00:21:33,520
 just hearing.

303
00:21:33,520 --> 00:21:44,160
 Someone's beating you, it's just feeling.

304
00:21:44,160 --> 00:21:46,560
 Someone of a radical way of looking at the world I guess,

305
00:21:46,560 --> 00:21:47,040
 right?

306
00:21:47,040 --> 00:21:53,200
 That sounds very radical.

307
00:21:53,200 --> 00:21:55,920
 But here's the question.

308
00:21:55,920 --> 00:22:00,240
 We have this thing we call reality.

309
00:22:00,240 --> 00:22:04,810
 And the very first question we should ask, that we never

310
00:22:04,810 --> 00:22:07,120
 asked and was never posed to

311
00:22:07,120 --> 00:22:12,370
 us, what are we going to do about this thing called reality

312
00:22:12,370 --> 00:22:12,920
?

313
00:22:12,920 --> 00:22:16,600
 What are we going to, what is our purpose?

314
00:22:16,600 --> 00:22:18,560
 What is the point?

315
00:22:18,560 --> 00:22:20,440
 We got caught up, right?

316
00:22:20,440 --> 00:22:24,430
 Okay, here I am born a child, these are my parents, we

317
00:22:24,430 --> 00:22:26,640
 never thought to ask what the

318
00:22:26,640 --> 00:22:28,640
 heck is going on?

319
00:22:28,640 --> 00:22:36,470
 We didn't have time, we had to eat, we were hungry, we had

320
00:22:36,470 --> 00:22:40,280
 to play, we were playful, and

321
00:22:40,280 --> 00:22:43,400
 then on and on and on through life.

322
00:22:43,400 --> 00:22:45,560
 And we never stopped to say, wait what am I doing?

323
00:22:45,560 --> 00:22:47,440
 What is this all?

324
00:22:47,440 --> 00:22:51,590
 We don't ask these fundamental questions until much later

325
00:22:51,590 --> 00:22:54,120
 and by then we're steeped in culture

326
00:22:54,120 --> 00:22:59,910
 and wrong views and all caught up in our own likes and disl

327
00:22:59,910 --> 00:23:02,640
ikes and it's very hard to be

328
00:23:02,640 --> 00:23:03,640
 a philosopher.

329
00:23:03,640 --> 00:23:12,260
 So it's very, we hold wise philosophers in great esteem, it

330
00:23:12,260 --> 00:23:14,280
's not easy.

331
00:23:14,280 --> 00:23:17,720
 It's very easy to have a wrong philosophy or a philosophy

332
00:23:17,720 --> 00:23:18,920
 that is flawed.

333
00:23:18,920 --> 00:23:22,280
 That's why you have philosophers arguing with each other,

334
00:23:22,280 --> 00:23:23,960
 even the really good ones.

335
00:23:23,960 --> 00:23:30,340
 And then you have people who have a fairly deep philosophy

336
00:23:30,340 --> 00:23:33,360
 but can't actually practice

337
00:23:33,360 --> 00:23:40,880
 it because they're too caught up in their own addictions

338
00:23:40,880 --> 00:23:44,440
 and their own problems.

339
00:23:44,440 --> 00:23:48,520
 Mindfulness is an answer to this very fundamental question.

340
00:23:48,520 --> 00:23:50,720
 It's not a question of hey, how am I going to live my life?

341
00:23:50,720 --> 00:23:54,680
 No, it's like what is going on right here and now?

342
00:23:54,680 --> 00:23:57,960
 How am I going to relate to reality?

343
00:23:57,960 --> 00:24:01,320
 It's not about being a good person in society.

344
00:24:01,320 --> 00:24:04,000
 That's a much later question.

345
00:24:04,000 --> 00:24:11,240
 That's a much more abstract question.

346
00:24:11,240 --> 00:24:16,040
 The very real question is here now, what's going on?

347
00:24:16,040 --> 00:24:24,610
 How do I relate to these experiences that I'm having here

348
00:24:24,610 --> 00:24:26,320
 and now?

349
00:24:26,320 --> 00:24:29,410
 And so it does address at this very fundamental level the

350
00:24:29,410 --> 00:24:30,880
 problem of suffering.

351
00:24:30,880 --> 00:24:35,360
 It's at this level there is pain.

352
00:24:35,360 --> 00:24:38,320
 How are you going to react to that?

353
00:24:38,320 --> 00:24:43,700
 There is sadness, there is anger, there is attachment, all

354
00:24:43,700 --> 00:24:45,240
 these things.

355
00:24:45,240 --> 00:24:50,440
 Mindfulness is this low level interaction with reality.

356
00:24:50,440 --> 00:24:55,320
 It's a very fundamental basic level of interaction.

357
00:24:55,320 --> 00:25:00,440
 That's of course what's so powerful about it because it

358
00:25:00,440 --> 00:25:03,280
 then cascades and affects every

359
00:25:03,280 --> 00:25:09,200
 part of our lives.

360
00:25:09,200 --> 00:25:12,500
 Brees us from all kinds of suffering because all kinds of

361
00:25:12,500 --> 00:25:14,480
 suffering eventually come down

362
00:25:14,480 --> 00:25:19,880
 to this experiential reality.

363
00:25:19,880 --> 00:25:24,690
 The fourth result of mindfulness is that it sets you on the

364
00:25:24,690 --> 00:25:25,920
 right path.

365
00:25:25,920 --> 00:25:27,880
 I've talked about all these before.

366
00:25:27,880 --> 00:25:31,000
 This is something that I talk about a lot.

367
00:25:31,000 --> 00:25:35,020
 The right path and this is what we've gone over for the

368
00:25:35,020 --> 00:25:36,200
 past weeks.

369
00:25:36,200 --> 00:25:39,860
 The right path is more of a way of being rather than a way

370
00:25:39,860 --> 00:25:42,000
 of a place that we're going.

371
00:25:42,000 --> 00:25:46,960
 You're not really going anywhere.

372
00:25:46,960 --> 00:25:53,660
 That's probably doesn't need to be emphasized too much

373
00:25:53,660 --> 00:25:57,120
 because it's quite clear it's about

374
00:25:57,120 --> 00:25:58,440
 changing who you are.

375
00:25:58,440 --> 00:26:02,950
 The way is becoming to live your life in a certain way of

376
00:26:02,950 --> 00:26:05,600
 experiencing reality in a certain

377
00:26:05,600 --> 00:26:07,600
 way.

378
00:26:07,600 --> 00:26:13,220
 But what it isn't is I must become a monk or even Buddhist

379
00:26:13,220 --> 00:26:18,240
 or quit my job, this or that,

380
00:26:18,240 --> 00:26:20,040
 go and live off in the forest.

381
00:26:20,040 --> 00:26:25,160
 It doesn't mean any of that.

382
00:26:25,160 --> 00:26:26,160
 The way is mindfulness.

383
00:26:26,160 --> 00:26:30,640
 I mean that's the key here.

384
00:26:30,640 --> 00:26:31,640
 What is the right way?

385
00:26:31,640 --> 00:26:33,480
 It's not too complicated.

386
00:26:33,480 --> 00:26:37,470
 I mean all that talk about the eightfold path and the seven

387
00:26:37,470 --> 00:26:40,120
 purifications is quite interesting

388
00:26:40,120 --> 00:26:42,120
 and useful.

389
00:26:42,120 --> 00:26:45,680
 But it's not even so complicated.

390
00:26:45,680 --> 00:26:50,690
 All of that comes about through the practice of mindfulness

391
00:26:50,690 --> 00:26:51,040
.

392
00:26:51,040 --> 00:26:58,770
 And finally the fifth result of practicing mindfulness is

393
00:26:58,770 --> 00:27:01,880
 the path to nibhana.

394
00:27:01,880 --> 00:27:05,000
 Mindfulness is the path to the deathless.

395
00:27:05,000 --> 00:27:07,870
 Mindfulness is what leads to that which has no beginning

396
00:27:07,870 --> 00:27:09,560
 and no end, that which doesn't

397
00:27:09,560 --> 00:27:18,000
 arise and doesn't cease, which is outside of samsara.

398
00:27:18,000 --> 00:27:22,850
 Through mindfulness one experiences freedom from suffering

399
00:27:22,850 --> 00:27:24,960
 in a very visceral and real

400
00:27:24,960 --> 00:27:25,960
 way.

401
00:27:25,960 --> 00:27:29,090
 I mean it's not just some abstract, "Hey I feel pain and I

402
00:27:29,090 --> 00:27:30,260
 don't suffer."

403
00:27:30,260 --> 00:27:35,550
 It's really and truly this freedom from this experience of

404
00:27:35,550 --> 00:27:40,320
 complete release, complete cessation.

405
00:27:40,320 --> 00:27:44,240
 It's the most peaceful thing.

406
00:27:44,240 --> 00:27:52,470
 It's the only peaceful thing that is true and unadulterated

407
00:27:52,470 --> 00:27:53,840
 peace.

408
00:27:53,840 --> 00:28:00,000
 That's nibhana, freedom from suffering.

409
00:28:00,000 --> 00:28:01,920
 So that's the first part of the sutta.

410
00:28:01,920 --> 00:28:03,240
 That's the first thing the Buddha says.

411
00:28:03,240 --> 00:28:04,680
 It's by way of introduction.

412
00:28:04,680 --> 00:28:07,040
 And then he says, "What is this one way?

413
00:28:07,040 --> 00:28:11,440
 It's the four satipatthana."

414
00:28:11,440 --> 00:28:14,360
 And then he goes into explaining the satipatthana.

415
00:28:14,360 --> 00:28:17,620
 So I'll go over all this and then we'll get into each one

416
00:28:17,620 --> 00:28:19,360
 in each section and we'll go

417
00:28:19,360 --> 00:28:24,920
 through each section one by one.

418
00:28:24,920 --> 00:28:28,730
 Today we talk about something that should be familiar to

419
00:28:28,730 --> 00:28:29,760
 many of you.

420
00:28:29,760 --> 00:28:30,760
 Why we're practicing.

421
00:28:30,760 --> 00:28:32,920
 What are the reasons?

422
00:28:32,920 --> 00:28:35,600
 What are the benefits?

423
00:28:35,600 --> 00:28:40,040
 What does it mean?

424
00:28:40,040 --> 00:28:42,800
 What does it mean this is the only way?

425
00:28:42,800 --> 00:28:46,800
 So there you go.

426
00:28:46,800 --> 00:28:59,240
 That's the Dhamma for tonight.

427
00:28:59,240 --> 00:29:01,800
 Tomorrow is no meeting.

428
00:29:01,800 --> 00:29:20,840
 Tomorrow we have sutta study, I think.

429
00:29:20,840 --> 00:29:40,120
 And we have one question tonight.

430
00:29:40,120 --> 00:29:43,020
 Do psychic powers naturally develop through meditation or

431
00:29:43,020 --> 00:29:44,480
 must they be actively worked

432
00:29:44,480 --> 00:29:47,480
 towards?

433
00:29:47,480 --> 00:30:01,800
 We can be both ways, but meditation is work.

434
00:30:01,800 --> 00:30:07,880
 That's an important point in response to this question.

435
00:30:07,880 --> 00:30:11,960
 Nothing comes naturally because meditation itself is work.

436
00:30:11,960 --> 00:30:16,800
 I say that I think we often get the idea that meditation is

437
00:30:16,800 --> 00:30:21,360
 something where you don't work.

438
00:30:21,360 --> 00:30:31,040
 You just sit and let things come to you.

439
00:30:31,040 --> 00:30:33,120
 The question really is how are you meditating?

440
00:30:33,120 --> 00:30:41,250
 Are you meditating in such a way that these psychic powers

441
00:30:41,250 --> 00:30:43,320
 will arise?

442
00:30:43,320 --> 00:30:48,220
 There are many types of meditation where it's unlikely for

443
00:30:48,220 --> 00:30:51,200
 the mind to go in that direction.

444
00:30:51,200 --> 00:30:55,000
 Mindfulness isn't highly likely to give you magical powers

445
00:30:55,000 --> 00:30:56,800
 because you're not working

446
00:30:56,800 --> 00:30:57,800
 in such a way.

447
00:30:57,800 --> 00:31:00,440
 I mean, it's not mysterious.

448
00:31:00,440 --> 00:31:11,800
 These things come from highly cultivated and trained minds.

449
00:31:11,800 --> 00:31:14,160
 So if you're not training your mind in that way, they're

450
00:31:14,160 --> 00:31:15,400
 not really going to arise.

451
00:31:15,400 --> 00:31:18,730
 But they can be incidental if your intention is a certain

452
00:31:18,730 --> 00:31:20,880
 way, but you happen to be training

453
00:31:20,880 --> 00:31:22,160
 yourself.

454
00:31:22,160 --> 00:31:27,060
 Your mind happens to be training or building up habits that

455
00:31:27,060 --> 00:31:30,040
 have some connection with magical

456
00:31:30,040 --> 00:31:31,040
 powers.

457
00:31:31,040 --> 00:31:33,830
 Like any kind of summit to practice, your intention is not

458
00:31:33,830 --> 00:31:35,280
 to gain magical powers, but

459
00:31:35,280 --> 00:31:38,720
 your mind is becoming so strong and so fixed and focused

460
00:31:38,720 --> 00:31:41,800
 that it's not hard for it to be

461
00:31:41,800 --> 00:31:51,680
 then nudged over into cultivating psychic powers.

462
00:31:51,680 --> 00:31:59,070
 So they don't come naturally, but the power of meditation,

463
00:31:59,070 --> 00:32:02,580
 many kinds of meditation can

464
00:32:02,580 --> 00:32:09,200
 be applied to various kinds of psychic power.

465
00:32:09,200 --> 00:32:21,130
 Or you can just outright cultivate that state of mind that

466
00:32:21,130 --> 00:32:26,680
 leads to psychic power.

467
00:32:26,680 --> 00:32:29,320
 So one question tonight.

468
00:32:29,320 --> 00:32:31,320
 Thank you all for tuning in.

469
00:32:31,320 --> 00:32:31,880
 Have a good night.

470
00:32:31,880 --> 00:32:32,880
 Thank you.

471
00:32:32,880 --> 00:33:02,880
 [

