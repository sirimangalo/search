1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:14,000
 Welcome to our live broadcast evening Dhamma session.

3
00:00:14,000 --> 00:00:26,320
 So I'm just going, still going over my stocks that I have

4
00:00:26,320 --> 00:00:29,000
 to give this weekend.

5
00:00:29,000 --> 00:00:34,110
 I thought it would be nice to go over a little more of it

6
00:00:34,110 --> 00:00:35,000
 with you.

7
00:00:35,000 --> 00:00:40,000
 Be my test audience.

8
00:00:40,000 --> 00:00:45,890
 So I'm going to give this talk on wrong mindfulness or

9
00:00:45,890 --> 00:00:50,000
 wrong practice.

10
00:00:50,000 --> 00:00:56,000
 And it's a somewhat difficult talk to get right because

11
00:00:56,000 --> 00:01:03,000
 mindfulness is always good.

12
00:01:03,000 --> 00:01:06,280
 The Buddha said, "Satiṁ ca kuāṁ bhikkhu eśa bati ka�

13
00:01:06,280 --> 00:01:07,000
� vadāmī"

14
00:01:07,000 --> 00:01:13,000
 Mindfulness is always useful. It's the only quality.

15
00:01:13,000 --> 00:01:17,060
 If you take the seven bhojangas, seven factors of

16
00:01:17,060 --> 00:01:19,000
 enlightenment,

17
00:01:19,000 --> 00:01:23,300
 some of them are only useful when you're agitated to calm

18
00:01:23,300 --> 00:01:25,000
 you down.

19
00:01:25,000 --> 00:01:29,260
 Some are only useful when you're lazy or overly

20
00:01:29,260 --> 00:01:35,000
 concentrated to perk you up.

21
00:01:35,000 --> 00:01:41,000
 Mindfulness is always useful, the Buddha said.

22
00:01:41,000 --> 00:01:48,060
 And the idea that mindfulness practice could lead to

23
00:01:48,060 --> 00:01:54,000
 problems, could be dangerous,

24
00:01:54,000 --> 00:02:08,000
 is especially strange because mindfulness stops problems.

25
00:02:08,000 --> 00:02:11,000
 That's what it's for. That's what it does.

26
00:02:11,000 --> 00:02:14,000
 It stops papancha.

27
00:02:14,000 --> 00:02:19,220
 It stops us from making more out of things than they

28
00:02:19,220 --> 00:02:21,000
 actually are.

29
00:02:21,000 --> 00:02:26,280
 The Buddha said, "Yaṇī sotāṇī lokasṁing sati teśa

30
00:02:26,280 --> 00:02:27,000
ṁ nivārayam"

31
00:02:27,000 --> 00:02:30,400
 Whatever streams there are in the world, the Buddha used

32
00:02:30,400 --> 00:02:35,000
 this word "stream", it's quite interesting.

33
00:02:35,000 --> 00:02:47,000
 Whatever channels there are for problems basically,

34
00:02:47,000 --> 00:02:54,000
 for stress, for suffering, for mental illness if you will.

35
00:02:54,000 --> 00:03:03,000
 Mindfulness stops them. Mindfulness prevents them.

36
00:03:03,000 --> 00:03:06,610
 Mindfulness stops all problems is basically it, because

37
00:03:06,610 --> 00:03:11,000
 mindfulness is about seeing things just as they are.

38
00:03:11,000 --> 00:03:16,930
 It's about grasping experience for what it is, not getting

39
00:03:16,930 --> 00:03:23,000
 lost in what we want it to be or what we think it is.

40
00:03:23,000 --> 00:03:31,630
 What we think of it, right, wrong, good, bad, me, mine, all

41
00:03:31,630 --> 00:03:36,000
 of those have no place in mindfulness.

42
00:03:36,000 --> 00:03:40,730
 So how is it that people practice meditation and have

43
00:03:40,730 --> 00:03:42,000
 problems?

44
00:03:42,000 --> 00:03:47,310
 I'm happy to say, I mean maybe verging on a little bit

45
00:03:47,310 --> 00:03:54,000
 proud to say that I've never had a meditator go crazy on me

46
00:03:54,000 --> 00:03:54,000
.

47
00:03:54,000 --> 00:03:58,000
 But I have seen meditators go crazy.

48
00:03:58,000 --> 00:04:03,220
 I spent three nights in a mental hospital before I was a

49
00:04:03,220 --> 00:04:07,000
 monk with a crazy patient, a crazy meditator.

50
00:04:07,000 --> 00:04:11,780
 A meditator who had gone off the reservation, said, "No,

51
00:04:11,780 --> 00:04:14,000
 off the..." What's the word?

52
00:04:14,000 --> 00:04:18,000
 "He'd gone crazy."

53
00:04:18,000 --> 00:04:22,000
 And it was quite an experience.

54
00:04:22,000 --> 00:04:24,000
 So how does it happen?

55
00:04:24,000 --> 00:04:27,940
 In my last talk I already talked about these, we'll go over

56
00:04:27,940 --> 00:04:29,000
 them again.

57
00:04:29,000 --> 00:04:33,000
 One, you're unmindful.

58
00:04:33,000 --> 00:04:40,000
 Two, your mindfulness is misdirected.

59
00:04:40,000 --> 00:04:45,000
 Three, your mindfulness is lapsed.

60
00:04:45,000 --> 00:04:52,000
 And four, your mindfulness is impotent or ineffectual.

61
00:04:52,000 --> 00:04:56,190
 These are the only ways I can see that mindfulness could go

62
00:04:56,190 --> 00:04:57,000
 wrong.

63
00:04:57,000 --> 00:05:01,000
 So first you'd be unmindful.

64
00:05:01,000 --> 00:05:03,360
 It goes without saying that if you don't ever practice

65
00:05:03,360 --> 00:05:07,000
 mindfulness it can't possibly help you.

66
00:05:07,000 --> 00:05:11,280
 But there are people who go to meditation centers and for

67
00:05:11,280 --> 00:05:16,000
 whatever reason don't actually practice mindfulness.

68
00:05:16,000 --> 00:05:20,730
 I remember once I had one, it's my most shameful experience

69
00:05:20,730 --> 00:05:22,000
 as a teacher.

70
00:05:22,000 --> 00:05:25,000
 I hadn't been a teacher that long.

71
00:05:25,000 --> 00:05:29,000
 So there was this young Thai man and I said to him,

72
00:05:29,000 --> 00:05:32,460
 "Okay, so start off with this many minutes walking, this

73
00:05:32,460 --> 00:05:35,000
 many minutes sitting."

74
00:05:35,000 --> 00:05:39,000
 We start off with like ten of walking and ten of sitting,

75
00:05:39,000 --> 00:05:41,000
 just do them together.

76
00:05:41,000 --> 00:05:45,000
 And he came back the next day and everything's fine.

77
00:05:45,000 --> 00:05:47,250
 The next day and the next day and it was about six, seven

78
00:05:47,250 --> 00:05:48,000
 days, nothing.

79
00:05:48,000 --> 00:05:55,020
 No pain, no stress, didn't have any challenges like he wasn

80
00:05:55,020 --> 00:05:58,000
't dealing with anything.

81
00:05:58,000 --> 00:06:00,990
 And so I asked him, "How many rounds are you doing a day?"

82
00:06:00,990 --> 00:06:05,000
 He said, "One."

83
00:06:05,000 --> 00:06:09,330
 Well I told him to do ten minutes walking, ten minutes

84
00:06:09,330 --> 00:06:12,000
 sitting, that's what he did.

85
00:06:12,000 --> 00:06:15,260
 And I said, "What did you do for the rest of the day?" I

86
00:06:15,260 --> 00:06:24,000
 sat around in red comics or something.

87
00:06:24,000 --> 00:06:28,640
 I didn't know what to do. He'd gone through half the course

88
00:06:28,640 --> 00:06:30,000
 by this time.

89
00:06:30,000 --> 00:06:34,100
 So I sent him off to one of the senior teachers and said, "

90
00:06:34,100 --> 00:06:37,000
We'll make him their problem."

91
00:06:37,000 --> 00:06:42,440
 But it felt kind of bad, it was partially my fault for not

92
00:06:42,440 --> 00:06:44,000
 being clear.

93
00:06:44,000 --> 00:06:49,000
 Ten minutes is per session.

94
00:06:49,000 --> 00:06:51,990
 And we don't of course meditation practice, but living in

95
00:06:51,990 --> 00:06:55,000
 an environment like this without being mindful,

96
00:06:55,000 --> 00:07:00,000
 that could very easily drive you crazy.

97
00:07:00,000 --> 00:07:06,000
 The second is misdirected mindfulness.

98
00:07:06,000 --> 00:07:11,030
 This is I think potentially where a lot of the problems

99
00:07:11,030 --> 00:07:12,000
 come from.

100
00:07:12,000 --> 00:07:17,240
 Because if you're mindful of concepts, there is a lot more

101
00:07:17,240 --> 00:07:18,000
 danger.

102
00:07:18,000 --> 00:07:23,450
 I would go on a limb and say samatha meditation, which

103
00:07:23,450 --> 00:07:25,000
 focuses on concepts,

104
00:07:25,000 --> 00:07:29,000
 is much more dangerous than vipassana meditation.

105
00:07:29,000 --> 00:07:32,220
 And I would bet there are statistics, you could find

106
00:07:32,220 --> 00:07:36,000
 statistics, you could accumulate statistics to show that.

107
00:07:36,000 --> 00:07:41,210
 Because reality is quite limited. I mean, isn't this, it

108
00:07:41,210 --> 00:07:44,000
 can be quite boring at times.

109
00:07:44,000 --> 00:07:47,620
 Reality turns out to be seeing, hearing, smelling, tasting,

110
00:07:47,620 --> 00:07:49,000
 feeling, thinking.

111
00:07:49,000 --> 00:07:52,000
 Where's the fun in that?

112
00:07:52,000 --> 00:07:56,250
 I mean, if you get into it, it's quite exciting, but there

113
00:07:56,250 --> 00:08:00,490
's none of the endless possibilities of tranquility

114
00:08:00,490 --> 00:08:02,000
 meditation.

115
00:08:02,000 --> 00:08:04,350
 What would you rather do, remember your past lives or sit

116
00:08:04,350 --> 00:08:08,000
 here saying, "Rising, falling, rising, falling."

117
00:08:08,000 --> 00:08:13,620
 Or gain magical powers, read people's minds, all these

118
00:08:13,620 --> 00:08:17,000
 things, that would be more fun.

119
00:08:17,000 --> 00:08:24,660
 The world of concepts is infinite. You can imagine a rabbit

120
00:08:24,660 --> 00:08:27,000
 with horns.

121
00:08:27,000 --> 00:08:30,780
 You can imagine a rabbit with antlers. You can imagine a

122
00:08:30,780 --> 00:08:37,000
 pink rabbit with antlers, or a blue, or a yellow.

123
00:08:37,000 --> 00:08:42,240
 And because concepts are infinite, you can easily get lost,

124
00:08:42,240 --> 00:08:47,000
 very easily, without strict guidance and regulation.

125
00:08:47,000 --> 00:08:52,500
 When you're mindful of concepts, and by mindful here again,

126
00:08:52,500 --> 00:08:56,000
 we just mean when you grasp the concept,

127
00:08:56,000 --> 00:09:01,000
 it's possible to get caught up in the concept.

128
00:09:01,000 --> 00:09:04,810
 I would still argue that mindfulness isn't to blame here,

129
00:09:04,810 --> 00:09:08,000
 because in samatha you still need mindfulness,

130
00:09:08,000 --> 00:09:16,000
 and if you have mindfulness, it can't possibly go wrong,

131
00:09:16,000 --> 00:09:17,000
 because you grasp the object.

132
00:09:17,000 --> 00:09:22,000
 Like, for example, a candle flame, say, easy example.

133
00:09:22,000 --> 00:09:26,350
 You focus on the candle flame, and if you're mindful of it,

134
00:09:26,350 --> 00:09:28,000
 then you know that this is fire.

135
00:09:28,000 --> 00:09:32,980
 And you say, "Fire, fire, fire." And you're grasping the

136
00:09:32,980 --> 00:09:35,000
 concept of fire well.

137
00:09:35,000 --> 00:09:40,200
 That mindfulness is a good thing. It's what keeps you from

138
00:09:40,200 --> 00:09:44,000
 seeing it as more, or different.

139
00:09:44,000 --> 00:09:49,000
 But the thing about concepts is that they can become,

140
00:09:49,000 --> 00:09:50,000
 because they're in your mind,

141
00:09:50,000 --> 00:09:54,110
 eventually you get the picture of the fire in your mind,

142
00:09:54,110 --> 00:09:55,000
 they can morph

143
00:09:55,000 --> 00:10:03,000
 and become different things.

144
00:10:03,000 --> 00:10:12,140
 If you're mindful of the past, mindful of the future, much

145
00:10:12,140 --> 00:10:15,000
 more likely,

146
00:10:15,000 --> 00:10:17,670
 and why this is really considered wrong, we talked about

147
00:10:17,670 --> 00:10:21,000
 this, is that it's just not the way to enlightenment.

148
00:10:21,000 --> 00:10:25,000
 There's nothing wrong with it. In fact, it can be actually

149
00:10:25,000 --> 00:10:29,650
 indirectly helpful, because it can gain great concentration

150
00:10:29,650 --> 00:10:30,000
.

151
00:10:30,000 --> 00:10:34,110
 But much more common, and you do see this quite a bit, are

152
00:10:34,110 --> 00:10:39,000
 people, these meditators who have been practicing for years

153
00:10:39,000 --> 00:10:39,000
,

154
00:10:39,000 --> 00:10:42,230
 and have a very strong meditation practice, but have never

155
00:10:42,230 --> 00:10:44,000
 attained nibhana,

156
00:10:44,000 --> 00:10:48,760
 have never realized cessation of suffering, because they

157
00:10:48,760 --> 00:10:51,000
 have pleasant experiences,

158
00:10:51,000 --> 00:10:59,290
 because they're focusing on concepts, and really clear on

159
00:10:59,290 --> 00:11:01,000
 reality.

160
00:11:01,000 --> 00:11:05,000
 The third, lapsed mindfulness. Lapsed mindfulness, I think,

161
00:11:05,000 --> 00:11:12,000
 is where real danger for one's mental health is.

162
00:11:12,000 --> 00:11:22,000
 And this you see. Because when you meditate, it brings up,

163
00:11:22,000 --> 00:11:25,000
 this is where meditation is potentially quite dangerous,

164
00:11:25,000 --> 00:11:30,540
 simply because it brings up deep dark emotions that can be

165
00:11:30,540 --> 00:11:32,000
 dangerous.

166
00:11:32,000 --> 00:11:35,260
 I don't want to scare you, and most people this isn't a

167
00:11:35,260 --> 00:11:39,000
 problem, but it's important to go slow,

168
00:11:39,000 --> 00:11:41,980
 and it's important, in some cases, it's really important to

169
00:11:41,980 --> 00:11:43,000
 have a teacher.

170
00:11:43,000 --> 00:11:47,350
 Most people don't have really crazy things inside, but it

171
00:11:47,350 --> 00:11:52,350
 can happen that things come up that you're not easily able

172
00:11:52,350 --> 00:11:54,000
 to deal with.

173
00:11:54,000 --> 00:12:02,570
 It's one of the great things of watching an experienced

174
00:12:02,570 --> 00:12:06,000
 teacher in action.

175
00:12:06,000 --> 00:12:09,900
 My teacher, there was one time, it stands out, I remember,

176
00:12:09,900 --> 00:12:12,000
 we were sitting up in his kruti,

177
00:12:12,000 --> 00:12:16,370
 waiting for meditators, and suddenly one of the nuns who

178
00:12:16,370 --> 00:12:18,000
 worked in the office,

179
00:12:18,000 --> 00:12:21,380
 a couple runs in and says, "There's a meditator outside of

180
00:12:21,380 --> 00:12:23,000
 the office, and she's going crazy.

181
00:12:23,000 --> 00:12:30,360
 She's saying nonsense, and running around like a crazy

182
00:12:30,360 --> 00:12:32,000
 person."

183
00:12:32,000 --> 00:12:34,680
 He goes, "Tell her to be mindful." Or no, he just said, "Be

184
00:12:34,680 --> 00:12:36,000
 mindful, be mindful."

185
00:12:36,000 --> 00:12:38,780
 Because this nun herself was quite unmindful, and the way

186
00:12:38,780 --> 00:12:40,000
 he just sat there and said,

187
00:12:40,000 --> 00:12:44,000
 "Yeah, yeah, just be mindful."

188
00:12:44,000 --> 00:12:49,240
 Like we're all jumping up and getting ready to go and save

189
00:12:49,240 --> 00:12:50,000
 the day.

190
00:12:57,000 --> 00:13:01,330
 Because things come up and you react to them, and you get

191
00:13:01,330 --> 00:13:03,000
 disturbed by them, you get taken off guard.

192
00:13:03,000 --> 00:13:08,000
 It happens with both positive and negative conditions.

193
00:13:08,000 --> 00:13:10,900
 With positive conditions you get lost in them. "Hey, that's

194
00:13:10,900 --> 00:13:13,000
 interesting, that's nice."

195
00:13:13,000 --> 00:13:16,010
 And because you stop being mindful, and both because you

196
00:13:16,010 --> 00:13:19,000
 stop being mindful and because of the...

197
00:13:19,000 --> 00:13:23,020
 They're so strong, and they're so powerful, they can really

198
00:13:23,020 --> 00:13:24,000
 suck you in.

199
00:13:24,000 --> 00:13:28,000
 Negative as well, if you have negative things from...

200
00:13:28,000 --> 00:13:31,000
 Most of us have something negative from the past.

201
00:13:31,000 --> 00:13:34,000
 It sucks you in, it comes as a result.

202
00:13:34,000 --> 00:13:37,080
 It shows itself as a result of the practice, so that's a

203
00:13:37,080 --> 00:13:38,000
 good thing.

204
00:13:38,000 --> 00:13:40,000
 Both of them are really good.

205
00:13:40,000 --> 00:13:43,450
 It's good that positive things come out, but negative

206
00:13:43,450 --> 00:13:44,000
 things come out.

207
00:13:44,000 --> 00:13:48,000
 It's good that the deep stuff comes out, because it's...

208
00:13:48,000 --> 00:13:53,410
 It means you're becoming more in tune with yourself and

209
00:13:53,410 --> 00:13:56,000
 more in tune with reality.

210
00:13:56,000 --> 00:14:01,000
 And you're getting a deeper experience of who you are.

211
00:14:01,000 --> 00:14:06,000
 But without a stabilizing presence, someone to remind you,

212
00:14:06,000 --> 00:14:07,000
 or without reminding yourself that,

213
00:14:07,000 --> 00:14:10,000
 "Hey, it's still just an experience."

214
00:14:10,000 --> 00:14:12,000
 It can be dangerous.

215
00:14:12,000 --> 00:14:16,670
 There were these... In the texts, in the Sanyutanika, I

216
00:14:16,670 --> 00:14:18,000
 think...

217
00:14:18,000 --> 00:14:21,680
 There were these monks who... Buddha taught them

218
00:14:21,680 --> 00:14:24,000
 mindfulness of...

219
00:14:24,000 --> 00:14:27,480
 I think it was mindfulness of the loathsome-ness of the

220
00:14:27,480 --> 00:14:28,000
 body, or...

221
00:14:28,000 --> 00:14:30,000
 I can't remember what it was now, but...

222
00:14:30,000 --> 00:14:32,410
 They taught them something, and they all killed themselves

223
00:14:32,410 --> 00:14:33,000
 as a result.

224
00:14:33,000 --> 00:14:35,000
 They had someone kill them.

225
00:14:35,000 --> 00:14:38,470
 They hired some guy to kill them, because they were so

226
00:14:38,470 --> 00:14:40,000
 revolted by...

227
00:14:40,000 --> 00:14:43,000
 It was just such a bizarre story.

228
00:14:43,000 --> 00:14:47,000
 Like, they reacted so violently to this.

229
00:14:47,000 --> 00:14:49,000
 The commentary says they had really bad karma.

230
00:14:49,000 --> 00:14:53,000
 They had been hunters in past lives, but...

231
00:14:53,000 --> 00:14:58,000
 The point is that things can come out, and if you're not...

232
00:14:58,000 --> 00:15:03,170
 If you're not ready for the truth, if you can't handle the

233
00:15:03,170 --> 00:15:04,000
 truth, then...

234
00:15:04,000 --> 00:15:07,000
 It can be potentially problematic.

235
00:15:07,000 --> 00:15:10,550
 So not to be clear that this is one way that mindfulness

236
00:15:10,550 --> 00:15:12,000
 practice is...

237
00:15:12,000 --> 00:15:17,000
 I've never seen it happen with any of my students, but...

238
00:15:17,000 --> 00:15:20,000
 Mindfulness meditation is potentially dangerous.

239
00:15:20,000 --> 00:15:25,200
 There was one meditator that I spent time in the mental

240
00:15:25,200 --> 00:15:27,000
 hospital with.

241
00:15:27,000 --> 00:15:31,670
 I wasn't her teacher, but the problem was she had, I think,

242
00:15:31,670 --> 00:15:35,000
 too much instruction from wrong people.

243
00:15:35,000 --> 00:15:38,990
 There were some people who weren't her teachers going in

244
00:15:38,990 --> 00:15:39,000
 and...

245
00:15:39,000 --> 00:15:44,110
 Going into her room and talking to her and feeding her all

246
00:15:44,110 --> 00:15:46,000
 sorts of weird ideas.

247
00:15:46,000 --> 00:15:49,460
 There's a lot of superstition and folk Buddhism and telling

248
00:15:49,460 --> 00:15:52,000
 her things about past lives and karma and...

249
00:15:52,000 --> 00:15:58,050
 Really getting her lost and winding her up, I think,

250
00:15:58,050 --> 00:16:02,000
 because suddenly she's out running around and...

251
00:16:02,000 --> 00:16:05,000
 She was using the mantras, but she was saying all...

252
00:16:05,000 --> 00:16:09,000
 She was using them to wind herself up, so...

253
00:16:09,000 --> 00:16:12,090
 She would say to herself, "Wisdom, wisdom, wisdom." I

254
00:16:12,090 --> 00:16:14,000
 remember that was one.

255
00:16:14,000 --> 00:16:17,800
 Like, she would just repeat a mantra to her, but it would

256
00:16:17,800 --> 00:16:20,000
 wind her up instead of...

257
00:16:20,000 --> 00:16:25,000
 Calm her down, because it wasn't based on any experience.

258
00:16:25,000 --> 00:16:29,000
 This is an example. People get the mantra wrong.

259
00:16:29,000 --> 00:16:32,820
 Instead of using it to note what's there, they use it to

260
00:16:32,820 --> 00:16:35,000
 note what they want to be there.

261
00:16:35,000 --> 00:16:41,000
 And that'll wind you up.

262
00:16:41,000 --> 00:16:45,100
 It's quite easy to get lost if you're not clear about the

263
00:16:45,100 --> 00:16:46,000
 practice.

264
00:16:46,000 --> 00:16:50,530
 I mean, mindfulness is just... The reason I think you don't

265
00:16:50,530 --> 00:16:58,000
 see people going crazy practicing mindfulness is because...

266
00:16:58,000 --> 00:17:01,000
 Because it's so...

267
00:17:01,000 --> 00:17:09,000
 Well, just because it's such a calming influence.

268
00:17:09,000 --> 00:17:14,000
 Mindfulness itself stops all that.

269
00:17:14,000 --> 00:17:17,070
 Anyway, it's with lapsed mindfulness. It happens with good

270
00:17:17,070 --> 00:17:20,000
 experiences. It happens with bad experiences.

271
00:17:20,000 --> 00:17:24,000
 If you're not careful to catch it, then keep going.

272
00:17:24,000 --> 00:17:32,740
 At least you'll get stuck at the worst. You can get caught

273
00:17:32,740 --> 00:17:39,000
 up and go temporarily AWOL.

274
00:17:39,000 --> 00:17:42,790
 And the fourth is, when I talked about impotent mindfulness

275
00:17:42,790 --> 00:17:46,800
, or probably what the Buddha would have explained as wrong

276
00:17:46,800 --> 00:17:48,000
 mindfulness.

277
00:17:48,000 --> 00:17:55,000
 And that is wrong mindfulness that is blocked by something.

278
00:17:55,000 --> 00:17:59,860
 So, one common example is with those people who have taken

279
00:17:59,860 --> 00:18:02,000
 the bodhisattva vows.

280
00:18:02,000 --> 00:18:06,160
 They've taken a vow to become a Buddha. They've taken a vow

281
00:18:06,160 --> 00:18:08,000
 to not become enlightened.

282
00:18:08,000 --> 00:18:18,000
 That's basically what it says. I vow not to enter into nibb

283
00:18:18,000 --> 00:18:18,000
ana until all beings are ready to become enlightened.

284
00:18:18,000 --> 00:18:23,480
 And that prevents them, of course, from... I mean, it

285
00:18:23,480 --> 00:18:26,000
 actually does. This is not a theoretical thing.

286
00:18:26,000 --> 00:18:30,320
 We've had meditators. We had one monk, another guy. I took

287
00:18:30,320 --> 00:18:34,000
 him to the mental hospital. I remember that night.

288
00:18:34,000 --> 00:18:39,320
 He tried to kill himself on several occasions. Because he

289
00:18:39,320 --> 00:18:43,000
 was on one hand trying to become a Buddha,

290
00:18:43,000 --> 00:18:47,230
 and on the other hand trying to practice insight meditation

291
00:18:47,230 --> 00:18:49,000
 to become an Arahant.

292
00:18:49,000 --> 00:18:53,470
 And he had other issues. It was probably fairly specific to

293
00:18:53,470 --> 00:19:03,000
 him, but he did eventually drive himself crazy.

294
00:19:03,000 --> 00:19:06,940
 But a more common one, I mean people who have a very strong

295
00:19:06,940 --> 00:19:10,000
 belief in self, or belief in God, or that kind of thing.

296
00:19:10,000 --> 00:19:13,830
 This prevents you from letting go and experiencing things

297
00:19:13,830 --> 00:19:15,000
 objectively.

298
00:19:15,000 --> 00:19:18,070
 If you have a sense that everything is going to work out,

299
00:19:18,070 --> 00:19:21,000
 or God has a plan for us, or something like that,

300
00:19:21,000 --> 00:19:25,260
 you can very much get in the way. I mean, this is wrong

301
00:19:25,260 --> 00:19:28,000
 view, according to Buddhism.

302
00:19:28,000 --> 00:19:31,860
 And I went through them. If you have wrong thought, if you

303
00:19:31,860 --> 00:19:34,000
're wishing ill of other people,

304
00:19:34,000 --> 00:19:40,420
 it obstructs your path, or if you have lots of desires or

305
00:19:40,420 --> 00:19:44,000
 ambitions, if you're arrogant, that kind of thing.

306
00:19:44,000 --> 00:19:49,410
 These will get in the way. If you have wrong speech, if you

307
00:19:49,410 --> 00:19:51,000
 say nasty things about people,

308
00:19:51,000 --> 00:19:55,410
 if you're a liar, or gossip, or that kind of thing, if you

309
00:19:55,410 --> 00:20:03,000
 just talk too much, that will get in your way.

310
00:20:03,000 --> 00:20:09,000
 Wrong action if you're a thief, a murderer, an adulterer.

311
00:20:09,000 --> 00:20:25,040
 If you have wrong livelihood, if you make money through

312
00:20:25,040 --> 00:20:28,000
 killing or stealing, that kind of thing.

313
00:20:28,000 --> 00:20:35,150
 If you have wrong effort, if you're lazy, or if you just

314
00:20:35,150 --> 00:20:40,000
 put your effort out to do bad things,

315
00:20:40,000 --> 00:20:52,420
 if you have wrong concentration, if you're unfocused, or if

316
00:20:52,420 --> 00:20:55,000
 you focus on the wrong things.

317
00:20:55,000 --> 00:21:01,000
 Mindfulness needs all of these other qualities.

318
00:21:01,000 --> 00:21:05,000
 Most especially things like right view and right action.

319
00:21:05,000 --> 00:21:10,380
 If you don't have morality and wisdom, concentration aspect

320
00:21:10,380 --> 00:21:19,000
 with mindfulness and so on can't develop.

321
00:21:19,000 --> 00:21:22,600
 For most people the consequence of wrong mindfulness is

322
00:21:22,600 --> 00:21:25,000
 simply they'll stop practicing.

323
00:21:25,000 --> 00:21:30,710
 They'll be discouraged and they'll reach an impasse where

324
00:21:30,710 --> 00:21:33,000
 they can't get past.

325
00:21:33,000 --> 00:21:38,690
 People practice, whether they practice properly or

326
00:21:38,690 --> 00:21:45,000
 improperly, without a solid training in mindfulness.

327
00:21:45,000 --> 00:21:51,370
 If you're a good teacher and a good mind that is able to

328
00:21:51,370 --> 00:21:55,740
 receive it, it's quite common for people to just give it up

329
00:21:55,740 --> 00:21:56,000
.

330
00:21:56,000 --> 00:22:00,520
 They get afraid or they get discouraged or they get dis

331
00:22:00,520 --> 00:22:04,000
interested because they don't get it.

332
00:22:04,000 --> 00:22:07,750
 But as I said, another consequence is that they continue

333
00:22:07,750 --> 00:22:11,790
 practicing but don't get anywhere and practice and they

334
00:22:11,790 --> 00:22:17,000
 just feel peaceful or calm, that kind of thing.

335
00:22:17,000 --> 00:22:24,610
 Without ever letting go, without ever becoming free from

336
00:22:24,610 --> 00:22:29,000
 ego or desire, that kind of thing.

337
00:22:29,000 --> 00:22:34,000
 And in extreme cases the consequences that people go crazy.

338
00:22:34,000 --> 00:22:37,030
 The crazy is usually temporary. I don't think I've ever

339
00:22:37,030 --> 00:22:39,680
 heard of meditation driving you crazy for the rest of your

340
00:22:39,680 --> 00:22:40,000
 life.

341
00:22:40,000 --> 00:22:44,970
 But it's temporary insanity. They get so wound up that they

342
00:22:44,970 --> 00:22:47,000
 find it hard to come back down.

343
00:22:47,000 --> 00:22:51,000
 And usually what happens is they get medicated.

344
00:22:51,000 --> 00:22:54,650
 Usually there's intervention because they start going crazy

345
00:22:54,650 --> 00:22:58,220
 and hitting people and running around tearing their clothes

346
00:22:58,220 --> 00:23:00,000
 off, that kind of thing.

347
00:23:00,000 --> 00:23:05,290
 So eventually they get institutionalized and medicated by

348
00:23:05,290 --> 00:23:09,000
 society and then they gradually calm down.

349
00:23:09,000 --> 00:23:14,710
 And maybe they go see a therapist and people start to think

350
00:23:14,710 --> 00:23:18,000
 Buddhism is maybe a bad thing.

351
00:23:18,000 --> 00:23:21,460
 You hear there are papers about this. Someone sent me a

352
00:23:21,460 --> 00:23:26,200
 paper about how mindfulness is practice or meditation

353
00:23:26,200 --> 00:23:28,000
 practice in general.

354
00:23:28,000 --> 00:23:32,300
 I didn't really read it because I don't know how interested

355
00:23:32,300 --> 00:23:34,000
 I am in what it says.

356
00:23:34,000 --> 00:23:37,880
 What I can say is I've never had it happen to any of my

357
00:23:37,880 --> 00:23:39,000
 students.

358
00:23:39,000 --> 00:23:46,000
 So cross your fingers. No. Quite confident it won't happen.

359
00:23:46,000 --> 00:23:51,000
 This technique is quite solid and concrete. Right?

360
00:23:51,000 --> 00:23:54,300
 It's hard to do it wrong. It's hard to have a wrong

361
00:23:54,300 --> 00:23:56,000
 understanding of how it works.

362
00:23:56,000 --> 00:24:00,000
 After a few days of me drilling it into you, exactly what

363
00:24:00,000 --> 00:24:05,000
 you have to do, it's pretty hard to get on the wrong path.

364
00:24:05,000 --> 00:24:08,000
 So it's not something anyone should worry about.

365
00:24:08,000 --> 00:24:10,380
 The only thing you have to worry about is if you're not

366
00:24:10,380 --> 00:24:13,000
 doing it. That's all.

367
00:24:13,000 --> 00:24:17,000
 So there you go. A little bit more done with today.

368
00:24:17,000 --> 00:24:21,000
 Hopefully it wasn't too much of a repetition.

369
00:24:21,000 --> 00:24:24,000
 Thank you all for coming out.

370
00:24:25,000 --> 00:24:27,000
 Thank you.

371
00:24:27,000 --> 00:24:29,000
 Thank you.

372
00:24:30,000 --> 00:24:32,000
 Thank you.

373
00:24:32,000 --> 00:24:34,000
 Thank you.

374
00:24:34,000 --> 00:25:01,000
 [silence]

375
00:25:01,000 --> 00:25:05,080
 And again, the meditation site is not loading so I'm not

376
00:25:05,080 --> 00:25:08,000
 going to answer questions tonight.

377
00:25:08,000 --> 00:25:11,000
 Thank you all for coming out.

378
00:25:13,000 --> 00:25:21,000
 [silence]

