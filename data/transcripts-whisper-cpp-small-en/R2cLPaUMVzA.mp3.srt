1
00:00:00,000 --> 00:00:05,440
 So in short, all I have to do is mindfully know everything

2
00:00:05,440 --> 00:00:07,440
 that arises in my mind, and

3
00:00:07,440 --> 00:00:08,680
 even if I don't know anything else about the Dhamma, I'd be

4
00:00:08,680 --> 00:00:12,220
 able to achieve Navāṇa.

5
00:00:12,220 --> 00:00:16,900
 So maybe I should not bother with anything else but

6
00:00:16,900 --> 00:00:18,520
 mindfulness.

7
00:00:18,520 --> 00:00:21,920
 Sounds good, no?

8
00:00:21,920 --> 00:00:25,360
 It could theoretically work.

9
00:00:25,360 --> 00:00:28,000
 The Buddha said, "Ek ayano yang bhikkhu e mango."

10
00:00:28,000 --> 00:00:34,200
 This is the one-way path or even the only path, depending

11
00:00:34,200 --> 00:00:37,360
 on how you interpret that.

12
00:00:37,360 --> 00:00:46,840
 But a simple answer is it only really works if you've got a

13
00:00:46,840 --> 00:00:51,400
 very strong will to do that.

14
00:00:51,400 --> 00:00:54,390
 And I would say therefore a very strong wholesomeness

15
00:00:54,390 --> 00:00:55,800
 already in your mind.

16
00:00:55,800 --> 00:01:00,560
 I would say you're already enlightened, let's say.

17
00:01:00,560 --> 00:01:05,200
 Or you have a teacher who can guide you through all the

18
00:01:05,200 --> 00:01:07,880
 mistakes that you're going to make,

19
00:01:07,880 --> 00:01:14,880
 all the times when you're actually not doing that.

20
00:01:14,880 --> 00:01:22,260
 But I would say for all intents and purposes, that's a

21
00:01:22,260 --> 00:01:25,400
 fairly good outlook.

22
00:01:25,400 --> 00:01:28,700
 And anything really wrong with the idea, if you fix that in

23
00:01:28,700 --> 00:01:30,400
 your mind that mindfulness

24
00:01:30,400 --> 00:01:37,880
 is really the key here, you won't go that far wrong.

25
00:01:37,880 --> 00:01:43,870
 It's just that while you're not at a state of good

26
00:01:43,870 --> 00:01:46,200
 mindfulness, you're going to make

27
00:01:46,200 --> 00:01:49,200
 lots of mistakes, right?

28
00:01:49,200 --> 00:01:53,480
 So because you're not yet there, you're still potentially

29
00:01:53,480 --> 00:01:55,320
 going to kill and steal and

30
00:01:55,320 --> 00:02:00,380
 lie and cheat and even more likely engage in things that

31
00:02:00,380 --> 00:02:03,040
 are going to lead you down the

32
00:02:03,040 --> 00:02:05,120
 wrong path.

33
00:02:05,120 --> 00:02:07,160
 You're going to misinterpret mindfulness.

34
00:02:07,160 --> 00:02:10,100
 You're going to not realize that you're not actually being

35
00:02:10,100 --> 00:02:10,760
 mindful.

36
00:02:10,760 --> 00:02:13,160
 Think that you're being mindful and you're actually forcing

37
00:02:13,160 --> 00:02:14,680
, you're actually being partial

38
00:02:14,680 --> 00:02:20,350
 and training the mind in a certain way and cultivating sank

39
00:02:20,350 --> 00:02:23,360
haras in the mind, artificial

40
00:02:23,360 --> 00:02:26,040
 constructs, artificial habits.

41
00:02:26,040 --> 00:02:29,100
 You're doing all sorts of unwholesome things because you're

42
00:02:29,100 --> 00:02:30,320
 not really mindful.

43
00:02:30,320 --> 00:02:32,480
 You don't really know how to be mindful.

44
00:02:32,480 --> 00:02:36,020
 You can only truly be mindful once you've cultivated, once

45
00:02:36,020 --> 00:02:37,680
 you understand, once you've

46
00:02:37,680 --> 00:02:44,160
 come to, once you've become practiced and become proficient

47
00:02:44,160 --> 00:02:45,120
 in it.

48
00:02:45,120 --> 00:02:49,390
 So until you get there, the question is how are you going

49
00:02:49,390 --> 00:02:51,480
 to get there on your own?

50
00:02:51,480 --> 00:02:55,200
 If you don't have a teacher, really the only way to do it

51
00:02:55,200 --> 00:02:57,040
 is to read a lot and to make

52
00:02:57,040 --> 00:03:03,970
 sure that you're not falling into any of the many pitfalls

53
00:03:03,970 --> 00:03:06,560
 that are discussed in the

54
00:03:06,560 --> 00:03:08,440
 tepidica.

55
00:03:08,440 --> 00:03:11,280
 But if you have a teacher, on the other hand, you just do

56
00:03:11,280 --> 00:03:13,000
 your mindfulness and they will

57
00:03:13,000 --> 00:03:14,000
 remind you.

58
00:03:14,000 --> 00:03:15,680
 They'll tell you, "Okay, you have to keep these rules."

59
00:03:15,680 --> 00:03:17,720
 So you keep those rules.

60
00:03:17,720 --> 00:03:19,360
 They'll tell you, "No, you have to stop this.

61
00:03:19,360 --> 00:03:20,360
 You have to stop that."

62
00:03:20,360 --> 00:03:21,880
 You're being mindful of this.

63
00:03:21,880 --> 00:03:24,680
 That's not really being mindful and so on and so on.

64
00:03:24,680 --> 00:03:26,700
 They'll be able to catch you and bring you back to being

65
00:03:26,700 --> 00:03:27,120
 mindful.

66
00:03:27,120 --> 00:03:29,960
 But absolutely, mindfulness is the key.

67
00:03:29,960 --> 00:03:33,580
 Mindfulness is kind of like the thread through the through

68
00:03:33,580 --> 00:03:35,120
 the through necklace.

69
00:03:35,120 --> 00:03:38,730
 So if the necklace is, the brief one is just morality,

70
00:03:38,730 --> 00:03:40,680
 concentration and wisdom.

71
00:03:40,680 --> 00:03:42,240
 Mindfulness is the thread that goes through them all.

72
00:03:42,240 --> 00:03:46,200
 It starts in the morality.

73
00:03:46,200 --> 00:03:48,920
 So true morality comes through mindfulness.

74
00:03:48,920 --> 00:03:52,040
 You have morality that it cultivates concentration.

75
00:03:52,040 --> 00:03:53,920
 Your mind starts to get focused.

76
00:03:53,920 --> 00:03:56,920
 But that comes through even more mindfulness.

77
00:03:56,920 --> 00:04:00,950
 And through being mindful in a concentrated state leads to

78
00:04:00,950 --> 00:04:01,800
 wisdom.

79
00:04:01,800 --> 00:04:05,120
 But mindfulness is what you use from beginning to end.

80
00:04:05,120 --> 00:04:08,120
 Really the most important one.

81
00:04:08,120 --> 00:04:14,290
 All the rest is just guidance to keep you on track when you

82
00:04:14,290 --> 00:04:16,760
 fall off, when you stop really

83
00:04:16,760 --> 00:04:17,480
 being mindful.

