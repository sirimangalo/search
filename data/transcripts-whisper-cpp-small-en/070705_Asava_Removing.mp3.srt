1
00:00:00,000 --> 00:00:02,760
 (birds chirping)

2
00:00:02,760 --> 00:00:11,800
 - Today I will continue the talks on the Asavat,

3
00:00:11,800 --> 00:00:18,760
 the defilements which cause the mind to go bad.

4
00:00:18,760 --> 00:00:26,320
 And today's topic is the subject,

5
00:00:26,320 --> 00:00:31,120
 on the subject of removing or discarding.

6
00:00:31,120 --> 00:00:32,720
 We know Tanabah Dapah.

7
00:00:32,720 --> 00:00:39,320
 Doing away with the defilements by discarding.

8
00:00:39,320 --> 00:00:49,400
 And this means discarding the unwholesome states of mind

9
00:00:49,400 --> 00:00:50,400
 as they arise.

10
00:00:50,400 --> 00:00:55,360
 Defilements in Buddhism there are

11
00:00:57,120 --> 00:00:59,120
 there are three kinds.

12
00:00:59,120 --> 00:01:02,040
 There are the Kamakilesa,

13
00:01:02,040 --> 00:01:07,040
 which are the actions which defile oneself.

14
00:01:07,040 --> 00:01:12,800
 Actions of killing or abusing, hurting other beings.

15
00:01:12,800 --> 00:01:19,360
 Actions of stealing or cheating

16
00:01:19,360 --> 00:01:22,100
 or actions of speech,

17
00:01:22,100 --> 00:01:25,040
 telling lies and so on.

18
00:01:27,040 --> 00:01:28,640
 These are the course defilements.

19
00:01:28,640 --> 00:01:33,640
 The moderate defilements are the ones

20
00:01:33,640 --> 00:01:34,680
 which exist in the mind.

21
00:01:34,680 --> 00:01:36,120
 These are the five hindrances.

22
00:01:36,120 --> 00:01:41,880
 So we have Kamachandha,

23
00:01:41,880 --> 00:01:42,880
 Payabhaada,

24
00:01:42,880 --> 00:01:46,920
 Dina-mita,

25
00:01:46,920 --> 00:01:49,920
 Uthaca-kukucha,

26
00:01:49,920 --> 00:01:50,920
 Amichika-cha,

27
00:01:50,920 --> 00:01:51,920
 Uchaha.

28
00:01:54,360 --> 00:01:56,800
 Which are sensual desire,

29
00:01:56,800 --> 00:01:58,240
 ill will,

30
00:01:58,240 --> 00:01:59,440
 sloth and torpor,

31
00:01:59,440 --> 00:02:02,800
 worry and restlessness and doubt.

32
00:02:02,800 --> 00:02:06,400
 In easy terms when we're practicing,

33
00:02:06,400 --> 00:02:08,720
 we use words which are easy to remember

34
00:02:08,720 --> 00:02:11,480
 and easy to put into practice.

35
00:02:11,480 --> 00:02:15,820
 Liking, disliking, drowsiness, distraction and doubt.

36
00:02:15,820 --> 00:02:19,200
 These are called the moderate defilements.

37
00:02:19,200 --> 00:02:21,480
 These are the defilements which are more subtle

38
00:02:21,480 --> 00:02:24,960
 than actions or speech,

39
00:02:24,960 --> 00:02:26,360
 bad actions or bad speech.

40
00:02:26,360 --> 00:02:28,120
 But they're still bad things

41
00:02:28,120 --> 00:02:29,680
 as they lead to suffering inside

42
00:02:29,680 --> 00:02:33,520
 and they can lead to action in the future.

43
00:02:33,520 --> 00:02:38,200
 Then there are the subtle defilements

44
00:02:38,200 --> 00:02:39,280
 which are the Ennusayat,

45
00:02:39,280 --> 00:02:43,360
 which are called latent

46
00:02:43,360 --> 00:02:46,080
 in the sense that they aren't something

47
00:02:46,080 --> 00:02:46,920
 which is arisen,

48
00:02:46,920 --> 00:02:50,240
 but they are that which causes

49
00:02:50,240 --> 00:02:54,120
 the defilements to arise.

50
00:02:54,120 --> 00:02:56,320
 So there's karmaraka,

51
00:02:56,320 --> 00:03:00,120
 patikat,

52
00:03:00,120 --> 00:03:03,800
 chitti-mana,

53
00:03:03,800 --> 00:03:04,800
 chitti-mana,

54
00:03:04,800 --> 00:03:07,240
 pavaraka,

55
00:03:07,240 --> 00:03:10,000
 avicha,

56
00:03:10,000 --> 00:03:14,040
 and uthacha.

57
00:03:14,040 --> 00:03:15,600
 So on there are seven.

58
00:03:15,600 --> 00:03:19,640
 These are the subtle defilements.

59
00:03:19,640 --> 00:03:22,720
 There's sensual lust

60
00:03:22,720 --> 00:03:28,040
 or the Anusayat which leads to sensual lust.

61
00:03:28,040 --> 00:03:32,840
 Patikat which means the tendency to dislike things.

62
00:03:32,840 --> 00:03:34,120
 These are opposites.

63
00:03:34,120 --> 00:03:35,840
 Some things we have,

64
00:03:35,840 --> 00:03:36,720
 we favor them.

65
00:03:36,720 --> 00:03:40,840
 Some things we have a tendency to dislike

66
00:03:40,840 --> 00:03:42,160
 by our nature.

67
00:03:42,160 --> 00:03:45,600
 This is an inherent character trait

68
00:03:45,600 --> 00:03:47,000
 which leads us to anger

69
00:03:47,000 --> 00:03:49,600
 and which leads us to greed.

70
00:03:49,840 --> 00:03:51,000
 There is wrong view

71
00:03:51,000 --> 00:03:52,800
 and there is conceit.

72
00:03:52,800 --> 00:03:56,760
 There is restlessness.

73
00:03:56,760 --> 00:03:58,720
 There is pavaraka,

74
00:03:58,720 --> 00:04:01,960
 desire to be.

75
00:04:01,960 --> 00:04:04,280
 The inclination to want to be this,

76
00:04:04,280 --> 00:04:06,200
 to want to be that.

77
00:04:06,200 --> 00:04:09,120
 And novicha which is ignorance.

78
00:04:09,120 --> 00:04:11,400
 These are the subtle defilements.

79
00:04:11,400 --> 00:04:12,960
 But here we're talking about those,

80
00:04:12,960 --> 00:04:14,080
 the moderate defilements,

81
00:04:14,080 --> 00:04:16,120
 the ones which exist in the mind.

82
00:04:16,120 --> 00:04:19,000
 And this is a very practical thing

83
00:04:19,000 --> 00:04:25,080
 that in meditation we are

84
00:04:25,080 --> 00:04:28,920
 practicing to remove the thoughts as they arise.

85
00:04:28,920 --> 00:04:31,360
 So when a thought of sensuality arises,

86
00:04:31,360 --> 00:04:33,600
 liking this or liking that,

87
00:04:33,600 --> 00:04:36,240
 wanting this or wanting that,

88
00:04:36,240 --> 00:04:40,360
 we don't allow it to build.

89
00:04:40,360 --> 00:04:43,320
 We don't allow it to

90
00:04:43,320 --> 00:04:45,800
 become more.

91
00:04:45,800 --> 00:04:50,200
 The Pali is we noteti.

92
00:04:50,200 --> 00:04:52,320
 We do away with it.

93
00:04:52,320 --> 00:04:54,640
 We noteti.

94
00:04:54,640 --> 00:04:56,960
 We remove it or dispel it right away.

95
00:04:56,960 --> 00:05:00,800
 We don't allow it to grow.

96
00:05:00,800 --> 00:05:05,080
 We send it off into nothingness.

97
00:05:05,080 --> 00:05:07,800
 We do away with it.

98
00:05:07,800 --> 00:05:09,800
 We cause for it not to be.

99
00:05:09,800 --> 00:05:13,400
 We cause it to drive it out of our minds.

100
00:05:13,400 --> 00:05:17,080
 The Pali phrase.

101
00:05:17,080 --> 00:05:21,160
 And this is accomplished

102
00:05:21,160 --> 00:05:22,880
 in terms of sensuality.

103
00:05:22,880 --> 00:05:27,080
 It's accomplished by focusing on the loathsome-ness

104
00:05:27,080 --> 00:05:30,720
 of the body or of the things around us.

105
00:05:30,720 --> 00:05:33,280
 So when we have lust, a very simple way to

106
00:05:33,280 --> 00:05:35,800
 do away with lust or desire is to

107
00:05:35,800 --> 00:05:38,160
 really examine the object

108
00:05:38,160 --> 00:05:41,310
 and come to see for ourselves whether it truly is desirable

109
00:05:41,310 --> 00:05:41,640
.

110
00:05:41,640 --> 00:05:43,960
 We can look at it and see where actually it's something

111
00:05:43,960 --> 00:05:46,520
 which is

112
00:05:46,520 --> 00:05:50,240
 organic and is

113
00:05:50,240 --> 00:05:52,280
 perhaps undesirable

114
00:05:52,280 --> 00:05:56,840
 in its real form. For instance, the human body.

115
00:05:56,840 --> 00:05:59,600
 We look at it as something desirable.

116
00:05:59,600 --> 00:06:02,400
 But if we consider carefully, we can see that this is only

117
00:06:02,400 --> 00:06:03,080
 because

118
00:06:03,080 --> 00:06:06,160
 we ourselves are human.

119
00:06:06,160 --> 00:06:09,920
 If we look at an animal body,

120
00:06:09,920 --> 00:06:12,840
 we don't find it at all attractive, but if

121
00:06:12,840 --> 00:06:17,600
 a certain animal looks at another animal's body of the same

122
00:06:17,600 --> 00:06:18,280
 type,

123
00:06:18,280 --> 00:06:21,190
 they will find it equally attractive. So when a dog looks

124
00:06:21,190 --> 00:06:22,000
 at a dog,

125
00:06:22,000 --> 00:06:25,080
 it seems attractive. When a pig looks at a pig,

126
00:06:25,080 --> 00:06:26,880
 the pig seems attractive.

127
00:06:26,880 --> 00:06:29,350
 And it's just so when the human looks at a human, the human

128
00:06:29,350 --> 00:06:30,040
 looks attractive.

129
00:06:30,040 --> 00:06:32,640
 But if we look at

130
00:06:32,640 --> 00:06:35,670
 these bodies from an objective point of view, they don't

131
00:06:35,670 --> 00:06:37,120
 seem really all that

132
00:06:37,120 --> 00:06:39,120
 attractive in the end.

133
00:06:39,120 --> 00:06:41,440
 They have

134
00:06:41,440 --> 00:06:44,680
 all sorts of liquids and fluids

135
00:06:44,680 --> 00:06:49,680
 pouring out and moving around and smells arising

136
00:06:49,680 --> 00:06:51,720
 falling apart.

137
00:06:51,720 --> 00:06:55,160
 Something like the Lord would have said, like

138
00:06:55,160 --> 00:06:58,440
 a very thin flimsy bag

139
00:06:58,440 --> 00:07:01,920
 with holes in it, full of rotten,

140
00:07:01,920 --> 00:07:03,920
 rotten,

141
00:07:03,920 --> 00:07:07,880
 disgusting, defiled things.

142
00:07:07,880 --> 00:07:10,880
 We just pull the bag out of a cesspool

143
00:07:10,880 --> 00:07:13,160
 full of filth,

144
00:07:13,160 --> 00:07:16,040
 full of excrement and so on, and the bag has holes in it

145
00:07:16,040 --> 00:07:17,640
 and we carry it around.

146
00:07:17,640 --> 00:07:21,040
 And so it's leaking all the time.

147
00:07:21,040 --> 00:07:26,640
 This is a very simple way to do away with sensual thoughts.

148
00:07:26,640 --> 00:07:30,240
 When we have thoughts of anger or ill will arise in the

149
00:07:30,240 --> 00:07:30,640
 mind,

150
00:07:30,640 --> 00:07:36,240
 the way to do away with them, a very simple way, is to

151
00:07:36,240 --> 00:07:39,080
 bring up thoughts of loving kindness.

152
00:07:39,080 --> 00:07:42,230
 So when we have anger towards someone or hatred towards

153
00:07:42,230 --> 00:07:43,120
 someone,

154
00:07:43,120 --> 00:07:46,550
 we can consider some of the good things, good qualities

155
00:07:46,550 --> 00:07:47,880
 about that person.

156
00:07:47,880 --> 00:07:52,000
 Perhaps they say bad things,

157
00:07:52,000 --> 00:07:53,800
 but they don't do bad things.

158
00:07:53,800 --> 00:07:56,680
 Or they do bad things, but they say good things.

159
00:07:56,680 --> 00:07:59,120
 Or they do and they say bad things, but

160
00:07:59,120 --> 00:08:03,200
 their minds are sometimes very pure.

161
00:08:03,200 --> 00:08:05,880
 We think about

162
00:08:05,880 --> 00:08:07,960
 some of the good things they have done, whereas

163
00:08:07,960 --> 00:08:10,880
 this they do bad things

164
00:08:10,880 --> 00:08:14,400
 in this way, but in this way they do good things.

165
00:08:14,400 --> 00:08:18,500
 They say bad things here, but other times they will say

166
00:08:18,500 --> 00:08:20,080
 good things.

167
00:08:20,080 --> 00:08:22,840
 Or we simply send out our love for them,

168
00:08:22,840 --> 00:08:25,620
 wishing that they be happy and be free from suffering and

169
00:08:25,620 --> 00:08:26,800
 realizing that

170
00:08:26,800 --> 00:08:29,920
 all beings are heirs of their karma.

171
00:08:29,920 --> 00:08:33,410
 When they do bad deeds, they will be the ones who get the

172
00:08:33,410 --> 00:08:35,000
 bad result.

173
00:08:35,000 --> 00:08:38,210
 And feeling sorry for them, wishing for them to be free

174
00:08:38,210 --> 00:08:39,120
 from suffering.

175
00:08:39,120 --> 00:08:43,430
 May they be able to leave behind their bad with their evil

176
00:08:43,430 --> 00:08:44,160
 ways.

177
00:08:44,160 --> 00:08:46,360
 This is a very simple way when we think about people in

178
00:08:46,360 --> 00:08:47,960
 this way, wishing for them happiness,

179
00:08:47,960 --> 00:08:50,840
 even though they may have done bad things for us.

180
00:08:50,840 --> 00:08:55,800
 A very simple way to overcome thoughts of anger.

181
00:08:55,800 --> 00:08:57,720
 But with thoughts of delusion,

182
00:08:57,720 --> 00:09:00,760
 and delusion here is very hard to

183
00:09:00,760 --> 00:09:03,160
 understand, very hard to pinpoint,

184
00:09:03,160 --> 00:09:07,480
 very hard to catch and realize when it exists in our mind.

185
00:09:07,480 --> 00:09:11,640
 It often takes someone else to point it out to us.

186
00:09:11,640 --> 00:09:15,080
 Delusion is things like wrong-few,

187
00:09:15,080 --> 00:09:17,870
 believing that bad deeds are good deeds, good deeds are bad

188
00:09:17,870 --> 00:09:18,160
 deeds,

189
00:09:18,160 --> 00:09:21,240
 or there's no difference between a good deed or a bad deed.

190
00:09:21,240 --> 00:09:23,520
 Bad deeds don't bring bad results,

191
00:09:23,520 --> 00:09:29,720
 good deeds don't bring good results, and so on.

192
00:09:29,720 --> 00:09:36,170
 You can see it, you can see it through our comparing or

193
00:09:36,170 --> 00:09:37,320
 judging.

194
00:09:37,320 --> 00:09:41,400
 For instance, we might hold on to a wrong-view,

195
00:09:41,400 --> 00:09:45,920
 we might hold on to the view that

196
00:09:45,920 --> 00:09:51,080
 killing is a good thing, or stealing is a good thing, or so

197
00:09:51,080 --> 00:09:51,080
 on.

198
00:09:51,080 --> 00:09:56,160
 And we might hold on to it as a view because of tradition.

199
00:09:56,160 --> 00:10:00,080
 We can look at religious views, and religious views often,

200
00:10:00,080 --> 00:10:03,170
 the only reason people hold on to them is because they are

201
00:10:03,170 --> 00:10:04,960
 our views.

202
00:10:04,960 --> 00:10:07,520
 We can see people saying,

203
00:10:07,520 --> 00:10:10,720
 "We are this religion," or "We are that religion,"

204
00:10:10,720 --> 00:10:13,920
 and "We do it this way, we do that."

205
00:10:13,920 --> 00:10:16,880
 And we can see how really this conceit is the only reason

206
00:10:16,880 --> 00:10:17,200
 often

207
00:10:17,200 --> 00:10:19,120
 why people hold on to their views.

208
00:10:19,120 --> 00:10:22,000
 There's nothing particular about their view which is

209
00:10:22,000 --> 00:10:24,480
 more wholesome or more beneficial than the view of other

210
00:10:24,480 --> 00:10:24,720
 people,

211
00:10:24,720 --> 00:10:27,520
 except for the fact that it is theirs,

212
00:10:27,520 --> 00:10:29,680
 and they hold on to it to be theirs.

213
00:10:29,680 --> 00:10:32,400
 This is mine.

214
00:10:32,400 --> 00:10:38,500
 And they compare and they judge based simply on their conce

215
00:10:38,500 --> 00:10:38,960
it,

216
00:10:38,960 --> 00:10:45,310
 their judging of different views and different ways of

217
00:10:45,310 --> 00:10:46,000
 being.

218
00:10:46,000 --> 00:10:48,320
 But we have conceit on a very simple level,

219
00:10:48,320 --> 00:10:52,400
 the conceit of comparing ourselves to other people,

220
00:10:52,400 --> 00:10:58,400
 meditators comparing their practice to others,

221
00:10:58,400 --> 00:11:02,320
 people in work or in society comparing themselves

222
00:11:02,320 --> 00:11:06,320
 in terms of their social status and so on.

223
00:11:06,320 --> 00:11:08,480
 Conceit, and then we have ignorance.

224
00:11:08,480 --> 00:11:14,320
 These things are all under the heading of delusion.

225
00:11:14,320 --> 00:11:16,080
 Ignorance is simply not knowing.

226
00:11:16,080 --> 00:11:18,800
 When we allow our ignorance to come up,

227
00:11:18,800 --> 00:11:24,320
 we say that we don't understand things,

228
00:11:24,320 --> 00:11:30,640
 or when we out of ignorance say something which is not true

229
00:11:30,640 --> 00:11:30,640
,

230
00:11:30,640 --> 00:11:34,800
 not knowing that it is false, or not knowing that it goes

231
00:11:34,800 --> 00:11:35,680
 against the truth,

232
00:11:35,680 --> 00:11:41,920
 saying that, for instance, that old age is not suffering,

233
00:11:41,920 --> 00:11:48,640
 or that there is no suffering in this world or so on.

234
00:11:48,640 --> 00:11:50,960
 And the method to overcome delusion, of course,

235
00:11:50,960 --> 00:11:53,680
 is in the same way to create the opposite.

236
00:11:53,680 --> 00:11:56,320
 So with greed and with anger, we have a way to do away with

237
00:11:56,320 --> 00:11:56,720
 them

238
00:11:56,720 --> 00:12:00,320
 on a temporary level, but because they are also based in

239
00:12:00,320 --> 00:12:01,280
 delusion,

240
00:12:01,280 --> 00:12:05,440
 due to ignorance, due to view and due to conceit,

241
00:12:05,440 --> 00:12:10,240
 we give rise to craving and give rise to aversion.

242
00:12:10,240 --> 00:12:12,790
 Because of that, because we haven't done away with the

243
00:12:12,790 --> 00:12:15,280
 delusion which underlies them,

244
00:12:15,280 --> 00:12:21,310
 we will always have the danger of further arising of greed

245
00:12:21,310 --> 00:12:23,040
 and of anger.

246
00:12:23,040 --> 00:12:28,720
 So the method to overcome the delusion

247
00:12:28,720 --> 00:12:30,240
 is also to create the opposite.

248
00:12:30,240 --> 00:12:34,640
 The opposite, of course, is insight or wisdom.

249
00:12:34,640 --> 00:12:37,280
 The opposite of ignorance is understanding.

250
00:12:37,280 --> 00:12:40,720
 The opposite of delusion is clarity or insight.

251
00:12:40,720 --> 00:12:46,360
 We pass on a clear seeing, or seeing through, or seeing

252
00:12:46,360 --> 00:12:51,040
 into, or seeing clearly.

253
00:12:51,040 --> 00:12:54,320
 And how this is brought about is simply by creating the

254
00:12:54,320 --> 00:12:55,360
 opposite thought.

255
00:12:55,360 --> 00:12:59,510
 So when there is ignorance or there is darkness in the mind

256
00:12:59,510 --> 00:12:59,520
,

257
00:12:59,520 --> 00:13:02,160
 we create light, we create clarity.

258
00:13:02,160 --> 00:13:06,080
 So when we say to ourselves, "Rising or falling,"

259
00:13:06,080 --> 00:13:08,320
 at the moment when the stomach is rising or falling,

260
00:13:08,320 --> 00:13:11,720
 instead of allowing for all sorts of views and conceits and

261
00:13:11,720 --> 00:13:13,040
 ignorance

262
00:13:13,040 --> 00:13:16,080
 and doubts and so on to arise in our mind,

263
00:13:16,080 --> 00:13:21,040
 which would then give rise to further distracted thoughts

264
00:13:21,040 --> 00:13:25,040
 and then further anger and greed and so on,

265
00:13:25,040 --> 00:13:28,480
 we create a clear thought, a thought which is clear,

266
00:13:28,480 --> 00:13:31,840
 which is devoid of delusion, which is devoid of darkness.

267
00:13:31,840 --> 00:13:34,240
 And this is rising.

268
00:13:34,240 --> 00:13:36,080
 It's a thought which arises in our mind

269
00:13:36,080 --> 00:13:41,280
 and replaces the distracted, unwholesome, errant thoughts,

270
00:13:41,280 --> 00:13:45,280
 thoughts which lead us down the wrong path.

271
00:13:45,280 --> 00:13:47,440
 And we find our thoughts not leading us anywhere.

272
00:13:47,440 --> 00:13:50,000
 Our thoughts are keeping us in the present moment,

273
00:13:50,000 --> 00:13:54,110
 allowing us to see clearer and clearer about the reality

274
00:13:54,110 --> 00:13:55,760
 around us.

275
00:13:55,760 --> 00:14:00,700
 And so this topic today is one of very great importance for

276
00:14:00,700 --> 00:14:01,440
 meditators,

277
00:14:01,440 --> 00:14:03,120
 and one that we should always keep in mind,

278
00:14:03,120 --> 00:14:06,480
 that we're on the lookout for as we practice rising,

279
00:14:06,480 --> 00:14:06,800
 falling,

280
00:14:06,800 --> 00:14:09,550
 as we're on the lookout for greed, we're on the lookout for

281
00:14:09,550 --> 00:14:09,920
 anger,

282
00:14:09,920 --> 00:14:12,960
 and most especially we're on the lookout for delusion.

283
00:14:12,960 --> 00:14:16,050
 If anger is overwhelming, we can take a break and send

284
00:14:16,050 --> 00:14:17,360
 loving-kindness.

285
00:14:17,360 --> 00:14:21,680
 If we feel angry or frustrated or bored or so on,

286
00:14:21,680 --> 00:14:24,160
 we can send love wishing ourselves to be happy,

287
00:14:24,160 --> 00:14:26,240
 wishing for all beings to be happy.

288
00:14:26,240 --> 00:14:29,680
 If we find ourselves practicing and are confronted by lust,

289
00:14:29,680 --> 00:14:34,270
 we can take a break and consider the repulsiveness of our

290
00:14:34,270 --> 00:14:34,640
 body

291
00:14:34,640 --> 00:14:38,990
 and the other bodies, maybe if we have lust towards someone

292
00:14:38,990 --> 00:14:39,760
 else's body,

293
00:14:39,760 --> 00:14:42,090
 we can say, "Well, actually, this is only because of our

294
00:14:42,090 --> 00:14:43,360
 condition,

295
00:14:43,360 --> 00:14:46,400
 the conditioning in the genes and the hormones which exist

296
00:14:46,400 --> 00:14:46,960
 in the body."

297
00:14:46,960 --> 00:14:51,930
 There's really no reason for that, and there's no guarantee

298
00:14:51,930 --> 00:14:52,240
,

299
00:14:52,240 --> 00:14:57,360
 or there is no possible way really that it could lead to

300
00:14:57,360 --> 00:14:57,680
 peace

301
00:14:57,680 --> 00:15:01,280
 and happiness and satisfaction if I chase after this thing,

302
00:15:01,280 --> 00:15:04,150
 this object of my attraction, because it is something which

303
00:15:04,150 --> 00:15:04,960
 has old age,

304
00:15:04,960 --> 00:15:09,470
 which has sickness, which has death as its final resting

305
00:15:09,470 --> 00:15:10,000
 place.

306
00:15:10,000 --> 00:15:14,880
 It is not something which is free from dissolution.

307
00:15:14,880 --> 00:15:19,520
 And so we look at things in this way, and these two med

308
00:15:19,520 --> 00:15:20,720
itations,

309
00:15:20,720 --> 00:15:24,160
 these sort of alternative meditations, they do away with

310
00:15:24,160 --> 00:15:25,520
 greed and anger temporarily

311
00:15:25,520 --> 00:15:27,600
 and allow us to continue the deeper practice,

312
00:15:27,600 --> 00:15:30,720
 which is the practice to do away with delusion.

313
00:15:30,720 --> 00:15:33,920
 And at the moment when we say rising, when we say falling,

314
00:15:33,920 --> 00:15:38,930
 we're creating the clear thought and replacing the deluded

315
00:15:38,930 --> 00:15:39,520
 thought,

316
00:15:39,520 --> 00:15:45,200
 the dark, the ignorant thought, which is giving rise ever

317
00:15:45,200 --> 00:15:45,680
 and again

318
00:15:45,680 --> 00:15:51,760
 to more greed and more anger, more frustration, more hatred

319
00:15:51,760 --> 00:15:51,760
,

320
00:15:51,760 --> 00:15:55,040
 and more lust and more desire, keeping us from being

321
00:15:55,040 --> 00:15:55,760
 content,

322
00:15:55,760 --> 00:15:58,480
 keeping us from being at peace.

323
00:15:58,480 --> 00:16:02,160
 When we're able to keep our minds and be simply content

324
00:16:02,160 --> 00:16:05,040
 with keeping our minds in the present moment,

325
00:16:05,040 --> 00:16:08,880
 this is when our life will truly have value and will truly

326
00:16:08,880 --> 00:16:14,320
 seem to be really and truly satisfying to us.

327
00:16:14,320 --> 00:16:17,450
 It is something which the meditators who come to practice

328
00:16:17,450 --> 00:16:18,240
 here can see for

329
00:16:18,240 --> 00:16:21,120
 themselves and are often able to remark for themselves

330
00:16:21,120 --> 00:16:26,560
 at how very real the results are of Vipassana meditation.

331
00:16:26,560 --> 00:16:29,760
 And at the moment when we are practicing, there seems to be

332
00:16:29,760 --> 00:16:30,560
 no goal

333
00:16:30,560 --> 00:16:38,880
 and there seems to be no external path.

334
00:16:38,880 --> 00:16:40,560
 That's simply all we're doing.

335
00:16:40,560 --> 00:16:43,040
 All we're simply doing is coming back to the present moment

336
00:16:43,040 --> 00:16:44,560
 and letting go

337
00:16:44,560 --> 00:16:48,400
 of our ambitions and our needs and our desires

338
00:16:48,400 --> 00:16:53,110
 and our views and coming back to nature, coming back to

339
00:16:53,110 --> 00:16:54,800
 reality.

340
00:16:54,800 --> 00:16:57,440
 And so this is a very important topic, something for us to

341
00:16:57,440 --> 00:16:58,400
 keep in mind when we

342
00:16:58,400 --> 00:17:02,640
 practice and to always be trying to bring our minds back to

343
00:17:02,640 --> 00:17:03,360
 the present moment,

344
00:17:03,360 --> 00:17:06,330
 not being content that we're practicing one hour, two hours

345
00:17:06,330 --> 00:17:07,040
, three hours,

346
00:17:07,040 --> 00:17:12,080
 how many hours a day, but how many moments are we clearly

347
00:17:12,080 --> 00:17:12,560
 aware of the

348
00:17:12,560 --> 00:17:15,600
 present moment. And when these moments gather together,

349
00:17:15,600 --> 00:17:19,440
 they will bring us to higher and higher clarity and

350
00:17:19,440 --> 00:17:19,600
 understanding

351
00:17:19,600 --> 00:17:23,600
 until finally we're able to break through the darkness

352
00:17:23,600 --> 00:17:28,810
 into the light and see clearly about the world around us at

353
00:17:28,810 --> 00:17:29,600
 all times.

354
00:17:29,600 --> 00:17:33,600
 This is the Lord Buddha's teaching on "Vinotana Pahata Pa"

355
00:17:33,600 --> 00:17:37,600
 to be doing away with or dispelling

356
00:17:37,600 --> 00:17:41,600
 the unwholesome thoughts which arise

357
00:17:41,600 --> 00:17:45,060
 as a means of doing away with the asava, as a means of

358
00:17:45,060 --> 00:17:45,600
 cleansing the mind

359
00:17:45,600 --> 00:17:49,600
 of its impurities, purifying the mind of

360
00:17:49,600 --> 00:17:53,930
 the things which make it go sour or go bad. And this is the

361
00:17:53,930 --> 00:17:55,600
 talk for today.

