1
00:00:00,000 --> 00:00:08,000
 Hello fellow YouTubers.

2
00:00:08,000 --> 00:00:12,080
 Today I'm here to give you an update on our DVD video

3
00:00:12,080 --> 00:00:15,000
 offering on how to meditate.

4
00:00:15,000 --> 00:00:19,930
 Now, in one of my earlier videos, I made it clear that we

5
00:00:19,930 --> 00:00:23,000
 had this DVD available to ship

6
00:00:23,000 --> 00:00:25,940
 to people around the world who are interested in learning

7
00:00:25,940 --> 00:00:27,000
 how to meditate.

8
00:00:27,000 --> 00:00:30,170
 And indeed, that's what we've been doing for the past year,

9
00:00:30,170 --> 00:00:33,000
 is mailing these out to people around the world

10
00:00:33,000 --> 00:00:37,000
 who are interested in our work.

11
00:00:37,000 --> 00:00:41,160
 And the DVD we've been offering for free, although we have

12
00:00:41,160 --> 00:00:46,000
 made it clear that we're required to recoup our costs,

13
00:00:46,000 --> 00:00:50,210
 the cost to production, which we estimate to be around $5

14
00:00:50,210 --> 00:00:51,000
 per disc.

15
00:00:51,000 --> 00:00:54,850
 Now, that's probably going to go down as we recover the

16
00:00:54,850 --> 00:00:58,000
 cost per se, the first 1,000 discs.

17
00:00:58,000 --> 00:01:02,640
 But we're still required to pay for the shipping for each

18
00:01:02,640 --> 00:01:03,000
 disc.

19
00:01:03,000 --> 00:01:06,770
 So if anyone would like to donate for the cost of the discs

20
00:01:06,770 --> 00:01:10,000
, you're certainly welcome to do so on our website.

21
00:01:10,000 --> 00:01:14,900
 But today I'm here to announce that not only are we giving

22
00:01:14,900 --> 00:01:17,000
 out the disc on how to meditate,

23
00:01:17,000 --> 00:01:20,740
 but we've made a second disc, which includes my videos on

24
00:01:20,740 --> 00:01:23,000
 why everyone should meditate.

25
00:01:23,000 --> 00:01:26,350
 So now we have a two-disc set, which we're shipping to

26
00:01:26,350 --> 00:01:28,000
 locations around the world

27
00:01:28,000 --> 00:01:31,000
 and giving out to people who come to visit us as well.

28
00:01:31,000 --> 00:01:37,240
 We have a great number of DVDs, which we've just printed,

29
00:01:37,240 --> 00:01:39,000
 and we'll be printing continuously

30
00:01:39,000 --> 00:01:41,840
 for as long as there is interest in our work and giving

31
00:01:41,840 --> 00:01:43,000
 them out for free.

32
00:01:43,000 --> 00:01:46,650
 So if you would like a free DVD or if you would like to

33
00:01:46,650 --> 00:01:49,000
 sponsor the donation process,

34
00:01:49,000 --> 00:01:52,890
 if you would like to become a donor and support our work

35
00:01:52,890 --> 00:01:54,000
 monetarily,

36
00:01:54,000 --> 00:01:58,220
 then you're welcome to do so either to request a DVD or to

37
00:01:58,220 --> 00:02:02,000
 support our work at our website on the Siri Moncolo page.

38
00:02:02,000 --> 00:02:06,000
 I'd like to thank everyone for your interest in our work,

39
00:02:06,000 --> 00:02:08,730
 and I appreciate all of the feedback and comments that

40
00:02:08,730 --> 00:02:10,000
 everyone's been giving,

41
00:02:10,000 --> 00:02:14,430
 and I look forward to spreading and helping to spread the

42
00:02:14,430 --> 00:02:16,000
 Buddha's teaching

43
00:02:16,000 --> 00:02:21,290
 and looking forward to getting people's help and people's

44
00:02:21,290 --> 00:02:23,000
 continued commitment in both the practice

45
00:02:23,000 --> 00:02:26,000
 and in supporting the practice of meditation.

46
00:02:26,000 --> 00:02:29,000
 So thank you all who have already supported our practice,

47
00:02:29,000 --> 00:02:33,000
 and also thank you for those who have sent us feedback,

48
00:02:33,000 --> 00:02:38,190
 who have received the DVD and have sent us feedback on how

49
00:02:38,190 --> 00:02:43,160
 well it was helping them to find peace, happiness, and

50
00:02:43,160 --> 00:02:44,000
 freedom from suffering.

51
00:02:44,000 --> 00:02:48,480
 So please don't be shy and feel free to send us a note

52
00:02:48,480 --> 00:02:50,000
 asking for a DVD.

53
00:02:50,000 --> 00:02:52,980
 Please include your mailing address. Otherwise, we have no

54
00:02:52,980 --> 00:02:54,000
 way to send it to you.

55
00:02:54,000 --> 00:02:58,000
 Thanks again. Thanks you all for tuning into our videos,

56
00:02:58,000 --> 00:03:00,660
 and look forward to hearing from you in the future. Have a

57
00:03:00,660 --> 00:03:02,000
 good day.

58
00:03:03,000 --> 00:03:18,080
 [

59
00:03:18,080 --> 00:03:29,080
 Music ]

