1
00:00:00,000 --> 00:00:04,000
 Hi, welcome back to Ask a Monk. Sorry I've been busy lately

2
00:00:04,000 --> 00:00:06,800
. I haven't had a chance to put any

3
00:00:06,800 --> 00:00:12,160
 Ask a Monk videos up. I've been trying to get ready to go

4
00:00:12,160 --> 00:00:15,680
 to Thailand where I'll be spending

5
00:00:15,680 --> 00:00:19,390
 the next two months leaving at the end of this month and I

6
00:00:19,390 --> 00:00:21,920
'll be there until around Christmas.

7
00:00:23,520 --> 00:00:31,120
 Anyway, next question comes from InsideIsVoid who asks, "

8
00:00:31,120 --> 00:00:34,080
When I get set to meditate, should I try to

9
00:00:34,080 --> 00:00:37,380
 seclude myself from things that might distract me, like

10
00:00:37,380 --> 00:00:39,440
 keeping pets out of the room or switching

11
00:00:39,440 --> 00:00:43,990
 off devices that make sounds? Or should I allow those

12
00:00:43,990 --> 00:00:47,360
 things in order to overcome them in meditation?"

13
00:00:47,360 --> 00:00:47,840
 Thanks.

14
00:00:51,600 --> 00:00:55,190
 I think there are cases where you want to avoid certain

15
00:00:55,190 --> 00:00:59,120
 things. Pets, I think, would be one of

16
00:00:59,120 --> 00:01:06,560
 them because of the conscious interaction and the

17
00:01:06,560 --> 00:01:14,560
 persistence of the phenomenon.

18
00:01:15,520 --> 00:01:21,230
 The real point here is that it's how comfortable you are

19
00:01:21,230 --> 00:01:23,360
 because in the end there should be no

20
00:01:23,360 --> 00:01:26,860
 difference between our formal practice and our practice in

21
00:01:26,860 --> 00:01:28,720
 daily life. Formal practice

22
00:01:28,720 --> 00:01:34,180
 is just that. It's formal. It's a way of formalizing the

23
00:01:34,180 --> 00:01:37,120
 act of being mindful.

24
00:01:38,640 --> 00:01:43,570
 The benefit of it is that it's easier, it's more intense,

25
00:01:43,570 --> 00:01:46,160
 it's like taking things out of the

26
00:01:46,160 --> 00:01:52,110
 environment so that you can do laboratory experiments and

27
00:01:52,110 --> 00:01:53,520
 the experiment in the laboratory

28
00:01:53,520 --> 00:01:58,690
 is much more pure. It's really the only way to train

29
00:01:58,690 --> 00:02:04,880
 yourself in the principles that we're

30
00:02:05,680 --> 00:02:08,820
 talking about here is to take yourself away from those

31
00:02:08,820 --> 00:02:14,160
 things. Now, that really only applies to

32
00:02:14,160 --> 00:02:18,220
 those things that are going to be a distraction in your

33
00:02:18,220 --> 00:02:21,760
 meditation. It sounds like a clock ticking

34
00:02:21,760 --> 00:02:29,540
 or air conditioning or even external sounds outside. Most

35
00:02:29,540 --> 00:02:33,200
 of the sounds that we have to deal

36
00:02:33,200 --> 00:02:36,320
 with cars going by or whatever don't need to be a

37
00:02:36,320 --> 00:02:38,720
 distraction. Obviously, they can become a part of

38
00:02:38,720 --> 00:02:42,280
 the meditation. It's where it gets to the point where it's

39
00:02:42,280 --> 00:02:44,560
 going to interrupt your meditation

40
00:02:44,560 --> 00:02:52,810
 and it's going to take you away from your stomach rising. A

41
00:02:52,810 --> 00:02:54,320
 cat crawling over you and

42
00:02:54,320 --> 00:02:58,010
 clawing you can be an object of meditation but the

43
00:02:58,010 --> 00:03:01,600
 intensity of it and the conscious interaction

44
00:03:01,600 --> 00:03:06,140
 will necessarily be a diversion. I think what you'll find

45
00:03:06,140 --> 00:03:08,800
 more is that when you try to avoid

46
00:03:08,800 --> 00:03:14,640
 certain phenomena that they actually distract you more in

47
00:03:14,640 --> 00:03:17,360
 life and it becomes an obsession and that

48
00:03:18,080 --> 00:03:21,950
 becomes something that blocks you in the practice. For

49
00:03:21,950 --> 00:03:24,880
 instance, people who try to take a clock out

50
00:03:24,880 --> 00:03:30,160
 of the room or remove certain sounds will find that they

51
00:03:30,160 --> 00:03:33,360
 then can never meditate with those sounds.

52
00:03:33,360 --> 00:03:37,210
 When I first started, everyone, we were always trying to

53
00:03:37,210 --> 00:03:39,600
 get the clock out of the room, put it

54
00:03:39,600 --> 00:03:43,570
 under a pillow or so on. I had an alarm clock someone gave

55
00:03:43,570 --> 00:03:45,120
 me and I was sticking it under

56
00:03:45,120 --> 00:03:47,230
 a pillow and then I could still hear it and there's a

57
00:03:47,230 --> 00:03:49,920
 blanket on top of that until I could no longer

58
00:03:49,920 --> 00:03:53,020
 hear it because I found that I was walking when I did my

59
00:03:53,020 --> 00:03:55,440
 walking meditation. I'd be walking in

60
00:03:55,440 --> 00:03:58,100
 sync with the clock all the time and when I did my sitting

61
00:03:58,100 --> 00:04:00,000
 meditation, I'd be breathing in sync

62
00:04:00,000 --> 00:04:04,690
 but then the problem came when I moved to another room with

63
00:04:04,690 --> 00:04:06,560
 a couple of other meditators and there

64
00:04:06,560 --> 00:04:10,810
 was a clock in the room and I had no opportunity to remove

65
00:04:10,810 --> 00:04:14,160
 it. It drove me crazy because it had

66
00:04:14,160 --> 00:04:17,350
 gotten worse because of my inability to accept it until

67
00:04:17,350 --> 00:04:20,560
 finally I just gave up and if I was walking

68
00:04:20,560 --> 00:04:23,070
 in sync with the clock, I was walking in sync with the

69
00:04:23,070 --> 00:04:25,360
 clock and eventually was able to overcome it

70
00:04:25,360 --> 00:04:28,890
 and that's obviously very important in the meditation

71
00:04:28,890 --> 00:04:31,920
 practice to overcome our attachments

72
00:04:31,920 --> 00:04:38,290
 as opposed to defeating them. Things like pets, people,

73
00:04:38,290 --> 00:04:41,440
 music, I would recommend all of these

74
00:04:41,440 --> 00:04:44,980
 things should be removed from your meditation practice. So

75
00:04:44,980 --> 00:04:46,960
 if there are people talking in the

76
00:04:46,960 --> 00:04:49,810
 room, that's incredibly difficult to meditate because you

77
00:04:49,810 --> 00:04:51,440
're obviously thinking about what

78
00:04:51,440 --> 00:04:54,100
 they're talking about. If there's music, that's quite

79
00:04:54,100 --> 00:04:55,760
 difficult to meditate because your mind

80
00:04:55,760 --> 00:05:04,880
 gets into this rhythm and it's kind of like it's a stimulus

81
00:05:04,880 --> 00:05:07,440
 and it keeps you happy, it keeps you calm,

82
00:05:08,080 --> 00:05:11,770
 it doesn't allow you to deal with the natural state of the

83
00:05:11,770 --> 00:05:15,280
 mind. Pets, obviously, as I said,

84
00:05:15,280 --> 00:05:20,660
 can be a distraction as well. So those are the few specific

85
00:05:20,660 --> 00:05:23,280
 things, the more extreme

86
00:05:23,280 --> 00:05:28,260
 sort of stimulants, but as far as noises and finding a

87
00:05:28,260 --> 00:05:30,880
 quiet place, I would recommend against

88
00:05:30,880 --> 00:05:34,080
 it. In fact, I would recommend trying to practice in places

89
00:05:34,080 --> 00:05:36,560
 where you wouldn't normally do. When I'm

90
00:05:36,560 --> 00:05:38,610
 in an airport, I'll be going to Thailand when I'm in the

91
00:05:38,610 --> 00:05:40,400
 airport, I'll do walking meditation and

92
00:05:40,400 --> 00:05:43,260
 sitting meditation, even with incredibly loud noise, even

93
00:05:43,260 --> 00:05:47,600
 with people talking and you'll be

94
00:05:47,600 --> 00:05:52,180
 vacuuming and vehicles going by or whatever because you can

95
00:05:52,180 --> 00:05:54,400
 pull these things into your

96
00:05:54,400 --> 00:06:00,450
 meditation practice. So in most cases, I would say, just

97
00:06:00,450 --> 00:06:03,680
 let the noise become a part of your

98
00:06:03,680 --> 00:06:08,020
 meditation as opposed to letting it build up the aversion

99
00:06:08,020 --> 00:06:10,720
 to it. Okay, so I hope that helps.

