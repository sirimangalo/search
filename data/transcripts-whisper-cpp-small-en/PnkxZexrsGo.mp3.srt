1
00:00:00,000 --> 00:00:04,520
 I have the same question as Brenna. Is it necessary for me

2
00:00:04,520 --> 00:00:08,000
 to sub-localize a description of the object I am aware of?

3
00:00:08,000 --> 00:00:11,630
 I am able to perceive objects without necessarily naming

4
00:00:11,630 --> 00:00:14,990
 them. Sometimes finding an English word to fit the thought

5
00:00:14,990 --> 00:00:16,000
 is distracting.

6
00:00:16,000 --> 00:00:21,880
 Again, this is only because you're untrained, because it's

7
00:00:21,880 --> 00:00:24,530
 new, because it's something that you're probably not very

8
00:00:24,530 --> 00:00:28,000
 good at, which is natural when you first start something.

9
00:00:28,000 --> 00:00:31,850
 Eventually you get better at recognizing things as they are

10
00:00:31,850 --> 00:00:36,450
, and really there's not that many experiences that after a

11
00:00:36,450 --> 00:00:41,730
 good many hours of doing this, you get pretty good at

12
00:00:41,730 --> 00:00:45,000
 finding names for just about everything.

13
00:00:45,000 --> 00:00:47,960
 I mean, it's not like there's millions of different words

14
00:00:47,960 --> 00:00:50,000
 that you could possibly have to use.

15
00:00:53,000 --> 00:00:57,960
 But we don't call it sub-vocalizing, we call it reminding

16
00:00:57,960 --> 00:01:03,000
 yourself, which is indeed how we understand the word sati,

17
00:01:03,000 --> 00:01:08,040
 which does literally mean to remember in the sense of re

18
00:01:08,040 --> 00:01:12,000
ifying the object.

19
00:01:12,000 --> 00:01:16,180
 Instead of going the next step of liking or disliking it,

20
00:01:16,180 --> 00:01:20,590
 instead of saying, "This is good, this is bad," you say, "

21
00:01:20,590 --> 00:01:22,000
This is this."

22
00:01:22,000 --> 00:01:24,190
 The mindfulness is actually reminding yourself. The

23
00:01:24,190 --> 00:01:27,030
 mindfulness of the Buddha is to remind yourself of the

24
00:01:27,030 --> 00:01:29,000
 Buddha, to remember the Buddha.

25
00:01:29,000 --> 00:01:31,570
 But you do that by saying to yourself, "Buddho, Buddho, Bh

26
00:01:31,570 --> 00:01:34,520
agavaya, Bhagavaya, Arahang, Arahang," it's all the

27
00:01:34,520 --> 00:01:37,220
 qualities of the Buddha that remind you of the Buddha, so

28
00:01:37,220 --> 00:01:42,740
 keep you focused on the idea of the Buddha with mindfulness

29
00:01:42,740 --> 00:01:47,120
 of the body, and do the same, but you remind yourself of

30
00:01:47,120 --> 00:01:48,000
 the body.

31
00:01:49,000 --> 00:01:52,370
 Otherwise, we don't consider it in this tradition to be

32
00:01:52,370 --> 00:01:56,160
 meditation. We consider it just sitting there zoning out,

33
00:01:56,160 --> 00:01:59,790
 which isn't kamathana, it isn't training, it isn't insight

34
00:01:59,790 --> 00:02:01,000
 meditation.

