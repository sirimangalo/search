1
00:00:00,000 --> 00:00:23,520
 [ Silence ]

2
00:00:23,520 --> 00:00:24,860
 >> Good evening everyone.

3
00:00:24,860 --> 00:00:30,080
 I'm not going to do a video broadcast this evening.

4
00:00:30,080 --> 00:00:40,960
 I'm having a midterm exam tomorrow on Mahayana Buddhism.

5
00:00:40,960 --> 00:00:44,840
 I have to talk about the Lotus Sutra.

6
00:00:47,060 --> 00:00:54,660
 I read an interesting class yesterday where I asked the

7
00:00:54,660 --> 00:00:59,140
 professor

8
00:00:59,140 --> 00:01:03,840
 where the Lotus Sutra situates itself,

9
00:01:03,840 --> 00:01:05,400
 where the Lotus Sutra recorded tradition.

10
00:01:05,400 --> 00:01:06,560
 When did it happen?

11
00:01:06,560 --> 00:01:12,540
 When was it taught according to tradition,

12
00:01:12,540 --> 00:01:19,000
 not according to history or scholasticism.

13
00:01:19,000 --> 00:01:21,380
 But the interesting part of his answer was he started

14
00:01:21,380 --> 00:01:21,720
 talking

15
00:01:21,720 --> 00:01:23,720
 about what is the Lotus Sutra.

16
00:01:23,720 --> 00:01:31,080
 So anyway, I don't know if it's not all that interesting.

17
00:01:31,080 --> 00:01:36,440
 Forget about it.

18
00:01:36,440 --> 00:01:43,440
 So tonight we have a quote about the five hindrances.

19
00:01:43,440 --> 00:01:48,120
 Let's talk about that instead.

20
00:01:48,120 --> 00:01:52,480
 We talk about the five hindrances often in our practice,

21
00:01:52,480 --> 00:01:56,720
 in the practice of Buddhist meditation.

22
00:01:56,720 --> 00:02:04,240
 And I think they're often a source of suffering for people

23
00:02:04,240 --> 00:02:09,320
 who feel bad that they still have these things,

24
00:02:09,320 --> 00:02:13,000
 feel guilty when they arise.

25
00:02:13,000 --> 00:02:16,360
 And that's not in and of itself wrong,

26
00:02:16,360 --> 00:02:20,200
 but it's not entirely beneficial.

27
00:02:20,200 --> 00:02:22,680
 It doesn't solve the problem to feel guilty

28
00:02:22,680 --> 00:02:25,280
 or to feel bad about these things.

29
00:02:25,280 --> 00:02:29,400
 Because they are wrong and they should be understood as

30
00:02:29,400 --> 00:02:31,160
 problems

31
00:02:31,160 --> 00:02:40,040
 or as, I mean, for a person who thinks there's something

32
00:02:40,040 --> 00:02:40,200
 good

33
00:02:40,200 --> 00:02:43,740
 to be had, there's some benefit to be had from these things

34
00:02:43,740 --> 00:02:43,840
,

35
00:02:43,840 --> 00:02:47,480
 that person is deluded.

36
00:02:47,480 --> 00:02:52,680
 A person who wants to be happy and yet actively engages

37
00:02:52,680 --> 00:03:02,320
 in cultivating the five hindrances is acting inconsistent

38
00:03:02,320 --> 00:03:07,680
 with their own desires, with their own intentions,

39
00:03:07,680 --> 00:03:10,000
 their own goals.

40
00:03:10,000 --> 00:03:13,000
 These things are intrinsically hindrances.

41
00:03:13,000 --> 00:03:20,360
 They have intrinsic hindering potential.

42
00:03:20,360 --> 00:03:21,880
 There's no ends, ifs or buts about it.

43
00:03:21,880 --> 00:03:27,760
 They're just not good things.

44
00:03:27,760 --> 00:03:33,680
 But I think this quote, it's quotes like this

45
00:03:33,680 --> 00:03:36,600
 that we should really look at and take at face value.

46
00:03:36,600 --> 00:03:40,160
 We shouldn't go the extra step and say, "Oh,

47
00:03:40,160 --> 00:03:44,360
 when I feel anger I should be upset at myself for that.

48
00:03:44,360 --> 00:03:46,760
 I should feel bad about that."

49
00:03:46,760 --> 00:03:50,120
 We should look at quotes like this and try to understand

50
00:03:50,120 --> 00:03:50,920
 them,

51
00:03:50,920 --> 00:04:00,640
 try to have this sort of inspiration arise in us,

52
00:04:00,640 --> 00:04:03,560
 to see that they weaken our wisdom, sense desires

53
00:04:03,560 --> 00:04:06,240
 and obstruction and hindrance which enshrouds the mind

54
00:04:06,240 --> 00:04:08,400
 and weakens wisdom.

55
00:04:08,400 --> 00:04:12,670
 Ill wills, sloth and torpor, restlessness and worry and

56
00:04:12,670 --> 00:04:13,520
 doubt,

57
00:04:13,520 --> 00:04:17,360
 obstructions and hindrances which enshroud the mind

58
00:04:17,360 --> 00:04:20,000
 and weaken wisdom.

59
00:04:20,000 --> 00:04:23,050
 Surely it is possible that after abandoning these obstruct

60
00:04:23,050 --> 00:04:23,280
ions

61
00:04:23,280 --> 00:04:27,040
 and hindrances which grow in and up over the mind

62
00:04:27,040 --> 00:04:30,800
 and weaken wisdom, being strong in wisdom,

63
00:04:30,800 --> 00:04:33,800
 one should know one's own good, the good of others,

64
00:04:33,800 --> 00:04:37,280
 the good of both, and attain that knowledge and vision

65
00:04:37,280 --> 00:04:40,640
 befitting noble ones and transcending human states.

66
00:04:40,640 --> 00:04:45,400
 And that's it.

67
00:04:45,400 --> 00:04:52,360
 I mean, it could easily lead you to freak out

68
00:04:52,360 --> 00:04:55,560
 because you realize you're on the wrong path

69
00:04:55,560 --> 00:04:59,280
 with all these hindrances inside.

70
00:04:59,280 --> 00:05:02,640
 That's not all that productive at all.

71
00:05:02,640 --> 00:05:06,240
 It's not the path, it's not the way to overcome them.

72
00:05:06,240 --> 00:05:13,640
 Once you understand this and once you see the problem

73
00:05:13,640 --> 00:05:14,200
 inherent

74
00:05:14,200 --> 00:05:17,560
 or even just see things as they are,

75
00:05:17,560 --> 00:05:20,360
 you don't have to judge ever really.

76
00:05:20,360 --> 00:05:23,120
 That's I think key in the practice.

77
00:05:23,120 --> 00:05:31,960
 That not only is it useless to judge bad things,

78
00:05:31,960 --> 00:05:34,040
 it's also not the way to get rid of them.

79
00:05:34,040 --> 00:05:54,280
 No, it's not only useless but the way

80
00:05:54,280 --> 00:06:01,830
 to be free from the hindrances is to just look, to see

81
00:06:01,830 --> 00:06:02,640
 clearly.

82
00:06:02,640 --> 00:06:08,080
 You don't even have to, in the end, realize they're bad.

83
00:06:08,080 --> 00:06:12,960
 It doesn't lead to any benefit.

84
00:06:12,960 --> 00:06:17,280
 It's not the end result.

85
00:06:17,280 --> 00:06:19,960
 We are just trying to see things as they are.

86
00:06:19,960 --> 00:06:22,200
 When you see things as they are, the hindrances

87
00:06:22,200 --> 00:06:25,040
 don't actually arise.

88
00:06:25,040 --> 00:06:33,600
 They arise based on misconception, misunderstanding,

89
00:06:33,600 --> 00:06:38,000
 misperception, perversion of perception.

90
00:06:38,000 --> 00:06:43,240
 If there's a beautiful thing in front of you

91
00:06:43,240 --> 00:06:49,040
 and you see it as it is, as seeing,

92
00:06:49,040 --> 00:06:50,720
 you see the reality of it.

93
00:06:50,720 --> 00:06:54,120
 There's no beauty in reality.

94
00:06:54,120 --> 00:07:01,560
 There's no appealing quality to reality.

95
00:07:01,560 --> 00:07:03,040
 There's no displacing quality either,

96
00:07:03,040 --> 00:07:05,840
 and there's no ugliness in nature either.

97
00:07:05,840 --> 00:07:08,040
 Those aren't intrinsic qualities of anything.

98
00:07:08,040 --> 00:07:17,920
 So when you see things, when you see clearly,

99
00:07:17,920 --> 00:07:21,880
 it's not arbitrary and it's not specific to Buddhism,

100
00:07:21,880 --> 00:07:24,640
 or it's not a belief-based thing,

101
00:07:24,640 --> 00:07:28,310
 then you see things as they are, the hindrances don't arise

102
00:07:28,310 --> 00:07:28,320
.

103
00:07:28,320 --> 00:07:29,760
 And that's what we should focus on.

104
00:07:29,760 --> 00:07:37,400
 Now, I guess another thing about this sort of quote

105
00:07:37,400 --> 00:07:43,880
 is it's a reflection, sort of a premeditative reflection,

106
00:07:43,880 --> 00:07:50,240
 because the quote is, "Surely it is possible.

107
00:07:50,240 --> 00:07:53,840
 Surely we can find a way."

108
00:07:53,840 --> 00:07:56,280
 So when you think like that, then it's

109
00:07:56,280 --> 00:07:59,880
 an impetus for you to practice meditation.

110
00:07:59,880 --> 00:08:05,000
 It's not actually meditation to bemoan the negative aspects

111
00:08:05,000 --> 00:08:07,480
 of these hindrances.

112
00:08:07,480 --> 00:08:09,600
 It is useful to see the problem with them,

113
00:08:09,600 --> 00:08:11,560
 to see how when you engage in anger,

114
00:08:11,560 --> 00:08:15,960
 or when you engage in greed, or when you engage in delusion

115
00:08:15,960 --> 00:08:15,960
,

116
00:08:15,960 --> 00:08:17,680
 it clouds your mind.

117
00:08:17,680 --> 00:08:21,120
 It makes you unable to see clearly.

118
00:08:21,120 --> 00:08:24,120
 But the great thing is you don't have to feel guilty.

119
00:08:24,120 --> 00:08:25,880
 The practice isn't feeling guilty about it.

120
00:08:25,880 --> 00:08:28,200
 You just have to, in that moment,

121
00:08:28,200 --> 00:08:32,760
 when you realize that you're angry, or greedy, or deluded,

122
00:08:32,760 --> 00:08:35,580
 you just have to say to yourself, liking, liking, disliking

123
00:08:35,580 --> 00:08:35,680
,

124
00:08:35,680 --> 00:08:40,160
 you should remind yourself what you're experiencing.

125
00:08:40,160 --> 00:08:42,480
 Clear your mind.

126
00:08:42,480 --> 00:08:45,640
 Be clear about what you're experiencing.

127
00:08:45,640 --> 00:08:51,360
 And there's no debate.

128
00:08:51,360 --> 00:08:52,800
 When you do that, your mind is clear.

129
00:08:52,800 --> 00:08:59,360
 And so the practice is quite--

130
00:08:59,360 --> 00:09:01,000
 it's right here in front of us.

131
00:09:01,000 --> 00:09:02,920
 It's not something you have to look to find,

132
00:09:02,920 --> 00:09:09,160
 or you have to learn over a course of years.

133
00:09:09,160 --> 00:09:11,360
 It's right here in front of you.

134
00:09:11,360 --> 00:09:12,760
 So it should never be discouraged

135
00:09:12,760 --> 00:09:15,040
 that we have defilements, or that our defilements

136
00:09:15,040 --> 00:09:20,920
 are too many, too thick, too entrenched.

137
00:09:20,920 --> 00:09:26,160
 You just have to be present here and now.

138
00:09:26,160 --> 00:09:28,360
 And you have to try to always be present,

139
00:09:28,360 --> 00:09:34,280
 and if you can do it often, and repeatedly,

140
00:09:34,280 --> 00:09:37,880
 it becomes habitual, and it changes who you are.

141
00:09:37,880 --> 00:09:40,400
 You come to see clearly.

142
00:09:40,400 --> 00:09:45,640
 The hindrance is weakened, and your life improves.

143
00:09:45,640 --> 00:09:46,640
 Your mind improves.

144
00:09:46,640 --> 00:09:48,840
 Your wisdom improves.

145
00:09:48,840 --> 00:09:51,360
 And eventually, you come to know--

146
00:09:51,360 --> 00:09:54,360
 slowly, you come to know in stages

147
00:09:54,360 --> 00:09:56,900
 what is to your own benefit, what is to the benefit of

148
00:09:56,900 --> 00:09:57,960
 others,

149
00:09:57,960 --> 00:10:01,920
 what is to the benefit of both, and you attain knowledge

150
00:10:01,920 --> 00:10:03,480
 and vision of the noble world.

151
00:10:03,480 --> 00:10:07,120
 OK.

152
00:10:07,120 --> 00:10:10,880
 So that's our Dhamma quote for today.

153
00:10:10,880 --> 00:10:14,320
 I'm going to hand off and do a little bit of studying

154
00:10:14,320 --> 00:10:17,600
 of the Lotus Sutra.

155
00:10:17,600 --> 00:10:19,560
 Have a good night, everyone.

