1
00:00:00,000 --> 00:00:03,900
 I understand desire is the root cause of evil but I still

2
00:00:03,900 --> 00:00:06,420
 keep on desiring and the guilt that I'm still

3
00:00:06,420 --> 00:00:10,120
 Desiring makes me desire even more. How do I stop?

4
00:00:10,120 --> 00:00:16,650
 It's true no the guilt will make you desire even more why

5
00:00:16,650 --> 00:00:20,680
 that happens though is interesting because guilt is

6
00:00:20,680 --> 00:00:21,640
 suffering and

7
00:00:21,640 --> 00:00:25,600
 Suffering leads you to want happiness

8
00:00:25,600 --> 00:00:29,200
 If you're happy you won't seek out happiness

9
00:00:30,240 --> 00:00:33,200
 If you're happy you won't have any reason to desire

10
00:00:33,200 --> 00:00:37,310
 So it's not the guilt that makes you desire more. It's the

11
00:00:37,310 --> 00:00:38,140
 suffering

12
00:00:38,140 --> 00:00:42,120
 You have to find happiness. That's the only answer

13
00:00:42,120 --> 00:00:45,920
 Your your meditation shouldn't be a

14
00:00:45,920 --> 00:00:49,280
 Stressful or a painful experience even though you might

15
00:00:49,280 --> 00:00:52,640
 experience great pain and great stress in the meditation

16
00:00:52,640 --> 00:00:56,360
 Your reaction to it should be the one that leads you to

17
00:00:56,360 --> 00:00:58,200
 find peace and happiness

18
00:00:58,880 --> 00:01:02,280
 to pacify to to quench to

19
00:01:02,280 --> 00:01:09,120
 Extinguish the fire of

20
00:01:09,120 --> 00:01:11,960
 desire and guilt

21
00:01:11,960 --> 00:01:14,650
 This is why I say when you look at the desire when when

22
00:01:14,650 --> 00:01:15,520
 desire comes up

23
00:01:15,520 --> 00:01:18,560
 You have to accept it and all the parts of it

24
00:01:18,560 --> 00:01:20,920
 especially the pleasure

25
00:01:20,920 --> 00:01:23,960
 Because once you reach the happiness, you don't need the

26
00:01:23,960 --> 00:01:24,880
 desire anymore

27
00:01:25,400 --> 00:01:28,220
 So if you focus on whatever happiness comes from desire,

28
00:01:28,220 --> 00:01:29,740
 which is the pleasure and so on

29
00:01:29,740 --> 00:01:33,810
 Then the desire will disappear if you just say to yourself

30
00:01:33,810 --> 00:01:36,120
 happy happy or feeling feeling

31
00:01:36,120 --> 00:01:39,280
 focusing on the

32
00:01:39,280 --> 00:01:44,790
 The actual experience then you find the desire will quite

33
00:01:44,790 --> 00:01:46,120
 quickly fade away

34
00:01:46,120 --> 00:01:50,720
 And when you when you have the suffering of the guilt

35
00:01:50,720 --> 00:01:53,830
 This is even more important because suffering is what makes

36
00:01:53,830 --> 00:01:56,830
 you strive after happiness and in as long as you have

37
00:01:56,830 --> 00:01:58,520
 suffering you'll always be

38
00:01:58,520 --> 00:02:03,310
 Striving to remove it and to bring about happiness pleasure

39
00:02:03,310 --> 00:02:04,480
 and happiness

40
00:02:04,480 --> 00:02:07,200
 So when there's guilt

41
00:02:07,200 --> 00:02:09,760
 It's actually suffering

42
00:02:09,760 --> 00:02:11,800
 focus on the suffering and and

43
00:02:11,800 --> 00:02:16,090
 React in such a way that you're okay with it that your mind

44
00:02:16,090 --> 00:02:19,680
 is no longer suffering as a result your mind's no longer

45
00:02:20,240 --> 00:02:23,670
 Reacting with suffering which is you know quite clearly to

46
00:02:23,670 --> 00:02:25,760
 be mindful when you say to yourself

47
00:02:25,760 --> 00:02:30,500
 Angry angry guilt feeling feeling or guilty guilty or

48
00:02:30,500 --> 00:02:34,800
 however you say it and when you clearly see the the

49
00:02:34,800 --> 00:02:35,320
 experience

50
00:02:35,320 --> 00:02:38,350
 You'll find your mind relaxing you'll find your mind

51
00:02:38,350 --> 00:02:40,580
 letting go and it has to be repeated

52
00:02:40,580 --> 00:02:43,670
 Has to be repetitive you have to continue to do this from

53
00:02:43,670 --> 00:02:47,280
 moment to moment because it'll come back you can't say okay

54
00:02:47,280 --> 00:02:50,220
 I did it and I solved it next moment. It's going to come

55
00:02:50,220 --> 00:02:52,760
 back and it'll be even even more persistent

56
00:02:52,760 --> 00:02:54,760
 So you have to be patient and?

57
00:02:54,760 --> 00:02:57,600
 And work on it

58
00:02:57,600 --> 00:02:59,600
 you

