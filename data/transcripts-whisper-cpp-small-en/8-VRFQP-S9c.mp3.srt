1
00:00:00,000 --> 00:00:05,760
 Hello, everyone.

2
00:00:05,760 --> 00:00:10,160
 Broadcasting live, getting ready to.

3
00:00:10,160 --> 00:00:12,480
 So see, on days where I'm doing the Dhammapada,

4
00:00:12,480 --> 00:00:16,200
 now I'm going to not broadcast that live,

5
00:00:16,200 --> 00:00:22,360
 because that will go up as its own YouTube video shortly.

6
00:00:22,360 --> 00:00:27,040
 So what we have now is just answering questions.

7
00:00:27,040 --> 00:00:30,780
 It's not going to be that long, because it's getting late

8
00:00:30,780 --> 00:00:32,040
 here.

9
00:00:32,040 --> 00:00:35,240
 I had two quizzes due today.

10
00:00:35,240 --> 00:00:48,440
 Tomorrow we have our day of mindfulness, if people come.

11
00:00:48,440 --> 00:00:54,880
 I think there's at least one man coming.

12
00:00:54,880 --> 00:01:03,000
 And another thing we're doing is Monday, Monday and Friday,

13
00:01:03,000 --> 00:01:09,560
 we're going to try and do a med mob, a McMaster, which

14
00:01:09,560 --> 00:01:13,240
 means we're just going to go and meditate in public

15
00:01:13,240 --> 00:01:16,760
 in the student center, which is like basically a cafeteria

16
00:01:16,760 --> 00:01:17,360
 area.

17
00:01:17,360 --> 00:01:18,360
 But it's more than that.

18
00:01:18,360 --> 00:01:19,360
 It's like the everything.

19
00:01:19,360 --> 00:01:23,160
 There's a Starbucks and Tim Hortons and restaurants

20
00:01:23,160 --> 00:01:27,000
 and the transportation hub where you buy your tickets

21
00:01:27,000 --> 00:01:31,000
 for public transportation.

22
00:01:31,000 --> 00:01:31,960
 But it's a big area.

23
00:01:31,960 --> 00:01:36,640
 And there are empty spaces where we could be meditating,

24
00:01:36,640 --> 00:01:39,160
 partly just because we don't have a space.

25
00:01:39,160 --> 00:01:44,920
 It's very difficult to book space at McMaster,

26
00:01:44,920 --> 00:01:51,840
 and partly to remind people to be public, to be present.

27
00:01:53,120 --> 00:01:55,400
 And to encourage that kind of thing and others.

28
00:01:55,400 --> 00:02:01,520
 So that's happening.

29
00:02:01,520 --> 00:02:03,400
 Do you have any questions?

30
00:02:03,400 --> 00:02:06,200
 We have questions.

31
00:02:06,200 --> 00:02:09,840
 How wide or narrow is the scope of the various experiences

32
00:02:09,840 --> 00:02:10,960
 we note?

33
00:02:10,960 --> 00:02:12,960
 For example, during walking, are we

34
00:02:12,960 --> 00:02:18,760
 noting at the foot sole, foot, leg, or whole body walking?

35
00:02:18,760 --> 00:02:22,200
 Or for breathing, is it one part or the whole abdomen?

36
00:02:22,200 --> 00:02:25,920
 Or maybe the skin rubbing against the clothes?

37
00:02:25,920 --> 00:02:27,640
 I think you might be overthinking it.

38
00:02:27,640 --> 00:02:29,200
 I wouldn't worry too much.

39
00:02:29,200 --> 00:02:31,480
 Stay with the foot when you walk, with the stomach when

40
00:02:31,480 --> 00:02:32,800
 you sit.

41
00:02:32,800 --> 00:02:35,520
 That might change.

42
00:02:35,520 --> 00:02:39,720
 Obsessing over it is probably a sign of the state of mind.

43
00:02:39,720 --> 00:02:42,440
 And then you should note that as well.

44
00:02:42,440 --> 00:02:46,120
 You shouldn't be worried about the specifics.

45
00:02:46,120 --> 00:02:50,400
 But to specify, we are focusing on the foot

46
00:02:50,400 --> 00:02:53,120
 when we walk and the abdomen when we sit.

47
00:02:53,120 --> 00:02:55,160
 Now, that experience might change.

48
00:02:55,160 --> 00:02:58,560
 It might sometimes just be the sole of the foot.

49
00:02:58,560 --> 00:03:00,760
 Sometimes just be the tip of the abdomen.

50
00:03:00,760 --> 00:03:04,440
 But you shouldn't.

51
00:03:04,440 --> 00:03:08,720
 The idea of purposefully trying for it to be one thing

52
00:03:08,720 --> 00:03:12,880
 or another is counter-productive.

53
00:03:12,880 --> 00:03:19,440
 [END PLAYBACK]

54
00:03:19,440 --> 00:03:22,360
 Where can the full commentary for the Dhammapada be found?

55
00:03:22,360 --> 00:03:28,840
 Burlingame.

56
00:03:28,840 --> 00:03:31,840
 You get it from Harvard.

57
00:03:31,840 --> 00:03:33,960
 Harvard-- well, you're talking about the translation,

58
00:03:33,960 --> 00:03:34,460
 I assume.

59
00:03:34,460 --> 00:03:37,040
 It's over 100 years old, I think.

60
00:03:37,040 --> 00:03:40,320
 But the only English translation--

61
00:03:40,320 --> 00:03:41,840
 you can actually get it on our website.

62
00:03:41,840 --> 00:03:45,720
 We have it in our PDF archive.

63
00:03:45,720 --> 00:03:49,440
 Someone sent it to me a while ago.

64
00:03:49,440 --> 00:03:51,200
 So if someone wants to post that link,

65
00:03:51,200 --> 00:03:53,160
 if someone knows where our PDF archive is.

66
00:03:53,160 --> 00:04:01,040
 I can post that after the broadcast, which might be soon

67
00:04:01,040 --> 00:04:04,240
 because that was the last question.

68
00:04:04,240 --> 00:04:04,740
 OK.

69
00:04:04,740 --> 00:04:08,520
 All right, then.

70
00:04:08,520 --> 00:04:09,440
 Thank you, everyone.

71
00:04:09,440 --> 00:04:11,240
 Have a good night.

72
00:04:11,240 --> 00:04:12,880
 See you all tomorrow.

73
00:04:12,880 --> 00:04:13,920
 Thank you, Bante.

74
00:04:13,920 --> 00:04:14,960
 Thanks, Robert.

75
00:04:14,960 --> 00:04:16,800
 Good night.

76
00:04:16,800 --> 00:04:19,760
 [END PLAYBACK]

