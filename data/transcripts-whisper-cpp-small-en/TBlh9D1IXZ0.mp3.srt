1
00:00:00,000 --> 00:00:04,820
 Can you talk about how to develop friendliness? It is very

2
00:00:04,820 --> 00:00:05,840
 hard for me to be

3
00:00:05,840 --> 00:00:12,040
 open-hearted. I always have a hard and unfriendly approach.

4
00:00:12,040 --> 00:00:24,760
 What do you guys think? I want to have... I want to develop

5
00:00:24,760 --> 00:00:25,880
 more friendliness? Is that

6
00:00:25,880 --> 00:00:31,560
 the question? Yeah. How to develop friendliness, Metta. It

7
00:00:31,560 --> 00:00:32,360
's very hard for me

8
00:00:32,360 --> 00:00:35,090
 to be open-hearted. I always have a hard and unfriendly

9
00:00:35,090 --> 00:00:37,040
 approach. Maybe Owen can

10
00:00:37,040 --> 00:00:40,120
 tell us what's going on in the Brahmapihara Damma Mahasi

11
00:00:40,120 --> 00:00:41,200
 Sayadaw.

12
00:00:41,200 --> 00:00:45,360
 What's his call on this?

13
00:00:45,360 --> 00:00:51,720
 Is Owen there? Oh, he left. He's here. Owen? I don't hear

14
00:00:51,720 --> 00:00:54,760
 Owen. It's because I've turned my microphone off and I

15
00:00:54,760 --> 00:00:54,960
 haven't

16
00:00:54,960 --> 00:00:59,110
 realized so much. I'm talking away, giving the answer, and

17
00:00:59,110 --> 00:01:00,720
 my microphone's turned off.

18
00:01:00,720 --> 00:01:06,520
 Well done. Is it a good end?

19
00:01:06,520 --> 00:01:12,950
 Yeah, well, maybe. Now, what I was saying is the best thing

20
00:01:12,950 --> 00:01:17,240
 to do would be to go and read the

21
00:01:17,240 --> 00:01:28,760
 discourse. I'm about over halfway through it, developing

22
00:01:28,760 --> 00:01:30,040
 such loving-kindness for

23
00:01:30,040 --> 00:01:37,670
 all beings, whether they're the closest people to you or

24
00:01:37,670 --> 00:01:38,600
 your worst

25
00:01:38,600 --> 00:01:47,120
 enemies. It's building up that general sense of loving-kind

26
00:01:47,120 --> 00:01:50,120
ness to all. From

27
00:01:50,120 --> 00:01:54,830
 reading the book, it states that you should start to

28
00:01:54,830 --> 00:01:57,080
 develop a meta towards

29
00:01:57,080 --> 00:02:03,640
 the people furthest away from you, as such. If you have

30
00:02:03,640 --> 00:02:05,080
 enemies or people you

31
00:02:05,080 --> 00:02:11,040
 dislike, you should start off by developing loving-kindness

32
00:02:11,040 --> 00:02:12,160
 towards those

33
00:02:12,160 --> 00:02:18,840
 kind of people, first of all, and then follow through a

34
00:02:18,840 --> 00:02:20,360
 process of trying to

35
00:02:20,360 --> 00:02:24,040
 develop for all others.

36
00:02:24,040 --> 00:02:27,720
 Are you sure it doesn't say, normally they say to go for

37
00:02:27,720 --> 00:02:29,400
 people you love first?

38
00:02:29,400 --> 00:02:32,650
 No, I was reading, I'm sure it said, because I was quite

39
00:02:32,650 --> 00:02:33,960
 surprised when it was saying that you

40
00:02:33,960 --> 00:02:39,770
 should develop loving-kindness for those who aren't as

41
00:02:39,770 --> 00:02:41,400
 closest to you, because you

42
00:02:41,400 --> 00:02:47,400
 naturally give loving-kindness to those very close to you,

43
00:02:47,400 --> 00:02:48,200
 as family, as such.

44
00:02:48,200 --> 00:02:52,960
 I know you're not supposed to develop it towards someone of

45
00:02:52,960 --> 00:02:54,520
 the opposite gender, if you're a

46
00:02:54,520 --> 00:02:57,560
 heterosexual person, because that can have problems.

47
00:02:57,560 --> 00:03:05,130
 But it's very good, and I definitely recommend, if you're

48
00:03:05,130 --> 00:03:06,200
 seriously trying to

49
00:03:06,200 --> 00:03:13,320
 develop that area in your life, to start reading up and

50
00:03:13,320 --> 00:03:17,160
 practicing the

51
00:03:17,160 --> 00:03:23,160
 techniques that are in there. Like you said earlier on,

52
00:03:23,160 --> 00:03:24,280
 once you start reading

53
00:03:24,280 --> 00:03:29,390
 a bit of a Mahasi Sayadaw book, you want to go away and

54
00:03:29,390 --> 00:03:31,720
 start either meditating or

55
00:03:31,720 --> 00:03:35,240
 practicing what he's teaching.

56
00:03:35,240 --> 00:03:45,000
 Actually, science is actually showing how the brain

57
00:03:45,000 --> 00:03:46,360
 actually rewires itself from

58
00:03:46,360 --> 00:03:51,000
 doing these practices. So the meta-meditation, compassion

59
00:03:51,000 --> 00:03:51,720
 meditations

60
00:03:51,720 --> 00:03:57,000
 actually help the brain. So if you're looking for, like you

61
00:03:57,000 --> 00:03:58,600
 asked to develop

62
00:03:58,600 --> 00:04:05,650
 friendliness, it's probably the best way to meditate,

63
00:04:05,650 --> 00:04:06,120
 compassion

64
00:04:06,120 --> 00:04:11,980
 meditations. You can actually rewire your transmitters in

65
00:04:11,980 --> 00:04:13,560
 your brain.

66
00:04:13,560 --> 00:04:16,860
 It takes some time to develop it. But the other thing is

67
00:04:16,860 --> 00:04:19,240
 not worry about it so much. Don't hate

68
00:04:19,240 --> 00:04:24,630
 yourself because you're hard and unfriendly. If you've got

69
00:04:24,630 --> 00:04:25,240
 anger, that's

70
00:04:25,240 --> 00:04:28,520
 the reason to develop meta. But some people are just hard

71
00:04:28,520 --> 00:04:29,240
 by nature and will

72
00:04:29,240 --> 00:04:36,740
 end up being very much alone because their very nature is

73
00:04:36,740 --> 00:04:39,640
 unfriendly, in a

74
00:04:39,640 --> 00:04:45,630
 non-emotional way. They have an abrupt way about them, and

75
00:04:45,630 --> 00:04:46,200
 that's just who they

76
00:04:46,200 --> 00:04:53,200
 are. We're all different. If you judge yourself and let

77
00:04:53,200 --> 00:04:54,680
 people's reactions

78
00:04:54,680 --> 00:05:01,810
 determine your actions, then you're just always going to be

79
00:05:01,810 --> 00:05:03,000
 disappointed and

80
00:05:03,000 --> 00:05:10,450
 upset. So there's nothing wrong with... it goes back to

81
00:05:10,450 --> 00:05:12,360
 talking about conflict in

82
00:05:12,360 --> 00:05:16,450
 relationships. Sometimes your behavior will cause conflict.

83
00:05:16,450 --> 00:05:17,080
 People will not like

84
00:05:17,080 --> 00:05:23,640
 your behavior even though you have no bad intentions in

85
00:05:23,640 --> 00:05:24,360
 your

86
00:05:24,360 --> 00:05:29,540
 heart. People will still be upset at what you do. That was

87
00:05:29,540 --> 00:05:30,600
 one big thing, one big

88
00:05:30,600 --> 00:05:34,640
 relief from the meditation. I don't really care if they don

89
00:05:34,640 --> 00:05:36,040
't like what I

90
00:05:36,040 --> 00:05:42,200
 do. There's no logical reason to be concerned if other

91
00:05:42,200 --> 00:05:43,080
 people get angry

92
00:05:43,080 --> 00:05:49,680
 because of the way I behave. You're not loving enough. You

93
00:05:49,680 --> 00:05:50,360
're not...

94
00:05:50,360 --> 00:05:54,470
 I had one monk yell at me, a very big monk. I did something

95
00:05:54,470 --> 00:05:56,040
 kind of wrong, but looking

96
00:05:56,040 --> 00:06:00,770
 back it was really just... I just expressed my opinion

97
00:06:00,770 --> 00:06:02,480
 about something that concerned

98
00:06:02,480 --> 00:06:06,990
 me in a very public way on my web blog and got in trouble

99
00:06:06,990 --> 00:06:09,040
 for it. So he called

100
00:06:09,040 --> 00:06:13,120
 me into his room and said, "You're a real troublemaker," or

101
00:06:13,120 --> 00:06:14,440
 something like that. He

102
00:06:14,440 --> 00:06:17,290
 said, "Meditation teachers have to have loving kindness..."

103
00:06:17,290 --> 00:06:17,880
 and he was yelling at

104
00:06:17,880 --> 00:06:20,500
 me, "Have to have loving kindness for people and have to be

105
00:06:20,500 --> 00:06:22,280
... have meta for

106
00:06:22,280 --> 00:06:29,910
 people." I'm looking at him. This is not the right lesson

107
00:06:29,910 --> 00:06:30,360
 on loving

108
00:06:30,360 --> 00:06:44,850
 kindness. But yeah, so it's not... not really necessary to

109
00:06:44,850 --> 00:06:46,520
 be open-hearted or

110
00:06:46,520 --> 00:06:50,080
 this or that. Remove the bad things from your mind and don

111
00:06:50,080 --> 00:06:50,840
't be concerned about

112
00:06:50,840 --> 00:06:55,790
 how people respond. People might hate you even though you

113
00:06:55,790 --> 00:06:58,880
're a nice person. It can

114
00:06:58,880 --> 00:07:06,530
 happen. Sometimes conflicts just happen. Misunderstandings

115
00:07:06,530 --> 00:07:07,360
 are so easy. It's

116
00:07:07,360 --> 00:07:11,100
 amazing how easy misunderstandings are. Some of the

117
00:07:11,100 --> 00:07:12,280
 misunderstandings I've seen.

118
00:07:12,280 --> 00:07:16,000
 Think about it. How you say something and you mean one

119
00:07:16,000 --> 00:07:18,280
 thing. Totally innocent and

120
00:07:18,280 --> 00:07:26,960
 how it just can destroy relationships. You didn't even mean

121
00:07:26,960 --> 00:07:29,720
 that. It's horrible in

122
00:07:29,720 --> 00:07:36,160
 Thailand because my Thais... I had to learn Thais, so

123
00:07:36,160 --> 00:07:36,960
 sometimes I would say

124
00:07:36,960 --> 00:07:41,080
 things and they would be interpreted totally the wrong way.

125
00:07:41,080 --> 00:07:41,600
 Because Thais is

126
00:07:41,600 --> 00:07:46,610
 very nuanced and when you say something, how you say it,

127
00:07:46,610 --> 00:07:48,440
 you know, the words you

128
00:07:48,440 --> 00:07:51,920
 use have incredible meaning. Simple words can have

129
00:07:51,920 --> 00:07:53,720
 incredible meaning because it's

130
00:07:53,720 --> 00:07:58,400
 all in the... not in the meaning of the words but in the

131
00:07:58,400 --> 00:08:00,400
 meaning that is implied

132
00:08:00,400 --> 00:08:05,400
 by them. Which, you know, as a Westerner, you just don't

133
00:08:05,400 --> 00:08:05,840
 have that

134
00:08:05,840 --> 00:08:09,440
 implication. You don't mean that. But it's taken very much

135
00:08:09,440 --> 00:08:11,080
 the wrong way. It can be

136
00:08:11,080 --> 00:08:13,770
 taken very much the wrong way. I had real headaches because

137
00:08:13,770 --> 00:08:18,000
 of that. But that's it.

138
00:08:18,000 --> 00:08:21,040
 You just learn that and all. That's the way. I mean, I

139
00:08:21,040 --> 00:08:23,600
 might wind up having an, in

140
00:08:23,600 --> 00:08:32,990
 fact, idea. For example, there was one case where someone

141
00:08:32,990 --> 00:08:35,400
... I had these

142
00:08:35,400 --> 00:08:40,780
 Buddha images, little Buddha images in my possession and

143
00:08:40,780 --> 00:08:42,920
 someone came... someone

144
00:08:42,920 --> 00:08:48,920
 told me and said, "Oh yes, make those into... into malas

145
00:08:48,920 --> 00:08:50,800
 and that's what they're

146
00:08:50,800 --> 00:08:53,560
 for. That's... shouldn't we ask the person who they belong

147
00:08:53,560 --> 00:08:54,960
 to?" That's what they're

148
00:08:54,960 --> 00:08:58,240
 for and because this was someone who was in a position of

149
00:08:58,240 --> 00:09:00,280
 authority. And so I did

150
00:09:00,280 --> 00:09:03,080
 this. I got them all prepared and I made them up and then I

151
00:09:03,080 --> 00:09:04,040
 got in real trouble

152
00:09:04,040 --> 00:09:06,380
 from the person who they belong to saying, you know, "Look,

153
00:09:06,380 --> 00:09:07,520
 he didn't even say it

154
00:09:07,520 --> 00:09:11,220
 because people will never... never confront you." But he

155
00:09:11,220 --> 00:09:11,960
 made it very clear that

156
00:09:11,960 --> 00:09:16,690
 basically I was stealing or using incorrectly. It's

157
00:09:16,690 --> 00:09:18,080
 something that I had no

158
00:09:18,080 --> 00:09:22,400
 permission to use. And I never told him that it was someone

159
00:09:22,400 --> 00:09:23,820
 else who had told me

160
00:09:23,820 --> 00:09:27,420
 to do this. That someone else in authority because I didn't

161
00:09:27,420 --> 00:09:28,160
 want to create

162
00:09:28,160 --> 00:09:31,380
 problems between those two people. I knew that if I wasn't

163
00:09:31,380 --> 00:09:32,440
 in trouble because of

164
00:09:32,440 --> 00:09:36,930
 it, this other person won't get in trouble because of it. I

165
00:09:36,930 --> 00:09:37,920
 only bring it up because

166
00:09:37,920 --> 00:09:41,240
 it's... it was a good example of where I... my relationship

167
00:09:41,240 --> 00:09:42,720
 with this... this other

168
00:09:42,720 --> 00:09:46,480
 person was strained because of something I had never done.

169
00:09:46,480 --> 00:09:47,120
 Because something I

170
00:09:47,120 --> 00:09:51,520
 wasn't even guilty of. And that's common in... in life. I

171
00:09:51,520 --> 00:09:52,280
 can think of a dozen

172
00:09:52,280 --> 00:09:55,600
 instances where that's happened to me. I know that even my

173
00:09:55,600 --> 00:09:56,600
 teacher at times...

174
00:09:56,600 --> 00:09:59,880
 there were times where my teacher got the wrong impression

175
00:09:59,880 --> 00:10:00,760
 people gave him

176
00:10:00,760 --> 00:10:03,320
 wrong information about me. Said, "I've done this or the

177
00:10:03,320 --> 00:10:04,880
 that." And then he... rather

178
00:10:04,880 --> 00:10:06,640
 than confront me about it because that's the thing in

179
00:10:06,640 --> 00:10:08,560
 Thailand. They don't say, "Why

180
00:10:08,560 --> 00:10:10,900
 did you do this? Why did you do that?" He just said, "Don't

181
00:10:10,900 --> 00:10:13,680
... don't do X. Don't go and do

182
00:10:13,680 --> 00:10:16,990
 this." And I said, "Okay." And I knew what he was relating

183
00:10:16,990 --> 00:10:18,040
 to. I knew that someone had

184
00:10:18,040 --> 00:10:21,070
 accused me of doing something that I hadn't done. And I

185
00:10:21,070 --> 00:10:21,880
 just said, "I won't do

186
00:10:21,880 --> 00:10:26,490
 it." And that was that. So potentially if my teacher hadn't

187
00:10:26,490 --> 00:10:27,120
 been the wonderful

188
00:10:27,120 --> 00:10:33,000
 person that he was, he could have been very much turned off

189
00:10:33,000 --> 00:10:34,320
 or upset about that.

190
00:10:34,320 --> 00:10:38,260
 That just happens in life. So I just wanted to point out

191
00:10:38,260 --> 00:10:39,760
 that you don't have

192
00:10:39,760 --> 00:10:45,430
 to endear yourself to people. You don't have to worry about

193
00:10:45,430 --> 00:10:47,000
 your behavior. Worry

194
00:10:47,000 --> 00:10:52,060
 about your... your intentions. Some people are very nice

195
00:10:52,060 --> 00:10:53,880
 and very kind to people and

196
00:10:53,880 --> 00:10:58,360
 actually miserable inside or terrible people inside. Very

197
00:10:58,360 --> 00:10:59,040
 good at winning

198
00:10:59,040 --> 00:11:05,440
 friends. You know, the four kinds of "mita pati rupa" or "

199
00:11:05,440 --> 00:11:07,680
pati rupa mita." Fake

200
00:11:07,680 --> 00:11:14,400
 friends. I don't know. It wasn't even quite what you were

201
00:11:14,400 --> 00:11:16,320
 asking but certainly the answer is

202
00:11:16,320 --> 00:11:18,740
 correct. Develop loving-kindness, that's the best way. I

203
00:11:18,740 --> 00:11:20,520
 just wanted to say, don't

204
00:11:20,520 --> 00:11:26,630
 worry about it so much. As long as you have "vipassana,"

205
00:11:26,630 --> 00:11:29,080
 people might hate you.

206
00:11:29,080 --> 00:11:34,230
 Everyone in the monastery, not everyone, many people, it

207
00:11:34,230 --> 00:11:35,600
 seemed like the whole

208
00:11:35,600 --> 00:11:38,520
 monastic community was upset with me, except my teacher.

209
00:11:38,520 --> 00:11:40,040
 And so I was like, "Well,

210
00:11:40,040 --> 00:11:42,180
 that's fine. The only one I care about is my teacher." And

211
00:11:42,180 --> 00:11:42,880
 that's how a lot of

212
00:11:42,880 --> 00:11:45,960
 people are in my monastery. They fight and they have lots

213
00:11:45,960 --> 00:11:46,960
 of problems with each

214
00:11:46,960 --> 00:11:51,000
 other but they don't care because they're only there for

215
00:11:51,000 --> 00:11:52,360
 one person. And so that's

216
00:11:52,360 --> 00:11:57,170
 kind of how the place gets along. But it was interesting to

217
00:11:57,170 --> 00:11:58,320
 feel so much hatred

218
00:11:58,320 --> 00:12:02,560
 from people. Like really vicious, mean looks and everything

219
00:12:02,560 --> 00:12:03,320
. And I was so happy

220
00:12:03,320 --> 00:12:07,030
 because I was with my teacher. I was like, "This is the...

221
00:12:07,030 --> 00:12:08,640
 I wouldn't have

222
00:12:08,640 --> 00:12:14,480
 traded it for anything." Which is an interesting situation.

223
00:12:15,840 --> 00:12:23,160
 Sometimes just getting a wish for the other person to be

224
00:12:23,160 --> 00:12:23,600
 mad at you,

225
00:12:23,600 --> 00:12:30,840
 whatever, and just a mental, "I hope that person gets more

226
00:12:30,840 --> 00:12:32,040
 compassionate for

227
00:12:32,040 --> 00:12:37,780
 themselves," kind of eases off the reaction to what they're

228
00:12:37,780 --> 00:12:38,640
 giving to you.

229
00:12:38,640 --> 00:12:44,280
 Yeah, loving-kindness meditation can certainly change...

230
00:12:44,280 --> 00:12:49,780
 If you have to really deal with them, your intentions are

231
00:12:49,780 --> 00:12:50,840
...

232
00:12:50,840 --> 00:12:54,280
 You can just let it be.

