1
00:00:00,000 --> 00:00:06,480
 Please give an overview on the state of the stream winner.

2
00:00:06,480 --> 00:00:18,400
 Stream winner. The word is Sota, Sotaapana. Sotaapana. Sota

3
00:00:18,400 --> 00:00:21,280
 means stream and

4
00:00:21,280 --> 00:00:29,090
 Apana means attained. So it means one who has attained the

5
00:00:29,090 --> 00:00:32,400
 stream. The word Sotaapana

6
00:00:32,400 --> 00:00:36,980
 generally as used in the suttas refers to one specific

7
00:00:36,980 --> 00:00:39,000
 thing. In the

8
00:00:39,000 --> 00:00:45,320
 commentaries there is actually another type of Sotaapana

9
00:00:45,320 --> 00:00:45,960
 and it the

10
00:00:45,960 --> 00:00:52,480
 commentary edition helps us to understand the the Suta

11
00:00:52,480 --> 00:00:53,600
 definition as

12
00:00:53,600 --> 00:00:56,250
 well because it helps us understand the way that word is

13
00:00:56,250 --> 00:00:57,240
 used in different

14
00:00:57,240 --> 00:01:00,280
 contexts. In the suttas it's only used in one way. It's

15
00:01:00,280 --> 00:01:01,280
 used to refer to the

16
00:01:01,280 --> 00:01:08,510
 person who has had a realization of the Four Noble Truths

17
00:01:08,510 --> 00:01:11,160
 has come to realize

18
00:01:11,160 --> 00:01:16,440
 Nibbana for the first time. They have entered into a kind

19
00:01:16,440 --> 00:01:16,600
 of a

20
00:01:16,600 --> 00:01:22,280
 cessation which we call Palasamapati where they have

21
00:01:22,280 --> 00:01:24,360
 realized that all

22
00:01:24,360 --> 00:01:27,870
 things cease because they've seen the cessation of all

23
00:01:27,870 --> 00:01:29,160
 phenomena without

24
00:01:29,160 --> 00:01:34,550
 remainder. They've come to regard all phenomena that arise

25
00:01:34,550 --> 00:01:36,240
 as impermanent,

26
00:01:36,240 --> 00:01:43,110
 unsatisfying and uncontrollable. That realization is only a

27
00:01:43,110 --> 00:01:44,520
 temporary

28
00:01:44,520 --> 00:01:48,530
 realization. They may still later on cling to things but

29
00:01:48,530 --> 00:01:49,480
 because they've had

30
00:01:49,480 --> 00:01:52,530
 that realization at the moment of that realization the mind

31
00:01:52,530 --> 00:01:53,560
 goes out, the mind

32
00:01:53,560 --> 00:01:58,620
 ceases and there is this experience of cessation. It can

33
00:01:58,620 --> 00:01:59,560
 last for a brief

34
00:01:59,560 --> 00:02:02,560
 moment, it can last for a minute, it can last for an hour,

35
00:02:02,560 --> 00:02:05,000
 it can be cultivated to

36
00:02:05,000 --> 00:02:09,940
 last, cultivated in some sense of determining to enter into

37
00:02:09,940 --> 00:02:10,920
 it for

38
00:02:10,920 --> 00:02:14,620
 up to seven days according to the texts. I had one student

39
00:02:14,620 --> 00:02:15,840
 who claimed to be able

40
00:02:15,840 --> 00:02:20,880
 to enter into it for 24 hours. That's what she said believe

41
00:02:20,880 --> 00:02:24,000
 it or not but she

42
00:02:24,000 --> 00:02:26,430
 certainly did seem to enter into it quite frequently. We'd

43
00:02:26,430 --> 00:02:29,160
 be in the car and

44
00:02:29,160 --> 00:02:34,680
 once we were in one of these red trucks that they have in

45
00:02:34,680 --> 00:02:35,600
 Chiang Mai and

46
00:02:35,600 --> 00:02:41,290
 we're sitting there and she's rocking in the bumps and she

47
00:02:41,290 --> 00:02:41,760
's like hitting

48
00:02:41,760 --> 00:02:45,540
 her head off the side of the truck and not feeling it and

49
00:02:45,540 --> 00:02:47,080
 not being

50
00:02:47,080 --> 00:02:50,420
 jolted at all and as soon as the truck stopped she opened

51
00:02:50,420 --> 00:02:52,120
 her eyes. Something

52
00:02:52,120 --> 00:02:54,490
 like that or else it was, I can't remember what it was, it

53
00:02:54,490 --> 00:02:55,760
 was like we're all trying

54
00:02:55,760 --> 00:02:58,350
 to get out of the car of the truck and she's still sitting

55
00:02:58,350 --> 00:02:59,080
 there. There was

56
00:02:59,080 --> 00:03:04,760
 another time where we were sitting in chanting and we do

57
00:03:04,760 --> 00:03:05,360
 chanting in the

58
00:03:05,360 --> 00:03:07,380
 evening and we finished chanting, we all got up and she was

59
00:03:07,380 --> 00:03:08,160
 still sitting there

60
00:03:08,160 --> 00:03:11,840
 because in the middle of chanting we do sitting meditation.

61
00:03:11,840 --> 00:03:12,360
 After sitting

62
00:03:12,360 --> 00:03:14,500
 meditation she didn't stop, she didn't do the rest of the

63
00:03:14,500 --> 00:03:15,560
 chanting, we all got up

64
00:03:15,560 --> 00:03:19,300
 and I sat there from my room looking through the hallway at

65
00:03:19,300 --> 00:03:19,960
 her sitting in

66
00:03:19,960 --> 00:03:23,080
 the hall for a while and after about five minutes she got

67
00:03:23,080 --> 00:03:23,360
 up, she opened her

68
00:03:23,360 --> 00:03:31,200
 eyes, got up and walked away. So this experience for the

69
00:03:31,200 --> 00:03:31,560
 first time

70
00:03:31,560 --> 00:03:34,890
 where the meditator enters into cessation and is able to

71
00:03:34,890 --> 00:03:35,760
 see the difference

72
00:03:35,760 --> 00:03:44,320
 between what one's thought was happiness and this cessation

73
00:03:44,320 --> 00:03:45,040
 of suffering and

74
00:03:45,040 --> 00:03:51,280
 realizes that all things that arise are unsatisfying, are

75
00:03:51,280 --> 00:03:52,080
 not happiness,

76
00:03:52,080 --> 00:03:57,360
 are not beneficial. One loses one's interest in them, loses

77
00:03:57,360 --> 00:03:58,280
 the attachment to

78
00:03:58,280 --> 00:04:04,010
 them. The first realization of nimvana doesn't free one

79
00:04:04,010 --> 00:04:05,440
 from attachment to

80
00:04:05,440 --> 00:04:08,030
 things. One will become less attached to things through

81
00:04:08,030 --> 00:04:09,000
 this realization because

82
00:04:09,000 --> 00:04:12,090
 one now has something to compare it to. Before the only

83
00:04:12,090 --> 00:04:13,240
 thing one could think of

84
00:04:13,240 --> 00:04:16,820
 as happy were arisen experiences. So maybe ice cream is

85
00:04:16,820 --> 00:04:18,160
 happiness or chocolate

86
00:04:18,160 --> 00:04:22,160
 is happiness or sex is happiness or drugs are happiness or

87
00:04:22,160 --> 00:04:23,000
 heaven is

88
00:04:23,000 --> 00:04:26,380
 happiness or so on. This is what one thought was happiness

89
00:04:26,380 --> 00:04:27,040
 so one chases

90
00:04:27,040 --> 00:04:30,140
 after them and finds no satisfaction because everything is

91
00:04:30,140 --> 00:04:30,880
 changing and

92
00:04:30,880 --> 00:04:33,650
 everything is impermanent and finds that all one is gaining

93
00:04:33,650 --> 00:04:34,680
 through this is more

94
00:04:34,680 --> 00:04:38,800
 and more greed, more and more attachment, more and more

95
00:04:38,800 --> 00:04:40,120
 addiction. And when one

96
00:04:40,120 --> 00:04:45,030
 realizes when one realizes nimvana for the first time one

97
00:04:45,030 --> 00:04:45,480
 has realized

98
00:04:45,480 --> 00:04:48,980
 something that is is totally free from any attachment, has

99
00:04:48,980 --> 00:04:50,000
 no building up of

100
00:04:50,000 --> 00:04:56,040
 craving, has no stress, no agitation of mind, it's pure

101
00:04:56,040 --> 00:04:58,080
 peace. The next moment

102
00:04:58,080 --> 00:05:02,520
 when one arises from it, this first experience, is the

103
00:05:02,520 --> 00:05:03,680
 happiest moment of

104
00:05:03,680 --> 00:05:09,300
 the person's life, of one's career in samsara really.

105
00:05:09,300 --> 00:05:10,040
 Because before that

106
00:05:10,040 --> 00:05:13,970
 one had never in all of the rounds of samsara never come in

107
00:05:13,970 --> 00:05:14,560
 contact with the

108
00:05:14,560 --> 00:05:18,250
 Buddha's teaching, never come gotten to this point where

109
00:05:18,250 --> 00:05:19,080
 one practiced

110
00:05:19,080 --> 00:05:24,660
 intensively this teaching and had never before experienced

111
00:05:24,660 --> 00:05:26,680
 the state where

112
00:05:26,680 --> 00:05:29,070
 all things disappear where there's no seeing, no hearing,

113
00:05:29,070 --> 00:05:29,880
 no smelling, no

114
00:05:29,880 --> 00:05:34,980
 tasting, no feeling, no thinking. So regardless of how you

115
00:05:34,980 --> 00:05:35,640
 know from people

116
00:05:35,640 --> 00:05:39,720
 who hadn't realized that this would sound more terrifying

117
00:05:39,720 --> 00:05:41,440
 or horrible or

118
00:05:41,440 --> 00:05:45,760
 totally undesirable, the cessation of seeing, hearing,

119
00:05:45,760 --> 00:05:46,600
 smelling, tasting,

120
00:05:46,600 --> 00:05:51,220
 feeling and thinking, because it's devoid of all of those

121
00:05:51,220 --> 00:05:51,760
 things which

122
00:05:51,760 --> 00:05:55,870
 one thought would be a cause of happiness. When the sotap

123
00:05:55,870 --> 00:05:58,000
ana arises from the

124
00:05:58,000 --> 00:06:04,450
 attainment of sotapana, of sotapati-manga, sotapati-pala,

125
00:06:04,450 --> 00:06:06,360
 they have

126
00:06:06,360 --> 00:06:11,680
 changed this opinion about reality. And they have changed

127
00:06:11,680 --> 00:06:13,080
 the idea that

128
00:06:13,080 --> 00:06:16,590
 anything might be permanent or anything might be lasting.

129
00:06:16,590 --> 00:06:18,400
 They lose three things

130
00:06:18,400 --> 00:06:21,350
 as a result of this, as a result of seeing or changing

131
00:06:21,350 --> 00:06:22,600
 their idea of what is

132
00:06:22,600 --> 00:06:25,620
 happiness and realizing the true peace and true happiness

133
00:06:25,620 --> 00:06:26,560
 which leaves them

134
00:06:26,560 --> 00:06:29,940
 with a clear and pure mind. A person who has entered into

135
00:06:29,940 --> 00:06:31,600
 it can be in a state of

136
00:06:31,600 --> 00:06:39,030
 bliss for days, just on cloud nine for a great period of

137
00:06:39,030 --> 00:06:40,320
 time because of the

138
00:06:40,320 --> 00:06:44,030
 change in their existence. Talking to some people who seem

139
00:06:44,030 --> 00:06:44,640
 to have gotten

140
00:06:44,640 --> 00:06:48,730
 this experience, of course we're in no position to judge,

141
00:06:48,730 --> 00:06:50,000
 who their whole

142
00:06:50,000 --> 00:06:53,990
 character changes and they're just in complete bliss and

143
00:06:53,990 --> 00:06:55,520
 contentment for a

144
00:06:55,520 --> 00:06:58,420
 great number of days until the defilements that are left

145
00:06:58,420 --> 00:06:59,000
 start coming

146
00:06:59,000 --> 00:07:10,140
 back and start affecting their mind again. But at the

147
00:07:10,140 --> 00:07:10,840
 moment of coming

148
00:07:10,840 --> 00:07:13,280
 back, they are free of three things. They may still have

149
00:07:13,280 --> 00:07:14,140
 defilements and the

150
00:07:14,140 --> 00:07:17,720
 defilements will slowly come back but they know something

151
00:07:17,720 --> 00:07:18,220
 now. They know

152
00:07:18,220 --> 00:07:22,960
 something new that is irreversible. You could think of it

153
00:07:22,960 --> 00:07:23,560
 like you have this

154
00:07:23,560 --> 00:07:29,750
 dam and as long as the dam is fully intact, you know that

155
00:07:29,750 --> 00:07:30,200
 it could last

156
00:07:30,200 --> 00:07:33,470
 for a great period of time. But as soon as you see the

157
00:07:33,470 --> 00:07:34,600
 first crack in the

158
00:07:34,600 --> 00:07:38,600
 dam, this dam can never be fixed and eventually the dam is

159
00:07:38,600 --> 00:07:38,800
 going to

160
00:07:38,800 --> 00:07:41,310
 break and the water is going to be released. In the same

161
00:07:41,310 --> 00:07:42,240
 way the sotapana

162
00:07:42,240 --> 00:07:46,600
 has entered into the stream. This is what the meaning of

163
00:07:46,600 --> 00:07:46,820
 the word

164
00:07:46,820 --> 00:07:50,340
 sota is. Sota means they've changed something about

165
00:07:50,340 --> 00:07:51,320
 themselves and they

166
00:07:51,320 --> 00:07:55,840
 have entered into a non-static state. This state is not

167
00:07:55,840 --> 00:07:57,080
 something that is

168
00:07:57,080 --> 00:08:02,130
 going to stay still. They're headed upstream, you might say

169
00:08:02,130 --> 00:08:02,480
, or they're

170
00:08:02,480 --> 00:08:06,630
 headed in the direction of nibbana. It's kind of like the

171
00:08:06,630 --> 00:08:08,040
 dam is cracked and

172
00:08:08,040 --> 00:08:12,790
 it's only a matter of time before it bursts loose. So they

173
00:08:12,790 --> 00:08:13,400
 lose three things.

174
00:08:13,400 --> 00:08:18,600
 They lose wrong view of self, sakayaditi. They lose any

175
00:08:18,600 --> 00:08:19,120
 idea that

176
00:08:19,120 --> 00:08:24,000
 there is a permanent soul because this is the profound

177
00:08:24,000 --> 00:08:24,560
 thing about the

178
00:08:24,560 --> 00:08:27,790
 cessation of all experience. You realize that all that

179
00:08:27,790 --> 00:08:28,960
 exists is experience.

180
00:08:28,960 --> 00:08:32,650
 Without experience there's nothing. There is nibbana, there

181
00:08:32,650 --> 00:08:34,760
 is the cessation,

182
00:08:34,760 --> 00:08:44,840
 which we call nibbana. And so as a result, this idea of a

183
00:08:44,840 --> 00:08:46,640
 self has no

184
00:08:46,640 --> 00:08:52,300
 place, has no meaning for them anymore. They lose the

185
00:08:52,300 --> 00:08:53,840
 attachment to rights and

186
00:08:53,840 --> 00:09:00,240
 rituals or practice that is useless. They lose the idea

187
00:09:00,240 --> 00:09:01,240
 that practice that is

188
00:09:01,240 --> 00:09:04,760
 unbeneficial or of no benefit in leading towards the

189
00:09:04,760 --> 00:09:06,120
 freedom from suffering,

190
00:09:06,120 --> 00:09:09,200
 thinking that it is a benefit. They now know what is the

191
00:09:09,200 --> 00:09:10,280
 right path. So

192
00:09:10,280 --> 00:09:13,440
 they're able to see that what is useless is useless. They

193
00:09:13,440 --> 00:09:14,400
 can see that doing

194
00:09:14,400 --> 00:09:18,140
 rights and rituals and performing ceremonies, that it's not

195
00:09:18,140 --> 00:09:18,800
 the way to

196
00:09:18,800 --> 00:09:23,240
 nibbana. It doesn't mean that they will stop and refuse to

197
00:09:23,240 --> 00:09:23,840
 perform these

198
00:09:23,840 --> 00:09:26,560
 things, but when they perform them they'll know that this

199
00:09:26,560 --> 00:09:27,200
 is meaningless

200
00:09:27,200 --> 00:09:31,260
 or it's only meaningful because of the state of mind of the

201
00:09:31,260 --> 00:09:32,360
 person. They come to

202
00:09:32,360 --> 00:09:34,620
 see that it's the state of the mind and one's intention and

203
00:09:34,620 --> 00:09:35,440
 one's clarity of

204
00:09:35,440 --> 00:09:38,060
 mind that's most important. So when they do chanting they

205
00:09:38,060 --> 00:09:40,080
'll try to use it as an

206
00:09:40,080 --> 00:09:43,290
 opportunity to reflect on the wholesome qualities of the

207
00:09:43,290 --> 00:09:45,720
 Buddha or so on. Or as

208
00:09:45,720 --> 00:09:48,460
 they progress they'll use the chanting as a time to be

209
00:09:48,460 --> 00:09:49,680
 aware of their lips

210
00:09:49,680 --> 00:09:55,190
 moving or of the feelings in the body and so on. They won't

211
00:09:55,190 --> 00:09:55,960
 have the idea that

212
00:09:55,960 --> 00:09:58,440
 chanting in and of itself has some power that's going to

213
00:09:58,440 --> 00:09:59,600
 lead them to enlightenment

214
00:09:59,600 --> 00:10:03,280
 or freedom from suffering and so on. They don't have the

215
00:10:03,280 --> 00:10:04,840
 idea that keeping this

216
00:10:04,840 --> 00:10:07,940
 rule or that rule is important. They don't have the idea

217
00:10:07,940 --> 00:10:09,040
 that, for example,

218
00:10:09,040 --> 00:10:12,050
 not touching money, for example, is necessary to become

219
00:10:12,050 --> 00:10:13,400
 enlightened so they

220
00:10:13,400 --> 00:10:15,800
 don't hold on to the precepts as tightly. If they break a

221
00:10:15,800 --> 00:10:16,720
 precept they're not

222
00:10:16,720 --> 00:10:20,810
 worried or feeling guilty about it. They just, you know,

223
00:10:20,810 --> 00:10:21,560
 reprimand themselves and

224
00:10:21,560 --> 00:10:25,160
 say that that was wrong and they make an effort to not do

225
00:10:25,160 --> 00:10:25,880
 it again and they

226
00:10:25,880 --> 00:10:28,400
 confess it, but they don't feel upset about it. They don't

227
00:10:28,400 --> 00:10:29,520
 think that, "Oh no, now

228
00:10:29,520 --> 00:10:32,200
 I'm going to go to hell because it's a concept, it's a

229
00:10:32,200 --> 00:10:34,260
 convention," and so

230
00:10:34,260 --> 00:10:42,000
 they don't cling to to rules as well or morality. They

231
00:10:42,000 --> 00:10:42,000
 cling, they

232
00:10:42,000 --> 00:10:44,120
 understand it in terms of the mind state, so they

233
00:10:44,120 --> 00:10:45,760
 understand that killing is always

234
00:10:45,760 --> 00:10:48,040
 wrong or stealing is always wrong, but they understand that

235
00:10:48,040 --> 00:10:49,160
 certain things are

236
00:10:49,160 --> 00:10:53,040
 only concepts, not eating after 12 o'clock. If they eat in

237
00:10:53,040 --> 00:10:53,800
 the afternoon as

238
00:10:53,800 --> 00:10:56,510
 a monk, for example, they won't feel guilty but they'll

239
00:10:56,510 --> 00:10:57,000
 maybe say to

240
00:10:57,000 --> 00:10:59,250
 themselves, "You've got to be more mindful," or so on, and

241
00:10:59,250 --> 00:11:00,720
 they will make a note and

242
00:11:00,720 --> 00:11:04,480
 try to correct their behavior. That's it. The third thing

243
00:11:04,480 --> 00:11:06,720
 is they lose doubt.

244
00:11:06,720 --> 00:11:08,820
 They lose doubt about the Buddha, doubt about the Buddha's

245
00:11:08,820 --> 00:11:10,000
 teaching, and doubt

246
00:11:10,000 --> 00:11:14,720
 about what makes a person an enlightened disciple of the

247
00:11:14,720 --> 00:11:15,800
 Buddha. They

248
00:11:15,800 --> 00:11:18,750
 understand what are the meaning of these three things and

249
00:11:18,750 --> 00:11:18,880
 they

250
00:11:18,880 --> 00:11:22,270
 understand that what the Buddha taught is the truth because

251
00:11:22,270 --> 00:11:23,380
 they've realized it

252
00:11:23,380 --> 00:11:25,760
 for themselves.

253
00:11:25,760 --> 00:11:36,080
 That's what the sutta is called, being a sotapane. Just one

254
00:11:36,080 --> 00:11:36,440
 more thing,

255
00:11:36,440 --> 00:11:40,440
 because what the Buddhaghosa mentions, this other kind of

256
00:11:40,440 --> 00:11:42,160
 stream, is

257
00:11:42,160 --> 00:11:45,980
 called a chula sotapan, sotapane. A chula sotapane is

258
00:11:45,980 --> 00:11:47,000
 someone who realizes the

259
00:11:47,000 --> 00:11:49,930
 second stage of knowledge. Altogether, there are 16 stages

260
00:11:49,930 --> 00:11:50,960
 of knowledge. When a

261
00:11:50,960 --> 00:11:53,180
 person realizes the second stage of knowledge, it means

262
00:11:53,180 --> 00:11:53,520
 they have an

263
00:11:53,520 --> 00:11:57,310
 understanding of karma. How this works is in the beginning

264
00:11:57,310 --> 00:11:58,540
 they see the body and

265
00:11:58,540 --> 00:12:01,860
 the mind functioning. They're able to see the stomach

266
00:12:01,860 --> 00:12:03,020
 rising and that's a

267
00:12:03,020 --> 00:12:05,920
 phenomenon of body, and they see the mind that goes out to

268
00:12:05,920 --> 00:12:06,980
 the stomach and is

269
00:12:06,980 --> 00:12:09,830
 aware of the stomach rising. So only when there is the

270
00:12:09,830 --> 00:12:11,380
 stomach rising and the mind

271
00:12:11,380 --> 00:12:14,500
 aware of it is there the experience of the stomach rising.

272
00:12:14,500 --> 00:12:15,280
 If the mind is

273
00:12:15,280 --> 00:12:17,930
 somewhere else or not thinking about the stomach, there is

274
00:12:17,930 --> 00:12:19,080
 also no experience. One

275
00:12:19,080 --> 00:12:22,230
 sees that there's the body in the mind. That's the first

276
00:12:22,230 --> 00:12:23,220
 stage of knowledge.

277
00:12:23,220 --> 00:12:26,000
 The second stage of knowledge, and this is not intellectual

278
00:12:26,000 --> 00:12:28,260
, is one begins to see

279
00:12:28,260 --> 00:12:30,850
 how the body and the mind work together. So one wants to

280
00:12:30,850 --> 00:12:31,920
 stand up and then there

281
00:12:31,920 --> 00:12:34,060
 is the standing. One wants to sit down and then there's the

282
00:12:34,060 --> 00:12:35,080
 sitting. There

283
00:12:35,080 --> 00:12:37,770
 arises pain in the body and as a result there arises disl

284
00:12:37,770 --> 00:12:39,540
iking. There arises

285
00:12:39,540 --> 00:12:42,960
 pleasure in the body and as a result there arises liking.

286
00:12:42,960 --> 00:12:43,960
 Because of the

287
00:12:43,960 --> 00:12:47,580
 disliking and because of the liking there arises clinging

288
00:12:47,580 --> 00:12:48,560
 and there arises

289
00:12:48,560 --> 00:12:53,280
 partiality and there arises becoming and acting and stress

290
00:12:53,280 --> 00:12:54,080
 and as a result

291
00:12:54,080 --> 00:12:57,800
 suffering. And so as a result they start to see the truth

292
00:12:57,800 --> 00:12:59,500
 of karma. A person who

293
00:12:59,500 --> 00:13:02,460
 gets to this stage of knowledge has an understanding, non-

294
00:13:02,460 --> 00:13:03,240
intellectual

295
00:13:03,240 --> 00:13:05,770
 understanding of karma. They understand how defilements

296
00:13:05,770 --> 00:13:08,560
 really inevitably lead

297
00:13:08,560 --> 00:13:11,470
 to stress and suffering. And so as a result they have

298
00:13:11,470 --> 00:13:12,920
 entered what we call the

299
00:13:12,920 --> 00:13:18,960
 little stream, which means whereas a sotapana, another

300
00:13:18,960 --> 00:13:19,440
 thing about the

301
00:13:19,440 --> 00:13:24,040
 sotapana is they're not liable to be born in states of woe

302
00:13:24,040 --> 00:13:25,400
 or states of

303
00:13:25,400 --> 00:13:28,400
 suffering. They're not liable to be born in hell. They're

304
00:13:28,400 --> 00:13:29,440
 not liable to be born as

305
00:13:29,440 --> 00:13:32,380
 an animal. They're not liable to be born as a ghost. They

306
00:13:32,380 --> 00:13:33,800
 can only be born as a

307
00:13:33,800 --> 00:13:39,710
 human or an angel or a god and eventually some of them will

308
00:13:39,710 --> 00:13:40,600
 in one

309
00:13:40,600 --> 00:13:43,190
 lifetime become enlightened, some of them in two or three

310
00:13:43,190 --> 00:13:44,360
 or four or five or six.

311
00:13:44,360 --> 00:13:47,590
 But the longest it will take them to become fully

312
00:13:47,590 --> 00:13:49,200
 enlightened and free from

313
00:13:49,200 --> 00:13:53,780
 samsara is said to be seven lifetime. That's what the texts

314
00:13:53,780 --> 00:13:55,880
 say. A chula sotapana

315
00:13:55,880 --> 00:13:58,060
 doesn't have this reassurance. Obviously they haven't

316
00:13:58,060 --> 00:13:59,240
 realized nirvana, but the

317
00:13:59,240 --> 00:14:02,960
 reassurance that they have is that on realizing the second

318
00:14:02,960 --> 00:14:03,960
 stage of knowledge

319
00:14:03,960 --> 00:14:10,020
 in this lifetime, at the end of this lifetime with the with

320
00:14:10,020 --> 00:14:10,480
 the breakup of the

321
00:14:10,480 --> 00:14:15,560
 body for this one lifetime, they will not be reborn in any

322
00:14:15,560 --> 00:14:16,960
 of the states of

323
00:14:16,960 --> 00:14:19,590
 suffering. They will not be reborn in hell. They will not

324
00:14:19,590 --> 00:14:20,360
 be reborn as an

325
00:14:20,360 --> 00:14:26,090
 animal. They will not be reborn as a ghost. So they are

326
00:14:26,090 --> 00:14:27,160
 also said to be a

327
00:14:27,160 --> 00:14:30,710
 sotapana for that reason, but it's a chula sotapana and it

328
00:14:30,710 --> 00:14:32,260
's just a designation to

329
00:14:32,260 --> 00:14:35,040
 mean that this is the stage that one needs to reach to be

330
00:14:35,040 --> 00:14:35,840
 safe for this

331
00:14:35,840 --> 00:14:39,350
 lifetime. Next lifetime they'll be born as a human or in a

332
00:14:39,350 --> 00:14:40,040
 good state.

333
00:14:40,040 --> 00:14:43,170
 They might still forget all of that and and begin to

334
00:14:43,170 --> 00:14:44,840
 practice unwholesome deeds

335
00:14:44,840 --> 00:14:52,480
 and be born in hell as a result without any difficulty. As

336
00:14:52,480 --> 00:14:54,480
 for the follow-up

337
00:14:54,480 --> 00:15:01,170
 question, many people who lived in the Buddha's time said

338
00:15:01,170 --> 00:15:02,240
 to be a streamwinners

339
00:15:02,240 --> 00:15:07,890
 for sure. Many many people. There were, Vissaka became a s

340
00:15:07,890 --> 00:15:10,200
otapana when she was

341
00:15:10,200 --> 00:15:13,520
 seven years old and Atapindika became a sotapana. King Vibh

342
00:15:13,520 --> 00:15:14,400
ishara became a

343
00:15:14,400 --> 00:15:20,520
 sotapana. They say in the commentaries there's many

344
00:15:20,520 --> 00:15:23,020
 mentions of sotapana, how

345
00:15:23,020 --> 00:15:25,400
 the Buddha would give a talk and the whole crowd would

346
00:15:25,400 --> 00:15:26,520
 become a sotapana or

347
00:15:26,520 --> 00:15:30,360
 so on. They say in Sawati where the Buddha spent a lot of

348
00:15:30,360 --> 00:15:31,160
 his a lot of his

349
00:15:31,160 --> 00:15:42,060
 lifetime something like 26 or 24 years that there were

350
00:15:42,060 --> 00:15:44,400
 seven koti of people.

351
00:15:44,400 --> 00:15:53,240
 And five koti were Aryapugala means sotapana or sakita gami

352
00:15:53,240 --> 00:15:53,960
 or anagami or

353
00:15:53,960 --> 00:16:00,310
 arahan. And one koti were kalyana, putujana which means

354
00:16:00,310 --> 00:16:02,440
 they were people who

355
00:16:02,440 --> 00:16:05,800
 practiced morality and practiced meditation but hadn't

356
00:16:05,800 --> 00:16:06,240
 become

357
00:16:06,240 --> 00:16:09,820
 enlightened yet. And then there were one last koti who were

358
00:16:09,820 --> 00:16:11,000
 putujana which means

359
00:16:11,000 --> 00:16:13,430
 they were full of defilements and had no interest in the

360
00:16:13,430 --> 00:16:14,560
 Buddha's teachings. So

361
00:16:14,560 --> 00:16:17,540
 there's a story of this pig butcher, Chunda, who lived very

362
00:16:17,540 --> 00:16:18,560
 close to Jethavana

363
00:16:18,560 --> 00:16:23,400
 near or in Sawati and every day the monks would go past and

364
00:16:23,400 --> 00:16:24,800
 they thought it was

365
00:16:24,800 --> 00:16:29,120
 remarkable that this guy lived so close to Jethavana and he

366
00:16:29,120 --> 00:16:29,560
 never came

367
00:16:29,560 --> 00:16:34,480
 to listen to the Dhamma once or do any good deeds of giving

368
00:16:34,480 --> 00:16:35,840
 charity or keeping

369
00:16:35,840 --> 00:16:38,590
 morality or so on. He just killed pigs for his whole life

370
00:16:38,590 --> 00:16:39,760
 and it was horrible

371
00:16:39,760 --> 00:16:42,760
 how he killed pigs. I think we did a video about that

372
00:16:42,760 --> 00:16:44,840
 already but just an

373
00:16:44,840 --> 00:16:48,000
 example. So I hope that's a fairly comprehensive

374
00:16:48,000 --> 00:16:49,360
 explanation of sotapana.

375
00:16:49,360 --> 00:16:52,350
 Is there anything else that I missed? I can't think of

376
00:16:52,350 --> 00:16:54,320
 anything that should be

377
00:16:54,320 --> 00:16:58,300
 added. I mean you know there there's always more that can

378
00:16:58,300 --> 00:16:59,600
 be said, lots of

379
00:16:59,600 --> 00:17:06,890
 theory involved. But a sotapana is a person who has

380
00:17:06,890 --> 00:17:08,480
 followed the meditation

381
00:17:08,480 --> 00:17:11,600
 practice to its consummation. At that point they're safe,

382
00:17:11,600 --> 00:17:13,400
 they have reached a

383
00:17:13,400 --> 00:17:19,200
 state of safety and this is because of the it's now become

384
00:17:19,200 --> 00:17:20,480
 a habit and it

385
00:17:20,480 --> 00:17:23,560
 begins to snowball and so it's not that they become content

386
00:17:23,560 --> 00:17:24,400
 or they can become

387
00:17:24,400 --> 00:17:27,310
 content, they can't become content. They have developed

388
00:17:27,310 --> 00:17:28,720
 this state of energy, this

389
00:17:28,720 --> 00:17:32,530
 state of confidence in the practice that will lead them to

390
00:17:32,530 --> 00:17:33,440
 always think about

391
00:17:33,440 --> 00:17:37,520
 practicing. Even though they might fall into liking or disl

392
00:17:37,520 --> 00:17:38,360
iking and even get

393
00:17:38,360 --> 00:17:43,460
 married and have children, Wysaka had 20 children or

394
00:17:43,460 --> 00:17:45,760
 something and she was a

395
00:17:45,760 --> 00:17:49,520
 sotapana. But they will always be thinking about meditation

396
00:17:49,520 --> 00:17:49,960
, always be

397
00:17:49,960 --> 00:17:52,710
 trying to find the time to meditate, always be thinking

398
00:17:52,710 --> 00:17:53,600
 about how they can

399
00:17:53,600 --> 00:17:56,680
 develop themselves and always be interested in good things

400
00:17:56,680 --> 00:17:57,440
 and spiritual

401
00:17:57,440 --> 00:18:01,130
 things because it's stuck in their minds. If there's a

402
00:18:01,130 --> 00:18:02,440
 crack and that crack

403
00:18:02,440 --> 00:18:08,930
 can't be fixed it will eventually overflow and make them

404
00:18:08,930 --> 00:18:09,560
 free.

