1
00:00:00,000 --> 00:00:09,840
 Hey, good evening everyone.

2
00:00:09,840 --> 00:00:13,960
 Welcome to our evening session.

3
00:00:13,960 --> 00:00:24,880
 I was invited next month to give a talk, a couple of talks.

4
00:00:24,880 --> 00:00:32,960
 One of them was asked to give a talk on wrong mindfulness.

5
00:00:32,960 --> 00:00:35,120
 And there actually is a term, right?

6
00:00:35,120 --> 00:00:46,520
 There's right mindfulness and there's wrong mindfulness.

7
00:00:46,520 --> 00:00:49,780
 Something in me wants to say that, well, mindfulness, as we

8
00:00:49,780 --> 00:00:52,760
 know from the Abhidhamma, is, mindfulness

9
00:00:52,760 --> 00:00:57,320
 is always wholesome.

10
00:00:57,320 --> 00:01:06,510
 In the suttat, the Buddha said, sattin chakwahang bhikkha

11
00:01:06,510 --> 00:01:09,400
 rei sabatikam vadami.

12
00:01:09,400 --> 00:01:12,160
 Mindfulness I tell you monks is always useful.

13
00:01:12,160 --> 00:01:15,560
 It's always good.

14
00:01:15,560 --> 00:01:23,380
 But then he talks about something called micha sati in the

15
00:01:23,380 --> 00:01:28,040
 context of the Eightfold Path.

16
00:01:28,040 --> 00:01:30,570
 I think it's a little misleading for us to say wrong

17
00:01:30,570 --> 00:01:31,480
 mindfulness.

18
00:01:31,480 --> 00:01:36,680
 I mean, technically, mindfulness is always right.

19
00:01:36,680 --> 00:01:39,480
 It's always good.

20
00:01:39,480 --> 00:01:43,880
 But from a conceptual perspective, when we talk about

21
00:01:43,880 --> 00:01:46,920
 mindfulness practice, when we talk

22
00:01:46,920 --> 00:01:51,010
 about being attentive and the English word mindfulness,

23
00:01:51,010 --> 00:01:53,080
 which is a rather general and

24
00:01:53,080 --> 00:02:04,920
 broad term, it is possible to understand it as good and bad

25
00:02:04,920 --> 00:02:05,720
.

26
00:02:05,720 --> 00:02:10,130
 So I thought I'd talk a little bit about ways that the

27
00:02:10,130 --> 00:02:12,400
 practice can go wrong.

28
00:02:12,400 --> 00:02:16,410
 Not specifically talking about the state of mind that is

29
00:02:16,410 --> 00:02:19,040
 wrong, but when you're practicing

30
00:02:19,040 --> 00:02:22,560
 meditation, what can go wrong?

31
00:02:22,560 --> 00:02:24,280
 This is quite interesting.

32
00:02:24,280 --> 00:02:29,060
 I think this was really what they're trying to get at with

33
00:02:29,060 --> 00:02:30,720
 this invitation.

34
00:02:30,720 --> 00:02:37,280
 They want me to talk about how the practice can go wrong.

35
00:02:37,280 --> 00:02:39,960
 You're practicing meditation.

36
00:02:39,960 --> 00:02:40,960
 What can go wrong?

37
00:02:40,960 --> 00:02:45,440
 How do you know you're practicing well, right?

38
00:02:45,440 --> 00:02:51,880
 Sometimes there's this doubt, am I doing it right?

39
00:02:51,880 --> 00:02:59,840
 So the first way that the practice can go wrong, obviously,

40
00:02:59,840 --> 00:03:03,200
 is you don't practice.

41
00:03:03,200 --> 00:03:11,240
 Of course, that seems quite obvious and kind of irrelevant.

42
00:03:11,240 --> 00:03:12,240
 We're talking about practice.

43
00:03:12,240 --> 00:03:15,520
 What does it mean to not practice?

44
00:03:15,520 --> 00:03:19,000
 Almost obviously it means those people who don't come,

45
00:03:19,000 --> 00:03:21,000
 those people who never thought

46
00:03:21,000 --> 00:03:23,000
 of practicing meditation.

47
00:03:23,000 --> 00:03:29,760
 Well, that's not going to get you anywhere.

48
00:03:29,760 --> 00:03:31,560
 But then there are those people who have heard of

49
00:03:31,560 --> 00:03:33,040
 meditation, those people who have been

50
00:03:33,040 --> 00:03:40,640
 even taught meditation, but are not interested in it.

51
00:03:40,640 --> 00:03:42,120
 It's also quite common.

52
00:03:42,120 --> 00:03:46,930
 Sometimes you'll teach someone and it just goes in one ear

53
00:03:46,930 --> 00:03:49,520
 and out the other, over their

54
00:03:49,520 --> 00:03:59,560
 head, not able to or not interested in understanding it.

55
00:03:59,560 --> 00:04:07,010
 But more importantly, for all of us, congratulations, you

56
00:04:07,010 --> 00:04:09,800
've passed that one.

57
00:04:09,800 --> 00:04:13,030
 What you might find instead is that sometimes even though

58
00:04:13,030 --> 00:04:14,920
 you intend to practice, you're

59
00:04:14,920 --> 00:04:18,160
 actually not practicing.

60
00:04:18,160 --> 00:04:22,080
 So walking and sitting unmindfully, we do a lot of walking

61
00:04:22,080 --> 00:04:23,840
 and a lot of sitting, and

62
00:04:23,840 --> 00:04:27,130
 I'm sure sometimes you'll notice that you weren't being

63
00:04:27,130 --> 00:04:29,320
 mindful, even though your intention

64
00:04:29,320 --> 00:04:32,720
 was to be mindful.

65
00:04:32,720 --> 00:04:39,760
 And you were trying your best to be mindful.

66
00:04:39,760 --> 00:04:47,610
 You find yourself getting distracted or slipping, slipping

67
00:04:47,610 --> 00:04:50,680
 back into old habits.

68
00:04:50,680 --> 00:04:54,020
 So unmindfulness is of course the first way that the

69
00:04:54,020 --> 00:04:55,640
 practice can go wrong.

70
00:04:55,640 --> 00:04:57,880
 And what's the result of that?

71
00:04:57,880 --> 00:05:00,400
 The result of that is generally just a lot of stress and

72
00:05:00,400 --> 00:05:01,120
 suffering.

73
00:05:01,120 --> 00:05:04,640
 Of course, if you don't ever practice mindfulness at all,

74
00:05:04,640 --> 00:05:06,920
 your life is going to be full of stress

75
00:05:06,920 --> 00:05:11,080
 and suffering as most people are.

76
00:05:11,080 --> 00:05:14,040
 Potentially of course, it is possible that things will be

77
00:05:14,040 --> 00:05:15,440
 good for a while, but it's

78
00:05:15,440 --> 00:05:21,730
 quite uncertain because you're very easily sidetracked and

79
00:05:21,730 --> 00:05:25,540
 get caught up in unwholesomeness.

80
00:05:25,540 --> 00:05:30,480
 As a meditator, meditating unmindfully, this is where it

81
00:05:30,480 --> 00:05:32,440
 becomes stressful.

82
00:05:32,440 --> 00:05:36,560
 Your movements are unmindful.

83
00:05:36,560 --> 00:05:38,960
 And so walking back and forth, back and forth and sitting

84
00:05:38,960 --> 00:05:40,400
 like anything else can cause a

85
00:05:40,400 --> 00:05:44,930
 lot of pain and aches and so on because of the tension in

86
00:05:44,930 --> 00:05:47,480
 your body, the stress in the

87
00:05:47,480 --> 00:05:48,480
 mind.

88
00:05:48,480 --> 00:05:55,320
 It makes you tired, makes you unhappy, bored.

89
00:05:55,320 --> 00:06:08,630
 It makes you to get bored if you're not being mindful,

90
00:06:08,630 --> 00:06:11,040
 right?

91
00:06:11,040 --> 00:06:14,940
 The next way the practice can go wrong is if you're being

92
00:06:14,940 --> 00:06:18,800
 mindful of the wrong thing.

93
00:06:18,800 --> 00:06:27,240
 And this of course comes in many ways, in many forms.

94
00:06:27,240 --> 00:06:30,800
 So you could be mindful of the past or the future.

95
00:06:30,800 --> 00:06:35,440
 If you get lost thinking about the past, sometimes in

96
00:06:35,440 --> 00:06:39,600
 meditation it is possible that it does

97
00:06:39,600 --> 00:06:44,120
 happen that you get caught up in the past or the future

98
00:06:44,120 --> 00:06:47,360
 thinking about things that happened

99
00:06:47,360 --> 00:06:50,160
 or planning about things that you're going to do, right?

100
00:06:50,160 --> 00:06:55,060
 It's quite common for meditators to get lost in planning or

101
00:06:55,060 --> 00:06:56,320
 reminiscing.

102
00:06:56,320 --> 00:07:00,720
 And that is considered a sort of a mindfulness.

103
00:07:00,720 --> 00:07:04,730
 In the Pali, I mean the word mindfulness is actually imprec

104
00:07:04,730 --> 00:07:06,640
ise, but it's sati in the sense

105
00:07:06,640 --> 00:07:11,180
 that your mind is focused on something, that you're recol

106
00:07:11,180 --> 00:07:14,080
lecting something, that your mind

107
00:07:14,080 --> 00:07:16,560
 is focused on something.

108
00:07:16,560 --> 00:07:21,400
 It's not really useful.

109
00:07:21,400 --> 00:07:24,760
 The past and the future are problematic because of the

110
00:07:24,760 --> 00:07:27,120
 effort it takes to be aware of them,

111
00:07:27,120 --> 00:07:30,240
 but more importantly because they're not real.

112
00:07:30,240 --> 00:07:34,390
 The past isn't here, doesn't actually exist except in your

113
00:07:34,390 --> 00:07:34,960
 mind.

114
00:07:34,960 --> 00:07:41,550
 And so the result of living in concepts, in memories or

115
00:07:41,550 --> 00:07:44,960
 planning about the future, the

116
00:07:44,960 --> 00:07:52,460
 result of living in these make believe realms is stress and

117
00:07:52,460 --> 00:07:56,480
 delusion because it's very easy

118
00:07:56,480 --> 00:07:57,480
 to get lost.

119
00:07:57,480 --> 00:08:04,910
 For example, the past, you get caught up in emotions about

120
00:08:04,910 --> 00:08:08,120
 the past or the future.

121
00:08:08,120 --> 00:08:14,760
 You get caught up in planning and get lost in potential

122
00:08:14,760 --> 00:08:19,320
ities that may never come to pass.

123
00:08:19,320 --> 00:08:20,720
 So there's a lot of illusion.

124
00:08:20,720 --> 00:08:26,080
 I mean, it's not real.

125
00:08:26,080 --> 00:08:33,800
 More common though, or equally common anyway, in meditation

126
00:08:33,800 --> 00:08:36,240
 is focused on something, not

127
00:08:36,240 --> 00:08:43,020
 past or future, but still focused on an object that is not

128
00:08:43,020 --> 00:08:44,520
 present.

129
00:08:44,520 --> 00:08:45,720
 This means a concept.

130
00:08:45,720 --> 00:08:49,800
 So meditators will often talk about how they're daydreaming

131
00:08:49,800 --> 00:08:50,120
.

132
00:08:50,120 --> 00:08:53,930
 That's a simple example of how they start to imagine

133
00:08:53,930 --> 00:08:56,000
 themselves here or there or in

134
00:08:56,000 --> 00:09:00,000
 some situation.

135
00:09:00,000 --> 00:09:08,140
 But they're actually meditations that involve focusing on

136
00:09:08,140 --> 00:09:10,320
 the concept.

137
00:09:10,320 --> 00:09:13,550
 And so this is a common misunderstanding or a

138
00:09:13,550 --> 00:09:14,840
 misunderstanding.

139
00:09:14,840 --> 00:09:27,370
 It's a common practice that is distinct from actually

140
00:09:27,370 --> 00:09:28,120
 seeing

141
00:09:28,120 --> 00:09:30,600
 things as they are being present.

142
00:09:30,600 --> 00:09:36,440
 If you focus on people, for example, wishing, "May all

143
00:09:36,440 --> 00:09:37,680
 beings be happy.

144
00:09:37,680 --> 00:09:40,360
 May I be happy.

145
00:09:40,360 --> 00:09:43,760
 May this person be happy," or so on.

146
00:09:43,760 --> 00:09:47,450
 If you focus on the parts of the body, if you focus on a

147
00:09:47,450 --> 00:09:49,640
 candle flame, I actually had

148
00:09:49,640 --> 00:09:56,420
 a ... when I was teaching recently at a high school, at my

149
00:09:56,420 --> 00:09:58,080
 high school, one of the teachers

150
00:09:58,080 --> 00:10:02,280
 there, he actually said that he just likes to look at a

151
00:10:02,280 --> 00:10:03,680
 candle flame.

152
00:10:03,680 --> 00:10:15,120
 And that helps calm him down and give him focus.

153
00:10:15,120 --> 00:10:19,320
 So a concept is a common object of meditation.

154
00:10:19,320 --> 00:10:25,270
 We have an idea or an image or a thing in our mind that we

155
00:10:25,270 --> 00:10:27,120
 conceive of.

156
00:10:27,120 --> 00:10:30,790
 And we take that as our object of meditation, which is

157
00:10:30,790 --> 00:10:31,840
 actually fine.

158
00:10:31,840 --> 00:10:35,360
 I mean, it's a valid meditation practice in itself.

159
00:10:35,360 --> 00:10:37,280
 There's nothing bad about it.

160
00:10:37,280 --> 00:10:40,280
 It's quite good.

161
00:10:40,280 --> 00:10:46,380
 But the thing about concepts is that they are distinct from

162
00:10:46,380 --> 00:10:47,680
 reality.

163
00:10:47,680 --> 00:10:48,680
 They're pleasant.

164
00:10:48,680 --> 00:10:49,680
 They're satisfying.

165
00:10:49,680 --> 00:10:50,680
 They're controllable.

166
00:10:50,680 --> 00:11:00,040
 They're predictable.

167
00:11:00,040 --> 00:11:04,490
 When you live in a world of concepts, you can make them any

168
00:11:04,490 --> 00:11:05,800
 way you wish.

169
00:11:05,800 --> 00:11:08,040
 And in meditation, this happens.

170
00:11:08,040 --> 00:11:12,550
 You get into states that are quite calm and you're able to

171
00:11:12,550 --> 00:11:15,120
 manipulate your awareness.

172
00:11:15,120 --> 00:11:21,980
 So make large, expansive awareness, infinite consciousness

173
00:11:21,980 --> 00:11:24,760
 and that kind of thing.

174
00:11:24,760 --> 00:11:28,300
 Or in a more mundane sense, you're able to imagine anything

175
00:11:28,300 --> 00:11:28,600
.

176
00:11:28,600 --> 00:11:30,680
 Imagination is great.

177
00:11:30,680 --> 00:11:33,160
 It's imagination by its nature.

178
00:11:33,160 --> 00:11:37,560
 It's preferable to reality in every way.

179
00:11:37,560 --> 00:11:38,720
 Imagination can be whatever you want.

180
00:11:38,720 --> 00:11:43,830
 This is why books and television are so popular because it

181
00:11:43,830 --> 00:11:47,520
's a means of manipulating reality,

182
00:11:47,520 --> 00:11:51,440
 controlling our awareness.

183
00:11:51,440 --> 00:11:58,520
 So imagination is better than reality in every way but one.

184
00:11:58,520 --> 00:12:07,660
 And that one important distinction is that it's not real.

185
00:12:07,660 --> 00:12:10,420
 You can't live in imagination.

186
00:12:10,420 --> 00:12:14,840
 Imagination can't be a refuge.

187
00:12:14,840 --> 00:12:16,840
 It's like virtual reality.

188
00:12:16,840 --> 00:12:20,260
 Virtual reality might be great but in the end you have to

189
00:12:20,260 --> 00:12:21,680
 turn it off and go eat or

190
00:12:21,680 --> 00:12:27,690
 you have to turn it off and you depend on reality is the

191
00:12:27,690 --> 00:12:28,840
 point.

192
00:12:28,840 --> 00:12:33,680
 And reality has some important differences.

193
00:12:33,680 --> 00:12:40,840
 It's distinct in important ways, distinct from imagination.

194
00:12:40,840 --> 00:12:48,080
 It's not like that.

195
00:12:48,080 --> 00:12:51,850
 So that's important to be clear about, to be aware of, that

196
00:12:51,850 --> 00:12:53,760
 we're trying to be conscious

197
00:12:53,760 --> 00:12:58,410
 of reality and so why we have you focus on your feet moving

198
00:12:58,410 --> 00:13:00,720
, your stomach, why we're

199
00:13:00,720 --> 00:13:06,160
 not doing meditation on a concept or something.

200
00:13:06,160 --> 00:13:10,380
 Why we're doing actually very mundane exercises because we

201
00:13:10,380 --> 00:13:12,640
 want to learn about reality.

202
00:13:12,640 --> 00:13:19,080
 It's much more interesting to us than anything else.

203
00:13:19,080 --> 00:13:23,680
 The mundane or what we call the mundane is the real and we

204
00:13:23,680 --> 00:13:26,320
 want to learn about the real.

205
00:13:26,320 --> 00:13:32,450
 The next way that meditation can go wrong or mindfulness

206
00:13:32,450 --> 00:13:34,200
 can go wrong.

207
00:13:34,200 --> 00:13:42,000
 First the problems or the effect of focusing on concepts.

208
00:13:42,000 --> 00:13:47,580
 See the most dangerous thing about concepts is that they're

209
00:13:47,580 --> 00:13:48,880
 infinite.

210
00:13:48,880 --> 00:13:50,160
 This really is a danger.

211
00:13:50,160 --> 00:13:52,960
 This one is where meditation goes wrong.

212
00:13:52,960 --> 00:13:58,190
 Because if you don't have a teacher practicing meditation

213
00:13:58,190 --> 00:14:01,520
 based on a concept or just imagining,

214
00:14:01,520 --> 00:14:06,900
 sitting in and letting your imagination go, it can lead to

215
00:14:06,900 --> 00:14:09,920
 psychotic episodes or whatever

216
00:14:09,920 --> 00:14:16,200
 you call them, states of temporary insanity.

217
00:14:16,200 --> 00:14:19,610
 If you see something and it's an image of something and you

218
00:14:19,610 --> 00:14:21,020
 just go with it and you

219
00:14:21,020 --> 00:14:25,040
 see where it leads and I've had meditators do that.

220
00:14:25,040 --> 00:14:30,200
 Without proper advice the meditator will manipulate and

221
00:14:30,200 --> 00:14:33,940
 expand and get caught up in some feedback

222
00:14:33,940 --> 00:14:36,760
 loop somewhere.

223
00:14:36,760 --> 00:14:44,760
 Then fear seeps in or anxieties or anger or an addiction

224
00:14:44,760 --> 00:14:49,640
 and it feeds and it becomes dangerous

225
00:14:49,640 --> 00:14:50,640
 to the mind.

226
00:14:50,640 --> 00:14:53,480
 It's not incredibly dangerous.

227
00:14:53,480 --> 00:14:56,580
 There are people who kill themselves because they get to

228
00:14:56,580 --> 00:14:57,920
 such a wound up state.

229
00:14:57,920 --> 00:15:00,640
 I think that's rather rare.

230
00:15:00,640 --> 00:15:03,030
 Mostly it just drives you crazy for a while and then you

231
00:15:03,030 --> 00:15:04,480
 stop meditating and get really

232
00:15:04,480 --> 00:15:07,720
 afraid of meditation.

233
00:15:07,720 --> 00:15:11,440
 The realm of concepts, it does that because it's infinite.

234
00:15:11,440 --> 00:15:12,440
 You can go anywhere.

235
00:15:12,440 --> 00:15:17,080
 You can easily, easily get caught up in who knows what.

236
00:15:17,080 --> 00:15:27,520
 Only unlimited number of dangerous feedback loops.

237
00:15:27,520 --> 00:15:32,300
 You don't have that problem with reality.

238
00:15:32,300 --> 00:15:34,840
 Reality is not going to get you caught up in anything.

239
00:15:34,840 --> 00:15:38,270
 Reality is just going to, at worst, it's going to be

240
00:15:38,270 --> 00:15:39,320
 stressful.

241
00:15:39,320 --> 00:15:42,560
 It's going to be uncomfortable.

242
00:15:42,560 --> 00:15:46,720
 But at best it's going to help you become invincible.

243
00:15:46,720 --> 00:15:50,400
 At best it's going to teach you.

244
00:15:50,400 --> 00:15:52,800
 It's going to teach you to see things clearly.

245
00:15:52,800 --> 00:16:00,080
 It's going to teach you enough so that you no longer react

246
00:16:00,080 --> 00:16:03,240
 to experience because all

247
00:16:03,240 --> 00:16:08,260
 of our imagination, all of the concepts, people, places and

248
00:16:08,260 --> 00:16:10,840
 things that we live with, they're

249
00:16:10,840 --> 00:16:13,840
 all based on experience, on reality.

250
00:16:13,840 --> 00:16:16,520
 They all have reality as their root.

251
00:16:16,520 --> 00:16:21,800
 You can't have concepts without reality.

252
00:16:21,800 --> 00:16:27,160
 Once you understand reality then the concepts that you live

253
00:16:27,160 --> 00:16:30,120
 with, that you work with become

254
00:16:30,120 --> 00:16:39,400
 innocent, become innocuous.

255
00:16:39,400 --> 00:16:41,280
 But there's still problems.

256
00:16:41,280 --> 00:16:48,880
 So the big problems that come up in meditation is

257
00:16:48,880 --> 00:16:51,400
 attachment.

258
00:16:51,400 --> 00:16:56,260
 And so the first attachment that a meditator comes to is

259
00:16:56,260 --> 00:16:58,880
 the negative attachment.

260
00:16:58,880 --> 00:17:04,280
 You start to practice properly and it's not very pleasant.

261
00:17:04,280 --> 00:17:07,080
 You see impermanence and you think something's wrong.

262
00:17:07,080 --> 00:17:10,680
 You must be doing something wrong because this sucks.

263
00:17:10,680 --> 00:17:11,680
 Meditation is not pleasant.

264
00:17:11,680 --> 00:17:15,520
 It's not comfortable.

265
00:17:15,520 --> 00:17:20,820
 This isn't the peaceful, blissful thing that you see on the

266
00:17:20,820 --> 00:17:23,040
 read about in books or see

267
00:17:23,040 --> 00:17:27,640
 in movies, people meditating, smiling.

268
00:17:27,640 --> 00:17:31,560
 You're sitting here crying and moaning and stressing and

269
00:17:31,560 --> 00:17:33,440
 freaking out in your room and

270
00:17:33,440 --> 00:17:34,440
 you're thinking.

271
00:17:34,440 --> 00:17:38,240
 You must be doing something wrong.

272
00:17:38,240 --> 00:17:42,920
 Aversion is the first one.

273
00:17:42,920 --> 00:17:48,900
 The stress that comes from looking at the things that we

274
00:17:48,900 --> 00:17:51,680
 normally try to avoid.

275
00:17:51,680 --> 00:18:08,320
 It's a great stress involved with that.

276
00:18:08,320 --> 00:18:09,320
 We see impermanence.

277
00:18:09,320 --> 00:18:10,320
 Everything's changing.

278
00:18:10,320 --> 00:18:11,320
 We see suffering.

279
00:18:11,320 --> 00:18:12,320
 We see non-self.

280
00:18:12,320 --> 00:18:17,160
 We find that we're not able to control our own minds.

281
00:18:17,160 --> 00:18:22,080
 We find that we're not really in charge, that our minds are

282
00:18:22,080 --> 00:18:30,760
 not responding and that it's

283
00:18:30,760 --> 00:18:36,160
 out of our control.

284
00:18:36,160 --> 00:18:41,170
 The second way that meditation goes wrong because

285
00:18:41,170 --> 00:18:44,680
 eventually you start to accept and

286
00:18:44,680 --> 00:18:49,080
 you start to see that there's nothing wrong with imper

287
00:18:49,080 --> 00:18:49,960
manent.

288
00:18:49,960 --> 00:18:51,360
 It's just the way things are.

289
00:18:51,360 --> 00:18:57,320
 All it means is the real problem is the clinging.

290
00:18:57,320 --> 00:18:58,680
 Reality has no problem with us.

291
00:18:58,680 --> 00:19:02,200
 We have a problem with reality.

292
00:19:02,200 --> 00:19:05,590
 As you become more familiar with it, you become more

293
00:19:05,590 --> 00:19:07,840
 comfortable with it in the sense of it

294
00:19:07,840 --> 00:19:11,720
 not bothering you.

295
00:19:11,720 --> 00:19:16,200
 The problem which some of you here are most likely to

296
00:19:16,200 --> 00:19:19,080
 encounter is the problem of good

297
00:19:19,080 --> 00:19:23,240
 experiences as you get more comfortable with the practice

298
00:19:23,240 --> 00:19:25,680
 and more proficient and stronger

299
00:19:25,680 --> 00:19:26,680
 in mind.

300
00:19:26,680 --> 00:19:30,670
 There will come a point where all of this early stress and

301
00:19:30,670 --> 00:19:33,000
 suffering, and I think you're

302
00:19:33,000 --> 00:19:37,770
 all getting there as it's becoming more comfortable or you

303
00:19:37,770 --> 00:19:41,200
're getting stronger, is the positive.

304
00:19:41,200 --> 00:19:45,440
 You get stuck on the positive.

305
00:19:45,440 --> 00:19:51,480
 The texts talk about the problem of good experiences.

306
00:19:51,480 --> 00:19:55,620
 If you're practicing correctly, you should have some good

307
00:19:55,620 --> 00:19:56,840
 experiences.

308
00:19:56,840 --> 00:19:58,240
 It should start to get smooth.

309
00:19:58,240 --> 00:20:00,560
 It should feel comfortable.

310
00:20:00,560 --> 00:20:01,560
 Not always.

311
00:20:01,560 --> 00:20:06,430
 Eventually you let go of this, but in the early stages it

312
00:20:06,430 --> 00:20:09,000
 can be quite comfortable.

313
00:20:09,000 --> 00:20:11,720
 The problem is not feeling comfortable.

314
00:20:11,720 --> 00:20:16,000
 The problem is that we cling to it.

315
00:20:16,000 --> 00:20:23,440
 You begin to favor it and you start to learn about what it

316
00:20:23,440 --> 00:20:28,080
 is that makes you comfortable.

317
00:20:28,080 --> 00:20:33,100
 Maybe you see lights or colors or pictures and you follow

318
00:20:33,100 --> 00:20:33,920
 them.

319
00:20:33,920 --> 00:20:38,800
 You get entranced by them.

320
00:20:38,800 --> 00:20:42,160
 You don't want to let them go.

321
00:20:42,160 --> 00:20:48,440
 You do what you can to cultivate them.

322
00:20:48,440 --> 00:20:54,120
 Maybe you feel calm or maybe you feel happy.

323
00:20:54,120 --> 00:20:55,240
 You just don't want to move.

324
00:20:55,240 --> 00:20:59,440
 You don't want to disturb the happiness or the calm.

325
00:20:59,440 --> 00:21:02,440
 You get attached to it.

326
00:21:02,440 --> 00:21:06,080
 These come because we're seeing clearly and we're no longer

327
00:21:06,080 --> 00:21:07,980
 stressing and our minds become

328
00:21:07,980 --> 00:21:11,400
 quite focused.

329
00:21:11,400 --> 00:21:15,180
 After four or five days of being here, you should all go

330
00:21:15,180 --> 00:21:16,520
 through these.

331
00:21:16,520 --> 00:21:18,680
 Some of them.

332
00:21:18,680 --> 00:21:22,820
 Maybe you start to know things and understand things about

333
00:21:22,820 --> 00:21:23,800
 yourself.

334
00:21:23,800 --> 00:21:27,880
 Maybe you start to have exceptional experiences of clarity

335
00:21:27,880 --> 00:21:29,080
 of knowledge.

336
00:21:29,080 --> 00:21:39,040
 I mean, magical sense. You start to read, be able to see

337
00:21:39,040 --> 00:21:40,160
 things far away or hear things.

338
00:21:40,160 --> 00:21:43,960
 Some people have clairvoyance.

339
00:21:43,960 --> 00:21:46,980
 Some people remember their past lives, these kind of things

340
00:21:46,980 --> 00:21:47,200
.

341
00:21:47,200 --> 00:21:49,440
 Magical powers, they say, can come.

342
00:21:49,440 --> 00:21:54,320
 But you're kind of neat and if they don't come, it's even

343
00:21:54,320 --> 00:21:56,600
 worse because you might be

344
00:21:56,600 --> 00:21:59,860
 waiting for them to come or looking for some kind of

345
00:21:59,860 --> 00:22:02,000
 extraordinary experience.

346
00:22:02,000 --> 00:22:06,190
 But more common is the meditator will start to understand

347
00:22:06,190 --> 00:22:08,400
 the problems in their lives.

348
00:22:08,400 --> 00:22:12,920
 They understand themselves better.

349
00:22:12,920 --> 00:22:13,920
 So they go with that.

350
00:22:13,920 --> 00:22:18,390
 They start to work out their problems and get lost in

351
00:22:18,390 --> 00:22:21,640
 worldly knowledge, which is good.

352
00:22:21,640 --> 00:22:25,360
 Meditation does help you solve everyday problems.

353
00:22:25,360 --> 00:22:29,700
 It's just that when you're doing that, you're no longer

354
00:22:29,700 --> 00:22:31,160
 being mindful.

355
00:22:31,160 --> 00:22:37,560
 And so while it's practically speaking good, from the point

356
00:22:37,560 --> 00:22:40,320
 of view of meditation practice,

357
00:22:40,320 --> 00:22:41,800
 it's not good.

358
00:22:41,800 --> 00:22:48,840
 You get caught up in it.

359
00:22:48,840 --> 00:22:52,600
 You can get great confidence and get caught up in that.

360
00:22:52,600 --> 00:22:53,920
 You can have great effort.

361
00:22:53,920 --> 00:22:56,640
 You can get caught up in that.

362
00:22:56,640 --> 00:23:02,360
 Sometimes meditators have great effort, exerting themselves

363
00:23:02,360 --> 00:23:03,680
 quite well.

364
00:23:03,680 --> 00:23:07,320
 But they get caught up in that and they stop being mindful.

365
00:23:07,320 --> 00:23:12,310
 Even mindfulness itself can cause you to stop being mindful

366
00:23:12,310 --> 00:23:14,560
 as you start to get a hang of

367
00:23:14,560 --> 00:23:15,560
 it.

368
00:23:15,560 --> 00:23:18,730
 You start to find that you're able to note everything and

369
00:23:18,730 --> 00:23:20,400
 you get attached to that and

370
00:23:20,400 --> 00:23:21,400
 you stop meditating.

371
00:23:21,400 --> 00:23:26,130
 And that can happen again and again until you start to lose

372
00:23:26,130 --> 00:23:27,960
 it's not a novelty and you're

373
00:23:27,960 --> 00:23:38,440
 just mindful, habitually mindful.

374
00:23:38,440 --> 00:23:43,070
 So these two, I mean clinging to the bad and clinging to

375
00:23:43,070 --> 00:23:44,160
 the good.

376
00:23:44,160 --> 00:23:48,230
 Now both bad and good are valid objects of insight

377
00:23:48,230 --> 00:23:49,560
 meditation.

378
00:23:49,560 --> 00:23:50,920
 But here's the point.

379
00:23:50,920 --> 00:23:54,710
 If you cling to the bad you're quite likely to stop med

380
00:23:54,710 --> 00:23:55,640
itating.

381
00:23:55,640 --> 00:24:00,440
 And this is what leads meditators to run away when they can

382
00:24:00,440 --> 00:24:02,200
't handle the bad.

383
00:24:02,200 --> 00:24:06,000
 Things they don't like, the stressful.

384
00:24:06,000 --> 00:24:10,050
 Those who get caught up in the good probably won't stop and

385
00:24:10,050 --> 00:24:11,980
 in fact might become quite

386
00:24:11,980 --> 00:24:15,670
 confident and quite attached to the meditation but they don

387
00:24:15,670 --> 00:24:16,920
't get anywhere.

388
00:24:16,920 --> 00:24:21,630
 This is called sukha patipata dantabinya, a person who has

389
00:24:21,630 --> 00:24:24,200
 pleasant practice but because

390
00:24:24,200 --> 00:24:29,060
 they're clinging to the pleasantness of it and their

391
00:24:29,060 --> 00:24:32,920
 faculties are not sharp, they progress

392
00:24:32,920 --> 00:24:39,400
 slowly.

393
00:24:39,400 --> 00:24:43,300
 So I mean the most important lesson for us is first of all

394
00:24:43,300 --> 00:24:46,400
 not to shun bad or good experiences.

395
00:24:46,400 --> 00:24:50,910
 But most importantly to be very careful that we're learning

396
00:24:50,910 --> 00:24:53,160
 to be objective, that we're

397
00:24:53,160 --> 00:24:57,950
 learning to be rooted and grounded, centered in reality as

398
00:24:57,950 --> 00:25:00,160
 opposed to centered in that

399
00:25:00,160 --> 00:25:07,320
 which we like and that which we don't like.

400
00:25:07,320 --> 00:25:11,080
 Trying to be mindful, you know.

401
00:25:11,080 --> 00:25:15,310
 If you come to understand what it means to be mindful then

402
00:25:15,310 --> 00:25:18,680
 you've found a way to be invincible.

403
00:25:18,680 --> 00:25:22,080
 There's a great thing about mindful meditation is that if

404
00:25:22,080 --> 00:25:24,120
 you understand it, it's not hard

405
00:25:24,120 --> 00:25:27,530
 to understand and you practice it correctly and

406
00:25:27,530 --> 00:25:30,440
 theoretically it's not hard to practice

407
00:25:30,440 --> 00:25:31,440
 either.

408
00:25:31,440 --> 00:25:35,600
 It's just challenging because the mind is so keen not to be

409
00:25:35,600 --> 00:25:36,560
 mindful.

410
00:25:36,560 --> 00:25:41,590
 But if you do it correctly, there's nothing that can stand

411
00:25:41,590 --> 00:25:42,920
 in your way.

412
00:25:42,920 --> 00:25:46,600
 There's nothing that can cause you suffering when you stop

413
00:25:46,600 --> 00:25:48,640
 reacting, when you start just

414
00:25:48,640 --> 00:25:55,960
 seeing, when you learn how to just see, how to just be.

415
00:25:55,960 --> 00:26:00,400
 So there you go, some thoughts on how the practice can go

416
00:26:00,400 --> 00:26:01,240
 wrong.

417
00:26:01,240 --> 00:26:02,240
 That's the Dhamma for tonight.

418
00:26:02,240 --> 00:26:03,240
 Thank you all for tuning in.

419
00:26:03,240 --> 00:26:03,240
 Go back to your practice.

420
00:26:03,240 --> 00:26:27,150
 So again, I have answered questions elsewhere but we're

421
00:26:27,150 --> 00:26:29,200
 experimenting with just answering

422
00:26:29,200 --> 00:26:34,170
 questions on the site because it kind of is a happy medium

423
00:26:34,170 --> 00:26:37,560
 between not answering any questions

424
00:26:37,560 --> 00:26:41,040
 and just answering every question.

425
00:26:41,040 --> 00:26:44,480
 Because it's static, people can ask the post questions

426
00:26:44,480 --> 00:26:46,720
 throughout the day, throughout the

427
00:26:46,720 --> 00:26:52,480
 week and eventually get answers.

428
00:26:52,480 --> 00:27:07,560
 If it ever loads, it's not loading for me right now.

429
00:27:07,560 --> 00:27:24,080
 Well, the site isn't loading.

430
00:27:24,080 --> 00:27:27,560
 I think I'm going to take that as an excuse not to answer

431
00:27:27,560 --> 00:27:29,400
 any questions tonight.

432
00:27:29,400 --> 00:27:32,860
 And hopefully it'll be working again and we'll answer

433
00:27:32,860 --> 00:27:34,800
 questions another night.

434
00:27:34,800 --> 00:27:35,800
 Thank you all for coming out.

435
00:27:35,800 --> 00:27:40,190
 I wish you all a good night, good practice and that you

436
00:27:40,190 --> 00:27:43,280
 find peace, happiness and freedom

437
00:27:43,280 --> 00:27:44,280
 from suffering.

438
00:27:44,280 --> 00:27:45,120
 Thank you.

439
00:27:45,120 --> 00:27:46,120
 [BLANK_AUDIO]

