1
00:00:00,000 --> 00:00:04,360
 Okay, talking about the fifth precept, I've read the

2
00:00:04,360 --> 00:00:05,640
 precepts a little different from

3
00:00:05,640 --> 00:00:07,920
 the other four.

4
00:00:07,920 --> 00:00:10,250
 Read this precept is a little different from the other four

5
00:00:10,250 --> 00:00:11,580
 which are always bad and always

6
00:00:11,580 --> 00:00:14,280
 have bad nature and consequences.

7
00:00:14,280 --> 00:00:16,920
 They say the bad is when we take the drug and become

8
00:00:16,920 --> 00:00:18,920
 careless which will lead the person

9
00:00:18,920 --> 00:00:20,960
 to violate the other precepts.

10
00:00:20,960 --> 00:00:23,420
 It almost sounds like it is okay to take drugs and drink

11
00:00:23,420 --> 00:00:25,040
 alcohol if you are advanced enough

12
00:00:25,040 --> 00:00:29,210
 to control yourself and not violate any of the other

13
00:00:29,210 --> 00:00:30,360
 precepts.

14
00:00:30,360 --> 00:00:33,370
 But is it possible to become so advanced that intoxicants

15
00:00:33,370 --> 00:00:35,280
 don't have effect on you or even

16
00:00:35,280 --> 00:00:38,320
 if they affect you, you are able to fully control yourself?

17
00:00:38,320 --> 00:00:41,800
 Well, this precept is different from the other four.

18
00:00:41,800 --> 00:00:43,040
 There's no question about that.

19
00:00:43,040 --> 00:00:50,000
 That has to be admitted that it isn't directly harming

20
00:00:50,000 --> 00:00:54,400
 another person and it isn't an act

21
00:00:54,400 --> 00:00:57,400
 in the same way that the other ones are because it's not

22
00:00:57,400 --> 00:01:03,480
 the act of drinking that is unethical.

23
00:01:03,480 --> 00:01:08,040
 It's really the intention to intoxicate yourself, right?

24
00:01:08,040 --> 00:01:11,490
 So the first thing I would say is that I disagree with this

25
00:01:11,490 --> 00:01:14,040
 interpretation of the fifth one.

26
00:01:14,040 --> 00:01:17,890
 The grammar doesn't really allow for it unless you want to

27
00:01:17,890 --> 00:01:19,760
 be tricky and try to break it

28
00:01:19,760 --> 00:01:24,760
 up and pretend that that's what it says.

29
00:01:24,760 --> 00:01:26,000
 It's not what it says.

30
00:01:26,000 --> 00:01:37,400
 It says Sura and Meria and Madja are things that are Madja

31
00:01:37,400 --> 00:01:40,160
 and Bhama Duttana.

32
00:01:40,160 --> 00:01:43,880
 They are things which are a basis for intoxication.

33
00:01:43,880 --> 00:01:46,270
 It's not saying take them to the extent that you become

34
00:01:46,270 --> 00:01:47,120
 intoxicated.

35
00:01:47,120 --> 00:01:50,040
 That's not what it says and it can't be read to say that.

36
00:01:50,040 --> 00:01:55,090
 Dhanana means base or foundation or it means a place really

37
00:01:55,090 --> 00:01:55,840
.

38
00:01:55,840 --> 00:02:04,500
 Dhanana, but it's used metaphorically to refer to a base or

39
00:02:04,500 --> 00:02:08,240
 something that, something anyway.

40
00:02:08,240 --> 00:02:12,720
 They're things that are their basis for negligence.

41
00:02:12,720 --> 00:02:15,640
 This is why, as I said, people get the idea that any base

42
00:02:15,640 --> 00:02:17,720
 of negligence, any base of bhamada,

43
00:02:17,720 --> 00:02:21,280
 which is the opposite of up-bhamada, is breaking the fifth

44
00:02:21,280 --> 00:02:22,960
 precept, but it's not.

45
00:02:22,960 --> 00:02:25,720
 I mean, it depends.

46
00:02:25,720 --> 00:02:28,380
 The other thing to note is that the precepts are not

47
00:02:28,380 --> 00:02:29,400
 commandments.

48
00:02:29,400 --> 00:02:32,200
 This is what people fail to understand.

49
00:02:32,200 --> 00:02:34,660
 I'm trying to say many things at once here, but I'll try to

50
00:02:34,660 --> 00:02:35,560
 keep it in order.

51
00:02:35,560 --> 00:02:41,800
 Let's try to get an order of this.

52
00:02:41,800 --> 00:02:46,170
 I was trying to explain this idea that the fifth precept is

53
00:02:46,170 --> 00:02:48,360
 only specifically dealing

54
00:02:48,360 --> 00:02:54,180
 with drugs and alcohol to people who are trying to tell me

55
00:02:54,180 --> 00:02:57,720
 that it was inclusive, including

56
00:02:57,720 --> 00:03:01,400
 gambling.

57
00:03:01,400 --> 00:03:05,760
 As I said, I asked them, "What is the first precept?"

58
00:03:05,760 --> 00:03:14,430
 And they said, "Ham kasat," which means, "Don't kill

59
00:03:14,430 --> 00:03:15,240
 animals."

60
00:03:15,240 --> 00:03:16,240
 You are forbidden.

61
00:03:16,240 --> 00:03:22,970
 Basically, "ham" means you're forbidden, but colloquially

62
00:03:22,970 --> 00:03:24,960
 it means don't.

63
00:03:24,960 --> 00:03:26,920
 The word itself means you're forbidden to.

64
00:03:26,920 --> 00:03:33,200
 I forbid you to, something like that.

65
00:03:33,200 --> 00:03:37,150
 The Buddha forbids you, or the precept forbids you to do

66
00:03:37,150 --> 00:03:37,800
 that.

67
00:03:37,800 --> 00:03:38,800
 I said, "Wrong."

68
00:03:38,800 --> 00:03:47,840
 What's this crazy fool I'm talking about?

69
00:03:47,840 --> 00:03:48,840
 They had faith in him.

70
00:03:48,840 --> 00:03:50,840
 What's he getting at?

71
00:03:50,840 --> 00:03:52,320
 He's getting clever here.

72
00:03:52,320 --> 00:03:53,320
 I said, "Anyone?

73
00:03:53,320 --> 00:03:54,320
 Anyone?

74
00:03:54,320 --> 00:03:55,320
 Ham tatjivit?"

75
00:03:55,320 --> 00:03:58,720
 They had all these different ways.

76
00:03:58,720 --> 00:04:01,160
 Don't stop life.

77
00:04:01,160 --> 00:04:06,040
 You're forbidden to cut life, tatjivit.

78
00:04:06,040 --> 00:04:07,880
 They had different interpretations.

79
00:04:07,880 --> 00:04:09,880
 I said, "No, wrong."

80
00:04:09,880 --> 00:04:12,600
 They're like, "Wrong, wrong."

81
00:04:12,600 --> 00:04:16,160
 I said, "No, the precepts are not commandments.

82
00:04:16,160 --> 00:04:18,320
 They're not forbidding anything.

83
00:04:18,320 --> 00:04:22,090
 The precepts, and it's clear from the writing that people

84
00:04:22,090 --> 00:04:24,360
 forget all the time, I undertake

85
00:04:24,360 --> 00:04:27,400
 to refrain from these things.

86
00:04:27,400 --> 00:04:30,000
 You can undertake to refrain from anything.

87
00:04:30,000 --> 00:04:31,600
 You can undertake to refrain from coffee.

88
00:04:31,600 --> 00:04:33,320
 I undertake to refrain from coffee.

89
00:04:33,320 --> 00:04:36,810
 There, you've got a new precept, a new sixth precept, if

90
00:04:36,810 --> 00:04:37,640
 you like.

91
00:04:37,640 --> 00:04:40,840
 Or I undertake to refrain from x, y, z.

92
00:04:40,840 --> 00:04:44,000
 I undertake to refrain from beating up my younger brother.

93
00:04:44,000 --> 00:04:46,570
 Well, it's not one of the five precepts, but I'm going to

94
00:04:46,570 --> 00:04:47,960
 undertake it as a precept.

95
00:04:47,960 --> 00:04:48,960
 Good for you.

96
00:04:48,960 --> 00:04:49,960
 It's a good thing.

97
00:04:49,960 --> 00:04:57,880
 Better not to beat up your little brother.

98
00:04:57,880 --> 00:05:02,120
 It's important to understand what the precepts are, that

99
00:05:02,120 --> 00:05:04,000
 they are a determination not to

100
00:05:04,000 --> 00:05:06,040
 do something.

101
00:05:06,040 --> 00:05:10,800
 The fifth precept is a determination not to take drugs and

102
00:05:10,800 --> 00:05:11,480
 alcohol.

103
00:05:11,480 --> 00:05:15,230
 You could interpret as, "And other things which are similar

104
00:05:15,230 --> 00:05:17,000
 bases of intoxication."

105
00:05:17,000 --> 00:05:25,990
 The things which are a basis for negligence in the same way

106
00:05:25,990 --> 00:05:28,600
 that alcohol is.

107
00:05:28,600 --> 00:05:30,680
 So not in the way that gambling is.

108
00:05:30,680 --> 00:05:33,440
 If you want to take a precept against gambling power to you

109
00:05:33,440 --> 00:05:35,320
, I don't think the fifth precept,

110
00:05:35,320 --> 00:05:39,810
 as it's traditionally understood, is that sort of

111
00:05:39,810 --> 00:05:41,520
 determination.

112
00:05:41,520 --> 00:05:47,480
 I think a person who breaks that, who does gamble, is in

113
00:05:47,480 --> 00:05:50,400
 for trouble, but they're not

114
00:05:50,400 --> 00:05:51,800
 violating that precept.

115
00:05:51,800 --> 00:05:54,330
 If they've taken this determination that I'm not going to

116
00:05:54,330 --> 00:05:55,960
 take drugs and alcohol, well,

117
00:05:55,960 --> 00:05:58,120
 I don't think gambling is going to break that determination

118
00:05:58,120 --> 00:05:59,440
, because it's on a whole different

119
00:05:59,440 --> 00:06:01,160
 level.

120
00:06:01,160 --> 00:06:08,770
 Gaming I think is not nearly as deleterious, detrimental as

121
00:06:08,770 --> 00:06:12,360
 taking drugs and alcohol.

122
00:06:12,360 --> 00:06:18,640
 This is one thing.

123
00:06:18,640 --> 00:06:22,530
 How that relates to your question is that, well, that

124
00:06:22,530 --> 00:06:24,880
 basically, I disagree with this

125
00:06:24,880 --> 00:06:29,530
 interpretation, the idea that don't take them to the extent

126
00:06:29,530 --> 00:06:32,040
 that they lead to negligence.

127
00:06:32,040 --> 00:06:34,730
 The point that they're raising is that negligence is the

128
00:06:34,730 --> 00:06:37,600
 real problem, not the drugs and alcohol.

129
00:06:37,600 --> 00:06:43,310
 But you're taking this vow because you understand that they

130
00:06:43,310 --> 00:06:46,000
 are bases of negligence.

131
00:06:46,000 --> 00:06:49,400
 Anyone who's practiced meditation and then gone back and

132
00:06:49,400 --> 00:06:51,120
 taken drugs or alcohol will

133
00:06:51,120 --> 00:06:53,160
 tell you that even the smallest ...

134
00:06:53,160 --> 00:06:59,180
 If you take enough that it's not going to intoxicate you,

135
00:06:59,180 --> 00:07:02,400
 then you're not taking enough

136
00:07:02,400 --> 00:07:04,080
 to break the precept.

137
00:07:04,080 --> 00:07:07,210
 As soon as you take even a small amount of any intoxicant,

138
00:07:07,210 --> 00:07:09,440
 you become a little bit intoxicated,

139
00:07:09,440 --> 00:07:11,240
 and it does affect your brain.

140
00:07:11,240 --> 00:07:14,110
 It affects your ability to think clearly, to think r

141
00:07:14,110 --> 00:07:14,920
ationally.

142
00:07:14,920 --> 00:07:17,400
 It has an effect on your mind.

143
00:07:17,400 --> 00:07:22,440
 Now, as Matt said in answer to your question, what's the

144
00:07:22,440 --> 00:07:23,360
 point?

145
00:07:23,360 --> 00:07:26,850
 That's another important point to make is that the very

146
00:07:26,850 --> 00:07:28,640
 intention to take drugs and

147
00:07:28,640 --> 00:07:30,760
 alcohol is a negative one.

148
00:07:30,760 --> 00:07:34,330
 The very intention to imbibe poison, which is something

149
00:07:34,330 --> 00:07:36,400
 that is actually going to poison

150
00:07:36,400 --> 00:07:41,100
 your body and affect your ability of the mind to function,

151
00:07:41,100 --> 00:07:43,600
 is a negative intention in the

152
00:07:43,600 --> 00:07:44,600
 first place.

153
00:07:44,600 --> 00:07:47,730
 An enlightened being would never give rise to such an

154
00:07:47,730 --> 00:07:48,640
 intention.

155
00:07:48,640 --> 00:07:55,360
 Why would they need to take drugs or alcohol?

156
00:07:55,360 --> 00:08:00,430
 The implication, moreover, is that they would abstain from

157
00:08:00,430 --> 00:08:02,880
 it because of the very nature

158
00:08:02,880 --> 00:08:03,880
 of it.

159
00:08:03,880 --> 00:08:05,560
 This is something that poisons the body.

160
00:08:05,560 --> 00:08:08,490
 It's like ... Or it's something that poisons the mind as

161
00:08:08,490 --> 00:08:08,920
 well.

162
00:08:08,920 --> 00:08:18,800
 It's something that is done for the purpose of escaping.

163
00:08:18,800 --> 00:08:23,560
 The idea that an enlightened being could drink alcohol ...

164
00:08:23,560 --> 00:08:24,560
 One monk was trying to tell me

165
00:08:24,560 --> 00:08:28,590
 that a sotapana, because a sotapana apparently keeps all

166
00:08:28,590 --> 00:08:30,920
 the precepts fully intact, will

167
00:08:30,920 --> 00:08:33,800
 never break any one of the five precepts.

168
00:08:33,800 --> 00:08:35,800
 He said, "It will never reach their lips.

169
00:08:35,800 --> 00:08:36,800
 It will magically ... "

170
00:08:36,800 --> 00:08:39,740
 He said, "There's always something will happen that it can

171
00:08:39,740 --> 00:08:41,120
't go into their mouth."

172
00:08:41,120 --> 00:08:45,640
 He was telling me because I was in this Cambodian monastery

173
00:08:45,640 --> 00:08:48,160
 and these lay people had brought

174
00:08:48,160 --> 00:08:50,240
 this rice, this sweet rice.

175
00:08:50,240 --> 00:08:51,880
 I've never had it since.

176
00:08:51,880 --> 00:08:55,120
 I've never been presented with it since.

177
00:08:55,120 --> 00:08:56,120
 It was sweet rice.

178
00:08:56,120 --> 00:08:57,120
 I was like, "Oh, nice."

179
00:08:57,120 --> 00:08:59,200
 I started eating it.

180
00:08:59,200 --> 00:09:01,400
 I said, "There's alcohol in this."

181
00:09:01,400 --> 00:09:02,920
 The other monk said, "No, no, no."

182
00:09:02,920 --> 00:09:05,620
 The lay people were right there and they were getting a

183
00:09:05,620 --> 00:09:06,840
 little bit worried.

184
00:09:06,840 --> 00:09:08,560
 I said, "No, there's alcohol in this."

185
00:09:08,560 --> 00:09:11,080
 I was a young monk, so I didn't think to be a little bit

186
00:09:11,080 --> 00:09:12,200
 tactful about it.

187
00:09:12,200 --> 00:09:14,560
 I said, "No, there's alcohol in this."

188
00:09:14,560 --> 00:09:16,910
 The lay people were like, "Oh," because they had brought

189
00:09:16,910 --> 00:09:17,720
 this rice.

190
00:09:17,720 --> 00:09:18,720
 He said, "No, no, no.

191
00:09:18,720 --> 00:09:19,720
 There's no alcohol."

192
00:09:19,720 --> 00:09:20,720
 They were arguing.

193
00:09:20,720 --> 00:09:21,720
 I said, "Look."

194
00:09:21,720 --> 00:09:25,020
 I took a little bit more and I was like, "Look, how did

195
00:09:25,020 --> 00:09:26,600
 they make this rice?"

196
00:09:26,600 --> 00:09:27,600
 I said, "Well."

197
00:09:27,600 --> 00:09:31,150
 He said, "Well, they put sugar in the rice and whatever,

198
00:09:31,150 --> 00:09:32,600
 milk or whatever."

199
00:09:32,600 --> 00:09:34,960
 "No, not milk, but I think just sugar and rice."

200
00:09:34,960 --> 00:09:37,200
 Then they put it out in the sun.

201
00:09:37,200 --> 00:09:40,280
 It's like, "This is alcohol.

202
00:09:40,280 --> 00:09:41,440
 That's how you make alcohol."

203
00:09:41,440 --> 00:09:48,480
 I think I got the lay people really upset.

204
00:09:48,480 --> 00:09:50,680
 Then the monk told me, he said, "Oh, yes.

205
00:09:50,680 --> 00:09:54,000
 A soda panda would never touch their lips."

206
00:09:54,000 --> 00:09:56,920
 He was saying, "I don't eat that stuff.

207
00:09:56,920 --> 00:09:59,180
 It's not that I think it's against the precepts, but I just

208
00:09:59,180 --> 00:10:00,120
 naturally don't."

209
00:10:00,120 --> 00:10:04,280
 Believe me, he wasn't a soda panda.

210
00:10:04,280 --> 00:10:13,840
 I think maybe he thought he was.

211
00:10:13,840 --> 00:10:16,930
 The idea that an Arahant enlightened being might take

212
00:10:16,930 --> 00:10:18,240
 alcohol, I don't know.

213
00:10:18,240 --> 00:10:21,800
 I'm not going to argue that it couldn't happen, that you

214
00:10:21,800 --> 00:10:23,920
 could pry their lips open and force

215
00:10:23,920 --> 00:10:24,920
 them to drink it.

216
00:10:24,920 --> 00:10:32,400
 To me, the idea that they would do something so absurd as

217
00:10:32,400 --> 00:10:37,400
 to drink poison, especially poison

218
00:10:37,400 --> 00:10:43,160
 that affects your mental clarity, to me, it's an

219
00:10:43,160 --> 00:10:45,480
 impossibility.

220
00:10:45,480 --> 00:10:47,760
 Doesn't quite exactly answer your question.

221
00:10:47,760 --> 00:10:51,230
 Your question is whether it might not have an effect on

222
00:10:51,230 --> 00:10:51,840
 them.

223
00:10:51,840 --> 00:10:54,480
 An enlightened being, if they did drink alcohol, it wouldn

224
00:10:54,480 --> 00:11:07,800
't have an effect on their understanding

225
00:11:07,800 --> 00:11:11,000
 of reality.

226
00:11:11,000 --> 00:11:15,670
 The mind would become clouded, but the cloudiness would be

227
00:11:15,670 --> 00:11:17,640
 simply a cloudy mind.

228
00:11:17,640 --> 00:11:19,040
 There would be no judging of it.

229
00:11:19,040 --> 00:11:25,950
 It would be impossible for them to give rise to liking or

230
00:11:25,950 --> 00:11:27,840
 disliking.

231
00:11:27,840 --> 00:11:31,360
 The mind would still have a one-to-one relationship,

232
00:11:31,360 --> 00:11:33,480
 because the mind has at that point given up

233
00:11:33,480 --> 00:11:34,480
 all partiality.

234
00:11:34,480 --> 00:11:42,090
 It's given up such a great portion of existence, which is

235
00:11:42,090 --> 00:11:44,640
 the negative unwholesome portion

236
00:11:44,640 --> 00:11:45,640
 of existence.

237
00:11:45,640 --> 00:11:47,800
 It can't arise again.

238
00:11:47,800 --> 00:11:50,760
 Their mind will be cloudy, but it will just be cloudy.

239
00:11:50,760 --> 00:11:52,680
 That's if it could happen at all.

240
00:11:52,680 --> 00:11:54,680
 As I said, some people say it could never happen.

241
00:11:54,680 --> 00:11:55,200
 Who knows?

242
00:11:55,720 --> 00:11:56,720
 But who knows?

243
00:11:56,720 --> 00:11:58,720
 [END]

