1
00:00:00,000 --> 00:00:08,280
 Hey, good evening everyone.

2
00:00:08,280 --> 00:00:24,840
 Broadcasting live January 13th, 2016.

3
00:00:24,840 --> 00:00:48,280
 Today's quote is about charity.

4
00:00:48,280 --> 00:00:57,860
 It's a very strong, powerful statement about the power of

5
00:00:57,860 --> 00:00:59,880
 charity.

6
00:00:59,880 --> 00:01:17,400
 If we knew the benefit of giving gifts, we wouldn't eat

7
00:01:17,400 --> 00:01:25,480
 without giving gifts.

8
00:01:25,480 --> 00:01:52,000
 And this is because of the power of the mind, the power of

9
00:01:52,000 --> 00:01:54,480
 karma.

10
00:01:54,480 --> 00:02:11,730
 So, giving a gift is much more than just the benefit that

11
00:02:11,730 --> 00:02:12,840
 accrues to the person.

12
00:02:12,840 --> 00:02:18,600
 It's even much more than the higher opinion that they'll

13
00:02:18,600 --> 00:02:20,760
 then have for you.

14
00:02:20,760 --> 00:02:25,880
 Giving gifts changes, you know.

15
00:02:25,880 --> 00:02:28,280
 It's choosing a fork in the road, right?

16
00:02:28,280 --> 00:02:35,640
 Coming to a fork in the road and choosing a path.

17
00:02:35,640 --> 00:02:37,160
 This is a description of karma.

18
00:02:37,160 --> 00:02:40,680
 So the real issue here is the issue of karma.

19
00:02:40,680 --> 00:02:42,680
 It's not just this.

20
00:02:42,680 --> 00:02:46,840
 This is specifically about charity, which makes it a

21
00:02:46,840 --> 00:02:50,360
 profound statement in its own right.

22
00:02:50,360 --> 00:02:54,680
 It reminds us of the importance of being charitable.

23
00:02:54,680 --> 00:02:58,240
 And not just the importance, but the greatness.

24
00:02:58,240 --> 00:03:00,440
 It's not like it should be an obligation.

25
00:03:00,440 --> 00:03:04,240
 It's like if you only knew what good it did, it's something

26
00:03:04,240 --> 00:03:06,520
 that really, really does good

27
00:03:06,520 --> 00:03:08,520
 things for us.

28
00:03:08,520 --> 00:03:11,480
 But it gives so much.

29
00:03:11,480 --> 00:03:15,680
 It gives more than, first of all, it does give the goodness

30
00:03:15,680 --> 00:03:17,840
 to another person, which

31
00:03:17,840 --> 00:03:21,120
 is a great thing in itself, right?

32
00:03:21,120 --> 00:03:27,600
 You help someone else, you've done a great thing.

33
00:03:27,600 --> 00:03:34,400
 You've created happiness for that person.

34
00:03:34,400 --> 00:03:39,500
 And then the second thing it does is it inclines them

35
00:03:39,500 --> 00:03:41,040
 positively towards you.

36
00:03:41,040 --> 00:03:50,080
 So that's another great thing.

37
00:03:50,080 --> 00:03:53,390
 But even greater than all that is the effect that it has on

38
00:03:53,390 --> 00:03:55,120
 your mind, from making you

39
00:03:55,120 --> 00:04:01,760
 kind and charitable and more importantly, able to let go.

40
00:04:01,760 --> 00:04:10,560
 It makes you less clingy.

41
00:04:10,560 --> 00:04:20,480
 It reduces your attachments.

42
00:04:20,480 --> 00:04:32,300
 It frees you from stinginess and miserliness and desire and

43
00:04:32,300 --> 00:04:35,200
 addiction.

44
00:04:35,200 --> 00:04:36,640
 So charity is good.

45
00:04:36,640 --> 00:04:39,760
 But there's a deeper message here.

46
00:04:39,760 --> 00:04:44,480
 There's two deeper, there's levels of deeper messages.

47
00:04:44,480 --> 00:04:49,980
 So a deeper message is the message of karma, that our

48
00:04:49,980 --> 00:04:53,320
 actions have consequences.

49
00:04:53,320 --> 00:04:58,790
 If they have consequences no matter what, like if you don't

50
00:04:58,790 --> 00:05:01,160
 look both ways before crossing

51
00:05:01,160 --> 00:05:05,870
 the street, you get hit by a car or actions in and of

52
00:05:05,870 --> 00:05:08,400
 themselves have consequences even

53
00:05:08,400 --> 00:05:12,960
 before the mind is factored in.

54
00:05:12,960 --> 00:05:15,040
 There are some people who actually live by this.

55
00:05:15,040 --> 00:05:20,150
 They try and guess what's going to happen if they do this

56
00:05:20,150 --> 00:05:22,160
 or if they do that.

57
00:05:22,160 --> 00:05:23,920
 Some people become superstitious about it.

58
00:05:23,920 --> 00:05:30,340
 If I walk under this ladder, they blow the consequences out

59
00:05:30,340 --> 00:05:32,360
 of proportion.

60
00:05:32,360 --> 00:05:42,470
 Simple acts like walking where a black cat has walked or

61
00:05:42,470 --> 00:05:46,600
 smashing a mirror, etc.

62
00:05:46,600 --> 00:05:51,800
 But actions do have consequences.

63
00:05:51,800 --> 00:05:55,450
 If you break something that belongs to someone else, then

64
00:05:55,450 --> 00:05:57,360
 you have to replace it for them

65
00:05:57,360 --> 00:06:01,230
 even if it wasn't intentional, it becomes your

66
00:06:01,230 --> 00:06:02,960
 responsibility.

67
00:06:02,960 --> 00:06:09,030
 But that's not of course what we usually mean by karma

68
00:06:09,030 --> 00:06:15,280
 because that's fairly superficial.

69
00:06:15,280 --> 00:06:18,360
 The results sure they happen.

70
00:06:18,360 --> 00:06:24,360
 But the results for that in that sense are quite limited.

71
00:06:24,360 --> 00:06:29,530
 We're more interested in the results of mental action,

72
00:06:29,530 --> 00:06:33,360
 mental volition behind our actions.

73
00:06:33,360 --> 00:06:35,840
 Are we doing it out of greed?

74
00:06:35,840 --> 00:06:37,160
 Are we doing it out of anger?

75
00:06:37,160 --> 00:06:39,400
 Because these have real meaning.

76
00:06:39,400 --> 00:06:42,240
 This is where we really get in trouble.

77
00:06:42,240 --> 00:06:46,480
 Or if we're doing something out of mindfulness or wisdom or

78
00:06:46,480 --> 00:06:48,840
 compassion or love, then it's

79
00:06:48,840 --> 00:06:50,840
 really powerful.

80
00:06:50,840 --> 00:06:55,480
 Obviously, if it's clear that you did something wrong as a

81
00:06:55,480 --> 00:06:58,080
 mistake without realizing it, people

82
00:06:58,080 --> 00:07:00,080
 are quick to forgive.

83
00:07:00,080 --> 00:07:06,560
 But if you really meant to hurt someone, you're much less

84
00:07:06,560 --> 00:07:11,040
 likely to be granted forgiveness.

85
00:07:11,040 --> 00:07:14,800
 Likewise, if you give someone something, something, suppose

86
00:07:14,800 --> 00:07:17,640
 you're going to throw something out

87
00:07:17,640 --> 00:07:21,480
 and instead of throwing it out, you say, "Hey, do you want

88
00:07:21,480 --> 00:07:22,240
 this?"

89
00:07:22,240 --> 00:07:24,200
 Not much of a gift, right?

90
00:07:24,200 --> 00:07:29,820
 But if there's something that you really enjoy, like you

91
00:07:29,820 --> 00:07:32,640
 see that there's the last cookie

92
00:07:32,640 --> 00:07:36,150
 in the jar and you're just looking at it and you're

93
00:07:36,150 --> 00:07:38,800
 thinking, "I want that cookie."

94
00:07:38,800 --> 00:07:41,560
 But then someone comes along and says, "Oh wow, there's one

95
00:07:41,560 --> 00:07:42,440
 cookie left.

96
00:07:42,440 --> 00:07:43,440
 Can I have it?"

97
00:07:43,440 --> 00:07:46,680
 What do you do, right?

98
00:07:46,680 --> 00:07:52,820
 When you say to yourself, when you say to the person, "Yes,

99
00:07:52,820 --> 00:07:54,840
 you can have it."

100
00:07:54,840 --> 00:07:58,280
 That's quite powerful.

101
00:07:58,280 --> 00:08:01,930
 We have parents do this for their children, sacrificing so

102
00:08:01,930 --> 00:08:02,520
 much.

103
00:08:02,520 --> 00:08:10,880
 Some parents, of course, not all.

104
00:08:10,880 --> 00:08:14,360
 Be able to sacrifice like that.

105
00:08:14,360 --> 00:08:16,960
 That's just more powerful, is the point.

106
00:08:16,960 --> 00:08:23,160
 So a gift, not all gifts are the same, right?

107
00:08:23,160 --> 00:08:27,520
 Not all charity is the same.

108
00:08:27,520 --> 00:08:31,790
 But the really interesting meaning or interesting aspect of

109
00:08:31,790 --> 00:08:34,240
 this sentence is actually not about

110
00:08:34,240 --> 00:08:36,240
 karma or giving at all.

111
00:08:36,240 --> 00:08:38,600
 It's about knowledge.

112
00:08:38,600 --> 00:08:42,220
 What the Buddha is saying is, "You guys don't know the

113
00:08:42,220 --> 00:08:43,040
 truth."

114
00:08:43,040 --> 00:08:48,040
 He's looking at us and he's saying, "You guys are so blind.

115
00:08:48,040 --> 00:08:54,000
 You people on this earth, it's actually a condemnation, a

116
00:08:54,000 --> 00:08:56,200
 criticism of us, that we're

117
00:08:56,200 --> 00:08:59,680
 blind."

118
00:08:59,680 --> 00:09:01,160
 This is one way of looking at it, of course.

119
00:09:01,160 --> 00:09:04,000
 I'm not claiming to know the Buddha's intentions for

120
00:09:04,000 --> 00:09:07,560
 teaching and being able to read his intentions,

121
00:09:07,560 --> 00:09:14,320
 but it can very easily be understood as a challenge to us.

122
00:09:14,320 --> 00:09:16,320
 Wake up call.

123
00:09:16,320 --> 00:09:23,480
 This is just an example of how ignorant you people are.

124
00:09:23,480 --> 00:09:28,600
 When the opportunity to give arises, you don't give.

125
00:09:28,600 --> 00:09:32,930
 That shows how ignorant you are because if you knew the

126
00:09:32,930 --> 00:09:35,520
 benefits of giving, you would

127
00:09:35,520 --> 00:09:47,520
 just give naturally.

128
00:09:47,520 --> 00:09:50,590
 I suppose you could argue that, "Well, what's the purpose

129
00:09:50,590 --> 00:09:51,920
 of giving in the end?"

130
00:09:51,920 --> 00:09:54,080
 It doesn't really lead you to enlightenment, does it?

131
00:09:54,080 --> 00:09:58,420
 It's not really a practice because it doesn't lead to

132
00:09:58,420 --> 00:10:01,080
 wisdom, to give, and so on.

133
00:10:01,080 --> 00:10:06,990
 People argue this way, "Why did the Buddha talk about

134
00:10:06,990 --> 00:10:08,400
 giving?"

135
00:10:08,400 --> 00:10:12,970
 The meaning here is ... I mean, that really actually proves

136
00:10:12,970 --> 00:10:14,840
 the point that this is just

137
00:10:14,840 --> 00:10:20,060
 an example because you can say, "Well, why talk about

138
00:10:20,060 --> 00:10:23,240
 giving when giving's not really

139
00:10:23,240 --> 00:10:25,600
 essential?"

140
00:10:25,600 --> 00:10:29,460
 The point is, here you have people who want to be happy,

141
00:10:29,460 --> 00:10:31,680
 who want pleasure, who want to

142
00:10:31,680 --> 00:10:35,260
 go to heaven, or who even if they don't believe in heaven

143
00:10:35,260 --> 00:10:37,600
 or don't think about heaven, they

144
00:10:37,600 --> 00:10:39,320
 want heaven.

145
00:10:39,320 --> 00:10:41,600
 They want what heaven represents.

146
00:10:41,600 --> 00:10:47,270
 They want states like heavenly states, pleasurable states

147
00:10:47,270 --> 00:10:48,680
 in general.

148
00:10:48,680 --> 00:10:51,150
 And yet they don't do anything to get them, is what he's

149
00:10:51,150 --> 00:10:52,840
 saying, because pleasure only

150
00:10:52,840 --> 00:10:59,870
 comes from goodness, from mundane, wholesome state, wholes

151
00:10:59,870 --> 00:11:01,960
ome activity.

152
00:11:01,960 --> 00:11:04,310
 Here these people want to be happy, and yet they do all

153
00:11:04,310 --> 00:11:05,920
 sorts of things to make themselves

154
00:11:05,920 --> 00:11:08,360
 unhappy.

155
00:11:08,360 --> 00:11:10,680
 People don't know what leads to happiness.

156
00:11:10,680 --> 00:11:13,520
 They don't do the things that lead to happiness.

157
00:11:13,520 --> 00:11:16,840
 That's what he's saying.

158
00:11:16,840 --> 00:11:21,210
 So it's really deeper and quite interesting for meditators

159
00:11:21,210 --> 00:11:23,480
 and non-meditators alike.

160
00:11:23,480 --> 00:11:28,380
 For meditators, it reminds us of what's most important,

161
00:11:28,380 --> 00:11:31,920
 knowledge, wisdom, understanding.

162
00:11:31,920 --> 00:11:34,240
 That's what we're aiming for.

163
00:11:34,240 --> 00:11:40,080
 That's what we're working for.

164
00:11:40,080 --> 00:11:41,080
 Meditation isn't magic.

165
00:11:41,080 --> 00:11:45,970
 It's not something that's going to transform you or

166
00:11:45,970 --> 00:11:47,400
 something.

167
00:11:47,400 --> 00:11:51,320
 You're not just going to break away from samsara.

168
00:11:51,320 --> 00:11:52,720
 You're going to learn about samsara.

169
00:11:52,720 --> 00:11:53,830
 You're going to learn about reality and the unbecke

170
00:11:53,830 --> 00:11:57,680
 experience.

171
00:11:57,680 --> 00:12:02,850
 You're going to see through the cloud of ignorance and

172
00:12:02,850 --> 00:12:06,400
 delusion and realize these things like,

173
00:12:06,400 --> 00:12:11,800
 wow, giving is such a powerful thing.

174
00:12:11,800 --> 00:12:16,760
 Morality and ethical ethics is such a powerful thing.

175
00:12:16,760 --> 00:12:28,280
 Meditation is such a powerful thing.

176
00:12:28,280 --> 00:12:29,760
 We don't see the way the Buddha sees.

177
00:12:29,760 --> 00:12:31,600
 That's the key.

178
00:12:31,600 --> 00:12:36,890
 Once we see the way the Buddha sees, Buddha saw, once we

179
00:12:36,890 --> 00:12:40,040
 learn and we understand reality,

180
00:12:40,040 --> 00:12:48,190
 and we free ourselves from suffering, we find true

181
00:12:48,190 --> 00:12:50,160
 happiness.

182
00:12:50,160 --> 00:12:56,800
 So just a little bit of dhamma for tonight.

183
00:12:56,800 --> 00:13:01,800
 That's our quote.

184
00:13:01,800 --> 00:13:06,160
 Maybe tomorrow we'll do question and answer.

185
00:13:06,160 --> 00:13:07,160
 Maybe dhamma-pada.

186
00:13:07,160 --> 00:13:12,850
 No, we're doing one dhamma-pada a week, one question and

187
00:13:12,850 --> 00:13:17,000
 answer, and one Buddhism 101.

188
00:13:17,000 --> 00:13:24,600
 I think that's the new regimen.

189
00:13:24,600 --> 00:13:26,960
 I'm giving more five-minute meditation lessons.

190
00:13:26,960 --> 00:13:29,480
 I'm going to try to get someone to take pictures so I can

191
00:13:29,480 --> 00:13:30,680
 share with everybody.

192
00:13:30,680 --> 00:13:33,440
 You get to see what it's like.

193
00:13:33,440 --> 00:13:35,360
 It's really pretty awesome.

194
00:13:35,360 --> 00:13:37,400
 I must have taught 50 people.

195
00:13:37,400 --> 00:13:41,360
 I think I taught over 50 people how to meditate on

196
00:13:41,360 --> 00:13:42,520
 yesterday.

197
00:13:42,520 --> 00:13:46,280
 It's pretty good.

198
00:13:46,280 --> 00:13:55,560
 I gave out 50 booklets on how to meditate.

199
00:13:55,560 --> 00:14:00,520
 It worked just about as well as I could have hoped.

200
00:14:00,520 --> 00:14:04,720
 Anyway, so that's all for tonight.

201
00:14:04,720 --> 00:14:06,640
 Thank you all for tuning in.

