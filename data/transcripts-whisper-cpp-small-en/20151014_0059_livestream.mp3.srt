1
00:00:00,000 --> 00:00:06,240
 Good evening everyone.

2
00:00:06,240 --> 00:00:11,240
 Broadcasting Live, October 13th, 2015.

3
00:00:11,240 --> 00:00:28,160
 I'm going to go with another Dhammapada video tonight.

4
00:00:28,160 --> 00:00:35,160
 Testing, testing, testing, testing.

5
00:00:35,160 --> 00:00:47,440
 Hello and welcome back to our study of the Dhammapada.

6
00:00:47,440 --> 00:00:55,430
 Today we continue with verses 85 and 86, which read as

7
00:00:55,430 --> 00:00:57,160
 follows.

8
00:00:57,160 --> 00:01:04,930
 Apakati Manu Seisu, yejana par gamino, ata yang ita rapa ja

9
00:01:04,930 --> 00:01:08,160
 tir mein wannu dahwati,

10
00:01:08,160 --> 00:01:16,140
 yejko samadakati dami dhammanu wati no, tejana par mei s

11
00:01:16,140 --> 00:01:22,760
anti machudhe yang surutta.

12
00:01:22,760 --> 00:01:29,450
 These verses are a common chant in Thailand, so quite

13
00:01:29,450 --> 00:01:30,160
 familiar.

14
00:01:30,160 --> 00:01:33,360
 These are the next ones in the chapter.

15
00:01:33,360 --> 00:01:42,580
 So these two mean, apakati manu seisu, few are they among

16
00:01:42,580 --> 00:01:45,760
 humans, yejana par gamino,

17
00:01:45,760 --> 00:01:53,920
 who go to the farther shore, ata yang ita rapa ja.

18
00:01:53,920 --> 00:02:01,330
 Here these other humans, other people, tir mei wannu dahw

19
00:02:01,330 --> 00:02:03,520
ati, simply run up and down the

20
00:02:03,520 --> 00:02:05,520
 shore.

21
00:02:05,520 --> 00:02:21,450
 Yejko samadakati, but those who, samadakati dami, in

22
00:02:21,450 --> 00:02:25,600
 regards to the well-taught dhammanu

23
00:02:25,600 --> 00:02:35,670
 wati no, are those who go according to the damma, tejana

24
00:02:35,670 --> 00:02:38,040
 par mei santi.

25
00:02:38,040 --> 00:02:51,720
 These people go to the farther shore, machudhe yang surutta

26
00:02:51,720 --> 00:02:52,600
.

27
00:02:52,600 --> 00:02:59,050
 These cross to the other side, machudhe yang to the kingdom

28
00:02:59,050 --> 00:03:00,880
 of death, which is hard to

29
00:03:00,880 --> 00:03:06,600
 cross, which is very hard to cross.

30
00:03:06,600 --> 00:03:11,300
 So we have some imagery here, the imagery of crossing an

31
00:03:11,300 --> 00:03:13,680
 ocean, and now most people

32
00:03:13,680 --> 00:03:17,160
 just run up and down the shore.

33
00:03:17,160 --> 00:03:26,800
 Theiram is this shore, anudahwati is running up and down.

34
00:03:26,800 --> 00:03:31,160
 So this was given in regards to another very short story.

35
00:03:31,160 --> 00:03:37,350
 It's rather more of a remark that the Buddha gave in

36
00:03:37,350 --> 00:03:39,680
 regards to the habits of those who

37
00:03:39,680 --> 00:03:41,240
 would come and listen to the dhamma.

38
00:03:41,240 --> 00:03:46,010
 It seems that there was a specific group of people who came

39
00:03:46,010 --> 00:03:48,200
 to listen to the Buddha's

40
00:03:48,200 --> 00:03:49,600
 teaching.

41
00:03:49,600 --> 00:03:53,360
 They had done good deeds, or they had supported the

42
00:03:53,360 --> 00:03:56,560
 monastery, and so they came and gave alms

43
00:03:56,560 --> 00:04:00,270
 to the monks and then stuck around intending to listen to

44
00:04:00,270 --> 00:04:01,920
 the dhamma all night.

45
00:04:01,920 --> 00:04:05,260
 There would be teachings of the dhamma where the monks

46
00:04:05,260 --> 00:04:07,360
 would take turns giving talks all

47
00:04:07,360 --> 00:04:10,450
 night or maybe even one monk would talk all night, probably

48
00:04:10,450 --> 00:04:11,240
 the former.

49
00:04:11,240 --> 00:04:16,360
 Probably it would be several monks taking turns.

50
00:04:16,360 --> 00:04:21,240
 We once did this in Thailand, something like this.

51
00:04:21,240 --> 00:04:27,440
 There were some ancient Buddha statues that were stolen.

52
00:04:27,440 --> 00:04:29,890
 They were actually kept behind bars, they were quite

53
00:04:29,890 --> 00:04:30,520
 valuable.

54
00:04:30,520 --> 00:04:34,210
 They were kept behind bars and someone actually cut through

55
00:04:34,210 --> 00:04:36,320
 the bars or bent the bars or something

56
00:04:36,320 --> 00:04:37,600
 to steal these Buddha images.

57
00:04:37,600 --> 00:04:44,520
 Cut through the bars I think to steal the Buddha images.

58
00:04:44,520 --> 00:04:51,240
 Somewhat miraculously one of them was found at the bottom

59
00:04:51,240 --> 00:04:54,600
 of one of the rivers I guess,

60
00:04:54,600 --> 00:04:58,630
 like a small river when it dried up or something in the dry

61
00:04:58,630 --> 00:04:59,520
 season.

62
00:04:59,520 --> 00:05:02,280
 It was found.

63
00:05:02,280 --> 00:05:06,720
 They pulled it up and brought it back and had an all night

64
00:05:06,720 --> 00:05:07,840
 ceremony.

65
00:05:07,840 --> 00:05:10,240
 It was something like this.

66
00:05:10,240 --> 00:05:12,240
 The details are imprecise.

67
00:05:12,240 --> 00:05:15,370
 I maybe never even paid too much attention to the details,

68
00:05:15,370 --> 00:05:16,720
 but it was something like

69
00:05:16,720 --> 00:05:17,720
 that.

70
00:05:17,720 --> 00:05:22,780
 We had an all night ceremony to honor the Buddha or maybe

71
00:05:22,780 --> 00:05:25,440
 to reestablish its sanctity

72
00:05:25,440 --> 00:05:26,680
 or something, its holiness.

73
00:05:26,680 --> 00:05:29,270
 I mean it was a very cultural sort of thing that I didn't

74
00:05:29,270 --> 00:05:30,800
 pay too much attention to.

75
00:05:30,800 --> 00:05:33,410
 It may have been broken or something and there was a fixing

76
00:05:33,410 --> 00:05:34,600
 that had to go on and at the

77
00:05:34,600 --> 00:05:40,600
 end of fixing it you have to re-bless it or something.

78
00:05:40,600 --> 00:05:43,050
 But it was kind of considered a miracle that they got it

79
00:05:43,050 --> 00:05:43,480
 back.

80
00:05:43,480 --> 00:05:45,960
 So it was a big deal.

81
00:05:45,960 --> 00:05:49,230
 All of the monks, I was a part of it, we would take turns

82
00:05:49,230 --> 00:05:51,280
 chanting or giving talks or that

83
00:05:51,280 --> 00:05:55,680
 kind of thing all night until the sun rose.

84
00:05:55,680 --> 00:05:58,640
 So the spirit of this still goes on.

85
00:05:58,640 --> 00:06:03,160
 This idea of an all night dhamma thing.

86
00:06:03,160 --> 00:06:05,860
 But it was sort of, I think it was Ajahn Dang that put it

87
00:06:05,860 --> 00:06:06,600
 together.

88
00:06:06,600 --> 00:06:08,920
 I don't know how often it happens.

89
00:06:08,920 --> 00:06:12,950
 I know in Burma they do all night chantings and all night

90
00:06:12,950 --> 00:06:15,280
 teachings from what I hear.

91
00:06:15,280 --> 00:06:19,430
 Anyway the situation here was all of them were trying to

92
00:06:19,430 --> 00:06:21,680
 listen to the dhamma but none

93
00:06:21,680 --> 00:06:28,210
 of them lasted or maybe not none of them but many of them

94
00:06:28,210 --> 00:06:31,120
 were unable to last.

95
00:06:31,120 --> 00:06:34,180
 None of them, they were unable to listen to the dhamma all

96
00:06:34,180 --> 00:06:34,760
 night.

97
00:06:34,760 --> 00:06:43,250
 Some of them were overcome by lust, sexual passion and went

98
00:06:43,250 --> 00:06:44,480
 home.

99
00:06:44,480 --> 00:06:52,920
 Some of them were overcome by anger, boredom maybe.

100
00:06:52,920 --> 00:06:55,840
 Anger is a very different kind, many different kinds.

101
00:06:55,840 --> 00:07:00,210
 It can be boredom, it can be dislike of the dhamma

102
00:07:00,210 --> 00:07:03,560
 teachings, it can be self hatred at

103
00:07:03,560 --> 00:07:09,710
 the idea that every time you hear something that hits too

104
00:07:09,710 --> 00:07:13,040
 close to home, maybe they're

105
00:07:13,040 --> 00:07:15,310
 talking about things that are unwholesome and you realize

106
00:07:15,310 --> 00:07:16,440
 that you are engaged in some

107
00:07:16,440 --> 00:07:19,950
 of those and so you get angry at yourself and feel guilty

108
00:07:19,950 --> 00:07:21,560
 and all these things.

109
00:07:21,560 --> 00:07:25,240
 So being overcome with that they were unable to stay and

110
00:07:25,240 --> 00:07:26,480
 they went home.

111
00:07:26,480 --> 00:07:35,880
 And some of them were overcome by mana, conceit, so that

112
00:07:35,880 --> 00:07:41,160
 would be the feeling self righteous

113
00:07:41,160 --> 00:07:45,790
 and displeased by the dhamma but not an angry thing, just

114
00:07:45,790 --> 00:07:48,040
 sort of feeling like they knew

115
00:07:48,040 --> 00:07:50,700
 better than the monks or these monks don't know what they

116
00:07:50,700 --> 00:07:52,440
're talking about, being attached

117
00:07:52,440 --> 00:07:55,360
 to views and that kind of thing.

118
00:07:55,360 --> 00:07:59,930
 Some of them got caught up with tinamida so they got tired,

119
00:07:59,930 --> 00:08:01,840
 that's a common one and were

120
00:08:01,840 --> 00:08:04,870
 nodding off and said I realized that they had to go home is

121
00:08:04,870 --> 00:08:06,160
 better and so they went

122
00:08:06,160 --> 00:08:27,440
 home in the middle of the teaching.

123
00:08:27,440 --> 00:08:32,360
 The next day the monks gathered and commented on this and

124
00:08:32,360 --> 00:08:35,080
 just were sort of remarking on

125
00:08:35,080 --> 00:08:39,050
 how difficult it is for people to listen to the dhamma and

126
00:08:39,050 --> 00:08:41,000
 how people aren't all that

127
00:08:41,000 --> 00:08:48,200
 keen to take the opportunity to listen to the dhamma.

128
00:08:48,200 --> 00:08:50,380
 And the Buddha said monks for the most of the Buddha came

129
00:08:50,380 --> 00:08:51,520
 in and asked them what they

130
00:08:51,520 --> 00:08:56,130
 were talking about and when they told him he remarked that

131
00:08:56,130 --> 00:08:57,920
 this is common for people

132
00:08:57,920 --> 00:09:08,490
 who said for the most part, for the most part they are

133
00:09:08,490 --> 00:09:11,840
 dependent upon or they are attached

134
00:09:11,840 --> 00:09:14,840
 to existence becoming.

135
00:09:14,840 --> 00:09:25,150
 Bhavaisu eva laga viharanthi, they dwell stuck, stuck in bh

136
00:09:25,150 --> 00:09:29,760
ava, stuck in becoming.

137
00:09:29,760 --> 00:09:33,290
 The meaning here bhava is a sort of a neutral term that's

138
00:09:33,290 --> 00:09:35,840
 sort of a blanket statement for

139
00:09:35,840 --> 00:09:39,600
 anything that leads to further becoming.

140
00:09:39,600 --> 00:09:43,110
 So when you want something on a very basic level that leads

141
00:09:43,110 --> 00:09:44,560
 to becoming here and now

142
00:09:44,560 --> 00:09:50,600
 you give rise to something new which is the plan to get

143
00:09:50,600 --> 00:09:52,640
 what you want.

144
00:09:52,640 --> 00:09:56,620
 You want to eat and so suddenly the plan to make food

145
00:09:56,620 --> 00:09:57,560
 arises.

146
00:09:57,560 --> 00:10:01,610
 Maybe you want to watch a movie so you plan to go out and

147
00:10:01,610 --> 00:10:03,920
 to the, I guess people don't

148
00:10:03,920 --> 00:10:10,840
 go to the video store anymore.

149
00:10:10,840 --> 00:10:16,240
 Whatever you want, you want to go to Thailand and so you

150
00:10:16,240 --> 00:10:19,560
 give rise to something, this, you

151
00:10:19,560 --> 00:10:24,290
 give rise to further becoming something that's not just

152
00:10:24,290 --> 00:10:27,640
 functional but it's actually a desire

153
00:10:27,640 --> 00:10:35,440
 based, a whole new set of variables.

154
00:10:35,440 --> 00:10:38,820
 Or if you're angry you give rise to something new as well,

155
00:10:38,820 --> 00:10:40,560
 at the very least a headache

156
00:10:40,560 --> 00:10:45,310
 but you can also out of anger give rise to argument,

157
00:10:45,310 --> 00:10:48,760
 friction, conflict, war even.

158
00:10:48,760 --> 00:10:53,250
 Of course war can be caused by greed and often it's caused

159
00:10:53,250 --> 00:10:54,880
 simply by greed.

160
00:10:54,880 --> 00:11:02,460
 Concede you give rise to this competition or this sense of

161
00:11:02,460 --> 00:11:06,200
 this conflict based on power

162
00:11:06,200 --> 00:11:11,900
 struggle and desire for dominance, oppression, this kind of

163
00:11:11,900 --> 00:11:12,840
 thing.

164
00:11:12,840 --> 00:11:15,160
 Oppressing others, belittling others, that kind of thing.

165
00:11:15,160 --> 00:11:21,550
 You create something new or you'd simply create a new plan

166
00:11:21,550 --> 00:11:23,280
 for yourself.

167
00:11:23,280 --> 00:11:29,340
 I deserve to be king so I'm going to fight my way to become

168
00:11:29,340 --> 00:11:30,360
 king.

169
00:11:30,360 --> 00:11:38,200
 I deserve this or that or I don't deserve this or that and

170
00:11:38,200 --> 00:11:42,280
 so you strive accordingly.

171
00:11:42,280 --> 00:11:46,260
 Sometimes it's just out of laziness, this gives rise to

172
00:11:46,260 --> 00:11:48,280
 sleep but it also gives rise

173
00:11:48,280 --> 00:11:53,620
 to sickness, it gives rise to conflict and problems when

174
00:11:53,620 --> 00:11:56,280
 you don't act according to your

175
00:11:56,280 --> 00:12:00,890
 duties where you just consume but don't produce and as a

176
00:12:00,890 --> 00:12:03,760
 result people become upset because

177
00:12:03,760 --> 00:12:12,960
 you're simply a consumer, a mooch, lazy, etc. and so on.

178
00:12:12,960 --> 00:12:16,470
 So this is the idea of becoming, it's really anything that

179
00:12:16,470 --> 00:12:18,560
 leads to, so there was a question

180
00:12:18,560 --> 00:12:22,950
 recently about what's wrong with, there's always questions

181
00:12:22,950 --> 00:12:24,880
 about what's wrong with seeking

182
00:12:24,880 --> 00:12:30,430
 out pleasure, ordinary pleasure because you do get some and

183
00:12:30,430 --> 00:12:33,120
 it's really this, these two

184
00:12:33,120 --> 00:12:40,660
 verses point to the mindset or the outlook of the Buddha

185
00:12:40,660 --> 00:12:44,280
 and of Buddhists in general

186
00:12:44,280 --> 00:12:47,450
 is that it's not enough and it's not sustainable because

187
00:12:47,450 --> 00:12:49,480
 the more you want, as I said, the

188
00:12:49,480 --> 00:12:54,380
 more you suffer, the less you get or the less satisfied you

189
00:12:54,380 --> 00:12:56,800
 are and in fact it's the wrong

190
00:12:56,800 --> 00:12:59,000
 way, it's unsustainable.

191
00:12:59,000 --> 00:13:02,210
 You find yourself bouncing back and forth or for a time you

192
00:13:02,210 --> 00:13:03,760
're able to balance things

193
00:13:03,760 --> 00:13:07,720
 before you fall one way or the other and then it's back and

194
00:13:07,720 --> 00:13:09,920
 forth again and it's building

195
00:13:09,920 --> 00:13:19,470
 up disappointment, building up attachment and stress or it

196
00:13:19,470 --> 00:13:23,440
's working for a time to either

197
00:13:23,440 --> 00:13:28,380
 repress, often it's simply to repress our desires and we

198
00:13:28,380 --> 00:13:30,760
 fluctuate back and forth and

199
00:13:30,760 --> 00:13:33,930
 go through life or we go through lives again and again and

200
00:13:33,930 --> 00:13:35,600
 again or born old sick die,

201
00:13:35,600 --> 00:13:41,520
 born old sick die.

202
00:13:41,520 --> 00:13:47,330
 So this is sort of the outlook that the Buddha's basing

203
00:13:47,330 --> 00:13:48,540
 this on.

204
00:13:48,540 --> 00:13:53,330
 These two verses are actually, one thing that stands out

205
00:13:53,330 --> 00:13:56,560
 for me besides the obvious symbolism

206
00:13:56,560 --> 00:14:03,880
 or imagery which is quite powerful, is sort of congrat

207
00:14:03,880 --> 00:14:05,520
ulatory.

208
00:14:05,520 --> 00:14:09,840
 It seemed like a good opportunity to congratulate all of

209
00:14:09,840 --> 00:14:14,560
 the people involved with this community,

210
00:14:14,560 --> 00:14:18,140
 all of the listeners, all of you who are listening to the D

211
00:14:18,140 --> 00:14:20,520
hamma, taking the time, some of you

212
00:14:20,520 --> 00:14:22,800
 every day too.

213
00:14:22,800 --> 00:14:24,360
 It's not something I never thought would happen.

214
00:14:24,360 --> 00:14:28,210
 I thought okay once a week maybe but some people actually

215
00:14:28,210 --> 00:14:30,160
 come out every day to listen

216
00:14:30,160 --> 00:14:32,600
 to the Dhamma.

217
00:14:32,600 --> 00:14:37,930
 You take the time to come online and sit still for an hour,

218
00:14:37,930 --> 00:14:40,280
 half an hour, an hour, even to

219
00:14:40,280 --> 00:14:45,130
 ask questions so to get involved, to perform this act of

220
00:14:45,130 --> 00:14:47,680
 good karma of asking questions

221
00:14:47,680 --> 00:14:52,050
 and many of you are very respectful and being respectful

222
00:14:52,050 --> 00:14:54,640
 and all sorts of good karma going

223
00:14:54,640 --> 00:14:58,680
 on.

224
00:14:58,680 --> 00:15:03,740
 And that's something to be proud of because even in the

225
00:15:03,740 --> 00:15:08,640
 context of the story, many people,

226
00:15:08,640 --> 00:15:12,200
 in the context of the story, these people are still to be

227
00:15:12,200 --> 00:15:14,120
 graduated because they tried

228
00:15:14,120 --> 00:15:17,060
 and it's not like they didn't hear some of the Dhamma, it's

229
00:15:17,060 --> 00:15:18,520
 just they tried to do something

230
00:15:18,520 --> 00:15:24,410
 beyond their capabilities but all of the people here should

231
00:15:24,410 --> 00:15:27,960
 be congratulated as well for having

232
00:15:27,960 --> 00:15:30,920
 the good intentions.

233
00:15:30,920 --> 00:15:35,300
 Many people don't even think about crossing or if they

234
00:15:35,300 --> 00:15:38,080
 think about crossing they never

235
00:15:38,080 --> 00:15:39,840
 do anything to try to cross.

236
00:15:39,840 --> 00:15:41,920
 They would never come and meditate.

237
00:15:41,920 --> 00:15:45,690
 If you look at the list of meditators that we have, you can

238
00:15:45,690 --> 00:15:48,600
 see how many we have tonight.

239
00:15:48,600 --> 00:15:52,320
 We've got actually a fairly small list today.

240
00:15:52,320 --> 00:15:57,560
 Uh oh and a bunch of orange people, maybe I spoke too soon.

241
00:15:57,560 --> 00:15:59,160
 What happened today?

242
00:15:59,160 --> 00:16:04,060
 Well, still we have many many people and we have people med

243
00:16:04,060 --> 00:16:06,400
itating on this side around

244
00:16:06,400 --> 00:16:09,760
 the clock.

245
00:16:09,760 --> 00:16:16,600
 People coming to listen to the Dhamma every day.

246
00:16:16,600 --> 00:16:19,600
 So this is a sign of at least wanting to cross.

247
00:16:19,600 --> 00:16:23,170
 Maybe some people just listen to the talks or watch my

248
00:16:23,170 --> 00:16:25,600
 YouTube videos and maybe dip their

249
00:16:25,600 --> 00:16:27,920
 foot in the water but don't ever cross.

250
00:16:27,920 --> 00:16:32,160
 But at the very least there's some thought and this is the

251
00:16:32,160 --> 00:16:33,320
 first step.

252
00:16:33,320 --> 00:16:39,370
 The first step is thinking about it, having the, giving

253
00:16:39,370 --> 00:16:43,120
 rise to the intention or the desire

254
00:16:43,120 --> 00:16:47,150
 to better oneself as opposed to just running up and down

255
00:16:47,150 --> 00:16:48,200
 the shore.

256
00:16:48,200 --> 00:16:51,320
 Because that's what it's like.

257
00:16:51,320 --> 00:16:54,050
 That's what the Buddha likens most of our activity to, just

258
00:16:54,050 --> 00:16:55,200
 running up and down the

259
00:16:55,200 --> 00:16:57,800
 shore.

260
00:16:57,800 --> 00:17:01,800
 Sometimes running up and down out of desire, sometimes

261
00:17:01,800 --> 00:17:04,240
 running up and down out of anger,

262
00:17:04,240 --> 00:17:07,720
 fear, worry, conceit.

263
00:17:07,720 --> 00:17:18,800
 We have many ways of running around in circles.

264
00:17:18,800 --> 00:17:21,040
 Running around in circles in the kingdom of death.

265
00:17:21,040 --> 00:17:26,760
 This is where we find ourselves being born again and again,

266
00:17:26,760 --> 00:17:29,240
 dying again and again.

267
00:17:29,240 --> 00:17:32,240
 Learning and forgetting.

268
00:17:32,240 --> 00:17:41,000
 Not really learning from our mistakes.

269
00:17:41,000 --> 00:17:45,330
 Our intention here, our practice here is to break that, to

270
00:17:45,330 --> 00:17:48,280
 cross over, to go beyond, to

271
00:17:48,280 --> 00:17:52,610
 go beyond death, to figure the system out, to come to

272
00:17:52,610 --> 00:17:56,000
 understand the system and to understand

273
00:17:56,000 --> 00:18:00,200
 becoming and as a result not be dependent upon it.

274
00:18:00,200 --> 00:18:02,770
 One way of explaining why we're dependent upon it is

275
00:18:02,770 --> 00:18:05,120
 because we don't understand it.

276
00:18:05,120 --> 00:18:09,480
 We're dependent upon it because it runs us.

277
00:18:09,480 --> 00:18:10,480
 We don't run it.

278
00:18:10,480 --> 00:18:14,080
 We don't lord over death.

279
00:18:14,080 --> 00:18:18,920
 Death lords over us becoming as well.

280
00:18:18,920 --> 00:18:27,970
 We act in such a way usually that we don't fully understand

281
00:18:27,970 --> 00:18:29,360
 the system.

282
00:18:29,360 --> 00:18:35,480
 We don't fully grasp what we're dealing with or what's in

283
00:18:35,480 --> 00:18:38,760
 store for us when we chase after

284
00:18:38,760 --> 00:18:39,760
 things.

285
00:18:39,760 --> 00:18:44,430
 We're not clear in our minds as to the nature of our add

286
00:18:44,430 --> 00:18:47,480
ictions and our aversions and our

287
00:18:47,480 --> 00:18:50,120
 conceits and our views.

288
00:18:50,120 --> 00:18:52,580
 We hold on to them often without a clear understanding or

289
00:18:52,580 --> 00:18:54,600
 usually without a clear understanding.

290
00:18:54,600 --> 00:18:59,240
 Once we understand them clearly, this is the crossing over.

291
00:18:59,240 --> 00:19:07,840
 This is the rising above.

292
00:19:07,840 --> 00:19:12,110
 It's rather a poetic verse, a simple story, simple verse

293
00:19:12,110 --> 00:19:14,200
 really, simple meaning.

294
00:19:14,200 --> 00:19:22,720
 It's kind of hard teaching.

295
00:19:22,720 --> 00:19:25,260
 It lays bare the fact that most of what we do is just

296
00:19:25,260 --> 00:19:27,120
 running up and down the shore.

297
00:19:27,120 --> 00:19:31,560
 It doesn't really accomplish anything in the end.

298
00:19:31,560 --> 00:19:33,890
 But we should, on the other hand, it should be an

299
00:19:33,890 --> 00:19:35,960
 encouraging teaching because whatever

300
00:19:35,960 --> 00:19:39,910
 the good things that we do, this is our coming closer to be

301
00:19:39,910 --> 00:19:42,040
 one of those few people who is

302
00:19:42,040 --> 00:19:44,240
 actually able to cross over.

303
00:19:44,240 --> 00:19:48,130
 They're actually able to rise above and free themselves

304
00:19:48,130 --> 00:19:50,560
 from the wheel of suffering, from

305
00:19:50,560 --> 00:19:53,960
 the kingdom of death.

306
00:19:53,960 --> 00:19:58,340
 Good work everyone and thank you all for tuning in and for

307
00:19:58,340 --> 00:20:01,040
 supporting these teachings with

308
00:20:01,040 --> 00:20:05,550
 your practice, for practicing and continuing on this

309
00:20:05,550 --> 00:20:06,720
 practice.

310
00:20:06,720 --> 00:20:09,200
 So that's the Dhammapada teaching for tonight.

311
00:20:09,200 --> 00:20:10,920
 Thank you all for tuning in.

312
00:20:10,920 --> 00:20:12,160
 Keep practicing and be well.

313
00:20:12,160 --> 00:20:35,440
 Stefanos is with me again tonight.

314
00:20:35,440 --> 00:20:36,440
 Hello.

315
00:20:36,440 --> 00:20:43,120
 Thank you, Robin's place.

316
00:20:43,120 --> 00:20:44,120
 Have any questions?

317
00:20:44,120 --> 00:20:45,120
 Yes.

318
00:20:45,120 --> 00:20:50,340
 First question is, the cat charity I do some volunteer work

319
00:20:50,340 --> 00:20:53,120
 for, running a raffle to raise

320
00:20:53,120 --> 00:20:54,120
 cash.

321
00:20:54,120 --> 00:20:57,430
 You say that gambling is not in line with the teachings,

322
00:20:57,430 --> 00:20:59,120
 but if the money raised goes

323
00:20:59,120 --> 00:21:03,880
 towards helping running something good and makes everyone

324
00:21:03,880 --> 00:21:06,120
 feel good, would that make

325
00:21:06,120 --> 00:21:07,120
 you feel good?

326
00:21:07,120 --> 00:21:08,120
 I think so.

327
00:21:08,120 --> 00:21:12,300
 I mean, no one's going to come out saying, feeling bad

328
00:21:12,300 --> 00:21:14,960
 about having donated, right?

329
00:21:14,960 --> 00:21:18,960
 No one's going to come out saying, if only I had, you know,

330
00:21:18,960 --> 00:21:20,920
 it's not, it's not exactly

331
00:21:20,920 --> 00:21:22,000
 the same as gambling.

332
00:21:22,000 --> 00:21:23,000
 It's not perfect.

333
00:21:23,000 --> 00:21:26,400
 And I wouldn't say you're scot free from all the unwholes

334
00:21:26,400 --> 00:21:28,240
omeness of gambling because

335
00:21:28,240 --> 00:21:31,160
 you're encouraging greed in people still.

336
00:21:31,160 --> 00:21:32,680
 It's like a white and black karma.

337
00:21:32,680 --> 00:21:35,050
 It's definitely a good example of a white and black karma

338
00:21:35,050 --> 00:21:37,200
 because people are doing it

339
00:21:37,200 --> 00:21:43,720
 out of desire to help, but also out of the desire to win.

340
00:21:43,720 --> 00:21:45,930
 It's you know, for most people, I don't think it's such a

341
00:21:45,930 --> 00:21:47,000
 big deal that they win.

342
00:21:47,000 --> 00:21:50,410
 They're just, it's a way of encouraging people, encouraging

343
00:21:50,410 --> 00:21:52,280
 the people's intention, but you're

344
00:21:52,280 --> 00:21:54,880
 encouraging it with, with a little bit of greed or

345
00:21:54,880 --> 00:21:56,760
 excitement for, Oh, what if I did

346
00:21:56,760 --> 00:21:58,680
 when wouldn't that be kind of neat?

347
00:21:58,680 --> 00:22:02,230
 I don't think most people buy raffle tickets thinking, yeah

348
00:22:02,230 --> 00:22:04,360
, I really want to win that,

349
00:22:04,360 --> 00:22:05,360
 but I don't know.

350
00:22:05,360 --> 00:22:06,360
 Really.

351
00:22:06,360 --> 00:22:09,340
 I think many people just do it to support the charity,

352
00:22:09,340 --> 00:22:10,680
 which is a good part.

353
00:22:10,680 --> 00:22:15,220
 Still, it's not, not how I would do things, not how I would

354
00:22:15,220 --> 00:22:17,440
 support a charity, but try

355
00:22:17,440 --> 00:22:20,440
 to get us charity supported.

356
00:22:20,440 --> 00:22:26,220
 When I am talking to someone in my thoughts while med

357
00:22:26,220 --> 00:22:30,480
itating, should I acknowledge this

358
00:22:30,480 --> 00:22:33,480
 as talking or thinking?

359
00:22:33,480 --> 00:22:34,480
 Thanks.

360
00:22:34,480 --> 00:22:37,480
 When you were talking to someone?

361
00:22:37,480 --> 00:22:40,890
 I think when they're talking to someone in their head, I

362
00:22:40,890 --> 00:22:46,480
 think, going over a conversation

363
00:22:46,480 --> 00:22:47,480
 or something.

364
00:22:47,480 --> 00:22:49,840
 Yeah, I would still say thinking, right?

365
00:22:49,840 --> 00:22:51,600
 Going over a conversation is more thinking.

366
00:22:51,600 --> 00:22:53,320
 If you want, you can say talking again.

367
00:22:53,320 --> 00:22:56,080
 The word is not the most important thing.

368
00:22:56,080 --> 00:22:59,140
 Which word you pick, it's just something that keeps you

369
00:22:59,140 --> 00:23:00,000
 objective.

370
00:23:00,000 --> 00:23:03,650
 So it's not damn that person who I talked with or boy, I

371
00:23:03,650 --> 00:23:06,040
 really love that person, really

372
00:23:06,040 --> 00:23:09,480
 attracted to that person, et cetera, et cetera.

373
00:23:09,480 --> 00:23:12,200
 Keeps you, whatever keeps you objective.

374
00:23:12,200 --> 00:23:15,370
 So if it's talking, just say talking, talking, and you'll

375
00:23:15,370 --> 00:23:17,280
 see that, Oh, it's actually just

376
00:23:17,280 --> 00:23:18,280
 a thought.

377
00:23:18,280 --> 00:23:20,950
 Your thinking is better because you'll see that it's just a

378
00:23:20,950 --> 00:23:21,560
 thought.

379
00:23:21,560 --> 00:23:26,230
 Or if they mean when they're actually talking to someone,

380
00:23:26,230 --> 00:23:29,560
 what should they do in their thoughts?

381
00:23:29,560 --> 00:23:30,800
 How should they be mindful?

382
00:23:30,800 --> 00:23:36,440
 You can be mindful of your lips moving, feeling, feeling.

383
00:23:36,440 --> 00:23:40,610
 Be mindful of the feelings in the body, the emotions, the

384
00:23:40,610 --> 00:23:42,800
 thoughts, the intentions to

385
00:23:42,800 --> 00:23:45,640
 speak.

386
00:23:45,640 --> 00:23:50,750
 Your mind is pretty quick, so you can do this all and still

387
00:23:50,750 --> 00:23:52,640
 continue to talk.

388
00:23:52,640 --> 00:23:59,580
 When I had emotions, for example, anger, I know anger, and

389
00:23:59,580 --> 00:24:02,640
 then I just know feeling,

390
00:24:02,640 --> 00:24:05,770
 feeling, because it seems that there isn't anger, but just

391
00:24:05,770 --> 00:24:06,640
 sensations.

392
00:24:06,640 --> 00:24:09,870
 Is this correct noting, or should I keep noting anger

393
00:24:09,870 --> 00:24:11,400
 because it was anger?

394
00:24:11,400 --> 00:24:12,400
 That's awesome.

395
00:24:12,400 --> 00:24:13,400
 Yeah.

396
00:24:13,400 --> 00:24:14,400
 No, that's very clear.

397
00:24:14,400 --> 00:24:15,560
 The emotions are like that.

398
00:24:15,560 --> 00:24:19,240
 They have physical counterparts, physical manifestations

399
00:24:19,240 --> 00:24:20,960
 and physical effects, and they

400
00:24:20,960 --> 00:24:24,670
 are distinct from the anger and the greed and the worry and

401
00:24:24,670 --> 00:24:26,280
 the fear and et cetera,

402
00:24:26,280 --> 00:24:27,280
 et cetera.

403
00:24:27,280 --> 00:24:28,280
 So acknowledge them separate.

404
00:24:28,280 --> 00:24:29,280
 Absolutely.

405
00:24:29,280 --> 00:24:29,970
 Sometimes during sitting meditation, I get very distracted

406
00:24:29,970 --> 00:24:30,280
 because I'm tired.

407
00:24:30,280 --> 00:24:31,280
 I start to fall asleep.

408
00:24:31,280 --> 00:24:45,160
 I find that I can focus better if my eyes are open.

409
00:24:45,160 --> 00:24:51,470
 Does this take away from the point of meditation, or is

410
00:24:51,470 --> 00:24:53,080
 this okay?

411
00:24:53,080 --> 00:24:54,080
 Yeah, it's okay.

412
00:24:54,080 --> 00:24:55,080
 It's not wrong.

413
00:24:55,080 --> 00:24:58,150
 It's better to have your eyes closed when you can, but if

414
00:24:58,150 --> 00:24:59,880
 you find it keeps you awake,

415
00:24:59,880 --> 00:25:02,960
 it's really not wrong to open your eyes.

416
00:25:02,960 --> 00:25:08,010
 It's better, of course, to overcome the drowsiness and keep

417
00:25:08,010 --> 00:25:10,640
 meditating, but you can do it either

418
00:25:10,640 --> 00:25:11,640
 way.

419
00:25:11,640 --> 00:25:13,280
 I mean, your mind shouldn't be with your eyes anyway.

420
00:25:13,280 --> 00:25:15,360
 It should be with whatever your object is.

421
00:25:15,360 --> 00:25:19,080
 So with the stomach.

422
00:25:19,080 --> 00:25:22,210
 In fact, in the end, it's probably just a trick and it's

423
00:25:22,210 --> 00:25:24,040
 not, I mean, it's not really

424
00:25:24,040 --> 00:25:26,200
 the cause of your drowsiness.

425
00:25:26,200 --> 00:25:34,430
 Once you get equally focused with your eyes open, you're

426
00:25:34,430 --> 00:25:38,640
 still going to get drowsy.

427
00:25:38,640 --> 00:25:41,460
 A better solution is probably to do walking meditation, I

428
00:25:41,460 --> 00:25:42,320
 would think.

429
00:25:42,320 --> 00:25:50,530
 How long do you recommend a student practice your teachings

430
00:25:50,530 --> 00:25:54,360
 on meditation according to

431
00:25:54,360 --> 00:25:58,620
 your book, before undertaking the formal study with you

432
00:25:58,620 --> 00:26:01,480
 over the schedule of 101 meetings

433
00:26:01,480 --> 00:26:04,960
 because of your education?

434
00:26:04,960 --> 00:26:06,160
 Thank you.

435
00:26:06,160 --> 00:26:07,160
 Take a week.

436
00:26:07,160 --> 00:26:12,010
 If you've done a week of at least an hour a day, two if you

437
00:26:12,010 --> 00:26:14,440
 can, but at least one hour

438
00:26:14,440 --> 00:26:18,240
 a day, then I'm happy to help.

439
00:26:18,240 --> 00:26:28,000
 What's a matter supposed to feel like?

440
00:26:28,000 --> 00:26:31,090
 It kind of bothers me that I can't feel much better, but I

441
00:26:31,090 --> 00:26:32,720
'm thinking I might have the wrong

442
00:26:32,720 --> 00:26:33,720
 idea about what it is.

443
00:26:33,720 --> 00:26:34,720
 Is it supposed to be sentimental?

444
00:26:34,720 --> 00:26:35,720
 Kind of, yeah.

445
00:26:35,720 --> 00:26:41,600
 I wouldn't worry too much about it.

446
00:26:41,600 --> 00:26:44,420
 Meta is something that comes naturally as you practice

447
00:26:44,420 --> 00:26:45,720
 insight meditation.

448
00:26:45,720 --> 00:26:49,760
 It can be concerned more about insight.

449
00:26:49,760 --> 00:26:54,440
 Meta is secondary.

450
00:26:54,440 --> 00:26:59,350
 But if you really want to experience it, focus on people

451
00:26:59,350 --> 00:27:02,120
 who you know you have love for,

452
00:27:02,120 --> 00:27:05,560
 like family members maybe, or good friends.

453
00:27:05,560 --> 00:27:10,760
 It's a sense of friendliness, wishing for them to be happy,

454
00:27:10,760 --> 00:27:14,920
 good intentions toward them.

455
00:27:14,920 --> 00:27:23,700
 Does the process of samsara, death and rebirth, occur on

456
00:27:23,700 --> 00:27:30,240
 other planets or in other solar systems?

457
00:27:30,240 --> 00:27:35,240
 I have no idea.

458
00:27:35,240 --> 00:27:37,240
 Sorry.

459
00:27:37,240 --> 00:27:42,720
 And that's it.

460
00:27:42,720 --> 00:27:43,720
 Okay.

461
00:27:43,720 --> 00:27:47,000
 I think we'll stop then.

462
00:27:47,000 --> 00:27:55,040
 Oh wait, maybe there is one more.

463
00:27:55,040 --> 00:27:59,600
 Can you tell us the best way to help feed you currently?

464
00:27:59,600 --> 00:28:02,040
 Isn't through a you caring site?

465
00:28:02,040 --> 00:28:06,690
 No, the you caring site is a project set up by Robin to

466
00:28:06,690 --> 00:28:09,920
 help support the monthly expenses

467
00:28:09,920 --> 00:28:14,760
 of just having a house, having a place.

468
00:28:14,760 --> 00:28:21,920
 So that's for things like rent and utilities.

469
00:28:21,920 --> 00:28:24,200
 That's something fairly specific.

470
00:28:24,200 --> 00:28:30,600
 Although it goes, that's the nonprofit organization that is

471
00:28:30,600 --> 00:28:33,920
 run by a group of volunteers.

472
00:28:33,920 --> 00:28:34,920
 So it's general.

473
00:28:34,920 --> 00:28:37,690
 I mean, it can be used for various things, but it isn't

474
00:28:37,690 --> 00:28:39,280
 used to support me actually.

475
00:28:39,280 --> 00:28:43,880
 Except in so far as I live here and depend very much on

476
00:28:43,880 --> 00:28:46,400
 this place and the bills and

477
00:28:46,400 --> 00:28:47,400
 so on.

478
00:28:47,400 --> 00:28:50,760
 I mean, it's really a big thing.

479
00:28:50,760 --> 00:28:52,840
 But yeah, Fernando has probably the right link.

480
00:28:52,840 --> 00:28:54,640
 That's probably your best bet.

481
00:28:54,640 --> 00:28:57,680
 I will say that I don't, you know, food is really not an

482
00:28:57,680 --> 00:28:59,320
 issue at this point, though

483
00:28:59,320 --> 00:29:03,700
 it's often the one thing that people would like to help

484
00:29:03,700 --> 00:29:04,480
 with.

485
00:29:04,480 --> 00:29:05,480
 It's not an easy thing.

486
00:29:05,480 --> 00:29:12,390
 I mean, ideally, if you have a house somewhere nearby, I

487
00:29:12,390 --> 00:29:15,880
 can come for alms one morning or

488
00:29:15,880 --> 00:29:19,240
 even not so close.

489
00:29:19,240 --> 00:29:23,400
 I could find time to travel throughout Ontario.

490
00:29:23,400 --> 00:29:29,480
 Suppose going to America for alms is a bit much.

491
00:29:29,480 --> 00:29:33,840
 Although, you know, outside of the rains, I can take time

492
00:29:33,840 --> 00:29:35,880
 to go and stay in someone's

493
00:29:35,880 --> 00:29:39,880
 backyard during the hot season.

494
00:29:39,880 --> 00:29:49,240
 But then it would be more like a trip and an alms giving.

495
00:29:49,240 --> 00:29:51,650
 Barring that, you know, there's ways of, well, they've got

496
00:29:51,650 --> 00:29:52,840
 to go to the food support.

497
00:29:52,840 --> 00:29:56,000
 That's what we figured out so far.

498
00:29:56,000 --> 00:29:59,070
 And that's Tina is doing most of the organizing there, I

499
00:29:59,070 --> 00:30:01,080
 think that there's several people

500
00:30:01,080 --> 00:30:06,840
 involved with that.

501
00:30:06,840 --> 00:30:11,160
 Okay, so that's all for today.

502
00:30:11,160 --> 00:30:12,160
 Thank you all for tuning in.

503
00:30:12,160 --> 00:30:13,160
 Thank you.

504
00:30:13,160 --> 00:30:14,160
 Thanks, Stefano, for taking part.

505
00:30:14,160 --> 00:30:15,160
 Helping out.

506
00:30:15,160 --> 00:30:15,160
 Have a good night, everyone.

507
00:30:15,160 --> 00:30:19,320
 [END]

508
00:30:19,320 --> 00:30:21,320
 1

509
00:30:21,320 --> 00:30:23,320
 1

510
00:30:23,320 --> 00:30:25,320
 1

511
00:30:25,320 --> 00:30:27,320
 1

