 Namarupa is interesting. It's a bit of a dilemma really and
 it's the dilemma that science has faced and fought against
 in many ways.
 The popular view according to science is that, well
 according to many scientists, is that there's only the
 material.
 And materialism has become quite popular or has always been
 I guess quite popular among material scientists, people who
 study the physical side of reality.
 So they want to be able to say that the mental side of
 reality is an epiphenomenon. It's something that arises
 based on the physical and is totally dependent on the
 physical, has no power to influence reality.
 It's like harmony that arises or sound that arises when you
 clap your hands. The sound is dependent on the hands.
 Buddhism isn't that different. I mean much consciousness
 does come from the physical and is conditioned very much by
 the physical.
 But it's really making an assumption to say that the
 relationship is one way. When you could just as easily say
 the relationship goes the other way, you could postulate
 and run experiments in the same way as people do in the
 material realm.
 You could run these experiments in the mental realm and
 come to the conclusion that the physical doesn't exist.
 The physical is just an epiphenomenon that is conditioned
 by the mind. In other words, all that exists is it's like a
 dream.
 And this is kind of I suppose similar to solipsism,
 although it's not really solipsistic. It's just a purely
 mental reality that all is just mind.
 And mind is the only thing that exists. Now it's pretty
 clear that the Buddha didn't ascribe to either of these
 theories.
 And he didn't really ascribe to theories at all. This is
 why I don't particularly like to talk about such things
 because or why I might be hesitant to talk about it because
 what's to talk about?
 Namarupa is something that you experience and it's
 something that you come to in the very early stages of
 meditation.
 But why I think on the other hand it can be good to talk to
 it about people who are meditating is because when you just
 start out you really don't know what it is that you're
 seeing.
 You look at it and you wonder might it be this, might it be
 that? And you get the idea of me and mind and self.
 So the idea of nama rupa, the Buddha didn't say one way or
 other whether nama comes from rupa, rupa comes from nama or
 whether they both exist independently or so on.
 He likened them to two men, two people. One of them, I
 believe it was the Buddha or one of his disciples, I can't
 remember.
 Anyway, there's a simile of these two men, one who is
 crippled and one who is blind. And so the body is like this
 blind man who can't see but has good legs and can walk.
 And the mind is like a crippled person who can see fine but
 can't walk. And so they work together.
 The cripple gets on the back of the blind man and they walk
 around and the cripple says go this way, go that way.
 And basically the idea here is that they're interdependent.
 They rely on each other and they perform different
 functions.
 But it isn't really a philosophical teaching as to whether
 one or the other is real or is not real.
 And I think that's an important, that's the best way to
 look at the Buddha's teaching. I don't think we should get
 into the idea of existence or reality because there's such
 arbitrary words.
 How do you define whether something really exists? You know
, besides just saying that it's experienced.
 The importance of nama rupa, especially for meditators, is
 not whether one or the other is real or whether they're
 both real or so on.
 It's that they are all that is real.
 Nama rupa or the mental and physical experience, and we don
't even have to separate them, but the experience that is
 partly described as mental and partly described as physical
 is all that exists.
 And so we're not trying to define one in contrast with the
 other. We're trying to define them in contrast with
 illusion, which is the self.
 The first thing you realize as a meditator is that it
 appears that there are two things.
 There's the physical, which is like the stomach rising and
 falling, and then there's the mind that goes to know it.
 They arise together. They cease together. But neither one
 continues on after their allotted time. The rising starts
 and it stops.
 And the stopping is total cessation. That phenomenon is
 gone.
 The knowing of the rising is also gone. It's gone at the
 moment that the rising ends.
 So they arise and cease together. The foot moving, when you
 say stepping right, stepping left, the foot moving is
 physical.
 The mind that knows the foot is moving, that's mental. Now,
 whether you say they're one or two or so on, they are
 clearly distinguishable.
 If you want to explain what's going on, you have to say,
 well, there's the foot moving and then there's the mind
 that knows it.
 But if you look carefully at it, you'll see that what there
 isn't is some being involved in the process.
 And look carefully. It just means if you practice it again
 and again and again and look again and again eventually,
 your mind will start to focus and to see clearly what's
 going on, that this is a process,
 a phenomena that arises and sees and arises incessantly.
 This is called momentary death. And this is one of the
 first things we realize.
 If people don't understand it, my teacher would often give
 a talk, just a short talk explaining this, the different
 kinds of death.
 Like normally death, we think of a person dying. And so he
 says that that kind of death is just conceptual.
 It's not real. It's not a part of reality. Reality is that
 the mind continues on and on and on.
 The physical actually continues on and on. It just changes.
 So at the moment of death, well, it's just like a wave
 crashing against the shore. The ocean remains the same.
 So a wave has a beginning and an end. But the stuff that it
's made of, in this case water,
 or in our case physical and mental phenomena or experiences
, continues.
 And it's not really important whether it does or not.
 Our experience right now is that it continues on and on.
 People want to debate as to whether rebirth is important
 considering that we can't experience it.
 It's not really so important. If you wait long enough,
 eventually you'll experience it.
 But what we're experiencing now is what is real. The idea
 that we might die is also in the future.
 What is real is our death at every moment, our birth and
 death, birth and death, birth and death.
 And this cuts through the delusion of self, which is
 incredibly important.
 Many of you are aware of why it's so important.
 But if you haven't heard much about Buddhism, the reason
 why it's so important is
 this is really the true cause of our suffering.
 The fact that we attach to things, identify with them,
 think of them as me and mine, as I,
 and therefore begin to judge them.
 The reason why we say something is good is we think it's
 good for me.
 And we want to get it for ourselves because we have the
 idea of a self that experiences pleasure and pain.
 When you start to see in this way, you realize that
 pleasure and pain arise and cease on their own.
 And there's really no experience or there's the mind, but
 that mind arises and ceases with the pleasure and with the
 pain.
 And again, this, this, I think for many people, will just
 sound like an intellectual theory and go way over their
 head
 and they're not meditating. But my hope is that many of you
 are meditating and therefore this should actually make some
 sense
 and relate in some way to what you're practicing.
 So that's a little bit about Nama and Rupa.
 For those of you who don't know these words, Nama, the
 definition of Nama is that which knows an object.
 The definition of Rupa is something that doesn't know an
 object, doesn't have awareness.
 So the mind is the awareness. When you move the foot, there
's the Rupa, which doesn't know anything.
 It's just the movement. And then there's the knowing of it.
 And they go together. The Rupa occurs and the Nama knows it
.
 So it just means body and mind, really. That's the language
 that we use.
