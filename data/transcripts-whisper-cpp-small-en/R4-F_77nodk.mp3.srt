1
00:00:00,000 --> 00:00:03,120
 There's this person who really annoys or even anchors me

2
00:00:03,120 --> 00:00:04,000
 sometimes.

3
00:00:04,000 --> 00:00:08,240
 What bothers me about this is to know that it's not him,

4
00:00:08,240 --> 00:00:10,000
 but I, who is causing myself to suffer.

5
00:00:10,000 --> 00:00:15,000
 But why can't I let it go? Why am I attached to suffering?

6
00:00:15,000 --> 00:00:18,000
 It's the question, right?

7
00:00:18,000 --> 00:00:24,000
 That's the curious truth.

8
00:00:24,000 --> 00:00:28,920
 It's a profound question. It's not something that normally

9
00:00:28,920 --> 00:00:30,000
 people ask, "Why do I suffer?"

10
00:00:30,000 --> 00:00:33,210
 And they say, "Well, you're making me suffer." And that's

11
00:00:33,210 --> 00:00:35,000
 the problem.

12
00:00:35,000 --> 00:00:39,720
 The curious thing about suffering is there are different

13
00:00:39,720 --> 00:00:43,000
 levels to our understanding of it,

14
00:00:43,000 --> 00:00:46,000
 which I've talked about before. I can go into it here.

15
00:00:46,000 --> 00:00:49,280
 I think talking about suffering will probably give you some

16
00:00:49,280 --> 00:00:50,000
 background,

17
00:00:50,000 --> 00:00:54,000
 better background to answer this question.

18
00:00:54,000 --> 00:01:01,000
 I think briefly I can say, before I explain the means,

19
00:01:01,000 --> 00:01:04,870
 the simple answer to your question is because that's not

20
00:01:04,870 --> 00:01:06,000
 the practice.

21
00:01:06,000 --> 00:01:10,000
 The practice is not to let go of suffering.

22
00:01:10,000 --> 00:01:16,000
 The practice is to observe reality.

23
00:01:16,000 --> 00:01:21,000
 The result of observing reality closely, objectively,

24
00:01:21,000 --> 00:01:21,000
 clearly,

25
00:01:21,000 --> 00:01:26,000
 is to understand reality for what it is.

26
00:01:26,000 --> 00:01:29,860
 The result of understanding reality for what it is is

27
00:01:29,860 --> 00:01:31,000
 letting go.

28
00:01:31,000 --> 00:01:34,000
 So it's a progress.

29
00:01:34,000 --> 00:01:38,370
 Most people understand suffering to be a feeling that you

30
00:01:38,370 --> 00:01:40,000
 get from time to time.

31
00:01:40,000 --> 00:01:45,000
 And so they try to find a way to avoid the feeling.

32
00:01:45,000 --> 00:01:49,000
 The understanding of suffering is that it's specific.

33
00:01:49,000 --> 00:01:54,000
 This or that experience causes suffering.

34
00:01:54,000 --> 00:01:57,330
 And so their means of escaping suffering is to run away

35
00:01:57,330 --> 00:01:58,000
 from the experience,

36
00:01:58,000 --> 00:02:03,730
 to chase it away, to blame other people, blame external

37
00:02:03,730 --> 00:02:05,000
 events

38
00:02:05,000 --> 00:02:10,000
 and circumstances and entities outside of themselves,

39
00:02:10,000 --> 00:02:14,000
 or of course in their own bodies, even in their own minds.

40
00:02:14,000 --> 00:02:19,000
 Blame those things and try their best to avoid them.

41
00:02:19,000 --> 00:02:22,000
 This is how ordinary people deal with suffering.

42
00:02:22,000 --> 00:02:25,270
 You have a headache, take a pill, you have a backache, get

43
00:02:25,270 --> 00:02:26,000
 a massage,

44
00:02:26,000 --> 00:02:32,000
 you have a stomachache, take some adhesin.

45
00:02:32,000 --> 00:02:39,160
 Eventually, a spiritual person, a person with a little bit

46
00:02:39,160 --> 00:02:41,000
 of wisdom at least,

47
00:02:41,000 --> 00:02:48,270
 will see that this isn't effective, this isn't a viable

48
00:02:48,270 --> 00:02:51,000
 solution to the problem.

49
00:02:51,000 --> 00:02:54,280
 Eventually there are certain sufferings that you can't run

50
00:02:54,280 --> 00:02:55,000
 away from,

51
00:02:55,000 --> 00:03:00,140
 you can't avoid, you can't be free from simply by chasing

52
00:03:00,140 --> 00:03:01,000
 away.

53
00:03:01,000 --> 00:03:09,000
 Old age, sickness, death, even extreme trauma,

54
00:03:09,000 --> 00:03:17,000
 loss of a loved one, loss of wealth, loss of health,

55
00:03:17,000 --> 00:03:29,000
 loss or sadness or any kind of great suffering,

56
00:03:29,000 --> 00:03:34,000
 will make a person realize that this isn't a viable

57
00:03:34,000 --> 00:03:35,000
 solution,

58
00:03:35,000 --> 00:03:39,000
 that these states of suffering you can't avoid.

59
00:03:39,000 --> 00:03:42,220
 So there has to be a better way to be able to actually deal

60
00:03:42,220 --> 00:03:43,000
 with suffering.

61
00:03:43,000 --> 00:03:46,000
 This is what often leads people to the spiritual life

62
00:03:46,000 --> 00:03:50,000
 and for us this is what leads us to come and meditate.

63
00:03:50,000 --> 00:03:56,000
 So this leads to the third understanding of suffering

64
00:03:56,000 --> 00:04:07,080
 as a quality inherent in reality in the same way that fire

65
00:04:07,080 --> 00:04:08,000
 is hot.

66
00:04:08,000 --> 00:04:18,000
 Experience is a cause for suffering.

67
00:04:18,000 --> 00:04:21,000
 We don't really have a word, I can't think of a word for it

68
00:04:21,000 --> 00:04:21,000
,

69
00:04:21,000 --> 00:04:27,000
 but it's like static electricity or potential energy.

70
00:04:27,000 --> 00:04:30,000
 There's no energy in it but it has the potential.

71
00:04:30,000 --> 00:04:34,000
 So a rock that's up high doesn't seem to have any energy,

72
00:04:34,000 --> 00:04:36,590
 it's not moving but it has potential energy because if you

73
00:04:36,590 --> 00:04:37,000
 push it,

74
00:04:37,000 --> 00:04:42,540
 it exhibits extreme amounts of energy as it falls to the

75
00:04:42,540 --> 00:04:44,000
 earth below.

76
00:04:44,000 --> 00:04:48,000
 So reality has this kind of static electricity

77
00:04:48,000 --> 00:04:51,000
 or this potential for causing suffering.

78
00:04:51,000 --> 00:04:56,000
 So we say fire is hot but it's not burning you.

79
00:04:56,000 --> 00:05:00,000
 So when we say that experience is suffering,

80
00:05:00,000 --> 00:05:04,000
 we don't mean that it hurts you,

81
00:05:04,000 --> 00:05:06,000
 it has the potential to hurt you.

82
00:05:06,000 --> 00:05:11,000
 And that which causes that suffering is craving,

83
00:05:11,000 --> 00:05:16,000
 so the cause of all of our suffering is craving.

84
00:05:16,000 --> 00:05:20,330
 So the third is this realization, this observation that

85
00:05:20,330 --> 00:05:21,000
 leads you to see

86
00:05:21,000 --> 00:05:25,390
 that the reason that you suffer is not really because you

87
00:05:25,390 --> 00:05:28,000
 experience this or that,

88
00:05:28,000 --> 00:05:34,000
 but it's because, well the third stage is it's because

89
00:05:34,000 --> 00:05:37,000
 nothing can satisfy you.

90
00:05:37,000 --> 00:05:43,000
 It's because, or it's the realization that all of reality

91
00:05:43,000 --> 00:05:46,590
 is contributing to your suffering because you're partial to

92
00:05:46,590 --> 00:05:47,000
 certain things

93
00:05:47,000 --> 00:05:50,000
 and partial against certain things.

94
00:05:50,000 --> 00:05:54,000
 It's the realization that that's not correct,

95
00:05:54,000 --> 00:05:58,000
 that's due simply to ignorance.

96
00:05:58,000 --> 00:06:02,470
 And it's the realization that all, really all experience is

97
00:06:02,470 --> 00:06:03,000
 the same,

98
00:06:03,000 --> 00:06:06,000
 that pain is really the same as pleasure,

99
00:06:06,000 --> 00:06:08,430
 good memories are the same as bad memories, they're still

100
00:06:08,430 --> 00:06:09,000
 just memories.

101
00:06:09,000 --> 00:06:14,000
 It's really only our judgment and our incorrect judgment

102
00:06:14,000 --> 00:06:17,460
 that points to certain things as being stable, as being

103
00:06:17,460 --> 00:06:19,000
 pleasant, as being controllable

104
00:06:19,000 --> 00:06:23,000
 and therefore leading to expectations and ultimately

105
00:06:23,000 --> 00:06:24,000
 disappointment

106
00:06:24,000 --> 00:06:30,000
 when we don't get what we want.

107
00:06:30,000 --> 00:06:35,000
 That's the third which leads ultimately to a realization

108
00:06:35,000 --> 00:06:36,000
 that,

109
00:06:36,000 --> 00:06:40,640
 or the truth of suffering, and that's what leads you to let

110
00:06:40,640 --> 00:06:41,000
 go.

111
00:06:41,000 --> 00:06:43,660
 So the third and the fourth are kind of like one and the

112
00:06:43,660 --> 00:06:44,000
 same,

113
00:06:44,000 --> 00:06:47,000
 it's the path and then the goal.

114
00:06:47,000 --> 00:06:51,000
 When you finally get it, the fourth one is this moment,

115
00:06:51,000 --> 00:06:54,630
 this sort of gestalt I guess, or this epiphany that you

116
00:06:54,630 --> 00:06:55,000
 have

117
00:06:55,000 --> 00:06:59,000
 when you realize that nothing is worth clinging to,

118
00:06:59,000 --> 00:07:02,000
 when you realize that everything is like this.

119
00:07:02,000 --> 00:07:07,000
 The third is the practice of observing and investigating

120
00:07:07,000 --> 00:07:10,000
 and seeing that the things that you thought were pleasant

121
00:07:10,000 --> 00:07:14,000
 and the cause for happiness and contentment

122
00:07:14,000 --> 00:07:17,600
 are actually making you more discontent, more unhappy, more

123
00:07:17,600 --> 00:07:18,000
 unpleasant,

124
00:07:18,000 --> 00:07:22,000
 more bitter and mean as a person.

125
00:07:22,000 --> 00:07:25,000
 And the fourth one is this final realization

126
00:07:25,000 --> 00:07:29,000
 that absolutely everything is not worth clinging to.

127
00:07:29,000 --> 00:07:32,000
 And so that's the moment where the mind lets go

128
00:07:32,000 --> 00:07:35,000
 and that's the realization of the freedom,

129
00:07:35,000 --> 00:07:40,000
 the freedom from suffering, which we call nirvana.

130
00:07:40,000 --> 00:07:44,000
 So why you can't let go, I would say, if I was going to be

131
00:07:44,000 --> 00:07:44,000
 kind of

132
00:07:44,000 --> 00:07:47,630
 smarmy about it, I would say because you haven't done the

133
00:07:47,630 --> 00:07:48,000
 work,

134
00:07:48,000 --> 00:07:51,000
 you haven't done what is required to let go.

135
00:07:51,000 --> 00:07:54,000
 Letting go is not something you do.

136
00:07:54,000 --> 00:07:57,000
 In fact, the irony there is that you're talking about

137
00:07:57,000 --> 00:08:01,000
 forcing yourself to let go, which are opposites.

138
00:08:01,000 --> 00:08:04,000
 Letting go means letting come, actually.

139
00:08:04,000 --> 00:08:09,000
 Letting go means really stopping to force things.

140
00:08:09,000 --> 00:08:13,000
 Once you can do that, you'll be free from suffering.

141
00:08:13,000 --> 00:08:16,000
 So the question is really a paradox,

142
00:08:16,000 --> 00:08:19,000
 or something like a paradox where you're asking,

143
00:08:19,000 --> 00:08:22,000
 "Why can't I force myself to let go?"

144
00:08:22,000 --> 00:08:26,000
 When you say, "Why can't I let go?"

145
00:08:26,000 --> 00:08:29,250
 You don't let go. The letting go occurs because you see

146
00:08:29,250 --> 00:08:30,000
 things clearly.

147
00:08:30,000 --> 00:08:35,630
 There's just no clinging because you would have no reason

148
00:08:35,630 --> 00:08:37,000
 to cling.

149
00:08:37,000 --> 00:08:40,690
 The question as to why we're attached to suffering is more

150
00:08:40,690 --> 00:08:42,000
 interesting.

151
00:08:42,000 --> 00:08:44,000
 And this is what's curious about this.

152
00:08:44,000 --> 00:08:48,000
 Why the heck are we attached to that which is suffering?

153
00:08:48,000 --> 00:08:50,810
 And this is the key to the essence of Buddhism, is that it

154
00:08:50,810 --> 00:08:52,000
's ignorance.

155
00:08:52,000 --> 00:08:55,000
 We aren't always attached to suffering.

156
00:08:55,000 --> 00:08:59,000
 It's more like we are being tossed about in the ocean.

157
00:08:59,000 --> 00:09:02,000
 And so sometimes we go this way, sometimes we go that way,

158
00:09:02,000 --> 00:09:04,000
 sometimes we can be really good.

159
00:09:04,000 --> 00:09:06,000
 Sometimes people are just naturally good.

160
00:09:06,000 --> 00:09:08,000
 They're going in that direction.

161
00:09:08,000 --> 00:09:10,450
 Sometimes people are naturally horrible, evil, mean

162
00:09:10,450 --> 00:09:11,000
 creatures.

163
00:09:11,000 --> 00:09:15,000
 We go up and down, back and forth, but randomly.

164
00:09:15,000 --> 00:09:18,000
 But it's all based on ignorance.

165
00:09:18,000 --> 00:09:22,540
 Once you know, then you become steered in a specific

166
00:09:22,540 --> 00:09:23,000
 direction.

167
00:09:23,000 --> 00:09:25,330
 And that's towards peace, happiness and freedom from

168
00:09:25,330 --> 00:09:26,000
 suffering.

169
00:09:26,000 --> 00:09:29,000
 So knowledge is something that directs the mind,

170
00:09:29,000 --> 00:09:33,000
 just like the captain directs the ship.

171
00:09:33,000 --> 00:09:37,500
 Without a captain, without mindfulness, the boat just is

172
00:09:37,500 --> 00:09:40,000
 tossed to the waves,

173
00:09:40,000 --> 00:09:49,000
 tossed by the waves to the current.

174
00:09:49,000 --> 00:09:58,000
 So, it's just, you could say kind of by chance,

175
00:09:58,000 --> 00:10:03,000
 it's where you find yourself, or it's one of the currents

176
00:10:03,000 --> 00:10:08,000
 is attachment to suffering.

177
00:10:08,000 --> 00:10:11,000
 It's not ignorance.

