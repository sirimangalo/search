1
00:00:00,000 --> 00:00:04,760
 Venerable Teiro, when I tried to bring a Lord Buddha's

2
00:00:04,760 --> 00:00:07,320
 little statue to my room in Japan,

3
00:00:07,320 --> 00:00:12,320
 my wife opposed to it crazily, and she suggested to divorce

4
00:00:12,320 --> 00:00:15,360
 and worship Lord Buddha.

5
00:00:15,360 --> 00:00:20,280
 She's a Soka Gakkai member.

6
00:00:20,280 --> 00:00:28,000
 The problems with marriage, no?

7
00:00:28,000 --> 00:00:29,440
 That's not really a question, though, is it?

8
00:00:29,440 --> 00:00:33,440
 I guess the question is what to do.

9
00:00:33,440 --> 00:00:39,000
 I'm not really for marriage and for these reasons.

10
00:00:39,000 --> 00:00:45,590
 If there's some need to be married, then I would just say

11
00:00:45,590 --> 00:00:47,640
 put up with it.

12
00:00:47,640 --> 00:00:54,960
 But otherwise, divorce, become a monk.

13
00:00:54,960 --> 00:01:01,470
 If you really are keen on doing the right, engaging in good

14
00:01:01,470 --> 00:01:04,480
 things, engaging in what

15
00:01:04,480 --> 00:01:07,600
 is of real benefit, that's the advice that we give.

16
00:01:07,600 --> 00:01:12,370
 Now the question is how far you can go towards putting that

17
00:01:12,370 --> 00:01:14,800
 sort of thing into effect.

18
00:01:14,800 --> 00:01:17,950
 It's not the case that you're probably not in a position to

19
00:01:17,950 --> 00:01:19,520
 ordain, and you just want

20
00:01:19,520 --> 00:01:24,330
 to do some practice, and probably you love your wife and

21
00:01:24,330 --> 00:01:27,160
 are very much attached to her.

22
00:01:27,160 --> 00:01:29,560
 But that's the reality of living in the world.

23
00:01:29,560 --> 00:01:31,920
 That's doubly so in terms of being married.

24
00:01:31,920 --> 00:01:36,130
 This is the difficulty that we face, because you're not

25
00:01:36,130 --> 00:01:38,920
 married to your wife, because the

26
00:01:38,920 --> 00:01:43,240
 reason you're married to your wife is not because of your

27
00:01:43,240 --> 00:01:45,320
 religious views, obviously.

28
00:01:45,320 --> 00:01:50,220
 So you have to therefore balance which is more important,

29
00:01:50,220 --> 00:01:53,320
 your religious views, or your attraction

30
00:01:53,320 --> 00:01:55,960
 to your wife.

31
00:01:55,960 --> 00:01:58,910
 And if they're both important, then you've got a conflict

32
00:01:58,910 --> 00:02:00,360
 that you have to resolve.

33
00:02:00,360 --> 00:02:12,400
 You have to juggle to conflicting aspects of your life.

34
00:02:12,400 --> 00:02:19,280
 Now there are ways to ameliorate it.

35
00:02:19,280 --> 00:02:27,470
 Obviously what you want is a way to keep them both, which

36
00:02:27,470 --> 00:02:30,760
 is not impossible.

37
00:02:30,760 --> 00:02:35,030
 It's so much simpler to just give it up and go your own way

38
00:02:35,030 --> 00:02:35,200
.

39
00:02:35,200 --> 00:02:41,240
 Once you find out that you're not so compatible as you

40
00:02:41,240 --> 00:02:42,720
 thought.

41
00:02:42,720 --> 00:02:44,220
 But it depends.

42
00:02:44,220 --> 00:02:50,690
 The real key question here that has to be answered through

43
00:02:50,690 --> 00:02:53,600
 test is whether your wife

44
00:02:53,600 --> 00:02:58,980
 has the potential to come to understand the truth, as we

45
00:02:58,980 --> 00:03:01,200
 understand it as Buddhists.

46
00:03:01,200 --> 00:03:05,190
 So as Buddhists we understand certain things to be the

47
00:03:05,190 --> 00:03:05,960
 truth.

48
00:03:05,960 --> 00:03:09,890
 Is your wife capable of understanding those things and

49
00:03:09,890 --> 00:03:12,440
 therefore letting go and actually

50
00:03:12,440 --> 00:03:15,040
 appreciating the Buddha?

51
00:03:15,040 --> 00:03:18,280
 Okay, that's actually just one question.

52
00:03:18,280 --> 00:03:36,070
 So an important thing to do is to begin to help her to

53
00:03:36,070 --> 00:03:42,200
 understand concepts like letting

54
00:03:42,200 --> 00:03:50,560
 go and concepts like purity and enlightenment and so on.

55
00:03:50,560 --> 00:03:57,200
 To help her to see things in the same way as you see them.

56
00:03:57,200 --> 00:04:01,560
 And if she has good qualities in mind, then she will come

57
00:04:01,560 --> 00:04:02,480
 around.

58
00:04:02,480 --> 00:04:06,510
 But the other thing is, as I thought about this, is that

59
00:04:06,510 --> 00:04:08,720
 you're talking about a Buddha

60
00:04:08,720 --> 00:04:12,800
 image which is not really intrinsically important to your

61
00:04:12,800 --> 00:04:14,440
 Buddhist practice.

62
00:04:14,440 --> 00:04:17,980
 You can do away with the Buddha statue or you can put it in

63
00:04:17,980 --> 00:04:20,320
 a place and just stop worshipping

64
00:04:20,320 --> 00:04:21,320
 it.

65
00:04:21,320 --> 00:04:25,280
 Not do Buddha puja or offering it food or so on.

66
00:04:25,280 --> 00:04:35,340
 There's no intrinsic duty for you to do those things.

67
00:04:35,340 --> 00:04:43,310
 So if it's causing conflict, then just put the Buddha

68
00:04:43,310 --> 00:04:48,800
 statue away and keep practicing.

69
00:04:48,800 --> 00:04:53,720
 It's not really even a sign that she's on the wrong path.

70
00:04:53,720 --> 00:04:59,350
 She may even be trying to warn you away from Sila Bhatta

71
00:04:59,350 --> 00:05:02,840
 Paramasa, which is clinging to

72
00:05:02,840 --> 00:05:03,840
 rituals.

73
00:05:03,840 --> 00:05:06,300
 So if you give food to the Buddha every day and you think

74
00:05:06,300 --> 00:05:07,760
 that that's the most important

75
00:05:07,760 --> 00:05:10,330
 thing and so on, or it's intrinsically important, then you

76
00:05:10,330 --> 00:05:11,880
 may be missing something that she's

77
00:05:11,880 --> 00:05:14,320
 trying to teach you.

78
00:05:14,320 --> 00:05:18,470
 But from the sounds of it, it's just people clinging to dog

79
00:05:18,470 --> 00:05:20,880
mas and opposing other people's

80
00:05:20,880 --> 00:05:22,040
 beliefs and so on.

81
00:05:22,040 --> 00:05:24,840
 And that's difficult to deal with.

82
00:05:24,840 --> 00:05:30,760
 This is why it's very important to don't just be blind.

83
00:05:30,760 --> 00:05:32,840
 Love is blind.

84
00:05:32,840 --> 00:05:38,560
 Love looks not with its eyes but with its mind.

85
00:05:38,560 --> 00:05:42,280
 And therefore is winged cupid, painted blind.

86
00:05:42,280 --> 00:05:47,400
 Nor hath's love's mind of any reason, taste.

87
00:05:47,400 --> 00:05:52,680
 Wings and no eyes figure on heedy haste.

88
00:05:52,680 --> 00:05:55,630
 We rush into things, not based on love, actually based on

89
00:05:55,630 --> 00:05:56,520
 attachment.

90
00:05:56,520 --> 00:05:58,960
 And that's a problem.

91
00:05:58,960 --> 00:06:02,920
 If you're going to get married, you should be very, very

92
00:06:02,920 --> 00:06:05,040
 sure that you're compatible,

93
00:06:05,040 --> 00:06:08,690
 that you're both going to be helping each other, supporting

94
00:06:08,690 --> 00:06:10,520
 each other in your practice.

95
00:06:10,520 --> 00:06:13,450
 Not maybe that she is, it may be that she's actually

96
00:06:13,450 --> 00:06:15,680
 helping you to teach you something,

97
00:06:15,680 --> 00:06:18,200
 to help you to open up.

98
00:06:18,200 --> 00:06:20,040
 But from the sounds of it, it's probably not.

99
00:06:20,040 --> 00:06:23,730
 It's probably something that you'll have to help her to let

100
00:06:23,730 --> 00:06:25,240
 go and to see the beauty of

101
00:06:25,240 --> 00:06:28,040
 the Buddha and the greatness of the Buddha.

102
00:06:28,040 --> 00:06:30,480
 I don't really know what Soka Gakkai is, I forgot.

103
00:06:30,480 --> 00:06:32,160
 I think that's the ones that they do.

104
00:06:32,160 --> 00:06:40,590
 Nam-myoho-ring-ge-kyo, and want to manifest things and so

105
00:06:40,590 --> 00:06:41,520
 on.

106
00:06:41,520 --> 00:06:42,720
 So yeah, you have to help her.

107
00:06:42,720 --> 00:06:46,690
 In that case, you'd have to help her give up materialism

108
00:06:46,690 --> 00:06:48,760
 and thinking about the future

109
00:06:48,760 --> 00:06:49,760
 and so on.

110
00:06:49,760 --> 00:06:53,580
 Help her to understand what is true Buddhism, what the

111
00:06:53,580 --> 00:06:55,920
 Buddha truly taught and so on.

112
00:06:55,920 --> 00:06:56,920
 Thank you.

