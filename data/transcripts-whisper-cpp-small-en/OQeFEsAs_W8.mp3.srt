1
00:00:00,000 --> 00:00:02,150
 "I've been a little confused about how much should I donate

2
00:00:02,150 --> 00:00:04,200
 after a retreat?"

3
00:00:04,200 --> 00:00:06,880
 Nothing. You shouldn't donate anything.

4
00:00:06,880 --> 00:00:11,400
 Your donation is the retreat. That is your donation.

5
00:00:11,400 --> 00:00:13,600
 You shouldn't donate after a retreat.

6
00:00:13,600 --> 00:00:15,520
 Now, in certain cases,

7
00:00:15,520 --> 00:00:18,440
 you have to feel pity on the organization

8
00:00:18,440 --> 00:00:22,400
 and kind of feel

9
00:00:22,400 --> 00:00:27,000
 concerned for the organization that

10
00:00:27,000 --> 00:00:31,120
 all of us have come and used up a lot of the resources of

11
00:00:31,120 --> 00:00:32,600
 the organization.

12
00:00:32,600 --> 00:00:36,240
 So if we don't support it,

13
00:00:36,240 --> 00:00:42,640
 first of all, it's kind of

14
00:00:42,640 --> 00:00:44,840
 it's kind of crass, I suppose.

15
00:00:44,840 --> 00:00:49,720
 But it's a problem for the organization.

16
00:00:49,720 --> 00:00:52,320
 But I don't think that should be a hard and fast rule and

17
00:00:52,320 --> 00:00:53,720
 personally I don't

18
00:00:53,720 --> 00:00:57,720
 subscribe to such theory

19
00:00:57,720 --> 00:01:01,240
 of reimbursing or

20
00:01:01,240 --> 00:01:05,840
 of even paying for any expenses in the course.

21
00:01:05,840 --> 00:01:08,880
 Now, in certain cases, that's the only way possible.

22
00:01:08,880 --> 00:01:12,600
 Now, for example, you might get a group of people together

23
00:01:12,600 --> 00:01:13,240
 and say, "Okay, let's

24
00:01:13,240 --> 00:01:14,000
 pitch in

25
00:01:14,000 --> 00:01:19,400
 to rent this place," and so on and so on and so on.

26
00:01:19,400 --> 00:01:24,440
 But this is just to cover the need.

27
00:01:24,440 --> 00:01:29,520
 I wrote a long blog post and

28
00:01:29,520 --> 00:01:31,660
 I didn't get many comments so I get the feeling that it was

29
00:01:31,660 --> 00:01:32,960
 just too wordy but

30
00:01:32,960 --> 00:01:36,690
 I still would recommend you to read that blog post if you

31
00:01:36,690 --> 00:01:37,960
 want my thoughts on it.

32
00:01:37,960 --> 00:01:43,520
 It was called "There is two such things as a free lunch."

33
00:01:43,520 --> 00:01:47,000
 And the gist of it is, in case it really is too wordy,

34
00:01:47,000 --> 00:01:52,480
 is that we shouldn't be trying to reimburse

35
00:01:52,480 --> 00:01:55,310
 and we shouldn't be trying to pay it forward. We should be

36
00:01:55,310 --> 00:01:56,080
 trying to pay it back.

37
00:01:56,080 --> 00:01:57,640
 We shouldn't be trying to pay it forward.

38
00:01:57,640 --> 00:02:01,560
 We should be trying to pay

39
00:02:01,560 --> 00:02:05,520
 for it. When there is a need,

40
00:02:05,520 --> 00:02:10,280
 we should provide resources as we're able

41
00:02:10,280 --> 00:02:14,100
 to fulfill that need. When there's no need, we should be

42
00:02:14,100 --> 00:02:15,000
 willing to take the

43
00:02:15,000 --> 00:02:18,160
 free lunch. We should

44
00:02:18,160 --> 00:02:22,480
 feel no remorse for taking the free lunch.

45
00:02:22,480 --> 00:02:26,800
 If everyone

46
00:02:26,800 --> 00:02:31,040
 were to believe in the philosophy of free lunches,

47
00:02:31,040 --> 00:02:34,840
 then everyone would have free lunches because it's not

48
00:02:34,840 --> 00:02:39,160
 taking, it's not the philosophy that you can take whatever

49
00:02:39,160 --> 00:02:39,560
 you want,

50
00:02:39,560 --> 00:02:40,600
 it's the philosophy

51
00:02:40,600 --> 00:02:45,920
 of providing free lunches. When someone needs something,

52
00:02:45,920 --> 00:02:50,520
 you give it to them. And when you need something,

53
00:02:50,520 --> 00:02:55,400
 you take it. When

54
00:02:55,400 --> 00:02:59,200
 you come to the meditation course, you're not looking to

55
00:02:59,200 --> 00:03:03,270
 support the organization. You're looking for something. You

56
00:03:03,270 --> 00:03:04,080
 need something.

57
00:03:04,080 --> 00:03:09,520
 You need meditation. So,

58
00:03:09,520 --> 00:03:12,920
 the interaction, the transaction, had nothing to do with

59
00:03:12,920 --> 00:03:16,040
 the organization's need. Now,

60
00:03:16,040 --> 00:03:19,730
 when the organization needs things, when you see that there

61
00:03:19,730 --> 00:03:21,080
's a retreat going on,

62
00:03:21,080 --> 00:03:24,480
 regardless of whether you're going to join the retreat,

63
00:03:24,480 --> 00:03:28,560
 you should consider, does the organization need resources

64
00:03:28,560 --> 00:03:28,560
 to

65
00:03:28,560 --> 00:03:31,190
 do this? So, even if you're not participating in the

66
00:03:31,190 --> 00:03:31,720
 retreat,

67
00:03:31,720 --> 00:03:35,360
 before you even think of participating, you should consider

68
00:03:35,360 --> 00:03:38,560
 what resources are needed for this organization.

69
00:03:38,560 --> 00:03:42,600
 If you believe in the organization, it should have nothing

70
00:03:42,600 --> 00:03:43,240
 to do with what you

71
00:03:43,240 --> 00:03:45,520
 take. What you take is because you need.

72
00:03:45,520 --> 00:03:49,010
 What you give is because they need. If they don't need, you

73
00:03:49,010 --> 00:03:49,800
 don't have to give.

74
00:03:49,800 --> 00:03:53,130
 Now, this doesn't apply for most organizations. Most

75
00:03:53,130 --> 00:03:53,960
 organizations don't

76
00:03:53,960 --> 00:03:55,760
 subscribe to such a

77
00:03:55,760 --> 00:03:59,840
 philosophy and therefore are quite insistent or

78
00:03:59,840 --> 00:04:03,840
 hinting anyway at the need to donate and

79
00:04:03,840 --> 00:04:07,680
 for every participant to donate. And

80
00:04:07,680 --> 00:04:10,960
 there are even many monastic organizations that

81
00:04:10,960 --> 00:04:14,080
 talk about this

82
00:04:14,080 --> 00:04:17,840
 repaying your debt to the Sangha. Because

83
00:04:17,840 --> 00:04:21,760
 if you take something from the monastery, if you

84
00:04:21,760 --> 00:04:24,560
 use something in the monastery, you're in debt to the

85
00:04:24,560 --> 00:04:25,600
 monastery. This is the

86
00:04:25,600 --> 00:04:29,840
 concept. Now, this is totally, goes against with the

87
00:04:29,840 --> 00:04:32,720
 Buddha, how the Buddha explained the allocation of

88
00:04:32,720 --> 00:04:37,600
 resources. The Buddha explained four types of

89
00:04:37,600 --> 00:04:42,400
 people who use resources. The first person is a person who

90
00:04:42,400 --> 00:04:43,280
 steals them.

91
00:04:43,280 --> 00:04:47,040
 The second person is a person who loans them,

92
00:04:47,040 --> 00:04:50,880
 borrows them. The third person is a person who

93
00:04:50,880 --> 00:04:55,920
 inherits them. And the fourth person is the person who

94
00:04:55,920 --> 00:04:56,880
 deserves them.

95
00:04:56,880 --> 00:05:04,760
 A person who steals resources

96
00:05:04,760 --> 00:05:10,440
 is a person who is there under false pretenses

97
00:05:10,440 --> 00:05:17,560
 or false, what do you call it, is lying about it. So

98
00:05:17,560 --> 00:05:18,680
 someone who goes to practice

99
00:05:18,680 --> 00:05:22,070
 but is just looking to pick up women or something, you know

100
00:05:22,070 --> 00:05:25,000
, pick up, sorry, pick up

101
00:05:25,000 --> 00:05:29,000
 a certain romantic engagement, for example.

102
00:05:30,840 --> 00:05:34,820
 No, that's not true. It has to be worse than that. It has

103
00:05:34,820 --> 00:05:35,080
 to be

104
00:05:35,080 --> 00:05:42,120
 specifically deceptive. So suppose,

105
00:05:42,120 --> 00:05:46,480
 I don't know what an example in a meditation course would

106
00:05:46,480 --> 00:05:47,240
 be, like an

107
00:05:47,240 --> 00:05:51,400
 undercover, suppose a person from another

108
00:05:51,400 --> 00:05:57,000
 religion were to come and just to spy.

109
00:05:57,000 --> 00:05:59,150
 So they pretend to take the meditation course but they're

110
00:05:59,150 --> 00:06:00,120
 really there just to

111
00:06:00,120 --> 00:06:05,160
 spy and to cause problems. This is a person who steals the

112
00:06:05,160 --> 00:06:08,440
 resources. So the example the Buddha gave is as a

113
00:06:08,440 --> 00:06:11,640
 monk, a person who pretends to be a monk and goes on

114
00:06:11,640 --> 00:06:13,620
 alms round and people say, "Oh, there's a Buddhist monk

115
00:06:13,620 --> 00:06:14,360
 coming and

116
00:06:14,360 --> 00:06:18,060
 let's give them food." But they're not really a Buddhist

117
00:06:18,060 --> 00:06:18,440
 monk,

118
00:06:18,440 --> 00:06:22,090
 as a person who steals. A person who borrows is a person

119
00:06:22,090 --> 00:06:23,080
 who is really there.

120
00:06:23,080 --> 00:06:25,240
 They sign up for a meditation course or they

121
00:06:25,240 --> 00:06:28,220
 really ordain as a monk but they don't practice. So they

122
00:06:28,220 --> 00:06:29,160
 sleep all day in the

123
00:06:29,160 --> 00:06:34,440
 meditation course or they goof off or they

124
00:06:34,440 --> 00:06:37,360
 masturbate or something. I'm not allowed to say that, I don

125
00:06:37,360 --> 00:06:38,520
't know.

126
00:06:38,520 --> 00:06:43,400
 It's true. The reason I think of it is because I had a

127
00:06:43,400 --> 00:06:44,120
 student once who

128
00:06:44,120 --> 00:06:48,040
 admitted it to me. He came and admitted to having

129
00:06:48,040 --> 00:06:50,800
 masturbated during the course. But sorry, that's a bit

130
00:06:50,800 --> 00:06:51,640
 crude. I'll try

131
00:06:51,640 --> 00:06:55,660
 to be a little bit more, a little bit less crude. But

132
00:06:55,660 --> 00:06:56,200
 somehow, you know,

133
00:06:56,200 --> 00:07:00,440
 it's nice to bring up things that people are not willing to

134
00:07:00,440 --> 00:07:01,080
 talk about,

135
00:07:01,080 --> 00:07:04,130
 especially that one, because it seems to be such a

136
00:07:04,130 --> 00:07:07,240
 pervasive, pervasive,

137
00:07:07,240 --> 00:07:12,210
 such a big problem for people. Anyway, they do things that

138
00:07:12,210 --> 00:07:14,040
 are not appropriate.

139
00:07:14,040 --> 00:07:18,360
 This is a person who is... let's not say do things that are

140
00:07:18,360 --> 00:07:20,840
 inappropriate, but who doesn't do things that are necessary

141
00:07:20,840 --> 00:07:21,080
.

142
00:07:21,080 --> 00:07:24,840
 This is a person who's borrowing their resources and they

143
00:07:24,840 --> 00:07:25,240
 are

144
00:07:25,240 --> 00:07:28,360
 truly in debt when they leave, because they've

145
00:07:28,360 --> 00:07:33,720
 taken all these things without any reason for deserving

146
00:07:33,720 --> 00:07:35,480
 them.

147
00:07:35,480 --> 00:07:39,640
 The third type of person who inherits them is a person who

148
00:07:39,640 --> 00:07:39,960
 actually

149
00:07:39,960 --> 00:07:45,480
 tries to practice, who may not be enlightened or may not

150
00:07:45,480 --> 00:07:48,050
 have achieved enlightened, but who undertakes the

151
00:07:48,050 --> 00:07:50,120
 meditation course

152
00:07:50,120 --> 00:07:55,080
 to develop themselves, to practice the Buddhist teacher.

153
00:07:55,080 --> 00:07:57,880
 This, the Buddha said, is someone who inherits the

154
00:07:57,880 --> 00:07:59,000
 resources.

155
00:07:59,000 --> 00:08:05,640
 Why? Because those resources were given to the Buddha or

156
00:08:05,640 --> 00:08:06,360
 given to the

157
00:08:06,360 --> 00:08:09,960
 organization, because by people who believe in what the

158
00:08:09,960 --> 00:08:13,480
 organization is doing, believe in the practice. Now, these

159
00:08:13,480 --> 00:08:16,360
 people are undertaking the practice and are in line with

160
00:08:16,360 --> 00:08:19,960
 the organization's

161
00:08:19,960 --> 00:08:25,480
 goals and

162
00:08:25,480 --> 00:08:35,240
 ideals. So these people are inheriting it

163
00:08:35,240 --> 00:08:38,060
 based on the worthiness or in people's minds of the

164
00:08:38,060 --> 00:08:39,160
 organization.

165
00:08:39,160 --> 00:08:42,620
 So not just talking about Buddhism, but the organization in

166
00:08:42,620 --> 00:08:43,400
 general.

167
00:08:43,400 --> 00:08:48,520
 The fourth type of person are those who deserve them,

168
00:08:48,520 --> 00:08:51,880
 are those who have practiced and are enlightened,

169
00:08:51,880 --> 00:08:55,360
 in the case of Buddhism, or who have developed, realized

170
00:08:55,360 --> 00:08:55,880
 the goals of the

171
00:08:55,880 --> 00:08:59,480
 organization. So they have practiced the meditation and

172
00:08:59,480 --> 00:09:00,920
 become enlightened

173
00:09:00,920 --> 00:09:04,280
 and are therefore deserving of whatever people give them.

174
00:09:04,280 --> 00:09:07,720
 Because their mind is pure, because they have

175
00:09:07,720 --> 00:09:11,880
 attained the goals of the organization, they are therefore

176
00:09:11,880 --> 00:09:15,150
 equivalent to the organization, in this case equivalent to

177
00:09:15,150 --> 00:09:16,200
 the Buddha.

178
00:09:16,200 --> 00:09:19,040
 And therefore people who have faith in the Buddha and give

179
00:09:19,040 --> 00:09:19,800
 to the monks without

180
00:09:19,800 --> 00:09:24,920
 faith in the Buddha are giving to, basically giving to the

181
00:09:24,920 --> 00:09:25,720
 Buddha,

182
00:09:25,720 --> 00:09:29,220
 because they're giving to someone who is in that sense, in

183
00:09:29,220 --> 00:09:30,280
 one sense, equivalent

184
00:09:30,280 --> 00:09:33,720
 to the Buddha, in the sense of being free from suffering.

185
00:09:33,720 --> 00:09:39,800
 So that's the Buddha's teaching, which sort of puts to rest

186
00:09:39,800 --> 00:09:40,520
 this idea of

187
00:09:40,520 --> 00:09:43,560
 being in debt. If you're actually practicing meditation

188
00:09:43,560 --> 00:09:46,440
 during the course, you're not in debt to the organization.

189
00:09:46,440 --> 00:09:53,340
 If you're putting in to practice their teachings, then, and

190
00:09:53,340 --> 00:09:54,360
 honestly,

191
00:09:54,360 --> 00:09:57,560
 and wholeheartedly, then you're not in debt to the

192
00:09:57,560 --> 00:09:59,480
 organization.

