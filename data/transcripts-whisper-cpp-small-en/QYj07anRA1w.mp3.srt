1
00:00:00,000 --> 00:00:05,800
 What are some connections between physics and Buddhism?

2
00:00:05,800 --> 00:00:12,320
 I'm not well versed in physics.

3
00:00:12,320 --> 00:00:17,790
 Well I think quantum physics has some important or has the

4
00:00:17,790 --> 00:00:21,920
 potential to to

5
00:00:21,920 --> 00:00:28,860
 give some insight for modern people into the importance of

6
00:00:28,860 --> 00:00:30,920
 the Buddha's teaching.

7
00:00:30,920 --> 00:00:39,590
 Obviously physics is not interested in the development of

8
00:00:39,590 --> 00:00:45,040
 the mind. Physics

9
00:00:45,040 --> 00:00:47,610
 doesn't talk about getting rid of greed, anger, and

10
00:00:47,610 --> 00:00:49,760
 delusion. So in that sense

11
00:00:49,760 --> 00:00:54,630
 there is no connection with the goal of physics and the

12
00:00:54,630 --> 00:00:55,760
 goal of Buddhism.

13
00:00:55,760 --> 00:01:09,240
 But the framework of quantum physics allows people to let

14
00:01:09,240 --> 00:01:12,680
 go of the idea of a

15
00:01:12,680 --> 00:01:19,620
 three-dimensional space that exists like a world and a

16
00:01:19,620 --> 00:01:27,320
 being and an

17
00:01:27,320 --> 00:01:33,040
 objective external reality.

18
00:01:36,760 --> 00:01:47,280
 The problem that quantum physics tried to address is why

19
00:01:47,280 --> 00:01:47,840
 reality

20
00:01:47,840 --> 00:01:58,080
 seems to depend very much on the observation as opposed to

21
00:01:58,080 --> 00:02:00,000
 some

22
00:02:00,000 --> 00:02:06,720
 objective laws of physics. So depending on the experiment,

23
00:02:06,720 --> 00:02:06,800
 depending on

24
00:02:06,800 --> 00:02:13,440
 the observation, there arises reality. So for modern

25
00:02:13,440 --> 00:02:15,640
 thinkers and for secularists

26
00:02:15,640 --> 00:02:20,920
 who maybe who had been materialists before when they study

27
00:02:20,920 --> 00:02:22,160
 quantum physics,

28
00:02:22,160 --> 00:02:29,630
 at least certain or orthodox quantum physics, it can help

29
00:02:29,630 --> 00:02:30,600
 them to overcome

30
00:02:30,600 --> 00:02:34,560
 their doubts about, for instance, the existence of the mind

31
00:02:34,560 --> 00:02:35,400
 and so on.

32
00:02:35,400 --> 00:02:38,750
 Now physics doesn't deal with the existence of the mind but

33
00:02:38,750 --> 00:02:39,560
 it pretty much

34
00:02:39,560 --> 00:02:44,360
 or quantum physics pretty much requires some interaction.

35
00:02:44,360 --> 00:02:45,920
 It points out that we

36
00:02:45,920 --> 00:02:48,790
 can only explain reality in terms of observation. If you

37
00:02:48,790 --> 00:02:49,880
 understand quantum

38
00:02:49,880 --> 00:02:56,000
 physics correctly then it shows, or not correctly, in a

39
00:02:56,000 --> 00:02:57,600
 certain way, then it

40
00:02:57,600 --> 00:03:08,320
 shows the truth of what the Buddha taught. There's a, this

41
00:03:08,320 --> 00:03:09,360
 professor that I keep

42
00:03:09,360 --> 00:03:13,240
 coming back to, Henry Stapp, who actually quotes a Buddhist

43
00:03:13,240 --> 00:03:15,080
 monk and explains how

44
00:03:15,080 --> 00:03:18,710
 things like reincarnation could be totally possible through

45
00:03:18,710 --> 00:03:19,440
 physics and so

46
00:03:19,440 --> 00:03:24,080
 on using quantum physics and so on. But I think the

47
00:03:24,080 --> 00:03:25,240
 important point is that

48
00:03:25,240 --> 00:03:29,350
 no, there's no overlap in the goals. Quantum physics, even

49
00:03:29,350 --> 00:03:30,280
 quantum physics,

50
00:03:30,280 --> 00:03:35,600
 never had any idea of changing the quality of one's mind.

51
00:03:35,600 --> 00:03:36,680
 It's physics.

52
00:03:36,680 --> 00:03:42,530
 You can't purify the physical and so you might say that

53
00:03:42,530 --> 00:03:44,480
 they complement each

54
00:03:44,480 --> 00:03:53,280
 other in one sense, but that they totally seek out

55
00:03:53,280 --> 00:03:56,040
 different goals. Buddhism is

56
00:03:56,040 --> 00:03:59,360
 for the purification of the mind. Physics is for the

57
00:03:59,360 --> 00:04:03,160
 understanding of the physical.

58
00:04:03,160 --> 00:04:09,120
 Now I don't have much to say about this. I only saw one

59
00:04:09,120 --> 00:04:11,800
 children's video about

60
00:04:11,800 --> 00:04:18,840
 quantum physics and was really quite impressed about what,

61
00:04:18,840 --> 00:04:20,600
 that everything is

62
00:04:20,600 --> 00:04:29,010
 is waves and, but I couldn't say that I really understand

63
00:04:29,010 --> 00:04:31,760
 that. Was it the one with the

64
00:04:31,760 --> 00:04:39,560
 double slit experiment? Exactly that, yeah. And it was so

65
00:04:39,560 --> 00:04:43,480
 astonishing that the

66
00:04:43,480 --> 00:04:49,760
 particles, when they were observed, they behaved different

67
00:04:49,760 --> 00:04:50,960
 from when they weren't

68
00:04:50,960 --> 00:05:03,400
 observed. So yeah, but I don't know why that they do that.

69
00:05:03,400 --> 00:05:05,880
 I mean I think that's

70
00:05:05,880 --> 00:05:08,070
 the important thing. Obviously there are many

71
00:05:08,070 --> 00:05:09,760
 interpretations of quantum physics

72
00:05:09,760 --> 00:05:12,810
 and many wacky theories about why it is and the idea of

73
00:05:12,810 --> 00:05:14,920
 multiverse and so on. But

74
00:05:14,920 --> 00:05:19,390
 as I said, if you understand it in a specific way, then it

75
00:05:19,390 --> 00:05:20,840
 really is

76
00:05:20,840 --> 00:05:24,370
 proof of the Buddha's teaching. But of course you have to,

77
00:05:24,370 --> 00:05:25,200
 everyone can

78
00:05:25,200 --> 00:05:30,850
 have their theory of why it's happening, but the mind, you

79
00:05:30,850 --> 00:05:31,440
 know, the Buddha said

80
00:05:31,440 --> 00:05:35,950
 manopu bangamadama, the mind comes first, the mind creates

81
00:05:35,950 --> 00:05:37,000
 the world. When the mind

82
00:05:37,000 --> 00:05:43,240
 ceases, the world ceases. And we create our reality. It

83
00:05:43,240 --> 00:05:44,560
 actually, the

84
00:05:44,560 --> 00:05:48,600
 realizations of quantum physics, if understood in the

85
00:05:48,600 --> 00:05:50,320
 orthodox sense, really

86
00:05:50,320 --> 00:05:54,480
 say the same things that the Buddha was saying, that

87
00:05:54,480 --> 00:05:56,400
 everything you do

88
00:05:56,400 --> 00:06:04,320
 affects the universe. Your actions have consequences. Even

89
00:06:04,320 --> 00:06:06,280
 the slightest change

90
00:06:06,280 --> 00:06:12,260
 of your attention or your mind will have consequences. So

91
00:06:12,260 --> 00:06:14,400
 our greed, our anger, our

92
00:06:14,400 --> 00:06:22,770
 delusion will have specific results and our wisdom and

93
00:06:22,770 --> 00:06:24,040
 mindfulness will also

94
00:06:24,040 --> 00:06:28,040
 have specific results.

