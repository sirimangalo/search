1
00:00:00,000 --> 00:00:04,100
 Hi, welcome back to Ask a Monk. Today I thought I would

2
00:00:04,100 --> 00:00:07,000
 answer three questions in a row because

3
00:00:07,000 --> 00:00:11,200
 they seem fairly simple to answer and it wouldn't... I don

4
00:00:11,200 --> 00:00:14,000
't think it would be worth making a video

5
00:00:14,000 --> 00:00:20,920
 about each one. The first one is from Shazruzal. "Dear Yut

6
00:00:20,920 --> 00:00:21,000
adamo, could you explain the importance

7
00:00:21,000 --> 00:00:27,000
 of the Bodhi tree?" The Bodhi tree, what is meant by the

8
00:00:27,000 --> 00:00:28,500
 Bodhi tree, is a fig, a sort

9
00:00:28,500 --> 00:00:32,670
 of a fig tree that is found in India and it happens to be

10
00:00:32,670 --> 00:00:35,000
 the tree that the historical

11
00:00:35,000 --> 00:00:38,660
 Buddha sat under to become enlightened. Now according to

12
00:00:38,660 --> 00:00:40,600
 the legends or what we have of

13
00:00:40,600 --> 00:00:45,240
 the Buddha's teaching, in other world periods and ancient

14
00:00:45,240 --> 00:00:47,600
 times, other Buddhas arose and

15
00:00:47,600 --> 00:00:51,440
 they all had their own type of tree. Basically what it

16
00:00:51,440 --> 00:00:53,800
 means is that the followers, the people

17
00:00:53,800 --> 00:00:58,150
 came after, would always venerate that sort of tree because

18
00:00:58,150 --> 00:01:00,000
 it was in the memory of the

19
00:01:00,000 --> 00:01:03,340
 Buddha. So whenever we see this tree, we'll generally try

20
00:01:03,340 --> 00:01:06,200
 to protect it and maybe put

21
00:01:06,200 --> 00:01:10,970
 a terrace around it. It came to signify the Buddha himself.

22
00:01:10,970 --> 00:01:13,200
 So instead of making a statue

23
00:01:13,200 --> 00:01:16,520
 of the Buddha, they would plant a Bodhi tree or make a

24
00:01:16,520 --> 00:01:18,680
 carving of a Bodhi leaf or you'll

25
00:01:18,680 --> 00:01:23,380
 see people with Bodhi leaves or leaves from this fig tree

26
00:01:23,380 --> 00:01:25,680
 or mala's made of the seeds

27
00:01:25,680 --> 00:01:31,440
 or so on. So basically it just represents the Buddha. It's

28
00:01:31,440 --> 00:01:34,360
 where the Buddha became enlightened.

29
00:01:34,360 --> 00:01:37,180
 When the Buddha sat down to meditate for the final time

30
00:01:37,180 --> 00:01:39,160
 before he was to become enlightened

31
00:01:39,160 --> 00:01:42,300
 to realize the truth that he then taught to people for the

32
00:01:42,300 --> 00:01:44,080
 rest of his life, it was under

33
00:01:44,080 --> 00:01:46,970
 a Bodhi tree, this type of tree. And so it's called a Bodhi

34
00:01:46,970 --> 00:01:48,400
 tree. The word Bodhi means

35
00:01:48,400 --> 00:01:55,400
 enlightenment or wisdom. So it's the tree of enlightenment.

36
00:01:55,400 --> 00:01:55,400
 And the original one is in

37
00:01:55,400 --> 00:02:01,410
 India. It's in Buddha Gaya which is south of a city called

38
00:02:01,410 --> 00:02:04,480
 Gaya in the province of Bihar.

39
00:02:04,480 --> 00:02:11,060
 And actually, as I'm sure people can tell you, I'm not so

40
00:02:11,060 --> 00:02:14,040
 clear on it, but I think this

41
00:02:14,040 --> 00:02:18,480
 is the fourth generation of in a sequence of trees. It was

42
00:02:18,480 --> 00:02:21,040
 killed, cut down and replanted

43
00:02:21,040 --> 00:02:26,250
 from Sri Lanka. The Sri Lankans took a cut from the

44
00:02:26,250 --> 00:02:30,020
 original Bodhi tree, brought it to

45
00:02:30,020 --> 00:02:34,440
 Sri Lanka and when the Bodhi tree was killed in Bodhagaya

46
00:02:34,440 --> 00:02:37,020
 in India, they brought it back

47
00:02:37,880 --> 00:02:42,480
 from Sri Lanka. So you'll find the Maha Bodhi tree there

48
00:02:42,480 --> 00:02:44,880
 which is probably the most, it's

49
00:02:44,880 --> 00:02:48,280
 sort of like the Buddhist Mecca, I guess you could say, the

50
00:02:48,280 --> 00:02:50,120
 most important Buddhist spot

51
00:02:50,120 --> 00:02:54,810
 in the world. It's where Buddhists all year round will go

52
00:02:54,810 --> 00:02:57,120
 to pay respect to the Buddha

53
00:02:57,120 --> 00:03:05,080
 and to his enlightenment. Next question is from Itong Tian.

54
00:03:05,080 --> 00:03:05,080
 Do you make your own food?

55
00:03:07,080 --> 00:03:10,250
 No, I don't make my own food. There are some videos about

56
00:03:10,250 --> 00:03:12,880
 the alms round that I've done

57
00:03:12,880 --> 00:03:17,000
 so you can take a look at those. As a monk, we aren't

58
00:03:17,000 --> 00:03:19,880
 allowed to store food, cook food,

59
00:03:19,880 --> 00:03:25,000
 or store food, cook food. That's about it. We're not

60
00:03:25,000 --> 00:03:28,280
 allowed to store food overnight.

61
00:03:28,280 --> 00:07:12,510
 We're not allowed to cook food. We're not allowed to take

62
00:07:12,510 --> 00:03:34,280
 food for ourselves. So, we're

63
00:03:35,280 --> 00:03:36,420
 not allowed to store food for ourselves. So, we couldn't go

64
00:03:36,420 --> 00:03:37,280
 into a store and buy food

65
00:03:37,280 --> 00:03:40,640
 or even go into someone's cupboard and if they've given us

66
00:03:40,640 --> 00:03:42,280
 permission to go in to take

67
00:03:42,280 --> 00:03:46,700
 food. We're only allowed to eat food that has been given

68
00:03:46,700 --> 00:03:49,280
 freely out of faith or as alms.

69
00:03:49,280 --> 00:03:53,830
 So in India, people would give food as a tradition, they

70
00:03:53,830 --> 00:03:57,040
 would give it to people of all religious

71
00:03:58,720 --> 00:04:02,530
 beliefs, they would believe that they're doing something

72
00:04:02,530 --> 00:04:05,520
 good, something, some sort of spiritually

73
00:04:05,520 --> 00:04:09,420
 beneficial practice. It's a moral, it's a charitable

74
00:04:09,420 --> 00:04:11,920
 practice, it's something that people

75
00:04:11,920 --> 00:04:15,820
 do because it makes them happy and it makes them feel like

76
00:04:15,820 --> 00:04:17,920
 good people. It makes them

77
00:04:17,920 --> 00:04:20,380
 feel like they're doing something of worth. So, they would

78
00:04:20,380 --> 00:04:22,880
 give that. Even nowadays, Buddhists

79
00:04:22,880 --> 00:04:25,860
 would do the same thing. They maybe aren't able to become

80
00:04:25,860 --> 00:04:27,320
 monks themselves or aren't

81
00:04:27,320 --> 00:04:32,170
 able to even go to practice meditation, but they're able to

82
00:04:32,170 --> 00:04:34,320
 practice at home and to do

83
00:04:34,320 --> 00:04:38,440
 good deeds to support the monks and to support people who

84
00:04:38,440 --> 00:04:41,200
 are practicing meditation. So,

85
00:04:41,200 --> 00:04:45,420
 I go to the restaurants, the local Asian restaurants and

86
00:04:45,420 --> 00:04:48,200
 they put food in my bowl every day and

87
00:04:48,200 --> 00:04:52,910
 I've been doing that for the past nine years in Thailand

88
00:04:52,910 --> 00:04:55,840
 and now in America. So, no, we're

89
00:04:56,080 --> 00:05:01,910
 not allowed to cook food. The third question is, "I realize

90
00:05:01,910 --> 00:05:03,080
 that I'm easily angered and

91
00:05:03,080 --> 00:05:07,090
 I've had a hard time to quit smoking because of it. What

92
00:05:07,090 --> 00:05:09,360
 kind of meditation should I do

93
00:05:09,360 --> 00:05:12,440
 to help with this? What kind of tai chi could I also do?"

94
00:05:12,440 --> 00:05:14,200
 Your answer would mean a lot, thanks.

95
00:05:14,200 --> 00:05:19,990
 This is from 915 Rehir. And the reason I'm not going to go

96
00:05:19,990 --> 00:05:21,200
 into detail about this is because

97
00:05:21,400 --> 00:05:25,080
 really I've given a specific set of meditation and I think

98
00:05:25,080 --> 00:05:27,160
 it really does help with anger.

99
00:05:27,160 --> 00:05:30,240
 If you're to put into practice the teachings that I've

100
00:05:30,240 --> 00:05:32,160
 posted, the teachings on how to

101
00:05:32,160 --> 00:05:35,220
 meditate, which I assume you haven't looked at since you're

102
00:05:35,220 --> 00:05:36,800
 asking this question, you'll

103
00:05:36,800 --> 00:05:41,440
 see that there's both the way of meditating and the reasons

104
00:05:41,440 --> 00:05:43,800
 why we meditate. One of the

105
00:05:43,800 --> 00:05:48,050
 reasons is to overcome things like anger, to give up add

106
00:05:48,050 --> 00:05:50,400
ictions like smoking. If you

107
00:05:50,400 --> 00:05:54,090
 are interested specifically in smoking, I think it's an

108
00:05:54,090 --> 00:05:56,760
 interesting adaptation of the meditation

109
00:05:56,760 --> 00:06:00,120
 practice. I see that's not exactly what you're asking, but

110
00:06:00,120 --> 00:06:01,720
 giving up smoking doesn't have

111
00:06:01,720 --> 00:06:04,560
 just to do with anger. It has to do with feelings of guilt,

112
00:06:04,560 --> 00:06:06,360
 which you have to do away with first

113
00:06:06,360 --> 00:06:09,190
 once you've given up the feelings of guilt, then go on to

114
00:06:09,190 --> 00:06:10,720
 the craving once you're able

115
00:06:10,720 --> 00:06:14,080
 to accept the craving and to just see it for what it is,

116
00:06:14,080 --> 00:06:15,880
 then go even deeper on to the

117
00:06:15,880 --> 00:06:19,180
 feeling, the happy feeling that comes with the craving, the

118
00:06:19,180 --> 00:06:20,600
 pleasure that comes with

119
00:06:20,600 --> 00:06:24,260
 wanting. When you can just experience the pleasure and say

120
00:06:24,260 --> 00:06:26,600
 to yourself, "Happy, happy,

121
00:06:26,600 --> 00:06:30,880
 or pleasure, pleasure, feeling, feeling," then you can do

122
00:06:30,880 --> 00:06:32,680
 away with the craving and

123
00:06:32,680 --> 00:06:35,120
 do away with the needing and do away with the getting and

124
00:06:35,120 --> 00:06:36,520
 do away with the smoking when

125
00:06:36,520 --> 00:06:40,340
 you're able to bring it right back to both the thoughts

126
00:06:40,340 --> 00:06:42,640
 about smoking and the feelings

127
00:06:42,640 --> 00:06:52,240
 about smoking. The pleasant feeling of excitement that you

128
00:06:52,240 --> 00:06:52,240
 get when you think of smoking. As

129
00:06:52,240 --> 00:06:55,950
 far as Tai Chi, I don't teach Tai Chi, so you're on your

130
00:06:55,950 --> 00:06:58,040
 own there. You have to find

131
00:06:58,040 --> 00:07:03,410
 someone who teaches that. There are three questions that

132
00:07:03,410 --> 00:07:06,000
 deserve answers, but I think

133
00:07:06,000 --> 00:07:09,990
 the answers are pretty simple. There you have it. Thanks

134
00:07:09,990 --> 00:07:11,800
 for the questions and keep practicing.

