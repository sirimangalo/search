 Good evening everyone.
 We're broadcasting live April 28th.
 Tonight's talk is about reverence.
 This monk comes and asks the Buddha.
 I asked him two questions actually.
 This quote only gives one.
 He says, "Konukobante he tukopajayo yena tathagate parinibh
ute sadhamo na kira titiko hoti."
 What is the cause? What is the reason?
 By which, when the tathagata has entered into parinibhana,
 the good dhamma doesn't last long.
 Doesn't last. So this quote only does last.
 The next question is, "What is the cause? What is the
 reason why it does last long?"
 "Kira titiko hoti."
 This is from the Anguttra Nikaya, book of sevens.
 So it's a list of seven things.
 Seven things that when you pay respect to them, reverence,
 the word is "garawa," "sagarawa."
 "Gara" means heavy, "garu."
 "Garu" means heavy.
 "Garu" actually is the word "guru" in Sanskrit.
 "Guru" means teacher.
 But I believe the origin is from the word "heavy."
 And "garawa" means to take something seriously.
 If we appreciate and think of as worth something, so we
 worship in a sense, in a literal sense.
 We place worth on these things.
 Then the dhamma will last long after the Buddha disappears.
 Because when the Buddha is around, he's a teacher of all
 beings.
 He's the unexcelled teacher, unexcelled trainer.
 So when he's around, there's no question.
 The dhamma will last.
 When he's gone, however, what we've got left is the Arahant
 disciples of the Buddha,
 and then they pass away, and then hopefully more Arahants
 come.
 The question is, what keeps the lineage going?
 What keeps the chain going?
 And so it's suttas like this that are of great interest to
 those of us in later generations,
 because it gives us an idea of how we support not only our
 own practice,
 but how we carry on the legacy of the Buddha, pass on the
 legacy to future generations as well.
 How we take what was given to us and cherish it and nurture
 it and keep it alive.
 It's like we're given a seed to a great tree,
 and it's up to us to plant it and to cultivate it for
 future generations.
 The dhamma is like that at some point, or it's like a fire.
 In ancient times, in cave time, in ancient, ancient times,
 they had to carry a coal with them from campfire to camp
fire.
 The easiest way to keep fire was to carry something,
 and if the fire went out, then you're in trouble.
 You had a fire carrier.
 I read a story about it once anyway.
 You have to care for the dhamma.
 So how do we care for the dhamma?
 What are the ways by which we ensure that this teaching
 will continue?
 So the Buddha gives seven things.
 We take these things seriously and appreciate them.
 And in a sense, worship them.
 But worship, not in the way we use it, just assign it some
 worth.
 And of course, the first three are the Buddha, the dhamma,
 and the sangha.
 The first one is the sata.
 Sata means teacher, but in Buddhism, it usually refers to
 the Buddha.
 And here that's what it refers to.
 It's a special word that is generally only used for the
 Buddha.
 It literally means teacher, something like teacher.
 And so if a monk or a bhikkhu or a bhikkhuni, a male or
 female monk,
 or a rupasaka or rupasika, a male or female lay disciple,
 it's just anyone who's a man or a woman,
 and anyone who's in between, anyone at all.
 If they do not take the teacher seriously and assign worth
 and appreciation to the teacher or the dhamma or the sangha
,
 they do not dwell agara, agarawa, vihranthi.
 They dwell without reverence, without appreciation,
 without taking seriously.
 And then the fourth is sikha, sikha, sikhaaya.
 This is the training, sikha.
 Sikha means the training, so the training in,
 in a general sense, the training in giving up craving,
 training to give up,
 and the training in morality,
 the training in concentration and wisdom.
 But here's specifically just the act of,
 and the things that we do like practicing meditation
 and keeping precepts.
 So the things that we do and the things that we don't do,
 we abstain from certain things
 and we take on certain behaviors, certain practices.
 This is the training and the learning,
 studying, listening to the dhamma, remembering the dhamma,
 reciting the dhamma, thinking about the dhamma,
 all of these things.
 This is all the training that we go through,
 taking that seriously.
 If you don't take that seriously,
 and number five is samadhi.
 We don't take concentration seriously.
 We go to spells it out explicitly, concentration.
 Samadhi is, samadhi is an interesting word.
 It could just mean focus, you know.
 Sammas is like same.
 It means level or even.
 So not too much, not too little.
 When your mind gets perfectly focused,
 that's why it's kind of like focused rather than
 concentration
 because it's like a camera lens.
 If you focus too much, it gets blurry, too little also
 blurry.
 You need perfect focus and then you can see.
 So your mind has to be balanced is really what samadhi is
 all about.
 People think of samadhi as being concentrated,
 so no thoughts, focused only on one thing, no distractions.
 But it's not necessarily that.
 It just means the mind that is seeing things clearly,
 the mind that is focused on something as it is in that
 moment.
 You don't have to block everything out
 and just concentrate on a single thing.
 You have to be focused on whatever arises clearly, seeing
 it clearly.
 That's why we use this word when we say to ourselves
 seeing or hearing or rising, falling.
 We're trying to focus.
 Focus means on this, the core of it.
 The core of this is rising or whatever rising means.
 This is falling.
 The core of seeing is seeing. The core of pain is pain.
 There's no judgments or reactions or anything.
 Get rid of all of those.
 Number six is apamada, apamade, agarwa, wihranati, apatissa
.
 Apamada is the last words of the Buddha
 is that we should cultivate apamada.
 Pamada comes from the root mud.
 Mud means to be drunk.
 Pamada is like really drunk.
 But it's just a prefix that modifies it, that augments it.
 So, pamada is like negligent or intoxicated, mixed up in
 the mind.
 Apamada is to be clear-minded, to be un-intoxicated, to be
 sober.
 So not drunk on lust or drunk on anger, drunk on delusion
 or arrogance or conceit.
 To not be drunk on any of these emotions.
 So we should take that seriously.
 We should appreciate that.
 If we don't appreciate that, that's what the Buddha says.
 And the seventh one is quite curious.
 But curious in a good way.
 It's just surprising, I think.
 It's not what you'd expect as the last one.
 Pati santara.
 Ajahn, my teacher talked about it. He mentions this.
 Many times the Buddha talks about pati santara.
 Pati santara agarwa.
 Pati santara agarwa.
 If we don't take seriously or appreciate pati santara.
 Pati santara is, I don't quite know the etymology, but it
 means hospitality.
 Or it could mean friendliness, it could mean goodwill,
 friendship.
 But it's really just hospitality when you welcome people.
 But in this context, it makes perfect sense, right?
 If you have a meditation center where you don't welcome
 people,
 when someone walks in, they don't know.
 Everyone looks at them kind of, "What are you doing here?"
 Sometimes you go to a monastery and no one wants to talk to
 you.
 Go to a meditation center and maybe no one, everyone's,
 if people are kind of putting on airs like they're real med
itators or something.
 Or if they just don't care and they're just concerned about
 their own practice.
 If such a place existed, then how would you ever share?
 How would that ever lead to spreading the dhamma?
 I've heard, there are places where you go into the office,
 the meditation center office and they just yell at you.
 They look down upon you and they don't know how to deal
 with people.
 And I've talked with other people.
 In Chum Tong, the people in the office would rotate.
 And so you had to catch the right person in the office
 or they never know what was going to happen.
 You want to book a room for someone and maybe they yell at
 you,
 maybe they're very nice.
 You have to find the right person and talk to them.
 It's very important.
 So it's something for us to remember.
 We have to welcome.
 And in general, you could talk about this.
 Something I was thinking about is it'd be neat someday
 if our community gets, if our community grows,
 if we could have groups of people, you know,
 like maybe once a week we could have kind of a more formal
 online session.
 And if someone was organizing a group in their home,
 then they could join the hangout with their group.
 Wow, wouldn't that be neat?
 That's what we should do.
 Once a week, we could do once a month to start.
 We have groups of people.
 And someone puts together a group in their home
 and people come to their home and they join the hangout.
 And we could have up to 10 groups.
 We have to talk about that.
 So the idea is to welcome people even into your home, you
 know.
 You set up a Dhamma group.
 People do this.
 They have a Dhamma group in their home.
 They've got space.
 And then they set up.
 They, you know, sometimes they just meditate together.
 Sometimes they listen to a talk.
 I know Dhamma groups that just put on some CD or something,
 you know, somebody giving a talk.
 And everybody listens and then they do meditation together.
 So we could do something like that.
 But it could be live, you know, live from all over the
 world.
 10 different groups.
 That's the maximum you can have 10 people in the hangout.
 So if anybody's interested in setting up a Dhamma group,
 let me know.
 And we'll try and arrange something where we meet together
 like this,
 but we'll have a special day where I'll give a real talk.
 I'll give a longer talk.
 And then we'll connect.
 All right.
 So those are the seven.
 That's one list of seven things that lead to lead to the D
hamma lasting.
 So if we don't spread it, if we don't share it,
 if we're not welcoming of people who want to learn.
 And the Buddha wasn't big on on spreading the Dhamma in
 terms of going around
 teaching people, but much more about wealth,
 as far as I can see much more about welcoming people who
 wanted to learn.
 And people wanted to learn.
 It was all about finding ways to accommodate them.
 If they do come, if they don't come,
 don't have to go out of our way looking for people.
 We're not trying to push it this on anybody.
 That's kind of how beautiful it is.
 We don't need students.
 We're not looking for students.
 And just people who are looking for it, we open the door
 for them,
 provide them the opportunity.
 Tonight a woman came to visit.
 She lived in Cambodia for nine years.
 And she just came and she's seen some of my videos,
 and I gave her the booklet.
 And then she just did meditation.
 I didn't meditate with her.
 I came upstairs and did my -- because I do walking.
 She didn't want to do walking.
 So -- but it would be nice if we could have a group here as
 well.
 I think probably what we'd do is just have people come up
 here at nine.
 If someone wants to hear the Dhamma, they can come at nine,
 come sit up with us here and -- for now, anyway.
 We're looking to get a bigger place, so.
 Anyway, so these seven, the Buddha, the Dhamma, the Sangha,
 take them seriously.
 That's another thing is often people don't take the Buddha
 too seriously
 or they -- you know, these talks about people who spit on
 Buddha images
 and burn them and just treat them like rubbish,
 thinking that's not the real Buddha.
 But there's something to it, you know.
 If you don't take the Buddha images seriously,
 what does that say about how you feel about the Buddha?
 Yeah, people say it's just an image of the Buddhas and
 whatever,
 but there's something to images.
 They represent something.
 In ancient times, they wouldn't even make images
 because they revered the Buddha, it seems,
 because they revered the Buddha so much.
 So when we have these images, we try to treat them quite
 carefully
 because we respect the Buddha so much.
 If you don't, I mean, this is the thing,
 this is the Dhamma won't last because there's no figure,
 there's no -- there's none of this sort of religious
 feeling
 that keeps things together, the sense of urgency,
 the sense of zeal and interest that's so powerful in any
 religion.
 It could be for the purposes of evil, it could be for the
 purposes of good,
 but it's a power.
 If you don't take these things seriously,
 I don't appreciate them, don't revere them.
 Very hard to keep going.
 You know, we talk about secular Buddhism,
 yeah, it's fine, but it's hard to get that feeling and
 appreciation, you know.
 Part of religion is the feeling,
 the sense of reverence, appreciation,
 not just taking something clinically in terms of,
 yes, this helps me relieve stress,
 but yes, this is the teaching of the perfectly enlightened
 Buddha,
 you know, it carries a lot more weight, you know, in that
 sense.
 And then the training, concentration,
 Apamada, which is vigilance or sobriety, and hospitality.
 We have to take the training seriously, both study and
 practice.
 We have to take the concentration,
 remember to try and be concentrated, remember and practice,
 not just study.
 And be mindful, Apamada actually being sober,
 the Buddha said it's a synonym for being mindful,
 so this really means using mindfulness,
 seeing things clearly as they are,
 grasping things as they are, remembering things as they are
.
 And finally, we have to be hospitable,
 so we have to welcome people to join,
 not just practicing for ourselves,
 but providing the opportunity and being friendly and
 welcoming.
 Don't just shy away and say,
 "Oh, I'm not a teacher, I can't teach you the Dhamma,
 anyone can teach."
 You teach how you were taught, pass it on.
 It doesn't mean you have to answer all of their questions
 and problems and give them a device,
 it just means you have to explain to them how to do
 meditation,
 which is quite simple.
 And just reassure them that it has benefits,
 and if they try it, they will see the benefits for
 themselves.
 That's all you need to do.
 So,
 well, that's the Dhamma for tonight.
 You guys can go.
 [
 silence ]
 Okay, Larry has a question.
 Well, noting posture, movements, and tensions,
 I might lapse into contemplating death
 or contemplating my good fortune,
 and I realize I'm contemplating and not noting.
 How should one's balance the process of noting
 and the process of also contemplation?
 Well, if that thought arises, that's fine,
 you can do both, you just have the thought
 and then you say thinking, thinking,
 or if you feel happy, you can say happy, happy.
 I mean, the thought arises on its own,
 we're just trying to be mindful of it,
 because even wholesomeness can be caught up in delusion,
 not directly, but you can become unwholesome
 about your wholesomeness if you start to get attached to it
,
 attached to the idea of it anyway.
 [ silence ]
 I mean, it won't lead to freedom,
 so you can switch back and forth,
 for the death one especially,
 if you're mindful of death, that's a useful meditation,
 it's good to give you the impetus to practice,
 it gives you those religious feeling,
 sanwega we call it,
 so that's mindful, it's a different meditation,
 it's useful to practice that
 and then go back to practicing mindfulness.
 As far as the contemplation of good fortune,
 be careful of that, because it can slip into complacency.
 The angels think like that, they think everything,
 they think they've got some safety or so on,
 they probably are not having that problem,
 but you have to be careful about that,
 I'm not convinced that it's wholesome.
 We talk about contentment,
 there's something in there that's probably
 associated with contentment,
 but appreciation is a lot like liking,
 and clinging, because things can change at any time,
 your safety is completely impermanent,
 so it's an illusion, there's no safety in the samsara.
 Everything can leave you in the moment,
 so in what way is it safe?
 In the end it's all just seeing, hearing,
 smelling, tasting, feeling, thinking.
 I'm continuing the series on the Dhammapada,
 it's probably the only reason I continue them
 is because people ask me the questions like this,
 because I always think,
 "Oh well, maybe nobody wants them anymore,
 I haven't heard, nobody's asked about them in a while,
 so probably people are sick of them."
 I was busy with finals, but it's kind of just,
 well now I'm not, so sure I can start up
 the Dhammapada series again.
 I mean it's not that I stopped, I can continue it,
 do more Dhammapada if people want it.
 How is it that we bow down to and revere
 the meditation practice, I mean the practice itself?
 Should we hold the triple gem or the practice
 in mind for reverence?
 Well I don't think you need to hold anything
 in mind for reverence,
 it's just taking it seriously.
 I mean you can do meditations on the Buddha,
 the Dhamma and the Sangha obviously,
 but that's not what I don't think what the Buddha's saying,
 he's just if they don't respect,
 because there's a lot of disrespect,
 disrespect for the Sangha for example,
 monks get a lot of disrespect,
 all of it, teachers get a lot of disrespect,
 not a lot, not mostly, mostly respect,
 but there's always people who have very little respect
 and the point is well that is harmful to the,
 you know, you could say respect has to be earned,
 sure fine, but there's something about being respectful,
 if you don't want to practice,
 you don't have to come practice,
 but if someone's teaching, you know,
 there's a lot of, and then there's disrespect to the Buddha
,
 disrespect to the Dhamma,
 you know, not taking it seriously,
 disrespect, not necessarily disrespect, right,
 but not taking it at practice seriously,
 you know, people who do walking meditation, talking on the
 phone,
 and it's not even how disrespectful that is,
 it's just, you know, if you don't take it seriously,
 you're not going to get any results,
 so the question if you do an hour of meditation,
 how much are you really meditating,
 are you really taking it seriously,
 are you, do you respect,
 and it's not respect in terms of disrespect,
 it's like do you appreciate, that's the best,
 do you appreciate,
 and Garo means heavy, so it's like taking it seriously,
 seeing it as a weighty thing,
 or do you see it, do you take it lightly, right,
 it's the opposite of taking something lightly,
 if you take meditation lightly,
 it's hard, you won't get the results,
 if you don't get the results, Buddhism will pass away.
 [Silence]
 But yeah, if you want to do this, if you want to pay
 respect,
 like there's a quick chant that we do,
 we do it in the opening ceremony,
 so I often do it as a, kind of like a mantra really,
 we pay respect to the five things,
 the Buddha, the Dhamma, the Sangha,
 the meditation practice,
 and the teacher who offers the meditation practice.
 Namami Buddha Ngunasa Garantang,
 [Silence]
 Namami Dhamma Muniraja Desitang,
 Namami Sangham Muniraja Savakang,
 Namami Kamathanak Nivana Dikamupayang,
 Namami Kamathanak Jayakacharya Nivana Magudesa Kang,
 Sabangdo Sang Kamantuno,
 the last part, Sabangdo Sang for all faults,
 for all faults, Kamantuno, may they forgive us.
 May they forgive us all,
 discretions, any wrongdoing.
 It's in the opening ceremony,
 I'm not sure where you get the opening ceremony.
 I think it's in our,
 where is the opening ceremony?
 Maybe it's not even online.
 But it might be taken from the Visuddhi manga,
 a lot of that is taken from the Visuddhi manga.
 Maybe not.
 Hmm.
 Yeah, respect for the five.
 It's what we do before we start the minute.
 It's the first thing we do in the opening ceremony,
 almost the first thing, first big thing.
 No, no, this is the first thing, isn't it?
 Yeah, it's how we start the opening ceremony
 and the closing ceremony.
 So I don't do ceremonies here, not yet anyway.
 But normally we'd have a ceremony
 where we go through all of this in Pali.
 It's quite nice.
 The kind of, it's the thing,
 you should take it seriously.
 If we were to take it seriously,
 we probably should do the ceremonies.
 But you know, in the West, sometimes, again,
 people don't take it seriously enough.
 It's hard to push that on them.
 It's not entirely necessary.
 It doesn't mean you do a ceremony.
 That's the thing, it's just a ceremony.
 But something to consider once we get established here.
 Maybe I can teach Michael,
 because another thing is you need two people.
 I do part, but I need a layperson to lead the meditators.
 I need someone to lead the meditators.
 I'll teach Michael how to do it someday.
 If he sticks around.
 Maybe he'll become a monk.
 If he does, I get to give him the name Mogaraja again.
 Because it's the closest thing to Michael, right?
 So our last Michael got the name Mogaraja,
 and boy did that cause a stir.
 You know, Sri Lankan people said they didn't want to bow
 down to it.
 Because the word Moga means useless or bad or stupid.
 But Mogaraja was one of the Buddha's chief disciples,
 one of the 80 great disciples.
 Shame, really.
 Yeah, I have the five reverences somewhere.
 Not sure where, but probably buried away somewhere in my
 documents folder.
 You copy them.
 Anyway, that's all for tonight.
 Thank you all for tuning in.
 Have a good night.
 Thank you.
