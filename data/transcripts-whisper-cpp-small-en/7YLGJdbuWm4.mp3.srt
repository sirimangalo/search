1
00:00:00,000 --> 00:00:02,920
 Hi, welcome back to Ask a Monk.

2
00:00:02,920 --> 00:00:11,650
 Today's question comes from jkix22, who says, "What is the

3
00:00:11,650 --> 00:00:13,160
 best way when you're starting

4
00:00:13,160 --> 00:00:15,320
 out not to get frustrated?

5
00:00:15,320 --> 00:00:25,280
 I can't seem to keep my mind quiet enough."

6
00:00:25,280 --> 00:00:29,070
 One thing to say is that the best way to start out is to

7
00:00:29,070 --> 00:00:31,400
 find a meditation center where you

8
00:00:31,400 --> 00:00:37,360
 can undergo an intensive meditation course because that

9
00:00:37,360 --> 00:00:40,400
 will help you to work out a lot

10
00:00:40,400 --> 00:00:46,050
 of the issues that you have, the frustration, the things

11
00:00:46,050 --> 00:00:48,320
 that lead to frustration.

12
00:00:48,320 --> 00:00:52,920
 When you're with the teacher, you get a lot of direct

13
00:00:52,920 --> 00:00:54,640
 explanation and the intensity of

14
00:00:54,640 --> 00:00:59,780
 the practice really allows you to overcome so much of what

15
00:00:59,780 --> 00:01:03,120
 gets in your way in the beginning.

16
00:01:03,120 --> 00:01:09,770
 There's such a power to the practice that rather than being

17
00:01:09,770 --> 00:01:12,440
 frustrated by the seeming

18
00:01:12,440 --> 00:01:18,540
 endless stream of hindrance and difficulty, you feel

19
00:01:18,540 --> 00:01:22,080
 encouraged by the fact that in a

20
00:01:22,080 --> 00:01:27,560
 matter of days or weeks, you're able to overcome it.

21
00:01:27,560 --> 00:01:31,010
 Barring that or even when you do go to do a meditation

22
00:01:31,010 --> 00:01:33,520
 course, it's important to understand

23
00:01:33,520 --> 00:01:40,780
 that you're really in a sense answering yourself.

24
00:01:40,780 --> 00:01:46,210
 The reason that you're frustrated is because in this case

25
00:01:46,210 --> 00:01:49,200
 or in generally, there's a need

26
00:01:49,200 --> 00:01:51,200
 to keep the mind quiet.

27
00:01:51,200 --> 00:01:53,120
 There's a judgment of the mind.

28
00:01:53,120 --> 00:01:59,060
 The mind is not quiet, that's bad, therefore I'm frustrated

29
00:01:59,060 --> 00:02:01,760
 and therefore there's something

30
00:02:01,760 --> 00:02:05,640
 wrong.

31
00:02:05,640 --> 00:02:06,640
 That's really the problem.

32
00:02:06,640 --> 00:02:08,080
 That's what we come to see in meditation.

33
00:02:08,080 --> 00:02:11,320
 We're not practicing meditation to attain a specific state

34
00:02:11,320 --> 00:02:12,760
 of being where the mind is

35
00:02:12,760 --> 00:02:15,240
 always calm and peaceful.

36
00:02:15,240 --> 00:02:21,770
 We're trying to create a sense of ease and peace but

37
00:02:21,770 --> 00:02:25,920
 lasting peace rather than peace

38
00:02:25,920 --> 00:02:29,320
 from its peace with where we're able to be at peace with

39
00:02:29,320 --> 00:02:31,040
 ourselves, at peace with the

40
00:02:31,040 --> 00:02:38,160
 mind where we're no longer judging the state of mind.

41
00:02:38,160 --> 00:02:43,040
 What we learn in the meditation boils down to three things.

42
00:02:43,040 --> 00:02:46,450
 We learn that everything inside of ourselves and in the

43
00:02:46,450 --> 00:02:48,440
 world around us is changing.

44
00:02:48,440 --> 00:02:51,740
 There's nothing which is stable, which is going to always

45
00:02:51,740 --> 00:02:53,520
 be in a certain way, is always

46
00:02:53,520 --> 00:02:57,360
 going to exist or is never going to arise.

47
00:02:57,360 --> 00:03:02,280
 Everything is uncertain and unstable and impermanent.

48
00:03:02,280 --> 00:03:05,630
 The second thing is that both inside of ourselves and in

49
00:03:05,630 --> 00:03:07,920
 the world around us, everything is

50
00:03:07,920 --> 00:03:12,290
 necessarily unsatisfying because it's impermanent, because

51
00:03:12,290 --> 00:03:13,560
 nothing lasts.

52
00:03:13,560 --> 00:03:15,810
 There's no one thing that you can hold onto and say, "That

53
00:03:15,810 --> 00:03:17,120
's going to make me happy."

54
00:03:17,120 --> 00:03:21,210
 There's nothing in this world that can be the object of

55
00:03:21,210 --> 00:03:22,760
 your happiness.

56
00:03:22,760 --> 00:03:28,130
 True happiness has to be unrelated to objects, unrelated to

57
00:03:28,130 --> 00:03:31,240
 external phenomenon, unrelated

58
00:03:31,240 --> 00:03:33,860
 to a specific experience.

59
00:03:33,860 --> 00:03:39,850
 This has to be a contentment that is able to experience all

60
00:03:39,850 --> 00:03:43,040
 things equally as they are.

61
00:03:43,040 --> 00:03:46,640
 The third thing that we realize in the meditation is that

62
00:03:46,640 --> 00:03:48,960
 both inside of ourselves and in the

63
00:03:48,960 --> 00:03:55,560
 world around us, there's nothing that belongs to us.

64
00:03:55,560 --> 00:03:59,890
 There's nothing in the world that we can say is under our

65
00:03:59,890 --> 00:04:02,440
 control, that is truly ours, because

66
00:04:02,440 --> 00:04:06,800
 at best we're borrowing it or we're in control of it for a

67
00:04:06,800 --> 00:04:08,000
 short time.

68
00:04:08,000 --> 00:04:12,900
 We're only in control of it in a very limited sense in

69
00:04:12,900 --> 00:04:16,440
 terms of giving rise to certain effects

70
00:04:16,440 --> 00:04:21,520
 based on certain causes.

71
00:04:21,520 --> 00:04:25,480
 We can't force our minds to always be quiet.

72
00:04:25,480 --> 00:04:29,080
 When we try, we find we only create tension and suffering.

73
00:04:29,080 --> 00:04:31,140
 We can't force pain to never arise.

74
00:04:31,140 --> 00:04:32,960
 We can't force happiness to stay.

75
00:04:32,960 --> 00:04:36,220
 We can't force things to always be the way we want.

76
00:04:36,220 --> 00:04:39,310
 It's because we don't see these three things, because we

77
00:04:39,310 --> 00:04:41,000
 think that the world around us

78
00:04:41,000 --> 00:04:45,970
 or certain things are permanent, that certain things are

79
00:04:45,970 --> 00:04:49,000
 satisfying and certain things are

80
00:04:49,000 --> 00:04:53,650
 controllable or under our control, that we give rise to

81
00:04:53,650 --> 00:04:57,320
 frustration, that we're not satisfied,

82
00:04:57,320 --> 00:05:00,780
 that we're always seeking and searching and wanting and

83
00:05:00,780 --> 00:05:02,800
 needing, that we're not truly

84
00:05:02,800 --> 00:05:05,240
 happy.

85
00:05:05,240 --> 00:05:10,190
 Really the answer to your question is that through the

86
00:05:10,190 --> 00:05:13,440
 practice you will see that, yes,

87
00:05:13,440 --> 00:05:16,160
 you can't keep your mind quiet and that's a good thing.

88
00:05:16,160 --> 00:05:18,870
 When you realize, it's not a good thing, it's a good thing

89
00:05:18,870 --> 00:05:20,360
 to realize that, because when

90
00:05:20,360 --> 00:05:24,550
 you realize that and you realize that that's the nature of

91
00:05:24,550 --> 00:05:26,960
 it rather than a problem, when

92
00:05:26,960 --> 00:05:30,630
 you change, when your mind shifts, because right now you're

93
00:05:30,630 --> 00:05:32,280
 in this conflict state and

94
00:05:32,280 --> 00:05:35,000
 this is what happens when you meditate.

95
00:05:35,000 --> 00:05:37,740
 It comes in conflict with the way you think things should

96
00:05:37,740 --> 00:05:38,160
 be.

97
00:05:38,160 --> 00:05:40,000
 You think that your mind should be controllable.

98
00:05:40,000 --> 00:05:44,540
 You think you should be able to say, "Stop thinking," and

99
00:05:44,540 --> 00:05:46,480
 the mind quiets down.

100
00:05:46,480 --> 00:05:49,000
 Now you're seeing that reality is not that way.

101
00:05:49,000 --> 00:05:52,120
 You're seeing the truth and it's clear from what you say.

102
00:05:52,120 --> 00:05:55,480
 You can't keep your mind quiet.

103
00:05:55,480 --> 00:06:00,160
 The problem is you have this judgment saying that it's not

104
00:06:00,160 --> 00:06:01,640
 quiet enough.

105
00:06:01,640 --> 00:06:05,060
 The question that you have to ask is, "Quiet enough for

106
00:06:05,060 --> 00:06:05,760
 what?"

107
00:06:05,760 --> 00:06:10,100
 Quiet enough for your standards, for your judgment, for

108
00:06:10,100 --> 00:06:10,800
 your need.

109
00:06:10,800 --> 00:06:13,800
 The problem is not the noise in the mind.

110
00:06:13,800 --> 00:06:17,970
 The problem is your judgment of it, is the fact that we say

111
00:06:17,970 --> 00:06:19,880
 to ourselves, "It's not

112
00:06:19,880 --> 00:06:21,460
 acceptable.

113
00:06:21,460 --> 00:06:24,720
 It's not quiet enough."

114
00:06:24,720 --> 00:06:28,950
 Once you can accept the fact that it's a part of nature and

115
00:06:28,950 --> 00:06:30,880
 it's created by your way of

116
00:06:30,880 --> 00:06:36,160
 life, the things you have done to make it that way, then

117
00:06:36,160 --> 00:06:38,680
 you start to realize that it's

118
00:06:38,680 --> 00:06:43,150
 not really a problem per se, or it's not your problem in

119
00:06:43,150 --> 00:06:45,860
 the sense that you have to do something

120
00:06:45,860 --> 00:06:46,860
 to fix it.

121
00:06:46,860 --> 00:06:48,640
 It's just naturally like that.

122
00:06:48,640 --> 00:06:53,980
 In fact, there is nothing particularly wrong with an active

123
00:06:53,980 --> 00:06:54,760
 mind.

124
00:06:54,760 --> 00:06:56,440
 That's just the nature of it.

125
00:06:56,440 --> 00:06:59,500
 The problem is that we feel the need for it to be in a

126
00:06:59,500 --> 00:07:00,720
 different way.

127
00:07:00,720 --> 00:07:04,340
 Once you can see it and see it for what it is and let it be

128
00:07:04,340 --> 00:07:06,120
 the way it is, you won't

129
00:07:06,120 --> 00:07:07,760
 become frustrated.

130
00:07:07,760 --> 00:07:10,540
 In a sense, you're actually practicing well and it's a sign

131
00:07:10,540 --> 00:07:11,840
 that you're actually doing

132
00:07:11,840 --> 00:07:12,840
 things properly.

133
00:07:12,840 --> 00:07:17,150
 The fact that you're seeing that things are not the way you

134
00:07:17,150 --> 00:07:19,240
 thought they were, you're

135
00:07:19,240 --> 00:07:23,960
 seeing that the mind is not under your control.

136
00:07:23,960 --> 00:07:26,730
 Slowly as you practice in this way, you overcome your

137
00:07:26,730 --> 00:07:27,680
 frustration.

138
00:07:27,680 --> 00:07:30,520
 Right now, you're fighting.

139
00:07:30,520 --> 00:07:33,140
 As you fight on and on and on, your mind slowly starts to

140
00:07:33,140 --> 00:07:35,040
 give in, starts to say, "Okay, okay,

141
00:07:35,040 --> 00:07:36,040
 okay.

142
00:07:36,040 --> 00:07:37,040
 Let it be the way it is.

143
00:07:37,040 --> 00:07:38,280
 I can't control it."

144
00:07:38,280 --> 00:07:39,520
 That's just the nature.

145
00:07:39,520 --> 00:07:40,980
 That's not a problem.

146
00:07:40,980 --> 00:07:42,320
 That's the way it is.

147
00:07:42,320 --> 00:07:45,250
 When you stop trying to force things, you stop trying to

148
00:07:45,250 --> 00:07:46,320
 control things.

149
00:07:46,320 --> 00:07:50,820
 That's really a key to the realization of enlightenment in

150
00:07:50,820 --> 00:07:51,840
 Buddhism.

151
00:07:51,840 --> 00:07:52,840
 I hope that helps.

152
00:07:52,840 --> 00:07:55,950
 I'd like to encourage you because from my point of view,

153
00:07:55,950 --> 00:07:57,560
 your practice is quite clear

154
00:07:57,560 --> 00:07:59,440
 and you're on the right path.

155
00:07:59,440 --> 00:08:03,010
 Just keep going and slowly and patiently you'll be able to

156
00:08:03,010 --> 00:08:05,000
 overcome this frustration.

157
00:08:05,000 --> 00:08:10,320
 Try to say to yourself, "Frustrated, frustrated or angry,

158
00:08:10,320 --> 00:08:10,880
 angry."

159
00:08:10,880 --> 00:08:13,720
 If you're thinking a lot, just say to yourself, "Distracted

160
00:08:13,720 --> 00:08:15,120
, distracted," so that you get

161
00:08:15,120 --> 00:08:18,400
 to the point where you're able to accept it and move on.

162
00:08:18,400 --> 00:08:19,400
 Okay?

