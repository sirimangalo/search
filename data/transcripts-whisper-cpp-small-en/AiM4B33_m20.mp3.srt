1
00:00:00,000 --> 00:00:04,320
 Hi and welcome back to our study of the Dhammapada.

2
00:00:04,320 --> 00:00:08,360
 Today we continue on with verse number 83 which reads as

3
00:00:08,360 --> 00:00:10,160
 follows.

4
00:00:10,160 --> 00:00:19,080
 "Sabhata vai sapurisa chajanti nakamakamalapayanti santoh

5
00:00:19,080 --> 00:00:29,160
 sukena vaputa atavadukena nakuchavacang pandita dasa yanti"

6
00:00:29,160 --> 00:00:39,450
 Which means "Sabhata vai sapurisa chajanti sapurisa" good

7
00:00:39,450 --> 00:00:42,160
 people are always renouncing, are everywhere.

8
00:00:42,160 --> 00:00:51,080
 In every instance, on every occasion renouncing, giving up,

9
00:00:51,080 --> 00:00:52,160
 giving up.

10
00:00:52,160 --> 00:00:59,860
 "Nakamakamalapayanti santoh" they don't mutter or blather

11
00:00:59,860 --> 00:01:11,160
 or chatter as though they were enamored by sensuality.

12
00:01:11,160 --> 00:01:19,170
 "In love with love" or lusting after lustful things, desir

13
00:01:19,170 --> 00:01:22,160
ous of the desirables.

14
00:01:22,160 --> 00:01:27,010
 "Sukenaputa atavadukena" when touched by either happiness

15
00:01:27,010 --> 00:01:28,160
 or suffering.

16
00:01:28,160 --> 00:01:32,760
 "Nuchavacang pandita dasa yanti" they don't exhibit, the

17
00:01:32,760 --> 00:01:38,160
 wise don't exhibit highs or lows.

18
00:01:38,160 --> 00:01:41,280
 So ordinary people when touched by suka or dukka, happiness

19
00:01:41,280 --> 00:01:46,160
 or suffering, they are affected, they are elated.

20
00:01:46,160 --> 00:01:52,940
 They rejoice about their good fortune, they become somehow

21
00:01:52,940 --> 00:01:55,160
 dependent on it.

22
00:01:55,160 --> 00:02:05,950
 They step into it and they come to expect it. When touched

23
00:02:05,950 --> 00:02:14,160
 by suffering then they lament and bemoan their state.

24
00:02:14,160 --> 00:02:19,680
 "Bhara sapurisa" is ever chajanti, is ever renouncing,

25
00:02:19,680 --> 00:02:21,160
 letting go.

26
00:02:21,160 --> 00:02:30,630
 So that's the verse. It's in regards to a story about 500

27
00:02:30,630 --> 00:02:34,160
 monks and 500 beggars.

28
00:02:34,160 --> 00:02:42,770
 So in the time of the Buddha, after the Buddha was recently

29
00:02:42,770 --> 00:02:46,160
 enlightened, he was staying in Wiranja.

30
00:02:46,160 --> 00:02:50,200
 He lived there for the rains. He had been invited to stay

31
00:02:50,200 --> 00:02:51,160
 in Wiranja.

32
00:02:51,160 --> 00:02:59,160
 If you read the beginning of the Vinayat,

33
00:02:59,160 --> 00:03:05,840
 "Tena ko panayana samayena buddho bhagavawe ranja yang wih

34
00:03:05,840 --> 00:03:06,160
arati"

35
00:03:06,160 --> 00:03:12,000
 the Buddha was dwelling in Wiranja and this Brahman invited

36
00:03:12,000 --> 00:03:17,160
 him to stay there and then forgot about him.

37
00:03:17,160 --> 00:03:23,310
 So the Buddhist and his 500 monks dwelt in great hardship

38
00:03:23,310 --> 00:03:28,280
 and they only were able to survive because of some horse

39
00:03:28,280 --> 00:03:29,160
 traders

40
00:03:29,160 --> 00:03:33,970
 who gave them horse food, gave them oats, something like

41
00:03:33,970 --> 00:03:36,160
 oats, some kind of grain.

42
00:03:36,160 --> 00:03:41,160
 They were able to live off this grain.

43
00:03:41,160 --> 00:03:44,210
 But it was great hardship so they went on alms but I guess

44
00:03:44,210 --> 00:03:45,160
 didn't get much.

45
00:03:45,160 --> 00:03:48,190
 This Brahman who invited them to this area where they weren

46
00:03:48,190 --> 00:03:49,160
't able to get alms

47
00:03:49,160 --> 00:03:54,160
 totally forgot about them until the end of the rains.

48
00:03:54,160 --> 00:03:58,390
 But it says here that there were a bunch of beggars living

49
00:03:58,390 --> 00:03:59,160
 with them.

50
00:03:59,160 --> 00:04:02,860
 "By the kindness of the monks 500 eaters of refuse lived

51
00:04:02,860 --> 00:04:05,160
 within the monastery enclosure."

52
00:04:05,160 --> 00:04:29,160
 [Silence]

53
00:04:29,160 --> 00:04:35,760
 It doesn't even say that but what it does say is that the

54
00:04:35,760 --> 00:04:40,160
 point is these beggars were before

55
00:04:40,160 --> 00:04:43,060
 so they weren't living I was wrong, they weren't living

56
00:04:43,060 --> 00:04:44,160
 with the Buddha at Wiranja.

57
00:04:44,160 --> 00:04:47,530
 But in Wiranja the monks, these 500 monks were all at least

58
00:04:47,530 --> 00:04:50,160
 dhotapana, maybe all arahants

59
00:04:50,160 --> 00:04:55,460
 and they were all living with the Buddha and they were

60
00:04:55,460 --> 00:04:58,160
 content with their poor fare.

61
00:04:58,160 --> 00:05:02,160
 They were at peace even in great hardship.

62
00:05:02,160 --> 00:05:07,350
 Later on they came to great wealth and prosperity, not

63
00:05:07,350 --> 00:05:08,160
 wealth in terms of money

64
00:05:08,160 --> 00:05:11,160
 but they were very well cared for.

65
00:05:11,160 --> 00:05:16,160
 They moved to Sawati and they lived there.

66
00:05:16,160 --> 00:05:22,160
 But they were still the same, they were still at peace.

67
00:05:22,160 --> 00:05:25,390
 So whether they lived in hardship or whether they lived in

68
00:05:25,390 --> 00:05:28,160
 great comfort they were the same.

69
00:05:28,160 --> 00:05:32,980
 And then you had these 500 beggars who appeared after there

70
00:05:32,980 --> 00:05:34,160
 was great wealth

71
00:05:34,160 --> 00:05:38,270
 and because of their own hardship decided that they would

72
00:05:38,270 --> 00:05:39,160
 hang out at the monastery

73
00:05:39,160 --> 00:05:47,020
 and so through the kindness of the monks these 500 beggars

74
00:05:47,020 --> 00:05:49,160
 lived off the leftover food

75
00:05:49,160 --> 00:05:52,420
 because everyone was always bringing food to the monks and

76
00:05:52,420 --> 00:05:54,160
 coming to hear the monks teach

77
00:05:54,160 --> 00:05:58,070
 and supporting the monks in different ways and so there was

78
00:05:58,070 --> 00:05:59,160
 lots of leftovers

79
00:05:59,160 --> 00:06:02,160
 and so these beggars would come.

80
00:06:02,160 --> 00:06:05,720
 And it says they would eat the choice food left over by the

81
00:06:05,720 --> 00:06:08,160
 monks and then lie down and sleep.

82
00:06:08,160 --> 00:06:10,620
 And then when they got up from their sleep they would be

83
00:06:10,620 --> 00:06:13,160
 full of energy and they would go and wrestle.

84
00:06:13,160 --> 00:06:17,200
 They would shout and jump and wrestle and play, misbehave

85
00:06:17,200 --> 00:06:19,160
 both within and without the monastery.

86
00:06:19,160 --> 00:06:23,160
 They did nothing but misbehave.

87
00:06:23,160 --> 00:06:28,750
 And so the monks were discussing this and they noted the

88
00:06:28,750 --> 00:06:30,160
 difference.

89
00:06:30,160 --> 00:06:37,890
 Because ostensibly a part of the problem for these beggars

90
00:06:37,890 --> 00:06:40,160
 was the opulence.

91
00:06:40,160 --> 00:06:44,510
 Once they got great food they got strength and energy and

92
00:06:44,510 --> 00:06:48,160
 they just started acting rambunctious.

93
00:06:48,160 --> 00:06:54,360
 Whereas the monks whether they were in hardship or in op

94
00:06:54,360 --> 00:06:59,160
ulence or affluence were unchanged.

95
00:06:59,160 --> 00:07:06,370
 And so the Buddha for that reason he told a jataka story

96
00:07:06,370 --> 00:07:08,160
 about horses

97
00:07:08,160 --> 00:07:11,160
 which is actually quite interesting because it points out

98
00:07:11,160 --> 00:07:15,160
 that even it's a story about some donkeys and horses

99
00:07:15,160 --> 00:07:21,370
 and these thoroughbred horses were fed, it was an accident

100
00:07:21,370 --> 00:07:26,680
 or something, they were fed wine of some sort or some kind

101
00:07:26,680 --> 00:07:27,160
 of alcohol.

102
00:07:27,160 --> 00:07:31,160
 And were unaffected by it.

103
00:07:31,160 --> 00:07:34,480
 But these donkeys who were hanging out with the thorough

104
00:07:34,480 --> 00:07:38,620
bred horses drank the same wine and started braying and

105
00:07:38,620 --> 00:07:40,160
 carrying on.

106
00:07:40,160 --> 00:07:45,950
 The point being that even alcohol, even though we rail

107
00:07:45,950 --> 00:07:50,160
 against it as being a terrible, terrible thing,

108
00:07:50,160 --> 00:07:55,160
 all it has the power to do is remove your inhibitions.

109
00:07:55,160 --> 00:07:59,710
 So if you don't have any harmful intentions in your mind it

110
00:07:59,710 --> 00:08:02,160
 doesn't actually hurt you.

111
00:08:02,160 --> 00:08:06,490
 Unfortunately for most of us that includes delusion, it

112
00:08:06,490 --> 00:08:08,160
 includes ego and so on.

113
00:08:08,160 --> 00:08:13,160
 So if you have none of that then you're fine.

114
00:08:13,160 --> 00:08:20,360
 Anyway he makes the point that there's a difference between

115
00:08:20,360 --> 00:08:21,160
 people.

116
00:08:21,160 --> 00:08:25,620
 That for an ordinary person it's actually an interesting

117
00:08:25,620 --> 00:08:28,160
 point because for ordinary people it's sometimes the other

118
00:08:28,160 --> 00:08:28,160
 way around.

119
00:08:28,160 --> 00:08:32,230
 Sometimes when things are fine people can be quite moral

120
00:08:32,230 --> 00:08:33,160
 and ethical.

121
00:08:33,160 --> 00:08:37,310
 When you don't need to steal, when you don't need to kill,

122
00:08:37,310 --> 00:08:41,160
 when there's no adversity, when there's no danger.

123
00:08:41,160 --> 00:08:45,920
 But when the going gets tough many people will abandon

124
00:08:45,920 --> 00:08:47,160
 their ethics,

125
00:08:47,160 --> 00:08:57,160
 abandon their goodness, their generosity.

126
00:08:57,160 --> 00:09:00,160
 For other people it's the other way around.

127
00:09:00,160 --> 00:09:06,160
 In hardship they are forced to behave themselves.

128
00:09:06,160 --> 00:09:09,890
 They have to act in such a way that other people respect

129
00:09:09,890 --> 00:09:11,160
 them and so on.

130
00:09:11,160 --> 00:09:16,160
 But when things are good they get lazy.

131
00:09:16,160 --> 00:09:20,160
 For monastics this is very much the case.

132
00:09:20,160 --> 00:09:25,500
 When things are rough you're forced to be mindful because

133
00:09:25,500 --> 00:09:28,500
 there's lots of physical pain and physical discomfort and

134
00:09:28,500 --> 00:09:33,160
 hunger and thirst and heat and cold that you can't escape.

135
00:09:33,160 --> 00:09:36,740
 But when you have the pleasure and the luxury then it's

136
00:09:36,740 --> 00:09:40,350
 very easy to get lazy, it's very easy to become indulgent

137
00:09:40,350 --> 00:09:41,160
 and so on.

138
00:09:41,160 --> 00:09:45,740
 It's actually quite an impressive feat that these monks

139
00:09:45,740 --> 00:09:51,090
 having been in such hardship going to such opulence and not

140
00:09:51,090 --> 00:09:53,160
 being affected by it.

141
00:09:53,160 --> 00:09:56,520
 But the point still stands that it works both ways and it

142
00:09:56,520 --> 00:09:59,160
 seems more to be about change than anything.

143
00:09:59,160 --> 00:10:03,230
 That after a while you become sort of stable in your

144
00:10:03,230 --> 00:10:04,160
 situation.

145
00:10:04,160 --> 00:10:09,510
 And if things don't change you become, you may become compl

146
00:10:09,510 --> 00:10:12,160
acent thinking that everything is okay.

147
00:10:12,160 --> 00:10:18,090
 But then when things change you see what you couldn't see

148
00:10:18,090 --> 00:10:21,160
 because of the stability.

149
00:10:21,160 --> 00:10:24,840
 Part of it points out the nature of impermanence or the

150
00:10:24,840 --> 00:10:27,160
 importance of impermanence.

151
00:10:27,160 --> 00:10:35,160
 The harm and the danger of change to one's psyche.

152
00:10:35,160 --> 00:10:39,270
 And also the usefulness of impermanence for that very

153
00:10:39,270 --> 00:10:40,160
 reason.

154
00:10:40,160 --> 00:10:44,710
 When things change you're able to better see your defile

155
00:10:44,710 --> 00:10:49,160
ment when you have the rug pulled out from under you.

156
00:10:49,160 --> 00:10:52,220
 When everything's going stable whether it's difficult, if

157
00:10:52,220 --> 00:10:53,160
 it's difficult you plot along.

158
00:10:53,160 --> 00:10:57,160
 If it's good then you feel calm and at peace.

159
00:10:57,160 --> 00:11:01,160
 But when things change you can be quite upset.

160
00:11:01,160 --> 00:11:06,000
 A person who is living in opulence, if they fall into

161
00:11:06,000 --> 00:11:13,350
 hardship, would have a very hard time coping mentally with

162
00:11:13,350 --> 00:11:14,160
 it.

163
00:11:14,160 --> 00:11:18,700
 So it goes both ways and this is continuing this theme in

164
00:11:18,700 --> 00:11:19,160
 this verse.

165
00:11:19,160 --> 00:11:23,160
 We've seen this recently quite a bit.

166
00:11:23,160 --> 00:11:26,900
 We talk about how wise people touched by either happiness

167
00:11:26,900 --> 00:11:32,160
 or unhappiness, happiness or suffering, show no change.

168
00:11:32,160 --> 00:11:37,160
 Na uccha waccha tasyanti.

169
00:11:37,160 --> 00:11:45,280
 Uccha means a high and a waccha means a low. So they

170
00:11:45,280 --> 00:11:50,160
 experience no highs or lows.

171
00:11:50,160 --> 00:11:54,310
 Which is interesting, people often think of that as being a

172
00:11:54,310 --> 00:11:57,160
 terribly dull and uninteresting state.

173
00:11:57,160 --> 00:12:03,230
 Certainly not something to be strived for, the highs and

174
00:12:03,230 --> 00:12:08,160
 the lows or the spice of life they would say. It gives life

175
00:12:08,160 --> 00:12:08,160
 meaning I guess.

176
00:12:08,160 --> 00:12:13,160
 It gives meaning to your happiness.

177
00:12:13,160 --> 00:12:16,390
 But it's actually quite the opposite. We find people who

178
00:12:16,390 --> 00:12:20,020
 are stuck on happiness and suffering, these are the ones

179
00:12:20,020 --> 00:12:21,160
 who are like zombies.

180
00:12:21,160 --> 00:12:29,770
 They can be very depressed and colorless in their constant

181
00:12:29,770 --> 00:12:36,220
 aversion to suffering or in their constant obsession with

182
00:12:36,220 --> 00:12:37,160
 happiness.

183
00:12:37,160 --> 00:12:43,160
 A zombie is someone who is constantly seeking out pleasure.

184
00:12:43,160 --> 00:12:47,810
 It's by seeking out pleasure that you become this zombie

185
00:12:47,810 --> 00:12:50,160
 that consumes and consumes.

186
00:12:50,160 --> 00:12:54,960
 Or you're a zombie working just to survive in great

187
00:12:54,960 --> 00:12:58,160
 hardship, just to cope with suffering.

188
00:12:58,160 --> 00:13:01,610
 A person who has had great loss becomes a zombie. A person

189
00:13:01,610 --> 00:13:04,640
 who exhibits neither highs or lows is actually quite at

190
00:13:04,640 --> 00:13:09,160
 peace, has quite a calm and uplifted mind, a light mind.

191
00:13:09,160 --> 00:13:13,460
 A mind that is flexible, a mind that is kind and

192
00:13:13,460 --> 00:13:18,850
 compassionate because they aren't caught up in their own

193
00:13:18,850 --> 00:13:20,160
 emotions.

194
00:13:20,160 --> 00:13:27,470
 So they're able to approach life with great zest and vigor

195
00:13:27,470 --> 00:13:32,160
 and clarity and goodness as well.

196
00:13:32,160 --> 00:13:36,610
 So their intentions to help others and that kind of thing

197
00:13:36,610 --> 00:13:41,020
 are pronounced. Their happiness and their peace are much

198
00:13:41,020 --> 00:13:42,160
 pronounced.

199
00:13:42,160 --> 00:13:44,160
 So they're anything but a zombie.

200
00:13:44,160 --> 00:13:49,250
 The zombie is the one who is caught up in likes and disl

201
00:13:49,250 --> 00:13:57,830
ikes. They become tired and they get into this trance-like

202
00:13:57,830 --> 00:14:06,960
 state, zombie-like state of consuming pleasure and coping

203
00:14:06,960 --> 00:14:10,160
 with suffering.

204
00:14:10,160 --> 00:14:25,420
 It's a warning for all of us not to become complacent. It's

205
00:14:25,420 --> 00:14:28,160
 easy to think that you're moral and ethical.

206
00:14:28,160 --> 00:14:31,010
 If you don't look closely, it's easy to think, "Well, I'm

207
00:14:31,010 --> 00:14:33,160
 not a bad person. I'm living fine."

208
00:14:33,160 --> 00:14:36,330
 But so much of that is often dependent on our pleasure and

209
00:14:36,330 --> 00:14:39,910
 when you start to meditate, you start to see, "Ah, yes,

210
00:14:39,910 --> 00:14:43,160
 actually I'm quite intoxicated by this pleasure."

211
00:14:43,160 --> 00:14:47,840
 And many of the things that I thought were harmless are

212
00:14:47,840 --> 00:14:51,690
 actually building me up to take a fall because when I'm not

213
00:14:51,690 --> 00:14:55,160
 able to indulge in sensual pleasures anymore,

214
00:14:55,160 --> 00:14:58,050
 I'm going to get angry and upset and frustrated. It's what

215
00:14:58,050 --> 00:15:04,230
 leads us to fight and quarrel and manipulate others, our

216
00:15:04,230 --> 00:15:13,160
 indulgence in sensuality.

217
00:15:13,160 --> 00:15:18,890
 It's really the crux of it. Our reactions to pleasant

218
00:15:18,890 --> 00:15:25,390
 experiences and our reactions to unpleasant experiences,

219
00:15:25,390 --> 00:15:29,610
 this about sums up all of the problems that we have, the

220
00:15:29,610 --> 00:15:34,160
 problems of the mind that we're working to overcome.

221
00:15:34,160 --> 00:15:38,830
 There's a third one that's not really explicitly mentioned

222
00:15:38,830 --> 00:15:42,980
 here and that's delusion. We consider these to be the three

223
00:15:42,980 --> 00:15:46,160
 evils in the mind, the three things that lead us to suffer.

224
00:15:46,160 --> 00:15:52,350
 But they're not equal. Happiness and unhappiness lead to

225
00:15:52,350 --> 00:15:56,000
 greed and anger, liking and disliking, our reactions to

226
00:15:56,000 --> 00:15:57,160
 these things.

227
00:15:57,160 --> 00:16:00,990
 These are, you could say, the surface problem, but the

228
00:16:00,990 --> 00:16:03,940
 underlying problem. The third one is the underlying problem

229
00:16:03,940 --> 00:16:04,160
.

230
00:16:04,160 --> 00:16:09,040
 Our delusion is how we approach these things. The reason we

231
00:16:09,040 --> 00:16:12,470
 get angry about unpleasant things, the reason we become

232
00:16:12,470 --> 00:16:16,260
 attached to pleasant things is because of our delusion,

233
00:16:16,260 --> 00:16:20,950
 because of our ignorance, our perception that this one's

234
00:16:20,950 --> 00:16:24,160
 going to make me happy, this one's something that I...

235
00:16:24,160 --> 00:16:28,080
 the way to be happy is to avoid it. If I chase after this

236
00:16:28,080 --> 00:16:31,450
 one, I'll be happy. If I push this one away, I'll also be

237
00:16:31,450 --> 00:16:32,160
 happy.

238
00:16:32,160 --> 00:16:35,610
 Not realizing that the more we push this one away, the more

239
00:16:35,610 --> 00:16:41,150
 unhappy we'll become. The more we cling to this one, the

240
00:16:41,150 --> 00:16:47,720
 more needy we will become and so the more unhappy we will

241
00:16:47,720 --> 00:16:49,160
 become.

242
00:16:49,160 --> 00:16:54,720
 And shameful we'll become. If you ask yourself, who would

243
00:16:54,720 --> 00:16:57,720
 you rather have as a friend, who would you rather associate

244
00:16:57,720 --> 00:17:03,910
 with? Someone who wants a lot or someone who doesn't have

245
00:17:03,910 --> 00:17:09,160
... who has great wanting or someone who has little wanting.

246
00:17:09,160 --> 00:17:14,240
 Who has great, great desires, strong, strong desires or

247
00:17:14,240 --> 00:17:19,160
 someone who has simple desires or is free from desire.

248
00:17:19,160 --> 00:17:22,250
 I don't know, some people might say they like people who

249
00:17:22,250 --> 00:17:25,540
 have great desires, but if you think about it, when people

250
00:17:25,540 --> 00:17:29,010
 have great desires, then they end up wanting a lot from you

251
00:17:29,010 --> 00:17:29,160
.

252
00:17:29,160 --> 00:17:34,340
 Friends who have great desires can be great, great burdens.

253
00:17:34,340 --> 00:17:40,000
 Friends who are hard to please or friends who have great...

254
00:17:40,000 --> 00:17:44,160
 people who have great aversion to suffering.

255
00:17:44,160 --> 00:17:49,200
 So this is the sort of situation we have here where part of

256
00:17:49,200 --> 00:17:54,730
 the implication here is that these guys are acting improper

257
00:17:54,730 --> 00:17:58,160
, are acting in an unsuitable way.

258
00:17:58,160 --> 00:18:01,760
 Having eaten this food that was meant for the monks and

259
00:18:01,760 --> 00:18:05,360
 sort of living in close quarters with these spiritual

260
00:18:05,360 --> 00:18:09,060
 people, they're acting like, well, like ordinary

261
00:18:09,060 --> 00:18:10,160
 individuals.

262
00:18:10,160 --> 00:18:14,890
 You get that a lot in monasteries when they get big in

263
00:18:14,890 --> 00:18:19,100
 Thailand. You can see this a lot where the workers in the

264
00:18:19,100 --> 00:18:22,160
 monasteries, not so very moral.

265
00:18:22,160 --> 00:18:28,020
 Someone... I don't know, I was talking to one of the

266
00:18:28,020 --> 00:18:34,070
 workers about cockroaches and she... I think she pulled out

267
00:18:34,070 --> 00:18:39,160
 a can of cockroach spray and was trying to hand it to me.

268
00:18:39,160 --> 00:18:46,690
 And... yeah. I remember going... we went on this parade

269
00:18:46,690 --> 00:18:49,890
 once. The monks, we were up in this boat. They had us up on

270
00:18:49,890 --> 00:18:51,160
 this float with a boat.

271
00:18:51,160 --> 00:18:53,910
 It was like a boat and people were putting food in the boat

272
00:18:53,910 --> 00:18:57,900
. It was a means of supporting, I don't know actually what

273
00:18:57,900 --> 00:19:00,720
 it was... what the food was going to be for, but it was a

274
00:19:00,720 --> 00:19:03,160
 parade with monks, which was kind of nice.

275
00:19:03,160 --> 00:19:07,740
 But then the board of directors for the monastery was

276
00:19:07,740 --> 00:19:11,680
 surrounding it and going on with us. And then they started

277
00:19:11,680 --> 00:19:15,500
 pulling... they pulled out alcohol and started drinking

278
00:19:15,500 --> 00:19:16,160
 whiskey.

279
00:19:16,160 --> 00:19:23,330
 Well, you know, all around us, they were holding onto the

280
00:19:23,330 --> 00:19:28,160
 edges of the boat and riding the float.

281
00:19:28,160 --> 00:19:34,860
 It's a shame really, that sort of thing, that living so

282
00:19:34,860 --> 00:19:40,230
 close to such goodness and such spiritual teachings that

283
00:19:40,230 --> 00:19:43,160
 they don't bother to put them into practice.

284
00:19:43,160 --> 00:19:47,190
 And part of it is this problem of having things too good.

285
00:19:47,190 --> 00:19:51,160
 When things are good, people may tend to misbehave.

286
00:19:51,160 --> 00:19:56,330
 It's part of the reason why monks are encouraged to go off

287
00:19:56,330 --> 00:20:02,160
 into the forest and live in simplest, simple accommodations

288
00:20:02,160 --> 00:20:02,160
.

289
00:20:02,160 --> 00:20:06,600
 To live more or less in hardship, because it does, it can

290
00:20:06,600 --> 00:20:13,160
 keep you honest. It forces you to be mindful.

291
00:20:13,160 --> 00:20:18,280
 In terms of the monastic life, hardship is quite often

292
00:20:18,280 --> 00:20:24,040
 important for spiritual development, because it's very easy

293
00:20:24,040 --> 00:20:27,160
 to get lazy, as we can see here.

294
00:20:27,160 --> 00:20:31,850
 Anyway, so a simple teaching. The verse itself has some

295
00:20:31,850 --> 00:20:34,160
 interesting points.

296
00:20:34,160 --> 00:20:38,270
 The idea that what makes a good person is their practice of

297
00:20:38,270 --> 00:20:43,160
 renunciation everywhere, whether they're happy or unhappy.

298
00:20:43,160 --> 00:20:46,640
 And they never blather on. "Lapayanti", it's an interesting

299
00:20:46,640 --> 00:20:50,160
 word. They chatter.

300
00:20:50,160 --> 00:20:54,830
 The implication is that people who are intoxicated with

301
00:20:54,830 --> 00:20:58,160
 sensuality will blather on about it.

302
00:20:58,160 --> 00:21:04,740
 Something we should catch ourselves in, talking about food

303
00:21:04,740 --> 00:21:10,160
 or clothing or music or art or this kind of thing.

304
00:21:10,160 --> 00:21:16,120
 But one who is wise, when touched by other happiness or by

305
00:21:16,120 --> 00:21:20,160
 sorrow, is at peace, is content.

306
00:21:20,160 --> 00:21:26,530
 Their happiness, their peace, their contentment is not

307
00:21:26,530 --> 00:21:30,390
 dependent. It's anamisa suka, a happiness that is not based

308
00:21:30,390 --> 00:21:31,160
 on an object.

309
00:21:31,160 --> 00:21:36,820
 It doesn't require this or that. It isn't affected. It isn

310
00:21:36,820 --> 00:21:40,160
't disturbed.

311
00:21:40,160 --> 00:21:44,670
 It isn't disturbed and it isn't dependent on external sense

312
00:21:44,670 --> 00:21:48,160
, external experiences or phenomena.

313
00:21:48,160 --> 00:21:52,600
 This is why meditation is so important. It's important to

314
00:21:52,600 --> 00:21:58,160
 understand change and to become familiar with change.

315
00:21:58,160 --> 00:22:01,870
 And to see that change is a part of life so that changes

316
00:22:01,870 --> 00:22:07,380
 don't affect us, so that our happiness can be, our peace of

317
00:22:07,380 --> 00:22:11,160
 mind can be undisturbed by change.

318
00:22:11,160 --> 00:22:15,800
 Anyway, that's the teaching of this Dhammapada verse. That

319
00:22:15,800 --> 00:22:18,160
's all for now. Thank you for tuning in.

320
00:22:18,160 --> 00:22:20,160
 Keep practicing and be well.

