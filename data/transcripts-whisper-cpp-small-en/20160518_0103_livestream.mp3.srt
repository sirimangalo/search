1
00:00:00,000 --> 00:00:05,000
 Hello, good evening everyone.

2
00:00:05,000 --> 00:00:10,000
 Broadcasting live, May 17th.

3
00:00:10,000 --> 00:00:24,000
 Today I'm in New York, at Bikubodi's Monstery.

4
00:00:24,000 --> 00:00:31,000
 With venerable...

5
00:00:31,000 --> 00:00:32,000
 ...Yen Zhong.

6
00:00:32,000 --> 00:00:41,000
 Ivan, the head monk of the Chinese monastery in Toronto.

7
00:00:41,000 --> 00:00:46,000
 So today finally we got to meet Bikubodi.

8
00:00:46,000 --> 00:00:49,000
 He's staying in a house just down the way.

9
00:00:49,000 --> 00:00:53,000
 I didn't realize this was actually a very big monastery.

10
00:00:53,000 --> 00:00:56,000
 It's like 100 acres.

11
00:00:56,000 --> 00:00:59,000
 So we have to walk to his house.

12
00:00:59,000 --> 00:01:17,000
 We're staying in the more main complex area.

13
00:01:17,000 --> 00:01:22,000
 So today's quote is about the Buddha.

14
00:01:22,000 --> 00:01:26,000
 I mean there's not much to say about it.

15
00:01:26,000 --> 00:01:27,000
 Clear as...

16
00:01:27,000 --> 00:01:32,590
 Interesting aspect, though this quote out of context sounds

17
00:01:32,590 --> 00:01:40,000
 like the Buddha is somehow...

18
00:01:40,000 --> 00:01:45,690
 ...focusing on how much better he is than all of us because

19
00:01:45,690 --> 00:01:47,000
 he was first.

20
00:01:47,000 --> 00:01:52,940
 It actually, I think the context and the feeling I got from

21
00:01:52,940 --> 00:01:55,000
 it anyway is...

22
00:01:55,000 --> 00:02:00,000
 It's actually more, that's the only difference.

23
00:02:00,000 --> 00:02:04,050
 So the difference between the Buddha and his followers is

24
00:02:04,050 --> 00:02:06,000
 just that he came first.

25
00:02:06,000 --> 00:02:16,000
 The freedom from suffering, the enlightenment is the same.

26
00:02:16,000 --> 00:02:21,000
 The goal is the same.

27
00:02:21,000 --> 00:02:27,000
 Nima is the same.

28
00:02:27,000 --> 00:02:32,120
 Which kind of puts us all on even footing and it's, I mean

29
00:02:32,120 --> 00:02:33,000
...

30
00:02:33,000 --> 00:02:37,550
 The important part of it is how encouraging that is for all

31
00:02:37,550 --> 00:02:39,000
 of us.

32
00:02:39,000 --> 00:02:45,840
 That we have inside of ourselves the potential for

33
00:02:45,840 --> 00:02:48,000
 enlightenment.

34
00:02:48,000 --> 00:02:56,000
 There's great potential inside of us.

35
00:02:56,000 --> 00:02:58,000
 We can do so many things.

36
00:02:58,000 --> 00:03:04,000
 We can become rich and famous.

37
00:03:04,000 --> 00:03:05,000
 We can go to heaven.

38
00:03:05,000 --> 00:03:07,000
 We can go to hell.

39
00:03:07,000 --> 00:03:17,000
 We can go to nirvana.

40
00:03:17,000 --> 00:03:22,510
 Simon, I don't remember whether I actually advertised the

41
00:03:22,510 --> 00:03:26,000
 fact that we had a Danish translation but...

42
00:03:26,000 --> 00:03:31,000
 Anyway, thanks for translating it.

43
00:03:31,000 --> 00:03:40,700
 I can't remember. I may have just put it up and forgot to

44
00:03:40,700 --> 00:03:42,000
 advertise it.

45
00:03:42,000 --> 00:03:47,000
 What do we have online today? A bunch of people.

46
00:03:47,000 --> 00:03:55,000
 Hello everyone.

47
00:03:55,000 --> 00:03:59,470
 I'll be back tomorrow. We're going to leave here after

48
00:03:59,470 --> 00:04:01,000
 breakfast I think.

49
00:04:01,000 --> 00:04:14,950
 And so tomorrow evening I'll be back to, should be back to

50
00:04:14,950 --> 00:04:20,000
 video broadcast.

51
00:04:20,000 --> 00:04:24,120
 If anyone has any questions I'm happy to stick around but

52
00:04:24,120 --> 00:04:25,000
...

53
00:04:25,000 --> 00:04:33,000
 Otherwise maybe we'll just cut it short today.

54
00:04:33,000 --> 00:04:45,000
 How are you all doing? How's your meditation going?

55
00:04:45,000 --> 00:04:54,560
 We just found out bad news that Michael is probably leaving

56
00:04:54,560 --> 00:04:55,000
 us.

57
00:04:55,000 --> 00:05:03,350
 He has a dog that we didn't know about and he left his dog

58
00:05:03,350 --> 00:05:06,000
 with his brother.

59
00:05:06,000 --> 00:05:11,000
 And his brother says he can't take care of the dog anymore.

60
00:05:11,000 --> 00:05:21,000
 So we're back in the market for a steward.

61
00:05:21,000 --> 00:05:29,000
 Anybody wants to come and stay with us long term?

62
00:05:29,000 --> 00:05:33,000
 And we're probably not going to, as a result, rent a...

63
00:05:33,000 --> 00:05:38,640
 We were thinking of renting a larger place but that's

64
00:05:38,640 --> 00:05:41,000
 probably not going to happen.

65
00:05:41,000 --> 00:05:44,800
 Because even taking care of a small place without a steward

66
00:05:44,800 --> 00:05:46,000
 is difficult.

67
00:05:46,000 --> 00:05:51,210
 We have more meditators in a larger place. I think it's too

68
00:05:51,210 --> 00:05:56,000
 much without someone helping.

69
00:05:56,000 --> 00:06:07,750
 So that's, well, you know, impermanent. That's an imper

70
00:06:07,750 --> 00:06:12,000
manent change.

71
00:06:12,000 --> 00:06:16,000
 If anybody wants to come and stay with us long term.

72
00:06:16,000 --> 00:06:19,570
 I guess preferably someone who lives in Canada so they don

73
00:06:19,570 --> 00:06:21,000
't have to worry about visas.

74
00:06:21,000 --> 00:06:24,000
 But it'll take whatever we can get.

75
00:06:24,000 --> 00:06:28,000
 If somebody has someone, there were two people.

76
00:06:28,000 --> 00:06:33,680
 Why do I want to say, who else was it? Was it Kathy, was it

77
00:06:33,680 --> 00:06:34,000
?

78
00:06:34,000 --> 00:07:01,000
 A steward. I'm going to look up this.

79
00:07:01,000 --> 00:07:22,000
 I can't remember who it was.

80
00:07:22,000 --> 00:07:31,070
 It was another person. It seems to me it was a female

81
00:07:31,070 --> 00:07:33,000
 person. That's all I can remember.

82
00:07:33,000 --> 00:07:37,000
 Did you see the large Buddha statue?

83
00:07:37,000 --> 00:07:43,000
 Ivan? Ivan? Ivan?

84
00:07:43,000 --> 00:07:46,000
 Did we see the big Buddha statue?

85
00:07:46,000 --> 00:07:49,000
 Did we see the largest Buddha statue?

86
00:07:49,000 --> 00:07:52,000
 No. Where is it?

87
00:07:52,000 --> 00:07:57,000
 It's here? This is the largest Buddha statue in...

88
00:07:57,000 --> 00:08:00,000
 What center?

89
00:08:00,000 --> 00:08:06,410
 In this monastery? This monastery has the largest Buddha

90
00:08:06,410 --> 00:08:11,000
 image in North America or something?

91
00:08:11,000 --> 00:08:19,000
 You never walk out of the room. That's what I mean.

92
00:08:19,000 --> 00:08:22,260
 I haven't actually seen the largest Buddha statue but now

93
00:08:22,260 --> 00:08:29,480
 that I know it's here I'll have to maybe go take a look at

94
00:08:29,480 --> 00:08:32,000
 it.

95
00:08:32,000 --> 00:08:42,000
 We need the new steward ASAP as soon as possible.

96
00:08:42,000 --> 00:08:46,000
 Hey, Sanka's here.

97
00:08:46,000 --> 00:08:53,000
 Sanka, I hope we're clear. I think we are.

98
00:08:53,000 --> 00:08:57,520
 I don't know how to say this but we're not really taking

99
00:08:57,520 --> 00:09:02,000
 seriously Sanka's plans for me in Sri Lanka, I hope.

100
00:09:02,000 --> 00:09:06,240
 Because it was a bit of a disaster last time. I think we're

101
00:09:06,240 --> 00:09:09,000
 on the same page with that.

102
00:09:09,000 --> 00:09:12,000
 Just think while I've got you on here.

103
00:09:12,000 --> 00:09:16,000
 He wants me to...

104
00:09:16,000 --> 00:09:20,400
 He's got like 20 places for me to give talks and half those

105
00:09:20,400 --> 00:09:21,000
 people I'm sure...

106
00:09:21,000 --> 00:09:24,680
 I know I recognize some of them are places where I went

107
00:09:24,680 --> 00:09:28,040
 last time and they didn't really understand why they were

108
00:09:28,040 --> 00:09:29,000
 inviting me.

109
00:09:29,000 --> 00:09:34,770
 And in certain... in some cases actually regretted inviting

110
00:09:34,770 --> 00:09:35,000
 me.

111
00:09:35,000 --> 00:09:45,570
 Because of course not everyone is into the same things we

112
00:09:45,570 --> 00:09:47,000
 are.

113
00:09:47,000 --> 00:09:52,000
 Sanka, thank you and we'll be seeing you next month.

114
00:09:52,000 --> 00:09:57,320
 I was wondering whether we should do more... publish more

115
00:09:57,320 --> 00:10:01,000
 books... booklets.

116
00:10:01,000 --> 00:10:05,710
 I know we're doing a lot but maybe we should consider if

117
00:10:05,710 --> 00:10:10,000
 that's doable because I'll be going there, right?

118
00:10:10,000 --> 00:10:13,610
 So I can bring some back. It might be a good idea. They're

119
00:10:13,610 --> 00:10:16,000
 not so expensive or anything.

120
00:10:16,000 --> 00:10:20,000
 I've still got probably 500.

121
00:10:20,000 --> 00:10:25,020
 I haven't counted them but it seems like I've got about

122
00:10:25,020 --> 00:10:26,000
 half left.

123
00:10:26,000 --> 00:10:45,000
 But it might be good to fix them up.

124
00:10:45,000 --> 00:10:49,330
 I mean if they are interested and if they know who I am and

125
00:10:49,330 --> 00:10:54,050
 if they are really interested in inviting me and not just "

126
00:10:54,050 --> 00:11:00,000
oh, Sudhoo Hamdru" or "oh, I don't know, Meditation Mount"

127
00:11:00,000 --> 00:11:08,000
 No, no, no, no.

128
00:11:08,000 --> 00:11:26,000
 Yeah, print probably a thousand. A thousand more.

129
00:11:26,000 --> 00:11:31,000
 I think that's such a big deal. I think we're okay with it.

130
00:11:31,000 --> 00:11:35,960
 We'll print more and I can just bring a big empty suitcase

131
00:11:35,960 --> 00:11:39,000
 to bring at least some of them back in.

132
00:11:39,000 --> 00:11:44,770
 I have to make sure I have luggage allowance but I can

133
00:11:44,770 --> 00:11:49,000
 bring some back. Probably not all of them.

134
00:11:49,000 --> 00:12:01,960
 Then we'll have to find people going back and forth from

135
00:12:01,960 --> 00:12:14,930
 Sri Lanka which is always happening so that should be do

136
00:12:14,930 --> 00:12:17,000
able.

137
00:12:17,000 --> 00:12:24,310
 I can see there's a lag between what I'm saying and what

138
00:12:24,310 --> 00:12:27,000
 you guys are typing.

139
00:12:27,000 --> 00:12:38,000
 Probably about 10 seconds.

140
00:12:38,000 --> 00:12:45,240
 I remember last year with some people were like "Well, I

141
00:12:45,240 --> 00:12:48,000
 just got a call from Sri Lanka."

142
00:12:48,000 --> 00:12:51,110
 And he said "You have to invite this monk so you don't

143
00:12:51,110 --> 00:12:54,530
 really know why you're inviting me or who I am or anything

144
00:12:54,530 --> 00:12:55,000
."

145
00:12:55,000 --> 00:13:07,000
 Not really good. Not the best.

146
00:13:07,000 --> 00:13:22,000
 Anyway.

147
00:13:22,000 --> 00:13:27,670
 Okay, I'm going to call in tonight. If I see anybody type

148
00:13:27,670 --> 00:13:30,000
 anything I'll just type back.

149
00:13:30,000 --> 00:13:34,620
 As you can see there's a bit of a lag so I think this is

150
00:13:34,620 --> 00:13:37,000
 enough audio tonight.

151
00:13:37,000 --> 00:13:41,630
 Regularly scheduled video broadcasts should resume tomorrow

152
00:13:41,630 --> 00:13:47,470
 if I'm still alive and if everything is as planned if

153
00:13:47,470 --> 00:13:57,180
 Canada still hasn't dissolved and the world still hasn't

154
00:13:57,180 --> 00:13:58,000
 broken down.

155
00:13:58,000 --> 00:14:02,950
 If the internet still exists tomorrow then I'll probably be

156
00:14:02,950 --> 00:14:05,000
 up and broadcasting.

157
00:14:05,000 --> 00:14:07,000
 So that's all for audio. Good night.

