1
00:00:00,000 --> 00:00:04,950
 Hello and welcome back to our study of the Dhammapandar.

2
00:00:04,950 --> 00:00:11,000
 Today we look at verse 172 which reads

3
00:00:11,000 --> 00:00:19,800
 "Yogpube, yogpube bhamantitva bhachasona bhamatiti, so mang

4
00:00:19,800 --> 00:00:26,000
lu kang bhabhasiti, abhum tova tan dima."

5
00:00:26,000 --> 00:00:40,000
 Which means, whoever was heedless before, negligent before,

6
00:00:40,000 --> 00:00:50,000
 but afterwards is no longer heedless, no longer negligent.

7
00:00:50,000 --> 00:00:57,350
 Such a person, so manglu kang bhabhamsiti, such a person

8
00:00:57,350 --> 00:01:04,280
 lights up this world like the moon when it comes out from

9
00:01:04,280 --> 00:01:09,000
 behind the cloud.

10
00:01:09,000 --> 00:01:17,160
 So this famous, another famous Dhammapada verse was taught

11
00:01:17,160 --> 00:01:22,000
 in relation to a monk with a broom.

12
00:01:22,000 --> 00:01:26,200
 It's also mentioned I think in the next one which is more

13
00:01:26,200 --> 00:01:33,300
 famous, the story coming up, or not the next one, but a

14
00:01:33,300 --> 00:01:34,000
 couple from now.

15
00:01:34,000 --> 00:01:37,570
 There's a story that's more famous that we'll look at and I

16
00:01:37,570 --> 00:01:44,600
 think the verse is mentioned there, but it's not the verse

17
00:01:44,600 --> 00:01:48,000
 of the story.

18
00:01:48,000 --> 00:01:58,000
 Anyway, this story,

19
00:01:58,000 --> 00:02:03,480
 the story is about a monk with a broom. And so the story

20
00:02:03,480 --> 00:02:09,040
 goes that there was one monk called Samundjani who was

21
00:02:09,040 --> 00:02:12,000
 always sweeping.

22
00:02:12,000 --> 00:02:15,320
 He would sweep in the morning, he would go for alms, and

23
00:02:15,320 --> 00:02:20,630
 when he came back he would sweep, sweep, sweep, sweep the

24
00:02:20,630 --> 00:02:22,000
 monastery.

25
00:02:22,000 --> 00:02:25,810
 There are these sorts of monks, I've seen these sorts of

26
00:02:25,810 --> 00:02:30,800
 monks in various monasteries that I've lived in or visited,

27
00:02:30,800 --> 00:02:37,320
 monks and nuns who sweep a lot or who work a lot, who do

28
00:02:37,320 --> 00:02:44,000
 this or that work and are always busy.

29
00:02:44,000 --> 00:02:48,510
 And not only did he do this himself, but one day he went

30
00:02:48,510 --> 00:02:50,000
 and saw R

31
00:02:50,000 --> 00:02:57,200
 who was the foremost in terms of seclusion. He was among

32
00:02:57,200 --> 00:03:01,000
 the Buddhist disciples. I think that's what it was. Anyway,

33
00:03:01,000 --> 00:03:04,650
 he was a monk who delighted in seclusion, never wanted to

34
00:03:04,650 --> 00:03:10,000
 be around people, just wanted to be off on his own.

35
00:03:10,000 --> 00:03:16,250
 So he lived in a, he normally lived in a very dense and imp

36
00:03:16,250 --> 00:03:22,120
enetrable jungle, kind of place where nobody else would want

37
00:03:22,120 --> 00:03:23,000
 to go.

38
00:03:23,000 --> 00:03:28,860
 He was interesting that way. But here he seems to have been

39
00:03:28,860 --> 00:03:32,000
 in Jaita Wana, just probably to visit the Buddha.

40
00:03:32,000 --> 00:03:39,480
 And he saw a rewatta, this monk Samunjan, he saw a rewatta

41
00:03:39,480 --> 00:03:40,000
 sitting there with his eyes closed.

42
00:03:40,000 --> 00:03:44,320
 And he noticed that a rewatta would just sit around all day

43
00:03:44,320 --> 00:03:50,000
 and not do any work. And he thought to himself, this monk,

44
00:03:50,000 --> 00:03:54,210
 he goes and takes the food, takes people's food and then he

45
00:03:54,210 --> 00:03:56,000
 comes back and he doesn't do any sweeping.

46
00:03:56,000 --> 00:04:01,280
 He could at least sweep out one room. And rewatta I think

47
00:04:01,280 --> 00:04:05,140
 had some kind of power because he seemed to have known what

48
00:04:05,140 --> 00:04:09,000
 was going on in this monk's, this other monk's mind.

49
00:04:09,000 --> 00:04:14,100
 Another monk called him, Samunjan called him a great idol

50
00:04:14,100 --> 00:04:18,000
 there. He was idol.

51
00:04:18,000 --> 00:04:37,050
 And so rewatta says, "I should teach him, I should teach

52
00:04:37,050 --> 00:04:39,000
 this monk something."

53
00:04:39,000 --> 00:04:43,450
 And so he called the monk over and he explained to him, he

54
00:04:43,450 --> 00:04:48,280
 said, "Look, of course you should be sweeping, you should

55
00:04:48,280 --> 00:04:52,650
 sweep first thing in the morning and go for alms and then

56
00:04:52,650 --> 00:04:56,000
 come back and practice meditation."

57
00:04:56,000 --> 00:05:00,780
 Basically, a very simple teaching. But he gave him a

58
00:05:00,780 --> 00:05:03,900
 specific meditation practice and said, "Then in the evening

59
00:05:03,900 --> 00:05:06,720
 then you can go and sweep again so you sweep in the morning

60
00:05:06,720 --> 00:05:10,400
, sweep in the evening, but you really need to give yourself

61
00:05:10,400 --> 00:05:11,000
 a chance."

62
00:05:11,000 --> 00:05:16,190
 He says, "Hokas, hokaso kattam or something like kattambo

63
00:05:16,190 --> 00:05:20,600
 or something like that, one should give oneself the

64
00:05:20,600 --> 00:05:25,000
 opportunity to do something more meaningful."

65
00:05:25,000 --> 00:05:29,070
 And so this monk took his admonishment and as a result

66
00:05:29,070 --> 00:05:32,890
 became enlightened. Now when he was, after he became

67
00:05:32,890 --> 00:05:35,000
 enlightened he no longer swept.

68
00:05:35,000 --> 00:05:40,000
 He swept a little in the morning and some in the evening.

69
00:05:40,000 --> 00:05:43,970
 The monk saw that this sweeping monk no longer did his work

70
00:05:43,970 --> 00:05:50,000
 and so the rooms got a little bit dingy and dusty.

71
00:05:50,000 --> 00:05:54,540
 The other monk saw that the monastery was no longer spic

72
00:05:54,540 --> 00:05:57,000
 and span like it used to be.

73
00:05:57,000 --> 00:06:01,900
 And so they came up to him and they said, "Why aren't you

74
00:06:01,900 --> 00:06:10,090
 sweeping anymore?" And he said, "I'm vulnerable today. I

75
00:06:10,090 --> 00:06:15,790
 used to do that when I was heedless. Now however I have

76
00:06:15,790 --> 00:06:19,000
 become heedful."

77
00:06:19,000 --> 00:06:22,160
 And the monks heard this and they were indignant because

78
00:06:22,160 --> 00:06:25,580
 they thought, "Look at this monk pretending to be something

79
00:06:25,580 --> 00:06:28,000
 enlightened or something like that."

80
00:06:28,000 --> 00:06:32,230
 So they went to see the Buddha and they told the Buddha, "

81
00:06:32,230 --> 00:06:36,280
That's just monk who's, he used to work but now just in

82
00:06:36,280 --> 00:06:40,740
 order to laze around he claims to be an Arahant, claims to

83
00:06:40,740 --> 00:06:43,000
 be somehow enlightened."

84
00:06:43,000 --> 00:06:47,790
 And the Buddha said, "Oh no, it's true. My son, he calls

85
00:06:47,790 --> 00:06:53,150
 him my son. My son used to be heedful. When he was heedless

86
00:06:53,150 --> 00:06:57,000
, he spent all his time sweeping.

87
00:06:57,000 --> 00:07:01,280
 But now he spends his time in the bliss of enlightenment

88
00:07:01,280 --> 00:07:04,000
 and therefore sweeps no more."

89
00:07:04,000 --> 00:07:08,930
 And then he said, "Yopu, Beipu, Majin, T'an, T'an." He was

90
00:07:08,930 --> 00:07:13,000
 heedless before. He'd less no more.

91
00:07:13,000 --> 00:07:20,550
 He illuminates this world as does the moon freed from a

92
00:07:20,550 --> 00:07:22,000
 cloud.

93
00:07:22,000 --> 00:07:25,920
 So I think again we separate the story and the verse

94
00:07:25,920 --> 00:07:30,120
 because the verse isn't really all that related to the

95
00:07:30,120 --> 00:07:31,000
 story.

96
00:07:31,000 --> 00:07:35,420
 But the story has an interesting lesson. It's the question

97
00:07:35,420 --> 00:07:43,000
 of work and more generally how we spend our time.

98
00:07:43,000 --> 00:07:48,810
 And so you can apply this to ordinary life and all aspects

99
00:07:48,810 --> 00:07:54,920
 of our life where we occupy ourselves with the various

100
00:07:54,920 --> 00:07:56,000
 tasks,

101
00:07:56,000 --> 00:08:02,370
 sometimes caught up in ambition where we have some kind of

102
00:08:02,370 --> 00:08:07,000
 drive that drives us to work very hard.

103
00:08:07,000 --> 00:08:15,100
 Sometimes it's a drive for money or power or status or

104
00:08:15,100 --> 00:08:17,000
 respect.

105
00:08:17,000 --> 00:08:26,000
 Often there's a sense of pride and righteousness.

106
00:08:26,000 --> 00:08:32,320
 I'm a hard worker. I worked hard and look what I've got to

107
00:08:32,320 --> 00:08:34,000
 show for it.

108
00:08:34,000 --> 00:08:37,620
 I met some Americans. When I was living in America, I met

109
00:08:37,620 --> 00:08:45,000
 people who were quite indignant that monks should not work.

110
00:08:45,000 --> 00:08:49,800
 So there was something noble or right. And many people

111
00:08:49,800 --> 00:08:52,960
 truly feel this way. There's something noble and right

112
00:08:52,960 --> 00:08:54,000
 about working.

113
00:08:54,000 --> 00:08:59,000
 Thoreau said, to give a modern example, he commented on

114
00:08:59,000 --> 00:09:09,000
 this and he said, "These people, they're like oxen.

115
00:09:09,000 --> 00:09:13,370
 They work, work, work very hard. What's the point?" And

116
00:09:13,370 --> 00:09:18,000
 they seem somehow to feel like it's right.

117
00:09:18,000 --> 00:09:23,590
 There's a rightness to the work that we do. We're taught

118
00:09:23,590 --> 00:09:30,160
 that this is the right way to live. It's right to have a

119
00:09:30,160 --> 00:09:31,000
 job.

120
00:09:31,000 --> 00:09:38,000
 Anyone who doesn't have a job is a bum, an unemployed bum.

121
00:09:38,000 --> 00:09:41,700
 And so you had this movement in America called the Dharma B

122
00:09:41,700 --> 00:09:42,000
ums.

123
00:09:42,000 --> 00:09:45,970
 And I'm assuming it was somehow related to this sort of

124
00:09:45,970 --> 00:09:50,360
 counterculture of people who realized, "Well, you don't

125
00:09:50,360 --> 00:09:52,000
 really need to work."

126
00:09:52,000 --> 00:09:57,110
 There's no imperative to be a hardworking individual in the

127
00:09:57,110 --> 00:10:00,250
 world. I imagine they were probably a little bit lazy in

128
00:10:00,250 --> 00:10:05,000
 the sense I sort of get from it.

129
00:10:05,000 --> 00:10:10,900
 In fact, I think work is quite valuable. It's just a

130
00:10:10,900 --> 00:10:13,000
 question of what is the right work.

131
00:10:13,000 --> 00:10:39,180
 And R

132
00:10:39,180 --> 00:10:46,640
 we have to care for our possessions. But the point is to

133
00:10:46,640 --> 00:10:49,180
 not let them own us.

134
00:10:49,180 --> 00:10:55,050
 Do not let our work consume us. Do not let our possessions

135
00:10:55,050 --> 00:10:57,180
 control us.

136
00:10:57,180 --> 00:11:10,450
 And we can see as we're mindful to what extent this is the

137
00:11:10,450 --> 00:11:12,900
 case. And we're naturally inclined towards simplifying our

138
00:11:12,900 --> 00:11:16,180
 life as we see the complexity and the busyness,

139
00:11:16,180 --> 00:11:25,850
 the overall lack of opportunity as Revata says, opportunity

140
00:11:25,850 --> 00:11:32,180
 to perform the more meaningful work.

141
00:11:32,180 --> 00:11:35,610
 I think it's a reminder to us. So the story is an important

142
00:11:35,610 --> 00:11:44,180
 reminder that busyness is not in and of itself valuable.

143
00:11:44,180 --> 00:11:56,180
 But work and energetic effort, this is valuable.

144
00:11:56,180 --> 00:12:03,820
 As for the story, as for the verse, this verse is quite

145
00:12:03,820 --> 00:12:07,180
 famous. I think it's both inspiring and instructive.

146
00:12:07,180 --> 00:12:12,250
 It's a clear reminder of what is important. This word 'app

147
00:12:12,250 --> 00:12:15,180
amada' that we talk about so often.

148
00:12:15,180 --> 00:12:19,770
 You don't hear about so much in English, I think. But the

149
00:12:19,770 --> 00:12:25,080
 Pali word 'appamada' is hard to translate because 'pamada'

150
00:12:25,080 --> 00:12:28,180
 really means intoxication of some sort.

151
00:12:28,180 --> 00:12:35,220
 But it's not intoxication with alcohol. It's emotional

152
00:12:35,220 --> 00:12:44,180
 intoxication or mental intoxication, mental drunkenness.

153
00:12:44,180 --> 00:12:52,500
 When you're drunk on greed or anger or delusion or all

154
00:12:52,500 --> 00:12:54,180
 three.

155
00:12:54,180 --> 00:12:59,910
 'Pamada' is not being mindful. The Buddha said again and

156
00:12:59,910 --> 00:13:05,550
 again that it's mindfulness. When you're mindful, that's

157
00:13:05,550 --> 00:13:13,180
 what it means to be unheavless,

158
00:13:13,180 --> 00:13:23,090
 unnegligent, non-negligent, to be sober. Mindfulness is

159
00:13:23,090 --> 00:13:32,180
 like waking up. This is why this imagery is so powerful.

160
00:13:32,180 --> 00:13:35,750
 It really is like coming out from behind a cloud where you

161
00:13:35,750 --> 00:13:39,280
 light up the world. When you're sitting at night and the

162
00:13:39,280 --> 00:13:43,180
 moon comes out and the whole world is lit.

163
00:13:43,180 --> 00:13:46,940
 This is like when mindfulness arises in the mind, it's like

164
00:13:46,940 --> 00:13:51,440
 you were asleep. It's like you were in a dark room and you

165
00:13:51,440 --> 00:13:55,180
 can only now see things as the light is turned on.

166
00:13:56,180 --> 00:14:04,950
 It speaks of the condition of being in the wrong in the

167
00:14:04,950 --> 00:14:14,180
 past. That we shouldn't be too discouraged.

168
00:14:14,180 --> 00:14:19,140
 My teacher would often recite this verse to people who were

169
00:14:19,140 --> 00:14:24,240
 discouraged about their past, bad things they had done,

170
00:14:24,240 --> 00:14:34,550
 feeling guilty about them, feeling perhaps unqualified for

171
00:14:34,550 --> 00:14:38,180
 the task at hand because of their past.

172
00:14:38,180 --> 00:14:43,850
 He would say, "Oh, that's all in the past. Who you are in

173
00:14:43,850 --> 00:14:49,180
 the past is not important in doing the right thing."

174
00:14:49,180 --> 00:14:53,180
 Then he would recite this verse.

175
00:14:53,180 --> 00:14:58,150
 We shouldn't dwell in the past. We should acknowledge that

176
00:14:58,150 --> 00:15:02,850
 we're all coming out of darkness. We're all coming out of

177
00:15:02,850 --> 00:15:04,180
 delusion.

178
00:15:04,180 --> 00:15:11,170
 Be clear of what takes us out of delusion. Be inspired by

179
00:15:11,170 --> 00:15:18,030
 how profound and wonderful is the change that comes when

180
00:15:18,030 --> 00:15:25,180
 one is mindful, how it lights up your mind, the earth.

181
00:15:25,180 --> 00:15:29,400
 It relates to the story in the way that it is the most

182
00:15:29,400 --> 00:15:34,200
 important work, the most valuable work. The Buddha doesn't

183
00:15:34,200 --> 00:15:38,200
 just say that this person brings light to themself. He says

184
00:15:38,200 --> 00:15:43,180
 this person lights up the world.

185
00:15:43,180 --> 00:15:47,390
 In a way, it's an implication that they do the most

186
00:15:47,390 --> 00:15:52,310
 important work because they bring light to the world. A

187
00:15:52,310 --> 00:15:55,180
 person who is mindful.

188
00:15:55,180 --> 00:16:01,410
 If you think about what is accomplished through the work

189
00:16:01,410 --> 00:16:08,480
 that we do, you ask yourself, these people who are proud of

190
00:16:08,480 --> 00:16:14,870
 their job, their fastigeseness, their effort that they put

191
00:16:14,870 --> 00:16:19,180
 into worldly affairs, ask them what is the result.

192
00:16:19,180 --> 00:16:26,730
 If I make things, or if I just crunch numbers, or if I sell

193
00:16:26,730 --> 00:16:34,870
 things, ask yourself, what are you accomplishing with your

194
00:16:34,870 --> 00:16:36,180
 work?

195
00:16:36,180 --> 00:16:39,680
 Even for yourself, you're only gaining money. But if you

196
00:16:39,680 --> 00:16:43,210
 ask, what are you doing for other people? What are you

197
00:16:43,210 --> 00:16:47,940
 doing for the world? It's quite useful for those people

198
00:16:47,940 --> 00:16:52,180
 living in the world to ask, what is it that I'm doing?

199
00:16:52,180 --> 00:16:57,950
 What effect does my job have on the world? Does my work

200
00:16:57,950 --> 00:17:03,350
 have on the world? I think that really frames things in a

201
00:17:03,350 --> 00:17:07,530
 more favorable light for a Buddhist monk or a Buddhist med

202
00:17:07,530 --> 00:17:08,180
itator.

203
00:17:08,180 --> 00:17:14,490
 Because the effect of our work, the work that we do as med

204
00:17:14,490 --> 00:17:21,320
itators, by cultivating mindfulness, it brings about the

205
00:17:21,320 --> 00:17:23,180
 best result.

206
00:17:23,180 --> 00:17:28,830
 It reduces, at the very least, the amount of greed, anger,

207
00:17:28,830 --> 00:17:34,180
 and delusion, the amount of defilements in the world.

208
00:17:34,180 --> 00:17:43,030
 But moreover, it provides a source and a resource for the

209
00:17:43,030 --> 00:17:52,780
 spread of mindfulness, of enlightenment, of peace, of

210
00:17:52,780 --> 00:17:59,830
 purity, of an absence, of all those things that are causing

211
00:17:59,830 --> 00:18:01,180
 the problems.

212
00:18:01,180 --> 00:18:08,530
 It's like this, this illness, this virus that spreads. The

213
00:18:08,530 --> 00:18:13,690
 human race is like an organism that is crippled by a

214
00:18:13,690 --> 00:18:16,180
 debilitating illness.

215
00:18:16,180 --> 00:18:20,570
 It's living and getting by. There's a lot of good that

216
00:18:20,570 --> 00:18:23,860
 happens in the world, but you can always see that it's sick

217
00:18:23,860 --> 00:18:30,180
. The human race is sick. It is ill.

218
00:18:30,180 --> 00:18:36,540
 And so we're spreading the cure, the antivirus, for the

219
00:18:36,540 --> 00:18:39,180
 practice of meditation, curing the human race.

220
00:18:39,180 --> 00:18:43,260
 Now, whether it will ever be cured, I mean, I think it ebbs

221
00:18:43,260 --> 00:18:49,830
 and flows, but there's no question or no doubt about the

222
00:18:49,830 --> 00:18:57,010
 result of the practice of meditation and mindfulness when

223
00:18:57,010 --> 00:18:59,180
 it does occur.

224
00:18:59,180 --> 00:19:06,320
 So it speaks of the result of mindfulness. When a person is

225
00:19:06,320 --> 00:19:10,170
 mindful, when a person is no longer caught up in heed

226
00:19:10,170 --> 00:19:13,180
lessness, caught up in indulgence,

227
00:19:13,180 --> 00:19:19,220
 and suddenly the world, it's like everything is set right.

228
00:19:19,220 --> 00:19:25,500
 Suddenly you're here, you're present, you're aware, you're

229
00:19:25,500 --> 00:19:28,180
 awake, your mind is clear.

230
00:19:28,180 --> 00:19:31,550
 Just like the moon when it comes out from behind the cloud.

231
00:19:31,550 --> 00:19:35,180
 It's just good imagery. It's a good verse.

232
00:19:35,180 --> 00:19:39,290
 So that's the Dhammapada for this week. Thank you all for

233
00:19:39,290 --> 00:19:41,180
 tuning in. Have a good night.

