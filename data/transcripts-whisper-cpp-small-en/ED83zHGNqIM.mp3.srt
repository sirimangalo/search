1
00:00:00,000 --> 00:00:04,000
 Is a Buddha able to withstand physical pain?

2
00:00:04,000 --> 00:00:08,000
 Has he not mastered the art of letting go?

3
00:00:08,000 --> 00:00:12,370
 What's the origin of physical pain? Is thought the origin

4
00:00:12,370 --> 00:00:13,000
 of physical pain?

5
00:00:13,000 --> 00:00:21,000
 What do you mean by withstand? That's I think the important

6
00:00:21,000 --> 00:00:22,000
 question here.

7
00:00:22,000 --> 00:00:25,630
 You mean not experience physical pain? No, withstand doesn

8
00:00:25,630 --> 00:00:26,000
't mean that.

9
00:00:26,000 --> 00:00:31,000
 I'm going to go with...

10
00:00:31,000 --> 00:00:36,000
 With your literally asking and of course yes,

11
00:00:36,000 --> 00:00:40,000
 yes the Buddha is able to master to withstand physical pain

12
00:00:40,000 --> 00:00:40,000
 and yes he has,

13
00:00:40,000 --> 00:00:44,000
 because yes he has mastered the art of letting go.

14
00:00:44,000 --> 00:00:48,550
 But it kind of sounds like he might be asking whether he

15
00:00:48,550 --> 00:00:51,000
 doesn't have physical pain and that's not true.

16
00:00:51,000 --> 00:00:56,110
 There are I think five things that a Buddha can't... the

17
00:00:56,110 --> 00:00:57,000
 Buddha is unable to prevent.

18
00:00:57,000 --> 00:01:01,000
 Death is one.

19
00:01:01,000 --> 00:01:25,000
 The activities of the body urinating and defecating is one.

20
00:01:25,000 --> 00:01:29,650
 There is the case where when the Buddha was getting close

21
00:01:29,650 --> 00:01:32,000
 to passing away he got very sick

22
00:01:32,000 --> 00:01:37,950
 but he suppressed the sickness because he knew that he

23
00:01:37,950 --> 00:01:39,000
 still had some work to do.

24
00:01:39,000 --> 00:01:42,150
 Not because he didn't want to feel the pain but because the

25
00:01:42,150 --> 00:01:45,000
 sickness would get in the way of him finishing his task.

26
00:01:45,000 --> 00:01:51,000
 So he suppressed it in order to finish his teaching.

27
00:01:51,000 --> 00:01:55,430
 There is another case where he didn't do that, where when

28
00:01:55,430 --> 00:01:57,000
 they went out and walked to rock on him

29
00:01:57,000 --> 00:02:03,010
 and he lay down and was mindful and Dr. Devaka came to him

30
00:02:03,010 --> 00:02:09,000
 and put some medicine on his foot and then left him.

31
00:02:09,000 --> 00:02:16,600
 And the Buddha was in great pain, I think the Buddha was in

32
00:02:16,600 --> 00:02:17,000
 great pain

33
00:02:17,000 --> 00:02:19,900
 and Dr. Devaka was amazed that the Buddha was able to

34
00:02:19,900 --> 00:02:21,000
 withstand the pain.

35
00:02:21,000 --> 00:02:31,000
 So it's a neat thing to read if you can go and find that.

36
00:02:31,000 --> 00:02:34,970
 But then he forgot, then he had to leave. He said I'll be

37
00:02:34,970 --> 00:02:41,190
 back in an hour or something to take the medicine off the

38
00:02:41,190 --> 00:02:42,000
 lake.

39
00:02:42,000 --> 00:02:45,780
 And he went into the city to perform some duties and he was

40
00:02:45,780 --> 00:02:48,000
 a little bit late and so on his way out

41
00:02:48,000 --> 00:02:52,160
 as he was getting to the city gate, they closed the city

42
00:02:52,160 --> 00:02:54,000
 gate before he got to it.

43
00:02:54,000 --> 00:02:56,000
 So he wasn't able to leave the city.

44
00:02:56,000 --> 00:03:00,160
 And the Buddha was lying there with his medicine and Dr.

45
00:03:00,160 --> 00:03:01,000
 Devaka was really worried

46
00:03:01,000 --> 00:03:05,420
 because if the medicine didn't come off there would be

47
00:03:05,420 --> 00:03:08,000
 problems and it was too strong or something like that.

48
00:03:08,000 --> 00:03:11,270
 It had to be taken off and so in the morning he rushed to

49
00:03:11,270 --> 00:03:14,580
 the Buddha but by that time the Buddha had understood the

50
00:03:14,580 --> 00:03:15,000
 situation

51
00:03:15,000 --> 00:03:18,000
 and told Ananda to take the medicine off.

52
00:03:18,000 --> 00:03:20,870
 So Devaka won't be able to come back today, take the

53
00:03:20,870 --> 00:03:23,000
 medicine off to Ananda to take it off.

54
00:03:23,000 --> 00:03:29,610
 And then he came back. So the point being is according to

55
00:03:29,610 --> 00:03:36,000
 that passage we understand that the Buddha didn't suppress

56
00:03:36,000 --> 00:03:36,000
 the pain

57
00:03:36,000 --> 00:03:39,900
 or wasn't interested at all in getting rid of the pain but

58
00:03:39,900 --> 00:03:47,210
 allowed to get to put this medicine on it to heal it or do

59
00:03:47,210 --> 00:03:51,000
 whatever.

60
00:03:51,000 --> 00:03:53,770
 And then the last part is thought, the origin of physical

61
00:03:53,770 --> 00:04:00,000
 pain. Well, not directly but indirectly

62
00:04:00,000 --> 00:04:04,370
 because without thought you can't be born. Thought maybe is

63
00:04:04,370 --> 00:04:07,000
 not exactly the right word but without intention

64
00:04:07,000 --> 00:04:11,790
 you can be born and therefore cannot have pain. But pain is

65
00:04:11,790 --> 00:04:23,000
 caused by physical, directly caused by the physical.

66
00:04:23,000 --> 00:04:34,360
 Of course it only gives a feeling of pain. It only gives a

67
00:04:34,360 --> 00:04:35,000
 feeling of pain in the mind.

68
00:04:35,000 --> 00:04:41,160
 It may deny mental jade to seek it. So without the mind you

69
00:04:41,160 --> 00:04:42,000
 don't feel pain.

70
00:04:42,000 --> 00:04:45,340
 If there is only the body there is no pain in the body.

71
00:04:45,340 --> 00:04:47,000
 Sorry, there is no feeling of pain.

72
00:04:47,000 --> 00:04:51,440
 There might be physical pain but without the mind present

73
00:04:51,440 --> 00:04:54,000
 there will be no feeling of it.

74
00:04:55,000 --> 00:05:00,000
 Thank you.

