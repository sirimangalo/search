1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:08,000
 Today we continue on with verse 125

3
00:00:08,000 --> 00:00:12,000
 which reads as follows.

4
00:00:12,000 --> 00:00:33,000
 (Tibetan)

5
00:00:33,000 --> 00:00:48,000
 Which means who should attack or offend against dusati,

6
00:00:48,000 --> 00:00:56,000
 a person who is without fault, apadutasa, narasa.

7
00:00:56,000 --> 00:01:05,010
 When both a man or a person who is sudha pure and anangana

8
00:01:05,010 --> 00:01:09,000
 without fault, without blame.

9
00:01:09,000 --> 00:01:29,000
 Tameva balang pachidipapang, such that evil, pappan, pachid

10
00:01:29,000 --> 00:01:29,000
ip, goes back upon tameva balang, that fool.

11
00:01:29,000 --> 00:01:32,000
 The evil goes back upon that fool.

12
00:01:32,000 --> 00:01:42,030
 Sukumurajho pattiwatamvakitau, just as rajah, dust, that is

13
00:01:42,030 --> 00:01:48,780
 sukuma, that is fine, just as a fine dust, that is thrown

14
00:01:48,780 --> 00:01:51,000
 against the wind.

15
00:01:51,000 --> 00:01:54,280
 So if you throw dust against the wind, it comes back into

16
00:01:54,280 --> 00:01:55,000
 your face.

17
00:01:55,000 --> 00:02:03,950
 In the same way evil done to someone who is blameless comes

18
00:02:03,950 --> 00:02:07,000
 back to bite you.

19
00:02:07,000 --> 00:02:14,000
 So quite memorable words.

20
00:02:14,000 --> 00:02:19,880
 This is one of those things for us to keep in mind, a

21
00:02:19,880 --> 00:02:24,000
 description of the law of karma.

22
00:02:24,000 --> 00:02:28,000
 That is what this refers to.

23
00:02:28,000 --> 00:02:34,480
 But it is even more because the idea is that evil does not

24
00:02:34,480 --> 00:02:38,000
 harm the one who is the object of evil.

25
00:02:38,000 --> 00:02:42,000
 Evil harms the one who performs the evil.

26
00:02:42,000 --> 00:02:46,310
 In sometimes in seeming magical ways, which is sort of what

27
00:02:46,310 --> 00:02:49,000
 this story behind this verse is about.

28
00:02:49,000 --> 00:02:57,000
 So the story goes that there was a hunter named Koka.

29
00:02:57,000 --> 00:03:08,000
 And this hunter was accompanied by a pack of hounds.

30
00:03:08,000 --> 00:03:11,060
 So when he would go hunting, he would take dogs with him

31
00:03:11,060 --> 00:03:15,220
 and the dogs would pick up his prey or maybe even help him

32
00:03:15,220 --> 00:03:20,000
 kill it, drag it down for him to kill.

33
00:03:20,000 --> 00:03:23,000
 And so he trained them to be quite vicious.

34
00:03:23,000 --> 00:03:27,470
 One day when he was doing his hunting in the forest, he

35
00:03:27,470 --> 00:03:29,000
 came across a monk.

36
00:03:29,000 --> 00:03:34,240
 And somehow he had this idea that seeing a monk was a bad o

37
00:03:34,240 --> 00:03:35,000
men.

38
00:03:35,000 --> 00:03:40,030
 Seeing a monk in the morning, this idea had been spread

39
00:03:40,030 --> 00:03:45,000
 about maybe by enemies of monks or enemies of Buddhism.

40
00:03:45,000 --> 00:03:49,000
 At any rate, he thought this was a bad omen to see a monk.

41
00:03:49,000 --> 00:03:56,000
 And so the monk went on his way for alms in the city.

42
00:03:56,000 --> 00:03:59,000
 And the hunter went on his way to try and catch prey.

43
00:03:59,000 --> 00:04:03,000
 And sure enough, the whole day he caught nothing.

44
00:04:03,000 --> 00:04:08,030
 Meanwhile, the monk came back and ate his lunch and was

45
00:04:08,030 --> 00:04:11,000
 perhaps meditating in the forest.

46
00:04:11,000 --> 00:04:15,140
 And the hunter on his way back through the forest hadn't

47
00:04:15,140 --> 00:04:19,460
 caught the sight of the hunter and caught sight of the monk

48
00:04:19,460 --> 00:04:22,000
 again and was furious.

49
00:04:22,000 --> 00:04:25,120
 And he thought, "This monk is the reason why I caught

50
00:04:25,120 --> 00:04:26,000
 nothing."

51
00:04:26,000 --> 00:04:32,320
 Somehow he had this idea. And so he yelled at the monk and

52
00:04:32,320 --> 00:04:40,000
 accused the monk of being the cause of his bad luck.

53
00:04:40,000 --> 00:04:43,790
 And he brought his hounds out and he was ready to set his h

54
00:04:43,790 --> 00:04:45,000
ounds on the monk.

55
00:04:45,000 --> 00:04:48,080
 And the monk begged and pleaded for him not to do such a

56
00:04:48,080 --> 00:04:51,660
 horrible thing, that he was innocent, that he had not done

57
00:04:51,660 --> 00:04:54,000
 anything against the hunter.

58
00:04:54,000 --> 00:04:58,920
 Part of it may have been because Buddhist monks were known

59
00:04:58,920 --> 00:05:01,000
 to be against killing.

60
00:05:01,000 --> 00:05:04,000
 They taught people to give up killing.

61
00:05:04,000 --> 00:05:08,480
 And so as a result, there was this idea that somehow they

62
00:05:08,480 --> 00:05:11,000
 were bad luckers or something.

63
00:05:11,000 --> 00:05:21,000
 Because it was something that they believed was wrong.

64
00:05:21,000 --> 00:05:26,170
 Anyway, this hunter refused to listen to the monk and set

65
00:05:26,170 --> 00:05:28,000
 his dogs on the monk.

66
00:05:28,000 --> 00:05:33,400
 And the monk immediately climbed up a tree and was standing

67
00:05:33,400 --> 00:05:36,000
 over one of the branches.

68
00:05:36,000 --> 00:05:40,170
 And so what the hunter did, he was so angry, he was really

69
00:05:40,170 --> 00:05:42,000
 out to get this monk.

70
00:05:42,000 --> 00:05:45,770
 And so he took one of his arrows and he poked up at the

71
00:05:45,770 --> 00:05:47,000
 monk's feet.

72
00:05:47,000 --> 00:05:50,360
 First the monk's right foot and the monk lifted up his

73
00:05:50,360 --> 00:05:53,000
 right foot and put his left foot down instead.

74
00:05:53,000 --> 00:05:56,410
 And the hunter poked his left foot and really gouged his

75
00:05:56,410 --> 00:05:57,000
 feet.

76
00:05:57,000 --> 00:06:01,190
 And so then the monk pulled up both feet, but he was in so

77
00:06:01,190 --> 00:06:02,000
 much pain.

78
00:06:02,000 --> 00:06:07,400
 And he started to lose mindfulness awareness of what he was

79
00:06:07,400 --> 00:06:08,000
 doing.

80
00:06:08,000 --> 00:06:14,490
 And he dropped his outer robe, the big robe that would act

81
00:06:14,490 --> 00:06:16,000
 as a blanket.

82
00:06:16,000 --> 00:06:22,000
 And it fell on the hunter and covered him.

83
00:06:22,000 --> 00:06:26,000
 And immediately the dogs turned and looked at the hunter

84
00:06:26,000 --> 00:06:28,000
 and thought, "The monk has fallen out of the tree."

85
00:06:28,000 --> 00:06:32,990
 And they turned on the hunter and ripped him to shreds and

86
00:06:32,990 --> 00:06:34,000
 killed him,

87
00:06:34,000 --> 00:06:40,930
 thinking they were following their master's orders because

88
00:06:40,930 --> 00:06:44,000
 they smelled this robe.

89
00:06:44,000 --> 00:06:47,670
 And the monk looking down at what was going, he picked up a

90
00:06:47,670 --> 00:06:49,000
 stick and he threw it at the dogs.

91
00:06:49,000 --> 00:06:53,220
 And the dogs looked up and saw that the monk was still in

92
00:06:53,220 --> 00:06:58,340
 the tree and scampered off into the forest, confused or

93
00:06:58,340 --> 00:07:04,000
 maybe embarrassed at how foolish they'd been.

94
00:07:04,000 --> 00:07:07,000
 That's the story.

95
00:07:07,000 --> 00:07:09,000
 And the monk actually felt bad about this.

96
00:07:09,000 --> 00:07:13,650
 He thought, "Well, it's because of me that this hunter died

97
00:07:13,650 --> 00:07:15,000
. It was really my fault.

98
00:07:15,000 --> 00:07:19,060
 It was because I dropped his robe on him, because I was un

99
00:07:19,060 --> 00:07:23,000
mindful. Do I have any fault in that?"

100
00:07:23,000 --> 00:07:26,990
 And of course the Buddha said, "No, be cool, you are bl

101
00:07:26,990 --> 00:07:28,000
ameless."

102
00:07:28,000 --> 00:07:31,000
 And moreover, he said, "This isn't the first time."

103
00:07:31,000 --> 00:07:35,640
 And he told the story of the past that apparently in a past

104
00:07:35,640 --> 00:07:40,000
 life this guy had the same sort of thing happen to him.

105
00:07:40,000 --> 00:07:43,200
 He was a cruel, terrible person and the same sort of thing

106
00:07:43,200 --> 00:07:44,000
 happened.

107
00:07:44,000 --> 00:07:49,230
 He was a physician and he was trying to sell his cures and

108
00:07:49,230 --> 00:07:52,710
 sell his services and no one was buying it and no one was

109
00:07:52,710 --> 00:07:53,000
 sick.

110
00:07:53,000 --> 00:07:58,090
 And he thought, "Well, maybe I'll just make people sick and

111
00:07:58,090 --> 00:08:01,970
 get them to... then they'll need me. They'll need my help

112
00:08:01,970 --> 00:08:03,000
 if they're sick."

113
00:08:03,000 --> 00:08:07,000
 And so he took a poisonous snake and he stuck it in a tree.

114
00:08:07,000 --> 00:08:11,350
 And he told these boys to go and fetch him this... that

115
00:08:11,350 --> 00:08:15,750
 there was a bird in there in this hole and they should go

116
00:08:15,750 --> 00:08:18,000
 and fetch it for him.

117
00:08:18,000 --> 00:08:22,340
 And so one of the boys stuck his hand in the tree and

118
00:08:22,340 --> 00:08:25,410
 grabbed the snake thinking it was this bird that he was

119
00:08:25,410 --> 00:08:26,000
 going to catch.

120
00:08:26,000 --> 00:08:29,850
 And when he realized it was a snake, he threw it, freaked

121
00:08:29,850 --> 00:08:32,000
 out and threw it really quickly.

122
00:08:32,000 --> 00:08:37,600
 And it hit the position in the head and wrapped around his

123
00:08:37,600 --> 00:08:41,000
 head and bit him and killed him.

124
00:08:41,000 --> 00:08:46,470
 Kind of silly stories. But the story of the hunter and the

125
00:08:46,470 --> 00:08:52,800
 monk in the tree is a memorable one of how evil can go

126
00:08:52,800 --> 00:08:54,000
 wrong.

127
00:08:54,000 --> 00:08:59,000
 How our deeds sometimes turn back upon us.

128
00:08:59,000 --> 00:09:03,840
 Now, there are different reasons why we can say this

129
00:09:03,840 --> 00:09:05,000
 happens.

130
00:09:05,000 --> 00:09:08,290
 I mean, the most obvious reason for our evil deeds to bring

131
00:09:08,290 --> 00:09:11,320
 evil upon ourselves is the effect that they have on our

132
00:09:11,320 --> 00:09:12,000
 minds.

133
00:09:12,000 --> 00:09:18,040
 So anger, end greed and delusion, the three defilements

134
00:09:18,040 --> 00:09:22,200
 make you careless when you're obsessed with something or

135
00:09:22,200 --> 00:09:24,000
 when you're angry.

136
00:09:24,000 --> 00:09:29,250
 It's bad karma first and foremost because of how it affects

137
00:09:29,250 --> 00:09:34,290
 your mind, because of how it changes you and changes the

138
00:09:34,290 --> 00:09:39,000
 actions and the choices that you make.

139
00:09:39,000 --> 00:09:43,280
 Furthermore, it does affect the world around you. The

140
00:09:43,280 --> 00:09:48,000
 second way it does, I think, is in how others look at you.

141
00:09:48,000 --> 00:09:53,000
 So in this case there were no other people involved.

142
00:09:53,000 --> 00:09:58,630
 But by disrupting the monk, he brought upon himself at

143
00:09:58,630 --> 00:10:04,500
 least you could say some sort of chaos in which many things

144
00:10:04,500 --> 00:10:06,000
 are possible.

145
00:10:06,000 --> 00:10:11,700
 When you attack someone, sometimes they freak out and it

146
00:10:11,700 --> 00:10:14,000
 disturbs the order.

147
00:10:14,000 --> 00:10:16,250
 But you see, I'm trying to get to the point where you could

148
00:10:16,250 --> 00:10:19,000
 actually say, "Well, this is a result of his bad karma,"

149
00:10:19,000 --> 00:10:22,000
 because it doesn't seem like it is.

150
00:10:22,000 --> 00:10:25,830
 It seems like it was just a freak chance and nine out of

151
00:10:25,830 --> 00:10:32,160
 ten times or 99 out of 100 times or 999 out of 1000 times,

152
00:10:32,160 --> 00:10:36,000
 the hunter would have killed the monk in the tree.

153
00:10:36,000 --> 00:10:40,250
 What we want to say is that there is potentially things

154
00:10:40,250 --> 00:10:45,000
 protecting people who are good, people who are blameless.

155
00:10:45,000 --> 00:10:48,020
 I don't know that you can actually. I don't know that it's

156
00:10:48,020 --> 00:10:50,000
 actually proper and true to say that.

157
00:10:50,000 --> 00:10:52,950
 We want to, and Buddhists like to try and say that, that

158
00:10:52,950 --> 00:10:56,000
 there are things protecting you. I'm not so sure.

159
00:10:56,000 --> 00:11:01,060
 I would agree that there are probably beings, like who

160
00:11:01,060 --> 00:11:06,460
 knows, maybe there was some angel or spirit involved with

161
00:11:06,460 --> 00:11:09,240
 this that pulled the robe off the monk and dropped it on

162
00:11:09,240 --> 00:11:10,000
 the hunter.

163
00:11:10,000 --> 00:11:13,640
 That makes sense to me if such beings exist. Of course,

164
00:11:13,640 --> 00:11:17,020
 many, many people think it's ridiculous that such beings

165
00:11:17,020 --> 00:11:18,000
 would exist.

166
00:11:18,000 --> 00:11:20,620
 I'd agree that it seems kind of fair-fetched that such a

167
00:11:20,620 --> 00:11:23,000
 being would just be there and get involved.

168
00:11:23,000 --> 00:11:26,000
 But hey, who knows? There's that kind of thing.

169
00:11:26,000 --> 00:11:29,280
 And so that's a direct result of karma. People don't, other

170
00:11:29,280 --> 00:11:32,830
 beings don't appreciate when you're cruel to people who don

171
00:11:32,830 --> 00:11:34,000
't deserve it.

172
00:11:34,000 --> 00:11:39,000
 And that sort of thing really is a part of karma.

173
00:11:39,000 --> 00:11:44,400
 Good people attract others who will protect them, attract

174
00:11:44,400 --> 00:11:46,000
 good friends.

175
00:11:46,000 --> 00:11:50,310
 And so if you try to harm a harmless being, you're less

176
00:11:50,310 --> 00:11:55,000
 likely to succeed just because they're surrounded by good

177
00:11:55,000 --> 00:11:56,000
 people.

178
00:11:56,000 --> 00:12:06,010
 But I think you could also argue that the power of goodness

179
00:12:06,010 --> 00:12:13,000
 has some sort of reverberations in the universe.

180
00:12:13,000 --> 00:12:17,060
 Like it sets things up in a certain way. I think you could

181
00:12:17,060 --> 00:12:18,000
 argue that.

182
00:12:18,000 --> 00:12:23,300
 I think you could point to ways in which it's true that a

183
00:12:23,300 --> 00:12:26,160
 person who does lots of good deeds, a person who's never

184
00:12:26,160 --> 00:12:27,000
 done evil deeds,

185
00:12:27,000 --> 00:12:35,950
 is somehow in a different sort of train or groove, you

186
00:12:35,950 --> 00:12:40,000
 might say, from a person who is intent upon evil.

187
00:12:40,000 --> 00:12:43,590
 Like they're so opposite that they're almost living in

188
00:12:43,590 --> 00:12:46,470
 different universes and it's very hard for them to come

189
00:12:46,470 --> 00:12:47,000
 together.

190
00:12:47,000 --> 00:12:50,100
 Now it's possible. But I think you could point to that as

191
00:12:50,100 --> 00:12:54,000
 being a reason why it takes a really great amount of effort

192
00:12:54,000 --> 00:12:57,000
 to harm someone who is harmless.

193
00:12:57,000 --> 00:13:00,710
 I don't want to say it's certainly not magic, but I think

194
00:13:00,710 --> 00:13:03,000
 there's much, much more going on.

195
00:13:03,000 --> 00:13:08,620
 And we often will, it's common for people to underestimate

196
00:13:08,620 --> 00:13:13,000
 the power of goodness and the power of evil.

197
00:13:13,000 --> 00:13:20,920
 Evil is something that shrinks inward or comes back upon

198
00:13:20,920 --> 00:13:22,000
 you.

199
00:13:22,000 --> 00:13:26,000
 Good is something that's expansive and it spreads.

200
00:13:26,000 --> 00:13:31,100
 So you can't spread, spreading evil is not the same as

201
00:13:31,100 --> 00:13:37,550
 spreading good. It's much more likely to come back upon you

202
00:13:37,550 --> 00:13:38,000
.

203
00:13:38,000 --> 00:13:42,000
 But I think at any rate, I would argue that there's room

204
00:13:42,000 --> 00:13:43,000
 for that.

205
00:13:43,000 --> 00:13:47,280
 I think it's something that is very hard to find proof or

206
00:13:47,280 --> 00:13:49,000
 evidence even for.

207
00:13:49,000 --> 00:13:53,240
 But I would say that there's room for the idea that

208
00:13:53,240 --> 00:13:58,660
 goodness protects good people and people who try to do evil

209
00:13:58,660 --> 00:14:02,780
 towards good people for various reasons have a harder time

210
00:14:02,780 --> 00:14:04,000
 of it.

211
00:14:04,000 --> 00:14:06,570
 And it's much more likely to come back and bite them in the

212
00:14:06,570 --> 00:14:07,000
 face.

213
00:14:07,000 --> 00:14:11,010
 I mean, absolutely, even when they get away with it, it's

214
00:14:11,010 --> 00:14:12,000
 far worse.

215
00:14:12,000 --> 00:14:19,230
 There's no question that it eventually comes back far, far

216
00:14:19,230 --> 00:14:24,000
 worse on the person who's done an evil deant towards

217
00:14:24,000 --> 00:14:26,000
 someone who doesn't deserve it.

218
00:14:26,000 --> 00:14:32,590
 I mean, who hasn't done anything to instigate it, who is bl

219
00:14:32,590 --> 00:14:34,000
ameless.

220
00:14:34,000 --> 00:14:38,990
 Like if this monk had gone around setting animals free,

221
00:14:38,990 --> 00:14:41,590
 well, then you could say maybe he's somewhat mental some

222
00:14:41,590 --> 00:14:42,000
 more.

223
00:14:42,000 --> 00:14:48,080
 If he had purposefully wished or if he had done everything

224
00:14:48,080 --> 00:14:53,000
 he could to ruin this hunter and to harm him.

225
00:14:53,000 --> 00:14:58,650
 We had done something to warrant even littlest bit of

226
00:14:58,650 --> 00:15:03,000
 malice, then it would be different.

227
00:15:03,000 --> 00:15:08,000
 You could argue and it would be less of a weighty crime.

228
00:15:08,000 --> 00:15:10,000
 But to harm someone is harmless.

229
00:15:10,000 --> 00:15:12,000
 I mean, you know it in your bones.

230
00:15:12,000 --> 00:15:14,000
 You don't have to be Buddhist to believe this.

231
00:15:14,000 --> 00:15:18,730
 It's just far more horrific to harm someone who has done

232
00:15:18,730 --> 00:15:21,000
 nothing to deserve it.

233
00:15:21,000 --> 00:15:25,360
 And for that reason, for sure, it's going to affect your

234
00:15:25,360 --> 00:15:29,600
 mind and make you blind to the dangers that you're facing

235
00:15:29,600 --> 00:15:33,880
 and blind to your actions and blind to the circumstances

236
00:15:33,880 --> 00:15:35,000
 and so on.

237
00:15:35,000 --> 00:15:39,630
 But I think you could argue moreover that it's such a

238
00:15:39,630 --> 00:15:44,000
 powerful act that it jars with the universe.

239
00:15:44,000 --> 00:15:47,000
 As a result, it's harder to perform.

240
00:15:47,000 --> 00:15:50,310
 I'd like to think that there are things that protect people

241
00:15:50,310 --> 00:15:55,810
's lives, just aspects of the universe that are fit together

242
00:15:55,810 --> 00:16:00,800
 in such a way that it's not as easy to kill someone as we

243
00:16:00,800 --> 00:16:04,000
 might think, especially if they don't deserve it.

244
00:16:04,000 --> 00:16:10,090
 The idea that to some extent most killing, most deeds for

245
00:16:10,090 --> 00:16:16,000
 good or for evil have something to do with past karma.

246
00:16:16,000 --> 00:16:19,000
 The idea that there is some sort of connection there.

247
00:16:19,000 --> 00:16:20,000
 I don't think all.

248
00:16:20,000 --> 00:16:23,000
 I think it seems possible to create new karma with someone.

249
00:16:23,000 --> 00:16:27,000
 So if someone is innocent, you can harm them.

250
00:16:27,000 --> 00:16:30,710
 It's a very weighty karma, but you're starting something

251
00:16:30,710 --> 00:16:31,000
 new.

252
00:16:31,000 --> 00:16:34,170
 And it's quite possible that they will come back and harm

253
00:16:34,170 --> 00:16:36,000
 you even unintentionally.

254
00:16:36,000 --> 00:16:41,000
 Like in this case, usually it takes lifetimes to do that.

255
00:16:41,000 --> 00:16:45,920
 But anyway, these are aspects of karma that are kind of

256
00:16:45,920 --> 00:16:51,160
 magical and hard to understand or hard to believe or people

257
00:16:51,160 --> 00:16:54,000
 don't think of them as true.

258
00:16:54,000 --> 00:16:57,000
 But this isn't so important for our practice.

259
00:16:57,000 --> 00:17:00,720
 What's important for our practice is, as with all of these

260
00:17:00,720 --> 00:17:04,000
 sorts of stories, the consequences.

261
00:17:04,000 --> 00:17:09,280
 No question, there are consequences to our evil deeds and

262
00:17:09,280 --> 00:17:11,000
 our good deeds.

263
00:17:11,000 --> 00:17:14,000
 I think that's exemplified here both.

264
00:17:14,000 --> 00:17:18,000
 If you're a good person, you don't deserve to be harmed.

265
00:17:18,000 --> 00:17:21,000
 It's harder for people to harm you.

266
00:17:21,000 --> 00:17:23,000
 It's harder for people to want to hurt you.

267
00:17:23,000 --> 00:17:29,000
 It takes a really base and evil person to do it.

268
00:17:29,000 --> 00:17:35,310
 And you've got to wonder whether this monk was pleading

269
00:17:35,310 --> 00:17:39,610
 with this hunter to stop for his own benefit or for the

270
00:17:39,610 --> 00:17:41,000
 benefit of the hunter.

271
00:17:41,000 --> 00:17:49,660
 Because certainly even death by dog, by wild, rabid dogs,

272
00:17:49,660 --> 00:17:52,000
 is preferable.

273
00:17:52,000 --> 00:17:56,130
 We don't think of it this way, but it's actually far

274
00:17:56,130 --> 00:18:01,000
 preferable than to torture an innocent person.

275
00:18:01,000 --> 00:18:11,390
 The Buddha said many things like this. He said you should

276
00:18:11,390 --> 00:18:13,000
 rather be killed or tortured than to do an evil deed.

277
00:18:13,000 --> 00:18:17,160
 Because being killed and tortured doesn't lead you to a bad

278
00:18:17,160 --> 00:18:18,000
 future.

279
00:18:18,000 --> 00:18:23,130
 It doesn't hurt your mind. It doesn't affect you in the way

280
00:18:23,130 --> 00:18:25,000
 that evil does.

281
00:18:25,000 --> 00:18:31,860
 So, I mean, it's a big part of meditation that a beginner

282
00:18:31,860 --> 00:18:39,000
 has to come to understand that pain in the meditation,

283
00:18:39,000 --> 00:18:39,000
 right?

284
00:18:39,000 --> 00:18:41,530
 When you're sitting and meditating and you feel pain, pain

285
00:18:41,530 --> 00:18:43,000
 is not your enemy.

286
00:18:43,000 --> 00:18:50,440
 Unpleasantness of all kinds, whether it's itching or heat

287
00:18:50,440 --> 00:18:58,000
 or loud noise or thoughts, nightmares, visions, etc.

288
00:18:58,000 --> 00:19:01,000
 This is not the enemy. This is not the problem.

289
00:19:01,000 --> 00:19:07,490
 The problem is evil, what we call unwholesomeness. So greed

290
00:19:07,490 --> 00:19:10,000
, anger and delusion. These are problems.

291
00:19:10,000 --> 00:19:17,000
 They bring about things like pain and stress and suffering.

292
00:19:17,000 --> 00:19:22,000
 So coming to see that difference is very important.

293
00:19:22,000 --> 00:19:28,000
 To see that it's the evil that we have a problem with.

294
00:19:28,000 --> 00:19:33,960
 So this quote is describing a very extreme sort of karma,

295
00:19:33,960 --> 00:19:39,510
 the evil deed that is performed towards someone who doesn't

296
00:19:39,510 --> 00:19:40,000
 deserve it.

297
00:19:40,000 --> 00:19:48,870
 But it brings up all these issues of who is the person who

298
00:19:48,870 --> 00:19:55,000
 should be, who really receives the effect of a deed.

299
00:19:55,000 --> 00:19:58,910
 If you try to harm someone, well, they might be hurt or

300
00:19:58,910 --> 00:20:01,000
 even killed, but that's it.

301
00:20:01,000 --> 00:20:04,600
 It's actually quite insignificant compared to the evil that

302
00:20:04,600 --> 00:20:06,000
 comes back upon you.

303
00:20:06,000 --> 00:20:09,000
 So this is throwing dust in the wind.

304
00:20:09,000 --> 00:20:12,250
 If you throw dust in the wind, it comes back on you. Evil

305
00:20:12,250 --> 00:20:14,000
 deeds are very much like that.

306
00:20:14,000 --> 00:20:16,000
 Don't think of it, right?

307
00:20:16,000 --> 00:20:18,920
 You think if you could get away with it, hurting someone

308
00:20:18,920 --> 00:20:22,000
 else is really bad for the person who you hurt or kill.

309
00:20:22,000 --> 00:20:25,450
 It's actually very much the other way. That's what this

310
00:20:25,450 --> 00:20:28,000
 verse teaches. This is the claim that's being made.

311
00:20:28,000 --> 00:20:33,000
 And I think there's evidence, especially for meditators,

312
00:20:33,000 --> 00:20:36,170
 that this is very much, I know there's evidence, this is

313
00:20:36,170 --> 00:20:37,000
 very much the case.

314
00:20:37,000 --> 00:20:42,240
 I was just trying to get the idea that maybe there's even

315
00:20:42,240 --> 00:20:47,100
 more than just the obvious scars it leaves on your mind,

316
00:20:47,100 --> 00:20:48,000
 because that's the worst.

317
00:20:48,000 --> 00:20:53,710
 But it seems that actually there are cases of being

318
00:20:53,710 --> 00:20:58,330
 protected where you harm someone and it actually physically

319
00:20:58,330 --> 00:21:00,000
 hurts you right then and there.

320
00:21:00,000 --> 00:21:04,410
 That's considered to be "Ditadamavidh" and "Ya Kamma" that

321
00:21:04,410 --> 00:21:08,000
 gives results in this life, right here and now,

322
00:21:08,000 --> 00:21:11,530
 as opposed to the indirect, where it changes you and as a

323
00:21:11,530 --> 00:21:15,210
 result you make different life choices and other people

324
00:21:15,210 --> 00:21:18,910
 make different choices in regards to you and so on, how

325
00:21:18,910 --> 00:21:20,000
 they think of you and so on.

326
00:21:20,000 --> 00:21:25,000
 And eventually it harms you in an indirect way.

327
00:21:25,000 --> 00:21:30,150
 Either way, this is all part of the concept of good and

328
00:21:30,150 --> 00:21:35,050
 evil. In Buddhism, in our meditation practice, we strive

329
00:21:35,050 --> 00:21:40,920
 for good and we do it rationally, not because we're told it

330
00:21:40,920 --> 00:21:45,000
's good or because it's good in and of itself,

331
00:21:45,000 --> 00:21:48,400
 but because we really believe that it's the way to peace,

332
00:21:48,400 --> 00:21:53,010
 happiness and freedom from suffering. That's our practice.

333
00:21:53,010 --> 00:21:55,000
 And that's the "Ditadamapada" for tonight.

334
00:21:55,000 --> 00:21:58,160
 Thank you for tuning in, wishing you all good practice and

335
00:21:58,160 --> 00:22:02,000
 peace, happiness and freedom from suffering. Thank you.

