1
00:00:00,000 --> 00:00:04,740
 Where to place observation while watching the stomach

2
00:00:04,740 --> 00:00:06,000
 during meditation?

3
00:00:06,000 --> 00:00:11,000
 Where to place observer?

4
00:00:11,000 --> 00:00:15,840
 What are you watching while you're just watching the

5
00:00:15,840 --> 00:00:17,000
 stomach?

6
00:00:17,000 --> 00:00:21,510
 An easy way is to lie on your back if it's really painful

7
00:00:21,510 --> 00:00:25,910
 to do this because our bodies are so stressed out and tense

8
00:00:25,910 --> 00:00:26,000
.

9
00:00:26,000 --> 00:00:29,220
 You have to lie down on your back just to confirm it and

10
00:00:29,220 --> 00:00:32,000
 you'll see it can be quite surprising for people.

11
00:00:32,000 --> 00:00:36,060
 People will come to ask for advice and say, "I really can't

12
00:00:36,060 --> 00:00:40,000
 do the rising and falling, it just doesn't happen for me."

13
00:00:40,000 --> 00:00:42,580
 And you explain to them, "Well, this is because your body

14
00:00:42,580 --> 00:00:44,000
 is in an unnatural state."

15
00:00:44,000 --> 00:00:47,000
 And they're, "Yeah, right. Why should I believe you?"

16
00:00:47,000 --> 00:00:50,940
 Have them lie down on their back and suddenly boom, their

17
00:00:50,940 --> 00:00:52,000
 stomach goes like a baby.

18
00:00:52,000 --> 00:00:55,380
 It's like magic all of a sudden because then they're

19
00:00:55,380 --> 00:00:56,000
 relaxed.

20
00:00:56,000 --> 00:00:58,610
 And so you're able to show them that it's only because of

21
00:00:58,610 --> 00:01:01,000
 your stress and tension that it's difficult.

22
00:01:01,000 --> 00:01:04,250
 The rising and falling, it's part of, I suppose, part of

23
00:01:04,250 --> 00:01:09,000
 the problem why nowadays it's so much...

24
00:01:09,000 --> 00:01:14,000
 What's the word?

25
00:01:14,000 --> 00:01:18,760
 People don't really like watching the stomach thing, which

26
00:01:18,760 --> 00:01:24,390
 wouldn't have been the case in times gone by when we would

27
00:01:24,390 --> 00:01:26,000
 naturally breathe through the stomach.

28
00:01:26,000 --> 00:01:28,850
 And it isn't the case for people living in the forest who

29
00:01:28,850 --> 00:01:32,000
 live simple, peaceful lives because they breathe naturally.

30
00:01:32,000 --> 00:01:37,500
 Like a baby, they'll breathe from the abdomen. It's quite a

31
00:01:37,500 --> 00:01:39,000
 natural thing.

32
00:01:39,000 --> 00:01:42,990
 You just observe that movement, but maybe the reason why

33
00:01:42,990 --> 00:01:45,720
 you're asking the question is because you're not able to

34
00:01:45,720 --> 00:01:48,000
 observe it and it's kind of confusing.

35
00:01:48,000 --> 00:01:51,330
 So try lying on your back is a good way to start until

36
00:01:51,330 --> 00:01:54,000
 eventually you can do it sitting up.

37
00:01:54,000 --> 00:01:56,600
 You can also, of course, put your hand on the stomach,

38
00:01:56,600 --> 00:01:58,000
 which helps you in the beginning.

39
00:01:58,000 --> 00:02:00,640
 Part of it is going to be going through the stage of

40
00:02:00,640 --> 00:02:02,000
 feeling the tension.

41
00:02:02,000 --> 00:02:05,430
 Eventually the tension will come up in your shoulders, in

42
00:02:05,430 --> 00:02:09,000
 your back, and you'll be able to see what the real problem

43
00:02:09,000 --> 00:02:09,000
 is.

44
00:02:09,000 --> 00:02:11,480
 From sitting still so long, the tension builds up and

45
00:02:11,480 --> 00:02:12,000
 builds up.

46
00:02:12,000 --> 00:02:16,530
 And then as you acknowledge the tension, tens, tens, the

47
00:02:16,530 --> 00:02:19,000
 body releases and lets go.

48
00:02:19,000 --> 00:02:22,290
 And then you'll find that actually the stomach is much

49
00:02:22,290 --> 00:02:24,000
 easier to be mindful of.

50
00:02:24,000 --> 00:02:27,360
 And also as you stop forcing the stomach and start letting

51
00:02:27,360 --> 00:02:30,070
 it rise and fall as it will, it becomes easier to

52
00:02:30,070 --> 00:02:31,000
 acknowledge.

53
00:02:31,000 --> 00:02:36,000
 [silence]

