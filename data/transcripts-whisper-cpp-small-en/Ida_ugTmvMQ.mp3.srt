1
00:00:00,000 --> 00:00:03,860
 Is it appropriate for lay people to tell others with non-B

2
00:00:03,860 --> 00:00:07,120
uddhist beliefs about the teachings of the Buddha as long as

3
00:00:07,120 --> 00:00:09,000
 it isn't a personal interpretation?

4
00:00:09,000 --> 00:00:12,390
 I feel I'm in no position to have a perfect understanding

5
00:00:12,390 --> 00:00:15,380
 of the Buddha's teaching and don't feel I should be

6
00:00:15,380 --> 00:00:17,000
 teaching others informally.

7
00:00:17,000 --> 00:00:27,540
 Well, there's no problem with talking to other people about

8
00:00:27,540 --> 00:00:29,000
 the teachings of the Buddha.

9
00:00:29,000 --> 00:00:31,280
 I think there's a problem with teaching in general. You

10
00:00:31,280 --> 00:00:35,700
 shouldn't ever think of yourself as a teacher or be trying

11
00:00:35,700 --> 00:00:37,000
 to teach people.

12
00:00:37,000 --> 00:00:41,820
 And it's all the better when you know that you don't have a

13
00:00:41,820 --> 00:00:45,000
 perfect understanding because the problem, the real problem

14
00:00:45,000 --> 00:00:49,190
 would be if you thought you had perfect understanding and

15
00:00:49,190 --> 00:00:52,770
 you start, you know, maybe making up your own understanding

16
00:00:52,770 --> 00:00:56,000
 and inserting your own ideas into it.

17
00:00:56,000 --> 00:01:00,540
 The best is when you admit to yourself, as we all do, that

18
00:01:00,540 --> 00:01:05,330
 we don't have perfect understanding and therefore stick to

19
00:01:05,330 --> 00:01:08,000
 the texts and pass on what you've been told.

20
00:01:08,000 --> 00:01:12,650
 This is one important part of the Theravada, the teaching

21
00:01:12,650 --> 00:01:17,280
 of the elders. There's a great emphasis placed on the idea,

22
00:01:17,280 --> 00:01:19,000
 "Thus have I heard."

23
00:01:19,000 --> 00:01:21,810
 So all the suttas start, "Ewa mai suttang" it's based on

24
00:01:21,810 --> 00:01:24,820
 what you have heard, it's not based on what you think or it

25
00:01:24,820 --> 00:01:27,000
's not something you've made up for yourself.

26
00:01:27,000 --> 00:01:30,280
 And one should only consider that one is teaching the

27
00:01:30,280 --> 00:01:34,020
 Buddha's teaching, one one has heard it from someone who is

28
00:01:34,020 --> 00:01:35,000
 reliable.

29
00:01:35,000 --> 00:01:38,010
 So the meaning is you're not inserting your own

30
00:01:38,010 --> 00:01:41,900
 interpretation, you're trying your best to stick with the

31
00:01:41,900 --> 00:01:45,370
 interpretation of those who have come before and most

32
00:01:45,370 --> 00:01:49,000
 especially with the teachings of the Buddha himself.

33
00:01:49,000 --> 00:01:52,520
 And in that case there is no problem, letting people know

34
00:01:52,520 --> 00:01:56,170
 about the teachings, even leading people in meditation isn

35
00:01:56,170 --> 00:01:59,000
't a problem as long as you can avoid it.

36
00:01:59,000 --> 00:02:02,870
 The important thing is not knowing everything, the

37
00:02:02,870 --> 00:02:07,090
 important thing is not inserting things that you haven't

38
00:02:07,090 --> 00:02:12,000
 learned, like making stuff up or interpreting things.

39
00:02:12,000 --> 00:02:15,200
 It's okay to say, "I don't know." It's okay to say, "I

40
00:02:15,200 --> 00:02:18,610
 really don't have an answer for that, but I can get back to

41
00:02:18,610 --> 00:02:22,000
 you or maybe I can find someone who does or maybe I can't.

42
00:02:22,000 --> 00:02:24,900
 But I do know this and this and this and I have heard this

43
00:02:24,900 --> 00:02:28,000
 and this and this." And give people just that.

44
00:02:28,000 --> 00:02:31,160
 I mean if you went out and taught people basic meditation,

45
00:02:31,160 --> 00:02:34,090
 for example, rising, falling and then went on to the

46
00:02:34,090 --> 00:02:37,610
 feelings, pain, aching, soreness and thinking, thinking in

47
00:02:37,610 --> 00:02:40,690
 here, if you just went by the book you could lead someone

48
00:02:40,690 --> 00:02:43,110
 to become enlightened, you could lead someone through an

49
00:02:43,110 --> 00:02:44,000
 entire meditation course.

50
00:02:44,000 --> 00:02:47,430
 The problem is when people start including their own

51
00:02:47,430 --> 00:02:51,160
 interpretations and that's really what makes or breaks a

52
00:02:51,160 --> 00:02:55,000
 teacher, I think, or makes one a really good teacher.

53
00:02:55,000 --> 00:03:00,140
 Is whether they can overcome this kind of fear of being

54
00:03:00,140 --> 00:03:04,120
 without an answer or this ego, whatever it is, many

55
00:03:04,120 --> 00:03:08,880
 different things that lead them to add their own

56
00:03:08,880 --> 00:03:12,000
 interpretations and a lot of weird things.

57
00:03:12,000 --> 00:03:15,300
 Like I've seen teachers do a lot of weird things. People

58
00:03:15,300 --> 00:03:17,880
 who call themselves teachers who have come, for instance,

59
00:03:17,880 --> 00:03:20,110
 to our monastery and done a little bit of teaching, a

60
00:03:20,110 --> 00:03:22,980
 little bit of training with our teacher, gone off on their

61
00:03:22,980 --> 00:03:26,000
 own and just listening to some of the things they say.

62
00:03:26,000 --> 00:03:30,000
 And then their students go crazy and so on as a result.

63
00:03:30,000 --> 00:03:33,870
 Your students won't go crazy if you, you won't drive your

64
00:03:33,870 --> 00:03:37,810
 students crazy if you just give them what's in the books.

65
00:03:37,810 --> 00:03:40,950
 If you just tell them say rising, falling, if anything

66
00:03:40,950 --> 00:03:46,290
 comes up thinking, if you are pedantic or, pedantic is the

67
00:03:46,290 --> 00:03:49,380
 right word, but if you stick very much by the books to the

68
00:03:49,380 --> 00:03:51,000
 point that people get bored of you,

69
00:03:51,000 --> 00:03:53,690
 then you can say at the very least they won't go crazy

70
00:03:53,690 --> 00:03:57,010
 because you can be inspiring and say, yes, yes, follow that

71
00:03:57,010 --> 00:04:01,000
, go with it. Yes, that's, that's the real self. That's God.

72
00:04:01,000 --> 00:04:02,000
 That's so on.

73
00:04:02,000 --> 00:04:04,790
 Then you can drive them crazy for sure. You can be a cause

74
00:04:04,790 --> 00:04:07,150
 for people to go crazy. But if you just stick to the books

75
00:04:07,150 --> 00:04:09,200
 and then people say, you know, this is boring. I want

76
00:04:09,200 --> 00:04:10,000
 something exciting.

77
00:04:10,000 --> 00:04:12,970
 Then you can say, well, this is what I have. And I know

78
00:04:12,970 --> 00:04:15,560
 that I'm not going to drive them crazy. I just might drive

79
00:04:15,560 --> 00:04:19,000
 them away. That's the worst thing.

80
00:04:19,000 --> 00:04:21,970
 But the other thing you have to realize, and another

81
00:04:21,970 --> 00:04:25,260
 important part of your answer to your question is that you

82
00:04:25,260 --> 00:04:28,430
 can't expect all people in the world or even most people in

83
00:04:28,430 --> 00:04:32,000
 the world to have any appreciation for these teachings.

84
00:04:32,000 --> 00:04:35,930
 They're just not in a position. Their ideas are very much

85
00:04:35,930 --> 00:04:39,800
 more in tune with the ways of the world, this clinging and

86
00:04:39,800 --> 00:04:41,000
 partiality.

87
00:04:41,000 --> 00:04:44,730
 Many people have firm belief in the importance of partial

88
00:04:44,730 --> 00:04:48,150
ity, the importance of our likes and dislikes, the

89
00:04:48,150 --> 00:04:51,220
 importance of ego, the importance of self-confidence, the

90
00:04:51,220 --> 00:04:55,120
 importance of self-image, the importance of ambition, the

91
00:04:55,120 --> 00:04:57,850
 importance of passion, the importance of sexual intercourse

92
00:04:57,850 --> 00:05:01,000
, the importance of society, the importance of humanity.

93
00:05:01,000 --> 00:05:05,180
 Well, this just goes on and on and on and on. And there are

94
00:05:05,180 --> 00:05:10,230
 people who will fight very ardently in favor of these

95
00:05:10,230 --> 00:05:11,000
 things.

96
00:05:11,000 --> 00:05:13,800
 I would say more people will than will be able to see

97
00:05:13,800 --> 00:05:16,870
 through them and give them up and be at all interested in

98
00:05:16,870 --> 00:05:18,000
 giving them up.

99
00:05:18,000 --> 00:05:22,280
 So I encourage, always encourage, and I think it's

100
00:05:22,280 --> 00:05:27,000
 important to encourage people to share the teachings.

101
00:05:27,000 --> 00:05:30,990
 And as I said, give them basic whatever you've learned,

102
00:05:30,990 --> 00:05:35,210
 whatever you've learned from this tradition. Anyway, I don

103
00:05:35,210 --> 00:05:39,360
't want to talk, I don't have any confidence in myself to

104
00:05:39,360 --> 00:05:42,980
 talk about other traditions, but whatever you learn from

105
00:05:42,980 --> 00:05:45,000
 this tradition, for sure, spread that.

106
00:05:45,000 --> 00:05:47,620
 If you tell other people about that, I say no problem.

107
00:05:47,620 --> 00:05:54,610
 There will be no problem. At worst, they'll be turned off

108
00:05:54,610 --> 00:05:57,000
 by it or so on.

109
00:05:57,000 --> 00:06:03,100
 And the worst thing is that if they practice it, is that

110
00:06:03,100 --> 00:06:09,430
 they might be a little bit suspicious or unable to develop

111
00:06:09,430 --> 00:06:11,000
 faith in it.

112
00:06:11,000 --> 00:06:13,970
 So they either run away or they move slowly. The point

113
00:06:13,970 --> 00:06:17,180
 being, even if they stick with it, they might move slowly

114
00:06:17,180 --> 00:06:20,360
 because you're not able to explain everything, because you

115
00:06:20,360 --> 00:06:22,000
 don't have all the answers.

116
00:06:22,000 --> 00:06:26,050
 And I would say much better that you don't try to make up

117
00:06:26,050 --> 00:06:29,000
 answers. Let them be suspicious. Let them go slowly.

118
00:06:29,000 --> 00:06:31,250
 Because all that happens when someone doesn't have faith in

119
00:06:31,250 --> 00:06:33,430
 what they're doing, it's not that they go on the wrong path

120
00:06:33,430 --> 00:06:35,000
, it's that they don't go anywhere.

121
00:06:35,000 --> 00:06:38,200
 So if you can give them even a little bit of faith to walk

122
00:06:38,200 --> 00:06:42,030
 slowly, slowly, slowly, then that's actually an incredible

123
00:06:42,030 --> 00:06:44,000
 gift that you've given to them.

124
00:06:44,000 --> 00:06:48,520
 You've given them something. It's obviously not as good as

125
00:06:48,520 --> 00:06:53,660
 if you're an amazing, profound, articulate and skilled

126
00:06:53,660 --> 00:06:57,820
 super teacher who can just guide people like the Buddha did

127
00:06:57,820 --> 00:06:59,000
, for example.

128
00:06:59,000 --> 00:07:02,360
 He could guide anyone, almost anyone. He could guide a

129
00:07:02,360 --> 00:07:05,000
 great number of people to enlightenment.

130
00:07:05,000 --> 00:07:10,570
 But what I wouldn't encourage people is trying to push

131
00:07:10,570 --> 00:07:13,000
 Buddhism on anyone.

132
00:07:13,000 --> 00:07:16,270
 Though I suppose you'll find that out for yourself. When

133
00:07:16,270 --> 00:07:19,260
 you push Buddhism on anyone, people will run away from you

134
00:07:19,260 --> 00:07:20,000
 in droves.

135
00:07:20,000 --> 00:07:23,880
 Even people who might be interested in it will leave you

136
00:07:23,880 --> 00:07:28,000
 behind, will not want to have anything to do with you.

137
00:07:28,000 --> 00:07:34,810
 So, yes, it's alright. But if you're asking, "Is it alright

138
00:07:34,810 --> 00:07:38,600
 for me to go out and proselytize or push people into

139
00:07:38,600 --> 00:07:40,000
 practicing?" then no.

140
00:07:40,000 --> 00:07:42,130
 But if you're just asking, then when people ask about

141
00:07:42,130 --> 00:07:45,260
 Buddhism and say, "Hey, you know about meditation? Can you

142
00:07:45,260 --> 00:07:47,000
 teach me?" Do it by all means.

143
00:07:47,000 --> 00:07:52,020
 When I went back home after my course, I started teaching

144
00:07:52,020 --> 00:07:53,000
 people.

145
00:07:53,000 --> 00:07:58,000
 Probably far too zealous. I turned some people away.

146
00:07:58,000 --> 00:08:02,310
 I got a little bit of instruction on the stages of

147
00:08:02,310 --> 00:08:06,000
 knowledge after I had done a few courses.

148
00:08:06,000 --> 00:08:10,600
 I went and taught some people. There's one man who said, "A

149
00:08:10,600 --> 00:08:12,000
 friend of a friend."

150
00:08:12,000 --> 00:08:14,620
 The friend came to me and said, "This guy, he's interested

151
00:08:14,620 --> 00:08:17,420
 in Buddhism. I told him to come and talk to you. Is that

152
00:08:17,420 --> 00:08:18,000
 okay?"

153
00:08:18,000 --> 00:08:20,300
 I said, "Yes." So, a friend of a friend came to me and said

154
00:08:20,300 --> 00:08:22,000
, "Can you teach me about Buddhism?"

155
00:08:22,000 --> 00:08:25,000
 I said, "Sure. I'll take you through the meditation."

156
00:08:25,000 --> 00:08:28,950
 So, I led him through these stages. First walking step,

157
00:08:28,950 --> 00:08:31,630
 second walking step, all the sitting techniques that we

158
00:08:31,630 --> 00:08:33,000
 have, giving him more time, more time.

159
00:08:33,000 --> 00:08:37,680
 Oh, it was wonderful. He was a biologist and he cut up rats

160
00:08:37,680 --> 00:08:39,000
. He ended up giving it up.

161
00:08:39,000 --> 00:08:43,150
 He got to the point where he had so much pain. I said to

162
00:08:43,150 --> 00:08:45,000
 him, "You have to make a choice.

163
00:08:45,000 --> 00:08:50,550
 You have to either be a biologist or be a Buddhist medit

164
00:08:50,550 --> 00:08:52,000
ator because you can't do both.

165
00:08:52,000 --> 00:08:57,120
 I mean, you can't cut up rats anyway." He gave it up. He

166
00:08:57,120 --> 00:08:59,000
 was in his fourth year in university.

167
00:08:59,000 --> 00:09:03,590
 He was ready to get his degree and he gave it up. He works

168
00:09:03,590 --> 00:09:07,000
 for the government now, I think, in Canada.

169
00:09:07,000 --> 00:09:15,540
 He was a really nice guy. There was another man as well who

170
00:09:15,540 --> 00:09:16,000
 I kind of kept in touch with.

171
00:09:16,000 --> 00:09:19,090
 I wasn't able to give them faith. I think neither of them

172
00:09:19,090 --> 00:09:23,100
 has any faith in me as a teacher anymore because I wasn't

173
00:09:23,100 --> 00:09:24,000
 able to...

174
00:09:24,000 --> 00:09:28,290
 Obviously, I wasn't the kind of person who should inspire

175
00:09:28,290 --> 00:09:33,000
 faith. Not to say that I am now either.

176
00:09:33,000 --> 00:09:36,620
 In the end, I don't think... Both of them moved on and have

177
00:09:36,620 --> 00:09:41,000
 taken other teachers or have kind of lost touch with them.

178
00:09:41,000 --> 00:09:43,260
 They're not at all interested in what I do. I don't know if

179
00:09:43,260 --> 00:09:46,000
 they would be now, but I feel like I did something good.

180
00:09:46,000 --> 00:09:48,750
 I'm not their teacher and they don't think of me as some

181
00:09:48,750 --> 00:09:52,260
 great teacher and it's none of my business, but I gave them

182
00:09:52,260 --> 00:09:53,000
 a gift.

183
00:09:53,000 --> 00:09:56,710
 That's how I feel. Good thing I did. Based on that

184
00:09:56,710 --> 00:10:00,230
 experience, I think anyone could do that because it wasn't

185
00:10:00,230 --> 00:10:01,000
 anything special that I had.

186
00:10:01,000 --> 00:10:04,700
 It was just basic understanding of meditation. If you haven

187
00:10:04,700 --> 00:10:09,450
't done, say, a meditation course, I wouldn't recommend that

188
00:10:09,450 --> 00:10:13,000
 leading someone through a course.

189
00:10:13,000 --> 00:10:16,850
 But as I said, giving them the basics is certainly not

190
00:10:16,850 --> 00:10:20,000
 harmful as long as you stick to the books.

191
00:10:22,000 --> 00:10:23,000
 Anything?

