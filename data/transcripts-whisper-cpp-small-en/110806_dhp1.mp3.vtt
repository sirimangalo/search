WEBVTT

00:00:00.000 --> 00:00:05.620
 Hello, today I will be beginning a new series of videos on

00:00:05.620 --> 00:00:08.960
 a text called the Dhammapada.

00:00:08.960 --> 00:00:15.450
 The Dhammapada is a group of 423 verses that are said to

00:00:15.450 --> 00:00:18.220
 have been taught by the Lord Buddha

00:00:18.220 --> 00:00:21.240
 at various times during his life.

00:00:21.240 --> 00:00:26.480
 So it's generally considered to be a fairly good summary of

00:00:26.480 --> 00:00:29.200
 the Buddha's teachings and

00:00:29.200 --> 00:00:33.790
 so I thought it would be quite a good subject for videos in

00:00:33.790 --> 00:00:36.280
 order to spread the Buddha's

00:00:36.280 --> 00:00:41.800
 teaching and help more people to realize the benefits of

00:00:41.800 --> 00:00:43.520
 the teaching.

00:00:43.520 --> 00:00:47.470
 So what I'm going to do is read you here, I've got the Pali

00:00:47.470 --> 00:00:49.160
 verse, I'm going to read

00:00:49.160 --> 00:00:53.990
 it in Pali one verse at a time and after the verse I will

00:00:53.990 --> 00:00:56.360
 translate it piece by piece into

00:00:56.360 --> 00:00:59.880
 English, give you an understanding of what it means, then I

00:00:59.880 --> 00:01:01.400
 will tell the story which

00:01:01.400 --> 00:01:06.300
 goes with the verse because each verse has a story that was

00:01:06.300 --> 00:01:08.580
 passed down that gives the

00:01:08.580 --> 00:01:12.690
 occasion and the circumstances in which the Buddha gave

00:01:12.690 --> 00:01:14.080
 this teaching.

00:01:14.080 --> 00:01:18.410
 And after I give this sort of a short summary of the story

00:01:18.410 --> 00:01:20.760
 then I'll explain how I think

00:01:20.760 --> 00:01:24.650
 we should understand this verse and how it can relate to

00:01:24.650 --> 00:01:27.000
 our lives and to our practice.

00:01:27.000 --> 00:01:31.630
 So one note about the stories is that there's two ways we

00:01:31.630 --> 00:01:34.560
 can go, some people will pay more

00:01:34.560 --> 00:01:37.880
 attention to the stories than the verses and remember the D

00:01:37.880 --> 00:01:40.560
hammapada as a series of stories.

00:01:40.560 --> 00:01:43.220
 Another group of people will never have heard or have read

00:01:43.220 --> 00:01:45.040
 the stories and will therefore

00:01:45.040 --> 00:01:48.680
 think of the Dhammapada as a group of verses.

00:01:48.680 --> 00:01:53.280
 And I think I'm going to try to hit a happy medium where we

00:01:53.280 --> 00:01:55.160
 don't go to either extreme

00:01:55.160 --> 00:01:59.470
 because the verses have a great benefit to them in

00:01:59.470 --> 00:02:02.600
 providing a good example of how the

00:02:02.600 --> 00:02:06.400
 teachings can be applied and of how they apply to our lives

00:02:06.400 --> 00:02:06.680
.

00:02:06.680 --> 00:02:10.040
 They provide us with encouragement when we hear about

00:02:10.040 --> 00:02:12.200
 people practicing good things and

00:02:12.200 --> 00:02:17.120
 help us to adjust ourselves when we hear about the results

00:02:17.120 --> 00:02:19.360
 of bad things and so on.

00:02:19.360 --> 00:02:23.760
 They help put it into context to some extent.

00:02:23.760 --> 00:02:27.150
 But what I'd like to say is that we should not let the

00:02:27.150 --> 00:02:28.800
 stories dictate the context of

00:02:28.800 --> 00:02:33.240
 the verse because even though they do provide some context,

00:02:33.240 --> 00:02:35.440
 it's clear that the Buddha was

00:02:35.440 --> 00:02:39.480
 not meaning to apply the verse only to the context in which

00:02:39.480 --> 00:02:40.660
 it was taught.

00:02:40.660 --> 00:02:44.240
 The teaching came as a result of a certain event or

00:02:44.240 --> 00:02:46.800
 occurrence in the Buddha's time is

00:02:46.800 --> 00:02:52.590
 true, but the teaching of course is much more broad and

00:02:52.590 --> 00:02:55.600
 deep often than the circumstance

00:02:55.600 --> 00:02:57.300
 allows for.

00:02:57.300 --> 00:03:00.530
 So today I'm going to give the first verse and this is of

00:03:00.530 --> 00:03:02.320
 the Yamakavaga which is the

00:03:02.320 --> 00:03:04.320
 first group of verses.

00:03:04.320 --> 00:03:11.580
 The Dhammapada 423 verses is separated into different

00:03:11.580 --> 00:03:12.680
 sections.

00:03:12.680 --> 00:03:16.280
 So this is the first verse of the first section.

00:03:16.280 --> 00:03:18.200
 And the verse goes in Pali.

00:03:18.200 --> 00:03:23.400
 Manopubangama dhamma mano-setha manomaya.

00:03:23.400 --> 00:03:31.300
 Manasatye padutena manasativa karotiva tatunangduka manvedi

00:03:31.300 --> 00:03:33.200
 jakungvahatopadam.

00:03:33.200 --> 00:03:36.720
 This is the Pali.

00:03:36.720 --> 00:03:43.580
 The translation, manopubangama dhamma, all dhammas, all d

00:03:43.580 --> 00:03:47.240
hammas are preceded, manopubanga

00:03:47.240 --> 00:03:50.440
 means they're preceded by the mind.

00:03:50.440 --> 00:03:56.470
 All things, all reality, everything is preceded by the mind

00:03:56.470 --> 00:03:56.680
.

00:03:56.680 --> 00:04:01.390
 Manosetha manomaya, they are governed by the mind and made

00:04:01.390 --> 00:04:02.640
 by the mind.

00:04:02.640 --> 00:04:05.600
 Setha mano-setha, they are governed by the mind.

00:04:05.600 --> 00:04:08.880
 Manomaya, they are made or formed by the mind.

00:04:08.880 --> 00:04:09.880
 All things.

00:04:09.880 --> 00:04:12.800
 This is the Buddhist words.

00:04:12.800 --> 00:04:20.790
 Manasatye padutena, if with an impure mind, vasa-ti-vah kar

00:04:20.790 --> 00:04:23.840
otiva, one acts or speaks.

00:04:23.840 --> 00:04:29.060
 If one acts or speaks with an impure mind, tatunangduka man

00:04:29.060 --> 00:04:31.640
vedi jakungvahatopadam.

00:04:31.640 --> 00:04:35.600
 Everything follows therefrom, just as the wheel of the kart

00:04:35.600 --> 00:04:37.360
 follows the ox that pulls

00:04:37.360 --> 00:04:40.040
 it.

00:04:40.040 --> 00:04:44.450
 So if an ox is pulling a kart, the wheel has to follow the

00:04:44.450 --> 00:04:45.040
 ox.

00:04:45.040 --> 00:04:46.400
 There's no way that it can't.

00:04:46.400 --> 00:04:48.760
 It can't stop.

00:04:48.760 --> 00:04:51.200
 It can't change its course.

00:04:51.200 --> 00:04:53.080
 It will have to follow after the foot.

00:04:53.080 --> 00:04:56.660
 So on the path you will see the footprint of the ox and you

00:04:56.660 --> 00:04:58.360
 will see the footprint of

00:04:58.360 --> 00:05:00.840
 the kart following it always.

00:05:00.840 --> 00:05:03.240
 As long as the ox is pulling the kart.

00:05:03.240 --> 00:05:07.050
 In the same way, when we act or speak, if our heart is imp

00:05:07.050 --> 00:05:09.320
ure, suffering will follow,

00:05:09.320 --> 00:05:12.200
 just as the kart follows the ox.

00:05:12.200 --> 00:05:14.840
 That's the saying.

00:05:14.840 --> 00:05:20.850
 So this verse was given in relation to the venerable elder

00:05:20.850 --> 00:05:23.040
 monk, cakubala.

00:05:23.040 --> 00:05:26.360
 Cakubala, cakul means "I".

00:05:26.360 --> 00:05:28.840
 So bala and bala means "guardian".

00:05:28.840 --> 00:05:33.440
 So his name means "one who guards his eye".

00:05:33.440 --> 00:05:34.640
 This is a name he was given.

00:05:34.640 --> 00:05:37.160
 His original name was actually bala.

00:05:37.160 --> 00:05:41.130
 But the story goes that he became ordained under the Buddha

00:05:41.130 --> 00:05:44.040
 a little bit later in life.

00:05:44.040 --> 00:05:46.760
 So he didn't spend so much time studying.

00:05:46.760 --> 00:05:49.170
 He stayed five years with the Buddha to do the basic

00:05:49.170 --> 00:05:50.280
 training as a monk.

00:05:50.280 --> 00:05:52.970
 But after that he asked permission to go off and practice

00:05:52.970 --> 00:05:54.440
 in the forest with some of his

00:05:54.440 --> 00:05:57.360
 fellow monks.

00:05:57.360 --> 00:06:03.490
 He spent three months of the rain season in the forest and

00:06:03.490 --> 00:06:06.560
 he made a determination not

00:06:06.560 --> 00:06:09.280
 to lie down for three months.

00:06:09.280 --> 00:06:13.780
 So he undertook this practice to only do walking, standing

00:06:13.780 --> 00:06:15.000
 and sitting.

00:06:15.000 --> 00:06:17.330
 And so he would do walking meditation, he would do sitting

00:06:17.330 --> 00:06:18.880
 meditation, he would do standing

00:06:18.880 --> 00:06:21.180
 meditation but he would never lie down, not for three

00:06:21.180 --> 00:06:22.320
 months, no sleeping.

00:06:22.320 --> 00:06:26.750
 So this means unless he would sleep sitting up or not off

00:06:26.750 --> 00:06:28.960
 sitting up by accident.

00:06:28.960 --> 00:06:35.080
 This is a practice that monks will undertake when they've

00:06:35.080 --> 00:06:38.240
 developed and after they've

00:06:38.240 --> 00:06:41.480
 progressed in their meditation or become proficient and

00:06:41.480 --> 00:06:43.360
 confident in their practice.

00:06:43.360 --> 00:06:46.520
 So he did this for three months and during this time he

00:06:46.520 --> 00:06:48.600
 developed a sickness, a disease

00:06:48.600 --> 00:06:51.680
 of the eye as a sickness in his eyes.

00:06:51.680 --> 00:06:54.610
 And this doctor told him that he had to take this medicine

00:06:54.610 --> 00:06:56.000
 and he said he would have to

00:06:56.000 --> 00:07:00.340
 lie down to take this medicine, something like it was put

00:07:00.340 --> 00:07:02.400
 in his nose or something,

00:07:02.400 --> 00:07:08.160
 some ancient diabetic cure.

00:07:08.160 --> 00:07:11.410
 And now Cakupala, he didn't say yes, he didn't say no, he

00:07:11.410 --> 00:07:12.560
 took the medicine and he went home

00:07:12.560 --> 00:07:15.280
 and he went back to the monastery and he thought to himself

00:07:15.280 --> 00:07:16.480
, "What should I do?"

00:07:16.480 --> 00:07:20.150
 He said, "Well, if I've given this vow that I'm not going

00:07:20.150 --> 00:07:23.920
 to lie down and I really want

00:07:23.920 --> 00:07:27.450
 to carry out my vow and to really exert myself in the

00:07:27.450 --> 00:07:28.520
 practice."

00:07:28.520 --> 00:07:30.950
 And so as a result he sat up and he kind of took the

00:07:30.950 --> 00:07:32.800
 medicine sitting up but he didn't

00:07:32.800 --> 00:07:34.440
 lie down.

00:07:34.440 --> 00:07:38.080
 As a result his sickness didn't get better, it in fact got

00:07:38.080 --> 00:07:38.760
 worse.

00:07:38.760 --> 00:07:41.530
 And he went back on alms round and the doctor came up to

00:07:41.530 --> 00:07:43.200
 him and asked him, "How is the

00:07:43.200 --> 00:07:44.200
 sickness getting better?"

00:07:44.200 --> 00:07:48.280
 And he said, "Oh, the wind is still hurting my eyes."

00:07:48.280 --> 00:07:50.760
 And he said, "Well, did you lie down to take the medicine?"

00:07:50.760 --> 00:07:53.280
 And he didn't say anything.

00:07:53.280 --> 00:07:56.740
 He just stood there and the doctor said, "Sir, you have to

00:07:56.740 --> 00:07:58.200
 lie down to take it."

00:07:58.200 --> 00:08:00.520
 And he said, "Well, thank you."

00:08:00.520 --> 00:08:01.520
 He went away.

00:08:01.520 --> 00:08:03.800
 And the doctor started to get suspicious and so he went

00:08:03.800 --> 00:08:05.320
 back to the monastery, followed

00:08:05.320 --> 00:08:10.340
 after the elder and went and looked at his dwelling and saw

00:08:10.340 --> 00:08:12.800
 that there was no bedding.

00:08:12.800 --> 00:08:14.680
 And he said, "Venerable sir, where is your bed?"

00:08:14.680 --> 00:08:16.720
 And he said, and he didn't say anything.

00:08:16.720 --> 00:08:19.280
 And he said, "Venerable sir, you can't do this.

00:08:19.280 --> 00:08:21.640
 You're going to go blind if you do this.

00:08:21.640 --> 00:08:24.680
 You have to take care of your eyes.

00:08:24.680 --> 00:08:26.440
 You need them if you want to be a monk, you're going to

00:08:26.440 --> 00:08:27.040
 need..."

00:08:27.040 --> 00:08:28.920
 He gave him a little lecture.

00:08:28.920 --> 00:08:32.210
 And the monk said, "Thank you, but I will know what to do

00:08:32.210 --> 00:08:33.080
 by myself.

00:08:33.080 --> 00:08:34.680
 I'll figure out what to do by myself."

00:08:34.680 --> 00:08:40.300
 And so the doctor said, "Fine, but don't tell anyone that I

00:08:40.300 --> 00:08:42.680
 was the one who cured you or

00:08:42.680 --> 00:08:44.560
 who gave you the medicine.

00:08:44.560 --> 00:08:48.480
 Don't tell them that I didn't want to have anything to do

00:08:48.480 --> 00:08:49.520
 with you."

00:08:49.520 --> 00:08:50.760
 And he said, "Yes, thank you."

00:08:50.760 --> 00:08:52.000
 And so the doctor went away.

00:08:52.000 --> 00:08:55.180
 So Chakupala, or Paala, he sat down and he thought, and he

00:08:55.180 --> 00:08:56.840
 said, "Vana, what do I do?

00:08:56.840 --> 00:08:59.920
 I can either take care of my eyes or I can take care of my

00:08:59.920 --> 00:09:00.760
 practice.

00:09:00.760 --> 00:09:05.020
 I can either guard the physical body or I can guard the

00:09:05.020 --> 00:09:05.480
 truth."

00:09:05.480 --> 00:09:10.430
 And he said to himself, "If I don't take this medicine, if

00:09:10.430 --> 00:09:12.440
 I don't take it properly,

00:09:12.440 --> 00:09:14.360
 then I might lose my eyes.

00:09:14.360 --> 00:09:16.160
 My eyes will be ruined."

00:09:16.160 --> 00:09:21.490
 And he said, "But these eyes, these ears, this body, this

00:09:21.490 --> 00:09:24.160
 self, this thing that I cling

00:09:24.160 --> 00:09:26.800
 to, eventually it will all be ruined.

00:09:26.800 --> 00:09:28.040
 It will all fall apart."

00:09:28.040 --> 00:09:32.190
 He said, "Why should I base my life on the well-being of

00:09:32.190 --> 00:09:34.200
 something physical?"

00:09:34.200 --> 00:09:37.220
 And he said, "It's much more important that I should guard

00:09:37.220 --> 00:09:38.040
 the Dhamma.

00:09:38.040 --> 00:09:40.870
 I should guard the truth and that I should guard the

00:09:40.870 --> 00:09:42.060
 physical body."

00:09:42.060 --> 00:09:44.360
 And so he didn't lie down and he didn't take the medicine

00:09:44.360 --> 00:09:46.560
 and he continued on with his

00:09:46.560 --> 00:09:47.560
 practice.

00:09:47.560 --> 00:09:50.840
 As a result of this, two things happened.

00:09:50.840 --> 00:09:55.440
 First is he lost his eyes and the second is that he gained

00:09:55.440 --> 00:09:56.500
 his eyes.

00:09:56.500 --> 00:09:59.780
 And this is how the text goes, that as he was doing walking

00:09:59.780 --> 00:10:01.200
 meditation, at the same

00:10:01.200 --> 00:10:05.320
 moment his eyes got worse and worse and worse, and finally

00:10:05.320 --> 00:10:07.680
 they deteriorated or something

00:10:07.680 --> 00:10:13.960
 changed, something switched off, his eyes suddenly became

00:10:13.960 --> 00:10:17.280
 useless, his physical eyes.

00:10:17.280 --> 00:10:20.160
 And at that same moment, as this was happening and as he

00:10:20.160 --> 00:10:22.120
 was watching it and worrying about

00:10:22.120 --> 00:10:25.050
 it and looking at his worrying and looking at his clinging

00:10:25.050 --> 00:10:26.440
 to the body and letting it

00:10:26.440 --> 00:10:31.290
 go and seeing the suffering inherent in his clinging mind,

00:10:31.290 --> 00:10:33.600
 wanting and liking the fact

00:10:33.600 --> 00:10:38.520
 that he can see in his eyes and the self and the ego and so

00:10:38.520 --> 00:10:41.080
 on, seeing that this is the

00:10:41.080 --> 00:10:42.080
 suffering.

00:10:42.080 --> 00:10:45.450
 He saw the portable truth, he saw suffering and the cause

00:10:45.450 --> 00:10:46.480
 of suffering.

00:10:46.480 --> 00:10:49.900
 And when he saw that he let go and realized the cessation

00:10:49.900 --> 00:10:51.000
 of suffering.

00:10:51.000 --> 00:10:55.370
 So at the same moment he saw the truth and he lost his

00:10:55.370 --> 00:10:58.600
 vision forever, forever, his physical

00:10:58.600 --> 00:11:00.440
 vision.

00:11:00.440 --> 00:11:05.000
 He became an Arahant at the same time.

00:11:05.000 --> 00:11:07.520
 So at the end of the rain season after his practice and

00:11:07.520 --> 00:11:08.960
 after he helped out the rest

00:11:08.960 --> 00:11:12.660
 of the other monks to become and practice correctly as well

00:11:12.660 --> 00:11:14.600
, he made his way back to

00:11:14.600 --> 00:11:18.730
 the Buddha and spent some time back at the monastery where

00:11:18.730 --> 00:11:20.680
 the Buddha was staying, I

00:11:20.680 --> 00:11:26.450
 believe in Sawati in India and in Jethavana, the great

00:11:26.450 --> 00:11:29.360
 monastery of the Buddha.

00:11:29.360 --> 00:11:32.100
 And while he was there, of course, there were other monks

00:11:32.100 --> 00:11:33.560
 taking care of him and men and

00:11:33.560 --> 00:11:38.250
 monks who he would teach and they would look after him

00:11:38.250 --> 00:11:41.040
 physically and many visiting monks.

00:11:41.040 --> 00:11:44.440
 And his name got around as a fairly proficient teacher.

00:11:44.440 --> 00:11:48.640
 People thought that the rumor went that he was enlightened.

00:11:48.640 --> 00:11:53.250
 And so monks would come to visit him one day, one night, in

00:11:53.250 --> 00:11:55.320
 the middle of the night he was

00:11:55.320 --> 00:11:58.840
 doing walking meditation and he would do the walking

00:11:58.840 --> 00:12:01.640
 meditation outside and it had rained.

00:12:01.640 --> 00:12:04.660
 So it had rained heavily all night and then in the early

00:12:04.660 --> 00:12:06.600
 morning, 3 AM or so on, he got

00:12:06.600 --> 00:12:09.040
 up to do walking meditation.

00:12:09.040 --> 00:12:14.050
 Now at those times in India and even now here in Sri Lanka,

00:12:14.050 --> 00:12:17.160
 in Thailand, they have something

00:12:17.160 --> 00:12:19.860
 like a termite or an ant that is really the most useless

00:12:19.860 --> 00:12:21.040
 animal in the world.

00:12:21.040 --> 00:12:24.800
 I don't know how they managed to survive because they die

00:12:24.800 --> 00:12:25.760
 in droves.

00:12:25.760 --> 00:12:29.560
 They fly around and they lose their wings and then they

00:12:29.560 --> 00:12:31.920
 just lie there and die, it seems

00:12:31.920 --> 00:12:33.480
 like.

00:12:33.480 --> 00:12:38.660
 And these insects were coming in the night, they come when

00:12:38.660 --> 00:12:41.400
 it rains because I guess their

00:12:41.400 --> 00:12:43.060
 layers get flooded.

00:12:43.060 --> 00:12:45.840
 And so they were covering this walking path.

00:12:45.840 --> 00:12:48.870
 And Chukupala came out in the morning after the rains on

00:12:48.870 --> 00:12:50.520
 the 3 AM or so on and started

00:12:50.520 --> 00:12:52.440
 doing walking meditation.

00:12:52.440 --> 00:12:55.160
 And as he was doing walking meditation, many of these

00:12:55.160 --> 00:12:57.000
 insects died as he was walking back

00:12:57.000 --> 00:12:58.000
 and forth.

00:12:58.000 --> 00:13:02.240
 He had no idea that they were there and they died.

00:13:02.240 --> 00:13:06.960
 Many many of these insects were squashed.

00:13:06.960 --> 00:13:09.960
 So in the morning, these monks came to see him, to meet him

00:13:09.960 --> 00:13:11.320
 and said, "Where is the

00:13:11.320 --> 00:13:12.320
 Venerable Chukupala?"

00:13:12.320 --> 00:13:15.600
 This is the name he was given.

00:13:15.600 --> 00:13:17.450
 And the monks who looked after him said, "That's his monast

00:13:17.450 --> 00:13:19.120
, that's his kuti over there."

00:13:19.120 --> 00:13:22.320
 And so they went over to look and they saw this walking

00:13:22.320 --> 00:13:24.280
 path that was covered with his

00:13:24.280 --> 00:13:30.790
 footprints stepping on these termites or these ants or

00:13:30.790 --> 00:13:33.400
 whatever they are.

00:13:33.400 --> 00:13:36.680
 And these monks were terribly offended and they thought, "

00:13:36.680 --> 00:13:37.640
This isn't it.

00:13:37.640 --> 00:13:41.350
 How can this be an enlightened monk who's here engaging in

00:13:41.350 --> 00:13:43.160
 wanton slaughter of these

00:13:43.160 --> 00:13:46.560
 innocent creatures?"

00:13:46.560 --> 00:13:49.210
 And so they were very offended and they went to see the

00:13:49.210 --> 00:13:51.320
 Buddha and they said, "This is

00:13:51.320 --> 00:13:54.760
 right, this monk should be taught how to practice correctly

00:13:54.760 --> 00:13:55.000
.

00:13:55.000 --> 00:14:00.910
 How can he be an elder and still not know how wrong it is

00:14:00.910 --> 00:14:02.320
 to kill?"

00:14:02.320 --> 00:14:06.030
 And this is where the Buddha gave this teaching which

00:14:06.030 --> 00:14:08.880
 actually becomes a part of a very famous

00:14:08.880 --> 00:14:12.570
 or the very important part of the Buddha's teaching, the

00:14:12.570 --> 00:14:14.520
 Buddha's teaching of karma,

00:14:14.520 --> 00:14:18.920
 which actually denies the efficacy of karma.

00:14:18.920 --> 00:14:20.760
 So people say the Buddha taught the theory of karma.

00:14:20.760 --> 00:14:24.040
 In fact, you can say the Buddha taught against the theory

00:14:24.040 --> 00:14:26.080
 of karma because the Buddha said,

00:14:26.080 --> 00:14:29.560
 "My son, Chukupala, is not guilty of anything.

00:14:29.560 --> 00:14:30.800
 He's innocent."

00:14:30.800 --> 00:14:34.600
 And then he said, "Bhanopu, Bhangamadamma, the mind preced

00:14:34.600 --> 00:14:35.880
es all dhammas."

00:14:35.880 --> 00:14:40.000
 The mind is what leads to suffering.

00:14:40.000 --> 00:14:43.520
 If you act or speak with an impure mind, that's where

00:14:43.520 --> 00:14:44.960
 suffering falls.

00:14:44.960 --> 00:14:47.820
 So the Buddha took this verse, this verse was given in a

00:14:47.820 --> 00:14:49.040
 negative context.

00:14:49.040 --> 00:14:51.890
 The point was not to say that suffering is going to, in the

00:14:51.890 --> 00:14:53.400
 positive sense of suffering,

00:14:53.400 --> 00:14:54.400
 will lead to this.

00:14:54.400 --> 00:14:57.330
 He was saying, "If your mind doesn't have those things in

00:14:57.330 --> 00:14:58.600
 it, then it can't lead to

00:14:58.600 --> 00:14:59.760
 suffering."

00:14:59.760 --> 00:15:05.190
 So it's the fact that karma cannot lead to unpleasant

00:15:05.190 --> 00:15:06.480
 results.

00:15:06.480 --> 00:15:09.990
 And this is an incredibly profound statement, I think,

00:15:09.990 --> 00:15:12.000
 because it's not something that we

00:15:12.000 --> 00:15:15.600
 would think of ourselves.

00:15:15.600 --> 00:15:18.040
 If someone gets hurt because of something we do, then we

00:15:18.040 --> 00:15:19.560
 think of ourselves as guilty.

00:15:19.560 --> 00:15:22.470
 We feel bad, and if we don't feel bad, maybe the first

00:15:22.470 --> 00:15:24.280
 person is angry at us, and they

00:15:24.280 --> 00:15:27.280
 can get angry at us whether we meant to do it or not.

00:15:27.280 --> 00:15:31.870
 But the point that the Buddha is making here really shows

00:15:31.870 --> 00:15:34.480
 the emphasis on the meditation

00:15:34.480 --> 00:15:35.480
 practice.

00:15:35.480 --> 00:15:40.850
 This teaching is really a practice of meditation and of

00:15:40.850 --> 00:15:42.640
 contemplation.

00:15:42.640 --> 00:15:43.920
 Why?

00:15:43.920 --> 00:15:48.390
 Because we don't understand things in terms of beings, in

00:15:48.390 --> 00:15:50.920
 terms of concepts, things that

00:15:50.920 --> 00:15:51.920
 we think of.

00:15:51.920 --> 00:15:53.800
 We think of that person, and I hurt them, and so on.

00:15:53.800 --> 00:15:57.090
 We think of it in terms of actual reality and the

00:15:57.090 --> 00:15:58.200
 experience.

00:15:58.200 --> 00:16:01.080
 So maybe the person does get angry at me for something that

00:16:01.080 --> 00:16:01.560
 I did.

00:16:01.560 --> 00:16:03.180
 Maybe these monks got angry at Chakupala, and therefore

00:16:03.180 --> 00:16:05.160
 they think, "Oh, that was bad

00:16:05.160 --> 00:16:08.160
 karma because it made people angry at him."

00:16:08.160 --> 00:16:11.080
 But at the same time, you can say, "Well, Chakupala doesn't

00:16:11.080 --> 00:16:12.480
 face him at all, really.

00:16:12.480 --> 00:16:15.880
 He had no bad intentions towards the insects.

00:16:15.880 --> 00:16:18.440
 He had no bad intentions towards these monks.

00:16:18.440 --> 00:16:22.800
 And if they get angry at him, it's really water off a duck

00:16:22.800 --> 00:16:24.840
's back, or it's like the

00:16:24.840 --> 00:16:30.600
 Buddha said, a mustard seed on a needle, or water off of a

00:16:30.600 --> 00:16:32.080
 lotus leaf.

00:16:32.080 --> 00:16:33.280
 It doesn't stick.

00:16:33.280 --> 00:16:36.350
 So because his mind is pure, because his mind doesn't have

00:16:36.350 --> 00:16:37.920
 any of his clinging, any of his

00:16:37.920 --> 00:16:38.920
 anger.

00:16:38.920 --> 00:16:41.700
 Whereas, on the other hand, if he did have anger, if he did

00:16:41.700 --> 00:16:43.200
 want to have bad intentions,

00:16:43.200 --> 00:16:46.210
 and if he did intend to kill these insects, that is what

00:16:46.210 --> 00:16:48.000
 would lead him to suffering.

00:16:48.000 --> 00:16:49.960
 And that's what makes it unethical.

00:16:49.960 --> 00:16:53.870
 An act is not unethical, as I've said before, simply

00:16:53.870 --> 00:16:56.400
 because it fits into a category, like

00:16:56.400 --> 00:16:58.520
 killing is unethical or so on.

00:16:58.520 --> 00:17:01.360
 Killing is only unethical because of the mind states that

00:17:01.360 --> 00:17:02.600
 are required to kill.

00:17:02.600 --> 00:17:05.720
 In order to intentionally kill something, you have to give

00:17:05.720 --> 00:17:07.360
 rise to a harmful and actually

00:17:07.360 --> 00:17:11.570
 a perverted mind state, one that doesn't want to die

00:17:11.570 --> 00:17:14.280
 oneself and yet wants to cause harm

00:17:14.280 --> 00:17:16.520
 to others.

00:17:16.520 --> 00:17:20.760
 So the mind is of ultimate importance, and this shows, as I

00:17:20.760 --> 00:17:22.720
 said, shows the emphasis

00:17:22.720 --> 00:17:25.750
 on meditation, because it's only through meditation that we

00:17:25.750 --> 00:17:26.880
 can affect the mind.

00:17:26.880 --> 00:17:30.720
 And the mind is what is affected through meditation.

00:17:30.720 --> 00:17:34.860
 As we practice meditation, we see the clinging, we see the

00:17:34.860 --> 00:17:37.240
 craving, we see the stress that

00:17:37.240 --> 00:17:43.490
 is caused by the impure mind, and we come to differentiate,

00:17:43.490 --> 00:17:45.800
 and we come to affect a

00:17:45.800 --> 00:17:48.320
 change on our minds.

00:17:48.320 --> 00:17:50.510
 When we see things as they are, when we're doing walking

00:17:50.510 --> 00:17:52.120
 meditation, or we're doing sitting

00:17:52.120 --> 00:17:57.080
 meditation, we learn to experience life in an interactive

00:17:57.080 --> 00:17:59.400
 rather than a reactive way,

00:17:59.400 --> 00:18:02.090
 so that when we see and we hear and we smell and we

00:18:02.090 --> 00:18:04.720
 experience things, we're able to experience

00:18:04.720 --> 00:18:06.040
 them as an experience.

00:18:06.040 --> 00:18:08.970
 So someone yells at us, we experience it as a sound,

00:18:08.970 --> 00:18:10.680
 someone hits us, we experience it

00:18:10.680 --> 00:18:11.960
 as a feeling.

00:18:11.960 --> 00:18:15.220
 We don't think of the person, we don't cling, we don't hold

00:18:15.220 --> 00:18:15.560
 on.

00:18:15.560 --> 00:18:18.780
 Because we've seen the suffering, and this is what the

00:18:18.780 --> 00:18:20.640
 Buddha was referring to, that

00:18:20.640 --> 00:18:25.240
 the mind that clings, the mind that is impure, the mind

00:18:25.240 --> 00:18:27.800
 that has stress in it, this is what

00:18:27.800 --> 00:18:29.300
 leads to suffering.

00:18:29.300 --> 00:18:32.440
 Suffering only comes from the mind.

00:18:32.440 --> 00:18:38.250
 All of the things that we create, all of the things that we

00:18:38.250 --> 00:18:40.860
 do, only have an influence

00:18:40.860 --> 00:18:44.120
 on our minds if we cling to them, if we have some

00:18:44.120 --> 00:18:46.520
 attachment in the mind at that moment.

00:18:46.520 --> 00:18:50.320
 And this is a warning from the Buddha, that anything that

00:18:50.320 --> 00:18:52.280
 we do with an impure mind will

00:18:52.280 --> 00:18:55.670
 have this influence on our lives, an influence on our minds

00:18:55.670 --> 00:18:57.600
 that will lead to greater stress

00:18:57.600 --> 00:19:01.600
 in our minds, greater clinging, greater suffering, just as

00:19:01.600 --> 00:19:03.800
 the cart follows through the foot

00:19:03.800 --> 00:19:04.800
 of the ox.

00:19:04.800 --> 00:19:07.740
 So this is the meaning of the verse, and the Buddha said, "

00:19:07.740 --> 00:19:09.280
In fact, all things come from

00:19:09.280 --> 00:19:10.520
 the mind.

00:19:10.520 --> 00:19:13.160
 If you act with an impure mind, it will lead you to

00:19:13.160 --> 00:19:14.000
 suffering.

00:19:14.000 --> 00:19:17.300
 If you want to know where suffering comes from, this is

00:19:17.300 --> 00:19:19.080
 where it and all other things

00:19:19.080 --> 00:19:21.040
 come from, from the mind."

00:19:21.040 --> 00:19:24.380
 And this is the first verse of the Dhamma Pada.

00:19:24.380 --> 00:19:27.080
 So I'd like to thank you for tuning in, and I hope that we

00:19:27.080 --> 00:19:28.680
 will have many more of these,

00:19:28.680 --> 00:19:35.470
 and I'll be able to get through all 423 verses before my

00:19:35.470 --> 00:19:37.000
 time is up.

00:19:37.000 --> 00:19:38.320
 So thank you for tuning in again.

00:19:38.320 --> 00:19:39.320
 I wish you all the best.

00:19:39.320 --> 00:19:40.320
 Thank you.

