1
00:00:00,000 --> 00:00:04,320
 Good evening. Good evening, everyone. I don't know when

2
00:00:04,320 --> 00:00:05,680
 exactly it starts recording.

3
00:00:05,680 --> 00:00:13,760
 Broadcasting live, January 6th, 2016.

4
00:00:13,760 --> 00:00:23,590
 Back on with our weekly meditations at McMaster, and we're

5
00:00:23,590 --> 00:00:25,280
 going to keep trying with

6
00:00:25,280 --> 00:00:30,560
 three times a week, actually. And next week we'll have our

7
00:00:30,560 --> 00:00:32,000
 five-minute meditation lesson.

8
00:00:32,000 --> 00:00:34,240
 I've got someone helping me out with that, hopefully.

9
00:00:34,240 --> 00:00:41,840
 I have to test it. I haven't done a five-minute meditation.

10
00:00:41,840 --> 00:00:43,600
 I've never taught meditation in five

11
00:00:43,600 --> 00:00:46,850
 minutes, not formally. So I'll have to figure out what's my

12
00:00:46,850 --> 00:00:49,920
 five minute... what's my elevator pitch,

13
00:00:49,920 --> 00:00:50,800
 is what they call it.

14
00:00:51,800 --> 00:00:55,560
 So we're going to set up...

15
00:00:55,560 --> 00:00:56,600
 You can practice with us.

16
00:00:56,600 --> 00:01:05,240
 Right. It's a five minute.

17
00:01:05,240 --> 00:01:11,080
 Good idea.

18
00:01:17,960 --> 00:01:23,120
 Okay, so the basis of meditation is about objectivity. I

19
00:01:23,120 --> 00:01:25,080
 don't know if I want to get

20
00:01:25,080 --> 00:01:30,630
 too philosophical because they shut out. But we're trying

21
00:01:30,630 --> 00:01:33,240
 to see things as they are.

22
00:01:33,240 --> 00:01:38,080
 We're trying to see things objectively without reacting to

23
00:01:38,080 --> 00:01:39,960
 them. So what we're going to do

24
00:01:41,000 --> 00:01:46,050
 is we're going to remind ourselves, in general, this is

25
00:01:46,050 --> 00:01:49,880
 this. So pain is pain.

26
00:01:49,880 --> 00:01:53,480
 Thoughts are thoughts. Anger is anger. Depression is

27
00:01:53,480 --> 00:01:55,560
 depression. And we're not going to react

28
00:01:55,560 --> 00:02:00,700
 to anything. Because, you see, when you react to things,

29
00:02:00,700 --> 00:02:04,520
 this multiplies them and continues them,

30
00:02:04,520 --> 00:02:08,830
 and they snowball out of control. So when you experience

31
00:02:08,830 --> 00:02:10,520
 pain, if you could just experience

32
00:02:10,520 --> 00:02:15,060
 that pain, it doesn't end up bothering you. When you feel

33
00:02:15,060 --> 00:02:17,720
 angry and then you say angry,

34
00:02:17,720 --> 00:02:20,610
 you remind yourself that you're angry and it doesn't turn

35
00:02:20,610 --> 00:02:23,720
 into a fight or a problem. It

36
00:02:23,720 --> 00:02:26,510
 doesn't make you want to do bad things or say bad things.

37
00:02:26,510 --> 00:02:29,800
 If you have a thought or someone

38
00:02:29,800 --> 00:02:34,100
 shouting at you and you just see it as a thought or as a

39
00:02:34,100 --> 00:02:37,160
 sound, just as the experience of it,

40
00:02:37,720 --> 00:02:40,360
 then it won't hurt you. How many minutes is that?

41
00:02:40,360 --> 00:02:42,760
 Oh, I'm sorry. I was a count.

42
00:02:42,760 --> 00:02:47,240
 Well, the last time you had a counter on your side of the

43
00:02:47,240 --> 00:02:48,120
 hangout, I'm sorry.

44
00:02:48,120 --> 00:02:51,320
 That was about a minute, I think, right? A couple minutes.

45
00:02:51,320 --> 00:02:55,140
 Okay. So then, so here's what we're going to do. Close your

46
00:02:55,140 --> 00:02:55,560
 eyes.

47
00:02:55,560 --> 00:03:00,500
 And maybe I won't tell them to close your eyes. First, I

48
00:03:00,500 --> 00:03:01,640
 have to say how we do it. So

49
00:03:02,280 --> 00:03:06,530
 how we do this is just simply reminding ourselves. You pick

50
00:03:06,530 --> 00:03:09,800
 a word that reminds you what's going on.

51
00:03:09,800 --> 00:03:12,860
 Like for instance, if you feel pain, you remind yourself

52
00:03:12,860 --> 00:03:14,200
 just with one word, pain,

53
00:03:14,200 --> 00:03:17,280
 and you can, that becomes your meditation. You'd say to

54
00:03:17,280 --> 00:03:21,400
 yourself, pain, pain. I don't think I'm

55
00:03:21,400 --> 00:03:23,640
 having them close their eyes yet. I think this is still

56
00:03:23,640 --> 00:03:25,080
 eyes open. I made a mistake.

57
00:03:25,080 --> 00:03:29,650
 There's a pain, pain. If you have a thought, you just say

58
00:03:29,650 --> 00:03:31,640
 thinking, thinking. If you hear a sound

59
00:03:31,640 --> 00:03:33,640
 like all this noise of these people, because we're right in

60
00:03:33,640 --> 00:03:35,160
 the middle of the hallway, really. It's

61
00:03:35,160 --> 00:03:40,340
 kind of funny. Is they hearing? And so this is the skill

62
00:03:40,340 --> 00:03:43,320
 that we're trying to cultivate. So

63
00:03:43,320 --> 00:03:46,180
 the technique that I'm going to show you to do this is to

64
00:03:46,180 --> 00:03:48,280
 train you to do this. So we pick something

65
00:03:48,280 --> 00:03:51,980
 that's neutral. We'll pick our breath. Focus on the breath.

66
00:03:51,980 --> 00:03:54,040
 When your breath comes into your body,

67
00:03:54,040 --> 00:03:56,270
 your stomach will rise. When the breath goes out of the

68
00:03:56,270 --> 00:03:58,040
 body, your stomach will fall. If you don't

69
00:03:58,040 --> 00:04:02,380
 feel it, you can put your hand there, but you just say to

70
00:04:02,380 --> 00:04:07,160
 yourself, rising, fall. You can close your

71
00:04:07,160 --> 00:04:11,230
 eyes and do it. Don't say it out loud. You just remind

72
00:04:11,230 --> 00:04:15,640
 yourself in your mind, rising, fall.

73
00:04:15,640 --> 00:04:21,780
 And that trains you to do that. Once you can do that, then

74
00:04:21,780 --> 00:04:24,760
 you begin to extrapolate it into,

75
00:04:25,640 --> 00:04:30,470
 really apply it in the rest of your life. So let's try that

76
00:04:30,470 --> 00:04:30,840
 now.

77
00:04:30,840 --> 00:04:36,990
 And then I'll have them do that after a minute or so,

78
00:04:36,990 --> 00:04:39,800
 probably have one minute left. Then I'll say,

79
00:04:39,800 --> 00:04:46,250
 okay, and then now I want you to apply the same concept of

80
00:04:46,250 --> 00:04:48,120
 seeing your stomach move clearly.

81
00:04:48,120 --> 00:04:50,450
 Apply it to things that really matter. Like when you feel

82
00:04:50,450 --> 00:04:52,600
 pain, remind yourself pain.

83
00:04:53,400 --> 00:04:55,540
 When you have thoughts, remind yourself thought thinking.

84
00:04:55,540 --> 00:04:58,360
 You have emotions, remind yourself

85
00:04:58,360 --> 00:05:01,830
 angry, frustrated, bored, sad, worry. When you want

86
00:05:01,830 --> 00:05:04,200
 something wanting, when you like something like

87
00:05:04,200 --> 00:05:11,700
 you. When you hear sounds, you're hearing. And see, the

88
00:05:11,700 --> 00:05:15,800
 thing is we don't know the time limit.

89
00:05:15,800 --> 00:05:17,890
 It's whenever they want to get up. If they want to sit

90
00:05:17,890 --> 00:05:19,480
 there and talk to me for a while, well,

91
00:05:19,480 --> 00:05:23,890
 I think that's fine. But I'm not sure how many mats to have

92
00:05:23,890 --> 00:05:25,480
. I think we'll have to experiment

93
00:05:25,480 --> 00:05:29,490
 because I think it's going to be too noisy for people to be

94
00:05:29,490 --> 00:05:31,640
 too far away. So it might be like me

95
00:05:31,640 --> 00:05:35,070
 and two other people at a time. And more than that, they

96
00:05:35,070 --> 00:05:37,400
 might get too hard for them to hear.

97
00:05:37,400 --> 00:05:43,470
 I think we're going to set up a nice carpet rug or

98
00:05:43,470 --> 00:05:48,760
 something. And real sitting mats,

99
00:05:48,760 --> 00:05:51,150
 and then tell people to take their shoes off. We can do

100
00:05:51,150 --> 00:05:54,280
 that. I don't think there's any way

101
00:05:54,280 --> 00:05:56,410
 around getting them to take their shoes or boots off. Or

102
00:05:56,410 --> 00:06:00,120
 maybe the yoga mats if a carpet is hard to

103
00:06:00,120 --> 00:06:06,480
 come by. Right. We can just put out a few yoga mats and

104
00:06:06,480 --> 00:06:08,840
 just leave it at that, right?

105
00:06:08,840 --> 00:06:11,880
 That would be something. Four yoga mats together or

106
00:06:11,880 --> 00:06:12,680
 something.

107
00:06:15,000 --> 00:06:19,320
 The other thing is how to get all this to the university.

108
00:06:19,320 --> 00:06:19,320
 The sign.

109
00:06:19,320 --> 00:06:25,880
 I guess it's not really carryable at that point. See, we're

110
00:06:25,880 --> 00:06:27,000
 supposed to have a locker.

111
00:06:27,000 --> 00:06:30,120
 Tomorrow I'm going to find out about our locker, why we don

112
00:06:30,120 --> 00:06:31,880
't have a locker for our club. Because

113
00:06:31,880 --> 00:06:37,750
 every club should have a locker. But we don't yet. But I

114
00:06:37,750 --> 00:06:43,400
 know. Anyway, that's future plans here.

115
00:06:43,400 --> 00:06:47,900
 So that seems doable, no? Yes. And on your timing, after I

116
00:06:47,900 --> 00:06:49,640
 realized that I wasn't timing it for you,

117
00:06:49,640 --> 00:06:53,350
 that was two minutes. That was only two minutes. So what

118
00:06:53,350 --> 00:06:56,120
 was to the from the point that I realized

119
00:06:56,120 --> 00:07:00,050
 that from the point that I hadn't been timing it from there

120
00:07:00,050 --> 00:07:02,440
 to the end, it was two minutes.

121
00:07:02,440 --> 00:07:04,980
 So I think you were under five minutes altogether, because

122
00:07:04,980 --> 00:07:07,000
 the whole second half was only two minutes.

123
00:07:07,800 --> 00:07:13,480
 Good. Did I leave anything out? No. People will have

124
00:07:13,480 --> 00:07:14,760
 questions, probably. But I mean,

125
00:07:14,760 --> 00:07:20,520
 that is a nice sign. It's misleading. Sign is misleading.

126
00:07:20,520 --> 00:07:23,800
 False advertising. It's only a two

127
00:07:23,800 --> 00:07:27,360
 minute meditation. No, that gives them more time to

128
00:07:27,360 --> 00:07:31,720
 actually meditate. Yep. That's part of the lesson.

129
00:07:33,640 --> 00:07:36,850
 That's gonna be great fun. I hope so. I gotta get someone

130
00:07:36,850 --> 00:07:38,200
 to take pictures.

131
00:07:38,200 --> 00:07:42,600
 Maybe even make a video of it. Put it on YouTube.

132
00:07:42,600 --> 00:07:50,690
 I should go on the silhouette, actually, I should talk to

133
00:07:50,690 --> 00:07:54,040
 the silhouette, the McMaster

134
00:07:54,040 --> 00:07:57,890
 University newspaper and the radio as well. Put on a press

135
00:07:57,890 --> 00:07:59,880
 release. That's what I'll do.

136
00:07:59,880 --> 00:08:02,710
 We have to put on a press release. We did this for the

137
00:08:02,710 --> 00:08:04,840
 meditation. We did this for the peace walk.

138
00:08:04,840 --> 00:08:08,120
 And they interviewed me on radio.

139
00:08:08,120 --> 00:08:21,790
 So questions, huh? We have questions. This one wasn't

140
00:08:21,790 --> 00:08:24,840
 exactly directed to you, but it's probably

141
00:08:24,840 --> 00:08:29,090
 a good one to answer anyway. What I don't know yet is, why

142
00:08:29,090 --> 00:08:31,640
 do you call him Bante instead of Yutadamo?

143
00:08:31,640 --> 00:08:35,890
 But don't I answer this one like every week? No, it just

144
00:08:35,890 --> 00:08:37,160
 seems that way.

145
00:08:37,160 --> 00:08:41,400
 I haven't answered it before.

146
00:08:41,400 --> 00:08:47,290
 There's new people all the time. Sorry? There are new

147
00:08:47,290 --> 00:08:48,120
 people all the time.

148
00:08:48,120 --> 00:08:51,760
 New people joining this group all the time. But those

149
00:08:51,760 --> 00:08:53,480
 questions you think people can answer

150
00:08:53,480 --> 00:08:59,350
 amongst each other. Someone else can answer. Okay. Bante, I

151
00:08:59,350 --> 00:09:01,160
 was very glad to hear that you

152
00:09:01,160 --> 00:09:04,190
 get to go see your teacher again. Would it be possible for

153
00:09:04,190 --> 00:09:05,880
 us to come together as a community

154
00:09:05,880 --> 00:09:09,390
 and put something together for you to offer to Ajahn Tong?

155
00:09:09,390 --> 00:09:11,640
 And if so, what would be appropriate?

156
00:09:11,640 --> 00:09:15,670
 Thank you, Bante. That'd be great. I'm not really thinking

157
00:09:15,670 --> 00:09:16,520
 that I'm going to go.

158
00:09:16,520 --> 00:09:20,120
 It was, you know, it's been bugging me that I haven't

159
00:09:20,120 --> 00:09:22,760
 talked to him about this new monastery,

160
00:09:22,760 --> 00:09:28,280
 but it's not really worth flying across the world to do it.

161
00:09:28,280 --> 00:09:34,340
 It's not, you know, it's not like I've got a free ticket to

162
00:09:34,340 --> 00:09:34,840
 go. So

163
00:09:34,840 --> 00:09:39,330
 it would have to be paid for, which is a considerable

164
00:09:39,330 --> 00:09:40,440
 expense.

165
00:09:40,440 --> 00:09:48,450
 So, yeah, absolutely. If I go, that's for sure. Let's keep

166
00:09:48,450 --> 00:09:50,760
 that in mind, but it probably won't be.

167
00:09:51,720 --> 00:09:54,310
 I don't know. We'll see, because there's talk that they

168
00:09:54,310 --> 00:09:57,400
 might support some or some of the ticket

169
00:09:57,400 --> 00:10:01,950
 anyway. I'll see if it's really, there's a legitimate

170
00:10:01,950 --> 00:10:04,520
 conference going on as well.

171
00:10:04,520 --> 00:10:10,020
 But the only real reason to go would be to talk to my

172
00:10:10,020 --> 00:10:10,760
 teacher.

173
00:10:10,760 --> 00:10:16,150
 And he already knows about the place. I just haven't told

174
00:10:16,150 --> 00:10:17,720
 him myself, haven't talked to him,

175
00:10:17,720 --> 00:10:21,630
 haven't gotten his instructions and his blessing and all

176
00:10:21,630 --> 00:10:22,520
 that.

177
00:10:22,520 --> 00:10:39,560
 Did the Buddha know why the universe exists?

178
00:10:42,840 --> 00:10:48,220
 I think the word, I think why is problematic. You know, it

179
00:10:48,220 --> 00:10:49,640
's a question that doesn't, it's

180
00:10:49,640 --> 00:10:56,030
 like asking an innocent man why he beats his wife. It's

181
00:10:56,030 --> 00:10:58,600
 like asking, why does a tree exist?

182
00:10:58,600 --> 00:11:02,760
 What is the purpose of a tree? It's a ridiculous question.

183
00:11:02,760 --> 00:11:08,310
 It's a sort of a theistic question, really, because the

184
00:11:08,310 --> 00:11:09,640
 only answer is God.

185
00:11:11,400 --> 00:11:16,950
 And it's a very human thing to want to find a purpose for

186
00:11:16,950 --> 00:11:18,520
 things.

187
00:11:18,520 --> 00:11:24,230
 Right? We've started to, we differentiated ourselves from

188
00:11:24,230 --> 00:11:27,240
 ordinary animals by our ability

189
00:11:27,240 --> 00:11:30,360
 to cultivate tools, right? So the idea of things having a

190
00:11:30,360 --> 00:11:32,360
 purpose, and if things don't have a

191
00:11:32,360 --> 00:29:54,270
 purpose, we destroy them or we get rid of them. We remove

192
00:29:54,270 --> 00:11:38,600
 them. You know, we've had a very

193
00:11:38,600 --> 00:11:44,010
 utilitarian bent to us in this sense. So there's no reason

194
00:11:44,010 --> 00:11:47,480
 to think that anything has a purpose.

195
00:11:47,480 --> 00:11:56,630
 I mean, I guess if the question were instead, how is it

196
00:11:56,630 --> 00:11:58,200
 that the universe exists?

197
00:11:58,200 --> 00:12:03,160
 I don't know whether the Buddha knew or not.

198
00:12:06,120 --> 00:12:08,440
 But again, I think it's probably a bad question.

199
00:12:08,440 --> 00:12:17,580
 It's incomprehensible to us that there shouldn't be an

200
00:12:17,580 --> 00:12:18,360
 answer to that.

201
00:12:18,360 --> 00:12:23,240
 But I think that's our problem. I think it's just the way

202
00:12:23,240 --> 00:12:23,960
 we think.

203
00:12:23,960 --> 00:12:32,480
 I have anxiety issues. And once I become too anxious about

204
00:12:32,480 --> 00:12:34,280
 something for a few days continuously,

205
00:12:34,840 --> 00:12:37,310
 it will get harder for me to meditate. And eventually I

206
00:12:37,310 --> 00:12:38,680
 stop meditating until the thing

207
00:12:38,680 --> 00:12:41,990
 I'm worried about is subsided. How can I make sure I don't

208
00:12:41,990 --> 00:12:43,960
 stop meditating even when I'm anxious

209
00:12:43,960 --> 00:12:47,560
 about something? Right, should meditate on the anxiety.

210
00:12:47,560 --> 00:12:49,000
 Look up some of the videos I've done on

211
00:12:49,000 --> 00:12:53,140
 anxiety particularly, because it's an interesting one.

212
00:12:53,140 --> 00:12:55,720
 Anxiety, it's fairly easy to see that a lot

213
00:12:55,720 --> 00:12:59,620
 of it's just physical. And physical aspects of anxiety aren

214
00:12:59,620 --> 00:13:03,320
't anxiety. If you go to video.series

215
00:13:03,320 --> 00:13:07,620
 it is a video. What is the website? Yes. Video.siri-mongolo

216
00:13:07,620 --> 00:13:09,320
.org. We got a whole bunch of my videos

217
00:13:09,320 --> 00:13:13,280
 categorized. And I think anxiety is under mental problems

218
00:13:13,280 --> 00:13:16,360
 or something. Yes.

219
00:13:16,360 --> 00:13:23,440
 And another thing you can do is lie down, do lying

220
00:13:23,440 --> 00:13:27,640
 meditation. But definitely meditate on

221
00:13:27,640 --> 00:13:31,840
 all the aspects of what you call anxiety because much of it

222
00:13:31,840 --> 00:13:34,600
 is physical. The anxiety only lasts a

223
00:13:34,600 --> 00:13:37,390
 moment. And if you're patient and keep noting anxiety then

224
00:13:37,390 --> 00:13:39,720
 you're doing very well. You're

225
00:13:39,720 --> 00:13:42,640
 learning a lot. You're studying the anxiety. And you're med

226
00:13:42,640 --> 00:13:45,400
itating. So it doesn't actually interrupt

227
00:13:45,400 --> 00:13:48,440
 your meditation. It becomes a part of your meditation. The

228
00:13:48,440 --> 00:13:52,760
 thing that you're anxious about,

229
00:13:53,960 --> 00:13:57,480
 note that thing. Thinking, thinking.

230
00:13:57,480 --> 00:14:04,130
 How do I balance my mind between lay terms and higher

231
00:14:04,130 --> 00:14:07,800
 states of awareness through meditation?

232
00:14:07,800 --> 00:14:11,540
 I look around me and see people unaware of life itself. And

233
00:14:11,540 --> 00:14:13,400
 they seem to be in a permanent state

234
00:14:13,400 --> 00:14:17,060
 of sleep. I'm finding this very difficult. People seem to

235
00:14:17,060 --> 00:14:19,320
 be more focused on self-gain rather than

236
00:14:19,320 --> 00:14:24,720
 global gain. Well there's no balancing them. They're oppos

237
00:14:24,720 --> 00:14:27,880
ites. The dhamma goes in the opposite

238
00:14:27,880 --> 00:14:36,080
 direction of material gain. So the conflict that you face,

239
00:14:36,080 --> 00:14:39,800
 choose in the end one or the other.

240
00:14:39,800 --> 00:14:44,860
 But I mean that's in the end if you decide that you want to

241
00:14:44,860 --> 00:14:47,320
 become a monk or something.

242
00:14:47,320 --> 00:14:58,950
 What exactly does reflection of an action mean in the quote

243
00:14:58,950 --> 00:15:00,120
 of today? Thanks.

244
00:15:00,120 --> 00:15:03,400
 We have to read the whole suttha.

245
00:15:03,400 --> 00:15:11,000
 Reflection here means before you do something,

246
00:15:14,920 --> 00:15:18,140
 before you do something you should reflect. Will this be to

247
00:15:18,140 --> 00:15:19,880
 my detriment or to the detriment of

248
00:15:19,880 --> 00:15:24,100
 others? Will this be a harmful thing that I'm about to do?

249
00:15:24,100 --> 00:15:27,080
 So then you shouldn't do it. If not,

250
00:15:27,080 --> 00:15:30,250
 then go ahead and do it. In the same well you're doing it,

251
00:15:30,250 --> 00:15:32,040
 in the same after you've done it, was

252
00:15:32,040 --> 00:15:36,730
 that thing that I did? Harmful or beneficial? If it is

253
00:15:36,730 --> 00:15:39,960
 harmful then you should not do it again.

254
00:15:40,680 --> 00:15:42,610
 While you're doing it, if you realize it's harmful you

255
00:15:42,610 --> 00:15:46,600
 should stop. So reflect in that way.

256
00:15:46,600 --> 00:15:57,450
 This is a talk to his son who was a young boy and it's very

257
00:15:57,450 --> 00:15:59,880
 fatherly. It's a unique

258
00:15:59,880 --> 00:16:03,640
 dialogue with these two have father and son.

259
00:16:09,320 --> 00:16:13,640
 If you are mindful of what is, I'm sorry, if you are

260
00:16:13,640 --> 00:16:16,600
 mindful of what it is, neutral and with no

261
00:16:16,600 --> 00:16:19,680
 desire, if you accept things for what they are, is there

262
00:16:19,680 --> 00:16:22,440
 any reason to help others? Is there any

263
00:16:22,440 --> 00:16:28,940
 reason to change things? Not really, except that it's the

264
00:16:28,940 --> 00:16:32,600
 right thing to do, it's natural thing to

265
00:16:32,600 --> 00:16:36,860
 do. We're not stones, we don't just sit around doing

266
00:16:36,860 --> 00:16:39,640
 nothing. So part of your equanimity is

267
00:16:39,640 --> 00:16:42,740
 what appears to be compassionate and I think you could even

268
00:16:42,740 --> 00:16:44,440
 argue that it is compassion but

269
00:16:44,440 --> 00:16:50,280
 it's just a word. I mean you act in the right way. Enlight

270
00:16:50,280 --> 00:16:52,360
ened beings appear to be very compassionate

271
00:16:52,360 --> 00:16:54,910
 but some people think well they're not really compassionate

272
00:16:54,910 --> 00:16:56,360
 because they don't really care.

273
00:16:59,000 --> 00:17:01,920
 They don't really care but the natural thing to do is to

274
00:17:01,920 --> 00:17:02,840
 help others.

275
00:17:02,840 --> 00:17:16,220
 Just as a Christian prays the Lord's Prayer on a daily

276
00:17:16,220 --> 00:17:19,240
 basis, do you have any Buddhist texts which

277
00:17:19,240 --> 00:17:23,530
 you read reflect on daily aside from your YouTube Dhammap

278
00:17:23,530 --> 00:17:24,600
ada videos?

279
00:17:28,120 --> 00:17:34,050
 Yeah, there's, we reflect on the Buddha, the Dhamma and the

280
00:17:34,050 --> 00:17:36,120
 Sangha, that's the most common.

281
00:17:36,120 --> 00:17:40,390
 So we have reflections of the qualities of the Buddha, the

282
00:17:40,390 --> 00:17:41,400
 qualities of the Dhamma and the

283
00:17:41,400 --> 00:17:50,540
 qualities of the Sangha. And then there are other Dhammas

284
00:17:50,540 --> 00:17:52,760
 that are to be reflected upon daily.

285
00:17:52,760 --> 00:17:56,160
 There's a list of five for lay people, there's a list of

286
00:17:56,160 --> 00:18:00,200
 ten for monks called the Abhinapachaweek.

287
00:18:00,200 --> 00:18:09,400
 So what are they? They are,

288
00:18:09,400 --> 00:18:14,840
 I will get old, I will get sick, I will die,

289
00:18:14,840 --> 00:18:19,440
 I will lose everything I have, I can't quite remember. And

290
00:18:19,440 --> 00:18:21,240
 I'm heir to my kamma.

291
00:18:21,240 --> 00:18:28,460
 Look at the chanting guide, there's a fair amount of

292
00:18:28,460 --> 00:18:29,240
 chanting.

293
00:18:29,240 --> 00:18:42,140
 I believe a person who wishes to ordain must have the

294
00:18:42,140 --> 00:18:45,240
 approval of parents.

295
00:18:45,240 --> 00:18:49,090
 Is that necessary for a grown adult? What if the parents

296
00:18:49,090 --> 00:18:50,200
 don't agree?

297
00:18:50,200 --> 00:18:56,440
 Yes, technically that's necessary of a grown adult.

298
00:18:56,440 --> 00:19:01,120
 If parents don't agree, then you starve yourself until they

299
00:19:01,120 --> 00:19:01,560
 give in.

300
00:19:01,560 --> 00:19:07,420
 Or threaten to throw yourself from a cliff or something

301
00:19:07,420 --> 00:19:08,840
 like that.

302
00:19:08,840 --> 00:19:14,880
 Maybe that's a bit of a problem. But there's something

303
00:19:14,880 --> 00:19:15,560
 about that.

304
00:19:17,080 --> 00:19:18,970
 Monks who actually did that, who threatened to kill

305
00:19:18,970 --> 00:19:19,640
 themselves and

306
00:19:19,640 --> 00:19:23,080
 that the starving yourself may work.

307
00:19:23,080 --> 00:19:34,440
 I think we can't become a monk, sorry.

308
00:19:44,520 --> 00:19:50,630
 And we could go and stand in front of their front door, sit

309
00:19:50,630 --> 00:19:54,840
 on their porch until they give in.

310
00:19:54,840 --> 00:20:01,040
 Does that happen a lot? Because people ask that question a

311
00:20:01,040 --> 00:20:01,240
 lot.

312
00:20:01,240 --> 00:20:06,720
 That parents don't agree. If your parents are evangelist

313
00:20:06,720 --> 00:20:07,160
 Christians.

314
00:20:07,160 --> 00:20:09,720
 Oh, that might be a little tough.

315
00:20:13,880 --> 00:20:18,840
 Yeah, I mean many, many people who are not Buddhist would

316
00:20:18,840 --> 00:20:20,280
 forbid their children.

317
00:20:20,280 --> 00:20:22,760
 Even when they get older.

318
00:20:22,760 --> 00:20:31,550
 My grandmother would never give consent. Although sometimes

319
00:20:31,550 --> 00:20:32,120
 it's interesting,

320
00:20:32,120 --> 00:20:33,560
 they'll just say do whatever you want.

321
00:20:33,560 --> 00:20:48,030
 Is there much difference between a monk from Thailand and

322
00:20:48,030 --> 00:20:49,160
 the teaching they learn

323
00:20:49,160 --> 00:20:51,480
 from a monk from Nepal?

324
00:20:51,480 --> 00:20:55,630
 Well, every monk teaches differently. But there's also

325
00:20:55,630 --> 00:20:57,480
 different kinds of Buddhism.

326
00:20:57,480 --> 00:21:09,530
 If the parent is deceased, I assume their approval is not

327
00:21:09,530 --> 00:21:10,040
 needed.

328
00:21:10,040 --> 00:21:15,480
 But what if that was just a comment? But what if you knew

329
00:21:15,480 --> 00:21:16,920
 the parents did not approve and then

330
00:21:16,920 --> 00:21:19,930
 they passed away? And you knew that that was not their will

331
00:21:19,930 --> 00:21:20,200
?

332
00:21:20,200 --> 00:21:21,880
 As long as they're dead.

333
00:21:21,880 --> 00:21:24,180
 As long as they're dead, it's okay. Even if they never

334
00:21:24,180 --> 00:21:26,280
 would have gone for it if they were alive.

335
00:21:26,280 --> 00:21:28,740
 No, I mean, it's not about disrespecting them. It's about

336
00:21:28,740 --> 00:21:29,800
 making them feel bad.

337
00:21:29,800 --> 00:21:31,240
 Okay.

338
00:21:31,240 --> 00:21:31,960
 They're dead, they're gone.

339
00:21:31,960 --> 00:21:36,590
 In fact, it's kind of an interesting rule because the

340
00:21:36,590 --> 00:21:39,320
 Buddha only gave the rule as they were out of

341
00:21:39,320 --> 00:21:46,690
 respect for his father who then passed away. But he inst

342
00:21:46,690 --> 00:21:48,760
ated it as a rule.

343
00:21:48,760 --> 00:21:51,840
 And a lot of the times the reason the Buddha instated it

344
00:21:51,840 --> 00:21:54,120
 isn't just the only reason he instated

345
00:21:54,120 --> 00:21:58,040
 it like the impetus. So it's not to say that he didn't

346
00:21:58,040 --> 00:21:59,800
 agree with his father.

347
00:21:59,800 --> 00:22:09,230
 I mean, I think you could argue that there are extenuating

348
00:22:09,230 --> 00:22:10,200
 circumstances.

349
00:22:10,200 --> 00:22:16,840
 Sorry, Buddha ordained his nephew without his nephew's

350
00:22:16,840 --> 00:22:17,720
 parents consent.

351
00:22:17,720 --> 00:22:20,760
 He said, "They have wrong view. I'm his father."

352
00:22:20,760 --> 00:22:35,400
 [chuckle]

353
00:22:35,400 --> 00:22:38,280
 A suggestion, not a question, but a suggestion to find a

354
00:22:38,280 --> 00:22:39,640
 way to remind

355
00:22:39,640 --> 00:22:44,420
 on YouTube the link to this page. I think you have like a

356
00:22:44,420 --> 00:22:46,680
 template set up for

357
00:22:46,680 --> 00:22:52,250
 your YouTube videos, don't you, Bonthe? Or can you have a

358
00:22:52,250 --> 00:22:52,680
 look?

359
00:22:52,680 --> 00:22:54,680
 I mean, really, I'm going to add it to every video.

360
00:22:54,680 --> 00:22:59,860
 No, but isn't there a template that you can use for your

361
00:22:59,860 --> 00:23:00,440
 videos?

362
00:23:00,440 --> 00:23:02,280
 I don't know. Maybe.

363
00:23:02,280 --> 00:23:06,740
 I mean, I just copy paste, copy paste. I do it for all the

364
00:23:06,740 --> 00:23:07,880
 Dhammapada videos, but

365
00:23:07,880 --> 00:23:11,240
 I'm not going to do it for every day that we've done.

366
00:23:12,600 --> 00:23:15,320
 No, no, but I thought there, because I thought with your

367
00:23:15,320 --> 00:23:17,560
 older videos, they had so much information

368
00:23:17,560 --> 00:23:20,920
 on it. I know, well, I don't think that you probably were

369
00:23:20,920 --> 00:23:22,920
 typing that in every time.

370
00:23:22,920 --> 00:23:26,040
 I just assumed there was some sort of a template, but maybe

371
00:23:26,040 --> 00:23:27,720
 you were just copying and pasting from.

372
00:23:27,720 --> 00:23:32,200
 Well, I have it saved in the Firefox extension.

373
00:23:32,200 --> 00:23:33,160
 Okay.

374
00:23:33,160 --> 00:23:35,800
 But it's not like it doesn't form me itself. I have to copy

375
00:23:35,800 --> 00:23:36,120
 it.

376
00:23:36,120 --> 00:23:37,320
 I see.

377
00:23:41,400 --> 00:23:43,560
 I can try to put that in as a comment so people know,

378
00:23:43,560 --> 00:23:47,320
 because people still do ask questions on

379
00:23:47,320 --> 00:23:50,040
 the YouTube comments.

380
00:23:50,040 --> 00:23:52,470
 Well, there you go. Someone will just say, "Hey, if you

381
00:23:52,470 --> 00:23:53,960
 want to ask questions, go here."

382
00:23:53,960 --> 00:23:57,240
 Yeah. I do put that in when I notice it.

383
00:23:57,240 --> 00:24:05,320
 It's okay. I don't want this place overrun. Small is okay.

384
00:24:07,800 --> 00:24:12,440
 It'll grow up too soon as it is.

385
00:24:12,440 --> 00:24:26,440
 How are classes so far, Bande?

386
00:24:26,440 --> 00:24:34,990
 Okay. Yeah, Buddhism in East Asia looks really good. I just

387
00:24:34,990 --> 00:24:36,040
 had a class today.

388
00:24:36,600 --> 00:24:41,720
 The professor's specialty is Indian Buddhism, which is neat

389
00:24:41,720 --> 00:24:43,720
 because that's my interest, of course.

390
00:24:43,720 --> 00:24:53,560
 But Indian monasticism, his thesis is doing his PhD on.

391
00:24:53,560 --> 00:24:57,730
 So I feel better about it than before because I wasn't

392
00:24:57,730 --> 00:25:00,040
 really that keen on East Asian Buddhism,

393
00:25:00,040 --> 00:25:03,680
 but it sounds like someone who will be interesting to talk

394
00:25:03,680 --> 00:25:04,120
 to.

395
00:25:05,800 --> 00:25:11,190
 And he's a really gregarious sort of person, so certainly

396
00:25:11,190 --> 00:25:13,560
 will keep it interesting.

397
00:25:13,560 --> 00:25:14,840
 Some people can't teach.

398
00:25:14,840 --> 00:25:20,780
 Nice people, but they're too timid or they're too anxious

399
00:25:20,780 --> 00:25:23,720
 or too anxious to please.

400
00:25:23,720 --> 00:25:28,270
 Sometimes people make these, the professor will make these

401
00:25:28,270 --> 00:25:29,000
 silly jokes.

402
00:25:29,000 --> 00:25:31,820
 They just don't know how to be funny and they try to be

403
00:25:31,820 --> 00:25:35,240
 funny and it's just teach.

404
00:25:35,240 --> 00:25:38,200
 It's cool. We don't need to be entertained.

405
00:25:38,200 --> 00:25:44,680
 Some people do, I suppose, and it's intimidating.

406
00:25:44,680 --> 00:25:47,080
 You get up there and all these students with glassy eyes

407
00:25:47,080 --> 00:25:47,240
 and

408
00:25:47,240 --> 00:25:51,480
 slouching on their phones.

409
00:25:51,480 --> 00:25:54,270
 It's really intimidating knowing when you're in front of a

410
00:25:54,270 --> 00:25:54,600
 room and

411
00:25:54,600 --> 00:25:57,160
 everyone looks bored about what you're saying.

412
00:25:57,160 --> 00:26:02,760
 So if you're not hard, if you're not strong, you really

413
00:26:02,760 --> 00:26:04,280
 just have to ignore your audience.

414
00:26:06,280 --> 00:26:07,800
 Are they large classes?

415
00:26:07,800 --> 00:26:11,560
 Yeah, they're large.

416
00:26:11,560 --> 00:26:14,550
 This one is a third year class, but it's a cross listed

417
00:26:14,550 --> 00:26:16,680
 with arts and science, which is neat

418
00:26:16,680 --> 00:26:19,620
 because arts and science, which was what I was in years ago

419
00:26:19,620 --> 00:26:21,720
, and it's this really special program

420
00:26:21,720 --> 00:26:25,400
 at McMaster for peoners, people who are just looking to

421
00:26:25,400 --> 00:26:25,800
 learn.

422
00:26:25,800 --> 00:26:29,240
 So we've got a bunch of upper year arts side people, which

423
00:26:29,240 --> 00:26:30,200
 is awesome.

424
00:26:30,200 --> 00:26:39,880
 And tomorrow I think I'm switching into another third year

425
00:26:39,880 --> 00:26:41,640
 peace studies class because

426
00:26:41,640 --> 00:26:45,670
 today I had a class on peace and popular culture, but I'm

427
00:26:45,670 --> 00:26:47,480
 afraid it sounds like it's going to be a

428
00:26:47,480 --> 00:26:49,640
 lot of music and movies.

429
00:26:49,640 --> 00:26:52,760
 That's not really a program.

430
00:26:56,040 --> 00:27:01,480
 I was like, could I justify that?

431
00:27:01,480 --> 00:27:05,880
 That's a bit of quandary.

432
00:27:05,880 --> 00:27:08,840
 It's also newspapers.

433
00:27:08,840 --> 00:27:12,150
 So I was thinking, well, maybe we're looking at newspapers

434
00:27:12,150 --> 00:27:13,960
 and newspaper articles.

435
00:27:13,960 --> 00:27:17,960
 But I don't think so.

436
00:27:17,960 --> 00:27:20,590
 I think peace studies is going to turn out to be not as

437
00:27:20,590 --> 00:27:21,800
 interesting as I thought it was.

438
00:27:21,800 --> 00:27:24,920
 It's very much about external peace.

439
00:27:26,920 --> 00:27:28,360
 Not so much about inner peace.

440
00:27:28,360 --> 00:27:32,670
 Like people that go to protests and get really angry

441
00:27:32,670 --> 00:27:35,000
 protesting about peace.

442
00:27:35,000 --> 00:27:37,240
 Too.

443
00:27:37,240 --> 00:27:40,520
 And also like brokering peace, peace talks.

444
00:27:40,520 --> 00:27:42,120
 There's all that kind of stuff.

445
00:27:42,120 --> 00:27:47,500
 Like peace corps, whenever that is, peace corps, peace

446
00:27:47,500 --> 00:27:48,360
 corps.

447
00:27:48,360 --> 00:27:48,600
 Yeah.

448
00:27:48,600 --> 00:29:54,270
 Like Canadian, we have the blue, we used to have the blue

449
00:29:54,270 --> 00:27:54,200
 berets, the peace, peace, peace.

450
00:27:56,120 --> 00:27:58,520
 God, peacekeepers.

451
00:27:58,520 --> 00:28:04,600
 I'm thinking religious studies actually would be more

452
00:28:04,600 --> 00:28:06,520
 interesting.

453
00:28:06,520 --> 00:28:08,520
 Thought it wouldn't.

454
00:28:08,520 --> 00:28:13,480
 But kind of key note on religious studies now.

455
00:28:13,480 --> 00:28:23,960
 Is that all our questions?

456
00:28:24,440 --> 00:28:27,240
 Just one more.

457
00:28:27,240 --> 00:28:29,400
 Did you hear back from TEDx, Bante?

458
00:28:29,400 --> 00:28:31,320
 No, they...

459
00:28:31,320 --> 00:28:33,480
 Let me see.

460
00:28:33,480 --> 00:28:37,720
 They just closed applications on January 4th.

461
00:28:37,720 --> 00:28:40,360
 So just a couple days ago.

462
00:28:40,360 --> 00:28:43,880
 So I think it's still early.

463
00:28:43,880 --> 00:28:44,920
 I'm not holding my breath.

464
00:28:44,920 --> 00:28:46,760
 I mean, not that it's a big deal.

465
00:28:46,760 --> 00:28:51,270
 I'm not really hopeful because I don't probably fit the

466
00:28:51,270 --> 00:28:52,840
 mold of a student.

467
00:28:52,840 --> 00:28:54,440
 And it's supposed to be student based.

468
00:28:54,440 --> 00:28:56,600
 So I don't know.

469
00:28:56,600 --> 00:28:57,400
 Maybe they like it.

470
00:28:57,400 --> 00:28:58,360
 Maybe they don't.

471
00:28:58,360 --> 00:29:00,280
 The other thing, it was supposed to be about change.

472
00:29:00,280 --> 00:29:03,000
 And I didn't talk about change in my application.

473
00:29:03,000 --> 00:29:04,120
 I realized that afterwards.

474
00:29:04,120 --> 00:29:08,510
 Forgot that I should try to tie it into their paradox of

475
00:29:08,510 --> 00:29:09,720
 change.

476
00:29:09,720 --> 00:29:15,320
 Nobody.

477
00:29:15,320 --> 00:29:19,400
 I've got lots of work to do as it is.

478
00:29:19,400 --> 00:29:21,560
 Yeah, it certainly is about change.

479
00:29:21,560 --> 00:29:24,920
 I mean, introducing technology to ancient teachings is...

480
00:29:24,920 --> 00:29:26,280
 That's all about change.

481
00:29:26,280 --> 00:29:27,080
 But...

482
00:29:27,080 --> 00:29:28,520
 Yeah, that's right.

483
00:29:28,520 --> 00:29:30,840
 I think it's close enough that they can imply it.

484
00:29:30,840 --> 00:29:33,240
 But they probably should at least use the word change.

485
00:29:33,240 --> 00:29:39,560
 Okay, that's all then.

486
00:29:39,560 --> 00:29:40,040
 Good night.

487
00:29:40,040 --> 00:29:43,000
 Thanks for joining us.

488
00:29:43,000 --> 00:29:45,400
 Thank you, Bante.

489
00:29:45,400 --> 00:29:46,520
 Thank you, Robin.

490
00:29:46,520 --> 00:29:47,000
 Good night.

491
00:29:47,000 --> 00:29:54,360
 Thank you.

