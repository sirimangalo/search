1
00:00:00,000 --> 00:00:04,530
 Do Buddhists see life as a kind of mistake that should sort

2
00:00:04,530 --> 00:00:07,000
 itself out by obtaining nibbāna?

3
00:00:07,000 --> 00:00:14,620
 If there was a way for life to avoid suffering, would it be

4
00:00:14,620 --> 00:00:18,000
 okay for it to continue in the physical realm?

5
00:00:18,000 --> 00:00:21,000
 Want to start?

6
00:00:24,000 --> 00:00:30,240
 Well, if there is no suffering then there wouldn't be any

7
00:00:30,240 --> 00:00:33,000
 point, I guess,

8
00:00:33,000 --> 00:00:37,000
 but you're bound to have suffering in the physical realm.

9
00:00:37,000 --> 00:00:43,000
 Just because that's the... that's just the way it is.

10
00:00:43,000 --> 00:00:47,430
 Your body is uncomfortable. Having to deal with things they

11
00:00:47,430 --> 00:00:49,000
 don't like is uncomfortable.

12
00:00:49,000 --> 00:00:53,940
 You're bound to experience pain emotionally, mentally,

13
00:00:53,940 --> 00:00:55,000
 physically.

14
00:00:55,000 --> 00:01:01,000
 So, I don't know, this just seems like a...

15
00:01:01,000 --> 00:01:06,000
 ...philosophizing about things.

16
00:01:06,000 --> 00:01:08,790
 It doesn't really seem to matter, well, if there wasn't

17
00:01:08,790 --> 00:01:10,000
 suffering in the physical realm,

18
00:01:10,000 --> 00:01:13,500
 because there is and there has to be. There's no way that

19
00:01:13,500 --> 00:01:15,000
 it couldn't be.

20
00:01:15,000 --> 00:01:17,000
 Hmm.

21
00:01:17,000 --> 00:01:23,860
 Yeah, just let's... if I can get a little bit technical

22
00:01:23,860 --> 00:01:25,000
 about the question.

23
00:01:25,000 --> 00:01:30,700
 A mistake kind of implies that at one point, this idea that

24
00:01:30,700 --> 00:01:33,000
 maybe one point in the past we were in nibbāna

25
00:01:33,000 --> 00:01:36,000
 and then we... oops, we somehow fell out of it.

26
00:01:38,000 --> 00:01:48,000
 You know, reality or existence is... it exists, no?

27
00:01:48,000 --> 00:01:53,410
 And it always has. It is called existence and in an

28
00:01:53,410 --> 00:01:56,520
 ultimate sense that's really all you can say about it, is

29
00:01:56,520 --> 00:01:58,000
 it exists.

30
00:01:58,000 --> 00:02:00,000
 There are a few more things you can say.

31
00:02:00,000 --> 00:02:04,000
 Existence, the characteristic of existence is to arise.

32
00:02:05,000 --> 00:02:09,040
 Anything that comes into existence, and maybe that's not

33
00:02:09,040 --> 00:02:10,000
 quite correct.

34
00:02:10,000 --> 00:02:15,000
 The word existence isn't quite correct, but experience, no?

35
00:02:15,000 --> 00:02:17,000
 Let's put it this way, because this is what we always say,

36
00:02:17,000 --> 00:02:19,000
 reality is experience.

37
00:02:19,000 --> 00:02:25,260
 So, experience is something that has been... or that exists

38
00:02:25,260 --> 00:02:26,000
, no?

39
00:02:26,000 --> 00:02:31,730
 And experience, that which is experienced arises, so there

40
00:02:31,730 --> 00:02:33,000
 is the arising.

41
00:02:33,000 --> 00:02:37,000
 That which arises is also of the nature to cease.

42
00:02:37,000 --> 00:02:40,000
 This is actually what we're trying to realize in Buddhism.

43
00:02:40,000 --> 00:02:43,250
 If you realize just that fact, you become free from

44
00:02:43,250 --> 00:02:44,000
 suffering.

45
00:02:44,000 --> 00:02:48,000
 That... or you enter at least and become a sotapan.

46
00:02:48,000 --> 00:02:50,000
 If you realize that everything...

47
00:02:50,000 --> 00:02:53,000
 (speaking in japanese)

48
00:02:53,000 --> 00:02:56,000
 If you realize that, that's the entry into sotapan.

49
00:02:56,000 --> 00:02:59,000
 But it doesn't just mean you think about it philosophically

50
00:02:59,000 --> 00:02:59,000
,

51
00:02:59,000 --> 00:03:01,790
 it means you actually experience the cessation of

52
00:03:01,790 --> 00:03:02,000
 everything.

53
00:03:02,000 --> 00:03:05,000
 So you realize that there's nothing that lasts.

54
00:03:05,000 --> 00:03:08,000
 We've never realized that.

55
00:03:08,000 --> 00:03:11,000
 It's not something that is a part of experience.

56
00:03:11,000 --> 00:03:13,000
 It's not something that is...

57
00:03:13,000 --> 00:03:19,000
 It's not a part of the loop, right?

58
00:03:19,000 --> 00:03:21,000
 So when you get caught up in the loop, you'll never be able

59
00:03:21,000 --> 00:03:23,000
 to see this arising and ceasing.

60
00:03:23,000 --> 00:03:27,000
 Because the loop is that of experiencing...

61
00:03:29,000 --> 00:03:33,840
 Clinging, acting, and then experiencing clinging and acting

62
00:03:33,840 --> 00:03:34,000
.

63
00:03:34,000 --> 00:03:36,000
 This is the what...

64
00:03:36,000 --> 00:03:40,000
 It starts with the kilesa, the reacting.

65
00:03:40,000 --> 00:03:42,000
 Then there's the kamma, the acting.

66
00:03:42,000 --> 00:03:47,240
 And then there's the vibhaka, which is the experiencing the

67
00:03:47,240 --> 00:03:48,000
 result.

68
00:03:48,000 --> 00:03:50,650
 And then when you experience the result, you react and so

69
00:03:50,650 --> 00:03:51,000
 on.

70
00:03:51,000 --> 00:03:54,010
 And because we're doing this, we never stop to actually

71
00:03:54,010 --> 00:03:56,000
 look at what's making up the experience.

72
00:03:56,000 --> 00:03:58,000
 We're constantly reacting.

73
00:03:58,000 --> 00:04:04,000
 So because of that, we've always been in samsara.

74
00:04:04,000 --> 00:04:10,310
 It's only a person who is able to stop the ferris wheel or

75
00:04:10,310 --> 00:04:13,000
 the merry-go-round

76
00:04:13,000 --> 00:04:16,630
 and actually begin to look at what they're doing, look at

77
00:04:16,630 --> 00:04:18,000
 what's going on,

78
00:04:18,000 --> 00:04:21,000
 is able to free themselves.

79
00:04:21,000 --> 00:04:23,000
 So it's not a mistake.

80
00:04:23,000 --> 00:04:26,000
 It's an experience.

81
00:04:26,000 --> 00:04:30,000
 It's the state of experiencing things.

82
00:04:30,000 --> 00:04:36,000
 Now, Buddhism isn't a religion of complaining and saying,

83
00:04:36,000 --> 00:04:38,900
 "You know, there's something wrong here. You have to do

84
00:04:38,900 --> 00:04:40,000
 something."

85
00:04:40,000 --> 00:04:45,000
 It's pointing out that this can never be stable.

86
00:04:45,000 --> 00:04:49,000
 It can never be satisfying.

87
00:04:49,000 --> 00:04:56,000
 Because it has the nature to... or it's not static, no?

88
00:04:56,000 --> 00:05:04,000
 So it has the nature to lead in one direction or the other.

89
00:05:04,000 --> 00:05:07,070
 If a person does good things, they become a good person and

90
00:05:07,070 --> 00:05:08,000
 become happy.

91
00:05:08,000 --> 00:05:13,050
 And then if in one... when they become negligent, they fall

92
00:05:13,050 --> 00:05:15,000
 down and become unhappy.

93
00:05:15,000 --> 00:05:18,000
 They do bad things and become unhappy.

94
00:05:18,000 --> 00:05:23,000
 So this is why it's... the reason why it's never possible

95
00:05:23,000 --> 00:05:24,000
 to avoid suffering

96
00:05:24,000 --> 00:05:27,000
 is because it's not static.

97
00:05:27,000 --> 00:05:33,140
 So even if you're able to avoid suffering for a billion,

98
00:05:33,140 --> 00:05:35,000
 billion, billion, billion years

99
00:05:35,000 --> 00:05:39,000
 or a Google years, Googleplex years, let's say,

100
00:05:39,000 --> 00:05:43,150
 or from the time of a big bang to a big crunch or a cold

101
00:05:43,150 --> 00:05:44,000
 death or whatever,

102
00:05:44,000 --> 00:05:48,000
 if you were able, this... whatever they conjecture.

103
00:05:48,000 --> 00:05:52,050
 Even that isn't freedom from suffering because it's still

104
00:05:52,050 --> 00:05:53,000
 impermanent.

105
00:05:53,000 --> 00:05:54,000
 It's still not static.

106
00:05:54,000 --> 00:05:58,000
 As long as there is the mind creating more karma,

107
00:05:58,000 --> 00:06:02,000
 based on its kilas, its desires, its partialities,

108
00:06:02,000 --> 00:06:04,000
 there will be more karma.

109
00:06:04,000 --> 00:06:06,190
 When there is more karma, there will be more results and

110
00:06:06,190 --> 00:06:07,000
 there will be a change.

111
00:06:07,000 --> 00:06:14,060
 The Buddha said, "Manopubangamadamma," reality experience

112
00:06:14,060 --> 00:06:16,000
 comes from the mind.

113
00:06:16,000 --> 00:06:18,970
 So the only way you could have something static is to get

114
00:06:18,970 --> 00:06:20,000
 rid of the mind.

115
00:06:20,000 --> 00:06:22,840
 If you get rid of the mind, you could have a robot that was

116
00:06:22,840 --> 00:06:25,000
 perhaps always happy.

117
00:06:25,000 --> 00:06:29,000
 But there would be... well, yeah, the robot would be fine.

118
00:06:29,000 --> 00:06:32,000
 You wouldn't say the robot would be unhappy.

119
00:06:32,000 --> 00:06:34,000
 This computer is never unhappy, for example.

120
00:06:34,000 --> 00:06:37,000
 Microphone is never unhappy.

121
00:06:37,000 --> 00:06:39,000
 It's got no problem because there's no mind.

122
00:06:39,000 --> 00:06:45,000
 But the mind that experiences has a problem

123
00:06:45,000 --> 00:06:48,000
 because it's clinging and always creating.

124
00:06:48,000 --> 00:06:49,000
 It's going around in this circle.

125
00:06:49,000 --> 00:06:55,000
 Buddhism teaches us to stop that and to create a mind

126
00:06:55,000 --> 00:06:58,990
 or to develop an experience of reality that doesn't cling

127
00:06:58,990 --> 00:07:01,000
 and that doesn't create.

128
00:07:01,000 --> 00:07:06,030
 Because it doesn't cling and doesn't create, it becomes

129
00:07:06,030 --> 00:07:08,000
 stable, it becomes satisfying,

130
00:07:08,000 --> 00:07:13,000
 it becomes permanent.

131
00:07:13,000 --> 00:07:16,000
 It becomes constant.

132
00:07:16,000 --> 00:07:18,000
 So there is never any suffering.

133
00:07:18,000 --> 00:07:21,920
 The experiences that have been created in the past begin to

134
00:07:21,920 --> 00:07:23,000
 fade away.

135
00:07:23,000 --> 00:07:27,000
 And in the end, there is only this basic experience of

136
00:07:27,000 --> 00:07:27,000
 seeing, hearing,

137
00:07:27,000 --> 00:07:30,930
 smelling, tasting, feeling, thinking, which also disappears

138
00:07:30,930 --> 00:07:31,000
.

139
00:07:31,000 --> 00:07:35,000
 And so the person who enters into nibbana does so

140
00:07:35,000 --> 00:07:39,140
 because they've entered into a state of non-clinging, of

141
00:07:39,140 --> 00:07:40,000
 non-creating.

142
00:07:40,000 --> 00:07:43,590
 And if you don't enter into that state, you're always going

143
00:07:43,590 --> 00:07:44,000
 to be creating,

144
00:07:44,000 --> 00:07:46,000
 you're always going to be changing things.

145
00:07:46,000 --> 00:07:52,000
 You have to remember that Buddhism hasn't avoided this.

146
00:07:52,000 --> 00:07:59,140
 The Buddha didn't slip by him, this idea of creating a

147
00:07:59,140 --> 00:08:03,000
 stable and lasting reality in samsara.

148
00:08:03,000 --> 00:08:07,000
 The Buddha actually remembered these kind of states.

149
00:08:07,000 --> 00:08:09,330
 And he talked about these kind of states, the state of a

150
00:08:09,330 --> 00:08:10,000
 Brahma, for example.

151
00:08:10,000 --> 00:08:14,010
 So these people talking about, maybe what if we put the

152
00:08:14,010 --> 00:08:16,000
 human mind into a machine

153
00:08:16,000 --> 00:08:20,310
 or we altered the brain in such a way, fixed it up so that

154
00:08:20,310 --> 00:08:21,000
 it never got old

155
00:08:21,000 --> 00:08:25,150
 and so that it was constantly in some kind of pleasure,

156
00:08:25,150 --> 00:08:26,000
 this kind of pleasurable state.

157
00:08:26,000 --> 00:08:29,230
 And they say, well, there you go. There's something, this

158
00:08:29,230 --> 00:08:30,000
 transhumanism.

159
00:08:30,000 --> 00:08:33,000
 Did you read about on our forum? Someone was asking this.

160
00:08:33,000 --> 00:08:38,000
 But that's pitiful in comparison with the age of a Brahma,

161
00:08:38,000 --> 00:08:40,000
 a god that the Buddha was talking about

162
00:08:40,000 --> 00:08:43,000
 and remembered and explained how to get to.

163
00:08:43,000 --> 00:08:48,000
 You don't need robots to do it. Just leave the body behind.

164
00:08:48,000 --> 00:08:51,640
 Develop the mind to the extent that it leaves behind the

165
00:08:51,640 --> 00:08:52,000
 body.

166
00:08:52,000 --> 00:08:57,490
 And it can do that for an entire kappa, an entire age, even

167
00:08:57,490 --> 00:08:59,000
 longer, I think.

168
00:08:59,000 --> 00:09:02,760
 There are certain Brahma realms that are above the Big Bang

169
00:09:02,760 --> 00:09:03,000
.

170
00:09:03,000 --> 00:09:06,270
 So even the Big Bang doesn't affect them, or the big crunch

171
00:09:06,270 --> 00:09:07,000
 or whatever.

172
00:09:07,000 --> 00:09:11,000
 The changing of the kappa doesn't affect them.

173
00:09:11,000 --> 00:09:14,730
 And then there's maybe, I don't know, cosmology is, it's

174
00:09:14,730 --> 00:09:17,000
 quite detailed actually.

175
00:09:17,000 --> 00:09:21,000
 This robot is certainly not going to last for that long.

176
00:09:21,000 --> 00:09:24,130
 But even a Brahma does, and even a Brahma is not free from

177
00:09:24,130 --> 00:09:25,000
 suffering.

178
00:09:25,000 --> 00:09:27,000
 You never can be.

179
00:09:27,000 --> 00:09:31,280
 As Nagasena was saying, by very nature, experience is

180
00:09:31,280 --> 00:09:33,000
 unsatisfying and uncontrollable.

181
00:09:33,000 --> 00:09:35,000
 It's not static.

182
00:09:35,000 --> 00:09:40,000
 You can't, it's not static because there is the intentions.

183
00:09:40,000 --> 00:09:43,390
 There is the Kilesa. This is why the Buddha said the Kilesa

184
00:09:43,390 --> 00:09:45,000
 are the problem, defilements in our mind.

185
00:09:45,000 --> 00:09:49,700
 Because we want something, because we are not satisfied

186
00:09:49,700 --> 00:09:51,000
 with things as they are,

187
00:09:51,000 --> 00:09:53,000
 we make some attempt to change things.

188
00:09:53,000 --> 00:09:57,000
 The only way to become satisfied is to give up our desire

189
00:09:57,000 --> 00:09:59,000
 for something that doesn't exist.

190
00:09:59,000 --> 00:10:04,000
 To give up our desire for any impermanent thing.

191
00:10:04,000 --> 00:10:07,590
 And once we give up that desire, the mind naturally incl

192
00:10:07,590 --> 00:10:11,000
ines towards nirvana and not towards the arising.

193
00:10:11,000 --> 00:10:13,960
 I mean, how could it? It wouldn't ever give rise to the

194
00:10:13,960 --> 00:10:16,000
 intention to create anything at all.

195
00:10:16,000 --> 00:10:19,000
 Because it doesn't give rise to the intention to create.

196
00:10:19,000 --> 00:10:25,450
 It can't help but, well, the things that are created will

197
00:10:25,450 --> 00:10:27,000
 not increase.

198
00:10:27,000 --> 00:10:29,610
 There's no feeding of the fire, so the fire eventually goes

199
00:10:29,610 --> 00:10:30,000
 out.

200
00:10:30,000 --> 00:10:35,000
 That's the answer.

