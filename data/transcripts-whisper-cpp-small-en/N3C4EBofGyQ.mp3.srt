1
00:00:00,000 --> 00:00:05,320
 "Bhante, I am a Nufasaka practicing at a monastery in the

2
00:00:05,320 --> 00:00:06,080
 USA.

3
00:00:06,080 --> 00:00:09,750
 I want to ordain and restrain according to the Patimoka to

4
00:00:09,750 --> 00:00:11,500
 the best of my ability.

5
00:00:11,500 --> 00:00:14,920
 I am very discouraged with how most monks in America have

6
00:00:14,920 --> 00:00:16,120
 seen practice.

7
00:00:16,120 --> 00:00:19,000
 Will you please help guide me?"

8
00:00:19,000 --> 00:00:21,000
 Yes.

9
00:00:21,000 --> 00:00:24,000
 Will I please help guide you?

10
00:00:24,000 --> 00:00:25,000
 Well, okay.

11
00:00:25,000 --> 00:00:26,000
 Yes.

12
00:00:26,000 --> 00:00:34,010
 I guess in the sense that this is clear that in today's age

13
00:00:34,010 --> 00:00:37,320
, the standard of keeping the

14
00:00:37,320 --> 00:00:44,760
 Buddhist monastic discipline is pretty low, unfortunately.

15
00:00:44,760 --> 00:00:50,880
 So there is a lot of breaking of rules and so on.

16
00:00:50,880 --> 00:00:53,960
 And now, probably going to get in trouble for even saying

17
00:00:53,960 --> 00:00:55,440
 that we're not supposed to

18
00:00:55,440 --> 00:00:57,840
 talk about it, but that's not really true.

19
00:00:57,840 --> 00:00:59,880
 We're not supposed to say, "This monk broke this rule.

20
00:00:59,880 --> 00:01:03,430
 That monk broke that rule," because we don't want to be

21
00:01:03,430 --> 00:01:06,800
 tattletales and cause conflicts.

22
00:01:06,800 --> 00:01:09,840
 But to just talk about the state of the Sangha, I think, is

23
00:01:09,840 --> 00:01:10,960
 very important.

24
00:01:10,960 --> 00:01:14,210
 It's important that we're not hiding it, because the point

25
00:01:14,210 --> 00:01:15,320
 is not to hide it.

26
00:01:15,320 --> 00:01:18,300
 The point is to be clear about who is the spokesperson for

27
00:01:18,300 --> 00:01:19,080
 the Sangha.

28
00:01:19,080 --> 00:01:23,640
 And I guess it's true that I'm not really the spokesperson.

29
00:01:23,640 --> 00:01:28,180
 That it can't be denied that when you go to the monasteries

30
00:01:28,180 --> 00:01:30,440
, if you spend some time there,

31
00:01:30,440 --> 00:01:33,550
 if you go to your superficial visitor, you often don't see

32
00:01:33,550 --> 00:01:33,880
 it.

33
00:01:33,880 --> 00:01:37,680
 But if you stay for a while, you start to see that it's

34
00:01:37,680 --> 00:01:39,160
 getting shaky.

35
00:01:39,160 --> 00:01:40,720
 That's not a good thing.

36
00:01:40,720 --> 00:01:44,560
 And it doesn't make for a good environment for the practice

37
00:01:44,560 --> 00:01:44,640
.

38
00:01:44,640 --> 00:01:47,480
 One thing you have to understand is it's tough being a monk

39
00:01:47,480 --> 00:01:47,560
.

40
00:01:47,560 --> 00:01:51,160
 So breaking the precepts happens.

41
00:01:51,160 --> 00:01:54,140
 And you have to understand the levels of breaking the

42
00:01:54,140 --> 00:01:54,960
 precepts.

43
00:01:54,960 --> 00:01:59,490
 So if a monk watches football, for example, which seems to

44
00:01:59,490 --> 00:02:00,360
...

45
00:02:00,360 --> 00:02:02,320
 Apparently everyone's watching football.

46
00:02:02,320 --> 00:02:05,190
 I'm not even sure if it's still on, but I think I heard

47
00:02:05,190 --> 00:02:07,160
 recently they're near the last

48
00:02:07,160 --> 00:02:08,160
 game.

49
00:02:08,160 --> 00:02:11,600
 But I'm not quite sure.

50
00:02:11,600 --> 00:02:13,320
 Germany, I think, is playing.

51
00:02:13,320 --> 00:02:18,120
 So that's as far as I go with that.

52
00:02:18,120 --> 00:02:24,240
 So if a monk is watching the World Cup of football, soccer,

53
00:02:24,240 --> 00:02:26,680
 whatever you call it, that's

54
00:02:26,680 --> 00:02:27,680
 not a good thing.

55
00:02:27,680 --> 00:02:29,160
 It's not a good sign.

56
00:02:29,160 --> 00:02:36,000
 But that monk could still recover.

57
00:02:36,000 --> 00:02:39,790
 It's not a major precept by any means, watching a football

58
00:02:39,790 --> 00:02:40,440
 game.

59
00:02:40,440 --> 00:02:43,350
 So that monk can come back and realize that there are

60
00:02:43,350 --> 00:02:45,080
 better things to do and can come

61
00:02:45,080 --> 00:02:48,280
 to see the uselessness of that and can reflect upon

62
00:02:48,280 --> 00:02:51,760
 watching the game and say to themselves,

63
00:02:51,760 --> 00:02:53,960
 "That's really not bringing me happiness."

64
00:02:53,960 --> 00:02:56,000
 And so it still has potential to develop.

65
00:02:56,000 --> 00:03:00,920
 And this sort of thing is something that goes on the other

66
00:03:00,920 --> 00:03:01,720
 side.

67
00:03:01,720 --> 00:03:04,600
 So some people are too forgiving and they just let monks do

68
00:03:04,600 --> 00:03:05,880
 whatever they want and say

69
00:03:05,880 --> 00:03:10,360
 whatever, it's fine, to each their own kind of thing.

70
00:03:10,360 --> 00:03:13,420
 But the other side is where people, every little thing they

71
00:03:13,420 --> 00:03:14,920
 begin to criticize and they

72
00:03:14,920 --> 00:03:17,840
 storm out of the monasteries and write nasty blog posts

73
00:03:17,840 --> 00:03:20,720
 because the monks did something.

74
00:03:20,720 --> 00:03:24,640
 You know, it can often be there was one monk who split a

75
00:03:24,640 --> 00:03:26,920
 community apart, I've told this

76
00:03:26,920 --> 00:03:31,420
 before, because he was eating eggs, I think, and because he

77
00:03:31,420 --> 00:03:32,920
 was eating meat.

78
00:03:32,920 --> 00:03:35,150
 And so he split a community apart just over something like

79
00:03:35,150 --> 00:03:36,360
 that, which isn't even in the

80
00:03:36,360 --> 00:03:38,040
 Vinaya.

81
00:03:38,040 --> 00:03:40,420
 Eating eggs that haven't been fertilized, eating meat that

82
00:03:40,420 --> 00:03:42,040
 hasn't been killed for you, this

83
00:03:42,040 --> 00:03:44,880
 is not against the Vinaya, the monks' rules.

84
00:03:44,880 --> 00:03:48,440
 And yet it managed to split an entire community apart.

85
00:03:48,440 --> 00:03:51,920
 Many people wouldn't even go to see him.

86
00:03:51,920 --> 00:03:55,140
 So that kind of thing is common in the East and in the West

87
00:03:55,140 --> 00:03:56,400
 and in all places.

88
00:03:56,400 --> 00:03:58,750
 Westerners go into Buddhist monasteries and say, "This isn

89
00:03:58,750 --> 00:03:59,280
't real."

90
00:03:59,280 --> 00:04:00,980
 They look at the Buddha image and they say, "That's not

91
00:04:00,980 --> 00:04:01,720
 really Buddhism.

92
00:04:01,720 --> 00:04:03,880
 I spit on your Buddha," that kind of thing.

93
00:04:03,880 --> 00:04:06,720
 And this is going too far.

94
00:04:06,720 --> 00:04:10,530
 I think you have to have compassion and understanding that

95
00:04:10,530 --> 00:04:13,400
 it's tough being a monk and it's especially

96
00:04:13,400 --> 00:04:17,880
 tough being a monk in the West.

97
00:04:17,880 --> 00:04:21,030
 If your only monastic training has been in a university,

98
00:04:21,030 --> 00:04:22,680
 which for a lot of these monks

99
00:04:22,680 --> 00:04:28,400
 it is, and right out of university you're given this

100
00:04:28,400 --> 00:04:32,480
 instruction on how to be a missionary

101
00:04:32,480 --> 00:04:35,910
 and then you go abroad, there's not the practical backing

102
00:04:35,910 --> 00:04:38,140
 to help you reflect upon these things

103
00:04:38,140 --> 00:04:40,200
 and help you let go of them.

104
00:04:40,200 --> 00:04:43,970
 So to some extent you have to be forgiving and understand

105
00:04:43,970 --> 00:04:45,800
 that it's still possible for

106
00:04:45,800 --> 00:04:50,240
 such people to recover.

107
00:04:50,240 --> 00:04:53,180
 Now if there's a lot of corruption and attachment to money

108
00:04:53,180 --> 00:04:54,920
 and like asking lay, some of the

109
00:04:54,920 --> 00:04:58,230
 bad signs are like asking lay people for money, over-att

110
00:04:58,230 --> 00:05:00,240
achment between the monks and the

111
00:05:00,240 --> 00:05:07,440
 women and over-familiarity there is a bad sign.

112
00:05:07,440 --> 00:05:12,010
 Obsessive luxury, where the monks have widescreen telev

113
00:05:12,010 --> 00:05:15,040
isions and you see satellite dishes are

114
00:05:15,040 --> 00:05:20,070
 usually a bad sign, but as I say, oh I don't know, it's not

115
00:05:20,070 --> 00:05:22,760
 a huge deal but not a good sign

116
00:05:22,760 --> 00:05:28,000
 when you see satellite dishes and widescreen televisions

117
00:05:28,000 --> 00:05:30,320
 and that kind of thing.

118
00:05:30,320 --> 00:05:35,480
 If the monks have stopped wearing robes, that's a really

119
00:05:35,480 --> 00:05:38,480
 bad sign, that kind of thing.

120
00:05:38,480 --> 00:05:42,930
 But that being said, there's no question that you will

121
00:05:42,930 --> 00:05:46,000
 benefit from a traditional monastic

122
00:05:46,000 --> 00:05:50,430
 environment where you at least are encouraged and allowed

123
00:05:50,430 --> 00:05:52,800
 and taught how to practice the

124
00:05:52,800 --> 00:05:53,800
 discipline of the Buddha.

125
00:05:53,800 --> 00:05:54,800
 I didn't have that.

126
00:05:54,800 --> 00:05:57,290
 You know, when I started out, what I was lucky is the

127
00:05:57,290 --> 00:05:59,160
 monastery was at, what they did have

128
00:05:59,160 --> 00:06:02,440
 was a strong tradition of meditation.

129
00:06:02,440 --> 00:06:07,560
 So everyone had to start by practicing meditation and there

130
00:06:07,560 --> 00:06:10,840
's an environment that is supportive

131
00:06:10,840 --> 00:06:12,040
 for the meditative practice.

132
00:06:12,040 --> 00:06:15,640
 Now, what I didn't have was a strong monastic training.

133
00:06:15,640 --> 00:06:19,130
 Actually I was able to make that up on my own by reading

134
00:06:19,130 --> 00:06:21,400
 the books and so even to this

135
00:06:21,400 --> 00:06:24,120
 day I don't mind being in that monastery surrounded by

136
00:06:24,120 --> 00:06:26,280
 monks who may not be keeping the rules.

137
00:06:26,280 --> 00:06:29,940
 It doesn't really bother me because I have the focus on the

138
00:06:29,940 --> 00:06:31,800
 meditation which allows me

139
00:06:31,800 --> 00:06:34,870
 to stay to myself and do my own discipline and not care

140
00:06:34,870 --> 00:06:36,280
 what other people do.

141
00:06:36,280 --> 00:06:39,640
 That's so much.

142
00:06:39,640 --> 00:06:43,650
 So that's one thing, is to focus more on the meditation

143
00:06:43,650 --> 00:06:46,000
 practice than on the discipline.

144
00:06:46,000 --> 00:06:49,410
 Does this place have a strong meditative tradition instead

145
00:06:49,410 --> 00:06:51,360
 of does it have a strong monastic

146
00:06:51,360 --> 00:06:53,360
 discipline?

147
00:06:53,360 --> 00:06:59,040
 But best is if you can find a place where they teach both.

148
00:06:59,040 --> 00:07:02,300
 I understand that Wat Panana Chad is a good place

149
00:07:02,300 --> 00:07:05,160
 especially to learn monastic discipline

150
00:07:05,160 --> 00:07:08,380
 but the best places that I've seen are in Sri Lanka.

151
00:07:08,380 --> 00:07:11,190
 If you want to ordain as a monk, if anyone wants to ordain

152
00:07:11,190 --> 00:07:12,600
 as a monk, your best bet is

153
00:07:12,600 --> 00:07:13,600
 to go to Sri Lanka.

154
00:07:13,600 --> 00:07:21,980
 Now the problem with going to Asia at all to become a monk

155
00:07:21,980 --> 00:07:25,840
 is that the general routine

156
00:07:25,840 --> 00:07:30,000
 is you have to actually go once, practice with them and

157
00:07:30,000 --> 00:07:32,160
 then get a promise from them

158
00:07:32,160 --> 00:07:34,700
 that they're going to write you a letter or whatever and

159
00:07:34,700 --> 00:07:36,520
 then you actually have to return

160
00:07:36,520 --> 00:07:43,030
 to your country and apply for a new visa with a letter of

161
00:07:43,030 --> 00:07:46,920
 permission to become a monk and

162
00:07:46,920 --> 00:07:51,030
 then fly back to the country again a second time to

163
00:07:51,030 --> 00:07:54,040
 actually think about ordaining because

164
00:07:54,040 --> 00:07:59,220
 you need a special visa to ordain and the only way you can

165
00:07:59,220 --> 00:08:01,800
 get that visa is with a letter

166
00:08:01,800 --> 00:08:04,500
 of permission from the monastery and the only way they're

167
00:08:04,500 --> 00:08:06,140
 going to give you that permission

168
00:08:06,140 --> 00:08:09,760
 is if you're already in the country practicing with them.

169
00:08:09,760 --> 00:08:12,880
 It's almost a catch-22 but what it means is you need to go

170
00:08:12,880 --> 00:08:14,360
 twice so that's what you have

171
00:08:14,360 --> 00:08:15,360
 to look at there.

172
00:08:15,360 --> 00:08:17,910
 An alternative is to find one of these places that does

173
00:08:17,910 --> 00:08:19,400
 keep them in North America.

174
00:08:19,400 --> 00:08:22,490
 Now they're usually full because obviously everybody wants

175
00:08:22,490 --> 00:08:23,040
 those.

176
00:08:23,040 --> 00:08:26,180
 There's what Abiyagiri in California, there's Bawana

177
00:08:26,180 --> 00:08:28,320
 Society, I don't know how strict they

178
00:08:28,320 --> 00:08:31,720
 are but they're probably fairly good.

179
00:08:31,720 --> 00:08:37,930
 There's Meta Monastery in San Diego and in general check

180
00:08:37,930 --> 00:08:41,120
 out the, oh I don't know, I

181
00:08:41,120 --> 00:08:44,120
 don't want to say that but no, I'm not going to say it.

182
00:08:44,120 --> 00:08:47,680
 Those are my only suggestions.

183
00:08:47,680 --> 00:08:50,510
 You're welcome to come here, you're going to see some of

184
00:08:50,510 --> 00:08:52,320
 the, but the USA is still difficult

185
00:08:52,320 --> 00:08:55,220
 for us because you still need a visa coming here and I don

186
00:08:55,220 --> 00:08:57,160
't even think that really works,

187
00:08:57,160 --> 00:09:00,620
 not over the long term.

188
00:09:00,620 --> 00:09:05,250
 But find a place that is focused on meditation because as I

189
00:09:05,250 --> 00:09:07,560
 said that can be another way

190
00:09:07,560 --> 00:09:09,560
 to center yourself.

191
00:09:09,560 --> 00:09:12,140
 The Buddha himself even said a lot of these rules, if you

192
00:09:12,140 --> 00:09:13,480
're practicing meditation you

193
00:09:13,480 --> 00:09:15,080
 don't really have to worry about the rules.

194
00:09:15,080 --> 00:09:19,120
 If you happen to break one it's not such a big deal.

195
00:09:19,120 --> 00:09:21,740
 But if you're in a place that's not practicing meditation,

196
00:09:21,740 --> 00:09:23,200
 in fact if you're in a place that's

197
00:09:23,200 --> 00:09:26,830
 not practicing insight meditation even if they have a

198
00:09:26,830 --> 00:09:29,160
 strong monastic practice it's still

199
00:09:29,160 --> 00:09:31,760
 not going to lead to enlightenment.

200
00:09:31,760 --> 00:09:34,680
 So focus more on the practice, that will give you an

201
00:09:34,680 --> 00:09:37,240
 ability to cultivate the moral discipline

202
00:09:37,240 --> 00:09:38,240
 yourself.

203
00:09:38,240 --> 00:09:42,100
 If without the practice it's very difficult to cultivate

204
00:09:42,100 --> 00:09:43,600
 moral discipline.

205
00:09:43,600 --> 00:09:46,890
 But vice versa, without the moral discipline if you're

206
00:09:46,890 --> 00:09:49,000
 breaking the moral discipline also

207
00:09:49,000 --> 00:09:52,080
 difficult.

208
00:09:52,080 --> 00:09:56,520
 But I still lean towards the meditation.

209
00:09:56,520 --> 00:09:59,800
 If you can find that you can make up the monastic

210
00:09:59,800 --> 00:10:01,640
 discipline yourself.

211
00:10:01,640 --> 00:10:05,460
 Don't rely too much on other people for that because it's

212
00:10:05,460 --> 00:10:06,520
 just rules.

213
00:10:06,520 --> 00:10:13,160
 So focus more, not completely, but more on the meditation.

