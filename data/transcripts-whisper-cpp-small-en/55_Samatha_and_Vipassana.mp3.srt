1
00:00:00,000 --> 00:00:08,960
 other types of meditation, kyone. Samatha and Vipassana

2
00:00:08,960 --> 00:00:13,990
 Question. I started with samatha meditation as I read a lot

3
00:00:13,990 --> 00:00:16,240
 that you have to calm your mind before

4
00:00:16,240 --> 00:00:22,140
 starting to practice vipassana. What is your interpretation

5
00:00:22,140 --> 00:00:24,240
 of the differences between these

6
00:00:24,240 --> 00:00:32,320
 two and which should we practice first? Answer. Samatha

7
00:00:32,320 --> 00:00:36,080
 means tranquility, vipassana means

8
00:00:36,080 --> 00:00:41,560
 seeing clearly. The Buddha used these terms together and

9
00:00:41,560 --> 00:00:44,320
 individually as qualities of mind.

10
00:00:44,320 --> 00:00:48,630
 When you talk about samatha meditation as something

11
00:00:48,630 --> 00:00:51,600
 separate from vipassana meditation,

12
00:00:52,240 --> 00:00:56,560
 you are taking the discussion into a post-canonical realm.

13
00:00:56,560 --> 00:01:00,320
 You are taking it out of the realm of what

14
00:01:00,320 --> 00:01:03,640
 the Buddha actually said and into the realm of

15
00:01:03,640 --> 00:01:09,760
 interpretation. I follow the orthodox interpretation

16
00:01:09,760 --> 00:01:13,450
 that does talk about samatha meditation and vipassana

17
00:01:13,450 --> 00:01:18,160
 meditation, so I do not mind talking about this.

18
00:01:20,400 --> 00:01:24,050
 For those people who advise to practice calming your mind

19
00:01:24,050 --> 00:01:26,880
 first, samatha meditation, and then

20
00:01:26,880 --> 00:01:32,630
 practicing to see clearly after vipassana meditation, this

21
00:01:32,630 --> 00:01:35,120
 means first entering into a

22
00:01:35,120 --> 00:01:41,460
 trance wherein you can see one thing very clearly. This is

23
00:01:41,460 --> 00:01:44,800
 only possible if that one thing is a

24
00:01:44,800 --> 00:01:50,630
 concept. Since concepts are lasting entities, the mind can

25
00:01:50,630 --> 00:01:55,280
 focus on at length. Understanding the

26
00:01:55,280 --> 00:02:01,520
 distinction between reality and concept is important. It

27
00:02:01,520 --> 00:02:03,840
 allows us to determine whether

28
00:02:03,840 --> 00:02:08,260
 a meditation practice will lead to a trance state or a

29
00:02:08,260 --> 00:02:10,800
 clear understanding of reality.

30
00:02:11,920 --> 00:02:17,070
 Focusing on concepts will not help you understand reality

31
00:02:17,070 --> 00:02:21,040
 because you are focusing on something you

32
00:02:21,040 --> 00:02:26,880
 have created in your mind. It is not real. It does not have

33
00:02:26,880 --> 00:02:30,400
 any of the qualities of ultimate reality.

34
00:02:30,400 --> 00:02:35,960
 It is not the same as watching your stomach rise and fall

35
00:02:35,960 --> 00:02:39,440
 because when you watch your stomach rise

36
00:02:39,440 --> 00:02:44,090
 and fall it changes constantly. The movements of the

37
00:02:44,090 --> 00:02:48,400
 stomach are impermanent, unsatisfying, and

38
00:02:48,400 --> 00:02:53,520
 uncontrollable. Watching the stomach helps you to see

39
00:02:53,520 --> 00:02:57,440
 reality as it is, and that is difficult.

40
00:02:57,440 --> 00:03:02,830
 It is much easier and pleasant to create a concept in your

41
00:03:02,830 --> 00:03:07,760
 mind. For example, you can close your eyes

42
00:03:07,760 --> 00:03:12,560
 and at your third eye in the center of your forehead

43
00:03:12,560 --> 00:03:16,400
 imagine a color and you say to yourself

44
00:03:16,400 --> 00:03:23,740
 blue, blue, or red, red, or white, white, just focusing on

45
00:03:23,740 --> 00:03:26,480
 the color you have imagined.

46
00:03:26,480 --> 00:03:32,200
 Once you can fix your mind only on the color thinking of

47
00:03:32,200 --> 00:03:33,680
 nothing else,

48
00:03:34,960 --> 00:03:38,970
 that is considered to be an attainment of the fruit of sam

49
00:03:38,970 --> 00:03:40,480
atha meditation.

50
00:03:40,480 --> 00:03:47,080
 Many religious traditions, especially in India, make use of

51
00:03:47,080 --> 00:03:50,400
 this sort of practice for the purpose

52
00:03:50,400 --> 00:03:55,290
 of gaining spiritual powers like remembering past lives,

53
00:03:55,290 --> 00:03:59,840
 reading people's minds, seeing heaven and

54
00:03:59,840 --> 00:04:06,560
 hell, seeing the future, and leaving your body. To make use

55
00:04:06,560 --> 00:04:10,400
 of samatha meditation for the subsequent

56
00:04:10,400 --> 00:04:16,240
 development of vipassana, you simply apply the principles

57
00:04:16,240 --> 00:04:19,520
 of satipatthana practice to the samatha

58
00:04:19,520 --> 00:04:26,210
 experiences. For example, you can note the focused mind as

59
00:04:26,210 --> 00:04:30,880
 focused focused. If the experience is

60
00:04:30,880 --> 00:04:39,640
 pleasurable, you can note happy happy. If you feel calm,

61
00:04:39,640 --> 00:04:44,960
 calm calm. If the experience involves lights

62
00:04:44,960 --> 00:04:52,040
 or colors, you note seeing seeing etc. And as a result of

63
00:04:52,040 --> 00:04:56,800
 the mindful interaction, the experience

64
00:04:56,800 --> 00:05:02,360
 will change from seeing the stable satisfying and controll

65
00:05:02,360 --> 00:05:06,480
able concept to the impermanent unsatisfying

66
00:05:06,480 --> 00:05:12,250
 and uncontrollable reality. Because samatha meditation

67
00:05:12,250 --> 00:05:15,920
 involves the cultivation of powerful

68
00:05:15,920 --> 00:05:21,360
 mental faculties, when you switch to vipassana meditation,

69
00:05:21,360 --> 00:05:26,080
 your mind is readily able to see

70
00:05:26,080 --> 00:05:31,560
 clearly the nature of reality. For someone who does not

71
00:05:31,560 --> 00:05:34,160
 take the time to practice samatha

72
00:05:34,160 --> 00:05:39,480
 meditation first, vipassana meditation can be a challenge

73
00:05:39,480 --> 00:05:42,320
 because the mind does not have the

74
00:05:42,320 --> 00:05:49,040
 prior training or on more pleasurable objects. You have to

75
00:05:49,040 --> 00:05:53,360
 face pain in the body, distractions in the

76
00:05:53,360 --> 00:05:58,390
 mind, and all kinds of emotions without any of the peace

77
00:05:58,390 --> 00:06:01,600
 and calm of samatha meditation.

78
00:06:03,040 --> 00:06:07,650
 For the most part, this means only that the meditation

79
00:06:07,650 --> 00:06:10,320
 practice is less pleasurable,

80
00:06:10,320 --> 00:06:16,510
 not slower or less effective. In fact, by starting with vip

81
00:06:16,510 --> 00:06:18,560
assana meditation,

82
00:06:18,560 --> 00:06:25,580
 one avoids the potential pitfalls of becoming complacent or

83
00:06:25,580 --> 00:06:27,840
 conceited about one's

84
00:06:27,840 --> 00:06:33,500
 samatha-based meditative attainment as their training of

85
00:06:33,500 --> 00:06:36,080
 the mind begins with ultimate

86
00:06:36,080 --> 00:06:40,810
 reality directly. The difference between the vipassana

87
00:06:40,810 --> 00:06:43,760
 meditation and samatha meditation is

88
00:06:43,760 --> 00:06:49,540
 quite profound. Samatha meditation is for the purpose of

89
00:06:49,540 --> 00:06:52,560
 calming the mind whereas vipassana

90
00:06:52,560 --> 00:06:57,670
 meditation is for the purpose of seeing clearly. This does

91
00:06:57,670 --> 00:07:00,800
 not mean that vipassana meditation does

92
00:07:00,800 --> 00:07:06,450
 not lead to tranquility. You should find that through the

93
00:07:06,450 --> 00:07:09,600
 practice of vipassana meditation,

94
00:07:09,600 --> 00:07:15,500
 you are much calmer in general. Vipassana meditation is

95
00:07:15,500 --> 00:07:18,240
 differentiated from samatha

96
00:07:18,240 --> 00:07:23,780
 meditation not because it does not involve tranquility but

97
00:07:23,780 --> 00:07:26,720
 because tranquility is not the

98
00:07:26,720 --> 00:07:31,430
 goal. Whether you feel calm or not is not practically

99
00:07:31,430 --> 00:07:34,880
 important. All that is important

100
00:07:34,880 --> 00:07:41,610
 is that you see your state of calm or distress clearly. Sam

101
00:07:41,610 --> 00:07:46,320
atha meditation cannot lead directly

102
00:07:46,320 --> 00:07:51,860
 to enlightenment because it is not focused on reality. The

103
00:07:51,860 --> 00:07:54,880
 only way that it can be used to attain

104
00:07:54,880 --> 00:08:00,010
 enlightenment is if you follow it up as stated with vipass

105
00:08:00,010 --> 00:08:04,640
ana meditation. The strength of mind

106
00:08:04,640 --> 00:08:09,730
 gained from practicing samatha meditation allows you to see

107
00:08:09,730 --> 00:08:12,560
 clearer than you would have otherwise.

108
00:08:14,160 --> 00:08:18,910
 Since the same can be said of developing mental fortitude

109
00:08:18,910 --> 00:08:21,200
 starting with vipassana meditation

110
00:08:21,200 --> 00:08:25,850
 directly, the only real benefit to practicing samatha

111
00:08:25,850 --> 00:08:29,280
 meditation first is the state of peace

112
00:08:29,280 --> 00:08:33,540
 and calm and maybe spiritual powers if you dedicate

113
00:08:33,540 --> 00:08:36,480
 yourself to it to a great degree.

114
00:08:39,120 --> 00:08:43,680
 It is in fact clear from the text that starting with vipass

115
00:08:43,680 --> 00:08:46,320
ana is the quicker option of the two.

116
00:08:46,320 --> 00:08:52,400
 A monk once approached the Buddha and said, "Venerable sir,

117
00:08:52,400 --> 00:08:56,160
 I am old. I do not have much

118
00:08:56,160 --> 00:09:00,960
 time left and my memory is not good. I am not able to learn

119
00:09:00,960 --> 00:09:04,960
 everything. Please teach me the basics

120
00:09:04,960 --> 00:09:10,020
 of the path." The Buddha told him that first he should

121
00:09:10,020 --> 00:09:13,360
 establish himself in an intellectual

122
00:09:13,360 --> 00:09:19,000
 acceptance of right view and purify his morality to make

123
00:09:19,000 --> 00:09:22,560
 sure he is ethical and moral in this

124
00:09:22,560 --> 00:09:27,630
 behavior. Once he has done that he has done enough to begin

125
00:09:27,630 --> 00:09:30,560
 practicing the four foundations

126
00:09:30,560 --> 00:09:36,430
 of mindfulness. There is no talk about first focusing the

127
00:09:36,430 --> 00:09:40,080
 mind or entering into any meditative

128
00:09:40,080 --> 00:09:47,670
 state before focusing on reality. In mindfulness one access

129
00:09:47,670 --> 00:09:51,280
es meditative states naturally

130
00:09:51,280 --> 00:09:56,530
 entering into them based on reality. Even just focusing on

131
00:09:56,530 --> 00:09:59,600
 the rising falling can be considered

132
00:09:59,600 --> 00:10:04,480
 an absorbed state for the moment that one is observing it.

133
00:10:04,480 --> 00:10:07,920
 When you say stepping right,

134
00:10:07,920 --> 00:10:13,310
 stepping left in walking meditation if your mind is clearly

135
00:10:13,310 --> 00:10:16,880
 aware of the movement you are

136
00:10:16,880 --> 00:10:21,390
 in an absorbed state for that moment. So you are

137
00:10:21,390 --> 00:10:26,000
 cultivating both samatha and vipassana at once.

138
00:10:27,120 --> 00:10:32,720
 You are tranquil and you are also seen clearly.

