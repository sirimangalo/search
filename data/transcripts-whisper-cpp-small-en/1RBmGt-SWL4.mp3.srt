1
00:00:00,000 --> 00:00:05,650
 Hi, and welcome to this series of videos on why everyone

2
00:00:05,650 --> 00:00:09,840
 should practice meditation.

3
00:00:09,840 --> 00:00:14,150
 This series is meant to be an addition to my series on how

4
00:00:14,150 --> 00:00:15,440
 to meditate.

5
00:00:15,440 --> 00:00:20,210
 And it was brought about as a result of many comments or

6
00:00:20,210 --> 00:00:23,400
 questions about not how to meditate

7
00:00:23,400 --> 00:00:27,970
 but why or whether ordinary people should meditate, whether

8
00:00:27,970 --> 00:00:29,840
 it's something just for

9
00:00:29,840 --> 00:00:32,540
 monks or whether it's something that is applicable to

10
00:00:32,540 --> 00:00:35,640
 people living ordinary everyday lives.

11
00:00:35,640 --> 00:00:39,600
 And so these videos are an attempt to answer that question

12
00:00:39,600 --> 00:00:41,840
 and give a response, "Yes."

13
00:00:41,840 --> 00:00:46,210
 That these videos are certainly not simply for people who

14
00:00:46,210 --> 00:00:48,760
 have left the everyday ordinary

15
00:00:48,760 --> 00:00:53,100
 life to live in the forest or so on, that these meditation

16
00:00:53,100 --> 00:00:55,920
 techniques are totally applicable

17
00:00:55,920 --> 00:00:58,520
 to everyday life.

18
00:00:58,520 --> 00:01:01,940
 So these are the top five reasons why everyone should

19
00:01:01,940 --> 00:01:03,680
 practice meditation.

20
00:01:03,680 --> 00:01:07,520
 And the first reason is because meditation is something

21
00:01:07,520 --> 00:01:09,480
 which purifies the mind.

22
00:01:09,480 --> 00:01:14,090
 When you practice meditation, it brings about a state of

23
00:01:14,090 --> 00:01:17,080
 purity and clarity in one's mind.

24
00:01:17,080 --> 00:01:21,770
 Now this is when we look at the ordinary state of being,

25
00:01:21,770 --> 00:01:24,960
 the ordinary state of our minds,

26
00:01:24,960 --> 00:01:27,540
 we can see that in our own minds if we're willing to be

27
00:01:27,540 --> 00:01:29,000
 honest with ourselves that we

28
00:01:29,000 --> 00:01:33,760
 have many things which we would rather do without.

29
00:01:33,760 --> 00:01:38,950
 Things like anger, things like greed or addiction, things

30
00:01:38,950 --> 00:01:42,480
 like worry, stress, things like fear,

31
00:01:42,480 --> 00:01:48,590
 anxiety, and many things which we would maybe be able to

32
00:01:48,590 --> 00:01:52,120
 admit to ourselves are a cause for

33
00:01:52,120 --> 00:01:55,460
 suffering for ourselves, a cause for suffering for other

34
00:01:55,460 --> 00:01:56,160
 people.

35
00:01:56,160 --> 00:01:59,520
 These sort of things that when people look at us, they are

36
00:01:59,520 --> 00:02:01,160
 turned off by when they see

37
00:02:01,160 --> 00:02:04,000
 in us, when they see that we're conceited, when we see that

38
00:02:04,000 --> 00:02:05,360
 we are opinionated, when

39
00:02:05,360 --> 00:02:07,820
 they see that we are jealous, when they see that we are

40
00:02:07,820 --> 00:02:09,280
 stingy, when they see that we

41
00:02:09,280 --> 00:02:13,990
 have all these qualities, they feel turned off by these

42
00:02:13,990 --> 00:02:14,960
 things.

43
00:02:14,960 --> 00:02:17,120
 And we ourselves suffer from them.

44
00:02:17,120 --> 00:02:19,840
 When we have these qualities of mind, it creates suffering

45
00:02:19,840 --> 00:02:21,880
 for ourselves, it also creates suffering

46
00:02:21,880 --> 00:02:23,680
 for the people around us.

47
00:02:23,680 --> 00:02:26,440
 These are all what we call the defilements of the mind,

48
00:02:26,440 --> 00:02:28,520
 these are things which are undesirable

49
00:02:28,520 --> 00:02:30,840
 qualities of mind.

50
00:02:30,840 --> 00:02:34,830
 Now normally we look at these things and we rationalize

51
00:02:34,830 --> 00:02:37,160
 them in many different ways.

52
00:02:37,160 --> 00:02:40,620
 For instance, we say to ourselves, "Well, it's just a part

53
00:02:40,620 --> 00:02:41,560
 of who I am."

54
00:02:41,560 --> 00:02:44,600
 Or we say, "These things are just human.

55
00:02:44,600 --> 00:02:46,760
 I'm only human."

56
00:02:46,760 --> 00:02:50,230
 And so we say, "If we didn't have these things, then we

57
00:02:50,230 --> 00:02:52,240
 would be somehow inhuman."

58
00:02:52,240 --> 00:02:55,540
 And we say that these things are unavoidable, that it's

59
00:02:55,540 --> 00:02:57,520
 just who I am, that I can't change

60
00:02:57,520 --> 00:03:00,840
 who I am, and so on.

61
00:03:00,840 --> 00:03:03,880
 And the truth is very much different from what we think.

62
00:03:03,880 --> 00:03:07,780
 And it's very clear that people are able to live without

63
00:03:07,780 --> 00:03:10,080
 these things, are able to train

64
00:03:10,080 --> 00:03:12,480
 themselves out of these things.

65
00:03:12,480 --> 00:03:15,550
 So if we simply say that these things are a part of who I

66
00:03:15,550 --> 00:03:17,160
 am, it's clear that we're

67
00:03:17,160 --> 00:03:18,440
 only saying this out of ignorance.

68
00:03:18,440 --> 00:03:21,260
 We're saying this because we've never tried or we've never

69
00:03:21,260 --> 00:03:22,600
 been given the tools with which

70
00:03:22,600 --> 00:03:24,280
 to do away with them.

71
00:03:24,280 --> 00:03:26,630
 So when people are angry, they say, "Well, I'm just an

72
00:03:26,630 --> 00:03:27,440
 angry person."

73
00:03:27,440 --> 00:03:29,520
 But they've never really looked at the anger to try to

74
00:03:29,520 --> 00:03:30,920
 understand it and to see where it

75
00:03:30,920 --> 00:03:34,580
 comes from and to see the disadvantages of it and to really

76
00:03:34,580 --> 00:03:36,640
 teach themselves and to create

77
00:03:36,640 --> 00:03:40,000
 an understanding in their minds to the disadvantages of

78
00:03:40,000 --> 00:03:42,680
 things like anger or things like craving,

79
00:03:42,680 --> 00:03:46,120
 things like addiction and so on.

80
00:03:46,120 --> 00:03:49,260
 When we practice meditation, we actually have the ability

81
00:03:49,260 --> 00:03:50,840
 to overcome these things.

82
00:03:50,840 --> 00:03:53,080
 And how we do it is we look at them.

83
00:03:53,080 --> 00:03:55,080
 Because we can say for ourselves, "Anger is not a good

84
00:03:55,080 --> 00:03:55,520
 thing.

85
00:03:55,520 --> 00:03:57,520
 I wish I wasn't so angry."

86
00:03:57,520 --> 00:04:00,480
 But we aren't actually looking at the anger and we don't

87
00:04:00,480 --> 00:04:02,280
 actually realize for ourselves

88
00:04:02,280 --> 00:04:05,720
 on an experiential level that anger is a bad thing.

89
00:04:05,720 --> 00:04:07,830
 At the moment when we look at the anger, when we focus on

90
00:04:07,830 --> 00:04:09,720
 the anger, when we say to ourselves,

91
00:04:09,720 --> 00:04:12,790
 "Angry," just reminding ourselves of what's going on right

92
00:04:12,790 --> 00:04:13,240
 now.

93
00:04:13,240 --> 00:04:17,720
 We say to ourselves, "Angry, angry, angry," for example.

94
00:04:17,720 --> 00:04:19,970
 We're able to see clearly the anger and sort of teach

95
00:04:19,970 --> 00:04:20,680
 ourselves.

96
00:04:20,680 --> 00:04:23,430
 It's like teaching ourselves something that we already know

97
00:04:23,430 --> 00:04:23,640
.

98
00:04:23,640 --> 00:04:25,040
 We know anger is not a good thing.

99
00:04:25,040 --> 00:04:28,140
 But when we look at it, we really see for ourselves that it

100
00:04:28,140 --> 00:04:29,440
's not a good thing and we're

101
00:04:29,440 --> 00:04:32,080
 able to do away with it in the here and now.

102
00:04:32,080 --> 00:04:36,120
 When we have any sort of stress or worry or frustration, we

103
00:04:36,120 --> 00:04:38,320
 can do the exact same thing.

104
00:04:38,320 --> 00:04:41,050
 So when we practice meditation, we're able to do away with

105
00:04:41,050 --> 00:04:41,920
 these things.

106
00:04:41,920 --> 00:04:44,510
 The other thing that people often say is, "If I didn't have

107
00:04:44,510 --> 00:04:45,880
 these things, who would I

108
00:04:45,880 --> 00:04:46,880
 be?

109
00:04:46,880 --> 00:04:49,000
 I'd be somehow inhuman."

110
00:04:49,000 --> 00:04:55,520
 I think this is sort of an argumentative sort of thing to

111
00:04:55,520 --> 00:04:59,240
 say that actually there's no reason

112
00:04:59,240 --> 00:05:01,800
 for us to say that negative things are somehow human.

113
00:05:01,800 --> 00:05:05,240
 In fact, you can just as easily say that these are inhuman.

114
00:05:05,240 --> 00:05:09,840
 When we have cruelty or addiction or jealousy or stinginess

115
00:05:09,840 --> 00:05:12,240
 or conceit or opinion, all of

116
00:05:12,240 --> 00:05:16,160
 these things, we can easily say that these things are what

117
00:05:16,160 --> 00:05:17,480
 make us inhuman.

118
00:05:17,480 --> 00:05:21,230
 If we really wanted to be human or humane, then we would

119
00:05:21,230 --> 00:05:23,160
 have to develop only the good

120
00:05:23,160 --> 00:05:26,090
 things and we should really try to do away with the bad

121
00:05:26,090 --> 00:05:26,720
 things.

122
00:05:26,720 --> 00:05:29,560
 There's no reason why we should have to have these things.

123
00:05:29,560 --> 00:05:31,800
 There's nothing intrinsic about them at all.

124
00:05:31,800 --> 00:05:35,030
 In fact, there are things which we have developed over the

125
00:05:35,030 --> 00:05:37,440
 years, over the course of our lifetime.

126
00:05:37,440 --> 00:05:42,470
 As we get older and we get more bitter, we get more angry,

127
00:05:42,470 --> 00:05:45,480
 or as we indulge in sensuality,

128
00:05:45,480 --> 00:05:47,880
 we get addicted to things more and more.

129
00:05:47,880 --> 00:05:50,780
 It's clear that these are not intrinsic in any way to who

130
00:05:50,780 --> 00:05:52,600
 we are and they can be changed.

131
00:05:52,600 --> 00:05:55,640
 Just as we can become more angry, we can also become less

132
00:05:55,640 --> 00:05:56,200
 angry.

133
00:05:56,200 --> 00:05:57,600
 We can train ourselves out of these.

134
00:05:57,600 --> 00:06:00,280
 This is the first benefit of practicing meditation.

135
00:06:00,280 --> 00:06:03,900
 The number five, counting down, is that meditation helps to

136
00:06:03,900 --> 00:06:05,960
 make our mind pure because it helps

137
00:06:05,960 --> 00:06:08,930
 us to understand and to see the difference between good

138
00:06:08,930 --> 00:06:10,520
 states of mind and bad states

139
00:06:10,520 --> 00:06:14,270
 of mind, good qualities and bad qualities. It helps us to

140
00:06:14,270 --> 00:06:16,040
 learn how to develop only good

141
00:06:16,040 --> 00:06:19,940
 qualities because we see clearly the difference and what is

142
00:06:19,940 --> 00:06:22,000
 the result of carrying around

143
00:06:22,000 --> 00:06:24,360
 good things and the result of carrying around bad things.

144
00:06:24,360 --> 00:06:28,000
 We can see it's an entirely different thing.

145
00:06:28,000 --> 00:06:29,000
 That's number five.

146
00:06:29,000 --> 00:06:31,850
 From now on, I'll be counting down, going on to number four

147
00:06:31,850 --> 00:06:33,040
 in the near future.

148
00:06:33,040 --> 00:06:34,040
 Thanks for tuning in.

