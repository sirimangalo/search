1
00:00:00,000 --> 00:00:08,720
 Okay, good morning.

2
00:00:08,720 --> 00:00:14,760
 Broadcasting live from Kyung Mai again.

3
00:00:14,760 --> 00:00:19,110
 Heading off to Bangkok this evening and then Sri Lanka on

4
00:00:19,110 --> 00:00:20,760
 the 20th.

5
00:00:20,760 --> 00:00:31,990
 Heading back in Thailand January 19th and then staying,

6
00:00:31,990 --> 00:00:33,600
 giving courses somewhere in

7
00:00:33,600 --> 00:00:48,280
 the forest until the 19th of February and back again.

8
00:00:48,280 --> 00:00:55,940
 I was, I heard a couple of things recently that made me

9
00:00:55,940 --> 00:01:00,040
 think about the issue of truth

10
00:01:00,040 --> 00:01:09,480
 and right view, subjectivity, objectivity.

11
00:01:09,480 --> 00:01:16,920
 There's a common theme in society, in our dealings with our

12
00:01:16,920 --> 00:01:21,760
 fellow human beings, which

13
00:01:21,760 --> 00:01:36,350
 concerns the, what I believe is a postmodern concept of

14
00:01:36,350 --> 00:01:44,840
 subjective truth, relative truth,

15
00:01:44,840 --> 00:01:55,320
 and circumstantial truth or right, that's expressed by the

16
00:01:55,320 --> 00:01:59,720
 idea to each their own.

17
00:01:59,720 --> 00:02:06,500
 Or, I believe that the idiom in the West, which doesn't

18
00:02:06,500 --> 00:02:10,040
 sound, I'm not sure exactly

19
00:02:10,040 --> 00:02:14,510
 what it's referring to, but different folks, different

20
00:02:14,510 --> 00:02:17,920
 strokes or different strokes, different

21
00:02:17,920 --> 00:02:22,100
 folks, I'm not quite sure exactly what that's referring to,

22
00:02:22,100 --> 00:02:23,480
 it doesn't sound.

23
00:02:23,480 --> 00:02:30,720
 Anyway, I won't touch that one.

24
00:02:30,720 --> 00:02:35,740
 That kind of thing to each their own, meaning, it can mean

25
00:02:35,740 --> 00:02:37,560
 different things.

26
00:02:37,560 --> 00:02:44,100
 The sense I get is people use this as a means of avoiding

27
00:02:44,100 --> 00:02:49,360
 conflict, where views are concerned.

28
00:02:49,360 --> 00:02:52,900
 They people, ordinary people in the world tend to be very

29
00:02:52,900 --> 00:02:54,840
 good at avoiding conflict.

30
00:02:54,840 --> 00:02:58,880
 They have to be otherwise they can't get along, they can't

31
00:02:58,880 --> 00:03:00,640
 get along in society.

32
00:03:00,640 --> 00:03:03,900
 So the conflict doesn't occur at the level of views because

33
00:03:03,900 --> 00:03:05,320
 people keep their views to

34
00:03:05,320 --> 00:03:06,320
 themselves.

35
00:03:06,320 --> 00:03:08,120
 It's not worth it.

36
00:03:08,120 --> 00:03:12,630
 And they don't hold the views very strongly because they're

37
00:03:12,630 --> 00:03:14,960
 more concerned with material

38
00:03:14,960 --> 00:03:23,000
 advancement and sufficiency.

39
00:03:23,000 --> 00:03:27,690
 So where they conflict is more in the realm of sensuality,

40
00:03:27,690 --> 00:03:29,880
 is they get caught up in, tend

41
00:03:29,880 --> 00:03:34,170
 to get caught up in sensual pleasures and as a result this

42
00:03:34,170 --> 00:03:36,360
 leads to conflict as a matter

43
00:03:36,360 --> 00:03:42,000
 of course fighting and bickering and arguing and wrangling

44
00:03:42,000 --> 00:03:44,800
 for competing for resources

45
00:03:44,800 --> 00:03:48,400
 and so on.

46
00:03:48,400 --> 00:03:54,080
 But for religious people, views are much more important and

47
00:03:54,080 --> 00:03:56,080
 define one's life.

48
00:03:56,080 --> 00:03:59,290
 And for those of us who tend to be a little bit antisocial,

49
00:03:59,290 --> 00:04:01,080
 sticking to ourselves, they

50
00:04:01,080 --> 00:04:04,860
 can become a sticking point when we get together because we

51
00:04:04,860 --> 00:04:07,040
 are much more absorbed in our own

52
00:04:07,040 --> 00:04:09,760
 views and our own paths.

53
00:04:09,760 --> 00:04:13,990
 So when we meet someone with a different path, they say

54
00:04:13,990 --> 00:04:16,840
 that religious people tend to fight

55
00:04:16,840 --> 00:04:21,840
 more over views and opinions.

56
00:04:21,840 --> 00:04:30,440
 And even are expected to debate and stick to their views.

57
00:04:30,440 --> 00:04:33,560
 Views are more important.

58
00:04:33,560 --> 00:04:35,560
 Beliefs and so on.

59
00:04:35,560 --> 00:04:39,160
 And so it's interesting to hear lay people talk about these

60
00:04:39,160 --> 00:04:40,800
 things and they say things

61
00:04:40,800 --> 00:04:49,880
 like tō kōng kō, it means it's right to them, it's right

62
00:04:49,880 --> 00:04:50,560
.

63
00:04:50,560 --> 00:04:56,280
 It's true for them, right for them.

64
00:04:56,280 --> 00:05:02,880
 Or kōm su kōng hao, that's where they find happiness.

65
00:05:02,880 --> 00:05:08,170
 Or that's their happiness, meaning you shouldn't judge them

66
00:05:08,170 --> 00:05:10,000
 to each their own.

67
00:05:10,000 --> 00:05:18,480
 You'll find happiness in different ways.

68
00:05:18,480 --> 00:05:23,020
 The interesting thing here from a Buddhist point of view is

69
00:05:23,020 --> 00:05:25,800
 that it's at once philosophically

70
00:05:25,800 --> 00:05:29,960
 useful and dangerous.

71
00:05:29,960 --> 00:05:34,470
 It's useful in the sense of reminding us that it's not

72
00:05:34,470 --> 00:05:38,120
 exactly that Buddhism tells you what

73
00:05:38,120 --> 00:05:45,280
 you should do or even what is right, not exactly.

74
00:05:45,280 --> 00:05:51,000
 Even though the Buddha used the words right and true and

75
00:05:51,000 --> 00:05:54,440
 truth, or right anyway, let's

76
00:05:54,440 --> 00:05:58,840
 not worry about truth yet, but talking about what's right,

77
00:05:58,840 --> 00:06:00,720
 the Buddha seems to be quite

78
00:06:00,720 --> 00:06:04,240
 open about the idea of there being different paths that

79
00:06:04,240 --> 00:06:06,080
 lead to different goals.

80
00:06:06,080 --> 00:06:10,030
 And in a sense, though I can't think of a place where he

81
00:06:10,030 --> 00:06:12,000
 actually said it, he seems

82
00:06:12,000 --> 00:06:15,450
 to have been quite open to the idea of to each their own in

83
00:06:15,450 --> 00:06:16,960
 the sense that he would

84
00:06:16,960 --> 00:06:21,400
 often just explain to people, "There's this path and it

85
00:06:21,400 --> 00:06:23,880
 leads here, there's that path

86
00:06:23,880 --> 00:06:25,520
 and it leads there."

87
00:06:25,520 --> 00:06:28,370
 And most often he wouldn't even say, "This path is better,

88
00:06:28,370 --> 00:06:29,640
 that path is better."

89
00:06:29,640 --> 00:06:32,560
 At times he did.

90
00:06:32,560 --> 00:06:37,560
 But for the most part he was quite open and quite happy to

91
00:06:37,560 --> 00:06:40,040
 just leave it to each their

92
00:06:40,040 --> 00:06:41,040
 own.

93
00:06:41,040 --> 00:06:42,040
 What would you choose?

94
00:06:42,040 --> 00:06:43,680
 "This path you do, it leads you to hell.

95
00:06:43,680 --> 00:06:45,800
 This path you do, it leads you to heaven.

96
00:06:45,800 --> 00:06:51,120
 This path you practice, it leads you to freedom."

97
00:06:51,120 --> 00:06:56,120
 And then just leave it up to the person.

98
00:06:56,120 --> 00:07:02,620
 So the useful side is in terms of not pushing or not

99
00:07:02,620 --> 00:07:08,280
 judging, but simply laying out a framework

100
00:07:08,280 --> 00:07:16,320
 of what is true and what is possible.

101
00:07:16,320 --> 00:07:23,340
 But it's of course dangerous because it most often slips

102
00:07:23,340 --> 00:07:24,880
 into right for them, something

103
00:07:24,880 --> 00:07:34,960
 that can be right or true or real for a certain individual.

104
00:07:34,960 --> 00:07:42,420
 And so it leads to the idea that anything goes and it

105
00:07:42,420 --> 00:07:46,920
 misses the fact that there are

106
00:07:46,920 --> 00:07:52,850
 laws of nature, especially beyond the laws that physics and

107
00:07:52,850 --> 00:07:55,960
 biology and so on, the material

108
00:07:55,960 --> 00:08:03,280
 sciences accept or recognize.

109
00:08:03,280 --> 00:08:08,220
 Because even with physical laws of gravity and evolution

110
00:08:08,220 --> 00:08:10,880
 and natural selection, even

111
00:08:10,880 --> 00:08:17,580
 there it just doesn't yet say anything about ethics, not

112
00:08:17,580 --> 00:08:21,400
 directly, meaning that one can

113
00:08:21,400 --> 00:08:31,040
 take it, take these laws and use them as one likes.

114
00:08:31,040 --> 00:08:39,230
 And in fact, potentially act unethically for one's own

115
00:08:39,230 --> 00:08:41,040
 benefit.

116
00:08:41,040 --> 00:08:45,580
 According to the laws of physics, the laws of biology,

117
00:08:45,580 --> 00:08:48,360
 chemistry, one can be a real jerk

118
00:08:48,360 --> 00:08:53,750
 or a real conniving manipulative individual and still

119
00:08:53,750 --> 00:08:56,880
 potentially have it be right for

120
00:08:56,880 --> 00:08:58,980
 them.

121
00:08:58,980 --> 00:09:06,240
 But on a less extreme level, people can do things like take

122
00:09:06,240 --> 00:09:09,120
 drugs or alcohol and the

123
00:09:09,120 --> 00:09:14,440
 laws of the material sciences don't negate the idea, don't

124
00:09:14,440 --> 00:09:19,560
 say anything necessarily about

125
00:09:19,560 --> 00:09:21,800
 this being a bad thing.

126
00:09:21,800 --> 00:09:27,080
 Some people want to drink alcohol all day, every day.

127
00:09:27,080 --> 00:09:33,110
 To each their own is what they would say, not what we would

128
00:09:33,110 --> 00:09:33,960
 say.

129
00:09:33,960 --> 00:09:41,110
 So the problem comes, what's missing here is the fact that

130
00:09:41,110 --> 00:09:44,280
 there is the claim made by

131
00:09:44,280 --> 00:09:49,450
 Buddhism that there are laws of nature that go beyond

132
00:09:49,450 --> 00:09:52,980
 simply the physical, that the mind

133
00:09:52,980 --> 00:09:56,310
 as well follows the laws of nature, which of course

134
00:09:56,310 --> 00:09:58,800
 material scientists should accept

135
00:09:58,800 --> 00:10:06,060
 because for a materialist or a physicalist, the mind is

136
00:10:06,060 --> 00:10:10,680
 simply a product of matter, which

137
00:10:10,680 --> 00:10:16,060
 of course we disagree with, but to some extent, where the

138
00:10:16,060 --> 00:10:19,160
 mind can arise based on matter,

139
00:10:19,160 --> 00:10:24,520
 that usually arises based on the mind.

140
00:10:24,520 --> 00:10:27,830
 I think the, I'm not even sure if the physical can create

141
00:10:27,830 --> 00:10:29,840
 the mind, but it can definitely

142
00:10:29,840 --> 00:10:33,800
 influence it.

143
00:10:33,800 --> 00:10:39,950
 Anyway, so the idea that the mind should follow certain

144
00:10:39,950 --> 00:10:43,960
 laws of its own or laws, anyway laws

145
00:10:43,960 --> 00:10:47,780
 of nature shouldn't be that foreign to scientific thinking

146
00:10:47,780 --> 00:10:48,520
 people.

147
00:10:48,520 --> 00:10:55,130
 It makes more sense than the alternative of it being

148
00:10:55,130 --> 00:10:59,280
 arbitrary or unimpeded in the sense

149
00:10:59,280 --> 00:11:05,820
 of being able to be in complete control of its reality,

150
00:11:05,820 --> 00:11:09,360
 which is how we normally take

151
00:11:09,360 --> 00:11:16,370
 it. We have this dichotomous sort of outlook on life where

152
00:11:16,370 --> 00:11:16,600
 the physical sciences are completely

153
00:11:16,600 --> 00:11:24,180
 deterministic and we, I am completely non-deterministic,

154
00:11:24,180 --> 00:11:31,840
 can't remember what the alternative is, but

155
00:11:31,840 --> 00:11:37,740
 we have freedom of will. For most of us, practically

156
00:11:37,740 --> 00:11:43,200
 speaking, this is how we approach life.

157
00:11:43,200 --> 00:11:45,260
 So therefore we get to come to the idea of to each their

158
00:11:45,260 --> 00:11:46,400
 own. These are the physical

159
00:11:46,400 --> 00:11:49,740
 laws, what you do with them is up to you. You want to be a

160
00:11:49,740 --> 00:11:51,840
 drug addict, maybe you'll

161
00:11:51,840 --> 00:12:00,640
 find happiness that way. You want to be a hedonistic, a sex

162
00:12:00,640 --> 00:12:05,040
 maniac, or you want to engage

163
00:12:05,040 --> 00:12:13,380
 in free love and whatever. Well, that's fine for you if you

164
00:12:13,380 --> 00:12:16,160
 want to practice abstinence,

165
00:12:16,160 --> 00:12:26,640
 celibacy, then to each their own. It usually devolves into

166
00:12:26,640 --> 00:12:29,920
 a sort of grabbing at whatever

167
00:12:29,920 --> 00:12:33,210
 pleasure you can in the here and now and things like celib

168
00:12:33,210 --> 00:12:35,320
acy are kind of frowned upon because

169
00:12:35,320 --> 00:12:40,490
 it assumes, it starts to assume sort of laws of nature,

170
00:12:40,490 --> 00:12:43,920
 often Christian, Judeo-Christian

171
00:12:43,920 --> 00:12:47,680
 in the sense of God doesn't want you to do that. So you can

172
00:12:47,680 --> 00:12:49,640
't just do whatever you want

173
00:12:49,640 --> 00:12:54,260
 to this idea of sort of a universal right and wrong, which

174
00:12:54,260 --> 00:12:56,480
 is sort of being discarded

175
00:12:56,480 --> 00:13:01,270
 or by many people has been discarded in favor of grasping

176
00:13:01,270 --> 00:13:03,960
 at whatever makes you happy, we

177
00:13:03,960 --> 00:13:09,850
 say, whatever makes you happy. But Buddhism cuts through

178
00:13:09,850 --> 00:13:11,760
 all of this by saying that it

179
00:13:11,760 --> 00:13:20,160
 can't make you happy. Yes, indeed, to each their own, do

180
00:13:20,160 --> 00:13:24,400
 what you want, but no, no to,

181
00:13:24,400 --> 00:13:28,060
 it's still the sense of whatever makes you happy, but there

182
00:13:28,060 --> 00:13:29,600
's only one set of things

183
00:13:29,600 --> 00:13:33,200
 that's going to make you happy. There's only a certain

184
00:13:33,200 --> 00:13:35,400
 subset of reality that will make,

185
00:13:35,400 --> 00:13:40,630
 that will lead to happiness. And this chafe set people, I

186
00:13:40,630 --> 00:13:43,560
 think, because we like to think

187
00:13:43,560 --> 00:13:47,760
 of our ability to find happiness wherever we want. I want

188
00:13:47,760 --> 00:13:49,960
 to be happy as a business

189
00:13:49,960 --> 00:13:55,060
 person. I want to be happy as a teacher. I want to be happy

190
00:13:55,060 --> 00:13:57,400
 as a husband or a wife. I

191
00:13:57,400 --> 00:14:01,000
 want to be happy doing this or doing that. I want to be

192
00:14:01,000 --> 00:14:03,200
 happy in heaven. People want

193
00:14:03,200 --> 00:14:06,620
 to be able to find their happiness, whatever makes them

194
00:14:06,620 --> 00:14:08,920
 happy. I'm happy eating this food

195
00:14:08,920 --> 00:14:14,920
 or that food. I'm happy drinking alcohol and going to

196
00:14:14,920 --> 00:14:18,520
 parties and bars, etc, etc.

197
00:14:18,520 --> 00:14:25,270
 Unfortunately, so Buddhism denies this, denounces this idea

198
00:14:25,270 --> 00:14:29,600
, rejects this idea as wrong, as

199
00:14:29,600 --> 00:14:35,440
 untrue, as delusional, but you can't find happiness

200
00:14:35,440 --> 00:14:38,080
 wherever you want. These things

201
00:14:38,080 --> 00:14:45,970
 do not lead to happiness. That's the claim. And so there's

202
00:14:45,970 --> 00:14:47,800
 nothing wrong with trying

203
00:14:47,800 --> 00:14:52,160
 to find, or trying to be sort of open to other people doing

204
00:14:52,160 --> 00:14:54,920
 whatever they want, but we should

205
00:14:54,920 --> 00:15:01,580
 never just accept, we should never go the next step and say

206
00:15:01,580 --> 00:15:06,920
, "If it makes you happy, or I'm

207
00:15:06,920 --> 00:15:10,220
 glad you're happy doing this or that." Even saying these

208
00:15:10,220 --> 00:15:13,160
 things isn't exactly wrong because

209
00:15:13,160 --> 00:15:19,860
 it often is a roundabout way of reminding people. So if

210
00:15:19,860 --> 00:15:23,400
 someone's an alcoholic, or let's

211
00:15:23,400 --> 00:15:26,590
 not say an alcoholic, but someone who has the firm belief

212
00:15:26,590 --> 00:15:28,120
 that drinking alcohol on a

213
00:15:28,120 --> 00:15:32,960
 regular basis for pleasure, for the feeling of freedom that

214
00:15:32,960 --> 00:15:35,080
 it gives to you, the feeling

215
00:15:35,080 --> 00:15:39,490
 that has the sense that this makes them happy, reminding

216
00:15:39,490 --> 00:15:42,040
 them, saying to them, "Well, as

217
00:15:42,040 --> 00:15:45,540
 long as it makes you happy," can actually be a good lesson

218
00:15:45,540 --> 00:15:47,200
 for these people because

219
00:15:47,200 --> 00:15:50,320
 it doesn't make them happy. And when you remind people of

220
00:15:50,320 --> 00:15:52,920
 why they're doing something, they

221
00:15:52,920 --> 00:15:55,680
 say they're doing it for happiness. You say, "As long as it

222
00:15:55,680 --> 00:15:56,920
 makes you happy, as long as

223
00:15:56,920 --> 00:16:04,470
 it really brings you happiness." You have to be open and

224
00:16:04,470 --> 00:16:07,660
 casual about it, but this kind

225
00:16:07,660 --> 00:16:12,060
 of lesson can actually be useful. But we have to be clear

226
00:16:12,060 --> 00:16:16,320
 that this is not actually possible.

227
00:16:16,320 --> 00:16:21,570
 That it's not possible for these things to lead to

228
00:16:21,570 --> 00:16:24,040
 happiness. Happiness doesn't come

229
00:16:24,040 --> 00:16:30,270
 from such a place, such a thing, because of the laws of

230
00:16:30,270 --> 00:16:32,480
 nature. So before I was talking

231
00:16:32,480 --> 00:16:36,860
 about Mara, how Mara gets in our way, we want to be happy

232
00:16:36,860 --> 00:16:39,320
 in the world, but that kind of

233
00:16:39,320 --> 00:16:43,110
 teaching of how evil is what is causing us, preventing us

234
00:16:43,110 --> 00:16:44,820
 from being happy, that kind

235
00:16:44,820 --> 00:16:49,210
 of teaching is kind of foundational. It's not really a deep

236
00:16:49,210 --> 00:16:51,360
 teaching about the ultimate

237
00:16:51,360 --> 00:16:55,160
 nature. It doesn't say much about the ultimate nature of

238
00:16:55,160 --> 00:16:57,680
 reality, not in terms of the general

239
00:16:57,680 --> 00:17:04,560
 idea of evil. There's another teaching that explains the

240
00:17:04,560 --> 00:17:06,360
 fundamental nature of reality

241
00:17:06,360 --> 00:17:16,830
 and why it's not possible to find happiness wherever you

242
00:17:16,830 --> 00:17:22,960
 want, and why the mind is unable

243
00:17:22,960 --> 00:17:29,610
 to delight in whatever path it follows, to be happy with

244
00:17:29,610 --> 00:17:32,560
 whatever it does. Be happy with

245
00:17:32,560 --> 00:17:43,700
 the things that it comes in contact with. And this is the

246
00:17:43,700 --> 00:17:48,400
 core teaching, a deep core

247
00:17:48,400 --> 00:17:55,900
 teaching of the Buddha on the three characteristics, which

248
00:17:55,900 --> 00:18:00,000
 many of us are familiar with. The three

249
00:18:00,000 --> 00:18:06,130
 characteristics of all arisen phenomena, all phenomena in

250
00:18:06,130 --> 00:18:09,440
 the world in samsara, and these

251
00:18:09,440 --> 00:18:14,470
 are the characteristics of impermanence, suffering and non-

252
00:18:14,470 --> 00:18:17,720
self, or impermanence, dukkha, how

253
00:18:17,720 --> 00:18:21,720
 do you translate dukkha? Suffering, I guess, is the best,

254
00:18:21,720 --> 00:18:23,920
 but it's not the best, it's just

255
00:18:23,920 --> 00:18:33,750
 all we've got, and non-self. Maybe dukkha would be best

256
00:18:33,750 --> 00:18:42,080
 translated as dis-ease, or unpleasing,

257
00:18:42,080 --> 00:18:48,240
 non-pleasing. Unpleasing might be a good one, just because

258
00:18:48,240 --> 00:18:51,600
 of what it means, unpleasing.

259
00:18:51,600 --> 00:18:56,540
 I wouldn't say unpleasant, maybe unpleasant also works, but

260
00:18:56,540 --> 00:18:59,120
 unpleasing, I don't even think

261
00:18:59,120 --> 00:19:02,930
 it's a word, but I just made up that word, it's a good one.

262
00:19:02,930 --> 00:19:04,800
 Dukkha means unpleasing,

263
00:19:04,800 --> 00:19:11,590
 in the sense of not pleasing. Maybe that's not even true,

264
00:19:11,590 --> 00:19:14,480
 because when you define please,

265
00:19:14,480 --> 00:19:21,940
 it's something that does make you delight temporarily. Uns

266
00:19:21,940 --> 00:19:25,520
atisfying, we have to use.

267
00:19:25,520 --> 00:19:29,100
 Anyway, once we explain it, it becomes clear. So suffering

268
00:19:29,100 --> 00:19:30,640
 is fine as long as we explain

269
00:19:30,640 --> 00:19:36,250
 it. What is meant, it's not actually the literal meaning,

270
00:19:36,250 --> 00:19:42,560
 it's just the word. So the problem,

271
00:19:42,560 --> 00:19:46,010
 the reason why we can't find happiness in the world, why we

272
00:19:46,010 --> 00:19:47,920
 can't just do whatever we want,

273
00:19:47,920 --> 00:19:52,520
 and why it doesn't work to say whatever makes you happy. No

274
00:19:52,520 --> 00:19:57,920
 thing will make you happy, there's

275
00:19:57,920 --> 00:20:01,840
 no thing in the world that can possibly make you happy,

276
00:20:01,840 --> 00:20:04,960
 because there are laws of nature that affect

277
00:20:04,960 --> 00:20:09,810
 the mind, that everything inside of us, in the mind, the

278
00:20:09,810 --> 00:20:12,480
 body, and in the whole world around us,

279
00:20:13,280 --> 00:20:20,050
 is impermanent. The reason why we seek things out, we

280
00:20:20,050 --> 00:20:25,200
 misunderstand them to be permanent,

281
00:20:25,200 --> 00:20:35,810
 to be stable, to be lasting. The nature of pleasure or

282
00:20:35,810 --> 00:20:40,640
 desire is to cling, is to

283
00:20:40,640 --> 00:20:53,420
 pull together moments in time and hold on to them, which is

284
00:20:53,420 --> 00:20:56,240
 sort of the equivalent of,

285
00:20:56,240 --> 00:21:03,720
 when I was young, I probably was about 12, and one of the

286
00:21:03,720 --> 00:21:07,200
 first times I went to, well not 12,

287
00:21:07,200 --> 00:21:14,480
 it was young, maybe 8, 10, I can't remember. We went to a

288
00:21:14,480 --> 00:21:17,600
 mall in Sudbury, this town in Northern

289
00:21:17,600 --> 00:21:21,680
 Ontario, Sydney in Northern Ontario, and they had these

290
00:21:21,680 --> 00:21:25,120
 escalators, for me growing up in the forest,

291
00:21:25,120 --> 00:21:28,040
 an escalator was kind of a big deal, so playing on the

292
00:21:28,040 --> 00:21:30,400
 escalators while my parents shopped.

293
00:21:31,840 --> 00:21:36,950
 I remember grabbing onto the side of the escalator, the

294
00:21:36,950 --> 00:21:38,880
 handrail, I was at the top,

295
00:21:38,880 --> 00:21:43,100
 and it was an up escalator, and I held up my arm and I

296
00:21:43,100 --> 00:21:47,520
 tried to push back on the arm rail,

297
00:21:47,520 --> 00:21:53,450
 and I pushed really hard and my elbow snapped, and I've had

298
00:21:53,450 --> 00:21:56,240
 tendonitis ever since.

299
00:21:58,480 --> 00:22:02,760
 It was exacerbated later on, but that was the point, I

300
00:22:02,760 --> 00:22:04,480
 think I've probably still got it,

301
00:22:04,480 --> 00:22:09,030
 if I were to exert my right arm. Anyway, it's kind of like

302
00:22:09,030 --> 00:22:12,640
 that, trying to stop an immovable forest,

303
00:22:12,640 --> 00:22:19,040
 you only end up hurting yourself. It's kind of dumb, but it

304
00:22:19,040 --> 00:22:21,440
's the nature of desire to do that,

305
00:22:21,440 --> 00:22:29,760
 to grab on and try to stop this unstoppable forest.

306
00:22:29,760 --> 00:22:36,020
 The power that you actually exert over things is so minisc

307
00:22:36,020 --> 00:22:40,240
ule, you can affect the outcome, but

308
00:22:40,240 --> 00:22:45,520
 the effect is much smaller than we think,

309
00:22:48,080 --> 00:22:51,410
 much smaller than we usually understand it to be. And so as

310
00:22:51,410 --> 00:22:53,600
 a result we end up hurting ourselves,

311
00:22:53,600 --> 00:23:01,970
 we end up suffering, because change is built into the

312
00:23:01,970 --> 00:23:07,040
 system. You can hold on to little things in

313
00:23:07,040 --> 00:23:11,320
 life for some time, but the desire that you have will

314
00:23:11,320 --> 00:23:13,520
 inevitably go counter to,

315
00:23:15,760 --> 00:23:19,440
 it will try to make things last, things that don't last.

316
00:23:19,440 --> 00:23:24,350
 And it builds up and builds up until there's a larger and

317
00:23:24,350 --> 00:23:26,240
 larger expectation

318
00:23:26,240 --> 00:23:31,100
 that is further and further outside of what is actually

319
00:23:31,100 --> 00:23:33,600
 possible, and then it crashes and

320
00:23:33,600 --> 00:23:37,520
 something changes significantly and you suffer.

321
00:23:40,160 --> 00:23:43,440
 So you can build up this sense of stability in life, you

322
00:23:43,440 --> 00:23:45,840
 have a nice house and a car and a job

323
00:23:45,840 --> 00:23:50,710
 and everything, and it can all come crashing down in a

324
00:23:50,710 --> 00:23:53,600
 moment. And if you have great desire in your

325
00:23:53,600 --> 00:23:56,020
 mind, of course if your mind is pure, then all of this is

326
00:23:56,020 --> 00:23:56,720
 no problem.

