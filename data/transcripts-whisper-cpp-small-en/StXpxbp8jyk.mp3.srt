1
00:00:00,000 --> 00:00:06,240
 I start to study seriously Buddhist teachings not so long

2
00:00:06,240 --> 00:00:09,560
 ago, so it's still really new,

3
00:00:09,560 --> 00:00:13,600
 so it's difficult to formulate questions.

4
00:00:13,600 --> 00:00:19,060
 Would you explain what goes from life to life if me or ego,

5
00:00:19,060 --> 00:00:22,080
 the Ataman, does not really

6
00:00:22,080 --> 00:00:28,720
 exist?

7
00:00:28,720 --> 00:00:36,290
 In an apple, you eating an apple, this apple eating eaten

8
00:00:36,290 --> 00:00:40,040
 up, you drop it on the ground

9
00:00:40,040 --> 00:00:48,960
 somewhere and it falls on fertile soil.

10
00:00:48,960 --> 00:00:59,080
 After some time, the seed starts to grow again and a new

11
00:00:59,080 --> 00:01:05,080
 apple tree is there and suddenly

12
00:01:05,080 --> 00:01:09,960
 after some time there are new apples.

13
00:01:09,960 --> 00:01:20,310
 So you can't say that the first apple and the last apple

14
00:01:20,310 --> 00:01:26,720
 are the same or that some thing

15
00:01:26,720 --> 00:01:35,460
 from the first apple had gone into the second apple, but

16
00:01:35,460 --> 00:01:40,560
 the first apple was necessary for

17
00:01:40,560 --> 00:01:48,880
 the second apple to arise, to come to be.

18
00:01:48,880 --> 00:01:53,230
 If there haven't been the first apple, the second apple

19
00:01:53,230 --> 00:01:54,920
 wouldn't be there.

20
00:01:54,920 --> 00:01:58,280
 So they are kind of interrelated.

21
00:01:58,280 --> 00:02:03,960
 There is a connection, but you can't say there is something

22
00:02:03,960 --> 00:02:06,760
 going from the first thing to

23
00:02:06,760 --> 00:02:08,720
 the second thing.

24
00:02:08,720 --> 00:02:16,550
 There is nothing you can grab, you can hold, but still

25
00:02:16,550 --> 00:02:21,960
 there is not something that is needed

26
00:02:21,960 --> 00:02:28,930
 that the first becomes the second or that when one life

27
00:02:28,930 --> 00:02:32,720
 ends, another life comes to

28
00:02:32,720 --> 00:02:34,480
 be.

29
00:02:34,480 --> 00:02:44,220
 You can imagine maybe a string and this string would be

30
00:02:44,220 --> 00:02:53,720
 your consciousness and your subconsciousness,

31
00:02:53,720 --> 00:02:59,810
 basically the subconsciousness and every single moment you

32
00:02:59,810 --> 00:03:03,320
 experience or better said, every

33
00:03:03,320 --> 00:03:11,260
 single moment that is experienced is a bead on that string

34
00:03:11,260 --> 00:03:14,960
 and then death would just be

35
00:03:14,960 --> 00:03:21,220
 another bead and the next birth would just be another bead

36
00:03:21,220 --> 00:03:24,640
 and it's all just lined up

37
00:03:24,640 --> 00:03:32,910
 on the string which is basically not conscious,

38
00:03:32,910 --> 00:03:39,320
 subconscious, some people say unconscious,

39
00:03:39,320 --> 00:03:48,360
 and some of it becomes conscious, some moments become

40
00:03:48,360 --> 00:03:51,720
 conscious.

41
00:03:51,720 --> 00:03:57,080
 You can't see where the string of consciousness started

42
00:03:57,080 --> 00:04:00,240
 when it started to be pearls that

43
00:04:00,240 --> 00:04:08,670
 are put onto the string and you can't say when it ends, but

44
00:04:08,670 --> 00:04:12,440
 you can note every single

45
00:04:12,440 --> 00:04:16,160
 moment and that's what we are doing with the mindfulness

46
00:04:16,160 --> 00:04:17,080
 practice.

47
00:04:17,080 --> 00:04:24,660
 We are trying to be mindful of every single moment, make it

48
00:04:24,660 --> 00:04:28,160
 conscious like that and be

49
00:04:28,160 --> 00:04:32,320
 in the present moment.

50
00:04:32,320 --> 00:04:37,090
 When you do this, you will understand that there is really

51
00:04:37,090 --> 00:04:39,320
 not so much a thing that you

52
00:04:39,320 --> 00:04:46,190
 could call me or ego, but it's just a moment after the

53
00:04:46,190 --> 00:04:50,880
 other passing by, just a process,

54
00:04:50,880 --> 00:04:56,320
 ongoing and ongoing.

55
00:04:56,320 --> 00:04:58,320
 Maybe you want to say more on it.

56
00:04:58,320 --> 00:05:01,730
 I normally answer the same way, but there is something that

57
00:05:01,730 --> 00:05:03,920
 has to be said in addition.

58
00:05:03,920 --> 00:05:09,400
 That is exactly how I would have said it.

59
00:05:09,400 --> 00:05:15,280
 Death is just another moment, but there is something else

60
00:05:15,280 --> 00:05:17,520
 that has to be said.

61
00:05:17,520 --> 00:05:21,210
 I just like to clarify that death does have some

62
00:05:21,210 --> 00:05:24,280
 significance because birth is kind of

63
00:05:24,280 --> 00:05:29,760
 like an explosion or not birth, but conception.

64
00:05:29,760 --> 00:05:33,660
 When a being takes a life, it takes a lot of energy to be

65
00:05:33,660 --> 00:05:35,960
 born, especially in a coarse

66
00:05:35,960 --> 00:05:37,320
 realm as a human.

67
00:05:37,320 --> 00:05:40,600
 It takes a lot of energy and there is this burst of energy

68
00:05:40,600 --> 00:05:42,300
 that sets us on this course

69
00:05:42,300 --> 00:05:47,280
 as a human being because even an arahant, when they give up

70
00:05:47,280 --> 00:05:50,140
 the attachment to rebirth,

71
00:05:50,140 --> 00:05:51,760
 they still have to live their life out.

72
00:05:51,760 --> 00:05:57,510
 This is because of the power of the greed and the craving

73
00:05:57,510 --> 00:06:00,620
 to be reborn that is no longer

74
00:06:00,620 --> 00:06:03,660
 with them, but that led them to be born.

75
00:06:03,660 --> 00:06:08,700
 At the moment of death, we create this new life.

76
00:06:08,700 --> 00:06:13,340
 We create the birth as a human being.

77
00:06:13,340 --> 00:06:17,860
 It's more like waves, I think.

78
00:06:17,860 --> 00:06:19,060
 The waves on the ocean.

79
00:06:19,060 --> 00:06:23,930
 Our existence is like all of this craving is forcing the

80
00:06:23,930 --> 00:06:26,540
 water, like the power of the

81
00:06:26,540 --> 00:06:30,100
 gravity forcing the water into a wave.

82
00:06:30,100 --> 00:06:32,820
 Then the wave crashes.

83
00:06:32,820 --> 00:06:36,180
 The next wave builds up.

84
00:06:36,180 --> 00:06:44,150
 Once you get rid of the force that's creating the waves,

85
00:06:44,150 --> 00:06:48,660
 then there is the end to the turbulent.

86
00:06:48,660 --> 00:07:02,610
 There is no moment of death that we can say, "There was

87
00:07:02,610 --> 00:07:04,220
 this life and there was the next

88
00:07:04,220 --> 00:07:05,220
 life."

89
00:07:05,220 --> 00:07:08,780
 But there is some build up of power and then the release of

90
00:07:08,780 --> 00:07:09,460
 power.

91
00:07:09,460 --> 00:07:13,300
 During this life, we build up more of this attachment.

92
00:07:13,300 --> 00:07:16,460
 When we die, there's another burst.

93
00:07:16,460 --> 00:07:17,940
 There's this build up.

94
00:07:17,940 --> 00:07:20,180
 An Arahant, an enlightened being, doesn't do that.

95
00:07:20,180 --> 00:07:24,420
 At the moment of death, there is nothing.

96
00:07:24,420 --> 00:07:27,020
 Death is an important moment.

97
00:07:27,020 --> 00:07:30,750
 It doesn't mean the death of a self or the death of any one

98
00:07:30,750 --> 00:07:31,500
 thing.

99
00:07:31,500 --> 00:07:35,180
 It couldn't because such a thing doesn't exist.

100
00:07:35,180 --> 00:07:41,700
 There is nothing in experience that can be seen as a self.

101
00:07:41,700 --> 00:07:45,060
 There is nothing in reality that can be seen as a self.

102
00:07:45,060 --> 00:07:51,200
 But it has some power and it is able to force us to be born

103
00:07:51,200 --> 00:07:52,420
 again.

104
00:07:52,420 --> 00:08:02,170
 The real answer to if there is no self, how does this

105
00:08:02,170 --> 00:08:05,660
 process occur?

106
00:08:05,660 --> 00:08:10,280
 You have to explain why things are or help people to

107
00:08:10,280 --> 00:08:12,380
 understand what Palanjani is talking

108
00:08:12,380 --> 00:08:17,060
 about that it's just moment to moment experience.

109
00:08:17,060 --> 00:08:21,660
 Reality doesn't admit of these philosophical arguments.

110
00:08:21,660 --> 00:08:23,860
 It doesn't admit of views.

111
00:08:23,860 --> 00:08:27,540
 It doesn't admit of the existence of entities.

112
00:08:27,540 --> 00:08:31,260
 It doesn't admit of the existence of a three dimensional

113
00:08:31,260 --> 00:08:31,940
 space.

114
00:08:31,940 --> 00:08:34,940
 All of these things arise in the mind.

115
00:08:34,940 --> 00:08:37,980
 What is truly real is experience.

116
00:08:37,980 --> 00:08:44,620
 That which is experience, that which is verifiable, that

117
00:08:44,620 --> 00:08:48,460
 which is empirical, that which we come

118
00:08:48,460 --> 00:08:52,380
 in contact with at every moment.

119
00:08:52,380 --> 00:09:00,780
 The whole view of a self has no place in this.

120
00:09:00,780 --> 00:09:05,350
 The whole theory of a self or the whole perception that

121
00:09:05,350 --> 00:09:07,940
 there might be a self has no basis.

122
00:09:07,940 --> 00:09:09,700
 It has no place.

123
00:09:09,700 --> 00:09:13,400
 It can't fit into this because what you have is moment to

124
00:09:13,400 --> 00:09:15,020
 moment experience.

125
00:09:15,020 --> 00:09:16,020
 That's what we have.

126
00:09:16,020 --> 00:09:18,100
 That's what we experience.

127
00:09:18,100 --> 00:09:28,020
 Buddhism teaches us to become in harmony with this.

128
00:09:28,020 --> 00:09:29,540
 So this is how we understand reality.

129
00:09:29,540 --> 00:09:33,510
 To help us to understand the world and reality in terms of

130
00:09:33,510 --> 00:09:34,740
 these things.

131
00:09:34,740 --> 00:09:35,740
 Reality is just here.

132
00:09:35,740 --> 00:09:40,140
 The question is there a soul, is there an impermanent soul?

133
00:09:40,140 --> 00:09:41,860
 What do these words mean?

134
00:09:41,860 --> 00:09:45,700
 These are words on a page.

135
00:09:45,700 --> 00:09:50,580
 They're thoughts that arise in the mind.

136
00:09:50,580 --> 00:09:53,420
 There's no way to answer such questions.

137
00:09:53,420 --> 00:09:54,580
 They have no meaning.

138
00:09:54,580 --> 00:09:55,900
 They're meaningless.

139
00:09:55,900 --> 00:09:58,380
 It's like asking is there an apple?

140
00:09:58,380 --> 00:10:03,270
 If you have the apple and it falls from the tree, is there

141
00:10:03,270 --> 00:10:04,380
 an apple?

142
00:10:04,380 --> 00:10:08,180
 It's a totally meaningless question from a Buddhist point

143
00:10:08,180 --> 00:10:09,060
 of view.

144
00:10:09,060 --> 00:10:10,420
 There is the experience.

145
00:10:10,420 --> 00:10:14,750
 When you talk about what is really there, there's the

146
00:10:14,750 --> 00:10:16,860
 experience of seeing.

147
00:10:16,860 --> 00:10:19,240
 If you hear the apple fall, there's the experience of

148
00:10:19,240 --> 00:10:19,860
 hearing.

149
00:10:19,860 --> 00:10:22,110
 If you think of it as an apple, there's the experience of

150
00:10:22,110 --> 00:10:22,660
 thinking.

151
00:10:22,660 --> 00:10:25,030
 If you wonder whether the apple exists, there's an

152
00:10:25,030 --> 00:10:26,420
 experience of wondering.

153
00:10:26,420 --> 00:10:28,600
 The question of whether the apple exists or not is

154
00:10:28,600 --> 00:10:29,420
 meaningless.

155
00:10:29,420 --> 00:10:30,420
 There's no answer.

156
00:10:30,420 --> 00:10:31,620
 You can't give an answer to it.

157
00:10:31,620 --> 00:10:33,790
 The only answer you could say is, "Well, according to

158
00:10:33,790 --> 00:10:35,380
 theory, no, it doesn't exist."

159
00:10:35,380 --> 00:10:36,540
 But it's meaningless.

160
00:10:36,540 --> 00:10:42,340
 The words, "Does the apple exist?" or "Is there a soul?"

161
00:10:42,340 --> 00:10:44,140
 have no meaning.

162
00:10:44,140 --> 00:10:47,240
 It's like there was one monk he said, "It's like if you ask

163
00:10:47,240 --> 00:10:48,900
 an innocent man why he killed

164
00:10:48,900 --> 00:10:52,740
 his wife or why he beat his wife."

165
00:10:52,740 --> 00:10:54,020
 He didn't beat his wife.

166
00:10:54,020 --> 00:10:56,260
 You can't ask the question of why.

167
00:10:56,260 --> 00:11:00,420
 These are questions that have no relationship to reality.

168
00:11:00,420 --> 00:11:02,100
 Reality is a moment-to-moment experience.

169
00:11:02,100 --> 00:11:04,460
 The idea is there a self, isn't there a self?

170
00:11:04,460 --> 00:11:06,700
 None of these questions apply.

171
00:11:06,700 --> 00:11:10,100
 There is this moment-to-moment experience.

172
00:11:10,100 --> 00:11:13,220
 It goes on regardless of whether there is a self or not.

173
00:11:13,220 --> 00:11:18,620
 Asking what goes from life to life can't be answered.

174
00:11:18,620 --> 00:11:19,860
 Life to life doesn't exist.

175
00:11:19,860 --> 00:11:20,860
 This life doesn't exist.

176
00:11:20,860 --> 00:11:21,860
 This life doesn't exist.

177
00:11:21,860 --> 00:11:24,380
 What exists is moment to moment.

178
00:11:24,380 --> 00:11:29,250
 It functions in a certain way that gives rise to this next

179
00:11:29,250 --> 00:11:29,980
 wave.

180
00:11:29,980 --> 00:11:39,830
 The power, the impetus, the impulse, the force that is

181
00:11:39,830 --> 00:11:43,940
 caused by the craving.

182
00:11:43,940 --> 00:11:46,240
 But that's just a part of how this moment-to-moment

183
00:11:46,240 --> 00:11:47,020
 experience works.

184
00:11:47,020 --> 00:11:48,100
 You can see how that works.

185
00:11:48,100 --> 00:11:49,980
 You can watch when you die.

186
00:11:49,980 --> 00:11:54,060
 You can watch your craving lead you to the next life.

187
00:11:54,060 --> 00:11:55,060
 You can see that happening.

188
00:11:55,060 --> 00:11:58,040
 The idea of what is it that's doing this or what is behind

189
00:11:58,040 --> 00:11:59,820
 all of this or what is the

190
00:11:59,820 --> 00:12:02,560
 substratum of this existence?

191
00:12:02,560 --> 00:12:04,860
 It has no meaning.

192
00:12:04,860 --> 00:12:05,860
 What could you say?

193
00:12:05,860 --> 00:12:08,340
 You could make up some kind of theory about what is the

194
00:12:08,340 --> 00:12:09,780
 substratum and you could even

195
00:12:09,780 --> 00:12:12,040
 see that it's verified in reality.

196
00:12:12,040 --> 00:12:15,170
 The problem is that we go, "This is exactly what science

197
00:12:15,170 --> 00:12:15,800
 does."

198
00:12:15,800 --> 00:12:22,530
 It theorizes and it tries to find theories that mesh with

199
00:12:22,530 --> 00:12:24,060
 reality.

200
00:12:24,060 --> 00:12:26,460
 But none of that has anything to do with reality.

201
00:12:26,460 --> 00:12:27,740
 Reality is the experience.

202
00:12:27,740 --> 00:12:32,840
 Whether you figure it out, what is the substratum or how it

203
00:12:32,840 --> 00:12:35,460
 works has no bearing on what it

204
00:12:35,460 --> 00:12:36,460
 is.

205
00:12:36,460 --> 00:12:43,060
 It has no bearing on reality.

206
00:12:43,060 --> 00:12:45,420
 The point being that as you practice meditation, these

207
00:12:45,420 --> 00:12:46,460
 questions drop away.

208
00:12:46,460 --> 00:12:48,980
 You don't ever think as a Buddhist, "Is there a self?

209
00:12:48,980 --> 00:12:52,300
 Isn't there a self?"

210
00:12:52,300 --> 00:12:56,290
 Once you become developed in meditation, these questions

211
00:12:56,290 --> 00:12:57,300
 drop away.

212
00:12:57,300 --> 00:13:01,020
 There's no answer.

213
00:13:01,020 --> 00:13:03,900
 They have no connection with what you're experiencing.

214
00:13:03,900 --> 00:13:05,560
 The experience is what it is.

215
00:13:05,560 --> 00:13:06,620
 It comes and it goes.

216
00:13:06,620 --> 00:13:07,620
 It arises.

217
00:13:07,620 --> 00:13:08,620
 It ceases.

218
00:13:08,620 --> 00:13:09,620
 It's moment to moment.

219
00:13:09,620 --> 00:13:12,740
 It's impermanent, unsatisfying, uncontrollable.

220
00:13:12,740 --> 00:13:14,740
 It's not worth clinging to.

221
00:13:14,740 --> 00:13:17,660
 So eventually the clinging drops away.

222
00:13:17,660 --> 00:13:19,340
 Clinging to anything drops away.

223
00:13:19,340 --> 00:13:21,060
 With the dropping away of clinging, there's the end of

224
00:13:21,060 --> 00:13:21,620
 suffering.

225
00:13:21,620 --> 00:13:26,820
 With the end of suffering, there's freedom.

226
00:13:26,820 --> 00:13:30,620
 Martin has a related question.

227
00:13:30,620 --> 00:13:35,860
 Can we say that the mind continues if someone asks?

228
00:13:35,860 --> 00:13:41,100
 I wouldn't say that.

229
00:13:41,100 --> 00:13:43,060
 Because it does not continue.

230
00:13:43,060 --> 00:13:46,260
 The mind is not stable.

231
00:13:46,260 --> 00:13:51,240
 The soul, talking about the soul or talking about the mind

232
00:13:51,240 --> 00:13:54,420
 continues, comes from the wish

233
00:13:54,420 --> 00:14:00,740
 that there is something to hold on, to grab.

234
00:14:00,740 --> 00:14:06,500
 But this wish is irrelevant.

235
00:14:06,500 --> 00:14:07,500
 It doesn't work.

236
00:14:07,500 --> 00:14:10,020
 It's a cause for suffering.

237
00:14:10,020 --> 00:14:13,060
 It doesn't work that way.

238
00:14:13,060 --> 00:14:15,620
 The mind does not continue.

239
00:14:15,620 --> 00:14:17,500
 Look at your mind.

240
00:14:17,500 --> 00:14:23,420
 This is the fastest thing that exists.

241
00:14:23,420 --> 00:14:26,540
 It's going quickly.

242
00:14:26,540 --> 00:14:31,180
 Every second your mind changes.

243
00:14:31,180 --> 00:14:34,140
 So you can't really say it continues.

244
00:14:34,140 --> 00:14:37,110
 If someone asks, tell them to stop asking questions and go

245
00:14:37,110 --> 00:14:37,860
 practice.

246
00:14:37,860 --> 00:14:41,860
 Well, yeah.

247
00:14:41,860 --> 00:14:42,860
 Thank you.

248
00:14:42,860 --> 00:14:43,860
 Thank you.

