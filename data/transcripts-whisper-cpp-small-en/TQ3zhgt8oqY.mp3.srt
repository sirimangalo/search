1
00:00:00,000 --> 00:00:14,000
 [ "I'm not a fan of English" ]

2
00:00:14,000 --> 00:00:16,000
 Okay, so today we're...

3
00:00:16,000 --> 00:00:18,000
 I'm talking English.

4
00:00:18,000 --> 00:00:43,000
 [ "I'm not a fan of English" ]

5
00:00:43,000 --> 00:00:50,000
 One of the difficulties that we have in meditation

6
00:00:50,000 --> 00:01:00,790
 often comes from trying to find a reason why we're med

7
00:01:00,790 --> 00:01:05,000
itating.

8
00:01:05,000 --> 00:01:17,230
 Our difficulty in meditation often comes from not being

9
00:01:17,230 --> 00:01:31,000
 able to see the reason why we meditate.

10
00:01:31,000 --> 00:01:36,830
 And this becomes a problem because meditation can be very

11
00:01:36,830 --> 00:01:38,000
 difficult.

12
00:01:38,000 --> 00:01:43,430
 And so we find ourselves often inclining towards more

13
00:01:43,430 --> 00:01:52,000
 peaceful and pleasurable states when we meditate.

14
00:01:52,000 --> 00:01:57,390
 We find ourselves easily falling into non-meditative states

15
00:01:57,390 --> 00:02:03,300
 where we're not actually contemplating or considering the

16
00:02:03,300 --> 00:02:05,000
 objects carefully.

17
00:02:05,000 --> 00:02:16,000
 We are enjoying the experience of pleasant states.

18
00:02:16,000 --> 00:02:28,000
 And when unpleasant states come, we are upset by them.

19
00:02:28,000 --> 00:02:39,110
 And so when we're taught to acknowledge and to accept

20
00:02:39,110 --> 00:02:43,000
 things as they are,

21
00:02:43,000 --> 00:02:48,000
 it arises a question in the mind, "Why?"

22
00:02:48,000 --> 00:03:00,000
 "Why sit and torture ourselves?"

23
00:03:00,000 --> 00:03:04,000
 This is a big question in the beginning.

24
00:03:04,000 --> 00:03:09,900
 Later on, once we've become more proficient in the practice

25
00:03:09,900 --> 00:03:14,190
, the question might not arise, but it's still very easy to

26
00:03:14,190 --> 00:03:17,000
 lose sight of why we're meditating

27
00:03:17,000 --> 00:03:23,000
 and lose our conviction and our exertion in the practice.

28
00:03:23,000 --> 00:03:35,320
 We find ourselves slacking off, or getting bored, losing

29
00:03:35,320 --> 00:03:46,000
 interest, losing our interest in the practice.

30
00:03:46,000 --> 00:03:51,360
 So I think we should always consider the results of what we

31
00:03:51,360 --> 00:03:53,000
 do. This is something important.

32
00:03:53,000 --> 00:03:56,060
 And when we do this, then we can see what is of real use,

33
00:03:56,060 --> 00:04:01,880
 and meditation becomes something that is very easy to see

34
00:04:01,880 --> 00:04:05,000
 the benefit of.

35
00:04:05,000 --> 00:04:10,700
 Because when we're not meditating, when we're not

36
00:04:10,700 --> 00:04:17,730
 practicing, when we're not seeing things clearly, there are

37
00:04:17,730 --> 00:04:20,000
 results as well.

38
00:04:20,000 --> 00:04:32,030
 When we cling to things, when we desire for things, when we

39
00:04:32,030 --> 00:04:41,000
 like things, our actions don't go without result.

40
00:04:41,000 --> 00:04:58,340
 When we dislike things, when we're upset, when we get angry

41
00:04:58,340 --> 00:05:10,000
, this isn't without results as well.

42
00:05:10,000 --> 00:05:15,660
 People who have agreed have a real hard time seeing the

43
00:05:15,660 --> 00:05:17,000
 problem.

44
00:05:17,000 --> 00:05:21,110
 People with anger tend to see the problem more. It's not to

45
00:05:21,110 --> 00:05:25,190
 say that they can't fix it, or that they can fix it, that

46
00:05:25,190 --> 00:05:26,000
 they're able to fix it.

47
00:05:26,000 --> 00:05:30,580
 But it's easier to see. Agreed is something very difficult

48
00:05:30,580 --> 00:05:34,510
 to see, and it's very difficult to see the downside, the

49
00:05:34,510 --> 00:05:37,000
 negative side of it.

50
00:05:37,000 --> 00:05:39,780
 It's associated with pleasure, and so we think that by

51
00:05:39,780 --> 00:05:42,430
 following after it, we're going to get more and more

52
00:05:42,430 --> 00:05:44,000
 pleasure and happiness.

53
00:05:44,000 --> 00:05:48,450
 And in fact, the opposite is the case. What we don't see is

54
00:05:48,450 --> 00:05:54,000
 that we slowly become ghosts. We slowly become spirits.

55
00:05:54,000 --> 00:06:01,510
 We waste away. We lose all of our substance, like a drug

56
00:06:01,510 --> 00:06:03,000
 addict.

57
00:06:03,000 --> 00:06:06,690
 And so this is why the Buddha said, "When you die, greed

58
00:06:06,690 --> 00:06:09,000
 leads you to be born as a ghost."

59
00:06:09,000 --> 00:06:15,070
 You can see it in people. They become like demons, like o

60
00:06:15,070 --> 00:06:24,630
gres, like ghosts wailing and complaining and clinging, very

61
00:06:24,630 --> 00:06:27,000
 much clinging.

62
00:06:27,000 --> 00:06:51,000
 People who have anger, they put themselves in hell.

63
00:06:51,000 --> 00:06:55,080
 When we get angry, you can see this in people. When they're

64
00:06:55,080 --> 00:06:59,000
 angry, they're in hell. They're in a hellish state.

65
00:06:59,000 --> 00:07:07,360
 They're in a state of deprivation, of loss, a state of pain

66
00:07:07,360 --> 00:07:10,000
 and suffering.

67
00:07:10,000 --> 00:07:17,520
 The Buddha said, "If you want to hurt someone, make them

68
00:07:17,520 --> 00:07:27,000
 angry." Someone who gets angry does their enemy a favor.

69
00:07:27,000 --> 00:07:30,650
 When we get angry at someone else, someone does something

70
00:07:30,650 --> 00:07:34,000
 or says something nasty and tries to make us upset.

71
00:07:34,000 --> 00:07:39,610
 When we get upset, we do them a favor. They want to hurt us

72
00:07:39,610 --> 00:07:45,000
. We hurt ourselves.

73
00:07:45,000 --> 00:07:53,440
 When we get angry at someone, we've accomplished their goal

74
00:07:53,440 --> 00:07:59,000
. They put ourselves in suffering.

75
00:07:59,000 --> 00:08:05,000
 So we go to hell. We fall into hell. Even in this life,

76
00:08:05,000 --> 00:08:12,140
 people who are always angry look like devils, demons, hell

77
00:08:12,140 --> 00:08:13,000
 beings.

78
00:08:13,000 --> 00:08:31,000
 When they die, the Buddha said, "This is where they go."

79
00:08:31,000 --> 00:08:36,510
 People with delusion, when we're full of ignorance and dis

80
00:08:36,510 --> 00:08:46,200
interest and arrogance and self-appreciation and views and

81
00:08:46,200 --> 00:08:50,000
 opinions.

82
00:08:50,000 --> 00:08:55,330
 This is the realm of animals. People who are ignorant, who

83
00:08:55,330 --> 00:09:01,000
 have no interest in mental development, self-development,

84
00:09:01,000 --> 00:09:08,000
 no interest in higher things like philosophy and religion,

85
00:09:08,000 --> 00:09:16,000
 no interest in science and understanding.

86
00:09:16,000 --> 00:09:25,000
 People who are full of ideas and views and opinions.

87
00:09:25,000 --> 00:09:31,060
 People who have these low states of mind of conceit and

88
00:09:31,060 --> 00:09:34,000
 arrogance, bravado.

89
00:09:34,000 --> 00:09:39,320
 These are animal states. This is the way of animals,

90
00:09:39,320 --> 00:09:42,000
 insects sometimes.

91
00:09:42,000 --> 00:09:45,760
 Look at these people who have a great amount of

92
00:09:45,760 --> 00:09:47,000
 intelligence.

93
00:09:47,000 --> 00:09:55,180
 You can't avoid thinking that they're a lot like insects

94
00:09:55,180 --> 00:10:02,070
 because of their insect-like behavior or animal-like

95
00:10:02,070 --> 00:10:03,000
 behavior.

96
00:10:03,000 --> 00:10:12,000
 People who boast and brag and show off.

97
00:10:12,000 --> 00:10:15,940
 All of these different states, this is the state of

98
00:10:15,940 --> 00:10:21,000
 delusion. It's a very low state.

99
00:10:21,000 --> 00:10:27,920
 It shows someone's inferiority, a sign of someone who is a

100
00:10:27,920 --> 00:10:31,000
 superior human being.

101
00:10:31,000 --> 00:10:39,530
 They don't show off. They don't hold opinions and views and

102
00:10:39,530 --> 00:10:41,000
 beliefs.

103
00:10:41,000 --> 00:10:45,470
 They don't try to impress themselves on others. They're

104
00:10:45,470 --> 00:10:48,000
 very keen on learning and understanding.

105
00:10:48,000 --> 00:10:56,000
 They're very keen on self-improvement.

106
00:10:56,000 --> 00:10:59,850
 When you see people like this, you can see that they're not

107
00:10:59,850 --> 00:11:01,000
 like animals.

108
00:11:01,000 --> 00:11:07,460
 They're not living their life just by eating and sleeping

109
00:11:07,460 --> 00:11:12,000
 and indulging in sensual pleasures.

110
00:11:12,000 --> 00:11:19,450
 They have something higher on their minds, a higher purpose

111
00:11:19,450 --> 00:11:20,000
.

112
00:11:20,000 --> 00:11:26,000
 These things also have results in these states of mind.

113
00:11:26,000 --> 00:11:28,630
 In this life they have very clear results and in the

114
00:11:28,630 --> 00:11:31,000
 afterlife they have very clear results.

115
00:11:31,000 --> 00:11:34,000
 There's no question where these things lead.

116
00:11:34,000 --> 00:11:38,000
 The Buddha said, "Jit dey, sanghilit dey, tukat dey patik

117
00:11:38,000 --> 00:11:38,000
ankar."

118
00:11:38,000 --> 00:11:44,000
 The mind that is defiled leads to suffering, leads to hell,

119
00:11:44,000 --> 00:11:49,000
 leads to a bad destination, without doubt.

120
00:11:49,000 --> 00:12:03,000
 Patikankar, undoubtedly.

121
00:12:03,000 --> 00:12:06,750
 The problem is when we don't meditate, these states are

122
00:12:06,750 --> 00:12:08,000
 ever-present.

123
00:12:08,000 --> 00:12:11,000
 We don't have to do anything special for them to arise.

124
00:12:11,000 --> 00:12:14,280
 All we have to do is live our lives as we ordinary would

125
00:12:14,280 --> 00:12:16,000
 and they come up anyway.

126
00:12:16,000 --> 00:12:23,470
 We get angry, we become greedy, and we're deluded all the

127
00:12:23,470 --> 00:12:25,000
 time.

128
00:12:25,000 --> 00:12:30,000
 It takes a special state of mind to overcome these.

129
00:12:30,000 --> 00:12:42,510
 Special states of kindness, compassion, caring, states of

130
00:12:42,510 --> 00:12:48,000
 morality and abstention,

131
00:12:48,000 --> 00:12:59,000
 states of wisdom and understanding.

132
00:12:59,000 --> 00:13:02,820
 This is a very important reason why we should practice

133
00:13:02,820 --> 00:13:04,000
 meditation.

134
00:13:04,000 --> 00:13:07,000
 Meditation is the development of the mind.

135
00:13:07,000 --> 00:13:13,000
 Without a developed mind, our animal minds, our hell minds,

136
00:13:13,000 --> 00:13:16,000
 our ghost mind will come up by itself.

137
00:13:16,000 --> 00:13:20,000
 We don't have to bring it up or wish for it to come up.

138
00:13:20,000 --> 00:13:24,060
 It's the natural state for us to be greedy, angry and del

139
00:13:24,060 --> 00:13:26,000
uded again and again.

140
00:13:26,000 --> 00:13:29,000
 We have to work hard to overcome these.

141
00:13:29,000 --> 00:13:32,380
 So the meaning is then we have to work hard just to be

142
00:13:32,380 --> 00:13:34,000
 happy, just to be at peace.

143
00:13:34,000 --> 00:13:38,480
 Happiness and peace don't come from slacking off, not in

144
00:13:38,480 --> 00:13:41,000
 the way we think that they do.

145
00:13:41,000 --> 00:13:45,190
 We think that by taking it easy, good things will come and

146
00:13:45,190 --> 00:13:47,000
 our minds will be happy.

147
00:13:47,000 --> 00:13:51,410
 And it's not really the case, we find ourselves becoming

148
00:13:51,410 --> 00:13:54,000
 angry and greedy and deluded.

149
00:13:54,000 --> 00:13:58,000
 But when we develop our minds, when we work on our minds,

150
00:13:58,000 --> 00:14:09,000
 try to see things as they are.

151
00:14:09,000 --> 00:14:11,680
 We're able to do away with these. We're able to overcome

152
00:14:11,680 --> 00:14:12,000
 these.

153
00:14:12,000 --> 00:14:20,230
 We're able to give rise to states of peace, of clarity of

154
00:14:20,230 --> 00:14:21,000
 mind.

155
00:14:21,000 --> 00:14:26,000
 Where we're no longer angry at bad things.

156
00:14:26,000 --> 00:14:30,000
 When bad things come to us, unpleasant things come to us.

157
00:14:30,000 --> 00:14:33,940
 We no longer get angry at them. We no longer see them as

158
00:14:33,940 --> 00:14:36,000
 unpleasant.

159
00:14:36,000 --> 00:14:41,000
 We're retraining our minds to see things as they are.

160
00:14:41,000 --> 00:14:44,000
 And it really is a retraining.

161
00:14:44,000 --> 00:14:47,000
 You can't say, "Oh, I already see things as they are,"

162
00:14:47,000 --> 00:14:49,000
 because we don't.

163
00:14:49,000 --> 00:14:52,000
 Every time we see something, we have a judgment of it.

164
00:14:52,000 --> 00:14:54,000
 Here's something, we have a judgment of it.

165
00:14:54,000 --> 00:14:57,000
 That's our natural state. We don't have to work to judge.

166
00:14:57,000 --> 00:15:02,000
 Judging is our ordinary way of behavior.

167
00:15:02,000 --> 00:15:07,090
 We have these chain reactions, and we have to break the

168
00:15:07,090 --> 00:15:08,000
 chain.

169
00:15:08,000 --> 00:15:12,000
 The work we have to do is to break the chain.

170
00:15:12,000 --> 00:15:20,320
 You can't just expect the chain to not form. It forms by

171
00:15:20,320 --> 00:15:22,000
 nature.

172
00:15:22,000 --> 00:15:26,000
 So meditation has a result. It has a very clear result.

173
00:15:26,000 --> 00:15:30,000
 When we do acknowledge, we see things clearly.

174
00:15:30,000 --> 00:15:34,090
 And that's an incredible benefit, because then the chain is

175
00:15:34,090 --> 00:15:35,000
 broken.

176
00:15:35,000 --> 00:15:40,000
 We don't go into liking or disliking.

177
00:15:40,000 --> 00:15:44,000
 We don't go on to judge the object.

178
00:15:44,000 --> 00:15:48,000
 Simply see it for what it is.

179
00:15:48,000 --> 00:15:53,000
 When we say seeing, seeing or hearing, hearing.

180
00:15:53,000 --> 00:15:56,330
 If we're clear about it, then there's no liking or disl

181
00:15:56,330 --> 00:15:57,000
iking.

182
00:15:57,000 --> 00:16:02,000
 There's no judging. There's no good or bad.

183
00:16:02,000 --> 00:16:09,000
 There just is. There is what there is.

184
00:16:09,000 --> 00:16:38,000
 [no speech detected]

185
00:16:38,000 --> 00:16:41,000
 The great thing about meditation is not only this,

186
00:16:41,000 --> 00:16:46,000
 but at the moment when we're mindful, our mind is clear.

187
00:16:46,000 --> 00:16:49,000
 The great thing is that it slowly changes our mind,

188
00:16:49,000 --> 00:16:52,000
 so that that becomes the default.

189
00:16:52,000 --> 00:16:56,000
 So that the default is seeing things clearly.

190
00:16:56,000 --> 00:16:59,190
 Slowly we begin to see things in a way that we didn't see

191
00:16:59,190 --> 00:17:00,000
 them before.

192
00:17:00,000 --> 00:17:03,830
 We begin to understand things in a way that we didn't

193
00:17:03,830 --> 00:17:05,000
 understand them before.

194
00:17:05,000 --> 00:17:15,990
 And our greed and our anger and our delusion become foreign

195
00:17:15,990 --> 00:17:16,000
.

196
00:17:16,000 --> 00:17:21,000
 Become foreign states that come in once in a while

197
00:17:21,000 --> 00:17:25,000
 and eventually don't come in at all.

198
00:17:25,000 --> 00:17:40,000
 We're not seeing them being replaced by the opposite.

199
00:17:40,000 --> 00:17:45,320
 And this is really the difference, there's a difference

200
00:17:45,320 --> 00:17:46,000
 between

201
00:17:46,000 --> 00:17:49,000
 the negative states and the positive states.

202
00:17:49,000 --> 00:17:54,000
 If you create enough wisdom and understanding,

203
00:17:54,000 --> 00:18:00,000
 you can never give rise to unwholesome states again.

204
00:18:00,000 --> 00:18:06,000
 Because you can't do away with your wisdom.

205
00:18:06,000 --> 00:18:09,000
 You can't give up your understanding.

206
00:18:09,000 --> 00:18:12,660
 Ignorance is something we can do away with by understanding

207
00:18:12,660 --> 00:18:13,000
.

208
00:18:13,000 --> 00:18:16,160
 But once you understand something, you can't go back to

209
00:18:16,160 --> 00:18:18,000
 being ignorant about it.

210
00:18:18,000 --> 00:18:30,220
 Once you see the way things work, your whole universe, your

211
00:18:30,220 --> 00:18:32,000
 whole existence changes.

212
00:18:32,000 --> 00:18:38,000
 It becomes lighter, it becomes simpler, it becomes cleaner,

213
00:18:38,000 --> 00:18:40,000
 it becomes clearer.

214
00:18:40,000 --> 00:18:51,810
 And there's less of the complication that we find in

215
00:18:51,810 --> 00:18:56,000
 ordinary life.

216
00:18:56,000 --> 00:18:58,750
 And this is really leading to what the Buddha meant by Nibb

217
00:18:58,750 --> 00:18:59,000
ana.

218
00:18:59,000 --> 00:19:03,000
 The real result of meditation is Nibbana, or freedom.

219
00:19:03,000 --> 00:19:06,000
 And we don't ever go back.

220
00:19:06,000 --> 00:19:10,590
 When a person enters into the light, they don't go back to

221
00:19:10,590 --> 00:19:12,000
 the darkness.

222
00:19:12,000 --> 00:19:18,000
 When a person sees clearly, they can never be fooled again.

223
00:19:18,000 --> 00:19:21,490
 All it takes is to see the truth once, clearly and

224
00:19:21,490 --> 00:19:23,000
 completely,

225
00:19:23,000 --> 00:19:27,000
 and you never go back to falsehood.

226
00:19:27,000 --> 00:19:32,000
 Your whole universe changes, your whole path changes.

227
00:19:32,000 --> 00:19:36,380
 We talk about the afterlife. Well, when you see things

228
00:19:36,380 --> 00:19:37,000
 clearly, your afterlife changes.

229
00:19:37,000 --> 00:19:40,000
 Your future changes.

230
00:19:40,000 --> 00:19:43,660
 Simply by changing the present, you can change your whole

231
00:19:43,660 --> 00:19:44,000
 life.

232
00:19:44,000 --> 00:19:49,200
 When the future looks dismal or unclear, simply by med

233
00:19:49,200 --> 00:19:50,000
itating,

234
00:19:50,000 --> 00:19:53,000
 you can change the whole of your future.

235
00:19:53,000 --> 00:19:58,320
 Everything changes for you when you start to see things

236
00:19:58,320 --> 00:19:59,000
 clearly,

237
00:19:59,000 --> 00:20:03,000
 when you start to give rise to wisdom.

238
00:20:03,000 --> 00:20:07,000
 So meditation certainly does have a result.

239
00:20:07,000 --> 00:20:10,530
 It's something with great benefit, and it's something that

240
00:20:10,530 --> 00:20:13,000
's necessary to retrain our minds

241
00:20:13,000 --> 00:20:16,000
 and to keep our minds from falling back again and again

242
00:20:16,000 --> 00:20:20,000
 into unwholesome state.

243
00:20:20,000 --> 00:20:24,000
 This is something that we should think about often.

244
00:20:24,000 --> 00:20:28,460
 We should review our meditation and ask ourselves whether

245
00:20:28,460 --> 00:20:31,000
 we really are putting our whole heart into the meditation

246
00:20:31,000 --> 00:20:34,290
 or whether we've lost sight of the goal and the reason and

247
00:20:34,290 --> 00:20:35,000
 the benefit

248
00:20:35,000 --> 00:20:41,000
 and therefore are not putting our whole heart into it.

249
00:20:41,000 --> 00:20:43,820
 It's only with putting our whole heart into it that we can

250
00:20:43,820 --> 00:20:45,000
 really expect results,

251
00:20:45,000 --> 00:20:49,000
 not just by walking and sitting blindly.

252
00:20:49,000 --> 00:20:55,000
 So it's worth encouraging in this way.

253
00:20:55,000 --> 00:21:00,000
 And this is the teaching for today.

254
00:21:00,000 --> 00:21:04,000
 I hope that's reduced to everyone.

255
00:21:04,000 --> 00:21:07,000
 Now we'll continue on with the practical part.

256
00:21:07,000 --> 00:21:10,630
 First we'll do mindful prostration and then walking and

257
00:21:10,630 --> 00:21:12,000
 then sitting.

