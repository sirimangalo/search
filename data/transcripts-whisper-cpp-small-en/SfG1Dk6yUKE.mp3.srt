1
00:00:00,000 --> 00:00:04,270
 Okay, ready. You can one overdose on Buddhist theory and D

2
00:00:04,270 --> 00:00:07,360
hamma. It seems to me that most

3
00:00:07,360 --> 00:00:10,460
 benefits come from meditation once the Buddhist basics are

4
00:00:10,460 --> 00:00:11,280
 understood.

5
00:00:11,280 --> 00:00:16,180
 Yes, well if you read, if you heard the, watched the latest

6
00:00:16,180 --> 00:00:17,680
 video on my channel,

7
00:00:17,680 --> 00:00:23,410
 my, our Venerable Teacher brought up a very good point and

8
00:00:23,410 --> 00:00:25,360
 that is that there are two ways of study.

9
00:00:26,160 --> 00:00:29,480
 There is studying from beginning to end and they call the

10
00:00:29,480 --> 00:00:32,480
 Andap which means in order and then

11
00:00:32,480 --> 00:00:36,730
 there's the studying the Sandot which means with Sandot

12
00:00:36,730 --> 00:00:40,240
 means contentment, Santuti, which means

13
00:00:40,240 --> 00:00:43,690
 just enough. So there are two ways to study. If you, if you

14
00:00:43,690 --> 00:00:47,600
 want to study, learn the whole of the

15
00:00:47,600 --> 00:00:50,290
 Buddhist teaching or if you want to really study then you

16
00:00:50,290 --> 00:00:53,360
 have to learn, really memorize as much

17
00:00:53,360 --> 00:00:54,690
 as you can. If you can memorize the whole of the three tip

18
00:00:54,690 --> 00:00:58,720
itikas, three pittikas, then you memorize

19
00:00:58,720 --> 00:01:01,570
 all three of them. If you can memorize one, then you memor

20
00:01:01,570 --> 00:01:04,320
ize one and you discuss it and you, you,

21
00:01:04,320 --> 00:01:09,070
 you analyze it and so on. But what he made clear is that

22
00:01:09,070 --> 00:01:12,400
 for the meditation practice all you need

23
00:01:12,400 --> 00:01:14,950
 is the, for example, the four satipatthana. If you can

24
00:01:14,950 --> 00:01:16,960
 learn the four satipatthana, that's enough.

25
00:01:16,960 --> 00:01:20,840
 So the point being that in Buddhism there is no requirement

26
00:01:20,840 --> 00:01:22,320
 that you study all of the Buddhist

27
00:01:22,320 --> 00:01:25,380
 teaching. And in fact, if you start studying the Buddhist

28
00:01:25,380 --> 00:01:27,760
 teaching, you can see that. And I think

29
00:01:27,760 --> 00:01:31,130
 it's difficult, especially as a practical meditator to miss

30
00:01:31,130 --> 00:01:33,120
 that, that the Buddha didn't teach the

31
00:01:33,120 --> 00:01:35,810
 tipitika to anyone, right? He didn't say, okay, now we're

32
00:01:35,810 --> 00:01:37,760
 going to start with the vinya-pittika,

33
00:01:37,760 --> 00:01:41,330
 sit down and hold onto your hats. We got a six month course

34
00:01:41,330 --> 00:01:43,360
 or a five year course. How long would

35
00:01:43,360 --> 00:01:46,920
 it take to teach the whole tipitika? He taught one sutta

36
00:01:46,920 --> 00:01:49,280
 and this is the joke I was joking. I say,

37
00:01:49,280 --> 00:01:51,340
 okay, and from now on what we're going to do is I'm going

38
00:01:51,340 --> 00:01:52,720
 to give one dhammatag and you have to

39
00:01:52,720 --> 00:01:55,890
 memorize that dhammatag and that's your dhammatag and

40
00:01:55,890 --> 00:01:58,880
 become enlightened on that. Because everyone's

41
00:01:58,880 --> 00:02:01,620
 saying, where's the next video? Where's the next video? And

42
00:02:01,620 --> 00:02:04,640
 that's, if you read the suttas, you see

43
00:02:04,640 --> 00:02:07,660
 that all the Buddha would give one, often one discourse

44
00:02:07,660 --> 00:02:09,520
 would be enough and they'd take that

45
00:02:09,520 --> 00:02:13,300
 discourse away and become enlightened based on it. Now we

46
00:02:13,300 --> 00:02:16,080
 obviously don't expect that, but we do expect

47
00:02:16,080 --> 00:02:21,530
 that you focus more on the practice. Now with an exception

48
00:02:21,530 --> 00:02:23,920
 Mahasya Sayadaw makes a really good point

49
00:02:23,920 --> 00:02:28,140
 that I think is worth bringing up is that as he says, if

50
00:02:28,140 --> 00:02:31,440
 you want to go it alone, if you don't

51
00:02:31,440 --> 00:02:34,720
 have a teacher, if you don't have the opportunity to

52
00:02:34,720 --> 00:02:37,520
 undertake a course where someone, by teacher

53
00:02:37,520 --> 00:02:40,630
 means, where you undertake a course with someone who is

54
00:02:40,630 --> 00:02:43,120
 daily adjusting your practice and catching

55
00:02:43,120 --> 00:02:48,930
 you when you and adjusting your understanding and pushing

56
00:02:48,930 --> 00:02:52,240
 you back on track when you're getting too

57
00:02:52,240 --> 00:02:55,800
 overconfident to pull you back, when you're getting too

58
00:02:55,800 --> 00:03:03,120
 underconfident or skeptical or unsure of

59
00:03:03,120 --> 00:03:06,680
 yourself to encourage you to push you forward, when you get

60
00:03:06,680 --> 00:03:09,120
 off track to keep you, to pull you back on

61
00:03:09,120 --> 00:03:12,130
 track when you're on track to keep you on track. These are

62
00:03:12,130 --> 00:03:13,840
 the four duties of a teacher. If you

63
00:03:13,840 --> 00:03:17,270
 have someone like that then all you need is the bare bare

64
00:03:17,270 --> 00:03:19,760
 minimum of theory. If you can memorize

65
00:03:19,760 --> 00:03:22,590
 the four satipatanas, what are the four foundations of

66
00:03:22,590 --> 00:03:24,960
 mindfulness, then that's more than enough. Some

67
00:03:24,960 --> 00:03:27,040
 people don't even do this, they think they can go and med

68
00:03:27,040 --> 00:03:29,600
itate and don't do this. You need to memorize

69
00:03:29,600 --> 00:03:32,460
 at least for example the four foundations of mindfulness.

70
00:03:32,460 --> 00:03:34,160
 If you're practicing mindfulness

71
00:03:34,160 --> 00:03:37,570
 everyone should memorize these four things. It's four words

72
00:03:37,570 --> 00:03:40,000
 body, feelings, mind, dhamma. If you can

73
00:03:40,000 --> 00:03:43,150
 memorize those four words that's by my teacher's word and I

74
00:03:43,150 --> 00:03:45,280
'm standing by him that's enough theory.

75
00:03:45,280 --> 00:03:48,760
 If you want to go it alone on the other hand as Mahasya Say

76
00:03:48,760 --> 00:03:49,440
adaw says,

77
00:03:49,440 --> 00:03:52,120
 then you've got your work cut out for you. Then you really

78
00:03:52,120 --> 00:03:54,000
 have to learn the whole tathipitaka.

79
00:03:54,000 --> 00:03:56,760
 You have to have everything because you've got to be ready

80
00:03:56,760 --> 00:03:58,480
 to catch absolutely everything that

81
00:03:58,480 --> 00:04:01,340
 might go wrong. I mean maybe that's overkill not the whole

82
00:04:01,340 --> 00:04:03,280
 tathipitaka but you have to memorize,

83
00:04:03,280 --> 00:04:05,940
 you have to know quite a bit. This is how Mahasya Sayadaw

84
00:04:05,940 --> 00:04:07,840
 puts it and I think that's fair

85
00:04:07,840 --> 00:04:11,140
 that without a teacher in order to not get distracted you

86
00:04:11,140 --> 00:04:12,160
 need a lot of theory.

87
00:04:12,160 --> 00:04:18,480
 You could get a half way from reading books on meditation.

88
00:04:18,480 --> 00:04:20,400
 They have modern

89
00:04:20,400 --> 00:04:23,180
 commentaries, modern meditation teachers like someone's

90
00:04:23,180 --> 00:04:25,040
 mentioning. Buddhadasa has the handbook

91
00:04:25,040 --> 00:04:29,600
 for mankind. You can follow that handbook and consider that

92
00:04:29,600 --> 00:04:31,600
 to be a middle ground where it's

93
00:04:31,600 --> 00:04:39,440
 kind of a teacher but it's a little bit more theoretical to

94
00:04:39,440 --> 00:04:44,240
 hopefully keep you in the realm

95
00:04:44,240 --> 00:04:55,270
 of right practice. You said everything there's to say about

96
00:04:55,270 --> 00:04:56,320
 that question.

97
00:04:59,760 --> 00:05:03,820
 The other aspect of the question is in regards to the

98
00:05:03,820 --> 00:05:06,160
 benefits because if you ask what are the

99
00:05:06,160 --> 00:05:11,060
 benefits to Buddhist theory and study versus the benefits

100
00:05:11,060 --> 00:05:14,240
 of practice, I mean well there's not much

101
00:05:14,240 --> 00:05:20,650
 to say. It's quite clear that if you eat the food you get

102
00:05:20,650 --> 00:05:22,960
 full. If you just sit there looking at the

103
00:05:22,960 --> 00:05:26,170
 menu you don't get anything but it's true. Many people will

104
00:05:26,170 --> 00:05:28,880
 start Dhamma groups just to study and

105
00:05:28,880 --> 00:05:36,160
 just to discuss and just to go over the texts and revel in

106
00:05:36,160 --> 00:05:40,080
 the profundity of them, maybe even

107
00:05:40,080 --> 00:05:44,710
 philosophizing and trying to relate them back to one's own

108
00:05:44,710 --> 00:05:47,280
 life in a very intellectual manner.

109
00:05:47,280 --> 00:05:51,280
 There are Dhamma groups around the world that do that and

110
00:05:51,280 --> 00:05:53,680
 think that they're getting real benefit

111
00:05:53,680 --> 00:05:56,050
 from it. They feel like it's really enriching their lives

112
00:05:56,050 --> 00:05:57,600
 and that they are active Buddhists.

113
00:05:58,720 --> 00:06:00,730
 These kind of people get amazed when they come to actually

114
00:06:00,730 --> 00:06:03,040
 practice intensive meditation and they

115
00:06:03,040 --> 00:06:08,020
 say, "Oh, so this is what impermanence means. It's not just

116
00:06:08,020 --> 00:06:11,120
 about I was young once and now I'm old or

117
00:06:11,120 --> 00:06:15,140
 yeah, yesterday was different from today or wow, the next

118
00:06:15,140 --> 00:06:18,000
 moment who knows what's going to come.

119
00:06:18,000 --> 00:06:21,030
 Maybe I'll break a leg or maybe I'll get run over by a car

120
00:06:21,030 --> 00:06:23,120
 or something." No, they can see

121
00:06:23,680 --> 00:06:31,520
 moment to moment the development of insight. Mahasya Saita

122
00:06:31,520 --> 00:06:33,360
 makes another good point about this.

123
00:06:33,360 --> 00:06:37,230
 He says some people say, "Well, couldn't you just study the

124
00:06:37,230 --> 00:06:39,600
 16 stages of knowledge?"

125
00:06:39,600 --> 00:06:41,950
 Like he teaches based on this commentary that goes through

126
00:06:41,950 --> 00:06:43,440
 the 16 stages of knowledge.

127
00:06:43,440 --> 00:06:47,250
 "Couldn't you just teach in a study course these 16 stages

128
00:06:47,250 --> 00:06:49,200
 of knowledge and talk to your students

129
00:06:49,200 --> 00:06:52,220
 about it and say, 'Okay, so now we're at the first stage.

130
00:06:52,220 --> 00:06:53,760
 Let's look at our body and say,

131
00:06:53,760 --> 00:06:56,220
 'Is this body a yes? This is body. Is this mind? Yes, this

132
00:06:56,220 --> 00:06:57,760
 is mind. Okay, now we're at the first

133
00:06:57,760 --> 00:07:01,820
 stage of knowledge.'" Mahasya Saita even entertains it. He

134
00:07:01,820 --> 00:07:04,480
 says, "I could go with that if they want to

135
00:07:04,480 --> 00:07:08,470
 say this. I could go with that if they could say that they

136
00:07:08,470 --> 00:07:10,960
're developing right concentration."

137
00:07:10,960 --> 00:07:14,020
 It's clear that they don't have right concentration because

138
00:07:14,020 --> 00:07:15,760
 they're looking at the body and saying,

139
00:07:16,400 --> 00:07:18,260
 "Yes, that's body," and they're looking at the mind and

140
00:07:18,260 --> 00:07:20,000
 saying, "Yes, that's mind," but they don't have

141
00:07:20,000 --> 00:07:24,030
 the concentration that allows the mind to really and truly

142
00:07:24,030 --> 00:07:25,920
 see that this is body, that this is

143
00:07:25,920 --> 00:07:30,590
 mind, and to be able to separate them out. This is the

144
00:07:30,590 --> 00:07:33,200
 categorical difference. For most people,

145
00:07:33,200 --> 00:07:35,230
 this is if you've studied Buddhism, you can see how

146
00:07:35,230 --> 00:07:38,000
 critical the Buddha is of monks who just study.

147
00:07:38,000 --> 00:07:42,950
 But for some people, they don't realize the difference

148
00:07:42,950 --> 00:07:44,880
 between, for example, watching all

149
00:07:44,880 --> 00:07:49,080
 of my YouTube videos and watching one of the YouTube videos

150
00:07:49,080 --> 00:07:51,120
, that's how to practice meditation

151
00:07:51,120 --> 00:07:54,600
 and actually meditating according to that. It's quite

152
00:07:54,600 --> 00:07:57,280
 different. It's a lot easier to watch hours

153
00:07:57,280 --> 00:08:00,270
 and hours of YouTube videos than it is to even do one hour

154
00:08:00,270 --> 00:08:03,840
 of meditation practice. For that

155
00:08:03,840 --> 00:08:07,180
 reason, the benefit is on a whole other level as well,

156
00:08:07,180 --> 00:08:08,800
 which you gain from it.

157
00:08:14,000 --> 00:08:19,820
 There is a story about monks. There was an older monk who

158
00:08:19,820 --> 00:08:22,400
 just meditated and didn't study any

159
00:08:22,400 --> 00:08:30,760
 Dharma. Other monks called him for that. He said, "Well, I

160
00:08:30,760 --> 00:08:36,160
'm old, so I prefer to meditate."

161
00:08:37,040 --> 00:08:41,880
 They told the Buddha, "This monk is not studying. He should

162
00:08:41,880 --> 00:08:43,680
 study the Dharma."

163
00:08:43,680 --> 00:08:50,400
 The Buddha said, "As far as I remember, that's fine for him

164
00:08:50,400 --> 00:08:52,320
 just to meditate."

165
00:08:52,320 --> 00:08:56,470
 There are several stories similar to that. One of them even

166
00:08:56,470 --> 00:08:58,640
 says, "This is my son. Who are you?"

167
00:08:58,640 --> 00:09:04,760
 The meaning of this, because one of them was an Arahant. No

168
00:09:04,760 --> 00:09:06,000
, the one who had actually meditated

169
00:09:06,000 --> 00:09:09,400
 was an Arahant. The one whose dad he was still a worldling,

170
00:09:09,400 --> 00:09:12,720
 who's basically, "Who are you to talk

171
00:09:12,720 --> 00:09:16,480
 to my son that way?" Because the son means someone who has

172
00:09:16,480 --> 00:09:19,760
 actually inherited his Buddhist fortune,

173
00:09:19,760 --> 00:09:25,520
 the Buddhist treasure.

