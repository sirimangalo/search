1
00:00:00,000 --> 00:00:09,630
 Okay, and we are starting Chapter 11 in the Gisudimagap,

2
00:00:09,630 --> 00:00:13,780
 page 337, and Emperor of Icecream,

3
00:00:13,780 --> 00:00:16,340
 could you start us off?

4
00:00:16,340 --> 00:00:17,340
 Sure.

5
00:00:17,340 --> 00:00:21,900
 Chapter 11, Concentration, Conclusion, Nutriment and the

6
00:00:21,900 --> 00:00:24,780
 Elements, Perception of Repulsiveness

7
00:00:24,780 --> 00:00:27,160
 and Nutriment.

8
00:00:27,160 --> 00:00:30,070
 Now comes the description of the development of the

9
00:00:30,070 --> 00:00:32,520
 perception of repulsiveness in nutriment,

10
00:00:32,520 --> 00:00:37,030
 which was listed as the one perception next to the imm

11
00:00:37,030 --> 00:00:38,720
aterial states.

12
00:00:38,720 --> 00:00:45,000
 Herein it nourishes Ahara tea, literally brings on.

13
00:00:45,000 --> 00:00:50,880
 Thus it is nutriment, Ahara, literally bringing on.

14
00:00:50,880 --> 00:00:55,130
 That is of four kinds as physical nutriment, nutriment

15
00:00:55,130 --> 00:00:57,560
 consisting of contact, nutriment

16
00:00:57,560 --> 00:01:02,650
 consisting of mental volition, and nutriment consisting of

17
00:01:02,650 --> 00:01:04,080
 consciousness.

18
00:01:04,080 --> 00:01:15,440
 But what is it here that nourishes, brings on what?

19
00:01:15,440 --> 00:01:21,230
 The nutriment, Kavalin, Hara, Hara, nourishes, brings on

20
00:01:21,230 --> 00:01:25,320
 the materiality of the octet that

21
00:01:25,320 --> 00:01:30,520
 has nutritive essence as state.

22
00:01:30,520 --> 00:01:34,030
 Contact as nutriment nourishes, brings on the three kinds

23
00:01:34,030 --> 00:01:34,960
 of feeling.

24
00:01:34,960 --> 00:01:40,460
 Mental volition as nutriment nourishes, brings on revered

25
00:01:40,460 --> 00:01:44,400
 linking in the three kinds of becoming.

26
00:01:44,400 --> 00:01:48,400
 Notice as nutriment nourishes, brings on mentality,

27
00:01:48,400 --> 00:01:55,160
 materiality at the moment of revered linking.

28
00:01:55,160 --> 00:02:00,790
 Now when there is physical nutriment, there is attachment,

29
00:02:00,790 --> 00:02:02,960
 which brings peril.

30
00:02:02,960 --> 00:02:08,500
 When there is nutriment as contact, there is approaching,

31
00:02:08,500 --> 00:02:10,360
 which brings peril.

32
00:02:10,360 --> 00:02:15,370
 When there is nutriment as mental volition, there is

33
00:02:15,370 --> 00:02:19,320
 revered linking, which brings peril.

34
00:02:19,320 --> 00:02:26,140
 And to show how they bring fear thus, physical nutriment

35
00:02:26,140 --> 00:02:30,640
 should be illustrated by the symbol

36
00:02:30,640 --> 00:02:34,400
 of a child's flesh.

37
00:02:34,400 --> 00:02:42,520
 Contact as nutriment by the symbol of the higher-less call.

38
00:02:42,520 --> 00:02:48,200
 Mental volition as nutriment by the symbol of the pit of

39
00:02:48,200 --> 00:02:49,880
 leaf-calls.

40
00:02:49,880 --> 00:02:55,160
 And consciousness as nutriment by the symbol of the hundred

41
00:02:55,160 --> 00:02:56,040
 spears.

42
00:02:56,040 --> 00:03:11,800
 Hi, Kathy.

43
00:03:11,800 --> 00:03:21,560
 Do you have a mic hooked up to be able to read for us?

44
00:03:21,560 --> 00:03:24,770
 I'm not hearing you, so possibly your mic isn't hooked up

45
00:03:24,770 --> 00:03:25,520
 just yet.

46
00:03:25,520 --> 00:03:28,320
 Maybe we'll go on to the next person.

47
00:03:28,320 --> 00:03:31,640
 But if you are able to, just break in next time.

48
00:03:31,640 --> 00:03:32,640
 Thank you.

49
00:03:32,640 --> 00:03:35,960
 Laszlo, could you read for us?

50
00:03:35,960 --> 00:03:36,960
 Sure.

51
00:03:36,960 --> 00:03:41,330
 But of these four kinds of nutriment, it is only physical

52
00:03:41,330 --> 00:03:43,600
 nutriment, classed as what is

53
00:03:43,600 --> 00:03:48,770
 eaten, drunk, chewed and tasted, that it is intended here

54
00:03:48,770 --> 00:03:51,320
 as nutriment in distance.

55
00:03:51,320 --> 00:03:55,710
 The perception arisen as the apprehension of the repulsive

56
00:03:55,710 --> 00:03:57,360
 aspect in that nutriment

57
00:03:57,360 --> 00:04:01,160
 is perception of repulsiveness in nutriment.

58
00:04:01,160 --> 00:04:12,520
 Hi, Marilyn, we're not hearing you.

59
00:04:12,520 --> 00:04:15,880
 Can you hear me now?

60
00:04:15,880 --> 00:04:17,840
 Yes, thank you.

61
00:04:17,840 --> 00:04:20,600
 Anyone who wants to develop that perception of repuls

62
00:04:20,600 --> 00:04:22,480
iveness in nutriment should learn

63
00:04:22,480 --> 00:04:25,970
 the meditation subject and see that he has no uncertainty

64
00:04:25,970 --> 00:04:27,640
 about even a single word of

65
00:04:27,640 --> 00:04:29,640
 what he has learned.

66
00:04:29,640 --> 00:04:32,780
 Then he should go into solitary retreat and review repuls

67
00:04:32,780 --> 00:04:34,440
iveness in ten aspects in the

68
00:04:34,440 --> 00:04:38,650
 physical nutriment classified as what is eaten, drunk, che

69
00:04:38,650 --> 00:04:40,960
wed and tasted, that is to say,

70
00:04:40,960 --> 00:04:46,310
 as to going, keeping, using, secretion, receptacles, what

71
00:04:46,310 --> 00:04:49,400
 is uncooked, undigested, what is cooked,

72
00:04:49,400 --> 00:04:56,680
 digested, fruit, outflow and searing.

73
00:04:56,680 --> 00:05:00,620
 Herein as to going, even when a man has gone forth in so

74
00:05:00,620 --> 00:05:03,440
 mighty a dispensation, still after

75
00:05:03,440 --> 00:05:07,070
 he has perhaps spent all night reciting the enlightened one

76
00:05:07,070 --> 00:05:08,960
's words or doing the ascetic's

77
00:05:08,960 --> 00:05:12,030
 work, even after he has risen early to do the duties

78
00:05:12,030 --> 00:05:14,200
 connected with the shrine terrace

79
00:05:14,200 --> 00:05:17,420
 and the enlightenment tree terrace, to set out the water

80
00:05:17,420 --> 00:05:19,200
 for drinking and washing, to

81
00:05:19,200 --> 00:05:22,490
 sweep the grounds and to see to the needs of the body,

82
00:05:22,490 --> 00:05:23,800
 after he has sat down on his

83
00:05:23,800 --> 00:05:27,450
 seat and given attention to his meditation subject twenty

84
00:05:27,450 --> 00:05:29,080
 or thirty times and got up

85
00:05:29,080 --> 00:05:32,790
 again, then he must take his bowl and outer robe, he must

86
00:05:32,790 --> 00:05:34,640
 leave behind the ascetic's woods

87
00:05:34,640 --> 00:05:38,320
 that are not crowded with people, offer the bliss of se

88
00:05:38,320 --> 00:05:40,720
clusion, possess shade and water

89
00:05:40,720 --> 00:05:43,520
 and are clean, cool, delightful places.

90
00:05:43,520 --> 00:05:46,310
 He must disregard the noble one's delight in seclusion and

91
00:05:46,310 --> 00:05:48,040
 he must set out for the village

92
00:05:48,040 --> 00:05:52,360
 in order to get nutriment as a jackal for the charnel

93
00:05:52,360 --> 00:05:53,360
 ground.

94
00:05:53,360 --> 00:05:59,120
 So it's trying to explain repulsiveness in many aspects.

95
00:05:59,120 --> 00:06:03,820
 It's going to, this is one example of exactly how detailed

96
00:06:03,820 --> 00:06:06,320
 the commenter is and how exact

97
00:06:06,320 --> 00:06:07,440
 and precise they are.

98
00:06:07,440 --> 00:06:10,740
 They really take absolutely every aspect, not just the

99
00:06:10,740 --> 00:06:12,640
 eating part, but every aspect

100
00:06:12,640 --> 00:06:16,200
 of food to be repulsive, starting from having to go for it.

101
00:06:16,200 --> 00:06:21,030
 So just having to leave the forest, having to leave your

102
00:06:21,030 --> 00:06:23,600
 pleasant place, and we're going

103
00:06:23,600 --> 00:06:28,440
 to see in the next paragraph even better.

104
00:06:28,440 --> 00:06:31,790
 It already starts to be repulsive, something that you

105
00:06:31,790 --> 00:06:33,880
 wouldn't want to do if you didn't

106
00:06:33,880 --> 00:06:34,880
 have to.

107
00:06:34,880 --> 00:06:40,730
 And he goes thus from the time when he steps down from his

108
00:06:40,730 --> 00:06:43,920
 bed or chair, he has to tread

109
00:06:43,920 --> 00:06:49,030
 on a carpet covered with dust of his feet, gecko's dro

110
00:06:49,030 --> 00:06:50,880
ppings and so on.

111
00:06:50,880 --> 00:06:53,850
 Next he has to see the doorstep which is more repulsive

112
00:06:53,850 --> 00:06:55,640
 than the inside of the room since

113
00:06:55,640 --> 00:07:00,230
 it has been often fouled with the droppings of rats, bats

114
00:07:00,230 --> 00:07:00,920
 and so on.

115
00:07:00,920 --> 00:07:04,340
 Next the lower terrace which is more repulsive than the

116
00:07:04,340 --> 00:07:06,760
 terrace above since it is all smeared

117
00:07:06,760 --> 00:07:11,480
 with droppings of owls, pigeons and so on.

118
00:07:11,480 --> 00:07:14,550
 Next the grounds which are more repulsive than the lower

119
00:07:14,550 --> 00:07:16,120
 floor since they are defiled

120
00:07:16,120 --> 00:07:20,380
 by all grass and leaves blown about the wind by sick nov

121
00:07:20,380 --> 00:07:23,400
ices, urine, excrements, bitter

122
00:07:23,400 --> 00:07:28,840
 and snot and in the rainy season by water mud and so on.

123
00:07:28,840 --> 00:07:34,810
 And he has to see the road to the monastery which is more

124
00:07:34,810 --> 00:07:38,360
 repulsive than the grounds.

125
00:07:38,360 --> 00:08:07,920
 Hi Sanka, you are muted.

126
00:08:07,920 --> 00:08:11,960
 Maybe not, I think it's quite late where Sanka is so maybe

127
00:08:11,960 --> 00:08:14,440
 we'll just bypass him for now.

128
00:08:14,440 --> 00:08:17,560
 Tina, are you able to read it?

129
00:08:17,560 --> 00:08:20,080
 Yes.

130
00:08:20,080 --> 00:08:26,150
 In due course, after standing in a debating lodge when he

131
00:08:26,150 --> 00:08:29,400
 has finished paying homage at

132
00:08:29,400 --> 00:08:35,930
 the enlightenment tree and the shrine, he sets out thinking

133
00:08:35,930 --> 00:08:36,400
.

134
00:08:36,400 --> 00:08:43,060
 Instead of looking at the shrine that is like a cluster of

135
00:08:43,060 --> 00:08:45,920
 pearls and the enlightenment

136
00:08:45,920 --> 00:08:54,140
 tree that is as lovely as a bouquet of peacock's tail

137
00:08:54,140 --> 00:09:00,000
 feathers and a boat that is as fair as

138
00:09:00,000 --> 00:09:11,610
 a god's palace, I must now turn my back on such a charming

139
00:09:11,610 --> 00:09:15,840
 place and go aboard for the

140
00:09:15,840 --> 00:09:22,710
 sake of food and on the way to the village the view of a

141
00:09:22,710 --> 00:09:29,120
 row of stumps and thrones and

142
00:09:29,120 --> 00:09:47,280
 uneven rope broken up by the force of water awaits him.

143
00:09:47,280 --> 00:09:51,330
 Next after he has put on his waist cloth as one who hides

144
00:09:51,330 --> 00:09:53,880
 an abscess and tied his waistband

145
00:09:53,880 --> 00:09:57,480
 as one who ties a bandage on a wound and robed himself in

146
00:09:57,480 --> 00:09:59,600
 his upper robes as one who hides

147
00:09:59,600 --> 00:10:03,710
 a skeleton and taken out his bowl as one who takes out a

148
00:10:03,710 --> 00:10:06,520
 pan for medicine, when he reaches

149
00:10:06,520 --> 00:10:09,750
 the vicinity of the village gate, perhaps the sight of an

150
00:10:09,750 --> 00:10:11,680
 elephant's carcass, a horse's

151
00:10:11,680 --> 00:10:15,490
 carcass, a buffalo's carcass, a human carcass, a snake's

152
00:10:15,490 --> 00:10:17,880
 carcass, or a dog's carcass awaits

153
00:10:17,880 --> 00:10:18,880
 him.

154
00:10:18,880 --> 00:10:23,430
 Not only that, but he has to suffer his nose to be assailed

155
00:10:23,430 --> 00:10:25,280
 by the smell of them.

156
00:10:25,280 --> 00:10:27,780
 Next as he stands in the village gateway, he must scan the

157
00:10:27,780 --> 00:10:28,960
 village streets in order

158
00:10:28,960 --> 00:10:35,480
 to avoid danger from savage elephants, horses, and so on.

159
00:10:35,480 --> 00:10:39,460
 So this repulsive experience beginning with the carpet that

160
00:10:39,460 --> 00:10:41,520
 has to be trodden on and ending

161
00:10:41,520 --> 00:10:45,260
 with the various kinds of carcasses that have to be seen

162
00:10:45,260 --> 00:10:47,440
 and smelled has to be undergone

163
00:10:47,440 --> 00:10:49,720
 for the sake of nutriment.

164
00:10:49,720 --> 00:10:53,240
 Oh, nutriment is indeed a repulsive thing.

165
00:10:53,240 --> 00:10:58,920
 This is how repulsiveness should be reviewed as to going.

166
00:10:58,920 --> 00:11:04,440
 Two, how was to seeking?

167
00:11:04,440 --> 00:11:08,270
 When he has endured the repulsiveness of going in this way

168
00:11:08,270 --> 00:11:10,240
 and has gone into the village

169
00:11:10,240 --> 00:11:15,460
 and is clothed in his cloak of patches, he has to wander in

170
00:11:15,460 --> 00:11:17,720
 the village streets from

171
00:11:17,720 --> 00:11:22,240
 house to house like a beggar with a ditch in his hand.

172
00:11:22,240 --> 00:11:27,170
 And in the rainy season, wherever he threads his feet

173
00:11:27,170 --> 00:11:29,800
 sinking to water and mired up to

174
00:11:29,800 --> 00:11:36,170
 the pledge of the cows, he has to hold the wall in one hand

175
00:11:36,170 --> 00:11:38,280
 and he's robbed up with the

176
00:11:38,280 --> 00:11:39,600
 other.

177
00:11:39,600 --> 00:11:44,320
 In the hot season, he has to go about with this body cover

178
00:11:44,320 --> 00:11:46,200
 with the deer, grass, and

179
00:11:46,200 --> 00:11:49,720
 those blown about by the wind.

180
00:11:49,720 --> 00:11:54,640
 On reaching such and such a house door, he has to see and

181
00:11:54,640 --> 00:11:57,360
 even to thread in gutters and

182
00:11:57,360 --> 00:12:03,600
 cesspools covered with blue bottles and setting with all

183
00:12:03,600 --> 00:12:07,280
 the species of worms, all messed

184
00:12:07,280 --> 00:12:13,790
 up with fish washings, meat washings, rice washings, a sp

185
00:12:13,790 --> 00:12:17,280
ittle, a snot, dogs, and pigs

186
00:12:17,280 --> 00:12:20,840
 excrement and whatnot.

187
00:12:20,840 --> 00:12:26,680
 From which flies come up and settle on his outer cloak of

188
00:12:26,680 --> 00:12:29,560
 patches and on his bowl and

189
00:12:29,560 --> 00:12:32,920
 on his head.

190
00:12:32,920 --> 00:12:39,400
 And when he enters a house, some give and some do not.

191
00:12:39,400 --> 00:12:45,040
 And when they give, some give yesterday cooked rice and

192
00:12:45,040 --> 00:12:48,480
 still stale cakes and then sipped

193
00:12:48,480 --> 00:12:52,560
 jelly, sauce and so on.

194
00:12:52,560 --> 00:12:59,360
 Some not even say, please pass on, bear in a world fear.

195
00:12:59,360 --> 00:13:25,200
 Others keep silent as if they did not see him.

196
00:13:25,200 --> 00:13:28,320
 Some avoid their faces.

197
00:13:28,320 --> 00:13:34,590
 Others treat him with harsh words such as "go away, you

198
00:13:34,590 --> 00:13:36,440
 blackhead".

199
00:13:36,440 --> 00:13:44,040
 When he has wandered for hours in the village in this way

200
00:13:44,040 --> 00:13:48,200
 like a beggar, he has to depart

201
00:13:48,200 --> 00:13:49,080
 from it.

202
00:13:49,080 --> 00:14:15,360
 Should I continue?

203
00:14:15,360 --> 00:14:18,160
 Yes please.

204
00:14:18,160 --> 00:14:21,060
 So this experience beginning with the entry into the

205
00:14:21,060 --> 00:14:23,240
 village and ending with the departure

206
00:14:23,240 --> 00:14:28,800
 from it, which is repulsive owing to the water, mud, etc.

207
00:14:28,800 --> 00:14:31,280
 that has to be trodden in and seen

208
00:14:31,280 --> 00:14:36,720
 and endured, has to be undergone for the sake of nutriment.

209
00:14:36,720 --> 00:14:40,080
 All nutriment is indeed a repulsive thing.

210
00:14:40,080 --> 00:14:47,060
 This is how repulsiveness should be reviewed as the "

211
00:14:47,060 --> 00:14:47,920
seeking".

212
00:14:47,920 --> 00:14:49,920
 How as to use it.

213
00:14:49,920 --> 00:14:52,730
 After he has sought the nutriment in this way and is

214
00:14:52,730 --> 00:14:54,680
 sitting at ease in a comfortable

215
00:14:54,680 --> 00:14:57,360
 place outside the village, then so is the water.

216
00:14:57,360 --> 00:15:01,440
 I'm not able to hear Marilynne beyond very, very slightly.

217
00:15:01,440 --> 00:15:03,360
 I can't make out what she's saying.

218
00:15:03,360 --> 00:15:05,240
 Am I the only one?

219
00:15:05,240 --> 00:15:07,360
 No, it's a very quiet.

220
00:15:07,360 --> 00:15:10,570
 Is there anything in your settings Marilynne that you can

221
00:15:10,570 --> 00:15:12,240
 turn up your output at all?

222
00:15:12,240 --> 00:15:14,760
 I'll try.

223
00:15:14,760 --> 00:15:29,600
 Yeah, I don't think so, but is this any better?

224
00:15:29,600 --> 00:15:31,160
 That's a little better, yes.

225
00:15:31,160 --> 00:15:35,280
 And I'll try and speak louder.

226
00:15:35,280 --> 00:15:39,560
 So three, how as to using.

227
00:15:39,560 --> 00:15:42,310
 After he has sought the nutriment in this way and is

228
00:15:42,310 --> 00:15:44,720
 sitting at ease in a comfortable place

229
00:15:44,720 --> 00:15:48,680
 outside the village, then so long as he has not dipped his

230
00:15:48,680 --> 00:15:50,840
 hand into it, he would be able

231
00:15:50,840 --> 00:15:55,440
 to invite a respected biku or a decent person, if he saw

232
00:15:55,440 --> 00:15:57,240
 one, to share it.

233
00:15:57,240 --> 00:16:00,300
 But as soon as he has dipped his hand into it out of desire

234
00:16:00,300 --> 00:16:01,920
 to eat, he would be ashamed

235
00:16:01,920 --> 00:16:04,240
 to say, "Take some."

236
00:16:04,240 --> 00:16:07,490
 And when he has dipped his hand in and is squeezing it up,

237
00:16:07,490 --> 00:16:09,160
 the sweat trickling down his

238
00:16:09,160 --> 00:16:18,860
 five fingers, wets any dry, crisp food there may be and

239
00:16:18,860 --> 00:16:22,040
 makes it soggy.

240
00:16:22,040 --> 00:16:24,740
 And when its good appearance has been spoiled by his

241
00:16:24,740 --> 00:16:26,560
 squeezing it up and it has faded to

242
00:16:26,560 --> 00:16:30,060
 a ball and to his mouth, then the lower teeth function as a

243
00:16:30,060 --> 00:16:31,920
 mortar, the upper teeth as a

244
00:16:31,920 --> 00:16:34,760
 pestle, and the tongue as a hand.

245
00:16:34,760 --> 00:16:38,460
 Like it's pounded there with the pestle of the teeth like a

246
00:16:38,460 --> 00:16:40,120
 dog's dinner and a dog's

247
00:16:40,120 --> 00:16:42,640
 trowel, while he turns it over and over with his tongue.

248
00:16:42,640 --> 00:16:45,960
 Then the thin spittle at the tip of the tongue smears it,

249
00:16:45,960 --> 00:16:47,600
 the spittle in the middle of the

250
00:16:47,600 --> 00:16:49,160
 tongue smears it.

251
00:16:49,160 --> 00:16:51,050
 And then the filth from the teeth and the parts where a

252
00:16:51,050 --> 00:16:52,200
 tooth stick cannot reach the

253
00:16:52,200 --> 00:16:57,400
 tongue smears it.

254
00:16:57,400 --> 00:17:01,900
 Then the smashed up and besmirred this peculiar compound,

255
00:17:01,900 --> 00:17:04,400
 now destitute of the original of

256
00:17:04,400 --> 00:17:09,060
 the colour and smell has reduced to a condition as utterly

257
00:17:09,060 --> 00:17:11,480
 nauseating as a dog's vomit in

258
00:17:11,480 --> 00:17:12,480
 a dog's trough.

259
00:17:12,480 --> 00:17:16,840
 Yet, notwithstanding that it is like this, it can be

260
00:17:16,840 --> 00:17:19,680
 swallowed because it is no longer

261
00:17:19,680 --> 00:17:21,440
 in the range of the highest focus.

262
00:17:21,440 --> 00:17:32,600
 Sorry, this is how repulsiveness should be reviewed as

263
00:17:32,600 --> 00:17:38,480
 teasing.

264
00:17:38,480 --> 00:17:52,240
 Now as to secretion, buddhas and pajjaka buddhas and wheel

265
00:17:52,240 --> 00:17:58,080
 turning monads have only

266
00:17:58,080 --> 00:18:09,560
 one of the four secretions consisting of bile.

267
00:18:09,560 --> 00:18:20,840
 Robin, how do you say this word again?

268
00:18:20,840 --> 00:18:32,890
 Flame, pulse and blood, but those with weak merit have all

269
00:18:32,890 --> 00:18:34,640
 four.

270
00:18:34,640 --> 00:18:41,630
 So when the food has arrived at the stage of being eaten

271
00:18:41,630 --> 00:18:45,320
 and it enters inside, then

272
00:18:45,320 --> 00:18:56,120
 in one whose secretion of bile is in excess, it becomes as

273
00:18:56,120 --> 00:19:02,520
 utterly now sitting as if smeared

274
00:19:02,520 --> 00:19:19,960
 with thick maduka oil.

275
00:19:19,960 --> 00:19:33,360
 As if smeared with the juice of nagabh bala leaves.

276
00:19:33,360 --> 00:19:40,960
 In one whose secretion of pulse is in excess, it is as if

277
00:19:40,960 --> 00:19:45,960
 smeared with rancid buttermilk.

278
00:19:45,960 --> 00:19:56,390
 In one whose secretion of blood is in excess, it is as

279
00:19:56,390 --> 00:20:02,720
 utterly now sitting as if smeared

280
00:20:02,720 --> 00:20:04,720
 with dye.

281
00:20:04,720 --> 00:20:17,340
 This is how repulsiveness should be reviewed as to

282
00:20:17,340 --> 00:20:19,740
 secretion.

283
00:20:19,740 --> 00:20:23,510
 How as to a receptacle, when it has gone inside the belly

284
00:20:23,510 --> 00:20:25,680
 and is smeared with one of these

285
00:20:25,680 --> 00:20:29,910
 secretions, then the receptacle it goes into is no gold

286
00:20:29,910 --> 00:20:32,200
 dish or crystal or silver dish

287
00:20:32,200 --> 00:20:34,000
 and so on.

288
00:20:34,000 --> 00:20:38,220
 On the contrary, if it is swallowed by one ten years old,

289
00:20:38,220 --> 00:20:40,200
 it finds itself in a place

290
00:20:40,200 --> 00:20:43,640
 like a cesspit unwashed for ten years.

291
00:20:43,640 --> 00:20:47,210
 If it is swallowed by one twenty years old, thirty, forty,

292
00:20:47,210 --> 00:20:49,320
 fifty, sixty, seventy, eighty,

293
00:20:49,320 --> 00:20:52,980
 ninety years old, if it is swallowed by one hundred years

294
00:20:52,980 --> 00:20:55,060
 old, it finds itself in a place

295
00:20:55,060 --> 00:21:00,060
 like a cesspit unwashed for a hundred years.

296
00:21:00,060 --> 00:21:02,940
 This is how repulsiveness should be reviewed as to recept

297
00:21:02,940 --> 00:21:03,400
acle.

298
00:21:03,400 --> 00:21:09,850
 I think we're not going to look at food ever again after

299
00:21:09,850 --> 00:21:10,400
 this.

300
00:21:10,400 --> 00:21:13,560
 Definitely not.

301
00:21:13,560 --> 00:21:26,300
 How as to what is uncooked, undigested, after this

302
00:21:26,300 --> 00:21:31,320
 nutriment has arrived at such a place for its receptacle,

303
00:21:31,320 --> 00:21:32,660
 then for as long as it remains

304
00:21:32,660 --> 00:21:36,590
 uncooked, it stays in the same place just described, which

305
00:21:36,590 --> 00:21:38,800
 is shrouded in absolute darkness,

306
00:21:38,800 --> 00:21:44,400
 provided by droughts, tainted by various smells of orger,

307
00:21:44,400 --> 00:21:48,640
 and utterly fetid and loathsome.

308
00:21:48,640 --> 00:21:51,480
 And just as when the cloud out of season has rained during

309
00:21:51,480 --> 00:21:52,960
 a drought, and bits of grass

310
00:21:52,960 --> 00:21:56,830
 and leaves and rushes and the carcasses of snakes, dogs and

311
00:21:56,830 --> 00:21:59,080
 human beings that have collected

312
00:21:59,080 --> 00:22:03,040
 in the pit at the gate of an outcast village remain there,

313
00:22:03,040 --> 00:22:04,680
 warmed by the sun's heat until

314
00:22:04,680 --> 00:22:09,510
 the pit becomes covered with froth and bubbles, so too what

315
00:22:09,510 --> 00:22:11,920
 has been swallowed that day and

316
00:22:11,920 --> 00:22:15,560
 yesterday and the day before remains there together, and

317
00:22:15,560 --> 00:22:17,400
 being smothered by the layer

318
00:22:17,400 --> 00:22:20,860
 of phlegm and covered with froth and bubbles, produced by

319
00:22:20,860 --> 00:22:23,080
 digestion through being fermented

320
00:22:23,080 --> 00:22:27,680
 by the heat of the bodily fires, it becomes quite loathsome

321
00:22:27,680 --> 00:22:27,720
.

322
00:22:27,720 --> 00:22:30,490
 This is how our pulse ofness should be reviewed as to what

323
00:22:30,490 --> 00:22:36,240
 is uncooked.

324
00:22:36,240 --> 00:22:37,240
 7.

325
00:22:37,240 --> 00:22:40,240
 How as to what is cooked.

326
00:22:40,240 --> 00:22:45,220
 When it has been completely cooked there by the bodily

327
00:22:45,220 --> 00:22:48,840
 fires, it does not turn into gold,

328
00:22:48,840 --> 00:22:54,150
 silver, etc. as the horse of gold, silver, etc. do through

329
00:22:54,150 --> 00:22:55,440
 a smell tube.

330
00:22:55,440 --> 00:23:00,330
 Instead giving off froth and bubbles, it turns into excre

331
00:23:00,330 --> 00:23:03,240
ment and fills the receptacle for

332
00:23:03,240 --> 00:23:06,000
 digested food.

333
00:23:06,000 --> 00:23:13,680
 Like brown clay, a squeeze with a smooth thin throwal and

334
00:23:13,680 --> 00:23:14,480
 packing to a tube, and it turns

335
00:23:14,480 --> 00:23:17,920
 into urine and fills the blood.

336
00:23:17,920 --> 00:23:22,990
 This is how repulsiveness should be reviewed as to what is

337
00:23:22,990 --> 00:23:24,040
 cooked.

338
00:23:24,040 --> 00:23:25,040
 8.

339
00:23:25,040 --> 00:23:28,040
 How as to fruit.

340
00:23:28,040 --> 00:23:33,220
 When it has been readily cooked, it produces the various

341
00:23:33,220 --> 00:23:35,960
 kinds of odor consisting of head,

342
00:23:35,960 --> 00:23:42,120
 ears, body hairs, nails, teeth, and the rest.

343
00:23:42,120 --> 00:23:47,420
 When wrongly cooked, it produces the hundred pieces

344
00:23:47,420 --> 00:23:49,840
 beginning with each.

345
00:23:49,840 --> 00:23:59,020
 Ranked arm, smallpox, leprosy, plague, consumption, cost,

346
00:23:59,020 --> 00:24:01,720
 flux, and so on.

347
00:24:01,720 --> 00:24:04,200
 Such is its fruit.

348
00:24:04,200 --> 00:24:13,920
 This is how repulsiveness should be reviewed as to fruit.

349
00:24:13,920 --> 00:24:15,200
 Can you hear me?

350
00:24:15,200 --> 00:24:16,920
 Yes, we can.

351
00:24:16,920 --> 00:24:17,920
 Thank you, Kati.

352
00:24:17,920 --> 00:24:20,520
 Okay, I will try.

353
00:24:20,520 --> 00:24:22,720
 How as to outflow.

354
00:24:22,720 --> 00:24:27,080
 On being swallowed, it enters by one door after which it

355
00:24:27,080 --> 00:24:29,440
 flows out by several doors in

356
00:24:29,440 --> 00:24:34,560
 the way beginning, eye dirt from the eye, ear dirt from the

357
00:24:34,560 --> 00:24:37,200
 ear, and on being swallowed,

358
00:24:37,200 --> 00:24:41,680
 it is swallowed even in the company of large gatherings.

359
00:24:41,680 --> 00:24:47,140
 But on flowing out, now converted into excrement, urine,

360
00:24:47,140 --> 00:24:51,280
 etc., it is excreted only in solitude.

361
00:24:51,280 --> 00:24:55,630
 On the first day one is delighted to eat it, elated and

362
00:24:55,630 --> 00:24:58,060
 full of happiness and joy.

363
00:24:58,060 --> 00:25:02,910
 On the second day one stops one's nose to avoid it, with a

364
00:25:02,910 --> 00:25:06,140
 wry face disgusted and dismayed.

365
00:25:06,140 --> 00:25:11,760
 On the first day one swallows it lustfully, greedily, gl

366
00:25:11,760 --> 00:25:15,280
uttonlessly, infatuated, but on

367
00:25:15,280 --> 00:25:19,690
 the second day after a single night has passed one excretes

368
00:25:19,690 --> 00:25:23,100
 it with the taste, ashamed, humiliated,

369
00:25:23,100 --> 00:25:28,060
 and disgusted, hence the ancients say, "The food and drink

370
00:25:28,060 --> 00:25:30,520
 so greatly prized, the crisp

371
00:25:30,520 --> 00:25:36,610
 to chew, the soft to suck, go in all by a single door, but

372
00:25:36,610 --> 00:25:44,960
 by nine doors come oozing out."

373
00:25:44,960 --> 00:25:54,700
 Take care, Amber.

374
00:25:54,700 --> 00:26:12,860
 We'll see you next time.

375
00:26:12,860 --> 00:26:16,900
 The food and drinks so greatly prized, the crisp to chew,

376
00:26:16,900 --> 00:26:18,900
 the soft to suck, men like to

377
00:26:18,900 --> 00:26:23,400
 eat in company, but to screit in secrecy.

378
00:26:23,400 --> 00:26:27,680
 The food and drinks so greatly prized, the crisp to chew,

379
00:26:27,680 --> 00:26:30,400
 the soft to suck, this a man

380
00:26:30,400 --> 00:26:35,320
 eats with heightened light, and then screits with dumb

381
00:26:35,320 --> 00:26:36,460
 disgust.

382
00:26:36,460 --> 00:26:40,930
 The food and drinks so greatly prized, the crisp to chew,

383
00:26:40,930 --> 00:26:43,500
 the soft to suck, a single night

384
00:26:43,500 --> 00:26:49,220
 will be enough to bring them to the pure trinity.

385
00:26:49,220 --> 00:26:54,940
 This is how repulsive nature will be reviewed as to outflow

386
00:26:54,940 --> 00:26:54,980
.

387
00:26:54,980 --> 00:26:57,300
 How as the smearing?

388
00:26:57,300 --> 00:27:00,860
 At the time of using it, he smears his hands, lips, tongue,

389
00:27:00,860 --> 00:27:03,400
 and palate, and they become repulsive

390
00:27:03,400 --> 00:27:05,740
 by being smeared with it.

391
00:27:05,740 --> 00:27:09,340
 And even when washed, they have to be washed again and

392
00:27:09,340 --> 00:27:11,940
 again in order to remove the smell.

393
00:27:11,940 --> 00:27:15,630
 And just as when rice is being boiled, the husks, the red

394
00:27:15,630 --> 00:27:17,660
 powder covering the grain,

395
00:27:17,660 --> 00:27:22,450
 etc., rise up and smear the mouth, rim, and lid of the c

396
00:27:22,450 --> 00:27:24,100
auldron, so too when eaten it

397
00:27:24,100 --> 00:27:27,150
 rises up during its cooking and simmering by the bodily

398
00:27:27,150 --> 00:27:28,780
 fire that pervades the whole

399
00:27:28,780 --> 00:27:30,100
 body.

400
00:27:30,100 --> 00:27:33,950
 It turns into tartar, which smears the teeth, and it turns

401
00:27:33,950 --> 00:27:36,500
 into spittle, phlegm, etc., which

402
00:27:36,500 --> 00:27:41,420
 respectively smear the tongue, palate, etc., and it turns

403
00:27:41,420 --> 00:27:44,180
 into eye dirt, ear dirt, snot,

404
00:27:44,180 --> 00:27:49,920
 urine, excrement, etc., which respectively smear the eyes,

405
00:27:49,920 --> 00:27:53,100
 ears, nose, and other passages.

406
00:27:53,100 --> 00:27:56,460
 And when these doors are smeared by it, they never become

407
00:27:56,460 --> 00:27:58,300
 either clean or pleasing, even

408
00:27:58,300 --> 00:28:00,820
 though washed every day.

409
00:28:00,820 --> 00:28:04,280
 And after one has washed a certain one of these, the hand

410
00:28:04,280 --> 00:28:05,920
 has to be washed again.

411
00:28:05,920 --> 00:28:08,970
 And after one has washed a certain one of these, repuls

412
00:28:08,970 --> 00:28:10,660
iveness does not depart from

413
00:28:10,660 --> 00:28:14,670
 it even after two or three washings with cow dung and clay

414
00:28:14,670 --> 00:28:16,380
 and scented powder.

415
00:28:16,380 --> 00:28:21,730
 This is how repulsiveness should be reviewed as to smearing

416
00:28:21,730 --> 00:28:21,900
.

417
00:28:21,900 --> 00:28:22,900
 That's OK, Fernando.

418
00:28:22,900 --> 00:28:23,900
 I got mixed up too.

419
00:28:23,900 --> 00:28:30,300
 László, could you read 25?

420
00:28:30,300 --> 00:28:34,170
 As he reviews repulsiveness in this way in ten aspects and

421
00:28:34,170 --> 00:28:36,140
 strikes at it with thoughts

422
00:28:36,140 --> 00:28:41,250
 and applies thoughts, physical nutriment becomes evident to

423
00:28:41,250 --> 00:28:43,780
 him in its repulsive aspect.

424
00:28:43,780 --> 00:28:47,990
 He cultivates that sign again and again, develops and

425
00:28:47,990 --> 00:28:50,260
 repeatedly practices it.

426
00:28:50,260 --> 00:28:54,960
 As he does so, the hindrances are suppressed and his mind

427
00:28:54,960 --> 00:28:58,620
 is concentrated in excess concentration,

428
00:28:58,620 --> 00:29:02,840
 but without reaching absorption because of the profundity

429
00:29:02,840 --> 00:29:04,780
 of physical nutriment as a

430
00:29:04,780 --> 00:29:08,700
 state with an individual essence.

431
00:29:08,700 --> 00:29:12,740
 But perception is evident here in the apprehension of the

432
00:29:12,740 --> 00:29:15,340
 repulsive aspect, which is why this

433
00:29:15,340 --> 00:29:22,630
 meditation subject goes by the name of perception of repuls

434
00:29:22,630 --> 00:29:25,780
iveness in nutriment.

435
00:29:25,780 --> 00:29:29,000
 When Abhikkhu devotes himself to this perception of repuls

436
00:29:29,000 --> 00:29:32,460
iveness and nutriment, his mind retreats,

437
00:29:32,460 --> 00:29:37,020
 retracks and recoils from craving for flavors.

438
00:29:37,020 --> 00:29:40,080
 He nourishes himself with nutriment without vanity and only

439
00:29:40,080 --> 00:29:41,540
 for the purpose of crossing

440
00:29:41,540 --> 00:29:45,290
 over suffering as one who seeks to cross over the desert

441
00:29:45,290 --> 00:29:47,740
 eats his own dead child's flesh.

442
00:29:47,740 --> 00:29:51,500
 Then his greed for the five cords of sense desire comes to

443
00:29:51,500 --> 00:29:54,180
 fully understand without difficulty

444
00:29:54,180 --> 00:29:57,550
 by means of the full understanding of the physical nutrim

445
00:29:57,550 --> 00:29:58,020
ent.

446
00:29:58,020 --> 00:30:01,020
 He fully understands the materiality aggregate by means of

447
00:30:01,020 --> 00:30:02,580
 the full understanding of the

448
00:30:02,580 --> 00:30:05,820
 five cords of sense desires.

449
00:30:05,820 --> 00:30:08,550
 Development of mindfulness occupied with the body comes to

450
00:30:08,550 --> 00:30:09,960
 perfection in him through the

451
00:30:09,960 --> 00:30:13,580
 repulsiveness of what is uncooked and the rest.

452
00:30:13,580 --> 00:30:16,840
 He has entered upon a way that is in conformity with the

453
00:30:16,840 --> 00:30:19,200
 perception of foulness and by keeping

454
00:30:19,200 --> 00:30:22,300
 to this way, even if he does not experience the deathless

455
00:30:22,300 --> 00:30:23,820
 goal in this life, he is at

456
00:30:23,820 --> 00:30:26,660
 least bound for a happy destiny.

457
00:30:26,660 --> 00:30:29,160
 This is the detailed explanation of the development of the

458
00:30:29,160 --> 00:30:31,260
 perception of repulsiveness in nutriment.

459
00:30:31,260 --> 00:30:36,320
 I'm going to have to ask that we quit there because I have

460
00:30:36,320 --> 00:30:38,740
 something I have to do and

461
00:30:38,740 --> 00:30:44,750
 I've been very distracted today by various things so I have

462
00:30:44,750 --> 00:30:47,300
 to go and edit and submit

463
00:30:47,300 --> 00:30:48,780
 an essay.

464
00:30:48,780 --> 00:30:51,940
 Okay, thank you Bante.

465
00:30:51,940 --> 00:30:53,940
 Sadhu Bante.

466
00:30:53,940 --> 00:30:54,940
 Sadhu.

467
00:30:54,940 --> 00:30:55,940
 Sadhu.

468
00:30:55,940 --> 00:30:56,940
 Sadhu.

469
00:30:56,940 --> 00:30:57,940
 Sadhu.

470
00:30:57,940 --> 00:30:58,940
 Sadhu Bante.

471
00:30:58,940 --> 00:30:59,940
 Sadhu Bante.

472
00:30:59,940 --> 00:31:00,940
 Sadhu Bante.

473
00:31:00,940 --> 00:31:01,940
 Sadhu Bante.

474
00:31:01,940 --> 00:31:07,280
 No, Marylinne, don't worry, I got mixed up and I asked Bern

475
00:31:07,280 --> 00:31:07,560
ando to read.

476
00:31:07,560 --> 00:31:11,180
 It was my mistake so please don't apologize.

477
00:31:11,180 --> 00:31:12,860
 Sometimes we sound alike, you know.

