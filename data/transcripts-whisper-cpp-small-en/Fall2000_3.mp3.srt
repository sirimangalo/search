1
00:00:00,000 --> 00:00:10,170
 I thought I have finished with the passage, but still there

2
00:00:10,170 --> 00:00:16,000
 is something I want to talk about.

3
00:00:16,000 --> 00:00:22,540
 So we will go back to the phrase, "removing covetousness

4
00:00:22,540 --> 00:00:26,000
 and grief in the world."

5
00:00:28,000 --> 00:00:34,370
 We understand from the two phrases, "adden clearly

6
00:00:34,370 --> 00:00:39,000
 comprehending and mindful"

7
00:00:39,000 --> 00:00:44,000
 and "removing covetousness and grief in the world,"

8
00:00:44,000 --> 00:00:49,000
 that we must make effort and we must try to be mindful.

9
00:00:49,000 --> 00:00:56,130
 And when our mindfulness becomes strong, we will see the

10
00:00:56,130 --> 00:00:58,000
 objects clearly,

11
00:00:58,000 --> 00:01:03,000
 we will see the true nature of things, and at the same time

12
00:01:03,000 --> 00:01:03,000
,

13
00:01:03,000 --> 00:01:10,680
 we remove the covetousness and grief, or we remove the

14
00:01:10,680 --> 00:01:13,000
 mental defilements.

15
00:01:13,000 --> 00:01:19,820
 When we understand this much, we are happy and we are

16
00:01:19,820 --> 00:01:23,000
 pleased with ourselves.

17
00:01:23,000 --> 00:01:29,580
 But ancient teachers went deeper than just this much

18
00:01:29,580 --> 00:01:32,000
 understanding.

19
00:01:32,000 --> 00:01:43,440
 The commandary pointed out to us that the phrase, "removing

20
00:01:43,440 --> 00:01:45,000
 covetousness and grief in the world,"

21
00:01:45,000 --> 00:01:57,390
 shows more than, what do we call it, more than it appears

22
00:01:57,390 --> 00:01:59,000
 to us.

23
00:01:59,000 --> 00:02:04,580
 The commandary said that by this phrase, Buddha pointed out

24
00:02:04,580 --> 00:02:05,000
 to us,

25
00:02:05,000 --> 00:02:16,000
 "the power of a yogi, the ability of a yogi."

26
00:02:16,000 --> 00:02:25,620
 There are two mental states that are removed, they are cove

27
00:02:25,620 --> 00:02:28,000
tousness and grief.

28
00:02:28,000 --> 00:02:33,000
 So there are two mental states that are removed,

29
00:02:33,000 --> 00:02:42,400
 and by the removing of covetousness, or let us just call it

30
00:02:42,400 --> 00:02:44,000
 attachment,

31
00:02:44,000 --> 00:02:51,240
 by the removing of attachment, Buddha meant the removing of

32
00:02:51,240 --> 00:02:53,000
 satisfaction

33
00:02:53,000 --> 00:02:58,000
 rooted in bodily gratification.

34
00:02:58,000 --> 00:03:04,140
 That means when our body is in good shape, when we are

35
00:03:04,140 --> 00:03:05,000
 healthy,

36
00:03:05,000 --> 00:03:11,000
 when we are strong, and when the body is beautiful,

37
00:03:11,000 --> 00:03:14,000
 then we are satisfied with the body.

38
00:03:14,000 --> 00:03:18,000
 That is, if we do not practice mindfulness,

39
00:03:18,000 --> 00:03:25,500
 if we practice mindfulness, we will be able to remove that

40
00:03:25,500 --> 00:03:27,000
 satisfaction

41
00:03:27,000 --> 00:03:31,000
 rooted in the bodily gratification.

42
00:03:31,000 --> 00:03:39,220
 And by removing of grief, Buddha meant the removing of

43
00:03:39,220 --> 00:03:41,000
 dissatisfaction

44
00:03:41,000 --> 00:03:44,000
 rooted in bodily misfortune.

45
00:03:44,000 --> 00:03:49,590
 That means when our body is not in good shape, when we are

46
00:03:49,590 --> 00:03:50,000
 sick,

47
00:03:50,000 --> 00:03:54,480
 when we have pain, and when we are ugly, we are

48
00:03:54,480 --> 00:03:57,000
 dissatisfied with our body.

49
00:03:57,000 --> 00:04:00,000
 But when we practice mindfulness meditation,

50
00:04:00,000 --> 00:04:09,170
 we are able to remove or avoid dissatisfaction rooted in

51
00:04:09,170 --> 00:04:12,000
 bodily misfortune.

52
00:04:12,000 --> 00:04:18,540
 So by the phrase "removing covetousness and grief", Buddha

53
00:04:18,540 --> 00:04:19,000
 meant

54
00:04:19,000 --> 00:04:22,010
 the removing of satisfaction rooted in the bodily

55
00:04:22,010 --> 00:04:23,000
 gratification

56
00:04:23,000 --> 00:04:37,180
 and removing of dissatisfaction rooted in bodily misfortune

57
00:04:37,180 --> 00:04:38,000
.

58
00:04:38,000 --> 00:04:45,610
 Again, by removing of covetousness or attachment, Buddha

59
00:04:45,610 --> 00:04:46,000
 meant

60
00:04:46,000 --> 00:04:50,000
 the removing of delight in the body.

61
00:04:50,000 --> 00:04:54,000
 Now people take delight in the body,

62
00:04:54,000 --> 00:04:59,770
 and taking delight in the body is more pronounced now than

63
00:04:59,770 --> 00:05:02,000
 ever before.

64
00:05:02,000 --> 00:05:07,000
 There are many fitness centers in this country,

65
00:05:07,000 --> 00:05:12,000
 and people are going to these centers and taking exercises,

66
00:05:12,000 --> 00:05:14,000
 paying a lot of money.

67
00:05:14,000 --> 00:05:19,000
 And they go to these centers not just to be healthy.

68
00:05:19,000 --> 00:05:23,000
 They go there to have a beautiful body,

69
00:05:23,000 --> 00:05:27,930
 and then they are taught to love their body, to be pleased

70
00:05:27,930 --> 00:05:29,000
 with their body.

71
00:05:29,000 --> 00:05:32,000
 So there is the delight in the body,

72
00:05:32,000 --> 00:05:36,000
 and delight in the body means attachment to the body.

73
00:05:36,000 --> 00:05:41,000
 But when a person practices mindfulness,

74
00:05:41,000 --> 00:05:45,440
 or we pass on our meditation, and when he is able to remove

75
00:05:45,440 --> 00:05:46,000
 attachment,

76
00:05:46,000 --> 00:05:56,000
 that means he is removing that delight in the body.

77
00:05:56,000 --> 00:06:03,490
 Will such people want to practice contemplation of the body

78
00:06:03,490 --> 00:06:05,000
 meditation?

79
00:06:05,000 --> 00:06:09,350
 Contemplation of the body means contemplating on the

80
00:06:09,350 --> 00:06:11,000
 different parts of the body,

81
00:06:11,000 --> 00:06:15,000
 trying to see the body as impermanent and so on,

82
00:06:15,000 --> 00:06:19,150
 and also trying to see different parts of the body as rep

83
00:06:19,150 --> 00:06:20,000
ulsive.

84
00:06:20,000 --> 00:06:26,580
 So such people will not want to practice contemplation on

85
00:06:26,580 --> 00:06:28,000
 the body.

86
00:06:28,000 --> 00:06:32,000
 But when a person practices vipassana meditation

87
00:06:32,000 --> 00:06:38,000
 and is able to remove grief or aversion,

88
00:06:38,000 --> 00:06:47,740
 that means he is able to remove resistance to contemplation

89
00:06:47,740 --> 00:06:50,000
 of the body.

90
00:06:50,000 --> 00:06:57,030
 Such a person will not refuse to practice contemplation of

91
00:06:57,030 --> 00:06:58,000
 the body.

92
00:06:58,000 --> 00:07:05,740
 That is because he is able to remove or he is able to avoid

93
00:07:05,740 --> 00:07:07,000
 aversion

94
00:07:07,000 --> 00:07:11,000
 regarding the body.

95
00:07:11,000 --> 00:07:25,420
 And also this phrase shows that a yogi is able to avoid

96
00:07:25,420 --> 00:07:29,000
 putting something unreal

97
00:07:29,000 --> 00:07:35,680
 onto the object and taking away something real from the

98
00:07:35,680 --> 00:07:37,000
 object.

99
00:07:37,000 --> 00:07:45,000
 Now when a yogi is able to avoid attachment,

100
00:07:45,000 --> 00:07:57,000
 that means he does not add anything unreal to the object.

101
00:07:57,000 --> 00:08:02,450
 Now Buddha taught that all conditioned things are imper

102
00:08:02,450 --> 00:08:03,000
manent,

103
00:08:03,000 --> 00:08:11,000
 suffering, non-soul or insubstantial and unlovely.

104
00:08:11,000 --> 00:08:17,760
 But those who do not practice meditation think that they

105
00:08:17,760 --> 00:08:20,000
 are permanent,

106
00:08:20,000 --> 00:08:28,000
 good, soul or everlasting and lovely.

107
00:08:28,000 --> 00:08:32,000
 So if you think something to be lovely,

108
00:08:32,000 --> 00:08:38,000
 that means you are adding something which is unreal,

109
00:08:38,000 --> 00:08:44,000
 according to the teaching of the Buddha, onto the object.

110
00:08:44,000 --> 00:08:49,000
 When you are able to avoid attachment to an object,

111
00:08:49,000 --> 00:08:57,970
 then you are able to avoid adding something unreal onto the

112
00:08:57,970 --> 00:08:59,000
 object.

113
00:08:59,000 --> 00:09:09,000
 So that is one kind of the power of a yogi.

114
00:09:09,000 --> 00:09:20,000
 When a person sees an unlovely object, he does not like it.

115
00:09:20,000 --> 00:09:25,000
 He has aversion to that object.

116
00:09:25,000 --> 00:09:29,000
 That means he does not like it to be unlovely.

117
00:09:29,000 --> 00:09:32,000
 He wanted it to be lovely.

118
00:09:32,000 --> 00:09:38,000
 So that means he wants to take away something that is real,

119
00:09:38,000 --> 00:09:42,000
 that is, unloveliness, from the object.

120
00:09:42,000 --> 00:09:47,000
 So by being able to remove grief,

121
00:09:47,000 --> 00:10:00,210
 a yogi is able to avoid taking something real away from the

122
00:10:00,210 --> 00:10:03,000
 object.

123
00:10:03,000 --> 00:10:08,950
 These three are described in the commentary as the power of

124
00:10:08,950 --> 00:10:10,000
 a yogi.

125
00:10:10,000 --> 00:10:17,160
 He is free from satisfaction and dissatisfaction regarding

126
00:10:17,160 --> 00:10:18,000
 the body,

127
00:10:18,000 --> 00:10:27,150
 and he is able to stand firm against delight and non-del

128
00:10:27,150 --> 00:10:28,000
ight.

129
00:10:28,000 --> 00:10:34,940
 And he is free from putting in something unreal and taking

130
00:10:34,940 --> 00:10:37,000
 away something real.

131
00:10:37,000 --> 00:10:42,000
 So these three are called the power of a yogi.

132
00:10:42,000 --> 00:10:46,000
 And a yogi who possesses these three kinds of powers

133
00:10:46,000 --> 00:10:56,000
 is called a yogi who is accomplished in meditation.

134
00:10:56,000 --> 00:10:59,000
 When we practice meditation, it is very important

135
00:10:59,000 --> 00:11:05,000
 that we do not add anything onto the object,

136
00:11:05,000 --> 00:11:10,000
 and also we do not take away anything from the object.

137
00:11:10,000 --> 00:11:16,000
 Now if we think that a certain thing is permanent,

138
00:11:16,000 --> 00:11:26,010
 then we are putting or adding permanence which is unreal

139
00:11:26,010 --> 00:11:29,000
 onto the object which is impermanent.

140
00:11:29,000 --> 00:11:35,000
 And when we want an impermanent to be permanent,

141
00:11:35,000 --> 00:11:39,630
 that means we want to take away the impermanency which is

142
00:11:39,630 --> 00:11:42,000
 real from the object.

143
00:11:42,000 --> 00:11:49,000
 And if we add something and if we take away something

144
00:11:49,000 --> 00:11:50,000
 unreal

145
00:11:50,000 --> 00:11:54,000
 and if we take something real from the object,

146
00:11:54,000 --> 00:11:57,000
 then we will not see the object as it is.

147
00:11:57,000 --> 00:12:04,000
 And so we will be carried away by liking or disliking

148
00:12:04,000 --> 00:12:07,000
 satisfaction or dissatisfaction,

149
00:12:07,000 --> 00:12:11,000
 the light or non-delight regarding the objects.

150
00:12:11,000 --> 00:12:19,590
 So Vipassana gives us the power to see things as they

151
00:12:19,590 --> 00:12:21,000
 really are,

152
00:12:21,000 --> 00:12:25,780
 to see things without any additions, to see things without

153
00:12:25,780 --> 00:12:27,000
 any omissions.

154
00:12:27,000 --> 00:12:32,000
 We just take the objects as they are.

155
00:12:32,000 --> 00:12:36,000
 And it is very important to see things as they are,

156
00:12:36,000 --> 00:12:42,500
 so that we can avoid both attachment and aversion to the

157
00:12:42,500 --> 00:12:44,000
 objects.

158
00:12:44,000 --> 00:12:49,460
 And these powers mentioned in the commentary can be

159
00:12:49,460 --> 00:12:56,000
 obtained only by those who practice Vipassana meditation,

160
00:12:56,000 --> 00:13:02,000
 only by those who make effort and who practice mindfulness

161
00:13:02,000 --> 00:13:07,000
 and who comprehends the object clearly.

162
00:13:07,000 --> 00:13:13,900
 So at the same time when a yogi comprehends the object

163
00:13:13,900 --> 00:13:15,000
 clearly,

164
00:13:15,000 --> 00:13:21,360
 he is able to take the object without any additions or o

165
00:13:21,360 --> 00:13:23,000
missions.

166
00:13:23,000 --> 00:13:28,000
 So it is only a yogi, it is only a Vipassana yogi

167
00:13:28,000 --> 00:13:33,880
 that can take the object as it is without additions or o

168
00:13:33,880 --> 00:13:35,000
missions.

169
00:13:35,000 --> 00:13:38,000
 And so it is called a power of the yogi,

170
00:13:38,000 --> 00:13:44,460
 and a yogi who possesses this power is actually an

171
00:13:44,460 --> 00:13:48,000
 accomplished meditator.

172
00:13:48,000 --> 00:14:01,000
 Now we will go to the practice.

173
00:14:01,000 --> 00:14:12,000
 So there are some preliminary qualities to be fulfilled

174
00:14:12,000 --> 00:14:19,000
 before a person practices meditation.

175
00:14:19,000 --> 00:14:27,400
 And once a monk went to the Buddha and asked the Buddha to

176
00:14:27,400 --> 00:14:30,000
 teach him in brief

177
00:14:30,000 --> 00:14:35,270
 so that after listening to the instructions given by the

178
00:14:35,270 --> 00:14:36,000
 Buddha,

179
00:14:36,000 --> 00:14:42,000
 he could go to a secluded place and practice meditation.

180
00:14:42,000 --> 00:14:47,250
 To him the Buddha said, "You should first purify the

181
00:14:47,250 --> 00:14:54,000
 beginning of all wholesome things.

182
00:14:54,000 --> 00:14:59,250
 You should first purify the beginning of all wholesome

183
00:14:59,250 --> 00:15:01,000
 things.

184
00:15:01,000 --> 00:15:06,000
 And what is the beginning of all wholesome things?

185
00:15:06,000 --> 00:15:16,000
 Sila, that is well purified, or Tila, that is very pure,

186
00:15:16,000 --> 00:15:24,270
 and the view that is straight, that means a view that is

187
00:15:24,270 --> 00:15:26,000
 correct.

188
00:15:26,000 --> 00:15:33,260
 When your Sila is very pure and you are understanding

189
00:15:33,260 --> 00:15:34,000
 correct,

190
00:15:34,000 --> 00:15:40,000
 then supported and aided by Sila,

191
00:15:40,000 --> 00:15:46,000
 you may cultivate the four foundations of mindfulness."

192
00:15:46,000 --> 00:15:54,990
 So in this discourse, Buddha taught to that monk two things

193
00:15:54,990 --> 00:15:57,000
 that he should practice,

194
00:15:57,000 --> 00:16:02,500
 he should accomplish before he practiced the foundations of

195
00:16:02,500 --> 00:16:04,000
 mindfulness.

196
00:16:04,000 --> 00:16:12,000
 The first is very pure Sila, the purity of moral conduct.

197
00:16:12,000 --> 00:16:19,000
 And the second is the correct understanding.

198
00:16:19,000 --> 00:16:29,480
 Although this advice is given to a monk, lay people also

199
00:16:29,480 --> 00:16:32,000
 should follow this advice.

200
00:16:32,000 --> 00:16:38,790
 So lay people also who want to practice meditation must

201
00:16:38,790 --> 00:16:41,000
 fulfill these two things,

202
00:16:41,000 --> 00:16:46,000
 the purity of moral conduct and then correct understanding.

203
00:16:46,000 --> 00:16:50,000
 Now the purity of moral conduct is important.

204
00:16:50,000 --> 00:16:56,090
 It is actually a basis for practice of meditation or

205
00:16:56,090 --> 00:16:58,000
 concentration,

206
00:16:58,000 --> 00:17:04,540
 because when the moral conduct is not pure, concentration

207
00:17:04,540 --> 00:17:06,000
 cannot be gained.

208
00:17:06,000 --> 00:17:11,000
 Because when moral conduct is not pure,

209
00:17:11,000 --> 00:17:22,340
 the feeling of guilt or feeling of remorse can interfere

210
00:17:22,340 --> 00:17:27,000
 with the practice.

211
00:17:27,000 --> 00:17:35,210
 Say I am now practicing meditation and that my Sila is not

212
00:17:35,210 --> 00:17:36,000
 pure,

213
00:17:36,000 --> 00:17:39,180
 although other people may think that I am pure in my Sila,

214
00:17:39,180 --> 00:17:42,000
 I know that I am not pure and so on.

215
00:17:42,000 --> 00:17:46,000
 So he criticizes himself, he blames himself,

216
00:17:46,000 --> 00:17:50,070
 and so he is not able to concentrate on the meditation

217
00:17:50,070 --> 00:17:51,000
 object.

218
00:17:51,000 --> 00:17:58,710
 So if one's moral conduct is not pure, one cannot hope to

219
00:17:58,710 --> 00:18:03,000
 attain Samadhi or concentration.

220
00:18:03,000 --> 00:18:13,320
 So that is why the purity of moral conduct or pure Sila is

221
00:18:13,320 --> 00:18:17,000
 very important.

222
00:18:17,000 --> 00:18:27,330
 And for lay people to purify their moral conduct is not so

223
00:18:27,330 --> 00:18:30,000
 difficult.

224
00:18:30,000 --> 00:18:35,330
 They can take the precepts and keep them, and their moral

225
00:18:35,330 --> 00:18:37,000
 conduct is pure.

226
00:18:37,000 --> 00:18:43,070
 And when we look at the stories during the time of the

227
00:18:43,070 --> 00:18:48,000
 Buddha, we see that

228
00:18:48,000 --> 00:18:54,350
 the purity of moral conduct can be achieved just at the

229
00:18:54,350 --> 00:18:57,000
 beginning of the practice of meditation.

230
00:18:57,000 --> 00:19:02,010
 Now that is why yogis are made to take precepts at the

231
00:19:02,010 --> 00:19:05,000
 beginning of the practice.

232
00:19:05,000 --> 00:19:12,590
 So that is to fulfill this one requirement for the practice

233
00:19:12,590 --> 00:19:15,000
 of meditation.

234
00:19:15,000 --> 00:19:20,960
 Minimum moral requirement for lay people is keeping five

235
00:19:20,960 --> 00:19:22,000
 precepts.

236
00:19:22,000 --> 00:19:28,000
 And these five precepts are called universal precepts.

237
00:19:28,000 --> 00:19:36,990
 These precepts can be found or can be taken even when there

238
00:19:36,990 --> 00:19:38,000
 are no Buddhas

239
00:19:38,000 --> 00:19:42,000
 or there are no teachings of the Buddha available.

240
00:19:42,000 --> 00:19:47,000
 So even outside the dispensation of the Buddha,

241
00:19:47,000 --> 00:19:51,560
 it is said in our books that these five precepts were kept

242
00:19:51,560 --> 00:19:53,000
 by lay people.

243
00:19:53,000 --> 00:19:58,990
 So these five precepts are the minimum moral requirement

244
00:19:58,990 --> 00:20:00,000
 for lay people.

245
00:20:00,000 --> 00:20:03,000
 And you all know the five precepts.

246
00:20:03,000 --> 00:20:07,260
 Abstention from killing, abstention from stealing, abst

247
00:20:07,260 --> 00:20:09,000
ention from sexual misconduct,

248
00:20:09,000 --> 00:20:13,860
 abstention from telling lies, and abstention from taking

249
00:20:13,860 --> 00:20:15,000
 intoxicants.

250
00:20:15,000 --> 00:20:21,940
 So if you can abstain from these five, then your moral

251
00:20:21,940 --> 00:20:24,000
 conduct is pure.

252
00:20:24,000 --> 00:20:29,930
 But at this retreat, or at our retreats, yogis take eight

253
00:20:29,930 --> 00:20:31,000
 precepts.

254
00:20:31,000 --> 00:20:36,320
 Now taking eight precepts and keeping them is better than

255
00:20:36,320 --> 00:20:39,000
 taking five precepts.

256
00:20:39,000 --> 00:20:48,780
 So taking eight precepts, yogis can avoid some unwholesome

257
00:20:48,780 --> 00:20:50,000
 states

258
00:20:50,000 --> 00:20:55,000
 and also they get more time to devote to the practice.

259
00:20:55,000 --> 00:21:00,390
 So at our retreats, eight precepts is a standard practice

260
00:21:00,390 --> 00:21:02,000
 for all yogis.

261
00:21:02,000 --> 00:21:08,700
 So after taking the precepts, you can rest assured that

262
00:21:08,700 --> 00:21:11,000
 your moral conduct is pure

263
00:21:11,000 --> 00:21:16,600
 and so you have a firm basis for the practice of meditation

264
00:21:16,600 --> 00:21:17,000
.

265
00:21:17,000 --> 00:21:24,000
 There is a question regarding the purity of moral conduct.

266
00:21:24,000 --> 00:21:31,270
 How long must a person be pure in moral conduct before he

267
00:21:31,270 --> 00:21:35,000
 can practice meditation?

268
00:21:35,000 --> 00:21:41,840
 Now many people think that moral conduct should be pure all

269
00:21:41,840 --> 00:21:44,000
 the life,

270
00:21:44,000 --> 00:21:49,600
 or moral conduct should be pure for a long time before they

271
00:21:49,600 --> 00:21:53,000
 can practice meditation.

272
00:21:53,000 --> 00:21:56,150
 And so there are many people who would say, "First you pur

273
00:21:56,150 --> 00:21:58,000
ify your moral conduct.

274
00:21:58,000 --> 00:22:02,000
 Do not practice meditation yet."

275
00:22:02,000 --> 00:22:11,200
 And they say that you have to purify your moral conduct for

276
00:22:11,200 --> 00:22:13,000
 a long time.

277
00:22:13,000 --> 00:22:21,000
 But that is not correct because when we see the stories

278
00:22:21,000 --> 00:22:29,000
 that occurred during the time of the Buddha, we know that

279
00:22:29,000 --> 00:22:38,000
 those who are not pure in their moral conduct,

280
00:22:38,000 --> 00:22:43,390
 after they met the Buddha and listened to the Dhamma and

281
00:22:43,390 --> 00:22:46,000
 got enlightenment.

282
00:22:46,000 --> 00:23:02,890
 So from these stories, we conclude that it is not essential

283
00:23:02,890 --> 00:23:04,000
 for a person

284
00:23:04,000 --> 00:23:09,950
 to be pure in moral conduct for a long time before he

285
00:23:09,950 --> 00:23:12,000
 practices meditation.

286
00:23:12,000 --> 00:23:17,000
 Although if his moral conduct can be pure for a long time,

287
00:23:17,000 --> 00:23:20,000
 it's very good.

288
00:23:20,000 --> 00:23:26,760
 But sometimes people are afraid that, "Oh, my moral conduct

289
00:23:26,760 --> 00:23:28,000
 is not pure

290
00:23:28,000 --> 00:23:31,410
 and so I cannot practice meditation and so they do not

291
00:23:31,410 --> 00:23:32,000
 practice meditation

292
00:23:32,000 --> 00:23:37,000
 and they are deprived of this opportunity."

293
00:23:37,000 --> 00:23:46,000
 So for laypeople to be pure in moral conduct, it just be a

294
00:23:46,000 --> 00:23:47,000
 few moments

295
00:23:47,000 --> 00:23:55,000
 or a few hours or a few days.

296
00:23:55,000 --> 00:24:10,760
 There are many stories to show that even those who are not

297
00:24:10,760 --> 00:24:12,000
 pure in moral conduct

298
00:24:12,000 --> 00:24:17,000
 for a long time can gain enlightenment.

299
00:24:17,000 --> 00:24:21,000
 I will give you only two stories.

300
00:24:21,000 --> 00:24:28,290
 During the time of the Buddha, there was a man who was

301
00:24:28,290 --> 00:24:30,000
 fishing.

302
00:24:30,000 --> 00:24:35,360
 On that morning when Buddha surveyed the world with his

303
00:24:35,360 --> 00:24:37,000
 Buddha eye,

304
00:24:37,000 --> 00:24:42,000
 he saw this person in his mind.

305
00:24:42,000 --> 00:24:47,380
 So in the morning when that person was fishing, Buddha went

306
00:24:47,380 --> 00:24:48,000
 that way

307
00:24:48,000 --> 00:24:53,000
 on his arms round along with his disciples.

308
00:24:53,000 --> 00:25:00,960
 So when Buddha got near the man, the man put down the

309
00:25:00,960 --> 00:25:03,000
 fishing rod

310
00:25:03,000 --> 00:25:06,000
 and then paid respects to the Buddha.

311
00:25:06,000 --> 00:25:10,000
 Then the Buddha asked him, "What is your name?"

312
00:25:10,000 --> 00:25:14,000
 Then he said, "My name is Mr. Pure."

313
00:25:14,000 --> 00:25:21,620
 Then Buddha said, "Those who kill beings, who inflict

314
00:25:21,620 --> 00:25:23,000
 suffering on beings

315
00:25:23,000 --> 00:25:27,000
 are not called pure, Mr. Pure.

316
00:25:27,000 --> 00:25:34,390
 Only those who do not inflict suffering on beings are

317
00:25:34,390 --> 00:25:36,000
 called Mr. Pure."

318
00:25:36,000 --> 00:25:42,000
 So after hearing this verse uttered by the Buddha,

319
00:25:42,000 --> 00:25:46,000
 it is said in the books that he became a Sauta Panna.

320
00:25:46,000 --> 00:25:52,000
 Now he was fishing, so his moral conduct was not pure

321
00:25:52,000 --> 00:25:55,000
 until he met the Buddha.

322
00:25:55,000 --> 00:26:01,000
 So after he heard the verse uttered by the Buddha,

323
00:26:01,000 --> 00:26:06,000
 he must have made a resolution in his mind.

324
00:26:06,000 --> 00:26:08,000
 I will no longer do this.

325
00:26:08,000 --> 00:26:13,000
 So when he has made this resolution in his mind,

326
00:26:13,000 --> 00:26:17,000
 he has purified his moral conduct.

327
00:26:17,000 --> 00:26:21,000
 So he listened to the teaching of the Buddha

328
00:26:21,000 --> 00:26:27,320
 and actually he went through the different stages of Vipass

329
00:26:27,320 --> 00:26:28,000
ana

330
00:26:28,000 --> 00:26:31,000
 while he was listening to the teaching.

331
00:26:31,000 --> 00:26:35,000
 At the end of the teaching, he reached Sauta Panna.

332
00:26:35,000 --> 00:26:37,000
 He became a Sauta Panna.

333
00:26:37,000 --> 00:26:39,000
 So that is one story.

334
00:26:39,000 --> 00:26:43,000
 So in this story, now you know that until he met the Buddha

335
00:26:43,000 --> 00:26:43,000
,

336
00:26:43,000 --> 00:26:49,000
 his moral conduct was not pure.

337
00:26:49,000 --> 00:26:54,000
 The other story is about two pickpockets.

338
00:26:54,000 --> 00:26:57,000
 Again, during the time of the Buddha,

339
00:26:57,000 --> 00:27:01,000
 there were two friends who were pickpockets.

340
00:27:01,000 --> 00:27:08,090
 So one day they went to where the Buddha was preaching to a

341
00:27:08,090 --> 00:27:10,000
 multitude of people.

342
00:27:10,000 --> 00:27:14,000
 So with the intention of picking a pocket or two,

343
00:27:14,000 --> 00:27:17,000
 they entered into the audience.

344
00:27:17,000 --> 00:27:23,250
 And one of them was able to snatch some money from a

345
00:27:23,250 --> 00:27:25,000
 listener.

346
00:27:25,000 --> 00:27:30,870
 But the other pickpocket got interested in the teachings of

347
00:27:30,870 --> 00:27:32,000
 the Buddha.

348
00:27:32,000 --> 00:27:37,000
 And so he did not snatch anything from anybody,

349
00:27:37,000 --> 00:27:41,000
 but he listened to the Dharma, he listened to the preaching

350
00:27:41,000 --> 00:27:41,000
.

351
00:27:41,000 --> 00:27:47,670
 And at the end of that preaching, again he became a Sauta P

352
00:27:47,670 --> 00:27:49,000
anna.

353
00:27:49,000 --> 00:27:55,000
 In this story also, the one who became a Sauta Panna,

354
00:27:55,000 --> 00:28:01,280
 his moral conduct was not pure until he got interested in

355
00:28:01,280 --> 00:28:05,000
 the teachings of the Buddha.

356
00:28:05,000 --> 00:28:09,000
 So he got interested in the teachings of the Buddha,

357
00:28:09,000 --> 00:28:15,370
 and he must have made a resolution at that moment that I

358
00:28:15,370 --> 00:28:18,000
 will no longer do this pickpocketing.

359
00:28:18,000 --> 00:28:25,530
 And so he purified his Sila while listening to the teaching

360
00:28:25,530 --> 00:28:27,000
 of the Buddha.

361
00:28:27,000 --> 00:28:30,000
 And then he may practice meditation while sitting.

362
00:28:30,000 --> 00:28:37,000
 And so at the end of the teaching, he became a Sauta Panna.

363
00:28:37,000 --> 00:28:50,020
 So for lay people, it is easy to purify Sila or to achieve

364
00:28:50,020 --> 00:28:55,000
 the purity of Sila.

365
00:28:55,000 --> 00:29:00,650
 It is good. It would be best to be pure in Sila for a long

366
00:29:00,650 --> 00:29:02,000
 time,

367
00:29:02,000 --> 00:29:06,000
 to be pure in Sila for the whole of your life.

368
00:29:06,000 --> 00:29:12,000
 So I am not encouraging you to be fishermen and pickpockets

369
00:29:12,000 --> 00:29:12,000
,

370
00:29:12,000 --> 00:29:19,000
 and then come to the center and practice meditation.

371
00:29:19,000 --> 00:29:25,080
 But I want to let people know that even if their moral

372
00:29:25,080 --> 00:29:29,000
 conduct was not pure in the past,

373
00:29:29,000 --> 00:29:32,000
 still they can practice meditation,

374
00:29:32,000 --> 00:29:37,650
 because they can purify their Sila right before they sit

375
00:29:37,650 --> 00:29:40,000
 down and practice meditation.

376
00:29:40,000 --> 00:29:46,990
 So it is good to keep moral conduct pure as long as you can

377
00:29:46,990 --> 00:29:47,000
,

378
00:29:47,000 --> 00:29:52,000
 and it is an ideal to be pure in moral conduct.

379
00:29:52,000 --> 00:29:56,000
 But even if you are not pure in moral conduct before,

380
00:29:56,000 --> 00:30:02,720
 still you can practice meditation because you can purify

381
00:30:02,720 --> 00:30:04,000
 your moral conduct

382
00:30:04,000 --> 00:30:08,000
 right at the beginning of the practice.

383
00:30:08,000 --> 00:30:14,750
 Purity of moral conduct is emphasized in the teachings of

384
00:30:14,750 --> 00:30:16,000
 the Buddha,

385
00:30:16,000 --> 00:30:23,000
 because purity of moral conduct leads to non-remorse.

386
00:30:23,000 --> 00:30:27,280
 Now if your moral conduct is not pure, you have remorseful

387
00:30:27,280 --> 00:30:28,000
 feelings.

388
00:30:28,000 --> 00:30:31,990
 So if your moral conduct is pure, you are free from remorse

389
00:30:31,990 --> 00:30:32,000
.

390
00:30:32,000 --> 00:30:38,480
 And that freedom from remorse leads to joy, and joy leads

391
00:30:38,480 --> 00:30:40,000
 to happiness,

392
00:30:40,000 --> 00:30:43,760
 happiness leads to peacefulness, peacefulness leads to

393
00:30:43,760 --> 00:30:45,000
 concentration,

394
00:30:45,000 --> 00:30:49,660
 and concentration leads to seeing things as they really are

395
00:30:49,660 --> 00:30:50,000
.

396
00:30:50,000 --> 00:30:58,000
 And this ultimately leads to attainment of enlightenment.

397
00:30:58,000 --> 00:31:07,540
 So the purity of moral conduct is at the very base of the

398
00:31:07,540 --> 00:31:11,000
 practice taught by the Buddha.

399
00:31:11,000 --> 00:31:19,420
 And so when the monk asked him to teach in brief what he

400
00:31:19,420 --> 00:31:20,000
 must do,

401
00:31:20,000 --> 00:31:26,000
 Buddha said, "You purify your seelah first."

402
00:31:35,000 --> 00:31:41,380
 The second requirement Buddha taught to that monk is the

403
00:31:41,380 --> 00:31:44,000
 view that is straight.

404
00:31:44,000 --> 00:31:48,000
 So that means the correct view.

405
00:31:48,000 --> 00:31:56,390
 And the correct view is explained in the commentary as the

406
00:31:56,390 --> 00:31:58,000
 understanding

407
00:31:58,000 --> 00:32:05,670
 that beings have karma alone as their property, or in other

408
00:32:05,670 --> 00:32:06,000
 words,

409
00:32:06,000 --> 00:32:12,000
 understanding of and belief in the law of karma.

410
00:32:12,000 --> 00:32:24,330
 And the belief in law of karma does not mean blind faith in

411
00:32:24,330 --> 00:32:27,000
 the law of karma.

412
00:32:27,000 --> 00:32:33,000
 It is belief founded upon understanding.

413
00:32:33,000 --> 00:32:38,850
 Now, according to the teachings of the Buddha, there is

414
00:32:38,850 --> 00:32:41,000
 what is called karma.

415
00:32:41,000 --> 00:32:46,000
 And there are the results of karma.

416
00:32:46,000 --> 00:32:52,530
 Now when you do something, a mental state arises in the

417
00:32:52,530 --> 00:32:53,000
 mind,

418
00:32:53,000 --> 00:33:00,180
 a mental state that encourages you to do the deed, that

419
00:33:00,180 --> 00:33:04,000
 urges you to do the deed.

420
00:33:04,000 --> 00:33:08,000
 And that mental state is called volition.

421
00:33:08,000 --> 00:33:18,190
 And when Buddha taught about karma, the Buddha meant the

422
00:33:18,190 --> 00:33:19,000
 volition.

423
00:33:19,000 --> 00:33:23,810
 So that mental state that arises in the mind when you do

424
00:33:23,810 --> 00:33:25,000
 something good

425
00:33:25,000 --> 00:33:29,000
 or something bad is called karma.

426
00:33:29,000 --> 00:33:36,940
 And that mental state has the ability or potential to give

427
00:33:36,940 --> 00:33:40,000
 results in the future.

428
00:33:40,000 --> 00:33:52,270
 Mind has great power. And when mind is headed by this vol

429
00:33:52,270 --> 00:33:56,000
ition, it can give results.

430
00:33:56,000 --> 00:34:05,000
 Now, we look at our body.

431
00:34:05,000 --> 00:34:12,150
 If this body is connected with mind, it moves, it walks, it

432
00:34:12,150 --> 00:34:13,000
 eats and so on,

433
00:34:13,000 --> 00:34:16,000
 it does many activities.

434
00:34:16,000 --> 00:34:22,830
 Once, mind is separated from the body. What happens? We are

435
00:34:22,830 --> 00:34:24,000
 dead.

436
00:34:24,000 --> 00:34:30,000
 So a dead body cannot even move.

437
00:34:30,000 --> 00:34:41,000
 But a body that is alive can do many activities.

438
00:34:41,000 --> 00:34:51,510
 Now, this hand is rupa, this is the matter. So this is

439
00:34:51,510 --> 00:34:53,000
 heavy.

440
00:34:53,000 --> 00:34:59,000
 Although this is heavy, the mind can make it move.

441
00:34:59,000 --> 00:35:02,940
 Now, I have the intention to move and so there is movement

442
00:35:02,940 --> 00:35:04,000
 of the hand.

443
00:35:04,000 --> 00:35:09,000
 So my movement of the hand is caused by my mind.

444
00:35:09,000 --> 00:35:14,000
 If I do not want to move, my hand will not move.

445
00:35:14,000 --> 00:35:22,000
 So my mind has the power to make the hand move.

446
00:35:22,000 --> 00:35:26,600
 Mind cannot be seen, but the hand can be seen and it is

447
00:35:26,600 --> 00:35:29,000
 heavy compared to mind.

448
00:35:29,000 --> 00:35:37,900
 But with the power of mind, I can move my hand. So mind has

449
00:35:37,900 --> 00:35:39,000
 power.

450
00:35:39,000 --> 00:35:45,420
 And among the mental states, there is one that is called

451
00:35:45,420 --> 00:35:47,000
 volition or kama

452
00:35:47,000 --> 00:35:52,000
 and that has the potential to give results in the future.

453
00:35:52,000 --> 00:36:00,080
 And so when the conditions are ripe for the kama to give

454
00:36:00,080 --> 00:36:04,000
 results, then the results are produced.

455
00:36:04,000 --> 00:36:09,530
 So there is what we call kama and there is what we call the

456
00:36:09,530 --> 00:36:11,000
 results of kama.

457
00:36:11,000 --> 00:36:17,450
 And we Buddhists believe in this law of kama. That is, that

458
00:36:17,450 --> 00:36:22,000
 is kama and that is the results of kama.

459
00:36:22,000 --> 00:36:28,000
 And also we believe that good kama produces good results

460
00:36:28,000 --> 00:36:38,000
 and bad kama produces bad results.

461
00:36:38,000 --> 00:36:43,640
 For Buddhists, this second requirement is not difficult

462
00:36:43,640 --> 00:36:46,000
 because since we are Buddhists,

463
00:36:46,000 --> 00:36:54,850
 we already have understanding of and belief in the law of k

464
00:36:54,850 --> 00:36:56,000
ama.

465
00:36:56,000 --> 00:37:02,000
 But what about other people who belong to other religions?

466
00:37:02,000 --> 00:37:09,810
 They want to practice meditation. What should we say to

467
00:37:09,810 --> 00:37:11,000
 them?

468
00:37:11,000 --> 00:37:14,440
 If you don't believe in the law of kama, you cannot

469
00:37:14,440 --> 00:37:22,000
 practice meditation and turn them away.

470
00:37:22,000 --> 00:37:26,000
 I don't want to do that.

471
00:37:26,000 --> 00:37:31,550
 So I'm going to say something like this. You should have

472
00:37:31,550 --> 00:37:34,000
 believed in the law of cause and effect.

473
00:37:34,000 --> 00:37:39,320
 Not the law of kama. I think everybody can accept this, the

474
00:37:39,320 --> 00:37:41,000
 law of cause and effect

475
00:37:41,000 --> 00:37:43,160
 because when there is a cause, there is an effect and

476
00:37:43,160 --> 00:37:45,000
 effect is always caused by the cause.

477
00:37:45,000 --> 00:37:50,610
 So this relationship as cause and effect can be seen

478
00:37:50,610 --> 00:37:56,000
 everywhere and I think everybody can accept this.

479
00:37:56,000 --> 00:38:00,000
 So to those who do not believe in kama, so we would say,

480
00:38:00,000 --> 00:38:03,000
 okay, if you don't believe in kama, it's okay.

481
00:38:03,000 --> 00:38:07,550
 But do you believe in the law of cause and effect? I think

482
00:38:07,550 --> 00:38:09,000
 he will say yes.

483
00:38:09,000 --> 00:38:14,000
 Then you can practice meditation.

484
00:38:14,000 --> 00:38:32,000
 So according to this discourse given by the Buddha,

485
00:38:32,000 --> 00:38:39,000
 there are two preliminary requirements before one practices

486
00:38:39,000 --> 00:38:42,000
 the foundations of mindfulness.

487
00:38:42,000 --> 00:38:49,580
 But there are some other things also that a person should

488
00:38:49,580 --> 00:38:53,000
 do before he practices meditation.

489
00:38:53,000 --> 00:38:59,060
 And that is asking forgiveness. Now we do asking

490
00:38:59,060 --> 00:39:01,000
 forgiveness every morning.

491
00:39:01,000 --> 00:39:14,570
 That is because it is important. Asking forgiveness is very

492
00:39:14,570 --> 00:39:19,000
 important for a person who is practicing meditation.

493
00:39:19,000 --> 00:39:25,000
 So that is what we are going to do.

