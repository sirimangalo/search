1
00:00:00,000 --> 00:00:02,000
 Am I suppressing emotions?

2
00:00:02,000 --> 00:00:05,600
 Question.

3
00:00:05,600 --> 00:00:08,200
 I am trying to observe my emotions,

4
00:00:08,200 --> 00:00:10,400
 but I am somewhat out of touch with them,

5
00:00:10,400 --> 00:00:11,760
 and I control them a lot.

6
00:00:11,760 --> 00:00:15,000
 When an emotion goes away,

7
00:00:15,000 --> 00:00:17,920
 how can I tell if it has subsided on its own

8
00:00:17,920 --> 00:00:19,240
 or if I suppressed it?

9
00:00:19,240 --> 00:00:21,600
 Answer.

10
00:00:21,600 --> 00:00:25,440
 I would not be so analytical about it

11
00:00:25,440 --> 00:00:27,200
 because the analysis itself

12
00:00:27,200 --> 00:00:29,120
 is somewhat of a misunderstanding.

13
00:00:30,120 --> 00:00:32,520
 The concern that you are suppressing,

14
00:00:32,520 --> 00:00:33,720
 not accepting,

15
00:00:33,720 --> 00:00:35,920
 and not learning about the experience

16
00:00:35,920 --> 00:00:37,720
 is often a misunderstanding

17
00:00:37,720 --> 00:00:39,320
 of what is actually happening.

18
00:00:39,320 --> 00:00:43,920
 The emotions themselves will cause stress,

19
00:00:43,920 --> 00:00:46,120
 and that is why there is stress in the mind.

20
00:00:46,120 --> 00:00:49,520
 It is not because you are somehow suppressing them.

21
00:00:49,520 --> 00:00:52,720
 When certain emotions arise,

22
00:00:52,720 --> 00:00:56,520
 we often give rise to additional conflicting emotions,

23
00:00:56,520 --> 00:00:58,920
 and that is where it starts to get confusing.

24
00:00:59,920 --> 00:01:03,120
 The desire to suppress is stressful,

25
00:01:03,120 --> 00:01:06,120
 and this is something you can see if your mind is quiet.

26
00:01:06,120 --> 00:01:08,720
 Anger is stressful,

27
00:01:08,720 --> 00:01:11,520
 and the desire to suppress anger is stressful.

28
00:01:11,520 --> 00:01:15,120
 This will lead to things like tension in the body

29
00:01:15,120 --> 00:01:17,120
 and suffering in the mind,

30
00:01:17,120 --> 00:01:19,320
 so what you are probably experiencing

31
00:01:19,320 --> 00:01:20,920
 is quite a bit of suffering.

32
00:01:20,920 --> 00:01:24,920
 Try to think about the suffering objectively.

33
00:01:24,920 --> 00:01:27,520
 When these thoughts arise,

34
00:01:27,520 --> 00:01:31,520
 along with the worries, confusion, and uncertainty,

35
00:01:31,520 --> 00:01:34,520
 remember that part of the difficulty that you are having

36
00:01:34,520 --> 00:01:36,520
 is the struggle to see impermanence,

37
00:01:36,520 --> 00:01:38,920
 suffering, and non-self.

38
00:01:38,920 --> 00:01:42,720
 It is a struggle to stop trying to fix things.

39
00:01:42,720 --> 00:01:45,920
 Thinking that something is wrong with your practice

40
00:01:45,920 --> 00:01:47,720
 is actually kind of misleading.

41
00:01:47,720 --> 00:01:50,320
 There is nothing called practice,

42
00:01:50,320 --> 00:01:51,920
 and you are not doing it.

43
00:01:51,920 --> 00:01:53,920
 There is only experience,

44
00:01:53,920 --> 00:01:55,720
 and it is coming and going.

45
00:01:56,720 --> 00:01:58,320
 Once you realize that,

46
00:01:58,320 --> 00:02:00,120
 then all of your problems disappear

47
00:02:00,120 --> 00:02:02,120
 like knots becoming untied.

48
00:02:02,120 --> 00:02:05,720
 Every moment is a new knot.

49
00:02:05,720 --> 00:02:10,720
 Every moment in your meditation is a new challenge,

50
00:02:10,720 --> 00:02:14,320
 and the stress occurs when we fail to meet the challenge,

51
00:02:14,320 --> 00:02:16,120
 when at any given moment,

52
00:02:16,120 --> 00:02:19,120
 we abandon mindfulness and cling to something.

53
00:02:19,120 --> 00:02:23,120
 Without mindfulness and wisdom,

54
00:02:23,120 --> 00:02:24,920
 we do not have the flexibility

55
00:02:24,920 --> 00:02:27,720
 to be able to move from one challenge to the next.

56
00:02:27,720 --> 00:02:30,520
 This flexibility is one reason

57
00:02:30,520 --> 00:02:34,120
 that enlightened people are so bright and clear in mind.

58
00:02:34,120 --> 00:02:38,320
 For example, when focusing on pain,

59
00:02:38,320 --> 00:02:41,320
 thinking, pain, pain,

60
00:02:41,320 --> 00:02:45,720
 after some time your mind finally realizes it is just pain,

61
00:02:45,720 --> 00:02:48,520
 then maybe some kind of pleasure comes up,

62
00:02:48,520 --> 00:02:50,920
 but you are still thinking about the pain,

63
00:02:50,920 --> 00:02:54,520
 which makes you unable to deal with the new experience.

64
00:02:55,920 --> 00:02:58,920
 Each experience has to be dealt with as it is,

65
00:02:58,920 --> 00:03:01,720
 with no reference to past or future.

66
00:03:01,720 --> 00:03:05,920
 The first shift of awareness regarding the pain

67
00:03:05,920 --> 00:03:08,120
 was to convince yourself that the pain,

68
00:03:08,120 --> 00:03:09,920
 rather than being bad,

69
00:03:09,920 --> 00:03:11,520
 is just pain.

70
00:03:11,520 --> 00:03:14,320
 Then pleasure arises,

71
00:03:14,320 --> 00:03:15,120
 and again,

72
00:03:15,120 --> 00:03:17,320
 you have to shift your perspective

73
00:03:17,320 --> 00:03:19,920
 to see that it is just a feeling of pleasure

74
00:03:19,920 --> 00:03:22,520
 caused by chemicals arising in the body.

75
00:03:23,720 --> 00:03:25,720
 You still have to see it for what it is,

76
00:03:25,720 --> 00:03:29,720
 but the pleasure is coming from another angle of being,

77
00:03:29,720 --> 00:03:31,120
 quote-unquote,

78
00:03:31,120 --> 00:03:32,520
 a good thing,

79
00:03:32,520 --> 00:03:34,920
 making you want to cling to it.

80
00:03:34,920 --> 00:03:38,520
 You have to teach yourself to be present

81
00:03:38,520 --> 00:03:40,720
 and objective with each experience.

82
00:03:40,720 --> 00:03:44,920
 The difficulty is not in suppressing.

83
00:03:44,920 --> 00:03:48,720
 The difficulty is adapting to the situation,

84
00:03:48,720 --> 00:03:51,990
 which simply means that no matter which angle the mind is

85
00:03:51,990 --> 00:03:52,720
 taking,

86
00:03:52,720 --> 00:03:54,320
 you must straighten it out.

87
00:03:54,320 --> 00:03:56,720
 If it is crooked this way,

88
00:03:56,720 --> 00:03:59,120
 one must straighten it back that way.

89
00:03:59,120 --> 00:04:01,920
 Everything has to be straightened out.

90
00:04:01,920 --> 00:04:04,720
 Whatever the mind is clinging to,

91
00:04:04,720 --> 00:04:07,920
 one must bring it back to being straight and centered,

92
00:04:07,920 --> 00:04:10,920
 clearly aware of everything just as it is.

93
00:04:10,920 --> 00:04:16,360
 When the feeling that you identify as "suppressing" comes

94
00:04:16,360 --> 00:04:16,720
 up,

95
00:04:16,720 --> 00:04:19,120
 acknowledge it as just a feeling,

96
00:04:19,120 --> 00:04:21,920
 and you should be able to see that it is just a feeling

97
00:04:21,920 --> 00:04:24,320
 based on defilements and emotions.

98
00:04:24,320 --> 00:04:27,320
 Sometimes you might say,

99
00:04:27,320 --> 00:04:30,920
 "When I acknowledge thinking, thinking,

100
00:04:30,920 --> 00:04:32,920
 it seems to give me a headache.

101
00:04:32,920 --> 00:04:35,620
 When I am thinking a lot,

102
00:04:35,620 --> 00:04:37,120
 I don't have the headache,

103
00:04:37,120 --> 00:04:40,020
 but as soon as I start noting, thinking,

104
00:04:40,020 --> 00:04:41,820
 I get this big headache."

105
00:04:41,820 --> 00:04:45,230
 And you start to think that the noting is causing you

106
00:04:45,230 --> 00:04:45,820
 suffering.

107
00:04:45,820 --> 00:04:48,120
 But if you are observant,

108
00:04:48,120 --> 00:04:51,120
 you will realize that you are building up the headache

109
00:04:51,220 --> 00:04:53,920
 without knowing it during the time you were thinking.

110
00:04:53,920 --> 00:04:56,920
 And when you realize you were thinking,

111
00:04:56,920 --> 00:05:00,520
 and you start noting, thinking, thinking,

112
00:05:00,520 --> 00:05:03,220
 you suddenly have to deal with the headache.

113
00:05:03,220 --> 00:05:06,620
 When you have lust or desire,

114
00:05:06,620 --> 00:05:08,820
 you might focus on it by saying,

115
00:05:08,820 --> 00:05:10,420
 "Wanting, wanting,

116
00:05:10,420 --> 00:05:12,820
 or pleasure, pleasure."

117
00:05:12,820 --> 00:05:16,320
 And you may feel tension in the body as you do so,

118
00:05:16,320 --> 00:05:20,020
 but the tension is there because of the desire itself.

119
00:05:21,220 --> 00:05:22,420
 Instead of tension,

120
00:05:22,420 --> 00:05:26,020
 you might feel the dullness of mind that comes with desire.

121
00:05:26,020 --> 00:05:27,520
 And you may think,

122
00:05:27,520 --> 00:05:30,420
 "Oh, this is coming from meditation.

123
00:05:30,420 --> 00:05:33,020
 I am suppressing this in the meditation,

124
00:05:33,020 --> 00:05:35,320
 so it is giving me this feeling of dullness."

125
00:05:35,320 --> 00:05:38,720
 But the desire is what causes the dullness.

126
00:05:38,720 --> 00:05:43,080
 The emotions themselves bring the physical feelings as a by

127
00:05:43,080 --> 00:05:44,020
product.

128
00:05:44,020 --> 00:05:46,920
 When you start to practice,

129
00:05:46,920 --> 00:05:49,820
 you are just beginning to see objectively,

130
00:05:51,120 --> 00:05:52,620
 the moment when you had desire,

131
00:05:52,620 --> 00:05:54,820
 you were not seeing things objectively.

132
00:05:54,820 --> 00:05:58,620
 That is why the desire feels wonderful until you are

133
00:05:58,620 --> 00:05:59,820
 mindful of it,

134
00:05:59,820 --> 00:06:03,820
 because suddenly, you are objective again,

135
00:06:03,820 --> 00:06:05,820
 and you are seeing both sides,

136
00:06:05,820 --> 00:06:07,820
 the good and the bad.

137
00:06:07,820 --> 00:06:11,320
 With desire, you are running towards the good

138
00:06:11,320 --> 00:06:13,320
 and running away from the bad,

139
00:06:13,320 --> 00:06:16,680
 so that you never really experience the bad until you stop

140
00:06:16,680 --> 00:06:17,320
 running,

141
00:06:18,320 --> 00:06:21,320
 and then you experience it and have to deal with it.

142
00:06:21,320 --> 00:06:24,620
 I would advise not to worry too much,

143
00:06:24,620 --> 00:06:26,620
 even if you are suppressing.

144
00:06:26,620 --> 00:06:30,320
 The most important thing is to watch the suppression,

145
00:06:30,320 --> 00:06:32,420
 not to stop suppressing.

146
00:06:32,420 --> 00:06:35,620
 Just watch and see what is going on,

147
00:06:35,620 --> 00:06:39,120
 because inevitably, you will come to see impermanence,

148
00:06:39,120 --> 00:06:41,620
 suffering, and non-self.

149
00:06:41,620 --> 00:06:45,620
 You will see that you cannot control your experiences.

150
00:06:46,620 --> 00:06:49,120
 You will see that even the suppressing is something that

151
00:06:49,120 --> 00:06:51,620
 only happens based on causes and conditions.

152
00:06:51,620 --> 00:06:55,620
 It is not worth getting caught up in or attached to.

153
00:06:55,620 --> 00:07:13,620
 [

