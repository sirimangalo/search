1
00:00:00,000 --> 00:00:18,840
 Good evening, everyone.

2
00:00:18,840 --> 00:00:22,520
 Broadcasting live, March 17th.

3
00:00:22,520 --> 00:00:33,280
 Hopefully the audio is working fine.

4
00:00:33,280 --> 00:00:39,880
 Tonight's quote actually seems a lot like a repeat.

5
00:00:39,880 --> 00:00:47,000
 And on the face of it is a pretty simple quote.

6
00:00:47,000 --> 00:00:54,000
 And what is it?

7
00:00:54,000 --> 00:01:03,160
 If we only look at the first part of the quote, we might

8
00:01:03,160 --> 00:01:09,960
 not immediately think of the answer.

9
00:01:09,960 --> 00:01:10,960
 I think I did.

10
00:01:10,960 --> 00:01:14,320
 I think when I read it I was like, "Oh, I know what he's

11
00:01:14,320 --> 00:01:15,520
 going to say."

12
00:01:15,520 --> 00:01:16,520
 Of course.

13
00:01:16,520 --> 00:01:18,820
 Oh no, actually once he said the eight things, he said

14
00:01:18,820 --> 00:01:20,000
 there are eight things.

15
00:01:20,000 --> 00:01:23,420
 He said, "What conditions when developed and practiced lead

16
00:01:23,420 --> 00:01:24,280
 to nirvana?

17
00:01:24,280 --> 00:01:27,600
 Have nirvana as their goal, culminate in nirvana?"

18
00:01:27,600 --> 00:01:33,520
 And he says, "There are nandya eight things."

19
00:01:33,520 --> 00:01:39,040
 And then of course we know what he's going to say.

20
00:01:39,040 --> 00:01:42,760
 Eight things, when you develop eight things, when you

21
00:01:42,760 --> 00:01:45,080
 practice eight things, it leads to

22
00:01:45,080 --> 00:01:46,080
 nirvana.

23
00:01:46,080 --> 00:01:53,520
 They have nirvana as their goal, they culminate in nirvana.

24
00:01:53,520 --> 00:01:57,440
 And of course then he says the eightfold noble paths.

25
00:01:57,440 --> 00:02:01,310
 Right view, right thought, right speech, right action,

26
00:02:01,310 --> 00:02:04,040
 right livelihood, right effort, right

27
00:02:04,040 --> 00:02:12,080
 mindfulness and right concentration.

28
00:02:12,080 --> 00:02:20,320
 It's a simple quote.

29
00:02:20,320 --> 00:02:25,000
 We might not have much to say about it except it interests

30
00:02:25,000 --> 00:02:28,160
 me particularly having just written

31
00:02:28,160 --> 00:02:37,490
 an article on some, what I would consider non-mainstream

32
00:02:37,490 --> 00:02:41,400
 Buddhist teachings.

33
00:02:41,400 --> 00:02:50,200
 And so it gives opportunity to sort of highlight this quote

34
00:02:50,200 --> 00:02:56,560
 as an example of really right Buddhism.

35
00:02:56,560 --> 00:03:08,250
 It's easy to lose sight of the core practices that we're

36
00:03:08,250 --> 00:03:11,360
 aiming for.

37
00:03:11,360 --> 00:03:19,480
 We get caught up in theory and well argumentation really.

38
00:03:19,480 --> 00:03:29,680
 And there's so many new doctrines, new practices, new ideas

39
00:03:29,680 --> 00:03:30,320
.

40
00:03:30,320 --> 00:03:37,440
 The idea that things have to be, that there is a

41
00:03:37,440 --> 00:03:42,800
 degradation or more it seems that the

42
00:03:42,800 --> 00:03:49,220
 old, the old stops working and so rather than try to figure

43
00:03:49,220 --> 00:03:51,840
 out what we're doing wrong,

44
00:03:51,840 --> 00:04:04,320
 we change the parameters.

45
00:04:04,320 --> 00:04:14,200
 And so on a sort of the microscopic or the personal level,

46
00:04:14,200 --> 00:04:21,800
 an example of this is where

47
00:04:21,800 --> 00:04:26,800
 we lose sight of mindfulness.

48
00:04:26,800 --> 00:04:35,350
 When something happens and we're unable to resolve the

49
00:04:35,350 --> 00:04:37,640
 situation.

50
00:04:37,640 --> 00:04:42,680
 For example, we might have bright lights or colors arise.

51
00:04:42,680 --> 00:04:50,470
 We might have rapturous experiences of excitement in the

52
00:04:50,470 --> 00:04:53,480
 body or in the mind.

53
00:04:53,480 --> 00:04:59,640
 We might have sounds or smells or tastes and feelings.

54
00:04:59,640 --> 00:05:07,160
 We might have thoughts that arise that we don't know how to

55
00:05:07,160 --> 00:05:08,920
 deal with.

56
00:05:08,920 --> 00:05:13,320
 And we lose sight of the very basic practice.

57
00:05:13,320 --> 00:05:20,000
 We can get lost in exceptional experiences.

58
00:05:20,000 --> 00:05:28,670
 The ten upa kilas are good examples of these, as some of

59
00:05:28,670 --> 00:05:33,480
 them I mentioned actually.

60
00:05:33,480 --> 00:05:38,790
 Or we can get caught up in trying to find causes for our

61
00:05:38,790 --> 00:05:42,040
 problems and creating stories

62
00:05:42,040 --> 00:05:46,400
 for well I'm this way because of this and this and this.

63
00:05:46,400 --> 00:05:49,740
 And a lot of people come with the idea that they're stuck

64
00:05:49,740 --> 00:05:51,120
 in their practice.

65
00:05:51,120 --> 00:05:54,040
 I hear that a lot.

66
00:05:54,040 --> 00:06:04,450
 I've just gotten stuck in my practice which is probably in

67
00:06:04,450 --> 00:06:09,360
 90% of the cases, it's just

68
00:06:09,360 --> 00:06:21,320
 a wrong focus, having lost focus of the core practice.

69
00:06:21,320 --> 00:06:23,080
 There's no way to get stuck.

70
00:06:23,080 --> 00:06:24,320
 There's no such thing as being stuck.

71
00:06:24,320 --> 00:06:25,720
 I mean we're all stuck.

72
00:06:25,720 --> 00:06:26,880
 We're stuck in these bodies.

73
00:06:26,880 --> 00:06:32,960
 We're stuck with these minds that are imperfect malfunction

74
00:06:32,960 --> 00:06:33,640
ing.

75
00:06:33,640 --> 00:06:42,180
 And so it's not really a matter even of getting unstuck in

76
00:06:42,180 --> 00:06:45,000
 the situation.

77
00:06:45,000 --> 00:06:48,960
 It's about getting unstuck in our reactions to the

78
00:06:48,960 --> 00:06:50,160
 situation.

79
00:06:50,160 --> 00:06:53,590
 So if you're stuck, what you should really be asking is

80
00:06:53,590 --> 00:06:55,160
 does that bother you?

81
00:06:55,160 --> 00:06:58,120
 Does it bother you that you're stuck?

82
00:06:58,120 --> 00:07:00,120
 How do you feel about that?

83
00:07:00,120 --> 00:07:03,480
 Do you identify with that?

84
00:07:03,480 --> 00:07:04,480
 Do you experience?

85
00:07:04,480 --> 00:07:06,820
 And moreover I guess what do you mean by being stuck

86
00:07:06,820 --> 00:07:09,280
 because that's just a concept and gets

87
00:07:09,280 --> 00:07:11,430
 back to this idea of losing sight of what's really

88
00:07:11,430 --> 00:07:12,160
 happening.

89
00:07:12,160 --> 00:07:14,910
 There's no, you know, what does it mean you have something

90
00:07:14,910 --> 00:07:16,200
 sticky on your fingers?

91
00:07:16,200 --> 00:07:17,200
 No.

92
00:07:17,200 --> 00:07:20,440
 When you say I'm stuck it doesn't mean anything really.

93
00:07:20,440 --> 00:07:22,960
 It's a conclusion that you come into.

94
00:07:22,960 --> 00:07:26,680
 It's not an observation.

95
00:07:26,680 --> 00:07:27,680
 Observations are momentary.

96
00:07:27,680 --> 00:07:29,360
 They arise and they cease.

97
00:07:29,360 --> 00:07:30,680
 There's nothing to stick to.

98
00:07:30,680 --> 00:07:37,640
 You know, they think sticky is our reaction to things.

99
00:07:37,640 --> 00:07:41,210
 All of this, I mean it relates to this quote because of how

100
00:07:41,210 --> 00:07:42,680
 simple the quote is.

101
00:07:42,680 --> 00:07:46,820
 You know, if you have these eight things that's all you

102
00:07:46,820 --> 00:07:47,480
 need.

103
00:07:47,480 --> 00:07:49,780
 You're reading that question and you're wondering, "Yeah,

104
00:07:49,780 --> 00:07:51,000
 what is it that leads to Iran?

105
00:07:51,000 --> 00:07:52,240
 What do you need to get?"

106
00:07:52,240 --> 00:07:55,120
 You're like, "Oh right, it's just a hateful normal path."

107
00:07:55,120 --> 00:08:04,310
 And it's kind of almost anti-climatic because you think, "

108
00:08:04,310 --> 00:08:08,400
Well, there must be more than

109
00:08:08,400 --> 00:08:09,400
 that."

110
00:08:09,400 --> 00:08:10,400
 And that's it.

111
00:08:10,400 --> 00:08:11,400
 That's all the news.

112
00:08:11,400 --> 00:08:15,540
 And this is why people create new teachings, new ideas

113
00:08:15,540 --> 00:08:18,640
 because, well yeah, I know the hateful

114
00:08:18,640 --> 00:08:21,260
 normal path but there must be something more than that,

115
00:08:21,260 --> 00:08:21,800
 right?

116
00:08:21,800 --> 00:08:26,500
 Besides the hateful normal path, it's like I remember I

117
00:08:26,500 --> 00:08:28,760
 went to a teacher once and I

118
00:08:28,760 --> 00:08:34,230
 was really sort of depressed about my situation and I'd

119
00:08:34,230 --> 00:08:37,640
 gotten real trouble with a layman

120
00:08:37,640 --> 00:08:40,360
 who was my teacher at the time.

121
00:08:40,360 --> 00:08:43,190
 They were saying all sorts of nasty things about me and I

122
00:08:43,190 --> 00:08:44,720
 felt kind of like, "Gee, maybe

123
00:08:44,720 --> 00:08:46,360
 I'm an evil person."

124
00:08:46,360 --> 00:08:50,000
 So I went to this one of the monks and I said, "How do you

125
00:08:50,000 --> 00:08:52,440
 know if you're an evil person?"

126
00:08:52,440 --> 00:08:56,950
 I was really at my wit's end and he gave this very stock

127
00:08:56,950 --> 00:08:57,960
 answer.

128
00:08:57,960 --> 00:09:02,900
 He said, "Well, there are these ten types of evil, the Aku

129
00:09:02,900 --> 00:09:05,160
sela Kamapada, which are,"

130
00:09:05,160 --> 00:09:08,760
 and they relate actually to the hateful normal path in some

131
00:09:08,760 --> 00:09:10,920
 ways, basically wrong action,

132
00:09:10,920 --> 00:09:14,920
 wrong speech and wrong thought.

133
00:09:14,920 --> 00:09:21,420
 And he said, "If you're breaking any of those, that's evil

134
00:09:21,420 --> 00:09:22,040
."

135
00:09:22,040 --> 00:09:24,500
 And I thought about it and I said, "Well, what if you're

136
00:09:24,500 --> 00:09:25,720
 okay with all of those but

137
00:09:25,720 --> 00:09:29,080
 you still feel like you're an evil person?"

138
00:09:29,080 --> 00:09:33,600
 It's like that kind of, it was a silly question really.

139
00:09:33,600 --> 00:09:38,200
 We think too much about things.

140
00:09:38,200 --> 00:09:42,000
 We think there's a problem.

141
00:09:42,000 --> 00:09:46,260
 We think something's wrong when in fact it comes down to

142
00:09:46,260 --> 00:09:47,280
 reality.

143
00:09:47,280 --> 00:09:48,720
 Is it good or is it bad?

144
00:09:48,720 --> 00:09:49,720
 Is it right?

145
00:09:49,720 --> 00:09:50,720
 Is it wrong?

146
00:09:50,720 --> 00:09:51,720
 It is or it isn't?

147
00:09:51,720 --> 00:09:52,720
 Very simple.

148
00:09:52,720 --> 00:09:55,720
 The Dhamma is very simple.

149
00:09:55,720 --> 00:10:07,320
 And so I made a statement about one text recently as being

150
00:10:07,320 --> 00:10:11,880
 sophistic, sophism, and all of that

151
00:10:11,880 --> 00:10:15,430
 is disagreeable to people who like that text and I

152
00:10:15,430 --> 00:10:16,960
 understand that.

153
00:10:16,960 --> 00:10:23,600
 It's probably problematic to be bringing these things up on

154
00:10:23,600 --> 00:10:25,560
 the internet.

155
00:10:25,560 --> 00:10:29,240
 The point is this.

156
00:10:29,240 --> 00:10:30,320
 This is not sophistic.

157
00:10:30,320 --> 00:10:33,320
 This is simple.

158
00:10:33,320 --> 00:10:38,880
 There's no high or exalted teachings here.

159
00:10:38,880 --> 00:10:41,840
 There's a perfect teaching and it's a very simple teaching.

160
00:10:41,840 --> 00:10:42,840
 It's a perfect teaching.

161
00:10:42,840 --> 00:10:43,840
 It's a perfect teaching.

162
00:10:43,840 --> 00:10:44,840
 It's a perfect teaching.

163
00:10:44,840 --> 00:10:45,840
 It's a perfect teaching.

164
00:10:45,840 --> 00:10:46,840
 It's a perfect teaching.

165
00:10:46,840 --> 00:10:47,840
 It's a perfect teaching.

166
00:10:47,840 --> 00:10:48,840
 It's a perfect teaching.

167
00:10:48,840 --> 00:10:49,840
 It's a perfect teaching.

168
00:10:49,840 --> 00:10:50,840
 It's a perfect teaching.

169
00:10:50,840 --> 00:10:51,840
 It's a perfect teaching.

170
00:10:51,840 --> 00:10:52,840
 It's a perfect teaching.

171
00:10:52,840 --> 00:10:53,840
 It's a perfect teaching.

172
00:10:53,840 --> 00:10:54,840
 It's a perfect teaching.

173
00:10:54,840 --> 00:10:55,840
 It's a perfect teaching.

174
00:10:55,840 --> 00:10:56,840
 It's a perfect teaching.

175
00:10:56,840 --> 00:10:57,840
 It's a perfect teaching.

176
00:10:57,840 --> 00:10:58,840
 It's a perfect teaching.

177
00:10:58,840 --> 00:10:59,840
 It's a perfect teaching.

178
00:10:59,840 --> 00:11:00,840
 It's a perfect teaching.

179
00:11:00,840 --> 00:11:01,840
 It's a perfect teaching.

180
00:11:01,840 --> 00:11:02,840
 It's a perfect teaching.

181
00:11:02,840 --> 00:11:03,840
 It's a perfect teaching.

182
00:11:03,840 --> 00:11:04,840
 It's a perfect teaching.

183
00:11:04,840 --> 00:11:05,840
 It's a perfect teaching.

184
00:11:05,840 --> 00:11:06,840
 It's a perfect teaching.

185
00:11:06,840 --> 00:11:07,840
 It's a perfect teaching.

186
00:11:07,840 --> 00:11:08,840
 It's a perfect teaching.

187
00:11:08,840 --> 00:11:11,840
 It's a perfect teaching.

188
00:11:11,840 --> 00:11:12,840
 It's a perfect teaching.

189
00:11:12,840 --> 00:11:13,840
 It's a perfect teaching.

190
00:11:13,840 --> 00:11:14,840
 It's a perfect teaching.

191
00:11:14,840 --> 00:11:15,840
 It's a perfect teaching.

192
00:11:15,840 --> 00:11:16,840
 It's a perfect teaching.

193
00:11:16,840 --> 00:11:17,840
 It's a perfect teaching.

194
00:11:17,840 --> 00:11:18,840
 It's a perfect teaching.

195
00:11:18,840 --> 00:11:19,840
 It's a perfect teaching.

196
00:11:19,840 --> 00:11:20,840
 It's a perfect teaching.

197
00:11:20,840 --> 00:11:21,840
 It's a perfect teaching.

198
00:11:21,840 --> 00:11:22,840
 It's a perfect teaching.

199
00:11:22,840 --> 00:11:23,840
 It's a perfect teaching.

200
00:11:23,840 --> 00:11:24,840
 It's a perfect teaching.

201
00:11:24,840 --> 00:11:25,840
 It's a perfect teaching.

202
00:11:25,840 --> 00:11:26,840
 It's a perfect teaching.

203
00:11:26,840 --> 00:11:27,840
 It's a perfect teaching.

204
00:11:27,840 --> 00:11:28,840
 It's a perfect teaching.

205
00:11:28,840 --> 00:11:29,840
 It's a perfect teaching.

206
00:11:29,840 --> 00:11:30,840
 It's a perfect teaching.

207
00:11:30,840 --> 00:11:31,840
 It's a perfect teaching.

208
00:11:31,840 --> 00:11:32,840
 It's a perfect teaching.

209
00:11:32,840 --> 00:11:33,840
 It's a perfect teaching.

210
00:11:33,840 --> 00:11:34,840
 It's a perfect teaching.

211
00:11:34,840 --> 00:11:35,840
 It's a perfect teaching.

212
00:11:35,840 --> 00:11:36,840
 It's a perfect teaching.

213
00:11:36,840 --> 00:11:37,840
 It's a perfect teaching.

214
00:11:37,840 --> 00:11:38,840
 It's a perfect teaching.

215
00:11:38,840 --> 00:11:39,840
 It's a perfect teaching.

216
00:11:39,840 --> 00:11:40,840
 It's a perfect teaching.

217
00:11:40,840 --> 00:11:41,840
 It's a perfect teaching.

218
00:11:41,840 --> 00:11:42,840
 It's a perfect teaching.

219
00:11:42,840 --> 00:11:43,840
 It's a perfect teaching.

220
00:11:43,840 --> 00:11:44,840
 It's a perfect teaching.

221
00:11:44,840 --> 00:11:45,840
 It's a perfect teaching.

222
00:11:45,840 --> 00:11:46,840
 It's a perfect teaching.

223
00:11:46,840 --> 00:11:47,840
 It's a perfect teaching.

224
00:11:47,840 --> 00:11:48,840
 It's a perfect teaching.

225
00:11:48,840 --> 00:11:49,840
 It's a perfect teaching.

226
00:11:49,840 --> 00:11:50,840
 It's a perfect teaching.

227
00:11:50,840 --> 00:11:51,840
 It's a perfect teaching.

228
00:11:51,840 --> 00:11:52,840
 It's a perfect teaching.

229
00:11:52,840 --> 00:11:53,840
 It's a perfect teaching.

230
00:11:53,840 --> 00:11:54,840
 It's a perfect teaching.

231
00:11:54,840 --> 00:11:55,840
 It's a perfect teaching.

232
00:11:55,840 --> 00:11:56,840
 It's a perfect teaching.

233
00:11:56,840 --> 00:11:57,840
 It's a perfect teaching.

234
00:11:57,840 --> 00:11:58,840
 It's a perfect teaching.

235
00:11:58,840 --> 00:11:59,840
 It's a perfect teaching.

236
00:11:59,840 --> 00:12:00,840
 It's a perfect teaching.

237
00:12:00,840 --> 00:12:01,840
 It's a perfect teaching.

238
00:12:01,840 --> 00:12:02,840
 It's a perfect teaching.

239
00:12:02,840 --> 00:12:03,840
 It's a perfect teaching.

240
00:12:03,840 --> 00:12:04,840
 It's a perfect teaching.

241
00:12:04,840 --> 00:12:07,840
 It's a perfect teaching.

242
00:12:07,840 --> 00:12:08,840
 It's a perfect teaching.

243
00:12:08,840 --> 00:12:09,840
 It's a perfect teaching.

244
00:12:09,840 --> 00:12:10,840
 It's a perfect teaching.

245
00:12:10,840 --> 00:12:11,840
 It's a perfect teaching.

246
00:12:11,840 --> 00:12:12,840
 It's a perfect teaching.

247
00:12:12,840 --> 00:12:13,840
 It's a perfect teaching.

248
00:12:13,840 --> 00:12:14,840
 It's a perfect teaching.

249
00:12:14,840 --> 00:12:15,840
 It's a perfect teaching.

250
00:12:15,840 --> 00:12:16,840
 It's a perfect teaching.

251
00:12:16,840 --> 00:12:17,840
 It's a perfect teaching.

252
00:12:17,840 --> 00:12:18,840
 It's a perfect teaching.

253
00:12:18,840 --> 00:12:19,840
 It's a perfect teaching.

254
00:12:19,840 --> 00:12:20,840
 It's a perfect teaching.

255
00:12:20,840 --> 00:12:21,840
 It's a perfect teaching.

256
00:12:21,840 --> 00:12:22,840
 It's a perfect teaching.

257
00:12:22,840 --> 00:12:23,840
 It's a perfect teaching.

258
00:12:23,840 --> 00:12:24,840
 It's a perfect teaching.

259
00:12:24,840 --> 00:12:25,840
 It's a perfect teaching.

260
00:12:25,840 --> 00:12:26,840
 It's a perfect teaching.

261
00:12:26,840 --> 00:12:27,840
 It's a perfect teaching.

262
00:12:27,840 --> 00:12:28,840
 It's a perfect teaching.

263
00:12:28,840 --> 00:12:29,840
 It's a perfect teaching.

264
00:12:29,840 --> 00:12:30,840
 It's a perfect teaching.

265
00:12:30,840 --> 00:12:31,840
 It's a perfect teaching.

266
00:12:31,840 --> 00:12:32,840
 It's a perfect teaching.

267
00:12:32,840 --> 00:12:33,840
 It's a perfect teaching.

268
00:12:33,840 --> 00:12:34,840
 It's a perfect teaching.

269
00:12:34,840 --> 00:12:35,840
 It's a perfect teaching.

270
00:12:35,840 --> 00:12:36,840
 It's a perfect teaching.

271
00:12:36,840 --> 00:12:37,840
 It's a perfect teaching.

272
00:12:37,840 --> 00:12:38,840
 It's a perfect teaching.

273
00:12:38,840 --> 00:12:39,840
 It's a perfect teaching.

274
00:12:39,840 --> 00:12:40,840
 It's a perfect teaching.

275
00:12:40,840 --> 00:12:41,840
 It's a perfect teaching.

276
00:12:41,840 --> 00:12:42,840
 It's a perfect teaching.

277
00:12:42,840 --> 00:12:43,840
 It's a perfect teaching.

278
00:12:43,840 --> 00:12:44,840
 It's a perfect teaching.

279
00:12:44,840 --> 00:12:45,840
 It's a perfect teaching.

280
00:12:45,840 --> 00:12:46,840
 It's a perfect teaching.

281
00:12:46,840 --> 00:12:47,840
 It's a perfect teaching.

282
00:12:47,840 --> 00:12:48,840
 It's a perfect teaching.

283
00:12:48,840 --> 00:12:49,840
 It's a perfect teaching.

284
00:12:49,840 --> 00:12:50,840
 It's a perfect teaching.

285
00:12:50,840 --> 00:12:51,840
 It's a perfect teaching.

286
00:12:51,840 --> 00:12:52,840
 It's a perfect teaching.

287
00:12:52,840 --> 00:12:53,840
 It's a perfect teaching.

288
00:12:53,840 --> 00:12:54,840
 It's a perfect teaching.

289
00:12:54,840 --> 00:12:55,840
 It's a perfect teaching.

290
00:12:55,840 --> 00:12:56,840
 It's a perfect teaching.

291
00:12:56,840 --> 00:12:57,840
 It's a perfect teaching.

292
00:12:57,840 --> 00:12:58,840
 It's a perfect teaching.

293
00:12:58,840 --> 00:12:59,840
 It's a perfect teaching.

294
00:12:59,840 --> 00:13:00,840
 It's a perfect teaching.

295
00:13:00,840 --> 00:13:03,840
 It's a perfect teaching.

296
00:13:03,840 --> 00:13:06,840
 It's a perfect teaching.

297
00:13:06,840 --> 00:13:08,840
 It's a perfect teaching.

298
00:13:08,840 --> 00:13:10,840
 It's a perfect teaching.

299
00:13:10,840 --> 00:13:12,840
 It's a perfect teaching.

300
00:13:12,840 --> 00:13:14,840
 It's a perfect teaching.

301
00:13:14,840 --> 00:13:16,840
 It's a perfect teaching.

302
00:13:16,840 --> 00:13:18,840
 It's a perfect teaching.

303
00:13:18,840 --> 00:13:20,840
 It's a perfect teaching.

304
00:13:20,840 --> 00:13:22,840
 It's a perfect teaching.

305
00:13:22,840 --> 00:13:24,840
 It's a perfect teaching.

306
00:13:24,840 --> 00:13:26,840
 It's a perfect teaching.

307
00:13:26,840 --> 00:13:28,840
 It's a perfect teaching.

308
00:13:28,840 --> 00:13:30,840
 It's a perfect teaching.

309
00:13:30,840 --> 00:13:32,840
 It's a perfect teaching.

310
00:13:32,840 --> 00:13:34,840
 It's a perfect teaching.

311
00:13:34,840 --> 00:13:36,840
 It's a perfect teaching.

312
00:13:36,840 --> 00:13:38,840
 It's a perfect teaching.

313
00:13:38,840 --> 00:13:40,840
 It's a perfect teaching.

314
00:13:40,840 --> 00:13:42,840
 It's a perfect teaching.

315
00:13:42,840 --> 00:13:44,840
 It's a perfect teaching.

316
00:13:44,840 --> 00:13:46,840
 It's a perfect teaching.

317
00:13:46,840 --> 00:13:48,840
 It's a perfect teaching.

318
00:13:48,840 --> 00:13:50,840
 It's a perfect teaching.

319
00:13:50,840 --> 00:13:52,840
 It's a perfect teaching.

320
00:13:52,840 --> 00:13:54,840
 It's a perfect teaching.

321
00:13:54,840 --> 00:13:56,840
 It's a perfect teaching.

322
00:13:56,840 --> 00:13:58,840
 It's a perfect teaching.

323
00:13:58,840 --> 00:14:00,840
 It's a perfect teaching.

324
00:14:00,840 --> 00:14:02,840
 It's a perfect teaching.

325
00:14:02,840 --> 00:14:04,840
 It's a perfect teaching.

326
00:14:04,840 --> 00:14:06,840
 It's a perfect teaching.

327
00:14:06,840 --> 00:14:08,840
 It's a perfect teaching.

328
00:14:08,840 --> 00:14:10,840
 It's a perfect teaching.

329
00:14:10,840 --> 00:14:12,840
 It's a perfect teaching.

330
00:14:12,840 --> 00:14:14,840
 It's a perfect teaching.

331
00:14:14,840 --> 00:14:16,840
 It's a perfect teaching.

332
00:14:16,840 --> 00:14:18,840
 It's a perfect teaching.

333
00:14:18,840 --> 00:14:20,840
 It's a perfect teaching.

334
00:14:20,840 --> 00:14:22,840
 It's a perfect teaching.

335
00:14:22,840 --> 00:14:24,840
 It's a perfect teaching.

336
00:14:24,840 --> 00:14:26,840
 It's a perfect teaching.

337
00:14:26,840 --> 00:14:28,840
 It's a perfect teaching.

338
00:14:28,840 --> 00:14:30,840
 It's a perfect teaching.

339
00:14:30,840 --> 00:14:32,840
 It's a perfect teaching.

340
00:14:32,840 --> 00:14:34,840
 It's a perfect teaching.

341
00:14:34,840 --> 00:14:36,840
 It's a perfect teaching.

342
00:14:36,840 --> 00:14:38,840
 It's a perfect teaching.

343
00:14:38,840 --> 00:14:40,840
 It's a perfect teaching.

344
00:14:40,840 --> 00:14:42,840
 It's a perfect teaching.

345
00:14:42,840 --> 00:14:44,840
 It's a perfect teaching.

346
00:14:44,840 --> 00:14:46,840
 It's a perfect teaching.

347
00:14:46,840 --> 00:14:48,840
 It's a perfect teaching.

348
00:14:48,840 --> 00:14:50,840
 It's a perfect teaching.

349
00:14:50,840 --> 00:14:52,840
 It's a perfect teaching.

350
00:14:52,840 --> 00:14:55,840
 Most people would argue that it's not attainable, okay?

351
00:14:55,840 --> 00:14:57,840
 We have a belief that it's attainable.

352
00:14:57,840 --> 00:14:59,840
 That's our belief.

353
00:14:59,840 --> 00:15:01,840
 It's a claim.

354
00:15:01,840 --> 00:15:03,840
 You want to find out,

355
00:15:03,840 --> 00:15:06,840
 well, here's eight things that you should do.

356
00:15:06,840 --> 00:15:09,840
 Cultivate these eight things.

357
00:15:09,840 --> 00:15:14,840
 You can see for yourself,

358
00:15:14,840 --> 00:15:16,840
 whether there is such a thing as

359
00:15:16,840 --> 00:15:19,840
 freedom from suffering, cessation of suffering.

360
00:15:19,840 --> 00:15:22,840
 You agree?

361
00:15:22,840 --> 00:15:26,840
 That's the dhamma for tonight.

362
00:15:26,840 --> 00:15:32,840
 And Vanessa's back from Austria.

363
00:15:32,840 --> 00:15:41,840
 Vanessa's back.

364
00:15:41,840 --> 00:15:43,840
 Robin left.

365
00:15:46,840 --> 00:15:50,840
 More people coming? I don't know.

366
00:15:50,840 --> 00:15:55,840
 We have room if anyone wants to come and do a course here.

367
00:15:55,840 --> 00:16:00,840
 We also have room on the meeting pages.

368
00:16:00,840 --> 00:16:08,840
 If anyone wants to do an online course,

369
00:16:08,840 --> 00:16:11,840
 which is really a great way to start.

370
00:16:11,840 --> 00:16:20,840
 If you want to learn how to meditate,

371
00:16:20,840 --> 00:16:26,840
 if you want to start, but coming to do an intensive course

372
00:16:26,840 --> 00:16:31,290
 seems too daunting, or you don't have the ability to come

373
00:16:31,290 --> 00:16:32,840
 out here,

374
00:16:32,840 --> 00:16:35,840
 do an online course. It's a good way to start.

375
00:16:35,840 --> 00:16:46,840
 We may get a basic understanding of the teaching.

376
00:16:46,840 --> 00:16:56,840
 Anyway, that's all for tonight.

377
00:16:56,840 --> 00:17:00,530
 If anyone has questions, you're welcome to join the hangout

378
00:17:00,530 --> 00:17:00,840
.

379
00:17:01,840 --> 00:17:07,840
 Otherwise, we have someone on the hangout.

380
00:17:07,840 --> 00:17:12,840
 Someone with the name of Affixient.

381
00:17:12,840 --> 00:17:17,840
 Hello, do you have a question?

382
00:17:17,840 --> 00:17:20,840
 Your mic is muted.

383
00:17:20,840 --> 00:17:29,840
 You have to unmute yourself.

384
00:17:29,840 --> 00:17:38,840
 I can't hear you or see you.

385
00:17:38,840 --> 00:17:44,840
 You can go.

386
00:17:44,840 --> 00:17:47,840
 We'll meet tomorrow.

387
00:17:47,840 --> 00:17:51,840
 What's tomorrow, Friday? We'll meet tomorrow at five.

388
00:17:51,840 --> 00:17:55,840
 I should give you the first paper. You have ten days?

389
00:17:55,840 --> 00:17:59,840
 I have more than ten days.

390
00:17:59,840 --> 00:18:02,840
 I think twenty days.

391
00:18:02,840 --> 00:18:04,840
 Good.

392
00:18:04,840 --> 00:18:08,840
 You've done these before, no?

393
00:18:08,840 --> 00:18:10,840
 You did these last time you were here, right?

394
00:18:10,840 --> 00:18:13,840
 Yeah.

395
00:18:23,840 --> 00:18:25,840
 What happened to your foot?

396
00:18:25,840 --> 00:18:27,840
 Yeah, I sure did.

397
00:18:27,840 --> 00:18:31,840
 It's a little bit difficult to sit with. It's okay.

398
00:18:31,840 --> 00:18:33,840
 But it's a bit better.

399
00:18:33,840 --> 00:18:35,840
 How about walking? Walking is okay?

400
00:18:35,840 --> 00:18:41,560
 Yeah. I can do it completely, but I don't know if it's okay

401
00:18:41,560 --> 00:18:41,840
.

402
00:18:41,840 --> 00:18:45,840
 Don't strain it. Do what you can.

403
00:18:45,840 --> 00:18:48,840
 I'm not sure. I'm not really okay.

404
00:18:48,840 --> 00:18:50,840
 Okay.

405
00:18:50,840 --> 00:18:53,840
 Okay.

406
00:18:53,840 --> 00:18:56,840
 And, yeah.

407
00:18:56,840 --> 00:19:06,050
 Also, the kind of, it's so like, over how many come back

408
00:19:06,050 --> 00:19:06,840
 here?

409
00:19:06,840 --> 00:19:09,840
 There's like so many things.

410
00:19:09,840 --> 00:19:13,840
 How are things back home?

411
00:19:13,840 --> 00:19:25,620
 Well, I think it would kind of hard actually, I can really

412
00:19:25,620 --> 00:19:26,840
 try to work with it.

413
00:19:26,840 --> 00:19:28,840
 Yeah.

414
00:19:28,840 --> 00:19:34,840
 And it's, I don't know, like, I don't know if I really,

415
00:19:34,840 --> 00:19:38,670
 this maybe you have like, most of them it's so scary when I

416
00:19:38,670 --> 00:19:39,840
 don't have to do anything.

417
00:19:39,840 --> 00:19:42,930
 I don't know what I just, like, what I can, like, I just

418
00:19:42,930 --> 00:19:45,840
 wanted to sleep and I didn't want to go out.

419
00:19:45,840 --> 00:19:51,540
 Yeah, yeah. My family was the same. They called me a zombie

420
00:19:51,540 --> 00:19:51,840
.

421
00:19:51,840 --> 00:19:55,840
 Yeah, me too.

422
00:19:55,840 --> 00:19:58,840
 Oh, wow.

423
00:19:58,840 --> 00:20:03,300
 And, but you, did you lose your job? Did that go, no? You

424
00:20:03,300 --> 00:20:04,840
're okay?

425
00:20:04,840 --> 00:20:09,840
 Yeah.

426
00:20:09,840 --> 00:20:20,430
 I feel like I have to be like strong, you know, like, kind

427
00:20:20,430 --> 00:20:23,450
 of like, my definition, but like, you know, in the thunder

428
00:20:23,450 --> 00:20:23,840
storm, maybe.

429
00:20:23,840 --> 00:20:26,840
 I have to be alone.

430
00:20:26,840 --> 00:20:29,840
 In 10 hours.

431
00:20:29,840 --> 00:20:34,840
 Yeah, that's, you think of it as a duty.

432
00:20:34,840 --> 00:20:39,170
 If I don't do it, I don't eat. If I don't, I don't live. So

433
00:20:39,170 --> 00:20:40,840
 you do it.

434
00:20:40,840 --> 00:20:44,000
 Just don't, don't, you know, remember, it's all about our

435
00:20:44,000 --> 00:20:44,840
 reactions.

436
00:20:44,840 --> 00:20:47,840
 I'm not so concerned about our situation.

437
00:20:47,840 --> 00:20:51,140
 Yeah, I mean, yeah, you can't be as mindful and you're

438
00:20:51,140 --> 00:20:53,840
 working many hours a day, but

439
00:20:53,840 --> 00:20:59,840
 take time after work, between, you know, at lunch and so on

440
00:20:59,840 --> 00:21:04,840
, to re-center yourself, get yourself more mindful.

441
00:21:04,840 --> 00:21:08,480
 And eventually you'll just shave off your hair and become a

442
00:21:08,480 --> 00:21:08,840
 monk.

443
00:21:08,840 --> 00:21:11,840
 I didn't know that.

444
00:21:11,840 --> 00:21:14,840
 Maybe not this life, but

445
00:21:14,840 --> 00:21:21,840
 hey, if you want to this life, you know, I'll go with you.

446
00:21:21,840 --> 00:21:26,840
 Yeah, it's like, it's the first time you're going to touch

447
00:21:26,840 --> 00:21:29,840
 the, like, similar normal life.

448
00:21:29,840 --> 00:21:35,140
 And then at the same time, I'm already laying in there, so

449
00:21:35,140 --> 00:21:37,840
 it's strange.

450
00:21:37,840 --> 00:21:41,200
 Take your time. You don't, it'll come by itself. Mind

451
00:21:41,200 --> 00:21:44,840
fulness is, stay on the path.

452
00:21:44,840 --> 00:21:47,840
 Everything else is just details.

453
00:21:47,840 --> 00:21:51,840
 I'm really lucky because...

454
00:21:51,840 --> 00:21:53,840
 I'm glad to have you.

455
00:21:53,840 --> 00:21:59,840
 It gets so entangled sometimes, like, you know,

456
00:21:59,840 --> 00:22:06,950
 the latest games, it's like, you know, like, trying to make

457
00:22:06,950 --> 00:22:10,600
 enough pressure so that all people keep working more and

458
00:22:10,600 --> 00:22:12,840
 more, and things like that.

459
00:22:12,840 --> 00:22:20,580
 And I really, I like them all, because I see that they're

460
00:22:20,580 --> 00:22:23,840
 not, I see that they're all suffering and everything.

461
00:22:23,840 --> 00:22:25,840
 It's true.

462
00:22:25,840 --> 00:22:30,930
 I noticed that still I can't really stay that conscious,

463
00:22:30,930 --> 00:22:33,840
 like, in the evening when I come home, like, a dead fly.

464
00:22:33,840 --> 00:22:35,840
 Mm-hmm.

465
00:22:35,840 --> 00:22:42,840
 But, I'm...

466
00:22:42,840 --> 00:22:52,840
 Do what you can. Do what you can with what you've got.

467
00:22:52,840 --> 00:22:56,060
 But that's the thing, you know, if you stay in the world,

468
00:22:56,060 --> 00:22:57,840
 you've got to deal with that.

469
00:22:57,840 --> 00:23:02,280
 And you're lucky some people have really hard, right? Some

470
00:23:02,280 --> 00:23:06,840
 countries, some places, it's quite scary, actually.

471
00:23:06,840 --> 00:23:11,840
 Very little opportunity for reflection and mindfulness.

472
00:23:11,840 --> 00:23:15,840
 That's true.

473
00:23:15,840 --> 00:23:17,840
 No.

474
00:23:17,840 --> 00:23:29,840
 The, what do they call it? You have to...

475
00:23:29,840 --> 00:23:33,840
 I'm going to give the word. It's not perfect.

476
00:23:33,840 --> 00:23:37,340
 No. You live in the world, so you can't be mindful all the

477
00:23:37,340 --> 00:23:37,840
 time.

478
00:23:37,840 --> 00:23:41,840
 You have to work, you have to interact.

479
00:23:41,840 --> 00:23:43,840
 I'm glad you're here.

480
00:23:43,840 --> 00:23:47,840
 Get started. We'll talk again tomorrow at five.

481
00:23:47,840 --> 00:23:49,840
 Okay? Get settled in.

482
00:23:49,840 --> 00:23:53,280
 You ready to start, or you want to start tomorrow, or are

483
00:23:53,280 --> 00:23:54,840
 you okay to start now?

484
00:23:54,840 --> 00:23:57,700
 You've got time. You don't have to start tonight if you

485
00:23:57,700 --> 00:23:59,840
 want to take it easy in the beginning.

486
00:23:59,840 --> 00:24:01,840
 I'll see. I'll go to the...

487
00:24:01,840 --> 00:24:04,580
 See how it goes. You're still jet-lagged, I guess. You flew

488
00:24:04,580 --> 00:24:05,840
 today, no?

489
00:24:05,840 --> 00:24:08,470
 You've got, you have lots of time. If you don't want to

490
00:24:08,470 --> 00:24:09,840
 start today, you can start tomorrow.

491
00:24:09,840 --> 00:24:15,420
 You can meet the next day. We can meet tomorrow, but if you

492
00:24:15,420 --> 00:24:16,840
 don't want to switch, we can...

493
00:24:16,840 --> 00:24:20,310
 You don't do enough practice on this one. We'll meet

494
00:24:20,310 --> 00:24:21,840
 tomorrow at five, either way.

495
00:24:21,840 --> 00:24:23,840
 I will.

496
00:24:23,840 --> 00:24:25,840
 You see how it goes.

497
00:24:25,840 --> 00:24:27,840
 I will see, because I'm very...

498
00:24:27,840 --> 00:24:29,840
 You're ready?

499
00:24:29,840 --> 00:24:33,540
 I might, yeah. I may go to sleep soon, but there's a lot of

500
00:24:33,540 --> 00:24:33,840
 it.

501
00:24:33,840 --> 00:24:35,840
 Good.

502
00:24:35,840 --> 00:24:39,840
 I don't really... something like pre-work...

503
00:24:39,840 --> 00:24:42,840
 Yeah, yeah, go ahead. You know where food is, everything?

504
00:24:42,840 --> 00:24:43,840
 It's all...

505
00:24:43,840 --> 00:24:45,840
 Oh, thank you.

506
00:24:45,840 --> 00:24:47,840
 Lots of food. If you need anything special, let us know.

507
00:24:47,840 --> 00:24:49,840
 If there's anything missing in the food or something.

508
00:24:49,840 --> 00:24:53,220
 No, that is just what I heard. You know, like, when they

509
00:24:53,220 --> 00:24:55,840
 just tell me when there's something to do, like...

510
00:24:55,840 --> 00:24:58,840
 I know what came and what and whatever...

511
00:24:58,840 --> 00:25:01,840
 Yeah, there's really not. I'm...

512
00:25:01,840 --> 00:25:06,430
 I don't really know. There's not much. Just... Yeah, it's

513
00:25:06,430 --> 00:25:07,840
 good if you make sure, like, if something needs...

514
00:25:07,840 --> 00:25:10,320
 If you see something's dirty or needs sweeping or so on,

515
00:25:10,320 --> 00:25:11,840
 great. If you have time...

516
00:25:11,840 --> 00:25:15,100
 If you have time every day to do a little bit of sweeping

517
00:25:15,100 --> 00:25:17,840
 or a little bit of something, it's good.

518
00:25:17,840 --> 00:25:23,840
 And if there's something you just...

519
00:25:23,840 --> 00:25:27,840
 We're getting a blender. So you can make...

520
00:25:27,840 --> 00:25:31,840
 There's frozen fruit. You can make smoothies if you want.

521
00:25:31,840 --> 00:25:37,460
 Frozen fruit's apparently easiest because it doesn't go bad

522
00:25:37,460 --> 00:25:37,840
.

523
00:25:37,840 --> 00:25:41,160
 So there's... Robin brought frozen fruit. When we get the

524
00:25:41,160 --> 00:25:44,430
 thing, you can... I don't know how it works, but it just

525
00:25:44,430 --> 00:25:45,840
 makes me...

526
00:25:45,840 --> 00:25:51,840
 Okay, good.

527
00:25:51,840 --> 00:25:54,840
 All right, have a good night.

528
00:25:54,840 --> 00:25:56,840
 See you tomorrow.

529
00:25:58,840 --> 00:26:00,840
 All right.

530
00:26:00,840 --> 00:26:05,850
 Try to figure out what's going on here. I've had, like,

531
00:26:05,850 --> 00:26:08,840
 three people trying to call me.

532
00:26:08,840 --> 00:26:13,650
 Is this people actually trying to get... Or is this just a

533
00:26:13,650 --> 00:26:15,840
 malfunction of my phone?

534
00:26:15,840 --> 00:26:18,590
 People actually trying to get into the... If you're trying

535
00:26:18,590 --> 00:26:20,780
 to get into the hangout, if you're watching this and

536
00:26:20,780 --> 00:26:22,840
 thinking, "How can I join? How can I join?"

537
00:26:22,840 --> 00:26:26,840
 You have to click on the link. There's a link.

538
00:26:26,840 --> 00:26:29,840
 Really? I also... I tried this one.

539
00:26:29,840 --> 00:26:33,330
 No, I'm talking to people actually on the internet right

540
00:26:33,330 --> 00:26:34,840
 now. There's people watching this.

541
00:26:34,840 --> 00:26:35,840
 Oh, sorry.

542
00:26:35,840 --> 00:26:36,840
 It's okay.

543
00:26:36,840 --> 00:26:37,840
 Okay.

544
00:26:37,840 --> 00:26:40,580
 You don't have to pay attention to this. But there's people

545
00:26:40,580 --> 00:26:41,840
, I think, trying to get on.

546
00:26:41,840 --> 00:26:44,950
 If you want to get on, you have to go to meditation.siri-m

547
00:26:44,950 --> 00:26:49,840
ongolo.org and click on the link. It's the only way.

548
00:26:49,840 --> 00:26:51,840
 Good night.

549
00:26:52,840 --> 00:26:53,840
 Good night.

550
00:26:53,840 --> 00:27:06,840
 And since no one's joining except for this mute and...

551
00:27:06,840 --> 00:27:14,910
 mysterious, pictureless person, I think we're going to say

552
00:27:14,910 --> 00:27:16,840
 good night.

553
00:27:20,840 --> 00:27:24,840
 If you were trying to get on, try again tomorrow.

554
00:27:24,840 --> 00:27:27,840
 Good night, everyone.

555
00:27:27,840 --> 00:27:28,840
 Thank you.

556
00:27:28,840 --> 00:27:29,840
 Thank you.

557
00:27:29,840 --> 00:27:30,840
 Thank you.

558
00:27:30,840 --> 00:27:31,840
 Thank you.

559
00:27:31,840 --> 00:27:32,840
 Thank you.

560
00:27:33,840 --> 00:27:34,840
 Thank you.

561
00:27:35,840 --> 00:27:36,840
 Thank you.

562
00:27:38,840 --> 00:27:39,840
 Thank you.

563
00:27:40,840 --> 00:27:41,840
 Thank you.

564
00:27:42,840 --> 00:27:43,840
 Thank you.

565
00:27:44,840 --> 00:27:45,840
 Thank you.

