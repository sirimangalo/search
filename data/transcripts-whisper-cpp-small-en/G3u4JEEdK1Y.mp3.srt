1
00:00:00,000 --> 00:00:04,560
 Hi, welcome back to Ask a Monk. Today's question comes from

2
00:00:04,560 --> 00:00:10,000
 BD 951, who says, "Can you explain

3
00:00:10,000 --> 00:00:13,770
 what an insight is in regards to meditation? Is it a

4
00:00:13,770 --> 00:00:15,400
 thought that arises that causes you

5
00:00:15,400 --> 00:00:19,640
 to let go, or does it occur at a deeper level? Also, when a

6
00:00:19,640 --> 00:00:21,920
 person reaches sotapana state

7
00:00:21,920 --> 00:00:25,120
 of mind, is that a distinct event in time?"

8
00:00:25,120 --> 00:00:33,770
 Okay, first question, now what is an insight? Well, there

9
00:00:33,770 --> 00:00:37,320
 are two levels of insight, the

10
00:00:37,320 --> 00:00:44,940
 first one being the thought level, where one has a sort of

11
00:00:44,940 --> 00:00:49,160
 epiphany, a realization on an

12
00:00:49,160 --> 00:00:54,070
 intellectual level, and the second type is a meditative

13
00:00:54,070 --> 00:00:56,800
 experience of reality for what

14
00:00:56,800 --> 00:01:01,440
 it is, a realization of the way things are based on

15
00:01:01,440 --> 00:01:04,440
 meditation practice. It's the second

16
00:01:04,440 --> 00:01:09,880
 type that is really, in answer to your question, in regards

17
00:01:09,880 --> 00:01:12,920
 to meditation practice, it's an

18
00:01:12,920 --> 00:01:17,410
 experience. It's not an intellectual thought, there is no

19
00:01:17,410 --> 00:01:19,920
 need for thought because one sees

20
00:01:19,920 --> 00:01:26,160
 things as they are. I can go briefly through the insights.

21
00:01:26,160 --> 00:01:27,720
 There are 16 insights that one

22
00:01:27,720 --> 00:01:31,350
 realizes in the practice of the meditation. If one does an

23
00:01:31,350 --> 00:01:32,960
 intensive course, it can be

24
00:01:32,960 --> 00:01:37,210
 expected that you get through several of these, and if the

25
00:01:37,210 --> 00:01:40,240
 course is long enough and comprehensive

26
00:01:40,240 --> 00:01:44,590
 enough, you can possibly go through all 16. The first one

27
00:01:44,590 --> 00:01:46,080
 is called Namarupa Parisi and

28
00:01:46,080 --> 00:01:49,680
 Tayana, which is the insight into the separation between

29
00:01:49,680 --> 00:01:51,920
 body and mind, that all beings are

30
00:01:51,920 --> 00:01:54,320
 separated into two parts, one being the physical and the

31
00:01:54,320 --> 00:01:55,880
 other being the mental, that there's

32
00:01:55,880 --> 00:02:00,250
 no self, no soul, but only these two realities of the

33
00:02:00,250 --> 00:02:03,320
 physical and mental phenomena that

34
00:02:03,320 --> 00:02:07,050
 arise. Number two is realizing the nature of these two

35
00:02:07,050 --> 00:02:08,520
 things, how they work together,

36
00:02:08,520 --> 00:02:13,700
 how they work in terms of cause and effect, the knowledge

37
00:02:13,700 --> 00:02:15,880
 of cause and effect. Number

38
00:02:15,880 --> 00:02:19,740
 three is called Samasana Yana, seeing things as they are,

39
00:02:19,740 --> 00:02:22,280
 knowledge of comprehension, I

40
00:02:22,280 --> 00:02:26,100
 believe it's translated as, starting to see impermanent

41
00:02:26,100 --> 00:02:28,360
 suffering and non-self, the three

42
00:02:28,360 --> 00:02:32,320
 characteristics inherent in everything, why nothing in the

43
00:02:32,320 --> 00:02:34,320
 world is worth clinging to,

44
00:02:34,320 --> 00:02:36,720
 starting to see that the things that we thought were

45
00:02:36,720 --> 00:02:38,640
 permanent or impermanent, the things

46
00:02:38,640 --> 00:02:42,640
 that we thought were satisfying, can't satisfy us because

47
00:02:42,640 --> 00:02:44,840
 they're impermanent, and that they're

48
00:02:44,840 --> 00:02:48,920
 not self, they're not under our control, and we can't force

49
00:02:48,920 --> 00:02:51,880
 reality to be this way or that,

50
00:02:51,880 --> 00:02:56,630
 as we might wish. Number four is Utayyabaya Yana, knowledge

51
00:02:56,630 --> 00:02:58,520
 of arising and ceasing, and

52
00:02:58,520 --> 00:03:01,630
 this is starting to really be able to break reality down

53
00:03:01,630 --> 00:03:03,560
 into the arising and ceasing.

54
00:03:03,560 --> 00:03:06,490
 So rather than seeing things as good and bad and so on, we

55
00:03:06,490 --> 00:03:08,160
 simply see them as arising and

56
00:03:08,160 --> 00:03:11,670
 ceasing, coming and going. Number five is knowledge of

57
00:03:11,670 --> 00:03:13,120
 cessation, so starting to see

58
00:03:13,120 --> 00:03:17,110
 that everything ceases, as opposed to watching things arise

59
00:03:17,110 --> 00:03:19,080
 and cease, starting to really

60
00:03:19,080 --> 00:03:23,560
 see that there's nothing in the world that's permanent and

61
00:03:23,560 --> 00:03:25,760
 starting to accept the fact

62
00:03:25,760 --> 00:03:30,330
 that everything has to cease. Number six is knowledge of

63
00:03:30,330 --> 00:03:32,760
 the danger or fierceness by Yayana,

64
00:03:34,520 --> 00:03:37,510
 which starts to realize that there's a problem here, that

65
00:03:37,510 --> 00:03:40,520
 if everything ceases, then really

66
00:03:40,520 --> 00:03:44,400
 we can't cling. Number seven is knowledge of the

67
00:03:44,400 --> 00:03:47,560
 disadvantages of clinging, starting

68
00:03:47,560 --> 00:03:52,000
 to see the suffering that's caused by it. Number eight is

69
00:03:52,000 --> 00:03:54,240
 knowledge of disenchantment

70
00:03:54,240 --> 00:03:57,950
 or dispassion, where nibhita yana, where one becomes dis

71
00:03:57,950 --> 00:04:00,520
enchanted with the reality, starting

72
00:04:00,520 --> 00:04:04,230
 to realize that it's only the same old things over and over

73
00:04:04,230 --> 00:04:06,280
 again. There's nothing in the

74
00:04:06,280 --> 00:04:09,650
 world which is really unique, exceptional, special in any

75
00:04:09,650 --> 00:04:11,480
 way. In the end, it's all just

76
00:04:11,480 --> 00:04:14,200
 seeing, hearing, smelling, tasting, feeling and thinking.

77
00:04:14,200 --> 00:04:15,440
 And this is starting to turn

78
00:04:15,440 --> 00:04:19,430
 away and realize that happiness doesn't lie in the

79
00:04:19,430 --> 00:04:22,120
 experiences that we have. Number nine

80
00:04:22,120 --> 00:04:26,580
 is munti tukamayatayana, knowledge of desire for release or

81
00:04:26,580 --> 00:04:29,120
 freedom, wanting to be free,

82
00:04:29,120 --> 00:04:33,210
 wanting to get away and starting to actually turn away in

83
00:04:33,210 --> 00:04:36,120
 another direction and seek happiness,

84
00:04:36,120 --> 00:04:41,220
 seek peace inside, no longer looking into external objects

85
00:04:41,220 --> 00:04:43,560
 for peace and happiness.

86
00:04:43,560 --> 00:04:48,250
 Number ten is following this idea and actually looking

87
00:04:48,250 --> 00:04:51,800
 inward. Bhattisankayana means going

88
00:04:51,800 --> 00:04:56,170
 over everything that you've been watching in the meditation

89
00:04:56,170 --> 00:04:58,300
 again, and this time giving

90
00:04:58,300 --> 00:05:02,530
 it a closer look, trying to see where exactly you're

91
00:05:02,530 --> 00:05:05,300
 clinging to rather than where you can

92
00:05:05,300 --> 00:05:12,260
 find happiness. Number eleven is sankarubhika yana,

93
00:05:12,260 --> 00:05:13,240
 knowledge of equanimity. Once you change

94
00:05:13,240 --> 00:05:17,640
 the way you look at things, instead of trying to find

95
00:05:17,640 --> 00:05:20,800
 happiness in the reality, starting

96
00:05:20,800 --> 00:05:23,410
 to see where you're clinging, you're able to work out the

97
00:05:23,410 --> 00:05:25,240
 clinging, work out your attachments,

98
00:05:25,240 --> 00:05:29,590
 work out the kinks in the mind, and then your mind starts

99
00:05:29,590 --> 00:05:32,240
 to calm down and feel equanimous

100
00:05:32,240 --> 00:05:38,090
 in regards to all things. It sees everything equally and is

101
00:05:38,090 --> 00:05:40,180
 very finely tuned. And this

102
00:05:40,180 --> 00:05:45,740
 is the height of insight in a worldly sense. After sankarub

103
00:05:45,740 --> 00:05:47,180
hika yana there arises several

104
00:05:49,760 --> 00:05:54,290
 knowledges in succession. Next one is anuloma yana, where

105
00:05:54,290 --> 00:05:56,760
 the mind gets to the finest point.

106
00:05:56,760 --> 00:06:01,770
 It's like the consummation of insight where it's so finely

107
00:06:01,770 --> 00:06:04,000
 tuned that you're able to see

108
00:06:04,000 --> 00:06:07,200
 things exactly as there are. This is really the insight

109
00:06:07,200 --> 00:06:09,080
 that we're going for. Anuloma

110
00:06:09,080 --> 00:06:13,530
 yana means to come to the middle or to finally get it and

111
00:06:13,530 --> 00:06:16,080
 go with the grain as opposed to

112
00:06:19,200 --> 00:06:23,270
 going against the grain, that we're finally going with the

113
00:06:23,270 --> 00:06:25,320
 stream of reality and in tune

114
00:06:25,320 --> 00:06:29,230
 with the way things are. And that's just a very brief

115
00:06:29,230 --> 00:06:31,120
 moment in time. The next one is

116
00:06:31,120 --> 00:06:35,810
 ghotrubhayana, where one changes one's lineage, they say.

117
00:06:35,810 --> 00:06:38,120
 It means changes one's state of

118
00:06:38,120 --> 00:06:43,800
 mind from being an ordinary human being to being one who

119
00:06:43,800 --> 00:06:46,560
 realizes the truth. The next

120
00:06:46,560 --> 00:06:50,700
 moment is called magayana, where one attains the path,

121
00:06:50,700 --> 00:06:53,560
 where one's mind actually goes inward,

122
00:06:53,560 --> 00:06:58,390
 not following a path outward. But magayana is the path

123
00:06:58,390 --> 00:07:01,120
 inward, where the mind enters

124
00:07:01,120 --> 00:07:04,800
 onto the path to enlightenment just for a moment. Then

125
00:07:04,800 --> 00:07:05,800
 there's pala yana, which is the

126
00:07:05,800 --> 00:07:10,150
 realization of the fruit of the path, where one sees,

127
00:07:10,150 --> 00:07:12,800
 realizes nirvana or nirvana for the

128
00:07:14,160 --> 00:07:19,920
 first time. And then number 16 is called bhajawika nayana,

129
00:07:19,920 --> 00:07:21,160
 where one reflects upon this, comes

130
00:07:21,160 --> 00:07:26,000
 out with the realization of nirvana and starts to reflect

131
00:07:26,000 --> 00:07:28,760
 on the results, seeing what has

132
00:07:28,760 --> 00:07:34,010
 changed in the mind as a result of realizing nirvana. So

133
00:07:34,010 --> 00:07:36,240
 the second question, the realization

134
00:07:36,240 --> 00:07:40,410
 of sottabhana. Sottabhana is the moment of realizing magay

135
00:07:40,410 --> 00:07:43,160
ana, the 14th stage of insight.

136
00:07:43,160 --> 00:07:46,960
 Once one attains that state, one is said to be the first

137
00:07:46,960 --> 00:07:49,240
 type of holy person, the first

138
00:07:49,240 --> 00:07:53,720
 type of enlightened being, which is the sottapati maga yana

139
00:07:53,720 --> 00:07:56,240
, the realization of the path of sottapana.

140
00:07:56,240 --> 00:08:00,750
 Then the next moment, when one attains pala yana, one is

141
00:08:00,750 --> 00:08:03,440
 the second type of holy person,

142
00:08:03,440 --> 00:08:08,880
 which is one who has realized sottapati pala or fruition.

143
00:08:08,880 --> 00:08:10,280
 From that point on, one is considered

144
00:08:10,280 --> 00:08:14,610
 to be a sottapana, one who has entered the stream. Because

145
00:08:14,610 --> 00:08:16,760
 that realization has fundamentally

146
00:08:16,760 --> 00:08:20,540
 changed the way the person looks at the world. There's no

147
00:08:20,540 --> 00:08:22,640
 going back to an ordinary state

148
00:08:22,640 --> 00:08:26,210
 of being. So one is considered to have entered the stream,

149
00:08:26,210 --> 00:08:27,960
 so to speak, and will be born

150
00:08:27,960 --> 00:08:31,460
 a maximum of seven more lifetimes. So this is pretty

151
00:08:31,460 --> 00:08:33,800
 technical and very Buddhist. And

152
00:08:33,800 --> 00:08:39,440
 I don't always go into things on such a technical level in

153
00:08:39,440 --> 00:08:40,800
 my videos. But okay, so thanks for

154
00:08:40,800 --> 00:08:45,820
 the opportunity. And there it is. Those are the 16 stages

155
00:08:45,820 --> 00:08:48,440
 of knowledge, Reader's Digest

156
00:08:48,440 --> 00:08:48,840
 version.

157
00:08:48,840 --> 00:08:55,840
 [

