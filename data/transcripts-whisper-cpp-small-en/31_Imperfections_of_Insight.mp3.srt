1
00:00:00,000 --> 00:00:05,160
 imperfections of insight. The following is condensed from

2
00:00:05,160 --> 00:00:06,320
 chapter 4,

3
00:00:06,320 --> 00:00:11,210
 imperfections of insight from how to meditate too by Vener

4
00:00:11,210 --> 00:00:12,800
able Yutadamo Bhikkhu.

5
00:00:12,800 --> 00:00:18,320
 When the meditator sees the causal relationship between the

6
00:00:18,320 --> 00:00:20,640
 body and the mind, the meditator

7
00:00:20,640 --> 00:00:25,160
 begins to naturally resist some habitual tendencies and

8
00:00:25,160 --> 00:00:27,520
 realizes that certain built-up

9
00:00:27,520 --> 00:00:31,750
 habits are a cause of great suffering to oneself and others

10
00:00:31,750 --> 00:00:34,960
. A meditator also realizes that it is

11
00:00:34,960 --> 00:00:39,010
 the reactivity to desires and aversions that is a real

12
00:00:39,010 --> 00:00:42,400
 cause for stress and suffering. It is this

13
00:00:42,400 --> 00:00:45,730
 sort of realization that leads to the next stage of

14
00:00:45,730 --> 00:00:48,400
 knowledge which is based on a deepening

15
00:00:48,400 --> 00:00:52,460
 understanding that the objects of experience are unworthy

16
00:00:52,460 --> 00:00:54,880
 of the obsession we normally give to

17
00:00:54,880 --> 00:00:58,530
 them. Before this understanding comes through fruition,

18
00:00:58,530 --> 00:01:00,720
 however, it is common for the mind to

19
00:01:00,720 --> 00:01:05,390
 rebel, recalling from the sudden onslaught of instability,

20
00:01:05,390 --> 00:01:08,480
 insipidness, and chaos that arises

21
00:01:08,480 --> 00:01:12,530
 from no longer chasing after pleasantness nor running away

22
00:01:12,530 --> 00:01:15,360
 from unpleasantness. In the beginning,

23
00:01:15,360 --> 00:01:19,140
 the experience of reality is more likely to lead one to

24
00:01:19,140 --> 00:01:22,080
 seek out an alternative to that which is

25
00:01:22,080 --> 00:01:26,170
 impermanent, stressful, and uncontrollable. As a result, a

26
00:01:26,170 --> 00:01:28,560
 new meditator will tend to cling to

27
00:01:28,560 --> 00:01:33,320
 anything that appears at first glance even remotely stable,

28
00:01:33,320 --> 00:01:35,760
 satisfying, or controllable.

29
00:01:35,760 --> 00:01:39,550
 Such objects of clinging are called the imperfections of

30
00:01:39,550 --> 00:01:43,360
 insights of which 10 are enumerated in the text.

31
00:01:44,160 --> 00:01:49,170
 One of us are illumination, experience of visions or bright

32
00:01:49,170 --> 00:01:52,960
 lights to be nurtured as seeing, seeing,

33
00:01:52,960 --> 00:01:57,140
 until they disappear or are no longer an object of interest

34
00:01:57,140 --> 00:02:01,520
 for the mind. Two, nyana, knowledge,

35
00:02:01,520 --> 00:02:06,480
 mental activity directed towards solving mundane problem in

36
00:02:06,480 --> 00:02:09,360
 one's life to be noted as thinking,

37
00:02:09,360 --> 00:02:14,590
 thinking, or knowing, knowing. Three, piti, rapture,

38
00:02:14,590 --> 00:02:17,360
 experience of ecstatic states,

39
00:02:17,360 --> 00:02:21,530
 floating, flowing energy, uncontrollable laughing, or

40
00:02:21,530 --> 00:02:24,560
 crying which should be noted objectively.

41
00:02:24,560 --> 00:02:30,640
 Four, asadi, tranquility, experience of tranquility, calm

42
00:02:30,640 --> 00:02:34,560
ness, or quietitude to be noted as quiet,

43
00:02:34,560 --> 00:02:41,300
 quiet, or calm, calm. Five, sukha, happiness, experience of

44
00:02:41,300 --> 00:02:44,160
 pleasant feeling to be noted as

45
00:02:44,160 --> 00:02:49,880
 happy, happy. Six, adimoka, resolve, experience of

46
00:02:49,880 --> 00:02:54,400
 confidence, self-assurance, or thinking oneself

47
00:02:54,400 --> 00:02:58,780
 has obtained a supermungent state of being. These various

48
00:02:58,780 --> 00:03:01,920
 states should be noted reminding oneself

49
00:03:01,920 --> 00:03:07,510
 of their various natures. Seven, pagarha, exertion,

50
00:03:07,510 --> 00:03:11,440
 experience of becoming hyper-energetic or feeling

51
00:03:11,440 --> 00:03:15,480
 as if one could practice all day and night without stopping

52
00:03:15,480 --> 00:03:18,480
. One should note energy when it becomes

53
00:03:18,480 --> 00:03:24,290
 apparent. Eight, upatana, attention, state of attentiveness

54
00:03:24,290 --> 00:03:26,880
 brought about by the constant

55
00:03:26,880 --> 00:03:31,750
 practice of mindfulness that becomes an obstacle when one

56
00:03:31,750 --> 00:03:34,560
 takes up an object of observation,

57
00:03:34,560 --> 00:03:38,460
 something that is outside the present moment. All of these

58
00:03:38,460 --> 00:03:40,480
 activities should be seen as

59
00:03:40,480 --> 00:03:45,990
 distractions from the path and noted. Nine, pupeka, equanim

60
00:03:45,990 --> 00:03:50,560
ity, experience of equanimity can also lead

61
00:03:50,560 --> 00:03:55,000
 to delusions of enlightenment as one reflects that one is

62
00:03:55,000 --> 00:03:58,400
 no longer plagued by likes or dislikes. Such

63
00:03:58,400 --> 00:04:02,230
 thoughts should be noted as they arise and one should

64
00:04:02,230 --> 00:04:07,280
 remind oneself calm, calm. Ten, kakanti,

65
00:04:07,280 --> 00:04:12,770
 desire, experience of desire arising for other items on

66
00:04:12,770 --> 00:04:16,080
 this list or for sense pleasure or for

67
00:04:16,080 --> 00:04:20,640
 insight itself to be recognized as harmful obstacles to

68
00:04:20,640 --> 00:04:24,080
 true understanding of reality and noted

69
00:04:24,080 --> 00:04:29,120
 accordingly. Meditators should take notes of the many ways

70
00:04:29,120 --> 00:04:32,240
 in which one can become distracted from

71
00:04:32,240 --> 00:04:37,050
 the path enticed into a false conception of stability,

72
00:04:37,050 --> 00:04:40,480
 satisfaction and control by the many

73
00:04:40,480 --> 00:04:44,710
 pleasant and even positive byproducts of the meditation

74
00:04:44,710 --> 00:04:47,680
 practice. Meditators must stand

75
00:04:47,680 --> 00:04:51,460
 ready to observe such experiences with a clear mind

76
00:04:51,460 --> 00:04:54,960
 reminding themselves of their true essence

77
00:04:54,960 --> 00:04:59,210
 of these experiences using simple mantras that capture

78
00:04:59,210 --> 00:05:02,160
 their nature. Once they are able to do

79
00:05:02,160 --> 00:05:05,910
 this they will be ready to enter the realm of true and

80
00:05:05,910 --> 00:05:09,440
 profound insights into the nature of reality.

81
00:05:09,440 --> 00:05:19,440
 [BLANK_AUDIO]

