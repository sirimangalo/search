 Okay, so here we are with our new live stream audio
 broadcast.
 We are sitting out by a smaller pond within what Jumtong.
 The sun is shining, the birds are singing, and it's the
 kind of setting that makes you
 think that all is right in the world.
 This is a big question that comes up in life.
 You look at the world around you, sometimes it seems like
 everything is just okay and
 everything is all right.
 You wonder why is it that with all the good that there is,
 all the seeming perfection
 there is in the world, why is it that we are still beset by
 suffering?
 When you look around we've really got everything we need.
 We have food and shelter and life shouldn't be good.
 But even when you've got all these things, why is it that
 even then we still seem to
 be beset by suffering?
 One way of answering this question, why do we suffer when
 this is the comeback that makes
 us suffer in the world?
 The Buddha's teaching on Mara.
 It's similar to the concept of Satan in Christianity.
 Mara literally, a fairly literal translation would be evil.
 In etymologically it probably comes from the root mar which
 means to die.
 So the commentaries define it as that which, mariti, that's
 what causes goodness to die,
 all good things to die, that which kills good things, that
 which ruins the good life, which
 prevents us from being happy.
 That's the thing really, you can't say that life is good,
 there's all is well.
 As we know that it's not really true, there's evil in the
 world.
 There are five kinds of evil in the world.
 This is what ruins the, ruins our happiness, ruins our
 peace.
 So there, according to the Buddha there are five kinds of
 Mara.
 If we understand these five then we're well equipped to
 meet the problem head on.
 Five kinds of evil that exist in the world are, first of
 all what we call khanda-ma.
 Khanda means the aggregates.
 So the aggregates are a type of form of evil, they spoil
 things.
 Like you might be, I might be sitting here enjoying this
 lovely morning but then suddenly
 the chair I'm sitting in starts to chafe against my rear
 end sitting in this hard plastic chair
 and so pain arises.
 And it's like just had to come and spoil the, spoil the
 mood.
 All is well until that happened.
 All is well until that happened.
 Mainly the body, but also the other kind of the other
 aggregates.
 Finding out that experience has an element of suffering in
 it.
 But I said nati khanda-ma dukha, there's no suffering like
 the aggregates.
 The body, feelings, recognitions, thoughts, consciousness,
 these are actually dukha.
 They can't be a source of refuge or peace for us.
 Because they're impermanent suffering in non-self you can't
 rely upon them, even the good ones,
 the good aspects.
 They only come in turns with the bad aspects.
 So this is the first evil that we have to deal with.
 This is really where our practice focuses on overcoming
 this.
 What do you do if you have pain from sitting too long or a
 headache or a stomach ache or
 back ache?
 What do you do when you're sick?
 What do you do when you meet with pains and aches?
 What do you do when you meet with bad thoughts and have bad
 memories, anguishing over the
 past or the future?
 This is what the meditation aims to address.
 Because when you're mindful you can't get rid of the evil
 of the body unless you're
 the evil of the aggregates, unless you just don't be born
 again.
 But until then the path out of suffering is to free
 yourself from the control, free yourself
 from the control, being controlled by these things, free
 yourself from their power.
 When pain no longer upsets you, that's freedom from the
 evil of it.
 This is the first evil.
 The second evil is called kilesa mara.
 Now this one we aim to be truly free from as soon and as
 completely as possible.
 Kilesa means defilement.
 So something that makes the mind unclean, impure.
 It's just a figurative, figure of speech refers to those
 states of mind that lead to suffering.
 They're evil.
 They're evil because they lead us to suffer.
 Hunger leads us to suffer.
 Greed leads us to suffer.
 Delusion leads us to suffer.
 And all of the many types of these defilements in all their
 many flavors.
 So these are just reactions to experience and they become
 habitual to the point where
 we react without thinking.
 We like certain things and want certain things because we
've cultivated a recognition based
 on our recognition that this is like that and based on our
 judgments, our thoughts.
 I like this or this is good.
 We cultivate the habit of liking certain things and disl
iking other things based on our aversion
 based on the pain that they caused and our conceiving of
 the pain as bad.
 Our delusion, our misunderstanding of reality is becoming
 partial to certain experiences.
 And they cultivate the habit of delusion, conceit and
 arrogance and just all around ignorance
 that leads us to misunderstand, misrepresent.
 And respond or react inappropriately.
 React in a way that leads us to suffer.
 So the meditation does away with this one completely
 because it changes the way you
 react to things.
 Instead of reacting, you simply interact.
 So seeing is just seeing and you react appropriately.
 Hearing is just hearing, smelling, tasting, feeling,
 thinking.
 No matter what you experience, it's all just one of these
 six.
 And when you see that, you react appropriately.
 Really all of our suffering and all of our defilements of
 mind are just because we don't
 see things as they are.
 If you really saw things as they were, you wouldn't react
 inappropriately because we
 want to be happy ostensibly.
 It goes to stands to reason that if we really were clear of
 mind, we would never do anything
 that caused us unhappiness.
 It's irrational.
 The third Mara is Abhisankara Mara, which refers to our
 actions, karma.
 And karma can be a nasty evil.
 It traps us.
 Good karma isn't so bad.
 Even good deeds isn't so bad.
 There's nothing practically speaking wrong with it.
 Though in the end, an enlightened being gives up even good
 karma.
 Meaning, when they do good deeds, they have no intention or
 no wish for a result.
 That's really the thing is that even good karma is
 generally done with a wish for a
 certain result.
 That's fine if your wish is to be a pure of mind and free
 from suffering.
 The reason an enlightened being has done away with this is
 because they don't need it.
 They already are it.
 But often we do good deeds wishing to become rich or
 wishing to go to heaven or so on.
 Wishing to have good friends as you do unto others as you
 would have them do unto you.
 So we're wishing for people to do good things to us and
 when they do bad things we still
 get angry.
 The biggest evil here is in regards to bad deeds.
 Words that hurt others because they come back and bite you
 in the butt.
 They come back and get you in the end.
 What goes around comes around.
 That which we perform it becomes habitual.
 It changes us to the story of this man who was very, very
 mean to an enlightened being
 and how that stuck with him and eventually he was born.
 All he would eat was his own excrement.
 Everyone thought that was such a weird story but then I
 looked it up on Wikipedia and it's
 actually a certain people actually do suffer from this
 delusion and they have only a desire
 to eat their own excrement.
 It's kind of extreme but the kind of thing that just the
 claim that this sort of thing
 is caused by the perversion of mind that comes from evil
 thoughts.
 I mean no matter what you believe it's clear to see that
 people who do nasty things become
 perverse in mind.
 There's that famous book, Crime and Punishment, which
 illustrates this well.
 You don't realize it maybe if you're around people who, you
 know what they say, people
 who are mean to their pets tend to be abusive to their
 family members as well and this kind
 of thing.
 But it changes you.
 It makes you less happy.
 It makes you more scared.
 When you're mean to others you become suspicious yourself.
 When you cheat others you don't trust others.
 When you're a mean person you tend to surround yourself
 with mean people.
 You don't like to be in the company of good people because
 they make you feel uncomfortable,
 this kind of thing.
 So it definitely changes your life when you start to do
 evil.
 There's this show that my family was watching called
 Breaking Bad.
 I don't know whether this show, I never of course watched
 it, but I wonder if you watched
 that show you'd be able to see whether it was actually
 according to reality.
 The other problem is that we watch a lot of television and
 television rarely.
 I mean of course it's all in the minds of the people who
 produce it so unless they're
 enlightened which is highly unlikely, they're probably
 going to get reality wrong, right?
 It'll make you think that it's not the way it is.
 So it's difficult for us to actually see the truth of karma
 because we have all these wrong
 ideas from media and from culture and in our even religion.
 But if we take a look at reality we can see how it's
 changing us when we do good and bad
 deeds.
 And so these are dangerous.
 It would be a cause for great suffering.
 Again meditation clarifies this.
 You don't have to get rid of good deeds.
 In fact you shouldn't.
 Good deeds are a means of keeping you from performing bad
 deeds.
 You keep your mind in a place of wishing goodness for
 others.
 And then you'll just be doing good deeds as a matter of
 course because when someone wants
 your help you have none of this selfishness that leads us
 to reject other people.
 If someone really is in need of help and you know that you
 can help them, you just do it.
 It's number three.
 The fourth evil in the world is called Machumara.
 This is the evil of death.
 Yeah, death is evil.
 It's pretty easy to understand conventionally how this is
 true because at least conventionally
 speaking it's the real showstopper in life.
 No matter what your goals are, no matter what your plans
 are, they're all going to be circumscribed
 by the fact that you have to die.
 They're all in the context of your imminent death.
 So if someone says I'm going to build a civilization and
 watch it flourish
 for the next one thousand years or the next one hundred
 years, then they're probably not
 being realistic.
 So the goodness that we can do, the great things that we
 can do are circumscribed by
 death.
 Limited by our mortality because when you die you don't
 know where you're going.
 In most cases you won't remember anything.
 You lose everything from the confusion and the limitations
 of your new existence.
 Seems like the brain limits our understanding.
 So if you're born with a brain you're going to be stuck.
 It's like being stuck in a prison.
 Death is a real problem and so we consider it evil.
 It's evil to have everything cut short.
 You don't control that.
 It's not like we say well I've had enough of this life, it
's time to die.
 Most of us are wishing we could stay on longer, have more
 time.
 More time as human beings.
 Of course there are some people, then there are those
 people who wish to die, people who
 are suicidal.
 Which on the surface seems actually kind of like you've
 beaten evil, right?
 It's no longer evil to you.
 I have no problem with death, I'm ready to die, that kind
 of thing.
 But it's not really that people who kill themselves aren't
 usually, it's not really that they
 want to die, it's that they can't stand living.
 And so it's ostensibly a good idea to just end it, right?
 Unfortunately it doesn't really end anything.
 That's the big surprise.
 In fact it makes things worse because your aversion to life
 and your inability to cope
 is going to have serious consequences when you're
 confronted with your deathbed visions.
 You're going to react strongly to whatever you experience
 because you haven't cultivated
 the ability to deal with pleasant and unpleasant
 experiences.
 So you'll cling to anything pleasant that comes, or
 anything unpleasant that comes,
 even the most coarse base visions.
 And so it means you're indiscriminate.
 This is the problem with dying and being born again, most
 people are indiscriminate and
 they'll grab at anything.
 That's quite dangerous because you don't know what that is.
 It's usually, as you can see from meditation, it usually
 starts off with the most coarse.
 It's not like you're dying, you'll be immediately presented
 by someone who says, "Hey, want
 to go to heaven?"
 It's a much more refined, quiet mind.
 The much louder minds are more likely to take your
 attention, like the ones that say, "I'm
 hungry," or "I'm thirsty," or "Oh, this looks shiny and
 nice."
 The ones that are carnal and coarse, gross.
 The fear base, the anger base, the greed base, these ones.
 You can see this from meditation.
 This isn't theoretical.
 Meditation gives you an insight into how the mind works.
 It's going to be the same when you pass away because when
 you die, similar to meditation
 in the sense that your mind is quiet.
 You're not getting the stimulus from the outside world.
 When you meditate, you see the coarse things coming up.
 Working through them is the first order of business.
 When you die, you won't have to deal with them anymore.
 At the very least, when you die, you should have a very
 refined mind and be able to discriminate
 between the various things that present themselves.
 So suicide is not such a bright move.
 It's like you're in class and you're learning all the
 things you have to learn.
 Some ways in, you say, "This class is terrible.
 This sucks.
 Just give me the exam now and let's get it over with."
 It's not a very bright move, academically speaking, because
 when you take the exam and
 you're not ready for it, your chances of passing are
 reduced to say the least.
 So that's the fourth one, the fifth evil that we have to
 cope with is actually called Devaputamara,
 which refers to the iconic satanic figure, the fallen angel
.
 Interesting in Buddhist cosmology, Mara, Mara as an angel,
 as a divine being, is actually
 in the highest of the sensual realm heavens.
 The idea being that there is some sort of structure to the
 universe beyond the human
 realm.
 So there are the four great kings that look after the
 directions and they watch over the
 earth maybe.
 All of the earth angels report to them and there's some
 kind of social structure going
 on and they report to the next level of angels and so on,
 all the way up to this highest
 level of the bureaucracy.
 At the highest level, there's the Mara's.
 I think it's the highest level.
 Yeah, Paranimita Vasavati.
 Paranimita Vasavati means the delight in the creations of
 others.
 Delight in the creations of others means they're happy that
 we're all messed up down here and
 that we're in chaos.
 They enjoy watching this.
 If you've ever, you know, in Hare Krishna, they talk about
 the Krishna Leela.
 This is all just Krishna's games.
 It's kind of the same thing.
 Krishna's maybe a Mara according to Buddhism, evil, because
 they enjoy the fact that we're
 stuck in samsara and they delight in watching us go to war
 and all sorts of things.
 Not just the bad stuff, but they just want to keep us in s
amsara.
 This is why according to the tradition, they don't like
 Buddhists very much and they weren't
 very happy about the Buddha.
 They like playing games with us and so on.
 Buddhist meditators who are trying very hard to leave sams
ara will sometimes be confronted
 by images and strange things and we often attribute that to
 these angels who are trying
 to get in our way.
 There's actually stories in the suttas, so if you believe
 it or not, it seems like there
 was the idea that this was going on like silly things.
 There was once this, the monks were meditating and this cow
 came and threatened to knock
 over their alms bowls that were all stacked up.
 When the Buddha was there and the Buddha said, "That's Mara
.
 Don't let it get to you."
 It was just a Mara giving them this vision and disturbing
 their concentration.
 You see this with monks sometimes thinking visions of
 things and you often just think
 it's their own minds creating it, but who knows?
 Sometimes they talk to, they have voices in their head.
 Just this idea of the fact that angels shouldn't be trusted
.
 For religious people, it does have a certain significance
 because people in other religions,
 of course in the time of the Buddha, there were plenty of
 these, would talk about being
 able to communicate with angels or God.
 Nowadays people say they're talking to God and so on,
 talking to angels.
 In Thai Buddhism, many people talk about being the avatar,
 so they hear from these angels
 and they have teachers who tell them this and that, tell
 them to do this and do that.
 The idea is usually they think that the only criticism they
 have, the only thing that they
 have to face is the people's disbelief in the fact that
 they're actually communicating
 with these beings.
 So as long as they can prove that they're actually
 communicating with these beings,
 then everything's all right.
 So they ask themselves, "Am I really hearing this?"
 And it's like, "Yes, I'm quite sure that this is a real
 being, it's not just my mind coming
 up with this."
 So they think, "Okay, well that's enough for me to listen
 to it."
 In fact, it's a ridiculous conclusion.
 It's the conclusion that these people seem to come to is, "
Oh, this is real, so I better
 follow what they say."
 So this teaching that Mara is the highest angel is really a
 stab at that.
 Because how do you know that these angels have your best
 interests at heart?
 How do you know they're on your side?
 Even if we accept the fact that you are actually talking to
 angels or even God, how do you
 know they're doing what's in your best interest?
 Why do Christians believe that God has their best interests
 at heart?
 It's just to say that we're not slaves or play toys, an
 experiment, depending on what
 you believe.
 Just because if you hear someone come in, if a human comes
 in and tells you something,
 do you immediately believe it because it's a human, because
 they're real and you're not
 imagining them?
 Why would you do the same?
 Why would you think any differently for angels?
 Obviously, there's more power up in heaven, but they say
 about power.
 So that's the fifth Mara that we have to contend with.
 If you hear anything or see anything or think you're in
 communication with higher beings,
 just understand that the world is full of evil, all realms,
 all dimensions.
 Everywhere there's good and evil.
 We talk about things like evil and suffering because that's
 the problem.
 There's a lot of good in the world as well, even the human
 world.
 We shouldn't get down on ourselves or down on the world and
 think it's full of evil either.
 So once we've understood what is the true evil, then we can
 deal with it.
 Once that's gone, then no problem.
 So that's the live Dhamma for today.
 Thank you all for tuning in and have a great meditation.
 You're welcome.
