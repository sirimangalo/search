WEBVTT

00:00:00.000 --> 00:00:07.200
 Okay. Hello everyone. Welcome back to our question and

00:00:07.200 --> 00:00:12.840
 answer series. Today's question is in

00:00:12.840 --> 00:00:19.070
 regards to peaceful meditation practice. Restful was the

00:00:19.070 --> 00:00:23.760
 word that was used in the question.

00:00:23.760 --> 00:00:31.130
 So this person describes the state of practice that they're

00:00:31.130 --> 00:00:37.920
 going through that is effortful.

00:00:37.920 --> 00:00:42.250
 It takes a lot of effort for them to be mindful. They'd

00:00:42.250 --> 00:00:45.160
 like to be mindful throughout the day,

00:00:45.160 --> 00:00:52.060
 but it's quite tiresome. Being mindful is difficult,

00:00:52.060 --> 00:00:55.720
 basically. They look at these

00:00:55.720 --> 00:01:04.620
 meditation masters whose demeanor appears quite peaceful,

00:01:04.620 --> 00:01:08.080
 and it appears that they're able to

00:01:08.080 --> 00:01:16.760
 stay mindful quite easily. So the question is, how are they

00:01:16.760 --> 00:01:20.800
 able to do that? How do we know that

00:01:20.800 --> 00:01:26.200
 we're on the right path towards that? So there's somewhat a

00:01:26.200 --> 00:01:35.120
 sense of discouragement. That's a word.

00:01:35.120 --> 00:01:46.280
 Feeling discouraged, I think. This in some sense relates to

00:01:46.280 --> 00:01:53.160
 the last question about meditation

00:01:53.160 --> 00:01:59.040
 being unpleasant. And so part of the answer is in regards

00:01:59.040 --> 00:02:03.200
 to that. It's already been answered,

00:02:03.200 --> 00:02:09.810
 but meditation can be quite difficult, and it can be quite

00:02:09.810 --> 00:02:12.680
 stressful. As I've said,

00:02:12.680 --> 00:02:18.140
 depends a lot on your state of mind because mindfulness

00:02:18.140 --> 00:02:23.440
 meditation is designed to confront

00:02:23.440 --> 00:02:29.680
 your mind state. So the first thing to talk about is this

00:02:29.680 --> 00:02:39.160
 idea of peaceful meditation as a choice.

00:02:39.160 --> 00:02:46.770
 Where a person chooses to practice meditation peacefully or

00:02:46.770 --> 00:02:51.460
 manufactures a peaceful meditation

00:02:51.460 --> 00:02:57.150
 as sort of a starting point. As a person, I think from the

00:02:57.150 --> 00:03:01.080
 sounds of it isn't an advanced

00:03:01.080 --> 00:03:03.400
 meditation practitioner because they're comparing

00:03:03.400 --> 00:03:05.720
 themselves to advanced meditation practitioners.

00:03:05.720 --> 00:03:15.270
 And so there's this question about how do you practice

00:03:15.270 --> 00:03:21.280
 peacefully, first of all. And so to

00:03:21.280 --> 00:03:29.090
 some extent I've always kind of tried to disabuse people of

00:03:29.090 --> 00:03:35.520
 this notion that meditation should or

00:03:35.520 --> 00:03:43.520
 perhaps even could be peaceful for them. Because that can

00:03:43.520 --> 00:03:48.360
 be a problematic sort of outlook to have,

00:03:48.360 --> 00:03:52.360
 especially because it's not always possible. But to some

00:03:52.360 --> 00:03:54.640
 extent it is possible. There are

00:03:54.640 --> 00:04:01.400
 ways by which you can enter into meditation practice and

00:04:01.400 --> 00:04:05.760
 have it be smooth sailing. I think

00:04:05.760 --> 00:04:11.320
 the most extreme method to achieve this would be to put

00:04:11.320 --> 00:04:16.280
 aside meditation practice and spend

00:04:16.280 --> 00:04:22.100
 lifetime after lifetime cultivating wholesome qualities,

00:04:22.100 --> 00:04:25.520
 cultivating perhaps the ten perfections

00:04:25.520 --> 00:04:30.370
 of the Buddha. Maybe not complete perfection, but

00:04:30.370 --> 00:04:34.080
 recognizing that meditation is going to be

00:04:34.080 --> 00:04:39.470
 quite stressful for me. And so in order to make it easier I

00:04:39.470 --> 00:04:42.120
 would like to do all the preparatory

00:04:42.120 --> 00:04:45.920
 work in advance. Make myself a better person, right?

00:04:45.920 --> 00:04:47.960
 Because I don't want to confront the

00:04:47.960 --> 00:04:51.250
 crappy person that I am. So let's make myself a better

00:04:51.250 --> 00:04:54.040
 person first. That's certainly possible.

00:04:54.040 --> 00:04:58.950
 And it's recognized in Buddhism. It's recognized that for

00:04:58.950 --> 00:05:00.720
 some people it's not that they're

00:05:00.720 --> 00:05:05.010
 crappy people, it's that their hearts aren't really into a

00:05:05.010 --> 00:05:08.640
 meditative lifestyle. And so there's an

00:05:08.640 --> 00:05:13.210
 encouragement for them to cultivate goodness. And so there

00:05:13.210 --> 00:05:15.960
's a twofold benefit of cultivating

00:05:15.960 --> 00:05:19.900
 goodness. One, it makes meditation easier when and if you

00:05:19.900 --> 00:05:22.160
 do decide to practice meditation,

00:05:22.160 --> 00:05:26.720
 if you're a better person, nicer person, a kinder person.

00:05:26.720 --> 00:05:29.600
 But also it makes you more inclined to

00:05:29.600 --> 00:05:32.360
 practice meditation if you're a better person, nicer person

00:05:32.360 --> 00:05:37.720
, a kinder person. I mean, it's often

00:05:37.720 --> 00:05:42.600
 said that modern day people are quite selfish and modern

00:05:42.600 --> 00:05:46.280
 day spiritual practitioners can often be

00:05:46.280 --> 00:05:51.280
 quite selfish. I think we see this with some of the

00:05:51.280 --> 00:05:55.800
 emphasis of compassion and loving kindness.

00:05:55.800 --> 00:05:59.910
 Something that's not really in the ancient texts is this

00:05:59.910 --> 00:06:03.560
 sense of love for yourself and kindness

00:06:03.560 --> 00:06:07.850
 towards yourself. There's a sense that we hate ourselves

00:06:07.850 --> 00:06:10.240
 and so we should try to be kind to

00:06:10.240 --> 00:06:17.230
 ourselves. And you know, it cuts both ways because that's

00:06:17.230 --> 00:06:21.480
 not what kindness and compassion are really

00:06:21.480 --> 00:06:24.630
 for. They're meant to make you a nicer person, a kinder

00:06:24.630 --> 00:06:27.200
 person towards others, a better person.

00:06:27.200 --> 00:06:31.260
 And I think to some extent we get misled by this whole

00:06:31.260 --> 00:06:34.200
 spiritual movement of thinking that we're

00:06:34.200 --> 00:06:37.670
 special, we're perfect, you're great just the way you are.

00:06:37.670 --> 00:06:40.280
 That's not really the truth. We're broken.

00:06:40.280 --> 00:06:44.940
 We're all crap really. Just the fact that we were born as

00:06:44.940 --> 00:06:47.200
 human beings is a sign that we're not

00:06:47.200 --> 00:06:52.550
 perfect. We've still got craving as sort of a base, a

00:06:52.550 --> 00:06:56.480
 baseline. Our desire for the human existence

00:06:56.480 --> 00:07:01.430
 has led us to be born as humans. And so much of our life

00:07:01.430 --> 00:07:05.560
 surrounds this sort of baseline desire for

00:07:05.560 --> 00:07:11.740
 procreation, for romance, for sensual desire and so on. And

00:07:11.740 --> 00:07:15.320
 so we bring that into spiritual practice

00:07:15.320 --> 00:07:20.740
 while craving for pleasant spiritual states. And the

00:07:20.740 --> 00:07:24.240
 problem is that's not in any way what leads

00:07:24.240 --> 00:07:27.420
 to peaceful or even pleasant states. Happiness doesn't lead

00:07:27.420 --> 00:07:30.040
 to happiness. It's goodness that

00:07:30.040 --> 00:07:32.390
 leads to happiness. And the same goes with peace. So

00:07:32.390 --> 00:07:39.320
 goodness is something first and foremost to

00:07:39.320 --> 00:07:43.530
 keep in mind. If your practice is difficult, before

00:07:43.530 --> 00:07:46.520
 everything else, goodness is going to play a

00:07:46.520 --> 00:07:50.670
 preliminary role in that. Are you a kind person to others?

00:07:50.670 --> 00:07:53.280
 Are you compassionate towards others?

00:07:53.280 --> 00:08:00.760
 Or are you selfish? Are you clingy? Are you mean hearted,

00:08:00.760 --> 00:08:03.760
 mean spirited? Do you have a lot of

00:08:03.760 --> 00:08:09.880
 negative and unpleasant states of mind? So those and those

00:08:09.880 --> 00:08:12.720
 sorts of things, it's kind of like,

00:08:12.720 --> 00:08:22.220
 you know, we come to meditation thinking, probably as you

00:08:22.220 --> 00:08:29.440
 could point to as another problem, is this

00:08:29.440 --> 00:08:34.030
 idea of meditation as just another fix. Like these pills

00:08:34.030 --> 00:08:36.960
 that we take. If you have anxiety or

00:08:36.960 --> 00:08:41.480
 depression, we take pills as a cure. I take these pills, it

00:08:41.480 --> 00:08:44.040
's going to fix me. The pill is very

00:08:44.040 --> 00:08:47.890
 special and it's good. And so we look at meditation like

00:08:47.890 --> 00:08:51.920
 that. Well, here's this meditation. And I was

00:08:51.920 --> 00:08:56.530
 thinking of it kind of like our expectation is we practice

00:08:56.530 --> 00:08:59.760
 meditation, we're buying a sports car.

00:08:59.760 --> 00:09:03.800
 And meditation, I think, should be likened to a sports car

00:09:03.800 --> 00:09:05.880
 in a way because it's not just an

00:09:05.880 --> 00:09:09.370
 ordinary vehicle. Mindfulness meditation, we praise it as

00:09:09.370 --> 00:09:11.440
 being some very special vehicle.

00:09:11.440 --> 00:09:15.320
 So we're talking about a high quality vehicle here, for

00:09:15.320 --> 00:09:17.760
 sure. And when it's described as that,

00:09:17.760 --> 00:09:21.630
 and we hear it described as this special thing, sort of a

00:09:21.630 --> 00:09:24.760
 real fix, a real cure, we expect we'll

00:09:24.760 --> 00:09:27.560
 just get in it and drive away and it'll be smooth sailing.

00:09:27.560 --> 00:09:30.120
 When we get in and we, it feels more like

00:09:30.120 --> 00:09:34.640
 we bought a lemon and we've gotten in a crappy rundown

00:09:34.640 --> 00:09:37.480
 vehicle. So it's probably better to

00:09:37.480 --> 00:09:44.620
 describe this as us envisioning a sports car. And we see

00:09:44.620 --> 00:09:48.520
 these other people driving these very

00:09:48.520 --> 00:09:51.320
 special vehicles. This is the advanced meditators with

00:09:51.320 --> 00:09:55.200
 their peaceful meditation practices. And we

00:09:55.200 --> 00:09:59.170
 think, wow, I'll start doing that. And then we cobble

00:09:59.170 --> 00:10:01.920
 together all these parts that we have lying

00:10:01.920 --> 00:10:05.350
 around. And we realized, oh, there's more to a sports car

00:10:05.350 --> 00:10:07.680
 than just having four wheels and a frame.

00:10:07.680 --> 00:10:13.010
 And we cobbled together this crappy vehicle. And that's

00:10:13.010 --> 00:10:17.960
 where we start. And so you can fit all of

00:10:17.960 --> 00:10:20.980
 the other stuff in here, like all the good things that we

00:10:20.980 --> 00:10:22.920
've done in the past are like all the

00:10:22.920 --> 00:10:27.400
 amenities, all the features, the optional features that you

00:10:27.400 --> 00:10:29.840
 can get with your vehicle and all the

00:10:29.840 --> 00:10:39.850
 the comforts surrounding it. But when we start off, we're

00:10:39.850 --> 00:10:46.840
 starting off with a low quality vehicle.

00:10:46.840 --> 00:10:52.840
 It's kind of like shooting an arrow through a bow. You know

00:10:52.840 --> 00:10:57.760
, if you have a perfect arrow and a perfect

00:10:57.760 --> 00:11:04.440
 bow, even a beginner could shoot the arrow quite straight.

00:11:04.440 --> 00:11:07.760
 But our mind is like this arrow. And if

00:11:07.760 --> 00:11:11.330
 our mind is not straight and it's all crooked, you have to

00:11:11.330 --> 00:11:16.720
 spend time straightening. And so the

00:11:16.720 --> 00:11:20.850
 second, so the first part then is about these things we can

00:11:20.850 --> 00:11:23.800
 do. And sorry, there's just one more thing

00:11:23.800 --> 00:11:26.920
 to talk about. And that's the practice of meditations that

00:11:26.920 --> 00:11:28.440
 are designed to calm you down,

00:11:28.440 --> 00:11:33.870
 that are designed to create peaceful states. And that's

00:11:33.870 --> 00:11:39.800
 another potentially useful practice. Many

00:11:39.800 --> 00:11:45.230
 people will spend years practicing meditation just to calm

00:11:45.230 --> 00:11:49.200
 them down as sort of a baseline. And it

00:11:49.200 --> 00:11:52.360
 changes their baseline for when they do come and practice

00:11:52.360 --> 00:11:55.240
 insight meditation. I mean, that's highly

00:11:55.240 --> 00:11:59.870
 respected. I don't teach that. But it's a highly respected

00:11:59.870 --> 00:12:02.560
 practice where you engage in intense

00:12:02.560 --> 00:12:06.400
 states of trance and concentration before you ever think to

00:12:06.400 --> 00:12:09.440
 practice insight meditation. Now what

00:12:09.440 --> 00:12:15.760
 we practice is considered to be a little more feasible for

00:12:15.760 --> 00:12:18.720
 the average person, especially in

00:12:18.720 --> 00:12:23.020
 modern times. We're going off into the forest and

00:12:23.020 --> 00:12:27.000
 practicing intense tranquility meditation. It's

00:12:27.000 --> 00:12:30.670
 not really feasible. So we instead try to cultivate them

00:12:30.670 --> 00:12:34.680
 together, tranquility and insight at the same

00:12:34.680 --> 00:12:40.270
 time. And hence the general malaise that goes along with

00:12:40.270 --> 00:12:44.720
 that. People are complaining about it being

00:12:44.720 --> 00:12:47.870
 not very peaceful, because you're going to jumping right in

00:12:47.870 --> 00:12:49.800
 with a crappy vehicle. And you haven't

00:12:49.800 --> 00:12:53.500
 done all the time required to tweak it or your arrow is not

00:12:53.500 --> 00:12:55.760
 straight. And so you have to do a

00:12:55.760 --> 00:12:59.830
 lot of straightening. And so that comes to the second part

00:12:59.830 --> 00:13:02.240
 of what I wanted to talk about. And

00:13:02.240 --> 00:13:08.430
 that's the actual practice practicing practicing with a

00:13:08.430 --> 00:13:12.640
 with a substandard vehicle with a mind

00:13:12.640 --> 00:13:19.960
 that is not yet restful. So to some extent, there's no

00:13:19.960 --> 00:13:25.760
 avoiding this people who have beautiful minds

00:13:25.760 --> 00:13:29.120
 who are by nature through lifetime after lifetime,

00:13:29.120 --> 00:13:32.520
 generally, I'm doing good deeds, often have an

00:13:32.520 --> 00:13:37.140
 easier time. But it doesn't mean they don't see the stuff

00:13:37.140 --> 00:13:40.480
 inside that's problematic. It's not to say

00:13:40.480 --> 00:13:47.530
 that it's it's all smooth sailing. For the majority of

00:13:47.530 --> 00:13:52.000
 people, this is going to be at least half the

00:13:52.000 --> 00:13:55.300
 time, where their practice is going to be stressful is

00:13:55.300 --> 00:14:06.440
 going to be unpleasant. And so I've talked about

00:14:06.440 --> 00:14:12.170
 that before, about how we have to change our expectations.

00:14:12.170 --> 00:14:19.000
 And we have to learn to change,

00:14:19.000 --> 00:14:21.340
 change from thinking that something's wrong with our

00:14:21.340 --> 00:14:25.840
 practice when it's unpleasant, to understand

00:14:25.840 --> 00:14:30.030
 that that's a part of learning the learning process, that

00:14:30.030 --> 00:14:34.160
 the unpleasantness has causes. And by

00:14:34.160 --> 00:14:40.270
 observing those unpleasantness is and their causes. And by

00:14:40.270 --> 00:14:44.160
 observing the things that we react to more

00:14:44.160 --> 00:14:51.120
 clearly, that will change the response away from reacting.

00:14:51.120 --> 00:14:54.760
 But there was one other thing that I

00:14:54.760 --> 00:14:59.050
 wanted to say, and that was just a realization that in the

00:14:59.050 --> 00:15:03.080
 beginning, a lot of our practice is not

00:15:03.080 --> 00:15:06.590
 it's not that we've gotten into a sports car, and we're

00:15:06.590 --> 00:15:09.080
 just have to drive in the right direction.

00:15:09.840 --> 00:15:14.310
 It really is that we're learning how to meditate. Prel

00:15:14.310 --> 00:15:17.440
iminary meditation practice is described as

00:15:17.440 --> 00:15:22.090
 learning how to meditate. And so let that sink in that for

00:15:22.090 --> 00:15:25.080
 the first period of your practice, and to

00:15:25.080 --> 00:15:28.820
 some extent, to some extent, you could argue the whole of

00:15:28.820 --> 00:15:31.200
 your practice is about learning how to

00:15:31.200 --> 00:15:39.370
 practice. And it should never be seen as a smooth ride that

00:15:39.370 --> 00:15:43.680
 you can just now coast. But it's always

00:15:43.680 --> 00:15:48.460
 going to be adjusting. The process of enlightenment will

00:15:48.460 --> 00:15:51.800
 and should surprise you every step of the

00:15:51.800 --> 00:16:01.420
 way. In the sense that it's teaching you new things, or new

00:16:01.420 --> 00:16:07.040
 things about things, new things about new

00:16:07.040 --> 00:16:13.850
 things, in fact, it's teaching you what you're doing wrong

00:16:13.850 --> 00:16:17.840
 in your practice to see what you're

00:16:17.840 --> 00:16:23.130
 doing wrong, right? It's level after level of change. And

00:16:23.130 --> 00:16:26.200
 it's going to be constantly changing

00:16:26.200 --> 00:16:31.150
 you fundamentally. So this analogy of constantly not only

00:16:31.150 --> 00:16:36.000
 shooting the arrow through the bow, but

00:16:36.000 --> 00:16:40.330
 also constantly refining, straightening the arrow. I mean,

00:16:40.330 --> 00:16:43.160
 straightening the arrow is the arrow being

00:16:43.160 --> 00:16:47.790
 the mind. And the fact that you're using the mind to learn

00:16:47.790 --> 00:16:51.880
 about the mind, you see, you're using your

00:16:51.880 --> 00:16:57.310
 mind to fix the broken mind. Then you say, well, that means

00:16:57.310 --> 00:17:01.400
 you're using something that's broken to

00:17:01.400 --> 00:17:03.770
 fix something that's broken, you see. So in some ways, it

00:17:03.770 --> 00:17:05.640
 sounds actually impossible. It sounds like

00:17:05.640 --> 00:17:10.440
 aha, then we have a trap here for Buddhists that they're

00:17:10.440 --> 00:17:14.600
 doing something that's not possible. So I

00:17:14.600 --> 00:17:17.180
 leave it up to you to investigate to see whether it is

00:17:17.180 --> 00:17:19.720
 possible. But I wouldn't I think that's just a

00:17:19.720 --> 00:17:22.420
 silly, logical paradox, you know, if you if you

00:17:22.420 --> 00:17:26.120
 intellectualize it, you can come up with that. But

00:17:26.120 --> 00:17:29.610
 it's not really about using the mind of a broken thing to

00:17:29.610 --> 00:17:32.560
 fix a broken thing. It's about that broken

00:17:32.560 --> 00:17:36.670
 thing healing itself. It's about recognizing that you

00:17:36.670 --> 00:17:41.400
 yourself are broken. It's a sort of a, it's a

00:17:41.400 --> 00:17:45.640
 bit less intellectual than that. You know, it's just the

00:17:45.640 --> 00:17:51.680
 process of healing in some sense. But it's

00:17:51.680 --> 00:17:56.460
 but it really is healing the self healing, healing, healing

00:17:56.460 --> 00:18:00.040
, self healing. You're not using a perfect

00:18:00.080 --> 00:18:03.650
 instrument to heal something that's broken. It's not like

00:18:03.650 --> 00:18:06.000
 that. And so our practice is going to in

00:18:06.000 --> 00:18:09.820
 the beginning very much be about simply learning how to

00:18:09.820 --> 00:18:12.720
 practice and realizing that we're doing

00:18:12.720 --> 00:18:18.250
 things wrong. And, and that's most important to to allow us

00:18:18.250 --> 00:18:21.040
 to avoid repeatedly making the same

00:18:21.040 --> 00:18:24.160
 mistakes. Right. In the beginning, we get discouraged

00:18:24.160 --> 00:18:26.160
 because it feels like our practice,

00:18:26.160 --> 00:18:29.340
 you know, I'm doing the way they told me to do it, and it's

00:18:29.340 --> 00:18:31.600
 just not working. And so we should rest

00:18:31.600 --> 00:18:34.480
 assured that in the beginning, we're doing it all wrong.

00:18:34.480 --> 00:18:37.120
 And we're only learning how to do it. Right.

00:18:37.120 --> 00:18:40.310
 Like, if I tell you to watch your stomach and say rising,

00:18:40.310 --> 00:18:42.480
 falling, it's a very simple thing to tell

00:18:42.480 --> 00:18:45.280
 you to do. But the quality of mind when you do that is

00:18:45.280 --> 00:18:49.120
 going to be full of all sorts of preconceptions

00:18:49.120 --> 00:18:52.430
 and delusions. And it's going to be very forceful, mostly,

00:18:52.430 --> 00:18:54.480
 we're so used to when we apply our mind

00:18:54.480 --> 00:18:57.500
 to something, we want to control it. So when I tell you

00:18:57.500 --> 00:18:59.600
 just to watch the stomach, the mind isn't

00:18:59.600 --> 00:19:01.970
 going to do that, it's going to be trying to control it,

00:19:01.970 --> 00:19:03.440
 controlling, trying to control it.

00:19:03.440 --> 00:19:07.520
 Instead of just watching rising, falling, you're going to

00:19:07.520 --> 00:19:09.840
 be trying to force it and you'll see that.

00:19:09.840 --> 00:19:13.200
 And then you'll feel like often the question comes out, how

00:19:13.200 --> 00:19:14.880
 do I stop myself from forcing?

00:19:14.880 --> 00:19:19.830
 And there's no direct answer to that, because that's the

00:19:19.830 --> 00:19:20.960
 doing it wrong,

00:19:21.920 --> 00:19:24.790
 the learning about how you do things wrong. And it's a very

00:19:24.790 --> 00:19:25.840
 important part of it.

00:19:25.840 --> 00:19:33.320
 Because meditation is not separate from our life. So when

00:19:33.320 --> 00:19:34.880
 you ask this question, how do I,

00:19:34.880 --> 00:19:38.370
 how do I, how does my meditation become peaceful? Well, be

00:19:38.370 --> 00:19:39.600
 a peaceful person.

00:19:39.600 --> 00:19:42.700
 Why is it that these advanced meditators can practice so

00:19:42.700 --> 00:19:44.160
 peacefully? Well, that's the whole

00:19:44.160 --> 00:19:52.400
 point. If your meditation is stressful, it's hard work. It

00:19:52.400 --> 00:19:58.640
 means that your mind is suboptimal,

00:19:58.640 --> 00:20:03.090
 your mind is messed up. And great, because that's why you

00:20:03.090 --> 00:20:06.560
're meditating. There should be

00:20:06.560 --> 00:20:09.670
 no surprise that our meditation is difficult. It's about

00:20:09.670 --> 00:20:13.520
 dealing with our mind. If your mind was

00:20:13.520 --> 00:20:16.750
 perfect, you wouldn't need to meditate. If meditation were

00:20:16.750 --> 00:20:18.240
 restful, it would be a sign

00:20:18.240 --> 00:20:21.120
 that you're already, at that end, it would be a sign that

00:20:21.120 --> 00:20:22.800
 you're already enlightened.

00:20:22.800 --> 00:20:27.440
 Now, as I said, there are ways to make it more comfortable

00:20:27.440 --> 00:20:29.840
 by doing a lot of the work in advance,

00:20:29.840 --> 00:20:34.240
 by unscrewing up your mind, making your mind less messed up

00:20:34.240 --> 00:20:37.680
 in advance, in other more mundane ways.

00:20:39.200 --> 00:20:42.780
 All of those ways will be and should be kept in mind during

00:20:42.780 --> 00:20:44.800
 your practice. If you want to make

00:20:44.800 --> 00:20:48.880
 the process of practice easier, you should also be generous

00:20:48.880 --> 00:20:52.000
. You should also be kind. You should

00:20:52.000 --> 00:20:56.350
 aim to be ethical. Ethics is one I didn't mention. You

00:20:56.350 --> 00:20:59.120
 should stop killing and stealing and lying

00:20:59.120 --> 00:21:03.080
 and cheating. Taking drugs and alcohol, you should give up

00:21:03.080 --> 00:21:05.040
 entertainment as best you can.

00:21:05.040 --> 00:21:18.390
 You should try to eat less, sleep less, talk less, Facebook

00:21:18.390 --> 00:21:19.920
 less.

00:21:19.920 --> 00:21:26.280
 And so, you know, this is why doing meditation courses is

00:21:26.280 --> 00:21:27.920
 great. When you go off and do a

00:21:27.920 --> 00:21:33.510
 meditation course, you're forced into a situation where you

00:21:33.510 --> 00:21:36.720
're not able to engage in so many of these

00:21:36.720 --> 00:21:40.750
 distracting and unwholesome states and activities that are

00:21:40.750 --> 00:21:43.600
 going to get in the way of your practice.

00:21:43.600 --> 00:21:48.300
 So the last part of the question, the real question that

00:21:48.300 --> 00:21:52.480
 was asked is, how then do we become

00:21:53.360 --> 00:21:56.370
 like these people who are advanced meditators and who have

00:21:56.370 --> 00:21:57.440
 gotten to the point

00:21:57.440 --> 00:22:01.240
 where their practice is peaceful? And how do we know that

00:22:01.240 --> 00:22:03.040
 we're on the right path?

00:22:03.040 --> 00:22:10.160
 So the peace of someone who's practiced for a long time is

00:22:10.160 --> 00:22:12.240
 different from the peace of someone who's,

00:22:12.240 --> 00:22:15.430
 for example, just a good person but never meditated. The

00:22:15.430 --> 00:22:18.320
 peace of someone who's practiced a

00:22:18.320 --> 00:22:24.330
 long time is deeper and it's more comprehensive. It comes

00:22:24.330 --> 00:22:30.880
 from seeing all things with equanimity.

00:22:30.880 --> 00:22:40.440
 It comes from the state of observing or the quality of

00:22:40.440 --> 00:22:42.640
 seeing things

00:22:44.320 --> 00:22:48.480
 just as they are. So we're seeing is just seeing, hearing

00:22:48.480 --> 00:22:52.000
 is just hearing. Painful experiences are

00:22:52.000 --> 00:22:55.270
 just painful experiences. And what that means is there's no

00:22:55.270 --> 00:22:56.240
 reaction to them.

00:22:56.240 --> 00:23:03.130
 Through our practice of mindfulness, when we note to

00:23:03.130 --> 00:23:05.680
 ourselves, for example, seeing,

00:23:05.680 --> 00:23:09.910
 seeing, or when we say pain, pain, we're teaching ourselves

00:23:09.910 --> 00:23:13.120
 what's happening. We're reminding

00:23:13.120 --> 00:23:17.510
 ourselves of the reality of the experience. Our experience

00:23:17.510 --> 00:23:19.600
 is made up of the reality of it

00:23:19.600 --> 00:23:24.400
 and then all of our baggage. This is good, this is bad,

00:23:24.400 --> 00:23:26.560
 this is me, this is mine.

00:23:26.560 --> 00:23:30.520
 I like this, I don't like this, I agree with this, I

00:23:30.520 --> 00:23:33.280
 disagree with this, all of the baggage,

00:23:33.280 --> 00:23:38.280
 which is not a part of the reality. I mean, seeing is just

00:23:38.280 --> 00:23:40.560
 seeing. That's the only part of it that's

00:23:40.560 --> 00:23:44.040
 real. Even when you say I see a cat, the cat isn't part of

00:23:44.040 --> 00:23:48.800
 the seeing. Cat is a concept in our mind

00:23:48.800 --> 00:23:52.170
 related to having seen other things in the past that are a

00:23:52.170 --> 00:23:54.640
 similar shape and so on.

00:23:54.640 --> 00:23:59.680
 Make similar sounds like that. Oh, meowd, it must be a cat,

00:23:59.680 --> 00:24:00.320
 for example.

00:24:00.320 --> 00:24:07.760
 And so when we're mindful in this way, we start to see that

00:24:07.760 --> 00:24:13.450
 all of the baggage that we carry around, especially our

00:24:13.450 --> 00:24:16.880
 reactions, are unwarranted.

00:24:16.880 --> 00:24:21.810
 We start to see on a more general level that the things we

00:24:21.810 --> 00:24:24.000
 cling to and strive after

00:24:24.000 --> 00:24:28.180
 are not worth clinging to or striving after. They're not

00:24:28.180 --> 00:24:29.600
 stable in the way that we thought

00:24:29.600 --> 00:24:32.100
 they were going to be. They're not satisfying in the way

00:24:32.100 --> 00:24:34.480
 that we thought they were. They're not

00:24:34.480 --> 00:24:39.610
 controllable in the way that we thought they were. They

00:24:39.610 --> 00:24:43.920
 have no sense of being me or mine or I.

00:24:43.920 --> 00:24:49.710
 Seeing is really just seeing, and everything else about it

00:24:49.710 --> 00:24:51.680
 just goes out the window.

00:24:51.680 --> 00:24:58.050
 As you cultivate this, the description of the practice is

00:24:58.050 --> 00:25:00.640
 not so many steps or so complicated.

00:25:03.120 --> 00:25:06.750
 This is why insight is not some thesis that you have to

00:25:06.750 --> 00:25:09.440
 write pages and pages about. It simply

00:25:09.440 --> 00:25:12.930
 means seeing impermanent suffering and non-self, and it's a

00:25:12.930 --> 00:25:17.120
 very simple thing. Those three qualities

00:25:17.120 --> 00:25:23.580
 sum up what they are not. All of this baggage that is unw

00:25:23.580 --> 00:25:29.440
arranted, this seeking stability,

00:25:29.440 --> 00:25:36.180
 the seeking satisfaction, seeking self and control and

00:25:36.180 --> 00:25:39.280
 possessiveness and so on.

00:25:39.280 --> 00:25:44.360
 And as you see this more clearly, it just starts to

00:25:44.360 --> 00:25:45.520
 resonate with you.

00:25:45.520 --> 00:25:47.850
 This is the straightening of the arrow or this is the

00:25:47.850 --> 00:25:50.640
 upgrading of the vehicle,

00:25:50.640 --> 00:25:54.770
 until eventually it does feel like you're in very much a

00:25:54.770 --> 00:25:58.080
 sports car and it is more clear sailing

00:25:59.200 --> 00:26:00.240
 because whatever comes up,

00:26:00.240 --> 00:26:07.600
 the mind sees it just for what it is and doesn't give rise

00:26:07.600 --> 00:26:09.360
 to all the reactions and all the stress.

00:26:09.360 --> 00:26:14.090
 Mindfulness should not be and is not by its own by its

00:26:14.090 --> 00:26:15.440
 simple nature.

00:26:15.440 --> 00:26:21.520
 Difficult. All of the difficulty comes from

00:26:24.160 --> 00:26:29.280
 the qualities of our vehicle. We haven't changed the oil.

00:26:29.280 --> 00:26:40.400
 We are using our shocks, are in poor condition, don't have

00:26:40.400 --> 00:26:42.960
 air conditioning and so on and so on.

00:26:42.960 --> 00:26:48.950
 The tires are threadbare. All of the aspects of our mind

00:26:48.950 --> 00:26:53.040
 that are conspiring against us

00:26:53.920 --> 00:26:57.660
 and much of that can be improved through meditation

00:26:57.660 --> 00:26:59.600
 practice. The practice of meditation

00:26:59.600 --> 00:27:03.640
 is learning how to meditate very much in the beginning and

00:27:03.640 --> 00:27:05.440
 to some extent all throughout our

00:27:05.440 --> 00:27:10.110
 practice. The final state of meditation before

00:27:10.110 --> 00:27:13.280
 enlightenment is called Anuloma.

00:27:13.280 --> 00:27:17.920
 I've used this word before. Anuloma is the grain of wood.

00:27:17.920 --> 00:27:21.400
 Wood has a grain to it. If you've ever cut wood, you know

00:27:21.400 --> 00:27:22.320
 this. You can't cut

00:27:24.160 --> 00:27:27.390
 firewood sideways because it has a grain but if you cut the

00:27:27.390 --> 00:27:29.600
 firewood down the top,

00:27:29.600 --> 00:27:33.260
 it will split in half which is a grain to the wood. Anu

00:27:33.260 --> 00:27:36.560
 means according in line with the following.

00:27:36.560 --> 00:27:40.220
 Following the grain, going with the grain. And so the grain

00:27:40.220 --> 00:27:42.080
 here is the grain of reality.

00:27:42.080 --> 00:27:46.130
 Reality is that everything that arises ceases. It's the

00:27:46.130 --> 00:27:49.280
 reality of impermanence, uncertainty.

00:27:49.280 --> 00:27:54.440
 It's the reality of dissatisfaction or inability to satisfy

00:27:54.440 --> 00:27:55.440
 suffering.

00:27:55.440 --> 00:28:01.300
 That the clinging to anything and chasing after anything is

00:28:01.300 --> 00:28:04.240
 stressful, is unwarranted

00:28:04.240 --> 00:28:11.640
 and non-self. That everything is just is what it is. There

00:28:11.640 --> 00:28:13.440
's nothing more to it. There's no entity,

00:28:13.440 --> 00:28:20.880
 no self or so on. It's when you finally see that and you

00:28:20.880 --> 00:28:23.920
 know it's surrounded by really a practice.

00:28:23.920 --> 00:28:28.280
 It's what leads up to that moment is a practice that is so

00:28:28.280 --> 00:28:31.680
 finely tuned that you're seeing

00:28:31.680 --> 00:28:36.180
 everything with equanimity as I said. An advanced meditator

00:28:36.180 --> 00:28:38.160
 is able to get to that state

00:28:40.160 --> 00:28:44.640
 and through the kind of this it's kind of like a feedback

00:28:44.640 --> 00:28:47.360
 loop. It just resonates more and more

00:28:47.360 --> 00:28:50.950
 and you build up this stronger and stronger focus. That's

00:28:50.950 --> 00:28:53.440
 really a natural focus. It's no longer

00:28:53.440 --> 00:28:59.780
 effortful because it's nothing to do with intending. It's

00:28:59.780 --> 00:29:02.400
 just seeing. When you see that

00:29:02.400 --> 00:29:05.810
 everything is impermanent, unsatisfying and controllable,

00:29:05.810 --> 00:29:07.520
 you don't cling to anything.

00:29:07.520 --> 00:29:12.380
 You don't strive after anything. By seeing that again and

00:29:12.380 --> 00:29:14.320
 again it gets stronger and stronger

00:29:14.320 --> 00:29:18.640
 until it sparks like heating up, rubbing two sticks

00:29:18.640 --> 00:29:22.560
 together to produce fire. Once it gets

00:29:22.560 --> 00:29:26.360
 builds and builds and builds the heat then there's the

00:29:26.360 --> 00:29:28.480
 moment where the mind lets go.

00:29:32.960 --> 00:29:36.660
 The point being that that moment is described as the moment

00:29:36.660 --> 00:29:39.920
 when you actually are in line with

00:29:39.920 --> 00:29:43.910
 reality. It means you're actually meditating. Everything up

00:29:43.910 --> 00:29:46.160
 to that point was just really

00:29:46.160 --> 00:29:51.270
 learning how to meditate. Expecting the meditation to start

00:29:51.270 --> 00:29:54.160
 there is to some extent

00:29:54.160 --> 00:29:57.570
 a misunderstanding of what meditation is and how meditation

00:29:57.570 --> 00:30:00.960
 works. You only get the sports car, the

00:30:02.320 --> 00:30:06.820
 perfect vehicle at the very last moment when you're already

00:30:06.820 --> 00:30:10.960
 really good. It's much less about

00:30:10.960 --> 00:30:17.910
 a linear cruising as it is to an uphill forming of the

00:30:17.910 --> 00:30:20.720
 vehicle that is the mind.

00:30:20.720 --> 00:30:30.000
 That's the observation of people who have already done that

00:30:30.000 --> 00:30:32.000
. You see them in that state.

00:30:32.000 --> 00:30:34.910
 It's because of the work that they've done at your level

00:30:34.910 --> 00:30:36.640
 where it was difficult,

00:30:36.640 --> 00:30:44.400
 at the beginner level where it's stressful and hard to do.

00:30:44.400 --> 00:30:49.950
 Some advice, I've given now a bunch of advice, but a little

00:30:49.950 --> 00:30:51.840
 more specific advice.

00:30:51.840 --> 00:30:55.930
 If the meditation is tiring, this is in particular what

00:30:55.930 --> 00:30:57.840
 this person is talking about,

00:30:59.440 --> 00:31:02.200
 one of the things you want to do is to be mindful of the

00:31:02.200 --> 00:31:04.400
 fact that you're tired and you'll be

00:31:04.400 --> 00:31:07.590
 surprised that that works sometimes. Often you think, "I

00:31:07.590 --> 00:31:09.440
 just can't meditate anymore except you're

00:31:09.440 --> 00:31:11.770
 tired," and then you say tired, tired, and it just

00:31:11.770 --> 00:31:13.760
 disappears and you gain strength again.

00:31:13.760 --> 00:31:18.400
 On the other side, a recognition that your practice is

00:31:18.400 --> 00:31:22.160
 really going to be imperfect in the beginning

00:31:23.120 --> 00:31:27.520
 and be encouraged by the fact that you've actually done

00:31:27.520 --> 00:31:31.120
 some work. When it does feel tiring, then

00:31:31.120 --> 00:31:33.680
 that's a sign that you're starting to see how your mind,

00:31:33.680 --> 00:31:36.720
 the state that your mind is in. You can take

00:31:36.720 --> 00:31:39.420
 a break and come back and try again with the knowledge that

00:31:39.420 --> 00:31:40.960
 eventually you're going to get

00:31:40.960 --> 00:31:45.140
 better at it, especially if you keep it up, most especially

00:31:45.140 --> 00:31:47.120
 if you take some time to do it

00:31:47.120 --> 00:31:52.080
 intensively or if you do it systematically and continuously

00:31:52.080 --> 00:31:53.920
 throughout your life.

00:31:53.920 --> 00:32:00.560
 If you accompany it with other good qualities, if you

00:32:00.560 --> 00:32:03.520
 dedicate yourself to being a better person,

00:32:03.520 --> 00:32:08.460
 dedicate yourself to ways of being that are conducive to

00:32:08.460 --> 00:32:11.920
 the meditation practice in all sorts of ways.

00:32:15.520 --> 00:32:17.630
 Hopefully that answered the question. I think there's a lot

00:32:17.630 --> 00:32:18.240
 in there that's

00:32:18.240 --> 00:32:24.370
 of use. Thank you all for coming out. Again, we're over 70

00:32:24.370 --> 00:32:26.080
 viewers on the live stream.

00:32:26.080 --> 00:32:29.800
 If you'd like to ask questions, again, the link should be

00:32:29.800 --> 00:32:31.920
 very soon in the description.

00:32:31.920 --> 00:32:35.490
 You can go to our meditation site. I don't answer questions

00:32:35.490 --> 00:32:36.160
 on YouTube

00:32:36.160 --> 00:32:42.850
 for some very simple reasons, but it's free. I mean, I'm

00:32:42.850 --> 00:32:44.720
 not trying to... there's no other reason

00:32:46.400 --> 00:32:51.980
 to... I have no ulterior motive in directing you to our

00:32:51.980 --> 00:32:54.640
 meditation site. I suppose

00:32:54.640 --> 00:32:57.720
 a minor ulterior motive is to encourage people to actually

00:32:57.720 --> 00:32:58.480
 meditate.

00:32:58.480 --> 00:33:03.220
 But there's no charge or you don't get put on an email list

00:33:03.220 --> 00:33:06.480
 or anything. You don't start to get

00:33:06.480 --> 00:33:11.460
 advertising or something. If you go and sign up there, it's

00:33:11.460 --> 00:33:16.240
 just... it separates the serious people

00:33:16.240 --> 00:33:24.960
 from people who come on to ask, perhaps, flippant or irreve

00:33:24.960 --> 00:33:27.680
rent questions.

00:33:27.680 --> 00:33:33.090
 At least to some extent. So thank you all for tuning in. I

00:33:33.090 --> 00:33:34.880
 wish you all the best. Have a good day.

00:33:34.880 --> 00:33:51.600
 [

