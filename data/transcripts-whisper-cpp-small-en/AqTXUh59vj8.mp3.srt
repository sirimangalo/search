1
00:00:00,000 --> 00:00:04,110
 My understanding of the Buddhist conception of love is that

2
00:00:04,110 --> 00:00:04,920
 it is a kind

3
00:00:04,920 --> 00:00:08,390
 of delusion, an attachment with attendant negative con

4
00:00:08,390 --> 00:00:09,800
notations. If this is the

5
00:00:09,800 --> 00:00:12,370
 case, how could we characterize the emotion motivation

6
00:00:12,370 --> 00:00:13,880
 which impels a bodhisattva

7
00:00:13,880 --> 00:00:19,880
 to be reborn? Well, love is not delusion. And again, back

8
00:00:19,880 --> 00:00:20,820
 to the abhidhamma, love

9
00:00:20,820 --> 00:00:30,920
 is a word and is ambiguous because most often in the West

10
00:00:30,920 --> 00:00:31,200
 we're

11
00:00:31,200 --> 00:00:39,240
 referring to eros, eros which is lustful love or not even

12
00:00:39,240 --> 00:00:39,840
 lustful, but just

13
00:00:39,840 --> 00:00:45,360
 desire for something. I love apples or I love my wife or my

14
00:00:45,360 --> 00:00:46,260
 girlfriend or my

15
00:00:46,260 --> 00:00:52,920
 boyfriend or I love my children, adoration and so on, is

16
00:00:52,920 --> 00:00:55,040
 often unwholesome,

17
00:00:55,040 --> 00:01:00,570
 not delusion, based on delusion, but is craving or greed

18
00:01:00,570 --> 00:01:04,080
 based, desire based. And

19
00:01:04,080 --> 00:01:10,240
 so that's unwholesome. But love that is a desire for beings

20
00:01:10,240 --> 00:01:11,400
 to be happy, wishing

21
00:01:11,400 --> 00:01:18,920
 for them to be happy or acting. The impulsion that push

22
00:01:18,920 --> 00:01:20,120
 moves us to act

23
00:01:20,120 --> 00:01:23,210
 for the benefit of others, this is metta and this is a

24
00:01:23,210 --> 00:01:25,200
 positive state. It's

25
00:01:25,200 --> 00:01:28,510
 wholesome. It's worldly, it won't get you to nirvana, but

26
00:01:28,510 --> 00:01:29,800
 it's wholesome and if

27
00:01:29,800 --> 00:01:33,860
 cultivated can eventually lead to limitless love for the

28
00:01:33,860 --> 00:01:35,000
 whole universe

29
00:01:35,000 --> 00:01:39,540
 and which can lead you to be born as a brahma and can give

30
00:01:39,540 --> 00:01:40,640
 great states of

31
00:01:40,640 --> 00:01:44,340
 concentration which are also beneficial to the practice of

32
00:01:44,340 --> 00:01:46,800
 insight meditation.

33
00:01:46,800 --> 00:01:51,370
 So depends what you mean by love, but the emotion

34
00:01:51,370 --> 00:01:53,200
 motivation that impels a bodhisattva

35
00:01:53,200 --> 00:01:59,230
 to be reborn. Okay, the reason a bodhisattva in the Therav

36
00:01:59,230 --> 00:02:00,560
ada tradition is

37
00:02:00,560 --> 00:02:04,210
 reborn and I'm assuming you're talking about the Theravada

38
00:02:04,210 --> 00:02:05,400
 bodhisattva without

39
00:02:05,400 --> 00:02:09,270
 the V because with the V in there it gets kind of weird and

40
00:02:09,270 --> 00:02:09,840
 the Mahayana

41
00:02:09,840 --> 00:02:16,470
 conception gets pretty radical. But what impels a bodhisatt

42
00:02:16,470 --> 00:02:17,840
va to be reborn again

43
00:02:17,840 --> 00:02:21,540
 is delusion and defilement. The only reason we're reborn is

44
00:02:21,540 --> 00:02:22,120
 because of some

45
00:02:22,120 --> 00:02:27,680
 kind of attachment. Now all that a bodhisattva has done is

46
00:02:27,680 --> 00:02:32,040
 refused to listen

47
00:02:32,040 --> 00:02:39,840
 or to accept an enlightenment that is devoid of omniscience

48
00:02:39,840 --> 00:02:41,760
. So given

49
00:02:41,760 --> 00:02:44,520
 the opportunity to become enlightened in this life without

50
00:02:44,520 --> 00:02:46,200
 being omniscient,

51
00:02:46,200 --> 00:02:54,350
 they discard it. There's that. Now what moves them to do it

52
00:02:54,350 --> 00:02:56,320
? Again,

53
00:02:56,320 --> 00:02:59,890
 so now we have to distinguish between bodhisattvas. Some

54
00:02:59,890 --> 00:03:00,840
 bodhisattvas are not

55
00:03:00,840 --> 00:03:04,880
 going to become Buddhas and they make a determination to

56
00:03:04,880 --> 00:03:05,600
 become a Buddha

57
00:03:05,600 --> 00:03:09,340
 and are unsuccessful and often these determinations that

58
00:03:09,340 --> 00:03:10,440
 they make are based

59
00:03:10,440 --> 00:03:16,970
 on ego or desire or whatever. In the case of our Buddha, he

60
00:03:16,970 --> 00:03:19,600
 made a vow out of, I

61
00:03:19,600 --> 00:03:25,000
 guess you could say wisdom and confidence and faith because

62
00:03:25,000 --> 00:03:25,680
 he was a

63
00:03:25,680 --> 00:03:30,680
 highly developed ascetic who had magical powers and insight

64
00:03:30,680 --> 00:03:32,320
 and he knew with

65
00:03:32,320 --> 00:03:34,950
 certainty, with confidence that if he listened to the

66
00:03:34,950 --> 00:03:36,080
 Buddha's teaching he

67
00:03:36,080 --> 00:03:42,200
 would become an Arahant very quickly, right away. And so he

68
00:03:42,200 --> 00:03:43,080
 made the

69
00:03:43,080 --> 00:03:46,330
 determination to go one step further, to go the next step

70
00:03:46,330 --> 00:03:47,920
 which was a huge step

71
00:03:47,920 --> 00:03:53,580
 out of wisdom and understanding. It's interesting, curious

72
00:03:53,580 --> 00:03:54,280
 to note that as soon

73
00:03:54,280 --> 00:03:56,690
 as he became a Buddha, one of the first things he thought

74
00:03:56,690 --> 00:03:59,840
 of was not taking

75
00:03:59,840 --> 00:04:03,140
 advantage of his omniscience, was to pass away into parinib

76
00:04:03,140 --> 00:04:05,720
hana which, you know, he

77
00:04:05,720 --> 00:04:09,110
 would have saved himself a lot of time had he just done

78
00:04:09,110 --> 00:04:10,400
 that back in the time

79
00:04:10,400 --> 00:04:17,840
 of Deepankara Buddha. But the result of his omniscience was

80
00:04:17,840 --> 00:04:18,800
 such that he

81
00:04:18,800 --> 00:04:21,680
 wasn't able to do it because immediately he was asked to

82
00:04:21,680 --> 00:04:23,200
 teach. Brahma came down

83
00:04:23,200 --> 00:04:28,160
 and asked him to teach because he was omniscient. Brahma

84
00:04:28,160 --> 00:04:28,640
 would not come

85
00:04:28,640 --> 00:04:31,110
 down and ask me to teach, for example, because I'm not omn

86
00:04:31,110 --> 00:04:32,760
iscient. But he came

87
00:04:32,760 --> 00:04:36,310
 down and asked the Buddha to teach and so it was based on

88
00:04:36,310 --> 00:04:37,480
 this quality. What

89
00:04:37,480 --> 00:04:42,200
 motivated it, and so again this differs from person to

90
00:04:42,200 --> 00:04:42,680
 person, but the

91
00:04:42,680 --> 00:04:45,720
 real bodhisattvas, it would seem they do it out of

92
00:04:45,720 --> 00:04:48,160
 confidence and wisdom on that

93
00:04:48,160 --> 00:04:52,520
 occasion where they are recognized and where another Buddha

94
00:04:52,520 --> 00:04:53,960
 says, "See that guy's

95
00:04:53,960 --> 00:04:57,430
 lying in the mud, he's going to become a Buddha in a future

96
00:04:57,430 --> 00:04:58,760
 life."

