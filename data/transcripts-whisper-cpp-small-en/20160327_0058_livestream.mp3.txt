 Good evening everyone.
 I'm practicing live March 26th.
 The day's quote is a generic quote because there's nothing
 really generic about it except
 it is something that appears in the Dibhittika in lots of
 different places.
 In fact, I'm not sure that this is where he's taking it
 from, but the reference is to the
 Salaika Sutta, which is to Chandu.
 It's actually quite dense, but Mahasya Sayadawd wrote a
 book on it, gave talks and then turned
 it into a book which is very much worth reading.
 Salaika Sutta, a commentary by Mahasya Sayadawd, very much
 worth reading.
 That's not what this quote is about though.
 The quote is tacked on at the end.
 "Yangko sattara karuni yang savakanam."
 Whatever should be done by a teacher for his students.
 "Hite sinanukampakena anukampangupada."
 Out of compassion, with reference to compassion, the
 teacher who is compassionate, who is thinking
 of or desiring the welfare of those students.
 "Katang wo tangmaya."
 Done for you, that is by me.
 Done for you, that is by me.
 It's very Yoda.
 Pali.
 "Katang dan wo for you, tang vatmaya by me."
 "Katang wo tangmaya."
 I have done that for you or that has been done for you by
 me.
 They're very passive, they have a lot of passive in Pali
 and Sanskrit.
 Twist things around.
 Instead of saying, "I've done that for you," they say, "
That was done for you by me."
 I have done for you what I need to do as your teacher.
 Means I've taught you what you need to know basically.
 "Etani," over there, "chunda," "rukamulani," those tree
 roots.
 "Etani sunyagarani," over there are empty huts.
 "Jayata," "Jayata," "Jayata," "junda," meditate junda.
 "Mapa madata," don't be negligent.
 "Mapa chahvi pati saarino ahu vata," ahu vata is
 interesting.
 I think that's like a future perfect or something.
 Don't be one who has...
 I don't know, ahu vata is...
 What is that?
 I think that's a panchami or something.
 I forget.
 Don't be one who is...
 "Vipati saarino," who feels guilty afterwards or is vexed
 by their negligence afterwards.
 "Mapa chahvi pati saarino ahu vata," ayankho amha kanganu s
asanmi.
 This is my exhortation to all of you.
 This is the Buddha's standard exhortation.
 It's interesting for many reasons, but for the one reason
 specifically that it hints at the idea of simplicity or
 contentment in learning.
 He's saying, "Look, I've taught you now enough of that."
 He's basically saying, "Enough. That's enough. Now go med
itate."
 Directing the conversation, directing the attention of the
 students.
 When I teach meditation at McMaster, this five minute
 meditation lesson, it's interesting the range of responses.
 I don't have enough feedback yet to gauge what the fruit of
 the teaching is, but I get the feeling that for some people
 it's...
 They're just looking for information.
 Maybe the idea of meditation sounds good or something, but
 absolutely not all of them.
 Many people are actually into it.
 Sometimes it's easy to say, "I want to meditate." It's easy
 to say, "I should meditate."
 It's easy to pick up a book and learn how to meditate. It's
 easy to take a lesson on meditation.
 But it's a whole other thing to sit down and say, "Oh yeah,
 what that means is actually meditating.
 Actually paying attention. Actually doing something."
 Jayata. Bhikkhu-ve. Jayata. Jayata. Meditate.
 Why that's interesting to me is because I get lots of
 questions and lots of inquiries.
 People... I've talked about many times asking questions
 about their life.
 "How do I deal with this? How do I deal with that?"
 This quote speaks to me in that way. It reflects some of
 the dilemma there of wanting to help people.
 But trying to explain to people what is really helpful. It
's not talking or...
 There's no solution. It's not like there's anything I'm
 going to say that's going to fix your problem.
 And then, "Aha! That information will allow me to solve my
 problems. That's not it."
 We want to fix things. We still want to fix everything.
 And in fact, that's really the wrong way of looking at
 things. There is no fix.
 You have to begin to accept that things are broken.
 That there's an intrinsic... Like Leonard Cohen said, there
's a crack in everything.
 Everything is broken.
 And to stop trying to find solutions and to start
 understanding problems, understanding the nature of the
 experiences behind the problems.
 And so on.
 So, in that vein, I'm not going to talk too much. Probably
 already talked more than I should.
 I should have just said, "Ah, you see? The Buddha says, 'Go
 meditate. That's enough for tonight.'"
 But no, I have to.
 It's the duty of a teacher to instruct and to advise and to
 explain.
 And so here I've hopefully done a little bit of that.
 If anyone has any questions about their meditation, I will
 post the hangout.
 I missed Second Life today, though it sounds like maybe I
 had already discussed that I would be missing.
 And it just totally skipped my mind. So probably at some
 point I did say,
 "I'm going to probably be too busy this weekend. I can't
 remember."
 I'm writing my essay. Slow going.
 And so we've kind of worked out.
 Sounds like month of June I'll be away in Asia.
 So if you want to meet me in Thailand, I'd probably be
 there about two weeks.
 It's probably not worth going for two weeks to Thailand.
 If you're in Thailand or in Asia, welcome to come with me.
 I can come meet my teacher, but it would probably just be a
 brief meeting.
 He's very old and probably still very tired. Overworked.
 So yeah, that's something. That's everything. Nothing else.
 That's all I can think of. No questions? And I'm going to
 say goodnight. Go meditate.
 Thank you.
