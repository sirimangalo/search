1
00:00:00,000 --> 00:00:07,000
 Okay, so we're broadcasting live.

2
00:00:07,000 --> 00:00:16,880
 I try to get up a little bit of done my everyday from now

3
00:00:16,880 --> 00:00:20,000
 on.

4
00:00:20,000 --> 00:00:42,700
 So today, that I talk about the problems that make us seek

5
00:00:42,700 --> 00:00:48,000
 out meditation.

6
00:00:48,000 --> 00:00:59,000
 In Buddhism we talk about three problems.

7
00:00:59,000 --> 00:01:14,000
 There are three characteristics of just about everything.

8
00:01:14,000 --> 00:01:18,000
 Almost everything.

9
00:01:18,000 --> 00:01:30,650
 That I think you can identify as the impetus for people to

10
00:01:30,650 --> 00:01:37,000
 come and practice meditation generally speaking.

11
00:01:37,000 --> 00:01:46,510
 The first one is impermanence. That in our lives we have to

12
00:01:46,510 --> 00:01:52,000
 deal with change or the threat of change.

13
00:01:52,000 --> 00:02:01,870
 That's the instability of the world around us. So either we

14
00:02:01,870 --> 00:02:10,000
've suffered from change, we've lost something,

15
00:02:10,000 --> 00:02:21,370
 or we've acquired something and we mourn the loss or we

16
00:02:21,370 --> 00:02:26,000
 mourn the burden.

17
00:02:26,000 --> 00:02:33,840
 And this upsets us. Or else we have something or we're free

18
00:02:33,840 --> 00:02:38,000
 from something and we're afraid of losing or gaining.

19
00:02:38,000 --> 00:02:48,000
 We're afraid of change. We fear the future.

20
00:02:48,000 --> 00:02:58,330
 We have an awareness of the uncertainty of life and this

21
00:02:58,330 --> 00:03:06,000
 causes us worry and stress and suffering.

22
00:03:06,000 --> 00:03:12,000
 And so as a result we come to practice meditation.

23
00:03:12,000 --> 00:03:22,000
 We search the world and we are unable to find stability.

24
00:03:22,000 --> 00:03:26,950
 We're looking for stability and we take on meditation as a

25
00:03:26,950 --> 00:03:31,000
 means of attaining some sort of stability.

26
00:03:31,000 --> 00:03:36,910
 Not everyone does this. Others will find, will seek out

27
00:03:36,910 --> 00:03:40,000
 stability elsewhere.

28
00:03:40,000 --> 00:03:43,830
 Often we still think there's something in the world that

29
00:03:43,830 --> 00:03:47,000
 will bring us the stability we're seeking for.

30
00:03:47,000 --> 00:03:55,000
 It could be a new job or a new relationship.

31
00:03:55,000 --> 00:04:03,590
 For some people it's drugs or alcohol or death, even

32
00:04:03,590 --> 00:04:05,000
 suicide.

33
00:04:05,000 --> 00:04:14,020
 We have many different extreme measures we take when we're

34
00:04:14,020 --> 00:04:21,000
 confronted with this uncertainty.

35
00:04:21,000 --> 00:04:28,350
 The second characteristic is suffering. So sometimes we

36
00:04:28,350 --> 00:04:30,000
 suffer.

37
00:04:30,000 --> 00:04:45,000
 We might have great pain or sadness, depression, anxiety.

38
00:04:45,000 --> 00:05:03,000
 We might be sick or we might have some condition. We might

39
00:05:03,000 --> 00:05:03,000
 have become paralyzed or we might have lost the use of part

40
00:05:03,000 --> 00:05:03,000
 of our body.

41
00:05:03,000 --> 00:05:12,000
 We might have acquired a disease or we might be old and we

42
00:05:12,000 --> 00:05:15,000
 might be dying.

43
00:05:15,000 --> 00:05:18,000
 We might have some sort of suffering.

44
00:05:18,000 --> 00:05:21,980
 So in life there's a lot of suffering, not all suffering.

45
00:05:21,980 --> 00:05:26,000
 There's a lot of happiness as well.

46
00:05:26,000 --> 00:05:33,140
 At times we experience suffering and so we seek out

47
00:05:33,140 --> 00:05:37,000
 something that is happiness.

48
00:05:37,000 --> 00:05:42,040
 Or to be more general we are dissatisfied. We may not be

49
00:05:42,040 --> 00:05:43,000
 suffering.

50
00:05:43,000 --> 00:05:47,530
 Although sometimes that's the case. At times when we just

51
00:05:47,530 --> 00:05:50,000
 feel dissatisfied we feel like life is meaningless,

52
00:05:50,000 --> 00:05:53,000
 pointless, useless.

53
00:05:53,000 --> 00:05:57,530
 And so we're looking for satisfaction. We're looking for

54
00:05:57,530 --> 00:05:59,000
 contentment.

55
00:05:59,000 --> 00:06:04,000
 We're discontent with life, having to work, mindless jobs,

56
00:06:04,000 --> 00:06:15,000
 having to deal with mindless relationships,

57
00:06:15,000 --> 00:06:21,240
 having to deal with our own minds, dissatisfied with our

58
00:06:21,240 --> 00:06:26,000
 own minds, our own bodies, our own selves.

59
00:06:26,000 --> 00:06:30,000
 And so we seek out satisfaction.

60
00:06:30,000 --> 00:06:33,510
 Meditation is a way for us to find this sort of

61
00:06:33,510 --> 00:06:35,000
 satisfaction.

62
00:06:35,000 --> 00:06:41,440
 Again there are other ways but often people come to

63
00:06:41,440 --> 00:06:47,000
 practice meditation thinking it will satisfy them.

64
00:06:47,000 --> 00:06:57,000
 The third characteristic is non-self.

65
00:06:57,000 --> 00:07:05,000
 So in life we find ourselves unable to control ourselves.

66
00:07:05,000 --> 00:07:09,000
 We find our mind out of control.

67
00:07:09,000 --> 00:07:13,660
 Maybe we have some kind of addiction that is out of control

68
00:07:13,660 --> 00:07:14,000
.

69
00:07:14,000 --> 00:07:18,760
 Maybe we have anger issues and we're out of control. Worry

70
00:07:18,760 --> 00:07:23,000
 or fear, depression, something that is out of our control

71
00:07:23,000 --> 00:07:26,000
 and we're unable to fix.

72
00:07:26,000 --> 00:07:33,620
 Maybe the world around us is collapsing and we're unable to

73
00:07:33,620 --> 00:07:39,000
 maintain our job or family situation.

74
00:07:39,000 --> 00:07:47,010
 So we're looking for control. Looking to find a way to get

75
00:07:47,010 --> 00:07:50,000
 a hold of ourselves.

76
00:07:50,000 --> 00:08:01,000
 A way to regain our little control that we've lost.

77
00:08:01,000 --> 00:08:05,000
 Maybe we've lost ourselves and we don't know who we are.

78
00:08:05,000 --> 00:08:08,230
 So we come to practice meditation to find ourselves, to

79
00:08:08,230 --> 00:08:14,060
 find out who we are and gain a sense of control, sense of

80
00:08:14,060 --> 00:08:18,000
 self, sense of identity.

81
00:08:18,000 --> 00:08:24,360
 And we realize we can't, or we lose things, or we hold

82
00:08:24,360 --> 00:08:29,430
 precious, or we find ourselves changing in ways that we don

83
00:08:29,430 --> 00:08:31,000
't like.

84
00:08:31,000 --> 00:08:35,460
 So we want to regain a sense of identity and figure out who

85
00:08:35,460 --> 00:08:41,920
 we are and get rid of some of the things that we don't want

86
00:08:41,920 --> 00:08:43,000
 to be.

87
00:08:43,000 --> 00:08:51,000
 So meditation is an ideal candidate, we think.

88
00:08:51,000 --> 00:08:58,000
 The problem is that none of this is really true.

89
00:08:58,000 --> 00:09:07,000
 Meaning meditation itself is not stable.

90
00:09:07,000 --> 00:09:12,000
 Meditation itself is not satisfying.

91
00:09:12,000 --> 00:09:18,000
 Meditation itself is not at all about control or identity.

92
00:09:18,000 --> 00:09:26,000
 It's not controllable. It's not you.

93
00:09:26,000 --> 00:09:34,000
 Meditation isn't about leaving the world behind.

94
00:09:34,000 --> 00:09:38,740
 We think we can go off to a quiet place and leave the world

95
00:09:38,740 --> 00:09:43,000
 behind and not have to deal with our problems.

96
00:09:43,000 --> 00:09:49,000
 That's exactly the opposite of what meditation is.

97
00:09:49,000 --> 00:09:54,650
 It's none of the problems that we face have anything to do

98
00:09:54,650 --> 00:09:59,000
 with the world around us, not directly.

99
00:09:59,000 --> 00:10:05,040
 The problems have to do with how we face, how we react, how

100
00:10:05,040 --> 00:10:08,000
 we judge our interactions with the world around us.

101
00:10:08,000 --> 00:10:16,660
 And all of these judgments can be seen sitting alone in the

102
00:10:16,660 --> 00:10:22,000
 room with no external stimulus.

103
00:10:22,000 --> 00:10:27,470
 Meditation is like a laboratory where you're going to

104
00:10:27,470 --> 00:10:31,000
 experiment on yourself.

105
00:10:31,000 --> 00:10:36,600
 You're going to examine yourself and observe the nature of

106
00:10:36,600 --> 00:10:44,000
 you as a being, as an entity.

107
00:10:44,000 --> 00:10:50,660
 And so you're going to see how you're going to see life,

108
00:10:50,660 --> 00:10:55,000
 you're going to see your life,

109
00:10:55,000 --> 00:11:05,000
 how you behave, how you react, you're going to see it all.

110
00:11:05,000 --> 00:11:12,120
 Meditation doesn't take you away from your problems. It

111
00:11:12,120 --> 00:11:14,000
 dissects them, breaks them up into pieces,

112
00:11:14,000 --> 00:11:20,000
 and shows you what's going on behind the scenes.

113
00:11:20,000 --> 00:11:26,000
 So you can expect your meditation to be unstable.

114
00:11:26,000 --> 00:11:31,240
 Watching the stomach, you think the goal is to observe the

115
00:11:31,240 --> 00:11:35,000
 stomach rising and falling smoothly.

116
00:11:35,000 --> 00:11:39,400
 This is not at all the case. The rising and falling is a

117
00:11:39,400 --> 00:11:44,000
 perfect example of how the world is not stable,

118
00:11:44,000 --> 00:11:58,000
 how reality is not predictable.

119
00:11:58,000 --> 00:12:01,440
 Meditation isn't even a predictable set of changes. It's

120
00:12:01,440 --> 00:12:03,000
 unpredictable.

121
00:12:03,000 --> 00:12:06,680
 Just when we think we've got it, we've got the hang of this

122
00:12:06,680 --> 00:12:08,000
 meditation thing.

123
00:12:08,000 --> 00:12:13,000
 It'll change in a way we never thought it would.

124
00:12:13,000 --> 00:12:18,000
 It has to. Meditation has to challenge us.

125
00:12:18,000 --> 00:12:23,110
 It's going to change in all ways possible until finally we

126
00:12:23,110 --> 00:12:25,000
're able to adapt.

127
00:12:25,000 --> 00:12:31,060
 Finally we become flexible and able to deal with change

128
00:12:31,060 --> 00:12:32,000
 truly,

129
00:12:32,000 --> 00:12:40,660
 so that change doesn't ever bother us in any way, shape or

130
00:12:40,660 --> 00:12:42,000
 form.

131
00:12:44,000 --> 00:12:47,230
 We should expect meditation to be uncomfortable, unsatisf

132
00:12:47,230 --> 00:12:48,000
ying.

133
00:12:48,000 --> 00:12:52,140
 We should expect it to show us all sorts of unsatisfying

134
00:12:52,140 --> 00:12:53,000
 things.

135
00:12:53,000 --> 00:12:57,830
 We should expect to realize that the things that we have

136
00:12:57,830 --> 00:13:02,000
 thought were satisfying are unsatisfying.

137
00:13:02,000 --> 00:13:07,950
 We should expect it to show us again and again things that

138
00:13:07,950 --> 00:13:09,000
 upset us

139
00:13:09,000 --> 00:13:13,000
 until finally we're no longer upset by them,

140
00:13:13,000 --> 00:13:20,200
 until finally we're content regardless of what we get or

141
00:13:20,200 --> 00:13:21,000
 don't get,

142
00:13:21,000 --> 00:13:26,000
 what we gain or what we lose,

143
00:13:26,000 --> 00:13:33,000
 until finally our happiness is unrelated to our possessions

144
00:13:33,000 --> 00:13:38,000
, unrelated to our situation, our circumstances.

145
00:13:40,000 --> 00:13:48,080
 Our peace of mind is imperterbed by, not perturbed by the

146
00:13:48,080 --> 00:13:54,000
 stimuli of the world around us.

147
00:13:54,000 --> 00:14:05,000
 So we shouldn't expect meditation to be comfortable.

148
00:14:05,000 --> 00:14:10,000
 The desire for happiness and pleasure and so on.

149
00:14:10,000 --> 00:14:14,000
 This is what we're studying.

150
00:14:14,000 --> 00:14:25,000
 This is what we're meditating on.

151
00:14:25,000 --> 00:14:29,000
 This is what we're working on to become content,

152
00:14:29,000 --> 00:14:34,140
 because true contentment can't exist in the face of wants

153
00:14:34,140 --> 00:14:39,000
 and needs and hopes and desires.

154
00:14:39,000 --> 00:14:42,000
 You can never be content when you still want things.

155
00:14:42,000 --> 00:14:49,000
 And getting what you want doesn't cure the want.

156
00:14:49,000 --> 00:14:55,000
 It actually feeds it, leads to more and greater wants.

157
00:14:55,000 --> 00:15:01,000
 And so we can never be content this way.

158
00:15:01,000 --> 00:15:11,000
 We should expect meditation to be uncontrollable.

159
00:15:11,000 --> 00:15:14,490
 I think meditation is about controlling your mind, not to

160
00:15:14,490 --> 00:15:23,000
 think, not to feel, not to react.

161
00:15:23,000 --> 00:15:28,500
 The path to not reacting, the path to being at peace is not

162
00:15:28,500 --> 00:15:30,000
 about control.

163
00:15:30,000 --> 00:15:34,970
 Any control that we gain, no matter how pleasant or

164
00:15:34,970 --> 00:15:37,000
 powerful it might be,

165
00:15:37,000 --> 00:15:43,000
 is only an illusion, it's only a temporary state.

166
00:15:43,000 --> 00:15:48,000
 This illusion of control because of cause and effect.

167
00:15:48,000 --> 00:15:52,350
 If it were control, we could just put a pin in it and stick

168
00:15:52,350 --> 00:15:56,000
 it there forever and not have to work at it.

169
00:15:56,000 --> 00:15:58,710
 But the only way you can control your mind is to work and

170
00:15:58,710 --> 00:16:00,000
 work and work,

171
00:16:00,000 --> 00:16:03,000
 cultivate it into a certain state.

172
00:16:03,000 --> 00:16:08,000
 And all it takes is for you to stop cultivating it.

173
00:16:08,000 --> 00:16:12,140
 And even the insights you gain through meditation will

174
00:16:12,140 --> 00:16:13,000
 disappear.

175
00:16:13,000 --> 00:16:19,260
 Unless you can learn to let go, unless you can learn to

176
00:16:19,260 --> 00:16:24,000
 give up yourself,

177
00:16:24,000 --> 00:16:34,000
 the need for control.

178
00:16:34,000 --> 00:16:39,000
 The idea is for us to see how out of control our minds are,

179
00:16:39,000 --> 00:16:42,660
 how out of control the world around us is and the world

180
00:16:42,660 --> 00:16:44,000
 inside of us is.

181
00:16:44,000 --> 00:16:45,000
 This is the idea.

182
00:16:45,000 --> 00:16:49,010
 Meditation is supposed to be unpleasant, unsatisfying,

183
00:16:49,010 --> 00:16:50,000
 uncontrollable,

184
00:16:50,000 --> 00:16:53,000
 impermanent, unstable, all of these things.

185
00:16:53,000 --> 00:16:57,000
 Why? Because it's showing us this about the world.

186
00:16:57,000 --> 00:17:00,000
 It's showing us what's wrong with the world.

187
00:17:00,000 --> 00:17:02,670
 Why is the world causing us suffering? Why is it unsatisf

188
00:17:02,670 --> 00:17:03,000
ying?

189
00:17:03,000 --> 00:17:06,000
 Why aren't we already at peace with ourselves?

190
00:17:06,000 --> 00:17:09,150
 This is what it's showing us. It's giving us an answer to

191
00:17:09,150 --> 00:17:13,000
 the question why.

192
00:17:13,000 --> 00:17:18,660
 It's showing us the building blocks of our experience, of

193
00:17:18,660 --> 00:17:22,000
 our reality.

194
00:17:22,000 --> 00:17:27,360
 And that's where the cure lies, with this wisdom, this

195
00:17:27,360 --> 00:17:30,000
 insight that comes from meditation.

196
00:17:30,000 --> 00:17:33,180
 Not from finding something that's going to please us or

197
00:17:33,180 --> 00:17:34,000
 satisfy us,

198
00:17:34,000 --> 00:17:42,000
 that we can rest upon.

199
00:17:42,000 --> 00:17:46,640
 The only rest that we have, the only peace that we can hope

200
00:17:46,640 --> 00:17:47,000
 for,

201
00:17:47,000 --> 00:17:50,930
 is when we don't need anything, when we no longer cling to

202
00:17:50,930 --> 00:17:52,000
 anything.

203
00:17:52,000 --> 00:17:58,000
 It's the only surefire way, the only certain solution.

204
00:17:58,000 --> 00:18:05,320
 Because as long as your freedom or happiness or peace

205
00:18:05,320 --> 00:18:09,000
 depends upon something,

206
00:18:09,000 --> 00:18:15,000
 you'll always be dissatisfied when that something is gone.

207
00:18:15,000 --> 00:18:20,370
 The only way to maintain it is to jump and chase and run

208
00:18:20,370 --> 00:18:25,000
 after this elusive state of

209
00:18:25,000 --> 00:18:34,000
 temporary satisfaction or peace.

210
00:18:34,000 --> 00:18:38,650
 In Buddhism we talk about something called nirvana or nibb

211
00:18:38,650 --> 00:18:40,000
ana.

212
00:18:40,000 --> 00:18:43,290
 And so we think, well that's something I should chase after

213
00:18:43,290 --> 00:18:44,000
 or run after.

214
00:18:44,000 --> 00:18:48,360
 In a sense you'd be right, but chasing and running doesn't

215
00:18:48,360 --> 00:18:49,000
 lead to nirvana,

216
00:18:49,000 --> 00:19:00,000
 nirvana is the state of not clinging.

217
00:19:00,000 --> 00:19:04,720
 That's maybe not fair to say because we're not always

218
00:19:04,720 --> 00:19:06,000
 clinging.

219
00:19:06,000 --> 00:19:10,190
 Nirvana is freedom from suffering, it is true freedom from

220
00:19:10,190 --> 00:19:11,000
 suffering.

221
00:19:11,000 --> 00:19:17,000
 But the past is nibbana or nirvana.

222
00:19:17,000 --> 00:19:20,440
 It's to let go of everything because nirvana isn't really a

223
00:19:20,440 --> 00:19:25,000
 thing, it's freedom.

224
00:19:25,000 --> 00:19:29,000
 Freedom from all the suffering that comes from clinging,

225
00:19:29,000 --> 00:19:38,320
 from expecting, from wanting, from identifying with

226
00:19:38,320 --> 00:19:42,000
 anything.

227
00:19:42,000 --> 00:19:47,000
 So it's good that we come to practice meditation,

228
00:19:47,000 --> 00:19:53,540
 it's good that we seek that which is free from impermanent

229
00:19:53,540 --> 00:19:55,000
 suffering and non-self,

230
00:19:55,000 --> 00:19:58,190
 it's good that we seek that which is free from these

231
00:19:58,190 --> 00:20:00,000
 problems.

232
00:20:00,000 --> 00:20:03,360
 But we shouldn't expect meditation to be free from these

233
00:20:03,360 --> 00:20:04,000
 problems.

234
00:20:04,000 --> 00:20:09,000
 The purpose of meditation is to show these problems to us,

235
00:20:09,000 --> 00:20:12,000
 to help us to let go.

236
00:20:12,000 --> 00:20:16,390
 The more we see of this nature of reality, the more

237
00:20:16,390 --> 00:20:20,000
 inclined we are to let go.

238
00:20:20,000 --> 00:20:24,000
 Naturally, you don't have to force yourself to let go,

239
00:20:24,000 --> 00:20:27,000
 this is counterproductive.

240
00:20:27,000 --> 00:20:32,000
 All you have to do is look and see and learn,

241
00:20:32,000 --> 00:20:37,000
 and once you see the truth you'll let go.

242
00:20:37,000 --> 00:20:41,620
 You'll be free when you're free, there's nothing left to do

243
00:20:41,620 --> 00:20:42,000
.

244
00:20:42,000 --> 00:20:46,000
 That's it.

245
00:20:46,000 --> 00:20:50,000
 That's the path.

246
00:20:50,000 --> 00:20:54,000
 And that's the Dharma for tonight.

247
00:20:54,000 --> 00:20:58,000
 So thanks for tuning in.

248
00:20:58,000 --> 00:21:01,000
 See you all next time.

