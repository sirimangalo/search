1
00:00:00,000 --> 00:00:04,820
 Okay, so welcome everyone to tonight's monk radio. It's

2
00:00:04,820 --> 00:00:06,320
 April 1st

3
00:00:06,320 --> 00:00:09,420
 2012

4
00:00:09,420 --> 00:00:11,420
 but

5
00:00:11,420 --> 00:00:15,440
 No jokes for us all seriousness today

6
00:00:15,440 --> 00:00:21,050
 And first thing I'd like to introduce our panel for this

7
00:00:21,050 --> 00:00:23,860
 evening. I'm you to demo

8
00:00:26,240 --> 00:00:30,190
 Most of you are familiar with me. This is Tarindu to say

9
00:00:30,190 --> 00:00:31,180
 hello Tarindu

10
00:00:31,180 --> 00:00:51,440
 This is good even good good night

11
00:00:51,440 --> 00:00:56,080
 This is Sumedha you can introduce her

12
00:00:57,560 --> 00:01:01,380
 Yeah, next to me is sitting Sumedha and

13
00:01:01,380 --> 00:01:10,520
 And at the end is Palanyani who is our good evening or good

14
00:01:10,520 --> 00:01:15,840
 morning first resident bikwini

15
00:01:15,840 --> 00:01:20,560
 So announcement

16
00:01:20,560 --> 00:01:22,600
 I

17
00:01:22,840 --> 00:01:25,550
 Guess for those of you who don't know him before someone

18
00:01:25,550 --> 00:01:30,320
 asks a Nagasena left us. Well, I don't want to say too much

19
00:01:30,320 --> 00:01:30,600
 but

20
00:01:30,600 --> 00:01:36,040
 We're happy to have him and now he's gone on to do other

21
00:01:36,040 --> 00:01:40,560
 things we have any other announcements anything

22
00:01:40,560 --> 00:01:45,160
 Happening

23
00:01:45,160 --> 00:01:48,520
 It was

24
00:01:48,600 --> 00:01:52,380
 Might be going to Thailand in May. I don't see how that's

25
00:01:52,380 --> 00:01:52,920
 really

26
00:01:52,920 --> 00:01:56,870
 Of interest to people here. Ah, yes. Yes. I know what the

27
00:01:56,870 --> 00:01:58,800
 big announcement was is that

28
00:01:58,800 --> 00:02:02,520
 Sumedha

29
00:02:02,520 --> 00:02:05,280
 Actually, I think she's still referring to herself as as

30
00:02:05,280 --> 00:02:08,940
 Meredith, but we're not so

31
00:02:08,940 --> 00:02:13,680
 She is planning to ordain

32
00:02:14,480 --> 00:02:18,520
 If all goes as planned and it's still not 100% sure the

33
00:02:18,520 --> 00:02:21,280
 date is not 100% sure but on

34
00:02:21,280 --> 00:02:24,600
 May 5th

35
00:02:24,600 --> 00:02:29,200
 Which is does anyone have any idea what you're gonna see if

36
00:02:29,200 --> 00:02:30,080
 anyone knew?

37
00:02:30,080 --> 00:02:35,540
 We suck we suck is on the 5th of May which is the Buddha's

38
00:02:35,540 --> 00:02:36,000
 birthday

39
00:02:36,000 --> 00:02:40,370
 His enlightenment and his brainy bhana or his final passing

40
00:02:40,370 --> 00:02:42,920
 away. It's the celebration of that

41
00:02:42,920 --> 00:02:45,640
 This was a full moon celebration. So many of you are

42
00:02:45,640 --> 00:02:46,960
 probably familiar with it

43
00:02:46,960 --> 00:02:49,470
 And I was just gonna say that on the off chance that

44
00:02:49,470 --> 00:02:50,000
 someone's

45
00:02:50,000 --> 00:02:52,640
 Not busy that day

46
00:02:52,640 --> 00:02:56,600
 You're all welcome to to come out. That's gonna be May 5th

47
00:02:56,600 --> 00:02:59,880
 2012 or

48
00:02:59,880 --> 00:03:03,200
 2555 by this singhalese reckoning

49
00:03:03,200 --> 00:03:09,160
 Her her parents or at least her father will come so

50
00:03:09,800 --> 00:03:13,880
 So yeah, just the father will come so everybody

51
00:03:13,880 --> 00:03:17,720
 Can from America can join him?

52
00:03:17,720 --> 00:03:22,840
 He's kind of he's the unknown right because he might not be

53
00:03:22,840 --> 00:03:25,540
 available on the 5th of May

54
00:03:25,540 --> 00:03:29,950
 So pending his availability, it'll be the 5th of May.

55
00:03:29,950 --> 00:03:33,040
 Otherwise, it might be a little later a little earlier

56
00:03:33,040 --> 00:03:35,040
 We have to talk about that

57
00:03:35,040 --> 00:03:39,000
 That's all for announcements

58
00:03:39,000 --> 00:03:41,000
 you

59
00:03:41,000 --> 00:03:43,000
 you

