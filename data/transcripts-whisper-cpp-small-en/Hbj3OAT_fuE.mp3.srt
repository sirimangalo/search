1
00:00:00,000 --> 00:00:07,600
 The next question comes from Zach Earthworm.

2
00:00:07,600 --> 00:00:11,250
 In one video you mentioned that there is a popular notion

3
00:00:11,250 --> 00:00:12,880
 that is a myth, that is that

4
00:00:12,880 --> 00:00:16,120
 people who are bad get along well while people who are good

5
00:00:16,120 --> 00:00:17,280
 don't succeed.

6
00:00:17,280 --> 00:00:21,380
 What can you say about the similar myth, as in the adage,

7
00:00:21,380 --> 00:00:23,240
 ignorance is bliss?

8
00:00:23,240 --> 00:00:29,390
 Well, ignorance is a word that can be interpreted in

9
00:00:29,390 --> 00:00:33,120
 different ways and can be used in different

10
00:00:33,120 --> 00:00:35,460
 ways.

11
00:00:35,460 --> 00:00:38,240
 I would say we acknowledge generally that there are two

12
00:00:38,240 --> 00:00:39,480
 kinds of ignorance.

13
00:00:39,480 --> 00:00:44,880
 There's ignorance of ordinary things and then there's

14
00:00:44,880 --> 00:00:48,600
 ignorance that leads to defilement,

15
00:00:48,600 --> 00:00:54,840
 that leads to greed or anger, that leads us to evil.

16
00:00:54,840 --> 00:00:59,290
 Ignorance of ordinary things of facts is like not knowing

17
00:00:59,290 --> 00:01:02,280
 mathematics or geography or history

18
00:01:02,280 --> 00:01:05,990
 or so on and not knowing people's names or so on, being

19
00:01:05,990 --> 00:01:08,120
 ignorant even of customs and

20
00:01:08,120 --> 00:01:13,480
 traditions and so on, being ignorant of concepts.

21
00:01:13,480 --> 00:01:16,630
 Being ignorant of ultimate reality is the one that we're

22
00:01:16,630 --> 00:01:18,640
 concerned about, being ignorant

23
00:01:18,640 --> 00:01:23,520
 about the nature of reality.

24
00:01:23,520 --> 00:01:27,040
 Is ignorance bliss?

25
00:01:27,040 --> 00:01:29,640
 I would say ignorance of certain concepts can be blissful

26
00:01:29,640 --> 00:01:30,900
 because it means you don't

27
00:01:30,900 --> 00:01:38,760
 have to involve yourself in that field.

28
00:01:38,760 --> 00:01:43,680
 It can often absolve you from any need to help people or so

29
00:01:43,680 --> 00:01:44,280
 on.

30
00:01:44,280 --> 00:01:47,460
 I would say people who are simply concerned for their own

31
00:01:47,460 --> 00:01:49,480
 happiness can find a great amount

32
00:01:49,480 --> 00:01:54,940
 of bliss from not studying and not learning about worldly

33
00:01:54,940 --> 00:01:56,020
 things.

34
00:01:56,020 --> 00:01:59,210
 People who are concerned with finding peace and happiness

35
00:01:59,210 --> 00:02:00,960
 don't have to study, don't have

36
00:02:00,960 --> 00:02:06,920
 to engage themselves in learning things.

37
00:02:06,920 --> 00:02:11,780
 But if you want to be happy even just for yourself or even

38
00:02:11,780 --> 00:02:14,080
 if you want to help others,

39
00:02:14,080 --> 00:02:16,640
 you have to give up the ignorance in regards to reality.

40
00:02:16,640 --> 00:02:20,370
 What we're trying to do in Buddhism is do away with the

41
00:02:20,370 --> 00:02:22,760
 ignorance which obviously gets

42
00:02:22,760 --> 00:02:26,900
 in the way of bliss, gets in the way of happiness, gets in

43
00:02:26,900 --> 00:02:29,360
 the way of peace and that's ignorance

44
00:02:29,360 --> 00:02:32,200
 of the way things work, of reality.

45
00:02:32,200 --> 00:02:34,590
 Specifically in Buddhism we talk about ignorance in regards

46
00:02:34,590 --> 00:02:35,880
 to four things and these are the

47
00:02:35,880 --> 00:02:38,280
 four noble truths.

48
00:02:38,280 --> 00:02:40,840
 Ignorance in regards to suffering.

49
00:02:40,840 --> 00:02:44,510
 That means we see certain objects as being desirable, as

50
00:02:44,510 --> 00:02:46,600
 being satisfying, that there

51
00:02:46,600 --> 00:02:49,330
 are certain things in this world that are going to, when we

52
00:02:49,330 --> 00:02:50,760
 attain them or obtain them,

53
00:02:50,760 --> 00:02:53,670
 are going to satisfy us, are going to make us truly happy

54
00:02:53,670 --> 00:02:55,520
 and that's simply not the case.

55
00:02:55,520 --> 00:02:58,680
 So it's a case of ignorance.

56
00:02:58,680 --> 00:03:02,520
 The second one is ignorance of the cause of suffering.

57
00:03:02,520 --> 00:03:09,700
 Assess that the arising of our states of upset, our states

58
00:03:09,700 --> 00:03:14,640
 of suffering, of pain, of discomfort

59
00:03:14,640 --> 00:03:19,280
 and so on have a cause and what that cause is.

60
00:03:19,280 --> 00:03:22,570
 So often we'll think the cause is the object itself and we

61
00:03:22,570 --> 00:03:24,880
'll try to run away from it.

62
00:03:24,880 --> 00:03:29,990
 We'll think that the cause is someone else or the cause is

63
00:03:29,990 --> 00:03:32,800
 the experience but the Buddha

64
00:03:32,800 --> 00:03:36,090
 thought that the cause is our attachment to the object, our

65
00:03:36,090 --> 00:03:37,520
 needing for things to be a

66
00:03:37,520 --> 00:03:42,120
 certain way or to not be a certain way, needing for certain

67
00:03:42,120 --> 00:03:44,640
 objects of the sense and for the

68
00:03:44,640 --> 00:03:50,320
 absence of other objects of the sense.

69
00:03:50,320 --> 00:03:54,260
 So ignorance, not knowing this is something that obviously

70
00:03:54,260 --> 00:03:56,160
 leads us to give rise to these

71
00:03:56,160 --> 00:04:00,520
 states when we know that clinging and needing now leads to

72
00:04:00,520 --> 00:04:02,720
 suffering then we stop needing,

73
00:04:02,720 --> 00:04:06,480
 we stop desiring and so we stop suffering.

74
00:04:06,480 --> 00:04:09,600
 The third one is ignorance of the cessation of suffering

75
00:04:09,600 --> 00:04:11,280
 which is when there's no more

76
00:04:11,280 --> 00:04:16,200
 needing, no more wanting, there's no more suffering.

77
00:04:16,200 --> 00:04:20,070
 And number four is ignorance of the path, so ignorance of

78
00:04:20,070 --> 00:04:22,200
 the way that leads us to become

79
00:04:22,200 --> 00:04:24,980
 free from suffering and that is the clear awareness and

80
00:04:24,980 --> 00:04:26,720
 understanding of things as they

81
00:04:26,720 --> 00:04:28,360
 are.

82
00:04:28,360 --> 00:04:31,540
 So this is the only real wisdom that we need and this is

83
00:04:31,540 --> 00:04:33,520
 the understanding of things as

84
00:04:33,520 --> 00:04:37,600
 they are, the understanding of reality.

85
00:04:37,600 --> 00:04:41,660
 You don't have to understand everything, you don't have to

86
00:04:41,660 --> 00:04:44,000
 know everything, to be blissful,

87
00:04:44,000 --> 00:04:47,790
 to be happy and often ignorance of many things as I said

88
00:04:47,790 --> 00:04:50,120
 can be blissful but ignorance of

89
00:04:50,120 --> 00:04:53,400
 these four things is never blissful and will certainly not

90
00:04:53,400 --> 00:04:55,200
 bring you peace or happiness.

91
00:04:55,200 --> 00:04:58,530
 Okay so thanks for the question, hope that works for an

92
00:04:58,530 --> 00:04:59,240
 answer.

