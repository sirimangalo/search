1
00:00:00,000 --> 00:00:09,660
 Okay, hello everyone, we're broadcasting live November 10th

2
00:00:09,660 --> 00:00:17,000
, 2015.

3
00:00:17,000 --> 00:00:22,560
 What's our quote look like today?

4
00:00:22,560 --> 00:00:31,560
 And with one's own good and the good of others.

5
00:00:31,560 --> 00:00:34,560
 I want to read us the quote rather.

6
00:00:34,560 --> 00:00:35,560
 Sure.

7
00:00:35,560 --> 00:00:38,830
 How is one concerned with his own good and the good of

8
00:00:38,830 --> 00:00:39,560
 others.

9
00:00:39,560 --> 00:00:42,880
 Concerning this, a certain person is quick, he grasps

10
00:00:42,880 --> 00:00:44,560
 teachings that are profitable.

11
00:00:44,560 --> 00:00:47,260
 He has learned by heart and by understanding both the

12
00:00:47,260 --> 00:00:50,890
 letter and spirit of the Dhamma, and walks in accordance

13
00:00:50,890 --> 00:00:51,560
 with it.

14
00:00:51,560 --> 00:00:55,810
 He also has a beautiful voice and delivery possesses urbane

15
00:00:55,810 --> 00:01:00,200
 speech, distinctly and clearly enunciate so as to make his

16
00:01:00,200 --> 00:01:01,560
 meaning clear.

17
00:01:01,560 --> 00:01:13,530
 He teaches urges insights and gladdens his companions and

18
00:01:13,530 --> 00:01:20,560
 holy life.

19
00:01:20,560 --> 00:01:24,560
 It's actually four types of people in the sutta.

20
00:01:24,560 --> 00:01:31,550
 There's the person who works for their own benefit and not

21
00:01:31,550 --> 00:01:34,560
 for the benefit of others.

22
00:01:34,560 --> 00:01:39,650
 One who works for the benefit of others and not for one's

23
00:01:39,650 --> 00:01:40,560
 own benefit.

24
00:01:40,560 --> 00:01:44,560
 And one who neither works for one's own benefit of others

25
00:01:44,560 --> 00:01:48,560
 and then one who works for both.

26
00:01:48,560 --> 00:01:52,490
 So one's own benefit is being quick in attending to wholes

27
00:01:52,490 --> 00:01:55,600
ome teachings, being able to retain in mind the teachings

28
00:01:55,600 --> 00:01:58,560
 one has heard,

29
00:01:58,560 --> 00:02:02,380
 and examining the meaning of the teachings and then

30
00:02:02,380 --> 00:02:05,560
 practicing in according to the Dhamma.

31
00:02:05,560 --> 00:02:09,040
 So we have attending to the teachings, retaining them in

32
00:02:09,040 --> 00:02:11,560
 mind, examining the meaning and practicing.

33
00:02:11,560 --> 00:02:16,250
 These are the four, I think these are the four qualities of

34
00:02:16,250 --> 00:02:17,560
 a bahusutta.

35
00:02:17,560 --> 00:02:20,920
 Someone who has said to have learned much in the Buddhist

36
00:02:20,920 --> 00:02:23,560
 teaching is attending to them.

37
00:02:23,560 --> 00:02:29,360
 Kipanipati, I think, something that's quick to attend. Kip

38
00:02:29,360 --> 00:02:31,560
anisanti.

39
00:02:31,560 --> 00:02:49,780
 Kipanipati. Quick observation, quick to pay attention maybe

40
00:02:49,780 --> 00:02:50,560
.

41
00:02:50,560 --> 00:02:59,160
 Sutanan, sutanan chadamananda rakchat jatigoi. Second is

42
00:02:59,160 --> 00:02:59,560
 someone who memorizes,

43
00:02:59,560 --> 00:03:03,560
 able to retain in mind the teachings he has heard.

44
00:03:03,560 --> 00:03:07,560
 Number three, examine the meanings.

45
00:03:07,560 --> 00:03:14,560
 Examine the meaning of the teachings you have retained.

46
00:03:14,560 --> 00:03:20,260
 So it's not enough just to listen, but you have to be

47
00:03:20,260 --> 00:03:24,560
 someone who is keen to listen, keen to hear.

48
00:03:24,560 --> 00:03:27,850
 You have to be able to retain, apart from that, you have to

49
00:03:27,850 --> 00:03:29,560
 be able to keep it in mind.

50
00:03:29,560 --> 00:03:34,920
 This is a skill we have to learn, to find ways to remember

51
00:03:34,920 --> 00:03:37,560
 things, write it down.

52
00:03:37,560 --> 00:03:44,900
 I remember recording all of Ajahn Tong's teachings on mini-

53
00:03:44,900 --> 00:03:45,560
discs.

54
00:03:45,560 --> 00:03:49,980
 I don't know where those mini-discs are, I think I left

55
00:03:49,980 --> 00:03:52,560
 them in Sri Lanka, but I have the WAV files somewhere,

56
00:03:52,560 --> 00:03:53,560
 hopefully.

57
00:03:53,560 --> 00:03:57,560
 I transferred it all to WAV eventually.

58
00:03:57,560 --> 00:04:00,510
 But I used to listen to these talks and try to figure out

59
00:04:00,510 --> 00:04:03,560
 what he was saying, because he'd quote Pali.

60
00:04:03,560 --> 00:04:15,560
 But his pronunciation is a little bit unique.

61
00:04:15,560 --> 00:04:18,810
 So trying to look up the Pali, I would spend a lot of time

62
00:04:18,810 --> 00:04:21,560
 searching for these things.

63
00:04:21,560 --> 00:04:25,320
 Once I figured out exactly what he was quoting, I would

64
00:04:25,320 --> 00:04:29,560
 write it down and commit it to memory.

65
00:04:29,560 --> 00:04:38,560
 So that's important.

66
00:04:38,560 --> 00:04:42,500
 But then not only we're keeping it in memory, you also have

67
00:04:42,500 --> 00:04:43,560
 to consider it.

68
00:04:43,560 --> 00:04:46,690
 You have to think about how it relates to you and your

69
00:04:46,690 --> 00:04:47,560
 practice.

70
00:04:47,560 --> 00:04:50,560
 You have to think about what it exactly really means.

71
00:04:50,560 --> 00:05:03,560
 And consider what is the true meaning of the teachings.

72
00:05:03,560 --> 00:05:05,560
 And finally you have to put it into practice.

73
00:05:05,560 --> 00:05:09,560
 Practice and according to it. That's the fourth.

74
00:05:09,560 --> 00:05:12,560
 So that is what's good for one's self.

75
00:05:12,560 --> 00:05:17,560
 Someone who does that is working for one's own welfare.

76
00:05:17,560 --> 00:05:21,060
 But if one does all that and is not a good speaker, or is a

77
00:05:21,060 --> 00:05:23,560
 speaker but doesn't have a good delivery,

78
00:05:23,560 --> 00:05:28,350
 doesn't brush up their speech, that it becomes polished,

79
00:05:28,350 --> 00:05:31,560
 clear, articulate, expressive of the meaning.

80
00:05:31,560 --> 00:05:36,120
 And one does not instruct, encourage, inspire, and gladden

81
00:05:36,120 --> 00:05:37,560
 one's fellow monks.

82
00:05:37,560 --> 00:05:41,560
 And one is not working for the welfare of others.

83
00:05:41,560 --> 00:05:44,730
 So to work for the welfare of others, you have to be a good

84
00:05:44,730 --> 00:05:46,560
 speaker with a good delivery.

85
00:05:46,560 --> 00:05:50,560
 You have to be gifted with speech that is polished, clear,

86
00:05:50,560 --> 00:05:52,560
 articulate, expressive of the meaning.

87
00:05:52,560 --> 00:06:00,910
 And you have to use that speech to encourage, inspire, and

88
00:06:00,910 --> 00:06:05,560
 gladden others in the dhamma.

89
00:06:05,560 --> 00:06:08,550
 So again the Buddha is not saying that you have to actually

90
00:06:08,550 --> 00:06:09,560
 do one or the other.

91
00:06:09,560 --> 00:06:13,830
 He is just pointing out what is to one's own welfare, what

92
00:06:13,830 --> 00:06:15,560
 is to the welfare of others.

93
00:06:15,560 --> 00:06:17,560
 You choose.

94
00:06:17,560 --> 00:06:21,420
 Just don't be, I think there is an implication, don't be

95
00:06:21,420 --> 00:06:24,070
 the person who works neither for one's own welfare or

96
00:06:24,070 --> 00:06:25,560
 another.

97
00:06:25,560 --> 00:06:28,880
 And probably don't be the one, well pretty clearly, don't

98
00:06:28,880 --> 00:06:31,560
 be the one who works only for the welfare of others.

99
00:06:31,560 --> 00:06:33,560
 Because that's counterproductive.

100
00:06:33,560 --> 00:06:40,610
 If everyone is working for the benefit of others, then no

101
00:06:40,610 --> 00:06:44,560
 one actually ever benefits.

102
00:06:44,560 --> 00:06:46,560
 So do we have any questions today?

103
00:06:46,560 --> 00:06:50,730
 We do. We can start with the three we answered last night

104
00:06:50,730 --> 00:06:53,610
 when we thought we were still on air after the technical

105
00:06:53,610 --> 00:06:54,560
 difficulties.

106
00:06:54,560 --> 00:06:57,610
 Right, we had a good session after you all went away after

107
00:06:57,610 --> 00:07:01,560
 we were sitting there and we said, "It's not live."

108
00:07:01,560 --> 00:07:05,220
 But I'm pretty sure after the computer crashed it did say

109
00:07:05,220 --> 00:07:06,560
 live at one time.

110
00:07:06,560 --> 00:07:08,570
 For a moment and then it was gone all of a sudden and we

111
00:07:08,570 --> 00:07:09,560
 recorded any of it.

112
00:07:09,560 --> 00:07:12,560
 We went on and on answering questions.

113
00:07:12,560 --> 00:07:15,900
 So the first one in that category was, "Is it considered a

114
00:07:15,900 --> 00:07:18,560
 good deed to care for the environment?"

115
00:07:18,560 --> 00:07:23,560
 Right, and I said it's a bit of a roundabout deed.

116
00:07:23,560 --> 00:07:27,580
 It's a good deed insofar as it helps beings, but a tree isn

117
00:07:27,580 --> 00:07:29,560
't considered a being.

118
00:07:29,560 --> 00:07:32,320
 So helping a tree isn't considered a good deed

119
00:07:32,320 --> 00:07:33,560
 intrinsically.

120
00:07:33,560 --> 00:07:36,560
 Now that being said, good deeds are all in the mind.

121
00:07:36,560 --> 00:07:39,860
 So if you help a tree thinking that the tree feels and that

122
00:07:39,860 --> 00:07:42,660
 the tree benefits from it, then it actually is to some

123
00:07:42,660 --> 00:07:43,560
 extent a good deed.

124
00:07:43,560 --> 00:07:48,240
 To some extent it's problematic because it involves falsity

125
00:07:48,240 --> 00:07:48,560
.

126
00:07:48,560 --> 00:07:52,560
 It's like a child who takes care of their stuffed animal.

127
00:07:52,560 --> 00:07:56,430
 And in taking care of their stuffed animal, they develop

128
00:07:56,430 --> 00:07:58,560
 love and they develop compassion and so on.

129
00:07:58,560 --> 00:08:02,440
 But there is a problem because it's not real and there's a

130
00:08:02,440 --> 00:08:05,560
 bit of delusion involved with the child.

131
00:08:05,560 --> 00:08:08,440
 We would say probably with the person who looks after the

132
00:08:08,440 --> 00:08:10,560
 tree because they anthropomorphize it.

133
00:08:10,560 --> 00:08:13,590
 They give it qualities that it doesn't possess, saying the

134
00:08:13,590 --> 00:08:15,560
 tree can feel pain, etc., etc.

135
00:08:15,560 --> 00:08:20,560
 But in fact there's no reason to think that the tree cares.

136
00:08:20,560 --> 00:08:25,460
 I think some people would argue that and say, "Oh no, trees

137
00:08:25,460 --> 00:08:26,560
 feel..."

138
00:08:26,560 --> 00:08:31,760
 But there's not enough, even if it is considered to be

139
00:08:31,760 --> 00:08:36,560
 sentient, which I think it's pretty clearly not.

140
00:08:36,560 --> 00:08:47,560
 If a tree were sentient, I don't think a tree is sentient.

141
00:08:47,560 --> 00:08:53,560
 Is all thinking useless?

142
00:08:53,560 --> 00:09:00,580
 It's not useless because some thinking can lead to benefit,

143
00:09:00,580 --> 00:09:03,220
 can lead you closer to nirvana, can be of support on the

144
00:09:03,220 --> 00:09:03,560
 path.

145
00:09:03,560 --> 00:09:07,390
 But thinking is intrinsically valueless in the sense that

146
00:09:07,390 --> 00:09:09,560
 in the end you have to throw away all thought.

147
00:09:09,560 --> 00:09:13,060
 In the end you don't keep any of it. You don't hold on to

148
00:09:13,060 --> 00:09:15,560
 any of it as having any value.

149
00:09:15,560 --> 00:09:19,560
 But that has to come through some thought to some extent.

150
00:09:19,560 --> 00:09:25,910
 You have to use thought in order to realize that thought is

151
00:09:25,910 --> 00:09:27,560
 valueless.

152
00:09:27,560 --> 00:09:33,800
 I mean, you don't have to, but generally it's useful for

153
00:09:33,800 --> 00:09:35,560
 most people.

154
00:09:35,560 --> 00:09:41,560
 But intrinsically no, intrinsically valueless.

155
00:09:41,560 --> 00:09:45,810
 How did monks measure their meditation time in ancient

156
00:09:45,810 --> 00:09:46,560
 times?

157
00:09:46,560 --> 00:09:51,850
 One way that my teacher taught us, teacher mentioned, was

158
00:09:51,850 --> 00:09:53,560
 to use incense sticks.

159
00:09:53,560 --> 00:09:58,840
 You measure walking by one incense stick when it's burnt

160
00:09:58,840 --> 00:10:01,560
 down, and that's the end.

161
00:10:01,560 --> 00:10:05,560
 And then you do sitting with one instant stick.

162
00:10:05,560 --> 00:10:07,850
 And of course you could cut them in half. Anything with

163
00:10:07,850 --> 00:10:08,560
 fire, there's lots of things.

164
00:10:08,560 --> 00:10:11,870
 Fire, water is another way, sand is another way, time

165
00:10:11,870 --> 00:10:12,560
 pieces.

166
00:10:12,560 --> 00:10:23,580
 There were ancient ways to keep time. Even the sun you

167
00:10:23,580 --> 00:10:29,560
 could use.

168
00:10:29,560 --> 00:10:32,560
 Hello, Banté. I want to start learning Pali language.

169
00:10:32,560 --> 00:10:35,560
 Can you please give me some suggestions? Thanks.

170
00:10:35,560 --> 00:10:41,560
 Spend your time meditating. Here's a person who's yellow.

171
00:10:41,560 --> 00:10:45,560
 Well, that was yesterday. That was yesterday.

172
00:10:45,560 --> 00:10:52,560
 Oh, yeah. Okay. So it's yellow for a reason.

173
00:10:52,560 --> 00:10:55,770
 Yeah, well, don't bother too much. If you're a monk, it's a

174
00:10:55,770 --> 00:10:56,560
 good thing.

175
00:10:56,560 --> 00:10:58,620
 If you're not a monk, it's generally a bit difficult

176
00:10:58,620 --> 00:11:01,560
 because you've got lots of things,

177
00:11:01,560 --> 00:11:03,560
 other things to think about.

178
00:11:03,560 --> 00:11:05,860
 So the amount of Pali that you're going to learn and that

179
00:11:05,860 --> 00:11:10,560
 you're going to use is limited.

180
00:11:10,560 --> 00:11:18,560
 If you're still dedicated to learning Pali,

181
00:11:18,560 --> 00:11:20,560
 you have to find someone who teaches it properly.

182
00:11:20,560 --> 00:11:25,110
 There are ways of learning it out there, but most of them

183
00:11:25,110 --> 00:11:27,560
 are not based in the English ones.

184
00:11:27,560 --> 00:11:29,870
 Most of them are not based on the traditional way of

185
00:11:29,870 --> 00:11:30,560
 learning.

186
00:11:30,560 --> 00:11:34,600
 So I tried to put together a course, put together a little

187
00:11:34,600 --> 00:11:35,560
 sort of a guidebook

188
00:11:35,560 --> 00:11:41,640
 and of course that went along traditional lines, but I've

189
00:11:41,640 --> 00:11:53,560
 never really had enough time to take it seriously.

190
00:11:53,560 --> 00:11:57,060
 Are there other states during meditation that can seem like

191
00:11:57,060 --> 00:11:57,560
 nirvana?

192
00:11:57,560 --> 00:12:00,850
 In other words, a peaceful trance where one doesn't feel

193
00:12:00,850 --> 00:12:03,560
 body or time and there is nothing to note?

194
00:12:03,560 --> 00:12:10,140
 That's nothing like nirvana. What you're describing is

195
00:12:10,140 --> 00:12:14,560
 nothing like nirvana.

196
00:12:14,560 --> 00:12:18,560
 I don't know. I mean, it's all words, right?

197
00:12:18,560 --> 00:12:22,560
 But if it seems like anything, then it's not nirvana.

198
00:12:22,560 --> 00:12:28,560
 If it feels peaceful, then it's not nirvana.

199
00:12:28,560 --> 00:12:34,830
 If it feels quiet, it's still not nirvana. There's no

200
00:12:34,830 --> 00:12:37,560
 feeling in nirvana.

201
00:12:37,560 --> 00:12:41,970
 A little earlier, the person had noted he was not sure if n

202
00:12:41,970 --> 00:12:45,560
irvana, he or she, was not sure if nirvana or fell asleep.

203
00:12:45,560 --> 00:12:54,560
 So maybe fell asleep.

204
00:12:54,560 --> 00:12:58,110
 Sometimes when a thought form arises, I just note it and it

205
00:12:58,110 --> 00:13:01,240
 stops, but other times I get engrossed and lose myself in

206
00:13:01,240 --> 00:13:01,560
 it.

207
00:13:01,560 --> 00:13:04,560
 How can I stop that?

208
00:13:04,560 --> 00:13:07,950
 You can't. It's not under your control. That's a part of

209
00:13:07,950 --> 00:13:11,560
 what we're learning, that we're not in charge.

210
00:13:11,560 --> 00:13:15,580
 Of course, if you practice more, you'll cultivate the habit

211
00:13:15,580 --> 00:13:19,980
 of being able to catch things and slowly lean in that

212
00:13:19,980 --> 00:13:21,560
 direction.

213
00:13:21,560 --> 00:13:26,990
 But it's a habit. You're not in control. You can't just

214
00:13:26,990 --> 00:13:28,560
 stop things.

215
00:13:28,560 --> 00:13:32,560
 What you can do is build habits and change your habits.

216
00:13:32,560 --> 00:13:37,560
 That's what the meditation is meant to accomplish.

217
00:13:37,560 --> 00:13:39,560
 It just takes time, though.

218
00:13:39,560 --> 00:13:53,560
 [silence]

219
00:13:53,560 --> 00:13:58,560
 We are all caught up and still lies.

220
00:13:58,560 --> 00:14:03,560
 Right.

221
00:14:03,560 --> 00:14:07,500
 Okay, well that's all then. Dhammapada again tomorrow,

222
00:14:07,500 --> 00:14:08,560
 hopefully.

223
00:14:08,560 --> 00:14:12,810
 Today we have a woman from Switzerland visiting. She just

224
00:14:12,810 --> 00:14:15,560
 came for the evening. I think she's left already.

225
00:14:15,560 --> 00:14:21,560
 But she visits her aunt in Toronto. So she came by here.

226
00:14:21,560 --> 00:14:25,830
 Took a bus all the way from Toronto and the bus back in the

227
00:14:25,830 --> 00:14:32,560
 rain. That's dedication.

228
00:14:32,560 --> 00:14:35,770
 That's good to hear that people are finding that where you

229
00:14:35,770 --> 00:14:40,560
 are there. That's wonderful.

230
00:14:40,560 --> 00:14:43,560
 Okay, good night. Thank you, Robin.

231
00:14:43,560 --> 00:14:46,560
 Thank you, Bante. Good night.

