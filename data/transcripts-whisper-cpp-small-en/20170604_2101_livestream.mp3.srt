1
00:00:00,000 --> 00:00:12,150
 metropolitan, very urban. And then there were these mystics

2
00:00:12,150 --> 00:00:14,480
, shamans, is where the word shaman

3
00:00:14,480 --> 00:00:20,930
 probably comes from, shramana, who were from a fairly

4
00:00:20,930 --> 00:00:24,280
 indigenous culture, but they were these

5
00:00:24,280 --> 00:00:30,130
 wise men mostly who went off in the forest and had deep

6
00:00:30,130 --> 00:00:33,680
 mystical experiences, or maybe they were

7
00:00:33,680 --> 00:00:41,290
 just crazy, but were looked upon as holy men. And so there

8
00:00:41,290 --> 00:00:45,280
's, as society developed, there was a

9
00:00:45,280 --> 00:00:50,590
 question of how these men fit in and what was the proper,

10
00:00:50,590 --> 00:00:54,560
 as everything, as all these indigenous

11
00:00:54,560 --> 00:00:59,240
 cultures go, they eventually become institutionalized. And

12
00:00:59,240 --> 00:01:02,280
 so eventually they tried at least, it seems,

13
00:01:02,280 --> 00:01:07,800
 to set up some kind of system because it was disruptive for

14
00:01:07,800 --> 00:01:10,520
 men to just decide, and women

15
00:01:10,520 --> 00:01:14,600
 sometimes that they were going to leave home and go off and

16
00:01:14,600 --> 00:01:19,640
 live in the forest. And so they made

17
00:01:19,640 --> 00:01:24,240
 these four ages. The proper way to do it was to wait until

18
00:01:24,240 --> 00:01:27,520
 you're old and retire. You're a student

19
00:01:27,520 --> 00:01:34,780
 and then you're a householder and then you retire, but live

20
00:01:34,780 --> 00:01:39,120
 at home. And then once everything is

21
00:01:39,120 --> 00:01:42,530
 settled, passed on to your children, you'll even go off and

22
00:01:42,530 --> 00:01:47,040
 live in the forest. The sannyasa stage.

23
00:01:47,040 --> 00:01:54,220
 And so this is what the Buddha came into and the Buddha was

24
00:01:54,220 --> 00:01:58,600
 of course one of these men, although he

25
00:01:58,600 --> 00:02:04,130
 didn't wait to retire. He was quite the rebel. He left home

26
00:02:04,130 --> 00:02:06,920
 when he was 29 apparently.

27
00:02:06,920 --> 00:02:18,320
 Still just preparing to become leader of his clan. But I

28
00:02:18,320 --> 00:02:31,480
 think what you can see from the Buddha's

29
00:02:31,480 --> 00:02:34,350
 teaching, and maybe you can get a sense of where I'm going

30
00:02:34,350 --> 00:02:36,480
 with this, is that there isn't a clear

31
00:02:36,480 --> 00:02:41,150
 division in the Buddha's teaching. There isn't a decision

32
00:02:41,150 --> 00:02:46,360
 made on one way or the other. I think

33
00:02:46,360 --> 00:02:54,700
 clearly the goal is renouncing the world in one sense, but

34
00:02:54,700 --> 00:02:59,920
 in another sense it's to some extent

35
00:02:59,920 --> 00:03:09,160
 taking other people with you in general. And thereby

36
00:03:09,160 --> 00:03:16,400
 working with the world. Freedom from

37
00:03:16,400 --> 00:03:20,680
 the world doesn't come without the world. It's not like a

38
00:03:20,680 --> 00:03:22,880
 jail cell you can just break out of.

39
00:03:22,880 --> 00:03:27,450
 This is what you're learning in the meditation course. You

40
00:03:27,450 --> 00:03:30,160
 can't just get away from your problems.

41
00:03:30,160 --> 00:03:34,530
 That's not the way out of suffering. That's your problems

42
00:03:34,530 --> 00:03:37,280
 that free you from your problems.

43
00:03:37,280 --> 00:03:42,980
 The satipatthana suta is a very good, it's a very special

44
00:03:42,980 --> 00:03:49,160
 teaching, very powerful teaching. Just

45
00:03:49,160 --> 00:03:52,110
 simple words when you're angry, know that you're angry.

46
00:03:52,110 --> 00:03:55,240
 When you're in pain, know that it's pain.

47
00:03:55,240 --> 00:03:59,500
 When you're walking, know that you're walking. Not running

48
00:03:59,500 --> 00:04:04,480
 away from anything. And the world

49
00:04:04,480 --> 00:04:08,190
 either. To some extent the world is part of our practice.

50
00:04:08,190 --> 00:04:10,400
 Other people are part of our practice.

51
00:04:10,400 --> 00:04:18,400
 And so engagement is a part of our practice. Renunciation

52
00:04:18,400 --> 00:04:23,600
 and engagement. And so when I look

53
00:04:23,600 --> 00:04:27,300
 at Buddhism, I look at this division and I think it's an

54
00:04:27,300 --> 00:04:30,080
 important one. But rather than a division

55
00:04:30,080 --> 00:04:34,870
 among Buddhists, it's a division among Buddhist practices.

56
00:04:34,870 --> 00:04:37,520
 There are social practices and there

57
00:04:37,520 --> 00:04:40,790
 are spiritual practice. They're engaged and they're renunci

58
00:04:40,790 --> 00:04:45,680
ate practices. And both I think arguably

59
00:04:45,680 --> 00:04:49,200
 are, should be, or at least can be part of our path to

60
00:04:49,200 --> 00:04:57,040
 enlightenment. So what I'm going to try

61
00:04:57,040 --> 00:05:01,940
 to do in my talk, in fact this might be exciting, I'm going

62
00:05:01,940 --> 00:05:05,120
 to try to figure out the exact time and

63
00:05:05,120 --> 00:05:08,980
 see if we can do a Second Life session live. So I have all

64
00:05:08,980 --> 00:05:12,080
 of you here with me and then about halfway

65
00:05:12,080 --> 00:05:14,280
 through just pop it up and show that you've all been

66
00:05:14,280 --> 00:05:20,880
 sitting there listening. Just to show the

67
00:05:20,880 --> 00:05:27,870
 power of Second Life really as a communication platform.

68
00:05:27,870 --> 00:05:32,200
 But I'm going to try to highlight some

69
00:05:32,200 --> 00:05:40,320
 of the tools that I've created or some of the means by

70
00:05:40,320 --> 00:05:46,320
 which I've used the internet, used technology.

71
00:05:46,320 --> 00:05:50,390
 And as some other people have created, and I mean talk

72
00:05:50,390 --> 00:05:53,480
 about the Buddha Center for example.

73
00:06:01,320 --> 00:06:05,560
 But I want to make clear this distinction that our social

74
00:06:05,560 --> 00:06:07,240
 practice, I mean a good example of a social

75
00:06:07,240 --> 00:06:17,270
 practice is Metta. Metta is a meditative practice. You just

76
00:06:17,270 --> 00:06:20,600
 sit quietly alone and send love to the

77
00:06:20,600 --> 00:06:30,040
 whole world. But it's social as well in the sense of it

78
00:06:30,040 --> 00:06:35,520
 changes the way we react to others, it

79
00:06:35,520 --> 00:06:45,210
 changes the way we interact with others. We approach other

80
00:06:45,210 --> 00:06:49,160
 people with love as a result. I

81
00:06:49,160 --> 00:06:53,040
 mean when we talk about engaged Buddhism we mean changing

82
00:06:53,040 --> 00:06:55,720
 the world. We mean doing something to

83
00:06:55,720 --> 00:06:59,600
 improve the world not just running away and freeing

84
00:06:59,600 --> 00:07:02,520
 ourselves. The fact is improving your

85
00:07:02,520 --> 00:07:07,540
 own mind does improve the world. Metta is a very social

86
00:07:07,540 --> 00:07:11,680
 practice. Changing yourself is a very social

87
00:07:11,680 --> 00:07:15,830
 practice. To that end, renunciation in a Buddhist sense,

88
00:07:15,830 --> 00:07:18,240
 which of course is internal, it has nothing

89
00:07:18,240 --> 00:07:25,590
 really to do with running away to the forest. It is in fact

90
00:07:25,590 --> 00:07:29,960
 a social practice. I mean part of it

91
00:07:29,960 --> 00:07:34,060
 is social in the sense that it changes your social

92
00:07:34,060 --> 00:07:38,000
 interactions. It makes you wiser. I mean the most

93
00:07:38,000 --> 00:07:42,990
 important thing it does is teaches you. Teaches you right

94
00:07:42,990 --> 00:07:46,920
 from wrong, good from bad. It makes

95
00:07:46,920 --> 00:07:52,620
 your mind clear when dealing with challenges, difficulties,

96
00:07:52,620 --> 00:08:01,440
 conflicts. I mean I don't know if

97
00:08:01,440 --> 00:08:07,940
 if there really is this division in fact. It may be two

98
00:08:07,940 --> 00:08:11,960
 sides of the practice. You know the Buddha

99
00:08:11,960 --> 00:08:14,890
 answering this question of helping yourself or helping

100
00:08:14,890 --> 00:08:16,760
 others. He says when you help yourself

101
00:08:16,760 --> 00:08:19,950
 you help other people. When you help other people you help

102
00:08:19,950 --> 00:08:25,520
 yourself. I don't think there was a

103
00:08:25,520 --> 00:08:30,110
 equivalence there. It's very much clear that helping

104
00:08:30,110 --> 00:08:33,040
 yourself is a much better thing to focus

105
00:08:33,040 --> 00:08:37,900
 on. There are certainly teachings to that extent. Ren

106
00:08:37,900 --> 00:08:40,400
unciation and going off to live in the forest

107
00:08:40,400 --> 00:08:44,940
 is a great and wonderful thing. But it's certainly not all

108
00:08:44,940 --> 00:08:47,120
 of Buddhism and I don't think there's a

109
00:08:47,120 --> 00:08:55,950
 need to create a division of sorts. That's the first part

110
00:08:55,950 --> 00:08:59,840
 of my talk I think and to talk about

111
00:08:59,840 --> 00:09:03,800
 all the technology I use. Of course that's very interesting

112
00:09:03,800 --> 00:09:05,760
 but I don't know that it's a topic

113
00:09:05,760 --> 00:09:10,580
 for tonight's Dhamma. So that's sort of the direction I'm

114
00:09:10,580 --> 00:09:13,600
 heading to talk about. This idea

115
00:09:13,600 --> 00:09:19,530
 of engaged versus renunciate. Renunciant. I don't know the

116
00:09:19,530 --> 00:09:27,080
 actual word. But for us I think that's

117
00:09:27,080 --> 00:09:30,750
 important. It's important to be clear. This isn't running

118
00:09:30,750 --> 00:09:33,240
 away. Coming here isn't running away and

119
00:09:33,240 --> 00:09:36,780
 your practice doesn't end here in the meditation center.

120
00:09:36,780 --> 00:09:39,200
 Mindfulness is a tool that you use in your

121
00:09:39,200 --> 00:09:43,870
 life and it's a tool that you use here to help you with

122
00:09:43,870 --> 00:09:49,040
 your life to better deal with your challenges

123
00:09:49,040 --> 00:09:52,270
 and conflicts and so on. So a little bit of food for

124
00:09:52,270 --> 00:09:56,000
 thought. Dhamma for tonight. Thank you all

125
00:09:56,000 --> 00:10:02,110
 for coming up. And I've got the question page up so I can

126
00:10:02,110 --> 00:10:13,480
 answer some questions here. So completely

127
00:10:13,480 --> 00:10:16,710
 coincidental. The top question and it is completely coinc

128
00:10:16,710 --> 00:10:19,160
idental. The top question is about what my

129
00:10:19,160 --> 00:10:22,690
 thoughts are on important issues like climate change and

130
00:10:22,690 --> 00:10:24,760
 how realization could help ease

131
00:10:24,760 --> 00:10:28,520
 people's bias against these crises. In fact it's a good

132
00:10:28,520 --> 00:10:34,880
 point to bring up in my talk. Because of

133
00:10:34,880 --> 00:10:37,850
 course things like climate change are brought about by

134
00:10:37,850 --> 00:10:40,000
 nothing more than greed at the root.

135
00:10:40,000 --> 00:10:44,940
 You know if we weren't so greedy for consumption even just

136
00:10:44,940 --> 00:10:48,160
 greedy to procreate too many babies.

137
00:10:48,160 --> 00:10:53,160
 As the Dalai Lama said we need more monks. There's too many

138
00:10:53,160 --> 00:10:56,080
 people in this world. I'm not sure about

139
00:10:56,080 --> 00:11:03,910
 that. I mean I don't know what the ethics are there but I

140
00:11:03,910 --> 00:11:08,520
 don't know. I don't know about population

141
00:11:08,520 --> 00:11:13,160
 and we won't touch that one but certainly for consumption.

142
00:11:13,160 --> 00:11:15,840
 Our consumption is way out of hand

143
00:11:15,840 --> 00:11:23,700
 and our need for products regardless of the cost to the

144
00:11:23,700 --> 00:11:28,960
 environment. Our need for instant

145
00:11:28,960 --> 00:11:34,820
 gratification is completely even just our need for meat for

146
00:11:34,820 --> 00:11:38,040
 example. We don't need to eat meat. We

147
00:11:38,040 --> 00:11:43,030
 have no reason to eat meat and it's incredibly expensive to

148
00:11:43,030 --> 00:11:46,040
 the climate and even to our individuals.

149
00:11:46,040 --> 00:11:55,150
 So yeah I think our practice certainly improves these and I

150
00:11:55,150 --> 00:11:57,760
've talked about this before but I

151
00:11:57,760 --> 00:12:01,370
 think things like climate change are not incredibly worth

152
00:12:01,370 --> 00:12:04,000
 focusing on because it's like a band-aid

153
00:12:04,000 --> 00:12:09,060
 solution. Sure you get people to be concerned about the

154
00:12:09,060 --> 00:12:12,000
 climate but if you don't well I mean

155
00:12:12,000 --> 00:12:16,950
 looking at the climate does make people more aware of their

156
00:12:16,950 --> 00:12:19,560
 greed and force people to give

157
00:12:19,560 --> 00:12:23,800
 up their greed but I mean let's be clear that the real

158
00:12:23,800 --> 00:12:27,400
 important the real problem is important

159
00:12:27,400 --> 00:12:37,450
 thing to to to address is our greed. Did the Buddha possess

160
00:12:37,450 --> 00:12:43,360
 any supernatural powers or miracles?

161
00:12:43,360 --> 00:12:51,250
 I mean I've never met the Buddha not in this life obviously

162
00:12:51,250 --> 00:12:55,760
 but I have no problem believing that the

163
00:12:55,760 --> 00:13:03,400
 Buddha had powers beyond what most of us would believe

164
00:13:03,400 --> 00:13:08,780
 possible or powers beyond what most of

165
00:13:08,780 --> 00:13:14,510
 us are capable of. We have to hold our hands up to our

166
00:13:14,510 --> 00:13:17,560
 stomach as opposed to laying them on our

167
00:13:17,560 --> 00:13:21,590
 laps is either position okay yes the body position in in

168
00:13:21,590 --> 00:13:24,000
 our tradition not so important.

169
00:13:24,000 --> 00:13:28,150
 It's all about the mind the mind is what's more important.

170
00:13:28,150 --> 00:13:32,720
 In Theravada I recently read that only

171
00:13:32,720 --> 00:13:35,910
 those who practice meditative monastic life can attain

172
00:13:35,910 --> 00:13:38,560
 spiritual perfection enlightenment

173
00:13:38,560 --> 00:13:41,820
 is not thought possible for those living in the secular

174
00:13:41,820 --> 00:13:44,320
 life. Of course it's easier that way but

175
00:13:44,320 --> 00:13:46,100
 could it be possible yes whoever wrote that doesn't know

176
00:13:46,100 --> 00:13:48,560
 what they're talking about. There's a queen

177
00:13:48,560 --> 00:13:53,000
 who became an arahant very famous example it's very Therav

178
00:13:53,000 --> 00:13:55,720
ada Buddhist queen and the Buddha said ah

179
00:13:55,720 --> 00:13:58,480
 well either she becomes a bhikkhuni or she just wastes away

180
00:13:58,480 --> 00:14:00,040
 because there's no way she's gonna

181
00:14:00,040 --> 00:14:03,540
 be able to be a queen and so the king said well then let

182
00:14:03,540 --> 00:14:10,800
 her become a bhikkhuni. Okay this is a

183
00:14:10,800 --> 00:14:14,530
 long question I can read it all but well you know maybe it

184
00:14:14,530 --> 00:14:17,160
's nice person who has suffered extreme

185
00:14:17,160 --> 00:14:20,540
 stress for many years ten plus years withdrawn into serious

186
00:14:20,540 --> 00:14:22,560
 depression debilitating anxiety

187
00:14:22,560 --> 00:14:25,710
 been seriously studying your YouTube videos for a few

188
00:14:25,710 --> 00:14:27,880
 months but I'm new to consistent daily

189
00:14:27,880 --> 00:14:31,630
 practice and today I don't know if you've picked up my

190
00:14:31,630 --> 00:14:34,240
 booklet so there are many ways to practice

191
00:14:34,240 --> 00:14:37,390
 but I recommend reading my booklet if you haven't today

192
00:14:37,390 --> 00:14:42,240
 experience something wonderful so a sudden

193
00:14:42,240 --> 00:14:47,490
 burst of warm intense happiness tears of joy can't recall

194
00:14:47,490 --> 00:14:50,640
 or such an overwhelming sensation

195
00:14:50,640 --> 00:14:53,690
 came out of nowhere half of me just wants to share my

196
00:14:53,690 --> 00:14:56,040
 excitement and the other half curious

197
00:14:56,040 --> 00:15:01,140
 wants to know is this normal um so well great I mean it's

198
00:15:01,140 --> 00:15:04,200
 great for you to be able to see that

199
00:15:04,200 --> 00:15:12,460
 you're not cursed you're not stuck I would say you can get

200
00:15:12,460 --> 00:15:15,480
 and see that now that your brain is

201
00:15:15,480 --> 00:15:24,470
 capable of pleasure and that the meditation clearly helps

202
00:15:24,470 --> 00:15:28,680
 you break out of this rut because

203
00:15:28,680 --> 00:15:33,750
 our our behaviors become habits and so things like

204
00:15:33,750 --> 00:15:37,260
 depression for 10 years that's got you

205
00:15:37,260 --> 00:15:41,260
 stuck in a fairly deep rut but miracle of miracles just a

206
00:15:41,260 --> 00:15:43,800
 little bit of meditation and you can see

207
00:15:43,800 --> 00:15:47,800
 you can pop yourself back out of it why I mean because

208
00:15:47,800 --> 00:15:51,160
 those 10 years they aren't it's not like

209
00:15:51,160 --> 00:15:55,430
 you add up those 10 years and you've got 10 years worth of

210
00:15:55,430 --> 00:15:58,200
 depression old stuff fades we're still

211
00:15:58,200 --> 00:16:00,790
 only dealing with the present moment so if in this moment

212
00:16:00,790 --> 00:16:02,840
 you start meditating people wonder how can

213
00:16:02,840 --> 00:16:06,450
 a few weeks of meditation counteract years of depression or

214
00:16:06,450 --> 00:16:08,360
 anxieties because those years

215
00:16:08,360 --> 00:16:12,480
 don't exist their past it's just you counting the truth is

216
00:16:12,480 --> 00:16:15,240
 well what are you now and it's based on

217
00:16:15,240 --> 00:16:17,950
 what you've done before there's no question that all of

218
00:16:17,950 --> 00:16:19,840
 that had an effect but you're still just

219
00:16:19,840 --> 00:16:24,570
 dealing with you and your brain right and so creating new

220
00:16:24,570 --> 00:16:28,200
 habits of mindfulness I mean it works it

221
00:16:28,200 --> 00:16:33,010
 pops you out of it so your next step of course is to learn

222
00:16:33,010 --> 00:16:36,360
 to become mindful of the joy because

223
00:16:36,360 --> 00:16:39,420
 that's not the goal it's a good sign it's a sign that you

224
00:16:39,420 --> 00:16:41,840
're learning to let go of the depression

225
00:16:41,840 --> 00:16:46,660
 and so on and find ways to be more centered more more

226
00:16:46,660 --> 00:16:50,720
 peaceful but the joy itself is something that

227
00:16:50,720 --> 00:16:52,580
 you have to know it otherwise you get attached to it and

228
00:16:52,580 --> 00:16:54,880
 you'll be looking for it and you'll even

229
00:16:54,880 --> 00:16:58,400
 get upset when it doesn't come I mean you'll you'll see

230
00:16:58,400 --> 00:17:01,560
 this as you practice but just to give you a

231
00:17:01,560 --> 00:17:04,290
 bit of a shortcut so you don't get caught up in it you don

232
00:17:04,290 --> 00:17:06,320
't get caught up in anything the Buddha

233
00:17:06,320 --> 00:17:11,160
 said nothing is worth clinging to so congratulations good

234
00:17:11,160 --> 00:17:14,600
 for you but the next step when and maybe it

235
00:17:14,600 --> 00:17:18,900
 won't come again maybe not for a long time maybe never but

236
00:17:18,900 --> 00:17:21,280
 learn to deal with it when it comes

237
00:17:21,280 --> 00:17:26,090
 a pleasure when it comes and desire for it excitement about

238
00:17:26,090 --> 00:17:28,120
 it all of that is part of the

239
00:17:28,120 --> 00:17:33,480
 practice it's not to take away from this experience it's a

240
00:17:33,480 --> 00:17:36,360
 great sign it's just onward and upward

241
00:17:36,360 --> 00:17:40,330
 there's there's more to do good for you really good really

242
00:17:40,330 --> 00:17:42,760
 nice to hear thank you for writing

243
00:17:42,760 --> 00:17:47,040
 that out I suffer from low self-esteem and it can only be

244
00:17:47,040 --> 00:17:49,960
 boosted through activities how can I

245
00:17:49,960 --> 00:17:53,980
 cultivate it permanently well we don't need self-esteem we

246
00:17:53,980 --> 00:17:57,200
're worthless you are worthless I am worthless

247
00:17:57,200 --> 00:18:01,710
 if you can if you can figure that out then you don't have

248
00:18:01,710 --> 00:18:05,200
 to worry about self-esteem stop

249
00:18:05,200 --> 00:18:09,220
 esteeming yourself it's useless we are worthless we are

250
00:18:09,220 --> 00:18:12,680
 worthless means all of this is worthless

251
00:18:12,680 --> 00:18:16,350
 meaningless doesn't it's what you make of it you can

252
00:18:16,350 --> 00:18:19,640
 pretend that this house and your job and your

253
00:18:19,640 --> 00:18:24,790
 face and your body has some meaning your brain your

254
00:18:24,790 --> 00:18:29,160
 intelligence your memory you can claim that

255
00:18:29,160 --> 00:18:33,730
 it all has meaning and worth but it's all just subjective

256
00:18:33,730 --> 00:18:37,720
 it's what you make of it and that

257
00:18:37,720 --> 00:18:40,670
 frees you seeing that frees you because then you can look

258
00:18:40,670 --> 00:18:42,880
 at well what's really important and what's

259
00:18:42,880 --> 00:18:49,620
 really important is happiness and suffering peace and

260
00:18:49,620 --> 00:18:54,880
 stress I have sense of always being alone even

261
00:18:54,880 --> 00:18:57,970
 when with others in groups not a sense of loneliness but it

262
00:18:57,970 --> 00:19:00,240
's like I'm the only one there is this normal

263
00:19:00,240 --> 00:19:04,120
 so these questions of what is normal are not I've talked

264
00:19:04,120 --> 00:19:06,720
 about this before it's not proper to ask

265
00:19:06,720 --> 00:19:10,520
 I should put it in the things please don't ask is it normal

266
00:19:10,520 --> 00:19:13,040
 or please you know rephrase it and ask

267
00:19:13,040 --> 00:19:16,190
 yourself what do you really want to know and you want to

268
00:19:16,190 --> 00:19:18,400
 know whether you are a freak and is that

269
00:19:18,400 --> 00:19:22,060
 what you're asking because you are you and that's most

270
00:19:22,060 --> 00:19:25,280
 important it's most important to see what is

271
00:19:25,280 --> 00:19:30,220
 asking what's normal doesn't do us any good because who

272
00:19:30,220 --> 00:19:33,680
 cares maybe normal is a horrible thing to be

273
00:19:33,680 --> 00:19:39,250
 right we're not trying to be normal we're trying to be good

274
00:19:39,250 --> 00:19:41,920
 and pure and and perfect to some extent

275
00:19:41,920 --> 00:19:46,590
 free we're trying to be free so good question would be is

276
00:19:46,590 --> 00:19:49,440
 that is that a positive state is it

277
00:19:49,440 --> 00:19:51,960
 a negative state those would be better questions to ask I

278
00:19:51,960 --> 00:19:54,240
 mean I'm not criticizing this or people

279
00:19:54,240 --> 00:19:57,680
 ask this all the time but it's just a I think it may be a

280
00:19:57,680 --> 00:19:59,840
 little lazy because we you know

281
00:19:59,840 --> 00:20:03,480
 is this normal it doesn't it's not a useful question to got

282
00:20:03,480 --> 00:20:06,160
 it twice tonight and I'm not

283
00:20:06,160 --> 00:20:08,460
 I'm not attacking you for it I think I would probably ask

284
00:20:08,460 --> 00:20:09,280
 the same thing but

285
00:20:09,280 --> 00:20:12,760
 it's you have to think it through what do you really want

286
00:20:12,760 --> 00:20:16,240
 to know so good question is is it

287
00:20:16,240 --> 00:20:22,350
 is it good is it bad those are probably more important so

288
00:20:22,350 --> 00:20:25,040
 it's neither I mean it's an experience

289
00:20:25,600 --> 00:20:30,270
 so try to note it and be mindful of your feeling feeling or

290
00:20:30,270 --> 00:20:33,440
 something if you like it or dislike it

291
00:20:33,440 --> 00:20:40,110
 you know that and so on okay and that's all the questions

292
00:20:40,110 --> 00:20:43,760
 for tonight thank you all for tuning in

293
00:20:43,760 --> 00:20:51,680
 have a good night

