1
00:00:00,000 --> 00:00:07,710
 Hey everyone, just another video quickly to announce that

2
00:00:07,710 --> 00:00:12,080
 we have created a video archive

3
00:00:12,080 --> 00:00:18,490
 for the Ask a Monk, Monk Radio question and answer videos

4
00:00:18,490 --> 00:00:21,540
 that I've made over the years.

5
00:00:21,540 --> 00:00:26,880
 So there's hundreds of them and I'm not sure I don't go

6
00:00:26,880 --> 00:00:29,880
 looking for them myself except

7
00:00:29,880 --> 00:00:33,000
 when I need a particular one that I know what I'm looking

8
00:00:33,000 --> 00:00:33,680
 for.

9
00:00:33,680 --> 00:00:39,510
 So I'm just assuming that it's difficult to find the

10
00:00:39,510 --> 00:00:43,680
 subject that you're looking for.

11
00:00:43,680 --> 00:00:47,290
 I think especially because I see the same questions being

12
00:00:47,290 --> 00:00:48,960
 asked again and again.

13
00:00:48,960 --> 00:00:55,770
 So it seemed to be quite worthwhile to somehow categorize

14
00:00:55,770 --> 00:00:59,560
 and offer the videos up in a more

15
00:00:59,560 --> 00:01:05,000
 organized fashion than YouTube allows easily.

16
00:01:05,000 --> 00:01:11,600
 So with that in mind, I created a page on our website that

17
00:01:11,600 --> 00:01:14,360
 has a list of videos and

18
00:01:14,360 --> 00:01:18,800
 it's been put together by volunteers who have been

19
00:01:18,800 --> 00:01:22,280
 following my web blog and Facebook who

20
00:01:22,280 --> 00:01:28,290
 have been adding videos over the past week or so and it now

21
00:01:28,290 --> 00:01:31,480
 has I think 700 videos or

22
00:01:31,480 --> 00:01:32,980
 so on it.

23
00:01:32,980 --> 00:01:36,290
 And there may be duplicates because some videos fit in more

24
00:01:36,290 --> 00:01:37,600
 than one category.

25
00:01:37,600 --> 00:01:41,290
 But the point is that those videos are up now and they're

26
00:01:41,290 --> 00:01:43,240
 listed, they're all still

27
00:01:43,240 --> 00:01:44,480
 hosted on YouTube.

28
00:01:44,480 --> 00:01:52,510
 It's just that we've organized them hopefully in a manner

29
00:01:52,510 --> 00:01:56,960
 that allows easier finding of

30
00:01:56,960 --> 00:02:00,470
 the videos that you're looking for, the subject that you

31
00:02:00,470 --> 00:02:02,000
 have a question about.

32
00:02:02,000 --> 00:02:05,610
 So go over there if you're looking for something on a

33
00:02:05,610 --> 00:02:08,360
 particular topic or even if you're not

34
00:02:08,360 --> 00:02:09,800
 just go and browse through them.

35
00:02:09,800 --> 00:02:14,600
 And I think a lot of people will be surprised to see just

36
00:02:14,600 --> 00:02:17,280
 how many videos there are and

37
00:02:17,280 --> 00:02:21,300
 probably find videos that they had never watched before on

38
00:02:21,300 --> 00:02:23,680
 topics that they were interested

39
00:02:23,680 --> 00:02:24,680
 in.

40
00:02:24,680 --> 00:02:26,120
 So hopefully that's a good resource.

41
00:02:26,120 --> 00:02:29,510
 I think it's something that's highly overdue and something

42
00:02:29,510 --> 00:02:31,120
 that we will keep adding to

43
00:02:31,120 --> 00:02:32,620
 in the future.

44
00:02:32,620 --> 00:02:35,830
 If you'd like to help, there's a special admin page that

45
00:02:35,830 --> 00:02:38,080
 many people have been using to actually

46
00:02:38,080 --> 00:02:42,110
 add videos there, but I'm not going to make that too open

47
00:02:42,110 --> 00:02:44,160
 because I figure if there's

48
00:02:44,160 --> 00:02:48,840
 too many people adding to it, then it just gets chaotic.

49
00:02:48,840 --> 00:02:52,750
 So if you'd like to help and you know of videos that should

50
00:02:52,750 --> 00:02:55,040
 be on there or you see something

51
00:02:55,040 --> 00:02:58,890
 wrong with the page, just let me know and I'll send you the

52
00:02:58,890 --> 00:03:00,760
 link and you can help out.

53
00:03:00,760 --> 00:03:03,360
 Otherwise head on over there.

54
00:03:03,360 --> 00:03:09,660
 The URL is in the description to this video, but it is

55
00:03:09,660 --> 00:03:14,520
 simply video.siri-mongolo.org.

56
00:03:14,520 --> 00:03:18,750
 So if you head on over there, you can access the list and

57
00:03:18,750 --> 00:03:20,440
 see what you think.

58
00:03:20,440 --> 00:03:21,440
 Let me know.

59
00:03:21,440 --> 00:03:22,440
 I hope that's useful.

60
00:03:22,440 --> 00:03:23,440
 Thank you all.

61
00:03:23,440 --> 00:03:27,880
 Keep meditating and keep practicing and be well, everyone.

