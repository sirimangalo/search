1
00:00:00,000 --> 00:00:26,920
 [

2
00:00:42,840 --> 00:00:24,080
 No, it's not time yet. You have a few more weeks, right? On

3
00:00:24,080 --> 00:00:48,440
 the full moon?

4
00:00:48,440 --> 00:00:50,440
 I click family.

5
00:00:50,440 --> 00:00:54,440
 I'm not supposed to click on that link.

6
00:00:54,440 --> 00:01:16,440
 Hi.

7
00:01:18,440 --> 00:01:20,440
 I don't know who you are. What's your name?

8
00:01:20,440 --> 00:01:22,440
 My name is Anne.

9
00:01:22,440 --> 00:01:24,440
 Hi Anne.

10
00:01:24,440 --> 00:01:26,440
 You're live on the internet.

11
00:01:26,440 --> 00:01:30,440
 You're live on the internet.

12
00:01:30,440 --> 00:01:34,440
 I wasn't expecting that. I just clicked the link.

13
00:01:34,440 --> 00:01:36,440
 Life is full of unexpected things.

14
00:01:36,440 --> 00:01:38,440
 That's okay.

15
00:01:38,440 --> 00:01:40,440
 You're now part of our broadcast.

16
00:01:40,440 --> 00:01:44,120
 Can we pull her up if I go like this? Does everyone see her

17
00:01:44,120 --> 00:01:44,440
?

18
00:01:44,440 --> 00:01:52,220
 I think that everyone sees our profiles of smaller pictures

19
00:01:52,220 --> 00:01:52,440
.

20
00:01:52,440 --> 00:01:54,440
 There you go.

21
00:01:54,440 --> 00:02:08,440
 I can't force everyone to see her.

22
00:02:08,440 --> 00:02:10,440
 Ah, present to everyone.

23
00:02:10,440 --> 00:02:12,440
 Okay.

24
00:02:12,440 --> 00:02:16,440
 This is our guest. Hello, you're now me.

25
00:02:16,440 --> 00:02:19,830
 But it's too high. You need to move your camera down a

26
00:02:19,830 --> 00:02:20,440
 little bit.

27
00:02:20,440 --> 00:02:22,440
 Now you can see my dirty room.

28
00:02:22,440 --> 00:02:26,440
 Well done. Well done. I understand.

29
00:02:26,440 --> 00:02:28,440
 Okay, I'm going to stop presenting.

30
00:02:28,440 --> 00:02:30,440
 I'm going to be back a little bit.

31
00:02:30,440 --> 00:02:34,440
 So Anne.

32
00:02:34,440 --> 00:02:36,440
 Welcome Anne.

33
00:02:36,440 --> 00:02:40,440
 Did you have a question for us tonight?

34
00:02:40,440 --> 00:02:42,440
 You want to tell us about yourself?

35
00:02:42,440 --> 00:02:45,430
 Okay, tell us about yourself first. Do you practice

36
00:02:45,430 --> 00:02:46,440
 meditation?

37
00:02:46,440 --> 00:02:48,440
 Yes.

38
00:02:48,440 --> 00:02:50,440
 How long have you been practicing meditation for?

39
00:02:50,440 --> 00:02:58,440
 On and off for about maybe six or seven years.

40
00:02:58,440 --> 00:03:02,580
 For the last six months I've been practicing Buddhist

41
00:03:02,580 --> 00:03:04,440
 meditation regularly.

42
00:03:04,440 --> 00:03:08,440
 Okay. Have you tried practicing the way we practice?

43
00:03:08,440 --> 00:03:10,440
 Yes.

44
00:03:10,440 --> 00:03:12,440
 Have you read my booklet?

45
00:03:12,440 --> 00:03:14,440
 My question tonight is about my reading.

46
00:03:14,440 --> 00:03:16,440
 Did you read my booklet on how to meditate?

47
00:03:16,440 --> 00:03:22,440
 I watched the video on swimming and walking meditation.

48
00:03:22,440 --> 00:03:24,440
 Okay.

49
00:03:24,440 --> 00:03:29,610
 There's a booklet that explains it probably a little bit

50
00:03:29,610 --> 00:03:30,440
 clearer.

51
00:03:30,440 --> 00:03:34,440
 And if you have a chance I'd recommend you read.

52
00:03:34,440 --> 00:03:37,250
 Speaking of, remind me about that. I have to talk about the

53
00:03:37,250 --> 00:03:38,440
 booklet afterwards.

54
00:03:38,440 --> 00:03:40,440
 But okay, so you have a question. Go ahead.

55
00:03:40,440 --> 00:03:44,440
 You get priority because you were bold enough to join us.

56
00:03:44,440 --> 00:03:48,440
 Even if you wanted to see your own.

57
00:03:48,440 --> 00:03:54,440
 My question is, and I could type it, but is it appropriate

58
00:03:54,440 --> 00:03:58,440
 and is it useful to note

59
00:03:58,440 --> 00:04:03,750
 throughout the day outside of meditation, like washing my

60
00:04:03,750 --> 00:04:04,440
 dishes.

61
00:04:04,440 --> 00:04:09,820
 I noted while I washed my dishes and sometimes sitting at

62
00:04:09,820 --> 00:04:12,440
 my desk at work, I'll try to note that.

63
00:04:12,440 --> 00:04:14,440
 Is that useful?

64
00:04:14,440 --> 00:04:19,420
 Absolutely. In fact, you could say in many ways it's the

65
00:04:19,420 --> 00:04:21,440
 purpose of training in meditation.

66
00:04:21,440 --> 00:04:25,640
 Why we sit, do walking and sitting meditation is to train

67
00:04:25,640 --> 00:04:29,440
 ourselves to be able to be that way in our lives.

68
00:04:29,440 --> 00:04:31,440
 To be mindful during our lives.

69
00:04:31,440 --> 00:04:35,020
 So when you get upset during the day, you can say upset,

70
00:04:35,020 --> 00:04:35,440
 upset.

71
00:04:35,440 --> 00:04:40,420
 Training all the time is incredibly useful because then it

72
00:04:40,420 --> 00:04:41,440
 becomes a habit, right?

73
00:04:41,440 --> 00:04:44,820
 Which is what we're trying for. We're trying to change our

74
00:04:44,820 --> 00:04:45,440
 habits.

75
00:04:45,440 --> 00:04:48,010
 Normally our habit is when we experience things we judge

76
00:04:48,010 --> 00:04:48,440
 them.

77
00:04:48,440 --> 00:04:51,510
 We're very reactionary reacting to just about everything,

78
00:04:51,510 --> 00:04:53,350
 whether it's a thought we have or whether it's an

79
00:04:53,350 --> 00:04:54,440
 experience outside.

80
00:04:54,440 --> 00:05:00,440
 We react liking, disliking, identifying, pushing away.

81
00:05:00,440 --> 00:05:04,440
 So changing that to be objective is incredibly useful.

82
00:05:04,440 --> 00:05:06,440
 It will change your whole life.

83
00:05:06,440 --> 00:05:10,590
 It will free you up from so much of the stress that comes

84
00:05:10,590 --> 00:05:16,440
 from being judgmental and having opinions and worrying

85
00:05:16,440 --> 00:05:16,440
 about things.

86
00:05:16,440 --> 00:05:20,440
 Because you think they're yours and so on.

87
00:05:20,440 --> 00:05:22,750
 So absolutely, if you're washing dishes then you're just

88
00:05:22,750 --> 00:05:23,440
 washing dishes.

89
00:05:23,440 --> 00:05:25,440
 And when it's done, it's done and you feel so much better

90
00:05:25,440 --> 00:05:28,890
 than if you're washing dishes and you were worried about

91
00:05:28,890 --> 00:05:30,440
 your work.

92
00:05:30,440 --> 00:05:32,440
 Your mind is somewhere else.

93
00:05:32,440 --> 00:05:37,160
 The Buddha said, "A person who is not in the present moment

94
00:05:37,160 --> 00:05:40,440
 is like grass that is cut off from the source."

95
00:05:40,440 --> 00:05:44,440
 When you cut grass, it shrivels up and dries.

96
00:05:44,440 --> 00:05:48,620
 And he said, "A person who is cut off from reality is the

97
00:05:48,620 --> 00:05:49,440
 same."

98
00:05:49,440 --> 00:05:52,440
 So what you're doing is connecting yourself with reality

99
00:05:52,440 --> 00:05:58,880
 and this nourishes the mind, keeps the mind fresh and alive

100
00:05:58,880 --> 00:05:59,440
.

101
00:05:59,440 --> 00:06:00,440
 Thank you.

102
00:06:00,440 --> 00:06:03,980
 You should read the booklet and this chapter six talks a

103
00:06:03,980 --> 00:06:05,440
 little bit about that.

104
00:06:05,440 --> 00:06:08,710
 Or anything, if you read anything in this tradition, like

105
00:06:08,710 --> 00:06:11,800
 anything by Mahasi Sayadaw talks all about that kind of

106
00:06:11,800 --> 00:06:12,440
 thing.

107
00:06:12,440 --> 00:06:17,440
 There's lots of good advice.

108
00:06:17,440 --> 00:06:24,440
 I just, I looked at the booklet in the chat there.

109
00:06:24,440 --> 00:06:25,440
 Thank you.

110
00:06:25,440 --> 00:06:27,440
 How is it?

111
00:06:27,440 --> 00:06:28,440
 Where do you live?

112
00:06:28,440 --> 00:06:30,440
 I live in Florida.

113
00:06:30,440 --> 00:06:31,440
 I live in Houston, Florida.

114
00:06:31,440 --> 00:06:33,440
 What part of Florida?

115
00:06:33,440 --> 00:06:35,440
 Central Florida.

116
00:06:35,440 --> 00:06:37,440
 How far from Tampa?

117
00:06:37,440 --> 00:06:40,670
 Well, I'm actually planning on coming and seeing you on the

118
00:06:40,670 --> 00:06:41,440
 26th.

119
00:06:41,440 --> 00:06:42,440
 Oh, awesome.

120
00:06:42,440 --> 00:06:46,440
 Yeah, a couple of hours from Tampa.

121
00:06:46,440 --> 00:06:47,440
 Not too bad.

122
00:06:47,440 --> 00:06:48,440
 Okay.

123
00:06:48,440 --> 00:06:51,170
 I'll be in Dunedin, which is on the coast or near the coast

124
00:06:51,170 --> 00:06:51,440
.

125
00:06:51,440 --> 00:06:52,440
 I love Dunedin.

126
00:06:52,440 --> 00:06:54,440
 Beautiful, though.

127
00:06:54,440 --> 00:06:57,440
 That's where my mother lives.

128
00:06:57,440 --> 00:07:06,250
 My father came to visit today and he thinks I should stay

129
00:07:06,250 --> 00:07:08,440
 in school.

130
00:07:08,440 --> 00:07:17,540
 So that is something, you know, your parents have to think

131
00:07:17,540 --> 00:07:19,440
 about it.

132
00:07:19,440 --> 00:07:22,440
 I submitted an essay on Locke today.

133
00:07:22,440 --> 00:07:31,440
 I think I'll send him my essay for him to read.

134
00:07:31,440 --> 00:07:36,440
 Got a new robe today from, from all of you, actually.

135
00:07:36,440 --> 00:07:39,440
 Many of the people here were involved with that.

136
00:07:39,440 --> 00:07:44,440
 So thank you and blessings to all of you.

137
00:07:44,440 --> 00:07:48,440
 May it bring you peace and happiness to know that you gave

138
00:07:48,440 --> 00:07:52,440
 someone cloth to cover their body.

139
00:07:52,440 --> 00:08:02,370
 A ward of cold and heat and mosquitoes and ticks and to

140
00:08:02,370 --> 00:08:09,440
 cover up the private parts of the body.

141
00:08:09,440 --> 00:08:12,440
 Not to mention the snow in Canada.

142
00:08:12,440 --> 00:08:14,440
 Yeah, still waiting for it.

143
00:08:14,440 --> 00:08:19,440
 I'm told there will be snow.

144
00:08:19,440 --> 00:08:21,440
 Haven't seen any yet?

145
00:08:21,440 --> 00:08:23,440
 We did see a little.

146
00:08:23,440 --> 00:08:32,440
 Yeah, it snowed a couple of times.

147
00:08:32,440 --> 00:08:35,350
 Yeah, the big problem with switching colors is everybody's

148
00:08:35,350 --> 00:08:39,260
 going to comment on it and wonder if you changed, changed

149
00:08:39,260 --> 00:08:40,440
 somehow.

150
00:08:40,440 --> 00:08:41,440
 Did you change schools?

151
00:08:41,440 --> 00:08:43,440
 Are you now a Mahayana monk?

152
00:08:43,440 --> 00:08:45,440
 That's what they think in Thailand.

153
00:08:45,440 --> 00:08:49,660
 When you become Mahayana, I switched to purple for a while,

154
00:08:49,660 --> 00:08:53,440
 the burgundy that they use quite a bit in Sri Lanka.

155
00:08:53,440 --> 00:08:56,440
 Same thing.

156
00:08:56,440 --> 00:08:59,440
 Oh, yeah, so you're a Mahayana monk now.

157
00:08:59,440 --> 00:09:01,440
 Ridiculous.

158
00:09:01,440 --> 00:09:04,360
 Well, that was interesting what you were explaining before

159
00:09:04,360 --> 00:09:06,840
 someone had asked about the darker colors and you were

160
00:09:06,840 --> 00:09:10,080
 explaining that they're a little more affiliated with

161
00:09:10,080 --> 00:09:11,440
 certain groups.

162
00:09:11,440 --> 00:09:15,440
 Most of us just wear whatever we get.

163
00:09:15,440 --> 00:09:18,550
 If you talk to the average monk, they switch and they wear

164
00:09:18,550 --> 00:09:20,440
 this color and then that color.

165
00:09:20,440 --> 00:09:21,440
 Nobody really cares.

166
00:09:21,440 --> 00:09:25,290
 It's only at the big monasteries and the really strict and

167
00:09:25,290 --> 00:09:28,000
 formal schools where for some reason they get caught up in

168
00:09:28,000 --> 00:09:28,440
 colors.

169
00:09:28,440 --> 00:09:31,440
 I mean, it's more for an image than anything.

170
00:09:31,440 --> 00:09:34,440
 It becomes like a uniform.

171
00:09:34,440 --> 00:09:37,690
 But to some extent it was meant to be a uniform, but I don

172
00:09:37,690 --> 00:09:40,440
't think the color was meant to be uniform.

173
00:09:40,440 --> 00:09:43,440
 There certainly was never a color that the Buddha picked.

174
00:09:43,440 --> 00:09:48,400
 He just gave a range because it depends on what dye you use

175
00:09:48,400 --> 00:09:51,440
, what dye you have available.

176
00:09:51,440 --> 00:10:01,440
 Anyway, I'm going to get lots of questions about it.

177
00:10:03,440 --> 00:10:06,440
 What else?

178
00:10:06,440 --> 00:10:09,440
 Something else.

179
00:10:09,440 --> 00:10:20,710
 We have a nice quote today. It's a little long, but it's a

180
00:10:20,710 --> 00:10:24,440
 really good one.

181
00:10:24,440 --> 00:10:26,440
 We have some questions, though?

182
00:10:26,440 --> 00:10:28,440
 We have questions, yes.

183
00:10:28,440 --> 00:10:31,440
 Let's go for the questions then.

184
00:10:31,440 --> 00:10:33,440
 Sure.

185
00:10:33,440 --> 00:10:41,700
 One day I'm traveling abroad with someone I do not get

186
00:10:41,700 --> 00:10:45,440
 along with and I feel terribly homesick.

187
00:10:45,440 --> 00:10:48,220
 How should I deal with these emotions when I feel like this

188
00:10:48,220 --> 00:10:49,440
 time will never end?

189
00:10:49,440 --> 00:10:51,440
 Thank you.

190
00:10:57,440 --> 00:11:01,440
 Have you read my booklet on how to meditate?

191
00:11:01,440 --> 00:11:05,440
 Because it sounds like a fairly simple problem.

192
00:11:05,440 --> 00:11:09,440
 I mean, you have to be mindful.

193
00:11:09,440 --> 00:11:13,440
 I'm not getting along with someone.

194
00:11:13,440 --> 00:11:19,130
 It's partly them, but it's partly you that you get upset

195
00:11:19,130 --> 00:11:20,440
 and so on.

196
00:11:20,440 --> 00:11:23,110
 The fact that you feel terribly homesick, well, it's just a

197
00:11:23,110 --> 00:11:23,440
 feeling.

198
00:11:23,440 --> 00:11:28,460
 If you can remind yourself of that, then that will solve

199
00:11:28,460 --> 00:11:30,440
 the problem there.

200
00:11:30,440 --> 00:11:33,430
 Feeling like the time will never end, well, that's also

201
00:11:33,430 --> 00:11:34,440
 just a feeling.

202
00:11:34,440 --> 00:11:36,440
 It's not going to kill you.

203
00:11:36,440 --> 00:11:39,710
 So if you can learn to see it just as a feeling, then it

204
00:11:39,710 --> 00:11:41,440
 will solve the problem.

205
00:11:41,440 --> 00:11:44,500
 I'd recommend reading my booklet and starting to practice

206
00:11:44,500 --> 00:11:45,440
 accordingly.

207
00:11:45,440 --> 00:11:49,440
 It's quite helpful in that way.

208
00:11:50,440 --> 00:11:53,440
 Thank you, Monté.

209
00:11:53,440 --> 00:11:55,960
 You said when there are more objects, the choice of the

210
00:11:55,960 --> 00:11:58,440
 noted objects should be based on clarity.

211
00:11:58,440 --> 00:12:01,530
 But other times you say that one should note mental states,

212
00:12:01,530 --> 00:12:03,440
 such as wanting or disliking or distracted.

213
00:12:03,440 --> 00:12:07,460
 Does that mean we should give priority to, and/or actively

214
00:12:07,460 --> 00:12:10,440
 search for certain unwholesome states?

215
00:12:10,440 --> 00:12:14,440
 No, I mean, they're present.

216
00:12:14,440 --> 00:12:17,630
 If you have pain, for example, and you don't like it, then

217
00:12:17,630 --> 00:12:19,440
 you should probably note disliking first.

218
00:12:19,440 --> 00:12:24,440
 Usually we ignore the disliking because we cling to it.

219
00:12:24,440 --> 00:12:29,630
 We hold on to the disliking and we look at the pain and say

220
00:12:29,630 --> 00:12:31,440
, "There's the problem, there's the problem."

221
00:12:31,440 --> 00:12:34,580
 And in fact, the problem is here. The problem is in the

222
00:12:34,580 --> 00:12:35,440
 disliking.

223
00:12:35,440 --> 00:12:37,740
 So it's not that it's less clear, it's that we ignore it

224
00:12:37,740 --> 00:12:38,440
 completely.

225
00:12:38,440 --> 00:12:40,760
 You feel happy and you like it, and so you note the

226
00:12:40,760 --> 00:12:42,440
 happiness, but you never note the liking,

227
00:12:42,440 --> 00:12:46,440
 even though the liking is actually clearer.

228
00:12:46,440 --> 00:12:51,440
 It's not that it's not clear.

229
00:12:51,440 --> 00:12:54,440
 But there will be times where the feeling will be clearer,

230
00:12:54,440 --> 00:12:56,440
 then focus on the feeling.

231
00:12:56,440 --> 00:12:59,890
 In fact, you can go back and forth, and there are different

232
00:12:59,890 --> 00:13:00,440
 aspects.

233
00:13:00,440 --> 00:13:03,520
 In a desire state, there's the desire, there's the feeling,

234
00:13:03,520 --> 00:13:05,440
 there's the thoughts about it,

235
00:13:05,440 --> 00:13:08,800
 there's the physical activities, there's the object of your

236
00:13:08,800 --> 00:13:09,440
 desire.

237
00:13:09,440 --> 00:13:14,770
 All of these things at different times can become

238
00:13:14,770 --> 00:13:17,440
 meditation objects.

239
00:13:17,440 --> 00:13:20,580
 You should not leave anyone out, but whichever one is cle

240
00:13:20,580 --> 00:13:31,440
arest in the mind, that's the one you should focus on.

241
00:13:31,440 --> 00:13:34,100
 And the next question is from Anne, so thank you for

242
00:13:34,100 --> 00:13:36,440
 popping on and asking that. That was awesome.

243
00:13:36,440 --> 00:13:40,440
 Anne names herself Amasaro. Is that Pali?

244
00:13:40,440 --> 00:13:44,440
 No, it's my first initial and my last name.

245
00:13:44,440 --> 00:13:49,810
 But I recognized the Asaro, so I looked it up in the Pali

246
00:13:49,810 --> 00:13:51,440
 dictionary,

247
00:13:51,440 --> 00:13:57,440
 and I saw that the A-S-A-R-O means solution or unreality.

248
00:13:57,440 --> 00:14:00,440
 What is your name, Masaro or Asaro?

249
00:14:00,440 --> 00:14:03,440
 The last name is Masaro.

250
00:14:03,440 --> 00:14:07,440
 Looks very Pali. I was trying to figure out a Masaro.

251
00:14:07,440 --> 00:14:12,440
 A Masaro I could probably get, but Masaro, Masa, Masu.

252
00:14:12,440 --> 00:14:16,440
 I think Masu is like, what is Masu?

253
00:14:16,440 --> 00:14:19,440
 I want to say mustache, but I don't think so.

254
00:14:19,440 --> 00:14:31,440
 It's an interesting name, it could almost be Pali.

255
00:14:33,440 --> 00:14:36,310
 And your first question on the robe. Did you level up, B

256
00:14:36,310 --> 00:14:38,440
ante? Brown is good.

257
00:14:38,440 --> 00:14:43,760
 How is an Arahant different from Anagami? Is it a big shift

258
00:14:43,760 --> 00:14:48,440
 from Anagami to Arahant, or is it more gradual?

259
00:14:53,440 --> 00:15:00,540
 It's gradual, but you don't become half of an Arahant. You

260
00:15:00,540 --> 00:15:02,600
 stay an Anagami until the moment of attaining Arahantship,

261
00:15:02,600 --> 00:15:04,440
 and then it's complete.

262
00:15:04,440 --> 00:15:08,940
 It's quite a difference. An Anagami might seem somewhat

263
00:15:08,940 --> 00:15:12,440
 like an Arahant, but they still have ambition.

264
00:15:12,440 --> 00:15:15,440
 They can still have ambition, they can still be confused.

265
00:15:15,440 --> 00:15:18,650
 An Arahant doesn't have any of that. They can still have

266
00:15:18,650 --> 00:15:19,440
 conceit.

267
00:15:20,440 --> 00:15:23,440
 Even an Anagami can have conceit.

268
00:15:23,440 --> 00:15:36,440
 And that is it for questions so far.

269
00:15:45,440 --> 00:15:50,440
 See, there is a word, amasa.

270
00:15:50,440 --> 00:15:56,440
 What does that mean?

271
00:15:56,440 --> 00:16:03,440
 Amasaro means touch. Amasati means to touch.

272
00:16:03,440 --> 00:16:09,370
 Amasaro would be probably one who has touched, or one who

273
00:16:09,370 --> 00:16:14,440
 has been touched, maybe. The touch-er, maybe.

274
00:16:14,440 --> 00:16:19,440
 It means one who has touched nirvana, you could say.

275
00:16:19,440 --> 00:16:25,440
 That's a very good name. That's a pretty awesome name.

276
00:16:25,440 --> 00:16:28,830
 It would be easy to find you a Pali name if you want to ord

277
00:16:28,830 --> 00:16:30,440
ain as a monk someday.

278
00:16:30,440 --> 00:16:32,440
 Okay.

279
00:16:33,440 --> 00:16:36,440
 I shouldn't say I do, but I do.

280
00:16:36,440 --> 00:16:40,440
 But I have children, so...

281
00:16:40,440 --> 00:16:51,440
 Yeah, masu is a beard, right?

282
00:16:51,440 --> 00:16:54,440
 A beard?

283
00:16:55,440 --> 00:17:01,610
 So masara is one who has a beard, probably. Not the Arah. A

284
00:17:01,610 --> 00:17:06,440
rah is odd, but I think you can put rah at the end.

285
00:17:06,440 --> 00:17:20,000
 Masara, one who has a beard or something like that. Not

286
00:17:20,000 --> 00:17:21,440
 exactly.

287
00:17:21,440 --> 00:17:24,440
 Well, you said masara is one who has a beard?

288
00:17:24,440 --> 00:17:28,440
 Masu is a beard, so masuka is one.

289
00:17:28,440 --> 00:17:32,440
 So, would amasaro be one without a beard?

290
00:17:32,440 --> 00:17:36,940
 That's right. Amasuka is one without a beard. A woman. A

291
00:17:36,940 --> 00:17:38,440
 woman is amasuka.

292
00:17:38,440 --> 00:17:41,440
 It wouldn't be a masara.

293
00:17:41,440 --> 00:17:53,440
 Masara has to do with touching.

294
00:17:54,440 --> 00:18:02,440
 But it's more physical than metaphorical.

295
00:18:02,440 --> 00:18:09,440
 Because pusati, or pas... pusati.

296
00:18:09,440 --> 00:18:14,060
 When you touch nimbana, it's a different word that they use

297
00:18:14,060 --> 00:18:14,440
.

298
00:18:14,440 --> 00:18:18,440
 Amas.

299
00:18:19,440 --> 00:18:21,440
 I don't have it.

300
00:18:21,440 --> 00:18:23,440
 I don't have it.

301
00:18:49,440 --> 00:18:51,440
 More questions?

302
00:18:51,440 --> 00:18:55,690
 Yes. What do you think about the Theravadan claim that an

303
00:18:55,690 --> 00:18:57,440
 anagami goes to some kind of heaven after that?

304
00:18:57,440 --> 00:19:00,500
 Okay, okay. This is the same person. He's coming back and

305
00:19:00,500 --> 00:19:02,880
 asking again. I told you, no more questions. You have to go

306
00:19:02,880 --> 00:19:03,440
 and meditate.

307
00:19:03,440 --> 00:19:09,420
 He's got all these anagami questions. I'm not going to

308
00:19:09,420 --> 00:19:10,440
 answer them.

309
00:19:10,440 --> 00:19:13,340
 You want to come here? You meditate with us and ask

310
00:19:13,340 --> 00:19:15,440
 questions about your meditation.

311
00:19:15,440 --> 00:19:18,440
 No more anagami questions. I'm not interested.

312
00:19:18,440 --> 00:19:21,400
 Okay. I'm not sure you like the next question either then,

313
00:19:21,400 --> 00:19:22,440
 but we'll give it a try.

314
00:19:22,440 --> 00:19:26,440
 Are concentration and effort always balanced in Arahant?

315
00:19:26,440 --> 00:19:32,440
 Maybe not.

316
00:19:32,440 --> 00:19:37,240
 I think you might want to say yes, but I think there can be

317
00:19:37,240 --> 00:19:38,440
 an excess.

318
00:19:38,440 --> 00:19:42,660
 I don't think they have to always be balanced. It's

319
00:19:42,660 --> 00:19:44,440
 interesting. Interesting question.

320
00:19:44,440 --> 00:19:46,440
 But again, why?

321
00:19:47,440 --> 00:19:50,440
 Who cares? Go meditate.

322
00:19:50,440 --> 00:19:54,440
 Patrick, isn't this the Patrick who meditates with us?

323
00:19:54,440 --> 00:19:56,440
 It is, yes.

324
00:19:56,440 --> 00:19:57,440
 Why are you yellow?

325
00:19:57,440 --> 00:20:01,710
 Maybe Patrick is meditating after. Oh, Patrick meditates

326
00:20:01,710 --> 00:20:06,070
 all the time. He must be meditating after. Hang out tonight

327
00:20:06,070 --> 00:20:06,440
.

328
00:20:06,440 --> 00:20:10,440
 Go meditate. Stop asking these questions.

329
00:20:10,440 --> 00:20:25,440
 [silence]

330
00:20:25,440 --> 00:20:27,440
 We have a long quote.

331
00:20:27,440 --> 00:20:31,020
 I don't want to get into it. It's been a long day. I've got

332
00:20:31,020 --> 00:20:33,440
 meditators here. I just finished my essay.

333
00:20:33,440 --> 00:20:39,440
 My father came to visit. We had misudimaga this afternoon.

334
00:20:39,440 --> 00:20:42,510
 I think we've done enough today, no? There was a robe

335
00:20:42,510 --> 00:20:43,440
 offering.

336
00:20:43,440 --> 00:20:47,440
 We had a long volunteer meeting.

337
00:20:47,440 --> 00:20:49,440
 How did that go? Tell us about that.

338
00:20:49,440 --> 00:20:53,160
 Well, you actually came in at the end of it. It was a great

339
00:20:53,160 --> 00:20:54,440
 volunteer meeting.

340
00:20:54,440 --> 00:20:56,750
 We started out saying, "This will be short because Banthe

341
00:20:56,750 --> 00:20:58,440
 can't make it to the volunteer meeting.

342
00:20:58,440 --> 00:21:00,440
 We won't have much to talk about."

343
00:21:00,440 --> 00:21:03,440
 We ended up having quite a few things to talk about anyway.

344
00:21:03,440 --> 00:21:08,220
 Towards the end, Banthe arrived. We were trying to plan for

345
00:21:08,220 --> 00:21:11,440
 a small ceremony to offer the robe to Banthe

346
00:21:11,440 --> 00:21:13,930
 and try to figure out how everyone could get there at the

347
00:21:13,930 --> 00:21:14,440
 same time.

348
00:21:14,440 --> 00:21:19,040
 It just wasn't working out because it's tough. It's tough

349
00:21:19,040 --> 00:21:21,440
 to get people in the same place at one time.

350
00:21:21,440 --> 00:21:25,160
 Then we just realized, "Well, the present moment is perfect

351
00:21:25,160 --> 00:21:27,440
. There were some meditators there."

352
00:21:27,440 --> 00:21:30,390
 They presented the robe to Banthe while we were still

353
00:21:30,390 --> 00:21:31,440
 online with them.

354
00:21:31,440 --> 00:21:34,440
 It's all recorded as part of our volunteer meeting.

355
00:21:34,440 --> 00:21:37,440
 We can post that for anybody who would like to see it.

356
00:21:37,440 --> 00:21:41,810
 It was nice. It worked out nicely. Nothing like the present

357
00:21:41,810 --> 00:21:42,440
 moment.

358
00:21:42,440 --> 00:21:49,440
 Anything of interest else happened in the meeting?

359
00:21:49,440 --> 00:21:53,440
 Snowblower.

360
00:21:53,440 --> 00:21:57,340
 We were talking about the fact that your electric bill is

361
00:21:57,340 --> 00:21:58,440
 quite high.

362
00:21:58,440 --> 00:22:00,440
 Electric bill?

363
00:22:00,440 --> 00:22:02,900
 Your electric bill is quite high. It was about $300 for two

364
00:22:02,900 --> 00:22:03,440
 months.

365
00:22:03,440 --> 00:22:08,050
 That seems high given the fact that your heat is natural

366
00:22:08,050 --> 00:22:08,440
 gas.

367
00:22:08,440 --> 00:22:14,440
 The electricity should be just lights and appliances.

368
00:22:14,440 --> 00:22:15,440
 I'm going to...

369
00:22:15,440 --> 00:22:20,920
 Does Aruna use an electric heater in his room? No, I don't

370
00:22:20,920 --> 00:22:21,440
 think so.

371
00:22:21,440 --> 00:22:24,440
 No, he said he wasn't using it.

372
00:22:24,440 --> 00:22:27,320
 I was going to contact the electric company and see if they

373
00:22:27,320 --> 00:22:29,440
 would be willing to do an energy audit.

374
00:22:29,440 --> 00:22:32,440
 Generally, the utilities will do those things.

375
00:22:32,440 --> 00:22:35,660
 If it turns out there's something wrong with the meter, if

376
00:22:35,660 --> 00:22:39,440
 they're charging more wattage than what they should,

377
00:22:39,440 --> 00:22:42,440
 because that does seem a little high.

378
00:22:42,440 --> 00:22:46,020
 Jeff said by comparison his is about $50 a month, although

379
00:22:46,020 --> 00:22:48,440
 he is in a different province.

380
00:22:48,440 --> 00:22:50,440
 He might have lower rates.

381
00:22:50,440 --> 00:22:53,300
 That does seem strange. I'm trying to think. My electric

382
00:22:53,300 --> 00:22:56,440
 heater maybe, but it's not a big one.

383
00:22:56,440 --> 00:23:00,440
 If they do an audit, they can just see if there's something

384
00:23:00,440 --> 00:23:03,440
 radically wrong with the meter,

385
00:23:03,440 --> 00:23:06,440
 or maybe one of the appliances.

386
00:23:06,440 --> 00:23:10,230
 If a fridge or freezer is very old and not energy efficient

387
00:23:10,230 --> 00:23:11,440
, it can be a big draw.

388
00:23:11,440 --> 00:23:14,110
 Sometimes it's actually worth it to invest in a newer, more

389
00:23:14,110 --> 00:23:15,440
 energy-efficient model.

390
00:23:15,440 --> 00:23:20,440
 The deep freeze downstairs maybe, but still $150 a month.

391
00:23:20,440 --> 00:23:25,440
 It seems like a lot, yes. We're going to look into that.

392
00:23:25,440 --> 00:23:30,440
 Let's see.

393
00:23:30,440 --> 00:23:35,250
 I'm still wondering about your winter project, what we can

394
00:23:35,250 --> 00:23:42,440
 do to help with your winter project that you wanted to do.

395
00:23:42,440 --> 00:23:45,440
 Is there any more information on that, Bonté?

396
00:23:45,440 --> 00:23:50,740
 No. Sandamalie said Monday or Tuesday someone's going to go

397
00:23:50,740 --> 00:23:51,440
 to the place and talk to me.

398
00:23:51,440 --> 00:23:56,440
 But there was something else. What was it? The booklet.

399
00:23:56,440 --> 00:24:02,440
 Someone is going to Sri Lanka on the fifth.

400
00:24:02,440 --> 00:24:05,440
 Shehan is going to Sri Lanka on the fifth.

401
00:24:05,440 --> 00:24:09,710
 I was going to get them to bring back some booklets with

402
00:24:09,710 --> 00:24:10,440
 them.

403
00:24:10,440 --> 00:24:15,400
 The organization could arrange that, or we could have

404
00:24:15,400 --> 00:24:16,440
 everyone.

405
00:24:16,440 --> 00:24:23,440
 If anyone wants to support that, be a part of the printing,

406
00:24:23,440 --> 00:24:25,440
 we could do an online thing.

407
00:24:25,440 --> 00:24:26,440
 What do you think?

408
00:24:26,440 --> 00:24:29,440
 Sure. Now what do you mean, booklet? Do you mean your book?

409
00:24:29,440 --> 00:24:30,440
 Yes.

410
00:24:30,440 --> 00:24:32,440
 Oh, so they'll actually print it up for you?

411
00:24:32,440 --> 00:24:33,440
 A thousand of them.

412
00:24:33,440 --> 00:24:35,440
 Oh, awesome.

413
00:24:35,440 --> 00:24:39,440
 Yes.

414
00:24:39,440 --> 00:24:45,150
 It's fifty-five rupees each, which I think is, how much is

415
00:24:45,150 --> 00:24:47,440
 fifty-five rupees?

416
00:24:47,440 --> 00:24:59,440
 Twenty-five cents?

417
00:24:59,440 --> 00:25:00,440
 No, that's about fifty-six.

418
00:25:00,440 --> 00:25:04,440
 .823 US dollars, so eighty-two cents.

419
00:25:04,440 --> 00:25:07,440
 No.

420
00:25:07,440 --> 00:25:10,440
 Are you sure? You're looking at the Indian rupee.

421
00:25:10,440 --> 00:25:11,970
 Well, it says fifty-five Indian rupee is Sri Lankan rupee

422
00:25:11,970 --> 00:25:12,440
 different.

423
00:25:12,440 --> 00:25:14,440
 Not Sri Lankan rupee, yes, it's different.

424
00:25:14,440 --> 00:25:15,440
 Oh, okay.

425
00:25:15,440 --> 00:25:16,440
 It's about fifty cents.

426
00:25:16,440 --> 00:25:17,440
 Okay.

427
00:25:17,440 --> 00:25:21,440
 So fifty cents each.

428
00:25:21,440 --> 00:25:23,440
 Fifty cents each.

429
00:25:23,440 --> 00:25:28,440
 For a thousand is five hundred dollars.

430
00:25:28,440 --> 00:25:31,440
 Five hundred dollars. That's a lot worth it.

431
00:25:31,440 --> 00:25:35,440
 That's a great way to share that.

432
00:25:35,440 --> 00:25:39,440
 So yeah, if, what do you think, Robin?

433
00:25:39,440 --> 00:25:42,680
 Yeah, that would definitely be something we'd want to

434
00:25:42,680 --> 00:25:43,440
 support.

435
00:25:43,440 --> 00:25:45,440
 Do you have time to work on that?

436
00:25:45,440 --> 00:25:47,440
 Definitely.

437
00:25:47,440 --> 00:25:48,440
 Okay.

438
00:25:48,440 --> 00:25:52,640
 Okay, so we will do that, and also the winter project, so

439
00:25:52,640 --> 00:25:55,440
 we'll have two projects going.

440
00:25:55,440 --> 00:26:00,440
 That'll be nice.

441
00:26:00,440 --> 00:26:03,440
 Awesome.

442
00:26:03,440 --> 00:26:05,850
 So we just need a little more information on the other

443
00:26:05,850 --> 00:26:06,440
 project.

444
00:26:06,440 --> 00:26:09,990
 And the other thing just going on within the volunteer

445
00:26:09,990 --> 00:26:14,440
 group, as Sri Mongol International is in the process of red

446
00:26:14,440 --> 00:26:14,440
oing bylaws,

447
00:26:14,440 --> 00:26:18,630
 so people that are interested in reviewing the bylaws and

448
00:26:18,630 --> 00:26:20,440
 coming to a meeting to vote on them.

449
00:26:20,440 --> 00:26:23,440
 That would be our next volunteer meeting.

450
00:26:23,440 --> 00:26:26,440
 We're just going to ask people to take a look at it.

451
00:26:26,440 --> 00:26:32,240
 So if anyone hasn't been involved but would like to become

452
00:26:32,240 --> 00:26:34,440
 involved, if you can just send me an email,

453
00:26:34,440 --> 00:26:39,990
 or our Facebook group also, Sri Mongol International

454
00:26:39,990 --> 00:26:41,440
 Volunteers on Facebook,

455
00:26:41,440 --> 00:26:43,440
 we're in a bunch of different places.

456
00:26:43,440 --> 00:26:46,880
 But I'll post my email in the meditation chat, and

457
00:26:46,880 --> 00:26:50,440
 definitely looking for more people to become involved,

458
00:26:50,440 --> 00:26:54,890
 and to take a look at the new bylaws, which are, they're

459
00:26:54,890 --> 00:26:58,510
 actually, it's a template provided by the province of

460
00:26:58,510 --> 00:26:59,440
 Ontario.

461
00:26:59,440 --> 00:27:03,190
 So it's something that they actually provide, and it lets

462
00:27:03,190 --> 00:27:05,620
 you know the rules that a nonprofit organization is

463
00:27:05,620 --> 00:27:07,440
 required to abide by.

464
00:27:07,440 --> 00:27:09,500
 So it seems pretty solid, but always go to have different

465
00:27:09,500 --> 00:27:11,440
 people take a look at it and say,

466
00:27:11,440 --> 00:27:15,440
 "Hey, this is maybe something that is a concern."

467
00:27:15,440 --> 00:27:16,440
 So just send me an email.

468
00:27:16,440 --> 00:27:19,010
 If you'd like to become involved, we would be happy to have

469
00:27:19,010 --> 00:27:19,440
 you.

470
00:27:19,440 --> 00:27:24,910
 And we have a question from Brenna, who states that even

471
00:27:24,910 --> 00:27:28,440
 though she's yellow, she did meditate today.

472
00:27:28,440 --> 00:27:32,010
 "Renunciation is considered a wholesome deed, but can fix

473
00:27:32,010 --> 00:27:36,500
ating, clinging to the idea of renunciation be considered un

474
00:27:36,500 --> 00:27:38,440
wholesome?"

475
00:27:38,440 --> 00:27:45,000
 Brenna, you get a pass. You don't have to be clear. You get

476
00:27:45,000 --> 00:27:48,440
 a lifetime pass, so don't worry about that.

477
00:27:48,440 --> 00:27:56,140
 Absolutely, yeah. If you're fixated on anything, the Buddha

478
00:27:56,140 --> 00:27:58,440
 said, "Sabei dhamma na langabhi nywaya sahya."

479
00:27:58,440 --> 00:28:03,440
 No dhammas are worth clinging to. That includes nibbana.

480
00:28:03,440 --> 00:28:09,440
 Clinging to any dhamma is a problem.

481
00:28:09,440 --> 00:28:15,080
 Often we want to escape our problems, so it's not really

482
00:28:15,080 --> 00:28:19,440
 renunciation that we're talking about.

483
00:28:19,440 --> 00:28:31,000
 Often it's running away, not wanting to have to deal with

484
00:28:31,000 --> 00:28:35,440
 the stress of possessions.

485
00:28:35,440 --> 00:28:40,440
 I mean, at face value, that can be wholesome, right?

486
00:28:40,440 --> 00:28:43,160
 If you just can't be bothered, if you've given up any

487
00:28:43,160 --> 00:28:45,440
 desire for possessions, that's good.

488
00:28:45,440 --> 00:28:50,690
 But if you're upset about having to deal with possessions,

489
00:28:50,690 --> 00:28:52,440
 that's not good.

490
00:28:52,440 --> 00:28:55,940
 So it's kind of weird, because true renunciation you could

491
00:28:55,940 --> 00:28:57,440
 never be attached to.

492
00:28:57,440 --> 00:29:03,300
 But true renunciation is renouncing even desire to renounce

493
00:29:03,300 --> 00:29:07,440
, desire to be free, desire to escape.

494
00:29:07,440 --> 00:29:11,980
 In a sense, it's really hard to talk, because there is some

495
00:29:11,980 --> 00:29:15,440
 sense where desire to be free and desire to escape is good,

496
00:29:15,440 --> 00:29:16,440
 is wholesome.

497
00:29:16,440 --> 00:29:21,350
 But it's not really desire. It's just the knowledge-based

498
00:29:21,350 --> 00:29:24,440
 impetus that leads you there.

499
00:29:24,440 --> 00:29:27,850
 But there's no upset if it doesn't go the way you planned

500
00:29:27,850 --> 00:29:33,440
 or anything, right? There's no attachment involved.

501
00:29:33,440 --> 00:29:38,440
 It's just more of an inclination.

502
00:29:38,440 --> 00:29:44,440
 So what we want to develop is the inclination to renounce.

503
00:29:44,440 --> 00:29:54,440
 You just become inclined to turn away, as the Buddha said.

504
00:29:54,440 --> 00:29:57,440
 You become nibhida, which means you become disenchanted.

505
00:29:57,440 --> 00:30:26,440
 This is the path of purity.

506
00:30:26,440 --> 00:30:30,440
 Okay. Good night.

507
00:30:30,440 --> 00:30:32,440
 Thank you, Pante.

508
00:30:32,440 --> 00:30:35,560
 Thank you for being brave and joining us. You're welcome

509
00:30:35,560 --> 00:30:36,440
 anytime.

510
00:30:36,440 --> 00:30:40,440
 Thank you so much. Thank you, Robin.

511
00:30:40,440 --> 00:30:44,030
 Thank you, and thank you, Pante. Have a good evening,

512
00:30:44,030 --> 00:30:44,440
 everyone.

513
00:30:44,440 --> 00:30:48,440
 Good night, evening, everyone. Thanks for joining us.

514
00:30:48,440 --> 00:31:03,440
 Thank you.

