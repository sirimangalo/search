1
00:00:00,000 --> 00:00:16,000
 Good evening everyone.

2
00:00:16,000 --> 00:00:22,000
 Broadcasting Live April 3rd 2016.

3
00:00:22,000 --> 00:00:32,000
 Today's quote again from the Yangud

4
00:00:32,000 --> 00:00:36,930
 Again he's not doing a very good job of translating, in my

5
00:00:36,930 --> 00:00:40,000
 opinion.

6
00:00:48,000 --> 00:00:52,000
 I don't know where he would get attention from.

7
00:00:52,000 --> 00:00:55,070
 So we have to really ignore this quote, the English,

8
00:00:55,070 --> 00:00:58,940
 because he's done a poor job translating. There's no way

9
00:00:58,940 --> 00:01:00,000
 around it.

10
00:01:00,000 --> 00:01:09,270
 So the dhammas here are the four dhammas that lead to, and

11
00:01:09,270 --> 00:01:11,000
 not progress, they lead to benefit.

12
00:01:11,000 --> 00:01:20,000
 The word is "hita" which just means welfare or benefit.

13
00:01:20,000 --> 00:01:23,760
 And it's not even worldly progress, so he's not even

14
00:01:23,760 --> 00:01:27,000
 translating, he's very much paraphrasing.

15
00:01:34,000 --> 00:01:46,000
 He's talking to someone named "bhiyaga pajja".

16
00:01:46,000 --> 00:01:50,000
 I don't know this in the back story, anyway, not important.

17
00:01:50,000 --> 00:01:54,000
 "Chattaromi dhamma" there are these four dhammas.

18
00:01:54,000 --> 00:01:59,000
 "Kuluputasa dita dhamma hitaya sanghatanti"

19
00:01:59,000 --> 00:02:04,050
 So "dita dhamma" again we have this phrase, "dita" means

20
00:02:04,050 --> 00:02:07,000
 seen, "dhamma" is reality.

21
00:02:07,000 --> 00:02:12,120
 So in the reality that is seen, in other words right now,

22
00:02:12,120 --> 00:02:15,000
 without delay, not in the next life.

23
00:02:15,000 --> 00:02:18,000
 "Dita dhamma" often means referring to this life.

24
00:02:18,000 --> 00:02:22,000
 So "dita dhamma hitaya" means benefit in this life.

25
00:02:22,000 --> 00:02:26,530
 So it's not exactly worldly, but the point is, these are

26
00:02:26,530 --> 00:02:30,000
 the things that lead to benefit in this life.

27
00:02:30,000 --> 00:02:37,880
 Because it doesn't take practice of the Buddha's teaching

28
00:02:37,880 --> 00:02:41,000
 to succeed in this life.

29
00:02:41,000 --> 00:02:45,880
 You can actually be a terrible, terrible person and still

30
00:02:45,880 --> 00:02:48,000
 succeed to some extent in this life.

31
00:02:48,000 --> 00:02:52,920
 Your life may be shorter than others or it may eventually

32
00:02:52,920 --> 00:02:58,000
 turn around, but even evil people.

33
00:02:58,000 --> 00:03:03,980
 So for that reason, benefit in this life that only touches

34
00:03:03,980 --> 00:03:08,000
 this life, it's not so much related to the dhamma.

35
00:03:08,000 --> 00:03:12,000
 And hence the use of the word worldly.

36
00:03:12,000 --> 00:03:14,870
 But that's not exactly what it means. These are important,

37
00:03:14,870 --> 00:03:16,000
 but they're more important.

38
00:03:16,000 --> 00:03:20,620
 They're important in a broad sense. The four actually can

39
00:03:20,620 --> 00:03:24,000
 be applied, well generally, with some.

40
00:03:24,000 --> 00:03:26,960
 I mean they can be applied to a monk, they can be applied

41
00:03:26,960 --> 00:03:28,000
 to meditators.

42
00:03:28,000 --> 00:03:32,950
 It's mostly actually directed to lay people, and people

43
00:03:32,950 --> 00:03:35,000
 living in the world.

44
00:03:35,000 --> 00:03:39,000
 But nonetheless they're good to keep in mind.

45
00:03:39,000 --> 00:03:41,690
 And these are actually one of the core teachings in

46
00:03:41,690 --> 00:03:43,000
 cultural Buddhism.

47
00:03:43,000 --> 00:03:47,090
 Like in Thailand it's a big one. These four are something

48
00:03:47,090 --> 00:03:50,000
 that we used to learn in schools.

49
00:03:50,000 --> 00:03:53,150
 And the novices learned in schools, one of the first things

50
00:03:53,150 --> 00:03:54,000
 we learned.

51
00:03:54,000 --> 00:04:02,220
 Dhammi prayot nailokni, dhammas that have benefit in this

52
00:04:02,220 --> 00:04:03,000
 life.

53
00:04:07,000 --> 00:04:12,810
 Prayotokni, prayotlokna. So they pair with another set of d

54
00:04:12,810 --> 00:04:21,000
hammas, which are those that have samparayaka hita.

55
00:04:21,000 --> 00:04:26,000
 Samparayaka hita, something like that.

56
00:04:26,000 --> 00:04:33,000
 Which is the dhammas that have benefit in the next life.

57
00:04:33,000 --> 00:04:36,300
 It's another set of four dhammas, so we'll talk about those

58
00:04:36,300 --> 00:04:37,000
 as well.

59
00:04:37,000 --> 00:04:41,000
 In fact we even have a third set, so we can round it off

60
00:04:41,000 --> 00:04:44,000
 and talk about all the dhammas that have benefit.

61
00:04:44,000 --> 00:04:48,000
 Lompoc chodok, this monk in Bangkok, he used to talk about

62
00:04:48,000 --> 00:04:49,000
 these a lot.

63
00:04:49,000 --> 00:04:53,640
 Prayot lokni, prayot lokna, prayot zhulsu. The third one is

64
00:04:53,640 --> 00:04:59,000
 zhulsu, so it means the very highest benefit.

65
00:04:59,000 --> 00:05:06,000
 Prayot kunipan, that which leads you to niban, nirvana.

66
00:05:06,000 --> 00:05:12,650
 But this first four, utana sampada means effort. You have

67
00:05:12,650 --> 00:05:18,000
 to be endowed with effort.

68
00:05:18,000 --> 00:05:23,120
 ditadhamahitaya, ditadhammasukai, if you want benefit, if

69
00:05:23,120 --> 00:05:28,760
 you're looking for your welfare and happiness in the here

70
00:05:28,760 --> 00:05:31,000
 and now, you need effort.

71
00:05:31,000 --> 00:05:36,000
 You need to work. So he explains what that means.

72
00:05:36,000 --> 00:05:40,490
 You have to make your living, you have to be skillful and

73
00:05:40,490 --> 00:05:45,390
 diligent, possessing judgment about it in order to carry it

74
00:05:45,390 --> 00:05:48,000
 out and arrange it properly.

75
00:05:48,000 --> 00:05:51,270
 Initiative, bhikkhu bodhi translates as initiative. I'm

76
00:05:51,270 --> 00:05:58,790
 still not convinced, but I mean the gumption, taking up the

77
00:05:58,790 --> 00:06:00,000
 task.

78
00:06:00,000 --> 00:06:06,040
 In all things, this is the case. The question is, are you

79
00:06:06,040 --> 00:06:09,000
 making effort in your work?

80
00:06:09,000 --> 00:06:12,550
 So this applies for meditators as well. If you want the

81
00:06:12,550 --> 00:06:18,000
 meditation to be successful, you have to work at it.

82
00:06:18,000 --> 00:06:22,000
 To some extent, we can apply this.

83
00:06:22,000 --> 00:06:37,130
 Right. And this is actually related to, as I said, it was

84
00:06:37,130 --> 00:06:39,000
 being taught to

85
00:06:39,000 --> 00:06:53,000
 ditajanu, who goes by the clan name Vyagapanja. Anyway.

86
00:06:53,000 --> 00:06:57,730
 So he says, we use sandalwood from kasi, we wear garlands,

87
00:06:57,730 --> 00:07:02,220
 scents and ungivens, we receive gold and silver. Meaning,

88
00:07:02,220 --> 00:07:04,000
 we're not monks.

89
00:07:04,000 --> 00:07:08,360
 We're not even spiritually religious people. We live in the

90
00:07:08,360 --> 00:07:14,130
 world, we work and we play and we laugh and we sing and we

91
00:07:14,130 --> 00:07:17,000
 eat and we drink and make merry.

92
00:07:17,000 --> 00:07:20,850
 But what can we do? He says, teach us the Dhamma in a way

93
00:07:20,850 --> 00:07:23,740
 that will lead to our welfare and happiness in this present

94
00:07:23,740 --> 00:07:26,000
 life and in future lives.

95
00:07:26,000 --> 00:07:41,000
 And then the other one, so in future lives, samparaya hita,

96
00:07:41,000 --> 00:07:41,000
 samparaya sakaya. It's the other one.

97
00:07:41,000 --> 00:07:45,070
 So for the next life. So the Buddha separates them because

98
00:07:45,070 --> 00:07:47,000
 they're two different things.

99
00:07:47,000 --> 00:07:50,740
 As I said, in this life, it's more related to sort of

100
00:07:50,740 --> 00:07:55,000
 functional things. So the first one is you have to work,

101
00:07:55,000 --> 00:07:56,000
 you have to have the initiative.

102
00:07:56,000 --> 00:08:00,620
 You can't just sit around and wait for happiness and

103
00:08:00,620 --> 00:08:05,360
 welfare to come to you. If you want to succeed in life, you

104
00:08:05,360 --> 00:08:07,000
 have to work.

105
00:08:07,000 --> 00:08:14,570
 Number two, arakasampada. You have to guard or protect or

106
00:08:14,570 --> 00:08:20,620
 save up. You have to guard your wealth, which you have

107
00:08:20,620 --> 00:08:22,000
 amassed.

108
00:08:22,000 --> 00:08:25,840
 You have to think to yourself, how can I prevent kings and

109
00:08:25,840 --> 00:08:29,840
 thieves from taking it, fire from burning it, floods from

110
00:08:29,840 --> 00:08:35,000
 sweeping it off and displeasing airs from taking it.

111
00:08:35,000 --> 00:08:46,500
 You know, airs can be displeasing. Aries as in people who

112
00:08:46,500 --> 00:08:47,380
 inherit things. Maybe they come looking for their

113
00:08:47,380 --> 00:08:48,000
 inheritance early or something.

114
00:08:48,000 --> 00:08:57,750
 So what can I do to sustain my life? Again, a worldly thing

115
00:08:57,750 --> 00:09:00,000
, but it works for all of us.

116
00:09:00,000 --> 00:09:08,000
 I have to concern myself about my robes and my belongings.

117
00:09:08,000 --> 00:09:13,000
 But in regards to meditation, this is important as well.

118
00:09:13,000 --> 00:09:19,090
 Not only do we have to work hard in meditation, but we have

119
00:09:19,090 --> 00:09:21,000
 to be careful.

120
00:09:21,000 --> 00:09:25,900
 Ajahn Tong talks about this similarly, I think, from the

121
00:09:25,900 --> 00:09:30,000
 Visuddhi Maga, of someone rocking a cradle.

122
00:09:30,000 --> 00:09:33,470
 In the olden days, they would have the baby in a hammock,

123
00:09:33,470 --> 00:09:36,990
 maybe, I don't know, or in a cradle, I guess, and they have

124
00:09:36,990 --> 00:09:38,000
 it on a string.

125
00:09:38,000 --> 00:09:41,540
 So they could sit far away and they'd just pull on the

126
00:09:41,540 --> 00:09:44,000
 string to keep the rocker going.

127
00:09:44,000 --> 00:09:48,530
 And that would keep the baby asleep. But you had to be

128
00:09:48,530 --> 00:09:52,990
 careful. If you didn't keep your eye on it, the baby would

129
00:09:52,990 --> 00:09:54,000
 wake up.

130
00:09:54,000 --> 00:09:58,420
 So the idea is that meditation is like something very

131
00:09:58,420 --> 00:10:02,000
 delicate that you have to keep your mind on.

132
00:10:02,000 --> 00:10:09,000
 If you're not paying attention, the baby will wake up.

133
00:10:09,000 --> 00:10:13,200
 You'll lose your mindfulness. It's very easy to get off

134
00:10:13,200 --> 00:10:14,000
 track.

135
00:10:14,000 --> 00:10:17,000
 You see, meditation again and again will get off track.

136
00:10:17,000 --> 00:10:20,960
 And you have to bring yourself back again and again,

137
00:10:20,960 --> 00:10:22,000
 guarding.

138
00:10:22,000 --> 00:10:28,000
 So it's analogous to the... it works on that level as well.

139
00:10:28,000 --> 00:10:33,000
 But in terms of anything, you could put this on any level.

140
00:10:33,000 --> 00:10:39,000
 You need to... anything you do, you need to have effort.

141
00:10:39,000 --> 00:10:46,050
 And you have to be careful, guard. Guard what you have

142
00:10:46,050 --> 00:10:49,000
 gained through your effort.

143
00:10:49,000 --> 00:10:53,740
 So we don't squander. Sometimes people come to meditate

144
00:10:53,740 --> 00:10:56,350
 here and then they go out in the world and think they're

145
00:10:56,350 --> 00:10:57,000
 invincible.

146
00:10:57,000 --> 00:11:00,150
 Because it's easier to be invincible here. There's not so

147
00:11:00,150 --> 00:11:02,000
 much bothering you.

148
00:11:02,000 --> 00:11:04,350
 When you go out there, you realize, "Oh, I didn't... maybe

149
00:11:04,350 --> 00:11:07,290
 I didn't quite get as much out of the meditation as I

150
00:11:07,290 --> 00:11:08,000
 thought.

151
00:11:08,000 --> 00:11:11,000
 Maybe I got this much and you thought you got this much."

152
00:11:11,000 --> 00:11:16,800
 Of course, your expectations are not met because you're not

153
00:11:16,800 --> 00:11:18,000
 mindful.

154
00:11:18,000 --> 00:11:22,000
 Sometimes we go out and it's a shock.

155
00:11:22,000 --> 00:11:25,890
 You wake up, call and you realize you have to guard

156
00:11:25,890 --> 00:11:27,000
 yourself.

157
00:11:27,000 --> 00:11:32,200
 Part of it is negligence. But that's the thing, is you can

158
00:11:32,200 --> 00:11:33,000
't be negligent.

159
00:11:33,000 --> 00:11:36,220
 If you want to succeed in anything, meditation, spiritual

160
00:11:36,220 --> 00:11:42,000
 or worldly, you have to be careful.

161
00:11:42,000 --> 00:11:49,000
 Number three is, we need good friends. Kalyana Mitata.

162
00:11:49,000 --> 00:11:53,000
 And this, of course, goes with everything.

163
00:11:53,000 --> 00:11:57,260
 In Thailand, in meditation, this was a problem. Too many

164
00:11:57,260 --> 00:11:58,000
 people in the monastery.

165
00:11:58,000 --> 00:12:02,590
 It's easy to get caught up with the wrong people. Easy to

166
00:12:02,590 --> 00:12:07,000
 sit down and chat and to get sidetracked.

167
00:12:07,000 --> 00:12:11,000
 And we sidetracked each other as well.

168
00:12:11,000 --> 00:12:15,000
 Luckily here in Hamilton, we have very small centers.

169
00:12:15,000 --> 00:12:19,000
 Our meditators come and they're not bothered by others.

170
00:12:19,000 --> 00:12:23,000
 But by good friendship, it means people who are practicing,

171
00:12:23,000 --> 00:12:26,000
 who appreciate practice, who teach practice,

172
00:12:26,000 --> 00:12:33,000
 who accommodate you in your practice.

173
00:12:33,000 --> 00:12:40,640
 People who care about you, care about your spiritual

174
00:12:40,640 --> 00:12:42,000
 practice.

175
00:12:42,000 --> 00:12:46,530
 People who can offer you advice and who can give you the

176
00:12:46,530 --> 00:12:50,000
 space you need and that kind of thing.

177
00:12:50,000 --> 00:12:54,000
 People who can guide you and be an example for you.

178
00:12:54,000 --> 00:12:56,790
 That's a good friend. The Buddha is our good friend, really

179
00:12:56,790 --> 00:12:57,000
.

180
00:12:57,000 --> 00:13:00,000
 That's why we think about the Buddha a lot.

181
00:13:00,000 --> 00:13:03,550
 Because as someone who, when you think about him, it gives

182
00:13:03,550 --> 00:13:05,000
 you a good example.

183
00:13:05,000 --> 00:13:11,150
 It gives you something to relate your practice to. Someone

184
00:13:11,150 --> 00:13:15,000
 who is gone the distance.

185
00:13:15,000 --> 00:13:20,000
 Number four is samadhi-vita.

186
00:13:20,000 --> 00:13:24,000
 Samadhi-vita.

187
00:13:24,000 --> 00:13:30,000
 Samadhi-vita.

188
00:13:30,000 --> 00:13:36,000
 Samadhi-vita means living according to your means.

189
00:13:36,000 --> 00:13:39,000
 So this is a fairly worldly one.

190
00:13:39,000 --> 00:13:42,640
 So with friendship, of course, that's obviously important

191
00:13:42,640 --> 00:13:44,000
 in the world as well.

192
00:13:44,000 --> 00:13:48,000
 If you don't have good friends, you won't succeed.

193
00:13:48,000 --> 00:13:51,550
 It's having to deal with the stress of enemies and having

194
00:13:51,550 --> 00:13:53,000
 to deal with their problems,

195
00:13:53,000 --> 00:13:54,510
 having to deal with people who lead you in the wrong

196
00:13:54,510 --> 00:13:55,000
 direction.

197
00:13:55,000 --> 00:14:00,470
 It applies in all aspects. But samadhi-vita is mostly

198
00:14:00,470 --> 00:14:01,000
 related to the world.

199
00:14:01,000 --> 00:14:05,000
 It's explicitly. This means living within your means.

200
00:14:05,000 --> 00:14:08,520
 So if you want to succeed in the world, you have to not

201
00:14:08,520 --> 00:14:13,000
 only acquire and protect your wealth,

202
00:14:13,000 --> 00:14:17,000
 but you have to not squander it yourself.

203
00:14:17,000 --> 00:14:22,460
 You have to budget and you have to be content with what you

204
00:14:22,460 --> 00:14:23,000
 have.

205
00:14:23,000 --> 00:14:27,000
 Because if you live outside your means, this is where

206
00:14:27,000 --> 00:14:27,000
 people go into debt.

207
00:14:27,000 --> 00:14:30,000
 It might seem like a trite sort of statement,

208
00:14:30,000 --> 00:14:34,000
 but it's amazing how many people live outside their means.

209
00:14:34,000 --> 00:14:39,030
 Aren't they able to stop themselves from borrowing money

210
00:14:39,030 --> 00:14:42,000
 and getting into serious debt.

211
00:14:42,000 --> 00:14:47,000
 You have to be careful.

212
00:14:47,000 --> 00:14:57,000
 But the simple word we can apply to meditation as well.

213
00:14:57,000 --> 00:15:01,000
 Because I want to really apply this to meditation.

214
00:15:01,000 --> 00:15:05,000
 We want to be talking about what is of most benefit.

215
00:15:05,000 --> 00:15:08,170
 We don't have all that much time to focus too much on

216
00:15:08,170 --> 00:15:11,000
 worldly things, even though they're helpful.

217
00:15:11,000 --> 00:15:14,600
 And also I've got a meditator here listening, so I have to

218
00:15:14,600 --> 00:15:15,000
 help him.

219
00:15:15,000 --> 00:15:21,000
 Give him something to think about.

220
00:15:21,000 --> 00:15:31,000
 But samajivita means the way you're living to be balanced.

221
00:15:31,000 --> 00:15:34,000
 Not too much excess, but not too little.

222
00:15:34,000 --> 00:15:46,190
 So the excess and the lack, if not enough, too much, both

223
00:15:46,190 --> 00:15:47,000
 are problematic.

224
00:15:47,000 --> 00:15:50,000
 If you don't practice enough, if you practice too much,

225
00:15:50,000 --> 00:15:53,320
 if you practice without taking a break, it can also be to

226
00:15:53,320 --> 00:15:55,000
 your detriment.

227
00:15:55,000 --> 00:16:00,530
 But mostly living throughout the day balanced with a

228
00:16:00,530 --> 00:16:02,000
 balanced mind.

229
00:16:02,000 --> 00:16:08,000
 Just balancing your faculties.

230
00:16:08,000 --> 00:16:13,360
 Confidence, effort, mindfulness, concentration, and wisdom

231
00:16:13,360 --> 00:16:16,000
 balancing them.

232
00:16:16,000 --> 00:16:20,000
 Not practicing too hard, not practicing too little.

233
00:16:20,000 --> 00:16:27,000
 We know the work we have to do.

234
00:16:27,000 --> 00:16:32,000
 So we do it consistently and systematically.

235
00:16:32,000 --> 00:16:35,000
 That's what's important.

236
00:16:35,000 --> 00:16:39,420
 Anyway, those are the benefits in this world, mostly

237
00:16:39,420 --> 00:16:41,000
 worldly things.

238
00:16:41,000 --> 00:16:45,650
 The ones that lead the benefit in the next life, I think he

239
00:16:45,650 --> 00:16:50,000
 then goes on.

240
00:16:50,000 --> 00:16:54,490
 There are four other things that lead to happiness and

241
00:16:54,490 --> 00:16:56,000
 welfare in future lives.

242
00:16:56,000 --> 00:17:00,080
 So these are a little bit more spiritual, but this is more

243
00:17:00,080 --> 00:17:03,000
 leading to heaven.

244
00:17:03,000 --> 00:17:07,000
 Let's see if I can get to Pali.

245
00:17:07,000 --> 00:17:08,000
 Here we are.

246
00:17:08,000 --> 00:17:13,000
 Sadha Sampada, Sila Sampada, Jaga Sampada, Panya Sampada.

247
00:17:13,000 --> 00:17:19,000
 So the four, these are the Samparaya Hita.

248
00:17:19,000 --> 00:17:24,000
 Samparaya Hita, Samparaya Sukaya.

249
00:17:24,000 --> 00:17:27,000
 These are the things that lead you to heaven.

250
00:17:27,000 --> 00:17:32,000
 These are the positive mundane results.

251
00:17:32,000 --> 00:17:35,680
 Heaven being still mundane, but these are the things that

252
00:17:35,680 --> 00:17:37,000
 lead you to heaven.

253
00:17:37,000 --> 00:17:40,210
 So they are the positive mundane results of spiritual

254
00:17:40,210 --> 00:17:43,000
 practice, like meditation.

255
00:17:43,000 --> 00:17:47,280
 Actually these four are not all that mundane at all, but

256
00:17:47,280 --> 00:17:49,000
 they are relating to going to heaven.

257
00:17:49,000 --> 00:17:53,000
 Because again, he is telling this to a layman.

258
00:17:53,000 --> 00:17:59,000
 He is not probably meditating, but probably should.

259
00:17:59,000 --> 00:18:02,000
 But anyway, Buddha gives him these four.

260
00:18:02,000 --> 00:18:06,000
 Sadha means confidence, or you could say faith.

261
00:18:06,000 --> 00:18:09,930
 Sadha Sampada means you have right faith, faith in the

262
00:18:09,930 --> 00:18:14,000
 right thing, faith in the Buddha, the Dhammasanga,

263
00:18:14,000 --> 00:18:19,530
 faith in spiritual teaching, faith in good people, faith in

264
00:18:19,530 --> 00:18:21,000
 good things.

265
00:18:21,000 --> 00:18:25,090
 Confidence in yourself, confidence in good things about

266
00:18:25,090 --> 00:18:26,000
 yourself,

267
00:18:26,000 --> 00:18:32,000
 confidence in your ability to cultivate goodness.

268
00:18:32,000 --> 00:18:40,000
 Number two, Sila Sampada, morality, endowed with morality.

269
00:18:40,000 --> 00:18:50,000
 The five precepts, guarding your senses,

270
00:18:50,000 --> 00:18:53,700
 considering the use of your requisites so that the things

271
00:18:53,700 --> 00:19:00,000
 you use, you don't use them for the wrong purposes,

272
00:19:00,000 --> 00:19:07,000
 and right livelihood.

273
00:19:07,000 --> 00:19:12,000
 Number three, Chaga Sampada, endowed with generosity.

274
00:19:12,000 --> 00:19:17,960
 Chaga is maybe renunciation, or abandoning, giving up, I

275
00:19:17,960 --> 00:19:19,000
 guess.

276
00:19:19,000 --> 00:19:21,000
 We usually translate it as generosity.

277
00:19:21,000 --> 00:19:23,000
 In a mundane sense, that's what it means.

278
00:19:23,000 --> 00:19:27,620
 It means giving gifts and supporting others and being

279
00:19:27,620 --> 00:19:29,000
 charitable.

280
00:19:29,000 --> 00:19:33,000
 But on a deeper level, it means giving up.

281
00:19:33,000 --> 00:19:38,150
 Giving up your desire is actually a great gift, because the

282
00:19:38,150 --> 00:19:39,000
 less desire you have,

283
00:19:39,000 --> 00:19:41,990
 the more you're able to give and give up and help and

284
00:19:41,990 --> 00:19:45,000
 support others and do for others.

285
00:19:45,000 --> 00:19:48,770
 But if you're always obsessed with your own benefit, very

286
00:19:48,770 --> 00:19:54,000
 little time to give to others.

287
00:19:54,000 --> 00:19:58,000
 Number four, Upanya Sampada, wisdom.

288
00:19:58,000 --> 00:20:02,000
 One should be endowed with wisdom. This is a great benefit.

289
00:20:02,000 --> 00:20:05,430
 It's also a benefit for this life, but sometimes wise

290
00:20:05,430 --> 00:20:08,000
 people still may not do that well in this life.

291
00:20:08,000 --> 00:20:11,000
 Nonetheless, they do well in their minds.

292
00:20:11,000 --> 00:20:16,140
 Wisdom is the greatest, the most powerful weapon we have,

293
00:20:16,140 --> 00:20:22,000
 or tool we have, for benefit, for welfare, for happiness.

294
00:20:22,000 --> 00:20:24,000
 Because with wisdom, you know right from wrong.

295
00:20:24,000 --> 00:20:27,710
 With wisdom, you're able to rise above your problems when

296
00:20:27,710 --> 00:20:29,000
 things go wrong.

297
00:20:29,000 --> 00:20:31,400
 You're able to see them as they are, and it's not really

298
00:20:31,400 --> 00:20:33,000
 even about rising above them.

299
00:20:33,000 --> 00:20:38,000
 It's seeing through the cloud of ignorance that leads us to

300
00:20:38,000 --> 00:20:40,000
 see things as problems.

301
00:20:40,000 --> 00:20:42,000
 Because there's no such thing.

302
00:20:42,000 --> 00:20:45,790
 Problem is just a name that we give, a label that we give

303
00:20:45,790 --> 00:20:47,000
 to something.

304
00:20:47,000 --> 00:20:49,000
 The truth is there's experience.

305
00:20:49,000 --> 00:20:52,000
 Wisdom helps us to see that.

306
00:20:52,000 --> 00:20:55,220
 As a result, we don't get attached to things because we,

307
00:20:55,220 --> 00:20:58,000
 out of wisdom, we see that that's a problem.

308
00:20:58,000 --> 00:21:01,000
 It doesn't lead to happiness.

309
00:21:01,000 --> 00:21:03,820
 We don't get angry about things because we see that that

310
00:21:03,820 --> 00:21:05,000
 leads to problems.

311
00:21:05,000 --> 00:21:08,120
 When we give up delusion, wisdom is the opposite of

312
00:21:08,120 --> 00:21:09,000
 delusion.

313
00:21:09,000 --> 00:21:11,000
 Wisdom is like the bright light.

314
00:21:11,000 --> 00:21:17,000
 When you shine the bright light, the darkness goes away.

315
00:21:17,000 --> 00:21:23,000
 So the Buddha said, "Vinayyalokeya bijadomana sama."

316
00:21:23,000 --> 00:21:28,840
 Giving up in this world greed and anger, desire and

317
00:21:28,840 --> 00:21:30,000
 aversion.

318
00:21:30,000 --> 00:21:33,000
 Ajahn Tong, he said, "Why doesn't he ask?"

319
00:21:33,000 --> 00:21:36,000
 I think the commenter actually says, "Why doesn't?"

320
00:21:36,000 --> 00:21:40,770
 But I remember a talk, Ajahn Tong said, "Why doesn't the

321
00:21:40,770 --> 00:21:44,000
 Buddha mention delusion?"

322
00:21:44,000 --> 00:21:47,000
 It's because mindfulness, this is from the satipatthana,

323
00:21:47,000 --> 00:21:49,000
 so mindfulness is like a bright light.

324
00:21:49,000 --> 00:21:53,000
 When you shine it in, the darkness disappears.

325
00:21:53,000 --> 00:21:55,000
 So you don't even have to mention delusion.

326
00:21:55,000 --> 00:21:57,520
 When you're being mindful, you're working to give up greed

327
00:21:57,520 --> 00:21:58,000
 and anger.

328
00:21:58,000 --> 00:22:03,310
 Because you've removed the delusion during that time, there

329
00:22:03,310 --> 00:22:04,000
's wisdom.

330
00:22:04,000 --> 00:22:13,000
 Wisdom can arise about the greed and the anger.

331
00:22:13,000 --> 00:22:17,180
 So those four are that which leads to benefit in future

332
00:22:17,180 --> 00:22:18,000
 lives.

333
00:22:18,000 --> 00:22:20,690
 And the third group that I said, the one which leads to nir

334
00:22:20,690 --> 00:22:21,000
vana,

335
00:22:21,000 --> 00:22:25,000
 is actually the four satipatthana, I think.

336
00:22:25,000 --> 00:22:29,000
 I'm pretty sure that's what Lompal Chodok used to say.

337
00:22:29,000 --> 00:22:32,000
 What are the four that lead to nirvana?

338
00:22:32,000 --> 00:22:34,810
 Well, it doesn't say in this suta, this only gives the two

339
00:22:34,810 --> 00:22:35,000
 sets.

340
00:22:35,000 --> 00:22:38,000
 But the third set you have to talk about.

341
00:22:38,000 --> 00:22:40,030
 If you're looking for that which is the ultimate benefit,

342
00:22:40,030 --> 00:22:41,000
 it's the four satipatthana.

343
00:22:41,000 --> 00:22:44,000
 Because the Buddha said, eka yanoa yang bika vaymango.

344
00:22:44,000 --> 00:22:50,000
 This is the one way, the direct way, that leads to nirvana.

345
00:22:50,000 --> 00:22:57,000
 And that's the four satipatthana.

346
00:22:57,000 --> 00:23:03,000
 Anyway, so another little bit of dhamma tonight.

347
00:23:03,000 --> 00:23:05,000
 I still have more work to do tonight.

348
00:23:05,000 --> 00:23:06,000
 We're at crunch time.

349
00:23:06,000 --> 00:23:11,230
 Tomorrow I don't know if I'll be able to broadcast the sym

350
00:23:11,230 --> 00:23:13,000
posium on Tuesday.

351
00:23:13,000 --> 00:23:16,000
 And people who have been emailing us,

352
00:23:16,000 --> 00:23:18,000
 "Ah, students, leave everything to the last moment.

353
00:23:18,000 --> 00:23:20,000
 I should have known."

354
00:23:20,000 --> 00:23:23,370
 So they're all emailing us all days apologizing for waiting

355
00:23:23,370 --> 00:23:24,000
 for the last minute

356
00:23:24,000 --> 00:23:29,000
 and hoping they can join the symposium.

357
00:23:29,000 --> 00:23:34,000
 Anyway, so that's all for tonight.

358
00:23:34,000 --> 00:23:37,000
 Have a good night everyone.

359
00:23:39,000 --> 00:23:40,000
 Thank you.

360
00:23:41,000 --> 00:23:56,080
 [

