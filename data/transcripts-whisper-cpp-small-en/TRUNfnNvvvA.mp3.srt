1
00:00:00,000 --> 00:00:04,790
 Hi, so now I'll get into some of the issues that I talked

2
00:00:04,790 --> 00:00:07,960
 about in the last video.

3
00:00:07,960 --> 00:00:12,480
 The first one is the absolute nature of morals.

4
00:00:12,480 --> 00:00:17,440
 And morality of course is something that is heartily

5
00:00:17,440 --> 00:00:20,800
 denounced by the materialists as

6
00:00:20,800 --> 00:00:25,650
 something that is simply, generally, as something that is

7
00:00:25,650 --> 00:00:28,360
 simply a product of the sort of the

8
00:00:28,360 --> 00:00:30,720
 way it worked or the way it turned out.

9
00:00:30,720 --> 00:00:34,670
 The reason that we have morality at all is because of

10
00:00:34,670 --> 00:00:35,840
 evolution.

11
00:00:35,840 --> 00:00:41,550
 So if there was a group of people who, a group of beings

12
00:00:41,550 --> 00:00:46,440
 who had no morality, well they've,

13
00:00:46,440 --> 00:00:49,230
 they may have existed but they've become extinct because of

14
00:00:49,230 --> 00:00:51,280
 their inability to work together,

15
00:00:51,280 --> 00:00:55,480
 because of the non-desirability of these things.

16
00:00:55,480 --> 00:00:59,720
 Now okay, for sure agreed that our morality, but listen to

17
00:00:59,720 --> 00:01:02,280
 what you're saying, our morality

18
00:01:02,280 --> 00:01:07,020
 as it exists today is, exists that way because of the way

19
00:01:07,020 --> 00:01:09,040
 it's been developed.

20
00:01:09,040 --> 00:01:10,600
 And of course that's the case.

21
00:01:10,600 --> 00:01:14,290
 That doesn't mean that the way we look at morality is in

22
00:01:14,290 --> 00:01:15,520
 any way moral.

23
00:01:15,520 --> 00:01:18,230
 So using that as an argument to say well that's why

24
00:01:18,230 --> 00:01:21,040
 morality is relative or that's why morality

25
00:01:21,040 --> 00:01:24,720
 is simply a product of nature.

26
00:01:24,720 --> 00:01:27,440
 Doesn't say anything at all.

27
00:01:27,440 --> 00:01:33,220
 Now if you were to say that well our current state of being

28
00:01:33,220 --> 00:01:36,280
 is perfect or is the best that

29
00:01:36,280 --> 00:01:38,080
 we could possibly be, then yes.

30
00:01:38,080 --> 00:01:40,870
 Then you would have to say well yeah then the perfect

31
00:01:40,870 --> 00:01:44,480
 morality is, or real morality is

32
00:01:44,480 --> 00:01:48,910
 relative or is based on this sort of, or is exactly the way

33
00:01:48,910 --> 00:01:50,200
 we look at it.

34
00:01:50,200 --> 00:01:54,200
 The way we carry it out and it's very much relative.

35
00:01:54,200 --> 00:01:55,920
 Certain times we keep it, certain times we don't.

36
00:01:55,920 --> 00:02:00,440
 But the truth is relative morality has not served us very

37
00:02:00,440 --> 00:02:01,160
 well.

38
00:02:01,160 --> 00:02:04,280
 Neither has absolute morality as we understand it.

39
00:02:04,280 --> 00:02:09,320
 Now this doesn't disprove the fact, the idea that there

40
00:02:09,320 --> 00:02:12,200
 might be absolute morality.

41
00:02:12,200 --> 00:02:15,500
 And I'm going to discuss why it's possible to understand

42
00:02:15,500 --> 00:02:17,680
 that there is absolute morality.

43
00:02:17,680 --> 00:02:21,040
 Now what does it mean to say there is absolute morality?

44
00:02:21,040 --> 00:02:24,550
 Saying that there is absolute morality is simply saying

45
00:02:24,550 --> 00:02:26,640
 that there is a law which governs

46
00:02:26,640 --> 00:02:28,960
 the nature of the mind.

47
00:02:28,960 --> 00:02:32,820
 And this is exactly what we would expect if we were to come

48
00:02:32,820 --> 00:02:35,040
 to a scientific understanding

49
00:02:35,040 --> 00:02:36,040
 of the mind.

50
00:02:36,040 --> 00:02:39,560
 If we were to admit in scientific terms the existence of

51
00:02:39,560 --> 00:02:41,560
 the mind we should expect, we

52
00:02:41,560 --> 00:02:45,820
 should expect and not be suspicious when we find that it

53
00:02:45,820 --> 00:02:48,680
 actually runs according to certain

54
00:02:48,680 --> 00:02:51,880
 laws just as the physical world does.

55
00:02:51,880 --> 00:02:54,690
 So we look at the physical world and we say well gravity,

56
00:02:54,690 --> 00:02:55,960
 yeah it's universal.

57
00:02:55,960 --> 00:02:58,130
 It's not a relative thing where sometimes the body goes

58
00:02:58,130 --> 00:02:59,400
 according to it and sometimes

59
00:02:59,400 --> 00:03:03,500
 the body, the physical doesn't.

60
00:03:03,500 --> 00:03:07,970
 Gravity is universal as are all physical laws as far as we

61
00:03:07,970 --> 00:03:09,960
 can understand them.

62
00:03:09,960 --> 00:03:12,800
 Now what we understand is that the mind runs according to

63
00:03:12,800 --> 00:03:14,360
 certain laws as well and these

64
00:03:14,360 --> 00:03:17,200
 laws we could call morality.

65
00:03:17,200 --> 00:03:20,980
 Now I'm going to give a very specific definition of

66
00:03:20,980 --> 00:03:24,440
 morality or a definition of good I suppose.

67
00:03:24,440 --> 00:03:28,360
 We consider something to be good when it brings happiness.

68
00:03:28,360 --> 00:03:33,620
 This is a view or this is sort of a judgment call.

69
00:03:33,620 --> 00:03:37,540
 You could say that suffering or unhappiness is a good thing

70
00:03:37,540 --> 00:03:37,840
.

71
00:03:37,840 --> 00:03:43,050
 I don't think that's a very good explanation of reality

72
00:03:43,050 --> 00:03:46,200
 because by definition once we call

73
00:03:46,200 --> 00:03:48,460
 it suffering it's not a good thing.

74
00:03:48,460 --> 00:03:51,890
 So we're going to say that a good thing it brings happiness

75
00:03:51,890 --> 00:03:52,160
.

76
00:03:52,160 --> 00:03:55,230
 Therefore anything that is moral is something that brings

77
00:03:55,230 --> 00:03:56,040
 happiness.

78
00:03:56,040 --> 00:04:00,540
 We will say those things which bring true happiness, those

79
00:04:00,540 --> 00:04:02,560
 things are truly moral.

80
00:04:02,560 --> 00:04:05,200
 And when we look at it this way we can see that there

81
00:04:05,200 --> 00:04:07,080
 actually for sure must be certain

82
00:04:07,080 --> 00:04:12,270
 laws which bring about these states of happiness, this

83
00:04:12,270 --> 00:04:16,360
 reality which we would then call happiness

84
00:04:16,360 --> 00:04:21,580
 or freedom from suffering.

85
00:04:21,580 --> 00:04:26,140
 This allows us to at least form a basis of what we might

86
00:04:26,140 --> 00:04:28,600
 call ultimate morality.

87
00:04:28,600 --> 00:04:31,930
 And in fact people who are generally call themselves

88
00:04:31,930 --> 00:04:34,120
 relative moralists or people who

89
00:04:34,120 --> 00:04:37,310
 say that morality is something that changes over time

90
00:04:37,310 --> 00:04:39,400
 actually have a very rigid, very

91
00:04:39,400 --> 00:04:41,400
 universal understanding of morality anyway.

92
00:04:41,400 --> 00:04:46,170
 They would say that for instance in the case of dictators

93
00:04:46,170 --> 00:04:48,680
 or terribly cruel people you

94
00:04:48,680 --> 00:04:51,650
 have a moral responsibility to kill them for instance or to

95
00:04:51,650 --> 00:04:52,760
 do away with them.

96
00:04:52,760 --> 00:04:56,680
 And so they say for that reason morality is relative.

97
00:04:56,680 --> 00:05:00,260
 Well this is really absurd because what you're saying is

98
00:05:00,260 --> 00:05:02,560
 that we always have the moral, you

99
00:05:02,560 --> 00:05:05,930
 know it's moral, we have this moral principle that in order

100
00:05:05,930 --> 00:05:07,640
 to create happiness we have

101
00:05:07,640 --> 00:05:10,200
 to kill someone, we have to do something that other people

102
00:05:10,200 --> 00:05:11,600
 might say is a bad deed but this

103
00:05:11,600 --> 00:05:14,040
 is actually a good thing it actually brings happiness.

104
00:05:14,040 --> 00:05:15,760
 And so this is a moral law.

105
00:05:15,760 --> 00:05:21,900
 And they would say that always accrues, that always holds.

106
00:05:21,900 --> 00:05:25,570
 You couldn't say well what if there was, could you

107
00:05:25,570 --> 00:05:28,400
 sometimes kill someone who's doing bad

108
00:05:28,400 --> 00:05:32,610
 deeds or stop someone who's doing bad deeds and not

109
00:05:32,610 --> 00:05:34,520
 sometimes and so on.

110
00:05:34,520 --> 00:05:37,920
 And so what we're actually looking at is we're all trying

111
00:05:37,920 --> 00:05:40,000
 to find this understanding of what

112
00:05:40,000 --> 00:05:43,680
 are the laws of the mind, the ways that the mind can then

113
00:05:43,680 --> 00:05:45,840
 experience peace, experience

114
00:05:45,840 --> 00:05:48,280
 happiness, experience freedom from suffering and yet we

115
00:05:48,280 --> 00:05:49,680
 just approach them in different

116
00:05:49,680 --> 00:05:50,680
 ways.

117
00:05:50,680 --> 00:05:54,590
 So I don't think there's any reason for us to think that

118
00:05:54,590 --> 00:05:57,000
 there should not be an absolute

119
00:05:57,000 --> 00:06:01,880
 guide of what is right and what is wrong.

120
00:06:01,880 --> 00:06:04,960
 I think in general we're all looking for this.

121
00:06:04,960 --> 00:06:07,830
 We're all sort of expecting that there will be some laws

122
00:06:07,830 --> 00:06:09,440
 that govern the nature of the

123
00:06:09,440 --> 00:06:10,440
 mind.

124
00:06:10,440 --> 00:06:13,290
 Some people deny the existence of the mind but they still

125
00:06:13,290 --> 00:06:15,040
 think that there is some guiding

126
00:06:15,040 --> 00:06:17,840
 rules on how to find happiness and peace.

127
00:06:17,840 --> 00:06:24,200
 And those rules I would call moral.

128
00:06:24,200 --> 00:06:30,060
 The addition which I would give here is that once we give

129
00:06:30,060 --> 00:06:33,040
 the mind a real existence we

130
00:06:33,040 --> 00:06:37,250
 then have to realize that everything we do is going to then

131
00:06:37,250 --> 00:06:39,120
 affect the nature of the

132
00:06:39,120 --> 00:06:40,120
 mind.

133
00:06:40,120 --> 00:06:43,760
 So every time we kill it is going to affect our mind.

134
00:06:43,760 --> 00:06:46,920
 Every time we do something because killing or stealing or

135
00:06:46,920 --> 00:06:48,520
 all of these things which we

136
00:06:48,520 --> 00:06:51,300
 generally consider to be immoral we do have to understand

137
00:06:51,300 --> 00:06:52,680
 that they are going to have

138
00:06:52,680 --> 00:06:54,240
 an impact on our mind.

139
00:06:54,240 --> 00:06:56,240
 They are going to change the way we think.

140
00:06:56,240 --> 00:06:57,560
 This is why people feel guilt.

141
00:06:57,560 --> 00:06:59,520
 This is why people feel upset.

142
00:06:59,520 --> 00:07:02,040
 This is why people feel rage.

143
00:07:02,040 --> 00:07:03,800
 This is why people have nightmares.

144
00:07:03,800 --> 00:07:08,040
 It's why people who do bad things tend to have difficulty

145
00:07:08,040 --> 00:07:10,280
 meditating for instance.

146
00:07:10,280 --> 00:07:11,640
 They find that they have mental instability.

147
00:07:11,640 --> 00:07:14,320
 So this is the first of the things that I wanted to discuss

148
00:07:14,320 --> 00:07:14,560
.

149
00:07:14,560 --> 00:07:17,000
 The idea that there might be an absolute morality.

150
00:07:17,000 --> 00:07:20,770
 In fact through meditation it's very easy to see that there

151
00:07:20,770 --> 00:07:22,280
 is a law which governs the

152
00:07:22,280 --> 00:07:25,230
 mind but without any meditation I'm just going to leave it

153
00:07:25,230 --> 00:07:26,640
 at that and give sort of this

154
00:07:26,640 --> 00:07:31,600
 superficial explanation.

155
00:07:31,600 --> 00:07:34,870
 As soon as you practice any kind of meditation which allows

156
00:07:34,870 --> 00:07:36,480
 you to see the reality or the

157
00:07:36,480 --> 00:07:38,730
 nature of the mind it's very easy to see that it is

158
00:07:38,730 --> 00:07:40,620
 governed by very specific laws and these

159
00:07:40,620 --> 00:07:46,080
 are an absolute morality.

160
00:07:46,080 --> 00:07:48,320
 There are many other things I could say about morality.

161
00:07:48,320 --> 00:07:52,110
 Things like people talk about how absolute morality could

162
00:07:52,110 --> 00:07:54,080
 lead to suffering and so on.

163
00:07:54,080 --> 00:07:57,820
 The idea is that absolute morality could never lead to

164
00:07:57,820 --> 00:08:00,320
 suffering otherwise it wouldn't be

165
00:08:00,320 --> 00:08:05,280
 moral since we say that morality leads to happiness.

166
00:08:05,280 --> 00:08:07,640
 This is a very deep subject perhaps I'll have to give

167
00:08:07,640 --> 00:08:09,840
 another talk on exactly what is happiness

168
00:08:09,840 --> 00:08:14,360
 and arguing some of the fine details but I think this is

169
00:08:14,360 --> 00:08:17,480
 sort of a good basic understanding.

170
00:08:17,480 --> 00:08:18,480
 So thank you for watching.

171
00:08:18,480 --> 00:08:22,550
 This is the first in my series of sort of corallaries on

172
00:08:22,550 --> 00:08:24,840
 the existence of the mind.

