1
00:00:00,000 --> 00:00:05,170
 Is becoming a Sotah Pana something spontaneous or is it

2
00:00:05,170 --> 00:00:07,080
 gradual? Can one be a

3
00:00:07,080 --> 00:00:12,190
 Sotah Pana and not be aware of it or have some sort of

4
00:00:12,190 --> 00:00:13,840
 doubt about it?

5
00:00:13,840 --> 00:00:17,920
 Hmm, these are good questions.

6
00:00:23,920 --> 00:00:29,650
 Well, it's both really, you know. Becoming a Sotah Pana is

7
00:00:29,650 --> 00:00:31,760
 one moment. You're not

8
00:00:31,760 --> 00:00:36,520
 kind of a Sotah Pana, you're not partially Sotah Pana. You

9
00:00:36,520 --> 00:00:37,760
 either are or you aren't.

10
00:00:37,760 --> 00:00:43,040
 It's a categorical difference. A Sotah Pana is someone who

11
00:00:43,040 --> 00:00:43,280
's seen

12
00:00:43,280 --> 00:00:48,800
 Nibhana, a non-Sotah Pana is someone who is not.

13
00:00:49,040 --> 00:00:55,840
 That means it's spontaneous or instantaneous, but it's not

14
00:00:55,840 --> 00:00:56,600
 spontaneous

15
00:00:56,600 --> 00:01:01,430
 in the sense that it is gradual practice. You have to

16
00:01:01,430 --> 00:01:04,000
 cultivate awareness to the

17
00:01:04,000 --> 00:01:10,960
 point where the mind is able to break away from Sankaras

18
00:01:10,960 --> 00:01:12,400
 and reach the

19
00:01:12,400 --> 00:01:26,420
 asankata, the unformed, which is Nibhana. The second

20
00:01:26,420 --> 00:01:28,560
 question is more difficult

21
00:01:28,560 --> 00:01:34,470
 because I don't want to say the wrong thing and

22
00:01:34,470 --> 00:01:35,920
 misrepresent the truth.

23
00:01:35,920 --> 00:01:43,080
 But to me, you know, so this is how am I going to be the

24
00:01:43,080 --> 00:01:44,120
 arbiter of this question,

25
00:01:44,120 --> 00:01:47,360
 right? This is a question you have to ask the Buddha. A Sot

26
00:01:47,360 --> 00:01:48,360
ah Pana doesn't have

27
00:01:48,360 --> 00:01:58,730
 doubt, but my understanding and I think the commentary is

28
00:01:58,730 --> 00:02:00,200
 understanding is that,

29
00:02:00,200 --> 00:02:06,170
 and maybe even the tupitaka, what we have of the Buddha's

30
00:02:06,170 --> 00:02:06,480
 teaching

31
00:02:06,480 --> 00:02:10,560
 is under the understanding that that refers to specifically

32
00:02:10,560 --> 00:02:11,120
 doubt in the

33
00:02:11,120 --> 00:02:15,450
 Buddha, doubt in the Dhamma and doubt in the Sangha. Thus,

34
00:02:15,450 --> 00:02:16,840
 the Buddha is

35
00:02:16,840 --> 00:02:22,860
 enlightened, his teachings are the path to freedom from

36
00:02:22,860 --> 00:02:26,760
 suffering, and a person

37
00:02:26,760 --> 00:02:34,370
 who practices these teachings, a person who is enlightened,

38
00:02:34,370 --> 00:02:34,640
 is the

39
00:02:34,640 --> 00:02:41,250
 person who has practiced these teachings. That's the

40
00:02:41,250 --> 00:02:46,280
 confidence of a Sotah Pana,

41
00:02:46,280 --> 00:02:51,320
 that they have no doubt in these three things. It doesn't

42
00:02:51,320 --> 00:02:51,760
 say that they

43
00:02:51,760 --> 00:02:56,190
 have no doubt in themselves. And I think that's reasonable.

44
00:02:56,190 --> 00:02:58,120
 I don't want to give a

45
00:02:58,120 --> 00:03:03,200
 direct, a categorical answer here, but to me that seems

46
00:03:03,200 --> 00:03:05,560
 reasonable because Sotah

47
00:03:05,560 --> 00:03:08,850
 Pana is just a word and this happens all the time in

48
00:03:08,850 --> 00:03:11,320
 meditation. You think, you get

49
00:03:11,320 --> 00:03:14,180
 this idea that it's something, that it's an entity. Am I

50
00:03:14,180 --> 00:03:15,440
 yet? Am I there yet?

51
00:03:15,440 --> 00:03:20,640
 You're waiting for the signpost and so you're looking, how

52
00:03:20,640 --> 00:03:21,920
 do you know if you're

53
00:03:21,920 --> 00:03:25,760
 a Sotah Pana? I had some experience that makes me think

54
00:03:25,760 --> 00:03:26,920
 that I'm a Sotah Pana, but

55
00:03:26,920 --> 00:03:33,280
 am I a Sotah Pana? So we don't tend to answer people's

56
00:03:33,280 --> 00:03:34,360
 questions when they ask

57
00:03:34,360 --> 00:03:38,160
 about how do you know you're a Sotah Pana? Do you have

58
00:03:38,160 --> 00:03:38,480
 greed?

59
00:03:38,480 --> 00:03:41,160
 Do you have anger? Do you have delusion? Well, then there's

60
00:03:41,160 --> 00:03:42,040
 still further to go.

61
00:03:42,040 --> 00:03:45,330
 That's all you should know because the only thing that it

62
00:03:45,330 --> 00:03:46,400
 would do if you did

63
00:03:46,400 --> 00:03:49,720
 know that you're a Sotah Pana is it might, it would give

64
00:03:49,720 --> 00:03:51,040
 you the reassurance that

65
00:03:51,040 --> 00:03:54,200
 would maybe make you stop practicing or go make it lazy. If

66
00:03:54,200 --> 00:03:55,840
 you are a Sotah Pana

67
00:03:55,840 --> 00:03:58,340
 and you have doubt about it, you're still gonna work really

68
00:03:58,340 --> 00:04:00,600
 hard to push on, to cut

69
00:04:00,600 --> 00:04:06,720
 off more defilements. But I would say yes, a Sotah Pana can

70
00:04:06,720 --> 00:04:08,400
 have doubt. My guess is,

71
00:04:08,400 --> 00:04:15,430
 and I'm not sure that it's correct, a Sotah Pana can have

72
00:04:15,430 --> 00:04:20,040
 doubt because it's not,

73
00:04:20,040 --> 00:04:31,980
 it's not a sure thing. I would say an Arahant. I see I don

74
00:04:31,980 --> 00:04:32,680
't want to make these

75
00:04:32,680 --> 00:04:38,950
 categorical statements. I think it's reasonable to suggest

76
00:04:38,950 --> 00:04:39,880
 that a Sotah Pana

77
00:04:39,880 --> 00:04:43,560
 could have doubt. I think it's reasonable to suggest that

78
00:04:43,560 --> 00:04:45,320
 an Arahant may not have

79
00:04:45,320 --> 00:04:50,240
 doubt because the Arahant is free from delusion. So the Ara

80
00:04:50,240 --> 00:04:51,640
hant is in my mind

81
00:04:51,640 --> 00:04:57,730
 more likely of the two to be free from doubt in themselves

82
00:04:57,730 --> 00:04:58,360
 because in the

83
00:04:58,360 --> 00:05:03,530
 Buddha said, "Bhusitang brahmacaryanya katankaraniyam nati

84
00:05:03,530 --> 00:05:05,840
 dani punabhuoti nati

85
00:05:05,840 --> 00:05:11,760
 dani itarat nati." He knows for himself that there is

86
00:05:11,760 --> 00:05:14,200
 nothing further. This is an

87
00:05:14,200 --> 00:05:17,670
 Arahant. The Arahant knows for themselves. I would say a S

88
00:05:17,670 --> 00:05:18,580
otah Pana because they

89
00:05:18,580 --> 00:05:22,090
 still have greed, anger and delusion, they might still have

90
00:05:22,090 --> 00:05:23,360
 this confusion inside

91
00:05:23,360 --> 00:05:27,010
 of themselves and thus still have doubt. The doubt that has

92
00:05:27,010 --> 00:05:27,800
 disappeared is in the

93
00:05:27,800 --> 00:05:29,970
 Buddha Dhamma Sangha. So whether they know that they

94
00:05:29,970 --> 00:05:31,240
 themselves have reached it,

95
00:05:31,240 --> 00:05:34,460
 they'll just be confused and doubt about themselves. But

96
00:05:34,460 --> 00:05:35,560
 when they look deep down

97
00:05:35,560 --> 00:05:39,210
 they will be able to say that they have no doubt in the

98
00:05:39,210 --> 00:05:42,640
 Buddha, the Dhamma and the Sangha.

