1
00:00:00,000 --> 00:00:04,560
 In terms of generating karma, what difference is there

2
00:00:04,560 --> 00:00:10,000
 between making people happy and relieving people suffering?

3
00:00:21,000 --> 00:00:30,510
 Yeah, well on a mundane level or a conceptual level there

4
00:00:30,510 --> 00:00:33,000
 is a difference.

5
00:00:33,000 --> 00:00:38,910
 Bringing people happy feelings, for example, is quite

6
00:00:38,910 --> 00:00:41,000
 different from freeing them from suffering.

7
00:00:41,000 --> 00:00:46,000
 But on an ultimate level there's no difference.

8
00:00:46,000 --> 00:00:49,160
 There's no difference between happiness and freedom from

9
00:00:49,160 --> 00:00:50,000
 suffering.

10
00:00:50,000 --> 00:00:54,310
 Happiness, peace, peace, happiness and freedom from

11
00:00:54,310 --> 00:00:57,000
 suffering are all the same thing.

12
00:00:57,000 --> 00:01:09,200
 It is important to understand. It's important to note that

13
00:01:09,200 --> 00:01:12,000
 happy feelings are not the goal.

14
00:01:12,000 --> 00:01:16,000
 Even peaceful feelings are not the goal.

15
00:01:19,000 --> 00:01:23,000
 Because they're impermanent, they're part of the flux.

16
00:01:23,000 --> 00:01:26,550
 They come and they go and they might very well be replaced

17
00:01:26,550 --> 00:01:28,000
 by something else.

18
00:01:28,000 --> 00:01:30,460
 If the goal were those things then the goal would be

19
00:01:30,460 --> 00:01:32,000
 something that is impermanent.

20
00:01:32,000 --> 00:01:36,770
 And moreover it would be a goal that is subject to

21
00:01:36,770 --> 00:01:38,000
 attachment.

22
00:01:38,000 --> 00:01:44,170
 So it would be a goal that can still be clung to and it

23
00:01:44,170 --> 00:01:47,630
 would be a goal that wasn't capable of removing your

24
00:01:47,630 --> 00:01:48,000
 thinking.

25
00:01:48,000 --> 00:01:51,420
 Because it wasn't capable of removing your clinging, it

26
00:01:51,420 --> 00:01:57,000
 would still lead to suffering when the feelings disappear.

27
00:01:57,000 --> 00:02:04,340
 So if you're talking about that kind of happiness, it's

28
00:02:04,340 --> 00:02:06,000
 actually the opposite.

29
00:02:06,000 --> 00:02:11,160
 It's to give up the attainment of happy feelings or

30
00:02:11,160 --> 00:02:12,000
 sensations.

31
00:02:13,000 --> 00:02:17,840
 That truly does free you from suffering. And as long as

32
00:02:17,840 --> 00:02:21,260
 there is seeking out these states, some kind of preference

33
00:02:21,260 --> 00:02:22,000
 for these states,

34
00:02:22,000 --> 00:02:27,130
 thinking of them as the goal, then there will always be

35
00:02:27,130 --> 00:02:30,000
 suffering following it because they don't last.

36
00:02:30,000 --> 00:02:34,460
 And because of the nature of clinging, it necessitates

37
00:02:34,460 --> 00:02:36,000
 disappointment.

38
00:02:37,000 --> 00:02:44,000
 It takes you out of balance and creates stress.

