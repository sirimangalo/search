1
00:00:00,000 --> 00:00:03,990
 There it is. Wouldn't a Pacheka Buddha be benefiting others

2
00:00:03,990 --> 00:00:06,000
 because everything is interdependent?

3
00:00:06,000 --> 00:00:11,000
 I see. Just by becoming enlightened.

4
00:00:11,000 --> 00:00:15,670
 First of all, let's dispel the myth. A Pacheka Buddha is

5
00:00:15,670 --> 00:00:18,000
 not an island in isolation.

6
00:00:18,000 --> 00:00:22,000
 A Pacheka Buddha still interacts with their fellow beings,

7
00:00:22,000 --> 00:00:24,000
 still benefits very, very much benefits.

8
00:00:24,000 --> 00:00:28,530
 Their fellow beings simply as an example, for starters, and

9
00:00:28,530 --> 00:00:31,000
 also because of their teachings.

10
00:00:31,000 --> 00:00:35,640
 A Pacheka Buddha is not able to teach the Buddha's teaching

11
00:00:35,640 --> 00:00:36,000
.

12
00:00:36,000 --> 00:00:38,520
 It's not able to come up with, for example, the four satip

13
00:00:38,520 --> 00:00:43,000
atanas, the five khanda, the four noble truths.

14
00:00:43,000 --> 00:00:46,600
 But they are able to teach people. They are able to say

15
00:00:46,600 --> 00:00:50,000
 things, to encourage other people.

16
00:00:50,000 --> 00:00:53,160
 They just don't have what it takes to really understand

17
00:00:53,160 --> 00:00:56,000
 what it is that they've realized themselves.

18
00:00:56,000 --> 00:01:01,490
 They can talk about good things, and they can encourage

19
00:01:01,490 --> 00:01:04,000
 people in good things.

20
00:01:04,000 --> 00:01:08,000
 So they can... I'm not sure how far they can go with that.

21
00:01:08,000 --> 00:01:11,420
 I don't want to step out of line, but there seems no reason

22
00:01:11,420 --> 00:01:16,450
 for them to not be able to even teach people to give up

23
00:01:16,450 --> 00:01:18,000
 desires and so on,

24
00:01:18,000 --> 00:01:20,540
 and to practice meditation, to encourage them insofar as

25
00:01:20,540 --> 00:01:22,000
 they understand themselves.

26
00:01:22,000 --> 00:01:25,000
 There doesn't seem to be anything stopping them.

27
00:01:25,000 --> 00:01:27,220
 So the idea that a Pacheka Buddha is someone who keeps

28
00:01:27,220 --> 00:01:32,340
 their mouth shut and never whispers a word of what they've

29
00:01:32,340 --> 00:01:35,000
 come to know for themselves,

30
00:01:35,000 --> 00:01:40,000
 is I think has no backing.

31
00:01:40,000 --> 00:01:44,590
 I mean, in the commentaries there's a story that's probably

32
00:01:44,590 --> 00:01:48,000
 fairly famous among Buddhists of Anuruddha.

33
00:01:48,000 --> 00:01:53,620
 Anuruddha in a past life was a servant, a serving man, and

34
00:01:53,620 --> 00:01:59,000
 he gave alms to a Pacheka Buddha.

35
00:01:59,000 --> 00:02:04,250
 And he made a wish. He gave his only meal for the day

36
00:02:04,250 --> 00:02:08,240
 because, of course, he was so poor that he only had one

37
00:02:08,240 --> 00:02:09,000
 meal.

38
00:02:09,000 --> 00:02:11,470
 He gave his one meal of the day, he gave it to a Pacheka

39
00:02:11,470 --> 00:02:16,490
 Buddha, and he said, "As a result of this gift, may I never

40
00:02:16,490 --> 00:02:19,000
 hear the words 'nati'?"

41
00:02:19,000 --> 00:02:25,000
 The word 'nati' means there isn't any, or there is not.

42
00:02:25,000 --> 00:02:27,000
 But in this case, his meaning was there isn't any.

43
00:02:27,000 --> 00:02:29,590
 He would never... "May I never hear the words, there isn't

44
00:02:29,590 --> 00:02:30,000
 any."

45
00:02:30,000 --> 00:02:33,650
 So whenever... meaning whenever he wanted something, he

46
00:02:33,650 --> 00:02:35,000
 would always get it.

47
00:02:35,000 --> 00:02:36,950
 And the Pacheka Buddha said something like, "Ei wang hotu,

48
00:02:36,950 --> 00:02:41,000
 may it be like that?" and went away.

49
00:02:41,000 --> 00:02:44,000
 So Anuruddha was very, very happy, and he went back.

50
00:02:44,000 --> 00:02:46,740
 I think he also made a vow, "May I also realize the

51
00:02:46,740 --> 00:02:50,000
 teaching, the truth that you have come to realize?"

52
00:02:50,000 --> 00:02:53,470
 And, "Well, I still haven't realized the truth, may I never

53
00:02:53,470 --> 00:02:55,000
 hear the word 'nati'?"

54
00:02:55,000 --> 00:03:05,000
 When he goes back, and he tells... he's so happy that...

55
00:03:05,000 --> 00:03:06,530
 something like... he's so happy that his boss, his owner,

56
00:03:06,530 --> 00:03:08,000
 or whatever, his employer,

57
00:03:08,000 --> 00:03:12,760
 asks him, "What happened? Why is he so happy?" and he tells

58
00:03:12,760 --> 00:03:13,000
 him.

59
00:03:13,000 --> 00:03:17,180
 And the owner says, "I'll give you something like a

60
00:03:17,180 --> 00:03:21,000
 thousand gold coins if you give me the merit.

61
00:03:21,000 --> 00:03:25,650
 The goodness of what you've done. Give me all of your

62
00:03:25,650 --> 00:03:29,000
 goodness and I'll give you a thousand gold coins."

63
00:03:29,000 --> 00:03:31,390
 All that you've gained from it. Because there was a big

64
00:03:31,390 --> 00:03:34,000
 idea that, even in old times,

65
00:03:34,000 --> 00:03:38,000
 that doing these deeds got you some kind of punya,

66
00:03:38,000 --> 00:03:39,000
 something that could...

67
00:03:39,000 --> 00:03:43,110
 The idea that he had was that maybe it could be bought,

68
00:03:43,110 --> 00:03:46,000
 because of course he was probably a fairly rich man,

69
00:03:46,000 --> 00:03:49,000
 and everything of course could be bought, even people.

70
00:03:49,000 --> 00:03:52,000
 So why couldn't you buy punya?

71
00:03:52,000 --> 00:03:57,050
 Anuruddha, or the man who was to be Anuruddha, he was of

72
00:03:57,050 --> 00:04:00,000
 course totally horrified by this,

73
00:04:00,000 --> 00:04:04,290
 and afraid that it might somehow be possible to buy his pun

74
00:04:04,290 --> 00:04:06,000
ya, his goodness.

75
00:04:06,000 --> 00:04:09,000
 The goodness that he had gained from doing this.

76
00:04:09,000 --> 00:04:13,320
 And so he said, "No way, I would never give this to anyone,

77
00:04:13,320 --> 00:04:15,000
 not for any price."

78
00:04:15,000 --> 00:04:21,730
 And the man, the guy was so upset and so desirous of this

79
00:04:21,730 --> 00:04:25,000
 goodness that he said,

80
00:04:25,000 --> 00:04:31,470
 "Well, okay, you give me half of your punya and keep half

81
00:04:31,470 --> 00:04:33,000
 for yourself,

82
00:04:33,000 --> 00:04:38,000
 and I'll buy half of it for whatever, 500 gold coins."

83
00:04:38,000 --> 00:04:40,000
 And here Anuruddha was thinking, "But what does that mean?

84
00:04:40,000 --> 00:04:43,000
 Does that mean I lose half of my goodness?"

85
00:04:43,000 --> 00:04:46,380
 He's thinking and he's not sure, and then he goes back and

86
00:04:46,380 --> 00:04:48,000
 he asks his wife,

87
00:04:48,000 --> 00:04:50,450
 and his wife says, "Well, why don't you go ask the Pacheka

88
00:04:50,450 --> 00:04:51,000
 Buddha?"

89
00:04:51,000 --> 00:04:54,000
 And so he goes back and he goes to the Pacheka Buddha and

90
00:04:54,000 --> 00:04:54,000
 he says,

91
00:04:54,000 --> 00:04:58,480
 "No, look, this is the case. I've got this great wonderful

92
00:04:58,480 --> 00:05:03,000
 merit for doing this good deed of giving you alms,

93
00:05:03,000 --> 00:05:07,580
 but now my boss wants to take half of it. Does that mean I

94
00:05:07,580 --> 00:05:09,000
'm going to lose half of my punya?"

95
00:05:09,000 --> 00:05:12,430
 But the Pacheka Buddha teaches him. It's the only direct

96
00:05:12,430 --> 00:05:16,620
 teaching that I can remember of a Pacheka Buddha that

97
00:05:16,620 --> 00:05:18,000
 occurs.

98
00:05:18,000 --> 00:05:22,000
 It's the only one I can remember.

99
00:05:22,000 --> 00:05:27,120
 He says, "Well, consider this. Suppose you have a candle

100
00:05:27,120 --> 00:05:30,000
 and you light the candle,

101
00:05:30,000 --> 00:05:33,980
 and then you've got one light. Now suppose everyone in your

102
00:05:33,980 --> 00:05:35,000
 whole village

103
00:05:35,000 --> 00:05:40,000
 comes and lights their candle from your candle.

104
00:05:40,000 --> 00:05:42,470
 Suppose you were to do that for a thousand people. Would

105
00:05:42,470 --> 00:05:45,000
 you lose the light from your candle?"

106
00:05:45,000 --> 00:05:48,800
 And he replied, he said, "No, it would be like one candle

107
00:05:48,800 --> 00:05:53,000
 became a thousand. One light became a thousand lights."

108
00:05:53,000 --> 00:05:57,070
 And the Pacheka Buddha says, "In the very same way,

109
00:05:57,070 --> 00:05:59,000
 goodness can never be transferred.

110
00:05:59,000 --> 00:06:05,000
 It can only be multiplied. When you dedicate your merit,

111
00:06:05,000 --> 00:06:08,000
 your goodness to someone else,

112
00:06:08,000 --> 00:06:11,000
 it's as though your goodness had become twice."

113
00:06:11,000 --> 00:06:13,220
 And of course he was very happy with this because he's

114
00:06:13,220 --> 00:06:15,000
 going to get money and keep his punya.

115
00:06:15,000 --> 00:06:18,560
 So he goes back and he says, "Yes, yes, I dedicate half of

116
00:06:18,560 --> 00:06:21,000
 my punya, or even I think something like all of my punya.

117
00:06:21,000 --> 00:06:24,000
 I dedicate it to you. I give it all over to you."

118
00:06:24,000 --> 00:06:28,000
 Which actually, I used to use this story as an example of

119
00:06:28,000 --> 00:06:31,000
 how actually it gives you more merit to do that.

120
00:06:31,000 --> 00:06:34,410
 When you dedicate your merit and wish for your merit, the

121
00:06:34,410 --> 00:06:38,000
 power of your goodness may it benefit someone else.

122
00:06:38,000 --> 00:06:42,670
 You actually get more merit as a result, of course, because

123
00:06:42,670 --> 00:06:45,000
 your heart becomes pure.

124
00:06:45,000 --> 00:06:49,000
 So that's an example of the teachings of Pacheka Buddha.

125
00:06:49,000 --> 00:06:52,150
 This story goes on and talks about how he actually does

126
00:06:52,150 --> 00:06:54,000
 never hear the word "nutty".

127
00:06:54,000 --> 00:07:02,000
 And as a result, one day his mother runs out of cakes.

128
00:07:02,000 --> 00:07:05,000
 And he says, "Well then, bring the nutty."

129
00:07:05,000 --> 00:07:08,000
 She says, "Tell him there are no cakes. Nutty."

130
00:07:08,000 --> 00:07:11,000
 And he says, "Nutty? Nutty? What does this mean?"

131
00:07:11,000 --> 00:07:13,680
 And he says, "Well then, tell her to bring the nutty cakes

132
00:07:13,680 --> 00:07:14,000
."

133
00:07:14,000 --> 00:07:18,000
 That's fine. I'll take the nutty cakes.

134
00:07:18,000 --> 00:07:24,000
 That's funnier when I read it.

135
00:07:24,000 --> 00:07:28,000
 So, okay, I haven't even answered the question yet.

136
00:07:28,000 --> 00:07:32,000
 So the point being that they do benefit directly.

137
00:07:32,000 --> 00:07:35,600
 They are able to teach directly. They're just not able to

138
00:07:35,600 --> 00:07:39,000
 teach someone the path to become enlightened.

139
00:07:39,000 --> 00:07:41,710
 That's what makes them a Pacheka Buddha. And of course,

140
00:07:41,710 --> 00:07:43,000
 they would be different.

141
00:07:43,000 --> 00:07:46,000
 So some of them might be better to teach than others.

142
00:07:46,000 --> 00:07:50,010
 Some of them might even teach things that lead people on to

143
00:07:50,010 --> 00:07:53,000
 practice the Buddha's teaching later on.

144
00:07:53,000 --> 00:07:57,950
 I wouldn't say there are any hard and fast rules in that

145
00:07:57,950 --> 00:07:59,000
 regard.

146
00:07:59,000 --> 00:08:02,570
 But whether someone, be they a Pacheka Buddha or even an A

147
00:08:02,570 --> 00:08:07,000
rahal, benefits others by just becoming enlightened,

148
00:08:07,000 --> 00:08:13,000
 simply because of interdependence, I'm all for it.

149
00:08:13,000 --> 00:08:18,760
 I think as we better ourselves, those people who are

150
00:08:18,760 --> 00:08:22,000
 connected with us benefit as well.

151
00:08:22,000 --> 00:08:28,090
 Consider what it would have been like had you not practiced

152
00:08:28,090 --> 00:08:31,000
 and become enlightened.

153
00:08:31,000 --> 00:08:33,610
 If you take just an ordinary meditator, suppose they

154
00:08:33,610 --> 00:08:34,000
 practice,

155
00:08:34,000 --> 00:08:37,000
 suppose they become an Arahant.

156
00:08:37,000 --> 00:08:41,710
 Suppose they hadn't done that. If you compare the state of

157
00:08:41,710 --> 00:08:44,000
 things, if they hadn't done that, and if they had,

158
00:08:44,000 --> 00:08:48,000
 if they become an Arahant and then they die or something,

159
00:08:48,000 --> 00:08:50,290
 or then they go off in the forest and live the rest of

160
00:08:50,290 --> 00:08:51,000
 their life.

161
00:08:51,000 --> 00:08:54,000
 Well, if they hadn't done that, they would have, of course,

162
00:08:54,000 --> 00:08:56,000
 based on their defilements, based on their attachments,

163
00:08:56,000 --> 00:09:00,000
 they would have encouraged all sorts of unpleasant things.

164
00:09:00,000 --> 00:09:03,650
 If you consider it just from that aspect, they've benefited

165
00:09:03,650 --> 00:09:05,000
 others immensely.

166
00:09:05,000 --> 00:09:09,000
 They've freed others from the karmic cycle.

167
00:09:09,000 --> 00:09:12,000
 So if you have a karmic cycle of vengeance towards someone,

168
00:09:12,000 --> 00:09:14,000
 where you might not have bad feelings towards them,

169
00:09:14,000 --> 00:09:17,000
 but somehow you're going to do something to hurt them

170
00:09:17,000 --> 00:09:19,000
 because they've hurt you in the past,

171
00:09:19,000 --> 00:09:23,600
 then by becoming enlightened, you, you know, and going off

172
00:09:23,600 --> 00:09:28,000
 in the forest, for example, you free others from that.

173
00:09:28,000 --> 00:09:30,390
 So you might even say that by becoming enlightened, the

174
00:09:30,390 --> 00:09:34,000
 world around us becomes freer.

175
00:09:34,000 --> 00:09:37,430
 You'll also often find that these beings pull others in

176
00:09:37,430 --> 00:09:38,000
 with them.

177
00:09:38,000 --> 00:09:42,000
 So as a result of their becoming enlightened as an example,

178
00:09:42,000 --> 00:09:47,070
 or even just because of karmic relationships, they pull all

179
00:09:47,070 --> 00:09:50,000
 the people around them in as well.

180
00:09:51,000 --> 00:10:07,000
 [

