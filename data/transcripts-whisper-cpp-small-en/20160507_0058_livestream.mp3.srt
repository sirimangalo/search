1
00:00:00,000 --> 00:00:16,320
 Good evening everyone.

2
00:00:16,320 --> 00:00:25,320
 I'm broadcasting live May 6th.

3
00:00:25,320 --> 00:00:41,640
 Today's quote is somewhat curious.

4
00:00:41,640 --> 00:00:44,120
 Not quite, well, Bhikkhobodi's translation of course is

5
00:00:44,120 --> 00:00:47,200
 preferred, but the word isn't

6
00:00:47,200 --> 00:00:48,480
 quite false.

7
00:00:48,480 --> 00:00:50,680
 It's 'wipati'.

8
00:00:50,680 --> 00:00:59,320
 'Wipati' is failure, failings as Bhikkhobodi translates it.

9
00:00:59,320 --> 00:01:07,680
 The 'wipati' and 'sampati'.

10
00:01:07,680 --> 00:01:12,200
 'Wipati' and 'sampati'.

11
00:01:12,200 --> 00:01:20,720
 'Sampati' means lacking.

12
00:01:20,720 --> 00:01:37,960
 'Sampati' means attaining our failings and our attainment.

13
00:01:37,960 --> 00:01:44,440
 So the Buddha says, and it's kind of, I wouldn't put too

14
00:01:44,440 --> 00:01:47,840
 much on this because the context of

15
00:01:47,840 --> 00:01:50,960
 the quote is that Devadatta just left.

16
00:01:50,960 --> 00:01:56,600
 Devadatta of course was the Buddha's cousin who, well, he

17
00:01:56,600 --> 00:02:00,960
 came to the Buddha and he said,

18
00:02:00,960 --> 00:02:06,560
 he said, "Venerable sir, you're getting old.

19
00:02:06,560 --> 00:02:14,240
 So why don't you pass the Sangha on to me, all the Sangha?"

20
00:02:14,240 --> 00:02:16,440
 And the Buddha said to him, "I wouldn't even hand it over

21
00:02:16,440 --> 00:02:17,960
 to Sariputta and Moggallana,

22
00:02:17,960 --> 00:02:22,200
 let alone some insignificant little person like you."

23
00:02:22,200 --> 00:02:28,600
 He said something really mean to him actually, kind of.

24
00:02:28,600 --> 00:02:31,650
 Because Devadatta is out of control and so there's only one

25
00:02:31,650 --> 00:02:33,000
 way it's going to end.

26
00:02:33,000 --> 00:02:35,360
 In the end the Buddha has to be firm.

27
00:02:35,360 --> 00:02:39,350
 It's rare because he's not usually like this but with Devad

28
00:02:39,350 --> 00:02:41,240
atta he had to be firm at all

29
00:02:41,240 --> 00:02:42,720
 times.

30
00:02:42,720 --> 00:02:49,620
 And so Devadatta got very upset and he tried to scheme up a

31
00:02:49,620 --> 00:02:52,880
 way to take control of the

32
00:02:52,880 --> 00:02:57,590
 Sangha and so he came to the Buddha later and asked him to

33
00:02:57,590 --> 00:03:00,160
 instate five rules for monks

34
00:03:00,160 --> 00:03:04,780
 that they should always live in the forest, that they

35
00:03:04,780 --> 00:03:07,640
 should only eat vegetarian food

36
00:03:07,640 --> 00:03:10,850
 and things that actually were contentious and seemed

37
00:03:10,850 --> 00:03:13,080
 reasonable but the Buddha couldn't

38
00:03:13,080 --> 00:03:16,820
 enforce and he stopped short of forcing monks even though

39
00:03:16,820 --> 00:03:18,920
 he praised living in the forest

40
00:03:18,920 --> 00:03:22,370
 and even though obviously eating meat is problematic and

41
00:03:22,370 --> 00:03:24,840
 there are types of meat that absolutely

42
00:03:24,840 --> 00:03:29,960
 shouldn't be eaten and that kind of thing.

43
00:03:29,960 --> 00:03:35,720
 So he refused and Devadatta used that as grounds to start a

44
00:03:35,720 --> 00:03:36,880
 schism.

45
00:03:36,880 --> 00:03:41,520
 He got monks on his side saying, "Hey, the Buddha clearly

46
00:03:41,520 --> 00:03:43,840
 doesn't know what he's doing."

47
00:03:43,840 --> 00:03:46,880
 But the monks he got on his side were actually new monks.

48
00:03:46,880 --> 00:03:49,240
 They're monks who didn't know better.

49
00:03:49,240 --> 00:03:52,230
 So they hadn't studied their mind, they hadn't really

50
00:03:52,230 --> 00:03:54,320
 learned the intricacies of monastic

51
00:03:54,320 --> 00:03:59,640
 life and so they thought Devadatta was the more serious.

52
00:03:59,640 --> 00:04:03,400
 More serious than the Buddha.

53
00:04:03,400 --> 00:04:11,120
 The Buddha seemed to be actually kind of lax and indulgent

54
00:04:11,120 --> 00:04:12,840
 and so they went with Devadatta

55
00:04:12,840 --> 00:04:16,490
 but later the Buddha had Sariputta and Mughalana go to see

56
00:04:16,490 --> 00:04:18,520
 them and it's kind of funny how

57
00:04:18,520 --> 00:04:20,600
 it turned out.

58
00:04:20,600 --> 00:04:24,440
 This sutta is actually before that but later on Sariputta

59
00:04:24,440 --> 00:04:26,920
 and Mughalana went to see Devadatta

60
00:04:26,920 --> 00:04:30,130
 and Devadatta said, "Oh look, here come the Buddha's two

61
00:04:30,130 --> 00:04:32,120
 chief disciples, they're joining

62
00:04:32,120 --> 00:04:34,120
 us as well."

63
00:04:34,120 --> 00:04:38,730
 Sariputta and Mughalana didn't say anything and so Devad

64
00:04:38,730 --> 00:04:41,360
atta said, "Sariputta and Mughalana,

65
00:04:41,360 --> 00:04:44,720
 you teach the monks, now I'm tired."

66
00:04:44,720 --> 00:04:47,190
 And so he pretended to be like the Buddha where the Buddha

67
00:04:47,190 --> 00:04:48,360
 would say, "You teach,"

68
00:04:48,360 --> 00:04:52,560
 and the Buddha would lie down and listen mindfully but

69
00:04:52,560 --> 00:04:56,240
 instead Devadatta lay down and fell asleep.

70
00:04:56,240 --> 00:05:00,560
 And so Sariputta and Mughalana taught the monks the truth

71
00:05:00,560 --> 00:05:02,680
 and converted them all and they

72
00:05:02,680 --> 00:05:06,360
 all went back to see the Buddha.

73
00:05:06,360 --> 00:05:08,990
 They all went back to be with the Buddha once they realized

74
00:05:08,990 --> 00:05:10,240
 what was right and what was

75
00:05:10,240 --> 00:05:15,380
 wrong and Devadatta's sort of second in command kicked Dev

76
00:05:15,380 --> 00:05:18,040
adatta in the chest to wake him

77
00:05:18,040 --> 00:05:22,540
 up and was very angry and said, "Look, look at what's

78
00:05:22,540 --> 00:05:23,720
 happened.

79
00:05:23,720 --> 00:05:27,950
 I told you to be careful of these two and you didn't listen

80
00:05:27,950 --> 00:05:28,360
."

81
00:05:28,360 --> 00:05:31,440
 But he kicked him in the chest and it wounded him and that

82
00:05:31,440 --> 00:05:32,880
 ended up being fatal.

83
00:05:32,880 --> 00:05:35,400
 That was what killed him in the end.

84
00:05:35,400 --> 00:05:41,520
 He got sick after that, coughed up in blood.

85
00:05:41,520 --> 00:05:46,470
 But so this quote is somewhere during this whole fiasco

86
00:05:46,470 --> 00:05:49,440
 when Devadatta has just left.

87
00:05:49,440 --> 00:05:54,150
 And so the Buddha kind of to introduce the topic, he says

88
00:05:54,150 --> 00:05:56,440
 it's appropriate from time

89
00:05:56,440 --> 00:06:02,730
 to time, "kalina kalam," to talk about one's own failings,

90
00:06:02,730 --> 00:06:05,520
 not talk about bhutu bhachawe

91
00:06:05,520 --> 00:06:08,520
 kita, to consider.

92
00:06:08,520 --> 00:06:17,320
 It is good that they be considered, reflected upon.

93
00:06:17,320 --> 00:06:23,440
 And it's good to reflect upon the failings of others.

94
00:06:23,440 --> 00:06:28,320
 And you have to take this with a grain of salt.

95
00:06:28,320 --> 00:06:30,960
 It's important not to get obsessed with the faults of

96
00:06:30,960 --> 00:06:32,840
 others because the Buddha has said,

97
00:06:32,840 --> 00:06:37,390
 of course, "nappre sang willo mani nappre sang gatagatango

98
00:06:37,390 --> 00:06:37,840
."

99
00:06:37,840 --> 00:06:42,170
 Don't worry about one should not become obsessed with other

100
00:06:42,170 --> 00:06:43,720
 people's faults.

101
00:06:43,720 --> 00:06:47,300
 So here he's cautious, he says, "kalina kalam," from time

102
00:06:47,300 --> 00:06:48,040
 to time.

103
00:06:48,040 --> 00:06:49,560
 It's useful, right?

104
00:06:49,560 --> 00:06:51,860
 Because if you look at Devadatta as an example and if you

105
00:06:51,860 --> 00:06:53,120
 talk about him and think about

106
00:06:53,120 --> 00:06:57,040
 him, you can say, "Hmm, that's not how I want to be.

107
00:06:57,040 --> 00:07:02,960
 That's something that leads to stress and suffering."

108
00:07:02,960 --> 00:07:05,370
 And he says it's good from time to time to think of your

109
00:07:05,370 --> 00:07:07,160
 own attainments and the attainments

110
00:07:07,160 --> 00:07:09,360
 of others.

111
00:07:09,360 --> 00:07:12,650
 But then he gets on to the meat of the sutta, which is

112
00:07:12,650 --> 00:07:14,840
 actually more interesting.

113
00:07:14,840 --> 00:07:20,210
 He says, well, he says, "Devadatta is going to hell," right

114
00:07:20,210 --> 00:07:20,600
?

115
00:07:20,600 --> 00:07:23,600
 That's what he says.

116
00:07:23,600 --> 00:07:31,400
 Wait, where is it?

117
00:07:31,400 --> 00:07:35,410
 There are these eight by eight, let me read the English

118
00:07:35,410 --> 00:07:37,120
 because I'm...

119
00:07:37,120 --> 00:07:40,720
 Because he was overcome and obsessed by eight bad

120
00:07:40,720 --> 00:07:43,840
 conditions, Devadatta is bound for the

121
00:07:43,840 --> 00:07:47,000
 plane of misery, bound for hell, and he will remain there

122
00:07:47,000 --> 00:07:47,800
 for a neon.

123
00:07:47,800 --> 00:07:51,290
 So the point is to introduce, he's now going to talk about

124
00:07:51,290 --> 00:07:53,720
 Devadatta's failings, what Devadatta

125
00:07:53,720 --> 00:07:54,720
 did wrong.

126
00:07:54,720 --> 00:07:59,450
 And these are interesting because these eight things are a

127
00:07:59,450 --> 00:08:01,840
 list of things that we have to

128
00:08:01,840 --> 00:08:04,000
 be not obsessed with.

129
00:08:04,000 --> 00:08:07,600
 We cannot let overcome us.

130
00:08:07,600 --> 00:08:10,800
 The first is gain.

131
00:08:10,800 --> 00:08:15,650
 Some people are obsessed with gain, wanting money, wanting

132
00:08:15,650 --> 00:08:18,080
 possessions, wanting to get

133
00:08:18,080 --> 00:08:19,080
 things.

134
00:08:19,080 --> 00:08:26,100
 And when you get obsessed with that, it overpowers you, it

135
00:08:26,100 --> 00:08:28,760
 inflames the mind.

136
00:08:28,760 --> 00:08:33,050
 Come by loss when you don't get what you want, when you

137
00:08:33,050 --> 00:08:34,880
 lose what you like.

138
00:08:34,880 --> 00:08:39,320
 Fame, number three is fame.

139
00:08:39,320 --> 00:08:43,160
 And people are obsessed with being famous.

140
00:08:43,160 --> 00:08:50,250
 People who post YouTube videos, they can be obsessed with

141
00:08:50,250 --> 00:08:53,720
 people's...how many subscribers

142
00:08:53,720 --> 00:08:54,720
 they have or so.

143
00:08:54,720 --> 00:08:58,960
 We go on Facebook, we're obsessed by how many likes we get.

144
00:08:58,960 --> 00:09:00,960
 That kind of thing.

145
00:09:00,960 --> 00:09:03,680
 By disrepute the opposite of fame.

146
00:09:03,680 --> 00:09:11,000
 You're obsessed when people have a bad image of you or when

147
00:09:11,000 --> 00:09:14,600
 you become infamous or when

148
00:09:14,600 --> 00:09:20,120
 no one knows who you are.

149
00:09:20,120 --> 00:09:25,720
 By honor, not sure what the context here of honor is.

150
00:09:25,720 --> 00:09:28,920
 Look at this.

151
00:09:28,920 --> 00:09:29,920
 Sakara.

152
00:09:29,920 --> 00:09:43,760
 Sakara is similar to fame, but it's when people present you

153
00:09:43,760 --> 00:09:46,920
 with things like respect and gifts

154
00:09:46,920 --> 00:09:54,280
 and praise and honor.

155
00:09:54,280 --> 00:09:56,240
 That's where he gets the word honor.

156
00:09:56,240 --> 00:09:58,640
 Sakara means doing rightly.

157
00:09:58,640 --> 00:10:08,970
 People are obsessed with other people, praising them and

158
00:10:08,970 --> 00:10:13,000
 honoring them and rewarding them,

159
00:10:13,000 --> 00:10:19,480
 that kind of thing.

160
00:10:19,480 --> 00:10:20,960
 Number seven by evil desires.

161
00:10:20,960 --> 00:10:23,480
 He was obsessed by evil desires.

162
00:10:23,480 --> 00:10:26,720
 Devadatta had the most evil of desires.

163
00:10:26,720 --> 00:10:27,840
 He wanted to kill the Buddha.

164
00:10:27,840 --> 00:10:30,960
 He wanted to become...he ended up trying to kill the Buddha

165
00:10:30,960 --> 00:10:31,160
.

166
00:10:31,160 --> 00:10:35,230
 He so wanted to be head of the monks, head of the Sangha so

167
00:10:35,230 --> 00:10:37,800
 badly that he did evil things.

168
00:10:37,800 --> 00:10:41,000
 He was jealous of the Buddha.

169
00:10:41,000 --> 00:10:44,890
 He was covetous of the Buddha's position, that kind of

170
00:10:44,890 --> 00:10:45,640
 thing.

171
00:10:45,640 --> 00:10:53,720
 And number eight, bad friendship.

172
00:10:53,720 --> 00:10:56,240
 Devadatta was actually the bad friendship himself.

173
00:10:56,240 --> 00:11:02,820
 He ended up making friends with King Bimbisara's son who

174
00:11:02,820 --> 00:11:06,520
 ended up killing his father.

175
00:11:06,520 --> 00:11:19,160
 And Jatasattu was actually able to kill his father.

176
00:11:19,160 --> 00:11:23,580
 But association with bad friends means if you're on a bad

177
00:11:23,580 --> 00:11:25,720
 path, it's not a good idea

178
00:11:25,720 --> 00:11:30,200
 to find people who have your own faults, right?

179
00:11:30,200 --> 00:11:31,960
 We say birds of a feather flock together.

180
00:11:31,960 --> 00:11:35,240
 Well, that's not always wise.

181
00:11:35,240 --> 00:11:37,900
 If you have faults, then maybe it's better to associate

182
00:11:37,900 --> 00:11:39,400
 with people who don't have those

183
00:11:39,400 --> 00:11:40,400
 faults.

184
00:11:40,400 --> 00:11:42,440
 They can teach you something.

185
00:11:42,440 --> 00:11:43,760
 They can remind you.

186
00:11:43,760 --> 00:11:47,880
 They can challenge you.

187
00:11:47,880 --> 00:11:49,760
 But we tend the other way, right?

188
00:11:49,760 --> 00:11:54,840
 We tend to flock towards birds with the same faults.

189
00:11:54,840 --> 00:12:01,630
 We tend to flock towards...if we are angry sort of person,

190
00:12:01,630 --> 00:12:04,400
 we tend to find solace in

191
00:12:04,400 --> 00:12:06,880
 other people who are angry.

192
00:12:06,880 --> 00:12:07,880
 We don't seem so out of place.

193
00:12:07,880 --> 00:12:10,110
 If we're around people who are not angry, we feel guilty

194
00:12:10,110 --> 00:12:10,800
 all the time.

195
00:12:10,800 --> 00:12:11,800
 We feel bad.

196
00:12:11,800 --> 00:12:14,480
 So it's actually easier to be around angry people, which is

197
00:12:14,480 --> 00:12:16,080
, of course, the most dangerous

198
00:12:16,080 --> 00:12:18,730
 thing because you're just going to become more of an angry

199
00:12:18,730 --> 00:12:19,280
 person.

200
00:12:19,280 --> 00:12:24,090
 Greed, if you're a greedy person, shouldn't be around other

201
00:12:24,090 --> 00:12:26,920
 addicts or other greedy people.

202
00:12:26,920 --> 00:12:31,770
 If you're deluded, if you're full of arrogance and conceit

203
00:12:31,770 --> 00:12:32,880
 and so on.

204
00:12:32,880 --> 00:12:35,880
 You should be surrounded by humble people.

205
00:12:35,880 --> 00:12:41,810
 You should go to seek out humble people to teach you

206
00:12:41,810 --> 00:12:43,440
 humility.

207
00:12:43,440 --> 00:12:47,660
 And then he says, "It is good for a bhikkhu to overcome

208
00:12:47,660 --> 00:12:50,240
 these eight things whenever they

209
00:12:50,240 --> 00:12:51,240
 arise."

210
00:12:51,240 --> 00:12:52,240
 "Overcome."

211
00:12:52,240 --> 00:12:57,760
 Let's see what the word he uses for overcome.

212
00:12:57,760 --> 00:13:05,160
 "Abibuya," and the commentary talks about "Abibuya."

213
00:13:05,160 --> 00:13:10,280
 This is an interesting word.

214
00:13:10,280 --> 00:13:17,640
 I guess it just means overcome, to be above.

215
00:13:17,640 --> 00:13:27,020
 There it says, "Abibhavitva, Maditva," to subjugate, to

216
00:13:27,020 --> 00:13:28,920
 conquer.

217
00:13:28,920 --> 00:13:32,200
 One should conquer these things.

218
00:13:32,200 --> 00:13:34,800
 These are our enemies.

219
00:13:34,800 --> 00:13:36,200
 The Vadatta wasn't the enemy.

220
00:13:36,200 --> 00:13:37,600
 The Buddha wasn't his enemy.

221
00:13:37,600 --> 00:13:42,200
 His enemy was these eight things.

222
00:13:42,200 --> 00:13:48,120
 This is obsession for these eight things, I should say.

223
00:13:48,120 --> 00:13:51,100
 Because of course, gain is not a problem, loss is not a

224
00:13:51,100 --> 00:13:51,840
 problem.

225
00:13:51,840 --> 00:13:56,070
 It's not these things that are the problem except for eva

226
00:13:56,070 --> 00:13:56,920
 wishes.

227
00:13:56,920 --> 00:13:58,680
 But the real problem is our obsession.

228
00:13:58,680 --> 00:14:01,800
 This goes actually with even our emotions.

229
00:14:01,800 --> 00:14:05,000
 If you're an angry person, that's not the biggest problem.

230
00:14:05,000 --> 00:14:09,000
 The biggest problem is your obsession with it when you let

231
00:14:09,000 --> 00:14:11,480
 it consume you, when you have

232
00:14:11,480 --> 00:14:12,480
 a desire.

233
00:14:12,480 --> 00:14:14,640
 That's not the biggest problem.

234
00:14:14,640 --> 00:14:18,900
 The biggest problem is if you fail to address it, fail to

235
00:14:18,900 --> 00:14:23,720
 rise above it, to overcome it.

236
00:14:23,720 --> 00:14:30,190
 When you have drowsiness, when you have distraction, when

237
00:14:30,190 --> 00:14:34,200
 you have doubt, you have doubt about

238
00:14:34,200 --> 00:14:37,470
 the Buddha's teaching or the practice, the doubt isn't the

239
00:14:37,470 --> 00:14:38,880
 biggest problem that you let

240
00:14:38,880 --> 00:14:41,460
 it get to you.

241
00:14:41,460 --> 00:14:43,880
 Even these things, they're bad.

242
00:14:43,880 --> 00:14:47,310
 But they really become bad when you follow them, when you

243
00:14:47,310 --> 00:14:49,280
 chase them, when you make much

244
00:14:49,280 --> 00:14:52,680
 of them.

245
00:14:52,680 --> 00:14:56,690
 And so with these eight things, it's really the obsession

246
00:14:56,690 --> 00:14:57,680
 with them.

247
00:14:57,680 --> 00:15:01,720
 The Vadatta wasn't able to see them clearly.

248
00:15:01,720 --> 00:15:05,560
 Dasmati had no...

249
00:15:05,560 --> 00:15:10,850
 And then he says, "Why should you not let them consume you

250
00:15:10,850 --> 00:15:11,360
?"

251
00:15:11,360 --> 00:15:21,380
 And he says, "Because there will be a Vigata Parila, which

252
00:15:21,380 --> 00:15:25,120
 is a fever burning.

253
00:15:25,120 --> 00:15:29,760
 Those taints, distressful and feverish, that might arise in

254
00:15:29,760 --> 00:15:32,000
 one who has not overcome gain

255
00:15:32,000 --> 00:15:34,840
 and so on, do not occur in one who has overcome it."

256
00:15:34,840 --> 00:15:42,850
 So your mind doesn't become inflamed, distressed, stressed,

257
00:15:42,850 --> 00:15:46,440
 fatigued, overwhelmed.

258
00:15:46,440 --> 00:15:56,670
 The Asa is outpouring emotions and defilement, the stresses

259
00:15:56,670 --> 00:16:02,080
 that arise when you've overcome

260
00:16:02,080 --> 00:16:08,380
 them, when you've come to see them for what they are and

261
00:16:08,380 --> 00:16:12,440
 discard them as unproductive.

262
00:16:12,440 --> 00:16:18,240
 For that reason you should train yourself.

263
00:16:18,240 --> 00:16:20,240
 Dasmati ha bhikkari.

264
00:16:20,240 --> 00:16:25,160
 Aivang sikidabang, it should be trained us.

265
00:16:25,160 --> 00:16:27,280
 We will dwell having overcome.

266
00:16:27,280 --> 00:16:31,920
 Abhibu ya abhibu ya wihari sam.

267
00:16:31,920 --> 00:16:38,050
 We will dwell having overcome gain and loss and fame and

268
00:16:38,050 --> 00:16:41,440
 infamy and honor and dishonor

269
00:16:41,440 --> 00:16:50,480
 and evil desires and evil friends.

270
00:16:50,480 --> 00:17:01,480
 Papi chattang, papam, mikattang, evil friendship.

271
00:17:01,480 --> 00:17:03,120
 So good list, good list.

272
00:17:03,120 --> 00:17:07,150
 It's almost the eight lokiyadamma but the last two would be

273
00:17:07,150 --> 00:17:09,960
 happiness and suffering.

274
00:17:09,960 --> 00:17:17,010
 Dukkha and sukha, instead of papi chattah, papam, papamit

275
00:17:17,010 --> 00:17:18,280
attah.

276
00:17:18,280 --> 00:17:28,080
 Anyway, there's our dhamma for tonight.

277
00:17:28,080 --> 00:17:29,080
 Don't be like devadatta.

278
00:17:29,080 --> 00:17:34,880
 Don't let these things overwhelm you.

279
00:17:34,880 --> 00:17:38,840
 Any questions?

280
00:17:38,840 --> 00:18:04,480
 Can you stay for a second?

281
00:18:04,480 --> 00:18:19,240
 We got a couple of questions first I want to say.

282
00:18:19,240 --> 00:18:20,800
 Michael is living here now.

283
00:18:20,800 --> 00:18:27,640
 He's the steward here for a while.

284
00:18:27,640 --> 00:18:29,960
 Just finished his advanced course.

285
00:18:29,960 --> 00:18:31,520
 He's done both courses.

286
00:18:31,520 --> 00:18:34,970
 We'll have to do an interview with Michael to see what he

287
00:18:34,970 --> 00:18:36,680
 thinks of the practice.

288
00:18:36,680 --> 00:18:42,600
 See how it helped him, if it helped him.

289
00:18:42,600 --> 00:18:47,000
 Sunday we're going to miss a saga together.

290
00:18:47,000 --> 00:18:54,120
 And then I have to go to New York.

291
00:18:54,120 --> 00:18:55,640
 We're going to drive to New York.

292
00:18:55,640 --> 00:19:00,360
 Are you going to drive to New York?

293
00:19:00,360 --> 00:19:03,640
 Would you be able to drive to New York?

294
00:19:03,640 --> 00:19:05,960
 How is it about crossing the border back and forth?

295
00:19:05,960 --> 00:19:08,920
 There's no problem, is there?

296
00:19:08,920 --> 00:19:10,600
 What did they say when you came in?

297
00:19:10,600 --> 00:19:12,560
 Did they ask you?

298
00:19:12,560 --> 00:19:13,560
 I had a lot of questions.

299
00:19:13,560 --> 00:19:14,560
 They did make trouble for you?

300
00:19:14,560 --> 00:19:15,340
 I wouldn't say trouble, but they had to wait for about an

301
00:19:15,340 --> 00:19:15,560
 hour.

302
00:19:15,560 --> 00:19:16,560
 An hour?

303
00:19:16,560 --> 00:19:17,140
 I mean, you know, when I mentioned your name, and then I

304
00:19:17,140 --> 00:19:17,560
 was staying in a monastery, you

305
00:19:17,560 --> 00:19:28,520
 know, they had many questions about that.

306
00:19:28,520 --> 00:19:30,840
 Like suspicious or just curious?

307
00:19:30,840 --> 00:19:32,440
 A little above.

308
00:19:32,440 --> 00:19:35,680
 Oh, we should talk about it.

309
00:19:35,680 --> 00:19:38,960
 You probably should give people letters in the future.

310
00:19:38,960 --> 00:19:41,520
 Letters of recommendation, like letters of invitation.

311
00:19:41,520 --> 00:19:42,520
 How weird.

312
00:19:42,520 --> 00:19:45,640
 Yeah, they did ask for that.

313
00:19:45,640 --> 00:19:46,640
 Yeah.

314
00:19:46,640 --> 00:19:48,880
 Yeah, I think we've had this issue before.

315
00:19:48,880 --> 00:19:52,200
 I just sent a letter of invitation to someone in Ukraine

316
00:19:52,200 --> 00:19:54,160
 who wants to come and meditate.

317
00:19:54,160 --> 00:19:57,560
 So hopefully that works for him.

318
00:19:57,560 --> 00:19:59,680
 So there's this monk I've been talking about.

319
00:19:59,680 --> 00:20:02,870
 He wants to go visit Bikubodi, so he wants me to go with

320
00:20:02,870 --> 00:20:03,440
 him.

321
00:20:03,440 --> 00:20:04,440
 And he was going to drive her.

322
00:20:04,440 --> 00:20:06,440
 I said, "Well, I've got a driver.

323
00:20:06,440 --> 00:20:09,840
 We've got a Prius, right?

324
00:20:09,840 --> 00:20:12,800
 It's a good mileage.

325
00:20:12,800 --> 00:20:14,120
 So probably he'll pay for gas."

326
00:20:14,120 --> 00:20:26,360
 But yeah, so that's a week and a half away, the 17th.

327
00:20:26,360 --> 00:20:32,360
 One day we're going to Mississauga for Wesaka.

328
00:20:32,360 --> 00:20:34,320
 Okay.

329
00:20:34,320 --> 00:20:37,360
 So questions.

330
00:20:37,360 --> 00:20:41,520
 Larry says, "During sitting meditation, can more than one

331
00:20:41,520 --> 00:20:44,640
 hindrance be recognized concurrently?"

332
00:20:44,640 --> 00:20:51,400
 Yeah, you won't have them both at once.

333
00:20:51,400 --> 00:20:54,200
 You can't have a virgin end craving concurrently.

334
00:20:54,200 --> 00:20:57,340
 I mean, it feels like that because over time you say, "Wow,

335
00:20:57,340 --> 00:20:59,000
 I was both angry and greedy,"

336
00:20:59,000 --> 00:21:03,160
 but you can't have them both at the exact same moment.

337
00:21:03,160 --> 00:21:06,320
 So concurrently and then broad sense, sure.

338
00:21:06,320 --> 00:21:07,560
 But in the absolute sense, no.

339
00:21:07,560 --> 00:21:09,560
 In the absolute sense, there's one at a time.

340
00:21:09,560 --> 00:21:16,960
 So whichever one is clear in that moment, just focus on it.

341
00:21:16,960 --> 00:21:21,960
 It doesn't really matter.

342
00:21:21,960 --> 00:21:25,480
 I wouldn't worry about which is which, just whichever one

343
00:21:25,480 --> 00:21:27,600
 is clear in whatever moment,

344
00:21:27,600 --> 00:21:28,600
 focus on it.

345
00:21:28,600 --> 00:21:30,160
 You don't have to catch them all.

346
00:21:30,160 --> 00:21:34,220
 But again, you can't have all of them at once, except for

347
00:21:34,220 --> 00:21:36,520
 the fact that aversion to something

348
00:21:36,520 --> 00:21:40,280
 is kind of like desire for it not to be, right?

349
00:21:40,280 --> 00:21:43,070
 So you could argue that it's actually in that same moment,

350
00:21:43,070 --> 00:21:44,240
 it's the same thing.

351
00:21:44,240 --> 00:21:46,040
 You're saying the same thing.

352
00:21:46,040 --> 00:21:49,880
 You dislike something means you want it to be gone.

353
00:21:49,880 --> 00:21:54,360
 So it's not quite craving or it is some people call that we

354
00:21:54,360 --> 00:22:00,920
 Bhavatan desire for non-existence.

355
00:22:00,920 --> 00:22:03,440
 Sabe sankara dukkha.

356
00:22:03,440 --> 00:22:07,080
 Does that hold true only for unenlightened beings?

357
00:22:07,080 --> 00:22:08,080
 No.

358
00:22:08,080 --> 00:22:10,520
 No, it does not.

359
00:22:10,520 --> 00:22:11,520
 They are dukkha.

360
00:22:11,520 --> 00:22:19,750
 Dukkha is their characteristic, means they are unsatisfying

361
00:22:19,750 --> 00:22:19,840
.

362
00:22:19,840 --> 00:22:32,920
 It means they are not good.

363
00:22:32,920 --> 00:22:34,640
 It's the intrinsic characteristic.

364
00:22:34,640 --> 00:22:39,440
 It doesn't mean that they are painful or they, when they

365
00:22:39,440 --> 00:22:42,640
 arise, cause one mental suffering

366
00:22:42,640 --> 00:22:45,000
 or even physical suffering.

367
00:22:45,000 --> 00:22:46,640
 That's not what dukkha means here.

368
00:22:46,640 --> 00:22:56,160
 Dukkha means unsatisfying or unable to satisfy.

369
00:22:56,160 --> 00:22:58,440
 Unable to bring happiness, means not happiness.

370
00:22:58,440 --> 00:23:01,940
 Remember these three characteristics are in opposition of

371
00:23:01,940 --> 00:23:03,560
 what we think things are.

372
00:23:03,560 --> 00:23:08,070
 We think things are stable, we think things are satisfying

373
00:23:08,070 --> 00:23:10,800
 or able to satisfy us or productive

374
00:23:10,800 --> 00:23:16,830
 of true happiness and we think things are ours or me or

375
00:23:16,830 --> 00:23:17,880
 mine.

376
00:23:17,880 --> 00:23:24,760
 We can control them.

377
00:23:24,760 --> 00:23:27,800
 So this is just realizing that they are not that.

378
00:23:27,800 --> 00:23:31,300
 That they are not nitya, that they are not sukha, that they

379
00:23:31,300 --> 00:23:32,200
 are not ata.

380
00:23:32,200 --> 00:23:33,880
 That's what these three things mean.

381
00:23:33,880 --> 00:23:46,040
 They are actually anitjas dukkha anata.

382
00:23:46,040 --> 00:23:47,720
 Can you go back to using the red room?

383
00:23:47,720 --> 00:23:50,240
 Wow, there's someone who actually likes that red.

384
00:23:50,240 --> 00:23:54,080
 No, I can't go back to using the red room.

385
00:23:54,080 --> 00:24:00,760
 It's disliking, disliking.

386
00:24:00,760 --> 00:24:16,080
 Sadhu means good.

387
00:24:16,080 --> 00:24:16,700
 Sadhu means in Hinduism sadhu is a word also for a holy

388
00:24:16,700 --> 00:24:35,600
 person like a monk or a recluse.

389
00:24:35,600 --> 00:24:45,150
 In Pali, I don't know in Sanskrit, but in Pali it's just

390
00:24:45,150 --> 00:24:48,720
 used to mean good.

391
00:24:48,720 --> 00:24:52,960
 When something is good you say sadhu, that thing is sadhu.

392
00:24:52,960 --> 00:24:58,880
 In Pali you would say dang sadhu, it is good.

393
00:24:58,880 --> 00:25:00,880
 You wouldn't say it like that.

394
00:25:00,880 --> 00:25:05,200
 But then like sadhu, it was used in the sukha, right?

395
00:25:05,200 --> 00:25:09,840
 Sadhu bhikkhu, bhikkhu, upanang, labang, abhibuya, abhibuya

396
00:25:09,840 --> 00:25:11,000
, wihariya.

397
00:25:11,000 --> 00:25:23,120
 It is good, or it would be good if one were to dwell.

398
00:25:23,120 --> 00:25:25,240
 But isn't suffering only mental?

399
00:25:25,240 --> 00:25:28,320
 No, there's two kinds of suffering.

400
00:25:28,320 --> 00:25:31,500
 There's physical suffering and mental suffering, but that's

401
00:25:31,500 --> 00:25:33,000
 again not what this means.

402
00:25:33,000 --> 00:25:35,930
 I don't know, I think there's a lag, so I'm not sure if

403
00:25:35,930 --> 00:25:37,680
 that was after my explanation

404
00:25:37,680 --> 00:25:39,680
 or before it.

405
00:25:39,680 --> 00:25:41,840
 But again, that's not what dukkha means here.

406
00:25:41,840 --> 00:25:54,080
 Dukkha means not happiness, or not conducive to happiness,

407
00:25:54,080 --> 00:26:00,520
 not productive of happiness.

408
00:26:00,520 --> 00:26:01,520
 Where is Robin?

409
00:26:01,520 --> 00:26:03,520
 Robin's here.

410
00:26:03,520 --> 00:26:06,770
 I was doing video questions, I was forcing people, trying

411
00:26:06,770 --> 00:26:08,280
 to get people to come on the

412
00:26:08,280 --> 00:26:19,920
 hangout, so didn't have any need for someone to read.

413
00:26:19,920 --> 00:26:24,300
 I think there's a way at some time and then we just stopped

414
00:26:24,300 --> 00:26:24,560
.

415
00:26:24,560 --> 00:26:27,760
 Everything changes, this is what you have to realize.

416
00:26:27,760 --> 00:26:31,000
 Don't get attached to things as they are.

417
00:26:31,000 --> 00:26:37,000
 Try to change the format around, shake things up.

418
00:26:37,000 --> 00:26:42,440
 Anyway, that's all for tonight.

419
00:26:42,440 --> 00:26:45,560
 Thank you all for tuning in.

420
00:26:45,560 --> 00:26:50,930
 Tomorrow, oh another thing is tomorrow at the university,

421
00:26:50,930 --> 00:26:53,200
 it's called May at Mac.

422
00:26:53,200 --> 00:26:56,720
 May at Mac, it's the yearly McMaster open house.

423
00:26:56,720 --> 00:27:05,070
 The Peace Studies department has a booth and they've asked

424
00:27:05,070 --> 00:27:08,120
 some of us to go and talk to

425
00:27:08,120 --> 00:27:13,520
 new students about the Peace Studies department.

426
00:27:13,520 --> 00:27:17,270
 Take it as an opportunity to get people to sign up for the

427
00:27:17,270 --> 00:27:18,640
 Peace Club as well.

428
00:27:18,640 --> 00:27:21,810
 It would be interesting, I'll go as a Buddhist monk to go

429
00:27:21,810 --> 00:27:24,120
 and promote the Peace Studies department.

430
00:27:24,120 --> 00:27:27,720
 I think that would be good.

431
00:27:27,720 --> 00:27:33,100
 I do believe in it, I'm not sure how, obviously these kind

432
00:27:33,100 --> 00:27:36,040
 of studies is not my priority,

433
00:27:36,040 --> 00:27:39,120
 but I want to encourage them and support them.

434
00:27:39,120 --> 00:27:42,870
 Anyone who's talking about peace, studying peace is just

435
00:27:42,870 --> 00:27:44,320
 all good in my book.

436
00:27:44,320 --> 00:27:48,440
 I'm happy to promote it.

437
00:27:48,440 --> 00:27:51,720
 Anyway, have a good night everyone.

438
00:27:51,720 --> 00:28:06,180
 You can stay up here for questions if you want, you can

439
00:28:06,180 --> 00:28:09,920
 also leave after if you want.

440
00:28:09,920 --> 00:28:15,610
 Maybe we can do a, from time to time we can do a dual monk

441
00:28:15,610 --> 00:28:18,920
 radio if you want to come on.

442
00:28:18,920 --> 00:28:21,960
 You can sit here, we can have a mic in the center or

443
00:28:21,960 --> 00:28:22,920
 something.

444
00:28:22,920 --> 00:28:26,520
 You see the ones with four of us, that was a lot of fun.

445
00:28:26,520 --> 00:28:29,560
 We had a whole table of us in Sri Lanka for a while.

