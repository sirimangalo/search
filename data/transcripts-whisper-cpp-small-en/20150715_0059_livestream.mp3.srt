1
00:00:00,000 --> 00:00:08,220
 Okay, in broadcasting life, today from Sunderland, Ontario,

2
00:00:08,220 --> 00:00:13,000
 where I'm visiting with family.

3
00:00:13,000 --> 00:00:20,220
 I'll be here until Friday and then back to Stony Creek,

4
00:00:20,220 --> 00:00:21,880
 Ontario.

5
00:00:21,880 --> 00:00:30,000
 And then on Monday, the 20th, we're planning on taking a

6
00:00:30,000 --> 00:00:38,360
 road trip, maybe, up to Mainatulan Island, where I was born

7
00:00:38,360 --> 00:00:38,880
.

8
00:00:38,880 --> 00:00:47,550
 I spent a few days visiting. I just wanted to show the

9
00:00:47,550 --> 00:00:53,150
 monks where I live because they're always telling me about

10
00:00:53,150 --> 00:00:55,880
 their homeland in southern Vietnam,

11
00:00:55,880 --> 00:01:05,390
 what used to be a part of Cambodia. And I said, "Oh, I

12
00:01:05,390 --> 00:01:12,730
 should show you where I grew up." Because they talk about

13
00:01:12,730 --> 00:01:17,150
 how beautiful Ontario is, and this is nothing to see in my

14
00:01:17,150 --> 00:01:17,880
 home.

15
00:01:17,880 --> 00:01:23,530
 I wanted to show them something natural, a chance to get

16
00:01:23,530 --> 00:01:29,400
 away, go off into the forest, and do some meditation up on

17
00:01:29,400 --> 00:01:30,880
 the cliffs.

18
00:01:30,880 --> 00:01:37,590
 Let's see. Anyway, just details of life, and everyone knows

19
00:01:37,590 --> 00:01:40,880
 what's happening with me.

20
00:01:40,880 --> 00:01:46,890
 Today's reading, again, we're reading from the words of the

21
00:01:46,890 --> 00:01:51,880
 Buddha, "Buddhavachana" by Damika.

22
00:01:51,880 --> 00:01:59,440
 And today's reading is about livelihood, really. Sorry, we

23
00:01:59,440 --> 00:02:07,880
 missed that yesterday. I missed yesterday. I wasn't online.

24
00:02:07,880 --> 00:02:13,800
 I'll try not to miss, try to be here every day, but I can't

25
00:02:13,800 --> 00:02:15,880
 always promise.

26
00:02:15,880 --> 00:02:21,860
 Anyway, today's is about livelihood. Someone's accusing Sir

27
00:02:21,860 --> 00:02:32,880
 Ibuddhav, improper consumption of goods.

28
00:02:32,880 --> 00:02:38,050
 And he says that he basically refrains from what would be

29
00:02:38,050 --> 00:02:48,140
 considered wrong livelihood for a monk, and therefore he

30
00:02:48,140 --> 00:02:54,880
 eats properly.

31
00:02:54,880 --> 00:03:03,260
 It's obviously, this is a specific teaching for monks, but

32
00:03:03,260 --> 00:03:09,840
 the general sense is clearly applicable, and it can apply

33
00:03:09,840 --> 00:03:11,880
 this to anyone, to everyone.

34
00:03:11,880 --> 00:03:18,050
 Right, livelihood is something, well, livelihood in general

35
00:03:18,050 --> 00:03:23,880
, it's an unavoidable consequence of living in the world.

36
00:03:23,880 --> 00:03:26,880
 You have to stay alive.

37
00:03:26,880 --> 00:03:33,110
 In modern times, this means a lot more than simply survival

38
00:03:33,110 --> 00:03:39,680
, it means taking part in systems and structures,

39
00:03:39,680 --> 00:03:46,880
 organizations, getting a job,

40
00:03:46,880 --> 00:03:51,710
 going to school, learning skills, all this, engaging with

41
00:03:51,710 --> 00:03:57,590
 society, voting, reading the news, livelihood can be quite

42
00:03:57,590 --> 00:04:03,880
 complex, just to stay alive as a society.

43
00:04:03,880 --> 00:04:10,840
 And so it is even more important that we understand the

44
00:04:10,840 --> 00:04:17,190
 ethical ins and outs of livelihood. It's a part of our

45
00:04:17,190 --> 00:04:21,880
 lives, a big part of our lives.

46
00:04:21,880 --> 00:04:28,050
 So the Buddha gave instruction on the sorts of livelihood

47
00:04:28,050 --> 00:04:32,030
 that are improper, and we've talked about this a bit on the

48
00:04:32,030 --> 00:04:32,880
 internet.

49
00:04:32,880 --> 00:04:39,430
 For example, the Buddha said selling weapons is considered

50
00:04:39,430 --> 00:04:41,880
 wrong livelihood.

51
00:04:41,880 --> 00:04:50,510
 But then what about working in a factory that makes weapons

52
00:04:50,510 --> 00:04:57,410
? And if hunting is wrong, what about this wife who, this

53
00:04:57,410 --> 00:05:01,880
 woman who was married to a man who hunted,

54
00:05:01,880 --> 00:05:09,440
 and she cleaned all of his implements for him, cleaned his

55
00:05:09,440 --> 00:05:21,880
 snares and so on. Is that wrong livelihood?

56
00:05:21,880 --> 00:05:26,340
 Of course, ultimately when we talk about right or wrong, we

57
00:05:26,340 --> 00:05:30,280
 deal with intention. There is no gray area, it's not hard

58
00:05:30,280 --> 00:05:30,880
 to understand.

59
00:05:30,880 --> 00:05:35,060
 If your mind is full of greed, anger or delusion, that's

60
00:05:35,060 --> 00:05:38,880
 wrong. It doesn't have anything to do with the actual acts.

61
00:05:38,880 --> 00:05:48,670
 So this is why it's possible to work in a factory or clean

62
00:05:48,670 --> 00:05:57,250
 implements of murder basis without impure thoughts, without

63
00:05:57,250 --> 00:06:01,050
 unwholesomeness, because your mind is simply performing an

64
00:06:01,050 --> 00:06:01,880
 action.

65
00:06:01,880 --> 00:06:06,200
 But when you sell something, you undertake, you make a

66
00:06:06,200 --> 00:06:10,690
 decision what to sell, you make a decision to sell weapons,

67
00:06:10,690 --> 00:06:11,880
 for example.

68
00:06:11,880 --> 00:06:17,090
 So there is something there that's undeniably unwholesomen

69
00:06:17,090 --> 00:06:17,880
ess.

70
00:06:17,880 --> 00:06:27,320
 Working in a store, if you work in a store as a clerk

71
00:06:27,320 --> 00:06:34,130
 selling things, it's not the same. But if you open the

72
00:06:34,130 --> 00:06:34,910
 liquor store or make a decision in a restaurant to sell

73
00:06:34,910 --> 00:06:35,880
 alcohol, I think you can be considered culpable.

74
00:06:35,880 --> 00:06:42,830
 You've made a decision to sell things to people. If you

75
00:06:42,830 --> 00:06:50,940
 work in a store, someone else's store, you could argue, I

76
00:06:50,940 --> 00:06:52,870
 don't know how successful, but you could argue that you're

77
00:06:52,870 --> 00:06:55,880
 simply responding to orders.

78
00:06:55,880 --> 00:07:00,500
 Someone says, I'd like to buy this, and if you just bring

79
00:07:00,500 --> 00:07:05,160
 them up, take their money, give their money, you're not

80
00:07:05,160 --> 00:07:09,880
 actually handling or getting involved with the sale.

81
00:07:09,880 --> 00:07:16,270
 You're just facilitating. So we could say facilitating is

82
00:07:16,270 --> 00:07:17,880
 also wrong.

83
00:07:17,880 --> 00:07:21,860
 But I think it's possible for the intention to be pure in

84
00:07:21,860 --> 00:07:26,160
 that case, for the person who's manufacturing weapons,

85
00:07:26,160 --> 00:07:28,880
 simply putting stuff together.

86
00:07:28,880 --> 00:07:33,270
 In fact, I think you could argue that selling weapons isn't

87
00:07:33,270 --> 00:07:38,910
 necessarily unwholesomeness, but I think it's the intention

88
00:07:38,910 --> 00:07:39,880
.

89
00:07:39,880 --> 00:07:45,730
 What I mean is a person who owns a weapons factory, builds

90
00:07:45,730 --> 00:07:51,640
 a weapons factory, or whatever, it's not unwholesomeness

91
00:07:51,640 --> 00:07:52,880
 the whole time.

92
00:07:52,880 --> 00:08:01,850
 Just owning it isn't accumulating karma necessarily. I mean

93
00:08:01,850 --> 00:08:09,340
, it doesn't seem like it, but the intentions and the

94
00:08:09,340 --> 00:08:12,880
 decision to continue, anyway,

95
00:08:12,880 --> 00:08:18,820
 the most important point is the intention. And so then we

96
00:08:18,820 --> 00:08:23,250
 go to the next level of actions that lead to wrong

97
00:08:23,250 --> 00:08:24,880
 livelihood.

98
00:08:24,880 --> 00:08:28,640
 The description of wrong livelihood is wrong livelihood

99
00:08:28,640 --> 00:08:31,880
 that involves wrong action and wrong speech.

100
00:08:31,880 --> 00:08:37,050
 Wrong action and wrong speech are speech that is still

101
00:08:37,050 --> 00:08:41,260
 based on the mind, and still the unwholesomeness of the

102
00:08:41,260 --> 00:08:41,880
 mind.

103
00:08:41,880 --> 00:08:47,650
 So when you speak harshly or you speak to create the

104
00:08:47,650 --> 00:08:52,880
 division between people, to divide people,

105
00:08:52,880 --> 00:09:01,300
 even use useless speech, the point is that there's a mind

106
00:09:01,300 --> 00:09:07,880
 behind it that is cultivating bad habits.

107
00:09:07,880 --> 00:09:12,880
 It's not the actual speech that is wrong.

108
00:09:12,880 --> 00:09:17,630
 Sometimes people say certain things to get the attention of

109
00:09:17,630 --> 00:09:18,880
 other people.

110
00:09:18,880 --> 00:09:22,740
 Parents, when they scold their children, sometimes it's

111
00:09:22,740 --> 00:09:25,880
 possible to do it without unwholesomeness.

112
00:09:25,880 --> 00:09:29,760
 Teachers, when they teach things, they can do it with a

113
00:09:29,760 --> 00:09:30,880
 good heart.

114
00:09:31,880 --> 00:09:42,880
 So still it's our mind that we have to purify that.

115
00:09:42,880 --> 00:09:48,210
 In the Noble Eightfold Path, we're talking about this kind

116
00:09:48,210 --> 00:09:49,880
 of livelihood.

117
00:09:49,880 --> 00:23:13,700
 Right livelihood is that which is free from making your

118
00:23:13,700 --> 00:09:59,880
 livelihood based on killing, stealing, lying, cheating.

119
00:09:59,880 --> 00:10:11,580
 If you do any of these things to make a living that's wrong

120
00:10:11,580 --> 00:10:14,880
 livelihood.

121
00:10:14,880 --> 00:10:18,110
 But here in the sutta we have something a little bit more

122
00:10:18,110 --> 00:10:18,880
 curious.

123
00:10:18,880 --> 00:10:29,990
 It's livelihood based on desire, based on greed, or based

124
00:10:29,990 --> 00:10:35,880
 on seeking, seeking out wealth or gain.

125
00:10:35,880 --> 00:10:37,880
 That's what's really being discussed here.

126
00:10:37,880 --> 00:10:43,660
 It's all these ways that religious people have to make a

127
00:10:43,660 --> 00:10:44,880
 living.

128
00:10:44,880 --> 00:10:48,560
 I want to live as a religious person, so I'm going to run

129
00:10:48,560 --> 00:10:51,880
 errands for people in order to make my livelihood.

130
00:10:51,880 --> 00:10:58,750
 Or I'm going to tell fortunes, or all these things that's

131
00:10:58,750 --> 00:11:01,880
 very good dimensions.

132
00:11:01,880 --> 00:11:06,690
 Right livelihood in its purest sense for a monk is not

133
00:11:06,690 --> 00:11:07,880
 livelihood at all.

134
00:11:07,880 --> 00:11:11,880
 A sense of not seeking out livelihood.

135
00:11:11,880 --> 00:11:16,890
 We have the bare ability to wander through the village in

136
00:11:16,890 --> 00:11:18,880
 the morning.

137
00:11:18,880 --> 00:11:22,910
 And the idea is to wander through the village looking to

138
00:11:22,910 --> 00:11:25,880
 see if anyone is already offering food.

139
00:11:25,880 --> 00:11:29,880
 Look to see if people are offering food.

140
00:11:29,880 --> 00:11:32,880
 Because they were a Indian.

141
00:11:32,880 --> 00:11:37,880
 It wasn't begging.

142
00:11:37,880 --> 00:11:40,880
 It wasn't something the Buddha created.

143
00:11:40,880 --> 00:11:45,310
 It was just a convenient aspect of society that they

144
00:11:45,310 --> 00:11:47,880
 supported religious people.

145
00:11:47,880 --> 00:11:57,880
 And that's really what proper religious life in heaven is.

146
00:11:57,880 --> 00:12:02,880
 It's living off of the support of others.

147
00:12:02,880 --> 00:12:07,880
 And support means people who actually support what you do.

148
00:12:07,880 --> 00:12:13,880
 Not trying to find a way to gain livelihood.

149
00:12:13,880 --> 00:12:18,500
 And living a life and behaving in such a way that people

150
00:12:18,500 --> 00:12:19,880
 support you.

151
00:12:19,880 --> 00:12:21,880
 This is for monks, you see.

152
00:12:21,880 --> 00:12:26,330
 There's something, as I said, that this can be easily

153
00:12:26,330 --> 00:12:27,880
 extrapolated.

154
00:12:27,880 --> 00:12:32,880
 Take a person working a nine to five job in an office.

155
00:12:32,880 --> 00:12:34,880
 They have options open to them.

156
00:12:34,880 --> 00:12:41,460
 Some people would take it as an opportunity to arise up in

157
00:12:41,460 --> 00:12:42,880
 ranks.

158
00:12:42,880 --> 00:12:47,880
 And they might do unwholesome things to get there.

159
00:12:47,880 --> 00:12:52,880
 Say things manipulate others.

160
00:12:52,880 --> 00:12:55,880
 Scheme and plot.

161
00:12:55,880 --> 00:13:02,880
 Lie and cheat to get no destiny, to be favored.

162
00:13:02,880 --> 00:13:12,880
 For what? To gain more great alive.

163
00:13:12,880 --> 00:13:17,880
 And the point here is to be content.

164
00:13:17,880 --> 00:13:19,880
 For some people that's how it is.

165
00:13:19,880 --> 00:13:25,880
 They work and they do it to gain money.

166
00:13:25,880 --> 00:13:33,050
 They don't ever think to gain more money and to become more

167
00:13:33,050 --> 00:13:36,880
 powerful.

168
00:13:36,880 --> 00:13:42,880
 Because livelihood is such an intrinsic part of our lives,

169
00:13:42,880 --> 00:13:44,880
 it necessarily colors our practice.

170
00:13:44,880 --> 00:13:48,930
 For a monk, if they're all constantly obsessing over

171
00:13:48,930 --> 00:13:49,880
 livelihood,

172
00:13:49,880 --> 00:13:56,340
 or how to get nicer food, better food, better roads, better

173
00:13:56,340 --> 00:13:59,880
 homes, those kind of things.

174
00:13:59,880 --> 00:14:01,880
 And their minds will never be at peace.

175
00:14:01,880 --> 00:14:04,880
 And they'll be able to find clarity of mind.

176
00:14:04,880 --> 00:14:11,820
 And for the lay people, it's very much a fame if you're

177
00:14:11,820 --> 00:14:14,880
 obsessed with your job.

178
00:14:14,880 --> 00:14:24,210
 And trying to succeed, worried about success in a worldly

179
00:14:24,210 --> 00:14:24,880
 sense.

180
00:14:24,880 --> 00:14:33,880
 You'll never succeed spiritually.

181
00:14:33,880 --> 00:14:36,880
 I've always come to my teacher asking for advice.

182
00:14:36,880 --> 00:14:40,880
 Should they get a new job or so on?

183
00:14:40,880 --> 00:14:43,800
 And invariably he would say, "No, no, I should stay with

184
00:14:43,800 --> 00:14:44,880
 what you've got."

185
00:14:44,880 --> 00:14:46,880
 They'll ask him what they should sell.

186
00:14:46,880 --> 00:14:50,040
 I remember once someone came and asked him, "What do you

187
00:14:50,040 --> 00:14:51,880
 think I should sell?"

188
00:14:51,880 --> 00:14:55,880
 "I don't want to open the store and sell something."

189
00:14:55,880 --> 00:14:58,880
 Of course John didn't want the answer.

190
00:14:58,880 --> 00:15:03,880
 He said, "We could sell salt."

191
00:15:03,880 --> 00:15:07,880
 "Sell salt." It was like reluctant advice.

192
00:15:07,880 --> 00:15:12,880
 And he had a good point because everyone needs salt.

193
00:15:12,880 --> 00:15:17,880
 But it's such a simple thing to say.

194
00:15:17,880 --> 00:15:20,880
 Stop looking for more.

195
00:15:20,880 --> 00:15:25,880
 Do something good and just get a job and work.

196
00:15:25,880 --> 00:15:28,880
 Sell salt.

197
00:15:28,880 --> 00:15:32,880
 Like this anagami who sold pots.

198
00:15:32,880 --> 00:15:34,880
 But he didn't really sell them.

199
00:15:34,880 --> 00:15:37,880
 He just put them by the side of the road and people came by

200
00:15:37,880 --> 00:15:41,880
 and offered whatever they thought the pot was worth.

201
00:15:41,880 --> 00:15:44,880
 He never put price tags on them.

202
00:15:44,880 --> 00:15:49,150
 When they asked how much is this, he would say, "Just leave

203
00:15:49,150 --> 00:15:50,880
 whatever you want."

204
00:15:50,880 --> 00:16:00,880
 That's ideal. That's beautiful.

205
00:16:00,880 --> 00:16:03,880
 It's such peace of mind if you can.

206
00:16:03,880 --> 00:16:07,480
 Of course then people will argue that not having enough

207
00:16:07,480 --> 00:16:10,880
 food to eat is hardly conducive to peace of mind.

208
00:16:10,880 --> 00:16:16,880
 And it's true. You do have to work.

209
00:16:16,880 --> 00:16:21,880
 And you should work. Livelihood is important.

210
00:16:21,880 --> 00:16:27,880
 But it shouldn't consume you. You should work hard at it.

211
00:16:27,880 --> 00:16:31,980
 So that you are working a sufficient amount, not to be

212
00:16:31,980 --> 00:16:32,880
 blamed.

213
00:16:32,880 --> 00:16:36,880
 Livelihood is a duty.

214
00:16:36,880 --> 00:16:40,880
 Monks have a duty to teach.

215
00:16:40,880 --> 00:16:44,400
 You might say it's not for livelihood purposes, but I think

216
00:16:44,400 --> 00:16:45,880
 partially it is.

217
00:16:45,880 --> 00:16:51,880
 A monk has a duty to teach.

218
00:16:51,880 --> 00:16:56,880
 Just like a man selling pots.

219
00:16:56,880 --> 00:16:59,880
 They don't teach to make money.

220
00:16:59,880 --> 00:17:01,880
 They teach because that's our duty.

221
00:17:01,880 --> 00:17:04,880
 And if people want us to continue to teach, well, no.

222
00:17:04,880 --> 00:17:06,880
 Make sure we get fed.

223
00:17:06,880 --> 00:17:10,880
 It's really what it is about. Support.

224
00:17:10,880 --> 00:17:14,310
 You work in a company. They value you. So they keep paying

225
00:17:14,310 --> 00:17:14,880
 you.

226
00:17:14,880 --> 00:17:23,880
 All you have to do is be valuable.

227
00:17:23,880 --> 00:17:26,880
 So something to think about. Livelihood.

228
00:17:26,880 --> 00:17:31,040
 Is our livelihood contributing to our spiritual practice or

229
00:17:31,040 --> 00:17:32,880
 is it detracting from us?

230
00:17:32,880 --> 00:17:36,230
 Hard work in a bad work environment doesn't necessarily

231
00:17:36,230 --> 00:17:38,880
 have to detract from spiritual practice.

232
00:17:38,880 --> 00:17:41,880
 Again, it's all about what your mind is.

233
00:17:41,880 --> 00:17:44,880
 If you work, it can be like a duty.

234
00:17:44,880 --> 00:17:50,980
 I remember after I began practicing meditation, I was still

235
00:17:50,980 --> 00:17:52,880
 a monk.

236
00:17:52,880 --> 00:17:56,880
 I went tree planting in Northern Ontario.

237
00:17:56,880 --> 00:17:59,880
 Tree planting is...

238
00:17:59,880 --> 00:18:02,880
 Wow, what a job.

239
00:18:02,880 --> 00:18:07,880
 Hard work. The hardest work you've ever seen.

240
00:18:07,880 --> 00:18:12,880
 You work... How many hours a day have you been working?

241
00:18:12,880 --> 00:18:16,880
 The morning to... something like eight hours.

242
00:18:16,880 --> 00:18:19,230
 For some reason I wanted to say ten, but I don't think that

243
00:18:19,230 --> 00:18:19,880
's for sure.

244
00:18:19,880 --> 00:18:29,880
 Anyway, you're working all day planting thousands of trees.

245
00:18:29,880 --> 00:18:32,880
 And the point is that some of the worst people...

246
00:18:32,880 --> 00:18:35,240
 From my experience, it was some of the worst people, but I

247
00:18:35,240 --> 00:18:36,880
 think I got a really bad company.

248
00:18:36,880 --> 00:18:39,880
 And they were just the lowest of the low.

249
00:18:39,880 --> 00:18:44,880
 The people who were supervising us were really awful.

250
00:18:44,880 --> 00:18:51,970
 And the... sort of the quality of people, I mean, it was a

251
00:18:51,970 --> 00:18:53,880
 lot of...

252
00:18:53,880 --> 00:18:58,880
 Obviously, I guess outcast.

253
00:18:58,880 --> 00:19:01,880
 I remember the environment was not also all that wholesome.

254
00:19:01,880 --> 00:19:03,880
 I mean, I shouldn't judge.

255
00:19:03,880 --> 00:19:08,160
 Obviously, some of the people were okay and everyone was

256
00:19:08,160 --> 00:19:08,880
 people.

257
00:19:08,880 --> 00:19:12,880
 They had hopes and dreams and wishes and so on,

258
00:19:12,880 --> 00:19:16,490
 but I just mean to say that it was a rather coarse

259
00:19:16,490 --> 00:19:17,880
 environment.

260
00:19:17,880 --> 00:19:21,880
 And I remember just...

261
00:19:21,880 --> 00:19:23,880
 trying to be as mindful as I could.

262
00:19:23,880 --> 00:19:25,880
 And the worst part, of course, was the mosquitoes.

263
00:19:25,880 --> 00:19:28,230
 There were black clouds of mosquitoes, so I would do

264
00:19:28,230 --> 00:19:28,880
 walking meditation,

265
00:19:28,880 --> 00:19:31,880
 wearing a full-body mosquito net.

266
00:19:31,880 --> 00:19:36,880
 Because there were honestly black clouds of mosquitoes.

267
00:19:36,880 --> 00:19:39,880
 You never see them.

268
00:19:39,880 --> 00:19:43,880
 So I tried to plant trees, mindful.

269
00:19:43,880 --> 00:19:48,880
 I think it's possible to do all these things.

270
00:19:48,880 --> 00:19:52,880
 To be surrounded by people who are not very mindful.

271
00:19:52,880 --> 00:19:55,880
 They still be mindful.

272
00:19:55,880 --> 00:20:00,500
 And Buddha said, "We're happy." Among those who are unhappy

273
00:20:00,500 --> 00:20:00,880
.

274
00:20:00,880 --> 00:20:04,010
 Of course, it's preferable to be with good people who

275
00:20:04,010 --> 00:20:04,880
 support you in practice.

276
00:20:04,880 --> 00:20:10,880
 That's what goes without saying.

277
00:20:10,880 --> 00:20:13,880
 We shouldn't be discouraged by our environment.

278
00:20:13,880 --> 00:20:16,880
 In fact, that's the key, is to not be discouraged.

279
00:20:16,880 --> 00:20:18,880
 To just see it as...

280
00:20:18,880 --> 00:20:20,880
 See it objectively as it is.

281
00:20:20,880 --> 00:20:22,880
 Seeing, hearing, smelling, tasting.

282
00:20:22,880 --> 00:20:25,500
 And thinking no matter what it is, even if you don't have

283
00:20:25,500 --> 00:20:25,880
 livelihood

284
00:20:25,880 --> 00:20:29,710
 and you're living on the street, dying of thirst and hunger

285
00:20:29,710 --> 00:20:29,880
,

286
00:20:29,880 --> 00:20:34,540
 it's still just seeing, hearing, smelling, tasting, feeling

287
00:20:34,540 --> 00:20:34,880
.

288
00:20:34,880 --> 00:20:37,880
 And that's the key.

289
00:20:37,880 --> 00:20:41,880
 That's how the Aahants, that's their look on them.

290
00:20:41,880 --> 00:20:44,880
 That's how we monks train ourselves.

291
00:20:44,880 --> 00:20:48,880
 "If I don't get food today, I won't eat."

292
00:20:48,880 --> 00:20:50,880
 There's this old monk in Chiang Mai.

293
00:20:50,880 --> 00:20:56,880
 He was passing on some words of wisdom.

294
00:20:56,880 --> 00:20:58,880
 That are quite common in that.

295
00:20:58,880 --> 00:21:01,280
 I remember hearing this though from the very head of the Ch

296
00:21:01,280 --> 00:21:01,880
iang Mai province.

297
00:21:01,880 --> 00:21:04,880
 I went to get something signed.

298
00:21:04,880 --> 00:21:08,220
 They started talking because they didn't get many rich

299
00:21:08,220 --> 00:21:08,880
 visitors.

300
00:21:08,880 --> 00:21:11,880
 He was talking about eating house food.

301
00:21:11,880 --> 00:21:14,880
 He said, "Ya, you know."

302
00:21:14,880 --> 00:21:18,880
 He said, "Nee kachan, mai nee kachoy."

303
00:21:18,880 --> 00:21:20,880
 That sounds so much better in Thai.

304
00:21:20,880 --> 00:21:22,880
 "Nee kachan, mai nee kachoy."

305
00:21:22,880 --> 00:21:24,880
 "Chan gapchoy." It's a play on words.

306
00:21:24,880 --> 00:21:28,880
 If you have food, you chan. If you don't have food, choy.

307
00:21:28,880 --> 00:21:32,880
 Chan choy is a common in Thai.

308
00:21:32,880 --> 00:21:34,880
 Words that sound similar.

309
00:21:34,880 --> 00:21:36,880
 Chan means eat. If you have food, you eat.

310
00:21:36,880 --> 00:21:38,880
 If you don't eat, you choy.

311
00:21:38,880 --> 00:21:42,880
 It means sit still or be neutral.

312
00:21:42,880 --> 00:21:47,880
 Choy means neutral.

313
00:21:47,880 --> 00:21:50,880
 Stay calm.

314
00:21:50,880 --> 00:21:56,880
 That's true livelihood.

315
00:21:56,880 --> 00:21:59,880
 Livelihood is not livelihood.

316
00:21:59,880 --> 00:22:02,880
 I think it's possible for lay people as well.

317
00:22:02,880 --> 00:22:04,880
 You just have to see it all as a duty.

318
00:22:04,880 --> 00:22:08,880
 They want me to work. Okay, I'll work.

319
00:22:08,880 --> 00:22:10,880
 They want to fire me. Okay.

320
00:22:10,880 --> 00:22:12,880
 I'll go without food today.

321
00:22:12,880 --> 00:22:15,880
 I'll go without money today.

322
00:22:15,880 --> 00:22:17,880
 Maybe not to that extent.

323
00:22:17,880 --> 00:22:21,880
 But seeing it as a duty, we can all get to that level of

324
00:22:21,880 --> 00:22:26,710
 not letting our work overwhelm us, not becoming obsessed

325
00:22:26,710 --> 00:22:27,880
 with work.

326
00:22:27,880 --> 00:22:35,880
 Just do it as a duty. Do our thing. Go home.

327
00:22:35,880 --> 00:22:38,880
 Your spiritual practice can take place anywhere.

328
00:22:38,880 --> 00:22:42,880
 Spiritual practice is not what you do, it's how you do it.

329
00:22:42,880 --> 00:22:48,880
 It's how your mind is when you do it.

330
00:22:48,880 --> 00:22:53,880
 So, here's wishing you all and all of us

331
00:22:53,880 --> 00:22:59,470
 find clarity of mind, the ability to do things with a clear

332
00:22:59,470 --> 00:22:59,880
 mind.

333
00:22:59,880 --> 00:23:03,880
 Live our lives with a clear mind.

334
00:23:03,880 --> 00:23:10,880
 And find true clarity of mind.

335
00:23:10,880 --> 00:23:13,880
 That's all. Thank you for tuning in. Have a good night.

