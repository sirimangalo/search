1
00:00:00,000 --> 00:00:22,840
 Now I'll give a short talk explaining what today means and

2
00:00:22,840 --> 00:00:24,560
 a little bit about the purpose

3
00:00:24,560 --> 00:00:41,380
 of this sort of ceremony. So our understanding is that 2600

4
00:00:41,380 --> 00:00:46,520
 years ago, according to Sri Lankan

5
00:00:46,520 --> 00:00:53,600
 Reckoning on this very day, 2600 years ago exactly, the

6
00:00:53,600 --> 00:01:03,120
 Buddha first gave his first teaching.

7
00:01:03,120 --> 00:01:05,510
 And for those of you who are not familiar with the story of

8
00:01:05,510 --> 00:01:06,720
 the Buddha, I'll just give

9
00:01:06,720 --> 00:01:14,870
 a little bit of background here. So the man who was to be

10
00:01:14,870 --> 00:01:17,720
 the Buddha, we understand, many

11
00:01:17,720 --> 00:01:22,800
 people know this much about the story, he was a prince. And

12
00:01:22,800 --> 00:01:26,320
 at the age of 29 he got

13
00:01:26,320 --> 00:01:37,050
 fed up with the meaninglessness of luxury and the life as a

14
00:01:37,050 --> 00:01:42,280
 prince in a royal household.

15
00:01:42,280 --> 00:01:47,860
 And so he decided to leave home in order to seek out some

16
00:01:47,860 --> 00:01:50,560
 answers to these questions.

17
00:01:50,560 --> 00:01:56,140
 Why we have to be born, why are we born, why do we get old,

18
00:01:56,140 --> 00:01:58,760
 why do we get sick, why do

19
00:01:58,760 --> 00:02:05,670
 we die, what's the point of it all, and how to overcome

20
00:02:05,670 --> 00:02:09,040
 this, what to do, what is it that

21
00:02:09,040 --> 00:02:17,380
 we have to do, what is the way to find freedom from

22
00:02:17,380 --> 00:02:23,480
 suffering. And so he spent six years

23
00:02:23,480 --> 00:02:30,470
 trying in vain to find an answer, a way out of suffering,

24
00:02:30,470 --> 00:02:33,800
 to find a real answer to the

25
00:02:33,800 --> 00:02:38,160
 problems in life. For his whole prince life it was the

26
00:02:38,160 --> 00:02:40,920
 understanding that the way out

27
00:02:40,920 --> 00:02:46,480
 of suffering is to find more and more pleasure to engage in

28
00:02:46,480 --> 00:02:49,600
 pleasant experiences and find

29
00:02:49,600 --> 00:02:53,890
 ways to avoid unpleasant experiences. And after 29 years of

30
00:02:53,890 --> 00:02:56,160
 living this way, he realized

31
00:02:56,160 --> 00:03:01,790
 that this wasn't sufficient. That as I've talked about

32
00:03:01,790 --> 00:03:05,920
 before, you can only escape unpleasant

33
00:03:05,920 --> 00:03:13,460
 situations for so long. Once you realize that there's no

34
00:03:13,460 --> 00:03:17,360
 way to escape old age, sickness

35
00:03:17,360 --> 00:03:26,810
 and death, then the answer that is seeking out sensual

36
00:03:26,810 --> 00:03:32,240
 pleasure is not sufficient. It's

37
00:03:32,240 --> 00:03:37,280
 like medicine that isn't strong enough for the sickness.

38
00:03:37,280 --> 00:03:39,920
 There's no amount of pleasure

39
00:03:39,920 --> 00:03:43,090
 which can take away the reality of old age, sickness and

40
00:03:43,090 --> 00:03:45,620
 death. And in fact, in some ways

41
00:03:45,620 --> 00:03:51,330
 it makes these things worse because it creates partiality

42
00:03:51,330 --> 00:03:54,440
 and addiction and aversion. And

43
00:03:54,440 --> 00:03:57,920
 so he realized that there was no amount of money or luxury,

44
00:03:57,920 --> 00:03:59,880
 no amount of power or influence

45
00:03:59,880 --> 00:04:03,340
 that could overcome these things, that could stop these

46
00:04:03,340 --> 00:04:05,360
 things. You can't buy your way

47
00:04:05,360 --> 00:04:10,600
 out of death and you can't survive. None of the luxury or

48
00:04:10,600 --> 00:04:12,720
 power or influence that you

49
00:04:12,720 --> 00:04:17,620
 have in this life survives with you after your death. You

50
00:04:17,620 --> 00:04:26,280
 have to give it all up, 100%.

51
00:04:26,280 --> 00:04:32,390
 So he spent six years trying to find a better answer. And

52
00:04:32,390 --> 00:04:36,200
 it was fruitless because he didn't

53
00:04:36,200 --> 00:04:42,550
 have a clear idea of where to look. The story goes, or the

54
00:04:42,550 --> 00:04:45,960
 commentary to the story is that

55
00:04:45,960 --> 00:04:51,070
 he had in a past life, because it took many, many lifetimes

56
00:04:51,070 --> 00:04:53,440
 for him to develop his mind

57
00:04:53,440 --> 00:04:57,810
 to the state where he was able to even come to these real

58
00:04:57,810 --> 00:05:00,560
izations and leave home without

59
00:05:00,560 --> 00:05:04,300
 a teacher, without anyone's guidance, to do it on his own,

60
00:05:04,300 --> 00:05:08,480
 especially from such a wonderful,

61
00:05:08,480 --> 00:05:12,920
 pleasurable lifestyle. It was something very difficult. But

62
00:05:12,920 --> 00:05:14,360
 in one of his past lifetimes,

63
00:05:14,360 --> 00:05:22,770
 some long, long time ago, he saw some, or he saw an

64
00:05:22,770 --> 00:05:27,800
 enlightened being, someone who had

65
00:05:27,800 --> 00:05:33,350
 done the same thing and come to realize the truth and come

66
00:05:33,350 --> 00:05:36,320
 to find a way out of suffering.

67
00:05:36,320 --> 00:05:39,700
 And he was an ignorant person at the time. And so he said

68
00:05:39,700 --> 00:05:41,740
 about that person, he said,

69
00:05:41,740 --> 00:05:46,090
 "Oh, they were torturing themselves." This person was tort

70
00:05:46,090 --> 00:05:48,340
uring himself. So he said something

71
00:05:48,340 --> 00:05:54,280
 nasty to this enlightened being. No useless, no good, good

72
00:05:54,280 --> 00:05:57,320
 for nothing, doing something

73
00:05:57,320 --> 00:06:01,400
 that's torturing yourself. And they say that's the reason,

74
00:06:01,400 --> 00:06:02,840
 because he had set himself on this

75
00:06:02,840 --> 00:06:07,720
 idea that a person who leaves home is torturing himself.

76
00:06:07,720 --> 00:06:09,400
 When it finally came time for him

77
00:06:09,400 --> 00:06:13,770
 to leave home, he had this wrong idea that torturing

78
00:06:13,770 --> 00:06:16,400
 himself was the right way out of

79
00:06:16,400 --> 00:06:19,810
 suffering. And in fact, this was a common belief at the

80
00:06:19,810 --> 00:06:21,600
 time of the Buddha. This was

81
00:06:21,600 --> 00:06:27,250
 the belief of people in India at the time that the way out

82
00:06:27,250 --> 00:06:31,320
 of suffering was to burn up your

83
00:06:31,320 --> 00:06:38,230
 attachments. So if you're attached to things that are

84
00:06:38,230 --> 00:06:44,000
 pleasant, then you should teach yourself

85
00:06:44,000 --> 00:06:48,040
 to be content with unpleasantness by creating unpleasant

86
00:06:48,040 --> 00:06:50,520
 situations, making it difficult

87
00:06:50,520 --> 00:06:55,680
 for yourself, creating suffering. Because this would offset

88
00:06:55,680 --> 00:06:58,280
 the pleasure that you have, the

89
00:06:58,280 --> 00:07:07,440
 attachment that you have to pleasure. And this is actually

90
00:07:07,440 --> 00:07:08,480
 quite reasonable, and it's

91
00:07:08,480 --> 00:07:11,780
 actually something that we do often. Even if we haven't

92
00:07:11,780 --> 00:07:13,440
 left the home life, we feel

93
00:07:13,440 --> 00:07:19,460
 guilty about our pleasures. So we torture ourselves. People

94
00:07:19,460 --> 00:07:22,440
 who work very hard will often have

95
00:07:22,440 --> 00:07:25,570
 this idea that they want this and want that, and they feel

96
00:07:25,570 --> 00:07:27,320
 guilty about it. And so they

97
00:07:27,320 --> 00:07:32,120
 think the right thing to do is to work very, very hard and

98
00:07:32,120 --> 00:07:35,600
 make trouble for yourself, really.

99
00:07:35,600 --> 00:07:39,800
 But they'll say how people who just indulge in pleasure are

100
00:07:39,800 --> 00:07:41,840
 lazy and so on, and they'll

101
00:07:41,840 --> 00:07:44,950
 develop this idea that if you want to be a good person, you

102
00:07:44,950 --> 00:07:46,400
 have to work. You need a

103
00:07:46,400 --> 00:07:50,340
 good work ethic, which is really pretty silly. It's

104
00:07:50,340 --> 00:07:53,720
 something that is driving our world.

105
00:07:53,720 --> 00:08:00,030
 There are two driving forces in the world. One is our

106
00:08:00,030 --> 00:08:03,960
 incessant quest for pleasure, and

107
00:08:03,960 --> 00:08:09,760
 the other is this idea that somehow we have to work,

108
00:08:09,760 --> 00:08:14,320
 somehow we have to develop and advance.

109
00:08:14,320 --> 00:08:17,790
 So they really go hand in hand. The idea that the best

110
00:08:17,790 --> 00:08:19,960
 thing we can do is indulge, and the

111
00:08:19,960 --> 00:08:25,600
 idea that the best thing we can do is develop or create.

112
00:08:25,600 --> 00:08:27,520
 The best thing we can do is work.

113
00:08:27,520 --> 00:08:30,400
 So we engage in all sorts of silly works to create things,

114
00:08:30,400 --> 00:08:32,080
 and thinking that somehow this

115
00:08:32,080 --> 00:08:37,460
 is a morally right thing. There was a famous philosopher in

116
00:08:37,460 --> 00:08:39,320
 America, Henry David Thoreau,

117
00:08:39,320 --> 00:08:46,960
 who was very critical of this idea, these people who work.

118
00:08:46,960 --> 00:08:49,200
 He told a story of something

119
00:08:49,200 --> 00:08:54,370
 about this man who had an ox pull a stone across the field

120
00:08:54,370 --> 00:08:56,840
 and then later on pull the

121
00:08:56,840 --> 00:09:00,650
 same stone back to the other side of the field. This is

122
00:09:00,650 --> 00:09:03,160
 basically what we do, is we engage

123
00:09:03,160 --> 00:09:08,550
 in useless works, and thinking that somehow it builds

124
00:09:08,550 --> 00:09:12,160
 character to torture yourself. The

125
00:09:12,160 --> 00:09:15,180
 Buddha did this intensively. This was the idea at the time

126
00:09:15,180 --> 00:09:16,760
 that somehow it had spiritual

127
00:09:16,760 --> 00:09:19,980
 benefit. It actually may have some limited spiritual

128
00:09:19,980 --> 00:09:23,200
 benefit. The Buddha said it was

129
00:09:23,200 --> 00:09:27,430
 useless, or not connected with what is right. But it

130
00:09:27,430 --> 00:09:30,200
 actually may have the preliminary benefit

131
00:09:30,200 --> 00:09:38,490
 of opening your mind up to more experiences. So it could

132
00:09:38,490 --> 00:09:42,680
 very well bring patience and forbearance

133
00:09:42,680 --> 00:09:47,530
 and the ability to tolerance. So it could be useful for

134
00:09:47,530 --> 00:09:48,960
 meditation in a preliminary

135
00:09:48,960 --> 00:09:52,970
 way. In this sense, we all have to go through this phase,

136
00:09:52,970 --> 00:09:55,000
 because when we first come to

137
00:09:55,000 --> 00:09:58,570
 meditate it can be torture. Having to sit still, having to

138
00:09:58,570 --> 00:10:00,200
 sit cross-legged, having

139
00:10:00,200 --> 00:10:04,460
 to eat only in the morning and so on can be quite difficult

140
00:10:04,460 --> 00:10:06,640
, because our tolerance is

141
00:10:06,640 --> 00:10:15,650
 quite low in the beginning. Our state of being is quite out

142
00:10:15,650 --> 00:10:18,920
 of balance. Just to create balance,

143
00:10:18,920 --> 00:10:21,050
 we often have to go through some suffering. Just the

144
00:10:21,050 --> 00:10:22,680
 tension in the body, for example.

145
00:10:22,680 --> 00:10:25,710
 Many people come to meditate, and just sitting still

146
00:10:25,710 --> 00:10:28,320
 creates tension in the shoulders, tension

147
00:10:28,320 --> 00:10:35,980
 in the back, because our minds are tense, our minds are in

148
00:10:35,980 --> 00:10:38,880
 a state of stress. So in

149
00:10:38,880 --> 00:10:41,810
 some sense we do have to go through a little bit of torture

150
00:10:41,810 --> 00:10:43,640
 in the beginning, but we should

151
00:10:43,640 --> 00:10:48,630
 never have the idea that somehow this is the way out of

152
00:10:48,630 --> 00:10:52,520
 suffering. When some people come

153
00:10:52,520 --> 00:10:56,460
 to meditate and they think the idea is to experience

154
00:10:56,460 --> 00:10:59,040
 suffering, and they will scorn

155
00:10:59,040 --> 00:11:02,720
 those people who meditate and find peace and happiness and

156
00:11:02,720 --> 00:11:04,080
 bliss. On the other hand, some

157
00:11:04,080 --> 00:11:07,870
 people come to meditate and think that it has to be a state

158
00:11:07,870 --> 00:11:10,400
 of bliss and peace and happiness.

159
00:11:10,400 --> 00:11:13,310
 And if you're not feeling calm and peaceful, then something

160
00:11:13,310 --> 00:11:15,040
 is wrong with your meditation.

161
00:11:15,040 --> 00:11:19,250
 Both of these are wrong understandings, and it leads back

162
00:11:19,250 --> 00:11:21,800
 to the two extremes. So this

163
00:11:21,800 --> 00:11:26,220
 is the context that we come into for the Buddha's first

164
00:11:26,220 --> 00:11:29,280
 teaching. Two months prior, he had become

165
00:11:29,280 --> 00:11:32,990
 enlightened, and he spent two months walking. That's not

166
00:11:32,990 --> 00:11:35,560
 true. He spent, wait a second,

167
00:11:35,560 --> 00:11:43,090
 let's get this right, May, June, July. So two months, and

168
00:11:43,090 --> 00:11:47,040
 he spent 49 days reflecting

169
00:11:47,040 --> 00:11:51,470
 on his enlightenment and living just in the bliss and peace

170
00:11:51,470 --> 00:11:54,280
 and happiness of enlightenment.

171
00:11:54,280 --> 00:12:00,760
 And then the last days, what is that? That's actually not

172
00:12:00,760 --> 00:12:05,160
 many days. We have 49 days, which

173
00:12:05,160 --> 00:12:12,510
 is 30, 19, so only 11 days or something. I hope I've got

174
00:12:12,510 --> 00:12:14,720
 this right. Anyway, that he

175
00:12:14,720 --> 00:12:21,120
 spent walking to find these five recklessness, because what

176
00:12:21,120 --> 00:12:23,800
 happened was he had been torturing

177
00:12:23,800 --> 00:12:27,740
 himself for six years, and then he realized it was useless.

178
00:12:27,740 --> 00:12:29,800
 He realized that if he tortured

179
00:12:29,800 --> 00:12:32,410
 himself any further, he would die. He couldn't go any

180
00:12:32,410 --> 00:12:34,440
 further. He'd done everything. He'd

181
00:12:34,440 --> 00:12:38,320
 stopped eating food. He'd even stopped breathing. He'd

182
00:12:38,320 --> 00:12:40,680
 tortured himself in every way possible,

183
00:12:40,680 --> 00:12:44,820
 and he said, "I can't go any farther with this, and yet

184
00:12:44,820 --> 00:12:47,400
 still I have no wisdom and understanding."

185
00:12:47,400 --> 00:12:50,840
 This is the key, because many people will practice

186
00:12:50,840 --> 00:12:53,080
 meditation and gain this or gain

187
00:12:53,080 --> 00:12:58,100
 that and never gain any wisdom or understanding about

188
00:12:58,100 --> 00:13:01,160
 themselves or about reality. This is

189
00:13:01,160 --> 00:13:03,410
 a wake-up call. It should be a wake-up call for all of us,

190
00:13:03,410 --> 00:13:04,440
 for those of us who sit in

191
00:13:04,440 --> 00:13:08,380
 meditation and find great peace or happiness, and also for

192
00:13:08,380 --> 00:13:10,040
 those of us who sit in meditation

193
00:13:10,040 --> 00:13:12,960
 and torture ourselves, that if we're not really gaining

194
00:13:12,960 --> 00:13:14,800
 wisdom and understanding, then there's

195
00:13:14,800 --> 00:13:20,630
 something wrong. The Buddha said, "Maybe there's another

196
00:13:20,630 --> 00:13:23,560
 way." He tried to allow these peaceful

197
00:13:23,560 --> 00:13:28,950
 and happy feelings to arise. He went and ate food and

198
00:13:28,950 --> 00:13:32,040
 brought his body back to a state

199
00:13:32,040 --> 00:13:36,900
 of normality, where it wasn't always groaning and

200
00:13:36,900 --> 00:13:40,160
 complaining to him. Then he started practicing

201
00:13:40,160 --> 00:13:45,460
 and allowed peaceful and happy feelings to come up and

202
00:13:45,460 --> 00:13:48,760
 allowed the natural state of the

203
00:13:48,760 --> 00:13:54,790
 mind to arise. It was quite peaceful and happy. He watched

204
00:13:54,790 --> 00:13:56,640
 this, and he watched everything

205
00:13:56,640 --> 00:14:00,200
 that came up, and he came to understand how attachment

206
00:14:00,200 --> 00:14:01,600
 works. This is what I've been saying

207
00:14:01,600 --> 00:14:05,000
 many times, is that if you want to understand attachment,

208
00:14:05,000 --> 00:14:06,960
 you have to let the pleasure come

209
00:14:06,960 --> 00:14:11,380
 up. You have to let the desire come up. If you just repress

210
00:14:11,380 --> 00:14:13,720
 them all the time, then you're

211
00:14:13,720 --> 00:14:16,870
 not going to be able to see the truth. What we want to

212
00:14:16,870 --> 00:14:18,520
 understand is our addictions and

213
00:14:18,520 --> 00:14:22,770
 how to overcome them. If we don't let them arise, then we

214
00:14:22,770 --> 00:14:25,160
'll never be able to understand

215
00:14:25,160 --> 00:14:29,600
 and overcome them. We'll just always be repressing them.

216
00:14:29,600 --> 00:14:31,000
 Once we understand the nature of the

217
00:14:31,000 --> 00:14:34,350
 object of our addiction and the nature of the addiction

218
00:14:34,350 --> 00:14:36,840
 itself, then we'll be able to

219
00:14:36,840 --> 00:14:39,400
 overcome it. We'll be able to give it up. This is the path

220
00:14:39,400 --> 00:14:41,720
 to enlightenment. The last thing

221
00:14:41,720 --> 00:14:47,480
 the Buddha realized was that everything that arises arises

222
00:14:47,480 --> 00:14:49,920
 based on the cause. He saw the

223
00:14:49,920 --> 00:14:53,330
 cause of suffering, and he saw the way out of suffering. He

224
00:14:53,330 --> 00:14:54,840
 saw that suffering is only

225
00:14:54,840 --> 00:15:01,430
 caused by craving and clinging. He saw how it works, that

226
00:15:01,430 --> 00:15:04,560
 there was no good reason to

227
00:15:04,560 --> 00:15:08,210
 cling to anything, that it was the clinging itself that was

228
00:15:08,210 --> 00:15:11,520
 causing the suffering. When

229
00:15:11,520 --> 00:15:13,910
 ordinarily we think that there are certain things that when

230
00:15:13,910 --> 00:15:15,040
 we cling to them, they're

231
00:15:15,040 --> 00:15:18,130
 going to bring us happiness. We think the way out of

232
00:15:18,130 --> 00:15:20,320
 suffering is to just arrange things

233
00:15:20,320 --> 00:15:23,490
 so that we only get those things that are worth clinging to

234
00:15:23,490 --> 00:15:24,520
. The Buddha realized that

235
00:15:24,520 --> 00:15:32,520
 it's the clinging itself that leads to suffering, nothing

236
00:15:32,520 --> 00:15:36,360
 to do with the object. The point is

237
00:15:36,360 --> 00:15:41,150
 that everyone else still thought that the way out of

238
00:15:41,150 --> 00:15:43,760
 suffering was to torture yourself.

239
00:15:43,760 --> 00:15:50,060
 These five followers of the Bodhisattva, the Buddha-to-be,

240
00:15:50,060 --> 00:15:53,440
 became quite disenchanted with

241
00:15:53,440 --> 00:15:57,350
 him and lost all their faith in him when he started taking

242
00:15:57,350 --> 00:15:59,760
 food again and stopped torturing

243
00:15:59,760 --> 00:16:08,640
 himself. They thought he had given up the path, given up

244
00:16:08,640 --> 00:16:14,120
 the practice, and so they left. Now

245
00:16:14,120 --> 00:16:18,480
 the Buddha went back to find them, and this is where he

246
00:16:18,480 --> 00:16:21,080
 gave the first teaching. The first

247
00:16:21,080 --> 00:16:24,930
 teaching started on this day. This is what this day

248
00:16:24,930 --> 00:16:29,120
 represents today. It starts with

249
00:16:29,120 --> 00:16:33,780
 these two extremes. Some people who read this don't quite

250
00:16:33,780 --> 00:16:35,720
 understand with the context of

251
00:16:35,720 --> 00:16:39,520
 this teaching, but this is the real context, the point that

252
00:16:39,520 --> 00:16:41,320
 the Buddha had come to, the

253
00:16:41,320 --> 00:16:45,160
 realization that these two extremes were useless. The fact

254
00:16:45,160 --> 00:16:47,200
 that these five recklessness still

255
00:16:47,200 --> 00:16:50,820
 thought that it was in terms of these two extremes, that

256
00:16:50,820 --> 00:16:52,760
 one extreme was the right way,

257
00:16:52,760 --> 00:16:55,140
 one extreme was the wrong way. The Buddha said both

258
00:16:55,140 --> 00:16:56,520
 extremes are the wrong way. They

259
00:16:56,520 --> 00:17:00,670
 thought he had gone back to the other extreme of indulgence

260
00:17:00,670 --> 00:17:03,040
, and he said no, actually both

261
00:17:03,040 --> 00:17:07,810
 of these. It's not that I think that pleasure is the right

262
00:17:07,810 --> 00:17:10,240
 extreme and pain is the wrong

263
00:17:10,240 --> 00:17:15,040
 extreme. It's that both extremes are useless. Engaging in

264
00:17:15,040 --> 00:17:17,520
 sensual pleasure leads only to

265
00:17:17,520 --> 00:17:21,030
 addiction and attachment and partiality, but engaging in

266
00:17:21,030 --> 00:17:22,960
 torturing yourself is also just

267
00:17:22,960 --> 00:17:29,640
 repressing and avoiding the nature of clinging. We don't

268
00:17:29,640 --> 00:17:31,640
 have a problem with unpleasant things.

269
00:17:31,640 --> 00:17:36,000
 We don't want them. We're not going to cling to unpleasant

270
00:17:36,000 --> 00:17:38,040
 things. We have to look at the

271
00:17:38,040 --> 00:17:40,520
 pleasant things that we cling to, because once we don't

272
00:17:40,520 --> 00:17:42,040
 cling to anything, then we'll

273
00:17:42,040 --> 00:17:46,520
 have no partiality, and the unpleasant things will no

274
00:17:46,520 --> 00:17:50,040
 longer be unpleasant. We make no comparison.

275
00:17:50,040 --> 00:17:59,040
 This is what we are undertaking here. We are taking the

276
00:17:59,040 --> 00:18:02,240
 first step, and we take this

277
00:18:02,240 --> 00:18:05,130
 step again and again when we take the precepts. It's a

278
00:18:05,130 --> 00:18:07,080
 reaffirmation that we have started

279
00:18:07,080 --> 00:18:11,140
 on the path and that we too want to come to see the truth

280
00:18:11,140 --> 00:18:13,640
 of reality and want to overcome

281
00:18:13,640 --> 00:18:23,090
 our attachments. The first step is to focus our energies

282
00:18:23,090 --> 00:18:27,240
 and to focus our activities in

283
00:18:27,240 --> 00:18:32,050
 terms of those activities which will bring clarity of mind

284
00:18:32,050 --> 00:18:34,600
 and wisdom and understanding

285
00:18:34,600 --> 00:18:37,990
 while avoiding those activities that are going to cloud the

286
00:18:37,990 --> 00:18:39,480
 mind. This is where we start

287
00:18:39,480 --> 00:18:46,730
 with these five or eight precepts, with the teaching of

288
00:18:46,730 --> 00:18:49,520
 morality. The three refuges is

289
00:18:49,520 --> 00:18:53,840
 simply an asservation, a determination that we are going to

290
00:18:53,840 --> 00:18:57,360
 practice. This is like saying,

291
00:18:57,360 --> 00:19:00,630
 I am committing myself to this path. The Buddha is my

292
00:19:00,630 --> 00:19:03,280
 teacher, the Dhamma is the teaching I'm

293
00:19:03,280 --> 00:19:08,960
 following, and the Sangha are my support group, the other

294
00:19:08,960 --> 00:19:11,880
 Buddhist meditators and practitioners.

295
00:19:11,880 --> 00:19:16,010
 I'm going to take these three things. There's a note here

296
00:19:16,010 --> 00:19:18,080
 that we really should take this

297
00:19:18,080 --> 00:19:21,910
 seriously, that we should think of the Buddha often. The

298
00:19:21,910 --> 00:19:24,000
 Buddha recommended to think of

299
00:19:24,000 --> 00:19:26,810
 him. We have Buddha images, and we do prostrations in front

300
00:19:26,810 --> 00:19:28,680
 of the Buddha image. It's not because

301
00:19:28,680 --> 00:19:32,520
 of worship or so on, it's just a relationship we have with

302
00:19:32,520 --> 00:19:34,560
 the Buddha, with someone who

303
00:19:34,560 --> 00:19:37,380
 reminds us of good things. It's not God, it's not someone

304
00:19:37,380 --> 00:19:39,920
 who's going to answer our prayers,

305
00:19:39,920 --> 00:19:42,860
 but it really does have a strong effect on your mind. This

306
00:19:42,860 --> 00:19:44,280
 is why theistic religions

307
00:19:44,280 --> 00:19:49,230
 really have a powerful effect on people's minds because it

308
00:19:49,230 --> 00:19:51,920
 creates faith and confidence.

309
00:19:51,920 --> 00:19:56,620
 If we channel those good qualities of mind in a way that is

310
00:19:56,620 --> 00:19:58,640
 actually meaningful, then

311
00:19:58,640 --> 00:20:02,460
 this is the practice to gain wisdom and understanding. Then

312
00:20:02,460 --> 00:20:04,360
 this faith will have a profound effect

313
00:20:04,360 --> 00:20:07,680
 on our practice, this confidence that we have, thinking,

314
00:20:07,680 --> 00:20:09,520
 yes, we have this great teacher

315
00:20:09,520 --> 00:20:14,570
 who taught these wonderful things, and he was right. We're

316
00:20:14,570 --> 00:20:17,080
 feeling good and confident

317
00:20:17,080 --> 00:20:20,160
 about that. When we have doubts, we just think of how hard

318
00:20:20,160 --> 00:20:21,680
 the Buddha had to work, and we

319
00:20:21,680 --> 00:20:26,130
 think of the sacrifice he made, and we think of his example

320
00:20:26,130 --> 00:20:28,400
, that it is possible and his

321
00:20:28,400 --> 00:20:33,940
 reassurance that someone can practice in this way. We

322
00:20:33,940 --> 00:20:34,880
 should think about the Buddha. The

323
00:20:34,880 --> 00:20:37,130
 Dhamma is something that we should think about. We should

324
00:20:37,130 --> 00:20:38,520
 study the Dhamma. The Dhamma is

325
00:20:38,520 --> 00:20:41,000
 the teaching, so we should take this seriously, and we

326
00:20:41,000 --> 00:20:42,720
 should actually spend time reading

327
00:20:42,720 --> 00:20:47,700
 the Buddha's teaching, listening to talks, and so on. You

328
00:20:47,700 --> 00:20:48,760
 can pick up books, and you

329
00:20:48,760 --> 00:20:53,230
 can look on the internet. There are textual teachings, and

330
00:20:53,230 --> 00:20:53,880
 so on.

331
00:20:53,880 --> 00:20:56,380
 And the Sangha, we really should take this seriously, and

332
00:20:56,380 --> 00:20:58,520
 here's a chance for me to plug

333
00:20:58,520 --> 00:21:06,680
 our new website, which is my.suryamungalu.org, which is a

334
00:21:06,680 --> 00:21:07,520
 chance for everyone to get involved

335
00:21:07,520 --> 00:21:10,910
 in our group. If you go and log on, I guess everyone here

336
00:21:10,910 --> 00:21:13,080
 has already logged on, but please

337
00:21:13,080 --> 00:21:17,000
 do make the most of it. If there are problems with it, let

338
00:21:17,000 --> 00:21:19,520
 me know. If you have suggestions,

339
00:21:19,520 --> 00:21:25,280
 let me know. And please do use this website at the very

340
00:21:25,280 --> 00:21:27,840
 least as a means of communicating

341
00:21:27,840 --> 00:21:29,890
 with each other. It doesn't mean that we have to come on

342
00:21:29,890 --> 00:21:31,040
 and post this and post that and

343
00:21:31,040 --> 00:21:33,880
 be really active. This means come on every so often. Let us

344
00:21:33,880 --> 00:21:35,080
 know what you're doing and

345
00:21:35,080 --> 00:21:38,900
 meditating, and if you have any questions, you can post

346
00:21:38,900 --> 00:21:42,960
 them on the ask.suryamungalu.org.

347
00:21:42,960 --> 00:21:48,960
 But organizing things. Someone started a Dhamma group in

348
00:21:48,960 --> 00:21:52,200
 England. Owen started a Dhamma group

349
00:21:52,200 --> 00:21:58,360
 in England based on the Mahasi Sayadaw tradition. And so

350
00:21:58,360 --> 00:22:03,120
 hopefully that group will come together

351
00:22:03,120 --> 00:22:09,920
 and can use the my.suryamungalu.org. Maybe that will help.

352
00:22:09,920 --> 00:22:11,720
 Different ways that we can use this at the very least to

353
00:22:11,720 --> 00:22:12,800
 see that there are other people

354
00:22:12,800 --> 00:22:16,330
 in our group. And we have somehow an international group,

355
00:22:16,330 --> 00:22:18,160
 those people who have come here to

356
00:22:18,160 --> 00:22:21,840
 practice. Using the Sangha as a support group, because it's

357
00:22:21,840 --> 00:22:23,960
 great encouragement and you really

358
00:22:23,960 --> 00:22:27,550
 do need this kind of encouragement, especially when you're

359
00:22:27,550 --> 00:22:29,520
 living on your own, surrounded

360
00:22:29,520 --> 00:22:32,410
 often by people who aren't meditating. So here is an

361
00:22:32,410 --> 00:22:34,520
 opportunity for us to come together

362
00:22:34,520 --> 00:22:37,900
 and just post something every so often and read things. You

363
00:22:37,900 --> 00:22:39,480
 can read, at the very least,

364
00:22:39,480 --> 00:22:43,630
 you can read what I'm doing, if that's interesting. What we

365
00:22:43,630 --> 00:22:45,520
're doing here and the projects that

366
00:22:45,520 --> 00:22:49,340
 we have and the work that's going on. So this is the three

367
00:22:49,340 --> 00:22:51,800
 refuges. Then the precepts are

368
00:22:51,800 --> 00:22:55,160
 the beginning of the practice. The three refuges are just a

369
00:22:55,160 --> 00:22:57,080
 determination of the practice that

370
00:22:57,080 --> 00:23:00,300
 we're going to take. The precepts are beginning of the

371
00:23:00,300 --> 00:23:02,160
 practice. So we have this determination,

372
00:23:02,160 --> 00:23:06,970
 okay, my first step is I'm going to focus my activities.

373
00:23:06,970 --> 00:23:08,160
 And this is going to create

374
00:23:08,160 --> 00:23:11,670
 concentration because I'm not going to have this guilt all

375
00:23:11,670 --> 00:23:12,960
 the time. I'm not going to

376
00:23:12,960 --> 00:23:16,180
 have clouding in my mind. I'm not going to have anger and

377
00:23:16,180 --> 00:23:17,480
 greed. I'm going to cut these

378
00:23:17,480 --> 00:23:20,000
 things off. I'm going to abstain from them. When I want

379
00:23:20,000 --> 00:23:21,440
 something, I'm going to force

380
00:23:21,440 --> 00:23:23,850
 myself to look at the wanting. When I don't like something,

381
00:23:23,850 --> 00:23:24,920
 I'm going to force myself

382
00:23:24,920 --> 00:23:29,350
 to look at the disliking. When I have views and opinions, I

383
00:23:29,350 --> 00:23:31,400
'm going to force myself to

384
00:23:31,400 --> 00:23:34,980
 challenge them and to give up views and opinions in favor

385
00:23:34,980 --> 00:23:37,400
 of realizations and understanding.

386
00:23:37,400 --> 00:23:42,600
 So this morality is really for the purpose of the

387
00:23:42,600 --> 00:23:46,960
 development of concentration or focus

388
00:23:46,960 --> 00:23:49,840
 and wisdom. Again, I don't like using the word

389
00:23:49,840 --> 00:23:52,960
 concentration because I think it's misleading.

390
00:23:52,960 --> 00:23:57,410
 It's probably not even appropriate. The word is samadhi.

391
00:23:57,410 --> 00:24:00,520
 Samadhi means composure really.

392
00:24:00,520 --> 00:24:03,950
 Sama means same. It doesn't mean same, but it comes from

393
00:24:03,950 --> 00:24:05,680
 the same root as the word same

394
00:24:05,680 --> 00:24:12,270
 in English. Sama means level. So when your mind is balanced

395
00:24:12,270 --> 00:24:14,680
, having a balanced mind,

396
00:24:14,680 --> 00:24:18,460
 it doesn't mean you concentrate your mind to the point

397
00:24:18,460 --> 00:24:20,800
 where you don't experience anything

398
00:24:20,800 --> 00:24:23,540
 else or so on. It's where you see something perfectly

399
00:24:23,540 --> 00:24:25,360
 clearly. It's like focusing a camera,

400
00:24:25,360 --> 00:24:28,580
 as I've said before. You have to focus the camera exactly

401
00:24:28,580 --> 00:24:30,640
 correctly. If you focus it

402
00:24:30,640 --> 00:24:33,850
 too much, this would be like concentrating, then it goes

403
00:24:33,850 --> 00:24:35,620
 out of focus again. If you don't

404
00:24:35,620 --> 00:24:40,290
 focus enough, then it doesn't work. You have to find

405
00:24:40,290 --> 00:24:43,240
 exactly the right focus. This is simply

406
00:24:43,240 --> 00:24:46,560
 seeing things as they are. So as we use the technique of

407
00:24:46,560 --> 00:24:48,480
 being mindful, reminding ourselves,

408
00:24:48,480 --> 00:24:51,570
 this is seeing, this is hearing, this is smelling, this is

409
00:24:51,570 --> 00:24:53,120
 tasting. As the Buddha said, "When

410
00:24:53,120 --> 00:24:55,530
 you see something, let it only be seeing. When you hear

411
00:24:55,530 --> 00:24:57,280
 something, let it only be hearing."

412
00:24:57,280 --> 00:25:00,230
 So remind yourself, this is seeing, this is hearing. When

413
00:25:00,230 --> 00:25:01,800
 it's pain, remind yourself,

414
00:25:01,800 --> 00:25:04,990
 this is pain. When you feel happy, remind yourself, this is

415
00:25:04,990 --> 00:25:06,600
 happiness. It's not good,

416
00:25:06,600 --> 00:25:11,160
 it's not bad, it's not me, it's not mine. By doing this,

417
00:25:11,160 --> 00:25:13,320
 your mind becomes focused. It

418
00:25:13,320 --> 00:25:17,190
 comes into focus and you start to see things clearly. Your

419
00:25:17,190 --> 00:25:19,280
 mind might not be concentrated

420
00:25:19,280 --> 00:25:21,670
 in the sense it might still jump here and there, but when

421
00:25:21,670 --> 00:25:22,880
 it jumps, you clearly see

422
00:25:22,880 --> 00:25:27,680
 it jumps. You see the next object in line as it is, not as

423
00:25:27,680 --> 00:25:29,880
 you'd like it to be or with

424
00:25:31,840 --> 00:25:35,070
 any projections, but simply and clearly as the reality for

425
00:25:35,070 --> 00:25:37,240
 what it is. It's actually

426
00:25:37,240 --> 00:25:41,750
 quite simple. So concentration or focus is therefore for

427
00:25:41,750 --> 00:25:44,240
 the purpose of gaining wisdom,

428
00:25:44,240 --> 00:25:47,850
 because once you're in focus, you will see things as they

429
00:25:47,850 --> 00:25:49,720
 are. Once you see things as

430
00:25:49,720 --> 00:25:54,270
 they are, you'll become free from suffering. Your mind will

431
00:25:54,270 --> 00:25:56,360
 let go. This is really the

432
00:25:56,360 --> 00:26:00,850
 key that the way to become free from suffering is not by

433
00:26:00,850 --> 00:26:03,360
 any force of mind. It's simply by

434
00:26:03,360 --> 00:26:08,440
 understanding things as they are. The only reason one would

435
00:26:08,440 --> 00:26:10,080
 ever create suffering for

436
00:26:10,080 --> 00:26:14,460
 oneself is when one doesn't realize that one is creating

437
00:26:14,460 --> 00:26:17,080
 suffering for oneself. Think about

438
00:26:17,080 --> 00:26:21,300
 it. No one would ever do something if they knew that that

439
00:26:21,300 --> 00:26:23,400
 thing was going to cause them

440
00:26:23,400 --> 00:26:27,780
 harm. The only reason we do things is because we think it

441
00:26:27,780 --> 00:26:30,240
 has some benefit, that there's

442
00:26:30,240 --> 00:26:33,860
 some purpose to it, that this is a good thing to do. Once

443
00:26:33,860 --> 00:26:35,040
 you see clearly that something

444
00:26:35,040 --> 00:26:37,150
 is not a good thing to do, that there's no benefit that

445
00:26:37,150 --> 00:26:38,400
 comes from it, then you give

446
00:26:38,400 --> 00:26:41,420
 it up. If you examine it and you see that there is benefit

447
00:26:41,420 --> 00:26:43,000
 and it does create goodness

448
00:26:43,000 --> 00:26:47,760
 and peace and happiness, then you develop it. So the only

449
00:26:47,760 --> 00:26:48,720
 thing we need to do in order

450
00:26:48,720 --> 00:26:51,450
 to become free from suffering is quite simple, is just to

451
00:26:51,450 --> 00:26:53,080
 see things as they are, because

452
00:26:53,080 --> 00:26:56,960
 you'll never do something that is useless when you know

453
00:26:56,960 --> 00:26:58,960
 that it's useless. The problem

454
00:26:58,960 --> 00:27:02,950
 is we don't know that many of the things that we do and say

455
00:27:02,950 --> 00:27:05,960
 and think are useless and detrimental

456
00:27:05,960 --> 00:27:12,190
 to ourselves and to other beings. So this is a brief

457
00:27:12,190 --> 00:27:14,200
 exposition of what this ceremony

458
00:27:16,600 --> 00:27:23,590
 means and what this day means or should mean to all of us.

459
00:27:23,590 --> 00:27:23,600
 I'd like to thank everyone for

460
00:27:23,600 --> 00:27:26,760
 joining. Maybe before we quit, for those of you who still

461
00:27:26,760 --> 00:27:28,440
 have time and it's not too late

462
00:27:28,440 --> 00:27:33,470
 or too early in the morning, we can do a short meditation,

463
00:27:33,470 --> 00:27:35,440
 group meditation. Then I'll take

464
00:27:35,440 --> 00:27:39,800
 questions if there are still people left and we can go from

465
00:27:39,800 --> 00:27:41,720
 there. So I'm going to start

466
00:27:41,720 --> 00:27:45,000
 with the meditation. You're welcome to leave whenever you

467
00:27:45,000 --> 00:27:46,400
 want. I'm not going to keep the

468
00:27:46,400 --> 00:27:50,400
 audio on now. I'm going to turn the audio off so you can

469
00:27:50,400 --> 00:27:52,520
 just set your own timer or just

470
00:27:52,520 --> 00:27:56,660
 peek every so often and see when the text chat is. I think

471
00:27:56,660 --> 00:27:58,000
 if you turn the sound on in the

472
00:27:58,000 --> 00:28:02,120
 text chat, there's a little speaker in the bottom right

473
00:28:02,120 --> 00:28:04,680
 corner. If you click that, then

474
00:28:04,680 --> 00:28:08,380
 every time someone says something, it gives you a beep. So

475
00:28:08,380 --> 00:28:10,360
 after 15 minutes I'm going

476
00:28:10,360 --> 00:28:14,760
 to say something and you'll hear that beep. Otherwise, this

477
00:28:14,760 --> 00:28:16,800
 has been another episode of

478
00:28:16,800 --> 00:28:18,360
 Monk Radio. Thanks for tuning in.

479
00:28:18,360 --> 00:28:25,360
 [

