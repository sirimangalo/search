1
00:00:00,000 --> 00:00:04,000
 Okay, good evening everyone.

2
00:00:04,000 --> 00:00:06,000
 Welcome.

3
00:00:06,000 --> 00:00:11,000
 Tonight's Dhamma talk.

4
00:00:11,000 --> 00:00:14,000
 Probably not going to be that long tonight, we'll see.

5
00:00:14,000 --> 00:00:19,000
 But I have something short, but important to bring us back

6
00:00:19,000 --> 00:00:23,000
 to, make us think of.

7
00:00:23,000 --> 00:00:31,350
 What I'd like to talk about tonight is the Baddhika Rata

8
00:00:31,350 --> 00:00:32,000
 Sutta,

9
00:00:32,000 --> 00:00:46,000
 which is often translated as the single excellent night.

10
00:00:46,000 --> 00:00:52,000
 It's the sutta on how to have one good day actually.

11
00:00:52,000 --> 00:00:57,440
 In Pali they use the word rati, which means night, to

12
00:00:57,440 --> 00:00:59,000
 signify a day.

13
00:00:59,000 --> 00:01:02,000
 They count by the nights.

14
00:01:02,000 --> 00:01:07,000
 So in English we count by days, so we say how many days.

15
00:01:07,000 --> 00:01:14,000
 And so we'd say one good day.

16
00:01:14,000 --> 00:01:18,260
 Why it's interesting to talk about this is because the

17
00:01:18,260 --> 00:01:19,000
 Buddha found it,

18
00:01:19,000 --> 00:01:24,000
 it seems to have found it fairly, outstandingly important.

19
00:01:24,000 --> 00:01:28,000
 Something that he taught repeatedly, we have,

20
00:01:28,000 --> 00:01:31,230
 and the people who put together the compilation of the

21
00:01:31,230 --> 00:01:36,000
 Buddha's teaching that we have,

22
00:01:36,000 --> 00:01:45,000
 gave us four different suttas on Baddhika Rata.

23
00:01:45,000 --> 00:01:59,000
 And the Majimini Kaya, 31 to 34.

24
00:01:59,000 --> 00:02:05,050
 One good day, something my teacher would often, well, would

25
00:02:05,050 --> 00:02:07,000
 quite often bring up.

26
00:02:07,000 --> 00:02:11,740
 It sums up, it's one of those things that suttas that sums

27
00:02:11,740 --> 00:02:14,000
 up mindfulness quite well.

28
00:02:14,000 --> 00:02:24,000
 And it's quite useful as advice for meditators.

29
00:02:24,000 --> 00:02:33,470
 I mean, just in and of itself it's a teaching, how to have

30
00:02:33,470 --> 00:02:36,000
 a good day.

31
00:02:36,000 --> 00:02:40,760
 Because what it says is it says, first of all, how to be

32
00:02:40,760 --> 00:02:44,000
 perfect, but also how to live now.

33
00:02:44,000 --> 00:02:47,000
 Right?

34
00:02:47,000 --> 00:02:50,000
 We talk about having a good day.

35
00:02:50,000 --> 00:02:53,000
 This is the answer to our questions.

36
00:02:53,000 --> 00:02:57,760
 How do we turn our hopes and our desires and our wants and

37
00:02:57,760 --> 00:02:59,000
 our wishes?

38
00:02:59,000 --> 00:03:03,000
 How do we find what we're looking for here and now?

39
00:03:03,000 --> 00:03:12,000
 How do we make it real so we can have that day?

40
00:03:12,000 --> 00:03:15,610
 And also, according to Buddhism, what does it mean to have

41
00:03:15,610 --> 00:03:17,000
 a good day, right?

42
00:03:17,000 --> 00:03:21,000
 Because for most of us, most people in the world,

43
00:03:21,000 --> 00:03:23,930
 a good day is something quite different from what the

44
00:03:23,930 --> 00:03:25,000
 Buddha had in mind.

45
00:03:25,000 --> 00:03:33,000
 And that kind of good day is not terribly meaningful.

46
00:03:33,000 --> 00:03:35,660
 Because when we talk about having a good day, it's

47
00:03:35,660 --> 00:03:40,000
 something that lasts a single day, right?

48
00:03:40,000 --> 00:03:43,000
 The Buddha meant something quite different.

49
00:03:43,000 --> 00:03:52,000
 And how do you get to the point where your day is good in a

50
00:03:52,000 --> 00:03:56,000
 transformative way?

51
00:03:56,000 --> 00:04:00,000
 So it's not just you have your good day and then it's over.

52
00:04:00,000 --> 00:04:05,000
 You get to the point where you achieve, you accomplish,

53
00:04:05,000 --> 00:04:16,000
 you become something worthwhile in that one day.

54
00:04:16,000 --> 00:04:22,160
 So it starts with a very famous, very, I think, well-known

55
00:04:22,160 --> 00:04:24,000
 teaching of the Buddha,

56
00:04:24,000 --> 00:04:29,000
 "Adityanan wakameyana patigankeyana agatang"

57
00:04:29,000 --> 00:04:34,010
 That will not go back to the past nor worry about the

58
00:04:34,010 --> 00:04:35,000
 future, right?

59
00:04:35,000 --> 00:04:37,480
 You may not have heard those words exactly before, but you

60
00:04:37,480 --> 00:04:41,000
 know that this is what the Buddha taught.

61
00:04:41,000 --> 00:04:46,910
 Don't go back to the past. Don't bring up the past. Don't

62
00:04:46,910 --> 00:04:51,000
 worry about the future.

63
00:04:51,000 --> 00:04:55,000
 "Adityan wakameyana patigankeyana agatang"

64
00:04:55,000 --> 00:04:58,860
 What's in the past is gone already. What's in the future

65
00:04:58,860 --> 00:05:00,000
 has not yet come.

66
00:05:00,000 --> 00:05:06,000
 And this is quite, it's obvious, right?

67
00:05:06,000 --> 00:05:10,150
 This is not a teaching that we have to be taught, do you

68
00:05:10,150 --> 00:05:11,000
 think?

69
00:05:11,000 --> 00:05:13,160
 I don't suppose there's anyone here who didn't know that

70
00:05:13,160 --> 00:05:14,000
 the past is already gone

71
00:05:14,000 --> 00:05:20,000
 or that the future hasn't come yet.

72
00:05:20,000 --> 00:05:25,000
 But it's important to repeat.

73
00:05:25,000 --> 00:05:31,730
 We act as though the future is here and that the future is

74
00:05:31,730 --> 00:05:34,000
 a certain thing.

75
00:05:34,000 --> 00:05:38,000
 And we act as though the past is here as well.

76
00:05:38,000 --> 00:05:41,000
 When we think about the past, it makes us suffer.

77
00:05:41,000 --> 00:05:47,960
 The bad things make us suffer just as they did when they

78
00:05:47,960 --> 00:05:51,000
 actually happened.

79
00:05:51,000 --> 00:05:56,000
 When we think about good things, we pine away after them.

80
00:05:56,000 --> 00:06:02,000
 We never really, well, we often don't live in the present.

81
00:06:02,000 --> 00:06:07,390
 We're caught up in the past and the future, pushed and

82
00:06:07,390 --> 00:06:09,000
 pulled.

83
00:06:09,000 --> 00:06:11,490
 And in fact, the only thing that's real and the only thing

84
00:06:11,490 --> 00:06:12,000
 that ever has been

85
00:06:12,000 --> 00:06:18,000
 or ever will be real is now, is the present.

86
00:06:18,000 --> 00:06:23,000
 Don't go back to the past. Don't go ahead to the future.

87
00:06:23,000 --> 00:06:29,000
 You know these people who make plans,

88
00:06:29,000 --> 00:06:34,000
 make plans for the future,

89
00:06:34,000 --> 00:06:41,210
 only to have them ruined by reality, by the uncertainty of

90
00:06:41,210 --> 00:06:42,000
 life.

91
00:06:42,000 --> 00:06:44,330
 In fact, you might say that all of our disappointments in

92
00:06:44,330 --> 00:06:48,000
 life come from making plans.

93
00:06:48,000 --> 00:06:50,730
 If we didn't have expectations, if we didn't live in the

94
00:06:50,730 --> 00:06:52,000
 future and think,

95
00:06:52,000 --> 00:06:56,480
 "Maybe tomorrow I'll get this or that. Maybe tomorrow I'll

96
00:06:56,480 --> 00:06:58,000
 have what I want."

97
00:06:58,000 --> 00:07:00,000
 If we weren't looking for something,

98
00:07:00,000 --> 00:07:05,140
 "Hey, even just the next moment if I open the fridge, there

99
00:07:05,140 --> 00:07:09,000
 will be some delicious food there."

100
00:07:09,000 --> 00:07:11,000
 If we didn't have any of that, we wouldn't be disappointed

101
00:07:11,000 --> 00:07:17,000
 when things turned out differently.

102
00:07:17,000 --> 00:07:20,720
 When we talk about the past, we have this story of Patach

103
00:07:20,720 --> 00:07:22,000
ara who lost her two sons

104
00:07:22,000 --> 00:07:25,420
 and her whole family and her husband in the same day and

105
00:07:25,420 --> 00:07:27,000
 went crazy.

106
00:07:27,000 --> 00:07:31,000
 She was totally lost her mind,

107
00:07:31,000 --> 00:07:34,000
 went out of her mind with grief,

108
00:07:34,000 --> 00:07:40,120
 losing her husband and both of her sons and then her whole

109
00:07:40,120 --> 00:07:41,000
 family.

110
00:07:41,000 --> 00:07:44,000
 The Buddha said to her,

111
00:07:44,000 --> 00:07:49,840
 "Yes, it's true that you've lost quite a bit and that this

112
00:07:49,840 --> 00:07:53,000
 is something that is making you very sad,

113
00:07:53,000 --> 00:07:56,710
 but all the tears that you've cried and the rounds of sams

114
00:07:56,710 --> 00:08:08,000
ara are greater than the waters in all the ocean."

115
00:08:08,000 --> 00:08:22,000
 Meaning that the past is gone and the past is...

116
00:08:22,000 --> 00:08:31,000
 The past is past.

117
00:08:31,000 --> 00:08:35,410
 It's quite insignificant what happened in the past at this

118
00:08:35,410 --> 00:08:37,000
 time or that time.

119
00:08:37,000 --> 00:08:43,000
 All we have now is in the present.

120
00:08:43,000 --> 00:08:47,000
 "Tvaadita nan mokamiya na patikankeyanankatam."

121
00:08:47,000 --> 00:08:50,000
 So then what should we do?

122
00:08:50,000 --> 00:08:52,680
 If we shouldn't go back to the past or we shouldn't bring

123
00:08:52,680 --> 00:08:54,000
 up the past or worry about the future.

124
00:08:54,000 --> 00:09:00,000
 "Patyupanankeyo damang tatatatavipasati."

125
00:09:00,000 --> 00:09:03,710
 Unless I talk about a lot, many of you have probably heard

126
00:09:03,710 --> 00:09:05,000
 me mention this before.

127
00:09:05,000 --> 00:09:09,000
 Patyupanank is the present moment.

128
00:09:09,000 --> 00:09:17,000
 "Yodamang"

129
00:09:17,000 --> 00:09:22,060
 Whatever "dhamma", "dhamma" is a thing or an experience in

130
00:09:22,060 --> 00:09:26,000
 this case, arises in the present.

131
00:09:26,000 --> 00:09:30,000
 "Tatatatatavipasati"

132
00:09:30,000 --> 00:09:32,590
 You hear this word "vipasati" is where we get the word "vip

133
00:09:32,590 --> 00:09:34,000
asana" from.

134
00:09:34,000 --> 00:09:39,000
 "Vipasana" means insight or it means seeing clearly.

135
00:09:39,000 --> 00:09:44,000
 "Vipasati" means he or she sees clearly.

136
00:09:44,000 --> 00:09:48,000
 That's the verb form.

137
00:09:48,000 --> 00:09:51,230
 So when someone doesn't go back to the past or the future,

138
00:09:51,230 --> 00:09:55,000
 one sees what's in the present clearly.

139
00:09:55,000 --> 00:09:58,000
 Whatever arises in the present.

140
00:09:58,000 --> 00:10:02,500
 That's the essence of our practice. That should be clear to

141
00:10:02,500 --> 00:10:04,000
 everyone.

142
00:10:04,000 --> 00:10:08,320
 But I think it bears mentioning and it bears reaffirming

143
00:10:08,320 --> 00:10:10,000
 how powerful that is.

144
00:10:10,000 --> 00:10:18,000
 And the far reaching implications of it.

145
00:10:18,000 --> 00:10:25,720
 That actually every problem that we have, right, becomes an

146
00:10:25,720 --> 00:10:27,000
 experience.

147
00:10:27,000 --> 00:10:31,000
 It really is applicable everywhere.

148
00:10:31,000 --> 00:10:34,560
 Someone's beating you with a stick. You really can be

149
00:10:34,560 --> 00:10:35,000
 mindful.

150
00:10:35,000 --> 00:10:39,000
 If you're present, it's not really a problem.

151
00:10:39,000 --> 00:10:41,000
 If you're worrying about when they're going to hit you next

152
00:10:41,000 --> 00:10:47,650
 or if you're sad or angry or upset about when they just hit

153
00:10:47,650 --> 00:10:48,000
 you.

154
00:10:48,000 --> 00:10:51,050
 Meaning if you're upset by this experience. That's what

155
00:10:51,050 --> 00:10:53,000
 causes suffering.

156
00:10:53,000 --> 00:10:58,000
 If you're caught up in the past and the future.

157
00:10:58,000 --> 00:11:01,600
 When you're really in the present moment, there's only dham

158
00:11:01,600 --> 00:11:06,000
ma. There's only experience.

159
00:11:06,000 --> 00:11:08,320
 If you lose your job, if you get kicked out of your

160
00:11:08,320 --> 00:11:12,000
 apartment, if you're living on the street.

161
00:11:12,000 --> 00:11:16,000
 If you're starving to death.

162
00:11:16,000 --> 00:11:20,320
 It's a wonderful thing about Buddhism and about this idea

163
00:11:20,320 --> 00:11:24,300
 of the present moment is that it's never going to end, you

164
00:11:24,300 --> 00:11:25,000
 know.

165
00:11:25,000 --> 00:11:29,000
 There's no failure in that sense.

166
00:11:29,000 --> 00:11:33,600
 It's not like you can do anything wrong or it's not like

167
00:11:33,600 --> 00:11:38,000
 you can do anything irrevocably wrong.

168
00:11:38,000 --> 00:11:42,580
 If you mess up this life really bad, then just come back in

169
00:11:42,580 --> 00:11:44,000
 the next life.

170
00:11:44,000 --> 00:11:47,150
 I remember talking, I mentioned this, I think talking to a

171
00:11:47,150 --> 00:11:50,880
 friend of mine who had taken a Buddhism course with me in

172
00:11:50,880 --> 00:11:54,000
 university many years ago.

173
00:11:54,000 --> 00:12:00,000
 Before I was a monk, I think.

174
00:12:00,000 --> 00:12:02,360
 She took the Buddhism course and she said, she was Catholic

175
00:12:02,360 --> 00:12:06,000
 and she said, "It's incredible."

176
00:12:06,000 --> 00:12:10,400
 This idea of rebirth, the idea that you could have another

177
00:12:10,400 --> 00:12:11,000
 chance.

178
00:12:11,000 --> 00:12:17,000
 Because in Christianity and in Catholicism, this is it.

179
00:12:17,000 --> 00:12:25,000
 One chance do it right or go to hell for eternity.

180
00:12:25,000 --> 00:12:29,000
 In Buddhism, hell isn't eternal.

181
00:12:29,000 --> 00:12:32,000
 There's always a second chance.

182
00:12:32,000 --> 00:12:40,820
 And so this power of not fearing the future, not fearing

183
00:12:40,820 --> 00:12:43,000
 consequences,

184
00:12:43,000 --> 00:12:48,620
 is not going to be. What he says next, "A-sang-hirang, a-s

185
00:12:48,620 --> 00:12:49,000
ang-kupang."

186
00:12:49,000 --> 00:12:56,000
 This is invincible, this is unshakable.

187
00:12:56,000 --> 00:13:01,000
 "Tangvidwa manubruhi."

188
00:13:01,000 --> 00:13:05,690
 Let him know that and be sure of it, invincibly, unshakably

189
00:13:05,690 --> 00:13:06,000
.

190
00:13:06,000 --> 00:13:10,000
 The present moment is invincible, it's unshakable.

191
00:13:10,000 --> 00:13:15,000
 No experience, no situation, no conflict.

192
00:13:15,000 --> 00:13:20,000
 That can't be solved, really.

193
00:13:20,000 --> 00:13:26,000
 Vanquished, defeated, conquered by just being mindful.

194
00:13:26,000 --> 00:13:29,270
 It's the difference between trying to change the world

195
00:13:29,270 --> 00:13:30,000
 around you

196
00:13:30,000 --> 00:13:33,330
 and learning to dance with it, to be flexible with it, to

197
00:13:33,330 --> 00:13:35,000
 roll with it,

198
00:13:35,000 --> 00:13:40,000
 to stop reacting to it, really.

199
00:13:40,000 --> 00:13:44,000
 So that's the first half.

200
00:13:44,000 --> 00:13:47,000
 We're halfway there, halfway to having a good day.

201
00:13:47,000 --> 00:13:52,000
 The second half, "Ajivakichamata pangko janya maranansuwe."

202
00:13:52,000 --> 00:13:56,260
 Well, this isn't exactly advice, this is an admonishment, a

203
00:13:56,260 --> 00:13:59,000
 reminder.

204
00:13:59,000 --> 00:14:03,000
 Most of the second half is more of a reminder.

205
00:14:03,000 --> 00:14:06,000
 Today, and this is something we remind ourselves of,

206
00:14:06,000 --> 00:14:09,000
 "Ajivakichamata pangko ji."

207
00:14:09,000 --> 00:14:12,000
 is when we should do the work.

208
00:14:12,000 --> 00:14:14,000
 So I just said you always have a second chance,

209
00:14:14,000 --> 00:14:20,000
 but eventually it's going to have to be done today.

210
00:14:20,000 --> 00:14:27,160
 And in fact, if it's not done today, this today, today,

211
00:14:27,160 --> 00:14:28,000
 today,

212
00:14:28,000 --> 00:14:30,600
 who knows when you'll have another chance, you might die

213
00:14:30,600 --> 00:14:31,000
 tomorrow.

214
00:14:31,000 --> 00:14:35,540
 "Kojanya maranansuwe." Who knows whether death might be

215
00:14:35,540 --> 00:14:37,000
 even tomorrow?

216
00:14:37,000 --> 00:14:41,000
 "Nahino sangarangtena mahase nina machuna."

217
00:14:41,000 --> 00:14:50,000
 There is no bargaining with him.

218
00:14:50,000 --> 00:14:56,000
 No bargain with death,

219
00:14:56,000 --> 00:15:02,000
 together with his great armies, or death's great armies.

220
00:15:02,000 --> 00:15:05,000
 I don't know what the great army or great army,

221
00:15:05,000 --> 00:15:08,990
 the great army of death is. I'm not sure of that mythology,

222
00:15:08,990 --> 00:15:10,000
 but...

223
00:15:10,000 --> 00:15:15,000
 Mahase nina. I have to look up in the commentaries.

224
00:15:15,000 --> 00:15:21,000
 But death and his hordes, death and his armies, his army.

225
00:15:21,000 --> 00:15:27,000
 There's no bargaining with death. It's just imagery.

226
00:15:27,000 --> 00:15:32,000
 Maybe the hordes of death are disease, illness, old age,

227
00:15:32,000 --> 00:15:40,000
 injury, war, murder.

228
00:15:40,000 --> 00:15:44,000
 Those are maybe the armies of death.

229
00:15:44,000 --> 00:15:50,000
 You can die crossing the street. You can die from food.

230
00:15:50,000 --> 00:16:01,000
 You can die because some psychopath decides to stab you

231
00:16:01,000 --> 00:16:15,000
 or blow you up or shoot you.

232
00:16:15,000 --> 00:16:19,340
 You do it now. You don't know when you'll get another

233
00:16:19,340 --> 00:16:21,000
 chance.

234
00:16:21,000 --> 00:16:24,000
 It's not even the most important reason.

235
00:16:24,000 --> 00:16:28,510
 You do it now because it's the best thing you could

236
00:16:28,510 --> 00:16:30,000
 possibly do.

237
00:16:30,000 --> 00:16:32,590
 Being in the present moment, there's no reason to do

238
00:16:32,590 --> 00:16:35,000
 anything else.

239
00:16:35,000 --> 00:16:38,250
 The past doesn't make you happy. The future doesn't make

240
00:16:38,250 --> 00:16:39,000
 you happy.

241
00:16:39,000 --> 00:16:45,000
 Clinging to things certainly doesn't make you happy. W

242
00:16:45,000 --> 00:16:45,000
ishing or wanting for things.

243
00:16:45,000 --> 00:16:48,490
 Fighting or... You know, all the things that we waste our

244
00:16:48,490 --> 00:16:49,000
 time with.

245
00:16:49,000 --> 00:16:51,870
 How much time do we waste fighting with each other, holding

246
00:16:51,870 --> 00:16:53,000
 grudges, bickering?

247
00:16:53,000 --> 00:16:56,330
 You wouldn't believe some of the things that go on in

248
00:16:56,330 --> 00:16:57,000
 Buddhist monasteries

249
00:16:57,000 --> 00:17:03,140
 and even meditation centers. How we waste our time and

250
00:17:03,140 --> 00:17:05,000
 energy fighting.

251
00:17:05,000 --> 00:17:09,000
 As I've said, it's understandable. Everyone has defilements

252
00:17:09,000 --> 00:17:10,000
.

253
00:17:10,000 --> 00:17:17,000
 Even people who are meditating get upset.

254
00:17:17,000 --> 00:17:22,000
 But we should remind ourselves not to turn it into conflict

255
00:17:22,000 --> 00:17:22,000
.

256
00:17:22,000 --> 00:17:25,790
 Not to waste our time with these things. We don't know when

257
00:17:25,790 --> 00:17:28,000
 we're going to die.

258
00:17:28,000 --> 00:17:31,000
 Here we have this great opportunity.

259
00:17:31,000 --> 00:17:39,000
 "Ivangvi haringa taping" - dwelling thus ardently.

260
00:17:39,000 --> 00:17:43,320
 All the ardor you're putting out here. Is that the right

261
00:17:43,320 --> 00:17:44,000
 word?

262
00:17:44,000 --> 00:17:52,000
 All the energy and effort the meditators are putting out.

263
00:17:52,000 --> 00:17:54,000
 This is the greatness.

264
00:17:54,000 --> 00:18:02,000
 "Ohora tama tanditang" - both day and night, relentlessly.

265
00:18:02,000 --> 00:18:06,000
 So some of you have been practicing late into the night.

266
00:18:06,000 --> 00:18:11,000
 Maybe even not sleeping at night.

267
00:18:11,000 --> 00:18:14,000
 Doing great work.

268
00:18:14,000 --> 00:18:19,420
 And those who have put this effort out, if you've listened

269
00:18:19,420 --> 00:18:20,000
 to some of the people

270
00:18:20,000 --> 00:18:24,170
 who've gone through these courses, how incredible the

271
00:18:24,170 --> 00:18:29,000
 transformation can be in some cases.

272
00:18:29,000 --> 00:18:36,350
 Great benefit comes. And how such people can affirm, "Yes,

273
00:18:36,350 --> 00:18:42,000
 they had a good day."

274
00:18:42,000 --> 00:18:48,000
 "Tangwe badekarato ti santo ajikate muni" -

275
00:18:48,000 --> 00:18:53,350
 the peaceful sage has called this one who has had a good

276
00:18:53,350 --> 00:18:58,000
 night or a good day.

277
00:18:58,000 --> 00:19:01,000
 So it's not a very complex teaching.

278
00:19:01,000 --> 00:19:04,670
 It happens to be one of the most important ones to stay in

279
00:19:04,670 --> 00:19:06,000
 the present moment.

280
00:19:06,000 --> 00:19:11,000
 To not go back to the past or head to the future.

281
00:19:11,000 --> 00:19:15,000
 To do your work today, to remember.

282
00:19:15,000 --> 00:19:19,000
 And now is when the work must be done. Right now.

283
00:19:19,000 --> 00:19:22,000
 Listening to me, are you mindful?

284
00:19:22,000 --> 00:19:26,000
 Are you here or are you in the past, in the future?

285
00:19:26,000 --> 00:19:31,000
 Are you caught up in concepts and illusions?

286
00:19:31,000 --> 00:19:37,690
 Or are you clearly aware of reality as it's happening every

287
00:19:37,690 --> 00:19:39,000
 moment?

288
00:19:39,000 --> 00:19:44,000
 There's only one way. There's only one thing to do.

289
00:19:44,000 --> 00:19:52,000
 Having a good day, it's not that hard to do.

290
00:19:52,000 --> 00:19:56,610
 Or it's not that complicated, it's actually something very

291
00:19:56,610 --> 00:19:58,000
 challenging.

292
00:19:58,000 --> 00:20:02,560
 But it's the work that is worth doing. It's the task that

293
00:20:02,560 --> 00:20:05,000
 is worth accomplishing.

294
00:20:05,000 --> 00:20:10,180
 So much appreciation to our meditators here who are working

295
00:20:10,180 --> 00:20:13,000
 diligently.

296
00:20:13,000 --> 00:20:18,000
 Some here in the main hall, some in the room, some outside.

297
00:20:18,000 --> 00:20:21,000
 But so many people wanting to come.

298
00:20:21,000 --> 00:20:26,000
 Javan's going to have to move into a tent soon.

299
00:20:26,000 --> 00:20:30,000
 It's not a joke. He actually is.

300
00:20:30,000 --> 00:20:34,000
 I'm jealous, kind of. I used to live in a tent.

301
00:20:34,000 --> 00:20:37,670
 Anyway, that's the dama for tonight. Thank you all for

302
00:20:37,670 --> 00:20:39,000
 coming up.

303
00:20:39,000 --> 00:20:51,000
 [silence]

304
00:20:51,000 --> 00:20:54,000
 We've got the questions up now.

305
00:20:54,000 --> 00:21:08,000
 [silence]

306
00:21:08,000 --> 00:21:11,650
 Can you clarify your statement yesterday? You can go. You

307
00:21:11,650 --> 00:21:13,000
 all don't have to stick around.

308
00:21:13,000 --> 00:21:16,800
 Can you clarify your statement yesterday that the second

309
00:21:16,800 --> 00:21:19,000
 way to have mindfulness go wrong

310
00:21:19,000 --> 00:21:23,000
 is to have mindfulness of a concept in light of the fact

311
00:21:23,000 --> 00:21:26,000
 that we have traditional meditations on concepts

312
00:21:26,000 --> 00:21:29,000
 such as mindfulness of the Buddha?

313
00:21:29,000 --> 00:21:33,540
 So I thought I made it clear and I thought we talked about

314
00:21:33,540 --> 00:21:35,000
 this afterwards.

315
00:21:35,000 --> 00:21:39,000
 In fact, it seems to me this question came up.

316
00:21:39,000 --> 00:21:43,000
 But it's not wrong in a general sense.

317
00:21:43,000 --> 00:21:50,000
 It's wrong for cultivating insight.

318
00:21:50,000 --> 00:21:56,000
 It's like if you want to go to Bangkok, you go this way.

319
00:21:56,000 --> 00:22:00,320
 Toronto's better. If you want to go to Toronto, you've got

320
00:22:00,320 --> 00:22:03,000
 to take the 403.

321
00:22:03,000 --> 00:22:07,000
 Don't get on the QEW to Niagara. It's the wrong way.

322
00:22:07,000 --> 00:22:10,220
 It doesn't mean there's anything wrong with the way to

323
00:22:10,220 --> 00:22:11,000
 Niagara.

324
00:22:11,000 --> 00:22:13,560
 The way to Niagara is a perfectly fine highway, but it won

325
00:22:13,560 --> 00:22:15,000
't get you to Toronto.

326
00:22:15,000 --> 00:22:20,000
 That's what it means by wrong.

327
00:22:20,000 --> 00:22:24,000
 When you're practicing insight, concepts are wrong because

328
00:22:24,000 --> 00:22:28,000
 they won't get you where you're trying to go.

329
00:22:28,000 --> 00:22:31,110
 What are your thoughts on meditating on loving kindness and

330
00:22:31,110 --> 00:22:32,000
 compassion?

331
00:22:32,000 --> 00:22:35,330
 I think they're good. I wouldn't take it as my primary

332
00:22:35,330 --> 00:22:38,000
 meditation, though you could.

333
00:22:38,000 --> 00:22:41,000
 But I think I've talked about this actually before.

334
00:22:41,000 --> 00:22:44,590
 They're supportive meditations. They're useful to support

335
00:22:44,590 --> 00:22:46,000
 insight meditation.

336
00:22:46,000 --> 00:22:56,000
 So I think, yes, it's good to practice some every day.

337
00:22:56,000 --> 00:23:00,990
 It's a way to change a username. You have to talk to our IT

338
00:23:00,990 --> 00:23:04,000
 people. You can send them an email.

339
00:23:04,000 --> 00:23:12,000
 Luckily, I'm blessed with a really awesome IT team now.

340
00:23:12,000 --> 00:23:15,000
 We've got a real and just a great team in general.

341
00:23:15,000 --> 00:23:18,690
 A shout out to our volunteer community who's put together

342
00:23:18,690 --> 00:23:22,000
 this lovely website and so much more.

343
00:23:22,000 --> 00:23:26,530
 It keeps this place running. At this point, I couldn't do

344
00:23:26,530 --> 00:23:27,000
 it alone anymore.

345
00:23:27,000 --> 00:23:32,790
 It's a group effort of real impressive proportions and just

346
00:23:32,790 --> 00:23:34,000
 growing.

347
00:23:34,000 --> 00:23:46,000
 So keep it up. Thank you, everyone.

348
00:23:46,000 --> 00:23:51,000
 Is this normal or am I doing something wrong?

349
00:23:51,000 --> 00:23:54,970
 Okay, we should have a note at the top. Please do not ask

350
00:23:54,970 --> 00:23:57,000
 if X is normal.

351
00:23:57,000 --> 00:24:01,000
 It's not useful to ask whether something is normal.

352
00:24:01,000 --> 00:24:04,620
 You have an experience. Rather than asking me whether it's

353
00:24:04,620 --> 00:24:07,000
 normal, try and figure out what to do about it,

354
00:24:07,000 --> 00:24:10,000
 how you should relate to it.

355
00:24:10,000 --> 00:24:15,000
 And as I said, your real question should be, is it...

356
00:24:15,000 --> 00:24:21,420
 When you say, "Am I doing something wrong?" So your real

357
00:24:21,420 --> 00:24:23,000
 question should be, "Am I doing something wrong?"

358
00:24:23,000 --> 00:24:27,000
 Is this wrong? Is this a bad state?

359
00:24:27,000 --> 00:24:32,910
 And I can answer that for you, but it's not really where we

360
00:24:32,910 --> 00:24:35,000
 go to tell you whether something is wrong or right.

361
00:24:35,000 --> 00:24:42,000
 In fact, in the end, it's not even a valid question.

362
00:24:42,000 --> 00:24:45,730
 The only thing that is right is mindfulness. And if you're

363
00:24:45,730 --> 00:24:48,000
 mindful about the state that you're in,

364
00:24:48,000 --> 00:24:52,110
 how you see things differently, then you'll let it go

365
00:24:52,110 --> 00:24:55,000
 because you're potentially conceited about it.

366
00:24:55,000 --> 00:24:58,320
 You get conceited thinking, "Hey, look at me. I'm

367
00:24:58,320 --> 00:25:02,000
 progressing, feeling good about yourself, confident."

368
00:25:02,000 --> 00:25:13,000
 Which can be misleading and misguided.

369
00:25:13,000 --> 00:25:18,000
 And so you like this new perspective and so on.

370
00:25:18,000 --> 00:25:20,050
 So I'm not going to tell you whether it's right or wrong. I

371
00:25:20,050 --> 00:25:24,000
 want you to be mindful of it and learn to let it go.

372
00:25:24,000 --> 00:25:34,000
 Even let go of the good things.

373
00:25:34,000 --> 00:25:37,420
 Again, we don't have... Someone wanting me to give their

374
00:25:37,420 --> 00:25:40,240
 thoughts, "Do you want me to tell you whether this is

375
00:25:40,240 --> 00:25:41,000
 normal?"

376
00:25:41,000 --> 00:25:46,820
 So, yeah, same advice. I'm not going to tell you one way or

377
00:25:46,820 --> 00:25:48,000
 the other.

378
00:25:48,000 --> 00:25:51,170
 Look at it, be mindful of it, and you'll see what leads to

379
00:25:51,170 --> 00:25:54,000
 suffering, you'll see what leads to happiness.

380
00:25:54,000 --> 00:25:57,000
 "When one is fully mindful, are there any emotions?"

381
00:25:57,000 --> 00:26:02,000
 Yes, there can be. There aren't always, but there can be.

382
00:26:02,000 --> 00:26:04,720
 I mean, it really depends what you mean by emotion because

383
00:26:04,720 --> 00:26:06,000
 that's a Western word.

384
00:26:06,000 --> 00:26:09,990
 We don't use that word in Buddhism. We don't have such a

385
00:26:09,990 --> 00:26:11,000
 word, really.

386
00:26:11,000 --> 00:26:19,000
 I mean, maybe I could think of some, but not really.

387
00:26:19,000 --> 00:26:24,000
 So, yeah, there are various states of mind.

388
00:26:24,000 --> 00:26:29,080
 In regards to wrong meditation, in walking meditation I

389
00:26:29,080 --> 00:26:31,000
 have a tendency to keep the mantra on autopilot.

390
00:26:31,000 --> 00:26:34,000
 My mind is drifting elsewhere.

391
00:26:34,000 --> 00:26:41,000
 It divides a new scheme, and schemes are bad.

392
00:26:41,000 --> 00:26:44,160
 Schemes are bad from the outright because you're trying to

393
00:26:44,160 --> 00:26:46,000
 fix, and that's not our goal.

394
00:26:46,000 --> 00:26:49,490
 So your mind drifts elsewhere. Mind drifting elsewhere is

395
00:26:49,490 --> 00:26:51,000
 teaching you something.

396
00:26:51,000 --> 00:26:54,620
 It's teaching you non-self that you're not in control, and

397
00:26:54,620 --> 00:26:56,000
 it's frustrating.

398
00:26:56,000 --> 00:27:00,410
 And that frustration and the exploration, there's no quick

399
00:27:00,410 --> 00:27:01,000
 way.

400
00:27:01,000 --> 00:27:04,780
 You have to study that, that distraction. You have to be

401
00:27:04,780 --> 00:27:06,000
 meticulous about it,

402
00:27:06,000 --> 00:27:09,420
 and catch it every time again and again, and stop trying to

403
00:27:09,420 --> 00:27:10,000
 fix it.

404
00:27:10,000 --> 00:27:15,000
 Stop trying to stop it from happening.

405
00:27:15,000 --> 00:27:19,990
 How it stops happening is when you let go of that which is

406
00:27:19,990 --> 00:27:21,000
 causing it.

407
00:27:21,000 --> 00:27:26,220
 So some kind of, potentially some kind of attachment or

408
00:27:26,220 --> 00:27:30,000
 curiosity about whatever it is that's causing you to drift.

409
00:27:30,000 --> 00:27:35,360
 Sometimes it's just laziness, a desire to just stop working

410
00:27:35,360 --> 00:27:36,000
 so hard.

411
00:27:36,000 --> 00:27:39,030
 So you just go on autopilot, it's more comfortable, that

412
00:27:39,030 --> 00:27:40,000
 kind of thing.

413
00:27:40,000 --> 00:27:45,700
 So look at those states. Learn about them. Once you see

414
00:27:45,700 --> 00:27:51,000
 them clearly, you'll let them go.

415
00:27:51,000 --> 00:27:53,720
 How much solo practice do you recommend before coming to

416
00:27:53,720 --> 00:27:57,000
 your meditation course for the most benefit?

417
00:27:57,000 --> 00:27:59,000
 Is there anything else that should be done beforehand?

418
00:27:59,000 --> 00:28:01,680
 Well, the best would be to do an online course if you have

419
00:28:01,680 --> 00:28:05,000
 time to spend some time.

420
00:28:05,000 --> 00:28:08,000
 We meet once a week, and we have a schedule on this site.

421
00:28:08,000 --> 00:28:10,750
 If you look and find it under the menu, you can reserve a

422
00:28:10,750 --> 00:28:15,000
 spot and then just call me up using Google Hangouts,

423
00:28:15,000 --> 00:28:17,000
 and we can talk every week.

424
00:28:17,000 --> 00:28:23,000
 Practicing solo, it's only the first exercise.

425
00:28:23,000 --> 00:28:26,400
 What I give in the booklet is only the first step in this

426
00:28:26,400 --> 00:28:27,000
 practice.

427
00:28:27,000 --> 00:28:30,000
 So it's more fruitful generally to actually do a course,

428
00:28:30,000 --> 00:28:32,000
 and we can do that online.

429
00:28:32,000 --> 00:28:37,000
 But other than that, there's no...

430
00:28:37,000 --> 00:28:40,220
 Other than that, come and do a course as soon as you can,

431
00:28:40,220 --> 00:28:44,810
 because the course is the best way to gain a good

432
00:28:44,810 --> 00:28:47,000
 foundation.

433
00:28:47,000 --> 00:28:51,960
 Should I go into a meditation center with having severe

434
00:28:51,960 --> 00:28:56,000
 stiffness and tension in my back and neck?

435
00:28:56,000 --> 00:28:58,000
 I mean, yes.

436
00:28:58,000 --> 00:29:03,000
 To find a meditation center that can accommodate that.

437
00:29:03,000 --> 00:29:06,150
 That gives you a chair or allows you to sit in a chair or

438
00:29:06,150 --> 00:29:09,710
 even lie down or sit against the wall or that kind of thing

439
00:29:09,710 --> 00:29:10,000
.

440
00:29:10,000 --> 00:29:12,000
 The body position is not that important.

441
00:29:12,000 --> 00:29:17,000
 It's much more important what's going on in the mind.

442
00:29:17,000 --> 00:29:21,050
 We have a woman here right now who has muscular dystrophy,

443
00:29:21,050 --> 00:29:25,000
 and she uses a walker to do her walking.

444
00:29:25,000 --> 00:29:27,000
 I said, "Dore, I'm going to use her as an example."

445
00:29:27,000 --> 00:29:29,560
 My teacher would always drag us out, the foreigners, and

446
00:29:29,560 --> 00:29:31,000
 say, "See if they can do it."

447
00:29:31,000 --> 00:29:34,000
 These people from other countries come here to do it.

448
00:29:34,000 --> 00:29:37,000
 All Yutai people can do it as well.

449
00:29:37,000 --> 00:29:40,000
 So I'll use her as an example.

450
00:29:40,000 --> 00:29:43,000
 I'd like to have her come here and talk about her practice.

451
00:29:43,000 --> 00:29:44,000
 But she's done great.

452
00:29:44,000 --> 00:29:50,000
 Finish the first course onto the second course.

453
00:29:50,000 --> 00:29:54,000
 If she can do it, you can.

454
00:29:54,000 --> 00:29:56,000
 This talk you mentioned a psychopath shoots you.

455
00:29:56,000 --> 00:29:58,720
 A psychopathy is a result of past karmic actions, a result

456
00:29:58,720 --> 00:30:01,000
 of unfortunate injury.

457
00:30:01,000 --> 00:30:03,000
 I'm not sure. I don't really know.

458
00:30:03,000 --> 00:30:06,000
 Psychopathy is just a word.

459
00:30:06,000 --> 00:30:08,000
 They say a person who has no feelings.

460
00:30:08,000 --> 00:30:11,000
 There's been studies that show, I did a little research,

461
00:30:11,000 --> 00:30:13,000
 that they actually do have feelings.

462
00:30:13,000 --> 00:30:18,170
 They're just repressed or the brain is in such a way that

463
00:30:18,170 --> 00:30:22,000
 it's very weak and that kind of thing.

464
00:30:22,000 --> 00:30:24,000
 What is psychopathy? I don't know.

465
00:30:24,000 --> 00:30:27,300
 I'd imagine there are various things that are labeled by

466
00:30:27,300 --> 00:30:36,000
 psychiatrists or psychotherapists as psychopathy.

467
00:30:36,000 --> 00:30:40,000
 They differentiate from psychosis and neurosis.

468
00:30:40,000 --> 00:30:44,000
 Psychosis is thought to be that which is biological.

469
00:30:44,000 --> 00:30:46,000
 It's a problem with the brain.

470
00:30:46,000 --> 00:30:49,000
 Neurosis is something mental, something that's learned.

471
00:30:49,000 --> 00:30:51,000
 So they try to differentiate.

472
00:30:51,000 --> 00:30:56,000
 I would say it's a combination, right?

473
00:30:56,000 --> 00:30:59,990
 Anyway, it's a fairly speculative question, so I wouldn't

474
00:30:59,990 --> 00:31:02,000
 worry about it too much.

475
00:31:02,000 --> 00:31:05,000
 "Cancel talking. Deer Park due to study group on Friday."

476
00:31:05,000 --> 00:31:07,000
 Is this today Friday?

477
00:31:07,000 --> 00:31:09,000
 Oh, are we supposed to have a meeting today?

478
00:31:09,000 --> 00:31:12,000
 Our study group today?

479
00:31:12,000 --> 00:31:16,000
 Wait, what's our schedule?

480
00:31:16,000 --> 00:31:18,000
 Did I miss study group today? I thought Sunday.

481
00:31:18,000 --> 00:31:21,000
 We're going to do one more Sunday?

482
00:31:21,000 --> 00:31:24,000
 I didn't even know it was Friday.

483
00:31:24,000 --> 00:31:29,000
 There you go, living in the present.

484
00:31:29,000 --> 00:31:33,130
 It's a problem with the problem for keeping appointments, I

485
00:31:33,130 --> 00:31:34,000
 suppose.

486
00:31:34,000 --> 00:31:36,620
 Well, you guys better let me know what our schedule for

487
00:31:36,620 --> 00:31:37,000
 that is.

488
00:31:37,000 --> 00:31:45,000
 Otherwise, I guess from now on, no more Fridays.

489
00:31:45,000 --> 00:31:49,000
 Oh.

490
00:31:49,000 --> 00:31:51,000
 They're supposed to be with Suddhi Maga.

491
00:31:51,000 --> 00:31:53,000
 All right, so Sunday.

492
00:31:53,000 --> 00:31:55,400
 Sunday was our last one, and then it was switching to

493
00:31:55,400 --> 00:31:56,000
 Friday.

494
00:31:56,000 --> 00:32:00,000
 I'm sorry.

495
00:32:00,000 --> 00:32:03,000
 No, let's do next Friday then take another break.

496
00:32:03,000 --> 00:32:07,000
 Sorry.

497
00:32:07,000 --> 00:32:11,000
 "How to deal with sub-perceptible sensations."

498
00:32:11,000 --> 00:32:16,380
 "It feels like I have a lot crammed down from poor habits

499
00:32:16,380 --> 00:32:18,000
 in the past."

500
00:32:18,000 --> 00:32:21,000
 I don't understand.

501
00:32:21,000 --> 00:32:23,000
 "Patience." I wouldn't worry about it.

502
00:32:23,000 --> 00:32:26,000
 "Whatever arises, be mindful of that."

503
00:32:26,000 --> 00:32:29,000
 "Pachupadanchayodhamang."

504
00:32:29,000 --> 00:32:43,000
 I'm really sorry about the Suddhi Maga. I'm clearly not

505
00:32:43,000 --> 00:32:45,000
 very good at appointments.

506
00:32:45,000 --> 00:32:49,660
 Yeah, I think that's enough for tonight, so try again next

507
00:32:49,660 --> 00:32:51,000
 week.

508
00:32:51,000 --> 00:32:54,000
 Apologies.

509
00:32:54,000 --> 00:32:56,000
 Have a good night, everyone.

