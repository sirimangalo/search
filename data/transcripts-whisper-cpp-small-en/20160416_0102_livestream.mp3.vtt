WEBVTT

00:00:00.000 --> 00:00:07.000
 Well, good evening everyone.

00:00:07.000 --> 00:00:17.200
 We're broadcasting live from New York City, April 15th,

00:00:17.200 --> 00:00:19.000
 2016.

00:00:19.000 --> 00:00:39.000
 Today is quote,

00:00:39.000 --> 00:00:46.250
 "The fool may be known by his deans, likewise the wise one

00:00:46.250 --> 00:00:50.000
 may be known by his deans."

00:00:50.000 --> 00:00:55.000
 Wisdom is manifested by one's deeds.

00:00:55.000 --> 00:01:04.030
 Now, I'm assuming the word "deans" is a translation of

00:01:04.030 --> 00:01:06.000
 karma or kamma.

00:01:06.000 --> 00:01:12.840
 And so you have to be somewhat careful about what is being

00:01:12.840 --> 00:01:14.000
 said here.

00:01:14.000 --> 00:01:18.000
 The Buddha isn't actually saying physical deeds.

00:01:18.000 --> 00:01:26.920
 He is saying their mental volition, the reasoning behind

00:01:26.920 --> 00:01:31.000
 everything they do and say,

00:01:31.000 --> 00:01:37.000
 "The intention."

00:01:37.000 --> 00:01:48.020
 Because people can do great things, fools can do good deeds

00:01:48.020 --> 00:01:51.000
.

00:01:51.000 --> 00:01:54.740
 You can't know the difference unless you're able to tell

00:01:54.740 --> 00:01:57.000
 the intentions behind their deeds.

00:01:57.000 --> 00:02:02.820
 So a fool might be charitable and helpful to others, but

00:02:02.820 --> 00:02:08.000
 they may do it for selfish reasons.

00:02:08.000 --> 00:02:11.000
 That's a foolish thing to do.

00:02:11.000 --> 00:02:23.000
 You couldn't say that person is wise just because they're

00:02:23.000 --> 00:02:25.000
 doing good deeds now.

00:02:25.000 --> 00:02:33.820
 It is a good sign if a person is doing and saying good

00:02:33.820 --> 00:02:36.000
 things often.

00:02:36.000 --> 00:02:40.000
 It's a good sign that they probably have good intentions.

00:02:40.000 --> 00:02:49.000
 But as we've seen before with some of the teachings,

00:02:49.000 --> 00:02:53.260
 it's possible for a person to think one thing, say one

00:02:53.260 --> 00:03:03.000
 thing, and do something else.

00:03:03.000 --> 00:03:10.000
 You have to be somewhat circumspect.

00:03:10.000 --> 00:03:17.100
 But what he's saying here is if someone says they're

00:03:17.100 --> 00:03:25.100
 enlightened or says they are able to spout the Buddha's

00:03:25.100 --> 00:03:26.000
 teachings,

00:03:26.000 --> 00:03:31.110
 the talk is cheap as soon as whether they're actually

00:03:31.110 --> 00:03:33.000
 practicing.

00:03:33.000 --> 00:03:37.980
 But if they're not moral, if they're not focused, if they

00:03:37.980 --> 00:03:45.390
're not seeing clearly, that's how you know them to be

00:03:45.390 --> 00:03:47.000
 foolish.

00:03:47.000 --> 00:03:54.000
 Even though they talk a good game, the talk is cheap.

00:03:54.000 --> 00:03:58.000
 It's much easier to teach others, much easier to...

00:03:58.000 --> 00:04:04.460
 It's a skill. To teach is a skill. But once you've learned

00:04:04.460 --> 00:04:07.000
 it, that's all it takes you can teach.

00:04:07.000 --> 00:04:19.000
 But to actually practice, this is the challenge.

00:04:19.000 --> 00:04:22.000
 So I'm happy to be here in New York.

00:04:22.000 --> 00:04:28.630
 I'm going to have several sessions that are mainly practice

00:04:28.630 --> 00:04:32.000
 space, which is the best.

00:04:32.000 --> 00:04:35.000
 There won't just be a lot of talk.

00:04:35.000 --> 00:04:41.070
 There will be talking about meditation and teaching

00:04:41.070 --> 00:04:47.000
 meditation and practicing meditation together, hopefully.

00:04:47.000 --> 00:05:04.000
 [Hindi]

00:05:04.000 --> 00:05:14.440
 It is karma that is our deans that divides us up, divides

00:05:14.440 --> 00:05:17.000
 up beings.

00:05:17.000 --> 00:05:24.540
 So the Buddha said the most important division between all

00:05:24.540 --> 00:05:29.000
 beings, not just humans, wasn't race.

00:05:29.000 --> 00:05:35.940
 It isn't, of course, social status. It isn't color of the

00:05:35.940 --> 00:05:37.000
 skin.

00:05:37.000 --> 00:05:42.000
 It isn't education.

00:05:42.000 --> 00:05:49.000
 It isn't gender. It isn't size. It's kam.

00:05:49.000 --> 00:05:56.190
 Kam mang satin. But the tiyatitangi nang panita taya, that

00:05:56.190 --> 00:06:00.000
 makes us refined and coarse.

00:06:00.000 --> 00:06:07.320
 So in the history of humanity, for example, we've often had

00:06:07.320 --> 00:06:18.000
 the idea that certain groups of humans are more refined.

00:06:18.000 --> 00:06:22.000
 I like that.

00:06:22.000 --> 00:06:28.000
 What was it? The Romans.

00:06:28.000 --> 00:06:35.360
 The Romans thought all these other groups of people were

00:06:35.360 --> 00:06:37.000
 barbarians.

00:06:37.000 --> 00:06:44.070
 The Europeans who went to study Indian religion, they

00:06:44.070 --> 00:06:54.000
 thought these Indians were, and they found savages.

00:06:54.000 --> 00:06:58.030
 And so they thought, they were quite surprised to learn

00:06:58.030 --> 00:07:02.140
 about the quite sophisticated religions of India, because

00:07:02.140 --> 00:07:06.000
 people were living like savages.

00:07:06.000 --> 00:07:09.000
 And they thought these people are coarse.

00:07:09.000 --> 00:07:17.000
 And so they had this theory that Asia has been in decline.

00:07:17.000 --> 00:07:22.000
 It used to be a refined place, but it's become savage.

00:07:22.000 --> 00:07:25.840
 The humans do this. The Asians, of course, like in Thailand

00:07:25.840 --> 00:07:32.000
, they think of Western Europeans as barbarians and savages.

00:07:32.000 --> 00:07:40.000
 It's all across the board.

00:07:40.000 --> 00:07:43.520
 The Buddha said, well, this is industrialization,

00:07:43.520 --> 00:07:47.000
 modernization, sophistication, all these things.

00:07:47.000 --> 00:07:51.000
 This is not how you call someone refined, of course.

00:07:51.000 --> 00:07:59.000
 Refined, of course, is in our actions, our ethics.

00:07:59.000 --> 00:08:03.000
 Are we performing ethical actions? Are we ethical?

00:08:03.000 --> 00:08:07.000
 Or are we unethical, basically?

00:08:07.000 --> 00:08:16.000
 The quality of our behavior.

00:08:16.000 --> 00:08:23.370
 It's why meditation is such a great thing, because

00:08:23.370 --> 00:08:30.000
 meditation is the purest form of behavior. It cultivates

00:08:30.000 --> 00:08:30.000
 such wholesome habits in the mind and thereby in the body.

00:08:30.000 --> 00:08:34.000
 It's an activity that is totally pure.

00:08:34.000 --> 00:08:40.850
 If you practice it regularly, it's like injecting this dose

00:08:40.850 --> 00:08:44.000
 of purity into your life.

00:08:44.000 --> 00:08:53.500
 It's kind of surprising, I think, that when you begin and

00:08:53.500 --> 00:09:00.160
 you wonder, well, how is this helping me, walking back and

00:09:00.160 --> 00:09:02.000
 forth and sitting down?

00:09:02.000 --> 00:09:05.980
 It's surprising how it does have an effect. I think in the

00:09:05.980 --> 00:09:09.870
 beginning it's quite confusing as to, well, why is this

00:09:09.870 --> 00:09:14.000
 doing such wonders to my surprising how it's changing?

00:09:14.000 --> 00:09:21.500
 Because it's pure. We underestimate the power of a pure

00:09:21.500 --> 00:09:25.000
 thought, of a pure mind.

00:09:25.000 --> 00:09:28.830
 When our mind is clear, it's not just clear in that moment,

00:09:28.830 --> 00:09:32.410
 it's creating clarity of mind, it's influencing, it's

00:09:32.410 --> 00:09:36.000
 impacting our life. This is karma.

00:09:36.000 --> 00:09:41.000
 It affects us, changes us.

00:09:41.000 --> 00:09:50.240
 It's like spending time washing your, cleaning your house

00:09:50.240 --> 00:09:55.800
 or washing your body. It has an effect and then you're pure

00:09:55.800 --> 00:09:56.000
.

00:09:56.000 --> 00:10:03.590
 The pure in the mind, which is of course far more value

00:10:03.590 --> 00:10:09.000
 than a pure body or a clean house.

00:10:09.000 --> 00:10:14.950
 So meditation is the real test. Can someone meditate if

00:10:14.950 --> 00:10:21.160
 they're able to meditate and if they are able to see the

00:10:21.160 --> 00:10:23.000
 benefits of meditation?

00:10:23.000 --> 00:10:27.970
 That's a test of whether they're refined. It's the best

00:10:27.970 --> 00:10:29.000
 test.

00:10:29.000 --> 00:10:36.840
 How well they're able to accept and appreciate an activity

00:10:36.840 --> 00:10:43.880
 that is pure as opposed to being obsessed with impure

00:10:43.880 --> 00:10:48.000
 behaviors and addictions.

00:10:48.000 --> 00:10:52.340
 Anyway, I'll try to come on for a few minutes every night

00:10:52.340 --> 00:10:56.210
 just to say hello. It's good to see everyone's still med

00:10:56.210 --> 00:10:57.000
itating.

00:10:57.000 --> 00:11:00.000
 I'm going to do my meditation right now, I think.

