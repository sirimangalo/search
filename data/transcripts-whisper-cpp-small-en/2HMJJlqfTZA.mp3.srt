1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:17,320
 Welcome to our evening Dhamma session.

3
00:00:17,320 --> 00:00:23,270
 So on Sunday we had a big celebration of the Buddha's

4
00:00:23,270 --> 00:00:27,400
 birthday in Mississauga.

5
00:00:27,400 --> 00:00:31,400
 And it's quite impressive every year all the different

6
00:00:31,400 --> 00:00:35,400
 Buddhist communities come out and

7
00:00:35,400 --> 00:00:46,200
 put on costumes, traditional costumes and have a parade and

8
00:00:46,200 --> 00:00:52,600
 chanting and cultural displays,

9
00:00:52,600 --> 00:01:03,670
 tents, tents, displaying, promoting all the many different

10
00:01:03,670 --> 00:01:07,680
 Buddhist organizations around.

11
00:01:07,680 --> 00:01:13,890
 And so last year I submitted a cultural display or an

12
00:01:13,890 --> 00:01:18,000
 exhibition, part of the show.

13
00:01:18,000 --> 00:01:25,050
 And I just put down meditation because it seemed to me that

14
00:01:25,050 --> 00:01:27,640
 as I said when I got up

15
00:01:27,640 --> 00:01:30,870
 on stage I forgot about it and when I finally saw the

16
00:01:30,870 --> 00:01:33,200
 program I saw that I was in the program

17
00:01:33,200 --> 00:01:37,480
 I thought there'd been a mistake.

18
00:01:37,480 --> 00:01:44,130
 So after all these dancing groups and here I was in line,

19
00:01:44,130 --> 00:01:47,360
 got up on stage, sat there

20
00:01:47,360 --> 00:01:50,710
 in front of oh it was maybe a thousand, couple thousand

21
00:01:50,710 --> 00:01:53,040
 people, maybe a thousand probably

22
00:01:53,040 --> 00:01:57,720
 not more, maybe less.

23
00:01:57,720 --> 00:02:04,840
 And I just taught a little meditation.

24
00:02:04,840 --> 00:02:09,560
 What I said was you know when the Buddha passed away,

25
00:02:09,560 --> 00:02:12,840
 before he passed away all the angels

26
00:02:12,840 --> 00:02:20,790
 from all over the universe came down and brought with them

27
00:02:20,790 --> 00:02:24,160
 flowers from heaven.

28
00:02:24,160 --> 00:02:28,000
 No, heavenly flowers are not like human, like flowers from

29
00:02:28,000 --> 00:02:29,280
 the human world.

30
00:02:29,280 --> 00:02:33,290
 Their beauty is of course beyond anything that we have here

31
00:02:33,290 --> 00:02:35,000
 and they scattered them

32
00:02:35,000 --> 00:02:36,280
 throughout the forest.

33
00:02:36,280 --> 00:02:41,760
 This is what the legends say.

34
00:02:41,760 --> 00:02:46,220
 And the Buddha saw this and he thought to himself, well he

35
00:02:46,220 --> 00:02:48,400
 said Duhananda, he said this

36
00:02:48,400 --> 00:02:55,310
 isn't how you honor someone, this isn't how you honor a

37
00:02:55,310 --> 00:02:56,760
 Buddha.

38
00:02:56,760 --> 00:02:59,920
 And the commentary, there's this one commentary that says

39
00:02:59,920 --> 00:03:01,720
 what the Buddha was thinking was

40
00:03:01,720 --> 00:03:17,000
 for as long as my fourfold followers, so monks and nuns and

41
00:03:17,000 --> 00:03:24,440
 laymen and laywomen, as long

42
00:03:24,440 --> 00:03:34,600
 as they pay homage to me through the homage of practice.

43
00:03:34,600 --> 00:03:55,350
 Dawa mama saasanangna pa madhte bunat chand do yi wa wiroj

44
00:03:55,350 --> 00:03:56,800
isati.

45
00:03:56,800 --> 00:04:03,850
 We continue to shine just like the full moon in the middle

46
00:04:03,850 --> 00:04:12,000
 of the sky shines on the earth.

47
00:04:12,000 --> 00:04:18,310
 So when we talk about paying homage to the Buddha, which is

48
00:04:18,310 --> 00:04:21,360
 something we talk about and

49
00:04:21,360 --> 00:04:29,120
 something we think about, there's many reasons for that.

50
00:04:29,120 --> 00:04:33,330
 One is simply because of what a beautiful and pure person

51
00:04:33,330 --> 00:04:34,080
 he was.

52
00:04:34,080 --> 00:04:39,010
 If you have a chance to read his teachings or even read

53
00:04:39,010 --> 00:04:42,800
 about his life, it's quite remarkable

54
00:04:42,800 --> 00:04:49,560
 the depth of his pronunciation, his compassion, his

55
00:04:49,560 --> 00:04:54,640
 dedication to truth, his dedication to

56
00:04:54,640 --> 00:05:03,200
 peace, to bringing peace to the earth.

57
00:05:03,200 --> 00:05:08,860
 But mostly we pay respect because through our practice we

58
00:05:08,860 --> 00:05:11,680
've come to appreciate what

59
00:05:11,680 --> 00:05:19,280
 he's given to us, what he's done for us.

60
00:05:19,280 --> 00:05:24,240
 And by paying homage to him it's such an affirmation of

61
00:05:24,240 --> 00:05:30,160
 what a good thing we've found, this expression

62
00:05:30,160 --> 00:05:32,680
 of gratitude.

63
00:05:32,680 --> 00:05:36,310
 And so of course, as with any famous person, there's always

64
00:05:36,310 --> 00:05:38,080
 many, many people who come

65
00:05:38,080 --> 00:05:42,660
 and pay homage to him and the Buddha was concerned that

66
00:05:42,660 --> 00:05:46,000
 before he passed away people should be

67
00:05:46,000 --> 00:05:51,720
 clear that it wasn't through flowers or candles or incense

68
00:05:51,720 --> 00:05:54,960
 or bowing down or doing chanting

69
00:05:54,960 --> 00:06:01,720
 or any of this that one truly pays homage to him.

70
00:06:01,720 --> 00:06:05,770
 He said through practicing the Dhamma, whatever person,

71
00:06:05,770 --> 00:06:08,040
 doesn't matter if they're a monk,

72
00:06:08,040 --> 00:06:13,610
 or a nun, or an ordinary man, ordinary woman, whoever

73
00:06:13,610 --> 00:06:17,040
 practices the Dhamma in line with

74
00:06:17,040 --> 00:06:21,770
 the Dhamma, to realize the Dhamma, such a person pays the

75
00:06:21,770 --> 00:06:24,320
 proper respect and homage to

76
00:06:24,320 --> 00:06:27,320
 the Buddha.

77
00:06:27,320 --> 00:06:34,370
 There was a story of this monk who liked to look at the

78
00:06:34,370 --> 00:06:35,920
 Buddha.

79
00:06:35,920 --> 00:06:40,360
 He liked to follow the Buddha around and was just in great

80
00:06:40,360 --> 00:06:42,680
 bliss because the Buddha was

81
00:06:42,680 --> 00:06:46,080
 quite beautiful apparently.

82
00:06:46,080 --> 00:06:50,810
 So this monk, Vakali, he liked to walk around after the

83
00:06:50,810 --> 00:06:53,840
 Buddha and wherever the Buddha was

84
00:06:53,840 --> 00:06:57,240
 he would just sit there and stare at the Buddha.

85
00:06:57,240 --> 00:07:03,140
 He was kind of infatuated or, well, he was very happy to

86
00:07:03,140 --> 00:07:05,120
 see the Buddha.

87
00:07:05,120 --> 00:07:07,320
 I gave him great bliss.

88
00:07:07,320 --> 00:07:09,240
 The Buddha thought, "Well, this isn't the way.

89
00:07:09,240 --> 00:07:10,960
 He's not going to become enlightened this way."

90
00:07:10,960 --> 00:07:15,640
 So the Buddha said to him, famous words, he said, "Yokovak

91
00:07:15,640 --> 00:07:17,560
ali dhammang pasati, soul mang

92
00:07:17,560 --> 00:07:20,320
 pasati," or something like that.

93
00:07:20,320 --> 00:07:23,920
 I can't remember the exact following.

94
00:07:23,920 --> 00:07:26,720
 Whoever sees the Dhamma sees me.

95
00:07:26,720 --> 00:07:34,000
 That's basically what he said.

96
00:07:34,000 --> 00:07:39,360
 So it always comes back to the Dhamma or the Dharma.

97
00:07:39,360 --> 00:07:41,560
 This is what I talked about on Sunday.

98
00:07:41,560 --> 00:07:46,040
 I said, "I'd like to share with you a little bit of Dharma,

99
00:07:46,040 --> 00:07:48,200
 five dharmas for us to reflect

100
00:07:48,200 --> 00:07:49,200
 upon."

101
00:07:49,200 --> 00:07:52,740
 I think this is a really useful thing for all of us to hear

102
00:07:52,740 --> 00:07:54,360
 and those of you who are

103
00:07:54,360 --> 00:07:57,580
 coming to the end of your course, as well as those of you

104
00:07:57,580 --> 00:07:59,480
 who have maybe never practiced

105
00:07:59,480 --> 00:08:01,980
 meditation.

106
00:08:01,980 --> 00:08:03,880
 Remember what it is we're practicing.

107
00:08:03,880 --> 00:08:05,960
 What is the Dharma?

108
00:08:05,960 --> 00:08:08,480
 The Dharma is reality.

109
00:08:08,480 --> 00:08:14,760
 Dharma, in this sense, means something that really exists.

110
00:08:14,760 --> 00:08:16,880
 The Dharma means to hold.

111
00:08:16,880 --> 00:08:22,580
 So it holds an existence, unlike imagination, which doesn't

112
00:08:22,580 --> 00:08:25,160
 actually hold anything.

113
00:08:25,160 --> 00:08:31,440
 You think about a dragon or you think about the Easter

114
00:08:31,440 --> 00:08:32,640
 Bunny.

115
00:08:32,640 --> 00:08:35,160
 They don't hold any reality.

116
00:08:35,160 --> 00:08:38,440
 They don't hold up in reality.

117
00:08:38,440 --> 00:08:41,040
 Dharma, this is the word.

118
00:08:41,040 --> 00:08:45,280
 Whereas a Dharma is truly real.

119
00:08:45,280 --> 00:08:46,280
 Real in what sense?

120
00:08:46,280 --> 00:08:48,920
 Well, real in the sense that you can experience it.

121
00:08:48,920 --> 00:08:52,920
 What are the dharmas, these five dharmas?

122
00:08:52,920 --> 00:08:56,030
 They're adapted from the four satipatanas, but let's just

123
00:08:56,030 --> 00:08:57,480
 talk about them as the five

124
00:08:57,480 --> 00:09:00,160
 dharmas.

125
00:09:00,160 --> 00:09:02,520
 The first one is the body.

126
00:09:02,520 --> 00:09:03,520
 So you can try this.

127
00:09:03,520 --> 00:09:07,840
 Close your eyes and reflect on your body.

128
00:09:07,840 --> 00:09:15,640
 There is physical reality.

129
00:09:15,640 --> 00:09:18,840
 There is the hardness and the softness of the seat that you

130
00:09:18,840 --> 00:09:19,920
're sitting on.

131
00:09:19,920 --> 00:09:21,800
 That's a feeling.

132
00:09:21,800 --> 00:09:24,240
 There's the heat and the cold and the room around you.

133
00:09:24,240 --> 00:09:28,720
 There's tension in your back and in your legs.

134
00:09:28,720 --> 00:09:33,120
 There's tension in your head.

135
00:09:33,120 --> 00:09:35,520
 This is all physical.

136
00:09:35,520 --> 00:09:39,510
 So the Buddha taught us to be mindful if you're sitting to

137
00:09:39,510 --> 00:09:42,480
 say to yourself, sitting, sitting,

138
00:09:42,480 --> 00:09:47,240
 standing, say standing, standing or walking, walking, lying

139
00:09:47,240 --> 00:09:49,520
, lying, or whoever your body

140
00:09:49,520 --> 00:09:56,000
 is deported.

141
00:09:56,000 --> 00:10:03,170
 When we do this, we practice meditation in order to stay

142
00:10:03,170 --> 00:10:05,880
 close to reality.

143
00:10:05,880 --> 00:10:10,310
 As we do that, our minds are going to stay with the body

144
00:10:10,310 --> 00:10:12,920
 and you're going to learn about

145
00:10:12,920 --> 00:10:13,920
 reality.

146
00:10:13,920 --> 00:10:16,840
 It sounds kind of banal, I think.

147
00:10:16,840 --> 00:10:19,500
 If you've never practiced meditation, you don't realize the

148
00:10:19,500 --> 00:10:21,680
 significance of doing that.

149
00:10:21,680 --> 00:10:25,600
 First of all, how challenging it is, but second of all, how

150
00:10:25,600 --> 00:10:27,800
 important that challenge is.

151
00:10:27,800 --> 00:10:33,980
 How much you learn about yourself when you do just that,

152
00:10:33,980 --> 00:10:37,080
 when you look at the body.

153
00:10:37,080 --> 00:10:39,410
 Because with the body, you see all these other realities,

154
00:10:39,410 --> 00:10:39,840
 right?

155
00:10:39,840 --> 00:10:41,920
 The second reality is the feelings.

156
00:10:41,920 --> 00:10:46,620
 So you start to see pain and you'll see happiness, so you

157
00:10:46,620 --> 00:10:47,960
'll see calm.

158
00:10:47,960 --> 00:10:53,000
 Some people feel very calm when they meditate.

159
00:10:53,000 --> 00:10:56,280
 Some people feel a lot of pain when they meditate.

160
00:10:56,280 --> 00:10:58,560
 You'll see all this.

161
00:10:58,560 --> 00:11:03,670
 And pain like the body or feelings, happiness like the body

162
00:11:03,670 --> 00:11:06,680
, all these feelings, they teach

163
00:11:06,680 --> 00:11:08,600
 you about yourself.

164
00:11:08,600 --> 00:11:10,600
 You get to see how you react to feelings.

165
00:11:10,600 --> 00:11:19,000
 You get to see how you interact with feelings.

166
00:11:19,000 --> 00:11:22,630
 When you focus on the pain and say pain, pain, you're

167
00:11:22,630 --> 00:11:25,360
 learning about feelings and therefore

168
00:11:25,360 --> 00:11:39,460
 you're learning about yourself and how you relate to the

169
00:11:39,460 --> 00:11:42,520
 feeling.

170
00:11:42,520 --> 00:11:47,640
 The third reality is the mind.

171
00:11:47,640 --> 00:11:50,360
 As you focus on the body and the feelings, you're also

172
00:11:50,360 --> 00:11:53,240
 going to notice that you're thinking.

173
00:11:53,240 --> 00:11:56,410
 You'll notice your thoughts more clearly, thoughts about

174
00:11:56,410 --> 00:11:58,080
 the past or future or thoughts

175
00:11:58,080 --> 00:12:02,040
 about the present or thoughts of imagination.

176
00:12:02,040 --> 00:12:03,440
 You'll see them clearly.

177
00:12:03,440 --> 00:12:09,370
 You'll be able to watch them, you'll be able to learn about

178
00:12:09,370 --> 00:12:10,320
 them.

179
00:12:10,320 --> 00:12:12,640
 What do we think about?

180
00:12:12,640 --> 00:12:14,760
 How often do we think?

181
00:12:14,760 --> 00:12:17,520
 How long do we think?

182
00:12:17,520 --> 00:12:20,940
 How obsessed do we get in thoughts?

183
00:12:20,940 --> 00:12:22,520
 How we react to our thoughts?

184
00:12:22,520 --> 00:12:25,520
 How we react to our memories?

185
00:12:25,520 --> 00:12:29,530
 All the memories that come up when you meditate, how we

186
00:12:29,530 --> 00:12:30,880
 react to them.

187
00:12:30,880 --> 00:12:32,240
 You'll start to see this.

188
00:12:32,240 --> 00:12:39,240
 You'll see relationships between the dharmas.

189
00:12:39,240 --> 00:12:45,510
 The fourth reality is our reactions, our emotions, not just

190
00:12:45,510 --> 00:12:49,080
 our reactions but our mind state.

191
00:12:49,080 --> 00:12:54,360
 So liking and disliking is our reactions.

192
00:12:54,360 --> 00:13:02,680
 Abortum, sadness, fear, depression, frustration.

193
00:13:02,680 --> 00:13:12,570
 On the other side, desire, wishfulness, loneliness, pining

194
00:13:12,570 --> 00:13:15,680
 away over things.

195
00:13:15,680 --> 00:13:19,430
 And then there's drowsiness and distraction and doubt and

196
00:13:19,430 --> 00:13:21,280
 confusion and worry and all

197
00:13:21,280 --> 00:13:22,280
 those.

198
00:13:22,280 --> 00:13:24,720
 And these are all real.

199
00:13:24,720 --> 00:13:28,320
 These are very important.

200
00:13:28,320 --> 00:13:31,830
 Most remarkable thing about the meditation is how it works

201
00:13:31,830 --> 00:13:33,400
 to free you from these.

202
00:13:33,400 --> 00:13:37,240
 How it works to sort out your emotions, sort out your mind

203
00:13:37,240 --> 00:13:40,320
 states so that if you're worried

204
00:13:40,320 --> 00:13:45,050
 and if you get good and learn the ins and outs of your mind

205
00:13:45,050 --> 00:13:47,560
, just saying to yourself,

206
00:13:47,560 --> 00:13:50,610
 worried, worried, and thinking about, acknowledging the

207
00:13:50,610 --> 00:13:52,600
 thinking about whatever is making you

208
00:13:52,600 --> 00:13:57,920
 worried and so on, you work it out.

209
00:13:57,920 --> 00:14:02,270
 You're able to free yourself from the worry, from the

210
00:14:02,270 --> 00:14:05,680
 stress, from boredom, sadness, fear,

211
00:14:05,680 --> 00:14:14,570
 depression, even addiction, just by being mindful, not even

212
00:14:14,570 --> 00:14:18,600
 by trying to fix anything.

213
00:14:18,600 --> 00:14:23,950
 The greatness of reality is that when you see it, when you

214
00:14:23,950 --> 00:14:26,520
 see it, you free yourself

215
00:14:26,520 --> 00:14:28,200
 from the power it has over you.

216
00:14:28,200 --> 00:14:33,140
 You free yourself from all the suffering that comes from

217
00:14:33,140 --> 00:14:34,480
 ignorance.

218
00:14:34,480 --> 00:14:36,880
 What's really the major quality of ignorance is that it

219
00:14:36,880 --> 00:14:37,880
 makes you suffer.

220
00:14:37,880 --> 00:14:41,000
 Because of course no one wants to suffer, so if you

221
00:14:41,000 --> 00:14:43,280
 actually knew the truth, you would

222
00:14:43,280 --> 00:14:45,280
 never suffer.

223
00:14:45,280 --> 00:14:47,840
 It's the brilliance of it.

224
00:14:47,840 --> 00:14:51,480
 Instead, no one else makes us suffer.

225
00:14:51,480 --> 00:14:57,840
 We cause ourselves suffering.

226
00:14:57,840 --> 00:15:05,830
 And the fifth dharma is the senses, seeing, hearing,

227
00:15:05,830 --> 00:15:10,840
 smelling, tasting, feeling.

228
00:15:10,840 --> 00:15:13,720
 This is the outside, the external.

229
00:15:13,720 --> 00:15:19,280
 The rest of them are mostly internal, but the doors to the

230
00:15:19,280 --> 00:15:22,000
 external are the senses.

231
00:15:22,000 --> 00:15:24,720
 So this is where we easily get lost.

232
00:15:24,720 --> 00:15:28,990
 As soon as you open your eyes, your mind is leaping out

233
00:15:28,990 --> 00:15:31,400
 through your eyes into all the

234
00:15:31,400 --> 00:15:36,120
 things that you experience.

235
00:15:36,120 --> 00:15:40,780
 So we use mindfulness as a guard, mindfulness as a door

236
00:15:40,780 --> 00:15:41,760
keeper.

237
00:15:41,760 --> 00:15:46,860
 And you see, say to yourself, seeing, seeing, hearing,

238
00:15:46,860 --> 00:15:50,720
 hearing, smelling, smelling, tasting,

239
00:15:50,720 --> 00:15:59,560
 and so on.

240
00:15:59,560 --> 00:16:03,960
 And you learn about the reality of your own experience.

241
00:16:03,960 --> 00:16:07,000
 You learn everything you need to know about the universe.

242
00:16:07,000 --> 00:16:09,920
 It's quite remarkable.

243
00:16:09,920 --> 00:16:17,720
 Yodham mangvasati, soul mangvasati.

244
00:16:17,720 --> 00:16:21,880
 You see the Buddha.

245
00:16:21,880 --> 00:16:34,080
 You come to see, see nibhana, see the Buddha.

246
00:16:34,080 --> 00:16:39,650
 And you pay the highest homage, the highest tribute, the

247
00:16:39,650 --> 00:16:41,680
 highest respect.

248
00:16:41,680 --> 00:16:45,940
 I mean, what's I suppose most important about that is that

249
00:16:45,940 --> 00:16:48,080
 you feel reassured that people

250
00:16:48,080 --> 00:16:51,630
 like the Buddha are really, this is, you know, people as

251
00:16:51,630 --> 00:16:53,920
 wise as that, praise that what you're

252
00:16:53,920 --> 00:16:57,120
 doing now.

253
00:16:57,120 --> 00:17:05,840
 When you feel kind of low about yourself or you're feeling

254
00:17:05,840 --> 00:17:10,320
 inadequate, you can, you can

255
00:17:10,320 --> 00:17:16,320
 be reassured and proud or confident, encouraged by the fact

256
00:17:16,320 --> 00:17:19,880
 that what you're doing was praised

257
00:17:19,880 --> 00:17:20,880
 by the Buddha.

258
00:17:20,880 --> 00:17:24,640
 What you're doing is what the Buddha taught us to do, not

259
00:17:24,640 --> 00:17:27,160
 to do all these religious ceremonies

260
00:17:27,160 --> 00:17:30,240
 with flowers and candles and incense.

261
00:17:30,240 --> 00:17:32,790
 That's all fine, but it's not really, well, it's not at all

262
00:17:32,790 --> 00:17:33,920
 what the Buddha taught us

263
00:17:33,920 --> 00:17:34,920
 to do.

264
00:17:34,920 --> 00:17:40,350
 Buddha said the best way to pay respect is through the dham

265
00:17:40,350 --> 00:17:42,600
ma and the best way to be

266
00:17:42,600 --> 00:17:49,000
 sure you're doing the right thing.

267
00:17:49,000 --> 00:17:53,160
 After I gave the talk, someone came from one of the Chinese

268
00:17:53,160 --> 00:17:55,360
 monasteries, I think, and he

269
00:17:55,360 --> 00:17:56,360
 invited me.

270
00:17:56,360 --> 00:17:58,920
 They want me to go to their monastery and teach meditation.

271
00:17:58,920 --> 00:18:01,400
 And he said, I learned so much.

272
00:18:01,400 --> 00:18:04,650
 I was only up there for less than five minutes, I think,

273
00:18:04,650 --> 00:18:06,040
 maybe five minutes.

274
00:18:06,040 --> 00:18:07,400
 And he said, oh, I learned so much.

275
00:18:07,400 --> 00:18:10,120
 It was quite interesting.

276
00:18:10,120 --> 00:18:12,160
 The dhamma is not something hard to teach.

277
00:18:12,160 --> 00:18:14,760
 It's not hard, something hard to understand.

278
00:18:14,760 --> 00:18:20,100
 But we get, we get lost and sidetracked and, you know, so

279
00:18:20,100 --> 00:18:22,800
 much of what we call Buddhism

280
00:18:22,800 --> 00:18:27,120
 is just fluff.

281
00:18:27,120 --> 00:18:30,530
 I think it's really great, you know, by doing this and we

282
00:18:30,530 --> 00:18:32,240
 set up a meditation tent.

283
00:18:32,240 --> 00:18:33,900
 Now there's copycats.

284
00:18:33,900 --> 00:18:38,120
 People are stealing the idea or with competition.

285
00:18:38,120 --> 00:18:40,530
 There's apparently another, there was on Sunday, there was

286
00:18:40,530 --> 00:18:42,000
 another meditation tent teaching

287
00:18:42,000 --> 00:18:44,280
 loving kindness.

288
00:18:44,280 --> 00:18:46,510
 Apparently meditations and all the monasteries are now

289
00:18:46,510 --> 00:18:48,240
 calling themselves meditation centers.

290
00:18:48,240 --> 00:18:49,240
 I'm not the only one.

291
00:18:49,240 --> 00:18:54,400
 It's not copying me, but there's been several moves.

292
00:18:54,400 --> 00:18:59,400
 It's picking up the pace.

293
00:18:59,400 --> 00:19:02,640
 More and more centers practicing mindfulness and everybody

294
00:19:02,640 --> 00:19:04,160
 else scrambling to get into

295
00:19:04,160 --> 00:19:06,800
 it.

296
00:19:06,800 --> 00:19:09,880
 All because really all because the Buddha was so clear.

297
00:19:09,880 --> 00:19:14,580
 I think right before he passed away, the fact that he was

298
00:19:14,580 --> 00:19:17,520
 so clear about this very important

299
00:19:17,520 --> 00:19:22,920
 because of the greatness of what he taught and how we can

300
00:19:22,920 --> 00:19:25,640
 still realize it today.

301
00:19:25,640 --> 00:19:26,640
 So there you go.

302
00:19:26,640 --> 00:19:29,360
 A little bit of Dharma for tonight.

303
00:19:29,360 --> 00:19:30,360
 Thank you all for tuning in.

304
00:19:30,360 --> 00:19:30,360
 Have a good night.

305
00:19:30,360 --> 00:19:32,120
 1

306
00:19:32,120 --> 00:19:33,120
 1

