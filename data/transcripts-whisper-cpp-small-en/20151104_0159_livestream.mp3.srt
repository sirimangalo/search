1
00:00:00,000 --> 00:00:20,500
 Test. Test. Okay. So now I'm recording to the audio stream

2
00:00:20,500 --> 00:00:22,300
 with this special means.

3
00:00:22,300 --> 00:00:29,540
 Can you talk, Robin? Sure. Say something for us. Sure. Let

4
00:00:29,540 --> 00:00:32,320
 me, I'll read the quotes.

5
00:00:32,320 --> 00:00:35,990
 I'll have something to say. Venerable Tisa, the nephew of

6
00:00:35,990 --> 00:00:37,640
 the Lord's Father, came to the Lord

7
00:00:37,640 --> 00:00:40,570
 crying. Then the Lord said to him, "What is wrong, Tisa?

8
00:00:40,570 --> 00:00:42,600
 Why do you stand beside me sad,

9
00:00:42,600 --> 00:00:45,880
 downcast and with tears running down your face?" So now I'm

10
00:00:45,880 --> 00:00:47,120
 recording. Lord, it is because all the

11
00:00:47,120 --> 00:00:50,860
 monks have been jeering at and mocking me. With this

12
00:00:50,860 --> 00:00:53,640
 special means. Can you talk, Robin? Does that

13
00:00:53,640 --> 00:00:58,580
 seem to be working okay? Say something for us. Sure. Let me

14
00:00:58,580 --> 00:01:02,360
, I'll read the quotes. I'll have something to

15
00:01:02,360 --> 00:01:07,050
 say. Venerable Tisa. Yeah, that worked really well. I'm

16
00:01:07,050 --> 00:01:13,920
 impressed. What did you do, Bante? Same as that last

17
00:01:13,920 --> 00:01:16,520
 time. Remember I tried something special last time to get

18
00:01:16,520 --> 00:01:18,600
 us both in the same stream and it was just,

19
00:01:18,600 --> 00:01:21,360
 the sound was really poor quality, but now the sound is

20
00:01:21,360 --> 00:01:24,080
 really good quality. So I'm gonna cancel

21
00:01:24,080 --> 00:01:24,800
 that test.

