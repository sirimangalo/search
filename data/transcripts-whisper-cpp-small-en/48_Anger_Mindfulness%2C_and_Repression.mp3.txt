 Anger, mindfulness, and repression.
 Question.
 When I experience a situation where I feel angry and try to
 be mindful,
 I feel that I am repressing it by saying angry, angry, and
 trying to investigate it.
 Afterwards, I feel frustrated.
 Am I doing something wrong in the practice?
 Am I catching the thought too late or am I still not
 advanced enough?
 Answer.
 From what I've seen, the reason why people feel like they
 are repressing anything by
 acknowledging or noting is because strong emotions like
 anger are associated with a
 painful feeling.
 Anger will often create physical suffering like headaches,
 tension, and pain throughout
 the body.
 When you say to yourself angry, angry, the anger disappears
 in the mind, but you are
 still left with the unpleasant feelings which are actually
 a result of the anger, not a result
 of mindfulness.
 Another example of this is when you are distracted,
 thinking about many things.
 When you finally realize you have been distracted in, say,
 thinking, thinking, or distracted,
 distracted, if you notice a headache from the brain
 activity you might think, "Oh,
 being mindful causes headaches," but that is not the case.
 It is actually the excessive thinking that causes the
 headaches.
 It can also happen that when you say angry, angry, you say
 it with anger or frustration,
 so instead of being mindful and having a wholesome state of
 mind, you are still biased and clinging
 to the experience.
 It can also happen that you do not have faith in the
 practice, so you note with doubt, you
 say angry, angry, but you are not focusing with confidence
 on the experience.
 You might just say the words and hope something good
 happens like magic.
 Finally, it can also happen that you are practicing
 correctly, but your mind is not working correctly.
 You are doing everything right, but your mind is not
 cooperating, which means you try to
 be mindful and you cannot be mindful.
 You try to catch something and you catch it too late.
 You try to clearly see something and it is already gone,
 and as a result of these shortcomings,
 you get frustrated.
 The truth is that this sort of experience is a sign that
 you are seeing clearly, seeing
 impermanence that even your own mind is unpredictable.
 Sometimes you can be mindful and sometimes you cannot,
 seeing suffering as your mind
 is not the way you want it to be and seeing non-self as you
 cannot control your own mind.
 While you are familiar with these qualities of reality, you
 may be susceptible to reactions
 like frustration.
 Those reactions are the problem, not the inability to make
 your experience stable, satisfying,
 and controllable.
 Impermanence, suffering, and non-self is the unavoidable
 nature of reality, but we do not
 like reality, we cannot bear it, we cannot bear imperman
ence, we cannot bear suffering,
 and we cannot bear non-self.
 Even just watching your stomach rise and fall may lead to
 frustration.
 The experience might feel pleasant and peaceful at first,
 but when it changes you think, "What
 am I doing wrong?"
 Then you try again and again to make the experience
 pleasant and peaceful, and you become frustrated
 thinking, "I cannot do this, this is useless."
 But the truth is, you are doing it, the problem is not the
 experience of impermanence, suffering,
 and non-self, the problem is the frustration.
 When frustration arises in the mind, you must focus on the
 frustration, saying to yourself,
 "Frostrated, frustrated."
 If you focus on it objectively, you will see that just like
 everything else, it disappears
 in its own time.
 Every time you get frustrated, it is a chance for you to
 build patience, and patience is
 an integral part of what we are trying to cultivate.
 Patience is associated with wisdom.
 True patience means just seeing things as they are and not
 getting upset about them
 when they come or go.
 But patience and the ability to see moment to moment
 experiences as they are.
 Your frustration about the meditation is stopping you from
 doing this.
 The frustration will hinder that ability to see the moment
 to moment experiences.
 Frustration will hinder it, desire will hinder it, all
 kinds of delusion will hinder it.
 All of our judgments about our practice will stop us from
 cultivating it.
 If people think the practice is not working for them, this
 is just wrong view and wrong
 thought.
 I have heard people tell me that they have been stuck in
 meditation for five or even
 ten years.
 When someone feels like they are stuck, the first thing for
 them to get rid of is this
 idea that they could possibly be stuck because there is no
 you and there is no stuck.
 There is only an experience and it is amazing that people
 could waste five years thinking
 that they are stuck when in fact at any moment you can
 become enlightened if you are clearly
 aware of the moment.
 All we are trying to do is cultivate that clear awareness
 and frustration and any kind
 of judgment about your practice will stop you from doing
 that.
 You are seeing the truth, you are seeing impermanence,
 suffering and non-self.
 It is the frustration that is the problem which is coming
 from your inability to accept
 those very important aspects of reality.
 Once you can accept them and you can see that that is the
 truth of life and this is the
 path to enlightenment, you will see that "es samagov sudya"
 this is the path to purification.
 Thank you.
 Thank you.
