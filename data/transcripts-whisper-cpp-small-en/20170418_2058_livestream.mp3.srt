1
00:00:00,000 --> 00:00:04,500
 Okay, good evening everyone.

2
00:00:04,500 --> 00:00:13,840
 Back after a week away.

3
00:00:13,840 --> 00:00:24,320
 Buddhism we talk about refuge.

4
00:00:24,320 --> 00:00:34,440
 It's a concept that makes sense in the context of suffering

5
00:00:34,440 --> 00:00:41,440
 and stress and seeking out happiness,

6
00:00:41,440 --> 00:00:42,440
 peace.

7
00:00:42,440 --> 00:00:47,720
 When we think about the suffering that exists in the world

8
00:00:47,720 --> 00:00:50,840
 and the potential from the danger

9
00:00:50,840 --> 00:01:03,420
 that we face, the idea of a refuge of course plays a part

10
00:01:03,420 --> 00:01:06,680
 in the conversation.

11
00:01:06,680 --> 00:01:10,390
 And so we see as a refuge people go many places, meant to

12
00:01:10,390 --> 00:01:11,520
 many things.

13
00:01:11,520 --> 00:01:16,080
 God is a common refuge.

14
00:01:16,080 --> 00:01:17,080
 Parents.

15
00:01:17,080 --> 00:01:18,760
 Parents and God.

16
00:01:18,760 --> 00:01:23,400
 It's the same sort of thing, something outside of yourself.

17
00:01:23,400 --> 00:01:27,800
 A power that is greater than you.

18
00:01:27,800 --> 00:01:31,960
 Because it's greater than you it offers greater protection.

19
00:01:31,960 --> 00:01:42,090
 You can rely upon it to help you deal with things that are

20
00:01:42,090 --> 00:01:46,160
 beyond your power.

21
00:01:46,160 --> 00:01:50,680
 We seek out refuge in our possessions, our houses, our...

22
00:01:50,680 --> 00:02:00,040
 We seek out refuge in our families, our loved ones.

23
00:02:00,040 --> 00:02:03,960
 We even seek out refuge in our minds.

24
00:02:03,960 --> 00:02:07,280
 Or someone say from our minds, but we seek out refuge by

25
00:02:07,280 --> 00:02:09,240
 directing our minds in certain

26
00:02:09,240 --> 00:02:10,240
 ways.

27
00:02:10,240 --> 00:02:15,250
 We seek out refuge, patterns of behavior that keep us from

28
00:02:15,250 --> 00:02:18,720
 having to confront our problems,

29
00:02:18,720 --> 00:02:25,960
 our refuge for us to escape.

30
00:02:25,960 --> 00:02:32,000
 But there's a pattern with all of these kinds of refuge.

31
00:02:32,000 --> 00:02:34,720
 First of all they don't last.

32
00:02:34,720 --> 00:02:39,680
 They're either ineffectual or their effects are ephemeral.

33
00:02:39,680 --> 00:02:42,940
 And more importantly they cultivate in us a sense of

34
00:02:42,940 --> 00:02:43,920
 avoidance.

35
00:02:43,920 --> 00:02:48,620
 If you rely on others to solve your problems or to protect

36
00:02:48,620 --> 00:02:51,000
 you from danger then you're

37
00:02:51,000 --> 00:02:53,360
 always going to be dependent on them.

38
00:02:53,360 --> 00:02:59,230
 You're dependent on things that are outside of your control

39
00:02:59,230 --> 00:02:59,600
.

40
00:02:59,600 --> 00:03:02,120
 When those things don't go as planned there's

41
00:03:02,120 --> 00:03:04,880
 disappointment, there's other suffering.

42
00:03:04,880 --> 00:03:17,840
 You're met with that which you didn't, which you tried to

43
00:03:17,840 --> 00:03:20,080
 avoid.

44
00:03:20,080 --> 00:03:29,920
 We all have our ways of dealing with stress.

45
00:03:29,920 --> 00:03:32,920
 This is part of what meditation and Buddhism is about.

46
00:03:32,920 --> 00:03:37,080
 It's about seeing our habits, the ways we deal with things.

47
00:03:37,080 --> 00:03:43,120
 It's part of our reactions to our experience.

48
00:03:43,120 --> 00:03:47,630
 How we try and fix our problems and to see what we're doing

49
00:03:47,630 --> 00:03:49,800
 wrong, to help ourselves

50
00:03:49,800 --> 00:04:04,040
 see the outcome and change when our solutions are found to

51
00:04:04,040 --> 00:04:07,680
 be wanting.

52
00:04:07,680 --> 00:04:12,600
 So whether it's finding a refuge in someone else or in some

53
00:04:12,600 --> 00:04:14,840
 spiritual entity or being

54
00:04:14,840 --> 00:04:19,680
 or teaching, whether it's finding refuge in your mind or

55
00:04:19,680 --> 00:04:22,160
 from your mind by avoiding your

56
00:04:22,160 --> 00:04:38,400
 problems or trying to fix them or finding ways to deal with

57
00:04:38,400 --> 00:04:40,840
 them.

58
00:04:40,840 --> 00:04:43,240
 This is what we learn about in the minute.

59
00:04:43,240 --> 00:04:44,920
 This is one of the big things that we learn.

60
00:04:44,920 --> 00:04:46,920
 So, thank you.

