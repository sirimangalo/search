1
00:00:00,000 --> 00:00:03,600
 Should we ever explore things intentionally that arise

2
00:00:03,600 --> 00:00:07,000
 naturally during the meditation, contrary to simply

3
00:00:07,000 --> 00:00:08,000
 observing?

4
00:00:08,000 --> 00:00:20,000
 I think it can be at times useful.

5
00:00:21,000 --> 00:00:24,350
 I think the times I can think of where it's useful is when

6
00:00:24,350 --> 00:00:28,080
 you're just banging your head against a wall and it feels

7
00:00:28,080 --> 00:00:31,950
 like you're not getting anywhere and the meditation is not

8
00:00:31,950 --> 00:00:33,000
 progressing.

9
00:00:33,000 --> 00:00:42,000
 I think it can be useful, you know, conventionally useful

10
00:00:42,000 --> 00:00:50,000
 to spend some time, a little bit of time sort of figuring

11
00:00:50,000 --> 00:00:50,000
 out

12
00:00:50,000 --> 00:00:57,770
 what you're doing wrong. This is called "wimangsa" which is

13
00:00:57,770 --> 00:01:00,900
, well, in one sense anyway, it refers to this sort of

14
00:01:00,900 --> 00:01:03,000
 stepping back and taking a look at things.

15
00:01:03,000 --> 00:01:08,170
 But I think that's the only point where I would recommend

16
00:01:08,170 --> 00:01:09,000
 this.

17
00:01:11,000 --> 00:01:14,180
 You know, explore things intentionally, you're kind of

18
00:01:14,180 --> 00:01:17,500
 misleading with that terminology because we are exploring

19
00:01:17,500 --> 00:01:20,400
 things, but the usage of that word is that the verbiage

20
00:01:20,400 --> 00:01:22,000
 there is kind of sneaky.

21
00:01:22,000 --> 00:01:25,730
 You know, I'm not blame, you're not criticizing exactly,

22
00:01:25,730 --> 00:01:28,820
 but you have to be careful that you're not tricking

23
00:01:28,820 --> 00:01:32,200
 yourself by using that word into backing up your sort of

24
00:01:32,200 --> 00:01:33,000
 argument.

25
00:01:34,000 --> 00:01:38,400
 Because you're not talking about exploring, you're talking

26
00:01:38,400 --> 00:01:46,000
 about, I mean, I don't know, exploring could go either way,

27
00:01:46,000 --> 00:01:49,160
 it's a fairly neutral word, but your implication is that

28
00:01:49,160 --> 00:02:03,000
 you want to sort of prod and delve, you know, which sort of

29
00:02:03,000 --> 00:02:03,000
,

30
00:02:03,000 --> 00:02:08,060
 from our point of view, implies leaving the middle way

31
00:02:08,060 --> 00:02:12,850
 because the middle way would be something a little bit less

32
00:02:12,850 --> 00:02:16,220
 than what you're suggesting, but it still could be called

33
00:02:16,220 --> 00:02:17,000
 exploration.

34
00:02:18,000 --> 00:02:24,320
 So what we're doing is an exploration and it's intentional.

35
00:02:24,320 --> 00:02:30,020
 It's just not prodding in the sense of going beyond a

36
00:02:30,020 --> 00:02:34,000
 simple awareness of things as they are.

37
00:02:34,000 --> 00:02:37,090
 What we're doing, as I said in the last question, is

38
00:02:37,090 --> 00:02:40,920
 observing. We're conducting a science experiment and we're

39
00:02:40,920 --> 00:02:43,000
 going to record our observations.

40
00:02:44,000 --> 00:02:47,960
 And in this science experiment, as with a perfect science

41
00:02:47,960 --> 00:02:51,660
 experiment, the conclusions will be obvious from the

42
00:02:51,660 --> 00:02:55,950
 observations. You don't come, you don't interpret the

43
00:02:55,950 --> 00:02:59,000
 observations beyond what is obvious.

44
00:03:00,000 --> 00:03:04,060
 So that's a perfect science experiment where you don't have

45
00:03:04,060 --> 00:03:07,980
 to interpret, where anyone who looks at the data can come

46
00:03:07,980 --> 00:03:12,650
 to the same conclusions. So that is what happens in

47
00:03:12,650 --> 00:03:16,000
 meditation, the conclusions come naturally.

48
00:03:17,000 --> 00:03:21,730
 And all that's important is the proper observation. I think

49
00:03:21,730 --> 00:03:25,240
 this question comes up because people feel that it's not

50
00:03:25,240 --> 00:03:30,420
 enough. They're not satisfied by simply observing. And so

51
00:03:30,420 --> 00:03:33,000
 they're looking for something more.

52
00:03:34,000 --> 00:03:41,820
 That is caused by something completely different. That's

53
00:03:41,820 --> 00:03:49,950
 caused by ambition and impatience, frustration, boredom,

54
00:03:49,950 --> 00:03:53,000
 all of these things.

55
00:03:54,000 --> 00:04:02,380
 And this sort of preconception that meditation has to be

56
00:04:02,380 --> 00:04:12,170
 more than simply observing. A lack of confidence or trust

57
00:04:12,170 --> 00:04:15,900
 and the inability to conceive of the fact that simply

58
00:04:15,900 --> 00:04:18,000
 observing things is going to get you the results.

59
00:04:19,000 --> 00:04:22,470
 It's hard to believe that by doing something so silly as

60
00:04:22,470 --> 00:04:26,320
 just reminding yourself, this is this, this is this, that

61
00:04:26,320 --> 00:04:29,000
 that's actually going to do anything.

62
00:04:29,000 --> 00:04:33,140
 Most people are like, "Well, I know everything about myself

63
00:04:33,140 --> 00:04:37,650
 already. I mean, how many years I've been watching myself?

64
00:04:37,650 --> 00:04:40,000
 What could I possibly learn?"

65
00:04:41,000 --> 00:04:44,060
 So there's this one teacher in Bangkok, some of you may

66
00:04:44,060 --> 00:04:48,200
 have heard me say this, he says, he told a 95 year old

67
00:04:48,200 --> 00:04:52,000
 woman, I said, "So, you know yourself pretty well, right?"

68
00:04:52,000 --> 00:04:56,170
 He said, "Yeah, you know your heart well, you know your

69
00:04:56,170 --> 00:04:59,530
 hand." And she said, "My hand, you know quite well. I've

70
00:04:59,530 --> 00:05:01,000
 used my hand all my life."

71
00:05:02,000 --> 00:05:06,120
 And he said, "How many knuckles do you have in one hand?

72
00:05:06,120 --> 00:05:10,000
 Don't count. Tell me how many knuckles do you have?"

73
00:05:10,000 --> 00:05:15,410
 Couldn't do it. She couldn't do it. No idea. I don't even

74
00:05:15,410 --> 00:05:19,000
 remember. It's 14, 15, something like that.

75
00:05:22,000 --> 00:05:24,780
 Just kind of as a funny joke, but our minds are very much

76
00:05:24,780 --> 00:05:27,870
 like that. You know, of course we've been spending so much

77
00:05:27,870 --> 00:05:31,000
 time with our minds, but we're not really investigating.

78
00:05:31,000 --> 00:05:34,630
 And it actually is something quite simple. And the answers

79
00:05:34,630 --> 00:05:39,000
 are quite obvious. It's amazing that we never look.

80
00:05:39,000 --> 00:05:42,520
 It's incredible, really incredible that we spent all this

81
00:05:42,520 --> 00:05:46,040
 time and didn't really see any of this. We were never

82
00:05:46,040 --> 00:05:49,000
 looking. Never trying to understand.

83
00:05:50,000 --> 00:05:52,310
 Or maybe we gave up too soon. We didn't have the tools to

84
00:05:52,310 --> 00:05:54,850
 understand. I suppose there always was a time where we

85
00:05:54,850 --> 00:05:56,000
 wanted to understand.

86
00:05:56,000 --> 00:06:00,470
 We just never really had the tools or a clarity necessary

87
00:06:00,470 --> 00:06:04,650
 to think of it as a science experiment where you need

88
00:06:04,650 --> 00:06:07,000
 objective observation.

89
00:06:07,000 --> 00:06:10,710
 You can't just think, you know, question what is the answer

90
00:06:10,710 --> 00:06:14,000
. You skip the most important part and that's observing.

91
00:06:17,000 --> 00:06:20,300
 But here that's exactly what's being asked here. Should you

92
00:06:20,300 --> 00:06:25,000
 do more than observe? No. Science doesn't work that way.

93
00:06:25,000 --> 00:06:29,840
 And there's a reason why it doesn't. Anything else that you

94
00:06:29,840 --> 00:06:33,000
 do is manipulating the experiment.

95
00:06:33,000 --> 00:06:37,060
 That's why you use double blinds and everything is to avoid

96
00:06:37,060 --> 00:06:40,000
 all that. Anyway.

