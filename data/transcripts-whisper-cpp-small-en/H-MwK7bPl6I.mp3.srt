1
00:00:00,000 --> 00:00:05,180
 Hello. We are happy to announce the Digital Poly Reader is

2
00:00:05,180 --> 00:00:08,000
 completed and ready to use in your web browser.

3
00:00:08,000 --> 00:00:11,000
 To start, go to digitalpolyreader.online.

4
00:00:11,000 --> 00:00:18,480
 The Digital Poly Reader, originally created by Bontayutad

5
00:00:18,480 --> 00:00:22,290
amo, is a tool that includes the polycanon and related

6
00:00:22,290 --> 00:00:24,180
 scriptures and has multiple interactive dictionaries to

7
00:00:24,180 --> 00:00:26,000
 facilitate reading.

8
00:00:26,000 --> 00:00:28,470
 It enables immersive study of the tepidica and the poly

9
00:00:28,470 --> 00:00:31,000
 language at an advanced level.

10
00:00:31,000 --> 00:00:34,660
 The DPR is used by Buddhist monks, scholars at universities

11
00:00:34,660 --> 00:00:38,000
 around the world, and lay people for study at home.

12
00:00:38,000 --> 00:00:41,120
 This is the welcome screen. Here you'll see the daily

13
00:00:41,120 --> 00:00:44,380
 Buddha Vatshana, which is a daily passage and can be a nice

14
00:00:44,380 --> 00:00:47,000
 way to jump right into a text if you want.

15
00:00:47,000 --> 00:00:50,000
 This button opens the sidebar.

16
00:00:50,000 --> 00:00:54,290
 In the sidebar, you'll see three main tabs, navigation,

17
00:00:54,290 --> 00:00:56,000
 search, and tools.

18
00:00:56,000 --> 00:01:00,120
 In navigation, there are two ways to navigate to texts.

19
00:01:00,120 --> 00:01:02,000
 There are quick links and the hierarchy.

20
00:01:02,000 --> 00:01:05,130
 With quick links, you can type in shorthand notation to

21
00:01:05,130 --> 00:01:08,920
 find texts. This info box shows you how to use this

22
00:01:08,920 --> 00:01:10,000
 notation.

23
00:01:10,000 --> 00:01:14,860
 So, for example, if we type "mn13", it opens up Majimana K

24
00:01:14,860 --> 00:01:16,000
aya 13.

25
00:01:16,000 --> 00:01:20,510
 You'll notice the cookie notification. You can accept this

26
00:01:20,510 --> 00:01:23,000
 to remember your preferences.

27
00:01:23,000 --> 00:01:26,070
 With the hierarchy, we can explore how the text is divided

28
00:01:26,070 --> 00:01:30,150
 into sections and subsections. You can select a set and

29
00:01:30,150 --> 00:01:31,000
 book.

30
00:01:31,000 --> 00:01:34,850
 The MAT buttons allow you to select Mula, the root text, At

31
00:01:34,850 --> 00:01:39,000
akata, the commentary, or Tikka, the sub-commentary.

32
00:01:39,000 --> 00:01:42,000
 Here is the book we've selected.

33
00:01:42,000 --> 00:01:44,940
 If you click on it, the index of the book and all its

34
00:01:44,940 --> 00:01:47,000
 subsections will be displayed.

35
00:01:47,000 --> 00:01:53,060
 If you click this button, all the subsections will be

36
00:01:53,060 --> 00:01:58,000
 written out in full combined on one page.

37
00:01:58,000 --> 00:02:01,890
 Each of these drop-downs contains the subsections of the

38
00:02:01,890 --> 00:02:04,000
 section selected above it.

39
00:02:04,000 --> 00:02:07,680
 Just like before, you can click these buttons on the right

40
00:02:07,680 --> 00:02:11,350
 to display all the subsections of a section combined on one

41
00:02:11,350 --> 00:02:12,000
 page.

42
00:02:12,000 --> 00:02:15,850
 To view the individual section selected at the bottom of

43
00:02:15,850 --> 00:02:18,000
 the hierarchy, click here.

44
00:02:18,000 --> 00:02:25,090
 Here are the history and bookmark menus. You can press the

45
00:02:25,090 --> 00:02:30,000
 B key to make bookmarks.

46
00:02:30,000 --> 00:02:33,440
 When we click on a word in the text, it sends it to the

47
00:02:33,440 --> 00:02:36,000
 bottom pane into the DPR analysis.

48
00:02:36,000 --> 00:02:39,000
 The analyzed word is broken into parts.

49
00:02:39,000 --> 00:02:42,330
 Words found in the Polytech Society's Poly to English

50
00:02:42,330 --> 00:02:45,000
 Dictionary, or PED, are shown in red.

51
00:02:45,000 --> 00:02:49,000
 The PED definition is shown in this space below.

52
00:02:49,000 --> 00:02:52,250
 The blue words are from the Concise Poly to English

53
00:02:52,250 --> 00:02:54,000
 Dictionary, or CPED.

54
00:02:54,000 --> 00:03:00,720
 Words from the dictionary of poly proper names are linked

55
00:03:00,720 --> 00:03:03,000
 to with a green N.

56
00:03:03,000 --> 00:03:10,740
 Numbers underneath the PED words indicate multiple

57
00:03:10,740 --> 00:03:15,000
 definitions of that word.

58
00:03:15,000 --> 00:03:18,800
 This word breakdown menu lets you choose different ways to

59
00:03:18,800 --> 00:03:22,360
 break up the analyzed word so you can find the breakdown

60
00:03:22,360 --> 00:03:24,000
 you are looking for.

61
00:03:24,000 --> 00:03:28,010
 The C button shows paradigm charts for conjugation of verbs

62
00:03:28,010 --> 00:03:31,000
 and declension of nouns and adjectives.

63
00:03:31,000 --> 00:03:34,700
 The analyzed word will appear red in the chart if it is in

64
00:03:34,700 --> 00:03:36,000
 that paradigm.

65
00:03:36,000 --> 00:03:41,220
 The pencil button allows you to edit the spelling of the

66
00:03:41,220 --> 00:03:43,000
 analyzed word.

67
00:03:43,000 --> 00:03:47,850
 These arrows go to the next or previous word alphabetically

68
00:03:47,850 --> 00:03:49,000
 in the PED.

69
00:03:49,000 --> 00:03:54,000
 History shows the words you have looked at.

70
00:03:54,000 --> 00:03:57,460
 These side buttons open up this dictionary pane, a

71
00:03:57,460 --> 00:04:02,090
 conversion or transliteration tool, a text pad, a

72
00:04:02,090 --> 00:04:07,000
 translation tool, and a conjugator.

73
00:04:07,000 --> 00:04:11,000
 This button opens up the context menu.

74
00:04:11,000 --> 00:04:13,800
 The context menu gives you quick access to various features

75
00:04:13,800 --> 00:04:14,000
.

76
00:04:14,000 --> 00:04:18,050
 For example, you can go to the next or previous section of

77
00:04:18,050 --> 00:04:20,000
 text with these arrows.

78
00:04:20,000 --> 00:04:23,000
 This goes to the book's index.

79
00:04:23,000 --> 00:04:28,000
 The M, A, T buttons go to the root text and commentaries.

80
00:04:28,000 --> 00:04:32,310
 You can switch between the Thai and Myanmar/Burmese tepidik

81
00:04:32,310 --> 00:04:35,000
as with the capital M and T buttons.

82
00:04:35,000 --> 00:04:38,650
 Notice variant readings are shown in the text, which can be

83
00:04:38,650 --> 00:04:41,000
 toggled on and off in the settings.

84
00:04:41,000 --> 00:04:46,220
 These icons open side-by-side English translations of the

85
00:04:46,220 --> 00:04:47,000
 text.

86
00:04:47,000 --> 00:04:51,730
 If you click on these diamonds, it sends a permalink to the

87
00:04:51,730 --> 00:04:53,000
 clipboard.

88
00:04:53,000 --> 00:04:57,700
 When you go to the permalink, the chosen portion of text

89
00:04:57,700 --> 00:05:01,000
 will show a green diamond next to it.

90
00:05:01,000 --> 00:05:05,000
 In the Search tab, you can search for words in the texts.

91
00:05:05,000 --> 00:05:09,410
 The search box accepts words typed in Veltuis or Unicode to

92
00:05:09,410 --> 00:05:12,000
 give letters diacritic marks.

93
00:05:12,000 --> 00:05:18,000
 You can look at this info box for help with Veltuis.

94
00:05:18,000 --> 00:05:23,870
 For example, if you type period M, it writes a nighahita or

95
00:05:23,870 --> 00:05:26,000
 the M with a dot under it.

96
00:05:26,000 --> 00:05:30,200
 If you type two As in a row, it writes a long A or the A

97
00:05:30,200 --> 00:05:32,000
 with a bar over it.

98
00:05:32,000 --> 00:05:37,510
 When you click Search, you can stop the search with this

99
00:05:37,510 --> 00:05:39,000
 red button.

100
00:05:39,000 --> 00:05:43,620
 You can click on different results of the search to see

101
00:05:43,620 --> 00:05:47,000
 what texts those terms are found in.

102
00:05:47,000 --> 00:05:51,860
 The Rx box lets you use regular expressions to search for

103
00:05:51,860 --> 00:05:54,000
 patterns of text.

104
00:05:54,000 --> 00:05:56,830
 This dropdown allows you to choose where you want your

105
00:05:56,830 --> 00:05:58,000
 search to take place,

106
00:05:58,000 --> 00:06:02,500
 be it within multiple sets, books in a set, a single book,

107
00:06:02,500 --> 00:06:04,000
 or part of a book.

108
00:06:04,000 --> 00:06:09,000
 The Check selects all or deselects all.

109
00:06:09,000 --> 00:06:12,660
 The Tools tab gives you access to dictionary searches and

110
00:06:12,660 --> 00:06:14,000
 other resources.

111
00:06:14,000 --> 00:06:18,000
 We can type a word into the search box using Veltuis again.

112
00:06:18,000 --> 00:06:22,000
 This dropdown selects what dictionary you will search.

113
00:06:22,000 --> 00:06:26,000
 You can also use the DPR analysis tool here.

114
00:06:26,000 --> 00:06:32,930
 Words not found in any dictionary will appear black in the

115
00:06:32,930 --> 00:06:35,000
 DPR analysis.

116
00:06:35,000 --> 00:06:40,620
 The advanced options include regex again and a fuzzy search

117
00:06:40,620 --> 00:06:41,000
.

118
00:06:41,000 --> 00:06:43,320
 When your search term does not appear in the selected

119
00:06:43,320 --> 00:06:44,000
 dictionary,

120
00:06:44,000 --> 00:06:48,000
 you'll get a few suggestions of what you may have meant.

121
00:06:48,000 --> 00:06:52,000
 When you click fuzzy search, you get many more suggestions

122
00:06:52,000 --> 00:06:56,000
 and approximations of your search term.

123
00:06:56,000 --> 00:07:01,160
 Full text allows you to search for a term that appears

124
00:07:01,160 --> 00:07:11,000
 within dictionary definition entries.

125
00:07:11,000 --> 00:07:17,930
 Start of word only allows you to search for all words that

126
00:07:17,930 --> 00:07:21,000
 start with your term.

127
00:07:21,000 --> 00:07:26,000
 Poly Language Resources includes useful links.

128
00:07:26,000 --> 00:07:30,000
 Then we have the buttons at the top of the sidebar.

129
00:07:30,000 --> 00:07:34,000
 This button brings you to the welcome screen.

130
00:07:34,000 --> 00:07:38,000
 This button allows you to install the DPR for offline use.

131
00:07:38,000 --> 00:07:41,000
 Google Chrome is recommended for offline use.

132
00:07:41,000 --> 00:07:44,730
 Select all the components you wish to download and click OK

133
00:07:44,730 --> 00:07:45,000
.

134
00:07:45,000 --> 00:07:48,000
 You will see a progress bar begin in the sidebar.

135
00:07:48,000 --> 00:07:50,560
 When it's finished, you will be able to use the downloaded

136
00:07:50,560 --> 00:07:54,000
 components without a network connection.

137
00:07:54,000 --> 00:07:58,000
 You may also install the DPR as an app by clicking here.

138
00:07:58,000 --> 00:08:06,000
 This will add a shortcut to your device as well.

139
00:08:06,000 --> 00:08:10,000
 The help button will bring you to this video.

140
00:08:10,000 --> 00:08:12,000
 This button will open up preferences.

141
00:08:12,000 --> 00:08:15,220
 Here you will see general settings, layout, and text

142
00:08:15,220 --> 00:08:16,000
 displays.

143
00:08:16,000 --> 00:08:20,740
 In text script, you can choose to display the text in Roman

144
00:08:20,740 --> 00:08:29,810
, Thai, Devanagari, Burmese, Sinhala, Bangla, or Telugu

145
00:08:29,810 --> 00:08:32,000
 script.

146
00:08:32,000 --> 00:08:36,000
 And this button collapses the sidebar.

147
00:08:36,000 --> 00:08:40,570
 Press the question mark key for a list of keyboard

148
00:08:40,570 --> 00:08:42,000
 shortcuts.

149
00:08:42,000 --> 00:08:45,250
 Press the feedback button if you'd like to contribute

150
00:08:45,250 --> 00:08:49,740
 comments, criticisms, questions, bugs, or anything else you

151
00:08:49,740 --> 00:08:51,000
'd like to share.

152
00:08:51,000 --> 00:08:56,000
 You may also email feedback to digitalpolyreader@gmail.com.

153
00:08:56,000 --> 00:09:00,000
 Thank you for your interest in the Digital Poly Reader.

154
00:09:00,000 --> 00:09:03,020
 Thank you to Bante Utadamo and all the hardworking

155
00:09:03,020 --> 00:09:05,000
 volunteers who have brought us this tool.

156
00:09:05,000 --> 00:09:08,000
 May all beings be happy.

