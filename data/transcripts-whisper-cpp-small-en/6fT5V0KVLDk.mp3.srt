1
00:00:00,000 --> 00:00:05,100
 Miriam says, "I've heard of married monks. Do you think

2
00:00:05,100 --> 00:00:08,000
 people should listen to the talks these monks give?"

3
00:00:08,000 --> 00:00:10,000
 I don't mean marry monks, do you?

4
00:00:10,000 --> 00:00:15,420
 But Tarindu was telling me earlier that he thinks the monks

5
00:00:15,420 --> 00:00:18,000
 at Shrimabodhi are married.

6
00:00:18,000 --> 00:00:24,000
 No, I think at Shrimabodhi it's those rishis, the guys that

7
00:00:24,000 --> 00:00:27,180
 look like monks but have these things under Hindu or

8
00:00:27,180 --> 00:00:28,000
 something.

9
00:00:28,000 --> 00:00:32,190
 I saw some of those in Anuradhapura and they might very

10
00:00:32,190 --> 00:00:34,000
 well be married.

11
00:00:34,000 --> 00:00:38,000
 Can I say that?

12
00:00:38,000 --> 00:00:40,000
 Yeah, please.

13
00:00:40,000 --> 00:00:44,000
 In Zen, you are allowed to marry.

14
00:00:44,000 --> 00:00:48,000
 So I know Japanese monks and they're married.

15
00:00:48,000 --> 00:00:52,000
 They're very good monks so you can't generalize.

16
00:00:54,000 --> 00:00:57,450
 So there are traditions in Buddhism where they are allowed

17
00:00:57,450 --> 00:00:58,000
 to marry.

18
00:00:58,000 --> 00:01:05,000
 But the fact that they're married does kind of take away

19
00:01:05,000 --> 00:01:08,000
 from the whole leaving samsara behind thing.

20
00:01:08,000 --> 00:01:11,000
 Well then you have to question Zen, essentially.

21
00:01:11,000 --> 00:01:15,170
 Yeah, but Zen, sometimes they want to become Buddhas, I don

22
00:01:15,170 --> 00:01:16,000
't know.

23
00:01:16,000 --> 00:01:20,000
 And then they have to go through everything again.

24
00:01:20,000 --> 00:01:24,000
 So it's a whole different. What route are they going? What

25
00:01:24,000 --> 00:01:24,000
 are they on?

26
00:01:24,000 --> 00:01:27,000
 What are they doing that they have become married?

27
00:01:27,000 --> 00:01:30,000
 But it seems that it would dilute the teachings of a life.

28
00:01:30,000 --> 00:01:34,200
 I mean there's this dating service that they have in Japan

29
00:01:34,200 --> 00:01:35,000
 for monks.

30
00:01:35,000 --> 00:01:41,000
 But it seems like it's just because they need more monks.

31
00:01:41,000 --> 00:01:45,000
 So they have babies and the babies become monks.

32
00:01:45,000 --> 00:01:48,000
 That's what the article said.

33
00:01:48,000 --> 00:01:51,550
 But okay, if you want to go there. I mean I know even in

34
00:01:51,550 --> 00:01:53,000
 our tradition there are people who are married.

35
00:01:53,000 --> 00:01:57,410
 S.N. Goenka is married. And he's a very well respected

36
00:01:57,410 --> 00:01:58,000
 person.

37
00:01:58,000 --> 00:02:04,000
 So yeah, I guess the marriage itself doesn't mean much.

38
00:02:04,000 --> 00:02:10,760
 But the whole concept of marrying as a part of Buddhism, I

39
00:02:10,760 --> 00:02:12,000
 think,

40
00:02:12,000 --> 00:02:17,700
 is what's question as diluting the teaching. Because what

41
00:02:17,700 --> 00:02:19,000
 is it to marry someone?

42
00:02:19,000 --> 00:02:23,540
 Is there a function to it? I mean, is there a dharmic

43
00:02:23,540 --> 00:02:25,000
 benefit to marrying?

44
00:02:25,000 --> 00:02:31,000
 Besides seeing the suffering of life.

45
00:02:31,000 --> 00:02:34,000
 Don't want to go into that.

46
00:02:34,000 --> 00:02:38,000
 Right. Well we don't want to go into that example.

47
00:02:38,000 --> 00:02:42,000
 We have a meditator here right now who's...

48
00:02:42,000 --> 00:02:46,000
 We don't want to go into that.

49
00:02:46,000 --> 00:02:51,300
 Well she's thinking of letting go and giving up the whole

50
00:02:51,300 --> 00:02:53,000
 idea of getting married.

51
00:02:53,000 --> 00:02:58,000
 So we were just talking about marriage.

52
00:02:58,000 --> 00:03:01,000
 Yeah, let's stop there.

