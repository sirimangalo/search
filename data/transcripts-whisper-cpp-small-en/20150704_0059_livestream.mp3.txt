 Okay, good evening.
 I'm broadcasting live from Stoney Creek, Ontario, Canada.
 I'm Bailey Dama.
 It's interesting to compare teaching meditation to
 practicing meditation in the context of
 insight meditation. It's clear that these are two different
 skills in many ways. It's possible
 to be a good meditator but lack certain skills that are
 required to be a good teacher. And
 it's possible to be a good teacher and a relatively poor
 meditator. But on the other hand, the
 activity being the same.
 First of all, obviously someone who's never done meditation
 would have a hard time teaching.
 And someone who has practiced extensive meditation would
 have a much easier time and a much clearer
 understanding of meditation. Not to mention how useful the
 benefits of meditation are
 in teaching in general, not just meditation but in relating
 to others.
 And then there's the fact that the activity is the same.
 Meditation is about practicing meditation
 or teaching meditation is both about training the mind.
 Training the mind. In teaching you're
 just training someone else's mind or you're being the inst
igation for the cultivation
 of understanding. So when you sit down to practice
 meditation, your first task is basically
 teaching yourself how to direct the mind. As a teacher, the
 view is the same, it's just
 someone else's mind that you're directing. And this is, I
 think this is useful to talk
 about because it allows us to say that to some extent
 anyway. You can teach yourself
 how to meditate or you can be your own teacher in the
 practice of meditation. If you understand
 the method and the means of the method behind how to direct
 your mind.
 So last week in our study of the Visuddhi Maga we came
 across just a brief passage, actually
 a set of methodology, whatever, means by which one can
 cultivate the mind, cultivate states
 of mind, meditative states of mind. And each one was
 explained. It was in the context of
 the meditation, but it's always been a teaching that stuck
 with me as something that I recognized
 in teaching especially. It made me see that the parallel
 between teaching someone else
 and doing it yourself. It said that the key to being a
 successful meditator is in knowing
 when, knowing when, when for things, when to exert the mind
, when to restrain the mind,
 knowing when to encourage the mind and knowing when to look
 upon the mind with equanimity.
 These are the four means of directing the mind. Important
 because it's clear that we
 shouldn't always engage in each of these. It should know
 when, so when to. It's this
 skill that is necessary both for a teacher in meditation
 and for a meditator. As a teacher
 you have to be alert and in tune with the student's state
 of mind. Are they in need of encouragement
 or are they in need of advice on, should they be told to
 push harder, should they be told
 to relax, restrain the mind, should they be told, should
 they be given encouragement
 or should they just be left alone and looked upon with equ
animity. Clearly all, not all
 of these are useful in the same situation. Just remembering
 these four is useful. Useful
 for a teacher but also quite useful for a meditator. A med
itator remembers these, they
 can adjust their practice themselves. So this is on the
 level of adjusting your practice.
 It's not talking about the actual practice of course. When
 we practice it's simply about
 cultivating mindfulness. But in order to frame that
 practice in the right, in the right light
 or in the right mindset, you have to adjust and so that's
 what this is. So exerting the
 mind means cultivating effort, energy. Cultivating those
 qualities of mind that bring about energy.
 This is when the mind is overly sluggish or intently
 focused and the mind is inflexible.
 The ability to adapt, to keep up with the pace of
 experience, cultivating effort. We have
 to know when to exert the mind. We should exert the mind.
 We should know when we're lacking
 in energy. So we exert the mind when we are overly focused,
 overly concentrated, when we're
 stuck on a single object or when we're not being mindful,
 when we're not really acknowledging
 the object, we're really aware of the object, maybe walking
 back and forth, not really aware.
 Exert our minds in this way. Become aware. This is when we
 cultivate effort, when we cultivate
 investigation, sending the mind to the object. Do we really
 know that we're walking? Do we
 really know that we're lifting the foot and moving the foot
? Do we really know that the
 stomach is rising and falling? Do we have to exert our mind
? Are we making the... we have
 to remind ourselves, rising and falling? Are we really med
itating? Are we just sitting?
 Are we just walking? Conversely, we have to know when to
 restrain the mind. This is when
 the mind is too energetic, too excited, so distracted. We
're thinking about too many
 things and again we're not mindful but for another reason
 we're not mindful because our
 minds are overactive. We have to know when to restrain the
 mind. Focus is a lot like
 the focus of a camera where you have to... it's not about
 going to one extreme or another,
 it's about finding the perfect balance. That's what we do.
 We try to balance the mind.
 If we're too energetic, we have to calm down. So we develop
 those qualities of mind that
 are calming. We still practice mindfulness but we become
 aware that our efforts are too
 energetic and we start to focus on fewer objects or we
 become aware of the overarching state
 of mind to collect our thoughts and keep them from becoming
 scattered. We just say to
 ourselves, scattered, scattered or distracted, or
 overwhelmed, overwhelmed. Our mind becomes
 more focused so it's the same activity but it's a different
 quality. The quality of energy
 when we're too stuck, fixed in the mind. The quality of
 calm and tranquility when our minds
 are too distracted. We have to know when to encourage the
 mind. This is when we're feeling
 very interested, when we have doubts or when we're disincl
ined to meditate, when we're
 disinclined to practice. Putting our full heart into it, it
's clear in the mind as to
 the benefit. It's not clearly of interest to us to meditate
. So here we need to encourage
 ourselves. Sometimes this is not even through meditation,
 this is only through listening
 to the Dhamma or associating with good people. That's why
 teachers are so useful because
 they're good at encouragement, they're good at reminding
 you why you're doing it, they're
 good at being an example of why you should do it. I don't
 know when we need encouragement.
 Sometimes we don't need encouragement and as a teacher, be
 careful not to encourage
 meditators. Sometimes they're overly confident and you have
 to make them question so they
 can see that their confidence may not be well founded.
 Sometimes it leads to wrong practice
 and we think we're doing it right, we just go all out,
 ignore any idea that it might be
 wrong. But certainly one of the teacher's main objectives
 is, for main purposes, is
 to encourage the meditator. But finally, sometimes there
 has to be a certain, it's interesting
 that there's nothing about discouraging the meditator,
 which is interesting but somehow
 rings true, I'm not sure exactly what the intention is but
 practically it really is
 the opposite of encouraging because the teacher actually
 seems unproductive from experience.
 Discouragement seems quite unproductive as a teacher. It
 actually doesn't seem useful
 for a teacher to discourage the meditator, it doesn't seem
 to work very well. The meditator
 is doing wrong. What seems to work incredibly well is
 exactly what is said here, looking
 on with equanimity. Actually in the context of this passage
, looking on with equanimity
 is when things are going well. The meditator is doing well.
 So when everything is going
 fine, the teacher, in this case the meditator, should just
 continue as they're going, should
 not adjust their practice at all. But this works quite well
 when the meditator needs
 reining in, when you yourself need to assess your practice,
 when you're not sure. Equanimity
 is the best means of figuring out what's wrong if anything.
 When you don't know, you don't
 know it's anything wrong. Equanimity allows you to see what
's wrong. As a teacher, the
 interesting thing is when you're a teacher and you're an
 equanimous, when you sit silently
 and listen to a meditator and don't give them the
 encouragement that they're expecting,
 there's sort of the greatest discouragement for wrong
 behavior that you can give. The
 meditator is excited about something that they shouldn't be
 excited about, where it's
 like a dead end. When meditators start freaking out over
 experiences they've had or emotions
 that come up. When they see that the teacher is unfazed by
 it, totally uninterested in
 it, the equanimous towards it, then likewise they treat it
 as something meaningless as
 well and don't get caught up in it. They come and tell the
 teacher what great things came
 for practice and the teacher says, "Oh yeah, were you
 mindful of it?" They say, "Okay,
 well that obviously wasn't enough to surprise me, to excite
 my teacher, so I'll give it
 up." Knowing when to be equanimous, where it's equally
 important, but a text describes
 it as important when things are going well. You should
 never become excited about things
 going well. When everything's going well, that's the time
 to coast, or not to coast,
 to be on cruise control, to cruise. To just continue
 exactly as you've been doing it,
 to not change anything. It's very close to the actual
 meditation practice of not rocking
 the boat, because you want to see what's going on. You're
 going to see when things go wrong.
 As soon as things go wrong, you'll see it if you're impart
ially watching, objectively
 observing your experience. So just a little something to
 think about. These four things
 are a good guideline you want to teach yourself. I think it
's essential that you remember these
 ideas, the idea of when. You should not always be exerting
 yourself, or pushing yourself.
 The whole idea of reprimanding the students coast for
 yourself shouldn't be hard on yourself.
 Most of the time be equanimous. It's only when you see that
 something's going wrong
 that you react by encouraging, by exerting yourself further
, or by restraining your mind,
 or by providing the encouragement that's missing. You're
 doing something wrong if the mind
 is misbehaving. You should just look on with equanimity and
 let go. Don't follow it, don't
 cling to it. Don't become excited by it. Anyway, there are
 some thoughts, some dhamma for tonight.
 Thank you all for tuning in, and be well.
