1
00:00:00,000 --> 00:00:10,000
 [BLANK_AUDIO]

2
00:00:10,000 --> 00:00:20,000
 [BLANK_AUDIO]

3
00:00:20,000 --> 00:00:30,000
 [BLANK_AUDIO]

4
00:00:30,000 --> 00:00:44,000
 Okay, good evening everybody.

5
00:00:44,000 --> 00:00:48,500
 [BLANK_AUDIO]

6
00:00:48,500 --> 00:00:54,000
 Broadcasting Live, February 21st.

7
00:00:54,000 --> 00:00:59,000
 [BLANK_AUDIO]

8
00:00:59,000 --> 00:01:05,240
 Today's quote is on the fruits of goodness,

9
00:01:05,240 --> 00:01:06,600
 the fruits of good deeds.

10
00:01:06,600 --> 00:01:16,600
 [BLANK_AUDIO]

11
00:01:16,600 --> 00:01:23,600
 I was asked today about the problem to do with being a good

12
00:01:23,600 --> 00:01:32,600
 person.

13
00:01:32,600 --> 00:01:37,600
 And I could ask this often.

14
00:01:37,600 --> 00:01:42,100
 [BLANK_AUDIO]

15
00:01:42,100 --> 00:01:48,100
 If you do good deeds, if you don't react,

16
00:01:48,100 --> 00:01:52,100
 if you don't stand up for yourself,

17
00:01:52,100 --> 00:01:56,100
 [BLANK_AUDIO]

18
00:01:56,100 --> 00:01:59,100
 If you don't hold on to the things that,

19
00:01:59,100 --> 00:02:02,100
 [BLANK_AUDIO]

20
00:02:02,100 --> 00:02:05,100
 You don't hold on to things,

21
00:02:05,100 --> 00:02:13,100
 then you risk getting trod upon.

22
00:02:13,100 --> 00:02:16,100
 And then people put it, they'll walk all over you.

23
00:02:16,100 --> 00:02:19,100
 How do you stop people from walking all over you?

24
00:02:19,100 --> 00:02:22,100
 [BLANK_AUDIO]

25
00:02:22,100 --> 00:02:23,100
 It's a sphere that we have.

26
00:02:23,100 --> 00:02:26,100
 [BLANK_AUDIO]

27
00:02:26,100 --> 00:02:27,100
 It's reasonable.

28
00:02:27,100 --> 00:02:30,100
 [BLANK_AUDIO]

29
00:02:30,100 --> 00:02:38,100
 Because our hold on many things as human beings is tenuous.

30
00:02:38,100 --> 00:02:47,100
 [BLANK_AUDIO]

31
00:02:47,100 --> 00:02:51,100
 The karma that God has here isn't generally very strong,

32
00:02:51,100 --> 00:02:55,100
 or can be somewhat weak.

33
00:02:55,100 --> 00:03:01,100
 So even life itself is precarious.

34
00:03:01,100 --> 00:03:08,100
 [BLANK_AUDIO]

35
00:03:08,100 --> 00:03:11,100
 It's a bit of a dilemma.

36
00:03:11,100 --> 00:03:15,100
 Because we would argue that holding on to good things,

37
00:03:15,100 --> 00:03:22,100
 holding on to your, even your life is bad.

38
00:03:22,100 --> 00:03:24,100
 Bad because we would argue that it leads to suffering.

39
00:03:24,100 --> 00:03:27,100
 But then you say, well, how can guarding your life,

40
00:03:27,100 --> 00:03:30,100
 how can protecting your life lead to suffering?

41
00:03:30,100 --> 00:03:34,100
 [BLANK_AUDIO]

42
00:03:34,100 --> 00:03:36,100
 Well, in the end, that's the case.

43
00:03:36,100 --> 00:03:38,100
 Fear of death is suffering.

44
00:03:38,100 --> 00:03:41,100
 [BLANK_AUDIO]

45
00:03:41,100 --> 00:03:44,100
 They ask anyone, do they want to die?

46
00:03:44,100 --> 00:03:47,100
 Usually the answer is no.

47
00:03:47,100 --> 00:03:50,100
 And not only do they not want to die, but they want very

48
00:03:50,100 --> 00:03:51,100
 much not to die.

49
00:03:51,100 --> 00:03:53,100
 [BLANK_AUDIO]

50
00:03:53,100 --> 00:03:55,100
 And that's where the problem arises.

51
00:03:55,100 --> 00:03:57,590
 Because then any time your life is threatened, you have to

52
00:03:57,590 --> 00:03:58,100
 suffer.

53
00:03:58,100 --> 00:04:01,100
 So we live our lives fighting.

54
00:04:01,100 --> 00:04:05,100
 Fighting to stay alive may not seem like it,

55
00:04:05,100 --> 00:04:10,100
 but it's kind of like a constant struggle.

56
00:04:10,100 --> 00:04:14,100
 [BLANK_AUDIO]

57
00:04:14,100 --> 00:04:16,100
 Just to stay alive.

58
00:04:16,100 --> 00:04:20,130
 Even if you're well off, you have to, every day you have to

59
00:04:20,130 --> 00:04:21,100
 rush out.

60
00:04:21,100 --> 00:04:29,100
 Every day I have to rush out to take an antidote for this

61
00:04:29,100 --> 00:04:30,100
 disease

62
00:04:30,100 --> 00:04:33,100
 that we all have called hunger.

63
00:04:33,100 --> 00:04:35,100
 Just to stay alive.

64
00:04:35,100 --> 00:04:38,100
 [BLANK_AUDIO]

65
00:04:38,100 --> 00:04:40,100
 Every day I have to take this antidote.

66
00:04:40,100 --> 00:04:43,100
 Can't get, can't get around it.

67
00:04:43,100 --> 00:04:49,100
 It's a chronic, there's no cure for this sickness.

68
00:04:49,100 --> 00:04:52,100
 And that's just one of many.

69
00:04:52,100 --> 00:04:55,100
 We need air, we need water.

70
00:04:55,100 --> 00:04:59,100
 And those are the basics.

71
00:04:59,100 --> 00:05:02,100
 When you cross the street, you might get run over.

72
00:05:02,100 --> 00:05:05,100
 If you walk in the wrong place at the wrong time,

73
00:05:05,100 --> 00:05:09,100
 you might be robbed or beaten or stabbed.

74
00:05:09,100 --> 00:05:15,100
 Raped, killed.

75
00:05:15,100 --> 00:05:19,100
 You might be blown up by a bomb.

76
00:05:19,100 --> 00:05:25,100
 [BLANK_AUDIO]

77
00:05:25,100 --> 00:05:27,100
 Any number of things can happen.

78
00:05:27,100 --> 00:05:33,100
 [BLANK_AUDIO]

79
00:05:33,100 --> 00:05:39,100
 So we can't, we can't find peace this way.

80
00:05:39,100 --> 00:05:43,100
 And I would argue that, I think, or you could argue,

81
00:05:43,100 --> 00:05:48,100
 that there, it's a good idea to try and stay alive.

82
00:05:48,100 --> 00:05:55,100
 It's as much a good idea to stop clinging to life.

83
00:05:55,100 --> 00:05:58,100
 So you say, well doesn't an Arahant want to stay alive?

84
00:05:58,100 --> 00:06:04,120
 Don't they still have one thing that they protect their

85
00:06:04,120 --> 00:06:05,100
 life?

86
00:06:05,100 --> 00:06:10,100
 It's an interesting theory and it may appear like that.

87
00:06:10,100 --> 00:06:14,100
 But there's clearly in everything we do, there's,

88
00:06:14,100 --> 00:06:18,820
 the ability to do things without desire, just as a matter

89
00:06:18,820 --> 00:06:20,100
 of course.

90
00:06:20,100 --> 00:06:25,100
 It's reasonable to, if you're walking down a path and

91
00:06:25,100 --> 00:06:29,280
 you come upon, say, a wild elephant, it's reasonable to go

92
00:06:29,280 --> 00:06:30,100
 around.

93
00:06:30,100 --> 00:06:35,100
 Now it's unreasonable to run like a child and

94
00:06:35,100 --> 00:06:39,100
 cry about it, to tremble in fear.

95
00:06:39,100 --> 00:06:42,100
 There's no reason, there's no point, there's no benefit.

96
00:06:42,100 --> 00:06:54,100
 [BLANK_AUDIO]

97
00:06:54,100 --> 00:07:01,100
 But that being said, there is the sense that,

98
00:07:01,100 --> 00:07:06,100
 yes, if people try to walk all over you, the best thing for

99
00:07:06,100 --> 00:07:07,100
 you to do is let them.

100
00:07:07,100 --> 00:07:11,100
 And this is hard and this seems unreasonable, I think.

101
00:07:11,100 --> 00:07:14,100
 [BLANK_AUDIO]

102
00:07:14,100 --> 00:07:20,100
 But all of that speaks to what's being said in this quote.

103
00:07:20,100 --> 00:07:26,490
 What's not said in this quote, but what this quote hints at

104
00:07:26,490 --> 00:07:27,100
.

105
00:07:27,100 --> 00:07:30,320
 And this quote makes some bold claims that good deeds have

106
00:07:30,320 --> 00:07:31,100
 good results.

107
00:07:31,100 --> 00:07:41,100
 And so this guy, Siha, Sainapati, Sainapati is a general.

108
00:07:41,100 --> 00:07:42,100
 Saina is the army.

109
00:07:42,100 --> 00:07:47,100
 Siha is his name, he was a general.

110
00:07:47,100 --> 00:07:51,730
 And I don't know which army, but I can probably look him up

111
00:07:51,730 --> 00:07:52,100
.

112
00:07:52,100 --> 00:07:55,100
 Let's look him up here.

113
00:07:55,100 --> 00:07:58,100
 Siha means lion.

114
00:07:58,100 --> 00:08:03,100
 Alitjavi, he was alitjavi in Vasani.

115
00:08:03,100 --> 00:08:07,100
 [BLANK_AUDIO]

116
00:08:07,100 --> 00:08:10,100
 So he goes to the Buddha and he asks him, can you see?

117
00:08:10,100 --> 00:08:15,100
 [BLANK_AUDIO]

118
00:08:15,100 --> 00:08:22,100
 Sakanukobante Bhagavat sanditikang dhanapalang panyape tung

119
00:08:22,100 --> 00:08:22,100
.

120
00:08:22,100 --> 00:08:28,100
 [BLANK_AUDIO]

121
00:08:28,100 --> 00:08:35,370
 Sakai, is it possible, panyape tung to show dhanapalang,

122
00:08:35,370 --> 00:08:38,100
 the fruit of charity?

123
00:08:38,100 --> 00:08:40,100
 So the subject here is actually charity.

124
00:08:40,100 --> 00:08:47,100
 Sanditikang, that is observable.

125
00:08:47,100 --> 00:08:51,100
 The dhamma is sanditiko, sanditiko.

126
00:08:51,100 --> 00:09:04,100
 [BLANK_AUDIO]

127
00:09:04,100 --> 00:09:07,870
 So it means you can see, you can observe it, you can

128
00:09:07,870 --> 00:09:09,100
 realize it.

129
00:09:09,100 --> 00:09:14,100
 Not something you have to believe in.

130
00:09:14,100 --> 00:09:17,100
 Not something you have to take on faith.

131
00:09:17,100 --> 00:09:24,100
 And the Buddha says, Sakai, Siha, it is possible, Siha.

132
00:09:24,100 --> 00:09:33,100
 Dayakko, Siha, dhanapati, bahuno jana sapiyoho timana apo.

133
00:09:33,100 --> 00:09:45,100
 One who gives dhanapati, dhanapati is a master or someone

134
00:09:45,100 --> 00:09:49,100
 who is accomplished in giving.

135
00:09:49,100 --> 00:09:54,100
 Sihapati means the master of a seṇa, an army.

136
00:09:54,100 --> 00:09:58,100
 So this guy was a general, means he was a seṇapati.

137
00:09:58,100 --> 00:10:03,920
 Dhanapati was one of his master of dhana, master of charity

138
00:10:03,920 --> 00:10:04,100
.

139
00:10:04,100 --> 00:10:07,100
 But I don't want to talk specifically about charity.

140
00:10:07,100 --> 00:10:12,100
 I've always loathed to focus too much on charity.

141
00:10:12,100 --> 00:10:16,500
 But goodness, this applies equally to goodness, even though

142
00:10:16,500 --> 00:10:17,100
 he was active.

143
00:10:17,100 --> 00:10:19,750
 It is the kind of thing that people would be concerned

144
00:10:19,750 --> 00:10:23,100
 about because for lay people in India

145
00:10:23,100 --> 00:10:26,960
 and even lay Buddhists, a big part of their religious

146
00:10:26,960 --> 00:10:29,100
 spirituality is charity.

147
00:10:29,100 --> 00:10:31,100
 They're not able to become monks.

148
00:10:31,100 --> 00:10:34,100
 They're not able to even maybe take meditation courses.

149
00:10:34,100 --> 00:10:37,100
 So they give charity for this kind of reason.

150
00:10:37,100 --> 00:10:39,100
 But it applies to goodness.

151
00:10:39,100 --> 00:10:41,100
 The point that we should focus on is goodness.

152
00:10:41,100 --> 00:10:43,100
 Meditation is goodness.

153
00:10:43,100 --> 00:10:47,100
 So what is the benefit of goodness in general?

154
00:10:47,100 --> 00:10:51,100
 Well, the first one is bahuno jana sapiyoho timana apo.

155
00:10:51,100 --> 00:10:58,720
 One becomes dear and pleasing to many people, to the

156
00:10:58,720 --> 00:11:06,100
 multitude, to the general populace.

157
00:11:06,100 --> 00:11:12,960
 And so this is the first, I mean all four of these are

158
00:11:12,960 --> 00:11:16,980
 arguments as to why we shouldn't worry about being stepped

159
00:11:16,980 --> 00:11:17,100
 on,

160
00:11:17,100 --> 00:11:21,310
 why we shouldn't be worried about letting people walk all

161
00:11:21,310 --> 00:11:22,100
 over us,

162
00:11:22,100 --> 00:11:28,970
 why we shouldn't worry about our possessions or worry about

163
00:11:28,970 --> 00:11:33,100
 being too good is what I mean.

164
00:11:33,100 --> 00:11:36,470
 Worry about the negative consequences of seeing the good in

165
00:11:36,470 --> 00:11:39,100
 people, of being nice,

166
00:11:39,100 --> 00:11:42,100
 of being like a Pollyanna, right?

167
00:11:42,100 --> 00:11:45,100
 Because we think Pollyanna was a stupid story.

168
00:11:45,100 --> 00:11:47,100
 It's kind of a story of Pollyanna.

169
00:11:47,100 --> 00:11:51,630
 It's kind of a wonderful story about this girl who just

170
00:11:51,630 --> 00:11:53,100
 sees the good in people

171
00:11:53,100 --> 00:11:55,720
 and only sees the good and just does all sorts of good

172
00:11:55,720 --> 00:11:56,100
 things

173
00:11:56,100 --> 00:12:01,100
 and changes all sorts of really nasty people to be nice.

174
00:12:01,100 --> 00:12:03,100
 And you think, wow, that doesn't happen.

175
00:12:03,100 --> 00:12:06,100
 But it doesn't happen because we aren't nice.

176
00:12:06,100 --> 00:12:08,100
 We aren't Pollyanna.

177
00:12:08,100 --> 00:12:13,460
 It's just a story, but it really is something I think we

178
00:12:13,460 --> 00:12:19,100
 can appreciate as Buddhists, goodness.

179
00:12:19,100 --> 00:12:21,870
 There's the first reason is that you say, well, people are

180
00:12:21,870 --> 00:12:23,100
 going to walk all over me.

181
00:12:23,100 --> 00:12:29,100
 Not if most people are holding you dear.

182
00:12:29,100 --> 00:12:32,100
 I mean, what happens when people hold you dear?

183
00:12:32,100 --> 00:12:35,100
 Do they let other people walk all over you?

184
00:12:35,100 --> 00:12:38,100
 Obviously they don't.

185
00:12:38,100 --> 00:12:42,100
 They cherish you and they treat you well.

186
00:12:42,100 --> 00:12:45,100
 And then on top of that, they protect you from others.

187
00:12:45,100 --> 00:12:49,100
 They become true friends.

188
00:12:49,100 --> 00:12:52,100
 The only way to gain true friends is to be a good person.

189
00:12:52,100 --> 00:12:55,100
 You can't buy friendship.

190
00:12:55,100 --> 00:12:56,100
 You can't buy love.

191
00:12:56,100 --> 00:12:59,100
 You can't buy respect.

192
00:12:59,100 --> 00:13:06,630
 So the idea is that performing good deeds of all kinds

193
00:13:06,630 --> 00:13:09,100
 protects you.

194
00:13:09,100 --> 00:13:12,450
 This is what the Buddha said, "Dhammo, hawai, rakati, dham

195
00:13:12,450 --> 00:13:13,100
matyari."

196
00:13:13,100 --> 00:13:16,100
 When he practices the Dhamma, the Dhamma protects.

197
00:13:16,100 --> 00:13:20,100
 The Dhamma protects a person who practices the Dhamma.

198
00:13:20,100 --> 00:13:22,100
 This is what he meant by that.

199
00:13:22,100 --> 00:13:24,100
 The power of goodness is real.

200
00:13:24,100 --> 00:13:27,100
 You're a good person. Everyone respects you.

201
00:13:27,100 --> 00:13:32,090
 People go out of their way to protect you, to support you,

202
00:13:32,090 --> 00:13:40,100
 to help you, to be kind to you, to speak well of you.

203
00:13:40,100 --> 00:13:42,100
 So that's the first one.

204
00:13:42,100 --> 00:13:46,100
 And then, "Poona japarang," furthermore, beyond this,

205
00:13:46,100 --> 00:14:07,100
 "Daya kang dana pating santo sa purisa bhajanti."

206
00:14:07,100 --> 00:14:11,100
 "Sapurisa," people who are...

207
00:14:11,100 --> 00:14:14,100
 This is the word we used just recently.

208
00:14:14,100 --> 00:14:17,100
 I gave a talk on these, right? The seven sa purisa dhammas.

209
00:14:17,100 --> 00:14:22,100
 "Sapurisa" is probably a literal translation, "gentleman."

210
00:14:22,100 --> 00:14:26,100
 But it means like a high-class individual or a good person,

211
00:14:26,100 --> 00:14:29,100
 a good fellow.

212
00:14:29,100 --> 00:14:40,700
 "Bhajanti," they associate, such good people associate with

213
00:14:40,700 --> 00:14:45,100
 one who is a "daya ka."

214
00:14:45,100 --> 00:14:53,100
 "Giver," "adana pati."

215
00:14:53,100 --> 00:14:56,330
 And so, when it's surrounded by good people, one associates

216
00:14:56,330 --> 00:14:58,100
 with good and wise people,

217
00:14:58,100 --> 00:15:05,100
 who are, again, good and supportive and trustworthy.

218
00:15:05,100 --> 00:15:08,100
 So one doesn't have friends who will walk all over you.

219
00:15:08,100 --> 00:15:17,100
 One doesn't have frenemies.

220
00:15:17,100 --> 00:15:31,100
 Number three, "Kalyanu kiti sando abhuga chati."

221
00:15:31,100 --> 00:15:36,100
 "Daya ka sadaana pati no" for a "daya ka," a giver.

222
00:15:36,100 --> 00:15:40,100
 "Daya ka pati," one who is accomplished in dana.

223
00:15:40,100 --> 00:15:45,100
 "Kalyanu," a beautiful "kiti sando," a report.

224
00:15:45,100 --> 00:15:55,100
 A celebrity or a person, a good report, I guess.

225
00:15:55,100 --> 00:15:57,100
 I can't think of the right word.

226
00:15:57,100 --> 00:16:00,100
 "Abhuga chati" is spread.

227
00:16:00,100 --> 00:16:03,100
 Their fame is spread.

228
00:16:03,100 --> 00:16:06,100
 Good things are said about such a person.

229
00:16:06,100 --> 00:16:09,890
 All these things, basically what I was talking about before

230
00:16:09,890 --> 00:16:10,100
,

231
00:16:10,100 --> 00:16:16,100
 good things come to those who do good things.

232
00:16:16,100 --> 00:16:21,100
 And so this worry that,

233
00:16:21,100 --> 00:16:25,210
 this worry that if we don't stop clinging, if we don't

234
00:16:25,210 --> 00:16:26,100
 cling,

235
00:16:26,100 --> 00:16:32,100
 if we stop clinging, it's like I always envision this,

236
00:16:32,100 --> 00:16:34,740
 it's more like these beings holding onto the side of a

237
00:16:34,740 --> 00:16:35,100
 cliff,

238
00:16:35,100 --> 00:16:39,100
 afraid we're going to fly, afraid we're going to fall.

239
00:16:39,100 --> 00:16:43,100
 Like a bird clinging to the side of its nest.

240
00:16:43,100 --> 00:16:47,100
 Afraid, a baby bird afraid that if the mother kicks it out

241
00:16:47,100 --> 00:16:47,100
 of the nest,

242
00:16:47,100 --> 00:16:50,100
 it's going to fall.

243
00:16:50,100 --> 00:16:53,590
 It's just us, we're afraid that if we let go of all the

244
00:16:53,590 --> 00:16:55,100
 things that we cling to,

245
00:16:55,100 --> 00:16:57,100
 we're going to have nothing.

246
00:16:57,100 --> 00:16:59,100
 It stands to reason. You have to work hard for it.

247
00:16:59,100 --> 00:17:01,100
 It means if you stop working, you're not going to get it.

248
00:17:01,100 --> 00:17:03,100
 It's actually not the case.

249
00:17:03,100 --> 00:17:06,100
 The fact that you're obsessing over these things

250
00:17:06,100 --> 00:17:09,820
 is actually sapping your energy, it's keeping you from good

251
00:17:09,820 --> 00:17:10,100
 things,

252
00:17:10,100 --> 00:17:14,100
 keeping you from flying.

253
00:17:14,100 --> 00:17:19,100
 And when you give up, you have everything.

254
00:17:19,100 --> 00:17:21,100
 And so the first one,

255
00:17:21,100 --> 00:17:29,750
 "Bhuna-chaparang siha taya koda-napati yangya deva parisang

256
00:17:29,750 --> 00:17:35,100
 upasankamati."

257
00:17:35,100 --> 00:17:37,100
 Oh, there's five.

258
00:17:37,100 --> 00:17:39,100
 The fourth one.

259
00:17:39,100 --> 00:17:43,100
 "Wherever they go, yad-keta katya parisang."

260
00:17:43,100 --> 00:17:47,100
 Whatever parisa upasankamati they go towards,

261
00:17:47,100 --> 00:17:50,100
 whether it be a katya parisang,

262
00:17:50,100 --> 00:17:53,100
 a group of nobles,

263
00:17:53,100 --> 00:17:55,100
 yadi brahmana parisang,

264
00:17:55,100 --> 00:17:59,100
 or whether it be a community of brahmanas,

265
00:17:59,100 --> 00:18:05,100
 yadi gahapati parisang,

266
00:18:05,100 --> 00:18:09,100
 whether it be a community of householders,

267
00:18:09,100 --> 00:18:11,100
 yadi samana parisang,

268
00:18:11,100 --> 00:18:19,100
 or whether it be a parisa of samanas, of recluses.

269
00:18:19,100 --> 00:18:24,350
 "Wisarado," again this word we had in the Wisudimaga study

270
00:18:24,350 --> 00:18:25,100
 recently,

271
00:18:25,100 --> 00:18:31,100
 means one is confident, one is unshakable, unshaken.

272
00:18:31,100 --> 00:18:34,100
 "Wisarado upasankamati."

273
00:18:34,100 --> 00:18:39,100
 "Amankubutam," not shy, one approaches them,

274
00:18:39,100 --> 00:18:50,100
 "un-trembling, unashamed."

275
00:18:50,100 --> 00:18:54,100
 "Wisarado, confident."

276
00:18:54,100 --> 00:18:57,920
 So one has confidence, a good person is confident, they're

277
00:18:57,920 --> 00:18:59,100
 radiant.

278
00:18:59,100 --> 00:19:02,100
 People see this, you can see this in them,

279
00:19:02,100 --> 00:19:04,100
 they have no fear wherever they go,

280
00:19:04,100 --> 00:19:11,100
 no fear of being chastised because they are good people.

281
00:19:11,100 --> 00:19:15,100
 So it changes you, doing good deeds makes you stronger,

282
00:19:15,100 --> 00:19:18,100
 makes you less susceptible to people walking.

283
00:19:18,100 --> 00:19:23,100
 Ajahn Tong watched over a series of weeks,

284
00:19:23,100 --> 00:19:25,890
 during the time when I was sitting with him daily, every

285
00:19:25,890 --> 00:19:26,100
 day,

286
00:19:26,100 --> 00:19:31,270
 these relatives of his, people who found him distant

287
00:19:31,270 --> 00:19:32,100
 relatives

288
00:19:32,100 --> 00:19:36,100
 would come every week and ask for money,

289
00:19:36,100 --> 00:19:39,100
 tell him, "Oh, they had this problem and that problem,

290
00:19:39,100 --> 00:19:41,100
 who knows what they were really doing with the money?"

291
00:19:41,100 --> 00:19:46,100
 But he'd look at them, and the gall of these people,

292
00:19:46,100 --> 00:19:48,100
 because he saw right through them,

293
00:19:48,100 --> 00:19:53,730
 but as soon as, no judgment was forever, he'd give them the

294
00:19:53,730 --> 00:19:54,100
 money.

295
00:19:54,100 --> 00:19:57,100
 And every time they came back, until finally his secretary

296
00:19:57,100 --> 00:19:59,100
 just blew up at them

297
00:19:59,100 --> 00:20:01,830
 and yelled and started to lock the door, and I was sitting

298
00:20:01,830 --> 00:20:02,100
 outside,

299
00:20:02,100 --> 00:20:07,100
 they didn't let us in, and basically told them what's what.

300
00:20:07,100 --> 00:20:09,100
 I don't think in the end it worked,

301
00:20:09,100 --> 00:20:13,100
 because they knew Ajahn Tong would never back down.

302
00:20:13,100 --> 00:20:21,100
 So he would give them money whenever they asked.

303
00:20:21,100 --> 00:20:23,100
 And people think that's a weakness,

304
00:20:23,100 --> 00:20:26,100
 but if you watch it, there's such a strength there.

305
00:20:26,100 --> 00:20:29,100
 He was so far above these people,

306
00:20:29,100 --> 00:20:35,380
 but so far above the concerns of worrying about giving them

307
00:20:35,380 --> 00:20:36,100
 stuff.

308
00:20:36,100 --> 00:20:40,680
 It was really terrible, because that money wasn't really

309
00:20:40,680 --> 00:20:42,100
 meant for that.

310
00:20:42,100 --> 00:20:47,100
 Could we put to such better use?

311
00:20:47,100 --> 00:20:48,930
 Again, I'm sorry, I don't mean to talk about monks using

312
00:20:48,930 --> 00:20:49,100
 money,

313
00:20:49,100 --> 00:20:53,100
 but it's common knowledge monks in Thailand use money.

314
00:20:53,100 --> 00:20:56,830
 I don't touch money, but my teacher does, and all the monks

315
00:20:56,830 --> 00:20:57,100
 there,

316
00:20:57,100 --> 00:21:01,100
 it's just you really can't get by in Thailand without it.

317
00:21:01,100 --> 00:21:04,100
 Everyone's constantly trying to give the monks money,

318
00:21:04,100 --> 00:21:07,100
 and it's kind of a shame, a shame.

319
00:21:07,100 --> 00:21:11,100
 Anyway, and that's another reason for not using money,

320
00:21:11,100 --> 00:21:15,100
 because people can't take advantage of you.

321
00:21:15,100 --> 00:21:21,080
 But it changes you, goodness changes you, makes you more

322
00:21:21,080 --> 00:21:23,100
 confident.

323
00:21:23,100 --> 00:21:24,100
 And number five,

324
00:21:24,100 --> 00:21:26,100
 Dayakkoh Dhanapati,

325
00:21:26,100 --> 00:21:33,580
 Kaya Sa Bheda, Bheda, Parangmarana, Sugating, Sagang Lokang

326
00:21:33,580 --> 00:21:35,100
, Upapadjati.

327
00:21:35,100 --> 00:21:39,100
 Dayaka Dhanapati, Kaya Sa Bheda,

328
00:21:39,100 --> 00:21:45,100
 with the breakup of the body, Parangmarana, after death,

329
00:21:45,100 --> 00:21:49,100
 Sugating, Sagang Lokang, Upapadjati.

330
00:21:49,100 --> 00:21:55,200
 Upapadjati arises Sugating in a good place, Sagang in

331
00:21:55,200 --> 00:21:56,100
 heaven,

332
00:21:56,100 --> 00:22:00,100
 Sagang Lokang in the world of heaven.

333
00:22:00,100 --> 00:22:03,100
 So good deeds lead to heaven.

334
00:22:03,100 --> 00:22:06,100
 Anyone who says the Buddha didn't teach about heaven,

335
00:22:06,100 --> 00:22:09,100
 that he only talked about this life,

336
00:22:09,100 --> 00:22:13,100
 doesn't really know what they're talking about.

337
00:22:16,100 --> 00:22:20,920
 Heaven, and heaven's a great place to go, because if you're

338
00:22:20,920 --> 00:22:22,100
 born in heaven,

339
00:22:22,100 --> 00:22:25,100
 it's very easy to practice meditation.

340
00:22:25,100 --> 00:22:27,100
 And heaven's full of Buddhists.

341
00:22:27,100 --> 00:22:29,680
 There's this wonderful Thai monk, one of Ajahn Tong's

342
00:22:29,680 --> 00:22:30,100
 teachers,

343
00:22:30,100 --> 00:22:32,100
 I never met him.

344
00:22:32,100 --> 00:22:36,100
 But they recorded all of his talks on cassette tape

345
00:22:36,100 --> 00:22:40,100
 and transferred them to MP3 later on.

346
00:22:40,100 --> 00:22:42,100
 And he says, "Don't be born as a human."

347
00:22:42,100 --> 00:22:46,820
 "Yepigat beng kon." This word, "kon," depending on how you

348
00:22:46,820 --> 00:22:48,100
 spell it, I think.

349
00:22:48,100 --> 00:22:49,720
 Actually, I'm not even sure if you have to change the

350
00:22:49,720 --> 00:22:50,100
 spelling,

351
00:22:50,100 --> 00:22:56,100
 but "kon" means "person," but it also means "to stir."

352
00:22:56,100 --> 00:22:58,100
 "Kon," when you "kon" something, you stir it.

353
00:22:58,100 --> 00:23:02,100
 And he said, "Kon, yepigat beng manot bhe wakon."

354
00:23:02,100 --> 00:23:04,100
 "Don't be born a human."

355
00:23:04,100 --> 00:23:06,100
 The human means "kon."

356
00:23:06,100 --> 00:23:07,100
 Human is a "kon."

357
00:23:07,100 --> 00:23:09,100
 "Kon bhe wayung."

358
00:23:10,100 --> 00:23:13,100
 "To stir" means "to ... "

359
00:23:13,100 --> 00:23:15,100
 It means ...

360
00:23:15,100 --> 00:23:19,100
 I think I've got this right.

361
00:23:19,100 --> 00:23:21,100
 "Yung" means "busy."

362
00:23:21,100 --> 00:23:24,100
 "Yung" means "crazy, busy."

363
00:23:24,100 --> 00:23:28,100
 All mixed up.

364
00:23:28,100 --> 00:23:30,100
 "Kon."

365
00:23:30,100 --> 00:23:33,100
 "Don't be born a human," he says.

366
00:23:33,100 --> 00:23:35,100
 If you want to be born anyway, be born in heaven.

367
00:23:35,100 --> 00:23:37,100
 That's where all the Buddhists are.

368
00:23:37,100 --> 00:23:40,510
 You'd be born with Anattapindika, Wysaka, they're all up

369
00:23:40,510 --> 00:23:41,100
 there in heaven.

370
00:23:41,100 --> 00:23:49,100
 So you don't have to worry.

371
00:23:49,100 --> 00:23:53,100
 I mean, good people don't have to worry about dying.

372
00:23:53,100 --> 00:23:55,100
 You have to worry about getting on the right path.

373
00:23:55,100 --> 00:23:58,100
 But up in heaven, a lot of opportunity.

374
00:23:58,100 --> 00:24:01,360
 If you look at even in the Buddha's time, the number of

375
00:24:01,360 --> 00:24:02,100
 angels who became enlightened.

376
00:24:02,100 --> 00:24:06,300
 But even now, you think we've still got the Buddha's

377
00:24:06,300 --> 00:24:07,100
 teaching.

378
00:24:07,100 --> 00:24:11,860
 Imagine how organized they are up in heaven with the

379
00:24:11,860 --> 00:24:15,100
 arising of the Buddha's teaching.

380
00:24:15,100 --> 00:24:19,100
 How well organized they are, how many people up in heaven

381
00:24:19,100 --> 00:24:21,100
 are practicing and becoming enlightened.

382
00:24:21,100 --> 00:24:27,100
 Probably a lot.

383
00:24:29,100 --> 00:24:32,100
 So, "ma bhikkhu eva itta punyanaan"

384
00:24:32,100 --> 00:24:35,100
 Don't go bhikkhus, be afraid of punya.

385
00:24:35,100 --> 00:24:39,810
 Don't think that you're going to be taken advantage of, or

386
00:24:39,810 --> 00:24:41,100
 so on.

387
00:24:41,100 --> 00:24:44,100
 If you worry, it's only going to be suffering.

388
00:24:44,100 --> 00:24:47,100
 Then you're just clinging to the side of the cliffs,

389
00:24:47,100 --> 00:24:51,840
 clinging to the nest, afraid to fall, like a bird, ignoring

390
00:24:51,840 --> 00:24:54,100
 the fact that it has wings.

391
00:24:54,100 --> 00:24:58,100
 So, that's the Dhamma for tonight.

392
00:24:58,100 --> 00:25:01,100
 Thank you all for tuning in.

393
00:25:01,100 --> 00:25:08,100
 I'll post the hangout if anybody wants to come on.

394
00:25:08,100 --> 00:25:19,290
 Come on if you've got questions, you can come on and ask

395
00:25:19,290 --> 00:25:20,100
 questions.

396
00:25:21,100 --> 00:25:42,100
 [Silence]

397
00:25:42,100 --> 00:25:45,100
 Hey Simon, you have a question?

398
00:25:45,100 --> 00:25:50,130
 No, I think I've pressed the link just before you said, "

399
00:25:50,130 --> 00:25:51,100
Come on if you have a question."

400
00:25:51,100 --> 00:25:54,610
 So, I just want to say hello and thank you for the Dhamma

401
00:25:54,610 --> 00:25:55,100
 talk.

402
00:25:55,100 --> 00:25:58,100
 Welcome.

403
00:25:58,100 --> 00:26:03,870
 These quotes are really helpful because I'm not sure I'd be

404
00:26:03,870 --> 00:26:06,100
 able to think of something to say every day,

405
00:26:06,100 --> 00:26:08,100
 if I didn't have a little prompt.

406
00:26:08,100 --> 00:26:13,100
 Yeah, it is from the book "What the Buddha Said Still".

407
00:26:13,100 --> 00:26:16,100
 No, it's "Buddhavatjana".

408
00:26:16,100 --> 00:26:20,100
 It's called "Buddhavatjana" as the book.

409
00:26:20,100 --> 00:26:21,100
 Yeah, okay.

410
00:26:21,100 --> 00:26:24,100
 365 quotes.

411
00:26:24,100 --> 00:26:28,100
 They're not the quotes I would have probably chosen, but...

412
00:26:28,100 --> 00:26:32,340
 You see, they're fairly heavy Dhaana-oriented or charity-

413
00:26:32,340 --> 00:26:33,100
oriented.

414
00:26:33,100 --> 00:26:37,100
 Or, you know, they're soft-core Buddhism.

415
00:26:37,100 --> 00:26:40,100
 Not all, but many of them are.

416
00:26:40,100 --> 00:26:42,100
 Maybe I shouldn't be so hard.

417
00:26:43,100 --> 00:26:45,460
 I definitely appreciate the stuff you add to the quotes

418
00:26:45,460 --> 00:26:46,100
 there.

419
00:26:46,100 --> 00:26:49,100
 It gives a lot of sense.

420
00:26:49,100 --> 00:26:56,100
 Yeah, but sorry, I don't really have any questions.

421
00:26:56,100 --> 00:26:58,100
 I just wanted to handle one.

422
00:26:58,100 --> 00:27:00,100
 Thank you.

423
00:27:00,100 --> 00:27:02,100
 Let me just pop back.

424
00:27:02,100 --> 00:27:06,100
 Today... What happened today?

425
00:27:06,100 --> 00:27:10,100
 Today, a couple from Sri Lanka came

426
00:27:10,100 --> 00:27:16,100
 to offer breakfast and lunch,

427
00:27:16,100 --> 00:27:19,100
 and just to be in touch.

428
00:27:19,100 --> 00:27:21,900
 And so they said they'd like to offer food every once a

429
00:27:21,900 --> 00:27:22,100
 week,

430
00:27:22,100 --> 00:27:24,100
 which is great.

431
00:27:24,100 --> 00:27:27,100
 I mean, they just want to be in touch with a monk.

432
00:27:27,100 --> 00:27:34,100
 And then two other guys came,

433
00:27:34,100 --> 00:27:38,100
 two of my students from McMaster.

434
00:27:39,100 --> 00:27:41,100
 And I showed one of them how to meditate.

435
00:27:41,100 --> 00:27:43,100
 And they wanted to come back and do a course

436
00:27:43,100 --> 00:27:47,100
 some months from now, four months, they said.

437
00:27:47,100 --> 00:27:50,100
 So that would be over the summer, probably.

438
00:27:50,100 --> 00:27:53,100
 Sounds wonderful.

439
00:27:53,100 --> 00:27:55,100
 Mm-hmm.

440
00:27:55,100 --> 00:28:02,100
 And someone tonight told me that

441
00:28:04,100 --> 00:28:09,100
 through this practice they overcame alcoholism.

442
00:28:09,100 --> 00:28:14,100
 So that was great.

443
00:28:14,100 --> 00:28:18,100
 And survived chemotherapy by meditating through it.

444
00:28:18,100 --> 00:28:20,100
 Wow.

445
00:28:20,100 --> 00:28:22,100
 That's very interesting.

446
00:28:22,100 --> 00:28:25,100
 I mean, I don't know physically, but mentally, you know?

447
00:28:25,100 --> 00:28:27,100
 Yeah.

448
00:28:27,100 --> 00:28:31,100
 And as far as dealing with the pain and the stress and all.

449
00:28:33,100 --> 00:28:36,990
 Yeah, I couldn't imagine meditation doing anything but good

450
00:28:36,990 --> 00:28:39,100
 for such patients.

451
00:28:39,100 --> 00:28:42,100
 Yeah, all these.

452
00:28:42,100 --> 00:28:46,100
 We should maybe have a page for testimonials.

453
00:28:46,100 --> 00:28:49,100
 We should just set up a page.

454
00:28:49,100 --> 00:28:51,100
 Testimonials.

455
00:28:51,100 --> 00:28:54,100
 Yeah.

456
00:28:54,100 --> 00:28:56,100
 Definitely.

457
00:28:56,100 --> 00:28:59,100
 And people can just, I don't know, maybe pages overkill.

458
00:28:59,100 --> 00:29:02,100
 Maybe just have people.

459
00:29:02,100 --> 00:29:05,100
 Yeah, we should have a page, but I post them or something.

460
00:29:05,100 --> 00:29:08,100
 Not letting people post them, but...

461
00:29:08,100 --> 00:29:13,100
 See if I have time.

462
00:29:13,100 --> 00:29:16,100
 Sounds like a wonderful idea.

463
00:29:16,100 --> 00:29:20,100
 Maybe I'm going to remind you of the...

464
00:29:20,100 --> 00:29:23,100
 It's a good idea.

465
00:29:23,100 --> 00:29:24,100
 Yeah.

466
00:29:24,100 --> 00:29:36,100
 Dante, I have a question.

467
00:29:36,100 --> 00:29:37,100
 Okay.

468
00:29:37,100 --> 00:29:42,410
 What would you recommend you do if you realized that one of

469
00:29:42,410 --> 00:29:46,100
 your friends is a clinical sociopath or narcissist?

470
00:29:47,100 --> 00:29:52,110
 Somebody who has no conscience and basically lives by the

471
00:29:52,110 --> 00:29:55,100
 opposite of the five precepts.

472
00:29:55,100 --> 00:29:59,100
 That doesn't sound like a very good friend.

473
00:29:59,100 --> 00:30:01,100
 Why?

474
00:30:01,100 --> 00:30:03,100
 It doesn't sound like something I'd want as a friend,

475
00:30:03,100 --> 00:30:05,100
 someone I'd want as a friend.

476
00:30:05,100 --> 00:30:10,100
 I think a big problem is we choose friends arbitrarily.

477
00:30:10,100 --> 00:30:14,020
 Why are you friends with people if they are narcissists and

478
00:30:14,020 --> 00:30:15,100
 sociopaths?

479
00:30:16,100 --> 00:30:19,480
 Well, this particular person was a co-worker of mine, and

480
00:30:19,480 --> 00:30:23,780
 these types of people are generally very charming in nature

481
00:30:23,780 --> 00:30:24,100
.

482
00:30:24,100 --> 00:30:27,790
 So it was not something that I realized until our

483
00:30:27,790 --> 00:30:30,100
 friendship had developed.

484
00:30:30,100 --> 00:30:33,100
 Chalk it up for experience and move on.

485
00:30:33,100 --> 00:30:36,960
 I mean, not to be cruel. You don't have to avoid the person

486
00:30:36,960 --> 00:30:37,100
.

487
00:30:37,100 --> 00:30:39,100
 I would to some extent.

488
00:30:39,100 --> 00:30:43,770
 At least not actively seek this person out and be careful

489
00:30:43,770 --> 00:30:45,100
 around them.

490
00:30:46,100 --> 00:30:48,100
 There's certainly...

491
00:30:48,100 --> 00:30:50,100
 Be mindful around them.

492
00:30:50,100 --> 00:30:53,240
 But don't let it... Don't suffer because of it. Don't lose

493
00:30:53,240 --> 00:30:54,100
 sleep over it.

494
00:30:54,100 --> 00:30:58,100
 Don't get upset when they try to take advantage of you.

495
00:30:58,100 --> 00:31:04,100
 Just, you know, don't have anything to do with such people.

496
00:31:04,100 --> 00:31:08,100
 Part of being mindful is learning not to engage.

497
00:31:08,100 --> 00:31:12,100
 Because we react to people.

498
00:31:12,100 --> 00:31:17,100
 People like that are really good at getting at you, right?

499
00:31:17,100 --> 00:31:21,100
 And evoking a response.

500
00:31:21,100 --> 00:31:24,900
 They'll say something ridiculous and you'll attack them for

501
00:31:24,900 --> 00:31:27,100
 it and then they'll bait and switch, right?

502
00:31:27,100 --> 00:31:31,100
 They bait you in with something and then, "Oh, sob story,"

503
00:31:31,100 --> 00:31:32,100
 and then you feel sorry for them.

504
00:31:32,100 --> 00:31:35,100
 And by the time you realize that they're just playing it,

505
00:31:35,100 --> 00:31:35,100
 you know?

506
00:31:35,100 --> 00:31:37,100
 So you have to be mindful of that.

507
00:31:37,100 --> 00:31:42,100
 You can't let your emotions have a lot to do with it.

508
00:31:42,100 --> 00:31:45,100
 And we're not perfect, so you're going to make mistakes.

509
00:31:45,100 --> 00:31:48,100
 Most important is to not suffer because of it.

510
00:31:48,100 --> 00:31:51,770
 And the only way to really not suffer is to learn to let go

511
00:31:51,770 --> 00:31:52,100
.

512
00:31:52,100 --> 00:31:54,100
 To learn to not cling.

513
00:31:54,100 --> 00:31:59,100
 Stop trying to protect yourself.

514
00:31:59,100 --> 00:32:04,100
 Thank you, Bhakti.

515
00:32:05,100 --> 00:32:07,100
 Yeah, I mean, to what extent you're able to do that.

516
00:32:07,100 --> 00:32:10,100
 In lay life, it's a tug of war, you know?

517
00:32:10,100 --> 00:32:12,510
 We're kind of caught in the middle of trying to live the D

518
00:32:12,510 --> 00:32:15,100
hamma life and trying to live in the world.

519
00:32:15,100 --> 00:32:20,100
 So it doesn't mean you have to give up everything.

520
00:32:20,100 --> 00:32:23,900
 But to the extent that you can give up, it is actually to

521
00:32:23,900 --> 00:32:25,100
 your benefit.

522
00:32:25,100 --> 00:32:33,100
 Good question.

523
00:32:34,100 --> 00:32:35,100
 Thank you.

524
00:32:35,100 --> 00:32:37,100
 You're welcome.

525
00:32:37,100 --> 00:32:43,100
 Okay, if that's all, I think I'm going to say good night.

526
00:32:43,100 --> 00:32:46,100
 See you all.

527
00:32:46,100 --> 00:32:47,100
 Good night, Pandit.

528
00:32:47,100 --> 00:32:48,100
 Good night.

529
00:32:48,100 --> 00:32:49,100
 Good night.

530
00:32:50,100 --> 00:32:51,100
 Okay.

531
00:32:52,100 --> 00:32:53,100
 Thank you.

532
00:32:55,100 --> 00:32:56,100
 Thank you.

533
00:32:57,100 --> 00:32:58,100
 Thank you.

534
00:32:58,100 --> 00:32:59,100
 Thank you.

535
00:33:01,100 --> 00:33:02,100
 Thank you.

