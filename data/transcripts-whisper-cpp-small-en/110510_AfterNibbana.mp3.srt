1
00:00:00,000 --> 00:00:05,100
 Hello, welcome back to Ask a Monk. Now I have two questions

2
00:00:05,100 --> 00:00:07,760
 that were asked together. The

3
00:00:07,760 --> 00:00:15,320
 first question is whether after realizing nirvana, nirvana,

4
00:00:15,320 --> 00:00:18,640
 which means freedom or release

5
00:00:18,640 --> 00:00:24,290
 or emancipation or however you want to translate it, is

6
00:00:24,290 --> 00:00:28,240
 there anything left to do or is nirvana

7
00:00:28,240 --> 00:00:32,100
 it? I mean is that all you have to do? That's the first

8
00:00:32,100 --> 00:00:35,040
 question. So I'll answer this question

9
00:00:35,040 --> 00:00:43,610
 first. The answer is it's easy. Nirvana is the last thing

10
00:00:43,610 --> 00:00:45,920
 that we ever have to do. Once

11
00:00:45,920 --> 00:00:48,390
 you realize nirvana there's nothing left to do. There's

12
00:00:48,390 --> 00:00:49,920
 nothing left that from a Buddhist

13
00:00:49,920 --> 00:00:54,490
 point of view you need to do. The point is at that point

14
00:00:54,490 --> 00:00:57,520
 you're free from suffering.

15
00:00:57,520 --> 00:01:02,480
 So what does this mean? Nirvana is the cessation of

16
00:01:02,480 --> 00:01:05,600
 suffering. This is what the definition

17
00:01:05,600 --> 00:01:11,230
 of it is. It's possible for a person to, not easy, but it's

18
00:01:11,230 --> 00:01:13,400
 possible for a human being

19
00:01:13,400 --> 00:01:18,640
 to realize freedom from suffering, to come to be, even

20
00:01:18,640 --> 00:01:21,200
 temporarily, to be free from the

21
00:01:21,200 --> 00:01:27,550
 arising of stress, the arising of any impermanent

22
00:01:27,550 --> 00:01:33,520
 phenomenon. At that point the mind is perfectly

23
00:01:33,520 --> 00:01:39,400
 free. It's possible to enter or to touch that state, to

24
00:01:39,400 --> 00:01:42,960
 experience or to see or to realize

25
00:01:42,960 --> 00:01:50,220
 that state without being free from suffering, without being

26
00:01:50,220 --> 00:01:52,560
 that way forever. This is the

27
00:01:52,560 --> 00:01:55,110
 first thing to point out. This person we have names for

28
00:01:55,110 --> 00:01:56,800
 them, the sotapana, someone who

29
00:01:56,800 --> 00:02:00,390
 has entered the stream, because though they still have to

30
00:02:00,390 --> 00:02:02,400
 suffer and they still have more

31
00:02:02,400 --> 00:02:09,960
 to do, they've already cracked the shell, so to speak, and

32
00:02:09,960 --> 00:02:12,800
 it's only a matter of time

33
00:02:12,800 --> 00:02:16,700
 before they become totally free from suffering. So this is

34
00:02:16,700 --> 00:02:18,800
 one understanding of nirvana. It's

35
00:02:18,800 --> 00:02:22,640
 an experience that someone has, and at that point one's

36
00:02:22,640 --> 00:02:25,040
 life starts to go in the direction

37
00:02:25,040 --> 00:02:29,870
 towards freedom from suffering. But what we mean by nirvana

38
00:02:29,870 --> 00:02:33,840
 or parinirvana or so on is

39
00:02:33,840 --> 00:02:37,980
 the complete freedom from suffering. So though a person

40
00:02:37,980 --> 00:02:40,600
 might still live, they have no more

41
00:02:40,600 --> 00:02:44,500
 greed, no more anger, no more delusion, these things can't

42
00:02:44,500 --> 00:02:47,080
 arise. There's perfect understanding

43
00:02:47,080 --> 00:02:51,700
 of reality, of the experiences around oneself. When you see

44
00:02:51,700 --> 00:02:54,680
, hear, smell, taste, feel, think,

45
00:02:54,680 --> 00:02:57,450
 there's only the awareness of the object. There's no

46
00:02:57,450 --> 00:02:59,040
 attachment to it. So we say this

47
00:02:59,040 --> 00:03:02,370
 is nirvana, this comes from realizing this state of

48
00:03:02,370 --> 00:03:04,400
 ultimate freedom. When you realize

49
00:03:04,400 --> 00:03:07,480
 it again and again, you start to see that there's nothing

50
00:03:07,480 --> 00:03:09,320
 that lasts, that all of the

51
00:03:09,320 --> 00:03:13,110
 experiences that we have simply arise and cease. There's

52
00:03:13,110 --> 00:03:15,120
 nothing lasting, there's nothing

53
00:03:15,120 --> 00:03:18,630
 permanent, there's nothing stable. And so one loses all of

54
00:03:18,630 --> 00:03:20,200
 one's attachment. At that

55
00:03:20,200 --> 00:03:25,870
 point one isn't subject to future rebirth. There's this

56
00:03:25,870 --> 00:03:29,040
 physical reality that we've somehow

57
00:03:29,040 --> 00:03:34,910
 built up that has to last out its lifetime. But at the end

58
00:03:34,910 --> 00:03:39,040
 of that cycle, that revolution,

59
00:03:39,040 --> 00:03:42,930
 at death there's nothing. So at death there's no more

60
00:03:42,930 --> 00:03:45,800
 arising. And at that point there's

61
00:03:45,800 --> 00:03:52,310
 freedom from suffering, freedom from impermanence, from

62
00:03:52,310 --> 00:03:56,920
 birth, from having to come back again.

63
00:03:56,920 --> 00:03:59,480
 So I think this is clear. The answer from a Buddhist point

64
00:03:59,480 --> 00:04:00,720
 of view, doctrinal point

65
00:04:00,720 --> 00:04:04,440
 of view is quite clear. After that there's nothing left to

66
00:04:04,440 --> 00:04:06,760
 do. The Buddha said, "Kattang

67
00:04:06,760 --> 00:04:09,820
 is done, done is what needs to be done." There's nothing

68
00:04:09,820 --> 00:04:13,400
 left to do, there's nothing more here.

69
00:04:13,400 --> 00:04:18,570
 But I think an important point to make that is often missed

70
00:04:18,570 --> 00:04:20,960
 is that you don't somehow

71
00:04:20,960 --> 00:04:26,160
 fall into this state. This is the ultimate, the samam bonam

72
00:04:26,160 --> 00:04:28,720
, the final emancipation. It

73
00:04:28,720 --> 00:04:32,700
 comes to someone who has had enough, who doesn't see any

74
00:04:32,700 --> 00:04:34,620
 benefit in coming back, doesn't see

75
00:04:34,620 --> 00:04:39,470
 any benefit in recreating this human state or any state

76
00:04:39,470 --> 00:04:41,840
 again and again and again. Most

77
00:04:41,840 --> 00:04:44,660
 of us are not there. Most of us think that there's still

78
00:04:44,660 --> 00:04:46,320
 something to be done. We want

79
00:04:46,320 --> 00:04:49,610
 this, we want that, we've been taught that it's good to

80
00:04:49,610 --> 00:04:51,720
 have a partner, to have a family,

81
00:04:51,720 --> 00:04:54,600
 to have a job, to help people and so on. And we think that

82
00:04:54,600 --> 00:04:56,200
 there's somehow some benefit

83
00:04:56,200 --> 00:05:00,740
 in this. On the other hand, we see that in ourselves there

84
00:05:00,740 --> 00:05:02,280
 are certain attachments that

85
00:05:02,280 --> 00:05:05,990
 we have that are causing our suffering. So this is where we

86
00:05:05,990 --> 00:05:07,360
're at. We're at the point

87
00:05:07,360 --> 00:05:10,580
 where we want to hold on to certain things but we want to

88
00:05:10,580 --> 00:05:12,600
 let go of other certain things.

89
00:05:12,600 --> 00:05:16,510
 So this just shows that we're somewhere along the path, we

90
00:05:16,510 --> 00:05:21,040
're somewhere between a useless

91
00:05:21,040 --> 00:05:25,270
 person and an enlightened person. And it's up to us which

92
00:05:25,270 --> 00:05:27,560
 way we're going to go. If we

93
00:05:27,560 --> 00:05:30,410
 take the side of letting go, we don't have to let go of

94
00:05:30,410 --> 00:05:32,080
 those things we don't want to

95
00:05:32,080 --> 00:05:34,840
 let go of, we let go of those things that we want to let go

96
00:05:34,840 --> 00:05:35,880
 of. And this is how the

97
00:05:35,880 --> 00:05:40,410
 path works. People who are afraid of things like nirvana,

98
00:05:40,410 --> 00:05:42,460
 afraid of letting go and afraid

99
00:05:42,460 --> 00:05:45,030
 of practicing meditation because they think if I practice

100
00:05:45,030 --> 00:05:46,320
 meditation I'm going to let

101
00:05:46,320 --> 00:05:49,080
 go of all these things I love and I love them and I want

102
00:05:49,080 --> 00:05:50,960
 those things, I don't want to let

103
00:05:50,960 --> 00:05:53,620
 go of them. And the truth is if you want it, if it's

104
00:05:53,620 --> 00:05:55,520
 something you're holding on to, you're

105
00:05:55,520 --> 00:05:59,630
 never going to let it go, that's the point. But what you

106
00:05:59,630 --> 00:06:02,120
 have to realize is that the more

107
00:06:02,120 --> 00:06:05,600
 you understand, the more mature you become, the more you

108
00:06:05,600 --> 00:06:07,480
 realize that you're not benefiting

109
00:06:07,480 --> 00:06:11,470
 anyone or anything by yourself or others, by holding on to

110
00:06:11,470 --> 00:06:13,360
 anything, by clinging to

111
00:06:13,360 --> 00:06:18,860
 others, by wanting things to be a certain way, it's not of

112
00:06:18,860 --> 00:06:21,160
 a benefit to you and it's

113
00:06:21,160 --> 00:06:23,610
 not of a benefit to other people. That true happiness doesn

114
00:06:23,610 --> 00:06:25,280
't come from these things.

115
00:06:25,280 --> 00:06:28,080
 You know, people when they hear about not coming back they

116
00:06:28,080 --> 00:06:29,520
 think well, the great thing

117
00:06:29,520 --> 00:06:32,620
 about Buddhism is that there is coming back. I remember

118
00:06:32,620 --> 00:06:34,600
 talking with some, with a Catholic

119
00:06:34,600 --> 00:06:38,420
 woman and she said oh it's so great to hear about how in

120
00:06:38,420 --> 00:06:40,660
 Buddhism you've got a chance

121
00:06:40,660 --> 00:06:43,860
 to come back because of course in the religion she had been

122
00:06:43,860 --> 00:06:45,700
 brought up with, that's it. When

123
00:06:45,700 --> 00:06:47,710
 you die it's either heaven or hell forever. But she was

124
00:06:47,710 --> 00:06:48,920
 thinking wow, you know, to be

125
00:06:48,920 --> 00:06:52,350
 able to come back and have another chance. So for Western

126
00:06:52,350 --> 00:06:53,520
ers I think this is common.

127
00:06:53,520 --> 00:06:57,200
 You think oh it's great, I'll be able to come back and try

128
00:06:57,200 --> 00:06:58,920
 again like this movie Groundhog

129
00:06:58,920 --> 00:07:02,150
 Day, you know, until you get it right. And that's really

130
00:07:02,150 --> 00:07:03,160
 the point, it's until you get

131
00:07:03,160 --> 00:07:07,160
 it right you're going to keep coming back. But whether this

132
00:07:07,160 --> 00:07:08,960
 should be looked at, seen

133
00:07:08,960 --> 00:07:13,680
 as a good thing or not is debatable. I mean ask yourself,

134
00:07:13,680 --> 00:07:15,840
 don't ask yourself how are you

135
00:07:15,840 --> 00:07:18,000
 feeling now because most people think well I'm okay and I

136
00:07:18,000 --> 00:07:19,200
've got plans, you know, if

137
00:07:19,200 --> 00:07:23,200
 I can just this and this and this, there's a chance that I

138
00:07:23,200 --> 00:07:26,200
'll be stable and happy and

139
00:07:26,200 --> 00:07:29,760
 living a good life. But then, you know, don't ask yourself,

140
00:07:29,760 --> 00:07:31,080
 don't look at that, ask yourself

141
00:07:31,080 --> 00:07:34,150
 what it's taken to get even to the point that you're at now

142
00:07:34,150 --> 00:07:35,920
. And think of all the lessons

143
00:07:35,920 --> 00:07:39,420
 you've had to learn. And if it's true that you'll come back

144
00:07:39,420 --> 00:07:41,120
 again and again and again,

145
00:07:41,120 --> 00:07:43,420
 then you're going to have to learn all of those. If you don

146
00:07:43,420 --> 00:07:44,600
't learn anything now and

147
00:07:44,600 --> 00:07:48,130
 change anything about this state, you don't somehow

148
00:07:48,130 --> 00:07:50,120
 increase your level of maturity in

149
00:07:50,120 --> 00:07:54,740
 a way that you've never done before in all your lives, then

150
00:07:54,740 --> 00:07:56,600
 you're just going to have

151
00:07:56,600 --> 00:07:58,590
 to learn these lessons again and go through all of the

152
00:07:58,590 --> 00:07:59,960
 suffering that we had to go through

153
00:07:59,960 --> 00:08:04,320
 as children, as teens, as young adults, as adults and so on

154
00:08:04,320 --> 00:08:06,640
. And wait until you get old,

155
00:08:06,640 --> 00:08:11,230
 sick and die, I mean see how that's going to be. And then

156
00:08:11,230 --> 00:08:12,400
 don't stop there, look at

157
00:08:12,400 --> 00:08:15,340
 the people around you and in the world around us. Who's to

158
00:08:15,340 --> 00:08:16,800
 say in next life you won't be

159
00:08:16,800 --> 00:08:22,640
 one of the people who suffer terribly in this life? So it's

160
00:08:22,640 --> 00:08:26,280
 not as simple a thing as some

161
00:08:26,280 --> 00:08:28,230
 people might think. They think, oh great, I'll come back, I

162
00:08:28,230 --> 00:08:29,040
'll get to do this again

163
00:08:29,040 --> 00:08:32,110
 and I'll do this differently and I'll learn more. No, it's

164
00:08:32,110 --> 00:08:33,320
 not like that. If you're not

165
00:08:33,320 --> 00:08:37,100
 careful, you know, some lifetimes down the road, it's very,

166
00:08:37,100 --> 00:08:38,720
 very possible that you'll

167
00:08:38,720 --> 00:08:42,490
 end up in a terrible situation, a situation of intense

168
00:08:42,490 --> 00:08:44,960
 suffering. Why do people suffer

169
00:08:44,960 --> 00:08:49,050
 in such horrible ways that they do? And who's to say you

170
00:08:49,050 --> 00:08:52,320
 won't end up like them? So there

171
00:08:52,320 --> 00:08:57,780
 is some, we can see some benefit in at least bringing

172
00:08:57,780 --> 00:09:01,600
 ourselves somewhat out of this state

173
00:09:01,600 --> 00:09:05,290
 to a state where we don't lose all of our memories every 50

174
00:09:05,290 --> 00:09:07,440
 to 100 years, where we're

175
00:09:07,440 --> 00:09:11,970
 able to keep them and to continue developing. That's at the

176
00:09:11,970 --> 00:09:16,520
 very least a good thing to do.

177
00:09:16,520 --> 00:09:19,480
 And so the development of the mind shouldn't scare people.

178
00:09:19,480 --> 00:09:21,280
 The practice of Buddhism shouldn't

179
00:09:21,280 --> 00:09:25,190
 be a scary thing that you're going to let go of things you

180
00:09:25,190 --> 00:09:27,000
 hold on to. Buddhism is about

181
00:09:27,000 --> 00:09:30,680
 learning, is about coming to grow, is growing up and coming

182
00:09:30,680 --> 00:09:32,680
 to learn things and understand

183
00:09:32,680 --> 00:09:37,010
 more about reality. It's not to brainwash us or to force

184
00:09:37,010 --> 00:09:39,480
 some kind of detached state.

185
00:09:39,480 --> 00:09:43,680
 It's to help us to learn what's causing us suffering.

186
00:09:43,680 --> 00:09:46,080
 Because all of us can tell if we're

187
00:09:46,080 --> 00:09:50,780
 not terribly blind, we can verify that there is a lot of

188
00:09:50,780 --> 00:09:53,680
 suffering in this world. And the

189
00:09:53,680 --> 00:09:56,800
 truth of the Buddhist teaching is that this suffering is

190
00:09:56,800 --> 00:09:58,640
 unnecessary. We don't have to

191
00:09:58,640 --> 00:10:01,790
 suffer in this way. The reason we do is because we don't

192
00:10:01,790 --> 00:10:04,400
 understand and because we don't understand,

193
00:10:04,400 --> 00:10:07,420
 we therefore do things that cause us suffering and that

194
00:10:07,420 --> 00:10:09,680
 cause other people suffering. So

195
00:10:09,680 --> 00:10:12,770
 slowly we learn how to overcome those things, how to be

196
00:10:12,770 --> 00:10:14,960
 free from them. And eventually we

197
00:10:14,960 --> 00:10:18,670
 learn how to be free from all suffering, how to never cause

198
00:10:18,670 --> 00:10:20,480
 suffering for others or for

199
00:10:20,480 --> 00:10:23,730
 ourselves, how to act in such a way that is not

200
00:10:23,730 --> 00:10:26,920
 contradictory to our wishes, where we wish

201
00:10:26,920 --> 00:10:29,950
 to be happy, we wish to live in peace and yet we find

202
00:10:29,950 --> 00:10:31,920
 ourselves acting and speaking

203
00:10:31,920 --> 00:10:36,560
 and thinking in ways that cause us suffering and cause dish

204
00:10:36,560 --> 00:10:39,200
armony in the world around us.

205
00:10:39,200 --> 00:10:43,220
 So this is the, I think, sort of a detailed answer of why

206
00:10:43,220 --> 00:10:46,040
 freedom would be the last thing.

207
00:10:46,040 --> 00:10:50,640
 Because once you have no clinging, once you have no

208
00:10:50,640 --> 00:10:54,120
 attachment and no delusion, you will

209
00:10:54,120 --> 00:11:01,110
 not cause any suffering for yourself. And as a result, you

210
00:11:01,110 --> 00:11:03,920
 will not have to come back

211
00:11:03,920 --> 00:11:11,360
 and there's no more creating, no more forming, no more

212
00:11:11,360 --> 00:11:15,240
 building, building up. You realize

213
00:11:15,240 --> 00:11:18,600
 that there's nothing worth clinging to, that true happiness

214
00:11:18,600 --> 00:11:20,240
 doesn't come from the objects

215
00:11:20,240 --> 00:11:24,240
 of the sense or any of the objects that are inside of us or

216
00:11:24,240 --> 00:11:26,600
 in the world around us. True

217
00:11:26,600 --> 00:11:29,500
 happiness comes from freedom and you come to see that this

218
00:11:29,500 --> 00:11:31,520
 freedom is the ultimate happiness.

219
00:11:31,520 --> 00:11:36,200
 So that's the answer to that question. The second question,

220
00:11:36,200 --> 00:11:37,560
 I'm going to have to deal

221
00:11:37,560 --> 00:11:40,370
 with it here so this video is going to be a little bit

222
00:11:40,370 --> 00:11:44,480
 longer. The second question is

223
00:11:44,480 --> 00:11:51,590
 whether karma has physical results only or affects the mind

224
00:11:51,590 --> 00:11:55,440
 as well. And you know, these

225
00:11:55,440 --> 00:11:58,890
 questions are actually fairly easy because they're just

226
00:11:58,890 --> 00:12:01,000
 simple doctrinal questions. But

227
00:12:01,000 --> 00:12:03,550
 they're interesting as well. Karma, first we have to

228
00:12:03,550 --> 00:12:05,600
 understand that karma is not a

229
00:12:05,600 --> 00:12:09,660
 thing. It doesn't exist in the world. You can't show me

230
00:12:09,660 --> 00:12:12,040
 karma. Karma means action. So

231
00:12:12,040 --> 00:12:15,150
 technically speaking, you can show me an action when you

232
00:12:15,150 --> 00:12:17,200
 know, show me the karma of killing.

233
00:12:17,200 --> 00:12:21,320
 So you kill something there, there's karma for you. But the

234
00:12:21,320 --> 00:12:22,360
 problem is that we come to

235
00:12:22,360 --> 00:12:25,730
 take karma as this substance that exists like something you

236
00:12:25,730 --> 00:12:27,680
're carrying around like a weight

237
00:12:27,680 --> 00:12:31,140
 over your shoulder or that's somehow tattooed on your skin

238
00:12:31,140 --> 00:12:33,040
 or something like that, that

239
00:12:33,040 --> 00:12:37,300
 you carry around and you're just waiting for it to drop on

240
00:12:37,300 --> 00:12:39,520
 your head or something. And

241
00:12:39,520 --> 00:12:48,190
 that's not the case. Karma is a law. It's a law that says

242
00:12:48,190 --> 00:12:51,960
 that there is an effect to our

243
00:12:51,960 --> 00:12:55,170
 actions and not just our, not our actions actually to our

244
00:12:55,170 --> 00:12:56,880
 intentions, to our state of

245
00:12:56,880 --> 00:13:03,970
 mind that the mind is able to affect the world around us.

246
00:13:03,970 --> 00:13:06,280
 It's not a doctrine of determinism.

247
00:13:06,280 --> 00:13:09,230
 If it were this substance or this thing that you could

248
00:13:09,230 --> 00:13:11,280
 measure, then there might be some

249
00:13:11,280 --> 00:13:15,300
 determinism. But it doesn't say that. Karma just says that

250
00:13:15,300 --> 00:13:17,360
 it doesn't say either way that

251
00:13:17,360 --> 00:13:20,820
 there is a free will or determinism. That's not the point

252
00:13:20,820 --> 00:13:22,760
 of karma. Karma says that when

253
00:13:22,760 --> 00:13:28,510
 there is action, when you do kill or steal or lie, when in

254
00:13:28,510 --> 00:13:31,320
 your mind you develop these

255
00:13:31,320 --> 00:13:36,130
 unwholesome tendencies to these ideas for the creation of

256
00:13:36,130 --> 00:13:39,040
 disharmony and suffering and

257
00:13:39,040 --> 00:13:44,920
 stress, then there isn't the effect. The effect is that

258
00:13:44,920 --> 00:13:48,040
 stress is created when you have the

259
00:13:48,040 --> 00:13:52,180
 intention to help people as well. There's another sort of

260
00:13:52,180 --> 00:13:54,920
 stress created. It's not an

261
00:13:54,920 --> 00:13:59,000
 unpleasant stress, but it's a stress of sorts that creates

262
00:13:59,000 --> 00:14:00,520
 pleasure. It's a vibration. When

263
00:14:00,520 --> 00:14:04,580
 you, it's like a vibration, when you want to help people,

264
00:14:04,580 --> 00:14:06,560
 when you have the intention

265
00:14:06,560 --> 00:14:14,300
 to do good things, to give, to help, to support, to teach,

266
00:14:14,300 --> 00:14:17,520
 to even to just be kind. And when

267
00:14:17,520 --> 00:14:20,970
 you study, when you learn, when you practice meditation,

268
00:14:20,970 --> 00:14:23,240
 all of these things have an effect.

269
00:14:23,240 --> 00:14:30,430
 They change who we are. And so that's, it's important to

270
00:14:30,430 --> 00:14:36,200
 understand that aspect of karma.

271
00:14:36,200 --> 00:14:40,620
 But the short answer to your question then is that karma,

272
00:14:40,620 --> 00:14:42,920
 our actions will affect both

273
00:14:42,920 --> 00:14:49,680
 the body and the mind. Technically speaking, karma gives

274
00:14:49,680 --> 00:14:52,240
 rise to experiences. It gives

275
00:14:52,240 --> 00:14:55,580
 rise to seeing, hearing, smelling, tasting, feeling,

276
00:14:55,580 --> 00:14:58,840
 thinking. This is the technical explanation

277
00:14:58,840 --> 00:15:03,740
 that it's going to give rise to sensual experience in the

278
00:15:03,740 --> 00:15:05,720
 future. If you do something and you

279
00:15:05,720 --> 00:15:08,300
 intend to do something, that's going to change the

280
00:15:08,300 --> 00:15:10,400
 experiences that you have. You're not

281
00:15:10,400 --> 00:15:12,810
 going to see the same things as if you hadn't done that,

282
00:15:12,810 --> 00:15:14,920
 right? If you kill someone, you're

283
00:15:14,920 --> 00:15:17,590
 going to see the inside of a jail and so on. You're going

284
00:15:17,590 --> 00:15:19,640
 to see hearing, smelling, tasting,

285
00:15:19,640 --> 00:15:22,160
 feeling, thinking. But from a technical point of view, all

286
00:15:22,160 --> 00:15:23,440
 that's what happens is there

287
00:15:23,440 --> 00:15:26,410
 are different experiences that you have. And those

288
00:15:26,410 --> 00:15:30,120
 experiences are both physical and mental.

289
00:15:30,120 --> 00:15:35,830
 So all it means is that our, we are affecting our destiny,

290
00:15:35,830 --> 00:15:38,720
 whether this is based on free

291
00:15:38,720 --> 00:15:43,270
 will or determinism isn't really the question. The point is

292
00:15:43,270 --> 00:15:45,320
 that when we do do something,

293
00:15:45,320 --> 00:15:51,530
 we are as a result. And so rather than worrying about such

294
00:15:51,530 --> 00:15:55,080
 questions as whether it's free will

295
00:15:55,080 --> 00:15:59,710
 or determinism or what does karma affect, is it a physical

296
00:15:59,710 --> 00:16:02,040
 or a mental thing, we should

297
00:16:02,040 --> 00:16:07,900
 train ourselves to give up the unwholesome tendencies that

298
00:16:07,900 --> 00:16:10,320
 cause us suffering. In the

299
00:16:10,320 --> 00:16:12,830
 end, the interesting thing is that you give up both kinds

300
00:16:12,830 --> 00:16:14,120
 of karma because you have no

301
00:16:14,120 --> 00:16:17,750
 intention to create happiness either to create happiness

302
00:16:17,750 --> 00:16:20,080
 and the sense of happy experiences.

303
00:16:20,080 --> 00:16:22,980
 You come to see that even pleasurable experiences are

304
00:16:22,980 --> 00:16:25,400
 temporary. Even if you create harmony in

305
00:16:25,400 --> 00:16:28,790
 the world, even if I were able to with my great karma,

306
00:16:28,790 --> 00:16:30,840
 create peace and harmony in the

307
00:16:30,840 --> 00:16:35,830
 world, with through some great act, a minor series of act,

308
00:16:35,830 --> 00:16:37,920
 it's temporary. And unless there's

309
00:16:37,920 --> 00:16:41,160
 wisdom and understanding that allows people to let go, they

310
00:16:41,160 --> 00:16:42,800
're just going to ruin it when

311
00:16:42,800 --> 00:16:47,000
 I'm gone. This is, for instance, the Buddha's teaching when

312
00:16:47,000 --> 00:16:49,040
 the Buddha was around, there

313
00:16:49,040 --> 00:16:54,510
 was a lot of good in India. And it lasted for some time and

314
00:16:54,510 --> 00:16:56,600
 then India went back and

315
00:16:56,600 --> 00:17:00,070
 now it's an ordinary country again. But for some time in

316
00:17:00,070 --> 00:17:01,880
 India, it was a special place

317
00:17:01,880 --> 00:17:06,240
 and it was very Buddhist and there was very little killing

318
00:17:06,240 --> 00:17:09,320
 and so on. From what we understand,

319
00:17:09,320 --> 00:17:11,990
 they're just an example, but the point is that it's imper

320
00:17:11,990 --> 00:17:13,440
manent and no matter what good

321
00:17:13,440 --> 00:17:16,370
 things you do, even the Buddha's teaching is impermanent.

322
00:17:16,370 --> 00:17:17,880
 It's not going to last forever

323
00:17:17,880 --> 00:17:21,270
 and this is a teaching of a perfectly enlightened Buddha as

324
00:17:21,270 --> 00:17:25,160
 we understand. So, you know, most

325
00:17:25,160 --> 00:17:30,250
 important is to become free and to become free from karma

326
00:17:30,250 --> 00:17:33,720
 as well, to not have any attachments

327
00:17:33,720 --> 00:17:36,650
 that things should turn out in a certain way, not to be

328
00:17:36,650 --> 00:17:38,600
 expecting or have expectations about

329
00:17:38,600 --> 00:17:45,680
 the future, to simply be content and in tune with reality,

330
00:17:45,680 --> 00:17:49,040
 to see reality for what it is

331
00:17:49,040 --> 00:17:53,630
 and to be at peace with that and to not cling and to not

332
00:17:53,630 --> 00:17:56,400
 want and to not hope and care and

333
00:17:56,400 --> 00:18:03,130
 worry and so on. But to live one's life in a dynamic way

334
00:18:03,130 --> 00:18:06,760
 where you can accept and react

335
00:18:06,760 --> 00:18:10,890
 and respond appropriately to every situation doesn't mean

336
00:18:10,890 --> 00:18:13,080
 that you live in one place and

337
00:18:13,080 --> 00:18:16,750
 do nothing, sit around and do nothing. It means that you're

338
00:18:16,750 --> 00:18:18,760
 able to live dynamically.

339
00:18:18,760 --> 00:18:23,660
 You're not a stick in the mud. When it's time to move, can

340
00:18:23,660 --> 00:18:26,800
't move. Your mind is able to

341
00:18:26,800 --> 00:18:33,740
 respond appropriately to all experiences and react without

342
00:18:33,740 --> 00:18:38,120
 attachment. So, you know, fairly

343
00:18:38,120 --> 00:18:41,720
 detailed answers to your questions. I hope they did hit the

344
00:18:41,720 --> 00:18:43,880
 mark to at least to some extent.

345
00:18:43,880 --> 00:18:46,200
 So thanks for the questions. All the best.

