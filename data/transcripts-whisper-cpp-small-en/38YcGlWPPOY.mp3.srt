1
00:00:00,000 --> 00:00:04,120
 Hello, Bante. From some of your videos, I have seen that

2
00:00:04,120 --> 00:00:06,520
 you have said that not sleeping

3
00:00:06,520 --> 00:00:11,570
 is healthy. I'm confused, really. Isn't sleeping for at

4
00:00:11,570 --> 00:00:13,840
 least a few hours a day? For if yours

5
00:00:13,840 --> 00:00:18,460
 per day very useful for both physical and mental health,

6
00:00:18,460 --> 00:00:21,000
 please explain. In most cases,

7
00:00:21,000 --> 00:00:27,370
 it's really a function of your state of mind. If your mind

8
00:00:27,370 --> 00:00:31,120
 is in a chaotic state, then you

9
00:00:31,120 --> 00:00:36,290
 need more sleep to take time out and to calm down. On the

10
00:00:36,290 --> 00:00:41,880
 other hand, the sleep state of a

11
00:00:41,880 --> 00:00:51,040
 mind that's in chaos is less restful. Even more so because

12
00:00:51,040 --> 00:00:56,000
 the sleep state of a mind that is chaotic

13
00:00:56,000 --> 00:01:03,980
 is the mind that is distracted, the mind that is turbulent.

14
00:01:03,980 --> 00:01:07,640
 The sleep state of such a mind is

15
00:01:09,320 --> 00:01:14,800
 less restful. It's actually exponential in a sense, or it's

16
00:01:14,800 --> 00:01:17,240
 not just an inverse correlation.

17
00:01:17,240 --> 00:01:25,260
 It's not one for one. A person whose mind is calm, first of

18
00:01:25,260 --> 00:01:27,480
 all, needs less sleep because they need

19
00:01:27,480 --> 00:01:31,750
 less time to recuperate from the mental activities and even

20
00:01:31,750 --> 00:01:34,000
 the physical activities, which are

21
00:01:34,000 --> 00:01:38,150
 generally more restful and more calm and more peaceful.

22
00:01:38,150 --> 00:01:42,800
 Also, their rest time is more restful,

23
00:01:42,800 --> 00:01:49,760
 so they need exponentially, to some extent, less sleep as a

24
00:01:49,760 --> 00:01:56,520
 result. There's no one answer. You

25
00:01:56,520 --> 00:02:00,280
 can't say more sleep is better or it's important to get so

26
00:02:00,280 --> 00:02:03,960
 many hours of sleep or more sleep

27
00:02:03,960 --> 00:02:07,390
 is always better for everyone, less sleep is always better

28
00:02:07,390 --> 00:02:09,480
 for everyone. But a restful mind

29
00:02:09,480 --> 00:02:14,030
 is better for everyone and a restful mind needs less sleep

30
00:02:14,030 --> 00:02:16,720
 exponentially. If one's mind is

31
00:02:16,720 --> 00:02:25,520
 perfectly balanced and one's situation is such that one

32
00:02:25,520 --> 00:02:30,300
 doesn't engage in activities that will

33
00:02:30,300 --> 00:02:35,580
 disturb that sense of calm, then there might be no need to

34
00:02:35,580 --> 00:02:38,280
 sleep at all. In the case of Chakupala,

35
00:02:38,280 --> 00:02:42,780
 this monk who didn't sleep for three months, that's not a

36
00:02:42,780 --> 00:02:47,000
 unique case. It's actually a practice

37
00:02:47,000 --> 00:02:50,750
 where people will not sleep for many days. But you have to

38
00:02:50,750 --> 00:02:52,600
 understand that they're in a state

39
00:02:52,600 --> 00:02:56,170
 where their minds are so peaceful, so clear, so calm that

40
00:02:56,170 --> 00:02:58,380
 they don't really need to sleep.

41
00:02:58,380 --> 00:03:03,290
 What would you need to sleep for if your mind and your body

42
00:03:03,290 --> 00:03:07,400
 are rested? The body doesn't need to

43
00:03:07,400 --> 00:03:12,490
 lie down necessarily if it's calm, if it's in a restful, if

44
00:03:12,490 --> 00:03:15,360
 you train it to be in a restful state,

45
00:03:15,360 --> 00:03:19,880
 you'll be able to sit still and so on. And certainly the

46
00:03:19,880 --> 00:03:23,720
 mind doesn't either, the brain doesn't either

47
00:03:23,720 --> 00:03:28,310
 if the mind is calm. So it has all to do with one's state

48
00:03:28,310 --> 00:03:29,320
 of mind.

