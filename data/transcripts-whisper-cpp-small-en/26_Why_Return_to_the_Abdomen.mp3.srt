1
00:00:00,000 --> 00:00:03,360
 Why return to the abdomen?

2
00:00:03,360 --> 00:00:07,750
 Why should one return to the abdomen rather than moving

3
00:00:07,750 --> 00:00:11,200
 from one object to another as they arise?

4
00:00:11,200 --> 00:00:15,450
 It is not intrinsically wrong to note one object after

5
00:00:15,450 --> 00:00:17,200
 another randomly.

6
00:00:17,200 --> 00:00:21,170
 In practice, however, this tends to lead to a sense of

7
00:00:21,170 --> 00:00:22,560
 anticipation.

8
00:00:22,560 --> 00:00:26,700
 Waiting for the next object to arrive is detrimental to

9
00:00:26,700 --> 00:00:28,640
 objective awareness.

10
00:00:28,640 --> 00:00:33,710
 The meditator is therefore instructed to note one object

11
00:00:33,710 --> 00:00:35,760
 until it disappears,

12
00:00:35,760 --> 00:00:40,560
 then return to the abdomen until another object takes one's

13
00:00:40,560 --> 00:00:41,760
 attention.

14
00:00:41,760 --> 00:00:45,360
 As the movements of the abdomen are reliably present,

15
00:00:45,360 --> 00:00:50,050
 they provide a good base to observe the present reality at

16
00:00:50,050 --> 00:00:51,200
 all times.

