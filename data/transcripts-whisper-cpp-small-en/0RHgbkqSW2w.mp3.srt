1
00:00:00,000 --> 00:00:10,360
 Hi. Today I thought I would talk about some of the things

2
00:00:10,360 --> 00:00:13,880
 that meditation isn't. Now I get a lot of

3
00:00:13,880 --> 00:00:22,990
 questions about what to do when X arises or descriptions of

4
00:00:22,990 --> 00:00:27,520
 experiences in meditation which

5
00:00:27,520 --> 00:00:33,030
 seem to be special or somehow outside of the normal course

6
00:00:33,030 --> 00:00:37,320
 of meditation and that's outside of what

7
00:00:37,320 --> 00:00:42,220
 we should expect one to deal with in an ordinary fashion.

8
00:00:42,220 --> 00:00:45,280
 Often what people experience are things

9
00:00:45,280 --> 00:00:50,700
 like lights or colors or pictures, visions. Sometimes

10
00:00:50,700 --> 00:00:53,840
 people will experience very quiet,

11
00:00:53,840 --> 00:00:59,360
 calm states or very focused and intense states, blissful

12
00:00:59,360 --> 00:01:03,360
 states, states of rapture, similar as

13
00:01:03,360 --> 00:01:08,860
 you would find in other religious traditions. Sometimes

14
00:01:08,860 --> 00:01:12,440
 there will be the arising of knowledge

15
00:01:12,440 --> 00:01:17,340
 on a mundane level, I mean understand things about one's

16
00:01:17,340 --> 00:01:20,320
 life or so on. Sometimes it will be great

17
00:01:20,320 --> 00:01:25,830
 levels of energy or kind of like a static electricity that

18
00:01:25,830 --> 00:01:29,120
 arises in the mind, all sorts

19
00:01:29,120 --> 00:01:32,520
 of many special states or states which could be labeled as

20
00:01:32,520 --> 00:01:34,800
 special. And so I just wanted to deal

21
00:01:34,800 --> 00:01:39,220
 with them here briefly sort of to give a bit of a warning

22
00:01:39,220 --> 00:01:42,040
 to people not regarding these states

23
00:01:42,040 --> 00:01:44,640
 themselves because there's nothing dangerous about any of

24
00:01:44,640 --> 00:01:46,200
 these states in and of themselves,

25
00:01:46,200 --> 00:01:51,270
 but a warning not to get misled by these states and that

26
00:01:51,270 --> 00:01:54,040
 thinking that they are indeed somehow

27
00:01:54,040 --> 00:01:59,520
 special, somehow higher, somehow the way, the path or the

28
00:01:59,520 --> 00:02:03,880
 goal of meditation. Because whatever

29
00:02:03,880 --> 00:02:07,550
 special state arises it in the end will eventually

30
00:02:07,550 --> 00:02:10,760
 disappear, will go away. So if we hold on to it

31
00:02:10,760 --> 00:02:14,690
 as being special in whatever way, then it's going to lead

32
00:02:14,690 --> 00:02:17,440
 us to an imbalance in our perception and

33
00:02:17,440 --> 00:02:21,320
 in our acceptance of reality, when in fact these states are

34
00:02:21,320 --> 00:02:24,480
 all simply natural, ordinary states of

35
00:02:24,480 --> 00:02:28,300
 mind. It's just that they're not ordinary to people who

36
00:02:28,300 --> 00:02:30,960
 haven't practiced meditation. If you haven't

37
00:02:30,960 --> 00:02:36,150
 practiced meditation, these states will seem to be somehow

38
00:02:36,150 --> 00:02:39,440
 outside of mundane reality. It's just

39
00:02:39,440 --> 00:02:44,010
 that in fact the reason that we don't experience them is

40
00:02:44,010 --> 00:02:47,200
 that our ordinary states of reality are

41
00:02:47,200 --> 00:02:50,530
 not meditative states of reality. That's the only reason.

42
00:02:50,530 --> 00:02:52,520
 When these things come up, they are

43
00:02:52,520 --> 00:02:55,880
 ordinary. They're ordinary to meditative state and they

44
00:02:55,880 --> 00:02:58,640
 should thus be treated as any other

45
00:02:58,640 --> 00:03:03,130
 meditative experience. So just as when we have say pain in

46
00:03:03,130 --> 00:03:05,200
 the legs or so on, we would say to

47
00:03:05,200 --> 00:03:09,880
 ourselves pain, pain and just treat it as what it is. The

48
00:03:09,880 --> 00:03:13,960
 same when we see visions or when we obtain

49
00:03:13,960 --> 00:03:16,730
 special, what we would call special experiences. So when we

50
00:03:16,730 --> 00:03:18,080
 see visions we should say to ourselves

51
00:03:18,080 --> 00:03:20,620
 seeing as if we were seeing something ordinary in our

52
00:03:20,620 --> 00:03:23,200
 everyday life. Just say to ourselves seeing,

53
00:03:23,200 --> 00:03:27,700
 seeing, seeing until the vision goes away. Just using it as

54
00:03:27,700 --> 00:03:30,160
 an object of meditation and coming to

55
00:03:30,160 --> 00:03:33,440
 see it clearly as it is. If we feel happy, states of bliss

56
00:03:33,440 --> 00:03:35,800
 or so on, we can say to ourselves happy,

57
00:03:35,800 --> 00:03:40,020
 happy, blissful. If we feel calm or quiet, we can say to

58
00:03:40,020 --> 00:03:43,240
 ourselves calm, calm. Just reminding ourselves

59
00:03:43,240 --> 00:03:48,070
 of exactly what's going on so that we don't get caught up

60
00:03:48,070 --> 00:03:51,400
 and become entranced or enchanted by

61
00:03:51,400 --> 00:03:56,520
 these simple ordinary meditation states that occur for most

62
00:03:56,520 --> 00:03:59,280
 people. I think this is one of the

63
00:04:00,520 --> 00:04:05,610
 most often asked questions that I get and so I think it's

64
00:04:05,610 --> 00:04:08,880
 probably worth it to make this sort

65
00:04:08,880 --> 00:04:11,960
 of a video to explain what's going on here. That these

66
00:04:11,960 --> 00:04:14,040
 states they come from meditation,

67
00:04:14,040 --> 00:04:16,910
 specifically from mostly coming from high states of

68
00:04:16,910 --> 00:04:19,720
 concentration and it's a perfectly natural thing

69
00:04:19,720 --> 00:04:23,930
 to occur. There's no reason to be afraid of them but there

70
00:04:23,930 --> 00:04:27,040
 is certainly no reason to become attached

71
00:04:27,040 --> 00:04:30,510
 to them or to make more of them than they actually are and

72
00:04:30,510 --> 00:04:33,400
 to think that somehow they are the path

73
00:04:33,400 --> 00:04:36,520
 that we should follow and pursue and encourage and develop.

74
00:04:36,520 --> 00:04:38,600
 They certainly aren't. The path that we

75
00:04:38,600 --> 00:04:42,320
 should follow and pursue and encourage and develop is the

76
00:04:42,320 --> 00:04:44,920
 acceptance, the awareness and the

77
00:04:44,920 --> 00:04:49,810
 acknowledgement of things, the clear awareness and

78
00:04:49,810 --> 00:04:54,160
 understanding of things as they are. Whatever

79
00:04:54,160 --> 00:04:57,720
 arises we see it for what it is and not liking it or disl

80
00:04:57,720 --> 00:05:02,240
iking it or becoming partial or intoxicated

81
00:05:02,240 --> 00:05:05,740
 by it. Of course that's exactly what happens with these

82
00:05:05,740 --> 00:05:08,960
 special states if we're not careful. For

83
00:05:08,960 --> 00:05:12,720
 this reason I think it's important to warn everybody not

84
00:05:12,720 --> 00:05:14,880
 that something dangerous is going

85
00:05:14,880 --> 00:05:18,390
 to happen. It is possible truly that if you follow after

86
00:05:18,390 --> 00:05:20,640
 any state it can lead you to states of

87
00:05:20,640 --> 00:05:24,370
 insanity. You could drive yourself crazy chasing after and

88
00:05:24,370 --> 00:05:27,360
 developing these states. People who

89
00:05:27,360 --> 00:05:32,160
 develop special states of trance or so on can actually, I

90
00:05:32,160 --> 00:05:34,640
 mean there are cases where they would

91
00:05:34,640 --> 00:05:37,850
 actually so to speak drive themselves crazy. There's

92
00:05:37,850 --> 00:05:40,040
 nothing dangerous with the states in

93
00:05:40,040 --> 00:05:43,450
 and of themselves. It's the attitude of the meditator that

94
00:05:43,450 --> 00:05:45,280
 becomes so wound up and so caught

95
00:05:45,280 --> 00:05:50,360
 up and so interested and keen and attached to something

96
00:05:50,360 --> 00:05:53,040
 that it ends up releasing in an

97
00:05:53,040 --> 00:05:56,840
 explosion of temporary insanity. There is that warning and

98
00:05:56,840 --> 00:05:58,800
 mostly what's going to happen instead

99
00:05:58,800 --> 00:06:01,320
 is people will just become afraid and think that it's

100
00:06:01,320 --> 00:06:03,400
 something too much or they don't understand

101
00:06:03,400 --> 00:06:06,910
 it. These things should be understood as they are, that

102
00:06:06,910 --> 00:06:11,320
 there's nothing dangerous or special in these

103
00:06:11,320 --> 00:06:14,190
 things in and of themselves and we shouldn't treat them

104
00:06:14,190 --> 00:06:16,680
 like that or for sure we lead to suffering

105
00:06:16,680 --> 00:06:20,130
 and discontent when they disappear or suffering when we

106
00:06:20,130 --> 00:06:23,280
 chase after them and therefore drive

107
00:06:23,280 --> 00:06:26,550
 ourselves crazy or any number of things could happen.

108
00:06:26,550 --> 00:06:29,840
 Thanks for tuning in. This is just a

109
00:06:29,840 --> 00:06:34,170
 little bit of explanation on what the meditation is not. It

110
00:06:34,170 --> 00:06:36,640
's not any special state that we might

111
00:06:36,640 --> 00:06:40,200
 experience. It's the clear awareness and understanding of

112
00:06:40,200 --> 00:06:42,560
 all states and the impartiality which comes

113
00:06:42,560 --> 00:06:47,330
 there from. Thanks for tuning in and hope to see you all

114
00:06:47,330 --> 00:06:50,040
 commenting and sending your comments in.

115
00:06:50,040 --> 00:06:52,040
 Thanks for tuning in. Talk to you later.

116
00:06:52,040 --> 00:07:22,040
 [

