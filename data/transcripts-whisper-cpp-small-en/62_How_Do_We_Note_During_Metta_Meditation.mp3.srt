1
00:00:00,000 --> 00:00:03,840
 How do we note during Metta Meditation?

2
00:00:03,840 --> 00:00:07,520
 In the practice of Metta Meditation, the mantra is used to

3
00:00:07,520 --> 00:00:08,960
 evoke friendliness.

4
00:00:08,960 --> 00:00:12,200
 So you would say to yourself something like, "May all

5
00:00:12,200 --> 00:00:13,520
 beings be happy."

6
00:00:13,520 --> 00:00:16,880
 In Vipassana Meditation, the mantra is used to evoke a

7
00:00:16,880 --> 00:00:18,320
 strong recognition,

8
00:00:18,320 --> 00:00:22,400
 Tirasana, of experiential reality. This process is

9
00:00:22,400 --> 00:00:24,160
 sometimes referred to as

10
00:00:24,160 --> 00:00:28,140
 "noting the object of experience." Since Metta Meditation

11
00:00:28,140 --> 00:00:30,000
 focuses on sentient beings,

12
00:00:30,000 --> 00:00:33,920
 which are conceptual, it is not really practical to combine

13
00:00:33,920 --> 00:00:37,040
 noting concurrently with Metta Meditation.

14
00:00:37,040 --> 00:00:40,170
 Technically, it is possible that when you are practicing M

15
00:00:40,170 --> 00:00:41,440
etta Meditation,

16
00:00:41,440 --> 00:00:44,790
 to switch to Vipassana Meditation and focus on the four

17
00:00:44,790 --> 00:00:46,960
 foundations of mindfulness,

18
00:00:46,960 --> 00:00:50,160
 but this is not the point of Metta Meditation.

19
00:00:50,160 --> 00:00:54,550
 Mahasi Sayadaw talked about how when giving gifts, some

20
00:00:54,550 --> 00:00:56,720
 people will insist on noting,

21
00:00:56,720 --> 00:01:00,880
 moving, placing, as they give something. He pointed out

22
00:01:00,880 --> 00:01:03,600
 that such focus is not the purpose of giving.

23
00:01:03,600 --> 00:01:06,920
 When someone gives a gift, they should make a determination

24
00:01:06,920 --> 00:01:07,280
.

25
00:01:07,280 --> 00:01:11,340
 For example, "May this gift be a support for my attainment

26
00:01:11,340 --> 00:01:12,560
 of enlightenment."

27
00:01:12,560 --> 00:01:16,710
 At the time when you are making a determination, it should

28
00:01:16,710 --> 00:01:19,120
 not be with mindfulness of ultimate

29
00:01:19,120 --> 00:01:22,710
 reality. It should be with full intention that the gift is

30
00:01:22,710 --> 00:01:25,040
 for the benefit of the recipient

31
00:01:25,040 --> 00:01:28,600
 and for the benefit of one's own mind, that one may become

32
00:01:28,600 --> 00:01:30,160
 free from suffering.

33
00:01:30,160 --> 00:01:35,550
 Charity, like Metta practice, is a conceptual practice. In

34
00:01:35,550 --> 00:01:37,520
 theory, one should be always

35
00:01:37,520 --> 00:01:41,340
 practicing Vipassana Meditation, but in order to get to the

36
00:01:41,340 --> 00:01:44,000
 level where such a state is possible,

37
00:01:44,000 --> 00:01:48,720
 practices like Metta, as well as charity and morality, are

38
00:01:48,720 --> 00:01:50,480
 of great practical benefit

39
00:01:50,480 --> 00:01:54,080
 and should be practiced independent of Vipassana Meditation

40
00:01:54,080 --> 00:01:54,480
.

