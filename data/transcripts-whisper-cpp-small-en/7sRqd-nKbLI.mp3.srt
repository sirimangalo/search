1
00:00:00,000 --> 00:00:05,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,000 --> 00:00:10,490
 Tonight we continue on with verse number 77, which reads as

3
00:00:10,490 --> 00:00:12,000
 follows.

4
00:00:12,000 --> 00:00:20,000
 "Ovadeya nusa seya asa bhajan niwariya"

5
00:00:20,000 --> 00:00:26,000
 "Satan hi so peyo hoti asatan ho te apiyo"

6
00:00:26,000 --> 00:00:42,220
 which means instruct and teach and prevent or forbid in

7
00:00:42,220 --> 00:00:45,000
 regards to what is harmful.

8
00:00:45,000 --> 00:00:50,000
 "Asa bhaj" which means not beautiful.

9
00:00:50,000 --> 00:00:56,310
 If you do this, "Satan hi so peyo hoti" such a person, in

10
00:00:56,310 --> 00:00:58,000
 fact it's not saying you do it, it's like one should.

11
00:00:58,000 --> 00:01:03,100
 One should teach, one should preach, one should instruct,

12
00:01:03,100 --> 00:01:05,000
 one should admonish,

13
00:01:05,000 --> 00:01:11,280
 and one should forbid in regards to what is harmful and

14
00:01:11,280 --> 00:01:13,000
 what is not beautiful.

15
00:01:13,000 --> 00:01:17,770
 Not beautiful is a way of saying, not wholesome, not

16
00:01:17,770 --> 00:01:20,000
 related to goodness.

17
00:01:20,000 --> 00:01:27,440
 The one who does so will be peyo, will be beloved, satan of

18
00:01:27,440 --> 00:01:32,000
 those who are mindful or of the good.

19
00:01:32,000 --> 00:01:34,000
 Maybe it just means of the good.

20
00:01:34,000 --> 00:01:39,300
 "Satan ho te apiyo" and they will be not beloved, unbeloved

21
00:01:39,300 --> 00:01:45,000
, they will be not loved, not liked by the bad, by the evil.

22
00:01:45,000 --> 00:01:48,000
 "Asa tam"

23
00:01:48,000 --> 00:01:53,410
 So this was actually quite an interesting verse and another

24
00:01:53,410 --> 00:01:57,690
 one of these aspects of the Buddhist teaching that teaches

25
00:01:57,690 --> 00:02:02,000
 its unique piece of the puzzle of the Buddhist teaching.

26
00:02:02,000 --> 00:02:06,910
 But it was told in regards to two disciples, we are told of

27
00:02:06,910 --> 00:02:12,000
 the two chief disciples who are Sariputta and Moggallana.

28
00:02:12,000 --> 00:02:17,510
 And even though they had ordained from the two chief

29
00:02:17,510 --> 00:02:23,840
 disciples themselves, they were "alaji" means they were

30
00:02:23,840 --> 00:02:26,000
 without shame.

31
00:02:26,000 --> 00:02:29,000
 They were bad mugs.

32
00:02:29,000 --> 00:02:37,870
 Like the Buddha said, even though a spoon dwells in the

33
00:02:37,870 --> 00:02:41,000
 soup, it stays with the soup.

34
00:02:41,000 --> 00:02:44,000
 You put a spoon in a cup of soup.

35
00:02:44,000 --> 00:02:47,000
 We had this verse already, right?

36
00:02:47,000 --> 00:02:50,290
 A spoon in a cup of soup, it can be there for a hundred

37
00:02:50,290 --> 00:02:53,000
 years, it will never taste the soup.

38
00:02:53,000 --> 00:02:58,930
 And so it is with someone who is foolish and doesn't absorb

39
00:02:58,930 --> 00:03:02,000
 the teachings of the wise.

40
00:03:02,000 --> 00:03:05,000
 They can be surrounded by wise people.

41
00:03:05,000 --> 00:03:09,040
 Even be right next to Sariputta and Moggallana, what we

42
00:03:09,040 --> 00:03:16,470
 wouldn't do to have that opportunity and waste the

43
00:03:16,470 --> 00:03:18,000
 opportunity.

44
00:03:18,000 --> 00:03:21,000
 They just wasted it.

45
00:03:21,000 --> 00:03:23,000
 What did they waste it doing?

46
00:03:23,000 --> 00:03:27,710
 Apparently they went off with their followers and planted

47
00:03:27,710 --> 00:03:32,000
 flowers and flowering trees and many other things besides.

48
00:03:32,000 --> 00:03:35,900
 But they did all sorts of things that are considered to be

49
00:03:35,900 --> 00:03:39,240
 wastes of time and moreover harmful to one's spiritual

50
00:03:39,240 --> 00:03:40,000
 development

51
00:03:40,000 --> 00:03:45,170
 because they cultivate this sense of desire and attachment

52
00:03:45,170 --> 00:03:47,000
 and passion and so on.

53
00:03:47,000 --> 00:03:50,000
 Yes, planting flowers does create attachment.

54
00:03:50,000 --> 00:03:53,000
 Our spirituality goes higher than beauty.

55
00:03:53,000 --> 00:03:54,000
 It goes beyond beauty.

56
00:03:54,000 --> 00:04:01,110
 It goes to the ultimate, the highest, which is objectivity,

57
00:04:01,110 --> 00:04:05,090
 which is this universal, this ability to understand the

58
00:04:05,090 --> 00:04:06,000
 universe,

59
00:04:06,000 --> 00:04:09,000
 which is quite beautiful in itself but it has nothing to do

60
00:04:09,000 --> 00:04:12,700
 with the appreciation of the smell of a flower or something

61
00:04:12,700 --> 00:04:13,000
.

62
00:04:13,000 --> 00:04:19,710
 Anyway, so they went off and did this and word came back to

63
00:04:19,710 --> 00:04:26,000
 the Buddha because they were acting inappropriately

64
00:04:26,000 --> 00:04:28,410
 and they were probably doing things to ingratiate

65
00:04:28,410 --> 00:04:30,000
 themselves with the lay people.

66
00:04:30,000 --> 00:04:34,490
 So it made it hard for proper monks to live there because

67
00:04:34,490 --> 00:04:38,000
 of course ordinary people who were probably not well versed

68
00:04:38,000 --> 00:04:40,000
 in the Buddha's teaching,

69
00:04:40,000 --> 00:04:43,360
 they would appreciate, the monks would pick flowers and

70
00:04:43,360 --> 00:04:45,000
 bring them to the lay people and people were like,

71
00:04:45,000 --> 00:04:47,800
 "Oh, look at these people bringing us flowers. That must be

72
00:04:47,800 --> 00:04:49,000
 what monks do."

73
00:04:49,000 --> 00:04:51,630
 So when the monks came with their downcast eyes, they would

74
00:04:51,630 --> 00:04:54,000
 say, "Who are these guys? They don't have flowers.

75
00:04:54,000 --> 00:04:57,260
 They aren't fun. They aren't singing and dancing and

76
00:04:57,260 --> 00:05:01,570
 playing with our children and talking to us about the

77
00:05:01,570 --> 00:05:06,000
 weather and so on and so on."

78
00:05:06,000 --> 00:05:09,430
 And they wouldn't run errands. The monks would run errands

79
00:05:09,430 --> 00:05:12,540
 for them because the monks didn't have jobs and that's a

80
00:05:12,540 --> 00:05:13,000
 big no-no.

81
00:05:13,000 --> 00:05:16,000
 I mean for very much the same reason, well for many reasons

82
00:05:16,000 --> 00:05:22,000
, but one because the monks were practicing meditation

83
00:05:22,000 --> 00:05:25,000
 and are expecting to run errands for the lay people.

84
00:05:25,000 --> 00:05:28,650
 Then there's a sense that you have to work for the food

85
00:05:28,650 --> 00:05:31,000
 that people give you, "Oh, hey, I'm giving you this food.

86
00:05:31,000 --> 00:05:35,190
 You'd better send this letter to this person or do this for

87
00:05:35,190 --> 00:05:37,000
 me or do that for me."

88
00:05:37,000 --> 00:05:42,020
 And then when are they going to practice meditation or

89
00:05:42,020 --> 00:05:46,000
 study the Buddha's teachings and so on?

90
00:05:46,000 --> 00:05:47,990
 So they did all these things that made it difficult for

91
00:05:47,990 --> 00:05:51,000
 other monks to live there because they just couldn't

92
00:05:51,000 --> 00:05:53,000
 compete with this.

93
00:05:53,000 --> 00:05:54,640
 And people weren't well versed enough in the Buddha's

94
00:05:54,640 --> 00:05:58,000
 teaching to understand that it wasn't really of any benefit

95
00:05:58,000 --> 00:06:00,000
 to have these monks running errands for them.

96
00:06:00,000 --> 00:06:04,000
 So bringing them flowers.

97
00:06:04,000 --> 00:06:07,490
 But the Buddha called Sariputta and Moggallana. He heard

98
00:06:07,490 --> 00:06:12,000
 about this and called them up and he said, "Go teach them.

99
00:06:12,000 --> 00:06:16,900
 And if they don't listen to you, kick them out. Expell them

100
00:06:16,900 --> 00:06:18,000
 from the order.

101
00:06:18,000 --> 00:06:24,240
 But if they do listen to you, then bring them back and

102
00:06:24,240 --> 00:06:26,000
 teach them."

103
00:06:26,000 --> 00:06:30,000
 Because often it's just because of a poor leader.

104
00:06:30,000 --> 00:06:35,600
 If you get the wrong person following you, you follow the

105
00:06:35,600 --> 00:06:37,000
 wrong person.

106
00:06:37,000 --> 00:06:40,420
 See, you can imagine they had like 500 followers each, I

107
00:06:40,420 --> 00:06:41,000
 think.

108
00:06:41,000 --> 00:06:43,000
 And you can imagine how that happened.

109
00:06:43,000 --> 00:06:46,610
 "Hey, I'm a student of... Oh, yes. Who are you? Oh, yes. I

110
00:06:46,610 --> 00:06:49,000
'm a student of Sariputta. I'm a student of Moggallana."

111
00:06:49,000 --> 00:06:53,160
 Like, "Oh, wow. You must be really a good meditator. You

112
00:06:53,160 --> 00:06:57,000
 must be a great teacher. Let's follow you."

113
00:06:57,000 --> 00:07:00,720
 And it makes me think of in modern days we get this as well

114
00:07:00,720 --> 00:07:01,000
.

115
00:07:01,000 --> 00:07:03,130
 You know, I'm a student of Ajahn Tong, Sri Moggall and

116
00:07:03,130 --> 00:07:06,620
 people are like, "Oh, yes. You must be a great monk because

117
00:07:06,620 --> 00:07:08,000
 he's a great teacher."

118
00:07:08,000 --> 00:07:11,000
 But it doesn't always hold true.

119
00:07:11,000 --> 00:07:15,020
 I think sometimes people will use this to their advantage.

120
00:07:15,020 --> 00:07:18,000
 They'll use a teacher's name.

121
00:07:18,000 --> 00:07:21,750
 You'll find this in spirituality. Especially Buddhism is

122
00:07:21,750 --> 00:07:23,000
 quite common.

123
00:07:23,000 --> 00:07:27,000
 "I'm a student of the Dalai Lama. Oh, you must be..."

124
00:07:27,000 --> 00:07:29,000
 Well, it doesn't mean anything really.

125
00:07:29,000 --> 00:07:34,050
 Ajahn Tong has many, many, many, many students of varying

126
00:07:34,050 --> 00:07:37,000
 degrees of enlightenment.

127
00:07:37,000 --> 00:07:41,510
 So don't go by such things. You have to at least take such

128
00:07:41,510 --> 00:07:44,290
 things with a grain of salt because it may tell you their

129
00:07:44,290 --> 00:07:45,000
 tradition,

130
00:07:45,000 --> 00:07:47,750
 but it probably doesn't tell you their spiritual... level

131
00:07:47,750 --> 00:07:49,000
 of spiritual attainment.

132
00:07:49,000 --> 00:07:52,500
 It may, if they're bragging about it a lot, it may show

133
00:07:52,500 --> 00:07:56,000
 that they're not a very high spiritual attainment.

134
00:07:56,000 --> 00:07:59,540
 But at any rate, so there were many, many monks following

135
00:07:59,540 --> 00:08:01,000
 these two corrupt monks,

136
00:08:01,000 --> 00:08:04,380
 and many of them would have just been misled, like the lay

137
00:08:04,380 --> 00:08:07,000
 people, into thinking that that's how monks should act.

138
00:08:07,000 --> 00:08:11,180
 And so the two chief disciples were instructed to go, and

139
00:08:11,180 --> 00:08:16,000
 they went to see and did exactly that before they went.

140
00:08:16,000 --> 00:08:20,430
 The Buddha explained to them why or how they should treat

141
00:08:20,430 --> 00:08:22,000
 this situation.

142
00:08:22,000 --> 00:08:27,000
 He said, "Kaccha tango."

143
00:08:27,000 --> 00:08:35,350
 There's something that he said. "Uvadantohi anusasanto ap

144
00:08:35,350 --> 00:08:41,000
andita nam yewa apyohoti amanapo."

145
00:08:41,000 --> 00:08:45,580
 Just basically what the Dhammapada quote says, it says, "

146
00:08:45,580 --> 00:08:49,000
Those who are not wives, those who are not pandita,

147
00:08:49,000 --> 00:08:55,000
 you will become unbeloved to them and undeer to them."

148
00:08:55,000 --> 00:08:59,280
 "Amanapo, unpleasing, it will be displeasing to them, but

149
00:08:59,280 --> 00:09:01,000
 pandita nam pana."

150
00:09:01,000 --> 00:09:06,980
 "But for the wise ones, piyohoti manapo, you will be dear,

151
00:09:06,980 --> 00:09:15,000
 and for one will be dear, one will be pleasing to them."

152
00:09:15,000 --> 00:09:17,000
 And then he told this verse.

153
00:09:17,000 --> 00:09:19,490
 So that's the story. And then they went, and some of them

154
00:09:19,490 --> 00:09:20,000
 listened, some of them didn't.

155
00:09:20,000 --> 00:09:25,440
 Some of them left the monastic life thinking, "What? You've

156
00:09:25,440 --> 00:09:28,000
 got to meditate? You've got to keep all these rules?"

157
00:09:28,000 --> 00:09:33,010
 That's not what I signed up for. And some of them were

158
00:09:33,010 --> 00:09:35,000
 expelled.

159
00:09:35,000 --> 00:09:39,000
 So how does this relate to our practice?

160
00:09:39,000 --> 00:09:44,010
 Well, the first interesting aspect is in regards to what is

161
00:09:44,010 --> 00:09:47,000
 proper practice and what is not.

162
00:09:47,000 --> 00:09:53,000
 In Buddhism, it's a very, fairly straight and narrow path.

163
00:09:53,000 --> 00:09:57,000
 There's not a lot of leeway, not a lot of wiggle room.

164
00:09:57,000 --> 00:10:00,350
 Like you can't somehow pretend that planting flowers is

165
00:10:00,350 --> 00:10:02,000
 part of the Dhammapada.

166
00:10:02,000 --> 00:10:06,560
 Well, people, I think, try to listening to music, these

167
00:10:06,560 --> 00:10:08,000
 kind of things.

168
00:10:08,000 --> 00:10:09,000
 I mean, that's very straight and narrow.

169
00:10:09,000 --> 00:10:13,440
 Most spirituality includes music, includes beauty, art,

170
00:10:13,440 --> 00:10:15,000
 these kind of things.

171
00:10:15,000 --> 00:10:17,870
 But not the Buddhist path, because we're going so much

172
00:10:17,870 --> 00:10:19,000
 higher than that.

173
00:10:19,000 --> 00:10:22,830
 It's like a science, really. Which I think will probably

174
00:10:22,830 --> 00:10:24,000
 turn some people off.

175
00:10:24,000 --> 00:10:29,000
 But that's the way this quote, what this quote shows us.

176
00:10:29,000 --> 00:10:34,000
 "A satan hoti abhiyo"

177
00:10:34,000 --> 00:10:37,460
 It will not be beloved. It will not be pleasing to those

178
00:10:37,460 --> 00:10:39,000
 who are not mindful,

179
00:10:39,000 --> 00:10:48,000
 who are not headed towards the truth.

180
00:10:48,000 --> 00:10:50,000
 Because we require absolute objectivity.

181
00:10:50,000 --> 00:10:52,970
 In order to understand the truth, you have to be strict

182
00:10:52,970 --> 00:10:54,000
 with yourself.

183
00:10:54,000 --> 00:10:57,000
 You have to be clear that you're going to be objective.

184
00:10:57,000 --> 00:11:03,000
 A flower can be of no more value to you than a pile of dung

185
00:11:03,000 --> 00:11:03,000
,

186
00:11:03,000 --> 00:11:05,000
 because you want to understand the truth of them.

187
00:11:05,000 --> 00:11:09,880
 If you turn your nose up at the pile of dung and stick your

188
00:11:09,880 --> 00:11:11,000
 nose in the flower,

189
00:11:11,000 --> 00:11:13,710
 forget about being objective. Forget about understanding

190
00:11:13,710 --> 00:11:14,000
 the truth.

191
00:11:14,000 --> 00:11:17,000
 It's not possible. Same goes with music.

192
00:11:17,000 --> 00:11:21,040
 Music and the sound of nails on the chalkboard have to be

193
00:11:21,040 --> 00:11:23,000
 of equal value.

194
00:11:23,000 --> 00:11:26,640
 You can't value, you can't place value judgments, because

195
00:11:26,640 --> 00:11:28,000
 that's subjective.

196
00:11:28,000 --> 00:11:32,140
 It's not objective anymore. It forbids you, prevents you

197
00:11:32,140 --> 00:11:34,000
 from seeing the truth.

198
00:11:34,000 --> 00:11:37,500
 But those who understand this, those who are wise and are

199
00:11:37,500 --> 00:11:40,000
 really keen on truth and understanding,

200
00:11:40,000 --> 00:11:44,000
 they will appreciate it. They will appreciate such things.

201
00:11:44,000 --> 00:11:47,000
 They'll be interested in such things.

202
00:11:47,000 --> 00:11:52,120
 That's important. It's an important thing to make known at

203
00:11:52,120 --> 00:11:53,000
 the get-go,

204
00:11:53,000 --> 00:11:57,520
 so people don't have these wrong ideas and end up bad-

205
00:11:57,520 --> 00:11:59,000
nothing

206
00:11:59,000 --> 00:12:02,750
 or end up being disappointed and being upset that they were

207
00:12:02,750 --> 00:12:05,000
 tricked into being objective.

208
00:12:05,000 --> 00:12:12,000
 But the other thing is in regards to instruction.

209
00:12:12,000 --> 00:12:15,000
 And it's something that we have to remember ourselves.

210
00:12:15,000 --> 00:12:18,790
 Sometimes when we don't like the instruction, it's not the

211
00:12:18,790 --> 00:12:20,000
 instructor's fault.

212
00:12:20,000 --> 00:12:24,000
 Sometimes it is. Sometimes the instructor is teaching that,

213
00:12:24,000 --> 00:12:27,000
 which is harmful.

214
00:12:27,000 --> 00:12:29,350
 Of course, this wouldn't have been the case with Sariputta

215
00:12:29,350 --> 00:12:30,000
 and Mughalana,

216
00:12:30,000 --> 00:12:33,410
 but it can be that a teacher can teach something that's

217
00:12:33,410 --> 00:12:34,000
 harmful,

218
00:12:34,000 --> 00:12:39,280
 which is what's wrong with this idea that all teachers are

219
00:12:39,280 --> 00:12:40,000
 equal.

220
00:12:40,000 --> 00:12:42,000
 Sorry, all teachings are equal.

221
00:12:42,000 --> 00:12:46,240
 So people say, "All religions teach the same thing," which

222
00:12:46,240 --> 00:12:47,000
 is not really true,

223
00:12:47,000 --> 00:12:50,760
 because a religion could easily be made up that teaches

224
00:12:50,760 --> 00:12:52,000
 terrible things.

225
00:12:52,000 --> 00:12:54,000
 Same goes with teachers.

226
00:12:54,000 --> 00:13:00,310
 But this quote is kind of unspoken here, but the opposite

227
00:13:00,310 --> 00:13:01,000
 holds true.

228
00:13:01,000 --> 00:13:06,760
 That which is harmful, that which leads one away from the

229
00:13:06,760 --> 00:13:09,000
 truth, leads one to delusion,

230
00:13:09,000 --> 00:13:13,940
 would be quite pleasing to those who are asatam, those who

231
00:13:13,940 --> 00:13:16,000
 are not on a good path.

232
00:13:16,000 --> 00:13:20,660
 And it would be pleasing, it would be displeasing to those

233
00:13:20,660 --> 00:13:23,000
 who are on the right path.

234
00:13:25,000 --> 00:13:30,000
 And so he said, "Well, how do I know what's right then?"

235
00:13:30,000 --> 00:13:33,460
 This is the danger of just taking things based on your

236
00:13:33,460 --> 00:13:35,000
 likes and dislikes.

237
00:13:35,000 --> 00:13:38,790
 The whole idea of follow your heart, that we often ascribe

238
00:13:38,790 --> 00:13:40,000
 to in the West.

239
00:13:40,000 --> 00:13:44,000
 Follow your heart or listen to your gut feeling.

240
00:13:44,000 --> 00:13:49,580
 There's no reason to listen to your stomach or your intest

241
00:13:49,580 --> 00:13:52,000
ines for that matter.

242
00:13:53,000 --> 00:13:55,000
 Or your heart for that matter.

243
00:13:55,000 --> 00:13:59,610
 It's interesting, the same saying exists in Asia, in

244
00:13:59,610 --> 00:14:02,000
 Thailand it's "tam jai."

245
00:14:02,000 --> 00:14:04,000
 "Tam jai" means follow your heart.

246
00:14:04,000 --> 00:14:07,780
 But the funny thing is, "tam jai" is usually considered to

247
00:14:07,780 --> 00:14:09,000
 be a criticism.

248
00:14:09,000 --> 00:14:12,000
 "Oh, konan tam jai tala."

249
00:14:12,000 --> 00:14:15,260
 Or it can be used, you say, "Oh, that person just follows

250
00:14:15,260 --> 00:14:17,000
 their heart all the time."

251
00:14:17,000 --> 00:14:20,000
 As an insult, not an insult criticism.

252
00:14:20,000 --> 00:14:23,750
 Because they don't use wisdom, they don't use mindfulness

253
00:14:23,750 --> 00:14:25,000
 or discernment.

254
00:14:25,000 --> 00:14:27,000
 They just follow their heart.

255
00:14:27,000 --> 00:14:29,690
 Which we think, "Oh, that's a good thing, follow your heart

256
00:14:29,690 --> 00:14:30,000
."

257
00:14:30,000 --> 00:14:33,000
 Not in Buddhism.

258
00:14:33,000 --> 00:14:37,000
 Or they'll say, "Konan tam jai lu."

259
00:14:37,000 --> 00:14:42,730
 They just follow the hearts of their children, which is

260
00:14:42,730 --> 00:14:44,000
 also a problem.

261
00:14:44,000 --> 00:14:46,070
 From a parental point of view, you can't just follow the

262
00:14:46,070 --> 00:14:47,000
 hearts of your children.

263
00:14:47,000 --> 00:14:51,770
 Heart here means emotional or the mind or the intention of

264
00:14:51,770 --> 00:14:53,000
 the person.

265
00:14:53,000 --> 00:14:55,000
 So you can't just follow your intentions.

266
00:14:55,000 --> 00:14:56,000
 You have to use discernment.

267
00:14:56,000 --> 00:14:58,000
 In fact, you shouldn't trust yourself.

268
00:14:58,000 --> 00:15:00,000
 We've gotten ourselves into this mess.

269
00:15:00,000 --> 00:15:05,000
 Any suffering you have, any mental problems, any bad habits

270
00:15:05,000 --> 00:15:06,000
, you have addictions.

271
00:15:06,000 --> 00:15:08,000
 Well, you did it to yourself.

272
00:15:08,000 --> 00:15:10,000
 So why would you trust your mind?

273
00:15:10,000 --> 00:15:12,000
 Why would you listen to your mind?

274
00:15:12,000 --> 00:15:16,000
 The untrained mind is your worst enemy, the Buddha said.

275
00:15:16,000 --> 00:15:20,340
 Of course, the plus side is that the trained mind is your

276
00:15:20,340 --> 00:15:22,000
 greatest friend.

277
00:15:22,000 --> 00:15:26,000
 But that's the truth, is you have to train your mind.

278
00:15:26,000 --> 00:15:30,630
 And this verse doesn't give us any indication of how to

279
00:15:30,630 --> 00:15:32,000
 know what is the truth and what is not.

280
00:15:32,000 --> 00:15:34,000
 It's not talking about that.

281
00:15:34,000 --> 00:15:38,250
 It's talking about, specifically it's talking about when

282
00:15:38,250 --> 00:15:39,000
 you know the truth.

283
00:15:39,000 --> 00:15:41,000
 Don't be afraid to say it.

284
00:15:41,000 --> 00:15:43,000
 And don't be worried that people are going to be upset.

285
00:15:43,000 --> 00:15:45,000
 That's the third thing that this does.

286
00:15:45,000 --> 00:15:49,610
 But before we get to that, just talking about it tells you

287
00:15:49,610 --> 00:15:51,000
 not to follow your heart,

288
00:15:51,000 --> 00:15:54,000
 not to follow what you like and dislike.

289
00:15:54,000 --> 00:15:56,000
 That's what I wanted to point out.

290
00:15:56,000 --> 00:15:59,250
 That's the second point, is that you have to follow your

291
00:15:59,250 --> 00:16:01,000
 wisdom, you have to follow knowledge.

292
00:16:01,000 --> 00:16:03,000
 You can't circumvent that.

293
00:16:03,000 --> 00:16:06,010
 There's no gut feeling that's going to tell you what's

294
00:16:06,010 --> 00:16:07,000
 right or not.

295
00:16:07,000 --> 00:16:09,000
 You have to see it clearly for yourself.

296
00:16:09,000 --> 00:16:12,400
 And until you see it clearly, you're very distrustful of

297
00:16:12,400 --> 00:16:14,000
 your heart and your mind.

298
00:16:14,000 --> 00:16:16,000
 Because they will trick you.

299
00:16:16,000 --> 00:16:18,000
 They're not yours.

300
00:16:18,000 --> 00:16:23,990
 They're based on causes and effects that are complex,

301
00:16:23,990 --> 00:16:27,000
 sometimes helpful, sometimes harmful.

302
00:16:27,000 --> 00:16:31,090
 Sometimes helpful, but helpful in leading you towards a

303
00:16:31,090 --> 00:16:32,000
 dead end.

304
00:16:32,000 --> 00:16:34,000
 So it seems, oh, this is working just fine.

305
00:16:34,000 --> 00:16:38,400
 And then you smash up against them all, bricks, metaphor

306
00:16:38,400 --> 00:16:40,000
ically speaking.

307
00:16:40,000 --> 00:16:44,400
 But the third point is when you do know the truth, which is

308
00:16:44,400 --> 00:16:47,000
 what the Buddha is really saying,

309
00:16:47,000 --> 00:16:51,010
 "Owa deya nusa se ya" teach, instruct, don't be afraid,

310
00:16:51,010 --> 00:16:54,000
 just because people don't like it.

311
00:16:54,000 --> 00:16:57,790
 But the Buddha often said, don't teach if you know the

312
00:16:57,790 --> 00:17:00,000
 person is not going to listen.

313
00:17:00,000 --> 00:17:02,320
 Sometimes you have to be careful if you're just going to

314
00:17:02,320 --> 00:17:03,000
 upset them.

315
00:17:03,000 --> 00:17:05,640
 But I think the point here is that there was a whole group

316
00:17:05,640 --> 00:17:06,000
 of monks,

317
00:17:06,000 --> 00:17:14,000
 and it was like what you call triage, triage in medicine.

318
00:17:14,000 --> 00:17:18,000
 Don't worry too much about those who you can't say.

319
00:17:18,000 --> 00:17:20,000
 Don't worry about those who you can say.

320
00:17:20,000 --> 00:17:23,280
 And if you don't do anything, all these monks are lost, all

321
00:17:23,280 --> 00:17:25,000
 these meditators are lost.

322
00:17:25,000 --> 00:17:27,000
 If you do something, well, some of them will listen.

323
00:17:27,000 --> 00:17:29,000
 So think about those monks.

324
00:17:29,000 --> 00:17:33,000
 And the rest, you just have to let go.

325
00:17:33,000 --> 00:17:35,680
 And so the Buddha told them in this specific case to go and

326
00:17:35,680 --> 00:17:37,000
 teach all these people.

327
00:17:37,000 --> 00:17:39,510
 It doesn't mean go out and preach to people, and then when

328
00:17:39,510 --> 00:17:40,000
 they get angry, say,

329
00:17:40,000 --> 00:17:43,000
 "Ah, that's your fault. You're just a useless person."

330
00:17:43,000 --> 00:17:45,000
 No, there's a time and a place to teach.

331
00:17:45,000 --> 00:17:47,580
 And if you're teaching and people aren't getting anything

332
00:17:47,580 --> 00:17:48,000
 out of it,

333
00:17:48,000 --> 00:17:51,000
 then you are at fault as a teacher.

334
00:17:51,000 --> 00:17:54,940
 It means either your teaching is terrible, or it means you

335
00:17:54,940 --> 00:17:56,000
're teaching it.

336
00:17:56,000 --> 00:18:00,050
 You don't understand yet how to teach, who to teach, when

337
00:18:00,050 --> 00:18:01,000
 to teach.

338
00:18:01,000 --> 00:18:05,290
 So don't always just go and teach, but it means don't be

339
00:18:05,290 --> 00:18:06,000
 afraid.

340
00:18:06,000 --> 00:18:10,070
 And if a person doesn't understand the teaching, don't give

341
00:18:10,070 --> 00:18:12,000
 up the teaching.

342
00:18:12,000 --> 00:18:16,390
 Just understand that, "Well, I'm not perfect, so I can't

343
00:18:16,390 --> 00:18:18,000
 know exactly who's ready

344
00:18:18,000 --> 00:18:22,000
 to understand the teaching." Obviously, that person wasn't.

345
00:18:22,000 --> 00:18:24,000
 It doesn't mean I should give up.

346
00:18:24,000 --> 00:18:28,000
 There's no reason to think that the teaching's at fault,

347
00:18:28,000 --> 00:18:30,000
 or the truth is any different.

348
00:18:30,000 --> 00:18:33,380
 It doesn't mean it's not true. It just means that person is

349
00:18:33,380 --> 00:18:35,000
 ready for it, potentially.

350
00:18:35,000 --> 00:18:38,000
 But again, you have to be sure.

351
00:18:38,000 --> 00:18:40,850
 You can't just say, "Well, I think this is right, and this

352
00:18:40,850 --> 00:18:42,000
 is my teaching,

353
00:18:42,000 --> 00:18:50,000
 and therefore they all can get lost if they don't like it."

354
00:18:50,000 --> 00:18:53,000
 No, don't follow your heart. Follow the truth.

355
00:18:53,000 --> 00:18:57,000
 So interesting teaching, quite useful for meditators.

356
00:18:57,000 --> 00:19:00,000
 And one more verse.

357
00:19:00,000 --> 00:19:02,000
 So thank you all for tuning in.

358
00:19:02,000 --> 00:19:05,000
 That's the teaching today.

359
00:19:05,000 --> 00:19:07,000
 Keep practicing and be well.

