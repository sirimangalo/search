1
00:00:00,000 --> 00:00:03,900
 Hello and welcome back to Ask a Monk.

2
00:00:03,900 --> 00:00:08,660
 Today I'll be answering a question in regards to physical

3
00:00:08,660 --> 00:00:14,580
 exercise and whether it is important

4
00:00:14,580 --> 00:00:20,580
 to give up or is it proper to give up physical exercise and

5
00:00:20,580 --> 00:00:23,920
 concern about the body's well-being

6
00:00:23,920 --> 00:00:26,720
 and health and welfare?

7
00:00:26,720 --> 00:00:29,670
 Or is it important for us to look after the body and

8
00:00:29,670 --> 00:00:32,000
 therefore important for us to engage

9
00:00:32,000 --> 00:00:33,880
 in physical exercise?

10
00:00:33,880 --> 00:00:37,480
 I think basically this is the question.

11
00:00:37,480 --> 00:00:42,720
 What role does physical exercise play in the life of, say,

12
00:00:42,720 --> 00:00:45,240
 an enlightened being, to put

13
00:00:45,240 --> 00:00:47,800
 it one way?

14
00:00:47,800 --> 00:00:50,280
 I think I have a pretty definite answer on this and that is

15
00:00:50,280 --> 00:00:51,440
 that it doesn't play any

16
00:00:51,440 --> 00:00:57,080
 part, the idea of outright physical exercise.

17
00:00:57,080 --> 00:01:03,040
 It seems to me a little bit silly really, as I think it

18
00:01:03,040 --> 00:01:06,440
 does for anyone who has taken

19
00:01:06,440 --> 00:01:15,960
 up a lifestyle where we eat food simply enough to live.

20
00:01:15,960 --> 00:01:21,870
 This may seem a little bit odd for most people because most

21
00:01:21,870 --> 00:01:23,640
 of us think that that's how we

22
00:01:23,640 --> 00:01:24,640
 eat.

23
00:01:24,640 --> 00:01:27,920
 We all eat simply enough to live, but really we don't.

24
00:01:27,920 --> 00:01:32,450
 I think in general we eat far more than is necessary to

25
00:01:32,450 --> 00:01:35,040
 live, but the amount that we

26
00:01:35,040 --> 00:01:38,510
 eat is generally necessary to keep up our lifestyles, which

27
00:01:38,510 --> 00:01:40,200
 tend to include things like

28
00:01:40,200 --> 00:01:44,790
 physical exercise, physical exertion, and a lot of mental

29
00:01:44,790 --> 00:01:47,360
 stress, a lot of emotional stress

30
00:01:47,360 --> 00:01:48,360
 and so on.

31
00:01:48,360 --> 00:01:53,560
 So as a result of that, the food is barely adequate.

32
00:01:53,560 --> 00:02:00,600
 For many people, the food is in fact is far too much.

33
00:02:00,600 --> 00:02:04,500
 There are people who become obese or who get sicknesses

34
00:02:04,500 --> 00:02:06,880
 because of the fact that they're

35
00:02:06,880 --> 00:02:11,760
 eating beyond their needs and their requirements.

36
00:02:11,760 --> 00:02:17,850
 But even for those people who are eating enough and what

37
00:02:17,850 --> 00:02:21,400
 seems to be enough to live by, it

38
00:02:21,400 --> 00:02:27,120
 means enough to live that lifestyle by a lifestyle of one

39
00:02:27,120 --> 00:02:31,200
 who is well built and has a good shape.

40
00:02:31,200 --> 00:02:34,640
 I remember thinking how silly it was the first time I

41
00:02:34,640 --> 00:02:36,800
 thought of this question when I've

42
00:02:36,800 --> 00:02:39,320
 been one of the meditators.

43
00:02:39,320 --> 00:02:44,640
 He became a monk and I think after he was a monk he was

44
00:02:44,640 --> 00:02:47,560
 talking about how as a monk

45
00:02:47,560 --> 00:02:51,810
 he felt kind of sad that he couldn't do the same things he

46
00:02:51,810 --> 00:02:53,360
 did as a lay person.

47
00:02:53,360 --> 00:02:56,630
 He said when he was meditating as a lay person in our

48
00:02:56,630 --> 00:02:59,240
 monastery, this was in Wat Doy Sootep

49
00:02:59,240 --> 00:03:03,600
 in Chiang Mai, Thailand, every day he would go running I

50
00:03:03,600 --> 00:03:06,400
 think or jogging down the steps.

51
00:03:06,400 --> 00:03:09,600
 This is a very large staircase, a thousand steps or

52
00:03:09,600 --> 00:03:11,840
 something and then jogging all the

53
00:03:11,840 --> 00:03:13,120
 way back up every day.

54
00:03:13,120 --> 00:03:14,520
 I said why would you do that?

55
00:03:14,520 --> 00:03:16,520
 He said I can't do that.

56
00:03:16,520 --> 00:03:18,520
 I said why would you do that?

57
00:03:18,520 --> 00:03:21,800
 I wanted to keep in shape.

58
00:03:21,800 --> 00:03:22,960
 It just struck me as so odd.

59
00:03:22,960 --> 00:03:25,250
 I think it's something that most people wouldn't find funny

60
00:03:25,250 --> 00:03:26,560
 but I found it incredibly funny

61
00:03:26,560 --> 00:03:31,160
 and I said to him, what shape do you want to be?

62
00:03:31,160 --> 00:03:32,920
 A square shape?

63
00:03:32,920 --> 00:03:33,920
 A round shape?

64
00:03:33,920 --> 00:03:36,040
 A triangle shape?

65
00:03:36,040 --> 00:03:39,720
 Is there some specific shape that you want to keep in?

66
00:03:39,720 --> 00:03:43,570
 And obviously we understand this expression and what it

67
00:03:43,570 --> 00:03:45,680
 means but if you think about it

68
00:03:45,680 --> 00:03:47,720
 it really is meaningless.

69
00:03:47,720 --> 00:03:50,790
 I mean what's the difference between this shape and that

70
00:03:50,790 --> 00:03:51,400
 shape?

71
00:03:51,400 --> 00:03:54,930
 The obvious answer being that our attachment to the body

72
00:03:54,930 --> 00:03:56,680
 leads us to wish for it to be

73
00:03:56,680 --> 00:04:00,200
 in a certain shape and our attachment to sensual pleasure

74
00:04:00,200 --> 00:04:02,200
 and romance and so on leads us to

75
00:04:02,200 --> 00:04:06,200
 want to have a body that is attractive to others as well.

76
00:04:06,200 --> 00:04:10,600
 And this is the only reason for wanting to be in a certain

77
00:04:10,600 --> 00:04:10,960
 shape.

78
00:04:10,960 --> 00:04:15,710
 But okay, so this is one part of the argument but the claim

79
00:04:15,710 --> 00:04:18,160
 that people make is that this

80
00:04:18,160 --> 00:04:20,480
 is necessary for your physical well-being.

81
00:04:20,480 --> 00:04:24,830
 But as I said, it's necessary if you want to eat the amount

82
00:04:24,830 --> 00:04:27,040
 of food that you're eating.

83
00:04:27,040 --> 00:04:30,590
 If a person eats a lot of food then I guess it will be

84
00:04:30,590 --> 00:04:33,200
 necessary and they go hand in hand.

85
00:04:33,200 --> 00:04:36,680
 If a person wants to exercise and keep in good shape then

86
00:04:36,680 --> 00:04:38,080
 they have to eat a lot.

87
00:04:38,080 --> 00:04:41,800
 If a person wants to eat a lot and wants to eat rich foods

88
00:04:41,800 --> 00:04:44,000
 and wants to eat all day, as

89
00:04:44,000 --> 00:04:46,910
 monks and meditators we only eat once in the morning so it

90
00:04:46,910 --> 00:04:48,920
 seems really ridiculous to consider

91
00:04:48,920 --> 00:04:53,440
 doing any real exercise.

92
00:04:53,440 --> 00:04:56,060
 But if you want to eat a lot and if you have a lifestyle

93
00:04:56,060 --> 00:04:57,720
 where you eat three times a day

94
00:04:57,720 --> 00:05:02,220
 or four times a day or have all sorts of snacks and so on

95
00:05:02,220 --> 00:05:05,720
 then yeah, you really need to exercise

96
00:05:05,720 --> 00:05:10,710
 because your body is being polluted by all of these toxins

97
00:05:10,710 --> 00:05:12,800
 and excess of even excess

98
00:05:12,800 --> 00:05:15,400
 of good things.

99
00:05:15,400 --> 00:05:18,380
 So it's really only, in short it's really only because of

100
00:05:18,380 --> 00:05:19,880
 our lifestyle that we think

101
00:05:19,880 --> 00:05:25,600
 that somehow exercise is going to be somehow necessary or a

102
00:05:25,600 --> 00:05:27,640
 part of our lives.

103
00:05:27,640 --> 00:05:31,800
 If you're meditating and especially if you're doing walking

104
00:05:31,800 --> 00:05:34,080
 meditation or just being mindful

105
00:05:34,080 --> 00:05:36,890
 in your daily life and doing all your activities with

106
00:05:36,890 --> 00:05:38,920
 mindfulness then your body will be in

107
00:05:38,920 --> 00:05:42,510
 such a state that the blood is able to flow and the

108
00:05:42,510 --> 00:05:45,760
 digestive system is able to work properly

109
00:05:45,760 --> 00:05:49,810
 and you'll find it incredible how healthy you are and how

110
00:05:49,810 --> 00:05:51,800
 little sickness you have in

111
00:05:51,800 --> 00:05:53,960
 the body that your body is fit.

112
00:05:53,960 --> 00:05:58,910
 I know monks that are 80, 90 years old and still quite fit

113
00:05:58,910 --> 00:06:01,200
 and able because they're also

114
00:06:01,200 --> 00:06:05,270
 good meditators and because of their meditation practice

115
00:06:05,270 --> 00:06:08,400
 they're able to keep themselves in

116
00:06:08,400 --> 00:06:13,800
 good shape or in a good physical condition.

117
00:06:13,800 --> 00:06:19,450
 I think there's room for physical exercise if you're sick

118
00:06:19,450 --> 00:06:22,120
 or if your body is out of its

119
00:06:22,120 --> 00:06:23,840
 ordinary condition.

120
00:06:23,840 --> 00:06:26,990
 It might be a necessary part of your rehabilitation to

121
00:06:26,990 --> 00:06:30,560
 undergo exercise even if a person is obese.

122
00:06:30,560 --> 00:06:33,410
 Obesity can be a real problem because it can lead to high

123
00:06:33,410 --> 00:06:35,040
 blood pressure, it can lead to

124
00:06:35,040 --> 00:06:38,240
 whatever high cholesterol, I don't know what exactly.

125
00:06:38,240 --> 00:06:43,630
 There are a lot of sicknesses, diabetes and so on comes

126
00:06:43,630 --> 00:06:45,320
 from obesity.

127
00:06:45,320 --> 00:06:48,440
 In that case it might be necessary and advisable for you to

128
00:06:48,440 --> 00:06:50,840
 undertake this sort of thing temporarily

129
00:06:50,840 --> 00:06:53,620
 while you're overcoming your condition and at the same time

130
00:06:53,620 --> 00:06:55,080
 undertaking a more reasonable

131
00:06:55,080 --> 00:06:56,520
 lifestyle.

132
00:06:56,520 --> 00:06:59,520
 Because ultimately that's what's the most important.

133
00:06:59,520 --> 00:07:02,620
 Taking care of your body means not putting things into it

134
00:07:02,620 --> 00:07:04,400
 that are going to pollute it,

135
00:07:04,400 --> 00:07:06,200
 that are going to make it unhealthy.

136
00:07:06,200 --> 00:07:09,610
 Because it's like a car, you have to be careful of the gas

137
00:07:09,610 --> 00:07:12,440
 and you have to make sure you have

138
00:07:12,440 --> 00:07:17,750
 high quality gasoline and you have engine oil all the time

139
00:07:17,750 --> 00:07:20,200
 and so on and that you take

140
00:07:20,200 --> 00:07:21,200
 care of it.

141
00:07:21,200 --> 00:07:24,220
 Taking care of your body is far more important than

142
00:07:24,220 --> 00:07:27,040
 something like exercise and in fact exercise

143
00:07:27,040 --> 00:07:32,180
 can have the effect of weakening the body or stressing the

144
00:07:32,180 --> 00:07:34,200
 body in some sense.

145
00:07:34,200 --> 00:07:39,600
 People say that it's good for cardiovascular health if you

146
00:07:39,600 --> 00:07:41,640
 do jogging or so on.

147
00:07:41,640 --> 00:07:45,160
 I don't know if I would exactly dispute that but I do know

148
00:07:45,160 --> 00:07:46,800
 that a person who meditates

149
00:07:46,800 --> 00:07:49,340
 doesn't have a problem with cardiovascular health, doesn't

150
00:07:49,340 --> 00:07:50,560
 have a problem with digestive

151
00:07:50,560 --> 00:07:54,110
 health, doesn't have a problem with obesity and so on if

152
00:07:54,110 --> 00:07:56,320
 they're undertaking meditation

153
00:07:56,320 --> 00:07:58,120
 in the meditator's lifestyle.

154
00:07:58,120 --> 00:08:01,080
 We can all eat, most of us can eat a lot less, I can

155
00:08:01,080 --> 00:08:03,280
 because I eat very little as it is,

156
00:08:03,280 --> 00:08:05,880
 I eat just enough to live.

157
00:08:05,880 --> 00:08:10,050
 And I've come to realize that that's really a proper way to

158
00:08:10,050 --> 00:08:12,220
 live, just enough that if

159
00:08:12,220 --> 00:08:15,550
 I didn't eat this much it would be difficult for me to walk

160
00:08:15,550 --> 00:08:17,080
 up the stairs every day.

161
00:08:17,080 --> 00:08:20,260
 But if I eat this much then okay I can walk up the stairs

162
00:08:20,260 --> 00:08:21,920
 and I can walk around and I

163
00:08:21,920 --> 00:08:26,590
 can have energy to teach and energy to do this and that and

164
00:08:26,590 --> 00:08:29,040
 energy to do walking meditation.

165
00:08:29,040 --> 00:08:31,560
 And that's enough for me.

166
00:08:31,560 --> 00:08:36,450
 If I eat more then I become actually quite lethargic and

167
00:08:36,450 --> 00:08:39,520
 you feel unhealthy having gotten

168
00:08:39,520 --> 00:08:46,160
 used to this lifestyle of eating just enough to survive.

169
00:08:46,160 --> 00:08:50,150
 And one final note is that in regards to this idea that

170
00:08:50,150 --> 00:08:53,160
 certain exercises might be important

171
00:08:53,160 --> 00:08:57,510
 to maintain a certain level of physical health is that I

172
00:08:57,510 --> 00:09:00,120
 wouldn't really take that as a very

173
00:09:00,120 --> 00:09:01,120
 good excuse.

174
00:09:01,120 --> 00:09:04,330
 I mean I would say in that case if people thought that

175
00:09:04,330 --> 00:09:06,360
 somehow their health was going

176
00:09:06,360 --> 00:09:10,640
 to be affected if they didn't exercise, their diet was okay

177
00:09:10,640 --> 00:09:13,640
 but they still felt that cardiovascular

178
00:09:13,640 --> 00:09:19,250
 health or this health or some kind of system in the body

179
00:09:19,250 --> 00:09:22,560
 would be benefited by exercise

180
00:09:22,560 --> 00:09:24,240
 of some sort.

181
00:09:24,240 --> 00:09:27,630
 I would give the same answer as I give to people who ask

182
00:09:27,630 --> 00:09:30,160
 whether yoga is a good idea,

183
00:09:30,160 --> 00:09:31,360
 stretching is a good idea.

184
00:09:31,360 --> 00:09:34,330
 Some people will say yoga is a spiritual practice and then

185
00:09:34,330 --> 00:09:35,920
 I've given an answer that if it's

186
00:09:35,920 --> 00:09:38,950
 a spiritual practice which it should be considered then you

187
00:09:38,950 --> 00:09:40,520
're following that spiritual path

188
00:09:40,520 --> 00:09:43,390
 and I don't think it's the same spiritual path as the

189
00:09:43,390 --> 00:09:44,640
 Buddha taught.

190
00:09:44,640 --> 00:09:48,360
 But if you're just undertaking yoga for the purpose of

191
00:09:48,360 --> 00:09:50,480
 stretching then what you're actually

192
00:09:50,480 --> 00:09:54,080
 getting involved in is this attachment to the body which I

193
00:09:54,080 --> 00:09:55,640
 think is part of this question

194
00:09:55,640 --> 00:09:58,840
 in regards to exercising.

195
00:09:58,840 --> 00:10:03,310
 Is that if it becomes an important part of your life that

196
00:10:03,310 --> 00:10:05,960
 your body should be in a healthy

197
00:10:05,960 --> 00:10:09,340
 condition and so therefore you go out of your way to do

198
00:10:09,340 --> 00:10:11,520
 things simply for the purpose of

199
00:10:11,520 --> 00:10:16,310
 keeping the body in a heightened state of health or some

200
00:10:16,310 --> 00:10:18,640
 perfectly healthy state then

201
00:10:18,640 --> 00:10:22,330
 you're really missing a part of the Buddha's teaching that

202
00:10:22,330 --> 00:10:24,080
 is the physical body is not

203
00:10:24,080 --> 00:10:27,290
 going to last forever and our attachment to the physical

204
00:10:27,290 --> 00:10:29,040
 body is only going to cause us

205
00:10:29,040 --> 00:10:31,700
 suffering because eventually it's going to break and it's

206
00:10:31,700 --> 00:10:33,800
 going to fail and break down.

207
00:10:33,800 --> 00:10:35,200
 Eventually we're going to have to let it go.

208
00:10:35,200 --> 00:10:39,150
 So if it comes to just doing the wise things that keep your

209
00:10:39,150 --> 00:10:41,360
 body healthy like eating right

210
00:10:41,360 --> 00:10:45,680
 and making movements like doing the walking meditation, not

211
00:10:45,680 --> 00:10:47,760
 simply sitting and lying down

212
00:10:47,760 --> 00:10:53,400
 all day but actually walking, doing some simple walking to

213
00:10:53,400 --> 00:10:56,480
 keep the body moving then there's

214
00:10:56,480 --> 00:10:57,480
 really no problem.

215
00:10:57,480 --> 00:11:00,630
 When it comes to doing something like intentional

216
00:11:00,630 --> 00:11:03,600
 stretching like yoga and stretching yourself

217
00:11:03,600 --> 00:11:10,200
 out then it seems to me more in the line of the non-accept

218
00:11:10,200 --> 00:11:13,600
ance of the current state of

219
00:11:13,600 --> 00:11:18,520
 the body where if you have aches and pains instead of

220
00:11:18,520 --> 00:11:21,480
 coming to accept and bear with

221
00:11:21,480 --> 00:11:26,040
 and eventually really overcome these things you try to work

222
00:11:26,040 --> 00:11:27,840
 your way out of them.

223
00:11:27,840 --> 00:11:31,170
 You try to get yourself into a state where those

224
00:11:31,170 --> 00:11:33,040
 experiences don't arise.

225
00:11:33,040 --> 00:11:36,080
 The same goes with someone who wants to do jogging because

226
00:11:36,080 --> 00:11:37,400
 they say "I feel great after

227
00:11:37,400 --> 00:11:41,410
 I jog and I feel great when I build up my muscles and I

228
00:11:41,410 --> 00:11:43,440
 feel great" and so on.

229
00:11:43,440 --> 00:11:47,920
 This freedom from any kind of sickness.

230
00:11:47,920 --> 00:11:51,050
 Another example is people who instead of just eating

231
00:11:51,050 --> 00:11:53,600
 ordinary food they have very specialized

232
00:11:53,600 --> 00:11:56,240
 diets because they know this is good for you.

233
00:11:56,240 --> 00:12:01,040
 I know quite a few people like this who have known people

234
00:12:01,040 --> 00:12:04,080
 who have taken these very special

235
00:12:04,080 --> 00:12:08,200
 diets and then a couple of them died of cancer.

236
00:12:08,200 --> 00:12:10,440
 Three people I know of that all died of cancer.

237
00:12:10,440 --> 00:12:15,090
 So in the end it wasn't really of any benefit to them at

238
00:12:15,090 --> 00:12:15,440
 all.

239
00:12:15,440 --> 00:12:18,650
 You could say in some sense it gave them pleasure and it

240
00:12:18,650 --> 00:12:21,100
 gave them a sense of health and well-being

241
00:12:21,100 --> 00:12:25,670
 but in the end that only became a disadvantage because when

242
00:12:25,670 --> 00:12:28,020
 they're dying of cancer they

243
00:12:28,020 --> 00:12:33,630
 have to see the opposite effect regardless of what they do

244
00:12:33,630 --> 00:12:36,520
 their body is deteriorating

245
00:12:36,520 --> 00:12:40,400
 and that's really the nature of reality.

246
00:12:40,400 --> 00:12:43,340
 We should not be overly concerned with our diet, we should

247
00:12:43,340 --> 00:12:44,800
 be overly concerned with our

248
00:12:44,800 --> 00:12:48,310
 health, we should try to minimize the impact that these

249
00:12:48,310 --> 00:12:50,040
 things have on our lives.

250
00:12:50,040 --> 00:12:56,350
 Not eating too much, not getting working too much, working

251
00:12:56,350 --> 00:12:59,120
 out really at all unless it's

252
00:12:59,120 --> 00:13:02,920
 say necessary for your job and of course a lot of this is

253
00:13:02,920 --> 00:13:05,680
 more inclined towards the lifestyle

254
00:13:05,680 --> 00:13:06,680
 of a monk.

255
00:13:06,680 --> 00:13:09,910
 Living in your life, I'm not saying it's unethical to do

256
00:13:09,910 --> 00:13:12,720
 physical exercise, I'm just saying my

257
00:13:12,720 --> 00:13:18,230
 advice as a monk living in the forest, in a cave, is that

258
00:13:18,230 --> 00:13:21,200
 it's not really an important

259
00:13:21,200 --> 00:13:24,290
 part of our lives and if we're looking to go further on

260
00:13:24,290 --> 00:13:26,120
 this path we really should work

261
00:13:26,120 --> 00:13:30,450
 towards giving it up and giving up all the things that

262
00:13:30,450 --> 00:13:32,960
 surround exercise and physical

263
00:13:32,960 --> 00:13:34,240
 health and so on.

264
00:13:34,240 --> 00:13:38,570
 I've often mentioned just as a final parting note food for

265
00:13:38,570 --> 00:13:40,880
 thought that it would be really

266
00:13:40,880 --> 00:13:44,280
 interesting I think to get some sort of sickness.

267
00:13:44,280 --> 00:13:48,190
 I think even now that if I had cancer that would be a

268
00:13:48,190 --> 00:13:51,200
 really unique experience to have.

269
00:13:51,200 --> 00:13:53,950
 People who are always trying to avoid these things are

270
00:13:53,950 --> 00:13:55,920
 really missing the point and missing

271
00:13:55,920 --> 00:13:58,240
 out on something quite incredible.

272
00:13:58,240 --> 00:14:01,760
 The experience of the die of cancer would I think be an

273
00:14:01,760 --> 00:14:04,200
 incredible chance and opportunity

274
00:14:04,200 --> 00:14:07,880
 for us to come to understand the nature of reality.

275
00:14:07,880 --> 00:14:11,080
 It's not what we think it is, it's not this wonderful body

276
00:14:11,080 --> 00:14:12,760
 and how we can make it strong

277
00:14:12,760 --> 00:14:13,760
 and so on.

278
00:14:13,760 --> 00:14:20,840
 It's actually quite fluid and uncertain and changing and it

279
00:14:20,840 --> 00:14:23,320
's quite dynamic.

280
00:14:23,320 --> 00:14:27,280
 You could be dead tomorrow, it's not certain.

281
00:14:27,280 --> 00:14:30,270
 If you're not able to experience the whole range of reality

282
00:14:30,270 --> 00:14:32,000
, the whole range of experience

283
00:14:32,000 --> 00:14:34,840
 then you're really going to get stuck and fall into

284
00:14:34,840 --> 00:14:35,600
 suffering.

285
00:14:35,600 --> 00:14:39,100
 We should try to condition ourselves out of this or take us

286
00:14:39,100 --> 00:14:40,840
 out of this conditioning so

287
00:14:40,840 --> 00:14:45,210
 that we are in a condition to accept the whole range of

288
00:14:45,210 --> 00:14:47,760
 experience which includes sickness,

289
00:14:47,760 --> 00:14:50,720
 which includes old age, which includes death.

290
00:14:50,720 --> 00:14:53,860
 And if we can do that then where could we be born, where

291
00:14:53,860 --> 00:14:55,560
 could we arise, where could

292
00:14:55,560 --> 00:14:57,970
 we go, what could we meet with that would ever give us

293
00:14:57,970 --> 00:14:59,560
 problems, that would ever cause

294
00:14:59,560 --> 00:15:00,560
 us stress and suffering.

295
00:15:00,560 --> 00:15:04,960
 If we can just come to accept things as they are and to

296
00:15:04,960 --> 00:15:08,080
 interact with reality rather than

297
00:15:08,080 --> 00:15:13,020
 react to it and just be, then there's really not much that

298
00:15:13,020 --> 00:15:15,400
 could ever cause us any sort

299
00:15:15,400 --> 00:15:18,080
 of difficulty or suffering.

300
00:15:18,080 --> 00:15:20,360
 So I hope that helps.

301
00:15:20,360 --> 00:15:23,820
 As usual I know there are going to be people who disagree

302
00:15:23,820 --> 00:15:25,520
 and get all off the norms of

303
00:15:25,520 --> 00:15:28,320
 this little office to each their own.

304
00:15:28,320 --> 00:15:31,710
 This is my ask a monk, so you've asked me this is my answer

305
00:15:31,710 --> 00:15:32,040
.

306
00:15:32,040 --> 00:15:32,320
 All the best.

307
00:15:32,320 --> 00:16:02,320
 Thank you.

