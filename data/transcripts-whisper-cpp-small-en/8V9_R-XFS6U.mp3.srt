1
00:00:00,000 --> 00:00:03,000
 Should I always be tolerant?

2
00:00:03,000 --> 00:00:06,000
 Hi, Bhante, I am a Buddhist starter.

3
00:00:06,000 --> 00:00:11,340
 The renovate worker next to my room works until midnight

4
00:00:11,340 --> 00:00:13,000
 every day and plays loud music.

5
00:00:13,000 --> 00:00:15,000
 I talk to them many times, but they didn't listen.

6
00:00:15,000 --> 00:00:20,000
 Should Buddhists be always tolerant?

7
00:00:20,000 --> 00:00:23,440
 It's not really the most important thing to ask, should

8
00:00:23,440 --> 00:00:25,000
 Buddhists always be tolerant?

9
00:00:25,000 --> 00:00:27,000
 Obviously, that's what most people would ask.

10
00:00:27,000 --> 00:00:31,690
 Just to point out that let's approach this from the point

11
00:00:31,690 --> 00:00:34,000
 of view of what's really going to benefit me.

12
00:00:34,000 --> 00:00:36,980
 Obviously, this isn't benefiting you, talking to them,

13
00:00:36,980 --> 00:00:39,000
 being intolerant.

14
00:00:39,000 --> 00:00:44,790
 I would say anybody who wants to be happy should be

15
00:00:44,790 --> 00:00:48,000
 tolerant because intolerance,

16
00:00:48,000 --> 00:00:50,680
 and there's going to be a proviso in here, there's a

17
00:00:50,680 --> 00:00:54,000
 qualifier here, but for the most, in general,

18
00:00:54,000 --> 00:00:57,370
 tolerance is always going to make you happier, it's always

19
00:00:57,370 --> 00:00:59,000
 going to bring you more peace than intolerance.

20
00:00:59,000 --> 00:01:06,230
 Now, there's a point where intolerance becomes delusion,

21
00:01:06,230 --> 00:01:13,000
 where it actually is the covering up of your inclination to

22
00:01:13,000 --> 00:01:14,000
 change things.

23
00:01:14,000 --> 00:01:19,610
 There's many cases of that, tolerance of your own faults,

24
00:01:19,610 --> 00:01:24,000
 tolerance of evil in the sense of condoning it,

25
00:01:24,000 --> 00:01:28,280
 when you have a chance to help someone, even just to advise

26
00:01:28,280 --> 00:01:29,000
 someone.

27
00:01:29,000 --> 00:01:32,380
 You see someone doing something that's going to cause them

28
00:01:32,380 --> 00:01:33,000
 suffering,

29
00:01:33,000 --> 00:01:35,830
 and right away there should be the inclination in a good

30
00:01:35,830 --> 00:01:37,000
 person to help them,

31
00:01:37,000 --> 00:01:40,290
 to want to help them to see and help them to become better

32
00:01:40,290 --> 00:01:41,000
 people.

33
00:01:41,000 --> 00:01:46,550
 But by quashing that and by tolerating their evil, at times

34
00:01:46,550 --> 00:01:49,000
, at least when it's your place to say something,

35
00:01:49,000 --> 00:01:52,350
 like if it's your children, parents who indulge their

36
00:01:52,350 --> 00:01:55,000
 children and allow them to do whatever they want,

37
00:01:55,000 --> 00:01:57,350
 or let them explore, that's fine if they want to do drugs

38
00:01:57,350 --> 00:02:01,180
 or whatever, let them explore if they want to get drunk,

39
00:02:01,180 --> 00:02:02,000
 and so on, and so on.

40
00:02:02,000 --> 00:02:05,360
 Of course, there are times where you can't control other

41
00:02:05,360 --> 00:02:13,000
 people, but there are times where you would just let it go,

42
00:02:13,000 --> 00:02:15,690
 and it's not even letting it go because there is the

43
00:02:15,690 --> 00:02:17,000
 inclination to help.

44
00:02:17,000 --> 00:02:20,420
 In a clear mind, there will be the natural inclination to

45
00:02:20,420 --> 00:02:22,000
 do something about it.

46
00:02:22,000 --> 00:02:24,300
 It's the correct thing to do, the correct response, and by

47
00:02:24,300 --> 00:02:25,000
 quashing that,

48
00:02:25,000 --> 00:02:28,390
 which many people, I think, confuse with true Buddhist

49
00:02:28,390 --> 00:02:29,000
 practice.

50
00:02:29,000 --> 00:02:31,890
 They say, "Well, you should just let it go," but you're not

51
00:02:31,890 --> 00:02:34,000
 really letting it go because there's the inclination,

52
00:02:34,000 --> 00:02:37,370
 the default is the inclination to help, the inclination to

53
00:02:37,370 --> 00:02:39,000
 engage, which is natural.

54
00:02:39,000 --> 00:02:41,000
 Human beings are engaged with each other.

55
00:02:41,000 --> 00:02:44,320
 We're a part of the universe. To be detached is not really

56
00:02:44,320 --> 00:02:45,000
 Buddhist.

57
00:02:45,000 --> 00:02:49,000
 We're part of the process. There is an engagement there,

58
00:02:49,000 --> 00:02:51,320
 which I think is quite crucial and many Buddhists don't

59
00:02:51,320 --> 00:02:52,000
 realize.

60
00:02:52,000 --> 00:02:53,000
 That's not what you're asking.

61
00:02:53,000 --> 00:02:56,530
 Obviously, it's not your place to help these people

62
00:02:56,530 --> 00:02:59,000
 overcome their music addiction.

63
00:02:59,000 --> 00:03:02,450
 If you were the landlord, it would be your place to tell

64
00:03:02,450 --> 00:03:06,000
 them the quiet down for the neighbors, for example.

65
00:03:06,000 --> 00:03:12,000
 But certainly loud music should not bother your sleeping,

66
00:03:12,000 --> 00:03:13,000
 should not bother your life.

67
00:03:13,000 --> 00:03:17,000
 This is something that absolutely you should meditate on

68
00:03:17,000 --> 00:03:21,000
 and cultivate tolerance and equanimity

69
00:03:21,000 --> 00:03:23,000
 and understand that it's just sound.

70
00:03:23,000 --> 00:03:27,110
 Obviously, we're conditioned against that in the West, in

71
00:03:27,110 --> 00:03:30,000
 Europe, in Canada, in Europe and North America.

72
00:03:30,000 --> 00:03:32,380
 We're conditioned to expect things from other people. We're

73
00:03:32,380 --> 00:03:35,000
 conditioned to expect quiet and so on.

74
00:03:35,000 --> 00:03:37,440
 If you go to Thailand, it's quite an eye-opener and it's

75
00:03:37,440 --> 00:03:40,560
 humbling because the things they tolerate there, it's

76
00:03:40,560 --> 00:03:42,000
 amazing.

77
00:03:42,000 --> 00:03:45,500
 In our monastery, there would be two weeks out of every

78
00:03:45,500 --> 00:03:47,000
 year, two solid weeks,

79
00:03:47,000 --> 00:03:51,240
 where basically 24/7 they would have music blaring into the

80
00:03:51,240 --> 00:03:55,000
 monastery, like really loud speakers up on the stage.

81
00:03:55,000 --> 00:03:59,480
 Maybe not during the day. Yeah, I think it was basically 24

82
00:03:59,480 --> 00:04:03,000
/7, like really 24 hours a day loud music.

83
00:04:03,000 --> 00:04:06,970
 I can't remember. It may stop somewhere in the early

84
00:04:06,970 --> 00:04:11,340
 morning and pick up in the morning, but it just seemed like

85
00:04:11,340 --> 00:04:12,000
 impossible.

86
00:04:12,000 --> 00:04:14,750
 How could you possibly meditate through this? But Thai

87
00:04:14,750 --> 00:04:16,000
 people were unfazed by it.

88
00:04:16,000 --> 00:04:19,210
 And so they'd meditate. It's not really a problem. Or they

89
00:04:19,210 --> 00:04:20,000
 were less phased by it.

90
00:04:20,000 --> 00:04:24,160
 And in general, the amount of noise that people in Asia put

91
00:04:24,160 --> 00:04:25,000
 up with.

92
00:04:25,000 --> 00:04:31,700
 So it really is just a problem of the highly developed and

93
00:04:31,700 --> 00:04:38,000
 privileged world, the imperial world,

94
00:04:38,000 --> 00:04:41,130
 the people who went and conquered the rest of the world and

95
00:04:41,130 --> 00:04:42,000
 got really rich off of it

96
00:04:42,000 --> 00:04:46,790
 because we expect something that is unreasonable. We expect

97
00:04:46,790 --> 00:04:50,000
 to be treated like royalty.

98
00:04:50,000 --> 00:04:53,140
 So we expect others to respect us. I mean, this is not

99
00:04:53,140 --> 00:04:56,000
 obviously what you're going through your mind,

100
00:04:56,000 --> 00:05:00,550
 but it's ingrained in us. So we have these expectations

101
00:05:00,550 --> 00:05:05,000
 that, well, if nothing else, are harmful, are causing us

102
00:05:05,000 --> 00:05:06,000
 suffering.

103
00:05:06,000 --> 00:05:09,120
 But without your expectations, it's probably a lot easier

104
00:05:09,120 --> 00:05:12,000
 to bear people around you blaring their loud music.

105
00:05:12,000 --> 00:05:15,700
 And so I would work very much on your expectations and on

106
00:05:15,700 --> 00:05:18,810
 your views and come to see that it's really just a cultural

107
00:05:18,810 --> 00:05:19,000
 thing,

108
00:05:19,000 --> 00:05:23,220
 where we require things from other people like quiet and

109
00:05:23,220 --> 00:05:25,000
 civility.

110
00:05:25,000 --> 00:05:31,350
 And we expect them to, we expect, as the Buddha said,

111
00:05:31,350 --> 00:05:35,000
 speech at the right time, speech that is polite.

112
00:05:35,000 --> 00:05:38,120
 We expect speech that is truthful, speech that is

113
00:05:38,120 --> 00:05:39,000
 beneficial.

114
00:05:39,000 --> 00:05:42,920
 But he said, you're always going to have speech that is un

115
00:05:42,920 --> 00:05:46,000
beneficial, speech that is harmful and so on.

116
00:05:46,000 --> 00:05:48,000
 You're always going to have things that disappoint you.

117
00:05:48,000 --> 00:05:53,780
 So it's like I think Milarepa was the one who said, if the

118
00:05:53,780 --> 00:05:57,460
 earth is, if the whole earth was covered in glass, broken

119
00:05:57,460 --> 00:06:00,000
 glass, you have two choices.

120
00:06:00,000 --> 00:06:04,770
 One, you can cover the whole world with leather. Or two,

121
00:06:04,770 --> 00:06:07,000
 you can wear sandals.

122
00:06:07,000 --> 00:06:09,990
 And where the metaphor kind of breaks down because we're

123
00:06:09,990 --> 00:06:12,910
 not trying to cover up the problem, but we're trying to

124
00:06:12,910 --> 00:06:14,000
 change ourselves.

125
00:06:14,000 --> 00:06:17,350
 It's clear what he's saying is you have two choices. Change

126
00:06:17,350 --> 00:06:20,790
 the world. Make the world so it's always as you would have

127
00:06:20,790 --> 00:06:21,000
 it.

128
00:06:21,000 --> 00:06:24,000
 Or change yourself so that you can deal with it.

129
00:06:24,000 --> 00:06:26,820
 Because in the end, I mean, think of what some people in

130
00:06:26,820 --> 00:06:28,000
 the world deal with.

131
00:06:28,000 --> 00:06:31,320
 There are people in the world who deal with starvation, who

132
00:06:31,320 --> 00:06:39,190
 deal with the constant threat to their very survival from

133
00:06:39,190 --> 00:06:43,000
 military or corruption or disease.

134
00:06:43,000 --> 00:06:46,000
 Or famine.

135
00:06:46,000 --> 00:06:53,540
 And so it's quite clear that we're just dealing with our

136
00:06:53,540 --> 00:06:58,000
 own expectations that are actually unreasonable.

137
00:06:58,000 --> 00:07:02,710
 And the point being that any of these things can come to us

138
00:07:02,710 --> 00:07:04,000
 at any time.

139
00:07:04,000 --> 00:07:06,180
 So if you're always expecting something from the world

140
00:07:06,180 --> 00:07:08,000
 around you, you're always vulnerable.

141
00:07:08,000 --> 00:07:13,020
 The only way to be invincible is to have no faults, to have

142
00:07:13,020 --> 00:07:18,270
 no hooks where if this then that, if this comes then I

143
00:07:18,270 --> 00:07:19,000
 react.

144
00:07:19,000 --> 00:07:22,000
 To where you no longer react to the vicissitudes of life.

