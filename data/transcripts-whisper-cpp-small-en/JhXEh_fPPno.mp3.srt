1
00:00:00,000 --> 00:00:04,580
 Hi, welcome back to Ask a Monk. Today's question comes from

2
00:00:04,580 --> 00:00:08,000
 a good friend of mine, Jerry Ibsen.

3
00:00:08,000 --> 00:00:12,000
 And he asks, "What is the nature of heaven in Buddhism?"

4
00:00:12,000 --> 00:00:15,910
 Or what is the meaning of the word heaven? What meaning

5
00:00:15,910 --> 00:00:18,000
 does it have for Buddhists?

6
00:00:18,000 --> 00:00:26,120
 First of all, the idea of heaven as a place can be somewhat

7
00:00:26,120 --> 00:00:27,000
 misleading.

8
00:00:27,000 --> 00:00:31,830
 It's important that we understand what do we mean by place

9
00:00:31,830 --> 00:00:33,000
 or by space.

10
00:00:33,000 --> 00:00:39,000
 Space in Buddhism is an extension of one's experience.

11
00:00:39,000 --> 00:00:43,000
 So all of my experience takes up a certain amount of space.

12
00:00:43,000 --> 00:00:47,000
 My being, my universe has a certain space.

13
00:00:47,000 --> 00:00:51,000
 And everyone around me also takes up a piece of space.

14
00:00:51,000 --> 00:00:55,000
 And so our interactions create the world around us.

15
00:00:55,000 --> 00:01:01,300
 We create the idea of three-dimensional space based on our

16
00:01:01,300 --> 00:01:03,000
 experience.

17
00:01:03,000 --> 00:01:08,650
 So in that sense, heaven is simply another level of

18
00:01:08,650 --> 00:01:12,960
 experience that is made up of the experiences of the beings

19
00:01:12,960 --> 00:01:15,000
 in that realm.

20
00:01:15,000 --> 00:01:21,220
 And the difference being simply that the experience of

21
00:01:21,220 --> 00:01:25,510
 reality of those beings is much more subtle and more

22
00:01:25,510 --> 00:01:29,000
 refined and more pure than the experience at this level.

23
00:01:29,000 --> 00:01:33,940
 So the reason we meet with each other here is because our

24
00:01:33,940 --> 00:01:36,000
 experience is coarse.

25
00:01:36,000 --> 00:01:41,910
 And so we meet on the same level. For those beings who have

26
00:01:41,910 --> 00:01:47,330
 more pure and refined minds, they rise above and they meet

27
00:01:47,330 --> 00:01:49,000
 at another level.

28
00:01:49,000 --> 00:01:53,390
 That's in terms of space. So you can think of hell as being

29
00:01:53,390 --> 00:01:58,000
 below and heaven as being above as is generally the case.

30
00:01:58,000 --> 00:02:02,540
 But more important to understand in terms of what we mean

31
00:02:02,540 --> 00:02:07,590
 by heaven and hell and the various realms of existence is

32
00:02:07,590 --> 00:02:10,000
 simply that in terms of purity of mind.

33
00:02:10,000 --> 00:02:15,100
 That for every individual we're on a path. We purify our

34
00:02:15,100 --> 00:02:17,000
 minds or we sell our minds.

35
00:02:17,000 --> 00:02:22,220
 And the more defiled our minds become, the lower we sink

36
00:02:22,220 --> 00:02:26,000
 and the more coarse our reality becomes.

37
00:02:26,000 --> 00:02:30,820
 As we complicate our minds and make our minds more coarse

38
00:02:30,820 --> 00:02:35,300
 and more twisted or however you want to look at it, we

39
00:02:35,300 --> 00:02:39,240
 become equally more dense and more physical and more

40
00:02:39,240 --> 00:02:43,000
 subject to suffering.

41
00:02:43,000 --> 00:02:46,990
 You could say it's similar to a person who does bad deeds

42
00:02:46,990 --> 00:02:50,710
 and is as a result placed in jail and has to spend some

43
00:02:50,710 --> 00:02:52,000
 time in jail.

44
00:02:52,000 --> 00:02:56,490
 As a human being, our past deeds have led us to this level

45
00:02:56,490 --> 00:02:58,000
 of coarseness.

46
00:02:58,000 --> 00:03:02,440
 If we had been more coarse in our past lives, we would have

47
00:03:02,440 --> 00:03:07,010
 sunken lower to become animals or even to go to a place

48
00:03:07,010 --> 00:03:09,000
 very similar to hell.

49
00:03:09,000 --> 00:03:14,970
 If it was extreme, if we were murderers or thieves or very

50
00:03:14,970 --> 00:03:17,000
 immoral people.

51
00:03:17,000 --> 00:03:21,070
 What it does to your mind, the very real effect that it has

52
00:03:21,070 --> 00:03:24,000
 on your mind causes you to sink lower.

53
00:03:24,000 --> 00:03:27,740
 By the same token, when you purify your mind as you do good

54
00:03:27,740 --> 00:03:31,260
 deeds and try to help other people and your mind becomes

55
00:03:31,260 --> 00:03:38,000
 purer, you feel lighter, you feel more free in the mind.

56
00:03:38,000 --> 00:03:42,760
 The reason why it doesn't happen where a human being does

57
00:03:42,760 --> 00:03:47,180
 good deeds and they start floating off the ground is

58
00:03:47,180 --> 00:03:50,000
 because of the weight of the karma.

59
00:03:50,000 --> 00:03:54,590
 In this sense, it's similar to being thrown in jail because

60
00:03:54,590 --> 00:03:58,210
 people in jail can reform themselves but it doesn't mean

61
00:03:58,210 --> 00:04:01,000
 they suddenly break out of the jail.

62
00:04:01,000 --> 00:04:04,000
 They have to live out the term according to their sentence.

63
00:04:04,000 --> 00:04:09,290
 The same is true for human beings. We have a very weighty

64
00:04:09,290 --> 00:04:12,560
 circle of karma that we've performed in the past that is

65
00:04:12,560 --> 00:04:16,620
 having these intense results that has placed us in this

66
00:04:16,620 --> 00:04:18,000
 physical existence.

67
00:04:18,000 --> 00:04:23,000
 That is manifesting itself as our human existence.

68
00:04:23,000 --> 00:04:27,470
 Until that has run its course, it's not very likely that we

69
00:04:27,470 --> 00:04:31,000
're going to just rise up to heaven or so on.

70
00:04:31,000 --> 00:04:36,050
 That being said, this is a good way to explain why good

71
00:04:36,050 --> 00:04:42,000
 people have a very great difficulty surviving in the world.

72
00:04:42,000 --> 00:04:46,010
 People say you have to fight dirty, it's a dog-eat-dog

73
00:04:46,010 --> 00:04:50,340
 world if you're not trying to cheat everybody else, you won

74
00:04:50,340 --> 00:04:53,000
't get ahead and so on.

75
00:04:53,000 --> 00:04:56,550
 That may be true but that's really just a sign that this is

76
00:04:56,550 --> 00:05:00,030
 not a very pure state of existence and those people with

77
00:05:00,030 --> 00:05:03,000
 pure states of existence are rising above it.

78
00:05:03,000 --> 00:05:08,630
 A person who does good things is eventually ejected into a

79
00:05:08,630 --> 00:05:12,550
 higher realm, which is really the case with much more

80
00:05:12,550 --> 00:05:14,000
 simple and pure realms.

81
00:05:14,000 --> 00:05:17,890
 If you're born in a level of purity and you die or you

82
00:05:17,890 --> 00:05:22,340
 leave from that state, it's merely a shift down or a shift

83
00:05:22,340 --> 00:05:23,000
 up.

84
00:05:23,000 --> 00:05:27,210
 It's not a death, it doesn't have to be a set length of

85
00:05:27,210 --> 00:05:31,000
 time. You can leave at any time. You can go up and down.

86
00:05:31,000 --> 00:05:34,130
 And beings in heaven are said to do this. They're said to

87
00:05:34,130 --> 00:05:38,530
 go from one level to another quite smoothly based on their

88
00:05:38,530 --> 00:05:41,480
 deeds, based on their state of mind and based on the

89
00:05:41,480 --> 00:05:43,000
 fulfillment of certain karmas.

90
00:05:43,000 --> 00:05:48,490
 When they've outlived the acts which brought them to this

91
00:05:48,490 --> 00:05:53,270
 state of purity, they can sink down and if they do more

92
00:05:53,270 --> 00:05:57,250
 good deeds eventually they'll be able to rise up when the

93
00:05:57,250 --> 00:05:59,000
 old karma has ceased.

94
00:05:59,000 --> 00:06:02,870
 It's really based on cause and effect. It's not something

95
00:06:02,870 --> 00:06:05,760
 magical where there's someone judging you, "Okay, now it's

96
00:06:05,760 --> 00:06:08,340
 your time to go up to heaven, now it's your time to go down

97
00:06:08,340 --> 00:06:09,000
 to hell."

98
00:06:09,000 --> 00:06:13,700
 It's based on whether you do good deeds or do bad deeds and

99
00:06:13,700 --> 00:06:18,100
 when the good and bad deeds you've done before, the effect

100
00:06:18,100 --> 00:06:20,000
 of them is expired.

101
00:06:20,000 --> 00:06:24,960
 So in this sense it's really here and now. Heaven is just

102
00:06:24,960 --> 00:06:28,000
 the change of this to something more pure.

103
00:06:28,000 --> 00:06:33,830
 Hell is the change of this to something more impure, more

104
00:06:33,830 --> 00:06:35,000
 coarse.

105
00:06:35,000 --> 00:06:40,760
 And so when our karma, the result of our karma that has led

106
00:06:40,760 --> 00:06:46,400
 us to be born as a human being and has created this reality

107
00:06:46,400 --> 00:06:47,000
 is up.

108
00:06:47,000 --> 00:06:50,960
 When it gets to its end then our mind is free and goes

109
00:06:50,960 --> 00:06:53,000
 according to the next karma.

110
00:06:53,000 --> 00:06:58,520
 It grasps onto something either pure or impure. It's able

111
00:06:58,520 --> 00:07:03,480
 to rise out just as a person who is let out of jail is free

112
00:07:03,480 --> 00:07:07,600
 to go on and to become a better person or go back and do

113
00:07:07,600 --> 00:07:11,630
 bad deeds and have to come back and spend more time in jail

114
00:07:11,630 --> 00:07:12,000
.

115
00:07:12,000 --> 00:07:17,060
 So I hope that helps make sense. Heaven can be thought of

116
00:07:17,060 --> 00:07:21,870
 as a place but only in the sense of it being a conglomer

117
00:07:21,870 --> 00:07:26,000
ation of equally pure or impure minds or beings.

118
00:07:26,000 --> 00:07:31,190
 But it should be better thought of as a state of mind where

119
00:07:31,190 --> 00:07:36,230
 the mind is pure and is freed from the effects of our imp

120
00:07:36,230 --> 00:07:41,410
urity that has led us to be born and have to spend time as a

121
00:07:41,410 --> 00:07:43,000
 human being.

122
00:07:43,000 --> 00:07:46,850
 Finally, that being said, it's important to understand that

123
00:07:46,850 --> 00:07:49,000
 heaven isn't the be all end all.

124
00:07:49,000 --> 00:07:53,170
 What you can say is that people who practice meditation or

125
00:07:53,170 --> 00:07:57,260
 practice on the right path do generally go to heaven a lot

126
00:07:57,260 --> 00:08:00,000
 easier because their minds are pure.

127
00:08:00,000 --> 00:08:04,100
 But it's also possible to go to heaven with on a temporary

128
00:08:04,100 --> 00:08:05,000
 purity.

129
00:08:05,000 --> 00:08:09,320
 If you work really hard on a mundane level and develop very

130
00:08:09,320 --> 00:08:14,020
 pure states of mind, you can do it by suppressing the bad

131
00:08:14,020 --> 00:08:15,000
 states.

132
00:08:15,000 --> 00:08:18,480
 You can practice certain types of meditation that give you

133
00:08:18,480 --> 00:08:22,690
 very pure states of mind and help you to rise above the imp

134
00:08:22,690 --> 00:08:26,900
ure state part of your mind temporarily, like with transcend

135
00:08:26,900 --> 00:08:29,220
ental meditation and so on, and can actually be born in

136
00:08:29,220 --> 00:08:31,000
 heaven even for long periods of time.

137
00:08:31,000 --> 00:08:34,730
 But once that wears thin and the results of all of that

138
00:08:34,730 --> 00:08:38,830
 work wears out, and obviously you wouldn't be doing that

139
00:08:38,830 --> 00:08:42,840
 work in heaven, you'd just be enjoying your time in this

140
00:08:42,840 --> 00:08:45,000
 blissful state of being.

141
00:08:45,000 --> 00:08:49,000
 The bad stuff comes back. The bad stuff is there.

142
00:08:49,000 --> 00:08:52,700
 And when the results, when the good things you did lose

143
00:08:52,700 --> 00:08:56,390
 their power and the results are worn out, you'll come

144
00:08:56,390 --> 00:08:58,000
 crashing back down.

145
00:08:58,000 --> 00:09:03,280
 So, heaven is not the goal. It's a sign that the mind is

146
00:09:03,280 --> 00:09:04,000
 pure.

147
00:09:04,000 --> 00:09:08,220
 But it's important to understand that the important thing

148
00:09:08,220 --> 00:09:10,000
 is to purify the mind.

149
00:09:10,000 --> 00:09:13,790
 And sometimes that's easier done in a place like the human

150
00:09:13,790 --> 00:09:17,350
 world because we're able to see the ups and the downs of

151
00:09:17,350 --> 00:09:18,000
 life.

152
00:09:18,000 --> 00:09:21,710
 A person who's born in heaven due to some temporary deed or

153
00:09:21,710 --> 00:09:25,530
 some mundane goodness that they've performed and that has

154
00:09:25,530 --> 00:09:28,920
 elevated their minds to that level can often become

155
00:09:28,920 --> 00:09:30,000
 intoxicated by it.

156
00:09:30,000 --> 00:09:33,000
 And they forget to develop themselves further.

157
00:09:33,000 --> 00:09:36,590
 If a person is born again and again as a human being or

158
00:09:36,590 --> 00:09:40,780
 even just is in one life a human being, during this life we

159
00:09:40,780 --> 00:09:44,110
're much better able to see, "When I do bad deeds, bad

160
00:09:44,110 --> 00:09:46,570
 things come to me. When I do good deeds, good things come

161
00:09:46,570 --> 00:09:47,000
 to me."

162
00:09:47,000 --> 00:09:49,450
 If you're born in heaven you can do bad deeds and because

163
00:09:49,450 --> 00:09:51,860
 of the power of your past good deeds you don't have to pay

164
00:09:51,860 --> 00:09:53,000
 for them right away.

165
00:09:53,000 --> 00:09:57,000
 So it can be dangerous.

166
00:09:57,000 --> 00:10:00,470
 On the other hand, one of the good things about a place

167
00:10:00,470 --> 00:10:04,180
 like heaven is you can sit in meditation quite easily and

168
00:10:04,180 --> 00:10:06,000
 without all the pain and suffering.

169
00:10:06,000 --> 00:10:09,820
 They say if you're really dedicated to meditation being

170
00:10:09,820 --> 00:10:13,000
 born in heaven can be a real great blessing.

171
00:10:13,000 --> 00:10:16,150
 Another thing is because there are obviously many medit

172
00:10:16,150 --> 00:10:19,820
ators who have gone up to heaven and so it's very easy to

173
00:10:19,820 --> 00:10:23,910
 find a group of people to meditate with and very easy to

174
00:10:23,910 --> 00:10:28,550
 sit in meditation because your body doesn't complain as

175
00:10:28,550 --> 00:10:29,000
 much.

176
00:10:29,000 --> 00:10:32,180
 You have a very pure state of being and you can sit for

177
00:10:32,180 --> 00:10:36,000
 months on end and contemplate the reality in front of you.

178
00:10:36,000 --> 00:10:41,040
 So there's really no saying good or bad. It's good in a

179
00:10:41,040 --> 00:10:45,380
 mundane sense but it's basically just another state of

180
00:10:45,380 --> 00:10:46,000
 purity.

181
00:10:46,000 --> 00:10:52,000
 And until you can really understand reality for what it is,

182
00:10:52,000 --> 00:10:57,920
 it's really a meaningless judgment as to where you are on

183
00:10:57,920 --> 00:10:59,000
 the scale.

184
00:10:59,000 --> 00:11:02,640
 Until it becomes a permanent thing where your mind is truly

185
00:11:02,640 --> 00:11:05,000
 pure and you have no more clinging.

186
00:11:05,000 --> 00:11:07,750
 You're not just suppressing your bad thoughts but you're

187
00:11:07,750 --> 00:11:10,560
 actually understanding them and you're actually letting go

188
00:11:10,560 --> 00:11:13,630
 of them and you're actually giving them up and they no

189
00:11:13,630 --> 00:11:15,000
 longer arise again.

190
00:11:15,000 --> 00:11:21,480
 At that point then not only are you born say in heaven but

191
00:11:21,480 --> 00:11:27,070
 eventually you're free from all of this arising of

192
00:11:27,070 --> 00:11:29,000
 experience.

193
00:11:29,000 --> 00:11:34,910
 You come back to the center and leave behind all of the

194
00:11:34,910 --> 00:11:37,000
 outside world.

195
00:11:37,000 --> 00:11:40,340
 That doesn't sound too fruity. It's not one of the things I

196
00:11:40,340 --> 00:11:43,310
 normally talk about but you asked and that's my

197
00:11:43,310 --> 00:11:46,000
 understanding of heaven in terms of Buddhism.

198
00:11:46,000 --> 00:11:50,140
 The most important thing being it's just a state of reality

199
00:11:50,140 --> 00:11:54,100
 and if you enter into that state of reality you should med

200
00:11:54,100 --> 00:11:56,510
itate on it and you should try to understand it and see it

201
00:11:56,510 --> 00:11:57,000
 clearly.

202
00:11:57,000 --> 00:12:01,730
 It's a sign that your mind is pure in some way or another

203
00:12:01,730 --> 00:12:06,270
 and the sign that you're on the right path but it only

204
00:12:06,270 --> 00:12:11,450
 serves you best to continue to develop greater and greater

205
00:12:11,450 --> 00:12:15,800
 state of purity not to become content with being born in

206
00:12:15,800 --> 00:12:17,000
 heaven or so on.

207
00:12:17,000 --> 00:12:20,000
 Okay, so thanks for the question. Keep them coming.

