1
00:00:00,000 --> 00:00:02,000
 Oh, should I say?

2
00:00:02,000 --> 00:00:03,000
 Go for it.

3
00:00:03,000 --> 00:00:08,000
 How do I meditate when I don't want to meditate, basically?

4
00:00:08,000 --> 00:00:11,400
 I know it sounds like a frivolous question, but it's a

5
00:00:11,400 --> 00:00:15,850
 serious issue for me because when I sit down, I can do it

6
00:00:15,850 --> 00:00:17,000
 for a short time.

7
00:00:17,000 --> 00:00:21,360
 But eventually, I get frustrated, my muscles cramp up, it

8
00:00:21,360 --> 00:00:24,330
 feels like my head is shaking, it becomes just really

9
00:00:24,330 --> 00:00:25,000
 difficult.

10
00:00:25,000 --> 00:00:28,240
 So I end up just getting up, whatever it is I'm making

11
00:00:28,240 --> 00:00:31,000
 excuse, it really gets very difficult.

12
00:00:31,000 --> 00:00:36,000
 So I guess my ego is against it or something.

13
00:00:36,000 --> 00:00:39,920
 I hope that's not a frivolous question because I don't want

14
00:00:39,920 --> 00:00:42,000
 people to come in and just sitting through it.

15
00:00:42,000 --> 00:00:45,410
 But either I don't have the willpower for it or I'm just

16
00:00:45,410 --> 00:00:46,000
 lazy.

17
00:00:46,000 --> 00:00:48,000
 Is there any like, pride still wise for that?

18
00:00:48,000 --> 00:00:52,130
 I think there's something we miss and that's the forcing

19
00:00:52,130 --> 00:00:56,590
 ourselves to meditate, which can be, you know, it's not

20
00:00:56,590 --> 00:01:00,780
 always the best idea because you will cultivate a aversion

21
00:01:00,780 --> 00:01:02,000
 to meditation.

22
00:01:02,000 --> 00:01:04,000
 And that's no good.

23
00:01:04,000 --> 00:01:07,070
 That being said, in the beginning, you don't know what's

24
00:01:07,070 --> 00:01:08,000
 good for you.

25
00:01:08,000 --> 00:01:14,260
 You can think of it as the learning process of a adolescent

26
00:01:14,260 --> 00:01:20,000
, you know, as a child, you have to force them to learn.

27
00:01:20,000 --> 00:01:24,170
 You have to push them to learn, not force, but you have to

28
00:01:24,170 --> 00:01:26,000
 at least direct them.

29
00:01:26,000 --> 00:01:31,110
 Even still, forcing doesn't generally have the desired

30
00:01:31,110 --> 00:01:35,070
 effect. And as a result, parents who push their children

31
00:01:35,070 --> 00:01:41,070
 too hard find them break and rebel or worse can destroy

32
00:01:41,070 --> 00:01:45,000
 their lives out of just the intense stress.

33
00:01:45,000 --> 00:01:46,000
 But you should look at it that way.

34
00:01:46,000 --> 00:01:51,000
 The mind, for all intents and purposes, we're children.

35
00:01:51,000 --> 00:01:53,000
 We never really grow up.

36
00:01:53,000 --> 00:01:55,000
 Many of us never really grow up.

37
00:01:55,000 --> 00:02:00,130
 The spiritual part for most of us is quite young,

38
00:02:00,130 --> 00:02:04,230
 especially if you acknowledge that you don't like to med

39
00:02:04,230 --> 00:02:05,000
itate.

40
00:02:05,000 --> 00:02:08,000
 Then you have to treat yourself as a child.

41
00:02:08,000 --> 00:02:11,350
 You know, you have a child's mind, which is for most of us

42
00:02:11,350 --> 00:02:12,000
 the case.

43
00:02:12,000 --> 00:02:17,030
 And so in the beginning, you have to be a little bit ins

44
00:02:17,030 --> 00:02:18,000
istent.

45
00:02:18,000 --> 00:02:19,610
 And if you think of it as a child, how would you teach a

46
00:02:19,610 --> 00:02:20,000
 child?

47
00:02:20,000 --> 00:02:23,980
 You obviously, hopefully you wouldn't beat the child or

48
00:02:23,980 --> 00:02:30,600
 force the child to, you know, into the extent that it

49
00:02:30,600 --> 00:02:34,000
 caused the child's stress and repression.

50
00:02:34,000 --> 00:02:36,880
 But on the other hand, you wouldn't just let the child sit

51
00:02:36,880 --> 00:02:40,000
 around eating candy all day or watching cartoons all day.

52
00:02:40,000 --> 00:02:44,000
 You'd try to direct them in a wholesome direction.

53
00:02:44,000 --> 00:02:48,160
 So there's no easy answer in the beginning, except to

54
00:02:48,160 --> 00:02:53,000
 answer, no, don't simply force yourself to meditate.

55
00:02:53,000 --> 00:02:56,780
 You have to understand that you're still a child and you're

56
00:02:56,780 --> 00:02:59,000
 not going to want to meditate.

57
00:02:59,000 --> 00:03:00,680
 On the other hand, don't say, well, then I just won't med

58
00:03:00,680 --> 00:03:01,000
itate.

59
00:03:01,000 --> 00:03:04,000
 Obviously, I'm sure you know that that's not the case.

60
00:03:04,000 --> 00:03:05,000
 Otherwise, you wouldn't be asking this question.

61
00:03:05,000 --> 00:03:07,000
 You want to meditate.

62
00:03:07,000 --> 00:03:09,000
 You just don't want to meditate.

63
00:03:09,000 --> 00:03:11,000
 You see the problem?

64
00:03:11,000 --> 00:03:12,000
 You know it's good for you.

65
00:03:12,000 --> 00:03:15,770
 You're all grown up intellectually, but part of you is

66
00:03:15,770 --> 00:03:19,000
 still, you know, a child.

67
00:03:19,000 --> 00:03:23,540
 A big part of it is our disconnect with the actual benefits

68
00:03:23,540 --> 00:03:25,000
 of meditation.

69
00:03:25,000 --> 00:03:29,200
 We're not machines where you can input the benefits of

70
00:03:29,200 --> 00:03:32,000
 meditation and have that stick.

71
00:03:32,000 --> 00:03:34,830
 You can tell yourself, you can listen to a talk on how

72
00:03:34,830 --> 00:03:38,270
 great meditation is and just say, yeah, meditation is

73
00:03:38,270 --> 00:03:39,000
 awesome.

74
00:03:39,000 --> 00:03:40,000
 I want to meditate.

75
00:03:40,000 --> 00:03:42,840
 Ten minutes later, you'll have forgotten it and it just won

76
00:03:42,840 --> 00:03:44,000
't be in your psyche.

77
00:03:44,000 --> 00:03:45,000
 It won't be an input.

78
00:03:45,000 --> 00:03:47,000
 So you'll be saying, why am I doing this again?

79
00:03:47,000 --> 00:03:51,570
 You know, you just don't have any desire for it, which is

80
00:03:51,570 --> 00:03:53,640
 kind of absurd because just the moment, you know, ten

81
00:03:53,640 --> 00:03:55,000
 minutes ago, you were clear.

82
00:03:55,000 --> 00:03:56,000
 Meditation is awesome.

83
00:03:56,000 --> 00:03:58,000
 I'm clear that it's a good thing.

84
00:03:58,000 --> 00:03:59,000
 It's really going to help me.

85
00:03:59,000 --> 00:04:01,000
 That can go on in the meditation course.

86
00:04:01,000 --> 00:04:02,000
 You'll be practicing.

87
00:04:02,000 --> 00:04:05,000
 You'll say, wow, this is so wonderful.

88
00:04:05,000 --> 00:04:07,000
 Meditation just helps me so much.

89
00:04:07,000 --> 00:04:09,000
 And then the next day you're like, I want to leave.

90
00:04:09,000 --> 00:04:10,000
 This is useless.

91
00:04:10,000 --> 00:04:11,000
 This is what am I doing?

92
00:04:11,000 --> 00:04:14,000
 I'm because we're organic.

93
00:04:14,000 --> 00:04:17,000
 This is because the mind is biological.

94
00:04:17,000 --> 00:04:21,930
 It is the sort of not biologically, but basically the mind

95
00:04:21,930 --> 00:04:24,000
 is a part of the organism.

96
00:04:24,000 --> 00:04:27,000
 And it's not going.

97
00:04:27,000 --> 00:04:29,000
 It's not like a computer.

98
00:04:29,000 --> 00:04:33,000
 It's going to act irrationally.

99
00:04:33,000 --> 00:04:38,430
 And so a part of it, you know, part of it is going to be

100
00:04:38,430 --> 00:04:41,680
 stepping back and saying, okay, yes, I don't want to med

101
00:04:41,680 --> 00:04:42,000
itate.

102
00:04:42,000 --> 00:04:47,000
 I acknowledge that and not ignoring that and pushing on.

103
00:04:47,000 --> 00:04:51,360
 I guess the meaning is accept that part, accept or not

104
00:04:51,360 --> 00:04:56,010
 accept, but but observe and acknowledge is the word,

105
00:04:56,010 --> 00:04:58,000
 acknowledge that part that I don't want to meditate.

106
00:04:58,000 --> 00:05:02,230
 And you'll find if you can truly acknowledge that, that you

107
00:05:02,230 --> 00:05:05,990
're able to actually meditate on it, meditate on the

108
00:05:05,990 --> 00:05:09,180
 frustration, meditate on the disliking, and you find it

109
00:05:09,180 --> 00:05:10,000
 evaporates.

110
00:05:10,000 --> 00:05:14,240
 At the same time as you do that, as you accept or not

111
00:05:14,240 --> 00:05:17,710
 accept, acknowledge and focus more on the fact that you don

112
00:05:17,710 --> 00:05:21,000
't want to meditate than on actually meditating through it.

113
00:05:21,000 --> 00:05:25,450
 At the same time, you know, re, you know, ask yourself the

114
00:05:25,450 --> 00:05:30,000
 question honestly and openly, is meditation good for me?

115
00:05:30,000 --> 00:05:32,970
 Bring yourself back to that question that you've probably

116
00:05:32,970 --> 00:05:36,000
 answered in the past, but answer it again for yourself.

117
00:05:36,000 --> 00:05:38,000
 Why am I doing this?

118
00:05:38,000 --> 00:05:39,000
 You know, is it really good for me?

119
00:05:39,000 --> 00:05:41,420
 Then don't just accept some answer that you heard on the

120
00:05:41,420 --> 00:05:43,640
 Internet, but really ask yourself, you know, what do I want

121
00:05:43,640 --> 00:05:44,000
 in life?

122
00:05:44,000 --> 00:05:46,000
 Is this really going to benefit me?

123
00:05:46,000 --> 00:05:49,570
 And sometimes that takes a little bit of soul searching,

124
00:05:49,570 --> 00:05:53,460
 soul searching, a bit of introspection, you know, saying,

125
00:05:53,460 --> 00:05:58,070
 you know, no, I really just want to play sports and have

126
00:05:58,070 --> 00:06:01,000
 sex and eat food all day.

127
00:06:01,000 --> 00:06:07,370
 But then you say, OK, yes, I acknowledge I want all that,

128
00:06:07,370 --> 00:06:11,460
 but really? And then you say to yourself, then if you're

129
00:06:11,460 --> 00:06:14,440
 honest with yourself, you say, but no, that doesn't

130
00:06:14,440 --> 00:06:17,000
 actually satisfy me, that isn't actually benefiting me.

131
00:06:17,000 --> 00:06:21,870
 And so eventually coming to the conclusion of meditation is

132
00:06:21,870 --> 00:06:27,430
 something I want to do. So there's room for having this

133
00:06:27,430 --> 00:06:33,900
 sort of introspection and reflection, sort of, we monks

134
00:06:33,900 --> 00:06:40,000
 means adjusting, you know, reflecting on your activity and

135
00:06:40,000 --> 00:06:45,000
 ensuring that you're directing yourself in the right way.

136
00:06:45,000 --> 00:06:50,620
 So I guess two parts, you know, try to be very, very aware

137
00:06:50,620 --> 00:06:56,830
 of the aversion to the meditation, understanding that after

138
00:06:56,830 --> 00:07:01,000
 some time, you really if you do grow up.

139
00:07:01,000 --> 00:07:03,420
 And again, I'm not this isn't an insult. Most of us are in

140
00:07:03,420 --> 00:07:08,420
 this situation. As you grow up, you'll want to meditate

141
00:07:08,420 --> 00:07:09,000
 more.

142
00:07:09,000 --> 00:07:12,740
 But understand that in the beginning, you're going to have

143
00:07:12,740 --> 00:07:16,860
 to finesse, you know, and you're going to have to play

144
00:07:16,860 --> 00:07:19,000
 games to help the child grow up.

145
00:07:19,000 --> 00:07:26,520
 And on the other and on the other side also have these kind

146
00:07:26,520 --> 00:07:32,360
 of philosophical conversations with yourself about, you

147
00:07:32,360 --> 00:07:34,770
 know, I don't know if it's so much a conversation, but it's

148
00:07:34,770 --> 00:07:36,000
 still just an acknowledgement.

149
00:07:36,000 --> 00:07:41,300
 You know, you have to accept the argument like a judge. You

150
00:07:41,300 --> 00:07:45,040
 can't just ignore one side because you know, the, you know,

151
00:07:45,040 --> 00:07:47,000
 this, this side is guilty.

152
00:07:47,000 --> 00:07:50,140
 You can't just ignore them and say, I'm only going to

153
00:07:50,140 --> 00:07:52,000
 listen to the prosecution.

154
00:07:52,000 --> 00:07:56,670
 You'll be a mistrial, you see, it's not the outcome isn't

155
00:07:56,670 --> 00:08:01,000
 no one is sure. No one is going to come out and say, yes,

156
00:08:01,000 --> 00:08:02,000
 it was proven that this person was good.

157
00:08:02,000 --> 00:08:06,000
 You didn't even give them a chance to present their case.

158
00:08:06,000 --> 00:08:08,980
 The mind is like that. If you don't give the defilements,

159
00:08:08,980 --> 00:08:13,000
 you know, the, their, their chance to speak.

160
00:08:13,000 --> 00:08:19,880
 Maybe not quite so much, but in a sense, yes, once the

161
00:08:19,880 --> 00:08:26,000
 aversion, for example, to meditation arises, it's too late.

162
00:08:26,000 --> 00:08:29,450
 It's already unwholesome. So pushing it away isn't going to

163
00:08:29,450 --> 00:08:32,760
 help. This is why the Buddha said when there's a version

164
00:08:32,760 --> 00:08:36,300
 arises in the mind, the key is to, to see that a version

165
00:08:36,300 --> 00:08:39,000
 has arisen in the mind, not to judge it at all.

166
00:08:39,000 --> 00:08:43,050
 This is how a judge works. They don't judge, they observe

167
00:08:43,050 --> 00:08:48,360
 and they come to a decision based on the facts, not based

168
00:08:48,360 --> 00:08:51,000
 on any kind of judgment.

169
00:08:51,000 --> 00:08:56,170
 So that's really what you have to do. Try and you'll feel

170
00:08:56,170 --> 00:08:59,310
 this, you'll, you'll feel that the difference between

171
00:08:59,310 --> 00:09:03,390
 forcing yourself to do something and just, you know, it's

172
00:09:03,390 --> 00:09:07,860
 like, it's like how religious people feel when they're kind

173
00:09:07,860 --> 00:09:10,730
 of believing in God just because they're afraid to go to

174
00:09:10,730 --> 00:09:13,400
 hell or because their parents have told them or because,

175
00:09:13,400 --> 00:09:17,170
 you know, intellectually they believe in God or whatever,

176
00:09:17,170 --> 00:09:20,000
 as opposed to actually truly.

177
00:09:20,000 --> 00:09:25,610
 Accepting something or I know I say understanding something

178
00:09:25,610 --> 00:09:26,000
.

179
00:09:26,000 --> 00:09:28,910
 You'll feel this, you'll feel the difference between

180
00:09:28,910 --> 00:09:32,040
 accepting meditation because someone told you or because

181
00:09:32,040 --> 00:09:35,320
 intellectually you believe it's right and actually getting

182
00:09:35,320 --> 00:09:37,980
 a sense of how good meditation is for you. It's much more

183
00:09:37,980 --> 00:09:39,000
 important.

184
00:09:39,000 --> 00:09:42,000
 So it's a very good question. Thank you.

185
00:09:42,000 --> 00:09:51,000
 Well, thank you so much.

