1
00:00:00,000 --> 00:00:05,760
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,760 --> 00:00:10,520
 Today we continue on with verse number 117, which reads as

3
00:00:10,520 --> 00:00:12,000
 follows.

4
00:00:12,000 --> 00:00:19,040
 "Papanjai puriso gahira, gahira nah nang gahira puna punang

5
00:00:19,040 --> 00:00:22,880
, nathamhichandang gahira

6
00:00:22,880 --> 00:00:34,080
 duko papasa ujjayo," which means, "Papanjai puriso gahira,

7
00:00:34,080 --> 00:00:37,200
 if one should do an evil deed,

8
00:00:37,200 --> 00:00:42,390
 nathnang gahira puna punang, one should not do such a deed

9
00:00:42,390 --> 00:00:45,440
 again and again, nathamhichandang

10
00:00:45,440 --> 00:00:53,210
 gahira, one should not become content or pleased by that

11
00:00:53,210 --> 00:00:57,840
 evil deed, by such evil deeds.

12
00:00:57,840 --> 00:01:05,580
 Dukho papasa ujjayo, because suffering comes from the

13
00:01:05,580 --> 00:01:09,160
 accumulation of evil.

14
00:01:09,160 --> 00:01:12,560
 This is actually quite an important verse.

15
00:01:12,560 --> 00:01:16,450
 The story is quite simple, although to get the full story

16
00:01:16,450 --> 00:01:18,760
 we have to go back to the Vinaya.

17
00:01:18,760 --> 00:01:22,960
 This is in relation to a monk called Sayyasaka.

18
00:01:22,960 --> 00:01:32,180
 Sayyasaka was a roommate or a fellow resident of the monk U

19
00:01:32,180 --> 00:01:38,160
daya or maybe Laludaya or Laludayi

20
00:01:38,160 --> 00:01:46,640
 who was not a very good friend.

21
00:01:46,640 --> 00:01:51,560
 He didn't have the best of habits himself.

22
00:01:51,560 --> 00:01:55,420
 Sayyasaka was a monk who was discontent with the homeless

23
00:01:55,420 --> 00:01:58,000
 life, discontent with the monastic

24
00:01:58,000 --> 00:01:59,720
 life.

25
00:01:59,720 --> 00:02:07,100
 He had been practicing but was very much attached to sens

26
00:02:07,100 --> 00:02:12,200
uality and the home life, the worldly

27
00:02:12,200 --> 00:02:16,660
 life and all the wonderful bubbles and attractions that

28
00:02:16,660 --> 00:02:22,000
 exist in the world that entice the senses.

29
00:02:22,000 --> 00:02:25,800
 He became rather sick, sickly.

30
00:02:25,800 --> 00:02:30,270
 He wasn't able to eat the food because he would miss more

31
00:02:30,270 --> 00:02:31,920
 luxurious food.

32
00:02:31,920 --> 00:02:33,600
 He wasn't comfortable with his bedding.

33
00:02:33,600 --> 00:02:35,880
 He wasn't comfortable with his lodging.

34
00:02:35,880 --> 00:02:37,760
 He just wasn't happy.

35
00:02:37,760 --> 00:02:46,230
 Not being happy, his body shriveled up and his demeanor

36
00:02:46,230 --> 00:02:51,040
 became unpleasant or he became

37
00:02:51,040 --> 00:02:52,040
 less radiant.

38
00:02:52,040 --> 00:02:56,640
 He lost the radiance and the sort of the glow about him.

39
00:02:56,640 --> 00:03:00,190
 So he became rather pale when they said the veins were

40
00:03:00,190 --> 00:03:02,320
 sticking out of his skin and so

41
00:03:02,320 --> 00:03:03,600
 on.

42
00:03:03,600 --> 00:03:05,760
 He looked really, really bad.

43
00:03:05,760 --> 00:03:10,960
 And so Laludayi, concerned, asked him what's wrong.

44
00:03:10,960 --> 00:03:13,920
 He said, "You look like you're having some trouble."

45
00:03:13,920 --> 00:03:14,920
 He said, "Yeah, I am.

46
00:03:14,920 --> 00:03:15,920
 I'm really not happy."

47
00:03:15,920 --> 00:03:18,720
 And he said, "Well, here's what you should do.

48
00:03:18,720 --> 00:03:26,100
 Sleep whenever you want, bathe whenever you want, and when

49
00:03:26,100 --> 00:03:30,280
 you get this desire, this sexual

50
00:03:30,280 --> 00:03:34,920
 desire to come up, it just...

51
00:03:34,920 --> 00:03:38,120
 Well he basically tells him to just masturbate.

52
00:03:38,120 --> 00:03:41,040
 Go ahead."

53
00:03:41,040 --> 00:03:45,680
 This is a story in relation to the Vinaya.

54
00:03:45,680 --> 00:03:50,480
 It does get into some fairly intimate details in that

55
00:03:50,480 --> 00:03:51,480
 regard.

56
00:03:51,480 --> 00:03:53,760
 The Buddha eventually catches wind of it.

57
00:03:53,760 --> 00:03:57,480
 So what happens is Sayyasaka becomes quite radiant again.

58
00:03:57,480 --> 00:04:02,520
 He regains his color and suddenly he looks normal again.

59
00:04:02,520 --> 00:04:06,570
 And the other monks look at him and say, "Wow, hey, you

60
00:04:06,570 --> 00:04:07,600
 look good.

61
00:04:07,600 --> 00:04:10,920
 You were looking really kind of sickly and pale there for a

62
00:04:10,920 --> 00:04:11,520
 while.

63
00:04:11,520 --> 00:04:12,520
 What happened?"

64
00:04:12,520 --> 00:04:15,880
 And he said, "Oh," and he told him the advice he'd been

65
00:04:15,880 --> 00:04:17,880
 given and how he had changed his

66
00:04:17,880 --> 00:04:22,680
 behavior.

67
00:04:22,680 --> 00:04:27,460
 And the monk said, "So do you use that same hand to eat the

68
00:04:27,460 --> 00:04:30,080
 food that people give you?

69
00:04:30,080 --> 00:04:32,520
 Is that the same hand that you're used to?"

70
00:04:32,520 --> 00:04:36,980
 I mean, not that it mattered which hand, but basically

71
00:04:36,980 --> 00:04:38,480
 making a point.

72
00:04:38,480 --> 00:04:41,240
 And then you go ahead and eat food that's been given as a

73
00:04:41,240 --> 00:04:42,720
 gift with the same hand.

74
00:04:42,720 --> 00:04:45,640
 And he said, "Yeah."

75
00:04:45,640 --> 00:04:49,760
 And everyone got very kind of ashamed and upset.

76
00:04:49,760 --> 00:04:52,680
 And the thing is, this was back in the beginning of the San

77
00:04:52,680 --> 00:04:54,480
gha and there was no rule against

78
00:04:54,480 --> 00:04:56,480
 masturbation.

79
00:04:56,480 --> 00:05:02,820
 So it wasn't actually against any rule that had been set

80
00:05:02,820 --> 00:05:03,880
 down.

81
00:05:03,880 --> 00:05:11,440
 But it clearly was unacceptable for many of the monks.

82
00:05:11,440 --> 00:05:14,190
 And so the word got around to the Buddha and the Buddha

83
00:05:14,190 --> 00:05:15,920
 called him up and admonished him

84
00:05:15,920 --> 00:05:23,640
 and said, "You worthless man, worthless monk."

85
00:05:23,640 --> 00:05:25,640
 It's just not something that...

86
00:05:25,640 --> 00:05:28,360
 The whole idea of being a monk is to be celibate.

87
00:05:28,360 --> 00:05:33,440
 And so masturbation just doesn't fit into the equation.

88
00:05:33,440 --> 00:05:38,280
 And so the Buddha then laid down the rule that this was

89
00:05:38,280 --> 00:05:40,240
 against the rule.

90
00:05:40,240 --> 00:05:42,920
 So that's the backstory.

91
00:05:42,920 --> 00:05:47,120
 But in this story, in the Dhammapada, which led the Buddha

92
00:05:47,120 --> 00:05:49,600
 to tell this verse was apparently

93
00:05:49,600 --> 00:05:55,530
 Sayyasaka, either during this time or after, he was unable

94
00:05:55,530 --> 00:05:57,920
 to give up this habit.

95
00:05:57,920 --> 00:06:01,080
 And so the Buddha then taught this verse to try and help

96
00:06:01,080 --> 00:06:02,600
 him give up the habit.

97
00:06:02,600 --> 00:06:05,080
 Whether he did or not, it's not clear, probably he didn't.

98
00:06:05,080 --> 00:06:07,200
 But other people gained from the verse.

99
00:06:07,200 --> 00:06:12,480
 It was beneficial as a teaching because other people were

100
00:06:12,480 --> 00:06:15,080
 able to see what comes of bad

101
00:06:15,080 --> 00:06:16,080
 deeds.

102
00:06:16,080 --> 00:06:18,960
 Now, the interesting thing here with this verse and with

103
00:06:18,960 --> 00:06:20,360
 the story is that it appears

104
00:06:20,360 --> 00:06:23,120
 to be the opposite, right?

105
00:06:23,120 --> 00:06:26,200
 Here was someone who was doing his best to refrain from un

106
00:06:26,200 --> 00:06:27,920
wholesomeness and suffering

107
00:06:27,920 --> 00:06:29,160
 from it.

108
00:06:29,160 --> 00:06:33,830
 And when he began to engage in unwholesome activity again,

109
00:06:33,830 --> 00:06:36,400
 he got better, he got healthier,

110
00:06:36,400 --> 00:06:37,400
 he got happier.

111
00:06:37,400 --> 00:06:40,560
 So this is an argument that people give in favor of

112
00:06:40,560 --> 00:06:42,840
 sexuality, in favor of all kinds

113
00:06:42,840 --> 00:06:43,840
 of sensual indulgence.

114
00:06:43,840 --> 00:06:48,620
 They say, well, it leads to both happiness and even, you

115
00:06:48,620 --> 00:06:51,280
 could say, physical health to

116
00:06:51,280 --> 00:06:54,360
 have good food and so on.

117
00:06:54,360 --> 00:06:57,790
 And there's arguments to be made in that regard both ways

118
00:06:57,790 --> 00:07:00,640
 because of course eating good food

119
00:07:00,640 --> 00:07:04,870
 eventually leads to obsession with good food and can lead

120
00:07:04,870 --> 00:07:07,200
 to an obsession with taste.

121
00:07:07,200 --> 00:07:12,120
 And as we know, our obsession with food and tastes often

122
00:07:12,120 --> 00:07:15,560
 does lead to great physical suffering.

123
00:07:15,560 --> 00:07:19,820
 As for things like sexuality, well, it leads to great, it

124
00:07:19,820 --> 00:07:22,120
 can lead to great obsession and

125
00:07:22,120 --> 00:07:27,480
 it does lead to irritation and can lead to other

126
00:07:27,480 --> 00:07:32,280
 complications, sexually transmitted

127
00:07:32,280 --> 00:07:33,280
 diseases.

128
00:07:33,280 --> 00:07:36,490
 But the point being with this kind of argument is that that

129
00:07:36,490 --> 00:07:38,440
's not really the important point

130
00:07:38,440 --> 00:07:39,440
 here.

131
00:07:39,440 --> 00:07:41,590
 We're not really talking about the Buddhist talking about

132
00:07:41,590 --> 00:07:42,720
 something quite a bit deeper

133
00:07:42,720 --> 00:07:44,600
 that's hard to really understand.

134
00:07:44,600 --> 00:07:49,350
 It's a reason that why people are understandably turned

135
00:07:49,350 --> 00:07:52,440
 away from, turned off by religious

136
00:07:52,440 --> 00:07:58,940
 teachings or are uninterested in things like self-control

137
00:07:58,940 --> 00:08:02,360
 or celibacy or giving up, just

138
00:08:02,360 --> 00:08:04,720
 in general giving up desires.

139
00:08:04,720 --> 00:08:06,700
 Even if it were possible, they say, why would you want to

140
00:08:06,700 --> 00:08:07,440
 give up desires?

141
00:08:07,440 --> 00:08:10,190
 Why would you want to give up the things that bring you

142
00:08:10,190 --> 00:08:11,040
 happiness?

143
00:08:11,040 --> 00:08:15,400
 And so the Buddha cuts deeper than that and agree with him

144
00:08:15,400 --> 00:08:16,200
 or not.

145
00:08:16,200 --> 00:08:23,530
 He's making a bold claim that actually the true result of

146
00:08:23,530 --> 00:08:27,920
 unwholesomeness is not happiness

147
00:08:27,920 --> 00:08:32,420
 but suffering, which appears to fly in the face of the

148
00:08:32,420 --> 00:08:33,560
 evidence.

149
00:08:33,560 --> 00:08:37,880
 And as a result, many people are not interested in

150
00:08:37,880 --> 00:08:41,520
 spirituality, in things like Buddhism,

151
00:08:41,520 --> 00:08:42,520
 for example.

152
00:08:42,520 --> 00:08:44,040
 It's quite a turn off of everything.

153
00:08:44,040 --> 00:08:48,070
 Maybe that's the wrong word in this context but it's quite

154
00:08:48,070 --> 00:08:50,720
 unpleasant for people here.

155
00:08:50,720 --> 00:08:58,090
 Even just the idea of celibacy for many people evokes

156
00:08:58,090 --> 00:09:03,440
 repression and a lot of suffering.

157
00:09:03,440 --> 00:09:07,200
 We have to deal with this in this verse in the three parts.

158
00:09:07,200 --> 00:09:12,240
 So he talks about not doing, if one should do deeds, do

159
00:09:12,240 --> 00:09:15,240
 things that are unwholesome.

160
00:09:15,240 --> 00:09:19,260
 First of all, the real problem here is not actually the

161
00:09:19,260 --> 00:09:21,360
 performance of the deed.

162
00:09:21,360 --> 00:09:25,750
 The real problem that the Buddha is talking about is

163
00:09:25,750 --> 00:09:28,440
 becoming pleased by them or becoming

164
00:09:28,440 --> 00:09:30,400
 attached to them.

165
00:09:30,400 --> 00:09:35,480
 So it becomes a habit because of course the underlying

166
00:09:35,480 --> 00:09:37,600
 problem is not the deed itself

167
00:09:37,600 --> 00:09:39,000
 but it's the unwholesomeness.

168
00:09:39,000 --> 00:09:42,220
 It's the, not even say unwholesomeness but it's the desire

169
00:09:42,220 --> 00:09:43,440
 attached to them.

170
00:09:43,440 --> 00:09:47,820
 Now why do we call things like desire unwholesome?

171
00:09:47,820 --> 00:09:52,730
 Because they become an obsession and well they do have and

172
00:09:52,730 --> 00:09:55,840
 the Buddha was quick to acknowledge

173
00:09:55,840 --> 00:09:57,320
 they have a benefit.

174
00:09:57,320 --> 00:10:01,110
 There is the gratification of sensual desires and that's

175
00:10:01,110 --> 00:10:03,240
 what you could say the good side

176
00:10:03,240 --> 00:10:04,240
 of it.

177
00:10:04,240 --> 00:10:06,590
 And yes, so there is pleasure that comes from the

178
00:10:06,590 --> 00:10:08,560
 gratification of all kinds of sensual

179
00:10:08,560 --> 00:10:09,560
 desires.

180
00:10:09,560 --> 00:10:14,090
 Sexual desire but also food and even beauty, music, all of

181
00:10:14,090 --> 00:10:16,360
 these things bring pleasure

182
00:10:16,360 --> 00:10:20,560
 and that's certainly a gratification.

183
00:10:20,560 --> 00:10:26,320
 Even the act of obtaining one of these desirable things is

184
00:10:26,320 --> 00:10:29,040
 not actually a problem.

185
00:10:29,040 --> 00:10:33,870
 It's not actually a problem to hear a beautiful sound that

186
00:10:33,870 --> 00:10:35,840
's not unwholesome.

187
00:10:35,840 --> 00:10:41,140
 It's not unwholesome to see something beautiful, it's not

188
00:10:41,140 --> 00:10:44,320
 unwholesome even to feel physical

189
00:10:44,320 --> 00:10:45,440
 pleasure.

190
00:10:45,440 --> 00:10:47,240
 It's not unwholesome to feel the pleasure.

191
00:10:47,240 --> 00:10:50,880
 Now the problem is unless you're truly mindful and

192
00:10:50,880 --> 00:10:53,760
 objective and seeing it as something that

193
00:10:53,760 --> 00:10:58,740
 arises and ceases, absolutely we're going to become

194
00:10:58,740 --> 00:11:00,440
 attached to it.

195
00:11:00,440 --> 00:11:03,730
 We're going to desire to like it and that's going to leave

196
00:11:03,730 --> 00:11:05,500
 an imprint on the brain which

197
00:11:05,500 --> 00:11:10,660
 causes us to want it and be discontent when we don't have

198
00:11:10,660 --> 00:11:13,240
 it such that we think about

199
00:11:13,240 --> 00:11:15,920
 how to get it, how to obtain it.

200
00:11:15,920 --> 00:11:20,650
 When we enter into the cycle of addiction and then we

201
00:11:20,650 --> 00:11:23,720
 wonder why we're dissatisfied,

202
00:11:23,720 --> 00:11:28,840
 why we have discontent in our lives, why we get frustrated,

203
00:11:28,840 --> 00:11:31,240
 why we get angry, why we get

204
00:11:31,240 --> 00:11:35,400
 bored, why we are given to rage, why we are given to

205
00:11:35,400 --> 00:11:38,320
 argumentation, why we fight with

206
00:11:38,320 --> 00:11:42,060
 each other.

207
00:11:42,060 --> 00:11:45,180
 If we looked closely, if we looked carefully, we would see

208
00:11:45,180 --> 00:11:46,880
 how closely related this is to

209
00:11:46,880 --> 00:11:48,360
 our desire.

210
00:11:48,360 --> 00:11:51,530
 We want something, we wish we could just enjoy all kinds of

211
00:11:51,530 --> 00:11:53,260
 sensual pleasures all the time

212
00:11:53,260 --> 00:12:00,240
 like this monk was trying to do without realizing that…

213
00:12:00,240 --> 00:12:07,010
 No, no, and then when we can't get it, we become more upset

214
00:12:07,010 --> 00:12:07,480
.

215
00:12:07,480 --> 00:12:10,260
 When someone stands in our way we get angry at them.

216
00:12:10,260 --> 00:12:12,960
 When someone comes and provides us with a stimulus that is

217
00:12:12,960 --> 00:12:14,360
 unpleasant, telling us, making

218
00:12:14,360 --> 00:12:17,540
 us hear something, saying words that are unpleasant make us

219
00:12:17,540 --> 00:12:19,400
 angry because we just want to listen

220
00:12:19,400 --> 00:12:21,970
 to music, we just want to see beautiful sights, we just

221
00:12:21,970 --> 00:12:23,680
 want to feel beautiful feelings and

222
00:12:23,680 --> 00:12:26,400
 so on.

223
00:12:26,400 --> 00:12:29,780
 And so it leads directly to great suffering.

224
00:12:29,780 --> 00:12:33,310
 Because of addiction, you see, you can say, "Well, I can

225
00:12:33,310 --> 00:12:35,300
 always get what I want, things

226
00:12:35,300 --> 00:12:37,240
 are going to be fine."

227
00:12:37,240 --> 00:12:41,010
 But you can never be sure and you can never say that it's

228
00:12:41,010 --> 00:12:42,640
 not going to change.

229
00:12:42,640 --> 00:12:45,280
 It could change at any time.

230
00:12:45,280 --> 00:12:50,050
 And in the meantime, all you're doing is building yourself

231
00:12:50,050 --> 00:12:52,960
 up to eventual disappointment.

232
00:12:52,960 --> 00:12:54,840
 You're not gaining anything else.

233
00:12:54,840 --> 00:12:56,920
 There's nothing else changing in your life.

234
00:12:56,920 --> 00:12:58,920
 You're not becoming more satisfied.

235
00:12:58,920 --> 00:13:02,320
 You're not becoming happier.

236
00:13:02,320 --> 00:13:05,080
 If anything, you become less happier because the way the

237
00:13:05,080 --> 00:13:06,720
 addiction cycle works is the more

238
00:13:06,720 --> 00:13:09,480
 you get what you want, the less satisfying it is.

239
00:13:09,480 --> 00:13:12,900
 This is why we have to indulge, indulge more and more and

240
00:13:12,900 --> 00:13:14,840
 why our tastes become more and

241
00:13:14,840 --> 00:13:17,120
 more exotic.

242
00:13:17,120 --> 00:13:21,200
 Because it's never, it's because it's diminishing returns.

243
00:13:21,200 --> 00:13:26,840
 It becomes less and less satisfying, in fact.

244
00:13:26,840 --> 00:13:30,200
 So anyone who says that this stuff leads you to happiness

245
00:13:30,200 --> 00:13:32,320
 is incredibly short-sighted.

246
00:13:32,320 --> 00:13:33,840
 I mean, most of us are.

247
00:13:33,840 --> 00:13:36,560
 There's no real criticism here.

248
00:13:36,560 --> 00:13:39,630
 I mean, it's just a general criticism that we are missing a

249
00:13:39,630 --> 00:13:41,200
 very important piece of the

250
00:13:41,200 --> 00:13:43,240
 puzzle in general.

251
00:13:43,240 --> 00:13:48,610
 It's very hard for us to see deep enough to get past this,

252
00:13:48,610 --> 00:13:51,440
 "Well, it makes me happy."

253
00:13:51,440 --> 00:13:55,560
 If you look deeper, and if you look really deep, meaning

254
00:13:55,560 --> 00:13:58,120
 look, and it's not looking far,

255
00:13:58,120 --> 00:14:00,630
 looking very closely at the present moment through

256
00:14:00,630 --> 00:14:02,360
 meditation, which is how this verse

257
00:14:02,360 --> 00:14:05,400
 very much relates to our practice.

258
00:14:05,400 --> 00:14:11,040
 We will see that, in fact, even pleasure, there's nothing

259
00:14:11,040 --> 00:14:14,120
 about it that makes it preferable

260
00:14:14,120 --> 00:14:15,120
 to pain.

261
00:14:15,120 --> 00:14:19,110
 If you think objectively, there's no reason to think that

262
00:14:19,110 --> 00:14:21,480
 pleasure is better than pain.

263
00:14:21,480 --> 00:14:23,720
 Why do we think that pleasure is better than pain?

264
00:14:23,720 --> 00:14:25,240
 It's a good question.

265
00:14:25,240 --> 00:14:29,190
 And if you look closely, you'll see it's just an experience

266
00:14:29,190 --> 00:14:29,400
.

267
00:14:29,400 --> 00:14:30,520
 Pleasure is just pleasure.

268
00:14:30,520 --> 00:14:35,610
 And so in the meditation, the way we deal with or the way

269
00:14:35,610 --> 00:14:38,840
 we approach this real problem,

270
00:14:38,840 --> 00:14:42,230
 the problem of addiction, the problem of attachment, this

271
00:14:42,230 --> 00:14:44,640
 problem that sets us up for such suffering

272
00:14:44,640 --> 00:14:49,620
 and keeps us so caught up in this cycle is to see it

273
00:14:49,620 --> 00:14:53,280
 clearly, to look clearly, to see

274
00:14:53,280 --> 00:14:56,590
 the pleasure as pleasure, to see the liking as liking, the

275
00:14:56,590 --> 00:14:58,440
 wanting as wanting, the seeing

276
00:14:58,440 --> 00:15:03,760
 as seeing, the hearing as hearing, the feeling as feeling,

277
00:15:03,760 --> 00:15:06,320
 to break it up and look at what's

278
00:15:06,320 --> 00:15:08,000
 really happening.

279
00:15:08,000 --> 00:15:09,740
 And when you see what's really happening, you see there's

280
00:15:09,740 --> 00:15:11,640
 nothing about it that's really

281
00:15:11,640 --> 00:15:13,560
 worth clinging to.

282
00:15:13,560 --> 00:15:17,810
 There's nothing in the world, nothing in existence that is

283
00:15:17,810 --> 00:15:19,400
 worth clinging to.

284
00:15:19,400 --> 00:15:23,260
 And you see that, and it's not a matter of repression, it's

285
00:15:23,260 --> 00:15:24,840
 just a matter of letting

286
00:15:24,840 --> 00:15:28,510
 go, a matter of freeing yourself from any need or any

287
00:15:28,510 --> 00:15:29,680
 partiality.

288
00:15:29,680 --> 00:15:34,130
 It's about rooting yourself in reality, as opposed to

289
00:15:34,130 --> 00:15:37,160
 clinging to the past or the future,

290
00:15:37,160 --> 00:15:44,120
 things that don't exist, always wanting, always being

291
00:15:44,120 --> 00:15:46,240
 unsatisfied.

292
00:15:46,240 --> 00:15:49,960
 Or having your satisfaction depend on things that are und

293
00:15:49,960 --> 00:15:52,800
ependable, that are not dependable,

294
00:15:52,800 --> 00:15:55,880
 you can't depend upon in the end.

295
00:15:55,880 --> 00:16:00,810
 So this is in regards to actually doing, now that's really

296
00:16:00,810 --> 00:16:03,520
 just the first part, Papanchi

297
00:16:03,520 --> 00:16:07,330
 Puri Sokaheera, so in regards to doing of unwholesand deeds

298
00:16:07,330 --> 00:16:07,360
.

299
00:16:07,360 --> 00:16:13,460
 Now the second part, Natnam Ki Ra Puna Puna deals with a

300
00:16:13,460 --> 00:16:14,620
 habit.

301
00:16:14,620 --> 00:16:18,120
 So I talked a little bit about it, but just to go through

302
00:16:18,120 --> 00:16:20,080
 this first, the second part

303
00:16:20,080 --> 00:16:24,350
 is in regards to how it becomes habit forming, how

304
00:16:24,350 --> 00:16:27,920
 addiction, the problem with addiction

305
00:16:27,920 --> 00:16:33,100
 is that anything you perform, any activity that you perform

306
00:16:33,100 --> 00:16:35,000
 becomes habitual.

307
00:16:35,000 --> 00:16:38,100
 So wanting breeds more wanting.

308
00:16:38,100 --> 00:16:41,050
 And the same goes with anger, if you're a person who gets

309
00:16:41,050 --> 00:16:42,560
 angry or averse to certain

310
00:16:42,560 --> 00:16:46,040
 things, you'll cultivate a habit of aversion.

311
00:16:46,040 --> 00:16:50,540
 So the idea of doing things again and again is a real issue

312
00:16:50,540 --> 00:16:53,120
 in Buddhism, a real problem,

313
00:16:53,120 --> 00:16:58,770
 something that we have to be very careful when cultivating

314
00:16:58,770 --> 00:17:02,160
 habits, that they are wholesome,

315
00:17:02,160 --> 00:17:03,960
 that they are beneficial.

316
00:17:03,960 --> 00:17:07,750
 So the whole idea behind constant meditation, daily

317
00:17:07,750 --> 00:17:10,680
 meditation, is to cultivate wholesome

318
00:17:10,680 --> 00:17:14,770
 habits, positive habits, habits that do lead to true peace

319
00:17:14,770 --> 00:17:15,920
 and happiness.

320
00:17:15,920 --> 00:17:21,900
 And clarity, that allows us to see things as they are and

321
00:17:21,900 --> 00:17:24,840
 not set ourselves up for real

322
00:17:24,840 --> 00:17:28,080
 disappointment.

323
00:17:28,080 --> 00:17:33,130
 And the third part is Natnami Chandan Ki Rata, not just

324
00:17:33,130 --> 00:17:36,680
 doing things again and again, but

325
00:17:36,680 --> 00:17:41,240
 on top of that, we become content with it.

326
00:17:41,240 --> 00:17:43,000
 We like the fact that we like.

327
00:17:43,000 --> 00:17:47,040
 We're happy about the fact that we want things.

328
00:17:47,040 --> 00:17:50,900
 We become, and not just wanting, but with anger, with

329
00:17:50,900 --> 00:17:55,560
 aversion, with arrogance and conceit,

330
00:17:55,560 --> 00:17:59,300
 we hold on to these and that's even worse.

331
00:17:59,300 --> 00:18:02,920
 So doing a bad deed, this is problematic.

332
00:18:02,920 --> 00:18:06,180
 Doing something if you hurt someone else or if you steal

333
00:18:06,180 --> 00:18:08,120
 from someone else or if you take

334
00:18:08,120 --> 00:18:10,820
 what is not yours, well those are fairly obvious unwholes

335
00:18:10,820 --> 00:18:11,500
ome deeds.

336
00:18:11,500 --> 00:18:12,500
 We call those evil.

337
00:18:12,500 --> 00:18:13,660
 They're evil, why?

338
00:18:13,660 --> 00:18:17,360
 Because they hurt others and they come from a point of view

339
00:18:17,360 --> 00:18:19,280
 of hypocrisy where you don't

340
00:18:19,280 --> 00:18:25,230
 want to experience it yourself and yet you impose suffering

341
00:18:25,230 --> 00:18:27,560
 on others or any number of

342
00:18:27,560 --> 00:18:31,610
 deeds that stains your mind in some way, whether it's

343
00:18:31,610 --> 00:18:34,320
 through anger or through greed.

344
00:18:34,320 --> 00:18:37,820
 Even if you just engage in some kind of addictive behavior,

345
00:18:37,820 --> 00:18:39,800
 well then it leaves a stain on your

346
00:18:39,800 --> 00:18:40,800
 mind.

347
00:18:40,800 --> 00:18:42,880
 It affects your mind.

348
00:18:42,880 --> 00:18:46,690
 Once off, it's still considered harmful, but when you do it

349
00:18:46,690 --> 00:18:48,480
 again and again, this is when

350
00:18:48,480 --> 00:18:53,240
 the real trouble comes, when you get caught up in it

351
00:18:53,240 --> 00:18:56,440
 because it changes your mind, it

352
00:18:56,440 --> 00:19:00,960
 changes your life, it changes your course.

353
00:19:00,960 --> 00:19:05,920
 The worst of all is when you become happy about it, when

354
00:19:05,920 --> 00:19:08,160
 you're content with it.

355
00:19:08,160 --> 00:19:10,530
 Sometimes you do something and you're like, "Oh yes, well

356
00:19:10,530 --> 00:19:11,720
 this is a problem I have and

357
00:19:11,720 --> 00:19:13,040
 I'm trying to work on it."

358
00:19:13,040 --> 00:19:18,460
 An alcoholic who knows they're an alcoholic better than an

359
00:19:18,460 --> 00:19:21,040
 alcoholic who is in denial and

360
00:19:21,040 --> 00:19:24,440
 who is trying to change.

361
00:19:24,440 --> 00:19:28,520
 People I think often dismiss this desire to change because,

362
00:19:28,520 --> 00:19:30,360
 well, the person wants to

363
00:19:30,360 --> 00:19:33,560
 change but they're not changing.

364
00:19:33,560 --> 00:19:34,560
 And it's true.

365
00:19:34,560 --> 00:19:37,530
 We could argue that sometimes we just use it as an excuse

366
00:19:37,530 --> 00:19:38,920
 or we feel guilty about it

367
00:19:38,920 --> 00:19:42,200
 instead of actually doing something about it.

368
00:19:42,200 --> 00:19:44,800
 But there's a story that I'm trying to find.

369
00:19:44,800 --> 00:19:50,640
 It's a story that I plan on sharing with a group here.

370
00:19:50,640 --> 00:19:51,840
 I'm pretty sure it's in the Jataka.

371
00:19:51,840 --> 00:19:57,100
 It's about this bird who lived in this forest and the

372
00:19:57,100 --> 00:19:59,760
 forest caught on fire.

373
00:19:59,760 --> 00:20:05,600
 And the bird didn't want to abandon the forest so it flew

374
00:20:05,600 --> 00:20:08,560
 all the way to the lake, plunged

375
00:20:08,560 --> 00:20:13,120
 into the lake and flew back to the forest and shook itself

376
00:20:13,120 --> 00:20:15,240
 vigorously over the fire.

377
00:20:15,240 --> 00:20:19,360
 And there was an angel or a god or something watching and

378
00:20:19,360 --> 00:20:21,520
 the angel said, "What are you

379
00:20:21,520 --> 00:20:22,520
 doing?"

380
00:20:22,520 --> 00:20:25,520
 And he said, "I'm trying to put out the fire."

381
00:20:25,520 --> 00:20:27,520
 And he said, "What are you crazy?

382
00:20:27,520 --> 00:20:29,520
 You're not going to put out the fire that way."

383
00:20:29,520 --> 00:20:31,040
 And he said, "Well, what else can I do?"

384
00:20:31,040 --> 00:20:33,960
 "Well, yeah, but what do you mean what else can do?

385
00:20:33,960 --> 00:20:36,520
 It's hopeless.

386
00:20:36,520 --> 00:20:39,740
 You're trying to do something and you're never going to

387
00:20:39,740 --> 00:20:41,400
 succeed," is the point.

388
00:20:41,400 --> 00:20:45,440
 And he gave some fairly wise teaching on why it's important

389
00:20:45,440 --> 00:20:46,120
 to try.

390
00:20:46,120 --> 00:20:49,670
 And this is, I think, important in Buddhism, the mind state

391
00:20:49,670 --> 00:20:51,000
, one's mind state.

392
00:20:51,000 --> 00:20:55,580
 Because when you get, if you compare that to someone who

393
00:20:55,580 --> 00:20:57,920
 gives up or gives in and says,

394
00:20:57,920 --> 00:21:02,130
 not even just gives in, but goes full on and says, "Yes, it

395
00:21:02,130 --> 00:21:04,000
's good to eat, drink and be

396
00:21:04,000 --> 00:21:07,920
 merry," for example, or like these two monks who got it in

397
00:21:07,920 --> 00:21:09,880
 their heads that if they were

398
00:21:09,880 --> 00:21:15,300
 having a problem with the holy life, they'll just stop

399
00:21:15,300 --> 00:21:17,060
 being so holy.

400
00:21:17,060 --> 00:21:19,400
 And somehow that would work.

401
00:21:19,400 --> 00:21:23,340
 Really problematic because then when you're 100% behind

402
00:21:23,340 --> 00:21:25,440
 something, you see, it becomes

403
00:21:25,440 --> 00:21:27,960
 much more powerful in the mind.

404
00:21:27,960 --> 00:21:30,840
 And so the result is going to be much stronger.

405
00:21:30,840 --> 00:21:32,200
 So consider these levels as well.

406
00:21:32,200 --> 00:21:33,200
 I think that's important.

407
00:21:33,200 --> 00:21:36,920
 Don't feel too guilty about bad things you do.

408
00:21:36,920 --> 00:21:38,160
 They're bad.

409
00:21:38,160 --> 00:21:42,000
 But just be careful about cultivating bad habits.

410
00:21:42,000 --> 00:21:47,940
 And even if you cultivate bad habits, be clear about where

411
00:21:47,940 --> 00:21:49,360
 you stand.

412
00:21:49,360 --> 00:21:53,800
 Because the worst would be if you become content with,

413
00:21:53,800 --> 00:21:56,800
 complacent, or if you become of the

414
00:21:56,800 --> 00:22:02,060
 view that it's good to become addicted, good to be angry,

415
00:22:02,060 --> 00:22:04,740
 good to be arrogant, good to

416
00:22:04,740 --> 00:22:06,240
 cultivate unwholesomeness.

417
00:22:06,240 --> 00:22:07,240
 Why?

418
00:22:07,240 --> 00:22:09,840
 And that's the fourth part.

419
00:22:09,840 --> 00:22:15,440
 Because the accumulation of evil, especially when you're

420
00:22:15,440 --> 00:22:18,120
 keen on it, when you're happy

421
00:22:18,120 --> 00:22:21,560
 about it, is suffering.

422
00:22:21,560 --> 00:22:26,080
 So that's the teaching for this verse.

423
00:22:26,080 --> 00:22:28,320
 Very simple story.

424
00:22:28,320 --> 00:22:29,320
 Very simple verse.

425
00:22:29,320 --> 00:22:33,160
 And we have its pair coming tomorrow, which is on more of a

426
00:22:33,160 --> 00:22:34,320
 lighter note.

427
00:22:34,320 --> 00:22:39,040
 But a very good teaching, a verse to remember.

428
00:22:39,040 --> 00:22:42,220
 If one should do evil, one should not do it again and again

429
00:22:42,220 --> 00:22:42,480
.

430
00:22:42,480 --> 00:22:48,170
 One should not cultivate contentment or desire for that, or

431
00:22:48,170 --> 00:22:50,840
 one should not be pleased by

432
00:22:50,840 --> 00:22:57,160
 that deed.

433
00:22:57,160 --> 00:23:00,800
 For the accumulation of evil is suffering.

434
00:23:00,800 --> 00:23:04,040
 So that's the Dhammapada teaching for tonight.

435
00:23:04,040 --> 00:23:05,040
 Thank you all for tuning in.

436
00:23:05,040 --> 00:23:05,040
 Wishing you all the best.

437
00:23:05,040 --> 00:23:06,040
 1

438
00:23:06,040 --> 00:23:07,040
 1

439
00:23:07,040 --> 00:23:08,040
 1

