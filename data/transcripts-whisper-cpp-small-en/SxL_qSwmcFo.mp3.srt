1
00:00:00,000 --> 00:00:07,000
 So, follow up on equanimity. The buffalo is an equilibrium,

2
00:00:07,000 --> 00:00:11,000
 isn't it? Can be broken, unlike equanimity.

3
00:00:11,000 --> 00:00:20,640
 So, okay, this is important because there is a lot of

4
00:00:20,640 --> 00:00:25,000
 misunderstanding about what is meant by equanimity.

5
00:00:25,000 --> 00:00:28,960
 And misunderstanding to the extent that people think equ

6
00:00:28,960 --> 00:00:35,000
animity is the most important and will actually create a

7
00:00:35,000 --> 00:00:35,000
 sankhara

8
00:00:35,000 --> 00:00:41,000
 means they will push themselves into an equanimous state.

9
00:00:41,000 --> 00:00:47,750
 And it almost becomes a mantra or it becomes a motto or a

10
00:00:47,750 --> 00:00:49,000
 habit.

11
00:00:49,000 --> 00:00:52,420
 Now, it's a sankhara that you're creating a formation in

12
00:00:52,420 --> 00:00:53,000
 the mind.

13
00:00:53,000 --> 00:01:00,740
 And a good way to understand and to overcome this is to

14
00:01:00,740 --> 00:01:07,000
 study the abhidhamma and to see where equanimity fits in.

15
00:01:07,000 --> 00:01:10,560
 Equanimity is all across the board. There's a lot of unwh

16
00:01:10,560 --> 00:01:14,000
olesome states that arise with equanimity.

17
00:01:14,000 --> 00:01:19,000
 And then there's wholesome states that arise with pleasure.

18
00:01:20,000 --> 00:01:25,000
 So, a person can be happy doing the right thing.

19
00:01:25,000 --> 00:01:29,500
 Meditators often miss this. They think they have to be dour

20
00:01:29,500 --> 00:01:33,000
-faced and flat-lining to be meditating.

21
00:01:33,000 --> 00:01:38,110
 But you can be very happy meditating. You can be very happy

22
00:01:38,110 --> 00:01:42,000
 doing great things, theoretically.

23
00:01:42,000 --> 00:01:46,870
 Because wholesomeness can arise with either equanimity or

24
00:01:46,870 --> 00:01:48,000
 pleasure.

25
00:01:49,000 --> 00:01:52,000
 Wholesomeness can arise with a pleasurable feeling.

26
00:01:52,000 --> 00:01:55,120
 Unwholesomeness can arise with all three types of feelings,

27
00:01:55,120 --> 00:01:57,450
 a pleasurable feeling, a neutral feeling, and a painful

28
00:01:57,450 --> 00:01:58,000
 feeling.

29
00:01:58,000 --> 00:02:06,180
 So, when we talk about the buffalo experience equanimity,

30
00:02:06,180 --> 00:02:11,000
 from a point of view of the abhidhamma, that is equanimity.

31
00:02:12,000 --> 00:02:16,000
 But the point is not to differentiate it to be something

32
00:02:16,000 --> 00:02:19,400
 other than equanimity. It's to see that equanimity isn't

33
00:02:19,400 --> 00:02:20,000
 the point.

34
00:02:20,000 --> 00:02:22,000
 The point is wisdom.

35
00:02:22,000 --> 00:02:30,620
 Because a result of the attainment of wisdom is a form of

36
00:02:30,620 --> 00:02:35,000
 equanimity that isn't a feeling.

37
00:02:36,000 --> 00:02:42,720
 It isn't a sensation of equanimity. It's a judgment. It's a

38
00:02:42,720 --> 00:02:45,000
 non-judgment.

39
00:02:45,000 --> 00:02:48,850
 In terms of our judgment of the experience, a person who is

40
00:02:48,850 --> 00:02:51,000
 in, for instance, sankarupeka nyana,

41
00:02:51,000 --> 00:02:55,760
 which is the knowledge of equanimity towards all formations

42
00:02:55,760 --> 00:02:59,900
, might have great pleasure, might be very happy in that

43
00:02:59,900 --> 00:03:01,000
 state.

44
00:03:02,000 --> 00:03:05,200
 There might be pleasurable feelings arising, and they might

45
00:03:05,200 --> 00:03:07,000
 disappear. They will come and they will go.

46
00:03:07,000 --> 00:03:08,580
 There will be neutral feelings. There will be pleasant

47
00:03:08,580 --> 00:03:09,000
 feelings.

48
00:03:09,000 --> 00:03:15,530
 Sankarupeka nyana is the highest of the world in knowledges

49
00:03:15,530 --> 00:03:20,000
 before the experience of cessation.

50
00:03:20,000 --> 00:03:27,000
 But what is missing is the judgment.

51
00:03:27,000 --> 00:03:35,590
 A buffalo can have an equanimous feeling, but they don't

52
00:03:35,590 --> 00:03:41,840
 have this wisdom. They're nowhere near. They're on the

53
00:03:41,840 --> 00:03:43,000
 polar opposite.

54
00:03:43,000 --> 00:03:46,970
 The truth is, buffaloes are not that dumb, but the

55
00:03:46,970 --> 00:03:50,400
 stereotype of a buffalo is that it's really the opposite of

56
00:03:50,400 --> 00:03:51,000
 wise.

57
00:03:51,000 --> 00:03:53,000
 It's ignorant.

58
00:03:56,000 --> 00:04:00,720
 So all we have to do is realign our thinking, that the

59
00:04:00,720 --> 00:04:04,000
 point is certainly not to become equanimous.

60
00:04:04,000 --> 00:04:08,200
 The point is to become wise, and wisdom leads to non-jud

61
00:04:08,200 --> 00:04:09,000
gment.

62
00:04:09,000 --> 00:04:14,200
 You should understand the equanimity that we talk about in

63
00:04:14,200 --> 00:04:18,000
 a Buddhist sense is just non-judgment.

64
00:04:18,000 --> 00:04:26,000
 It's not experiencing something flat-lined, like a feeling.

65
00:04:26,000 --> 00:04:34,240
 The most important thing certainly isn't the equanimity. It

66
00:04:34,240 --> 00:04:39,000
's the understanding, the seeing the object for what it is.

67
00:04:39,000 --> 00:04:43,320
 Which a buffalo isn't capable of. The reason why a buffalo,

68
00:04:43,320 --> 00:04:47,630
 or most animals, experience equanimity is because of their

69
00:04:47,630 --> 00:04:54,000
 inability to give rise to enough thought.

70
00:04:54,000 --> 00:05:00,480
 Like a young child is unable to give rise to the level of

71
00:05:00,480 --> 00:05:06,000
 thought required to hold a grudge, for example.

72
00:05:06,000 --> 00:05:09,730
 So people always say that children are so pure and innocent

73
00:05:09,730 --> 00:05:10,000
.

74
00:05:10,000 --> 00:05:13,170
 But children are neither pure nor innocent, and anyone who

75
00:05:13,170 --> 00:05:16,000
 knows anything about children can tell you this.

76
00:05:16,000 --> 00:05:19,530
 Children are simple, in the same way that animals are

77
00:05:19,530 --> 00:05:22,000
 simple, because the mind hasn't developed.

78
00:05:22,000 --> 00:05:26,950
 So I've had this question recently of a child who stole

79
00:05:26,950 --> 00:05:30,520
 something from a store, and she was actually going to be

80
00:05:30,520 --> 00:05:31,000
 arrested.

81
00:05:31,000 --> 00:05:34,360
 And she had this kid who couldn't even write her name, who

82
00:05:34,360 --> 00:05:37,000
 was forced to scribble on a piece of paper.

83
00:05:37,000 --> 00:05:40,550
 The security guard forced her, "You sign your name on this

84
00:05:40,550 --> 00:05:44,500
 whatever, and they were going to have her arrested," and so

85
00:05:44,500 --> 00:05:45,000
 on.

86
00:05:45,000 --> 00:05:47,740
 I think that their parents were like, "Look, she's four

87
00:05:47,740 --> 00:05:51,000
 years old, she can't even write her name. She can't write."

88
00:05:51,000 --> 00:05:54,790
 Well, just have her scribble, and so she had to scribble

89
00:05:54,790 --> 00:05:57,000
 with a pen on this piece of paper.

90
00:05:59,000 --> 00:06:04,000
 Animals are in that realm, so they don't have the ability

91
00:06:04,000 --> 00:06:11,820
 to manifest the kinds of things that we would call judgment

92
00:06:11,820 --> 00:06:12,000
.

93
00:06:12,000 --> 00:06:14,870
 But they're judging all the time. Everything is a judgment

94
00:06:14,870 --> 00:06:16,000
 for an animal.

95
00:06:16,000 --> 00:06:21,070
 There is liking and there is disliking. Mostly there is

96
00:06:21,070 --> 00:06:22,000
 delusion.

97
00:06:22,000 --> 00:06:25,370
 And I guess what you might say is that delusion itself

98
00:06:25,370 --> 00:06:28,000
 leads to a certain amount of equanimity,

99
00:06:28,000 --> 00:06:33,000
 because it doesn't allow for rational thinking.

100
00:06:33,000 --> 00:06:36,720
 It doesn't allow for the mind to gather itself together

101
00:06:36,720 --> 00:06:39,000
 long enough to make a judgment.

102
00:06:39,000 --> 00:06:45,000
 Okay. Nothing to add.

103
00:06:45,000 --> 00:06:50,000
 Oh, not really.

104
00:06:50,000 --> 00:06:59,860
 Maybe, yes. I think because Dama is writing, equanimity is

105
00:06:59,860 --> 00:07:04,410
 difficult as a concept because of the Western philosophical

106
00:07:04,410 --> 00:07:08,000
 construct of absolute good or evil,

107
00:07:08,000 --> 00:07:13,000
 whereas there are only good or bad acts.

108
00:07:14,000 --> 00:07:21,000
 Yes, in our Western minds equanimity is something different

109
00:07:21,000 --> 00:07:29,000
 and difficult maybe than it is in the Buddhist sense.

110
00:07:34,000 --> 00:07:39,980
 We have the idea that equanimity is just not reacting and

111
00:07:39,980 --> 00:07:43,610
 being ignorant, but in the Buddhist sense, this is not the

112
00:07:43,610 --> 00:07:44,000
 case.

113
00:07:44,000 --> 00:07:48,000
 This is not what is meant by equanimity.

114
00:07:48,000 --> 00:07:51,000
 That's a good point.

115
00:07:51,000 --> 00:07:58,000
 Because a buffalo may not react to things.

116
00:07:59,000 --> 00:08:04,390
 But I still say they're partial, no? But they may be

117
00:08:04,390 --> 00:08:06,000
 inhibited from being partial due to their delusion.

118
00:08:06,000 --> 00:08:09,000
 But it's certainly not the most important.

119
00:08:09,000 --> 00:08:15,190
 So as Simon says, not go looking for equanimous states. Yes

120
00:08:15,190 --> 00:08:17,000
, don't go looking for any states,

121
00:08:17,000 --> 00:08:20,110
 because as soon as you start to look for a certain state,

122
00:08:20,110 --> 00:08:22,680
 you're creating a sankara, you're creating a habit in the

123
00:08:22,680 --> 00:08:23,000
 mind.

124
00:08:23,000 --> 00:08:29,440
 Buddhism is very much about the deconstruction of habits

125
00:08:29,440 --> 00:08:34,000
 and creating a natural state of being.

126
00:08:34,000 --> 00:08:37,560
 You're seeing with wisdom will lead us not to judge and

127
00:08:37,560 --> 00:08:40,000
 divide reality into good and bad.

128
00:08:40,000 --> 00:08:42,000
 Sounds really good.

129
00:08:42,000 --> 00:08:46,000
 We're losing here.

130
00:08:46,000 --> 00:08:51,000
 So you got that, no?

131
00:08:51,000 --> 00:09:05,000
 The point being that equanimity isn't the most important.

132
00:09:05,000 --> 00:09:07,000
 Important to wisdom.

133
00:09:07,000 --> 00:09:09,000
 I have a question.

134
00:09:09,000 --> 00:09:16,000
 I don't know what's what is we've lost his question now.

135
00:09:16,000 --> 00:09:18,000
 Oh, we're still recording.

