 Okay, good evening everyone.
 Welcome to our evening dum, a question and answer period.
 Not sure what's happening with the audio.
 We're still getting...
 So, we had one session where the audio was fine and then
 another session where the audio
 sound was very deep and slow and wasn't on purpose.
 For those people who thought we're playing with special
 effects, we certainly are not
 playing with special effects here.
 Not at all concerned about whether my voice is deep or
 shallow.
 We only have seven questions tonight.
 So, it's a good sign, it's a sign that people's questions
 have all been answered and everyone
 is swiftly progressing towards enlightenment.
 Perhaps.
 So, anyway, without further ado, we will start.
 We'll start on these.
 I understand that we are to note emotions such as anger or
 annoyance and to try not
 to make positive or negative judgments about these emotions
.
 That seems accurate.
 Are we trying to develop a constant state of apathy?
 If not, how do we live properly without becoming apathetic?
 So this worry of, "I'm going to lose my emotions.
 If I meditate too much, I'll become emotionless."
 And that's a problem because I love my girlfriend.
 When Po Chodok tells this story of a Westerner who came to
 practice with him and said this,
 he said, "I think I'm going to let go of everything and I
 don't want that because I
 love my girlfriend."
 And his response was, "Don't worry about it.
 You've got lots of defilements left to cut off."
 But more to the point, there are ways to condition your
 mind, of course, and there are ways you
 could train yourself to become apathetic, whatever that
 means.
 I understand apathetic, and again, we have to be very
 technical.
 We can't just throw words out.
 We have to define things very precisely, but I assume it
 means having no emotions.
 So we come to a state where we have no emotions, where
 there is no liking or disliking.
 From a Buddhist perspective, we would say it like that.
 So simply to answer your question, no, we're not aiming for
 that, but we're aiming to understand
 our minds and acquire the change or become changed as a
 result of having understood our
 minds.
 You see, there are emotions in the mind that are causing us
 suffering.
 They're causing other people suffering.
 And it's those emotions that once you see them clearly,
 they will never come back.
 Over time, once you see them clearly enough, and
 furthermore, understand the senselessness
 of them, the senselessness of getting caught up and
 obsessed with things that are impermanent,
 unsatisfying, uncontrollable.
 And you let go.
 Your mind lets go and finds a higher freedom that is
 complete peace, and as a result, is
 no longer interested in these things.
 So there are other emotions like compassion or kindness,
 friendliness.
 Even joy doesn't ever disappear.
 And there are other kinds of joy because there are joys
 that are associated with lust or
 greed or desire, which are problematic.
 And you'll see that.
 So if you're worried about losing emotions, don't worry,
 because as long as you're still
 attached to your emotions, it's impossible even if you
 wanted to.
 As I said, you could condition yourself to not feel them,
 but that would ever only be
 temporary.
 And that's not at all what we're aiming for.
 So it's important to be clear what we're aiming for is just
 to understand our emotions.
 And the results come by themselves.
 Once you understand things, there's no way you would do the
 wrong thing, things that
 cause you stress and suffering.
 Why would you?
 It would be absurd to think that once you understand them
 fully, you would still continue
 them.
 I mean, we don't realize that there's a deeper
 understanding that's possible.
 It's not intellectual understanding.
 It's through clear seeing, which comes through meditation.
 Only.
 You don't hear pictures on.
 They can see you.
 Okay, the interface isn't working.
 I don't think buttons are not working.
 So I'm just going to read through these.
 How's YouTube doing?
 Is my voice okay?
 Can you hear?
 Is my voice strange sounding?
 I'm putting questions on YouTube.
 I make a rule not to answer them.
 I'd be here all night.
 That's not the question.
 So here's, what are your thoughts about the importance of
 an intellectual understanding
 of reincarnation?
 First of all, we don't use the word reincarnation.
 It's misleading.
 It's not accurate.
 In Buddhism, we just don't believe in death.
 Death is just a concept.
 The mind keeps arising and ceasing and experience continues
.
 I don't think it's at all really important to have much of
 an intellectual understanding
 of reincarnation.
 I think you should spend the time studying your mind.
 I mean, what we want to do is get rid of the concept that
 at the moment of death, something
 magically happens to stop your mind from proliferating,
 from continuing on this incessant
 process.
 It makes sense if you've never investigated the mind med
itatively, but once you've done
 it meditatively, it's how would you stop the mind from
 continuing to arise?
 How is it that the body does that?
 It seems absurd.
 But we have this strong belief because from children we're
 taught that the mind is the
 brain and the brain has blood and the blood is pumped by
 the heart and if the heart stops,
 no more brain, no more mind.
 Or we're taught even more strange things like then you fly
 up to heaven or down to hell
 for eternity or so on and so on.
 Your soul migrates from body to body and all these things
 were taught, none of which are
 really accurate.
 So you think that somehow learning about reincarnation
 would give you more encouragement in the practice?
 Yeah, perhaps, but it's not really accurate.
 What should give you encouragement is the fact that you're
 stuck with the consequences
 of your actions.
 Death is just meaningless really.
 In the large scheme of things it's meaningful in a sense.
 At that moment it's quite meaningful because the next
 moment is going to be quite different.
 But it's still just another moment and more experiences.
 What's much more important is to understand, yeah, you do
 change based on your experiences
 and those changes aren't things you can just then dump at
 the moment of death and say,
 "Oh well, I'm a terrible person, bye bye, see ya'll."
 No, you get to be a terrible person into the next life and
 suffer all the consequences
 of being a terrible person.
 None of the buttons are working.
 In what way does the noting technique address the Sīla
 aspect of Buddhist practice?
 So this is, I like this one.
 Not a difficult one for me to answer, but this is someone
 who I think has a good question.
 So the noting technique, we would theorize or explain,
 teach that the noting technique
 involves Sīla, Samādhi and Tanya.
 So morality, concentration and wisdom.
 Because you see, in Buddhism we're very concerned or very
 careful about our words and about
 precision in the things we say and in the words we use.
 So when you say morality, it's such a vague word, right?
 That in Buddhism we say, "No, what really is morality?"
 And we'd say, "Morality is a quality of a moment of
 experience.
 It can't be anything else or else it doesn't really exist."
 So morality we use to describe moments of experience that
 are not conducive for the
 arising of defilements, that are not conducive for, well,
 ultimately doing and saying bad
 things.
 So real morality is when you guard your mind.
 It's a guarded state of mindfulness, really, or there's
 many ways, but most effectively
 through mindfulness.
 Because when you're mindful, when you say to yourself, "
Seeing, seeing," it's a guarding
 process.
 Morality is really the meditation practice.
 It's where you are moral.
 Because the implication of not being mindful is that if
 something makes you angry, it comes
 up, you're going to get angry.
 If you're going to get angry, you're going to be immoral,
 act immoral.
 So yes, on the one hand, we could say morality is not
 killing, not stealing, not...
 It's ethical behavior of body and speech.
 That's one Buddhist way of explaining it.
 But on an ultimate level, morality is the state of you, of
 these moments of mind that
 are in a way, or arising in a way that isn't going to lead
 to bad speech or bad action.
 It isn't going to lead you to try to hurt other people or
 manipulate others or so on.
 So in that way, the noting addresses the seela aspect
 directly.
 You can see that in the fact that it leads to concentration
.
 Because the problem with unethical behavior is it's a
 leaving behind of reality in favor
 of your wants and your likes and so on.
 It's based on delusion.
 You would never kill someone or steal something if you were
 clearly aware of things.
 So when you're mindful, you could never do those things.
 As a result, it leads to focus.
 You're no longer distracted or disturbed by bad deeds or
 bad intentions.
 And as a result, there arises wisdom.
 Once you're focused, once you have samadhi, which is focus,
 things come into focus, then
 there arises understanding.
 "Oh, look at this habit.
 That's really not good for me.
 Oh, this habit is good.
 This leads to peace and happiness."
 And your mind starts to change based on that wisdom, that
 understanding.
 So seela samadhi pañjaví arise in the meditation practice
 like that.
 This one is good.
 There's really good questions in here, but I would really
 rather...
 The problem with a person who asks lots of questions is you
 get a sense that their mind
 is not very focused.
 Their mind is not very still.
 And so in some sense, you want them to calm down before you
 start answering their questions.
 Because it appears that all their mind is engaged in is
 questioning.
 And what good is it going to do if I answer for a person
 whose mind is all up in the air?
 What happens is they ask more questions.
 They think about what you answer and they puzzle it out.
 At best they just go away and leave you alone, but at worst
 they start to ask more and more
 questions.
 I'm not saying it's good that they leave you alone.
 I'm saying it's just less of a headache.
 But it would be much better if they were to note doubting,
 doubting.
 I remember about questions.
 One way of...
 A good way of solving all your questions is to say doubting
.
 Doubting or confused.
 Confused.
 And a lot of your questions will just disappear.
 But I'll go through these.
 I'll see which ones are actually.
 Because they're pretty good questions.
 It's not that the questions are wrong.
 It's just not keen on answering lots and lots of questions
 because of that.
 I've actually cut people off before.
 You're cut off.
 No more questions for you.
 Not forever.
 But too many questions I say.
 You have to go and meditate.
 I want to see fewer questions from you.
 Okay, I don't understand the first part so I'm just going
 to skip it.
 Oh no, I write it.
 It leads into the next part.
 I want to go all the way.
 What should I do?
 Come to the monastery?
 No, you can meditate at home.
 I mean of course if you come to the monastery it would be
 much easier.
 It would be much better to progress.
 You say you're gaining progress so just keep that up.
 If you want real intense progress, come and stay with us or
 find a place to dedicate yourself
 to the practice.
 What are the common mistakes in meditation?
 Didn't we have this question recently?
 I did have it recently.
 What is the most common mistake?
 Maybe I deleted it.
 I may have deleted it.
 I think it's problematic because I don't really have a
 sense of...
 What kind of a question is it to say what are the most
 common mistakes?
 The question I ask is why is that important for you to get
 an answer on that?
 So then we can figure out what's the problem because you're
 concerned that you might be
 practicing wrong.
 You're concerned that you might be making mistakes.
 That really shouldn't be a concern if you've read my
 booklet and really understood it because
 what's happening there is there's a rising doubt.
 Am I doing it right?
 As a teacher I think are you doing it or not doing it?
 There's not really much to it.
 You see what happens when you start to practice.
 Doubt arises.
 That's a part of the practice.
 It's valid.
 The answer, the response should be to say doubting, doub
ting.
 Because you'll never come up with an answer.
 You can't just sit there and say am I doing it right and
 hope for suddenly oh yes I'm
 doing it right.
 Only because doubt is habit forming.
 The more you doubt the more you're liable to doubt.
 So rather than answer what are the most common mistakes in
 meditation it's like read, study
 how to meditate or find a teacher and practice it.
 And when you have doubt just say to yourself doubting, doub
ting, am I doing it right?
 Because mostly that's just your mind's lack of strength,
 the weakness of the mind.
 The mind is weak and fickle.
 So flitting here and there and indulging in old habits of
 doubt.
 When I'm not strong, fall into doubt.
 It's a very strong emotion so you get caught up on it.
 Many different things, noting what's the order, what should
 we focus on.
 So we say to focus on the breath because it's a good, not
 the breath but the stomach rising
 and falling.
 It's a good object to always come back to.
 But then just note whatever else comes, one thing noted and
 when it's gone come back to
 the breath.
 If during your daily life much less organized I would say
 just whatever you can be mindful
 whenever you remember it.
 Note whatever comes.
 Do we need to say exactly what we are doing at the right
 time?
 It's not magic like somehow if you do you're going to
 unlock some secret door that gets
 you to enlightenment.
 It's just good for you.
 So take it more as organic.
 You know you don't have to this way, that way or however.
 Use it as a tool to learn more about how your mind works,
 what's going on right now to keep
 yourself present.
 Okay here's a question about non-self.
 So we have this phrase from the sabassava sutta, natimaya t
ati, wa asa sachato jietato jiti
 upachati.
 So the view arises that this is right, this is true, that
 my self doesn't exist.
 Natimaya tati means myself is not.
 So the question is wait is that wrong view because the budd
ha seems to be saying that
 it's wrong view.
 He doesn't say wrong view with any of this.
 He says these are views and that's the problem.
 What does it mean to say my self doesn't exist?
 You're already, the premise is already wrong.
 The framework you're starting with it's like saying we have
 this microphone, let's talk
 about this microphone.
 Does this microphone exist?
 As soon as you start that you've already set a base of the
 microphone.
 So the same with my self.
 My self doesn't exist, does it not exist?
 As soon as you say my self doesn't exist you're using, what
 you're engaging in a framework.
 Why that framework is wrong is because it has nothing to do
 with experience.
 My self has no relevance in experience.
 Experience is seeing, hearing, smelling, tasting, feeling,
 thinking.
 As soon as, the problem is we have this overarching sense
 of self and we've built it up lifetime
 after lifetime so we don't stay with experience.
 We get into it.
 This is me moving, this is me talking, this is me hearing,
 this is me seeing.
 But those are all just thoughts and again thoughts are
 experience.
 Anytime these other things come they're all just diti, they
're views and they're getting
 you off track.
 There's no benefit to saying nati meyata.
 It's quite a detriment actually because you're lost in
 intellectual thought.
 Is it true you can get enlightened by certain chanting and
 reflection of oneness?
 Survey says no.
 And that's all for tonight.
 So probably there are more questions actually.
 Usually people are still asking questions but unfortunately
, no.
 It's not working.
 So it's not working for us, it's probably not working for
 anybody.
 Dare I go and check, refresh the page?
 Maybe it's working now.
 Oh yes, it's working now.
 Oh wait, this one was here.
 Why didn't I read that one?
 Right, because this is one we were talking about.
 Wouldn't it be much more maintainable throughout the day to
 just be aware of experiences without
 the constant noting?
 Much more maintainable.
 By that you probably mean easier, right?
 Or I would, you may not have wanted to say that, but I
 would criticize or critique what
 you're saying by saying, "Doesn't that just mean easier?"
 You say it's, "A whole day of talking to myself seems
 pretty tiring."
 So you want to expend less energy.
 I mean, I'm putting words in your mouth and I'm rephrasing
 it in a less charitable way.
 But that's the argument I'd put back in you.
 If you want easy, there are lots of things to do that are
 easy.
 So that's not necessarily a sign that it's good.
 As far as not expending so much energy, we're actually
 interested in cultivating energy,
 in working out.
 It's like a mental workout in many ways.
 We want our minds to be sharper, to be stronger, and that's
 going to take some work.
 So yes, the meditation is artificial.
 Just like weights are artificial.
 You go and you sit on this weight bench and you pump
 weights.
 It's artificial.
 But what's the result?
 The result is a stronger body.
 Meditation, the noting, is artificial.
 No question.
 All meditation, really in a sense, is an artificial
 activity designed to strengthen the mind in
 some way, or train the mind, or do something to the mind.
 For us, it's to strengthen the mind's clarity, to be able
 to see things as they are.
 So the noting is indispensable for that.
 Could you gain insight without it?
 Probably.
 Would it be as powerful?
 I don't think it would be as powerful.
 I've answered this question a lot, so I was almost going to
 delete it.
 And it said, it really reminded me that you have to answer
 it again and again, the same
 question.
 This one, we're done.
 This one, we're done.
 So good.
 Uh-oh, another long one.
 Starting with the phrase, "I am a physicalist."
 I don't prefer the term of a materialist.
 Someone called me out.
 I used to use materialist, but someone said, "No, no, that
's not for the... not to be pedantic,
 but I'm just following what they said."
 I simply don't see any objective evidence to claim that the
 mind is non-material.
 Wow.
 Huh.
 I don't know what to say.
 What is material?
 If it's not non-material, then I assume you have some
 evidence to say that it's material?
 Right?
 Because you say there's no evidence to say that the mind is
 not material.
 Right?
 But there's no evidence to say that the mind is not a duck.
 Right?
 That the mind is not a platypus.
 You have no evidence that the mind is not a platypus.
 Right?
 It's basically what you're saying.
 So then I would say, "Wait a minute.
 It's not... the mind...
 You have no evidence to say that the mind is not a teapot.
 Russell's teapot is not a teapot orbiting the sun or
 whatever."
 Of course we have no evidence, but we're not required to
 provide evidence that it's not
 something, which I think you've caught yourself in, which
 is...
 You have to be careful there.
 It sounds like you're a scientist.
 I don't see an objective evidence to demonstrate the claim
 that the mind is non-material.
 We don't have to prove that.
 You have to prove that it's...
 You have to provide evidence that the mind is material,
 which is called the hard problem.
 And it's the hard problem.
 Google it.
 Philosophy.
 And people debate it, and of course materialists, physical
ists, they find ways to logically
 argue themselves in circles until they convince themselves
 that it's physical.
 But it's the hard problem because the mind is not physical.
 I mean, there's no connection.
 The real question is, what is physical?
 We don't really know.
 Quantum fields, strings, who knows?
 It's not atoms.
 It's not even subatomic particles.
 What's an electron?
 Right?
 Is it a wave?
 Is it a particle?
 Lots of theories.
 But it's absurd really because what's going on when that
 happens?
 When you talk about...
 When I just talked about all that, what was really
 happening was...
 When I point to my head, but...
 Was thoughts, which are experience, which are immaterial.
 I mean, there's nothing material there.
 There's just an experience.
 Maybe it's misleading to say material, non-material.
 There's experience.
 We know that.
 That's what Descartes saw.
 And he was very wise to see that.
 I mean, he wasn't the first one, obviously.
 In India, the Buddha was far, far beyond Descartes.
 But he wanted a modern example that the Westerners look up
 to.
 Kogito ergo sum.
 Kogito.
 It's the thinking.
 That's the ultimate base of reality.
 And he was right.
 The experience.
 So then, if that's the basis, then your argument and any
 arguments about physicalism or materialism
 are really hard to maintain because they're dependent on
 that experience.
 You know, the brain and the thought experiment.
 How do we...
 And that's where The Matrix came from, this movie in the '
90s.
 How do we know we're not just in some virtual reality world
?
 We can't know that we're not.
 We can't know that we're sitting here.
 Because I can postulate a thought experiment of some
 complex, advanced virtual reality
 thing that was keeping us this stimuli to make us think
 that this is where we were.
 And that thought experiment points out the fact that there
's very little we can know,
 if anything, beyond experience.
 Anything else is just conjecture based on noting or making
 note of observations.
 Gravity.
 We think that there's such a thing as gravity that when you
 drop something, it's going to
 fall.
 How do we know that the next time I drop something, it's
 going to not go up?
 We don't know.
 We don't know that gravity is not just some god playing
 tricks on us.
 And suddenly he's going to say, "Okay, no more gravity."
 I may be going too far with all of that.
 There may be some...
 Anyway, I don't want...
 It's not my intention to go that far.
 It's my intention to poke holes in the certainty of science
, the certainty of physical science,
 physics, and so on.
 Because I think the people who are really keen on it and
 scientists who are really working
 in science will agree with at least some of what I say when
 they say, "Look, we don't
 make such claims that the mind is physical."
 What do we do?
 We work with machines and we conduct experiments and we
 come up with results.
 And based on those observations, we do useful things.
 I've talked to scientists, physicists, doing their PhDs and
 stuff and say, "This is what
 we do."
 We do these experiments, we come up with results, and they
're useful.
 Some people have these theories about reality the Buddha
 didn't go for.
 There's much more of a guy doing experiments saying, "Look,
 if you do this, this is the
 result and it's useful."
 The Buddha was another one of these scientists who did
 something practical.
 And moreover, more importantly, he saw that that's reality,
 that's truth.
 Truth is not found in making theories about, "Is the mind
 material?
 Is the mind immaterial?"
 That's just rubbish.
 Not useful.
 Oh, here he goes on.
 This is interesting.
 So, I mean, certainly I'm not going to convince everyone
 with these arguments.
 The other part is the claim that reincarnation is true, so
 I've just talked about that.
 We don't believe in reincarnation.
 We just have the sense that you'd have to provide evidence
 based on this experience continuing
 through observation.
 You'd have to provide evidence that somehow that stops at
 the moment of death.
 You're the one that is required to provide evidence because
 you claim that at the moment
 of some abstract conception of death, that we see in other
 people but never don't have
 any memory or experience of having it happen to us
 firsthand, that by that somehow the
 mind just stops.
 It doesn't appear to be backed by evidence or observation.
 So then he says, "You seem to be very intelligent and I'm
 surprised you seem to cling to the
 worldview of some kind of dualism as opposed to just
 accepting that consciousness is simply
 our brain's process in reality."
 Yikes.
 How do we unpack this?
 Some kind of dualism.
 I don't see what the problem with dualism is.
 There's one thing, why can't there be two?
 Science likes to find the one theory of everything.
 What would that prove?
 You'd still have to figure out why is this theory the truth
, right?
 We still don't have an answer for that.
 Buddhism doesn't provide an answer like science is seeking
 for.
 Why the heck are things the way they are, right?
 What's the deal with the universe?
 That kind of thinking comes from a theistic background,
 really.
 It's what implicates or implies the existence of a higher
 being that created the world,
 even if it's as a backlash to that, a reaction to that.
 If you react and say, "No, there's no ... Then we're going
 to find out the true meaning."
 You're still barking up the wrong tree.
 The universe is kind of like, I would say, realities like a
 balloon.
 The further you go out, it stretches.
 You have to put more force and more force in.
 You're thinking more and more complicated.
 All you're doing is stretching.
 Cling to the worldview of dualism.
 That doesn't seem like a valid criticism.
 Not that I think I do cling to any kind of dualism.
 As I said, I think experience pretty much sums up reality.
 It's much more complicated than that, but the complications
 just show variation of experience.
 Brains aren't something that exist in my mind.
 A brain is an abstract concept.
 The thing we use to call a brain, we can see it actually
 affects the working of the mind.
 You can cut out part of the brain.
 Experience changes quite drastically.
 If you kill the brain, the experience will change.
 It will switch.
 That's what we call death, when the brain is no longer
 alive.
 That's not because the brain creates experience.
 It's because we've spent how many years?
 We start with nine months in the womb obsessing over this
 thing and identifying with it, based
 on all of our past defilements and delusions.
 Throughout our life, we reaffirm our connection with the
 brain.
 This is it.
 This is who I am.
 Yes, my arms.
 Then it becomes more and more convoluted and complicated as
 we live our lives.
 It's like a prison, really.
 Such that we really are at a loss.
 We're not equipped to deal with the loss of part of the
 brain, which is why there is a
 strong change of experience.
 Anyway, I don't want to offer this up as evidence, because
 I think it's misleading.
 I don't feel the need to offer evidence.
 There's no question in my mind, and it seems quite obvious.
 Unfortunately, we've just been so spun out of control with
 our physicalist views.
 Reality is experience.
 Experience is real.
 Read some philosophy.
 Not all philosophy.
 Read Descartes.
 Maybe that's not a good advice.
 I'm not that fond of Descartes.
 I just like what he said there about that.
 Just accepting that consciousness is simply our brain's
 processing reality.
 What the heck does that even mean?
 Do you know what a brain is?
 A brain is neurons firing.
 It's cells.
 What are you talking about?
 Brains don't process anything.
 Brains are just cells made up of subatomic particles firing
.
 It's like a complex pinball machine.
 How could that be, processing anything?
 There's a really interesting, not Buddhist, and I kind of
 lost interest after a while,
 but I can't remember.
 He's a fairly famous philosopher.
 He was talking about this idea of information.
 We say information being processed by the brain, and it's
 just absurd, because information
 is only a meaningful term when you're dealing with a person
 processing, a person conscious
 of the information.
 This computer has no information in it.
 It's information only to a person, only to a sentient being
, only to a consciousness
 that can process it.
 The brain is just a complex computer.
 There's no way you could possibly explain it as anything
 else.
 Now, I've talked to people who tried.
 One example goes way back to Socrates, Plato, who brought
 up the argument whether the mind
 is just like resonance.
 You have a banjo, and you play the banjo.
 Suddenly there's this sound.
 "Oh, where did that sound come from?"
 They say, "Ah, consciousness is just like that.
 It's an emergent property," or something like that.
 The funny thing about emergent property is smoothness.
 This is not smooth, but when you put it together in the
 right way, smoothness arises.
 Why couldn't the brain be like smoothness or whatever?
 The funny thing is that it's only smooth to the mind, to
 the person.
 It's not smooth in any other sense of the word.
 Hot.
 Something is only hot to the mind, to the experiencer.
 I don't want to get too much into it.
 If you really think I'm off the wall crazy, by all means, I
'm not looking for more students.
 Let's move on.
 There's more here, though.
 It's quite interesting.
 I don't see how any personal evidence you have from
 meditation couldn't easily be a hallucination
 of some sort.
 "Aha!"
 But then I turn the tables on you and say, "How do we know
 this all isn't a hallucination?"
 That's where Descartes was brilliant.
 He really got it.
 How did he get it?
 Because he sat down and meditated.
 He looked at his experience.
 He said the same thing that you're saying now.
 How do we know it's not all an hallucination?
 He said, "How do I know, for example, I'm not being tricked
 that I'm seeing this computer,
 this candle."
 He said, "This candle."
 He's looking at the candle.
 He said, "But wait a minute.
 Could I be tricked into thinking that I'm thinking?"
 He said, "No.
 Could I be tricked into thinking that I'm experiencing and
 not really be experiencing?"
 "Wait a minute.
 That's not possible.
 That's not possible to be tricked into thinking you're
 experiencing because you have to experience
 to think."
 Right?
 Make sense?
 So there's one thing that you can be sure of, and that's
 experience.
 Now you elaborate that and you say, "Okay, but what about
 all those things you experience?"
 We're only concerned with understanding experience much in
 the same way as a scientist would.
 So if I see that this experience is followed by that
 experience, the result of that, of
 seeing that, this experience leads to that experience, and
 that experience is unpleasant
 or is not what I was aiming for, the result of that is that
 your experience will change.
 You will see the result of that is that those experiences
 are diminished because the mind
 places less emphasis on them.
 The mind is less excited about them, and through the lack
 of excitement you'll see results.
 So it's just another scientist doing experiments and
 getting results and saying, "Hey, this
 works."
 It's not even really a question of what if tomorrow it
 doesn't work because it's working
 now.
 Right?
 It's like gravity.
 What if gravity just stopped working?
 What if meditation just stopped working?
 I can't say that it's not.
 Somehow the mind is suddenly going to work in a different
 way.
 You could hypothesize that that might happen.
 It doesn't really matter because I'm doing this and it's
 having the result.
 The scientists will say the same thing.
 It doesn't matter if gravity stops working.
 It's working now.
 As long as it's working, this is a useful experiment.
 It allows us to build rockets and stuff.
 If gravity changed well, then we'll figure out new
 experiments and new rockets and whatever.
 So that criticism also is not really valid.
 At the very least, why don't we just admit that we don't
 accept things blindly to an
 extent?
 At the very least, why don't we just admit that we
 currently don't know what consciousness
 is or whether reincarnation is true?
 You want to be hard on me and then say, "Look, why do we
 have to be so sure about these things?"
 I hope that some people have heard what I've said and said,
 "Okay, I kind of get where
 he's coming from and it's all good."
 I'm not suggesting a definition of consciousness.
 I'm just saying consciousness exists.
 I don't have to say anything else because you're the one
 that's trying to say, "You
 are not you," but the people who present questions like you
're presenting are the ones who have
 to find some way to connect it with the brain and not
 connect it because I agree that it's
 connected with what we call the brain, but to say that it's
 actually physical.
 It's not a hard problem.
 It's impossible.
 It's just a silly exercise.
 It's a wild goose chase.
 Of course, not everyone agrees with me, but you asked me.
 Reincarnation is true.
 Again, your challenge is to provide me with compelling
 evidence that the mind ceases to
 continue at the moment of death.
 There's some interesting evidence in that regard in terms
 of near-death experiences
 where the brain is totally dead and there's no measurable
 brain activity.
 Then the mind comes back and was able to remember
 experiences, the had experiences during the
 time that the brain was dead.
 It's not conclusive.
 I don't really want to go there, but it's interesting to
 talk about because it's not
 even a question of, "Oh, maybe there was some brain
 activity that we couldn't measure."
 Actual thought is accompanied by intense brain activity.
 Any theory of the brain creating consciousness requires
 complicated and highly measurable
 brain activity.
 They say, "That's just not there during this time."
 The mind is doing something else totally unrelated to the
 brain.
 There's not proof and people criticize and argue against
 this.
 Interesting anyway, I think admitting ignorance is the best
 action in this case.
 I hope you're a little bit clear about why I disagree with
 you in some sense.
 Yes, I think ignorance is the best action in many cases,
 admitting ignorance rather
 than being too sure of yourself.
 I think you are guilty of that in some regards as well.
 Instead of being ignorant, you're saying, "Oh, admitting
 ignorance."
 You're saying, "Why don't we just admit that it's the brain
?
 Why would we admit such a thing?
 We need evidence."
 Okay, last question.
 How do I raise my self-esteem?
 I feel like most of my life is affected by what I think of
 myself.
 People have told me I am smart, but I can't advance in math
 or any other subject without
 getting sad or frustrated.
 Deal with the sadness and frustration.
 Forget about the self-esteem.
 You're worthless.
 We're all worthless.
 We have no worth.
 You have no worth.
 Anyone who told you otherwise was wrong.
 But that makes you free because what does it mean to have
 worth?
 Maybe it's really the wrong.
 I don't know if that's such a fair thing to say.
 People don't like it when I say that.
 I understand.
 Meaningless, purposeless.
 We don't have any worth.
 We're worthless because worth is just an arbitrary measure.
 Someone says you're smart.
 What does that mean?
 Someone says you're stupid.
 What does that mean?
 They're comparing you to someone else.
 Why is that?
 We have to question so many of our presumptions.
 That's what philosophers do in many cases.
 They just question people's assumptions.
 We have so many assumptions that somehow my comparison with
 someone else is meaningful.
 Yeah, I got 90 on my test.
 Someone else got 95.
 What does that mean?
 I failed my test.
 What does that mean?
 I practiced meditation and became an arahant.
 I practiced meditation and I didn't get anywhere.
 What does that mean?
 None of it means anything.
 There's no meaning.
 All we can say about life is there are certain things that
 are consistent.
 In other words, I want this as the result and I get this as
 the result.
 And other things that are inconsistent.
 I want this as a result and it's not the result of what I'm
 doing.
 And if you're mindful, if you're observant and actually
 learn about reality, you become
 more consistent, which really translates into happiness and
 peace.
 Because you stop doing things that make you unhappy.
 Saying this will make me happy and it makes you unhappy.
 That's really the essence of mindfulness.
 So getting sad and frustrated, you have to look at your
 situation saying, "What is it?
 What of my situation is creating me sadness and frustration
?"
 Or, "What of my experience is creating me suffering?
 What of my experience is creating happiness?"
 Sorry, and the sadness and the frustration are creating
 suffering for you.
 So they're really what you have to address.
 Not that you're smart or stupid or hate yourself.
 No, that you hate yourself.
 The reactions you have.
 Not that you are useless or worse than other people.
 Don't compare well to other people.
 Not that you can't advance in math.
 I guarantee you that's not something important.
 It's useful.
 If you advance in math, you might get more money and get a
 better job, make more money.
 But it's not important with a capital I.
 Important is consistency.
 Doing things that make you happy.
 You want to be happy and yet you get sad and frustrated.
 What good is that?
 That doesn't advance your goal to be happy.
 So you have to work on that.
 Don't get sad or frustrated.
 How do you do that?
 Well, be more mindful.
 Study it.
 Examine these emotions and your mind will become less
 interested in you.
 And become more peaceful.
 More and more questions.
 That's not fair.
 I can't answer them fast enough.
 I'm cutting it off.
 These new questions have to wait until next week.
 I'm the tyrant.
 I'm in charge here.
 I can't delete questions.
 I'm not going to delete the new ones.
 They'll stay until next week.
 But you're not sneaking new ones in.
 That's not fair.
 Now it's not loading.
 Time is at 45 minutes.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
 I'm going to delete the new ones.
