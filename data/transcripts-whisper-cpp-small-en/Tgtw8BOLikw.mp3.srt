1
00:00:00,000 --> 00:00:05,620
 Tomac has a question. What's your opinion on other schools?

2
00:00:05,620 --> 00:00:06,320
 What do you think about

3
00:00:06,320 --> 00:00:09,630
 learning the Dharma from bhikkhus, practicing jhanas or

4
00:00:09,630 --> 00:00:11,860
 even people like Zen masters or

5
00:00:11,860 --> 00:00:23,080
 Tibetan ghrampushes? For what point can this be beneficial?

6
00:00:23,080 --> 00:00:25,820
 No I think contentment is the most beneficial. Be content

7
00:00:25,820 --> 00:00:27,440
 with whatever practice you have

8
00:00:27,440 --> 00:00:30,560
 and be content with the core of the Buddha's teaching. I

9
00:00:30,560 --> 00:00:32,720
 talked quite a while about this,

10
00:00:32,720 --> 00:00:38,320
 a bit about this anyway in the last Mungala, I think, yeah

11
00:00:38,320 --> 00:00:42,600
 the last talk on the blessings.

12
00:00:42,600 --> 00:00:44,640
 Contentment with the core of the Buddha's teaching. So one,

13
00:00:44,640 --> 00:00:45,640
 contentment with whatever

14
00:00:45,640 --> 00:00:48,930
 tradition you find yourself in, but two, contentment with

15
00:00:48,930 --> 00:00:50,960
 the core. So not worrying so much about

16
00:00:50,960 --> 00:00:55,590
 developing this or that jhana, this or that mental state,

17
00:00:55,590 --> 00:00:57,760
 or this or that attainment or

18
00:00:57,760 --> 00:01:01,060
 this or that practice. Pick one tradition that sticks to

19
00:01:01,060 --> 00:01:02,840
 the core of the Buddha's teaching

20
00:01:02,840 --> 00:01:06,310
 and develop that. Don't be concerned with everything else,

21
00:01:06,310 --> 00:01:07,360
 especially because in this

22
00:01:07,360 --> 00:01:13,240
 day and age our minds are so weak and so full of confusion

23
00:01:13,240 --> 00:01:16,560
 and delusion and defilement that

24
00:01:16,560 --> 00:01:20,160
 we have to really just take the essence of the Buddha's

25
00:01:20,160 --> 00:01:22,200
 teaching as best we can before

26
00:01:22,200 --> 00:01:25,840
 it disappears. Or even finding a good teacher, someone who

27
00:01:25,840 --> 00:01:27,680
 can lead you through the, even

28
00:01:27,680 --> 00:01:31,750
 the core of the Buddha, even just the very basics is

29
00:01:31,750 --> 00:01:33,760
 difficult. So we should be content

30
00:01:33,760 --> 00:01:37,340
 with that. Contentment is the most beneficial. Be content

31
00:01:37,340 --> 00:01:39,120
 with the core and content with

32
00:01:39,120 --> 00:01:43,490
 one practice that is based on the core. And I think that

33
00:01:43,490 --> 00:01:46,120
 would apply even in the Buddhist

34
00:01:46,120 --> 00:01:51,150
 time. That it's the contentment with the very essence of

35
00:01:51,150 --> 00:01:53,680
 reality that is most important

36
00:01:53,680 --> 00:01:57,250
 because that is the essence of Buddhism. It's contentment

37
00:01:57,250 --> 00:01:59,120
 with reality as it is, not needing

38
00:01:59,120 --> 00:02:04,250
 to find more, not needing to make something of reality. So

39
00:02:04,250 --> 00:02:06,720
 in that sense it's going to

40
00:02:06,720 --> 00:02:09,530
 be contentment all the way down, starting with contentment

41
00:02:09,530 --> 00:02:11,400
 for tradition, into the contentment

42
00:02:11,400 --> 00:02:14,370
 with the core of the tradition, the core of the practice,

43
00:02:14,370 --> 00:02:16,160
 the core of the Buddha's teaching,

44
00:02:16,160 --> 00:02:20,340
 and finally to contentment with everything that arises as

45
00:02:20,340 --> 00:02:22,280
 it is to see it, come and to

46
00:02:22,280 --> 00:02:24,800
 see it go and to not cling to it, to not want for it to

47
00:02:24,800 --> 00:02:26,320
 come or want for it to go and to

48
00:02:26,320 --> 00:02:28,520
 finally let go and become free.

49
00:02:28,520 --> 00:02:29,520
 Okay.

50
00:02:31,120 --> 00:02:32,120
 Okay.

