1
00:00:00,000 --> 00:00:04,600
 Okay, good evening everyone.

2
00:00:04,600 --> 00:00:10,880
 Welcome to our evening Dhamma session.

3
00:00:10,880 --> 00:00:17,220
 Today I thought we'd do a Dhammapada video, so we've been

4
00:00:17,220 --> 00:00:24,160
 doing a formal recording.

5
00:00:24,160 --> 00:00:33,160
 Hello and welcome back to our study of the Dhammapada.

6
00:00:33,160 --> 00:00:39,020
 Today we continue on with verse 159, which reads as follows

7
00:00:39,020 --> 00:00:41,160
.

8
00:00:41,160 --> 00:00:51,400
 Atanancheta Thakayira yatanyamanu sasati sudanto vatadham

9
00:00:51,400 --> 00:00:55,160
eta atahikira dudamo.

10
00:00:55,160 --> 00:01:08,160
 Which means...

11
00:01:08,160 --> 00:01:15,840
 As one yatanyamanu sasati, as one instructs others, if one

12
00:01:15,840 --> 00:01:20,160
 should make oneself so.

13
00:01:20,160 --> 00:01:26,810
 Meaning if one should deport oneself in a way that one

14
00:01:26,810 --> 00:01:31,160
 instructs others.

15
00:01:31,160 --> 00:01:47,140
 Or one should... sorry, one should deport oneself as one

16
00:01:47,140 --> 00:01:51,160
 instructs others.

17
00:01:51,160 --> 00:01:58,630
 Sudanto vatadhameta, those who are well trained, indeed may

18
00:01:58,630 --> 00:02:02,160
 train or should train others.

19
00:02:02,160 --> 00:02:12,460
 Atahikira dudamo, for one it is difficult to train the self

20
00:02:12,460 --> 00:02:13,160
.

21
00:02:13,160 --> 00:02:17,160
 The self is difficult to tame.

22
00:02:17,160 --> 00:02:25,160
 Atahikira dudamo, the self indeed is difficult to tame.

23
00:02:25,160 --> 00:02:31,160
 So, sorry to put it all together.

24
00:02:31,160 --> 00:02:35,160
 One should deport oneself as one instructs others.

25
00:02:35,160 --> 00:02:41,030
 Those who are well tamed, well trained, they should train

26
00:02:41,030 --> 00:02:42,160
 others.

27
00:02:42,160 --> 00:02:51,160
 For indeed, it is the self that is difficult to train.

28
00:02:51,160 --> 00:02:57,560
 So, similar to the last verse, and going in this vein of

29
00:02:57,560 --> 00:03:02,160
 the atavaga relating to the self.

30
00:03:02,160 --> 00:03:05,570
 The story behind this is an interesting story, another

31
00:03:05,570 --> 00:03:08,160
 short one, but interesting.

32
00:03:08,160 --> 00:03:13,430
 About an elder who received instruction from the Buddha and

33
00:03:13,430 --> 00:03:17,160
 gathered a very large company of monks together.

34
00:03:17,160 --> 00:03:21,160
 And they all went off to the forest.

35
00:03:21,160 --> 00:03:25,480
 So, it seems to have been a sort of a theme, there would be

36
00:03:25,480 --> 00:03:26,160
 a leader.

37
00:03:26,160 --> 00:03:34,710
 And he would collect a group of monks and they would go off

38
00:03:34,710 --> 00:03:37,160
 into the forest.

39
00:03:37,160 --> 00:03:40,160
 And live under his guidance.

40
00:03:40,160 --> 00:03:43,100
 So, they thought, well this looks like this monk has been

41
00:03:43,100 --> 00:03:45,160
 around for a while, we'll take his guidance.

42
00:03:45,160 --> 00:03:48,200
 And so, they went off into the forest and they would

43
00:03:48,200 --> 00:03:51,160
 practice meditation walking and sitting throughout the day.

44
00:03:51,160 --> 00:03:55,080
 And then at night, in the first watch of the night when

45
00:03:55,080 --> 00:03:58,160
 they were not sure what to do for the night.

46
00:03:58,160 --> 00:04:05,160
 So, the night of course is 12 hours long from 6pm to 6am.

47
00:04:05,160 --> 00:04:13,880
 And at around 6pm, the elder monk came in and comes in and

48
00:04:13,880 --> 00:04:15,160
 says to them,

49
00:04:15,160 --> 00:04:20,120
 "Listen here, you've received instruction from the Buddha

50
00:04:20,120 --> 00:04:21,160
 himself.

51
00:04:21,160 --> 00:04:24,160
 It wouldn't do for you to be lazy."

52
00:04:24,160 --> 00:04:27,160
 He said, "Be vigilant."

53
00:04:27,160 --> 00:04:29,160
 And so, they thought to themselves, "That's right.

54
00:04:29,160 --> 00:04:32,300
 The Buddha said we should sleep only in the middle watch of

55
00:04:32,300 --> 00:04:33,160
 the night."

56
00:04:33,160 --> 00:04:37,720
 So, the first watch of the night from 6pm to 10pm, we'll

57
00:04:37,720 --> 00:04:40,160
 stay up and we'll do meditation.

58
00:04:40,160 --> 00:04:44,570
 The sun goes down, we'll light some candles and we'll keep

59
00:04:44,570 --> 00:04:47,160
 doing our walking and sitting.

60
00:04:47,160 --> 00:04:48,160
 And so, they did this.

61
00:04:48,160 --> 00:04:53,080
 The elder monk, after saying this to the monks, went into

62
00:04:53,080 --> 00:04:57,160
 his room and laid down and went to sleep.

63
00:04:57,160 --> 00:05:00,780
 And the other monks, in the middle of the watch of the

64
00:05:00,780 --> 00:05:02,160
 night at around 10pm,

65
00:05:02,160 --> 00:05:05,820
 they got together and said, "Okay, so that's enough of that

66
00:05:05,820 --> 00:05:06,160
.

67
00:05:06,160 --> 00:05:11,160
 Let's spend this time mindfully lying and go to sleep."

68
00:05:11,160 --> 00:05:16,520
 And they would go to sleep thinking about getting up in 4

69
00:05:16,520 --> 00:05:20,160
 hours or whenever they were going to get up.

70
00:05:20,160 --> 00:05:24,540
 And so, they all laid down and mindfully went to sleep

71
00:05:24,540 --> 00:05:31,160
 after a very hard day of meditation.

72
00:05:31,160 --> 00:05:34,160
 Somewhere in the middle of the watch of the night, sometime

73
00:05:34,160 --> 00:05:36,160
 not long after these monks had fallen asleep,

74
00:05:36,160 --> 00:05:41,980
 the elder monk gets up and goes out and sees them all

75
00:05:41,980 --> 00:05:43,160
 sleeping.

76
00:05:43,160 --> 00:05:45,160
 And the same happened in the last watch.

77
00:05:45,160 --> 00:05:51,170
 They went to go to sleep and the elder monk gets up and

78
00:05:51,170 --> 00:05:54,160
 drives them out again.

79
00:05:54,160 --> 00:06:04,160
 And each time, of course, after he tells them...

