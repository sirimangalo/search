1
00:00:00,000 --> 00:00:05,570
 How to deal with the fear of leaving samsara and joining

2
00:00:05,570 --> 00:00:07,000
 the sangha?

3
00:00:07,000 --> 00:00:20,000
 I don't know. Fear, fear, afraid, afraid.

4
00:00:20,000 --> 00:00:24,000
 I don't know.

5
00:00:27,000 --> 00:00:30,660
 Leaving samsara? Well, I don't think that you're guaranteed

6
00:00:30,660 --> 00:00:34,000
 to leave samsara if you become a monk.

7
00:00:34,000 --> 00:00:42,450
 I mean, that's probably your goal in becoming a monk, but

8
00:00:42,450 --> 00:00:45,000
 it's not guaranteed.

9
00:00:45,000 --> 00:00:54,940
 And I don't think that leaving samsara is what is making

10
00:00:54,940 --> 00:00:57,000
 you afraid.

11
00:00:57,000 --> 00:01:00,530
 Because I don't think anyone's like, "Oh no, I don't want

12
00:01:00,530 --> 00:01:04,000
 to be born a million more times suffering."

13
00:01:04,000 --> 00:01:07,890
 I think it's just you're afraid of leaving all your

14
00:01:07,890 --> 00:01:11,000
 attachments and the things that you like.

15
00:01:11,000 --> 00:01:15,000
 It's not that you're afraid of not being born again.

16
00:01:15,000 --> 00:01:20,630
 And for dealing with your attachments and such, it's all

17
00:01:20,630 --> 00:01:26,000
 just you creating the drama of your life.

18
00:01:26,000 --> 00:01:31,990
 You just paint this picture of how everything is so that

19
00:01:31,990 --> 00:01:35,000
 you can cling to it.

20
00:01:35,000 --> 00:01:41,000
 It gives you a reason to cling to it. Because you have to

21
00:01:41,000 --> 00:01:41,000
 make things important for yourself.

22
00:01:41,000 --> 00:01:44,650
 You have to make things about yourself important, about

23
00:01:44,650 --> 00:01:47,000
 others and how they relate to you.

24
00:01:47,000 --> 00:01:52,280
 That's all it is, is you creating a drama. If you just see

25
00:01:52,280 --> 00:01:57,630
 that there's nothing really solid about any of it, then I

26
00:01:57,630 --> 00:02:00,000
 don't think that's such a big deal.

27
00:02:00,000 --> 00:02:10,450
 Well, I tried it. I joined the Sangha and couldn't leave s

28
00:02:10,450 --> 00:02:15,000
amsara by that. So it doesn't really work like that.

29
00:02:15,000 --> 00:02:22,110
 There is probably always fear before you ordain and high

30
00:02:22,110 --> 00:02:24,000
 expectations.

31
00:02:24,000 --> 00:02:29,620
 And that is probably why the fear arises so much, so strong

32
00:02:29,620 --> 00:02:33,530
. Because there are these strong expectations that you leave

33
00:02:33,530 --> 00:02:37,000
 samsara and something is going to happen.

34
00:02:37,000 --> 00:02:42,580
 And what Nagasena mentioned about the attachments. Of

35
00:02:42,580 --> 00:02:48,620
 course there is a fear that you cannot have these things

36
00:02:48,620 --> 00:02:50,000
 anymore.

37
00:02:51,000 --> 00:02:57,000
 I almost didn't ordain because I like bathing in a bathtub.

38
00:02:57,000 --> 00:03:03,000
 I used to bath every evening, almost every day.

39
00:03:03,000 --> 00:03:08,480
 I thought I couldn't live without it. And that really

40
00:03:08,480 --> 00:03:14,000
 caused fear. Like how can I live like that then?

41
00:03:14,000 --> 00:03:20,350
 You have to try. You just do it and then you know it's

42
00:03:20,350 --> 00:03:24,000
 possible. You can do it, usually.

43
00:03:43,000 --> 00:03:49,190
 It's easy to want to become a monk. It's easy to want to

44
00:03:49,190 --> 00:03:54,090
 join the Sangha. And it's a lot more difficult to actually

45
00:03:54,090 --> 00:03:55,000
 do it.

46
00:03:55,000 --> 00:04:01,680
 And it's even more difficult to enjoy and to be at peace

47
00:04:01,680 --> 00:04:03,000
 with it.

48
00:04:04,000 --> 00:04:09,230
 This is from experience because I've tried to help many

49
00:04:09,230 --> 00:04:12,750
 people to become monks. And some people just aren't ready

50
00:04:12,750 --> 00:04:13,000
 for it.

51
00:04:13,000 --> 00:04:20,880
 Most people are in this state where they like the idea. Not

52
00:04:20,880 --> 00:04:28,160
 most people, but most people who come across this question

53
00:04:28,160 --> 00:04:30,000
 are at the state where they like the idea.

54
00:04:30,000 --> 00:04:34,420
 They understand theoretically how great it is. But

55
00:04:34,420 --> 00:04:41,000
 emotionally and mentally they're not really there.

56
00:04:42,000 --> 00:04:49,230
 Their attachments and their views and opinions and often

57
00:04:49,230 --> 00:04:56,490
 the relationships, their attachment or the clinging to

58
00:04:56,490 --> 00:05:04,130
 friends and family is so strong in them that even were they

59
00:05:04,130 --> 00:05:07,000
 to ordain it would be miserable and it would be short.

60
00:05:07,000 --> 00:05:13,000
 And they would quickly leave it.

61
00:05:13,000 --> 00:05:23,000
 So I think there is room to say...

62
00:05:24,000 --> 00:05:29,610
 Start doing good deeds now because it's those good deeds

63
00:05:29,610 --> 00:05:35,000
 that are going to be a support for you to join the Sangha

64
00:05:35,000 --> 00:05:39,580
 and a support for you to give up that fear which is totally

65
00:05:39,580 --> 00:05:47,000
 based on attachment and based on

66
00:05:48,000 --> 00:05:54,000
 the aversion or the inability or expectations.

67
00:05:54,000 --> 00:06:02,870
 So being charitable in terms of giving up, giving up your

68
00:06:02,870 --> 00:06:07,000
 desires and your wants.

69
00:06:08,000 --> 00:06:13,610
 Being moral in terms of abstaining from many different

70
00:06:13,610 --> 00:06:18,900
 things that are unwholesome. This is a very important step

71
00:06:18,900 --> 00:06:22,320
 because it's going to prepare you for a greater amount of

72
00:06:22,320 --> 00:06:28,120
 giving up and the greater amount of abstention which is the

73
00:06:28,120 --> 00:06:29,000
 monkhood.

74
00:06:30,000 --> 00:06:34,980
 I think you can't emphasize enough the importance of

75
00:06:34,980 --> 00:06:41,140
 preparing for monasticism. It's not just a leap into

76
00:06:41,140 --> 00:06:52,000
 freedom. It's the application for a course of training.

77
00:06:52,000 --> 00:06:57,000
 It's the beginning. When you ordain you begin to train.

78
00:06:58,000 --> 00:07:02,180
 So I think there is room for that. But on the other hand,

79
00:07:02,180 --> 00:07:06,750
 it turned out in relation to what was already said, it's

80
00:07:06,750 --> 00:07:12,500
 not really that bad in the end and many of our fears turn

81
00:07:12,500 --> 00:07:15,000
 out to be irrational.

82
00:07:16,000 --> 00:07:23,210
 So for instance, I almost cried when I was a young monk and

83
00:07:23,210 --> 00:07:29,790
 I didn't have any soap. And I felt so sad for myself that I

84
00:07:29,790 --> 00:07:31,000
 had no soap.

85
00:07:31,000 --> 00:07:35,350
 And then it just hit me like an epiphany. "Well, if you don

86
00:07:35,350 --> 00:07:39,000
't have any soap, then bathe without soap."

87
00:07:40,000 --> 00:07:47,830
 It's funny how caught up we get. And it was that experience

88
00:07:47,830 --> 00:07:52,150
, really that simple, silly experience that set the tone for

89
00:07:52,150 --> 00:07:54,000
 a lot of different things.

90
00:07:54,000 --> 00:07:56,820
 Because then I didn't need laundry powder. If I didn't have

91
00:07:56,820 --> 00:07:59,550
 laundry powder, I would wash my clothes without laundry

92
00:07:59,550 --> 00:08:00,000
 powder.

93
00:08:01,000 --> 00:08:05,170
 Which is also, these are just silly things, but the

94
00:08:05,170 --> 00:08:08,900
 philosophical ramifications where you don't have a place to

95
00:08:08,900 --> 00:08:12,000
 stay, well go and sleep on a park bench.

96
00:08:12,000 --> 00:08:16,800
 And I've done that before and that is totally, wonderfully

97
00:08:16,800 --> 00:08:18,000
 liberating.

98
00:08:18,000 --> 00:08:24,520
 I don't have enough food to eat. Well, then today I won't

99
00:08:24,520 --> 00:08:25,000
 eat.

100
00:08:26,000 --> 00:08:32,540
 And so on and so on. Eventually you realize that you can do

101
00:08:32,540 --> 00:08:34,000
 without anything.

102
00:08:34,000 --> 00:08:42,540
 And the worst that's going to happen is that you die, which

103
00:08:42,540 --> 00:08:46,000
 of course could happen to you anyways.

104
00:08:49,000 --> 00:08:53,920
 And death isn't really that big of a deal. I know we often

105
00:08:53,920 --> 00:08:58,600
 get this idea that we should cherish this life that we have

106
00:08:58,600 --> 00:09:00,000
 as a human being.

107
00:09:00,000 --> 00:09:03,490
 And so we think we should work to protect it. But it's

108
00:09:03,490 --> 00:09:05,000
 actually not so clear cut.

109
00:09:05,000 --> 00:09:10,260
 Because if you're doing, if you're practicing renunciation

110
00:09:10,260 --> 00:09:15,120
 and your mind is becoming clear, then when you pass away,

111
00:09:15,120 --> 00:09:17,000
 you'll only go to a better state.

112
00:09:18,000 --> 00:09:23,690
 That is more, more conducive for the practice of meditation

113
00:09:23,690 --> 00:09:24,000
.

114
00:09:24,000 --> 00:09:27,170
 You might be born as an angel or you might be born in a

115
00:09:27,170 --> 00:09:32,080
 society where you can quickly become a monk and continue on

116
00:09:32,080 --> 00:09:34,000
 with the practice.

117
00:09:34,000 --> 00:09:40,490
 But even putting aside death, so many other things that we

118
00:09:40,490 --> 00:09:42,000
 shouldn't be afraid of.

119
00:09:43,000 --> 00:09:46,170
 People are afraid that if they don't like it, then they'll

120
00:09:46,170 --> 00:09:49,000
 wind up 40 years old without any life skills.

121
00:09:49,000 --> 00:09:57,000
 They'll want to disrobe and not be able to make it in life.

122
00:09:57,000 --> 00:09:59,760
 But if you talk to those people who have done that, who

123
00:09:59,760 --> 00:10:02,470
 have gotten to the point where they couldn't be a monk

124
00:10:02,470 --> 00:10:05,000
 anymore and ended up disrobing,

125
00:10:06,000 --> 00:10:10,010
 they actually don't regret it at all. They regret the fact

126
00:10:10,010 --> 00:10:14,000
 that they had to disrobe or regret the decision to disrobe.

127
00:10:14,000 --> 00:10:21,290
 But they don't find that themselves helpless at all. They

128
00:10:21,290 --> 00:10:24,030
 realize how much suffering there is in the world as

129
00:10:24,030 --> 00:10:25,000
 compared to being a monk,

130
00:10:25,000 --> 00:10:29,070
 but they're very much equipped to deal with the suffering.

131
00:10:29,070 --> 00:10:32,390
 They might not get high paying jobs, but they're not

132
00:10:32,390 --> 00:10:33,000
 concerned about it.

133
00:10:34,000 --> 00:10:38,060
 Some people in Thailand who used to be monks, now they

134
00:10:38,060 --> 00:10:42,300
 collect garbage recyclables and sell recyclables and make

135
00:10:42,300 --> 00:10:43,000
 money like that.

136
00:10:43,000 --> 00:10:47,000
 So make very little money, but are totally content and they

137
00:10:47,000 --> 00:10:53,000
 live in shacks and just total poverty.

138
00:10:53,000 --> 00:10:57,800
 But as a monk, it was even more poverty and you were

139
00:10:57,800 --> 00:10:59,000
 happier.

140
00:11:00,000 --> 00:11:02,710
 So as a monk, you only ate in the morning. Wow, now you can

141
00:11:02,710 --> 00:11:04,000
 eat whenever you want.

142
00:11:04,000 --> 00:11:09,000
 So you're able to deal with it much better.

143
00:11:09,000 --> 00:11:12,800
 This is because ordination is a training. You should see it

144
00:11:12,800 --> 00:11:14,000
 as a training course.

145
00:11:14,000 --> 00:11:17,960
 It's not a decision taking a leap into this or into that.

146
00:11:17,960 --> 00:11:22,000
 It's applying to...like you apply for a PhD course.

147
00:11:23,000 --> 00:11:27,090
 You don't think, "Oh, what if I join that PhD program?

148
00:11:27,090 --> 00:11:29,000
 Maybe I'll regret it."

149
00:11:29,000 --> 00:11:32,400
 All you get from a PhD program is more knowledge, more

150
00:11:32,400 --> 00:11:38,000
 skills. And that's really all you get through the ord

151
00:11:38,000 --> 00:11:40,000
ination. It's a training.

152
00:11:40,000 --> 00:11:46,000
 And eventually you become a trainer, you become a teacher.

153
00:11:49,000 --> 00:11:52,010
 The really great thing about being a monk, especially in a

154
00:11:52,010 --> 00:11:55,630
 Buddhist society, is that old monks are trained much better

155
00:11:55,630 --> 00:11:57,000
 than young monks.

156
00:11:57,000 --> 00:12:01,380
 So the older you get, you just have to get old and people

157
00:12:01,380 --> 00:12:04,520
 start to respect you and think, "Oh, he must be a venerable

158
00:12:04,520 --> 00:12:05,000
 monk."

159
00:12:05,000 --> 00:12:08,480
 And they take care of you and they start to think of you as

160
00:12:08,480 --> 00:12:10,000
 their monk and so on.

161
00:12:13,000 --> 00:12:17,000
 So the retirement plan is not so bad from what I've seen.

162
00:12:17,000 --> 00:12:20,000
 Obviously the medical plan is great.

163
00:12:20,000 --> 00:12:26,000
 The monk medicine is pretty easy to obtain.

164
00:12:26,000 --> 00:12:32,000
 This fear is actually very impermanent.

165
00:12:34,000 --> 00:12:40,000
 I noticed that for myself. I had fear before I ordained.

166
00:12:40,000 --> 00:12:47,430
 And now when I think of disrobing or being caused to dis

167
00:12:47,430 --> 00:12:48,000
robe,

168
00:12:48,000 --> 00:12:55,000
 I have the same fear arising of being a lay person again.

169
00:12:57,000 --> 00:13:04,140
 So it is really, you have that fear because it's something

170
00:13:04,140 --> 00:13:07,000
 that you don't know.

171
00:13:07,000 --> 00:13:11,000
 You're making a step into something unexpected.

172
00:13:11,000 --> 00:13:18,530
 And when you get to know what you're doing when you did

173
00:13:18,530 --> 00:13:24,000
 that step and ordained, then you will lose that fear, of

174
00:13:24,000 --> 00:13:24,000
 course.

175
00:13:25,000 --> 00:13:29,700
 It's only there before. So once you are ordained, it cannot

176
00:13:29,700 --> 00:13:33,000
 be there anymore because you've done it.

177
00:13:33,000 --> 00:13:37,000
 And then it might even change and turn around.

178
00:13:37,000 --> 00:13:45,100
 So the point is not the fear to deal with the fear before

179
00:13:45,100 --> 00:13:51,000
 ordination or before leaving samsara.

180
00:13:52,000 --> 00:13:56,000
 The fear in general we have to deal with.

181
00:13:57,000 --> 00:14:01,000
 The fear of being

