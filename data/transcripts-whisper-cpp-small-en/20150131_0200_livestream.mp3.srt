1
00:00:00,000 --> 00:00:06,000
 Okay, good morning everyone.

2
00:00:06,000 --> 00:00:10,590
 If I thought something a little bit different today, we

3
00:00:10,590 --> 00:00:13,440
 could have a little bit of a break

4
00:00:13,440 --> 00:00:16,440
 from the usual.

5
00:00:16,440 --> 00:00:28,720
 I'll get into something called the Arakakamatana.

6
00:00:28,720 --> 00:00:37,260
 These are four meditation practices that protect the medit

7
00:00:37,260 --> 00:00:40,240
ator, Arakakam.

8
00:00:40,240 --> 00:00:50,020
 So the meaning is they can be used in conjunction with our

9
00:00:50,020 --> 00:00:52,640
 main meditation practice.

10
00:00:52,640 --> 00:00:56,200
 The four meditations that we use on a regular basis,

11
00:00:56,200 --> 00:00:58,680
 regardless of what other meditations

12
00:00:58,680 --> 00:01:01,680
 we're practicing.

13
00:01:01,680 --> 00:01:09,520
 So it should be fairly brief, but I just thought I'd bring

14
00:01:09,520 --> 00:01:13,160
 these up to remind the meditators

15
00:01:13,160 --> 00:01:15,480
 of them.

16
00:01:15,480 --> 00:01:21,930
 That is not to get hung up on these or caught up on these,

17
00:01:21,930 --> 00:01:24,720
 caught up with them in the sense

18
00:01:24,720 --> 00:01:32,640
 of letting them take over your practice.

19
00:01:32,640 --> 00:01:41,920
 They're generally useful for specific conditions.

20
00:01:41,920 --> 00:01:54,760
 So they can be used to adjust your practice in different

21
00:01:54,760 --> 00:01:56,240
 ways.

22
00:01:56,240 --> 00:01:58,700
 That you shouldn't let them take over your practice, so

23
00:01:58,700 --> 00:02:00,920
 they shouldn't become a crutch.

24
00:02:00,920 --> 00:02:05,770
 They should be a nudge in the right direction, allowing you

25
00:02:05,770 --> 00:02:08,720
 to get back to your main meditation

26
00:02:08,720 --> 00:02:15,240
 practice when something seems overwhelming or there's an

27
00:02:15,240 --> 00:02:18,640
 obstacle to your practice.

28
00:02:18,640 --> 00:02:25,840
 So the four are Buddha, mindfulness of the Buddha, Metta,

29
00:02:25,840 --> 00:02:29,200
 practice of loving kindness

30
00:02:29,200 --> 00:02:43,200
 or friendliness, Asumbha, mindfulness of the unbeautiful or

31
00:02:43,200 --> 00:02:49,680
 lonesome nature of the body,

32
00:02:49,680 --> 00:03:01,520
 or the mindfulness of death.

33
00:03:01,520 --> 00:03:11,600
 These four are called the Arakaka Kamatana.

34
00:03:11,600 --> 00:03:20,290
 So each one has its own benefits and I suppose warnings as

35
00:03:20,290 --> 00:03:21,640
 well.

36
00:03:21,640 --> 00:03:26,100
 Buddha Nusati, mindfulness of the Buddha is an all around

37
00:03:26,100 --> 00:03:28,480
 useful meditation designed to

38
00:03:28,480 --> 00:03:34,050
 cultivate confidence, faith if you will, and more

39
00:03:34,050 --> 00:03:35,760
 conviction.

40
00:03:35,760 --> 00:03:42,930
 So when you feel, it's good for when you feel kind of

41
00:03:42,930 --> 00:03:47,280
 unsure of yourself, when you feel

42
00:03:47,280 --> 00:03:56,190
 down and lacking conviction when you're wavering in your

43
00:03:56,190 --> 00:03:58,320
 practice.

44
00:03:58,320 --> 00:03:59,320
 What of course happens?

45
00:03:59,320 --> 00:04:00,320
 It happens often.

46
00:04:00,320 --> 00:04:07,190
 It can happen often, so you have to be, you need tools

47
00:04:07,190 --> 00:04:08,400
 sometimes.

48
00:04:08,400 --> 00:04:11,040
 Sometimes it's not enough to just be mindful because you're

49
00:04:11,040 --> 00:04:11,920
 not sure if you want to be

50
00:04:11,920 --> 00:04:12,920
 mindful anymore.

51
00:04:12,920 --> 00:04:17,630
 You don't have the power to push yourself to be mindful

52
00:04:17,630 --> 00:04:18,760
 anymore.

53
00:04:18,760 --> 00:04:24,320
 Of course it's enough to just practice mindfulness.

54
00:04:24,320 --> 00:04:29,720
 This is of course our main practice, but these things give

55
00:04:29,720 --> 00:04:31,360
 you a nudge.

56
00:04:31,360 --> 00:04:35,690
 Remember when I was, when I did my first course so many

57
00:04:35,690 --> 00:04:38,480
 years ago, I was having a hard day

58
00:04:38,480 --> 00:04:42,630
 and I went to sleep and thought, "Well, it'll be better in

59
00:04:42,630 --> 00:04:43,920
 the morning."

60
00:04:43,920 --> 00:04:48,750
 And when I woke up, I woke up around 3 a.m. and it was the

61
00:04:48,750 --> 00:04:52,000
 same as, I tried to practice

62
00:04:52,000 --> 00:04:54,990
 meditation and it was the same, I just couldn't bring

63
00:04:54,990 --> 00:04:56,080
 myself to do it.

64
00:04:56,080 --> 00:05:02,240
 I remember why I was fed up and sort of at a loss as how to

65
00:05:02,240 --> 00:05:06,040
 continue and I remember walking

66
00:05:06,040 --> 00:05:09,470
 outside and sort of stumbling around in the dark trying to

67
00:05:09,470 --> 00:05:11,120
 figure out what I was going

68
00:05:11,120 --> 00:05:12,120
 to do.

69
00:05:12,120 --> 00:05:18,110
 And then I saw there was this bamboo hall with a big golden

70
00:05:18,110 --> 00:05:21,000
 Buddha image and they'd left

71
00:05:21,000 --> 00:05:25,160
 the light on all night in the hall.

72
00:05:25,160 --> 00:05:28,570
 Just the image of this big golden Buddha kind of drew me in

73
00:05:28,570 --> 00:05:28,880
.

74
00:05:28,880 --> 00:05:32,900
 And I remember bowing down in front of the Buddha and just

75
00:05:32,900 --> 00:05:35,600
 having this sort of a psychological

76
00:05:35,600 --> 00:05:40,920
 refuge was, it really changed the feeling of the course.

77
00:05:40,920 --> 00:05:45,030
 And from that moment something shifted and I was much more

78
00:05:45,030 --> 00:05:47,560
 confident going into my reporting

79
00:05:47,560 --> 00:05:54,870
 session and really felt like I had found something that I

80
00:05:54,870 --> 00:05:57,280
 could cling to.

81
00:05:57,280 --> 00:05:59,280
 It's a crutch really.

82
00:05:59,280 --> 00:06:03,820
 It's not a solution to your problems but just the reminder

83
00:06:03,820 --> 00:06:06,480
 that we have someone so powerful

84
00:06:06,480 --> 00:06:15,770
 backing us, someone so authoritative and so wise and pure

85
00:06:15,770 --> 00:06:17,960
 and strong.

86
00:06:17,960 --> 00:06:20,760
 It's a support for a beginner meditator and advanced medit

87
00:06:20,760 --> 00:06:21,680
ators as well.

88
00:06:21,680 --> 00:06:28,910
 But I think for a beginner especially, there is a real

89
00:06:28,910 --> 00:06:33,000
 power that it's even beyond just

90
00:06:33,000 --> 00:06:38,120
 the simple idea of someone, of the person.

91
00:06:38,120 --> 00:06:42,720
 There actually is a power to the idea of a Buddha.

92
00:06:42,720 --> 00:06:46,760
 It resonates deeply with the mind.

93
00:06:46,760 --> 00:06:53,620
 So practicing as a meditation can be a good way to build up

94
00:06:53,620 --> 00:06:56,200
 your confidence.

95
00:06:56,200 --> 00:06:59,940
 So people will practice being mindful of the qualities of

96
00:06:59,940 --> 00:07:00,960
 the Buddha.

97
00:07:00,960 --> 00:07:13,760
 Or hung that he was worthy of respect.

98
00:07:13,760 --> 00:07:20,500
 Samma Sambuddho that he became fully enlightened by himself

99
00:07:20,500 --> 00:07:20,960
.

100
00:07:20,960 --> 00:07:33,450
 Vinja Charanasampa knows that he knows the truth and

101
00:07:33,450 --> 00:07:38,360
 practices it as well.

102
00:07:38,360 --> 00:07:43,760
 Sugattho that he's gone in a good way or found the right

103
00:07:43,760 --> 00:07:44,720
 path.

104
00:07:44,720 --> 00:07:49,040
 Lokavidho that he knows everything about the world.

105
00:07:49,040 --> 00:07:54,840
 He understands the human world and the central realms.

106
00:07:54,840 --> 00:07:59,400
 He understands heaven and the gods.

107
00:07:59,400 --> 00:08:02,440
 He understands hell.

108
00:08:02,440 --> 00:08:06,360
 All the realms he understands.

109
00:08:06,360 --> 00:08:17,120
 Anuttaro Puri Sadamasarati, the unsurpassed trainer.

110
00:08:17,120 --> 00:08:19,600
 He's able to teach people.

111
00:08:19,600 --> 00:08:25,300
 So reflecting on all these qualities and more just

112
00:08:25,300 --> 00:08:29,560
 reflecting on the idea of the Buddhas

113
00:08:29,560 --> 00:08:31,080
 gives us confidence in what we're doing.

114
00:08:31,080 --> 00:08:36,090
 That this isn't something that I made up or something that

115
00:08:36,090 --> 00:08:38,280
 came out of psychology or

116
00:08:38,280 --> 00:08:40,280
 something like that.

117
00:08:40,280 --> 00:08:54,920
 This comes from one of the greatest minds in history.

118
00:08:54,920 --> 00:08:58,060
 So just being mindful of the Buddha, we repeat to ourselves

119
00:08:58,060 --> 00:09:00,240
, "Buddho, Buddho" sometimes.

120
00:09:00,240 --> 00:09:03,150
 My teacher said, "Remind yourself that the Buddhas are your

121
00:09:03,150 --> 00:09:03,760
 refuge.

122
00:09:03,760 --> 00:09:10,640
 Buddho Meenatoh." He even told me to tell foreigners this.

123
00:09:10,640 --> 00:09:15,180
 He said, "Tell everyone it's a good mantra to use when you

124
00:09:15,180 --> 00:09:16,760
're traveling."

125
00:09:16,760 --> 00:09:21,250
 I remember he was giving this talk and he said, "You

126
00:09:21,250 --> 00:09:22,760
 understand?"

127
00:09:22,760 --> 00:09:27,990
 He says, "Back when I was just learning Thai, tell all the

128
00:09:27,990 --> 00:09:30,200
 foreigners as well."

129
00:09:30,200 --> 00:09:33,830
 "Buddho Meenatoh," you can remember, "The Buddha is my

130
00:09:33,830 --> 00:09:34,680
 refuge."

131
00:09:34,680 --> 00:09:42,770
 It's a good mantra when troubles come, gives you confidence

132
00:09:42,770 --> 00:09:45,000
 in yourself.

133
00:09:45,000 --> 00:09:49,860
 Centers you back because you can be pulled out of your

134
00:09:49,860 --> 00:09:52,640
 element, out of mindfulness by

135
00:09:52,640 --> 00:09:56,840
 situations, by circumstances.

136
00:09:56,840 --> 00:10:02,500
 Being the Buddha brings you back to the universe of

137
00:10:02,500 --> 00:10:05,680
 meditation, Buddhism.

138
00:10:05,680 --> 00:10:12,040
 The second one is "Mita."

139
00:10:12,040 --> 00:10:13,680
 Mita comes from the word "mita."

140
00:10:13,680 --> 00:10:21,300
 "Mita" is the original word and then the state of being a "

141
00:10:21,300 --> 00:10:24,800
mita" is called "mita."

142
00:10:24,800 --> 00:10:27,680
 It becomes "e."

143
00:10:27,680 --> 00:10:30,610
 The word "mita" actually comes from "mita" which means "

144
00:10:30,610 --> 00:10:31,320
friend."

145
00:10:31,320 --> 00:10:35,320
 It actually does mean "friendliness," the state of being

146
00:10:35,320 --> 00:10:37,080
 friendly or a friend.

147
00:10:37,080 --> 00:10:40,210
 If you want to understand what we mean by "mita," this is

148
00:10:40,210 --> 00:10:41,760
 where the word comes from.

149
00:10:41,760 --> 00:10:47,560
 It comes directly from "mita" which means "friend."

150
00:10:47,560 --> 00:10:55,150
 Being friendly to others, the state of love or kindness or

151
00:10:55,150 --> 00:10:58,840
 desire for others to be happy.

152
00:10:58,840 --> 00:11:00,400
 It pairs with compassion.

153
00:11:00,400 --> 00:11:04,450
 They actually are kind of two sides of the same coin or two

154
00:11:04,450 --> 00:11:06,120
 halves of the whole.

155
00:11:06,120 --> 00:11:10,380
 "Mita" is the desire for others to be happy, so wishing

156
00:11:10,380 --> 00:11:12,560
 good things for others.

157
00:11:12,560 --> 00:11:18,780
 "Karuna" compassion is wishing for suffering to leave when

158
00:11:18,780 --> 00:11:21,800
 beings are in suffering, wishing

159
00:11:21,800 --> 00:11:24,800
 for them to be free from suffering.

160
00:11:24,800 --> 00:11:30,360
 They are really very close and very similar.

161
00:11:30,360 --> 00:11:31,840
 They focus on different aspects.

162
00:11:31,840 --> 00:11:34,840
 "Mita" focuses on happiness.

163
00:11:34,840 --> 00:11:41,560
 "Karuna" focuses on suffering, or alleviating suffering.

164
00:11:41,560 --> 00:11:46,560
 But this one is good for anger.

165
00:11:46,560 --> 00:11:50,280
 "Mita" is something that you should specifically focus on

166
00:11:50,280 --> 00:11:52,480
 when you have anger towards other

167
00:11:52,480 --> 00:11:57,670
 beings or even towards yourself, just frustration in

168
00:11:57,670 --> 00:11:58,880
 general.

169
00:11:58,880 --> 00:12:01,410
 Sending love to yourself is sometimes useful to remind you

170
00:12:01,410 --> 00:12:02,680
 that you deserve to be happy

171
00:12:02,680 --> 00:12:06,640
 and that happiness for yourself would be a good thing.

172
00:12:06,640 --> 00:12:07,640
 You don't have to be.

173
00:12:07,640 --> 00:12:08,640
 You don't have to suffer.

174
00:12:08,640 --> 00:12:11,640
 You don't have to feel guilty.

175
00:12:11,640 --> 00:12:13,640
 You don't have to worry about your past.

176
00:12:13,640 --> 00:12:18,480
 That you can and should strive to be happy.

177
00:12:18,480 --> 00:12:22,920
 But it's most useful for when you have problems with others

178
00:12:22,920 --> 00:12:25,640
, when you have anger or frustration,

179
00:12:25,640 --> 00:12:31,750
 when you're angry at me for giving you this hard work to do

180
00:12:31,750 --> 00:12:35,160
, when you have anger problems

181
00:12:35,160 --> 00:12:36,160
 come up.

182
00:12:36,160 --> 00:12:41,760
 It's just something that we often remind meditators, time

183
00:12:41,760 --> 00:12:43,920
 meditators will often practice after

184
00:12:43,920 --> 00:12:44,920
 they meditate.

185
00:12:44,920 --> 00:12:49,490
 It's a good way of sharing the aspirations that you have

186
00:12:49,490 --> 00:12:52,520
 for goodness, sharing them with

187
00:12:52,520 --> 00:12:53,520
 others.

188
00:12:53,520 --> 00:12:56,530
 So after you finish meditating and your mind is clear, you

189
00:12:56,530 --> 00:12:58,120
 can take some time before you

190
00:12:58,120 --> 00:13:02,910
 stand up to send good thoughts to others, first to yourself

191
00:13:02,910 --> 00:13:05,120
 and then to your family,

192
00:13:05,120 --> 00:13:07,520
 to your friends, to your enemies.

193
00:13:07,520 --> 00:13:10,630
 Or you can go in physical space, starting with yourself and

194
00:13:10,630 --> 00:13:12,000
 then the room that you're

195
00:13:12,000 --> 00:13:16,460
 in and then the building that you're in and then the area

196
00:13:16,460 --> 00:13:18,600
 that you're in and then the

197
00:13:18,600 --> 00:13:22,760
 city and the country and then the world.

198
00:13:22,760 --> 00:13:25,000
 There's different ways of sending it.

199
00:13:25,000 --> 00:13:28,120
 And Buddha mentioned it several different ways.

200
00:13:28,120 --> 00:13:32,060
 You can do it one direction at a time, so to the north, to

201
00:13:32,060 --> 00:13:34,000
 the east, to the west, to

202
00:13:34,000 --> 00:13:39,600
 the south, up and down.

203
00:13:39,600 --> 00:13:43,770
 You can send the specific types of beings, humans and

204
00:13:43,770 --> 00:13:46,280
 animals and angels one by one by

205
00:13:46,280 --> 00:13:47,280
 one.

206
00:13:47,280 --> 00:13:53,000
 Many different ways of practicing it.

207
00:13:53,000 --> 00:13:56,430
 Again we're not trying to make this our primary practice,

208
00:13:56,430 --> 00:13:59,560
 so it's just often good to take

209
00:13:59,560 --> 00:14:04,790
 it up after you're finished sitting or when anger or

210
00:14:04,790 --> 00:14:06,760
 frustration arises.

211
00:14:06,760 --> 00:14:11,350
 When you feel angry at someone, you can first send love to

212
00:14:11,350 --> 00:14:13,840
 yourself and then love to your

213
00:14:13,840 --> 00:14:17,370
 family and then send to someone you love first because it

214
00:14:17,370 --> 00:14:19,480
 helps you prepare for it and then

215
00:14:19,480 --> 00:14:24,370
 send, try to be kind to the person you're frustrated

216
00:14:24,370 --> 00:14:25,560
 towards.

217
00:14:25,560 --> 00:14:29,360
 Of course you should also be mindful saying angry, angry,

218
00:14:29,360 --> 00:14:32,560
 but when it becomes overwhelming,

219
00:14:32,560 --> 00:14:36,320
 Maita is useful.

220
00:14:36,320 --> 00:14:39,250
 So it's good also to practice while you're here, do a

221
00:14:39,250 --> 00:14:40,880
 little bit of it at the end of

222
00:14:40,880 --> 00:14:44,200
 your sessions.

223
00:14:44,200 --> 00:14:49,510
 It's a protective, it protects you from getting upset,

224
00:14:49,510 --> 00:14:54,440
 getting overwhelmed and lashing out

225
00:14:54,440 --> 00:14:56,960
 because your emotions can be very strong.

226
00:14:56,960 --> 00:14:59,400
 We meditate, we stop repressing our emotions and we stop

227
00:14:59,400 --> 00:15:00,680
 trying to control them so they

228
00:15:00,680 --> 00:15:06,900
 can come out quite strong until we're able to uproot them

229
00:15:06,900 --> 00:15:09,640
 and do away with them.

230
00:15:09,640 --> 00:15:13,810
 So you have to have something to deal with them when they

231
00:15:13,810 --> 00:15:14,880
 do come up.

232
00:15:14,880 --> 00:15:18,840
 Maita is useful for that.

233
00:15:18,840 --> 00:15:24,560
 On the opposite side of the spectrum there's asubha.

234
00:15:24,560 --> 00:15:25,560
 Asubha means beautiful.

235
00:15:25,560 --> 00:15:29,920
 Asubha means not beautiful.

236
00:15:29,920 --> 00:15:35,230
 So focusing on the not beautiful aspects of the body, the

237
00:15:35,230 --> 00:15:37,840
 ugly aspects of the body.

238
00:15:37,840 --> 00:15:42,250
 It's useful to help us overcome the opposite of anger which

239
00:15:42,250 --> 00:15:44,320
 would be lust or desire.

240
00:15:44,320 --> 00:15:49,820
 So when this comes up, this is one of the first things a

241
00:15:49,820 --> 00:15:51,840
 new monk learns.

242
00:15:51,840 --> 00:15:56,460
 It's part of becoming even a novice monk, is to learn asub

243
00:15:56,460 --> 00:15:58,600
ha because we have to deal

244
00:15:58,600 --> 00:16:02,760
 with the desires that we can't act out upon.

245
00:16:02,760 --> 00:16:06,720
 So be mindful of the...

246
00:16:06,720 --> 00:16:11,680
 Asubha is useful, temporarily useful.

247
00:16:11,680 --> 00:16:15,800
 Of course it doesn't uproot the problem but it can be

248
00:16:15,800 --> 00:16:16,840
 helpful.

249
00:16:16,840 --> 00:16:17,840
 So they have the...

250
00:16:17,840 --> 00:16:20,020
 The way you do it is you focus on parts of the body

251
00:16:20,020 --> 00:16:21,560
 starting with the hair on the head

252
00:16:21,560 --> 00:16:24,200
 and then the hair on the body.

253
00:16:24,200 --> 00:16:29,840
 There's a kei size, there's loma, nakka is your nails, d

254
00:16:29,840 --> 00:16:32,880
anta is the teeth, dajjho is

255
00:16:32,880 --> 00:16:33,880
 the skin.

256
00:16:33,880 --> 00:16:37,450
 These are normally things that we think of as kind of

257
00:16:37,450 --> 00:16:38,360
 beautiful.

258
00:16:38,360 --> 00:16:42,760
 We think of hair as beautiful, even body hair can be

259
00:16:42,760 --> 00:16:46,400
 attractive, nails, we paint our nails

260
00:16:46,400 --> 00:16:51,360
 or we file them, danta, teeth, and we think of them as

261
00:16:51,360 --> 00:16:55,080
 white and try to make them beautiful.

262
00:16:55,080 --> 00:17:03,020
 And our skin, we try to make it soft and pleasing and our

263
00:17:03,020 --> 00:17:06,600
 face and everything.

264
00:17:06,600 --> 00:17:08,120
 And so we begin to look at...

265
00:17:08,120 --> 00:17:10,480
 We try practicing to look at these things and see the truth

266
00:17:10,480 --> 00:17:11,720
 about them because the truth

267
00:17:11,720 --> 00:17:14,950
 about starting with the hair on our head is that it's all

268
00:17:14,950 --> 00:17:16,480
 kind of greasy and it really

269
00:17:16,480 --> 00:17:18,680
 does start to smell if you leave it alone.

270
00:17:18,680 --> 00:17:21,810
 The truth of it that we tried to hide is that it's not

271
00:17:21,810 --> 00:17:24,160
 beautiful, there's nothing beautiful

272
00:17:24,160 --> 00:17:25,160
 about it.

273
00:17:25,160 --> 00:17:30,440
 And they say, there's this one monk, he's giving a talk

274
00:17:30,440 --> 00:17:33,600
 about this and he says...

275
00:17:33,600 --> 00:17:36,880
 When you look at someone's hair and you think of it as

276
00:17:36,880 --> 00:17:39,000
 beautiful, so suppose you were sitting

277
00:17:39,000 --> 00:17:42,070
 down for dinner with someone you were attracted to and you

278
00:17:42,070 --> 00:17:44,160
 see their beautiful hair and compliment

279
00:17:44,160 --> 00:17:49,330
 them on their hair, but then one of their hair falls into

280
00:17:49,330 --> 00:17:50,560
 your food.

281
00:17:50,560 --> 00:17:51,960
 How do you feel about that?

282
00:17:51,960 --> 00:17:54,280
 When you look at the hair in your food, does it look

283
00:17:54,280 --> 00:17:55,040
 beautiful?

284
00:17:55,040 --> 00:17:57,930
 Do you think, oh, that's wonderful that this hair fell in

285
00:17:57,930 --> 00:17:58,640
 your food?

286
00:17:58,640 --> 00:18:02,280
 You eat it, you eat it along with the food.

287
00:18:02,280 --> 00:18:05,200
 You quite quickly become disgusting.

288
00:18:05,200 --> 00:18:08,570
 If you've ever been to a barber when all the hair is on the

289
00:18:08,570 --> 00:18:10,520
 floor, there's no one that's

290
00:18:10,520 --> 00:18:13,040
 really attracted to hair.

291
00:18:13,040 --> 00:18:16,320
 It's the illusion of the image.

292
00:18:16,320 --> 00:18:19,080
 And this is what we're trying to do away with through our

293
00:18:19,080 --> 00:18:19,760
 practice.

294
00:18:19,760 --> 00:18:22,840
 When you practice asumba, it helps you do away with the

295
00:18:22,840 --> 00:18:24,720
 solution, to see hair for what

296
00:18:24,720 --> 00:18:26,880
 it really is.

297
00:18:26,880 --> 00:18:31,250
 Body hair as well, if you see the image like a nice goatee

298
00:18:31,250 --> 00:18:33,400
 or mustache or so on, can be

299
00:18:33,400 --> 00:18:34,720
 quite attractive.

300
00:18:34,720 --> 00:18:38,840
 Eyebrows or eyelashes can be quite attractive.

301
00:18:38,840 --> 00:18:43,790
 But again, if they got in your food, it wouldn't be so

302
00:18:43,790 --> 00:18:45,040
 pleasing.

303
00:18:45,040 --> 00:18:47,360
 Nor are they in and of themselves attractive.

304
00:18:47,360 --> 00:18:50,320
 Body hair is where all the bacteria grow.

305
00:18:50,320 --> 00:18:53,900
 They say if you shave your armpits, you don't smell as bad

306
00:18:53,900 --> 00:18:55,480
 because the bacteria grow in

307
00:18:55,480 --> 00:18:57,080
 your hair.

308
00:18:57,080 --> 00:19:01,880
 That's why we have body odor.

309
00:19:01,880 --> 00:19:05,840
 Body hair is smelly and greasy.

310
00:19:05,840 --> 00:19:09,050
 The Visuddhi Maga is a good text if you want to learn about

311
00:19:09,050 --> 00:19:10,800
 this, to help you do away with

312
00:19:10,800 --> 00:19:12,840
 the illusion of beauty.

313
00:19:12,840 --> 00:19:15,120
 It goes into great detail.

314
00:19:15,120 --> 00:19:21,120
 Hair is like wheat or rice and it's planted in the head.

315
00:19:21,120 --> 00:19:27,080
 Hair is planted in blood and grease and skin flakes.

316
00:19:27,080 --> 00:19:32,430
 It gets into quite some detail about all the parts of the

317
00:19:32,430 --> 00:19:34,720
 body inside and out.

318
00:19:34,720 --> 00:19:41,370
 Talking about what the stomach is like, what the bones are

319
00:19:41,370 --> 00:19:42,400
 like.

320
00:19:42,400 --> 00:19:44,910
 Our nails, we think of them as wonderful, but that's

321
00:19:44,910 --> 00:19:47,480
 because we pretend and we try to

322
00:19:47,480 --> 00:19:49,480
 make them beautiful.

323
00:19:49,480 --> 00:19:55,360
 Naturally, our nails are not beautiful.

324
00:19:55,360 --> 00:19:58,360
 They become chipped and yellowed.

325
00:19:58,360 --> 00:20:08,600
 They're again just stuck in the skin.

326
00:20:08,600 --> 00:20:13,000
 The truth is that none of these things are disgusting.

327
00:20:13,000 --> 00:20:16,020
 But if you look at them, you will become disgusted because

328
00:20:16,020 --> 00:20:17,740
 this is how our mind, the ordinary

329
00:20:17,740 --> 00:20:18,740
 mind works.

330
00:20:18,740 --> 00:20:24,200
 Our ordinary mind is caught up in likes and dislikes.

331
00:20:24,200 --> 00:20:28,960
 The truth of it is that these things don't even exist.

332
00:20:28,960 --> 00:20:33,030
 This is just seeing and there's nothing good or bad about

333
00:20:33,030 --> 00:20:33,520
 it.

334
00:20:33,520 --> 00:20:36,870
 But if you look, you will become disgusted and this can

335
00:20:36,870 --> 00:20:38,680
 help to bring you back down to

336
00:20:38,680 --> 00:20:42,960
 earth and stop feelings of lust that are also based on

337
00:20:42,960 --> 00:20:44,040
 delusion.

338
00:20:44,040 --> 00:20:47,000
 It's sort of a counterbalance.

339
00:20:47,000 --> 00:20:51,000
 It's not the same as insight, which is the real cure.

340
00:20:51,000 --> 00:20:57,790
 But it's kind of a band-aid, sort of a medication that

341
00:20:57,790 --> 00:21:01,360
 helps to control and condition that's

342
00:21:01,360 --> 00:21:03,080
 out of control.

343
00:21:03,080 --> 00:21:05,080
 Bring down a fever.

344
00:21:05,080 --> 00:21:12,300
 Our teeth as well, they get brown and are yellowed and ch

345
00:21:12,300 --> 00:21:15,000
ipped, crooked.

346
00:21:15,000 --> 00:21:19,360
 We try to straighten them out and get cavities and food

347
00:21:19,360 --> 00:21:21,320
 stuck between them.

348
00:21:21,320 --> 00:21:22,880
 Look at where they're sitting.

349
00:21:22,880 --> 00:21:25,920
 They're stuck in flesh and blood.

350
00:21:25,920 --> 00:21:30,880
 There's nothing beautiful about teeth and yet we get

351
00:21:30,880 --> 00:21:34,720
 attached to the beauty, the illusion.

352
00:21:34,720 --> 00:21:37,640
 So taking these apart is a meditation practice.

353
00:21:37,640 --> 00:21:40,280
 Finally the skin as well.

354
00:21:40,280 --> 00:21:43,000
 The skin of course is probably the most disgusting part.

355
00:21:43,000 --> 00:21:45,520
 It's where the sweat comes from and smell comes from.

356
00:21:45,520 --> 00:21:48,600
 But we don't see any of that because we wash it daily.

357
00:21:48,600 --> 00:21:50,200
 We're obsessed with it.

358
00:21:50,200 --> 00:21:56,600
 But if you leave your body alone, you start to see skin fl

359
00:21:56,600 --> 00:21:59,720
aking off, smell coming from

360
00:21:59,720 --> 00:22:09,840
 your various parts of the body, grease building up.

361
00:22:09,840 --> 00:22:10,840
 So asubha.

362
00:22:10,840 --> 00:22:11,840
 It's useful.

363
00:22:11,840 --> 00:22:14,860
 It's not useful for people who are angry, people who get

364
00:22:14,860 --> 00:22:16,560
 frustrated easily because this

365
00:22:16,560 --> 00:22:19,440
 just makes them more depressed.

366
00:22:19,440 --> 00:22:20,440
 It's good for people who have lust.

367
00:22:20,440 --> 00:22:22,520
 It's good for people who have desires.

368
00:22:22,520 --> 00:22:24,920
 So these two, you have to be careful with them.

369
00:22:24,920 --> 00:22:27,740
 Metta on the other hand is not good for people who have

370
00:22:27,740 --> 00:22:28,280
 lust.

371
00:22:28,280 --> 00:22:32,080
 People who have a lot of desire should not practice metta

372
00:22:32,080 --> 00:22:33,960
 because not only will they

373
00:22:33,960 --> 00:22:37,760
 become attached to the pleasure of feeling love but they'll

374
00:22:37,760 --> 00:22:39,440
 also become attached to the

375
00:22:39,440 --> 00:22:43,030
 people who they feel love for and are more inclined to

376
00:22:43,030 --> 00:22:44,720
 become attracted to them.

377
00:22:44,720 --> 00:22:47,880
 They can actually trigger feelings of lust.

378
00:22:47,880 --> 00:22:50,720
 So these are not universal meditations.

379
00:22:50,720 --> 00:22:58,300
 They should be used for specific situations, specific types

380
00:22:58,300 --> 00:22:59,960
 of people.

381
00:22:59,960 --> 00:23:03,850
 Buddha on the other hand is probably useful for just about

382
00:23:03,850 --> 00:23:04,320
 everyone.

383
00:23:04,320 --> 00:23:07,080
 I suppose if you have a lot of faith, you should be careful

384
00:23:07,080 --> 00:23:08,360
 not to use it too much.

385
00:23:08,360 --> 00:23:12,920
 A person is very faithful, they should not.

386
00:23:12,920 --> 00:23:18,830
 To believe in someone who has just blind faith should be

387
00:23:18,830 --> 00:23:21,720
 careful with Buddha because it can

388
00:23:21,720 --> 00:23:25,810
 just augment your faith and then you just blindly follow

389
00:23:25,810 --> 00:23:28,120
 whatever you think is right.

390
00:23:28,120 --> 00:23:31,730
 So people have all sorts of crazy ideas about the Buddha

391
00:23:31,730 --> 00:23:33,320
 based on their own defilements

392
00:23:33,320 --> 00:23:37,360
 because they have too much faith and not enough wisdom.

393
00:23:37,360 --> 00:23:40,040
 That's good for people who have doubt.

394
00:23:40,040 --> 00:23:43,240
 The final one is marana.

395
00:23:43,240 --> 00:23:45,080
 Marana means death.

396
00:23:45,080 --> 00:23:50,040
 Death is good for people who, it's another one for people

397
00:23:50,040 --> 00:23:52,840
 who have lack of encouragement

398
00:23:52,840 --> 00:23:57,440
 but it's better for, more specifically for lack of effort,

399
00:23:57,440 --> 00:23:58,880
 lack of energy.

400
00:23:58,880 --> 00:24:04,800
 It gives you a kick in the pants to know that we only have

401
00:24:04,800 --> 00:24:06,040
 so long.

402
00:24:06,040 --> 00:24:11,040
 So thinking about death is something that's useful to help

403
00:24:11,040 --> 00:24:14,040
 us cultivate the sense of urgency,

404
00:24:14,040 --> 00:24:21,370
 the need to work, the need to practice, the need to put out

405
00:24:21,370 --> 00:24:22,760
 effort.

406
00:24:22,760 --> 00:24:29,200
 If we can't put this off until tomorrow we don't know when

407
00:24:29,200 --> 00:24:32,240
 death is going to come.

408
00:24:32,240 --> 00:24:35,660
 So people will actually meditate. Again these are all full

409
00:24:35,660 --> 00:24:37,440
 meditations in their own right.

410
00:24:37,440 --> 00:24:41,770
 It's just that when we use them as insight meditation

411
00:24:41,770 --> 00:24:43,920
 practitioners, we use them in an

412
00:24:43,920 --> 00:24:50,440
 auxiliary sense, supportive sense to protect our meditation

413
00:24:50,440 --> 00:24:50,440
.

414
00:24:50,440 --> 00:24:54,170
 So you can say to yourself, "Adu wangmi ji vitam" - life is

415
00:24:54,170 --> 00:24:55,040
 uncertain.

416
00:24:55,040 --> 00:24:59,360
 "Dhu wangmarana" - death is certain.

417
00:24:59,360 --> 00:25:09,610
 "Ji vitam niyatam" - life is impermanent, not permanent,

418
00:25:09,610 --> 00:25:11,800
 not sure.

419
00:25:11,800 --> 00:25:19,360
 "Dhu marnam niyatam" - death is sure.

420
00:25:19,360 --> 00:25:24,680
 The only certainty about life is that you're going to die.

421
00:25:24,680 --> 00:25:27,580
 So it's something, it's interesting if you think ahead

422
00:25:27,580 --> 00:25:29,240
 about the future, it's sobering

423
00:25:29,240 --> 00:25:33,600
 to think in a hundred years we won't be here.

424
00:25:33,600 --> 00:25:39,120
 In fifty years probably not all of us will be here.

425
00:25:39,120 --> 00:25:43,600
 But in a hundred years everything we've done will be gone.

426
00:25:43,600 --> 00:25:51,120
 Everything we are, our whole identity will be ended.

427
00:25:51,120 --> 00:25:56,560
 And more importantly will have gone on somewhere else so

428
00:25:56,560 --> 00:26:00,120
 death is a really decisive moment,

429
00:26:00,120 --> 00:26:02,490
 something that is like an exam that we're working up

430
00:26:02,490 --> 00:26:03,120
 towards.

431
00:26:03,120 --> 00:26:08,640
 Or it's a deadline that we have to be prepared for.

432
00:26:08,640 --> 00:26:15,440
 And if we're not prepared, we can get fired.

433
00:26:15,440 --> 00:26:20,120
 We'll go to a bad place.

434
00:26:20,120 --> 00:26:23,120
 We'll be demoted.

435
00:26:23,120 --> 00:26:29,820
 A reminder that all the enjoyment that we have in life,

436
00:26:29,820 --> 00:26:33,440
 anything that we might cling

437
00:26:33,440 --> 00:26:36,530
 to or strive for, all of our ambition in the end is

438
00:26:36,530 --> 00:26:38,440
 meaningless and pointless.

439
00:26:38,440 --> 00:26:40,840
 You can't take any of it with you.

440
00:26:40,840 --> 00:26:43,490
 You can't take your belongings, you can't take your

441
00:26:43,490 --> 00:26:45,520
 achievement, you can't take your

442
00:26:45,520 --> 00:26:51,960
 memories and your name, mostly we forget everything.

443
00:26:51,960 --> 00:26:58,510
 All we take with us are good and evil, qualities of mind,

444
00:26:58,510 --> 00:27:01,520
 the qualities carry on.

445
00:27:01,520 --> 00:27:07,610
 The quality of our next life will be similar to the quality

446
00:27:07,610 --> 00:27:10,760
 that we end this life with.

447
00:27:10,760 --> 00:27:14,340
 And so thinking about this from time to time is quite,

448
00:27:14,340 --> 00:27:16,320
 obviously quite beneficial.

449
00:27:16,320 --> 00:27:19,480
 It's a support for our practice.

450
00:27:19,480 --> 00:27:22,480
 When you feel lack of energy and effort you think, "I don't

451
00:27:22,480 --> 00:27:24,080
 know when I'm going to die,

452
00:27:24,080 --> 00:27:27,560
 this could be my last chance to cultivate goodness.

453
00:27:27,560 --> 00:27:28,560
 I can die tomorrow.

454
00:27:28,560 --> 00:27:30,640
 I can die in an hour.

455
00:27:30,640 --> 00:27:35,320
 I can die with the next breath."

456
00:27:35,320 --> 00:27:37,520
 Knowing that we become very clear in the present.

457
00:27:37,520 --> 00:27:40,520
 It's very good at bringing you back to the present moment.

458
00:27:40,520 --> 00:27:44,690
 You realize there's no real use worrying about the future

459
00:27:44,690 --> 00:27:46,520
 when it's uncertain.

460
00:27:46,520 --> 00:27:50,500
 And the only thing certain is that it's just going to end

461
00:27:50,500 --> 00:27:53,520
 and wind up meaningless anyway.

462
00:27:53,520 --> 00:27:57,360
 It sobers you up and brings you back to reality.

463
00:27:57,360 --> 00:28:03,570
 It helps you see what's truly important and that's here and

464
00:28:03,570 --> 00:28:04,400
 now.

465
00:28:04,400 --> 00:28:06,880
 So these are the four Arakaka Kamadham.

466
00:28:06,880 --> 00:28:10,620
 All four of these, as you can hopefully tell, are

467
00:28:10,620 --> 00:28:12,960
 supportive for our meditation practice.

468
00:28:12,960 --> 00:28:16,490
 Again, we should be focused on mindfulness, trying to be

469
00:28:16,490 --> 00:28:18,040
 mindful all the time.

470
00:28:18,040 --> 00:28:19,720
 But these four will support your practice.

471
00:28:19,720 --> 00:28:24,680
 It's hard doing an intensive meditation course especially.

472
00:28:24,680 --> 00:28:29,100
 These things can support you, give you encouragement and

473
00:28:29,100 --> 00:28:32,320
 relief from the difficulty of the insight

474
00:28:32,320 --> 00:28:34,040
 practice.

475
00:28:34,040 --> 00:28:37,640
 Just don't let them become a crutch, that's all.

476
00:28:37,640 --> 00:28:39,640
 So that's the Dhamma for today.

477
00:28:39,640 --> 00:28:43,760
 Thank you all for tuning in.

478
00:28:43,760 --> 00:28:44,280
 Keep practicing.

