1
00:00:00,000 --> 00:00:21,560
 Okay, good evening everyone.

2
00:00:21,560 --> 00:00:25,720
 Welcome to our evening session.

3
00:00:25,720 --> 00:00:36,560
 I was invited next month to give a talk, a couple of talks.

4
00:00:36,560 --> 00:00:40,970
 One of them was asked that I give a talk on wrong

5
00:00:40,970 --> 00:00:44,640
 mindfulness.

6
00:00:44,640 --> 00:00:46,800
 And there actually is a term, right?

7
00:00:46,800 --> 00:00:58,200
 There's right mindfulness and there's wrong mindfulness.

8
00:00:58,200 --> 00:01:01,500
 Something in me wants to say that, well, mindfulness, as we

9
00:01:01,500 --> 00:01:04,440
 know from the Abhidhamma, is, mindfulness

10
00:01:04,440 --> 00:01:09,000
 is always wholesome.

11
00:01:09,000 --> 00:01:16,880
 In the sutta, the Buddha said, "sati ncakwahang bhikkha rei

12
00:01:16,880 --> 00:01:21,080
 sabatikang vadami."

13
00:01:21,080 --> 00:01:23,880
 Mindfulness I tell you monks is always useful.

14
00:01:23,880 --> 00:01:27,240
 It's always good.

15
00:01:27,240 --> 00:01:35,060
 But then he talks about something called micha sati in the

16
00:01:35,060 --> 00:01:39,720
 context of the Eightfold Path.

17
00:01:39,720 --> 00:01:42,420
 I think it's a little misleading for us to say wrong

18
00:01:42,420 --> 00:01:43,400
 mindfulness.

19
00:01:43,400 --> 00:01:48,360
 I mean, technically, mindfulness is always right.

20
00:01:48,360 --> 00:01:51,160
 It's always good.

21
00:01:51,160 --> 00:01:55,570
 But from a conceptual perspective, when we talk about

22
00:01:55,570 --> 00:01:58,640
 mindfulness practice, when we talk

23
00:01:58,640 --> 00:02:02,700
 about being attentive and the English word mindfulness,

24
00:02:02,700 --> 00:02:04,760
 which is a rather general and

25
00:02:04,760 --> 00:02:12,840
 broad term, it is possible to understand it as good and bad

26
00:02:12,840 --> 00:02:13,400
.

27
00:02:13,400 --> 00:02:14,360
 Good or bad.

28
00:02:14,360 --> 00:02:17,400
 Sometimes good, sometimes bad.

29
00:02:17,400 --> 00:02:21,810
 So I thought I'd talk a little bit about ways that the

30
00:02:21,810 --> 00:02:24,080
 practice can go wrong.

31
00:02:24,080 --> 00:02:28,090
 Not specifically talking about the state of mind that is

32
00:02:28,090 --> 00:02:30,720
 wrong, but when you're practicing

33
00:02:30,720 --> 00:02:34,240
 meditation, what can go wrong?

34
00:02:34,240 --> 00:02:35,960
 This is quite interesting.

35
00:02:35,960 --> 00:02:40,750
 I think this was really what they're trying to get at with

36
00:02:40,750 --> 00:02:42,400
 this invitation.

37
00:02:42,400 --> 00:02:48,960
 They want me to talk about how the practice can go wrong.

38
00:02:48,960 --> 00:02:52,760
 You're practicing meditation, what can go wrong?

39
00:02:52,760 --> 00:02:57,120
 How do you know you're practicing well, right?

40
00:02:57,120 --> 00:03:03,560
 Sometimes there's this doubt, am I doing it right?

41
00:03:03,560 --> 00:03:11,540
 So the first way that the practice can go wrong, obviously,

42
00:03:11,540 --> 00:03:14,920
 is you don't practice.

43
00:03:14,920 --> 00:03:22,920
 Of course, that seems quite obvious and kind of irrelevant.

44
00:03:22,920 --> 00:03:23,920
 We're talking about practice.

45
00:03:23,920 --> 00:03:27,200
 What does it mean to not practice?

46
00:03:27,200 --> 00:03:30,680
 Almost obviously it means those people who don't come,

47
00:03:30,680 --> 00:03:32,680
 those people who never thought

48
00:03:32,680 --> 00:03:34,680
 of practicing meditation.

49
00:03:34,680 --> 00:03:41,440
 Well, that's not going to get you anywhere.

50
00:03:41,440 --> 00:03:43,240
 But then there are those people who have heard of

51
00:03:43,240 --> 00:03:44,720
 meditation, those people who have been

52
00:03:44,720 --> 00:03:52,320
 even taught meditation, but are not interested in it.

53
00:03:52,320 --> 00:03:53,640
 It's also quite common.

54
00:03:53,640 --> 00:03:58,560
 Sometimes you'll teach someone and it just goes in one ear

55
00:03:58,560 --> 00:04:01,200
 and out the other, over their

56
00:04:01,200 --> 00:04:11,240
 head, not able to or not interested in understanding it.

57
00:04:11,240 --> 00:04:18,690
 But more importantly, for all of us, congratulations, you

58
00:04:18,690 --> 00:04:21,480
've passed that one.

59
00:04:21,480 --> 00:04:24,710
 What you might find instead is that sometimes even though

60
00:04:24,710 --> 00:04:26,600
 you intend to practice, you're

61
00:04:26,600 --> 00:04:29,840
 actually not practicing.

62
00:04:29,840 --> 00:04:33,760
 So walking and sitting unmindfully, we do a lot of walking

63
00:04:33,760 --> 00:04:35,520
 and a lot of sitting, and

64
00:04:35,520 --> 00:04:38,810
 I'm sure sometimes you'll notice that you weren't being

65
00:04:38,810 --> 00:04:41,000
 mindful, even though your intention

66
00:04:41,000 --> 00:04:44,400
 was to be mindful.

67
00:04:44,400 --> 00:04:51,440
 And you were trying your best to be mindful.

68
00:04:51,440 --> 00:04:59,250
 You find yourself getting distracted or slipping, slipping

69
00:04:59,250 --> 00:05:02,320
 back into old habits.

70
00:05:02,320 --> 00:05:05,700
 So unmindfulness is of course the first way that the

71
00:05:05,700 --> 00:05:07,320
 practice can go wrong.

72
00:05:07,320 --> 00:05:09,560
 And what's the result of that?

73
00:05:09,560 --> 00:05:12,020
 The result of that is generally just a lot of stress and

74
00:05:12,020 --> 00:05:12,760
 suffering.

75
00:05:12,760 --> 00:05:16,290
 Of course, if you don't ever practice mindfulness at all,

76
00:05:16,290 --> 00:05:18,580
 your life is going to be full of stress

77
00:05:18,580 --> 00:05:21,800
 and suffering as most people are.

78
00:05:21,800 --> 00:05:25,450
 Potentially, of course, it is possible that things will be

79
00:05:25,450 --> 00:05:27,120
 good for a while, but it's

80
00:05:27,120 --> 00:05:33,420
 quite uncertain because you're very easily sidetracked and

81
00:05:33,420 --> 00:05:37,220
 get caught up in unwholesomeness.

82
00:05:37,220 --> 00:05:45,720
 As a meditator, meditating unmindfully, this comes in many

83
00:05:45,720 --> 00:05:49,120
 ways, in many forms.

84
00:05:49,120 --> 00:05:53,610
 So you could be mindful of the past or the future if you

85
00:05:53,610 --> 00:05:56,040
 get lost thinking about the

86
00:05:56,040 --> 00:05:56,720
 past.

