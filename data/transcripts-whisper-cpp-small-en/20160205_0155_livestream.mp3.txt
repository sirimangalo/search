 So good evening everyone.
 I'm broadcasting live February 4th, 2016.
 Today's quote is interesting.
 I had to read it a few times before I actually got a sense
 of what it means.
 Still I don't have it completely.
 Sounds a little bit weird.
 So let's break it up.
 Concerning those internal things, I'm not convinced that's
 a very good translation.
 Yeah I get rid of the word "those" because I don't really
 understand it.
 Don't think it's a part of the actual text.
 It's much simpler than it's actually written.
 So it's talking about a sekha, a learner, one who is still
 in training.
 So there are two kinds of monks, there are the sekha, sorry
, two kinds of Buddhists,
 sekha and assekha.
 Actually I guess it's two kinds of enlightened beings
 really.
 Sekha is someone who still is in training.
 Sekha, sekha, sikha-di, one who trains.
 Sekha is one who is training.
 Sekha is the training, sekha is one who trains.
 So it's just a standard description of someone who's still
 striving.
 Then we have apatamana-manasasa, anutraṁ yoga-kemam, pata
-yamanasa.
 One whose mind has not attained supreme freedom from yoga
 when it doesn't practice yoga, no?
 No, supreme freedom from yoga, yoga is a bond, something
 that keeps you tied, something that
 keeps you stuck.
 So to attain supreme freedom from bondage.
 Then there's this funny thing about internal things.
 I read it as one who dwells.
 And something about having made, who dwells having made
 internal that which is inside.
 I don't get it, my poly is not good enough.
 I think it means focusing on internal things.
 One who dwells focusing on internal things and it just says
 there's one thing, there's
 no other thing that is more useful than yoniso-manasikara.
 Again a very common Buddhist term that you almost don't
 want to translate, but he translates
 as giving close attention, giving close attention to the
 mind.
 Not quite how I would put it.
 Yoniso means, yonias actually means womb, the source or the
 womb.
 So what it means is keeping things in mind, keeping the
 sources of things in mind or getting
 to the source of things, analyzing things from a point of
 view of their source.
 So it can be used intellectually, it's usually not.
 It's usually used in terms of describing meditation.
 It's a really good description because insight meditation
 is all about getting to the source
 of things.
 That's why we have this word that we use to remind
 ourselves, to get to the yonit, to
 get to the source.
 Because normally when we experience things we create so
 much more out of them.
 It's all on a conceptual and abstract plane and we get
 stuck on this abstract plane that
 ends up being quite removed from reality, removed from the
 source.
 Yoniso manasikara is about returning to the source of
 things.
 Not grasping at the particulars, not grasping at the signs.
 Nimita is the sign, so when you see a flower, a sign of the
 flower is what makes you think
 it's a flower.
 When you hear a car, the sign of the car is what makes you
 think of it as a car.
 Sign, anubhianjana are the different characteristics, is it
 good or bad or beautiful or ugly or
 sweet or sour or so on.
 So the characteristics that we don't cling to, we get to
 the source.
 When you hear a car, the truth of it is the sound, the
 source of it is the sound.
 So the Buddha says this is the one thing that allows us, if
 you take nothing else, just
 have yoniso manasikara.
 And there's a nice verse that comes after it.
 Yoniso manasikara is the dhamma for bhikkhu in training.
 Natanyo e wang bhukkaru uttamatasapatiya Natanyo there is
 no other dhamma that is of
 such great benefit that does so much as this for the
 attainment of the highest benefit
 or the highest goal.
 Yoniso bandahang bhikkhu for a bhikkhu who strives for the
 source.
 Very clear instruction, get to the source of things.
 It doesn't mean go back to their causes in the past or so.
 Get to the source of the present.
 When you say to yourself, "Pain, pain."
 You're with the source of the pain, hearing.
 You're at the real true source of the experience.
 Kayang dukha sa papune.
 The destruction or the end of dukha, the end of suffering,
 papune, the reach.
 He reaches.
 Papune, not sure about that one.
 Papune means attain, I just don't know the tense.
 I think it means he reach.
 Yeah, here we are.
 Should reach, would reach.
 It's papune's potential.
 So a simple but very powerful teaching.
 Do good to explain it, to be clear about what it means.
 This is a quote, the translation is not ideal.
 It's not close attention to the mind.
 It's not the mind, it's the objects that the mind keeps
 close attention to.
 The mind, not just close attention, but attention to the
 source, yoniso.
 Yoniso padahang bhikkhu.
 A bhikkhu who strives for the source or sticks to the
 source.
 And get caught up in extrapolation or judgment or reaction.
 It's amazing how it changes your outlook.
 Just teaching students at the university tomorrow again, I
'll be teaching.
 Just give people a five minute meditation lesson.
 You can show them something that they've never seen before.
 On a door that they've never even knew was there.
 I've been teaching hour long sessions and people say how
 they never realized their mind
 was so chaotic or so messed up through not meditating.
 Teach a course on the internet.
 I've been teaching courses on the internet and people who I
 don't know say that people
 who have never met in person say they've changed their
 lives through meditation.
 And now we have three meditators here who are really on the
 sekha path here at our house
 who are doing real intensive meditation practicing many
 hours a day.
 All of this is the good work.
 The best part of Buddhism, the best part of the Buddhist
 teachings is called doing the
 Buddhist work.
 Like in other religions have the Lord's work or so on.
 We have the Buddha's work.
 So that's our dhamma drop for today.
 Thank you all for tuning in.
 Wishing you all the best in your practice.
 Good day.
 Thank you.
