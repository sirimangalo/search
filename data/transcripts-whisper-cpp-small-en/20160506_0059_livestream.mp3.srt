1
00:00:00,000 --> 00:00:15,160
 Good evening everyone.

2
00:00:15,160 --> 00:00:26,080
 We're broadcasting live May 5th, 2016.

3
00:00:26,080 --> 00:00:34,320
 Tonight's quote from the Itiutaka.

4
00:00:34,320 --> 00:00:37,240
 It's actually interesting.

5
00:00:37,240 --> 00:00:41,720
 The quote itself is fairly simple.

6
00:00:41,720 --> 00:00:43,080
 It's talking about two spheres.

7
00:00:43,080 --> 00:00:48,930
 The quote is actually mistranslated as usual, which may not

8
00:00:48,930 --> 00:00:49,800
 be important.

9
00:00:49,800 --> 00:00:55,080
 It's actually more of an interpretative translation.

10
00:00:55,080 --> 00:00:57,720
 The word for spiritual is interesting.

11
00:00:57,720 --> 00:01:00,670
 Those of you who know anything about Pali or the Dhamma

12
00:01:00,670 --> 00:01:02,960
 might be wondering, "Well, where

13
00:01:02,960 --> 00:01:04,280
 does that word come from?

14
00:01:04,280 --> 00:01:07,800
 I haven't seen that word before."

15
00:01:07,800 --> 00:01:08,800
 And you'd be right.

16
00:01:08,800 --> 00:01:10,800
 That's not the word that's being used.

17
00:01:10,800 --> 00:01:14,280
 Let's look at the Pali.

18
00:01:14,280 --> 00:01:19,000
 You can go to the commentary up.

19
00:01:19,000 --> 00:01:21,760
 Let's look at the original Pali.

20
00:01:21,760 --> 00:01:36,480
 There are these two types of gifts.

21
00:01:36,480 --> 00:01:37,920
 What two types of gifts?

22
00:01:37,920 --> 00:01:40,480
 A gift of amisa.

23
00:01:40,480 --> 00:01:45,480
 Amisa means an object or a physical thing.

24
00:01:45,480 --> 00:01:50,000
 Something physical is amisa.

25
00:01:50,000 --> 00:01:55,440
 And dhamma dana is a gift of dhamma.

26
00:01:55,440 --> 00:01:59,410
 So you can see how we get spiritual from that, but it's

27
00:01:59,410 --> 00:02:01,800
 important to be precise and clear

28
00:02:01,800 --> 00:02:03,960
 of what we're talking about.

29
00:02:03,960 --> 00:02:05,960
 A gift of truth.

30
00:02:05,960 --> 00:02:07,960
 Dhamma means truth.

31
00:02:07,960 --> 00:02:14,040
 The word dhamma is an interesting word.

32
00:02:14,040 --> 00:02:18,810
 Probably I haven't studied yet, but it was used in Hinduism

33
00:02:18,810 --> 00:02:21,400
 to mean something that someone

34
00:02:21,400 --> 00:02:24,800
 holds.

35
00:02:24,800 --> 00:02:31,730
 So a warrior's dharmma, because the root dharm means to

36
00:02:31,730 --> 00:02:35,680
 carry or to hold, to protect, to

37
00:02:35,680 --> 00:02:38,280
 guard, to keep.

38
00:02:38,280 --> 00:02:40,360
 Keep might be a good one.

39
00:02:40,360 --> 00:02:45,840
 So a warrior's dharmma would be the rules they keep or the

40
00:02:45,840 --> 00:02:48,160
 code that they kept.

41
00:02:48,160 --> 00:02:51,080
 It was what they held.

42
00:02:51,080 --> 00:02:56,660
 We say that in English when you hold something to be true

43
00:02:56,660 --> 00:03:00,400
 or you hold something to be right.

44
00:03:00,400 --> 00:03:03,040
 That's what dharm comes from.

45
00:03:03,040 --> 00:03:05,240
 So the Buddha twisted that.

46
00:03:05,240 --> 00:03:07,230
 By the time the Buddha came around, I've talked about this

47
00:03:07,230 --> 00:03:08,280
 before, by the time the Buddha

48
00:03:08,280 --> 00:03:11,310
 came around, it would have meant anything that someone

49
00:03:11,310 --> 00:03:12,400
 holds to be true.

50
00:03:12,400 --> 00:03:15,510
 So a teacher's dharmma, a spiritual teacher would have

51
00:03:15,510 --> 00:03:16,600
 their dharmma.

52
00:03:16,600 --> 00:03:19,180
 There were many different kinds of teachers who taught many

53
00:03:19,180 --> 00:03:20,480
 different things, and each

54
00:03:20,480 --> 00:03:24,440
 one what they taught was called their dharmma.

55
00:03:24,440 --> 00:03:30,420
 So the Buddha had his own dharmma, or dhamma as we say in

56
00:03:30,420 --> 00:03:31,360
 Pali.

57
00:03:31,360 --> 00:03:34,690
 But the Buddha explained it a bit differently, and he

58
00:03:34,690 --> 00:03:37,240
 talked about something that holds its

59
00:03:37,240 --> 00:03:42,940
 own, or something that holds, and it's another way we use

60
00:03:42,940 --> 00:03:45,720
 in English the word hold.

61
00:03:45,720 --> 00:03:47,320
 This holds, that holds.

62
00:03:47,320 --> 00:03:48,840
 It doesn't hold that.

63
00:03:48,840 --> 00:03:49,840
 It holds that.

64
00:03:49,840 --> 00:03:54,620
 It's a figurative way of saying something is true,

65
00:03:54,620 --> 00:03:57,040
 something is factual.

66
00:03:57,040 --> 00:04:00,920
 Meaning it holds up to inspection.

67
00:04:00,920 --> 00:04:06,130
 When you inspect it, it holds up, meaning it doesn't break

68
00:04:06,130 --> 00:04:06,920
 down.

69
00:04:06,920 --> 00:04:10,500
 So when you inspect a view, some views when you inspect

70
00:04:10,500 --> 00:04:12,760
 them, when you investigate them,

71
00:04:12,760 --> 00:04:14,480
 they break down.

72
00:04:14,480 --> 00:04:17,880
 You see they don't accord with reality.

73
00:04:17,880 --> 00:04:21,880
 But some hold up.

74
00:04:21,880 --> 00:04:24,840
 And so what that means is reality or truth.

75
00:04:24,840 --> 00:04:28,600
 So the Buddha's dharmma is the truth, is the truth.

76
00:04:28,600 --> 00:04:32,690
 Of course, every religious teacher says that, but that's

77
00:04:32,690 --> 00:04:34,000
 how he means it.

78
00:04:34,000 --> 00:04:38,810
 So that's how I think we should understand it here, a gift

79
00:04:38,810 --> 00:04:39,880
 of truth.

80
00:04:39,880 --> 00:04:43,970
 And he says, "Eta dangang bhikkave mei sangdvinang danaanam

81
00:04:43,970 --> 00:04:44,360
."

82
00:04:44,360 --> 00:04:46,160
 "Yedidang dhamma dana."

83
00:04:46,160 --> 00:04:49,320
 "Dhamma dana" is superior of the two.

84
00:04:49,320 --> 00:04:57,000
 A gift of dhamma is superior, a gift of truth.

85
00:04:57,000 --> 00:04:59,480
 And then he says the same about samvibhaga, which means

86
00:04:59,480 --> 00:05:00,120
 sharing.

87
00:05:00,120 --> 00:05:03,930
 There are two kinds of sharing, there are sharing of

88
00:05:03,930 --> 00:05:06,320
 material possessions and there

89
00:05:06,320 --> 00:05:10,160
 is sharing of truth.

90
00:05:10,160 --> 00:05:16,000
 And anugaha, which means support, assistance, help.

91
00:05:16,000 --> 00:05:20,920
 There are two kinds of help.

92
00:05:20,920 --> 00:05:25,640
 Helping someone materially, giving them money or giving

93
00:05:25,640 --> 00:05:28,200
 them food or giving them etc.

94
00:05:28,200 --> 00:05:34,800
 And then helping them out with the truth.

95
00:05:34,800 --> 00:05:39,960
 And he says in each case the truth is better.

96
00:05:39,960 --> 00:05:42,640
 The commentary has some interesting explanations.

97
00:05:42,640 --> 00:05:45,920
 It's what you'd expect.

98
00:05:45,920 --> 00:05:51,650
 Ami sadaana is the four basic requisites, giving physical

99
00:05:51,650 --> 00:05:54,280
 things that are needed.

100
00:05:54,280 --> 00:06:01,300
 Maybe it's not that interesting, but it uses some

101
00:06:01,300 --> 00:06:07,000
 interesting language like when you are

102
00:06:07,000 --> 00:06:13,050
 not up, when you are up, posuko, which means lazy or

103
00:06:13,050 --> 00:06:17,080
 inactive, you give up your life of

104
00:06:17,080 --> 00:06:22,800
 ease to teach others.

105
00:06:22,800 --> 00:06:25,840
 And how it's a gift, because to give someone the truth is

106
00:06:25,840 --> 00:06:27,560
 an interesting, it seems kind

107
00:06:27,560 --> 00:06:29,440
 of strange, I suppose.

108
00:06:29,440 --> 00:06:30,600
 How do you give someone the truth?

109
00:06:30,600 --> 00:06:34,920
 Well that means taking the time to teach them the truth.

110
00:06:34,920 --> 00:06:40,550
 Aposuko ahutwa, not being lazy or not being focused only on

111
00:06:40,550 --> 00:06:43,000
 your own well-being.

112
00:06:43,000 --> 00:06:47,420
 And then the second one is, or the third one, giving

113
00:06:47,420 --> 00:06:48,800
 assistance.

114
00:06:48,800 --> 00:06:57,440
 Pareisaan anugaan, anugaanhaan, ghanhaanal, anukampang.

115
00:06:57,440 --> 00:07:01,520
 Anukampang means compassionately.

116
00:07:01,520 --> 00:07:13,680
 It means kampa, kampanal, means to be moved.

117
00:07:13,680 --> 00:07:20,690
 And anukampanam means being moved to compassion or moved in

118
00:07:20,690 --> 00:07:24,440
 relation to someone, moved to

119
00:07:24,440 --> 00:07:27,520
 want to help them.

120
00:07:27,520 --> 00:07:33,760
 So you can help them in two ways, with physical material

121
00:07:33,760 --> 00:07:37,600
 possessions or with the truth.

122
00:07:37,600 --> 00:07:42,240
 So this is of course a perennial subject, we've seen many

123
00:07:42,240 --> 00:07:44,280
 quotes about giving.

124
00:07:44,280 --> 00:07:47,960
 What does it mean to give?

125
00:07:47,960 --> 00:07:55,190
 And the importance of giving, and I often bring up the

126
00:07:55,190 --> 00:07:58,840
 topic or the fact that it's a

127
00:07:58,840 --> 00:08:05,280
 bit of an awkward thing and it sounds somewhat self-serving

128
00:08:05,280 --> 00:08:08,280
 to teach generosity as a teacher

129
00:08:08,280 --> 00:08:12,330
 because it's a monastic because we rely upon people's

130
00:08:12,330 --> 00:08:13,520
 generosity.

131
00:08:13,520 --> 00:08:19,770
 But it's not something to be ashamed of, it's not something

132
00:08:19,770 --> 00:08:22,960
 to shy away from because it's

133
00:08:22,960 --> 00:08:27,540
 just a fact that in order to keep teaching, in order to

134
00:08:27,540 --> 00:08:30,040
 keep sharing the dhamma, we need

135
00:08:30,040 --> 00:08:34,310
 to be fed, we need to be given, we need to have the requ

136
00:08:34,310 --> 00:08:36,800
isites, the basics of life to

137
00:08:36,800 --> 00:08:40,200
 continue our work.

138
00:08:40,200 --> 00:08:44,640
 But this quote is for those who give what they should think

139
00:08:44,640 --> 00:08:47,240
 about giving and it's important

140
00:08:47,240 --> 00:08:52,070
 not to fixate on material possessions because yes, it's

141
00:08:52,070 --> 00:08:54,880
 good to give physical things but

142
00:08:54,880 --> 00:08:57,920
 it's not the best, it's not something we should focus on.

143
00:08:57,920 --> 00:09:02,780
 And so in the Buddha's time as well as in present, it's

144
00:09:02,780 --> 00:09:06,560
 easy to get caught up in amissadāna,

145
00:09:06,560 --> 00:09:14,040
 giving too much material and forgetting about the truth.

146
00:09:14,040 --> 00:09:18,940
 So a lot of people will support teachers or support

147
00:09:18,940 --> 00:09:22,400
 Buddhist monks but they never give

148
00:09:22,400 --> 00:09:24,910
 the dhamma, so they're constantly giving material

149
00:09:24,910 --> 00:09:27,560
 possessions but they never have any, they

150
00:09:27,560 --> 00:09:30,320
 never work to give the dhamma.

151
00:09:30,320 --> 00:09:35,250
 And that fact kind of I think shows that in fact material,

152
00:09:35,250 --> 00:09:37,640
 whereas it seems to be that

153
00:09:37,640 --> 00:09:40,750
 material possessions are more concrete, right, so they're

154
00:09:40,750 --> 00:09:42,400
 more weighty and so immediately

155
00:09:42,400 --> 00:09:43,880
 we think well this is good.

156
00:09:43,880 --> 00:09:47,930
 And if you think about giving the dhamma, well it sounds

157
00:09:47,930 --> 00:09:50,240
 kind of trivial, just telling

158
00:09:50,240 --> 00:09:54,400
 someone what to do teaching, that's easy.

159
00:09:54,400 --> 00:09:58,650
 But in fact the opposite is the case, it's not easy to

160
00:09:58,650 --> 00:09:59,480
 teach.

161
00:09:59,480 --> 00:10:06,400
 It's not easy to teach but it's something, it's far better,

162
00:10:06,400 --> 00:10:09,240
 it's far better to give the

163
00:10:09,240 --> 00:10:21,680
 dhamma than to give physical, material, material gifts.

164
00:10:21,680 --> 00:10:25,570
 It's like if you give the, the, the, no, giving physical,

165
00:10:25,570 --> 00:10:28,280
 giving material possessions, material

166
00:10:28,280 --> 00:10:34,160
 gifts is actually much easier, much more trivial.

167
00:10:34,160 --> 00:10:38,250
 If you've ever tried to teach someone, you've ever tried to

168
00:10:38,250 --> 00:10:40,240
 give someone the truth, you've

169
00:10:40,240 --> 00:10:43,500
 even tried to give advice to someone, it's not easy, it's

170
00:10:43,500 --> 00:10:45,280
 easy to give advice, it's not

171
00:10:45,280 --> 00:10:48,280
 as easy to give good advice.

172
00:10:48,280 --> 00:10:52,200
 Anyone can give advice and mostly we do, we're very good at

173
00:10:52,200 --> 00:10:54,120
 giving advice, we're not very

174
00:10:54,120 --> 00:10:57,560
 good at giving good advice, this is the problem.

175
00:10:57,560 --> 00:11:05,080
 To give good advice you need to work, you need to train.

176
00:11:05,080 --> 00:11:11,220
 But I absolutely can't stress enough the importance of

177
00:11:11,220 --> 00:11:13,720
 giving the dhamma.

178
00:11:13,720 --> 00:11:19,380
 Of course giving to yourself first, you should never teach

179
00:11:19,380 --> 00:11:22,640
 without practicing yourself but

180
00:11:22,640 --> 00:11:24,960
 the greatness of giving the dhamma, just sharing what you

181
00:11:24,960 --> 00:11:25,680
've learned.

182
00:11:25,680 --> 00:11:28,480
 I mean that's how Buddhism will continue.

183
00:11:28,480 --> 00:11:33,050
 If people stop spreading the dhamma, if we're content with

184
00:11:33,050 --> 00:11:35,840
 giving physical gifts, material

185
00:11:35,840 --> 00:11:41,200
 gifts, if our goodness focuses mostly as Buddhists, we

186
00:11:41,200 --> 00:11:45,720
 practice for ourselves and then we're generous

187
00:11:45,720 --> 00:11:49,160
 and with material possessions, Buddhism will never survive.

188
00:11:49,160 --> 00:11:51,970
 And you can see this in Buddhist cultures where giving

189
00:11:51,970 --> 00:11:54,120
 material possessions is stressed,

190
00:11:54,120 --> 00:11:59,020
 it becomes corrupt quite easily, monks become corrupt, the

191
00:11:59,020 --> 00:12:01,760
 institution becomes corrupt.

192
00:12:01,760 --> 00:12:06,970
 But where people give dhamma, there's more scrutiny and

193
00:12:06,970 --> 00:12:09,440
 there's more interest and it's

194
00:12:09,440 --> 00:12:13,800
 more public with more people practicing.

195
00:12:13,800 --> 00:12:19,200
 More people come in to learn, to understand.

196
00:12:19,200 --> 00:12:22,890
 There's a greater understanding when we're talking about

197
00:12:22,890 --> 00:12:24,800
 the dhamma, when we give the

198
00:12:24,800 --> 00:12:25,800
 dhamma.

199
00:12:25,800 --> 00:12:29,800
 So it's wrong to think that I'm not a teacher, I don't dare

200
00:12:29,800 --> 00:12:32,040
 teach because I'm not a teacher.

201
00:12:32,040 --> 00:12:34,200
 That's the wrong way of looking at it.

202
00:12:34,200 --> 00:12:38,690
 Buddha didn't think of himself or he did but he often

203
00:12:38,690 --> 00:12:41,680
 talked about himself or a teacher

204
00:12:41,680 --> 00:12:45,780
 as more of a good friend, someone who gives something.

205
00:12:45,780 --> 00:12:50,490
 And so often he used this kind of language, giving a gift

206
00:12:50,490 --> 00:12:51,560
 of truth.

207
00:12:51,560 --> 00:12:55,470
 That's what it is, if you know the truth, if you've been

208
00:12:55,470 --> 00:12:57,680
 taught the truth, share it.

209
00:12:57,680 --> 00:13:04,790
 It's like on Facebook we share all sorts of stuff because

210
00:13:04,790 --> 00:13:07,600
 we think it's good.

211
00:13:07,600 --> 00:13:11,350
 In Buddhist cultures they'll often print texts and they'll

212
00:13:11,350 --> 00:13:13,260
 print books, you know, people

213
00:13:13,260 --> 00:13:15,960
 who helped print this book.

214
00:13:15,960 --> 00:13:19,250
 That's a good point, if I'm going to Sri Lanka we might as

215
00:13:19,250 --> 00:13:21,040
 well print another thousand of

216
00:13:21,040 --> 00:13:22,760
 them, right?

217
00:13:22,760 --> 00:13:28,920
 So we should have another, here we go, this is a good.

218
00:13:28,920 --> 00:13:33,560
 But you know even printing books, it's secondary.

219
00:13:33,560 --> 00:13:37,120
 Printing books is useful, it's great.

220
00:13:37,120 --> 00:13:44,940
 But if you compare it to not to the writing of the book,

221
00:13:44,940 --> 00:13:47,320
 the creating of the book, and

222
00:13:47,320 --> 00:13:48,320
 that's what we need.

223
00:13:48,320 --> 00:13:52,350
 We need not people writing more books necessarily but

224
00:13:52,350 --> 00:13:58,640
 actually teaching, actually helping, actually

225
00:13:58,640 --> 00:14:00,400
 working.

226
00:14:00,400 --> 00:14:04,830
 So once we get a center, if we ever get a thriving

227
00:14:04,830 --> 00:14:08,680
 meditation center, maybe we can start training

228
00:14:08,680 --> 00:14:09,680
 teachers as well.

229
00:14:09,680 --> 00:14:10,680
 I've done that before.

230
00:14:10,680 --> 00:14:11,680
 It's not difficult.

231
00:14:11,680 --> 00:14:14,680
 It's not difficult to teach.

232
00:14:14,680 --> 00:14:18,680
 Again, it just takes time to teach well.

233
00:14:18,680 --> 00:14:24,480
 You have to work at that.

234
00:14:24,480 --> 00:14:25,480
 Quite a simple quote.

235
00:14:25,480 --> 00:14:30,400
 Not too much more to say.

236
00:14:30,400 --> 00:14:33,640
 Except to encourage, we should be encouraged to give gifts

237
00:14:33,640 --> 00:14:35,320
 of Dhamma, not just printing

238
00:14:35,320 --> 00:14:39,360
 books and giving, you know, because that's kind of actually

239
00:14:39,360 --> 00:14:41,280
 just physical, you know,

240
00:14:41,280 --> 00:14:43,400
 it's still just a material thing.

241
00:14:43,400 --> 00:14:46,650
 But actually giving someone the truth, that's very

242
00:14:46,650 --> 00:14:47,600
 difficult.

243
00:14:47,600 --> 00:14:49,850
 Something we should work at, it should be a part of our

244
00:14:49,850 --> 00:14:50,480
 practice.

245
00:14:50,480 --> 00:14:58,140
 The Buddha said, or the commentary says, reads it again, "A

246
00:14:58,140 --> 00:15:02,120
po suko ahutva," not being lazy,

247
00:15:02,120 --> 00:15:09,440
 not being indolent, inert.

248
00:15:09,440 --> 00:15:13,030
 It's easy as a meditator to just go off on your own and do

249
00:15:13,030 --> 00:15:15,040
 nothing, which is fine.

250
00:15:15,040 --> 00:15:17,920
 But it's much greater to share the Dhamma.

251
00:15:17,920 --> 00:15:24,920
 On the Sammwibha goal, the sharing of the Dhamma.

252
00:15:24,920 --> 00:15:25,920
 We should share.

253
00:15:25,920 --> 00:15:28,920
 I taught that in kindergarten.

254
00:15:28,920 --> 00:15:33,520
 Anyway, that's the quote for tonight.

255
00:15:33,520 --> 00:15:36,320
 Another drop of Dhamma.

256
00:15:36,320 --> 00:15:45,480
 Do you have any questions?

257
00:15:45,480 --> 00:15:47,850
 How can we give the Dhamma if we are surrounded by un

258
00:15:47,850 --> 00:15:49,080
interested people?

259
00:15:49,080 --> 00:15:50,280
 That's a good point.

260
00:15:50,280 --> 00:15:54,030
 If there's no one, I mean, it's not so much that interested

261
00:15:54,030 --> 00:15:56,240
, it's people who are in need

262
00:15:56,240 --> 00:16:00,960
 or are receptive.

263
00:16:00,960 --> 00:16:03,070
 So it doesn't mean people have to say, "Hey, I want to

264
00:16:03,070 --> 00:16:04,400
 learn about meditation."

265
00:16:04,400 --> 00:16:15,740
 But if someone's suffering and looking for a cure, offering

266
00:16:15,740 --> 00:16:22,000
 Buddhism as a cure is useful.

267
00:16:22,000 --> 00:16:23,000
 But that's a good question.

268
00:16:23,000 --> 00:16:24,000
 It's true.

269
00:16:24,000 --> 00:16:27,060
 If you're surrounded by people who are uninterested, well,

270
00:16:27,060 --> 00:16:29,200
 many of us find ourselves in that position,

271
00:16:29,200 --> 00:16:34,960
 that I think there's probably something to be said for,

272
00:16:34,960 --> 00:16:36,720
 well, it would be a reason for

273
00:16:36,720 --> 00:16:40,320
 moving to a place where there's more people interested,

274
00:16:40,320 --> 00:16:41,440
 surrounding yourself with people

275
00:16:41,440 --> 00:16:46,720
 who are interested, putting yourself in a position where

276
00:16:46,720 --> 00:16:47,640
 you're with people who are

277
00:16:47,640 --> 00:16:48,640
 interested.

278
00:16:48,640 --> 00:16:51,730
 But, you know, another argument could be because monks

279
00:16:51,730 --> 00:16:53,800
 sometimes find themselves alone.

280
00:16:53,800 --> 00:16:58,000
 It doesn't mean it's important.

281
00:16:58,000 --> 00:17:00,720
 It doesn't mean it's an intrinsic part of our practice.

282
00:17:00,720 --> 00:17:04,520
 In fact, that's something I should mention is that Dhanan

283
00:17:04,520 --> 00:17:06,680
 generosity isn't an intrinsic

284
00:17:06,680 --> 00:17:07,680
 part of the path.

285
00:17:07,680 --> 00:17:11,160
 It's not necessary.

286
00:17:11,160 --> 00:17:13,740
 So this is more like if you're going to give a gift, which

287
00:17:13,740 --> 00:17:15,920
 sort of gift should you give?

288
00:17:15,920 --> 00:17:18,360
 What sort of gift is better?

289
00:17:18,360 --> 00:17:22,320
 Because often we are in a position where we can give, can

290
00:17:22,320 --> 00:17:23,560
 help someone.

291
00:17:23,560 --> 00:17:27,280
 Someone's upset, we're giving them a box of chocolates is

292
00:17:27,280 --> 00:17:28,440
 probably not.

293
00:17:28,440 --> 00:17:30,680
 I'm giving them a candy or something.

294
00:17:30,680 --> 00:17:34,280
 It's probably not the best answer.

295
00:17:34,280 --> 00:17:43,160
 So you don't have to give gifts.

296
00:17:43,160 --> 00:17:48,210
 If there's truly no one interested or receptive, then well,

297
00:17:48,210 --> 00:17:50,880
 it's like if there's no one hungry,

298
00:17:50,880 --> 00:17:56,520
 well, don't go around giving people food if they're not

299
00:17:56,520 --> 00:17:57,680
 hungry.

300
00:17:57,680 --> 00:18:00,660
 Why are some intelligent people not wise, but wise people

301
00:18:00,660 --> 00:18:02,480
 seem to be always intelligent?

302
00:18:02,480 --> 00:18:06,880
 I don't know that that's necessarily true.

303
00:18:06,880 --> 00:18:10,090
 I mean, wisdom in Buddhism is involved with mindfulness and

304
00:18:10,090 --> 00:18:11,840
 that creates a certain clarity

305
00:18:11,840 --> 00:18:13,840
 of mind.

306
00:18:13,840 --> 00:18:22,070
 But like there was this teacher in Israel and he's not very

307
00:18:22,070 --> 00:18:25,600
 intelligent, I don't think.

308
00:18:25,600 --> 00:18:27,760
 I mean, he's always said that.

309
00:18:27,760 --> 00:18:34,610
 He's a Buddhist monk in my tradition, ordained under the

310
00:18:34,610 --> 00:18:36,720
 same teacher.

311
00:18:36,720 --> 00:18:40,040
 But he said his memory is very poor.

312
00:18:40,040 --> 00:18:43,840
 And I think it is from what I've seen.

313
00:18:43,840 --> 00:18:46,040
 I memorized the Patimoka and he just shook his head.

314
00:18:46,040 --> 00:18:49,280
 He said, "I don't know, I don't know how you do that."

315
00:18:49,280 --> 00:18:58,400
 But he's a very powerful presence and very pure.

316
00:18:58,400 --> 00:19:00,160
 So it's not necessarily the case.

317
00:19:00,160 --> 00:19:04,440
 Certain aspects of intelligent aren't necessary.

318
00:19:04,440 --> 00:19:14,110
 But don't let anyone fool you into thinking, into

319
00:19:14,110 --> 00:19:20,000
 dismissing their lack of clarity of mind

320
00:19:20,000 --> 00:19:22,520
 as being unimportant.

321
00:19:22,520 --> 00:19:27,140
 If someone's mind is not clear, there's a clarity that's

322
00:19:27,140 --> 00:19:29,440
 associated with wisdom.

323
00:19:29,440 --> 00:19:34,420
 So like there was this monk in the Visuddhi Maga talks

324
00:19:34,420 --> 00:19:46,520
 about this monk who couldn't remember,

325
00:19:46,520 --> 00:19:50,370
 who remembered, who had studied the Majimani Kaya and then

326
00:19:50,370 --> 00:19:52,680
 left it alone for 20 years and

327
00:19:52,680 --> 00:19:56,120
 hadn't memorized the Majimani Kaya.

328
00:19:56,120 --> 00:19:57,720
 And for 20 years he hadn't gone over it.

329
00:19:57,720 --> 00:20:01,440
 And some students came to see him and because he had become

330
00:20:01,440 --> 00:20:03,720
 enlightened he was able to remember

331
00:20:03,720 --> 00:20:08,780
 it all and he was able to recite it back to them or explain

332
00:20:08,780 --> 00:20:10,040
 it to them.

333
00:20:10,040 --> 00:20:18,240
 So the power of meditation and enlightenment is definitely

334
00:20:18,240 --> 00:20:22,720
 supportive of intelligence.

335
00:20:22,720 --> 00:20:32,480
 There is the story of Jula Pantaka who couldn't remember a

336
00:20:32,480 --> 00:20:35,600
 single stanza.

337
00:20:35,600 --> 00:20:38,730
 But then he became an Arahant and I'm pretty sure after he

338
00:20:38,730 --> 00:20:40,400
 became an Arahant the Buddha

339
00:20:40,400 --> 00:20:42,580
 asked him to give a talk and everyone was like, "Well, he

340
00:20:42,580 --> 00:20:44,360
 couldn't even remember a stanza."

341
00:20:44,360 --> 00:20:47,090
 But he was, because he was an Arahant, he was able to teach

342
00:20:47,090 --> 00:20:50,160
 all of the Buddha's teaching.

343
00:20:50,160 --> 00:20:56,840
 He was quite wise.

344
00:20:56,840 --> 00:21:02,950
 But I would say probably some, you may be able to argue

345
00:21:02,950 --> 00:21:07,360
 that certain aspects of intelligence

346
00:21:07,360 --> 00:21:12,360
 don't have anything to do with wisdom.

347
00:21:12,360 --> 00:21:18,400
 So why are some people who are intelligent not wise?

348
00:21:18,400 --> 00:21:22,520
 I would argue that there's some basis for that.

349
00:21:22,520 --> 00:21:27,180
 A person who is more intelligent has some wholesome base

350
00:21:27,180 --> 00:21:29,520
 which may not be associated

351
00:21:29,520 --> 00:21:30,920
 with wisdom.

352
00:21:30,920 --> 00:21:35,910
 You see, intelligence is considered to be a wholesome

353
00:21:35,910 --> 00:21:36,960
 result.

354
00:21:36,960 --> 00:21:41,180
 So wisdom is a wholesome karma so therefore it brings about

355
00:21:41,180 --> 00:21:42,920
 that sort of result.

356
00:21:42,920 --> 00:21:48,560
 But intelligence can also be a result of worldly karma,

357
00:21:48,560 --> 00:21:51,880
 potentially not even wisdom.

358
00:21:51,880 --> 00:21:55,480
 Because goodness can be done with wisdom or without wisdom.

359
00:21:55,480 --> 00:22:02,520
 Jnana sampayuta and jnana vipayuta.

360
00:22:02,520 --> 00:22:06,330
 I'd probably guess that intelligence is more on the wisdom

361
00:22:06,330 --> 00:22:06,960
 side.

362
00:22:06,960 --> 00:22:09,280
 I mean, they're of course closely related.

363
00:22:09,280 --> 00:22:12,360
 Who's to say what is intelligence and what is wisdom?

364
00:22:12,360 --> 00:22:13,360
 They're just words.

365
00:22:13,360 --> 00:22:18,480
 But as far as having a good memory and so on, may not be

366
00:22:18,480 --> 00:22:21,240
 associated with wisdom.

367
00:22:21,240 --> 00:22:24,760
 But probably more so, more likely associated with wisdom.

368
00:22:24,760 --> 00:22:27,760
 But at any rate, it's a result.

369
00:22:27,760 --> 00:22:32,720
 So a person may not keep up the karma.

370
00:22:32,720 --> 00:22:36,030
 As with everything, a person can be rich because of their

371
00:22:36,030 --> 00:22:38,180
 past generosity in the past life.

372
00:22:38,180 --> 00:22:40,180
 But then they're no longer generous.

373
00:22:40,180 --> 00:22:43,850
 Because they're rich, they're no longer generous, so they

374
00:22:43,850 --> 00:22:44,880
 give that up.

375
00:22:44,880 --> 00:22:49,280
 Our goodness, our states of goodness change.

376
00:22:49,280 --> 00:22:53,080
 But probably most intelligent people were wise at some

377
00:22:53,080 --> 00:22:53,800
 point.

378
00:22:53,800 --> 00:22:54,800
 Maybe not.

379
00:22:54,800 --> 00:22:55,800
 Probably.

380
00:22:55,800 --> 00:22:58,040
 But it's easy for that to change.

381
00:22:58,040 --> 00:23:10,540
 The only thing that doesn't change is insight into nirvana,

382
00:23:10,540 --> 00:23:17,480
 which breaks through samsara.

383
00:23:17,480 --> 00:23:18,480
 How was he suddenly?

384
00:23:18,480 --> 00:23:21,030
 Well, he wasn't maybe able to teach the whole teaching, but

385
00:23:21,030 --> 00:23:22,760
 he was able to give a very profound

386
00:23:22,760 --> 00:23:26,680
 talk because he was an arahant.

387
00:23:26,680 --> 00:23:30,440
 He understood the truth.

388
00:23:30,440 --> 00:23:31,920
 But no, maybe not.

389
00:23:31,920 --> 00:23:34,840
 But probably a great amount of it.

390
00:23:34,840 --> 00:23:39,110
 And moreover, I think the idea is that suddenly his memory

391
00:23:39,110 --> 00:23:40,160
 improved.

392
00:23:40,160 --> 00:23:43,640
 Suddenly his memory cleared out.

393
00:23:43,640 --> 00:23:48,720
 So I'm not convinced about this Israeli monk who, it sounds

394
00:23:48,720 --> 00:23:51,120
 reasonable, but it also could

395
00:23:51,120 --> 00:23:53,870
 be argued that the more you meditate, the more clear your

396
00:23:53,870 --> 00:23:55,460
 mind becomes, the better your

397
00:23:55,460 --> 00:23:56,460
 memory gets.

398
00:23:56,460 --> 00:24:00,750
 So like Ajahn Tong's memory, he forgets things and he mixes

399
00:24:00,750 --> 00:24:01,760
 things up.

400
00:24:01,760 --> 00:24:06,120
 And he has for a long time because he's tired mostly.

401
00:24:06,120 --> 00:24:09,840
 That's the other thing, is your brain, the quality of your

402
00:24:09,840 --> 00:24:15,000
 brain, the state of your brain.

403
00:24:15,000 --> 00:24:17,720
 But his memory, the things he remembers, suddenly he just

404
00:24:17,720 --> 00:24:19,480
 pulls something out and he mixes it

405
00:24:19,480 --> 00:24:25,220
 up sometimes, but always pulling out this or that from his

406
00:24:25,220 --> 00:24:26,120
 mind.

407
00:24:26,120 --> 00:24:31,040
 There's such a clarity.

408
00:24:31,040 --> 00:24:33,770
 So imagine, Jhula Pantaka had heard all the Buddha's

409
00:24:33,770 --> 00:24:35,760
 teaching but just couldn't remember

410
00:24:35,760 --> 00:24:36,760
 it.

411
00:24:36,760 --> 00:24:38,360
 And actually it was because of bad karma.

412
00:24:38,360 --> 00:24:44,990
 He had called some a bhichekha Buddha, I think, an imbecile

413
00:24:44,990 --> 00:24:48,320
 or something, or a Buddha.

414
00:24:48,320 --> 00:24:51,720
 It doesn't look very bad.

415
00:24:51,720 --> 00:24:55,600
 I know there was a monk who was at a stutter, I think, and

416
00:24:55,600 --> 00:24:56,800
 he made fun of it.

417
00:24:56,800 --> 00:24:59,640
 A monk who couldn't remember anything, I think, and he made

418
00:24:59,640 --> 00:25:01,280
 fun of this monk or something.

419
00:25:01,280 --> 00:25:03,280
 I can't remember.

420
00:25:03,280 --> 00:25:04,280
 See?

421
00:25:04,280 --> 00:25:05,280
 Remember it.

422
00:25:05,280 --> 00:25:06,280
 It's a funny thing.

423
00:25:06,280 --> 00:25:07,280
 I have to look it up.

424
00:25:07,280 --> 00:25:21,160
 There is a connection.

425
00:25:21,160 --> 00:25:32,880
 Left brain, right brain, I've heard that as well.

426
00:25:32,880 --> 00:25:39,600
 It's interesting.

427
00:25:39,600 --> 00:25:41,440
 I think it's more complicated than that.

428
00:25:41,440 --> 00:25:44,190
 I mean, that's a generalization, but there are centers in

429
00:25:44,190 --> 00:25:44,880
 the brain.

430
00:25:44,880 --> 00:25:48,000
 I was reading a couple of books about that, how different

431
00:25:48,000 --> 00:25:49,640
 centers in the brain are used

432
00:25:49,640 --> 00:25:53,200
 for different qualities.

433
00:25:53,200 --> 00:25:56,320
 Like there's this, the emotional, what is it called?

434
00:25:56,320 --> 00:25:59,760
 This book, we have it here, the emotional life of your

435
00:25:59,760 --> 00:26:01,840
 brain or something like that.

436
00:26:01,840 --> 00:26:02,840
 It's quite interesting.

437
00:26:02,840 --> 00:26:09,760
 It talks about the different spheres or different parts of

438
00:26:09,760 --> 00:26:11,360
 the brain.

439
00:26:11,360 --> 00:26:19,040
 Any more questions?

440
00:26:19,040 --> 00:26:44,680
 I'll then all say good night.

441
00:26:44,680 --> 00:26:50,560
 Good night, everyone.

442
00:26:50,560 --> 00:26:51,560
 Good night.

443
00:26:51,560 --> 00:26:51,560
 Good night.

444
00:26:51,560 --> 00:26:52,560
 Good night.

445
00:26:52,560 --> 00:26:53,560
 Good night.

