 So tonight is the full moon.
 And on the one hand you want to say it's just another day.
 As we have in the Pali saying,
 Sunakatang,
 Mohutitang, Sukhanosh, Sumahutto,
 Chah, Suyutang, Brahmacharya,
 Supatakina,
 Kamaan, Kamaan, Patakina, Patakina, Manokam,
 Muniti, Patakini.
 Whenever we live, whatever moment,
 we are living the holy life or the spiritual life.
 And we perform wholesome deeds of thought, wholesome deeds
 of speech, wholesome deeds of body.
 This is an auspicious day, this is an auspicious,
 auspicious day, auspicious month, auspicious day, ausp
icious moment.
 This is an auspicious instant, that instant is auspicious.
 We remember this and we remind ourselves that it's not the
 time or the place or the environment,
 but it's the actions and the speech and the deeds, the
 actions, speech and thoughts that we perform.
 This is what makes something auspicious, this is what makes
 something special.
 [silence]
 But on the other hand, you do kind of have to,
 have to admit that this time and this place and this
 occasion with this environment,
 that we're very lucky to be here and this is a very ausp
icious time for us.
 My teacher, when New Year came around, next month is the
 Thai New Year.
 And the whole country will go crazy, literally.
 I don't know the whole country, but many parts of it.
 We take the bus and see people getting in fist fights on
 the street, walking drunk in the middle of traffic,
 throwing water and ice cubes and all sorts of things at
 each other and cars as they went by and motorcycles as they
 went by.
 I always used to tell my students about this in Thailand,
 how they, when people talk about having a Happy New Year
 and they're always wishing Happy New Year to each other.
 But in Thailand anyway, and I think in other countries it's
 very much the same.
 December 31st is the most dangerous day of the year.
 Most fatalities of the whole year, most unnatural deaths in
 the country.
 The second most, the first most is the period of the Thai
 New Year in April.
 So he would explain how this is one way of rejoicing.
 This is one way of celebrating.
 When something good comes up, this is one way of being
 happy about things.
 A lot learn, so they not learn, they rejoice or they
 festive, they celebrate.
 Then he asks, "How should we celebrate as Buddhists? And
 what should we celebrate?
 What should we be happy about? Why should we rejoice?"
 It's a good question really, as we think sometimes.
 Everybody else gets all the fun.
 People in the world, they have good food to eat, they have
 nice clothes and houses and cars.
 So many wonderful things that they can enjoy.
 They can go where they want, they can live where they want.
 We have the picture of when you are a dainous monk, you
 live in the perfect hut, on the perfect mountain with the
 perfect sunset, the perfect forest.
 And then when you actually come to the monastery, you see
 the food is no good, the kuttis have leeks, the forests
 have leeches, snakes, scorpions, and mosquitoes.
 And the water is no good, nothing is ideal.
 And you think here I have to live off of this cold food,
 contaminated water, mosquitoes and snakes and scorpions and
 leeches, leaking roofs and so on, termites.
 You think why, why?
 It's not much to celebrate.
 Of course we don't think this and hopefully we are all very
 happy.
 If you are not, listen up.
 Because there are many things for us to be happy.
 We can use tonight as an example.
 Here we are under the full moon.
 The only thing between us and the full moon is the boli
 tree.
 This boli tree is a descendant from the tree under which
 the Buddha himself sat.
 In ancient times they brought a branch from Bodhgaya to Sri
 Lanka and planted it in Anuradhapura.
 And the tree in Anuradhapura has since been cultivated and
 brought to many places around the country.
 One branch came here.
 So this tree is a descendant from the original tree.
 And here we are in a Buddhist monastery in a Buddhist
 country,
 listening to a talk on Buddhism about the practice Buddhist
 meditation,
 living a life as a Buddhist meditator,
 striving to find the truth of life, freedom from suffering,
 striving to make ourselves better people, or to purify our
 minds,
 to cleanse our minds of all the family.
 This in and of itself is something very rare in the world,
 something very much worth rejoicing.
 I think maybe sometimes we don't realize how lucky we are.
 We should rejoice in that.
 Just this. Because why are we lucky? What makes us lucky?
 The first thing that makes us lucky is that we are born in
 a time when the Buddha is,
 the Buddha's teaching is here.
 The Buddha's aren't people that come to the world every
 three or four days.
 The Buddha didn't work for just a week or so to become a
 Buddha.
 You can look around and you can ask if there is anybody
 else since the time of the Buddha,
 2,500 years.
 But they say even 2,500 years is not the time that it takes
 to become a Buddha.
 Four uncountable eons and 100,000 epochs or countable eons.
 Like the time it takes for the Big Bang,
 from the Big Bang to whatever happens, the Big Crunch or
 whatever the end of the universe is.
 There's 100,000 of those. But that's the small part.
 The big part is four uncountable eons.
 From the time of the Big Bang to the Big Crunch you can
 count, apparently.
 But you can't count uncountable.
 So here we are, here we are in the time of the Buddha.
 We've missed the Buddha himself, but I'm not sure what we
 were doing when he was teaching.
 Maybe we were drinking or gambling or maybe we weren't even
 humans.
 Somehow we missed the chant.
 So all that's left for us now is the Dhamma.
 This is why we protect the Dhamma and we revere the Dhamma
 as well because it's all that we have left.
 The Dhamma and the Sangha.
 We have teachers, we have our, the people who have passed
 on the Buddha's teaching.
 They've been an example to us in our practice.
 And then we have the teaching itself. We still have this.
 And when it's gone, when it's gone it'll just be dark water
.
 Now we have an island slowly sinking into the ocean.
 And when the ocean is gone, when the island is gone, there
 will be no refuge for being.
 It'll be like drowning and drowning in water with no sign
 of shore.
 Floating around amongst the sharks and the dangers and the
 storms of the ocean.
 And darkness.
 So here we are, here we are, we have the island, we're on
 an island.
 We're on the island of the Dhamma, the Buddha's teaching.
 We have this teaching with us now and something we can put
 into practice.
 This is something that's very lucky for all people now in
 the world.
 For all beings now, to be and to be, this is an auspicious
 time.
 And we still have the Dhamma.
 The second thing that we should rejoice in and be happy
 about
 is that not only were we born in the time of the Buddha,
 but we've also been born a human being.
 And we've been born a human being who has all of our arms
 and all of our legs
 and all of our body parts, including our brain, working in
 fairly good order.
 Good enough to be able to walk, good enough to be able to
 sit,
 good enough to be able to think and to read and to study,
 good enough to practice.
 If you're born a dog or a cat or a pig or an insect or a
 snake, scorpion.
 I think it goes without saying that there's not much
 benefit that can come from your life.
 I don't know what we did. We must have done something right
.
 We were wandering about in the ocean with some sara and
 somehow the Buddha said,
 "Somehow we managed to be born a human being."
 The Buddha made a comparison with a turtle.
 If there's a turtle that lives at the bottom of the ocean,
 and every 100 years it has to come up for air, then someone
 were to throw a yoke.
 You know, these yokes, these things that they put, this
 piece of wood that they put on the,
 it has a bend in it to put around the neck of the ox.
 Or maybe it's a ring or something. They put on the ox.
 The Buddha said that every 100 years coming up,
 in the middle, in the whole of the ocean there's one yoke
 floating.
 The Buddha said it's more likely that that turtle when he
 comes up after 100 years
 will come up with his neck in the yoke than it is for a
 being to be born a,
 for an animal to be born a human being.
 Because as an animal there's not much you can do to
 cultivate your mind.
 As I said, mostly it's kill or be killed.
 The opportunity is to practice morality, we develop
 concentration and wisdom.
 Somehow floating around in the ocean we managed to put our
 neck up in the yoke.
 Here we were born a human being. We don't know how we got
 here.
 I don't think any of us know how or we did that we became
 human beings.
 Maybe if you have special knowledge then you know what you
 did.
 Mostly we don't have a clue, we're just happy to be here.
 We should be happy to be here.
 We've met the Buddha and we've met the Buddha as a human
 being.
 We're human.
 And this body is capable how long have we been alive and
 yet we've managed to avoid death.
 We've managed to avoid dismemberment with all the stupid
 things that we've done,
 all the crazy things that we've done.
 We've still managed to avoid death and dismemberment.
 We've managed to make it through school and job and make
 enough money to live our lives in a way
 that allows us to make our way here to this place.
 So these things are very difficult to find the Buddha.
 It's a lulabu.
 Kichang Buddha Namukpa dang. The arising of the Buddha is a
 difficult thing to find.
 Kichang Machana Ji Vitaang. Kichang Kicho Manusapati Lamo.
 Attaining of human life is difficult to find, rare to find.
 Difficult to find is the life that is free from illness,
 free from difficulties, free from obstacles
 that allow you to come all the way across the world from
 all the parts of the world.
 Here we have people from all over the world coming.
 Very few people have this opportunity.
 The third thing that makes us lucky is that we actually
 want to take the opportunity.
 Because of all the people in the world who don't have the
 opportunity,
 very few are the people who have the opportunity.
 But among those people who have the opportunity, even fewer
 are those people who want to take the opportunity.
 How many people, when you told them you were coming to med
itate,
 put confused looks on their face or gave you strange looks
 or ridiculed you or whatever,
 and when you decided you want to ordain, looked at you like
 you're crazy,
 or just abandoned you completely?
 How many people have you heard say, "Monks are useless,
 "monks are, meditators are wasting their time, selfish," so
 on?
 How many people have the opportunity but would never in a
 million years think about coming to a meditation course,
 let alone become a monk, let alone dedicate their lives to
 it?
 That's something worth rejoicing for sure, worth rejoicing
 about.
 How lucky we are to just have this mind that says, "That's
 not us, no?
 "That's not like we chose to have this mind," but suddenly
 our mind says,
 "I want to become a monk," or, "I want to go meditate."
 It's kind of funny really because it's not like we chose to
 have that mind.
 It's not like other people chose to have the mind that didn
't want to.
 We cultivated this.
 Somehow we managed to get something right. We should feel
 happy about that.
 We should rejoice in it.
 How wonderful that I have this wonderful mind that wants to
 meditate,
 that wants to devote my life to the Buddha's teaching.
 We maybe don't want to feel proud about it, but we can at
 least feel proud for a second, no?
 Give us some encouragement. Pride can be useful as
 encouragement.
 You have to take it only to the step of encouragement.
 When you need encouragement, you can think of that.
 I'm not a horrible person. I'm not a useless person.
 I may not be the best meditator, but at least I want to med
itate.
 At least I do meditate.
 So this is the third blessing that we have that we should
 be happy about.
 Rejoice.
 The fourth thing is that we actually take the opportunity.
 So this is what the Buddha said. He said,
 "Of all the people in the world, rare are those who
 understand
 in regards to that which needs to be done, that it needs to
 be done.
 Understand that something needs to be done.
 That we can't live like this. We can't be negligent
 and just assume that when we die we're going to go to a
 good place
 or nothing bad is going to happen to us."
 Actually, we're like walking a tightrope.
 And any moment we could snip off and fall to whatever it is
 that lies below.
 Being a human being is like walking a tightrope.
 If you were up here when the lightning struck two nights
 ago,
 it's quite an experience for me.
 Realizing how thin is the rope on which we're walking.
 Any moment, any moment, something could knock you off.
 And if you haven't prepared yourself,
 and done anything useful in your life, how will you be
 ready for it?
 And die without any, under any clarity of mind, any
 presence, any awareness.
 And die with a muddled, confused mind.
 So, some people, many people have no idea.
 Some people see this.
 Some people see this but then they do nothing about it.
 They think, "Yeah, it would be great if I could do some
 meditation
 or something spiritual anyway, but they don't do it."
 So he said, "Among those rare people who have that good
 intention
 or the understanding, very, very much more rare
 are the people who actually do something about it."
 So here we've accumulated quite a number of blessings in
 our lives
 by being in this one spot at this time.
 We've got the blessing of the Buddha and his teachings.
 We know what are the teachings of the Buddha.
 We know the Four Satipatahana, we know Vipassana,
 we know the Four Noble Truths, we know the Eightfold Noble
 Path.
 These aren't just public schools.
 These aren't the kind of things that anyone can teach you.
 We take them for granted sometimes, "Ya, ya, I know them."
 Even just the Four Foundations of Mindfulness is enough to
 get you to Nibbana,
 enough to free you from the entire web of samsara, all
 suffering.
 Just the Four Foundations of Mindfulness.
 "Eka yano ayang gikore mago"
 This path is the straight path, the one way.
 Just the Four Foundations of Mindfulness.
 And we have the ability to understand the teaching.
 We're human, we have the ability to practice the teaching.
 We can walk, we can sit, we can think, our mind works, our
 body works.
 We have the good intention to practice, we have confidence.
 Just the knowledge that the Buddha's teaching was a good
 thing.
 Many people can't even come to this, either they repudiate,
 repudiate, repudiate.
 The Buddha's teaching, or they doubt about it.
 Even if they think it might be good, they doubt.
 And they have so much doubt that they can't practice.
 Here we don't have any of that.
 We have enough confidence to allow us to practice.
 We have enough confidence that we decided to come to ordain
.
 That we've ordained.
 Enough confidence to fly across the world, to stay in a hut
 in a forest.
 How rare is that?
 We must have done something right to get here.
 And the fact that we've actually done it and succeeded, we
 made it.
 We're not there yet, but physically we've made it.
 We're here. We're where we should be. We're where we wanted
 to be.
 We're in a place where so many people have never had the
 chance to be.
 I consider this to be very lucky.
 I think we all should consider ourselves quite lucky.
 Consider this an auspicious time, an auspicious moment.
 We're developing good wholesome qualities of mind, wholes
ome qualities of speech, wholesome qualities of body, of
 action.
 It's quite fitting that this is what we're doing on a holy
 day.
 I always used to joke at D
 So today is a holiday.
 No, it doesn't mean you get a day off.
 It means you get to be holy.
 Buddhism, we take the holiness of the day on ourselves.
 We don't leave the holiness with the day.
 We take it to mean something for ourselves.
 That the holy day is the time for us to become holy, for us
 to increase our holiness.
 As in Buddhism, we don't look for someone else to be holy
 for us.
 We take the responsibility on ourselves.
 It's not that we don't believe in God or gods or angels or
 so on.
 But we figure they've got enough work to do for themselves.
 So rather than asking them or praying to them, we'll do it
 for ourselves.
 The holiness isn't for the day, it isn't for God or an
 angel.
 The holiness of the day is for ourselves.
 This is a Buddhist holiday.
 Buddhist holy day.
 It's not a day off.
 So this is what we should feel lucky about.
 This is what we should rejoice in.
 So how do we rejoice as Buddhists?
 I think there's only one answer to the best way to rejoice.
 The best way to rejoice is to practice.
 Practice to purify our minds.
 I think it's kind of difficult really, because the
 equivalent,
 the equivalent in a physical sense is on the holiday or on
 the festival or so on.
 The best way to rejoice, someone telling you the best way
 to rejoice is to go home
 and clean up your bathroom or clean up your kitchen.
 That doesn't seem like the best way to use the holy day.
 But when you do have the day off, when you do have the time
 off,
 when you do have the time to spare, this is when people do
 their house cleaning.
 So even if this were a day off, this would be the best use
 that we could have for it.
 But there's another way we can understand this, the
 practicing,
 because the practice is like an exaltation of ourselves.
 How difficult is it to even want to meditate, let alone to
 meditate?
 It's a way of rejoicing.
 Meditation itself is a way of rejoicing.
 It's taking a victory lap.
 It's like boasting to the whole universe.
 It's like proclaiming to the whole universe that we've won
 and we're victorious.
 We are meditating in spite of all the difficulty, in spite
 of all the
 improbability of us getting to this point. We made it.
 So our walking meditation or sitting meditation is actually
 a rejoicing.
 Sometimes it doesn't feel like it, but it's victory.
 It's the winning.
 This is the path to victory, the action that makes us
 victorious.
 And it's the act that purifies our minds, and so it's the
 act that brings us happiness.
 Without purity of mind there's no happiness.
 So this act of meditating is that which brings us true
 happiness.
 And so as a result it should be something that is happy for
 us.
 This is, I think, obviously, it goes without saying how
 important on a Buddhist holiday it is to meditate,
 but this is just one more way for us to think about it.
 That was the question today, "Are we doing anything special
 on a Buddhist holiday?"
 And I don't think we meditate more.
 But it's true, no? This is the time when people will
 practice more intensely.
 This is when the lay people come to the monastery and take
 the precepts.
 This is the time when people undertake the practice of
 morality, concentration and wisdom,
 because it's an excuse or it's a good opportunity for them.
 So it's an even better opportunity for us who are living
 here and who have the chance to take the opportunity
 to make it as a joyful occasion for ourselves.
 Here I have the opportunity to meditate and we show it, we
 make it clear when you're on the holiday.
 The holy day we practice with greater intention.
 It doesn't mean you even have to practice longer periods of
 time,
 more to be mindful, knowing that this is the holy day,
 knowing that there's the full moon and we're under the Bod
hi tree,
 thinking about the Buddha who sat under the Bodhi tree.
 We're 20 feet away from the Buddha, because we have the Bod
hi tree here under which the Buddha sat.
 We can also take it as a pujja for the Buddha, a way of
 paying homage to the Buddha as well.
 We wonder what should we do on the holy day to pay homage
 to the Buddha.
 The Buddha himself was very clear on his topic when he was
 passing away and they were bringing flowers
 and paying homage to him. The Buddha said, "This isn't how
 you pay homage to a Buddha."
 He said, "Whoever practices yoko, ananda, bhikkhu, bhikkhu
 niva, upasakhu, upasika, dhamma, nudhamma, paktipanno,
 anudhamma jadi, sodhata, kalukaroti, sakaroti, kalukaroti,
 manniti, pujeti, parama yaputai.
 Whether it's a bhikkhu or a bhikkhu nahi or an upasika, up
asika, upasika.
 When they practice according to the teaching, realize the
 teaching for themselves.
 This is a person who pays proper homage, pays proper
 respect, pays homage to the Buddha with the highest form of
 homage.
 "Pati pati bhansya." So every step that we take, this is
 like offering a flower to the Buddha.
 It's the highest form of offering that you can give to the
 Buddha, the Buddha's teaching,
 to our monastery, to our meditation center, to the world.
 That one step that you take with mindfulness.
 Clearly aware this is what it is, with morality,
 concentration and wisdom, understanding.
 So there's a pep talk for today, a Buddhist holiday.
 And now on with the show. There's still a mindful
 frustration. Then walking and then sitting.
