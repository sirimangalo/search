1
00:00:00,000 --> 00:00:10,920
 Welcome everyone to our daily meditation session. Please

2
00:00:10,920 --> 00:00:12,000
 sit in the chair. It's fine.

3
00:00:12,000 --> 00:00:18,000
 Oh no, don't worry about it. Sit comfortably.

4
00:00:18,000 --> 00:00:27,000
 Tonight we have, this is our largest audience yet I think.

5
00:00:27,000 --> 00:00:32,200
 There are seven of us here and we've got five people

6
00:00:32,200 --> 00:00:35,000
 watching from the internet.

7
00:00:35,000 --> 00:00:39,000
 They're a live broadcast.

8
00:00:39,000 --> 00:00:43,000
 So tonight is the English session.

9
00:00:43,000 --> 00:00:51,000
 I've been, I missed most of the Thai sessions lately.

10
00:00:51,000 --> 00:01:04,000
 But we'll try to get up so that we're here every day.

11
00:01:04,000 --> 00:01:12,000
 Tonight I wanted to talk about dealing with the truth.

12
00:01:12,000 --> 00:01:15,000
 Because Buddhism deals specifically with the truth.

13
00:01:15,000 --> 00:01:18,680
 I'm just going to talk here. You're welcome to close your

14
00:01:18,680 --> 00:01:20,000
 eyes and meditate.

15
00:01:20,000 --> 00:01:24,250
 Normally when I talk I close my eyes. I'm not trying to be

16
00:01:24,250 --> 00:01:26,000
 entertaining.

17
00:01:26,000 --> 00:01:32,000
 I'm trying to be meditative. So even when I talk I try to

18
00:01:32,000 --> 00:01:39,000
 stay in touch with the meditation practice.

19
00:01:39,000 --> 00:01:41,570
 When you listen you should also try to stay in touch with

20
00:01:41,570 --> 00:01:43,000
 the meditation practice.

21
00:01:43,000 --> 00:01:52,000
 You can use it as an opportunity to practice yourself.

22
00:01:52,000 --> 00:01:55,000
 So Buddhism deals specifically with the truth.

23
00:01:55,000 --> 00:02:05,220
 It's a very, it's the core issue in Buddhism is the

24
00:02:05,220 --> 00:02:07,000
 realization of the truth

25
00:02:07,000 --> 00:02:17,000
 or the realization of specific truths.

26
00:02:17,000 --> 00:02:25,000
 In brief the realization of the truth of reality

27
00:02:25,000 --> 00:02:31,000
 or coming to understand the truth about reality as it is.

28
00:02:31,000 --> 00:02:37,070
 And by reality we're talking here not about theoretical

29
00:02:37,070 --> 00:02:43,000
 reality or the reality of the physicists,

30
00:02:43,000 --> 00:02:47,860
 the reality of philosophers. We're talking about experient

31
00:02:47,860 --> 00:02:49,000
ial reality.

32
00:02:49,000 --> 00:02:54,400
 The reality which you can experience. Some people call it

33
00:02:54,400 --> 00:02:57,000
 subjective reality.

34
00:02:57,000 --> 00:03:00,500
 But actually in Buddhism we're not dealing with subjective

35
00:03:00,500 --> 00:03:01,000
 reality.

36
00:03:01,000 --> 00:03:06,660
 And we deny the claim that first person experience of

37
00:03:06,660 --> 00:03:11,000
 reality is necessarily subjective.

38
00:03:11,000 --> 00:03:19,990
 Subjective here meaning particularly different for everyone

39
00:03:19,990 --> 00:03:26,000
 or unscientific.

40
00:03:26,000 --> 00:03:29,590
 Because in a sense reality for us is subjective. We are the

41
00:03:29,590 --> 00:03:34,000
 subject or there is a subject and that's the mind.

42
00:03:34,000 --> 00:03:40,180
 As opposed to looking at things objectively in terms of the

43
00:03:40,180 --> 00:03:46,000
 object or in terms of a third person perspective.

44
00:03:46,000 --> 00:03:49,800
 But it's not subjective in the sense that my reality is not

45
00:03:49,800 --> 00:03:52,000
 different from your reality.

46
00:03:52,000 --> 00:03:56,770
 It is the truth that we try to come to realize. It's an

47
00:03:56,770 --> 00:04:02,000
 objective truth about subjective reality.

48
00:04:02,000 --> 00:04:06,230
 So a scientist would look at meditation and say you can't

49
00:04:06,230 --> 00:04:11,000
 get truth from that. It's subjective.

50
00:04:11,000 --> 00:04:14,220
 How do you know you're seeing the things you see? How do

51
00:04:14,220 --> 00:04:16,000
 you know you're hearing the things you hear?

52
00:04:16,000 --> 00:04:24,000
 How do you know it's real? It's subjective.

53
00:04:24,000 --> 00:04:31,000
 There's no way of telling whether it's real or not.

54
00:04:31,000 --> 00:04:35,560
 And that's the kind of reality that they're talking about

55
00:04:35,560 --> 00:04:39,000
 is not the reality we're talking about.

56
00:04:39,000 --> 00:04:43,930
 When we talk about reality in Buddhism we're talking about

57
00:04:43,930 --> 00:04:45,000
 experience.

58
00:04:45,000 --> 00:04:49,070
 And in that we claim that my experience is the same as your

59
00:04:49,070 --> 00:04:50,000
 experience.

60
00:04:50,000 --> 00:04:53,000
 Because no matter what you see, you still see.

61
00:04:53,000 --> 00:04:59,120
 No matter what I see it's still seeing and my seeing and

62
00:04:59,120 --> 00:05:06,000
 your seeing is the same in the sense that it's both seeing.

63
00:05:06,000 --> 00:05:10,840
 Meaning there are some essential building blocks of

64
00:05:10,840 --> 00:05:16,920
 experience which are common to all beings, the experience

65
00:05:16,920 --> 00:05:19,000
 of all beings.

66
00:05:19,000 --> 00:05:23,000
 When we break experience down into those building blocks

67
00:05:23,000 --> 00:05:26,890
 then we have an objective understanding of experiential

68
00:05:26,890 --> 00:05:28,000
 reality.

69
00:05:28,000 --> 00:05:40,000
 This is what we mean by reality.

70
00:05:40,000 --> 00:05:43,000
 So what is the truth that we're trying to realize?

71
00:05:43,000 --> 00:05:46,910
 Well, there's a lot of truth that can come from experient

72
00:05:46,910 --> 00:05:48,000
ial reality.

73
00:05:48,000 --> 00:05:54,710
 There are many truths that we're going to realize in the

74
00:05:54,710 --> 00:05:58,000
 course of the practice.

75
00:05:58,000 --> 00:06:04,480
 But in particular we're focusing on those truths that are

76
00:06:04,480 --> 00:06:07,000
 going to free our mind.

77
00:06:07,000 --> 00:06:14,870
 Those truths that are going to allow us to react

78
00:06:14,870 --> 00:06:20,000
 appropriately to reality.

79
00:06:20,000 --> 00:06:24,000
 We're not interested in what is it like to taste ice cream

80
00:06:24,000 --> 00:06:30,000
 or what is it like to see a supernova or what is it like to

81
00:06:30,000 --> 00:06:34,000
 hear an angel or so on.

82
00:06:34,000 --> 00:06:39,380
 Any mystical or magical experience which is very much a

83
00:06:39,380 --> 00:06:41,000
 part of reality.

84
00:06:41,000 --> 00:06:45,000
 We're focusing on those truths that are particularly useful

85
00:06:45,000 --> 00:06:51,970
 and have to do with the building blocks of experience, have

86
00:06:51,970 --> 00:07:03,000
 to do with our interaction with the world around us.

87
00:07:03,000 --> 00:07:15,000
 Because our experience of reality is not always pleasant.

88
00:07:15,000 --> 00:07:24,000
 We want to be happy. Everyone wants to be happy.

89
00:07:24,000 --> 00:07:29,870
 And yet among the many, many people who wish to be happy,

90
00:07:29,870 --> 00:07:35,360
 most of us are less than 50% happy, happy less than 50% of

91
00:07:35,360 --> 00:07:36,000
 the time.

92
00:07:36,000 --> 00:07:39,000
 Which means we're losing.

93
00:07:39,000 --> 00:07:46,000
 Our reality is contrary to our wishes.

94
00:07:46,000 --> 00:07:50,000
 So in brief something's wrong.

95
00:07:50,000 --> 00:07:53,570
 You can never say this is a proper state of being where we

96
00:07:53,570 --> 00:07:57,320
 want to be happy and yet we're happy less than 50% of the

97
00:07:57,320 --> 00:07:58,000
 time.

98
00:07:58,000 --> 00:08:19,000
 Some people live most of their lives unhappy.

99
00:08:19,000 --> 00:08:23,190
 Even just sitting here you can see that your experience of

100
00:08:23,190 --> 00:08:26,000
 reality is not entirely pleasant.

101
00:08:26,000 --> 00:08:31,080
 Maybe it's too hot, maybe it's too cold, maybe your head

102
00:08:31,080 --> 00:08:35,640
 hurts, maybe your back hurts, maybe your legs hurt, maybe

103
00:08:35,640 --> 00:08:40,820
 you're itching and scratching, maybe you're thinking about

104
00:08:40,820 --> 00:08:44,990
 unpleasant things, maybe you don't like what I'm eating or

105
00:08:44,990 --> 00:08:48,000
 up in the air, philosophical.

106
00:08:48,000 --> 00:08:56,590
 Doctrine or theory. We're talking about the building blocks

107
00:08:56,590 --> 00:08:58,000
 of our experience.

108
00:08:58,000 --> 00:09:02,000
 You're hot. Well heat is a building block. Heat is reality.

109
00:09:02,000 --> 00:09:04,000
 It's really hot.

110
00:09:04,000 --> 00:09:08,380
 How do you feel about the heat? You don't like it? Well

111
00:09:08,380 --> 00:09:11,000
 disliking is part of reality.

112
00:09:11,000 --> 00:09:15,830
 It's part of your reality. That's your reaction to the heat

113
00:09:15,830 --> 00:09:19,000
 or the cold or the itching or the pain.

114
00:09:19,000 --> 00:09:43,000
 [silence]

115
00:09:43,000 --> 00:09:46,730
 In the meditation we're going to see these things clearer

116
00:09:46,730 --> 00:09:51,150
 than we... we're going to see the way reality works clearer

117
00:09:51,150 --> 00:09:55,000
 than before in a way that we couldn't see it before.

118
00:09:55,000 --> 00:09:57,970
 Normally when it's... when there's pain in the body for

119
00:09:57,970 --> 00:10:02,100
 example, right away we don't like it. Right away we've made

120
00:10:02,100 --> 00:10:04,000
 up our mind it's bad.

121
00:10:04,000 --> 00:10:08,950
 And not only that we immediately take action without even

122
00:10:08,950 --> 00:10:14,250
 thinking, without even really looking at it and examining

123
00:10:14,250 --> 00:10:17,000
 it to see what is this pain?

124
00:10:17,000 --> 00:10:22,000
 Is it really bad? What's bad about it?

125
00:10:22,000 --> 00:10:27,700
 And it can get to the point where even just hearing a

126
00:10:27,700 --> 00:10:33,530
 simple sound, once we get a hatred or a dislike for a

127
00:10:33,530 --> 00:10:39,120
 phenomenon, we become so react... we can become reactionary

128
00:10:39,120 --> 00:10:40,000
 towards it.

129
00:10:40,000 --> 00:10:45,430
 Even just hearing someone's voice, when we get it in our

130
00:10:45,430 --> 00:10:51,360
 minds that this person is unwelcome or unappreciated, is an

131
00:10:51,360 --> 00:10:54,400
 unpleasant person, as soon as we hear the voice there's

132
00:10:54,400 --> 00:10:56,000
 nothing wrong with the voice.

133
00:10:56,000 --> 00:11:03,300
 It's a sound that arises in the ear. We right away get

134
00:11:03,300 --> 00:11:10,950
 angry. We've already made up our mind and we're ready to

135
00:11:10,950 --> 00:11:18,020
 block them out or do whatever we can to not have to hear

136
00:11:18,020 --> 00:11:20,000
 the sound.

137
00:11:20,000 --> 00:11:32,000
 This is our ordinary experience of reality.

138
00:11:32,000 --> 00:11:37,950
 The truth of reality that we're trying to realize, to cut

139
00:11:37,950 --> 00:11:43,650
 to the chase, to do your work for you so you don't have to

140
00:11:43,650 --> 00:11:47,440
 practice, I give you the answer. The answer is there's

141
00:11:47,440 --> 00:11:50,000
 nothing good or bad about reality.

142
00:11:50,000 --> 00:11:55,160
 The realization that we gain from meditation is that there

143
00:11:55,160 --> 00:12:01,510
's no positive or negative experience whatsoever. It's our

144
00:12:01,510 --> 00:12:18,000
 reaction to things based on an accumulated preference and

145
00:12:18,000 --> 00:12:22,770
 this habit of addiction, of needing things to be a certain

146
00:12:22,770 --> 00:12:27,480
 way of compartmentalizing reality, of thinking that somehow

147
00:12:27,480 --> 00:12:30,000
 these things are going to make us happy.

148
00:12:30,000 --> 00:12:33,980
 Somehow we're going to arrange our lives so that everything

149
00:12:33,980 --> 00:12:37,490
 that we like is going to come to us and everything we don't

150
00:12:37,490 --> 00:12:40,000
 like is going to stay away from us.

151
00:12:40,000 --> 00:12:48,850
 We compartmentalize reality. We build up these false

152
00:12:48,850 --> 00:12:58,000
 characterizations, judgments of innocent experience.

153
00:12:58,000 --> 00:13:02,230
 And it's to the point that we've stopped looking, we've

154
00:13:02,230 --> 00:13:06,910
 stopped examining, we've stopped appreciating reality for

155
00:13:06,910 --> 00:13:08,000
 what it is.

156
00:13:08,000 --> 00:13:13,050
 We already know the answer. Pain bad, okay, run. Change

157
00:13:13,050 --> 00:13:18,000
 position. Go see a doctor. Get rid of it.

158
00:13:18,000 --> 00:13:28,190
 And when we look at it, when we look at the pain, we see

159
00:13:28,190 --> 00:13:36,710
 that it's totally not what we thought. It's something

160
00:13:36,710 --> 00:13:42,000
 completely different from what we thought of it.

161
00:13:42,000 --> 00:13:46,780
 When we look at it, we watch it and we remind ourselves of

162
00:13:46,780 --> 00:13:54,780
 what it is. We teach ourselves, okay, we see this, this is

163
00:13:54,780 --> 00:13:56,000
 pain.

164
00:13:56,000 --> 00:13:59,820
 Okay, that wasn't so bad. When we look at it again, we see

165
00:13:59,820 --> 00:14:04,000
 pain. We just remind ourselves of this pain, pain, pain.

166
00:14:04,000 --> 00:14:09,980
 And as we open up to the experience, we see that there's

167
00:14:09,980 --> 00:14:18,000
 nothing negative about pain at all.

168
00:14:18,000 --> 00:14:21,610
 It's one of the most liberating experiences that come in

169
00:14:21,610 --> 00:14:25,000
 the meditation practice during the time that you're

170
00:14:25,000 --> 00:14:33,000
 practicing, is the ability to conquer the pain.

171
00:14:33,000 --> 00:14:37,160
 You can get to the point where pains that have been

172
00:14:37,160 --> 00:14:42,170
 haunting you through your whole practice for days and days

173
00:14:42,170 --> 00:14:44,000
 suddenly vanish.

174
00:14:44,000 --> 00:14:49,090
 Suddenly you conquer the pain. Your mind changes. Your mind

175
00:14:49,090 --> 00:14:52,660
 shifts. And it gives in. You know, you keep going back to

176
00:14:52,660 --> 00:14:55,000
 it. You say, you know, it's only pain.

177
00:14:55,000 --> 00:14:59,240
 Reminding yourself, it's only pain. It is what it is. It's

178
00:14:59,240 --> 00:15:03,440
 not bad, it's not good, it's not me, it's not mine. It's

179
00:15:03,440 --> 00:15:09,000
 not a problem. It's pain.

180
00:15:09,000 --> 00:15:13,670
 And if you do this enough, your mind shifts and your mind

181
00:15:13,670 --> 00:15:16,000
 says, "Oh, it's pain."

182
00:15:16,000 --> 00:15:23,320
 And it's like you've conquered an enemy. Suddenly there's

183
00:15:23,320 --> 00:15:28,000
 no fear, there's no anger or upset.

184
00:15:28,000 --> 00:15:36,200
 Towards that pain there's a confidence, there's an

185
00:15:36,200 --> 00:15:46,000
 understanding and a surety. You're certain in your mind.

186
00:15:46,000 --> 00:15:52,370
 You're not worried about it anymore. The pain comes, let it

187
00:15:52,370 --> 00:15:58,610
 come. And then it just disappears, like it was never there

188
00:15:58,610 --> 00:16:01,000
 in the first place.

189
00:16:01,000 --> 00:16:06,000
 Just as an example, all of our experience is like this.

190
00:16:06,000 --> 00:16:10,840
 Pain is a difficult one because it's ingrained in our minds

191
00:16:10,840 --> 00:16:14,790
 that pain means there's a problem. The body is trying to

192
00:16:14,790 --> 00:16:16,000
 tell us something, we say.

193
00:16:16,000 --> 00:16:19,460
 But there's so much more that we can point out that's in

194
00:16:19,460 --> 00:16:24,990
 the very same vein. When we hear things, as I said, we can

195
00:16:24,990 --> 00:16:27,000
 get so angry.

196
00:16:27,000 --> 00:16:32,230
 When children come and they're bothering us, children can

197
00:16:32,230 --> 00:16:35,000
 be very annoying sometimes.

198
00:16:35,000 --> 00:16:39,060
 But there's nothing annoying about the child, it's in our

199
00:16:39,060 --> 00:16:42,000
 own mind we get annoyed, we get angry.

200
00:16:42,000 --> 00:16:45,610
 We don't really hear the sound, maybe we're busy with

201
00:16:45,610 --> 00:16:47,000
 something else.

202
00:16:47,000 --> 00:16:52,980
 And so we've compartmentalized reality, this sound is bad,

203
00:16:52,980 --> 00:16:57,000
 this work that I'm focusing on is good.

204
00:16:57,000 --> 00:17:00,210
 And you can only accept part of reality, the part that you

205
00:17:00,210 --> 00:17:05,310
 can't accept you have to stop, you have to block, you have

206
00:17:05,310 --> 00:17:11,000
 to shout at the child or hit it or so on.

207
00:17:11,000 --> 00:17:21,000
 We work this way.

208
00:17:21,000 --> 00:17:26,120
 One of the interesting things people often, we have this ad

209
00:17:26,120 --> 00:17:30,000
age, this idiom about pain, about truth, sorry.

210
00:17:30,000 --> 00:17:34,000
 And we say truth hurts.

211
00:17:34,000 --> 00:17:37,650
 And it doesn't take much explaining to understand where

212
00:17:37,650 --> 00:17:39,000
 this comes from.

213
00:17:39,000 --> 00:17:42,000
 We do believe this, truth hurts.

214
00:17:42,000 --> 00:17:47,060
 And it's kind of like a necessary evil sometimes to tell

215
00:17:47,060 --> 00:17:49,000
 people the truth.

216
00:17:49,000 --> 00:17:52,840
 We say it would hurt someone if you said you're fat, by the

217
00:17:52,840 --> 00:17:58,390
 way you're fat, or you're ugly, or you're too tall, or this

218
00:17:58,390 --> 00:17:59,000
 or that.

219
00:17:59,000 --> 00:18:03,560
 I mean there's certain truths that we say you would never

220
00:18:03,560 --> 00:18:08,000
 say to someone, boy you're old, or so on.

221
00:18:08,000 --> 00:18:15,000
 Look at all those wrinkles on your face or whatever.

222
00:18:15,000 --> 00:18:19,000
 You look very bad with those glasses or whatever.

223
00:18:19,000 --> 00:18:24,000
 These truths, that dress looks terrible on you.

224
00:18:24,000 --> 00:18:30,000
 Sometimes we say telling the truth is painful.

225
00:18:30,000 --> 00:18:36,700
 But in all of these examples, even these examples that are

226
00:18:36,700 --> 00:18:41,230
 so taboo, and in fact it's funny because in Thailand many

227
00:18:41,230 --> 00:18:43,000
 of them are not taboo.

228
00:18:43,000 --> 00:18:47,190
 It's funny how culture defines what is right and what is

229
00:18:47,190 --> 00:18:48,000
 wrong.

230
00:18:48,000 --> 00:18:51,050
 I mean in America if you call someone fat, it's just so, I

231
00:18:51,050 --> 00:18:56,000
 don't know in America, in Canada it would be just,

232
00:18:56,000 --> 00:19:05,000
 it would be a total disgrace to call someone fat.

233
00:19:05,000 --> 00:19:08,000
 But in Thailand it's not such a big deal and they're often,

234
00:19:08,000 --> 00:19:10,000
 it's kind of funny in that way.

235
00:19:10,000 --> 00:19:14,130
 And you know in Thailand they ask women how old they are,

236
00:19:14,130 --> 00:19:18,750
 and you're supposed to tell them how old you are, things

237
00:19:18,750 --> 00:19:22,000
 like that.

238
00:19:22,000 --> 00:19:28,250
 So we build up these, in some cultures it's not wrong to be

239
00:19:28,250 --> 00:19:29,000
 large.

240
00:19:29,000 --> 00:19:33,260
 It's not such a big deal, or at least it's not a very

241
00:19:33,260 --> 00:19:35,000
 negative thing.

242
00:19:35,000 --> 00:19:42,000
 I saw these two monks in Thailand.

243
00:19:42,000 --> 00:19:46,350
 I think it was kind of going over the edge, but I think it

244
00:19:46,350 --> 00:19:47,000
 was going over the edge.

245
00:19:47,000 --> 00:19:50,960
 And especially as a Western it was kind of terrible to

246
00:19:50,960 --> 00:19:52,000
 listen to.

247
00:19:52,000 --> 00:19:54,740
 But this one monk, he was making fun of this dark skin monk

248
00:19:54,740 --> 00:19:55,000
.

249
00:19:55,000 --> 00:19:58,000
 The one monk had light skin, the other monk had dark skin.

250
00:19:58,000 --> 00:19:59,870
 And I guess in Thailand it's okay to make fun of people

251
00:19:59,870 --> 00:20:02,000
 because of their skin color.

252
00:20:02,000 --> 00:20:05,390
 Because we were up on this mountain and he said, "Oh we'll

253
00:20:05,390 --> 00:20:08,220
 leave you up here and when we come back the clouds will all

254
00:20:08,220 --> 00:20:09,000
 be dark."

255
00:20:09,000 --> 00:20:12,950
 The clouds will all be dark because your skin color rubbed

256
00:20:12,950 --> 00:20:14,000
 off on them.

257
00:20:14,000 --> 00:20:22,000
 And he was making jokes like this and that was horrifying.

258
00:20:22,000 --> 00:20:26,000
 But the point is, in some cases making fun of people I

259
00:20:26,000 --> 00:20:28,000
 think is always wrong.

260
00:20:28,000 --> 00:20:32,000
 I don't think we should ever go that way.

261
00:20:32,000 --> 00:20:35,070
 The other funny thing is the other monk didn't seem

262
00:20:35,070 --> 00:20:36,000
 offended by it.

263
00:20:36,000 --> 00:20:40,840
 Whereas if you said something like that in the West you

264
00:20:40,840 --> 00:20:43,000
 might get hit for it.

265
00:20:44,000 --> 00:20:46,790
 And so on. If you call someone a buffalo in Thailand you

266
00:20:46,790 --> 00:20:48,000
 might get hit.

267
00:20:48,000 --> 00:20:51,000
 In America people laugh at it.

268
00:20:51,000 --> 00:21:00,000
 I think a buffalo is strong, that's kind of a good thing.

269
00:21:00,000 --> 00:21:03,900
 The point is that our perception is cloud reality. We don't

270
00:21:03,900 --> 00:21:06,000
 see things for what they are.

271
00:21:06,000 --> 00:21:10,030
 My teacher said, "If someone ever calls you a buffalo just

272
00:21:10,030 --> 00:21:13,000
 turn around and see if you've got a tail.

273
00:21:13,000 --> 00:21:22,000
 If you don't have a tail you're not a buffalo."

274
00:21:22,000 --> 00:21:27,000
 And that really sums it all up.

275
00:21:27,000 --> 00:21:32,530
 There's no reason to be upset. If it's true it's true, if

276
00:21:32,530 --> 00:21:35,000
 it's false it's false.

277
00:21:37,000 --> 00:21:40,680
 And so even when someone calls you a nasty name or points

278
00:21:40,680 --> 00:21:43,000
 out even a real flaw,

279
00:21:43,000 --> 00:21:47,000
 you know, you say, "Oh, the truth hurts."

280
00:21:47,000 --> 00:21:55,000
 Sometimes people say, "Look, I hate to tell you but..."

281
00:21:55,000 --> 00:21:58,120
 And so on. So we say, "Oh, the truth hurts." But the truth

282
00:21:58,120 --> 00:21:59,000
 doesn't hurt.

283
00:21:59,000 --> 00:22:04,000
 And this is one thing I want to stress.

284
00:22:04,000 --> 00:22:08,230
 And it's important to stress I think because Buddhism is

285
00:22:08,230 --> 00:22:09,000
 probably...

286
00:22:09,000 --> 00:22:12,070
 I have to think about it, but I think Buddhism is one of

287
00:22:12,070 --> 00:22:15,000
 the most boring religions out there.

288
00:22:15,000 --> 00:22:21,000
 Buddhism in its essence is the ultimate boredom.

289
00:22:21,000 --> 00:22:26,000
 It's not, but that's how it's easily perceived.

290
00:22:26,000 --> 00:22:30,000
 I mean, there's nothing fancy, there's nothing...

291
00:22:30,000 --> 00:22:33,370
 It's the core of Buddhism. Now the cultural expression of

292
00:22:33,370 --> 00:22:35,000
 Buddhism is often very fancy

293
00:22:35,000 --> 00:22:42,000
 and flowery and magical and mystical.

294
00:22:42,000 --> 00:22:46,000
 But when you get deeper into it and start talking to monks

295
00:22:46,000 --> 00:22:47,000
 and start hanging out in monasteries,

296
00:22:47,000 --> 00:22:51,000
 it's actually... it can be quite dull.

297
00:22:51,000 --> 00:22:54,000
 You're expected to sit still.

298
00:22:54,000 --> 00:23:00,000
 You're expected to look at very mundane things.

299
00:23:00,000 --> 00:23:03,660
 Most of my Thai students would rather be seeing bright

300
00:23:03,660 --> 00:23:07,000
 lights and angels or ghosts.

301
00:23:07,000 --> 00:23:10,270
 It's exciting for them. And for them that's Buddhism. But

302
00:23:10,270 --> 00:23:12,000
 it's not Buddhism.

303
00:23:12,000 --> 00:23:16,000
 It's not the Buddhist teaching.

304
00:23:16,000 --> 00:23:19,000
 And many people are turned off by this.

305
00:23:19,000 --> 00:23:24,300
 I've had people write away when they... in some of my

306
00:23:24,300 --> 00:23:25,000
 videos I think I used the wrong word

307
00:23:25,000 --> 00:23:28,570
 or I used a word that's easily taken the wrong word and

308
00:23:28,570 --> 00:23:32,000
 that's the word enjoy.

309
00:23:32,000 --> 00:23:37,370
 Because the word enjoy is such a positive thing for us to

310
00:23:37,370 --> 00:23:40,000
 enjoy something.

311
00:23:40,000 --> 00:23:43,000
 To enjoy an experience.

312
00:23:43,000 --> 00:23:47,000
 I would say things like instead of enjoying it.

313
00:23:47,000 --> 00:23:54,000
 But the problem with the word enjoyment is it means to...

314
00:23:54,000 --> 00:23:58,820
 it means it carries a connotation of experiencing something

315
00:23:58,820 --> 00:24:00,000
 to its fullest.

316
00:24:00,000 --> 00:24:03,180
 But that's not what the word means. The word means to like

317
00:24:03,180 --> 00:24:04,000
 something.

318
00:24:04,000 --> 00:24:07,410
 But we give it this connotation of experiencing it to the

319
00:24:07,410 --> 00:24:08,000
 fullest.

320
00:24:08,000 --> 00:24:11,960
 So people would say, "Look at what you're telling us to

321
00:24:11,960 --> 00:24:13,000
 block things out,

322
00:24:13,000 --> 00:24:19,000
 to avoid things, to not experience things to the fullest."

323
00:24:19,000 --> 00:24:30,000
 Because this is what the word enjoy means.

324
00:24:30,000 --> 00:24:35,000
 And so people say that this is dull, it's dry.

325
00:24:35,000 --> 00:24:39,290
 And people are so afraid, like afraid of things like nir

326
00:24:39,290 --> 00:24:40,000
vana.

327
00:24:40,000 --> 00:24:43,250
 I remember when I first heard about it, when I first

328
00:24:43,250 --> 00:24:44,000
 started practicing Buddhism,

329
00:24:44,000 --> 00:24:50,000
 that was the major hurdle for me, was this idea of nirvana.

330
00:24:50,000 --> 00:24:54,690
 Because that's like no more. No more sensuality, no more

331
00:24:54,690 --> 00:24:57,000
 pleasure, nothing.

332
00:24:57,000 --> 00:25:00,160
 I know theoretically it made some sense but I was like, "

333
00:25:00,160 --> 00:25:06,000
Man, that's so hard to grasp, so hard to accept."

334
00:25:06,000 --> 00:25:15,000
 And we think of non-enjoyment of pleasures.

335
00:25:15,000 --> 00:25:17,450
 People look at the monk's life and they think of it as such

336
00:25:17,450 --> 00:25:19,000
 a terrible austere life,

337
00:25:19,000 --> 00:25:23,000
 must be full of pain and suffering.

338
00:25:23,000 --> 00:25:26,000
 Many people even look at meditation this way.

339
00:25:26,000 --> 00:25:31,000
 I think of meditation as torture.

340
00:25:31,000 --> 00:25:35,000
 The idea of sitting still with your eyes closed.

341
00:25:35,000 --> 00:25:37,600
 And they make up all sorts of, I think it's just rational

342
00:25:37,600 --> 00:25:39,000
izing when they say,

343
00:25:39,000 --> 00:25:41,690
 "How pointless it is," and so on. I think what they really

344
00:25:41,690 --> 00:25:45,000
 mean is, "I can't do it."

345
00:25:45,000 --> 00:25:55,000
 It's torture. It's unpleasant. It's boring.

346
00:25:55,000 --> 00:25:59,540
 So I think this idiom applies to Buddhism, "The truth hurts

347
00:25:59,540 --> 00:26:00,000
."

348
00:26:00,000 --> 00:26:04,650
 It applies in the sense that people think this about

349
00:26:04,650 --> 00:26:07,000
 practices like Buddhism.

350
00:26:07,000 --> 00:26:12,000
 And again, we're claiming to understand the truth.

351
00:26:12,000 --> 00:26:18,270
 And I would say that's the way it is. People would much

352
00:26:18,270 --> 00:26:24,000
 rather wander around in illusion.

353
00:26:24,000 --> 00:26:28,540
 I teach on the internet in many different arenas, and one

354
00:26:28,540 --> 00:26:31,000
 of them is virtual reality.

355
00:26:31,000 --> 00:26:34,680
 So we go in and there's the Deer Park and we're sitting in

356
00:26:34,680 --> 00:26:36,000
 front of the Buddha.

357
00:26:36,000 --> 00:26:39,000
 And people are coming in with such costumes.

358
00:26:39,000 --> 00:26:44,270
 Men dressed up as women, women not wearing clothes, or very

359
00:26:44,270 --> 00:26:45,000
 little clothes.

360
00:26:45,000 --> 00:26:52,000
 We've got people with furry heads and tails and wings.

361
00:26:52,000 --> 00:26:56,160
 Why we would rather have this is because it's exactly what

362
00:26:56,160 --> 00:26:57,000
 we want.

363
00:26:57,000 --> 00:27:01,000
 We can get what we want. We can control it.

364
00:27:01,000 --> 00:27:06,000
 You can't control reality.

365
00:27:06,000 --> 00:27:09,000
 You can't compartmentalize reality as they said.

366
00:27:09,000 --> 00:27:11,000
 You can say, "This is good, this is bad."

367
00:27:11,000 --> 00:27:13,480
 But you can't say, "Let the bad not come and only the good

368
00:27:13,480 --> 00:27:14,000
 come."

369
00:27:14,000 --> 00:27:17,000
 In illusion you can do that.

370
00:27:17,000 --> 00:27:19,000
 Your illusion can be whatever you want.

371
00:27:19,000 --> 00:27:22,860
 You go into this virtual reality and you say, "I want this,

372
00:27:22,860 --> 00:27:25,000
 I want that, I don't want that."

373
00:27:25,000 --> 00:27:30,400
 And 90% of the experience is exactly what you'd like it to

374
00:27:30,400 --> 00:27:31,000
 be.

375
00:27:31,000 --> 00:27:34,000
 You have to work for it, you have to pay for it.

376
00:27:34,000 --> 00:27:40,000
 It becomes an addiction, but you can get it.

377
00:27:40,000 --> 00:27:48,000
 So we'd rather be in illusion as we say, "Truth hurts."

378
00:27:48,000 --> 00:27:51,670
 But I said it before, I'll say it again, truth doesn't hurt

379
00:27:51,670 --> 00:27:52,000
.

380
00:27:52,000 --> 00:27:57,000
 What hurts is our inability to accept the truth.

381
00:27:57,000 --> 00:28:00,000
 Buddhism isn't a boring religion. It isn't dry.

382
00:28:00,000 --> 00:28:04,000
 Being a monk, being a meditator isn't a painful experience.

383
00:28:04,000 --> 00:28:08,000
 There's nothing painful about sitting still.

384
00:28:08,000 --> 00:28:13,170
 What's painful is our inability to accept things for what

385
00:28:13,170 --> 00:28:14,000
 they are.

386
00:28:14,000 --> 00:28:19,760
 And that comes from our inability to understand things as

387
00:28:19,760 --> 00:28:21,000
 they are.

388
00:28:21,000 --> 00:28:24,030
 Once we understand things for what they are, we see they're

389
00:28:24,030 --> 00:28:25,000
 not scary.

390
00:28:25,000 --> 00:28:29,000
 They're not negative, they're not unpleasant.

391
00:28:29,000 --> 00:28:31,600
 We see that all of that pleasure and happiness we were

392
00:28:31,600 --> 00:28:33,000
 trying to attain

393
00:28:33,000 --> 00:28:37,000
 and chasing after and spending so much of our effort,

394
00:28:37,000 --> 00:28:43,000
 just to get the very smallest piece of,

395
00:28:43,000 --> 00:28:49,000
 all of that happiness could have been amplified hundreds

396
00:28:49,000 --> 00:28:50,000
 and hundreds,

397
00:28:50,000 --> 00:28:55,300
 thousands of times simply by coming to understand the

398
00:28:55,300 --> 00:28:58,000
 reality in front of us,

399
00:28:58,000 --> 00:29:00,000
 turning this reality into heaven.

400
00:29:00,000 --> 00:29:02,000
 Look at things as they are.

401
00:29:02,000 --> 00:29:04,000
 We see something, that's seeing.

402
00:29:04,000 --> 00:29:08,000
 We hear something, that's hearing.

403
00:29:08,000 --> 00:29:12,000
 It's not good, it's not bad, it's not me, it's not mine.

404
00:29:12,000 --> 00:29:17,000
 It's not us and them and good, right and wrong and so on.

405
00:29:17,000 --> 00:29:20,000
 It is what it is.

406
00:29:20,000 --> 00:29:27,000
 Right now there's experience going on every moment.

407
00:29:27,000 --> 00:29:30,000
 Once we understand this experience and we can live with it

408
00:29:30,000 --> 00:29:35,000
 and accept it for what it is, there's nothing anywhere,

409
00:29:35,000 --> 00:29:39,000
 at any time that can cause suffering for us.

410
00:29:39,000 --> 00:29:44,110
 We're said to be freed, we're said to be released from

411
00:29:44,110 --> 00:29:46,000
 bondage

412
00:29:46,000 --> 00:29:50,000
 and released from suffering.

413
00:29:50,000 --> 00:29:58,000
 So this is why we practice, why we torture ourselves,

414
00:29:58,000 --> 00:30:00,000
 go through all this pain and suffering.

415
00:30:00,000 --> 00:30:02,230
 Because we realize that it's not the meditation that's

416
00:30:02,230 --> 00:30:03,000
 causing our suffering,

417
00:30:03,000 --> 00:30:07,000
 it's our own minds, our inability to accept.

418
00:30:07,000 --> 00:30:24,000
 This very ordinary, very simple reality.

419
00:30:24,000 --> 00:30:25,000
 The truth doesn't hurt.

420
00:30:25,000 --> 00:30:27,000
 There's another idiom and that goes,

421
00:30:27,000 --> 00:30:30,000
 "The truth will set you free."

422
00:30:30,000 --> 00:30:32,830
 And that's exactly what is meant by the practice of the

423
00:30:32,830 --> 00:30:36,000
 Buddhist teaching.

424
00:30:36,000 --> 00:30:39,000
 When you realize the truth, it will set you free.

425
00:30:39,000 --> 00:30:43,000
 So that's the Dhamma I thought to give today.

426
00:30:43,000 --> 00:30:48,000
 Now we'll go on to the more practical part of the evening

427
00:30:48,000 --> 00:30:51,000
 and that's to do the meditation practice.

428
00:30:51,000 --> 00:30:54,830
 So first we'll do mindful prostration, then walking and

429
00:30:54,830 --> 00:30:56,000
 sitting.

430
00:30:57,000 --> 00:30:58,000
 Thank you.

