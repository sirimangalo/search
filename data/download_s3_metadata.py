#!/usr/bin/env python3

# from dateutil.parser import parse
# import datetime
import json
# import srt
# from glob import glob
# from os import path
# import re
import requests
import xmltodict
import urllib.parse

STORAGE_HOST="https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com"
STORAGE_PREFIX="podcast-mirror"

# def get_title(idx):
#     return idx

# def get_date(idx):
#     d = re.search(r'([0-9]{2,4}[-_]?[01][0-9][-_]?[0123][0-9])', idx)
#     if d:
#         for fmt in ['%Y%m%d', '%y%m%d']:
#             try:
#                 return str(datetime.datetime.strptime(d.group(0).split('_')[0], fmt))[:10].replace('-', '')
#             except Exception as e:
#                 pass

#         print('failed parse date', d.group(0))

#     return ''


if __name__ == "__main__":
    marker = ''
    s3_metadata = {}
    for i in range(50):
        page = xmltodict.parse(requests.get('%s?prefix=%s&marker=%s' % (STORAGE_HOST, STORAGE_PREFIX, marker)).text)

        for item in page['ListBucketResult']['Contents']:
            if not item['Key'].endswith('.mp3'):
                continue
            
            if 'podcast-mirror/diraudio/Yuttadhammo/Live/160221_vism.mp3' == urllib.parse.quote(item['Key']):
                continue

            if 'podcast-mirror/broadcast/160221_vism.mp3' ==  urllib.parse.quote(item['Key']):
                continue

            if 'podcast-mirror/diraudio/Mingun%20Sayadaw/CD2/track_2.mp3' == urllib.parse.quote(item['Key']):
                continue

            if 'podcast-mirror/diraudio/Mingun%20Sayadaw/CD2/track_1.mp3' == urllib.parse.quote(item['Key']):
                continue

            idx = urllib.parse.quote(item['Key']).replace('.mp3', '').split('/')[-1]

            if idx in s3_metadata and item['Size'] != s3_metadata[idx]['size']:
                print(urllib.parse.quote(item['Key']), s3_metadata[idx]['url'])
                continue

            s3_metadata[idx] = {
                'size': item['Size'],
                'audio_url': urllib.parse.quote(item['Key'])
            }

        if not 'NextMarker' in page['ListBucketResult']:
            break

        marker = page['ListBucketResult']['NextMarker'] 


        with open('s3_metadata.json', 'w') as f:
            f.write(json.dumps(s3_metadata))
